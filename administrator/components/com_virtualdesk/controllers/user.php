<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

jimport('joomla.application.component.helper'); // include libraries/application/component/helper.php

/**
 * VirtualDesk Controller
 */
class VirtualDeskControllerUser extends JControllerForm
{


     /**
	 * Method to save a record.  
	 *
	 * @param   string  $key     The name of the primary key of the URL variable.
	 * @param   string  $urlVar  The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return  boolean  True if successful, false otherwise.
	 *
	 * @since   12.2
	 */
	public function save($key = null, $urlVar = null)
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$lang  = JFactory::getLanguage();
		$model = $this->getModel();
		$table = $model->getTable();
		$data  = $this->input->post->get('jform', array(), 'array');
		$checkin = property_exists($table, 'checked_out');
		$context = "$this->option.edit.$this->context";
		$task = $this->getTask();

		// Determine the name of the primary key for the data.
		if (empty($key))
		{
			$key = $table->getKeyName();
		}


		// To avoid data collisions the urlVar may be different from the primary key.
		if (empty($urlVar))
		{
			$urlVar = $key;
		}

		$recordId = $this->input->getInt($urlVar);

		// Populate the row id from the session.
		$data[$key] = $recordId;

		// Access check.
		if (!$this->allowSave($data, $key))
		{
			$this->setMessage(JText::_('JLIB_APPLICATION_ERROR_SAVE_NOT_PERMITTED'),'error');
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_list
					. $this->getRedirectToListAppend(), false
				)
			);

			return false;
		}

		// Validate the posted data.
		// Sometimes the form needs some posted data, such as for plugins and modules.
		$form = $model->getForm($data, false);

        //var_dump($form);
        //exit();

		if (!$form)
		{
			$app->enqueueMessage($model->getError(), 'error');

			return false;
		}


        // Se o registo NÂO tem o IdJos (ainda não foi criado o utilizador joomla) então a password é obrigatória
        if( (!isset($data['idjos'])) || (intval($data['idjos'])==0) )
        { $form->setFieldAttribute('password', 'required', 'true');
        }


		// Test whether the data is valid.
		$validData = $model->validate($form, $data);


		// Check for validation errors.
		if ($validData === false)
		{
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'error');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'error');
				}
			}

			// Save the data in the session.
			$app->setUserState($context . '.data', $data);

			// Redirect back to the edit screen.
			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);

			return false;
		}

		if (!isset($validData['tags']))
		{
			$validData['tags'] = null;
		}


        // Cria utilizador joomla com os dados inseridos
        //var_dump($validData);
        //exit();

        if( (!isset($validData['idjos'])) || (intval($validData['idjos'])==0) )
        // new joomla user        	
        { $validUserInJoomla = $this->addNewJoomlaUser ($validData['name'], $validData['login'], $validData['email'], $validData['password']);
        }
        else
        // edit joomla user        	
        { $validUserInJoomla = $this->editJoomlaUser ($validData['name'], $validData['login'], $validData['email'], $validData['password'],$validData['idjos']);
          //JFactory::getApplication()->enqueueMessage('after editJoomlaUser -> validUserInJoomla '.$validUserInJoomla->id);
        }


        if ($validUserInJoomla===false) 
        {  
        	// Save the data in the session.
			$app->setUserState($context . '.data', $validData);
				
			 $this->setMessage(JText::_('COM_VIRTUALDESK_PROFILE_SAVE_FAILED'), 'error');

			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);

			return false;
        }


        // só a atualiza o IdJos se ainda não tiver nenhum associado
        if( (!isset($validData['idjos'])) || (intval($validData['idjos'])==0) )
        {  $validData['idjos'] = $validUserInJoomla->id;
        }
       

		// Attempt to save the data.
		if (!$model->save($validData))
		{
			// Save the data in the session.
			$app->setUserState($context . '.data', $validData);

			// Redirect back to the edit screen.
			$this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()), 'error');
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);

			return false;
		}

		// Save succeeded, so check-in the record.
		if ($checkin && $model->checkin($validData[$key]) === false)
		{
			// Save the data in the session.
			$app->setUserState($context . '.data', $validData);

			// Check-in failed, so go back to the record and display a notice.
			$this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()),'error');
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					. $this->getRedirectToItemAppend($recordId, $urlVar), false
				)
			);

			return false;
		}

		$langKey = $this->text_prefix . ($recordId == 0 && $app->isSite() ? '_SUBMIT' : '') . '_SAVE_SUCCESS';
		$prefix  = JFactory::getLanguage()->hasKey($langKey) ? $this->text_prefix : 'JLIB_APPLICATION';

		$this->setMessage(JText::_($prefix . ($recordId == 0 && $app->isSite() ? '_SUBMIT' : '') . '_SAVE_SUCCESS'));

		// Redirect the user and adjust session state based on the chosen task.
         // Clear the record id and data from the session.
		$this->releaseEditId($context, $recordId);
		$app->setUserState($context . '.data', null);

		// Redirect to the list screen.
		$this->setRedirect(
			JRoute::_(
				'index.php?option=' . $this->option . '&view=' . $this->view_list
				. $this->getRedirectToListAppend(), false
			)
		);


		// Invoke the postSave method to allow for the child class to access the model.
		$this->postSaveHook($model, $validData);

		//JFactory::getApplication()->enqueueMessage('save -> final return true... ');
		return true;
	}


/*
* Adiciona novo utilizador e verifica se retornou tudo ok
*/
protected function addNewJoomlaUser ($name, $login, $email, $password)
{ 

   $userSave = $this->saveNewJoomlaUser ($name, $login, $email, $password);
   
   // Se é Novo valida se retornou o ID de user no joomla
   if (!isset($userSave->id))
        {  
           $this->setMessage(JText::_('COM_VIRTUALDESK_INVALID_USER'), 'error');
	       return false;
        }

   return  $userSave; 

}



/*
* Grava novo utilizador no joomla 
*/
protected function saveNewJoomlaUser ($name, $login, $email, $password)
{ 

 $firstname = $name; // generate $firstname
 $lastname = ''; // generate $lastname
 $username = $login; // username is the same as email

 /*
 I handle this code as if it is a snippet of a method or function!!
 First set up some variables/objects     */

 // get the ACL
 $acl =& JFactory::getACL();

 /* get the com_user params */

 $usersParams = &JComponentHelper::getParams( 'com_users' ); // load the Params

 // "generate" a new JUser Object
 $user = JFactory::getUser(0); // it's important to set the "0" otherwise your admin user information will be loaded

 $data = array(); // array for all user settings

 // get the default usertype
 $usertype = $usersParams->get( 'new_usertype' );
 //$defaultUserGroup = $userConfig->get('new_usertype', 2);
 if (!$usertype) {
     $usertype = 'Registered';
 }

 // set up the "main" user information

 //original logic of name creation
 //$data['name'] = $firstname.' '.$lastname; // add first- and lastname
 $data['name'] = $firstname.$lastname; // add first- and lastname

 $data['username'] = $username; // add username
 $data['email'] = $email; // add email
 //$data['gid'] = $acl->get_group_id( '', $usertype, 'ARO' );   generate the gid from the usertype
 $data['groups'] = array($usertype);
 //$data['gid'] = array($defaultUserGroup);

 /* no need to add the usertype, it will be generated automaticaly from the gid */

 $data['password']  = $password; // set the password
 $data['password2'] = $password; // confirm the password
 $data['sendEmail'] = 0; // should the user receive system mails?

 /* Now we can decide, if the user will need an activation */

 //$useractivation = $usersParams->get( 'useractivation' ); // in this example, we load the config-setting
 $useractivation = 1;

 if ($useractivation == 1) { // yeah we want an activation

     jimport('joomla.user.helper'); // include libraries/user/helper.php
     $data['block'] = 1; // block the User
     $data['activation'] =JApplication::getHash(JUserHelper::genRandomPassword()); // set activation hash (don't forget to send an activation email)

 }
 else { // no we need no activation
     $data['block'] = 0; // don't block the user
 }

 if (!$user->bind($data)) { // now bind the data to the JUser Object, if it not works....

     JError::raiseWarning('', JText::_( $user->getError())); // ...raise an Warning
     return false; // if you're in a method/function return false

 }

 if (!$user->save()) { // if the user is NOT saved...

     JError::raiseWarning('', JText::_( $user->getError())); // ...raise an Warning
     return false; // if you're in a method/function return false

 }
 //var_dump($user);
 //exit();
 return $user; // else return the new JUser object

 }


/*
* Edita utilizador joomla
*/
protected function editJoomlaUser ($name, $login, $email, $password, $idjos)
{ 
  $userSave = $this->saveExistingJoomlaUser ($name, $login, $email, $password, $idjos);

  //JFactory::getApplication()->enqueueMessage('editJoomlaUser -> userSave '.$userSave->id);

  return $userSave;
}


/*
* Altera dados de utilizador no joomla 
*/
protected function saveExistingJoomlaUser ($name, $login, $email, $password, $idjos)
{ 

 $firstname = $name; // generate $firstname
 $lastname  = ''; // generate $lastname
 $username  = $login; // username is the same as email
 
 // get JUser Object
 $user = JFactory::getUser($idjos); 

 if( (!isset($user->id)) || (intval($user->id)==0) ) return false;

 $data = array(); // array for all user settings
 
 // set up the "main" user information

 //original logic of name creation
 $data['name']     = $firstname.$lastname; // add first- and lastname
 $data['username'] = $username; // username
 $data['email']    = $email; //  email

 /* no need to add the usertype, it will be generated automaticaly from the gid */
 if( isset($password) && ((string)$password!="") )
 { $data['password']  = $password; // set the password
   $data['password2'] = $password; // confirm the password
 }
 
 /* Now we can decide, if the user will need an activation */
 //$useractivation = 0;
 //$data['block'] = 1; // don't block the user

 if (!$user->bind($data)) { // now bind the data to the JUser Object, if it not works....
     JError::raiseWarning('', JText::_( $user->getError())); // ...raise an Warning
     return false; // if you're in a method/function return false
 }

 if (!$user->save()) { // if the user is NOT saved...
     JError::raiseWarning('', JText::_( $user->getError())); // ...raise an Warning
     return false; // if you're in a method/function return false
 }

 //Debug
 //JFactory::getApplication()->enqueueMessage('save existing josis '.$idjos.' - '.$user->id);

 return $user; // else return the new JUser object

 }


}