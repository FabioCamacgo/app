<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');
//use Joomla\Utilities\ArrayHelper;

/**
 * VirtualDesks Controller
 */
class VirtualDeskControllerEventLogs extends JControllerAdmin
{

    public function __construct($config = array())
    {
        parent::__construct($config);
        // Registamos a tarefa e mapeamos para um método existente
        //$this->registerTask('disable', 'enable');
    }


	/**
	 * Proxy for getModel.
	 *
	 * @since       2.5
	 */
	public function getModel($name = 'EventLogs', $prefix = 'VirtualDeskModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}


//    /*
//    * Teste para ver o views + layouts
//   */
//    public function viewlayout3()
//    {
//        // get view
//        $view = $this->getView('eventlogs','html');
//
//        // set model
//        $view->setModel($this->getModel('eventlogs'), true);
//        //$view->setModel($this->getModel('useractivationlogs'), true);
//
//        $view->items = $view->get('Items');
//        $view->setLayout('layout3');
//        $view->display2();
//
//        //$redirectTo = JRoute::_('index.php?option='.JRequest::getVar('option'));
//        //$redirectTo = JRoute::_('index.php?option='. JFactory::getApplication()->input->get('option')."&".'view='.JFactory::getApplication()->input->get('view'), false);
//        //$this->setRedirect($redirectTo);
//    }
//

	/*
	 * Para testar o redirect a partir da toolbar
//	 */
//    public function redirect2logs()
//    {
//        $redirectTo = JRoute::_('index.php?option='. JFactory::getApplication()->input->get('option')."&".'view=useractivationlogs', false);
//        $this->setRedirect($redirectTo);
//    }

}