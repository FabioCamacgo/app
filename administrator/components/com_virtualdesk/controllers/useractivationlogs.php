<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');


  /**
 * VirtualDesks Controller
 * @since
 */
class VirtualDeskControllerUserActivationLogs extends JControllerAdmin
{

	/**
	 * Proxy for getModel.
	 * @param $name
     * @param $prefix
     * @param $config
     *
     * @return $model
     *
	 * @since       2.5
	 */
	public function getModel($name = 'UserActivationLogs', $prefix = 'VirtualDeskModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}