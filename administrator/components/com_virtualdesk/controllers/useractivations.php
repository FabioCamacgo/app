<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');
use Joomla\Utilities\ArrayHelper;

/**
 * VirtualDesks Controller
 */
class VirtualDeskControllerUserActivations extends JControllerAdmin
{

    public function __construct($config = array())
    {
        parent::__construct($config);
        // Registamos a tarefa e mapeamos para um método existente
        $this->registerTask('disable', 'enable');
    }

    /*
     * task enable: permite ligar/desligar um link de activação
     * por exemplo para ser invocada da lista de activações
     */
    public function enable()
    {
        // Initialise variables.
        //$user   = JFactory::getUser();
        $ids    = JRequest::getVar('cid', array(), '', 'array');
        $values = array('enable' => 1, 'disable' => 0);
        $task   = $this->getTask();
        $value  = ArrayHelper::getValue($values, $task, 0, 'int');
        if (empty($ids)) {
            JError::raiseWarning(500, JText::_('JERROR_NO_ITEMS_SELECTED'));
        }
        else {
            // Get the model.
            $model = $this->getModel('useractivations');
            // Publish the items.
            if (!$model->enable($ids, $value)) {
                JError::raiseWarning(500, $model->getError());
            }
        }

        //$redirectTo = JRoute::_('index.php?option='.JRequest::getVar('option'));
        $redirectTo = JRoute::_('index.php?option='. JFactory::getApplication()->input->get('option')."&".'view='.JFactory::getApplication()->input->get('view'), false);

        $this->setRedirect($redirectTo);
    }

	/**
	 * Proxy for getModel.
	 *
	 * @since       2.5
	 */
	public function getModel($name = 'UserActivations', $prefix = 'VirtualDeskModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}


	/*
	 * Para testar o redirect a partir da toolbar
	 */
    public function redirect2logs()
    {
        $redirectTo = JRoute::_('index.php?option='. JFactory::getApplication()->input->get('option')."&".'view=useractivationlogs', false);
        $this->setRedirect($redirectTo);
    }

}