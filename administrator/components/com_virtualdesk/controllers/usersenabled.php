<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');
jimport('joomla.application.component.controlleradmin');
use Joomla\Utilities\ArrayHelper;

/**
 * VirtualDesks Controller
 */
class VirtualDeskControllerUsersEnabled extends JControllerAdmin
{

    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @since   1.6
     * @see     JController
     */
    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->registerTask('block', 'changeUserBlock');
        $this->registerTask('unblock', 'changeUserBlock');
    }


	/**
	 * Proxy for getModel.
	 *
	 * @since       2.5
	 */
	public function getModel($name = 'User', $prefix = 'VirtualDeskModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}


    public function block()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
    }


    /**
     * Method to bloc a record.
     *
     * @return  void
     *
     * @since   1.6
     */
    public function changeUserBlock()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $ids    = $this->input->get('cid', array(), 'array');
        $values = array('block' => 1, 'unblock' => 0);
        $task   = $this->getTask();
        $value  = JArrayHelper::getValue($values, $task, 0, 'int');

        if (empty($ids))
        {
            JError::raiseWarning(500, JText::_('COM_VIRTUALDESK_USERS_NO_ITEM_SELECTED'));
        }
        else
        {
            // Get the model.
            $model = $this->getModel('Users');

            // Change the state of the records.
            if (!$model->block($ids, $value))
            {
                JError::raiseWarning(500, $model->getError());
            }
            else
            {
                if ($value == 1)
                {
                    if(count($ids)>1) {
                        $this->setMessage(JText::plural('COM_VIRTUALDESK_N_USERS_BLOCKED', count($ids)));
                    }
                    else {
                        $this->setMessage(JText::_('COM_VIRTUALDESK_USERS_BLOCKED'));
                    }
                }
                elseif ($value == 0)
                {   if(count($ids)>1) {
                    $this->setMessage(JText::plural('COM_VIRTUALDESK_N_USERS_UNBLOCKED', count($ids)));
                    }
                    else
                    {
                        $this->setMessage(JText::_('COM_VIRTUALDESK_USERS_UNBLOCKED'));
                    }
                }
            }
        }


        $this->setRedirect('index.php?option=com_virtualdesk&view=usersenabled');

    }

}