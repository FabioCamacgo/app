<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

jimport('joomla.application.component.helper'); // include libraries/application/component/helper.php

/**
 * VirtualDesk Controller
 * @since
 */
class VirtualDeskControllerUserActivation extends JControllerForm
{


	/**
	 * Proxy for getModel.
	 *
	 * @since       2.5
	 */
	public function getModel($name = 'UserActivation', $prefix = 'VirtualDeskModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}


   /**
    * 
   * Method to check if you can save a new or existing record.
   *
   * Overrides JControllerForm::allowSave to check the core.admin permission.	
   */
	/*protected function allowSave($data, $key = 'id')
	{
		return (JFactory::getUser()->authorise('core.admin', $this->option) && parent::allowSave($data, $key));
	}*/


	/**
	* @since
	*/
    public function sendconfirm()
    {
		//check if jToken is ok.
		if (!JSession::checkToken('get'))
		{
		   JFactory::getApplication()->redirect('index.php', JText::_('JINVALID_TOKEN'));
		}

		$jinput = JFactory::getApplication()->input;
		$idUserParam = $jinput->get->get('iduser','');

		$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=useractivation&layout=sendconfirm&iduser='.$idUserParam, false));
        return true;
    }


/*
* Create a activation link with a token and send it by email for the user
*/
public function send()
	{
		// Check the request token.
		JSession::checkToken('post') or jexit(JText::_('JINVALID_TOKEN'));


		//ToDo: Check if user can -> Grupo VD Admin or Super Admin

		//ToDo: Update VDusers + Joomla user -> put them not activated and blocked


		$app   = JFactory::getApplication();
		$model = $this->getModel('UserActivation', 'VirtualDeskModel');
		$data  = $this->input->post->get('jform', array(), 'array');

		// Submit activation request.
		$return = $model->processSend($data);

		// Check for a hard error.
		if ($return instanceof Exception)
		{
			// Get the error message to display.
			if ($app->get('error_reporting'))
			{
				$message = $return->getMessage();
			}
			else
			{
				$message = JText::_('COM_VIRTUALDESK_USERACTIVATION_SEND_ERROR');
			}

			// Get the route to the next page.
			$route  = 'index.php?option=com_virtualdesk&view=users';

			// Go back to the request form.
			$this->setRedirect(JRoute::_($route, false), $message, 'error');

			return false;
		}
		elseif ($return === false)
		{
			$route  = 'index.php?option=com_virtualdesk&view=useractivation&layout=sendconfirm';
			// Go back to the request form.
			$message = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_FAILED', $model->getError());
			$this->setRedirect(JRoute::_($route, false), $message, 'notice');
			return false;
		}
		else
		{
			// The request succeeded.
			// Get the route to the next page.
			$route  = 'index.php?option=com_virtualdesk&view=users';

//			// Proceed to step two.
//			$message = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_OK');

			// give joomla the message to display...
			$app->enqueueMessage(JText::_('COM_VIRTUALDESK_USERACTIVATION_OK'), 'Success' );

			$this->setRedirect(JRoute::_($route, false), '');

			return true;
		}
	}


	/**
	 * Method to cancel an edit.
	 * @since
	 */
	public function cancel($key = null)
	{
		$return = parent::cancel($key);

		// Redirect to the main page.
		$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=users', false));

		return $return;
	}

}