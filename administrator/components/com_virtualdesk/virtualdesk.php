<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// TESTE

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


if (!JFactory::getUser()->authorise('core.manage', 'com_virtualdesk'))
{
    throw new JAccessExceptionNotallowed(JText::_('JERROR_ALERTNOAUTHOR'), 403);
}


// Get an instance of the controller prefixed
$controller = JControllerLegacy::getInstance('VirtualDesk');
 
// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));
 
// Redirect if set by the controller
$controller->redirect();