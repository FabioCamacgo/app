<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

//JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');

/**
 * Model
 *
 * @since  0.0.1
 */
class VirtualDeskModelUserActivations extends JModelList
{

	public function __construct ($config = array())
	{	if (empty($config['filter_fields']))
		{	$config['filter_fields'] = array(
                'id', 'a.id',
                'name', 'b.name',
				'login', 'a.login',
				'email', 'a.email',
				'enabled', 'a.enabled',
                'activated', 'a.activated',
                'created', 'a.created',
                'modified', 'a.modified'
        	);
    }
		parent::__construct($config);
	}


	protected function populateState ($ordering = null, $direction = null)
	{
        $app = JFactory::getApplication('administrator');

        // Adjust the context to support modal layouts.
        if ($layout = $app->input->get('layout', 'default', 'cmd'))
        {
            $this->context .= '.' . $layout;
        }

        // Load the filter state.
        $this->setState('filter.search', $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search', '', 'string'));
        $this->setState('filter.enabled', $this->getUserStateFromRequest($this->context . '.filter.enabled', 'filter_enabled', '', 'cmd'));
        $this->setState('filter.activated', $this->getUserStateFromRequest($this->context . '.filter.activated', 'filter_activated', '', 'cmd'));

        // Load the parameters.
        $params = JComponentHelper::getParams('com_virtualdesk');
        $this->setState('params', $params);

        // List state information.
        parent::populateState($ordering, $direction);

	}



	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{	// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.enabled');
        $id .= ':' . $this->getState('filter.activated');
		return parent::getStoreId($id);
	}



	/*
	* Method to build an SQL query to load the list data.
	*
	* @return      string  An SQL query
	*/
	protected function getListQuery()
	{
		// Initialize variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
//		$user  = JFactory::getUser();
//		$app = JFactory::getApplication();

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.iduser, b.name, a.login, a.email, a.enabled, a.activated, a.created, a.modified, a.createdbyuser, a.createdbyip, a.activatedbyip'
			)
		);

		// DB table
		$query->from('#__virtualdesk_users_activation AS a');
        $query->join('LEFT', $db->quoteName('#__virtualdesk_users', 'b') . ' ON (' . $db->quoteName('a.iduser') . ' = ' . $db->quoteName('b.id') . ')' );

            // Filter by search in title.
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{   $search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
				$query->where('(a.login LIKE ' . $search . ') OR (a.email LIKE ' . $search . ') OR (b.name LIKE ' . $search . ')');
			}
		}


        // Filter: enabled
        $enabled = $db->escape($this->getState('filter.enabled'));
        if (is_numeric($enabled)) {
            $query->where('a.enabled = ' . (int) $enabled);
        }
        elseif ($enabled === '') {
            $query->where('(a.enabled = 0 OR a.enabled = 1)');
        }

        // Filter: Activated
        $enabled = $db->escape($this->getState('filter.activated'));
        if (is_numeric($enabled)) {
            $query->where('a.activated = ' . (int) $enabled);
        }
        elseif ($enabled === '') {
            $query->where('(a.activated = 0 OR a.activated = 1)');
        }


		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'DESC');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;

	}



	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */
	public function getItems()
	{
		$items = parent::getItems();
		if (JFactory::getApplication('administrator')->isSite())
		{
			$user = JFactory::getUser();
			$groups = $user->getAuthorisedViewLevels();

			for ($x = 0, $count = count($items); $x < $count; $x++)
			{
				// Check the access level. Remove articles the user shouldn't see
				if (!in_array($items[$x]->access, $groups))
				{
					unset($items[$x]);
				}
			}
		}

		return $items;
	}


     /*
     * faz enable/disable do user activation
     */
    function enable($cid,$enable) {
        if (count( $cid ))
        {   $app         = JFactory::getApplication('administrator');

            // Verifica se o utilizador atual é admin ou superadmin... caso contrário não deixa editar
            $user = JFactory::getUser();
            if(!VirtualDeskUserHelper::checkIfUserIsAdmin($user->id))
            {   $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CHECK_PERMISSIONS'), 'error');
                return false;
            }
            // verifica se pode editar...  NÂO IMPLEMENTEI porque a condição em cima de Admin é mais "forte"
            // if ($user->authorise('core.edit.own', 'com_content'))

            // Verifica o token de sessão
            if(!JSession::checkToken()) return false;

            $table = JTable::getInstance('UserActivation', 'VirtualDeskTable');
            $table->load($cid[0]);

            //check if table row id was loaded..
            if (isset($table->id) && (integer)$table->id == $cid[0]) {
                $data = array();
                $data['enabled'] = (integer) $enable;
                $data['activatedbyip'] = $app->input->server->get('REMOTE_ADDR');
                $data['modified'] = JFactory::getDate()->format("Y-m-d H:i:s");

                if (!$table->save($data)) {
                    $vdlog = new VirtualDeskLogHelper();
                    // erro ao gravar activação
                    $app->enqueueMessage(JText::_('COM_VIRTUALDESK_ACTIVATION_INSERTLOG_ERROR'), 'error');
                    $logdata['logstatus'] = 'ERR';
                    $logdata['logdesc'] = JText::_('erro ao gravar activação');
                    $vdlog->userActivationInsertLog($logdata);
                    return false;
                }
            }
        }
        return true;
    }

}