<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

//JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');

/**
 * Model
 *
 * @since  0.0.1
 */
class VirtualDeskModelEventLogs extends JModelList
{

	public function __construct ($config = array())
	{	if (empty($config['filter_fields']))
		{	$config['filter_fields'] = array(
                'id', 'a.id',
				'iduser', 'a.iduser',
				'accesslevel', 'a.accesslevel',
				'category', 'a.category',
                'title', 'a.title',
                'desc', 'a.desc',
                'created', 'a.created'
        	);
    }
		parent::__construct($config);
	}


	protected function populateState ($ordering = null, $direction = null)
	{
        $app = JFactory::getApplication('administrator');

        // Adjust the context to support modal layouts.
        if ($layout = $app->input->get('layout', 'default', 'cmd'))
        {
            $this->context .= '.' . $layout;
        }

        // Load the filter state.
        $this->setState('filter.search', $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search', '', 'string'));
        $this->setState('filter.accesslevel', $this->getUserStateFromRequest($this->context . '.filter.accesslevel', 'filter_accesslevel', '', 'cmd'));
        $this->setState('filter.category', $this->getUserStateFromRequest($this->context . '.filter.category', 'filter_category', '', 'cmd'));

        // Load the parameters.
        $params = JComponentHelper::getParams('com_virtualdesk');
        $this->setState('params', $params);

        // set limit
        $this->setState('list.limit', 50);
        $this->state->set('list.limit', 55);

        // List state information.
        parent::populateState($ordering, $direction);

	}



	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{	// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.accesslevel');
        $id .= ':' . $this->getState('filter.category');
		return parent::getStoreId($id);
	}



	/*
	* Method to build an SQL query to load the list data.
	*
	* @return      string  An SQL query
	*/
	protected function getListQuery()
	{
		// Initialize variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
//		$user  = JFactory::getUser();
//		$app = JFactory::getApplication();

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.iduser, a.accesslevel AS idaccesslevel, a.category AS idcategory, a.title, a.desc, a.created, a.ip, b.name as category, c.name as accesslevel, d.email, d.login'
			)
		);

		// DB table
		$query->from('#__virtualdesk_eventlog AS a')
                ->join('LEFT', $db->quoteName('#__virtualdesk_eventlog_category', 'b') . ' ON (' . $db->quoteName('a.category') . ' = ' . $db->quoteName('b.id') . ')')
                ->join('LEFT', $db->quoteName('#__virtualdesk_eventlog_accesslevel', 'c') . ' ON (' . $db->quoteName('a.accesslevel') . ' = ' . $db->quoteName('c.id') . ')')
            ->join('LEFT', $db->quoteName('#__virtualdesk_users', 'd') . ' ON (' . $db->quoteName('a.iduser') . ' = ' . $db->quoteName('d.id') . ')');

		// Filter by search in title.
		$search = $this->getState('filter.search');
		if (!empty($search))
		{	if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{   $search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
				$query->where('(a.title LIKE ' . $search . ') OR (a.desc LIKE ' . $search . ') ');
			}
		}


        // Filter accesslevel
        $accesslevel = $db->escape($this->getState('filter.accesslevel'));
        if (is_numeric($accesslevel)) {
            $query->where('a.accesslevel = ' . (int) $accesslevel);
        }


        // Filter category
        $category = $db->escape($this->getState('filter.category'));
        if (is_numeric($category)) {
            $query->where('a.category = ' . (int) $category);
        }


		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'DESC');


		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;

	}



	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */
	public function getItems()
	{
		$items = parent::getItems();
		if (JFactory::getApplication('administrator')->isSite())
		{
			$user = JFactory::getUser();
			$groups = $user->getAuthorisedViewLevels();
			for ($x = 0, $count = count($items); $x < $count; $x++)
			{
				// Check the access level. Remove articles the user shouldn't see
				if (!in_array($items[$x]->access, $groups))
				{
					unset($items[$x]);
				}
			}
		}
		return $items;
	}



//    public function getLogsLayout1()
//    {
//
//        $db    = JFactory::getDbo();
//        $db->setQuery(self::getListQuery());
//        $items = $db->loadObjectList();
//
//        if (JFactory::getApplication('administrator')->isSite())
//        {
//            $user = JFactory::getUser();
//            $groups = $user->getAuthorisedViewLevels();
//            for ($x = 0, $count = count($items); $x < $count; $x++)
//            {
//                // Check the access level. Remove articles the user shouldn't see
//                if (!in_array($items[$x]->access, $groups))
//                {
//                    unset($items[$x]);
//                }
//            }
//        }
//        return $items;
//    }


}