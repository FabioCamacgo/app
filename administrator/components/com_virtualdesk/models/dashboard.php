<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskStatsHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskstats.php');

/**
 * VirtualDesk Model
 */
class VirtualDeskModelDashboard extends JModelAdmin
{
    public function getForm($data = array(), $loadData = true)
    {

        VirtualDeskUserHelper::checkVirtualDeskGroups();

        return false;
    }


    public function getUserStats()
    {
        $UserStats = array();

        $UserStats['NumberOfUsers']         =  VirtualDeskStatsHelper::getNumberOfActiveUsers();
        $UserStats['NumberOfInactiveUsers'] =  VirtualDeskStatsHelper::getNumberOfInactiveUsers();

        return $UserStats;
    }



}