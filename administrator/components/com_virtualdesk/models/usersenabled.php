<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskTableUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/user.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
/**
 * Model
 *
 * @since  0.0.1
 */
class VirtualDeskModelUsersEnabled extends JModelList
{

	public function __construct ($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
                'id','a.id',
                'name','a.name',
                'login','a.login',
                'email','a.email',
                'blocked','a.blocked',
                'activated','a.activated'
        	);
    }

		parent::__construct($config);
	}


	protected function populateState ($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		$forcedLanguage = $app->input->get('forcedLanguage', '', 'cmd');

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}

		// Adjust the context to support forced languages.
		if ($forcedLanguage)
		{
			$this->context .= '.' . $forcedLanguage;
		}

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
        $this->setState('filter.blocked', $this->getUserStateFromRequest($this->context . '.filter.blocked', 'filter_blocked', '', 'cmd'));
        $this->setState('filter.activated', $this->getUserStateFromRequest($this->context . '.filter.activated', 'filter_activated', '', 'cmd'));


        // List state information.
		parent::populateState($ordering, $direction);

		// Force a language
		if (!empty($forcedLanguage))
		{
			$this->setState('filter.language', $forcedLanguage);
			$this->setState('filter.forcedLanguage', $forcedLanguage);
		}

	}



	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.name');
        $id .= ':' . $this->getState('filter.blocked');
        $id .= ':' . $this->getState('filter.activated');

		return parent::getStoreId($id);
	}



	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{

		// Initialize variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
//		$user  = JFactory::getUser();
//		$app = JFactory::getApplication();


		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.name, a.login, a.email, a.blocked, a.activated'
			)
		);

		// DB table
		$query->from('#__virtualdesk_users AS a');


		// Filter by search in title.
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
				$query->where('( (a.name LIKE ' . $search . ') ' . ' OR (a.login LIKE ' . $search . ')' .' OR (a.email LIKE ' . $search . ') )');
			}
		}



		$query->where(' ( a.blocked = 0 ) ');


//
//            // Filter: BLOCKED
//            $blocked = $db->escape($this->getState('filter.blocked'));
//            if (is_numeric($blocked)) {
//                $query->where('a.blocked = ' . (int)$blocked);
//            } elseif ($blocked === '') {
//                $query->where('(a.blocked = 0 OR a.blocked = 1)');
//            }
//
//            // Filter: ACTIVATED
//            $activated = $db->escape($this->getState('filter.activated'));
//            if (is_numeric($activated)) {
//                $query->where('a.activated = ' . (int)$activated);
//            } elseif ($activated === '') {
//                $query->where('(a.activated = 0 OR a.activated = 1)');
//            }



		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'desc');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;

	}



	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */
	public function getItems()
	{
		$items = parent::getItems();

		if (JFactory::getApplication()->isSite())
		{
			$user = JFactory::getUser();
			$groups = $user->getAuthorisedViewLevels();

			for ($x = 0, $count = count($items); $x < $count; $x++)
			{
				// Check the access level. Remove articles the user shouldn't see
				if (!in_array($items[$x]->access, $groups))
				{
					unset($items[$x]);
				}
			}
		}

		return $items;
	}


   /*
   * faz block/unblock do User
    * block no VD users + jos user e faz disabled de todas as ativações
   */
//   function block($cid,$enable)
//   {
//       if (count($cid)) {
//           $app = JFactory::getApplication('administrator');
//
//           // Verifica se o utilizador atual é admin ou superadmin... caso contrário não deixa editar
//           $user = JFactory::getUser();
//           if (!VirtualDeskUserHelper::checkIfUserIsAdmin($user->id)) {
//               $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CHECK_PERMISSIONS'), 'error');
//               return false;
//           }
//
//           // Verifica o token de sessão
//           if (!JSession::checkToken()) return false;
//
//
//           $dataJosUser            = array();
//           $dataVDUser             = array();
//           $IdUserVD               = $cid[0];
//           $dataJosUser['block']   = $enable;
//           $dataVDUser['blocked']  = $enable;
//
//           // Carrega dados atuais na base de dados para da tabela VDUsers
//           $db = JFactory::getDbo();
//           $origemTableVDUser = new VirtualDeskTableUser($db);
//           $origemTableVDUser->load(array('id' => $IdUserVD));
//           if ((integer)$origemTableVDUser->id !== (integer)$IdUserVD) return false;
//           $updateTableVDUser = clone  $origemTableVDUser;
//
//           //Carrega dados atuais do user joomla associado
//           $UserJoomlaID = $origemTableVDUser->idjos;
//           $origemTableJosUser = JFactory::getUser($UserJoomlaID);
//           if ((!isset($origemTableJosUser->id)) || (intval($origemTableJosUser->id) == 0)) return false;
//           $updateTableJosUser = clone $origemTableJosUser;
//
//           //UPDATE: JOS User
//           if (!empty($dataJosUser)) {
//               if (!$updateTableJosUser->bind($dataJosUser)) return false;
//               if (!$updateTableJosUser->save()) return false;
//           }
//
//           //UPDATE: VD User
//           // Bind the data.
//           $resVDUserBind = $updateTableVDUser->bind($dataVDUser);
//           if (!$resVDUserBind) {   // Reverte gravação anterior do joomla user associado
//               $origemTableJosUser->save();
//               return false;
//           }
//           // Store the data.
//           $resVDUserSave = $updateTableVDUser->save($dataVDUser);
//           if (!$resVDUserSave) {    // Reverte gravação anterior do joomla user associado
//               $origemTableJosUser->save();
//               return false;
//           }
//
//           // Se está a bloquear o utilizador, deve procurar todas as ativações pendentes e desligar ...
//           // pois se é para bloquear não faz sentido ter ativações
//           if($enable == 1) {
//               $query = $db->getQuery(true);
//               // Fields to update.
//               $fields = array(
//                   $db->quoteName('enabled') . ' = 0'
//               );
//               // Conditions for which records should be updated.
//               $conditions = array(
//                   $db->quoteName('iduser') . ' = ' . $IdUserVD,
//                   $db->quoteName('idjos') . ' = ' . $UserJoomlaID,
//                   $db->quoteName('enabled') . ' = 1'
//               );
//               $query->update($db->quoteName('#__virtualdesk_users_activation'))->set($fields)->where($conditions);
//               $db->setQuery($query);
//               $db->execute();
//           }
//
//
//           // Registar no evento de LOGS
//
//           $vdlog                 = new VirtualDeskLogHelper();
//           $eventdata              = array();
//           $eventdata['iduser']    = $IdUserVD;
//           $eventdata['idjos']     = $UserJoomlaID;
//           $eventdata['sendEmail'] = true;
//           $eventdata['title']     = JText::_('COM_VIRTUALDESK_EVENTLOG_USERENABLE_TITLE');
//           if($enable==1) {
//               $eventdata['desc']  = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_USERDISABLE_BY',$updateTableJosUser->username, $IdUserVD, JFactory::getUser()->username);
//           }
//           else {
//               $eventdata['desc']  = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_USERENABLE_BY',$updateTableJosUser->username, $IdUserVD, JFactory::getUser()->username);
//           }
//           $eventdata['category']  = JText::_('COM_VIRTUALDESK_EVENTLOG_USERENABLE_CAT');
//           $vdlog->insertEventLog($eventdata);
//
//
//
//           return true;
//       }
//   }

}