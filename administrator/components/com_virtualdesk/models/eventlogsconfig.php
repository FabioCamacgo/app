<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modeladmin');

/**
 * Model
 *
 * @since  0.0.1
 */
class VirtualDeskModelEventLogsConfig extends JModelAdmin
{

    public function getForm($data = array(), $loadData = true)
    {
        return false;
    }

    public function getAccessLevels()
    {
        $db    = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('id,name,modified');
        $query->from('#__virtualdesk_eventlog_accesslevel');
        $db->setQuery((string) $query);
        $res = $db->loadObjectList();
        return $res;
    }


    public function getCategories()
    {
        $db    = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('id,name,modified');
        $query->from('#__virtualdesk_eventlog_category');
        $db->setQuery((string) $query);
        $res = $db->loadObjectList();
        return $res;
    }


}