<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * HelloWorldList Model
 *
 * @since  0.0.1
 */
class VirtualDeskModelRequests extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Initialize variables.
		$db    = JFactory::getDbo();
		
		/*SELECT r.id, sub.id as subid, sub.title, sub.type, sub.`value`
		FROM
		frm_facileforms_records as r
		INNER JOIN 
		(
			select id, title, type, value, record 
			from frm_facileforms_subrecords 

		) sub ON r.id = sub.record */

       $subQuery = $db->getQuery(true);
	   $query    = $db->getQuery(true);

		// Create the base subQuery select statement.
		$subQuery->select(array('id', 'title', 'type', 'value','record'))
		         ->from($db->quoteName('#__facileforms_subrecords'));
		         //->where('type = \'File Upload\'');
		   
		// Create the base select statement.
		$query->select(array('r.form','r.id', 'sub.id as subid', 'sub.title as title', 'sub.type as type','sub.value as value'))
		    ->from($db->quoteName('#__facileforms_records').' AS r')
		    ->join('INNER', '(' . $subQuery .') sub ON  r.id = sub.record')
		    ->where('r.form = 15');
            
 

       return $query;


	}
}