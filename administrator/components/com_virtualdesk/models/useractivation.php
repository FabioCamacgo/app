<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
JLoader::register('VirtualDeskUserTable', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/virtualdeskuser.php');

/**
* VirtualDesk Model
*/
class VirtualDeskModelUserActivation extends JModelAdmin
{

public function getTable($type = 'UserActivation', $prefix = 'VirtualDeskTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

public function getForm($data = array(), $loadData = true)
	{
		return true;
	}


public function getSendConfirmForm($data = array(), $loadData = true)
	{
//		$jinput = JFactory::getApplication()->input;
		// Get the form.
        $data = array();
		$form = $this->loadForm('com_virtualdesk.useractivation', 'useractivation_sendconfirm', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}

protected function loadFormData()
	{
		// Find the user id
		$app         = JFactory::getApplication();
        $input = JFactory::getApplication()->input;
        // Get the form data
        $formData = new JInput($input->get('jform', '', 'array'));
        $idUserParam = $formData->getInt('iduser', 0);
        // Or get POSTED data
        if($idUserParam<=0) $idUserParam = $input->getInt('iduser');

		// Check for a user.
		if (empty($idUserParam) || $idUserParam<=0)
		{
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_INVALID_USERTOACTIVATE'),'error');
			return false;
		}

		try
		{   // carrega dados de utilizador da tabela
		    $VirtualDeskUser = VirtualDeskUserHelper::getUser($idUserParam);

            if(!is_array($VirtualDeskUser))
            { $app->enqueueMessage(JText::_('COM_VIRTUALDESK_DATABASE_ERROR'),'error');
              return false;
            }

			$userIdJOS   = $VirtualDeskUser['idjos'];
			$Name        = $VirtualDeskUser['name'];
			$userIdVD    = $VirtualDeskUser['id'];
		}
		catch (RuntimeException $e)
		{
            $app->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_DATABASE_ERROR', $e->getMessage()), 'error');
			return false;
		}


		// Check for a user: verifica se tem os 2 Ids: Joomla User + VD User
		if ( empty($userIdJOS) || empty($userIdVD))
		{
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_INVALID_EMAIL'),'error');
			return false;
		}

		// Get the user object: Joomla User
		$user = JUser::getInstance($userIdJOS);

        // Só deve verificar se o utilizador está bloqueado quando for para recuperar a password
        // Neste caso para activar pode estar bloqueado
		// Make sure the user isn't blocked.
		/*if ($user->block)
		{  $this->setError(JText::_('COM_VIRTUALDESK_USER_BLOCKED'));
			return false;
		}
		*/

		// Make sure the user isn't a Super Admin.
		if ($user->authorise('core.admin'))
		{	$app->enqueueMessage(JText::_('COM_VIRTUALDESK_REMIND_SUPERADMIN_ERROR'),'error');
			return false;
		}

        // Verifica que o email do Joomla User == Virtual DeskUser, caso contrário dá erro ...
        //ToDO : ou deve atualizar se o email não for igual ????

        if ($user->email != $VirtualDeskUser['email'])
        {	$app->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_EMAIL_NOT_MATCH'),'error');
            return false;
        }

		// Define dados para o formulário
		$data['email']  = $VirtualDeskUser['email'];
		$data['iduser'] = $userIdVD;
		$data['idjos']  = $userIdJOS;
		$data['name']   = $VirtualDeskUser['name'];

		return $data;
	}



	/**
	*
	* @param $data
	*/
	public function processSend($data)
	{
        $app = JFactory::getApplication();
//		$config = JFactory::getConfig();

		// Get the form.
		//$form = $this->getForm();
		$form = $this->getSendConfirmForm();
		$data['email'] = JStringPunycode::emailToPunycode($data['email']);

		// Check for an error.
		if ($form instanceof Exception)
		{
		    return $form;
		}

		// Filter and validate the form data.
		$data = $form->filter($data);
		$return = $form->validate($data);

		// Check for an error.
		if ($return instanceof Exception)
		{
			return $return;
		}

		// Check the validation results.
		if ($return === false)
		{
			// Get the validation messages from the form.
			foreach ($form->getErrors() as $formError)
			{  $app->enqueueMessage($formError->getMessage(), 'error');
                return false;
			}
			return false;
		}

		try
		{
            // Find the user id for the given email address.
            $VirtualDeskUser = VirtualDeskUserHelper::getUserTableByEmail($data['email']);

            if(!is_object($VirtualDeskUser))
            { $app->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_DATABASE_ERROR'),'error');
                return false;
            }

            $userIdJOS   = $VirtualDeskUser->idjos;
            $userIdVD    = $VirtualDeskUser->id;

		}
		catch (RuntimeException $e)
		{
            $app->enqueueMessage(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 'error');

			return false;
		}

        // Check for a user: verifica se tem os 2 Ids: Joomla User + VD User
        if ( empty($userIdJOS) || empty($userIdVD))
        {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_INVALID_EMAIL'),'error');
            return false;
        }

        // Get the user object: Joomla User
        $user = JUser::getInstance($userIdJOS);

        // Make sure the user isn't a Super Admin.
        if ($user->authorise('core.admin'))
        {	$app->enqueueMessage(JText::_('COM_VIRTUALDESK_REMIND_SUPERADMIN_ERROR'),'error');
            return false;
        }

        // Verifica que o email do Joomla User == Virtual DeskUser, caso contrário dá erro ...
        if ($user->email != $VirtualDeskUser->email)
        {	$app->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_EMAIL_NOT_MATCH'),'error');
            return false;
        }

		// ToDo: deve verificar as tentativas falhadas para o mesmo IP + copiar o checkresetlimit e ver como funciona
		// Make sure the user has not exceeded the reset limit
		/*if (!$this->checkResetLimit($user))
		{
			$resetLimit = (int) JFactory::getApplication()->getParams()->get('reset_time');
			$this->setError(JText::plural('COM_USERS_REMIND_LIMIT_ERROR_N_HOURS', $resetLimit));

			return false;
		}*/

		// Set the confirmation token.
		$token = JApplicationHelper::getHash(JUserHelper::genRandomPassword());

        $UserActivation = new VirtualDeskUserActivationHelper();

        $SaveUserActivation = $UserActivation->SetUserForActivation($VirtualDeskUser, $user,$token);

        if($SaveUserActivation==false)
        {  $app->enqueueMessage(JText::_(''), 'error');
           return false;
        }


//		// Assemble the password reset confirmation link.
//		$mode = $config->get('force_ssl', 0) == 2 ? 1 : (-1);
//		$itemid ="";
//		$itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
//		//$link   = 'index.php?option=com_users&view=reset&layout=confirm&token=' . $token . $itemid;
//        //$link_root   = JUri::root().'index.php?option=com_users&view=reset&layout=confirm&token=' . $token . $itemid;
//        //$link_root   = JUri::root().'index.php?option=com_users&task=registration.activate&token=' . $token . $itemid;
//        $link_root   = JUri::root().'/activate/?token=' . $token . $itemid;
//
//
//		// Put together the email template data.
//		$data = $user->getProperties();
//		$data['fromname'] = $config->get('fromname');
//		$data['mailfrom'] = $config->get('mailfrom');
//		$data['sitename'] = $config->get('sitename');
//		$data['link_text'] = JRoute::_($link_root, false, $mode);
//		//$data['link_html'] = JRoute::_($link, true, $mode);
//        $data['link_html'] = JRoute::_($link_root, true, $mode);
//
//		$data['token'] = $token;
//
//		//'COM_USERS_EMAIL_PASSWORD_RESET_SUBJECT',
//		$subject = JText::sprintf($data['sitename']);
//
//
//		$body = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY',$user->email,	$data['sitename'], $data['link_text'], $data['sitename'],$user->email, $data['token']);
//
//		// Send the password reset request email.
//		$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $user->email, $subject, $body);
//
//		// Check for an error.
//		if ($return !== true)
//		{
//			return new JException(JText::_('COM_USERS_MAIL_FAILED'), 500);
//		}

        $resSendActivatiobEmail = $UserActivation->SendActivationEmail($user, $token);
        if($resSendActivatiobEmail==false) return false;

		return true;
   }

}