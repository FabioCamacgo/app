<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

//JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
//JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');

/**
 * Model
 *
 * @since  0.0.1
 */
class VirtualDeskModelUserActivationLogs extends JModelList
{

	public function __construct ($config = array())
	{	if (empty($config['filter_fields']))
		{	$config['filter_fields'] = array(
                'id', 'a.id',
				'login', 'a.login',
				'email', 'a.email',
                'created', 'a.created',
                'ip', 'a.ip',
                'logstatus','a.logstatus'
        	);
        }
		parent::__construct($config);
	}


	protected function populateState ($ordering = null, $direction = null)
	{
        $app = JFactory::getApplication('administrator');

        // Adjust the context to support modal layouts.
        if ($layout = $app->input->get('layout', 'default', 'cmd'))
        {
            $this->context .= '.' . $layout;
        }

        // Load the filter state.
        $this->setState('filter.search', $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search', '', 'string'));
        $this->setState('filter.logstatus', $this->getUserStateFromRequest($this->context . '.filter.logstatus', 'filter_logstatus', '', 'string'));



        // Load the parameters.
        $params = JComponentHelper::getParams('com_virtualdesk');
        $this->setState('params', $params);


//        $app->input->set('limit',15);


        // List state information
        //$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->get('list_limit'));

        // List state information.
        parent::populateState($ordering, $direction);

//        $app->input->set('limit',15);
//        $app->input->set('list_limit',5);
//        $this->setState('list.limit', 10);
//        $this->setState('list_limit', 10);
//        $this->setState('list.limit', $this->getUserStateFromRequest($this->context . 'list.limit', 'list_limit', '5', 'string'));

	}



	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{	// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
        $id .= ':' . $this->getState('filter.logstatus');
		return parent::getStoreId($id);
	}


	/*
	* Method to build an SQL query to load the list data.
	*
	* @return      string  An SQL query
	*/
	protected function getListQuery()
	{
		// Initialize variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.logstatus, a.logdesc, a.login, a.email, a.token, a.created, a.ip'
			)
		);

		// DB table
		$query->from('#__virtualdesk_users_activation_received AS a');

		// Filter by search in title.
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{   $search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
				$query->where(' ((a.login LIKE ' . $search . ') OR (a.email LIKE ' . $search . ')) ');
			}
		}


        // Filter logstatus
        $logstatus = $db->escape($this->getState('filter.logstatus'));

        if ($logstatus !== '') {
            $logstatus = $db->quote($logstatus);
            $query->where(' ('.$db->escape('a.logstatus').'='.$logstatus.')');
        }

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'DESC');
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}



	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */
	public function getItems()
	{
		$items = parent::getItems();
		if (JFactory::getApplication('administrator')->isSite())
		{
			$user = JFactory::getUser();
			$groups = $user->getAuthorisedViewLevels();

			for ($x = 0, $count = count($items); $x < $count; $x++)
			{
				// Check the access level. Remove articles the user shouldn't see
				if (!in_array($items[$x]->access, $groups))
				{
					unset($items[$x]);
				}
			}
		}

		return $items;
	}


}