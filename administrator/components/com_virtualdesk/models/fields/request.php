<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
JFormHelper::loadFieldClass('list');
 
/**
 * VirtualDesk Form Field class for the  component
 *
 * @since  0.0.1
 */
class JFormFieldRequest extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'Request';
 
}