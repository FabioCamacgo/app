<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');


?>

<?php if (!empty( $this->sidebar)) : ?>
<div id="j-sidebar-container" class="span2">
    <?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
    <?php else : ?>
    <div id="j-main-container">
    <?php endif;?>
        <?php if (empty($this->AccessLevels)) : ?>
            <div class="alert alert-no-items">
                <?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
            </div>
        <?php else : ?>
            <h3> <?php echo JText::_('COM_VIRTUALDESK_VIRTUALDESK_EVENTLOGSCONFIG_ACCESSLEVEL_TITLE'); ?></h3>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th width="10%">
                        <?php echo JText::_('COM_VIRTUALDESK_ID'); ?>
                    </th>
                    <th width="40%">
                        <?php echo JText::_('COM_VIRTUALDESK_NAME'); ?>
                    </th>
                    </th>
                    <th width="40%">
                        <?php echo JText::_('COM_VIRTUALDESK_MODIFIED'); ?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($this->AccessLevels)) :
                    foreach ($this->AccessLevels as $i => $row) :
                        ?>
                        <tr>
                            <td>
                                <?php echo $row->id ?>
                            </td>
                            <td>
                                <?php echo $row->name; ?>
                            </td>
                            <td>
                                <?php echo $row->modified; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        <?php endif; ?>


        <?php if (empty($this->Categories)) : ?>
            <div class="alert alert-no-items">
                <?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
            </div>
        <?php else : ?>
            <h3> <?php echo JText::_('COM_VIRTUALDESK_VIRTUALDESK_EVENTLOGSCONFIG_CAT_TITLE'); ?></h3>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th width="10%">
                        <?php echo JText::_('COM_VIRTUALDESK_ID'); ?>
                    </th>
                    <th width="40%">
                        <?php echo JText::_('COM_VIRTUALDESK_NAME'); ?>
                    </th>
                    </th>
                    <th width="40%">
                        <?php echo JText::_('COM_VIRTUALDESK_MODIFIED'); ?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($this->Categories)) :
                    foreach ($this->Categories as $i => $row) :
                        ?>
                        <tr>
                            <td>
                                <?php echo $row->id ?>
                            </td>
                            <td>
                                <?php echo $row->name; ?>
                            </td>
                            <td>
                                <?php echo $row->modified; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        <?php endif; ?>

    </div>
</div>
