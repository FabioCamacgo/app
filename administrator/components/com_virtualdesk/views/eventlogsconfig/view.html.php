<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

JLoader::register('VirtualDeskTemplatesHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesktemplates.php');


/**
 * VirtualDesks View
 *
 * @since  0.0.1
 */
class VirtualDeskViewEventLogsConfig extends JViewLegacy
{
	/**
	 * 
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
     * @since
     */
	function display($tpl = null)
	{

        // Get data from the model
        $AccessLevels = $this->get('AccessLevels');   // Model
        $Categories   = $this->get('Categories');   // Model


        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode('<br />', $errors));

            return false;
        }
        // Assign the Data
        $this->AccessLevels = $AccessLevels;
        $this->Categories   = $Categories;

		// Set the toolbar
		$this->addToolBar();	

		$this->addSideBar();
 
        // Show sidebar
        $this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

	
	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{

        $canDo = JHelperContent::getActions('com_virtualdesk');

        JToolBarHelper::title(JText::_('COM_VIRTUALDESK_MANAGER_USERACTIVATIONS'));

        // Coloca as options
        if ($canDo->get('core.admin'))
        {
            JToolbarHelper::preferences('com_virtualdesk');
        }

	}
	
	
	

protected function addSideBar()
	{
       VirtualDeskTemplatesHelper::adminAddSideBar();
	}

}