<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * VirtualDesks View
 *
 * @since  0.0.1
 */
class VirtualDeskViewVirtualDesks extends JViewLegacy
{
	/**
	 * Display the Hello World view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Get data from the model
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
 
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
 
			return false;
		}
		
		
		// Set the toolbar
		$this->addToolBar();	

		$this->addSideBar();
 
        // Show sidebar
        $this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}
	
	
	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{

		/*
		 *  $canDo - para saber se o componente permite fazer determinadas ac��es
		 *  $user->authorise('core.xxxxx', 'com_virtualdesk') - para saber se o utilizador a atual tem essa permiss�o
		 */

		$state = $this->get('State');
		$canDo = JHelperContent::getActions('com_messages');
		$user  = JFactory::getUser();

		JToolBarHelper::title(JText::_('COM_VIRTUALDESK_MANAGER_VIRTUALDESKS'));

		// Coloca as
		if ($canDo->get('core.admin'))
		{
			JToolbarHelper::preferences('com_virtualdesk');
		}

		if ($canDo->get('core.edit.state'))
		{

		}


		// Add a button
		if ($user->authorise('core.admin', 'com_virtualdesk') && $canDo->get('core.edit.state') )
		{
			//$title = JText::_('TESTE AUTHORIZE');

			JToolbarHelper::publish('virtualdesks.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('virtualdesks.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolbarHelper::archiveList('virtualdesks.archive');

			// Instantiate a new JLayoutFile instance and render the batch button
			//$layout = new JLayoutFile('joomla.toolbar.batch');
			//$dhtml = $layout->render(array('title' => $title));
			//$bar->appendButton('Custom', $dhtml, 'batch');

		}

		// Add a button
		if ($user->authorise('core.delete', 'com_virtualdesk') && $canDo->get('core.delete') )
		{
			JToolBarHelper::deleteList ('virtualdesks.delete');
		}


		if ($canDo->get('core.create'))
		{
			JToolBarHelper::addNew('virtualdesk.add');
		}

		if ($canDo->get('core.edit'))
		{
			JToolBarHelper::editList('virtualdesks.edit');
		}

	}
	
	

protected function addSideBar()
	{
		JHtmlSidebar::addEntry( JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_USERS') , 'index.php?option=com_virtualdesk&view=users'); // 
		JHtmlSidebar::addEntry( JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_REQUESTS') , 'index.php?option=com_virtualdesk&view=requests'); //  
	}

}