<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.tooltip');

$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
?>

<form action="index.php?option=com_virtualdesk&view=useractivationlogs" method="post" id="adminForm" name="adminForm">

	<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif;?>
			<?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
			<?php if (empty($this->items)) : ?>
				<div class="alert alert-no-items">
					<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
				</div>
			<?php else : ?>

	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_ID', 'a.id', $listDirn, $listOrder); ?>
            </th>

			<th width="15%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USER', 'a.login', $listDirn, $listOrder); ?>
			</th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USER_EMAIL_LABEL', 'a.email', $listDirn, $listOrder); ?>
            </th>
            <th width="5%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_LOGSTATUS', 'a.logstatus', $listDirn, $listOrder); ?>
            </th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_LOGDESC', 'a.logdesc', $listDirn, $listOrder); ?>
            </th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_DATE', 'a.created', $listDirn, $listOrder); ?>
            </th>

		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) :
                $user = JFactory::getUser();
                foreach ($this->items as $i => $row) :

//                $canChange  = $user->authorise('core.edit.state', 'com_virtualdesk.useractivations' );
				$link = JRoute::_('index.php?option=com_virtualdesk&task=user.edit&id=' . $row->id);
//				$linkactivation = JRoute::_('index.php?option=com_virtualdesk&task=useractivation.sendconfirm&view=useractivation&layout=sendconfirm&iduser=' . $row->id.'&'.JSession::getFormToken().'=1');
				?>

					<tr>
						<td>
                            <?php echo $row->id; ?>
						</td>

						<td>
						<a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDIT_USER'); ?>">
							<?php echo $row->login; ?>
                        </a>
						</td>

                        <td>
                            <?php echo $row->email; ?>
                        </td>

                        <td>
                            <?php echo $row->logstatus; ?>
                        </td>

                        <td>
                            <?php echo $row->logdesc; ?>
                        </td>

                        <td align="center" class="hasTooltip" title="<?php echo ($row->ip); ?>">
                            <?php echo $row->created; ?>
                        </td>

					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>

			<?php endif; ?>

	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>

		</div>

</form>