<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * VIRTUALDESK View
 */
class VirtualDeskViewUserActivation extends JViewLegacy
{
	/**
	 * display method
	 *
	 * @return void
	 */
	public function display($tpl = null)
	{

		// check layout being requested
		if ($this->getLayout() == 'sendconfirm')
		{
			// get the Data
			$form = $this->get('SendConfirmForm');
			//$item = $this->get('Item');

			// Assign the Data
			$this->form = $form;
			//$item = $this->get('Item');
		}
		elseif ($this->getLayout() == 'send')
		{

		}


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{   JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Set the toolbar
		$this->addToolBar();

        //$this->addSideBar();
        // Show sidebar
        //$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;
		$input->set('hidemainmenu', true);
        JToolBarHelper::title(JText::_('COM_VIRTUALDESK_MANAGER_ACTIVATIONS'));
		//JToolBarHelper::cancel('USER.cancel', $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');
	}

    protected function addSideBar()
    {
        VirtualDeskTemplatesHelper::adminAddSideBar();
    }


}