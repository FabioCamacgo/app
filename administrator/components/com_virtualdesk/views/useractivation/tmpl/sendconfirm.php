<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
            Joomla.submitform(task);
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_virtualdesk&'); ?>"
      method="post" name="adminForm" id="adminForm">
    <div class="form-horizontal">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_VIRTUALDESK_USERACTIVATION_DETAILS'); ?></legend>
            <div class="row-fluid">
                <div class="span6">
                    <?php foreach ($this->form->getFieldset() as $field): ?>
                        <div class="control-group">
                            <div class="control-label"><?php echo $field->label; ?></div>
                            <div class="controls"><?php echo $field->input; ?></div>
                        </div>
                    <?php endforeach; ?>
                    <div>
                        <div>
        </fieldset>
        <div>
            <input type="hidden" name="task" value="useractivation.send" />
            <?php echo JHtml::_('form.token'); ?>

            <button type="button" class="btn" onclick="Joomla.submitbutton('useractivation.cancel')">
                <span class="icon-cancel"></span>&#160;<?php echo JText::_('JCANCEL') ?>
            </button>

            <button type="button" class="btn" onclick="Joomla.submitbutton('useractivation.send')">
                <span class="icon-save"></span>&#160;<?php echo JText::_('COM_VIRTUALDESK_USERACTIVATION_SENDCONFIRM') ?>
            </button>




</form>