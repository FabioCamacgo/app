<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>

<div id="j-sidebar-container" class="span2">
    <?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
    <?php // Main part of the component view ?>
<div>

    <?php echo JText::_('COM_VIRTUALDESK_STATS_TITLE'); ?>
    <table class="table table-striped">
        <thead>
        <tr>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        </tfoot>
        <tbody>
        <tr>
            <td>
                <strong><?php echo JText::_('COM_VIRTUALDESK_STATS_NACTIVEUSERS'); ?></strong>
            </td>
            <td>
                <?php  echo($this->UserStats['NumberOfUsers']); ?>
            </td>
        </tr>

        <tr>
            <td>
                <strong><?php echo JText::_('COM_VIRTUALDESK_STATS_NINACTIVEUSERS'); ?></strong>
            </td>
            <td>
                <?php  echo($this->UserStats['NumberOfInactiveUsers']); ?>
            </td>
        </tr>

        </tbody>
    </table>


</div>

</div>