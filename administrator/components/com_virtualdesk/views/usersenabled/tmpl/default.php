<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.tooltip');

$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
?>

<form action="index.php?option=com_virtualdesk&view=usersenabled" method="post" id="adminForm" name="adminForm">

	<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif;?>
			<?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
			<?php if (empty($this->items)) : ?>
				<div class="alert alert-no-items">
					<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
				</div>
			<?php else : ?>

	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%"><?php echo JText::_('COM_VIRTUALDESK_NUM'); ?></th>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<th width="20%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USERACCOUNT_NAME', 'a.name', $listDirn, $listOrder); ?>
			</th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USERACCOUNT_LOGIN', 'a.login', $listDirn, $listOrder); ?>
            </th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USERACCOUNT_EMAIL', 'a.email', $listDirn, $listOrder); ?>
            </th>
			<th width="20%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USERACCOUNT_ENABLED', 'a.blocked', $listDirn, $listOrder); ?>
			</th>
			<th width="20%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USERACCOUNT_ACTIVATED', 'a.activated', $listDirn, $listOrder); ?>
			</th>
			<th width="10%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_ID', 'a.id', $listDirn, $listOrder); ?>
			</th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				$link = JRoute::_('index.php?option=com_virtualdesk&task=user.edit&id=' . $row->id);
				$linkactivation = JRoute::_('index.php?option=com_virtualdesk&task=useractivation.sendconfirm&view=useractivation&layout=sendconfirm&iduser=' . $row->id.'&'.JSession::getFormToken().'=1');
			
				?>

					<tr>
						<td>
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td>
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td>
						<a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDIT_USER'); ?>">
							<?php echo $row->name; ?>
                        </a>
						</td>
                        <td>
                            <?php echo $row->login; ?>
                        </td>
                        <td>
                            <?php echo $row->email; ?>
                        </td>
						<td align="center">
                            <?php //echo JHtml::_('jgrid.block', $row->blocked, $i, 'users.', true, 'cb'); ?>
                            <?php echo JHtml::_('listactions.block', $row->blocked, $i, 'usersenabled', true, 'cb'); ?>
						</td>
						<td align="center">
                            <?php echo JHtml::_('listactions.activated', $row->activated, $i, '', false, ''); ?>
                            <?php if($row->blocked==0) : ?>
						    <a href="<?php echo $linkactivation; ?>" title="<?php echo JText::_('COM_VIRTUALDESK_USERACTIVATION_SENDCONFIRM'); ?>"><?php echo JText::_('COM_VIRTUALDESK_USERACTIVATION_SENDCONFIRM'); ?>
                            <?php endif; ?>
						</td>
						<td align="center">
							<?php echo $row->id; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>

			<?php endif; ?>

	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>

		</div>

</form>