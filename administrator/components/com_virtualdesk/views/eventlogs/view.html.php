<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JLoader::register('VirtualDeskTemplatesHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesktemplates.php');
require_once (JPATH_ADMINISTRATOR.'/components/com_virtualdesk/helpers/virtualdesklistactions.php');

/**
 * VirtualDesks View
 *
 * @since  0.0.1
 */
class VirtualDeskViewEventLogs extends JViewLegacy
{
	/**
	 * 
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{

        $layout  = JFactory::getApplication()->input->get('layout');
        //$layout ='';
        // $this->setLayout('default_layout1');
        // $this->setLayout('layout2');

        if($layout=='layout1') {
            $this->LogsLayout1 = $this->get('LogsLayout1');
            $this->items		= $this->get('Items');
            $this->setLayout('default_layout1');
        }
        else
        {   // Get data from the model
            $this->items		= $this->get('Items');   // Model getItems method
        }

		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');


		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{	JError::raiseError(500, implode('<br />', $errors));
 			return false;
		}
		
		
		// Set the toolbar
		$this->addToolBar();	

		$this->addSideBar();
 
        // Show sidebar
        $this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);
	}


    function display2($tpl = null)
    {
//        $this->items         = $this->get('Items');
        $this->pagination    = $this->get('Pagination');
        $this->state         = $this->get('State');
        $this->filterForm    = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');


        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {	JError::raiseError(500, implode('<br />', $errors));
            return false;
        }

        // Set the toolbar
        $this->addToolBar();
        $this->addSideBar();
        // Show sidebar
        $this->sidebar = JHtmlSidebar::render();
        // Display the template
        parent::display($tpl);
    }


	
		/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{

        $canDo = JHelperContent::getActions('com_virtualdesk');

        JToolBarHelper::title(JText::_('COM_VIRTUALDESK_MANAGER_USERACTIVATIONS'));

        // Coloca as options
        if ($canDo->get('core.admin'))
        {
            JToolbarHelper::preferences('com_virtualdesk');
        }

	}
	
	
	

protected function addSideBar()
	{
       VirtualDeskTemplatesHelper::adminAddSideBar();
	}

}