<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.tooltip');

$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
?>

<form action="index.php?option=com_virtualdesk&view=eventlogs" method="post" id="adminForm" name="adminForm">

	<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif;?>
			<?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
			<?php if (empty($this->items)) : ?>
				<div class="alert alert-no-items">
					<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
				</div>
			<?php else : ?>

                LAYOUT 2 bbbbbbbbbbbbbbbbbbb
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_ID', 'a.id', $listDirn, $listOrder); ?>
            </th>
			<th width="15%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_USER', 'a.iduser', $listDirn, $listOrder); ?>
			</th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_ACCESSLEVEL', 'a.accesslevel', $listDirn, $listOrder); ?>
            </th>
			<th width="5%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_CATEGORY', 'a.category', $listDirn, $listOrder); ?>
			</th>
			<th width="5%">
				<?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_TITLE', 'a.title', $listDirn, $listOrder); ?>
			</th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_DESC', 'a.desc', $listDirn, $listOrder); ?>
            </th>
            <th width="15%">
                <?php echo JHtml::_('searchtools.sort', 'COM_VIRTUALDESK_CREATED', 'a.created', $listDirn, $listOrder); ?>
            </th>

		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) :
                foreach ($this->items as $i => $row) :
				?>
					<tr>
						<td>
                            <?php //echo $this->pagination->getRowOffset($i); ?>
                            <?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
                        <td>
                            <?php echo $row->iduser; ?>
                        </td>
                        <td>
                            <?php echo $row->accesslevel; ?>
                        </td>
                        <td>
                            <?php echo $row->category; ?>
                        </td>
                        <td>
                            <?php echo $row->title; ?>
                        </td>
                        <td>
                            <?php echo $row->desc; ?>
                        </td>

                        <td align="center" class="hasTooltip" title="<?php echo ($row->ip); ?>">
                            <?php echo $row->created; ?>
                        </td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>

			<?php endif; ?>

	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>

		</div>

</form>