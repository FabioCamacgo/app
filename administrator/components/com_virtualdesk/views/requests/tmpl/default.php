<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>

<div id="j-sidebar-container" class="span2">
    <?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
    <?php // Main part of the component view
    ?>    
<form action="index.php?option=com_virtualdesk&view=requests" method="post" id="adminForm" name="adminForm">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%"><?php echo JText::_('COM_VIRTUALDESK_NUM'); ?></th>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<th width="25%">
				<?php echo JText::_('COM_VIRTUALDESK_TITLE') ;?>
			</th>
			<th width="15%">
				<?php echo JText::_('COM_VIRTUALDESK_TYPE'); ?>
			</th>
			<th width="15%">
				<?php echo JText::_('COM_VIRTUALDESK_VALUE'); ?>
			</th>
			<th width="2%">
				<?php echo JText::_('COM_VIRTUALDESK_ID'); ?>
			</th>
			<th width="2%">
				<?php echo JText::_('COM_VIRTUALDESK_SUBID'); ?>
			</th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				//$link = JRoute::_('index.php?option=com_virtualdesk&task=request.edit&id=' . $row->id);
				?>
 
					<tr>
						<td>
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td>
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td>
						
						<!--<a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDIT_REQUEST'); ?>">-->
						
							<?php echo $row->title; ?>
						</td>
						<td align="center">
						    <?php echo $row->type; ?>
							<?php //echo JHtml::_('jgrid.published', $row->published, $i, 'users.', true, 'cb'); 
							?>
						</td>
						<td align="center">
						    <?php if( $row->type =='File Upload') 
						          { //echo "<img src='".$row->value."'>"; 
						          echo "<img src='".str_replace("/home/facecons/public_html", "", $row->value)."'>"; 
						            
						          } 
						          else
						          { echo $row->value; }


						      ?>							
						</td>
						<td align="center">
							<?php echo $row->id; ?>
						</td>
						<td align="center">
							<?php echo $row->subid; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
		
</form>

</div>