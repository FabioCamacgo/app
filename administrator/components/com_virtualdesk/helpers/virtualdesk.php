<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskHelper
{
	
	
	public static function getActions()
	{

		// Get list of actions
		$result = JHelperContent::getActions('com_virtualdesk');

		return $result;
	}


    /**
     * Removes Joomla system messages from application queue
     *
     * Can remove messages by text or by type.
     *
     * @param   array  $messages2Remove  Messages to be removed by type
     * @param   array  $types2Remove     Types of messages to be removed
     *
     * @return   type  Description
     */
    public static function removeAppMessage($messages2Remove = array(), $types2Remove = array() )
    {
        if (empty($messages2Remove) && empty($types2Remove) )
        {
            return;
        }

        $messages2Remove = (array) $messages2Remove;
        $types2Remove = (array) $types2Remove;

        $app = JFactory::getApplication();
        $appReflection = new ReflectionClass(get_class($app));
        $_messageQueue = $appReflection->getProperty('_messageQueue');
        $_messageQueue->setAccessible(true);
        $messages = $_messageQueue->getValue($app);

        foreach ($messages as $key => $message)
        {
            if (in_array($message['message'], $messages2Remove) || in_array($message['type'], $types2Remove))
            {
                unset($messages[$key]);
            }
        }

        $_messageQueue->setValue($app, $messages);
    }



    public static function getLanguageTag()
    {
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
        switch ($language_tag) {
            case 'pt-PT':
                $fileLangSufix = 'pt_PT';
                break;
            default:
                $fileLangSufix = substr($language_tag, 0, 2);
                break;
        }
        return($fileLangSufix);
    }



    public static function removeAccents($string)
    {
        $normalizeChars = array(
            'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
            'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ń' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
            'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
            'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
            'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ń' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
            'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f',
            'ă' => 'a', 'î' => 'i', 'â' => 'a', 'ș' => 's', 'ț' => 't', 'Ă' => 'A', 'Î' => 'I', 'Â' => 'A', 'Ș' => 'S', 'Ț' => 'T',
        );
        $return  = strtr($string, $normalizeChars);
        return($return);
    }

}
