<?php
// no direct access
defined('_JEXEC') or die;
use Joomla\Utilities\ArrayHelper;
/**
 */
abstract class JHtmlListActions
{
    /**
     * @param   integer  $value      The featured value.
     * @param   integer  $i          Id of the item.
     * @param   boolean  $canChange  Whether the value can be changed or not.
     * @param   boolean  $controller name of controller to execute
     * @return  string	The anchor tag to toggle featured/unfeatured contacts.
     *
     * @since   1.6
     */
    static function enabled($value = 0, $i, $controller, $canChange = true)
    {
        //        Array of image,          task,          title,          action
        $states = array(
            0   => array('unpublish'  , 'enable'   ,  'Disabled',   'Toggle to Enable'),
            1   => array('publish'    , 'disable'  ,  'Enabled' ,   'Toggle to Disable'),
        );
        $state  = ArrayHelper::getValue($states, (int) $value, $states[1]);
        $icon  = $state[0];

        if ($canChange)
        {
            $html = '<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\''. $controller .'.'. $state[1] . '\')" class="btn btn-micro hasTooltip'
                . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[3]) . '"><span class="icon-' . $icon . '"></span></a>';
        }
        else
        {
            $html = '<a class="btn btn-micro hasTooltip disabled' . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[2])
                . '"><span class="icon-' . $icon . '"></span></a>';
        }

        return $html;
    }


    static function block($value = 0, $i, $controller, $canChange = true)
    {
        //               icon image,    task,        title,          action
        $states = array(
            1   => array('unpublish' , 'unblock'  ,  'Disabled'  ,   'Toggle to Enable User' ),
            0   => array('publish'  , 'block'    ,  'Enabled'    ,   'Toggle to Disable User'),
        );
        $state  = ArrayHelper::getValue($states, (int) $value, $states[1]);
        $icon  = $state[0];

        if ($canChange)
        {
            $html = '<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\''. $controller .'.'. $state[1] . '\')" class="btn btn-micro hasTooltip'
                . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[3]) . '"><span class="icon-' . $icon . '"></span></a>';
        }
        else
        {
            $html = '<a class="btn btn-micro hasTooltip disabled' . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[2])
                . '"><span class="icon-' . $icon . '"></span></a>';
        }

        return $html;
    }




    static function activated ($value = 0, $i, $controller, $canChange = true)
    {
        //               icon image,    task,        title,          action
        $states = array(
            0   => array('unpublish' , 'unblock'  ,  'Not Activated Yet'  ,   '' ),
            1  => array('publish'  , 'block'    ,  'Activated'    ,   ''),
        );
        $state  = ArrayHelper::getValue($states, (int) $value, $states[1]);
        $icon  = $state[0];

        if ($canChange)
        {
            $html = '<a href="#" onclick="return listItemTask(\'cb' . $i . '\',\''. $controller .'.'. $state[1] . '\')" class="btn btn-micro hasTooltip'
                . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[3]) . '"><span class="icon-' . $icon . '"></span></a>';
        }
        else
        {
            $html = '<a class="btn btn-micro hasTooltip disabled' . ($value == 1 ? ' active' : '') . '" title="' . JHtml::tooltipText($state[2])
                . '"><span class="icon-' . $icon . '"></span></a>';
        }

        return $html;
    }


}