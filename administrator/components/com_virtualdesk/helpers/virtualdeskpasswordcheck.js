/**
* JQuery Plugin to check a password
*
*/
(function( $ ) {

    $.fn.virtualDeskPasswordCheck = function(pass1, pass2, options) {

        //Parametros recebidos
        var settings = $.extend({
            // These are the defaults.
            m_minLength     : options.m_minLength,
            message_MinLength  : options.message_MinLength,
            m_strUpperCase  : "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            m_strLowerCase  : "abcdefghijklmnopqrstuvwxyz",
            m_strNumber     : "0123456789",
            m_strCharacters : "!@#$%^&*?+-_~",
            //check params login
            p_Username       : options.p_Username,
            p_UsernameElem   : options.p_UsernameElem,
            message_Username : options.message_Username,
            //check params email
            p_Email       : options.p_Email,
            p_EmailElem   : options.p_EmailElem,
            message_Email : options.message_Email,
            //check params for chars
            message_chartype: options.message_chartype,
            // lower letters
            p_AZMin       : options.p_AZMin,
            message_AZMin : options.message_AZMin,
            // upper letters
            p_AZMai       : options.p_AZMai,
            message_AZMai : options.message_AZMai,
            // numbers
            p_Num         : options.p_Num,
            message_Num   : options.message_Num,
            // numbers
            p_SpecialChar       : options.p_SpecialChar,
            message_SpecialChar : options.message_SpecialChar,
            // Spaces stars and ends
            message_Spaces : options.message_Spaces,
            // pass1 = pass2
            message_pass1pass2 : options.message_pass1pass2,



        }, options );

        var returnObj =  {
            message : '',
            return : true
        }

        var message = '';
        var charSep = '';


        // Não pode começar nem acabar com espaço
        if( pass1.startsWith(' ') || pass1.endsWith(' ') )
        {   message           = settings.message_Spaces;
            returnObj.message = message;
            returnObj.return  = false;
            return returnObj;
        }

        // Check Password length
        if ( pass1.length < settings.m_minLength )
        { message           = settings.message_MinLength;
          message           = message.replace('{0}', String(settings.m_minLength));
          returnObj.message = message;
          returnObj.return  = false;
          return returnObj;
        }

        // Check if password não contém o username/login
        if(settings.p_Username=='1')
        {  var usernameString = jQuery('#'+ settings.p_UsernameElem ).val();
            if(pass1.indexOf(usernameString) !== -1 && usernameString!='') {
                message = settings.message_Username;
                returnObj.message = message;
                returnObj.return = false;
                return returnObj;
            }
        }

        // Check if password não contém o email
        if(settings.p_Email=='1')
        {  var emailString = jQuery('#'+ settings.p_EmailElem ).val();
            if(pass1.indexOf(emailString) !== -1 && emailString!='' ) {
                message = settings.message_Email;
                returnObj.message = message;
                returnObj.return = false;
                return returnObj;
            }
        }


        // Check if password contem letras minúsculas
        if(settings.p_AZMin=='1')
        {  var nLowerCount = countContain(pass1, settings.m_strLowerCase);
           if(nLowerCount <= 0) {
               if(returnObj.message!='') charSep = ' , ';
                message = settings.message_AZMin;
                returnObj.message = returnObj.message + charSep + message;
                returnObj.return = false;
            }
        }


        // Check if password contem letras Maiscúlas
        if(settings.p_AZMai=='1')
        {   var nUpperCount = countContain(pass1, settings.m_strUpperCase);
            if(nUpperCount <= 0) {
                if(returnObj.message!='') charSep = ' , ';
                message = settings.message_AZMin;
                returnObj.message = returnObj.message + charSep + message;
                returnObj.return = false;
            }
        }

        // Check if password contem números
        if(settings.p_Num=='1')
        {   var nNumberCount = countContain(pass1, settings.m_strNumber);
            if(nNumberCount <= 0) {
                if(returnObj.message!='') charSep = ' , ';
                message = settings.message_Num;
                returnObj.message = returnObj.message + charSep + message;
                returnObj.return = false;
            }
        }

        // Check if password contem caracteres especiais
        if(settings.p_SpecialChar=='1')
        {   var nSpecialCharCount = countContain(pass1, settings.m_strCharacters);
            if(nSpecialCharCount <= 0) {
                if(returnObj.message!='') charSep = ' , ';
                message = settings.message_SpecialChar;
                returnObj.message = returnObj.message + charSep + message;
                returnObj.return = false;
            }
        }



        // check if is missing ANY caracteres obrigatórios
        if( returnObj.return===false)
        {   message = settings.message_chartype;
            message = message.replace('{0}', String(returnObj.message));
            returnObj.message = message;
            return returnObj;
        }


        // se vem o segundo elemento então as senhas devem ser iguais...
        if( pass1!=undefined && pass2!=undefined && pass1!='' && pass2!='' && pass1!=pass2 )
        {   message = settings.message_pass1pass2;
            returnObj.message = message;
            returnObj.return = false;
            return returnObj;
        }


        //

        return returnObj;

    };


// Checks a string for a list of characters
    function countContain(strPassword, strCheck)
    {
        // Declare variables
        var nCount = 0;

        for (i = 0; i < strPassword.length; i++)
        {
            if (strCheck.indexOf(strPassword.charAt(i)) > -1)
            {
                nCount++;
            }
        }

        return nCount;
    }


}( jQuery ));


