<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

/**
 *
 * @since  1.6
 */
class VirtualDeskLogHelper
{
    var $arTipoOP = array();

    public function __construct()
    {
    }

    /*
    * Adiciona Log na tentativa de activar um utilizador por link+token
    */
    public static function userActivationInsertLog($logdata)
    {
        $app = JFactory::getApplication();

        $data = array();
        $data['idactivation']  = $logdata['idactivation'];
        $data['logstatus']     = $logdata['logstatus'];
        $data['logdesc']       = $logdata['logdesc'];
        $data['email']         = $logdata['email'];
        $data['iduser']        = $logdata['iduser'];
        $data['idjos']         = $logdata['idjos'];
        $data['token']         = $logdata['token'];
        $data['ip']  = $app->input->server->get('REMOTE_ADDR');

        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
        $table = JTable::getInstance('UserActivationLog', 'VirtualDeskTable');

        if (!$table->save($data)) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_LOGSAVE_ERROR'), 'error');
            return false;
        }
        return true;
    }

    /*
     *  verifica o nº de log/activação de tentativas realizadas com erro nos últimos X minutos pelo IP indicado
     *  Retorna TRUE se estiver ok com o IP
     *  Retorna FALSE se o IP tiver que ser bloqueado
     */
    public static function userActivationCheckLogByCurrentIP()
    {
        $app           = JFactory::getApplication();
        $params        = JComponentHelper::getParams('com_virtualdesk');
        $limitTries    = $params->get('logerrorlimittries');
        $limitInterval = $params->get('logerrorlimitinterval');
        $timeZone      = $params->get('timezone');


        $IP            = $app->input->server->get('REMOTE_ADDR');

        $linkDataLimit = new DateTime();
        $linkDataLimit->setTimezone(new DateTimeZone($timeZone));
        $linkDataLimit->sub(new DateInterval('PT'.$limitInterval.'M'));

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select(array('id', 'ip'))
            ->from('#__virtualdesk_users_activation_received')
            ->where($db->quoteName('ip').' = \''.$IP.'\' and '.$db->quoteName('logstatus').'=\'ERR\' and '.$db->quoteName('created').'>=\''.$linkDataLimit->format('Y-m-d H:i:s').'\'');
        $db->setQuery($query);
        $db->execute();

        $NumLog = (integer) $db->getNumRows();

        if( ($NumLog==0) || ($NumLog<=$limitTries) ) return true;

        return false;

    }

    /*
   * Adiciona Log de um evento
   * @param $logdata
   *
   *   $logdata['title'];
   *   $logdata['desc'];
   *   $logdata['category']
   *   $logdata['accesslevel'];
   *   $logdata['sendEmail']
   *   $logdata['iduser'];
   *   $logdata['idjos'];
   *   $logdata['sessionuserid'];
   *   $logdata['rgpd'];        - rgpd true/false
   *   $logdata['id_tipo_op'];  - tipo de operação - update, create, etc
   *   $logdata['idmodulo'];
   *   $logdata['idplugin'];
   *   $logdata['ref'];   - referência, por exemplo ref do Alerta, ou um Id de uma outro módulo/plugin
   *
   *
   */
    public static function insertEventLog($logdata)
    {
        $app = JFactory::getApplication();
        if( empty($logdata['sendEmail']) ) $logdata['sendEmail'] = false;

        $objVDParams                 = new VirtualDeskSiteParamsHelper();

        $data = array();
        // Category : Activation, Autenticated, Request, Response, Status Change
        if(!array_key_exists('category',$logdata) ) {
            $data['category'] = 0;
        }
        else {
            $data['category'] = self::getCategoryId($logdata['category']);
        }

        // event title and description
        $data['title']         = $logdata['title'];
        $data['desc']          = $logdata['desc'];


        // Access Level : default =1 , admin access
        if(!array_key_exists('accesslevel',$logdata) ) {
            $data['accesslevel'] = 1;
        }
        else {
            $data['accesslevel'] = $logdata['accesslevel'];
        }

        $data['iduser']        = $logdata['iduser'];
        $data['idjos']         = $logdata['idjos'];
        $data['ip']            = $app->input->server->get('REMOTE_ADDR');

        // Access Level : default =1 , user access
        if(!array_key_exists('sessionuserid',$logdata) ) {
            $data['sessionuserid'] = 0;
        }
        else {
            $data['sessionuserid'] = $logdata['sessionuserid'];
        }

        // bit RGPD : true / false
        if(!array_key_exists('rgpd',$logdata) ) {
            $data['rgpd'] = false;
        }
        else {
            $data['rgpd'] = $logdata['rgpd'];
        }

        // Tipo de Operação: Create, Update, Delete, View, ...
        if(!array_key_exists('id_tipo_op',$logdata) ) {
            $data['id_tipo_op'] = 0;
        }
        else {
            $data['id_tipo_op'] = $logdata['id_tipo_op'];
        }

        // Id Módulo
        if(!array_key_exists('idmodulo',$logdata) ) {
            $data['idmodulo'] = 0;
        }
        else {
            $data['idmodulo'] = $logdata['idmodulo'];
        }

        // Id Plugin
        if(!array_key_exists('idplugin',$logdata) ) {
            $data['idplugin'] = 0;
        }
        else {
            $data['idplugin'] = $logdata['idplugin'];
        }

        // Referência do processo ou registo, útil para fazer ligação mais direta a um processo, para poder ser pesquisável
        if(!array_key_exists('ref',$logdata) ) {
            $data['ref'] = '';
        }
        else {
            $data['ref'] = $logdata['ref'];
        }

        // Lista de Ficheiros Adicionados ou Eliminados na operação
        if(!array_key_exists('filelist',$logdata) ) {
            $data['filelist'] = '';
        }
        else {
            $data['filelist'] = $logdata['filelist'];
        }


        $sendmailBit = self::getCategorySendMailBit($data['category']);
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
        $table = JTable::getInstance('EventLog', 'VirtualDeskTable');

        if (!$table->save($data)) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_EVENT_INSERT_ERROR'), 'error');
            $logdata['desc'] .= JText::_('COM_VIRTUALDESK_CONFIG_EVENT_INSERT_ERROR');
            if( $logdata['sendEmail']==true || $sendmailBit=="1" ) self::sendEventLogEmail($logdata);
            return false;
        }

        // Envia email para o Admin do site
        if( $logdata['sendEmail']==true || $sendmailBit=="1" ) {
            // aqui deve ser acrescentada no guest acrescenta a lista de ficheiros
            $data['filelist']      = '';
            if(!empty($logdata['filelist'])) $data['filelist'] = $logdata['filelist'];
            self::sendEventLogEmail($data);
        }

        // Se os registos ultrapassarem o limite estabelecido envia email

        $EventLogLimiteRegistosTabelaWarning  = (int)$objVDParams->getParamsByTag('EventLogLimiteRegistosTabelaWarning');
        if((int)$table->id<=0 || (int)$table->id>(int)$EventLogLimiteRegistosTabelaWarning)
        {
            self::sendEventLogEmail2LimitWarning($logdata);
        }


        // Verifica se o evento log está configurado para enviar emia também para o utilizador, ex quando for um user request
        $sendmail2userBit = self::getCategorySendMail2UserBit($data['category']);
        if( $sendmail2userBit=="1" ) {
            $getUserInfo = JFactory::getUser($data['sessionuserid']);
            $data['mailTo'] =  $getUserInfo->email;
            $data['mailToName'] = $getUserInfo->name;
            if(!empty($data['mailTo'])) self::sendEventLogEmail2User($data);
        }

        return true;
    }

  /*  private static function getAccessLevelId($AccessLevelName)
    {
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
        $table = JTable::getInstance('EventLogAccessLevel', 'VirtualDeskTable');
        $data = array('name' => $AccessLevelName);
        $table->load($data);
        return $table->id;
    }
  */


    public static function insertAuthFailLog($logdata)
    {
        $app          = JFactory::getApplication();
        $objVDParams  = new VirtualDeskSiteParamsHelper();
        $data         = array();

        // title and IP
        $data['referer']     = $logdata['referer'];
        $data['request']     = $logdata['request'];
        $data['ip']          = $app->input->server->get('REMOTE_ADDR');


        JLoader::register('VirtualDeskTableEventLogAuthFail', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/eventlog_authfail.php');
        $db          = JFactory::getDbo();
        $TableEvtLog = new VirtualDeskTableEventLogAuthFail($db);
        if (!$TableEvtLog->save($data)) {
            return false;
        }

        // Se os registos ultrapassarem o limite estabelecido envia email
        $EventLogLimiteRegistosTabelaWarning  = (int)$objVDParams->getParamsByTag('EventLogLimiteRegistosTabelaWarning');
        if((int)$TableEvtLog->id<=0 || (int)$TableEvtLog->id>(int)$EventLogLimiteRegistosTabelaWarning)
        {
            self::sendEventLogEmail2LimitWarning($logdata);
        }

        return true;
    }


    public static function insertRgpdLog($logdata)
    {
        $app          = JFactory::getApplication();
        $objVDParams  = new VirtualDeskSiteParamsHelper();
        $data         = array();

        // server referers and IP
        $data['referer']     = $logdata['referer'];
        $data['request']     = $logdata['request'];
        $data['ip']          = $app->input->server->get('REMOTE_ADDR');

        // Title
        $data['title']     = '';
        if(array_key_exists('title',$logdata) ) $data['title'] = $logdata['title'];

        // Descrição
        $data['descricao']     = '';
        if(array_key_exists('descricao',$logdata) ) $data['descricao'] = $logdata['descricao'];

        // Referência do processo ou registo, útil para fazer ligação mais direta a um processo, para poder ser pesquisável
        $data['ref']     = '';
        if(array_key_exists('ref',$logdata) ) $data['ref'] = $logdata['ref'];

        // Id do registo
        $data['ref_id']     = 0;
        if(array_key_exists('ref_id',$logdata) ) $data['ref_id'] = $logdata['ref_id'];

        // Access Level : default =1 , user access
        if(!array_key_exists('sessionuserid',$logdata) ) {
            $data['sessionuserid'] = 0;
        }
        else {
            $data['sessionuserid'] = $logdata['sessionuserid'];
        }

        // Tipo de Operação: Create, Update, Delete, View, ...
        if(!array_key_exists('id_tipo_op',$logdata) ) {
            $data['id_tipo_op'] = 0;
        }
        else {
            $data['id_tipo_op'] = $logdata['id_tipo_op'];
        }

        // Id Módulo
        if(!array_key_exists('idmodulo',$logdata) ) {
            $data['idmodulo'] = 0;
        }
        else {
            $data['idmodulo'] = $logdata['idmodulo'];
        }

        // Id Plugin
        if(!array_key_exists('idplugin',$logdata) ) {
            $data['idplugin'] = 0;
        }
        else {
            $data['idplugin'] = $logdata['idplugin'];
        }

        JLoader::register('VirtualDeskTableEventLogRgpd', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/eventlog_rgpd.php');
        $db          = JFactory::getDbo();
        $TableEvtLog = new VirtualDeskTableEventLogRgpd($db);
        if (!$TableEvtLog->save($data)) {
            return false;
        }

        // Se os registos ultrapassarem o limite estabelecido envia email
        $EventLogLimiteRegistosTabelaWarning  = (int)$objVDParams->getParamsByTag('EventLogLimiteRegistosTabelaWarning');
        if((int)$TableEvtLog->id<=0 || (int)$TableEvtLog->id>(int)$EventLogLimiteRegistosTabelaWarning)
        {
            self::sendEventLogEmail2LimitWarning($logdata);
        }

        return true;
    }


    private static function getCategoryId($CategoryName)
    {
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
        $table = JTable::getInstance('EventLogCategory', 'VirtualDeskTable');
        $data = array('name' => $CategoryName);
        $table->load($data);
        return $table->id;
    }

    private static function getCategorySendMailBit($CatId)
    {
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
        $table = JTable::getInstance('EventLogCategory', 'VirtualDeskTable');
        $table->load($CatId);
        return $table->sendmail;
    }

    private static function getCategorySendMail2UserBit($CatId)
    {
        JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');
        $table = JTable::getInstance('EventLogCategory', 'VirtualDeskTable');
        $table->load($CatId);
        return $table->sendmail2user;
    }

    private static function sendEventLogEmail($logdata)
    {
        $config = JFactory::getConfig();

        // Put together the email template data.
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $params         = JComponentHelper::getParams('com_virtualdesk');
        $AdminEmailNot  = $params->get('adminemailfornotification');
        $data['mailTo'] = $data['mailfrom'];
        if(!empty($AdminEmailNot)) $data['mailTo'] = $AdminEmailNot;

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog_email.html');

        $obParam      = new VirtualDeskSiteParamsHelper();
        $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
        $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
        $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
        $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
        $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
        $logoEmailAtivacaoConta = $obParam->getParamsByTag('logoEmailAtivacaoConta');

        if(empty($logdata["filelist"])) $logdata["filelist"] = '';

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_TITLE',$data['sitename']);
        $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_GREETING',$data['mailTo']);
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_MESSAGE',$data['sitename']);
        $BODY_EVENTDESC     = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_EVENTDESC',$logdata["desc"].$logdata["filelist"]." (ip:".$logdata["ip"].",cat:".$logdata["category"].")");
        $BODY_FOOTER        = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_FOOTER', JUri::root() );
        $BODY_NOMECAMARA_VALUE       = JText::sprintf($nomeMunicipio);
        $BODY_CONTACTOCOPYRIGHT_VALUE       = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_EMAILCOPYRIGHT_VALUE       = JText::sprintf($emailCopyrightGeral);
        $BODY_LINKCOPYRIGHT_VALUE       = JText::sprintf($copyrightAPP);
        $BODY_WEBSITECOPYRIGHT_VALUE       = JText::sprintf($dominioMunicipio);

        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_EVENTDESC",$BODY_EVENTDESC, $body);
        $body      = str_replace("%BODY_FOOTER",$BODY_FOOTER, $body);
        $body      = str_replace("%BODY_COPYRIGHT", " &copy; " . date("Y") . ' ' . $data['sitename'], $body);
        $body      = str_replace("%BODY_NOMECAMARA_VALUE",$BODY_NOMECAMARA_VALUE, $body);
        $body      = str_replace("%BODY_CONTACTOCOPYRIGHT_VALUE",$BODY_CONTACTOCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_EMAILCOPYRIGHT_VALUE",$BODY_EMAILCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_LINKCOPYRIGHT_VALUE",$BODY_LINKCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_WEBSITECOPYRIGHT_VALUE",$BODY_WEBSITECOPYRIGHT_VALUE, $body);

        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo( $data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient( $data['mailTo'] );
        $newActivationEmail->setSubject($data['sitename']);

        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoEmailAtivacaoConta, "banner", "Logo");

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'er
        
        ror');
            return false;
        }
        return true;
    }

    private static function sendEventLogEmail2User ($logdata)
    {
        $config = JFactory::getConfig();

        // Put together the email template data.
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $params         = JComponentHelper::getParams('com_virtualdesk');
        $AdminEmailNot  = $params->get('adminemailfornotification');
        $data['mailTo'] = $data['mailfrom'];
        if(!empty($AdminEmailNot)) $data['mailTo'] = $AdminEmailNot;

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog_email2user.html');

        $obParam      = new VirtualDeskSiteParamsHelper();
        $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
        $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
        $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
        $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
        $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
        $logoEmailAtivacaoConta = $obParam->getParamsByTag('logoEmailAtivacaoConta');

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_TITLE',$data['sitename']);
        $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_GREETING',$logdata['mailToName']);
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_MESSAGE_USER',$data['sitename']);
        $BODY_EVENTDESC     = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_EVENTDESC_USER',$logdata["desc"]);
        $BODY_FOOTER        = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_FOOTER', JUri::root() );
        $BODY_NOMECAMARA_VALUE       = JText::sprintf($nomeMunicipio);
        $BODY_CONTACTOCOPYRIGHT_VALUE       = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_EMAILCOPYRIGHT_VALUE       = JText::sprintf($emailCopyrightGeral);
        $BODY_LINKCOPYRIGHT_VALUE       = JText::sprintf($copyrightAPP);
        $BODY_WEBSITECOPYRIGHT_VALUE       = JText::sprintf($dominioMunicipio);

        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_EVENTDESC",$BODY_EVENTDESC, $body);
        $body      = str_replace("%BODY_FOOTER",$BODY_FOOTER, $body);
        $body      = str_replace("%BODY_COPYRIGHT", " &copy; " . date("Y") . ' ' . $data['sitename'], $body);
        $body      = str_replace("%BODY_NOMECAMARA_VALUE",$BODY_NOMECAMARA_VALUE, $body);
        $body      = str_replace("%BODY_CONTACTOCOPYRIGHT_VALUE",$BODY_CONTACTOCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_EMAILCOPYRIGHT_VALUE",$BODY_EMAILCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_LINKCOPYRIGHT_VALUE",$BODY_LINKCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_WEBSITECOPYRIGHT_VALUE",$BODY_WEBSITECOPYRIGHT_VALUE, $body);

        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo( $data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient( $data['mailTo'] );
        $newActivationEmail->setSubject($data['sitename'].' - '.$data['title']);

        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoEmailAtivacaoConta, "banner", "Logo");

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;
    }

    private static function sendEventLogEmail2LimitWarning($logdata)
    {
        $config = JFactory::getConfig();

        // Put together the email template data.
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $params         = JComponentHelper::getParams('com_virtualdesk');
        $AdminEmailNot  = $params->get('adminemailfornotification');
        $data['mailTo'] = $data['mailfrom'];
        if(!empty($AdminEmailNot)) $data['mailTo'] = $AdminEmailNot;

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog_email.html');

        $obParam      = new VirtualDeskSiteParamsHelper();
        $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
        $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
        $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
        $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
        $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
        $logoEmailAtivacaoConta = $obParam->getParamsByTag('logoEmailAtivacaoConta');

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_TITLE',$data['sitename']. JText::_('COM_VIRTUALDESK_EVENTLOG_LIMITEREGISTOSWARN_TITLE'));
        $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_GREETING',$data['mailTo']);
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_MESSAGE',$data['sitename']);
        $BODY_EVENTDESC     = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_EVENTDESC',JText::_('COM_VIRTUALDESK_EVENTLOG_LIMITEREGISTOSWARN_DESC'));
        $BODY_FOOTER        = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_SENDMAIL_BODY_FOOTER', JUri::root() );
        $BODY_NOMECAMARA_VALUE       = JText::sprintf($nomeMunicipio);
        $BODY_CONTACTOCOPYRIGHT_VALUE       = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_EMAILCOPYRIGHT_VALUE       = JText::sprintf($emailCopyrightGeral);
        $BODY_LINKCOPYRIGHT_VALUE       = JText::sprintf($copyrightAPP);
        $BODY_WEBSITECOPYRIGHT_VALUE       = JText::sprintf($dominioMunicipio);

        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_EVENTDESC",$BODY_EVENTDESC, $body);
        $body      = str_replace("%BODY_FOOTER",$BODY_FOOTER, $body);
        $body      = str_replace("%BODY_COPYRIGHT", " &copy; " . date("Y") . ' ' . $data['sitename'], $body);
        $body      = str_replace("%BODY_NOMECAMARA_VALUE",$BODY_NOMECAMARA_VALUE, $body);
        $body      = str_replace("%BODY_CONTACTOCOPYRIGHT_VALUE",$BODY_CONTACTOCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_EMAILCOPYRIGHT_VALUE",$BODY_EMAILCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_LINKCOPYRIGHT_VALUE",$BODY_LINKCOPYRIGHT_VALUE, $body);
        $body      = str_replace("%BODY_WEBSITECOPYRIGHT_VALUE",$BODY_WEBSITECOPYRIGHT_VALUE, $body);

        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo( $data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient( $data['mailTo'] );
        $newActivationEmail->setSubject($data['sitename']);

        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoEmailAtivacaoConta, "banner", "Logo");

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'er
        
        ror');
            return false;
        }
        return true;
    }

    public function getIdTipoOPCreate() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_tipo_op Where tag ='C' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getIdTipoOPUpdate() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_tipo_op Where tag ='U' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getIdTipoOPDelete() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_tipo_op Where tag ='D' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getIdTipoOPView() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_tipo_op Where tag ='V' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getIdTipoOPList() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_tipo_op Where tag ='L' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getModuleIdByTag($Tag='')
    {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_perm_modulo Where tagchave ='" . $db->escape($Tag). "'");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getModuleNameByTag($Tag='')
    {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select nome From #__virtualdesk_perm_modulo Where tagchave ='" . $db->escape($Tag). "'");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getIdAccessLevel4Admin() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_accesslevel Where tag ='admin' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getIdAccessLevel4Manager() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_accesslevel Where tag ='manager' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public function getIdAccessLevel4User() {
        try
        {   $db = JFactory::getDbo();
            $db->setQuery("Select id From #__virtualdesk_eventlog_accesslevel Where tag ='user' ");
            $res =  $db->loadResult();
            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Verifica se o IP indicado está nos últimas N tentativas falhadas dos últimos X minutos
    */
    public function checkIPAuthFail() {
        try
        {
            $app          = JFactory::getApplication();
            $objVDParams  = new VirtualDeskSiteParamsHelper();
            $ip_atual     = $app->input->server->get('REMOTE_ADDR');
            $current_date = new DateTime();
            $MaxCount     = (int)$objVDParams->getParamsByTag('eventLogAuthFailMaxCount'); // n vezes
            $TimeInterval = (int)$objVDParams->getParamsByTag('eventLogAuthFailTimeInterval');  // minutos

            # Se os parâmetros não estiverem definidos, colocamos valores por defeito
            if( (int)$MaxCount <=0 )  $MaxCount = 5;
            if( (int)$TimeInterval <=0 )  $TimeInterval = 10;

            $CreatedInterval =  date_sub($current_date, new DateInterval("PT".$TimeInterval."M"));
            $DateInSQL = $CreatedInterval->format('Y-m-d H:i:s');

            $db = JFactory::getDbo();
            $db->setQuery("Select count(id) From #__virtualdesk_eventlog_authfail Where ip ='".$ip_atual."' AND created>='".$DateInSQL."' ");
            $res = $db->loadResult();

            if((int)$res > $MaxCount ) return false;

            return true;
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



}