<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskTemplatesHelper
{

    /* Adiciona barra lateral no backoffice admin */
    public static function adminAddSideBar()
    {
        JHtmlSidebar::addEntry(JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_USERS'), 'index.php?option=com_virtualdesk&view=users'); //
        JHtmlSidebar::addEntry(JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_USERSENABLED'), 'index.php?option=com_virtualdesk&view=usersenabled'); //
        JHtmlSidebar::addEntry(JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_USERSBLOCKED'), 'index.php?option=com_virtualdesk&view=usersblocked'); //
        JHtmlSidebar::addEntry(JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_ACTIVATIONS'), 'index.php?option=com_virtualdesk&view=useractivations'); //
        JHtmlSidebar::addEntry(JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_ACTIVATIONLOGS'), 'index.php?option=com_virtualdesk&view=useractivationlogs'); //
//        JHtmlSidebar::addEntry(JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_REQUESTS'), 'index.php?option=com_virtualdesk&view=requests'); //
        JHtmlSidebar::addEntry( JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_EVENTLOGS') , 'index.php?option=com_virtualdesk&view=eventlogs');
        JHtmlSidebar::addEntry( JText::_('COM_VIRTUALDESK_VIRTUALDESK_SIDEMENU_EVENTLOGSCONFIG') , 'index.php?option=com_virtualdesk&view=eventlogsconfig');
    }

}
