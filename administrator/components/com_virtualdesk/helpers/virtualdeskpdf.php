<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 *
 * @since  1.6
 */

JLoader::register('VirtualDeskTableDocPdf', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/docpdf.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

class VirtualDeskPDFHelper
{
    public $generator;
    public $charset;
    public $mime;
    public $type;
    public $pdf_creator;

    public $pdf;

    private $filefolder;

    public $idprocesso;
    public $tagprocesso;
    public $filetype;

    public $resFileSaved;
    public $resFilesSave2Table;


    public function __construct()
    {
        if (JFile::exists(JPATH_ADMINISTRATOR.'/components/com_virtualdesk/helpers/assets/tcpdf/tcpdf.php')) {
             require_once(JPATH_ADMINISTRATOR.'/components/com_virtualdesk/helpers/assets/tcpdf/tcpdf.php');
            $this->pdf = new TCPDF();

            $this->filefolder = JComponentHelper::getParams('com_virtualdesk')->get('docpdf_filefolder');

        } else {
            throw new Exception('Document cannot be created - Loading of TCPDF library  failed', 500);
            //return false;
            exit();
        }

        $this->pdf_creator = 'Virtual Desk - TCPDF';
        $this->pdfSetDefault();
        $this->filetype    = 'pdf';
    }

    public function SetIdProcesso($idprocesso){
    $this->idprocesso = $idprocesso;
    }

    public function SetUserJoomlaId($iduser){
        $this->iduser = $iduser;
    }

    public function SetTagProcesso($tagprocesso){
        $this->tagprocesso = $tagprocesso;
    }

    public function pdfSetDefault()
    {
        $this->generator	= 'Virtual Desk PDF';
        $this->charset 	    = 'utf-8';
        $this->mime 		= 'application/pdf';
        $this->type 		= 'pdf';
    }

    public function pdfSetMargins()
    {
        /*
        $margin_left  = 10;
        $margin_top   = 10;
        $margin_right = 10;
        $margin_bottom = 10;
        $site_cell_height = 1.2;
        $font_type = 'freemono';
        $site_font_color = '#000000';
        $header_margin = 5;
        $footer_margin = 5;
        $image_scale   = 1;
        */

        /*
         * $pdf->SetMargins($margin_left, $margin_top, $margin_right);
        $pdf->SetAutoPageBreak(TRUE, $margin_bottom);
        $pdf->setCellHeightRatio($site_cell_height);
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //$pdf->setFont($font_type);
        $spotColors = $pdf->getAllSpotColors();
        $siteFontColor = TCPDF_COLORS::convertHTMLColorToDec($site_font_color, $spotColors);
        $pdf->SetTextColor($siteFontColor['R'], $siteFontColor['G'], $siteFontColor['B']);
        $pdf->SetHeaderMargin($header_margin);
        $pdf->SetFooterMargin($footer_margin);
        $pdf->setImageScale($image_scale);
        */
    }


    /*
    *
    */
    public function pdfTeste01()
    {

        // PDF Metadata
        $this->pdf->SetCreator($this->pdf_creator);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->pdf->AddPage();

        $dtz = new DateTimeZone("Europe/London"); //Your timezone
        $date = new DateTime('NOW', $dtz);
        $timenow = $date->getTimestamp();
        $data = $date->format('Y-m-d H:i:s');

        $doc_time  = 'doc_' . rand(100,999) . '_' . $timenow . '.pdf';

        $pdfLayout  = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/layouts/pdf_teste.html');

        $pdfContent = str_replace("%DATA",$data, $pdfLayout );

        // Print text using writeHTMLCell()
        //$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $this->pdf->writeHTML($pdfContent);

        // Para o Browser
        $this->pdf->Output($doc_time,'I');

        // Para um ficheiro
        // $this->pdf->Output(JPATH_SITE.'/tmp/'.$doc_time,'F');

        // Gravar  e associar a um processo...
        // O ficheiro é enviado para o dir temporário, a partir daí lemos e copiamos para o directório final e guardamos em base de dados

        $this->SetUserJoomlaID (0);
        $this->SetIdProcesso (0);
        $this->SetTagProcesso ("AAA");
        $resSave = $this->saveFile();

        if($resSave==true) {
            $resFilepath = $this->resFileSaved ['filepath'];
            $resFilename = $this->resFileSaved ['filename'];
            $body        = 'teste';

            $config      = JFactory::getConfig();
            $fromname    = $config->get('fromname');
            $mailfrom    = $config->get('mailfrom');
            $sitename    = $config->get('sitename');

            // enviar email com anexo
            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo( $mailfrom );
            $newActivationEmail->setSender( $mailfrom );
            $newActivationEmail->setFrom( $fromname );
            $newActivationEmail->addRecipient( 'nunocosta2004@gmail.com' );
            $newActivationEmail->addRecipient( 'info@theinterfaceprojects.com' );
            $newActivationEmail->addRecipient( 'fabio.camacho@theinterfaceprojects.com' );
            $newActivationEmail->setSubject($sitename.' - '.$body);

            $newActivationEmail->addAttachment(JPATH_BASE . DS .$resFilepath . DS .$resFilename);

            $return = $newActivationEmail->send();
        }



        exit();
    }




    public function pdfContentToBrowser($pdfContent)
    {
        // PDF Metadata
        $this->pdf->SetCreator($this->pdf_creator);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->pdf->AddPage();

        // Print text using writeHTMLCell()
        $this->pdf->writeHTML($pdfContent);

        $dtz = new DateTimeZone("Europe/London"); //Your timezone
        $date = new DateTime('NOW', $dtz);
        $timenow = $date->getTimestamp();
        $data = $date->format('Y-m-d H:i:s');

        $doc_time  = 'doc_' . rand(100,999) . '_' . $timenow . '.pdf';

        // Para o Browser
        $this->pdf->Output($doc_time,'I');

        exit();
    }


    /*
   *
   */

    public function pdfAlertaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone)
    {

        // PDF Metadata
        $this->pdf->SetCreator($this->pdf_creator);
        $this->pdf->SetPrintHeader(false);
        $this->pdf->SetPrintFooter(false);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->pdf->AddPage();
        $dtz = new DateTimeZone("Europe/London"); //Your timezone
        $date = new DateTime('NOW', $dtz);
        $data = $date->format('Y-m-d');
        $hora = $date->format('H:i:s');
        $dia = $date->format('d');
        $mes = $date->format('m');
        $ano = $date->format('Y');

        $obParam      = new VirtualDeskSiteParamsHelper();

        $logoPDF = $obParam->getParamsByTag('logoPDFAlerta');
        $introPDF = $obParam->getParamsByTag('introPDFAlerta');

        $image = $this->pdf->Image(JPATH_ROOT.$logoPDF, '10', '30', 0,20, '','','',true,300,'');


        $pdfLayout  = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/Alerta/PDF/pdf_Alerta_NovaOcorrencia.html');
        $pdfContent = str_replace("%DATA",$data, $pdfLayout );
        $pdfContent = str_replace("%introPDF",$introPDF, $pdfContent );
        $pdfContent = str_replace("%catName",$catName, $pdfContent );
        $pdfContent = str_replace("%subcatName",$subcatName, $pdfContent );
        $pdfContent = str_replace("%referencia",$referencia, $pdfContent );
        $pdfContent = str_replace("%descricao",$descricao, $pdfContent );
        $pdfContent = str_replace("%fregName",$fregName, $pdfContent );
        $pdfContent = str_replace("%sitio",$sitio, $pdfContent );
        $pdfContent = str_replace("%morada",$morada, $pdfContent );
        $pdfContent = str_replace("%ptosrefs",$ptosrefs, $pdfContent );
        $pdfContent = str_replace("%nome",$nome, $pdfContent );
        $pdfContent = str_replace("%email",$email, $pdfContent );
        $pdfContent = str_replace("%lat",$lat, $pdfContent );
        $pdfContent = str_replace("%long",$long, $pdfContent );
        $pdfContent = str_replace("%image",$image, $pdfContent );
        $pdfContent = str_replace("%fiscalid",$fiscalid, $pdfContent );
        $pdfContent = str_replace("%telefone",$telefone, $pdfContent );
        $pdfContent = str_replace("%data",$data, $pdfContent );
        $pdfContent = str_replace("%hora",$hora, $pdfContent );
        $pdfContent = str_replace("%dia",$dia, $pdfContent );
        $pdfContent = str_replace("%mes",$mes, $pdfContent );
        $pdfContent = str_replace("%ano",$ano, $pdfContent );
        $pdfContent = str_replace("%copyrightAPP",$copyrightAPP, $pdfContent );
        $pdfContent = str_replace("%copyrightEmailGeral",$emailCopyrightGeral, $pdfContent );
        $pdfContent = str_replace("%contactoTelefCopyrightEmail",$contactoTelefCopyrightEmail, $pdfContent );
        $pdfContent = str_replace("%copyrightMorada",$moradaMunicipio, $pdfContent );
        $pdfContent = str_replace("%copyrightCodPostal",$codPostalMunicipio, $pdfContent );
        $pdfContent = str_replace("%space",' ', $pdfContent );


        // Print text using writeHTMLCell()
        $this->pdf->writeHTML($pdfContent);
    }


    /*public function pdfCleanSafeAdmin($referencia, $name, $numeroAL, $reqType, $fregName, $nameReq, $emailReq, $nif, $telefoneReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio){

        $this->pdf->SetCreator($this->pdf_creator);
        $this->pdf->SetPrintHeader(false);
        $this->pdf->SetPrintFooter(false);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->pdf->AddPage();
        $dtz = new DateTimeZone("Europe/London"); //Your timezone
        $date = new DateTime('NOW', $dtz);
        $data = $date->format('Y-m-d');
        $hora = $date->format('H:i:s');
        $dia = $date->format('d');
        $mes = $date->format('m');
        $ano = $date->format('Y');

        $obParam      = new VirtualDeskSiteParamsHelper();

        $logoPDF = $obParam->getParamsByTag('logoPDFAlerta');
        $introPDF = $obParam->getParamsByTag('introPDFAlerta');

        $image = $this->pdf->Image(JPATH_ROOT.$logoPDF, '10', '30', 0,20, '','','',true,300,'');


        $pdfLayout  = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/Alerta/PDF/pdf_Alerta_NovaOcorrencia.html');
        $pdfContent = str_replace("%DATA",$data, $pdfLayout );
        $pdfContent = str_replace("%introPDF",$introPDF, $pdfContent );
        $pdfContent = str_replace("%catName",$catName, $pdfContent );
        $pdfContent = str_replace("%subcatName",$subcatName, $pdfContent );
        $pdfContent = str_replace("%referencia",$referencia, $pdfContent );
        $pdfContent = str_replace("%descricao",$descricao, $pdfContent );
        $pdfContent = str_replace("%fregName",$fregName, $pdfContent );
        $pdfContent = str_replace("%sitio",$sitio, $pdfContent );
        $pdfContent = str_replace("%morada",$morada, $pdfContent );
        $pdfContent = str_replace("%ptosrefs",$ptosrefs, $pdfContent );
        $pdfContent = str_replace("%nome",$nome, $pdfContent );
        $pdfContent = str_replace("%email",$email, $pdfContent );
        $pdfContent = str_replace("%lat",$lat, $pdfContent );
        $pdfContent = str_replace("%long",$long, $pdfContent );
        $pdfContent = str_replace("%image",$image, $pdfContent );
        $pdfContent = str_replace("%fiscalid",$fiscalid, $pdfContent );
        $pdfContent = str_replace("%telefone",$telefone, $pdfContent );
        $pdfContent = str_replace("%data",$data, $pdfContent );
        $pdfContent = str_replace("%hora",$hora, $pdfContent );
        $pdfContent = str_replace("%dia",$dia, $pdfContent );
        $pdfContent = str_replace("%mes",$mes, $pdfContent );
        $pdfContent = str_replace("%ano",$ano, $pdfContent );
        $pdfContent = str_replace("%copyrightAPP",$copyrightAPP, $pdfContent );
        $pdfContent = str_replace("%copyrightEmailGeral",$emailCopyrightGeral, $pdfContent );
        $pdfContent = str_replace("%contactoTelefCopyrightEmail",$contactoTelefCopyrightEmail, $pdfContent );
        $pdfContent = str_replace("%copyrightMorada",$moradaMunicipio, $pdfContent );
        $pdfContent = str_replace("%copyrightCodPostal",$codPostalMunicipio, $pdfContent );
        $pdfContent = str_replace("%space",' ', $pdfContent );


        // Print text using writeHTMLCell()
        $this->pdf->writeHTML($pdfContent);
    }
*/


    public function saveFile ()
    {
        $dtz     = new DateTimeZone("Europe/London"); //Your timezone
        $date    = new DateTime('NOW', $dtz);
        $timenow = $date->getTimestamp();
        $data = $date->format('Y-m-d H:i:s');

        $doc_name  = 'doc_' . rand(100,999) . '_' . $timenow ;

        $this->resFileSaved = $this->saveFileToFolder($doc_name);
        if ($this->resFileSaved !== false or $this->resFileSaved !== null) {

            $this->resFilesSave2Table = $this->saveFileToTable($this->resFileSaved);
        }

        return(true);
    }


    public function saveFileToFolder ($doc_name)
    {
        $FileSubFolderY =  DS . date("Y");
        $FileSubFolder =  $FileSubFolderY . DS  . date("m");

        // Make the file name unique.
        $saveFileName = JFile::makeSafe(md5($doc_name)); // $doc_name; //
        $saveFolderY  = $this->filefolder . $FileSubFolderY;
        $saveFolder   = $this->filefolder . $FileSubFolder;

        $savePath     = JPath::clean($saveFolder . DS  . $saveFileName . '.' . $this->filetype);

        // Se a pasta e subpasta onde guardar o ficheiro não existir , cria
        if (!JFolder::exists(JPath::clean($saveFolder)))
        {

            if( !JFolder::create($saveFolder) )
            { JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error' );
                return false;
            }

            // Na pasta deve também colocar um ficheiro vazio index.html
            if (!JFile::exists($saveFolderY. DS .'index.html')) {
                if (!JFile::write($saveFolderY . DS  . 'index.html', '<html></html>')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($saveFolderY. DS .'.htaccess')) {
                if (!JFile::write($saveFolderY . DS  . '.htaccess', "Deny from all\nAuthType Basic\nAuthName 'VirtualDesk'\nAuthUserFile .htpasswd\nRequire valid-user")) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($saveFolderY.DS .'.htpasswd')) {
                if (!JFile::write($saveFolderY . DS  . '.htpasswd', 'vduser123:$apr1$c5Po9wbO$4lupFVXknqpcfaYv/5CcN/')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }

            // Na pasta deve também colocar um ficheiro vazio index.html
            if (!JFile::exists($saveFolder.DS .'index.html')) {
                if (!JFile::write($saveFolder . DS  . 'index.html', '<html></html>')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($saveFolder.DS .'.htaccess')) {
                if (!JFile::write($saveFolder . DS  . '.htaccess', "Deny from all\nAuthType Basic\nAuthName 'VirtualDesk'\nAuthUserFile .htpasswd\nRequire valid-user")) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($saveFolder. DS .'.htpasswd')) {
                if (!JFile::write($saveFolder . DS  . '.htpasswd', 'vduser123:$apr1$c5Po9wbO$4lupFVXknqpcfaYv/5CcN/')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }

        }


        // File Path exists ?
        if (JFile::exists($savePath))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_EXISTS'), 'error' );
            return false;
        }


        try {
            $resFileOutput = $this->pdf->Output(JPATH_ROOT . DS . $savePath,'F');
        } catch (Exception $e) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_EXISTS'), 'error' );
            return false;
        }


        $resUploadedRow = array();
        $resUploadedRow['filename'] = $saveFileName.'.'.$this->filetype;
        $resUploadedRow['ext']      = $this->filetype;
        $resUploadedRow['desc']     = $saveFileName;
        $resUploadedRow['type']     = $this->filetype;
        $resUploadedRow['size']     = @filesize ($savePath);
        $resUploadedRow['basename'] = $doc_name;
        $resUploadedRow['filepath'] = $saveFolder;

        return $resUploadedRow;
    }


    public function saveFileToTable ($FileSaved )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( !is_array($FileSaved) )  return false;
        if ( empty($FileSaved) )  return false;

            $data['idprocesso']  = $this->idprocesso;
            $data['tagprocesso'] = $this->tagprocesso;
            $data['filename']    = $FileSaved['filename'];
            $data['basename']    = $FileSaved['basename'];
            $data['ext']         = $FileSaved['ext'];
            $data['type']        = $FileSaved['type'];
            $data['size']        = $FileSaved['size'];
            $data['desc']        = $FileSaved['desc'];
            $data['filepath']    = $FileSaved['filepath'];

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $TableContactUsFile = new VirtualDeskTableDocPdf($db);

            // Bind the data.
            if (!$TableContactUsFile->bind($data)) return false;

            // Store the data.
            if (!$TableContactUsFile->save($data)) return false;

        return true;
    }









}