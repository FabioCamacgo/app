<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');
JLoader::register('VirtualDeskTableUserActivation', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/useractivation.php');
JLoader::register('VirtualDeskTableUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/user.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');

/**
 * VirtualDesk User helper class.
 *
 * @since  1.6
 */
class VirtualDeskUserActivationHelper
{

    /*
   * Grava os dados do utilizador no joomla user e no VD users para marcá-lo como estando à espera de activação por email
   */
    public function SetUserForActivation(VirtualDeskTableUser $VDUser, JUser $JoomlaUser, $token)
    {

        if( ((int)$VDUser->id <= 0) || ((int)$JoomlaUser->id <= 0) || ((int)$JoomlaUser->id <> $VDUser->idjos) )
        {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_SAVE_FAILED'), 'error');
            return false;
        }

        // guarda tabela com valore iniciais dos utilizadores, se der erro num deles reverte gravando os dados existentes
        $VDUserBackState = clone $VDUser;
        $JoomlaUserState = clone $JoomlaUser;


        $hashedToken = JUserHelper::hashPassword($token);
        $JoomlaUser->activation = $hashedToken;
        $JoomlaUser->block = 1;

        // Save the joomla user to the database
        //ToDo -> vai gravar o utilizador para fazer um bloqueio e colocar como à espera de activação ; tem de fazer isto no joomla user e no VD users
        if (!$JoomlaUser->save(true)) {
            return new JException(JText::sprintf('COM_VIRTUALDESK_USER_SAVE_FAILED', $JoomlaUser->getError()), 500);
        }

        $VDUser->blocked = 1;
        $VDUser->activated = 0;
        $VDUser->modified = JFactory::getDate()->format("Y-m-d H:i:s");

        // Atualiza
        if (!$VDUser->store()) {   // estado anterior da tabela de dados
            $JoomlaUserState->save();
            return new JException(JText::sprintf('COM_VIRTUALDESK_USER_SAVE_FAILED'), 500);
        }

        //ToDo : FALTA -> gerar o registo na tabela users activation + inactivar todos os outros existente
        $SetActivationTable = $this->SetActivationTable($VDUser, $JoomlaUser, $token);

        if ($SetActivationTable == false) {   // estado anterior da tabela de dados
            $JoomlaUserState->save();
            $VDUserBackState->store();
            return new JException(JText::sprintf('COM_VIRTUALDESK_USER_SAVE_FAILED'), 500);
        }


        //ToDo : FALTA -> guardar pedido no histórico -> criar HELPER para enviar mensagem para histórico. OU TALVEZ TER tabela de histórico INTERNAS + tabela com LOG por utilizador + request
        //TODO: talvez ter __requesttype (popular logo à partida com os valores por defeito ao criar a base de dados) + __requesttypehistory

        //ToDo : OU Já criei a tabela __virtualdesk_users_activation_received  para LOG dos pedidos de activação,
        //TODO colocaros úlimos logs juntamos depois as VÀRIAS TABELA e surgem no dashboard inicial (se for admin surgem todos, se for user surgem apenas os do user atual)

        return true;
    }

    /*
    * Grava os dados do utilizador no joomla user e no VD users para indicar que está à espera de activação por email
    */
    private function SetActivationTable($VDUser, $JoomlaUser, $token)
    {
        // a inactivação do utilizador no VD e no joomla já foi feita antes

        // vamos inactivar todos os pedidos anteriores do utilizador VD atual
        // $data['enabled']  = 0;
        if (!$this->SetActivationTableDisabled($VDUser, $JoomlaUser)) {
            return new JException(JText::sprintf('COM_VIRTUALDESK_USER_SAVE_FAILED'), 500);
        }

        // vamos registar o NOVO pedido de ativação
        $db = JFactory::getDbo();
        // $table = new VirtualDeskTableUserActivation($db);
        $table = JTable::getInstance('UserActivation', 'VirtualDeskTable');

        //Get current username & userid
        $sessionuser = JFactory::getUser();

        $data = array();
        $data['iduser'] = $VDUser->id;
        $data['idjos'] = $JoomlaUser->id;
        $data['login'] = $VDUser->login;
        $data['email'] = $VDUser->email;
        $data['token'] = $token;
        $data['enabled'] = 1;

        $data['sessionuser'] = $sessionuser->username;;
        $data['sessionip'] = JFactory::getApplication()->input->server->get('REMOTE_ADDR');


        if (!$table->save($data)) {
            return new JException(JText::sprintf('COM_VIRTUALDESK_USER_SAVE_FAILED'), 500);
        }

        return true;
    }


    /*
    * Inactiva todos os pedidos anteriores do utilizador VD atual na tabela especifica para isso
    */
    private function SetActivationTableDisabled($VDUser, $JoomlaUser)
    {
        // Get a db connection.
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        // Fields to update.
        $fields = array(
            $db->quoteName('enabled') . ' = 0'
        );

        // Conditions for which records should be updated.
        $conditions = array(
            $db->quoteName('iduser') . ' = ' . $VDUser->id
        );

        $query->update($db->quoteName('#__virtualdesk_users_activation'))->set($fields)->where($conditions);

        $db->setQuery($query);

        return $db->execute();
    }


    /*
     * função a ser executada, neste momento no plugin no "site" mde modo a verificar se devemos acticar o token recebido
     *
     */
    public static function checkActivationToken($token)
    {
        $app         = JFactory::getApplication();
        $params      = JComponentHelper::getParams('com_virtualdesk');
        $limitInDays = $params->get('limitindays');
        $timeZone    = $params->get('timezone');

        $vdlog = new VirtualDeskLogHelper();

        // Verifica se o IP atual está bloqueado no log de activação por tentativs erradas
        if(!$vdlog->userActivationCheckLogByCurrentIP())
        {  $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_LOG_IPBLOCKED'), 'error');
           return false;
        }

        $linkDataLimit = new DateTime();
        $linkDataLimit->setTimezone(new DateTimeZone($timeZone));
        $linkDataLimit->sub(new DateInterval('P'.$limitInDays.'D'));

        // Do query for the received token
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select(array('id', 'iduser', 'idjos', 'token', 'email','login'))
            ->from('#__virtualdesk_users_activation')
            ->where($db->quoteName('token').' = \''.$token.'\' and '.$db->quoteName('enabled').'=1 and '.$db->quoteName('created').'>=\''.$linkDataLimit->format('Y-m-d').'\'');
        // Reset the query using our newly populated query object.
        $db->setQuery($query);

        $row = $db->loadAssoc();

        $activation_id = (integer)$row['id'];

        $logdata = array();
        $logdata['idactivation'] = $activation_id;
        $logdata['email'] = $row['email'];
        $logdata['iduser'] = $row['iduser'];
        $logdata['idjos']  = $row['idjos'];
        $logdata['login']  = $row['login'];
        $logdata['token']  = $token;

        // Se não devolver resultados, Regista IP de tentativa de activação com erro, coloca mensagem e retorna false
        if ((!is_array($row)) || (sizeof($row) < 0) || (integer)$activation_id <= 0) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_IDNOTFOUND'), 'error');
            $logdata['logstatus'] = 'ERR';
            $logdata['logdesc'] = JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_IDNOTFOUND');
            $vdlog->userActivationInsertLog($logdata);
            return false;
        }

        // Verifica que o utilizador a activar não é Admin ou VDAdmin
        if(!self::checkIfUser2ActivationIsAdmin($row['idjos'])) return false;

        $table = JTable::getInstance('UserActivation', 'VirtualDeskTable');
        $table->load($activation_id);

        $tableState =  clone $table;

        //check if table row id was loaded..
        if (isset($table->id) && (integer)$table->id == $activation_id) {
            $data2Save = array();
            // desliga o token, porque foi usado
            $data2Save['enabled'] = 0;
            // indica que o token+user foi tornado activo
            $data2Save['activated'] = 1;
            $data2Save['activatedbyip'] = $app->input->server->get('REMOTE_ADDR');
            $data2Save['modified'] = JFactory::getDate()->format("Y-m-d H:i:s");

            if (!$table->save($data2Save)) {
                // erro ao gravar activação
                $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_SAVE_ERROR'), 'error');
                $logdata['logstatus'] = 'ERR';
                $logdata['logdesc'] = JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_SAVE_ERROR');
                $vdlog->userActivationInsertLog($logdata);
                return false;
            } else {
                $logdata['logstatus'] = 'OK';
                $logdata['logdesc'] = JText::_('activado');
                $vdlog->userActivationInsertLog($logdata);

                // Faz activação dos users...
                $resUserActiv = self::SetUserActivated($logdata['iduser'], $logdata['login']);
                if($resUserActiv==false)
                { // Reverte os dados  anteriores se falhou a activação dos users
                    $tableState->store();
                    $logdata['logstatus'] = 'ERR';
                    $logdata['logdesc'] = JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_SAVEUSER_ERROR');
                    $vdlog->userActivationInsertLog($logdata);
                    return(false);
                }

                // Conseguimos ativar o utilizar então regista no event log do utilizador
                $eventdata                  = array();
                $eventdata['iduser']        = $logdata['iduser'];
                $eventdata['idjos']         = $logdata['idjos'];
                $eventdata['title']         = JText::_('COM_VIRTUALDESK_EVENTLOG_ACTIVATED_TITLE');
                $eventdata['desc']          = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_ACTIVATED_DESC', VirtualDeskUserHelper::getUserName($logdata['iduser']), $logdata['iduser']);
                $eventdata['category']      = JText::_('COM_VIRTUALDESK_EVENTLOG_ACTIVATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);


                return true;
            }
        } else { // erro, não conseguiu carregar o registo de ativação
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_RECORDNOTFOUND'), 'error');
            $logdata['logstatus'] = 'ERR';
            $logdata['logdesc'] = JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_RECORDNOTFOUND');
            $vdlog->userActivationInsertLog($logdata);
            return false;
        }

    }


    /*
      * Depois de ser validado o token de activação deve ser executada esta função de modo a ativar o utilizador no VD e no Joomla
      */
    private static function SetUserActivated($VDUserId, $VDUserLogin)
    {
        $app = JFactory::getApplication();

        // Check for a user: verifica se tem os 2 Ids: Joomla User + VD User
        if (empty($VDUserId) || empty($VDUserLogin)) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_USERNOTFOUND'), 'error');
            return false;
        }

        // Carrega user VD da Tabela com Utilizadores VD
        $VirtualDeskUser = JTable::getInstance('User', 'VirtualDeskTable');
        $VirtualDeskUser->load($VDUserId);

        $UserLoadedId = $VirtualDeskUser->id;
        $UserLoadedIdJos = $VirtualDeskUser->idjos;
        $UserLoadedLogin = $VirtualDeskUser->login;
        $UserLoadedEmail = $VirtualDeskUser->email;


        if (empty($UserLoadedId) || empty($UserLoadedIdJos)) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_USERNOTFOUND'), 'error');
            return false;
        }

        // Get the user object: Joomla User
        $UserJos = JUser::getInstance($UserLoadedIdJos);

        // Make sure the user isn't a Super Admin.
        if ($UserJos->authorise('core.admin')) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_USERNOTFOUND'), 'error');
            return false;
        }


        // Verifica que o email do Joomla User == Virtual DeskUser, caso contrário dá erro ...
        if ($UserJos->email != $UserLoadedEmail || $UserJos->username != $UserLoadedLogin) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_USERNOTFOUND'), 'error');
            return false;
        }


        // guarda tabela com valore iniciais dos utilizadores,
        // se der erro num deles reverte gravando os dados existentes
//        $VDUserBackState = clone $VirtualDeskUser;
        $JoomlaUserState = clone $UserJos;


        // Activa o utilizador joomla
        $UserJos->activation = '';
        $UserJos->block = 0;

        // Save the joomla user to the database
        if (!$UserJos->save(true)) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_USERNOTFOUND'), 'error');
            return false;
        }

        // Activa o utilizador VD
        $VirtualDeskUser->blocked = 0;
        $VirtualDeskUser->activated = 1;
        $VirtualDeskUser->modified = JFactory::getDate()->format("Y-m-d H:i:s");

        // Atualiza
        if (!$VirtualDeskUser->store()) {   // Deu erro, então coloca o user joomla estado anterior da tabela de dados
            $JoomlaUserState->save();
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_USERNOTFOUND'), 'error');
            return false;
        }

        return true;
    }


   /*
   * Durante o processo de activação não devemos poder activar utilizadores admin ou VDAdmin
   */
    private static function checkIfUser2ActivationIsAdmin($UserId)
    {
        $app = JFactory::getApplication();
        // Get the user object: Joomla User
        $UserJos = JUser::getInstance($UserId);
        // Make sure the user isn't a Super Admin.
        if ($UserJos->authorise('core.admin')) {
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_ADMINNOTALLOWED'), 'error');
            return false;
        }
        // Make sure the user isn't in group VD Admin.
        $VDGroupAdminId = VirtualDeskUserHelper::getVirtualDeskGroupAdminId();
        if( in_array($VDGroupAdminId, $UserJos->groups ) )
        {   $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CONFIG_ACTIVATIONLINK_ADMINNOTALLOWED'), 'error');
            return false;
        }
       return true;
    }


    public function SendActivationEmail(JUser $JoomlaUser, $token)
    {
        $config = JFactory::getConfig();

        // Assemble the password reset confirmation link.
        $mode = $config->get('force_ssl', 0) == 2 ? 1 : (-1);
//        $itemid ="";
//        $itemid = $itemid !== null ? '&Itemid=' . $itemid : '';
        $link_root   = JUri::root().'activate/?token=' . $token;

        // Put together the email template data.
        $data = $JoomlaUser->getProperties();
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');
        $data['link_text'] = JRoute::_($link_root, false, $mode);
        //$data['link_html'] = JRoute::_($link, true, $mode);
        $data['link_html'] = JRoute::_($link_root, true, $mode);
        $data['token'] = $token;

        $subject = JText::sprintf($data['sitename']);


//        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation_email.html');
//        $body      = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY',$JoomlaUser->email,	$data['sitename'], $data['link_text'], $data['sitename'],$JoomlaUser->email, $data['token']);
//        $body      = sprintf($emailHTML,$body);

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation_email.html');

        $obParam      = new VirtualDeskSiteParamsHelper();

        $logoEmailAtivacaoConta = $obParam->getParamsByTag('logoEmailAtivacaoConta');
        $digitalGovLink = $obParam->getParamsByTag('linkVersaoAPP');
        $digitalGovVersao = $obParam->getParamsByTag('nomeAPP');
        $projectLink = $obParam->getParamsByTag('LinkCopyright');
        $projectNome = $obParam->getParamsByTag('copyrightAPP');
        $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
        $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
        $websiteCopyrightEmail = $obParam->getParamsByTag('websiteCopyrightEmail');
        $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');
        $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
        $nameAPP = $obParam->getParamsByTag('nameAPP');

        $BODY_TITLE         = JText::sprintf('Ativação da conta',$nameAPP);
        $BODY_MESSAGE       = JText::sprintf('Obrigado por se registar no '.$nameAPP.'. A sua conta foi criada e precisa de ser ativada antes de poder aceder.');
        $BODY_ACTIVATE_DESC = JText::sprintf('Para ativar a sua conta clique no botão abaixo.');
        $BODY_ACTIVATE_LINK = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_ACTIVATE_LINK',$data['link_text']);
        $BODY_ACTIVATE_LINK_LABEL = JText::sprintf('Ativar a conta');
        $BODY_FOOTER        = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_FOOTER',$data['sitename'],$JoomlaUser->username);

        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_ACTIVATE_DESC", $BODY_ACTIVATE_DESC, $body);
        $body      = str_replace("%BODY_ACTIVATE_LINK", $BODY_ACTIVATE_LINK, $body);
        $body      = str_replace("%BODY_ACTIVATE_LABELLINK", $BODY_ACTIVATE_LINK_LABEL, $body);
        $body      = str_replace("%BODY_FOOTER",$BODY_FOOTER, $body);
        $body      = str_replace("%BODY_DIGITALGOV_LINK",$digitalGovLink, $body);
        $body      = str_replace("%BODY_DIGITALGOV_VALUE",$digitalGovVersao, $body);
        $body      = str_replace("%BODY_NOMECAMARA_LINK",$projectLink, $body);
        $body      = str_replace("%BODY_NOMECAMARA_VALUE",$projectNome, $body);
        $body = str_replace("%TELEF", $contactoTelefCopyrightEmail, $body);
        $body = str_replace("%NAMESITE", $copyrightAPP, $body);
        $body = str_replace("%websiteCopyrightEmail", $websiteCopyrightEmail, $body);
        $body = str_replace("%linkCopyrightEmail", $linkCopyrightEmail, $body);
        $body = str_replace("%emailCopyrightGeral", $emailCopyrightGeral, $body);
        $body      = str_replace("%BODY_COPYRIGHT", " &copy; " . date("Y") . ' ' . $data['sitename'] , $body);

       // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo( $data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($JoomlaUser->email);
        $newActivationEmail->setSubject($data['sitename']);

        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoEmailAtivacaoConta, "banner", "Logo");

        $return = $newActivationEmail->send();

        // Send the password reset request email.
//        $return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $JoomlaUser->email, $subject, $body);

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }



    public static function loadLanguageFiles()
    {
        $lang      = JFactory::getLanguage();
        $extension = 'com_virtualdesk';
        $base_dir  = JPATH_SITE;
        $jinput    = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');
        $reload    = true;
        $lang->load($extension, $base_dir, $language_tag, $reload);
    }

}