<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskStatsHelper
{

    public static function getNumberOfActiveUsers()
    {
        // Number of Active Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('blocked') . '=0 AND ' . $db->quoteName('activated') . '=1')
        );

        $db->execute();

        return($db->getNumRows());
    }


    public static function getNumberOfInactiveUsers()
    {
        // Number of Active Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('blocked') . '=1 OR ' . $db->quoteName('activated') . '=0')
        );

        $db->execute();

        return($db->getNumRows());
    }



}