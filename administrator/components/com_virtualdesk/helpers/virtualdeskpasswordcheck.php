<?php
defined('_JEXEC') or die;

class VirtualDeskPasswordCheckHelper
{
    protected $new;
    protected $app;
    protected $user;
    protected $isnew;
    protected $execution_check = true;
    public $output_warning = false;
    protected $output_warning_array = array();
    protected $params;


    function __construct($componentName)
    {
        $this->app     = JFactory::getApplication();
        $input         = $this->app->input;
        //$componentName = $input ->get('option');
        if(!empty($componentName)) $this->params  = JComponentHelper::getParams($componentName);
    }


    public function executeChecks($user, $new, $isnew)
    {

//        if (empty($new['password_clear'])) {
//            return true;
//        }

        $new['password_clear'] =  $new['password1'];

        $this->new   = $new;
        $this->user  = $user;
        $this->isnew = $isnew;
        $this->app   = JFactory::getApplication();

        //$this->loadLanguage('plg_system_forcepasswordcomplexity', JPATH_ADMINISTRATOR);
        $this->passwordChecks();

        if (!empty($this->output_warning_array)) {
            $this->output_warning = implode('<br />', $this->output_warning_array);
            return false;
        }
        return true;
    }


/**
     * Does the main password checks
     */
    public function passwordChecks()
    {
        // Length of password
        $this->passwordCheckSpacesEndStart();

        // Length of password
        $this->passwordCheckLength();

        // Name in the password
        $this->passwordCheckName();

        // Email address in the password
        $this->passwordCheckEmail();

        // Entropy of password
        $this->passwordCheckEntropy();

        // Qunatity per character
        $this->passwordCheckQuantity();

        // Consecutive same characters
        $this->passwordCheckConsecutive();

        // Check specified types
        $this->passwordCheckTypes();
    }


    private function passwordCheckSpacesEndStart()
    {

        if( (substr($this->new['password_clear'],0,1) == ' ') || (substr($this->new['password_clear'],-1,1) == ' ') )
        {
            $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_STARTSENDS');
        }
    }


    /**
     * Checks the password length
     *
     * @throws Exception
     */
    private function passwordCheckLength()
    {
        $length = (int)$this->params->get('passwordcheck_length', 8);
        $length_input = strlen($this->new['password_clear']);

        if($length > $length_input)
        {
            $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_LENGTH', $length);
        }
    }

    /**
     * Creates the error or warning output message
     *
     * @param string $language_string
     * @param bool   $argument
     *
     * @return bool
     * @throws Exception
     */
    private function passwordCheckHandleError($language_string, $argument = false)
    {
        if(!empty($argument))
        {
//            if(empty($this->output_warning))
//            {
//                throw new Exception(JText::sprintf($language_string, $argument));
//            }

            $this->output_warning_array[] = JText::sprintf($language_string.'_WARNING', $argument);

            return false;
        }

//        if(empty($this->output_warning))
//        {
//            throw new Exception(JText::_($language_string));
//        }

        $this->output_warning_array[] = JText::_($language_string.'_WARNING');
    }

    /**
     * Checks whether password contains part of the user name
     *
     * @throws Exception
     */
    private function passwordCheckName()
    {
        $no_name = (int)$this->params->get('passwordcheck_no_name', 1);

        if(!empty($no_name))
        {
            if(stripos($this->new['password_clear'], $this->new['username']) !== false)
            {
                $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_NAME');
            }
        }
    }

    /**
     * Checks whether password contains part of the email address
     *
     * @throws Exception
     */
    private function passwordCheckEmail()
    {
        $no_email = (int)$this->params->get('passwordcheck_no_email', 1);

        if(!empty($no_email))
        {
            $email_input = explode('@', $this->new['email']);
            $email_domain = str_replace(strrchr($email_input[1], '.'), '', $email_input[1]);

            if(stripos($this->new['password_clear'], $email_input[0]) !== false OR stripos($this->new['password_clear'], $email_domain) !== false)
            {
                $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_EMAIL');
            }
        }
    }

    /**
     * Check the entropy of the password
     *
     * @throws Exception
     */
    private function passwordCheckEntropy()
    {
        $entropy = (float)$this->params->get('passwordcheck_entropy', 2);
        $length_input = strlen($this->new['password_clear']);
        $entropy_input = (float)$this->entropy($this->new['password_clear'], $length_input);

        if($entropy > $entropy_input)
        {
            $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_ENTROPY');
        }
    }

    /**
     * Calculates the entropy of the entered password in Bits
     *
     * See:
     * http://stackoverflow.com/questions/3198005/help-with-the-calculation-and-usefulness-of-password-entropy
     * http://codepad.org/OvvRKwQj
     *
     * @param string $password
     * @param int    $length
     *
     * @return float
     */
    private function entropy($password, $length)
    {
        $h = 0;

        foreach(count_chars($password, 1) as $v)
        {
            $p = $v / $length;
            $h -= $p * log($p) / log(2);
        }

        return number_format($h / 1.44, 2, '.', '');
    }

    /**
     * Checks the password for identical characters
     *
     * @throws Exception
     */
    private function passwordCheckQuantity()
    {
        $quantity_per_character = (int)$this->params->get('passwordcheck_quantity_per_character', 2);

        if(!empty($quantity_per_character))
        {
            $quantity_per_character_input = $this->quantityPerCharacter($this->new['password_clear'], $quantity_per_character);

            if(empty($quantity_per_character_input))
            {
                $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_QUANTITY');
            }
        }
    }

    /**
     * Checks the quantity per character of the entered password
     *
     * @param string $password
     * @param int    $quantity_per_character
     *
     * @return boolean
     */
    private function quantityPerCharacter($password, $quantity_per_character)
    {
        foreach(count_chars($password, 1) as $value)
        {
            if($value > $quantity_per_character)
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks the password for consecutive characters
     *
     * @throws Exception
     */
    private function passwordCheckConsecutive()
    {
        $consecutive_characters = (int)$this->params->get('passwordcheck_consecutive_characters', 1);

        if(!empty($consecutive_characters))
        {
            $consecutive_characters_input = $this->consecutiveSameCharacters($this->new['password_clear'], $consecutive_characters);

            if(empty($consecutive_characters_input))
            {
                $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_CONSECUTIVE');
            }
        }
    }

    /**
     * Checks the quantity of consecutive same characters of the entered password
     *
     * @param string $password
     * @param int    $consecutive_characters
     *
     * @return boolean
     */
    private function consecutiveSameCharacters($password, $consecutive_characters)
    {
        return !preg_match('@(.)\1{'.$consecutive_characters.'}@', $password);
    }

    /**
     * Checks the password for character types
     *
     * @throws Exception
     */
    private function passwordCheckTypes()
    {
        $types = array();
        $typesaz = (int)$this->params->get('passwordcheck_types_azmin',1);
        $typesAZ = (int)$this->params->get('passwordcheck_types_azmai',0);
        $types09 = (int)$this->params->get('passwordcheck_types_num',1);
        $typesspecial = (int)$this->params->get('passwordcheck_types_special',1);

        if($typesaz==1)  $types[] = 'a-z';
        if($typesAZ==1)  $types[] = 'A-Z';
        if($types09==1)  $types[] = '0-9';
        if($typesspecial==1)  $types[] = 'special';

        if(!empty($types))
        {
            $types_input = $this->checkCharacterTypes($this->new['password_clear'], $types);

            if(empty($types_input))
            {
                $types_array = array();

                foreach($types as $type)
                {
                    $types_array[] = $this->errorTypesOutput($type);
                }

                $types_required = implode(', ', $types_array);

                $this->passwordCheckHandleError('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE', $types_required);
            }
        }
    }

    /**
     * Checks the entered character types of the password
     *
     * @param string $password
     * @param array  $types
     *
     * @return bool
     */
    private function checkCharacterTypes($password, $types)
    {
        foreach($types as $type)
        {
            if($type != 'special')
            {
                if(!preg_match('@['.$type.']@', $password))
                {
                    return false;
                }

                continue;
            }

            if(!preg_match('@[^0-9a-zA-Z]@', $password))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the translation of the error type
     *
     * @param string $type
     *
     * @return string
     */
    private function errorTypesOutput($type)
    {
        $type_replacement = '';

        if($type == 'A-Z')
        {
            $type_replacement = JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_UPPERCASELETTER');
        }
        elseif($type == 'a-z')
        {
            $type_replacement = JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_LOWERCASELETTER');
        }
        elseif($type == '0-9')
        {
            $type_replacement = JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_NUMBER');
        }
        elseif($type == 'special')
        {
            $type_replacement = JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_SPECIALCHARACTER');
        }

        return $type_replacement;
    }
}




