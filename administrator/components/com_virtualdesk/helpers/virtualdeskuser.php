<?php
/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');
JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables');

/**
 * VirtualDesk User helper class.
 *
 * @since  1.6
 */
class VirtualDeskUserHelper
{
    const groupAdminName = 'Virtual Desk Admin';

    const groupUserName  = 'Virtual Desk User';

    public static function getUser($UserID)
    {
        $table = JTable::getInstance('User', 'VirtualDeskTable');

        // Check for a user.
        if (empty($UserID))
        {
            return false;
        }

        try
        {
            $table->load($UserID);

            $dataReturn = array();
            $dataReturn['id']    = $table->id;
            $dataReturn['idjos'] = $table->idjos;
            $dataReturn['login']  = $table->login;
            $dataReturn['name']  = $table->name;
            $dataReturn['email'] = $table->email;

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getUserName($UserID)
    {
        $table = JTable::getInstance('User', 'VirtualDeskTable');

        // Check for a user.
        if (empty($UserID)) {
            return false;
        }
        try {
            $table->load($UserID);
            return($table->name);
        }
        catch (RuntimeException $e) {
            return false;
        }
    }


    public static function getUserJoomlaByVDUserId($UserID)
    {
        $table = JTable::getInstance('User', 'VirtualDeskTable');

        // Check for a user.
        if (empty($UserID)) {
            return false;
        }
        try {
            $table->load($UserID);
            if( (integer)$table->id !== (integer)$UserID ) return false;
            if( empty($table->idjos) ) return false;
            $userJos= JFactory::getUser($table->idjos); // Get the user object $app =
            if(empty($userJos)) return false;
            return($userJos);
        }
        catch (RuntimeException $e) {
            return false;
        }
    }


    public static function getUserEmail($UserID)
    {
        $table = JTable::getInstance('User', 'VirtualDeskTable');

        // Check for a user.
        if (empty($UserID)) {
            return false;
        }
        try {
            $table->load($UserID);
            return($table->email);
        }
        catch (RuntimeException $e) {
            return false;
        }
    }


    public static function getUserTableByEmail($EMail)
    {
        $table = JTable::getInstance('User', 'VirtualDeskTable');

        // Check for a user.
        if (empty($EMail))
        {
            return false;
        }

        try
        {   $filter = array ('email'=>$EMail);
            $table->load($filter);
            return($table);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getUserByEmail($EMail)
    {
        $table = JTable::getInstance('User', 'VirtualDeskTable');

        // Check for a user.
        if (empty($EMail))
        {
            return false;
        }

        try
        {   $filter = array ('email'=>$EMail);
            $table->load($filter);

            $dataReturn = array();
            $dataReturn['id']    = $table->id;
            $dataReturn['idjos'] = $table->idjos;
            $dataReturn['login']  = $table->login;
            $dataReturn['name']  = $table->name;
            $dataReturn['email'] = $table->email;

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * get group VDAdmin id by name
    */
    public static function getVirtualDeskGroupAdminId()
    {

        // Verifica se o campo de login é do tipo Text (livre) ou do tipo NIF
        $permvd_joomlagroup_vdadmin = JComponentHelper::getParams('com_virtualdesk')->get('permvd_joomlagroup_vdadmin');

        if((int)$permvd_joomlagroup_vdadmin>=10) return $permvd_joomlagroup_vdadmin;

        // Se não está nos parâmetros vai à BD
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id', 'title'))
            ->from("#__usergroups")
            ->where($db->quoteName('title').'=\''.$db->escape(self::groupAdminName).'\' and id >=10')
        );
        $row = $db->loadAssoc();

        if( (isset($row['id'])) && ((integer)$row['id']>0) && ((string)$row['title']==self::groupAdminName) )  return ($row['id']);

        return false; // return false if group name not found
    }


    /*
    * get group VDUser id by name
    */
    public static function getVirtualDeskGroupUserId()
    {
        // Verifica se o campo de login é do tipo Text (livre) ou do tipo NIF
        $permvd_joomlagroup_vduser = JComponentHelper::getParams('com_virtualdesk')->get('permvd_joomlagroup_vduser');

        if((int)$permvd_joomlagroup_vduser>=11) return $permvd_joomlagroup_vduser;

        // Se não está nos parâmetros vai à BD

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id', 'title'))
            ->from("#__usergroups")
            ->where($db->quoteName('title').'=\''.$db->escape(self::groupUserName).'\' and id >=11')
        );
        $row = $db->loadAssoc();

        if( (isset($row['id'])) && ((integer)$row['id']>0) && ((string)$row['title']==self::groupUserName) )  return ($row['id']);

        return false; // return false if group name not found
    }


    /*
   * Apresenta mensagem se não encontra dos grupos necessário
   */
    public static function checkVirtualDeskGroups()
    {
        $app = JFactory::getApplication();
        // Verifica o Group User
        $checkGroupUser  = self::getVirtualDeskGroupUserId();
        if($checkGroupUser===false)
        { $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CHECK_GROUPUSER_NOTFOUND'), 'error');
        }



        // Verifica o Group Admin
        $checkGroupAdmin = self::getVirtualDeskGroupAdminId();
        if($checkGroupAdmin===false)
        { $app->enqueueMessage(JText::_('COM_VIRTUALDESK_CHECK_GROUPADMIN_NOTFOUND'), 'error');
        }

        return ;
    }


   /*
   * Check if user is  Joomla Admin ou Super Admin
   */
    public static function checkIfUserIsAdmin($UserId)
    {
        // Get the user object: Joomla User
        $UserJos = JUser::getInstance($UserId);
        if ($UserJos->authorise('core.admin')) return true;

        // By default Joomla Admin users Super admin (Groups 8) and Administrator (Group 7).
        if( in_array(7,$UserJos->groups) )  return true;

        // else
        return false;
    }


    /*
    * Check if username / login is  Joomla Admin ou Super Admin
    */
    public static function checkIfUserNameIsCompliant($username)
    {
        if(empty($username)) return true;

        $isUsernameCompliant  = !(preg_match('#[<>"\'%;()&\\\\]|\\.\\./#', $username) || strlen(utf8_decode($username)) < 2 || trim($username) != $username);

        return $isUsernameCompliant;
    }


    /*
    * Check if username está configurado com o tipo NIF e valida o valor introduzido
    */
    public static function checkUserNameIsNIF($username)
    {
        if(empty($username)) return true;
        $resUserNameTypeNIF = true;

        // Verifica se o campo de login é do tipo Text (livre) ou do tipo NIF
        $setUserFieldLoginType = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
        if($setUserFieldLoginType=="login_as_nif") {
            $resUserNameTypeNIF =  VirtualDeskNIFCheckHelper::executeCheck($username);
        }
        return $resUserNameTypeNIF;
    }



    public static function randomPassword($size = 10) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'; //!@#$%^&*?+-_~
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $size; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /*
    * envia email com  senha quando é pedida nova senha .
    * Se for enviado o token é para enviar link para ativação da conta.
    *
    */
    public static function SendForgotPasswordEmail(JUser $JoomlaUser, $newpassword, $token = '')
    {
        if( empty($newpassword) || empty($JoomlaUser->email) ){
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        $obParam      = new VirtualDeskSiteParamsHelper();
        $logosendmailPassword = $obParam->getParamsByTag('logosendmailPassword');
        $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
        $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
        $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
        $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
        $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
        $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');
        $nameAPP  = $obParam->getParamsByTag('nameAPP');

        $config = JFactory::getConfig();
        // Assemble the password reset confirmation link.
        $mode      = $config->get('force_ssl', 0) == 2 ? 1 : (-1);
        $link_root = JUri::root();
        // Se tiver token é para enviar o link para ativar a conta e só depois é que pode utilizar a nova senha enviada
        if (!empty($token)) {
            $link_ativate   = JUri::root().'activate/?token=' . $token;
            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskforgotpassword_email_activate.html');
        }
        else {
            $link_ativate   = '';
            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskforgotpassword_email.html');
        }

        // Put together the email template data.
        $data = $JoomlaUser->getProperties();
        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');
        $data['link_text'] = JRoute::_($link_ativate, false, $mode);
        $data['link_html'] = JRoute::_($link_ativate, true, $mode);


        $linkToSite = '<a href="'. $link_root .'" >'. JText::sprintf($nameAPP . ' da ' . $nomeMunicipio) .'</a>';

        $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
        $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
        $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
        $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_TITLE');
        $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_GREETING',$JoomlaUser->email);
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_MESSAGE',$nameAPP, $nomeMunicipio);
        $BODY_NEWPASS_DESC  = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_NEWPASS_DESC',$newpassword);
        if (!empty($token)) {
            $BODY_ACTIVATE_DESC  = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_ACTIVATE_DESC');
            $BODY_ACTIVATE_LINK  = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_ACTIVATE_LINK',$data['link_text']);
            $BODY_ACTIVATE_LABEL = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_ACTIVATE_LABEL',$data['link_text']);
            $BODY_FOOTER = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_FOOTER_ACTIVATE', $linkToSite, $JoomlaUser->username);
        }
        else {
            $BODY_ACTIVATE_DESC  = '';
            $BODY_ACTIVATE_LINK  = '';
            $BODY_ACTIVATE_LABEL = '';
            $BODY_FOOTER = JText::sprintf('COM_VIRTUALDESK_FORGOTPASSWORD_SENDMAIL_BODY_FOOTER_NOACTIVATE', $linkToSite, $JoomlaUser->username);
        }

        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_ACTIVATE_DESC", $BODY_ACTIVATE_DESC, $body);
        $body      = str_replace("%BODY_ACTIVATE_LABEL", $BODY_ACTIVATE_LABEL, $body);
        $body      = str_replace("%BODY_ACTIVATE_LINK", $BODY_ACTIVATE_LINK, $body);
        $body      = str_replace("%BODY_NEWPASS_DESC", $BODY_NEWPASS_DESC, $body);
        $body      = str_replace("%BODY_FOOTER",$BODY_FOOTER, $body);
        $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
        $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
        $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
        $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
        $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($config->get('mailfrom'));
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($JoomlaUser->email);
        $newActivationEmail->setSubject($data['sitename']);
        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailPassword, "banner", "Tickets_BannerEmail.png");

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }



}