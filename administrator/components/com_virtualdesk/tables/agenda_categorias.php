<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableAgendaCategorias extends JTable
{

    public $id = null;
    public $cat_PT = null;
    public $cat_EN = null;
    public $cat_FR = null;
    public $cat_DE = null;
    public $Ref = null;
    public $estado = null;


    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_v_agenda_categoria', 'id', $db);
    }
}