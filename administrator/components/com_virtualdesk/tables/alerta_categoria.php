<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableAlertaCategoria extends JTable
{

    public $ID_categoria = null;
    public $creation_date = null;
    public $modify_data = null;
    public $name_EN = null;
    public $name_PT = null;
    public $Ref = null;
    public $id_concelho = null;
    public $conf_email = null;


    /**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_alerta_categoria', 'ID_categoria', $db);
	}
}