<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableUser extends JTable
{

    public $id = null;
    public $idjos = null;
    public $login = null;
    public $name = null;
    public $email = null;
    public $created = null;
    public $address = null;
    public $postalcode = null;
    public $civilid = null;
    public $fiscalid = null;
    public $useravatar = null;

    public $firma = null;
    public $designacaocomercial = null;
    public $sectoratividade = null;
    public $caeprincipal = null;
    public $phone1 = null;
    public $phone2 = null;
    public $website = null;
    public $facebook = null;
    public $emailsec = null;
    public $concelho = null;
    public $freguesia = null;
    public $localidade = null;
    public $areaact = null;
    public $managername = null;
    public $managerposition = null;
    public $managercontact = null;
    public $manageremail = null;
    public $maplatlong = null;

    public $cargo = null;
    public $funcao = null;

    public $veracidadedados = null;
    public $politicaprivacidade = null;


    /**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_users', 'id', $db);
	}
}