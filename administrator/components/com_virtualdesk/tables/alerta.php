<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableAlerta extends JTable
{

    public $Id_alerta = null;
    public $codOco = null;
    public $categoria = null;
    public $subcategoria = null;
    public $descricao = null;
    public $anexos = null;
    public $concelho = null;
    public $freguesia = null;
    public $sitio = null;
    public $local = null;
    public $latitude = null;
    public $longitude = null;
    public $pontos_referencia = null;
    public $data_criacao = null;
    public $data_alteracao = null;
    public $estado = null;
    public $publishFP = null;
    public $descritivoBO = null;
    public $observacoes = null;
    public $utilizador = null;
    public $email = null;
    public $nif = null;
    public $telefone = null;
    public $id_procmanager = null ;



    /**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_alerta', 'Id_alerta', $db);
	}
}