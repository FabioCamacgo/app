<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTablealertarpt extends JTable
{
    public $id = null;
    public $referencia = null;
    public $categoria = null;
    public $subcategoria = null;
    public $descricao = null;
    public $concelho = null;
    public $freguesia = null;
    public $sitio = null;
    public $morada = null;
    public $pontosReferencia = null;
    public $latitude = null;
    public $longitude = null;
    public $nome = null;
    public $email = null;
    public $telefone = null;
    public $dataEntrada = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_AlertarPT_ocorrencias', 'id', $db);
    }
}