<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableDocumentos extends JTable
{
    public $id = null;
    public $referencia = null;
    public $nome = null;
    public $id_nivel1 = null;
    public $id_nivel2 = null;
    public $id_nivel3 = null;
    public $id_nivel4 = null;
    public $estado = null;
    public $visualizacoes = null;
    public $tags = null;
    public $data_criacao = null;
    public $data_alteracao = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_Documentos', 'id', $db);
    }
}