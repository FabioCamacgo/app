<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableUploadsAppComercial extends JTable
{

    public $id = null;
    public $referencia = null;
    public $nome = null;
    public $email = null;
    public $proprietario = null;
    public $nome_foto = null;
    public $estado = null;
    public $veracidadedados = null;
    public $politicaprivacidade = null;
    public $data_criacao = null;
    public $data_alteracao = null;


    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_uploads_appcomercial', 'id', $db);
    }
}