<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableCulturaEventos extends JTable
{

    public $id_evento = null;
    public $nif = null;
    public $referencia_evento = null;
    public $cat_evento = null;
    public $nome_evento = null;
    public $desc_evento = null;
    public $freguesia_evento = null;
    public $local_evento = null;
    public $data_inicio = null;
    public $hora_inicio = null;
    public $data_fim = null;
    public $hora_fim = null;
    public $mes = null;
    public $Ano = null;
    public $latitude = null;
    public $longitude = null;
    public $website = null;
    public $facebook = null;
    public $instagram = null;
    public $preco = null;
    public $preco_criancas = null;
    public $preco_estudante = null;
    public $preco_adulto = null;
    public $preco_senior = null;
    public $preco_grupo = null;
    public $consideracoes_preco = null;
    public $observacoes = null;
    public $layout = null;
    public $estado = null;
    public $data_criacao = null;
    public $data_alteracao = null;


    /**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_Cultura_Eventos', 'id_evento', $db);
	}
}