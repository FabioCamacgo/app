<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableDiretorioServicos extends JTable
{
    public $id = null;
    public $referencia = null;
    public $nome_PT = null;
    public $nome_comercial = null;
    public $nipc = null;
    public $email_contacto = null;
    public $tipo_firma = null;
    public $descricao = null;
    public $categoria = null;
    public $subcategoria = null;
    public $concelho = null;
    public $freguesia = null;
    public $morada = null;
    public $latitude = null;
    public $longitude = null;
    public $telefone = null;
    public $website = null;
    public $email_comercial = null;
    public $email_administrativo = null;
    public $hora_abertura = null;
    public $hora_fecho = null;
    public $aberto_almoco = null;
    public $inicio_almoco = null;
    public $fim_almoco = null;
    public $abertoFimSemana = null;
    public $abertura_sabado = null;
    public $fecho_sabado = null;
    public $abertura_domingo = null;
    public $fecho_domingo = null;
    public $facebook = null;
    public $instagram = null;
    public $youtube = null;
    public $twitter = null;
    public $idiomas = null;
    public $outros_idiomas = null;
    public $cleanAndSafe = null;
    public $produtoMadeira = null;
    public $informacoes = null;
    public $videos = null;
    public $tipo_conta = null;
    public $estado = null;
    public $data_criacao = null;
    public $data_alteracao = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_MadeiraMagazine_DirServicos_Empresas', 'id', $db);
    }
}