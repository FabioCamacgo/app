<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableConfigPluginLayout extends JTable
{
    public $id = null;
    public $idplugin = null;
    public $idtipolayout = null;
    public $nome = null;
    public $tagchave = null;
    public $descricao = null;
    public $created = null;
    public $modified = null;


    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_config_plugin_layout', 'id', $db);
    }
}