<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableAlertaEncaminharGroupHist extends JTable
{
    public $id = null;
    public $id_encaminhar = null;
    public $id_group = null;
    public $atual = null;
    public $createdby = null;
    public $modifiedby = null;
    public $created = null;
    public $modified = null;

    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_alerta_encaminhar_group_hist', 'id', $db);
    }
}