<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableFormularioFiles extends JTable
{
    public $id = null;
    public $idprocesso = null;
    public $tagprocesso = null;
    public $desc = null;
    public $basename = null;
    public $ext = null;
    public $type = null;
    public $size = null;
    public $filepath = null;
    public $filename = null;
    public $created = null;
    public $modified = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_EuropaCriativa_files', 'id', $db);
    }
}