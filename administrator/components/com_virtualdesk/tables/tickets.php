<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableTickets extends JTable
{
    public $id = null;
    public $iduser = null;
    public $nif    = null;
    public $referencia = null;
    public $assunto = null;
    public $descricao = null;
    public $observacoes = null;
    public $id_departamento = null;
    public $id_estado = null;
    public $data_criacao = null;
    public $data_alteracao = null;



    /**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_tickets', 'id', $db);
	}
}