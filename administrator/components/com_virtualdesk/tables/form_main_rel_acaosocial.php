<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableFormMainRelAcaoSocial extends JTable
{

    public $id = null;
    public $form_id = null;
    public $ref = null;
    public $user_id = null;
    public $estado_id = null;
    public $create_date = null;
    public $modify_date = null;

    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_form_main_rel_acaosocial', 'id', $db);
    }
}