<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableNotificacaoHistorico extends JTable
{
    public $id = null;
    public $id_notificacao = null;
    public $nome = null;
    public $descricao = null;
    public $iduser = null;
    public $iduserjos = null;
    public $anulado = null;
    public $setbymanager = null;
    public $visiible4user = null;
    public $created = null;
    public $modified = null;

    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_notificacao_historico', 'id', $db);
    }
}