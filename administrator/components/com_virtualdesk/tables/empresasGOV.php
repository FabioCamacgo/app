<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableEmpresas extends JTable
{
    public $id = null;
    public $referencia = null;
    public $nome = null;
    public $designacao = null;
    public $apresentacao = null;
    public $categoria = null;
    public $subcategoria = null;
    public $morada = null;
    public $latitude = null;
    public $longitude = null;
    public $codigo_postal = null;
    public $freguesia = null;
    public $nif = null;
    public $telefone = null;
    public $email = null;
    public $website = null;
    public $facebook = null;
    public $instagram = null;
    public $nome_responsavel = null;
    public $cargo_responsavel = null;
    public $telefone_responsavel = null;
    public $mail_responsavel = null;
    public $nRegisto_Alojamento = null;
    public $capacidade_Alojamento = null;
    public $unidades_Alojamento = null;
    public $tipologia_Alojamento = null;
    public $nQuartos_Alojamento = null;
    public $nUtentes_Alojamento = null;
    public $nCamas_Alojamento = null;
    public $estado = null;
    public $layout = null;
    public $data_criacao = null;
    public $data_alteracao = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_Empresas', 'id', $db);
    }
}