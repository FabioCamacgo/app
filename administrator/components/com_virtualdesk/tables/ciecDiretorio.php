<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableCiecDiretorio extends JTable
{
    public $id = null;
    public $referencia = null;
    public $projeto = null;
    public $designacao = null;
    public $programa = null;
    public $entidade = null;
    public $lider = null;
    public $pais_lider = null;
    public $ano_Apoio = null;
    public $linha_Financiamento = null;
    public $escala = null;
    public $distrito = null;
    public $concelho = null;
    public $tipologia = null;
    public $categoria = null;
    public $link = null;
    public $data_Inicio = null;
    public $data_Fim = null;
    public $call = null;
    public $valorFinanciamento = null;
    public $layout = null;
    public $data_Criacao = null;
    public $data_Alteracao = null;
    public $estado = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_EuropaCriativa_ProjetosApoiados', 'id', $db);
    }
}