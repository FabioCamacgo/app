<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableVAgendaEmpresas extends JTable
{
    public $id = null;
    public $referencia = null;
    public $nome_PT = null;
    public $nome_comercial = null;
    public $nipc = null;
    public $email_contacto = null;
    public $descricao = null;
    public $categoria = null;
    public $subcategoria = null;
    public $tipoEventos = null;
    public $outroTipoEventos = null;
    public $concelho = null;
    public $freguesia = null;
    public $morada = null;
    public $latitude = null;
    public $longitude = null;
    public $telefone = null;
    public $website = null;
    public $email_comercial = null;
    public $email_administrativo = null;
    public $facebook = null;
    public $instagram = null;
    public $youtube = null;
    public $twitter = null;
    public $estado = null;
    public $data_criacao = null;
    public $data_alteracao = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_v_agenda_Empresas', 'id', $db);
    }
}