<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTablefaleConnosco extends JTable
{
    public $id = null;
    public $id_externo = null;
    public $website = null;
    public $tipo_form = null;
    public $assunto_form = null;
    public $nome_projeto = null;
    public $url_projeto = null;
    public $especificacao_assunto = null;
    public $populacao = null;
    public $categorias = null;
    public $outras_categorias = null;
    public $num_tecnicos = null;
    public $designacao = null;
    public $marca_municipal = null;
    public $outra_marca_municipal = null;
    public $obs_Gerais = null;
    public $esp_parceria = null;
    public $assunto_reuniao = null;
    public $data_reuniao = null;
    public $hora_reuniao = null;
    public $nome_reuniao = null;
    public $ref_vaga_trabalho = null;
    public $nomeCandidato = null;
    public $emailCandidato = null;
    public $apresentacao_vaga_trabalho = null;
    public $nomeCV = null;
    public $emailCV = null;
    public $apresentacao_cv = null;
    public $esp_bloco_parceria = null;
    public $outro_assunto = null;
    public $elogios_sugestoes = null;
    public $concelho = null;
    public $nif = null;
    public $email = null;
    public $nome = null;
    public $cargo = null;
    public $estado = null;
    public $dataEntrada = null;
    public $data_Alteracao = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_FaleConnosco', 'id', $db);
    }
}