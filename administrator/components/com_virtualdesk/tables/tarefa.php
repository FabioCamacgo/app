<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableTarefa extends JTable
{
    public $id = null;
    public $id_tarefa_estado = null;
    public $id_tarefa_tipoprocesso = null;
    public $nome = null;
    public $descricao = null;
    public $dataini = null;
    public $datafim = null;
    public $created = null;
    public $modified = null;

    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_tarefa', 'id', $db);
    }
}