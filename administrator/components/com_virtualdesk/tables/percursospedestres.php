<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 *
 * @since  0.0.1
 */
class VirtualDeskTablePercursosPedestres extends JTable
{

    public $id = null;
    public $referencia = null;
    public $percurso = null;
    public $descricao = null;
    public $descricaoEN = null;
    public $descricaoFR = null;
    public $descricaoDE = null;
    public $tipologia = null;
    public $paisagem = null;
    public $percurso_laurissilva = null;
    public $costa_laurissilva = null;
    public $costa_sol = null;
    public $mmg = null;
    public $sentido = null;
    public $terreno = null;
    public $vertigens = null;
    public $distancia = null;
    public $idDistancia = null;
    public $duracao = null;
    public $idDuracao = null;
    public $altitude_max = null;
    public $altitude_min = null;
    public $subida_acumulado = null;
    public $descida_acumulado = null;
    public $dificuldade = null;
    public $distrito = null;
    public $concelho = null;
    public $freguesia = null;
    public $inicio_freguesia = null;
    public $fim_freguesia = null;
    public $zonamento = null;
    public $inicio_sitio = null;
    public $fim_sitio = null;
    public $localidades = null;
    public $estado = null;
    public $creditosFotoCapa = null;
    public $linkCreditos = null;
    public $destaques = null;
    public $destaquesEN = null;
    public $destaquesFR = null;
    public $destaquesDE = null;
    public $hastags = null;
    public $patrocinador = null;
    public $data_criacao = null;
    public $data_alteracao = null;


    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_PercursosPedestres', 'id', $db);
    }
}