<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableAgendaEventos extends JTable
{

    public $id = null;
    public $referencia = null;
    public $nif = null;
    public $categoria = null;
    public $subcategoria = null;
    public $tags = null;
    public $nome_evento = null;
    public $descricao_evento = null;
    public $data_inicio = null;
    public $ano_inicio = null;
    public $mes_inicio = null;
    public $data_fim = null;
    public $ano_fim = null;
    public $mes_fim = null;
    public $hora_inicio = null;
    public $hora_fim = null;
    public $local_evento = null;
    public $distrito = null;
    public $concelho = null;
    public $freguesia = null;
    public $latitude = null;
    public $longitude = null;
    public $tipo_evento = null;
    public $observacoes_precario = null;
    public $facebook = null;
    public $instagram = null;
    public $youtube = null;
    public $vimeo = null;
    public $videosEvento = null;
    public $catVideosEvento = null;
    public $evento_premium = null;
    public $top_Evento = null;
    public $agenda_Municipal = null;
    public $mes_a_mes = null;
    public $estado_evento = null;
    public $balcao = null;
    public $data_criacao = null;
    public $data_alteracao = null;


    /**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_v_agenda_eventos', 'id', $db);
	}
}