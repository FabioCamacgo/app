<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 *
 * @since  0.0.1
 */
class VirtualDeskTableVertigens extends JTable
{

    public $id = null;
    public $name_PT = null;
    public $name_EN = null;
    public $name_FR = null;
    public $name_DE = null;
    public $estado = null;


    /**
     * Constructor
     *
     * @param   JDatabaseDriver  &$db  A database connector object
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_PercursosPedestres_Vertigens', 'id', $db);
    }
}