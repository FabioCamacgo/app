<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * @since  0.0.1
 */
class VirtualDeskTableRelacaoFormsCamposFiles extends JTable
{
    public $id = null;
    public $form_id = null;
    public $fields_id = null;
    public $enabled = null;
    public $ordem = null;
    public $data_criacao = null;
    public $data_alteracao = null;

    /**
     * Constructor
     * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
     */
    function __construct(&$db)
    {
        parent::__construct('#__virtualdesk_form_fields_rel', 'id', $db);
    }
}