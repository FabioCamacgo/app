<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 * @since  0.0.1
 */
class VirtualDeskTableEnterprise extends JTable
{
    public $id = null;
    public $nipc = null;
    public $iduser = null;
    public $iduserjos = null;
    public $name = null;
    public $email = null;
    public $created = null;
    public $address = null;
    public $postalcode = null;
    public $enterpriseavatar = null;
    public $phone1 = null;
    public $phone2 = null;
    public $website = null;
    public $facebook = null;
    public $freguesia = null;
    public $localidade = null;
    public $areaact = null;


    /**
	 * Constructor
	 * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_enterprise', 'id', $db);
	}
}