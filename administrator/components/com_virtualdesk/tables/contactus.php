<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
 
/**
 * @since  0.0.1
 */
class VirtualDeskTableContactUs extends JTable
{
    public $id = null;
    public $iduser = null;
    public $iduserjos = null;
    public $subject = null;
    public $description = null;
    public $description2 = null;
    public $description3 = null;
    public $description4 = null;
    public $idtype = null;
    public $idcategory = null;
    public $created = null;
    public $modified = null;
    public $attachment = null;
    public $obs = null;
    public $idsubcategory = null;
    public $idstatus = null;
    public $idctlanguage = null;
    public $vdwebsitelist = null;
    public $vdmenumain = null;
    public $vdmenusec = null;
    public $vdmenusec2 = null;


    /**
	 * Constructor
	 * @param   JDatabaseDriver  &$db  A database connector object
     * @since  0
	 */
	function __construct(&$db)
	{
		parent::__construct('#__virtualdesk_contactus', 'id', $db);
	}
}