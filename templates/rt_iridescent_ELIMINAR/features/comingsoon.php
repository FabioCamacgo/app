<?php
/**
* @version   $Id: comingsoon.php 26122 2015-01-27 15:27:42Z james $
* @author    RocketTheme http://www.rockettheme.com
* @copyright Copyright (C) 2007 - 2015 RocketTheme, LLC
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
*
*/

defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');
/**
 * @package     gantry
 * @subpackage  features
 */
class GantryFeatureComingSoon extends GantryFeature {
    var $_feature_name = 'comingsoon';

    function isEnabled(){
    	if ($this->get('enabled')) {
        	return true;
        }
    }
    
    function isInPosition($position) {
        return false;
    }
    function isOrderable(){
        return true;
    }
    
    function init() {
        global $gantry;
        $input   = JFactory::getApplication()->input;
        $user    = JFactory::getUser();
        $isAdmin = $user->get('isRoot');

        if ($input->getString('tmpl')!=='comingsoon') {
            if (!$isAdmin) {
                header("Location: ".$gantry->baseUrl."?tmpl=comingsoon");
            }
        }
    }
}
