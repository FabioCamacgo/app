<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$this->setGenerator('');
$jinput = JFactory::getApplication()->input;
$exportformat = $jinput->get('tmplformat','','string');
$exportorientation = $jinput->get('tmplorientation','portrait','string');

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

// Se for para enviar diretamente para um PDF
if($exportformat==='pdf') $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/html2pdf/html2pdf.bundle.min.js' . $addscript_end;

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<?php
/* load common headers for template: head, css, javascript */
require_once(dirname(__FILE__) . '/tpl_headers.php');
?>
<style>
    body {background-color: transparent;}
    .actions {display:none !important;}
    form .form-actions {display:none;}
    .form .form-bordered .form-group > div {padding: 2px;}
    .form .form-bordered .form-group .control-label {padding-top: 7px;}
    .form .form-bordered .form-group {border-bottom: 2px solid #d4d4d4;}
    <?php if($exportformat=='pdf') : ?>
        <?php if($exportorientation==='landscape') : ?>
        .container, .container-fluid {padding-left: 15px; padding-right: 65px;}
        <?php else: ?>
        .container, .container-fluid {width: 740px; padding-left: 5px; padding-right: 15px;}
        .form .form-bordered .form-group .control-label {padding-top: 0px;}
        <?php endif; ?>
    <?php elseif($exportformat==''): ?>
        <?php if($exportorientation=='landscape') : ?>
        .container, .container-fluid {width: 960px; padding-left: 15px; padding-right: 15px;}
        <?php else: ?>
        .container, .container-fluid {width: 750px; padding-left: 15px; padding-right: 15px;}
        .form .form-bordered .form-group .control-label {padding-top: 0px;}

        <?php endif; ?>
    <?php endif; ?>
</style>

<body class="site <?php echo $option
    . ' view-' . $view
    . ($layout ? ' layout-' . $layout : ' no-layout')
    . ($task ? ' task-' . $task : ' no-task')
    . ($itemid ? ' itemid-' . $itemid : '')
    . ($params->get('fluidContainer') ? ' fluid' : '');
echo ($this->direction == 'rtl' ? ' rtl' : '');
echo ('page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed');
?>"  >

<div class="page-container">

    <div class="container">

    <jdoc:include type="modules" name="position-2" style="none" />

	<jdoc:include type="component" />

    </div>
</div>

    <?php
    /*load common footer for template: javascript...*/
    require_once(dirname(__FILE__) . '/tpl_footers.php');
    ?>


<?php if($exportformat==='pdf') : ?>
<?php echo $localScripts; ?>
    <script>
       jQuery( window ).load(function() {
           console.log('loaded');

           setTimeout(function() {
               var element2PDF = document.body.childNodes[1]; // document.body;
               var opt2PDF = {
                   margin:       [1,1],
                   filename:     'vdprint.pdf',
                   pagebreak:    {avoid: ['img']},
                   image:        { type: 'jpeg', quality: 1 },
                   html2canvas:  { scale: 2 },
                   jsPDF:        { unit: 'mm', format: 'a4', orientation: '<?php echo $exportorientation; ?>' }
               };

               // New Promise-based usage:
               html2pdf().set(opt2PDF).from(element2PDF).save().then(function() {
                  setTimeout(function() { window.close(); }, 500);
               });

           }, 1000);

       });

    </script>
<?php endif; ?>

</body>
</html>

