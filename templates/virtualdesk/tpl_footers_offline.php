<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common footer for template: javascript...
 */

defined('_JEXEC') or die;
?>

<jdoc:include type="modules" name="debug" style="none" />

<?php
    // CUSTOM JS DASHboard
    echo ("<!--[if lt IE 9]>");
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/respond.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/excanvas.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/ie8.fix.min.js');
    echo ("<![endif]-->");
?>