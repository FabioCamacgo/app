<?php
    /**
     * @package     Joomla.Site
     * @subpackage  Templates.protostar
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $templateName      = 'virtualdesk';

    $this->setGenerator('');

    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;
    //$language_tag = $lang->getTag(); // loads the current language-tag
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);

    // Se não estiver ativo o registo de novos utilizadores não deve depois surgir o link
    $configSetNewRegistration = JComponentHelper::getParams('com_virtualdesk')->get('setnewregistration');

    // Se não estiver ativo o pedido de nova senha...
    $configSetForgotPassword  = JComponentHelper::getParams('com_virtualdesk')->get('setforgotpassword');

    // Verifica qual o layout de login a apresentar
    $setloginlayout = JComponentHelper::getParams('com_virtualdesk')->get('setloginlayout');

    // Verifica se o campo de login é do tipo Text (livre) ou do tipo NIF
    $setUserFieldLoginType = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');

    // Verifica se permite no login ter campos multiplos (exemplo NIF e Text), nesse caso por defeito é NIF, mas se receber por GET ou POST um parametro "allowtext"
    // Vai carregar o ecrã como se fosse texto para o login
    // Esse parametro "allowtextlogin" deve surgir no link abaixo do campos do form...
    $setUserFieldLoginTypeAllowMultiple = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type_allowmultiple');

    $getAllowTextLogin = $jinput->get('allowtextlogin', '', 'string');

    // Se por defeito é NIf ...
    if( $setUserFieldLoginType=="login_as_nif" ) {



        if( $setUserFieldLoginTypeAllowMultiple == '1' && $getAllowTextLogin=='allow' ) {
            $CampoForm_Username_Label = JText::_('COM_VIRTUALDESK_USER_LOGIN_LABEL');
            $CampoForm_LoginFormAlert = JText::_('COM_VIRTUALDESK_LOGINFORM_ALERT');
            $CampoForm_Username_MaxLength = "250" ;
            $setUserFieldLoginType_JS = -1;
        }
        else{
            // se não permite o texto livre no login OU não ativou a opção então mantém apenas NIF...
            $CampoForm_Username_Label = JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL');
            $CampoForm_LoginFormAlert = JText::_('COM_VIRTUALDESK_LOGINFORM_ALERT_NIF');
            $CampoForm_Username_MaxLength = "9";
            $setUserFieldLoginType_JS = 1;
        }
    }
    else {
        $CampoForm_Username_Label = JText::_('COM_VIRTUALDESK_USER_LOGIN_LABEL');
        $CampoForm_LoginFormAlert = JText::_('COM_VIRTUALDESK_LOGINFORM_ALERT');
        $CampoForm_Username_MaxLength = "250" ;
        $setUserFieldLoginType_JS = -1;
    }

    $obParam      = new VirtualDeskSiteParamsHelper();

    $appComercial = $obParam->getParamsByTag('appComercial');
    $cssFile = $obParam->getParamsByTag('cssFileGeral');
    $logosbrandpartner = $obParam->getParamsByTag('logosbrandpartner');
    $linkbrandpartner = $obParam->getParamsByTag('linkbrandpartner');
    $logowebsiteComercial = $obParam->getParamsByTag('logowebsiteComercial');
    $linkwebsiteComercial = $obParam->getParamsByTag('linkwebsiteComercial');
    $logoEntradaAPP = $obParam->getParamsByTag('logoEntradaAPP');
    $tabuladoresComercial = $obParam->getParamsByTag('tabuladoresComercial');
    $imgSliderAPP = $obParam->getParamsByTag('imgSliderAPP');
    $creditoImgSliderAPP = $obParam->getParamsByTag('creditoImgSliderAPP');

    if($appComercial == 1) {
        $explodeBrandPartnerLogo = explode(';;', $logosbrandpartner);
        $explodeBrandPartnerLink = explode(';;', $linkbrandpartner);
        ?>
        <link rel="stylesheet" href="templates/virtualdesk/<?php echo $cssFile;?>">

        <div class="barraSuperior">
            <div class="content">

                <?php
                    for($i=0; $i<count($explodeBrandPartnerLogo); $i++){
                        $indice = $i + 1;
                        ?>
                            <a href="<?php echo $explodeBrandPartnerLink[$i];?>" target="_blank">
                                <div class="botao brandpartner<?php echo $indice;?>">
                                    <img src="<?php echo $baseurl . 'images/v_agenda/brandpartner/' . $explodeBrandPartnerLogo[$i];?>" alt="brandpartner<?php echo $indice;?>"/>
                                </div>
                            </a>
                        <?php
                    }
                ?>

                <a href="<?php echo $linkwebsiteComercial;?>" class="logoWebsite">
                    <img src="<?php echo $logowebsiteComercial;?>" alt="website"/>
                </a>

            </div>
        </div>

        <div class="sliderMotion">
            <ul>
                <?php

                    if(!empty($imgSliderAPP)){
                        $explodeImgSlider = explode(';;', $imgSliderAPP);
                        $explodeCreditoImgSlider = explode(';;', $creditoImgSliderAPP);
                        $credito = 0;
                        for($i=0; $i<count($explodeImgSlider); $i++){
                            ?>
                            <li style="background-image:URL('<?php echo $baseurl . 'images/v_agenda/sliderAPP/' . $explodeImgSlider[$i]; ?>')">
                                <div class="creditos">
                                    <div class="credito credito<?php echo $i;?>" style="display:none;">
                                        <p class="namePhoto"><?php echo '"' . $explodeCreditoImgSlider[$credito] . '"';?></p>
                                        <p class="nameAuthor"><?php echo $explodeCreditoImgSlider[$credito + 1];?></p>
                                        <?php $credito = $credito + 2;?>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                    } else {
                        echo 'get by plugin';
                    }

                ?>

            </ul>
            <div class="sliderActions">
                <div class="sliderContent">
                    <div class="btn credits"><img src="<?php echo $baseurl;?>images/v_agenda/icons/i.png" alt="creditsIcon"/></div>
                    <a href="<?php echo $baseurl.'uploadfoto/?lang='.$language_tag; ?>"><div class="btn upload"><img src="<?php echo $baseurl;?>images/v_agenda/icons/Upload.png" alt="uploadIcon"/></div></a>
                </div>
            </div>
        </div>

        <div class="workSpace">
            <a href="<?php echo $baseurl;?>" class="logo">
                <img src="<?php echo $baseurl . $logoEntradaAPP;?>" alt="logo_plataforma"/>
            </a>

            <div class="tabs">
                <?php
                    $explodeTabs = explode(';;', $tabuladoresComercial);

                    for($i=0; $i<count($explodeTabs);$i++){
                        $index = $i + 1;

                        if($i == 0){
                            ?>
                            <div class="tab active" id="tab<?php echo $index;?>" >
                            <?php
                        } else {
                            ?>
                            <div class="tab" id="tab<?php echo $index;?>">
                            <?php
                        }
                            echo $explodeTabs[$i];?>
                        </div>
                        <?php
                    }
                ?>
            </div>

            <div class="tabContent1 active">
                <?php
                    $setlogincomercial = 'tpl_offline_layout_login_comercial.php';
                    require_once( dirname(__FILE__) . DIRECTORY_SEPARATOR. $setlogincomercial );
                ?>
            </div>

            <script>
                <?php
                    require_once (JPATH_SITE . '/templates/virtualdesk/comercial.js.php');
                ?>
            </script>

        </div>

        <?php


    } else {
        // Logo file or site title param
        if (empty($logoFile))
        {	$logoFile = $baseurl . '/templates/' . $templateName . '/images/logo/logo_virtualdesk_100.png'; }
        else
        { $logoFile = $baseurl . $logoFile;}

        if((string)$setloginlayout==''){
            $setloginlayout = 'tpl_offline_layout_center.php';
        }
        else {
            $setloginlayout .= '.php';
        }
        require_once( dirname(__FILE__) . DIRECTORY_SEPARATOR. $setloginlayout );
    }

