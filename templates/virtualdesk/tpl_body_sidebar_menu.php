<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common footer for template: javascript...
 */
defined('_JEXEC') or die;

$IsActiveHorizontalMenu = false;
if ( !empty($sitemenulayout) && (string)$sitemenulayout=='horizontal_menu' ) $IsActiveHorizontalMenu = true;
?>
<!-- if is active de horizontal menu then we should hide de side bar menu, it will only -->
<?php if ($IsActiveHorizontalMenu===true) : ?>
<style>
   @media (min-width: 992px) {
       .page-sidebar.collapse {
           max-height: none !important;
       }

       .page-full-width .page-sidebar {
           display: none !important;
       }
   }

   .page-sidebar.navbar-collapse { padding: 0;  box-shadow: none;}
   .page-content-wrapper .page-content {margin-left: 0px !important;}

   .page-header.navbar .hor-menu .navbar-nav>li>a>span,.page-header.navbar .hor-menu .navbar-nav>li .dropdown-menu li>a>span {margin-left: 5px;}


</style>
<?php endif; ?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse" style="">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- style padding-top: 25px !important; -->
        <ul class="page-sidebar-menu <?php if ($IsActiveHorizontalMenu===true) echo "visible-sm visible-xs"; ?>  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="">

            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <?php if ($IsActiveHorizontalMenu===false) : ?>
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <?php endif; ?>
            <!-- END SIDEBAR TOGGLER BUTTON -->

            <!-- MODULE MENU-->
            <?php if ($this->countModules('sidemenu-left')) : ?>
                <jdoc:include type="modules" name="sidemenu-left" style="none" />
            <?php endif; ?>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->