<?php
defined('_JEXEC') or die;
// Conjunto de funções JS para serem aplicadas se o utilizador atual tiver acesso a algum grupo de Manager ou Admin
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

// Check Permissões
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('notificacao','list4managers'); // Verifica permissões READALL no layout
$vbInGroupAM   = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if( $vbHasReadAll2 == false || $vbInGroupAM ==false)
{
    return '';
    exit();
}

// Por SEGURANÇA os links de acesso aos dados para o manager ficaram definidos definidos neste ficheiros e não nos elementos como nos módulos
// isto porque este é um de ficheiro de acesso para  managers

?>
var vdAjaxCall_Notificacao_Main4Manager = function () {

    // Carrega Todas as Notificações Novas / Não lidas
    var getNotificacaoAllNumAbertasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=notificacao.getNotificacoesListaAbertas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);

                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimasNotificacoesScroll');
                            var tId;
                            var htmlp1 = '<li><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4manager');?>"><span class="time">';
                            var htmlp2 = '</span><span class="details"><span class=""><i class="fa fa-comment-o"></i></span> ';
                            var htmlp3 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4manager');?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {   var dateAct = response['dt'][tId].created;
                                var nomeAct = response['dt'][tId].nome;
                                elAppend.append(htmlp1 + dateAct + htmlp2 + nomeAct + htmlp3);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    // Carrega quantas Notificações estão ABERTAS (por ler /novas) e coloca um icon com esse
    var getNotificacaoMinhasNumAbertasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=notificacao.getNotificacoesMinhasListaAbertas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);

                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimasNotificacoesScroll');
                            var tId;
                            var htmlp1 = '<li><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4manager#tab_ListaNotificacaoMinhas');?>"><span class="time">';
                            var htmlp2 = '</span><span class="details"><span class=""><i class="fa fa-comment-o"></i></span> ';
                            var htmlp3 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4manager#tab_ListaNotificacaoMinhas');?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {   var dateAct = response['dt'][tId].created;
                                var nomeAct = response['dt'][tId].nome;
                                elAppend.append(htmlp1 + dateAct + htmlp2 + nomeAct + htmlp3);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    // Carrega quantas Notificações estão ABERTAS (por ler /novas) e coloca um icon com esse
    var getNotificacaoMeusGruposNumAbertasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=notificacao.getNotificacoesMeusGruposListaAbertas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);

                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimasNotificacoesScroll');
                            var tId;
                            var htmlp1 = '<li><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk'.$paramMainMenuId.'&view=notificacao&layout=list4manager#tab_ListaNotificacaoMeusGrupos');?>"><span class="time">';
                            var htmlp2 = '</span><span class="details"><span class=""><i class="fa fa-comment-o"></i></span> ';
                            var htmlp3 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo JRoute::_('index.php?option=com_virtualdesk'.$paramMainMenuId.'&view=notificacao&layout=list4manager#tab_ListaNotificacaoMeusGrupos');?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {   var dateAct = response['dt'][tId].created;
                                var nomeAct = response['dt'][tId].nome;
                                elAppend.append(htmlp1 + dateAct + htmlp2 + nomeAct + htmlp3);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    return {
        getNotificacaoAllNumAbertasData : getNotificacaoAllNumAbertasData,
        getNotificacaoMinhasNumAbertasData : getNotificacaoMinhasNumAbertasData,
        getNotificacaoMeusGruposNumAbertasData : getNotificacaoMeusGruposNumAbertasData
    };

}();

var AlarmsNotificacao_HandleMain4Manager = function () {

    var getNotificacaoAllNumAbertasData = function (evt) {
        let el = jQuery('#vdMainTabNavNotificacaoAllNumAbertas4Manager');
        vdAjaxCall_Notificacao_Main4Manager.getNotificacaoAllNumAbertasData(el);
    };

    var getNotificacaoMinhasNumAbertasData = function (evt) {
        let el = jQuery('#vdMainTabNavNotificacaoMinhasNumAbertas4Manager');
        vdAjaxCall_Notificacao_Main4Manager.getNotificacaoMinhasNumAbertasData(el);
    };

    var getNotificacaoMeusGruposNumAbertasData = function (evt) {
        let el = jQuery('#vdMainTabNavNotificacaoMeusGruposNumAbertas4Manager');
        vdAjaxCall_Notificacao_Main4Manager.getNotificacaoMeusGruposNumAbertasData(el);
    };

    return {
        //main function to initiate the module
        init: function () {
            getNotificacaoAllNumAbertasData();
            getNotificacaoMinhasNumAbertasData();
            getNotificacaoMeusGruposNumAbertasData();
        },
        reload: function () {
            getNotificacaoAllNumAbertasData();
            getNotificacaoMinhasNumAbertasData();
            getNotificacaoMeusGruposNumAbertasData();
        },
        getNotificacaoAllNumAbertasData: getNotificacaoAllNumAbertasData,
        getNotificacaoMinhasNumAbertasData: getNotificacaoMinhasNumAbertasData,
        getNotificacaoMeusGruposNumAbertasData: getNotificacaoMeusGruposNumAbertasData
    };
}();

jQuery(document).ready(function() {
    AlarmsNotificacao_HandleMain4Manager.init();
});