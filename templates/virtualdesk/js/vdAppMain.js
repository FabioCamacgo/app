jQuery(document).ready(function() {

    // handle sidebar show/hide
    jQuery('body').on('click', '.sidebar-toggler', function (e) {

        var toggleDivIcon = jQuery('.vd-sidebar-toggler-icon');
        var toggleIcon =toggleDivIcon.find('i');

        if (toggleIcon.hasClass("icon-arrow-left")) {
            toggleIcon.removeClass("icon-arrow-left").addClass("icon-arrow-right");
            toggleDivIcon.addClass("vd-sidebar-toggler-icon-closed");
        } else {
            toggleIcon.removeClass("icon-arrow-right").addClass("icon-arrow-left");
            toggleDivIcon.removeClass("vd-sidebar-toggler-icon-closed");
        }
    });

});