<?php
defined('_JEXEC') or die;
// Conjunto de funções JS para serem aplicadas se o utilizador atual tiver acesso a algum grupo de Manager ou Admin
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

// Check Permissões
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','view4managers'); // Verifica permissões READALL no layout
$vbInGroupAM   = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if( $vbHasReadAll2 == false || $vbInGroupAM ==false)
{
    return '';
    exit();
}

$obParam          = new VirtualDeskSiteParamsHelper();
$avisosMaxRecords = (int)$obParam->getParamsByTag('avisosBarraSuperiorGeralMaxRecords');

// Por SEGURANÇA os links de acesso aos dados para o manager ficaram definidos definidos neste ficheiros e não nos elementos como nos módulos
// isto porque este é um de ficheiro de acesso para users e managers

?>
var vdAjaxCall_AlarmsTarefas_Main4Manager = function () {


    // Carrega quantas tarefas estão ABERTAS (não concluída e não anuladas) e coloca um icon com esse
    var getTarefaAllNumAbertasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefasListaAbertas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);

                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimasTarefasScroll');
                            var tId;
                            var htmlp1 = '<li><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager');?>"><span class="time">';
                            var htmlp2 = '</span><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-bell-o"></i></span>';
                            var htmlp3 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager');?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {   var dateAct = response['dt'][tId].created;
                                var nomeAct = response['dt'][tId].nome;
                                elAppend.append(htmlp1 + dateAct + htmlp2 + nomeAct + htmlp3);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    // Carrega quantas tarefas estão ABERTAS (não concluída e não anuladas) e coloca um icon com esse
    var getTarefaMinhasNumAbertasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefasMinhasListaAbertas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);

                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimasTarefasScroll');
                            var tId;
                            var htmlp1 = '<li><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager#tab_ListaTarefaMinhas');?>"><span class="time">';
                            var htmlp2 = '</span><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-bell-o"></i></span>';
                            var htmlp3 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager#tab_ListaTarefaMinhas');?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {   var dateAct = response['dt'][tId].created;
                                var nomeAct = response['dt'][tId].nome;
                                elAppend.append(htmlp1 + dateAct + htmlp2 + nomeAct + htmlp3);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();

                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    // Carrega quantas tarefas estão ABERTAS (não concluída e não anuladas) e coloca um icon com esse
    var getTarefaMeusGruposNumAbertasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefasMeusGruposListaAbertas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);

                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimasTarefasScroll');
                            var tId;
                            var htmlp1 = '<li><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager#tab_ListaTarefaMeusGrupos');?>"><span class="time">';
                            var htmlp2 = '</span><span class="details"><span class="label label-sm label-icon label-danger"><i class="fa fa-bell-o"></i></span>';
                            var htmlp3 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager#tab_ListaTarefaMeusGrupos');?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {   var dateAct = response['dt'][tId].created;
                                var nomeAct = response['dt'][tId].nome;
                                elAppend.append(htmlp1 + dateAct + htmlp2 + nomeAct + htmlp3);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    // Carrega quantas ocorrências estão em ESPERA/NOVAS e coloca um icon com esse
    var getAlertaNumNovasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.getAlertaListaNovas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);
                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimosAlertasScroll');
                            var tId;
                            var htmlp1 = '<li><a href="';
                            var htmlp2 = '"><span class="time">';
                            var htmlp3 = '</span><span class="details"><i class="fa fa-map-marker"></i>';
                            var htmlp4 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo $PathParaAlertas;?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {  var dateAct = response['dt'][tId].data_criacao;
                                var nomeAct = response['dt'][tId].codigo;
                                var hRef    = response['dt'][tId].dummy;
                                elAppend.append(htmlp1 + hRef + htmlp2 + dateAct + htmlp3 + nomeAct + htmlp4);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    // Carrega quantas ocorrências Encaminhadas para o utilizador ou para os seus grupos estão em ESPERA/NOVAS e coloca um icon com esse
    var getAlertaNumNovasEncaminhadasData = function (el) {
        let urlGetContent ="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.getAlertaListaNovasEncaminhadas4ManagerByAjax'); ?>";
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response['dt'].length>0) {
                            el.find('.vdValorNot').html(response['nr']);
                            el.parent('.vdBarraNotificacoes').find('.vdValorNotDropDown').html(response['nr']);
                            var elAppend = el.parent('.vdBarraNotificacoes').find('.vdUltimosAlertasScroll');
                            var tId;
                            var htmlp1 = '<li><a href="';
                            var htmlp2 = '"><span class="time">';
                            var htmlp3 = '</span><span class="details"><i class="fa fa-map-marker"></i>';
                            var htmlp4 = '</span></a></li>';
                            var moreInfo = '<li style="text-align:center"><a style="color:#337ab7" href="<?php echo $PathParaAlertasEnc;?>"><?php echo JText::_('COM_VIRTUALDESK_VERTUDO'); ?>...</a></li>';

                            for (tId in response['dt'])
                            {
                                var dateAct = response['dt'][tId].data_criacao;
                                var nomeAct = response['dt'][tId].codigo;
                                var hRef    = response['dt'][tId].dummy;
                                elAppend.append(htmlp1 + hRef + htmlp2 + dateAct + htmlp3 + nomeAct + htmlp4);
                            }
                            if(parseInt(response['nr'])>=<?php echo $avisosMaxRecords;?>) elAppend.append(moreInfo);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };



    return {
        getTarefaAllNumAbertasData  : getTarefaAllNumAbertasData,
        getTarefaMinhasNumAbertasData  : getTarefaMinhasNumAbertasData,
        getTarefaMeusGruposNumAbertasData  : getTarefaMeusGruposNumAbertasData,
        getAlertaNumNovasData : getAlertaNumNovasData,
        getAlertaNumNovasEncaminhadasData : getAlertaNumNovasEncaminhadasData
    };

}();

var AlarmsTarefas_HandleMain4Manager = function () {

    var getTarefaAllNumAbertasData = function (evt) {
        let el = jQuery('#vdMainTabNavTarefasAllNumAbertas4Manager');
        vdAjaxCall_AlarmsTarefas_Main4Manager.getTarefaAllNumAbertasData(el);
    };

    var getTarefaMinhasNumAbertasData = function (evt) {
        let el = jQuery('#vdMainTabNavTarefaMinhasNumAbertas4Manager');
        vdAjaxCall_AlarmsTarefas_Main4Manager.getTarefaMinhasNumAbertasData(el);
    };

    var getTarefaMeusGruposNumAbertasData = function (evt) {
        let el = jQuery('#vdMainTabNavTarefaMeusGruposNumAbertas4Manager');
        vdAjaxCall_AlarmsTarefas_Main4Manager.getTarefaMeusGruposNumAbertasData(el);
    };

    var getAlertaNovasData = function (evt) {
        let el = jQuery('#vdMainTabNavAlertaNovas4Manager');
        vdAjaxCall_AlarmsTarefas_Main4Manager.getAlertaNumNovasData(el);
    };

    var getAlertaNovasEncaminhadasData = function (evt) {
        let el = jQuery('#vdMainTabNavAlertaNovasEncaminhadas4Manager');
        vdAjaxCall_AlarmsTarefas_Main4Manager.getAlertaNumNovasEncaminhadasData(el);
    };

    return {
        //main function to initiate the module
        init: function () {
            getTarefaAllNumAbertasData();
            getTarefaMinhasNumAbertasData();
            getTarefaMeusGruposNumAbertasData();
            getAlertaNovasData();
            getAlertaNovasEncaminhadasData();
        },
        getTarefaAbertasReload: function () {
            getTarefaAllNumAbertasData();
            getTarefaMinhasNumAbertasData();
            getTarefaMeusGruposNumAbertasData();
        },
        getTarefaAllNumAbertasData: getTarefaAllNumAbertasData,
        getTarefaMinhasNumAbertasData: getTarefaMinhasNumAbertasData,
        getTarefaMeusGruposNumAbertasData: getTarefaMeusGruposNumAbertasData,
        getAlertaNovasData : getAlertaNovasData,
        getAlertaNovasEncaminhadasData : getAlertaNovasEncaminhadasData
    };
}();

jQuery(document).ready(function() {
    AlarmsTarefas_HandleMain4Manager.init();
});