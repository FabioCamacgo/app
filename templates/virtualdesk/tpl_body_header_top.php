<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common footer for template: javascript...
 */
defined('_JEXEC') or die;


JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$config          = JFactory::getConfig();
$sitename        = $config->get('sitename');
$sitedescription = $config->get('sitedescription');
$sitetitle       = $config->get('sitetitle');
$baseurl         = JUri::base();
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$logoFile        = $templateParams->get('logoFile');
$templateName    = 'virtualdesk';
$sitemenulayout  = $templateParams->get('sitemenulayout');
$doc             = JFactory::getDocument();
$doc->setHtml5(true);


$obParam      = new VirtualDeskSiteParamsHelper();

$Logo = $obParam->getParamsByTag('logoInteriorAPP');
$cssFileGeral = $obParam->getParamsByTag('cssFileGeral');


$userInSession   = JFactory::getUser();


$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/' . $cssFileGeral);
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/custom.js');


$IsActiveHorizontalMenu = false;
if ( !empty($sitemenulayout) && (string)$sitemenulayout=='horizontal_menu' ) $IsActiveHorizontalMenu = true;



// Conjunto de funções JS para serem aplicadas se o utilizador atual tiver acesso a algum grupo de Manager ou Admin
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


// Check - Verifica se o User é MANAGER ou ADMIN - Vai servir para saber se deve ir para o Tickets de Manager ou para o User
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('tickets', 'addnew4managers'); // verifica permissão acesso ao layout para editar
$vbInGroupAM   = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP

$TicketsMenuId4Manager = null;
$PathParaTickets       = '';
if($vbInGroupAM===true && $vbHasAccess2===true) {
  $TicketsMenuId4Manager = $obParam->getParamsByTag('Tickets_Menu_Id_By_Default_4Managers');
  $PathParaTickets = JRoute::_('index.php?option=com_virtualdesk&view=tickets&layout=addnew4manager&Itemid='.$TicketsMenuId4Manager);
}
else{
    $TicketsMenuId4User = $obParam->getParamsByTag('Tickets_Menu_Id_By_Default_4Users');
  $PathParaTickets = JRoute::_('index.php?option=com_virtualdesk&view=tickets&layout=addnew4user&Itemid='.$TicketsMenuId4User);
}

// Só coloquei verificação por manager por uma questão de desempenho. Além no JS das tarefasm são carregadas cos alertas e não faz verificação
$PathParaAlertas    = '';
$PathParaAlertasEnc = '';
if($vbInGroupAM ===true ) {
    $AlertaMenuId4Manager = $obParam->getParamsByTag('Alerta_Menu_Id_By_Default_4Managers');
    $PathParaAlertas    = JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=list4manager&Itemid='.$AlertaMenuId4Manager);
    $PathParaAlertasEnc = JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=list4manager#tab_ListaAlertasEncaminhadas&Itemid='.$AlertaMenuId4Manager);
}


// Main Menu ID para dser incluido nos links da barra superior, caso contrário dá erro quando estiver no menu diferente do main
$paramMainMenuId = $obParam->getParamsByTag('Main_Menu_Id_PT');

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
switch($language_tag)
{   case 'en-GB':
    // Main Menu ID para dser incluido nos links da barra superior, caso contrário dá erro quando estiver no menu diferente do main
    $paramMainMenuId = $obParam->getParamsByTag('Main_Menu_Id_EN');
    break;
    default:
        break;
}


?>
<style>
    .vdBarraNotificacoes > a {text-align: center; font-size:24px; margin-top: 5px;}
</style>

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">

        <!-- BEGIN LOGO -->
        <div class="page-logo">

            <a href="<?php echo $baseurl; ?>/">
                <img src="<?php echo $baseurl;?><?php echo $Logo ?>"  alt="logo" class="logo-default"/>
            </a>


            <!-- Menu "hamburger" -->
            <?php if ($IsActiveHorizontalMenu===false) : ?>
            <div class="menu-toggler sidebar-toggler">
<!--                <jdoc:include type="modules" name="position-0" style="none" />-->
                <div class="vd-sidebar-toggler-icon">
                <i class="icon-arrow-left"></i>
                </div>
            <!--<span></span>-->
            </div>
            <?php endif; ?>

        </div>
        <!-- END LOGO -->



        <?php
        /*load body -> horizontal + menu*/
        if ( !empty($sitemenulayout) && (string)$sitemenulayout=='horizontal_menu' ) require_once(dirname(__FILE__) . '/tpl_body_horizontal_menu.php');
        ?>

        <!-- TOP HEADER POSITION-->
        <?php if ($this->countModules('header-top')) : ?>
            <jdoc:include type="modules" name="header-top" style="none" />
        <?php endif; ?>


        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->


        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">


                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavNotificacaoAllNumAbertas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_TODASASNOTIFICACOES'); ?>" >
                        <i class="icon-speech " ></i>
                        <span class="badge badge-info vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_TODASASNOTIFICACOES'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_NAOCONCLUIDAS'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4manager');?>"><?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_VERNOTIFICACOES'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimasNotificacoesScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavNotificacaoMinhasNumAbertas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_ASMINHASNOTIFICACOES'); ?>" >
                        <i class="icon-speech " ></i>
                        <span class="badge badge-info vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_ASMINHASNOTIFICACOES'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_NAOCONCLUIDAS'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4manager#tab_ListaNotificacaoMinhas');?>"><?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_VERNOTIFICACOES'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimasNotificacoesScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavNotificacaoMeusGruposNumAbertas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_NOTIFICACOESDOSMEUSGRUPOS'); ?>" >
                        <i class="icon-speech " ></i>
                        <span class="badge badge-info vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_NOTIFICACOESDOSMEUSGRUPOS'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_NAOCONCLUIDAS'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4manager#tab_ListaNotificacaoMeusGrupos');?>"><?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_VERNOTIFICACOES'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimasNotificacoesScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavAlertaNovas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA'); ?>" >
                        <i class="icon-pointer " ></i>
                        <span class="badge badge-danger vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_TITLE_ESPERA'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_($PathParaAlertas);?>"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_VEROCORRENCIAS'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimosAlertasScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavAlertaNovasEncaminhadas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTAENCAMINHADASMIM'); ?>" >
                        <i class="icon-pointer " ></i>
                        <span class="badge badge-danger vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTAENCAMINHADASMIM'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_TITLE_ESPERA'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_($PathParaAlertasEnc);?>"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_VEROCORRENCIAS'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimosAlertasScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavTarefasAllNumAbertas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_TAREFA_TODASASTAREFAS'); ?>" >
                        <i class="icon-bell " ></i>
                        <span class="badge badge-danger vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_TAREFA_TODASASTAREFAS'); ?><br/>
                                 <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_TAREFA_NAOCONCLUIDAS'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager');?>"><?php echo JText::_('COM_VIRTUALDESK_TAREFA_VERTAREFAS'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimasTarefasScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavTarefaMinhasNumAbertas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_TAREFA_ASMINHASTAREFAS'); ?>" >
                        <i class="icon-user " ></i>
                        <span class="badge badge-danger vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_TAREFA_ASMINHASTAREFAS'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_TAREFA_NAOCONCLUIDAS'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager#tab_ListaTarefaMinhas');?>"><?php echo JText::_('COM_VIRTUALDESK_TAREFA_VERTAREFAS'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimasTarefasScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavTarefaMeusGruposNumAbertas4Manager" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_TAREFA_TAREFASDOSMEUSGRUPOS'); ?>" >
                        <i class="icon-users " ></i>
                        <span class="badge badge-danger vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_TAREFA_TAREFASDOSMEUSGRUPOS'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_TAREFA_NAOCONCLUIDAS'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=tarefa&layout=list4manager#tab_ListaTarefaMeusGrupos');?>"><?php echo JText::_('COM_VIRTUALDESK_TAREFA_VERTAREFAS'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimasTarefasScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <li class="dropdown dropdown-extended dropdown-notification vdBarraNotificacoes" >
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"
                       id="vdMainTabNavNotificacaoMinhasNumAbertas4User" title="<?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_ASMINHASNOTIFICACOES'); ?>" >
                        <i class="icon-speech " ></i>
                        <span class="badge badge-info vdValorNot"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>
                                <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_ASMINHASNOTIFICACOES'); ?><br/>
                                <span class="bold vdValorNotDropDown"></span> <?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_NAOCONCLUIDAS'); ?>
                            </h3>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&Itemid='.$paramMainMenuId.'&view=notificacao&layout=list4user');?>"><?php echo JText::_('COM_VIRTUALDESK_NOTIFICACAO_VERNOTIFICACOES'); ?></a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller vdUltimasNotificacoesScroll" style="height: 250px;" data-handle-color="#637283">

                            </ul>
                        </li>
                    </ul>

                </li>
                <!-- END NOTIFICATION DROPDOWN -->

                <!-- BEGIN Módulo Tickets -->
                <li class="dropdown dropdown-tickets">
                    <a class="dropdown" href="<?php echo $PathParaTickets ?>">
                        <i class="fa fa-envelope-o"></i>
                    </a>

                </li>
                <!-- END Módulo Tickets -->


                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <!--<img alt="" class="img-circle" src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/assets/layouts/layout/img/avatar_vd3.jpg" />-->
                        <svg enable-background="new 0 0 32 32" height="32px" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g>
                                <g>
                                    <g>
                                        <g>
                                            <path d="M16,31C7.729,31,1,24.271,1,16S7.729,1,16,1s15,6.729,15,15S24.271,31,16,31z M16,2C8.28,2,2,8.28,2,16      s6.28,14,14,14s14-6.28,14-14S23.72,2,16,2z" fill="#263238"/>
                                        </g>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <g>
                                            <g>
                                                <g>
                                                    <g>
                                                        <path d="M23.64,20.713l-4.762-1.652l-0.323-2.584c-0.215,0.307-0.523,0.546-0.924,0.671l0.293,2.345c0.023,0.189,0.152,0.349,0.332,0.41l5.055,1.756c0.9,0.314,1.689,1.427,1.689,2.381v-0.007c0,0.276,0.224,0.5,0.5,0.5         c0.275,0,0.499-0.223,0.5-0.498C25.997,22.656,24.94,21.168,23.64,20.713z" />
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <g>
                                                    <g>
                                                        <path d="M6.5,24.532c-0.276,0-0.5-0.224-0.5-0.5v0.007c0-1.379,1.059-2.871,2.359-3.326l4.762-1.641l0.012-0.28c0.034-0.274,0.289-0.465,0.559-0.434c0.273,0.034,0.468,0.284,0.434,0.559l-0.051,0.589         c-0.023,0.189-0.153,0.348-0.333,0.41l-5.054,1.742C7.789,21.973,7,23.086,7,24.039v-0.007C7,24.309,6.776,24.532,6.5,24.532z" />
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <g>
                                                    <g>
                                                        <g>
                                                            <path d="M16,18.039c-2.779,0-4.192-1.844-4.201-6.469c-0.002-1.174,0.123-2.363,1.227-3.469C13.729,7.396,14.729,7.039,16,7.039s2.271,0.357,2.975,1.063c1.104,1.105,1.229,2.295,1.227,3.469          C20.192,16.195,18.779,18.039,16,18.039z M16,8.039c-1.009,0-1.75,0.252-2.267,0.769c-0.632,0.633-0.938,1.2-0.935,2.761c0.008,4.018,1.055,5.471,3.201,5.471s3.193-1.453,3.201-5.471c0.003-1.561-0.303-2.128-0.935-2.761C17.75,8.291,17.009,8.039,16,8.039z"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>

                        <span class="username username-hide-on-mobile"> <?php if (!empty($userInSession)) echo $userInSession->name; ?></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <?php if ($this->countModules('position-7')) : ?>
                                <jdoc:include type="modules" name="position-7" style="well" />
                            <?php endif; ?>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->


            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->

<?php
echo ('<script>');
require_once (JPATH_SITE . '/templates/virtualdesk/js/appMainTarefa4Manager.js.php');
require_once (JPATH_SITE . '/templates/virtualdesk/js/appMainNotificacao4Manager.js.php');
require_once (JPATH_SITE . '/templates/virtualdesk/js/appMainNotificacao4User.js.php');
echo ('</script>');
?>