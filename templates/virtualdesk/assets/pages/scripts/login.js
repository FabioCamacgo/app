var Login = function() {

    var handleLogin = function() {

        jQuery('#form-login').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                username: {
                    vdnifcheck: ['', 'username'],
                    required: true
                },
                password: {
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                jQuery("#MainMessageAlertBlockSimple").show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                jQuery('#cover-spin').show(0);
                form.submit(); // form validation success, call ajax form submit
            }
        });

        jQuery('#form-login').find('input').keypress(function(e) {
            if (e.which == 13) {
                if ( jQuery('#form-login').validate().form()) {
                    jQuery('#cover-spin').show(0);
                    jQuery('#form-login').submit();
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleLogin();
        }

    };

}();

/*
 * Inicialização da validação
 */
jQuery(document).ready(function() {

    jQuery.validator.addMethod("vdnifcheck", function(value, element, params) {
        // Verifica se o username é NIF
        if(setUserFieldLoginType_JS<1) return true;

        var NIFElem = null;
        if(params[1]!='') NIFElem =  jQuery('#'+params[1]);
        var resNIFCheckJS = jQuery().virtualDeskNIFCheck(NIFElem.val(),virtualDeskNIFCheckOptions);
        params[0] = resNIFCheckJS.message;
        return resNIFCheckJS.return ;
    }, jQuery.validator.format(" {0} "));

    Login.init();
});