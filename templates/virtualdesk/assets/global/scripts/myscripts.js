var n1=1;
var n2=2;
var n3=3;
var n4=4;
var n5=5;
var emespera=1;
var analise=0;
var concluido=0;
var c1="#0b306b";
var c2="#fff";
var c3="#000";
var c4="#e1e1e1";


function smoth(posicao){
    if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        jQuery('html, body').animate({
            scrollTop: jQuery(posicao).offset().top - 100
        }, 1500, function(){
            window.location.hash = '';
        });
    }
}

function slideContent(n1, n2, n3, n4, n5){
    if(n1 == 6){
        n1=1;
    }
    if(n2 == 6){
        n2=1;
    }
    if(n3 == 6){
        n3=1;
    }
    if(n4 == 6){
        n4=1;
    }
    if(n5 == 6){
        n5=1;
    }

    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n5).animate({ left: '-150%'}, 'slow');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n5).css('z-index','-1');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n1).animate({ left: '0%'}, 'slow');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n1).css('z-index','9');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n2).css('left','105%');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n2).css('z-index','0');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n3).css('left','105%');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n3).css('z-index','0');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n4).css('left','105%');
    jQuery('.ultimosalertas .ocorrencia .ocorre.n' + n4).css('z-index','0');

    setTimeout(function(){
        n1=n1+1;
        n2=n2+1;
        n3=n3+1;
        n4=n4+1;
        n5=n5+1;
        slideContent(n1, n2, n3, n4, n5);
    }, 7000);
}

function tabocorrencias(tab1, tab2, tab3){
    jQuery('.tabulador #button_' + tab1).css('border-color',c1);
    jQuery('.tabulador #button_' + tab1).css('background',c1);
    jQuery('.tabulador #button_' + tab1).css('color',c2);
    jQuery('#' + tab1).css('display','block');
    jQuery('.tabulador #button_' + tab2).css('border-color',c4);
    jQuery('.tabulador #button_' + tab2).css('background',c2);
    jQuery('.tabulador #button_' + tab2).css('color',c3);
    jQuery('#' + tab2).css('display','none');
    jQuery('.tabulador #button_' + tab3).css('border-color',c4);
    jQuery('.tabulador #button_' + tab3).css('background',c2);
    jQuery('.tabulador #button_' + tab3).css('color',c3);
    jQuery('#' + tab3).css('display','none');
}



jQuery( document ).ready(function() {

    jQuery("#button_espera").click(function() {
        if(emespera == 0){
            tabocorrencias('espera', 'analise', 'concluido');
            emespera=1;
            analise=0;
            concluido= 0;
        }
    });

    jQuery("#button_analise").click(function() {
        if(analise == 0){
            tabocorrencias('analise', 'espera', 'concluido');
            emespera=0;
            analise=1;
            concluido= 0;
        }
    });

    jQuery("#button_concluido").click(function() {
        if(concluido == 0){
            tabocorrencias('concluido', 'espera', 'analise');
            emespera=0;
            analise=0;
            concluido= 1;
        }
    });

    jQuery( "#portal" ).click(function() {
        smoth('#ancor');
        jQuery( "#PortalMunicipal" ).slideDown('slow');
    });

    jQuery( "#close" ).click(function() {
        jQuery( "#PortalMunicipal" ).slideUp('slow');
    });

    jQuery( "#menu" ).click(function() {
        jQuery('#closeMainMenu').css('display','inline-block');
        jQuery('#menu').css('display','none');
        jQuery( "#menuContent" ).slideDown('slow');
    });

    jQuery( "#closeMainMenu" ).click(function() {
        jQuery('#menu').css('display','inline-block');
        jQuery('#closeMainMenu').css('display','none');
        jQuery( "#menuContent" ).slideUp('slow');
    });

    slideContent(n1, n2, n3, n4, n5);
});

