<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common headers for template: head, css, javascript
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Output as HTML5
$doc->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

// Logo file or site title param
if ($params->get('logoFile'))
{	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($params->get('sitetitle'))
{	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<!--<![endif]-->
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <?php if ($this->error->getCode()) : ?>
        <title><?php echo $this->error->getCode() ?> - <?php echo $this->title; ?></title>
    <?php endif; ?>
        <link href="<?php echo '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'; ?>" rel="stylesheet">
        <link href="<?php echo $this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/font-awesome/css/font-awesome.min.css'; ?>" rel="stylesheet">
        <link href="<?php echo $this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'; ?>" rel="stylesheet">
        <link href="<?php echo $this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/css/bootstrap.min.css'; ?>" rel="stylesheet">
        <link href="<?php echo $this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'; ?>" rel="stylesheet">
        <link href="<?php echo $this->baseurl . '/templates/' . $this->template . '/assets/global/css/components-rounded.min.css'; ?>" rel="stylesheet">
        <link href="<?php echo $this->baseurl . '/templates/' . $this->template . '/assets/global/css/plugins.min.css'; ?>" rel="stylesheet">
        <link href="<?php echo $this->baseurl . '/templates/' . $this->template . '/assets/pages/css/login.min.css'; ?>" rel="stylesheet">
</head>
<body class="site login">

<!-- BEGIN LOGO -->
<div class="logo">
    <div class="row">
        <div class="col-md-4">
            <?php if ($logoFile) : ?>
                <a href="<?php echo $baseurl; ?>/">
                    <img src="<?php echo $logoFile ?>"  alt="logo" class="logo-default"/>
                </a>
            <?php endif; ?>
        </div>
        <div class="col-md-8" style="text-align: center; color:#fff; margin-top: 15px;">
            <?php if ($sitename) : ?>
                <?php echo '<h3>' . htmlspecialchars($sitename, ENT_COMPAT, 'UTF-8') . '</h3>'; ?>
            <?php endif; ?>
            <?php if ($sitedescription) : ?>
                <?php echo '<div class="site-description">' . htmlspecialchars($sitedescription, ENT_COMPAT, 'UTF-8') . '</div>'; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- END LOGO -->

<!-- BEGIN LOGIN -->
<div class="content">
    <?php if ($this->error->getCode()) : /* check if we are on error page, if yes - display error message */ ?>
    <div class="text-center">
        <h3 class="form-title font-green uppercase "><?php echo JText::_('JERROR_AN_ERROR_HAS_OCCURRED'); ?></h3>
        <h4 class="form-title font-green uppercase "><?php echo JText::_('JERROR_ERROR'); ?> <?php echo $this->error->getCode(); ?></h4>
    </div>
        <p class="hint"><?php echo $this->error->getMessage() ?></p>
        <p ><?php echo  JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?></p>

        <div class="text-center">
        <a class="btn green uppercase" href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a>
        </div>

        <p><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?>.</p>
    <?php endif; ?>

</div>

<div class="copyright"><?php echo $year = date("Y") . ' ' . $sitename; ?> ©</div>
</body>
</html>
