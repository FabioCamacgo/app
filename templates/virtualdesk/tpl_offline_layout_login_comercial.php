<?php
    /**
     * @package     VirtualDesk
     * @subpackage  Templates.VirtualDesk
     * load common headers for template: head, css, javascript
     */

    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $user            = JFactory::getUser();
    $this->language  = $doc->language;
    $this->direction = $doc->direction;


    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Output as HTML5
    $doc->setHtml5(true);

    // Getting params from template
    $params = $app->getTemplate(true)->params;

    // Detecting Active Variables
    $option   = $app->input->getCmd('option', '');
    $view     = $app->input->getCmd('view', '');
    $layout   = $app->input->getCmd('layout', '');
    $task     = $app->input->getCmd('task', '');
    $itemid   = $app->input->getCmd('Itemid', '');
    $sitename = $app->get('sitename');


    // Add JavaScript Frameworks
    JHtml::_('jquery.framework',false);
    JHtml::_('bootstrap.framework',false);

    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/chosen.jquery.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-migrate.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/system/js/caption.js']);

    if(array_key_exists('text/javascript', $doc->_script))
    {
        $doc->_script['text/javascript'] = preg_replace('%jQuery\(window\).on\(\'load\',\s*function\(\)\s*\{\s*new\s*JCaption\(\'img.caption\'\)\;\s*\}\)\;\s*%', '', $doc->_script['text/javascript']);
    }

    // CUSTOM JS - dashboard
    //BEGIN CORE PLUGINS
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/js/bootstrap.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/js.cookie.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.blockui.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');

    // BEGIN PAGE LEVEL PLUGINS
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js');
    // END PAGE LEVEL PLUGINS



    //BEGIN PAGE LEVEL SCRIPTS
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/scripts/login.js');
    // END PAGE LEVEL SCRIPTS
    $doc->addScriptVersion($this->baseurl . '/administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js');

    //BEGIN GLOBAL MANDATORY STYLES
    $doc->addStyleSheet('//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/css/bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/components-rounded.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/plugins.min.css');
    //END THEME GLOBAL STYLES

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

?>

<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"   style="overflow-x:hidden;">
    <!--<![endif]-->
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <jdoc:include type="head" />
    </head>



    <?php if($setUserFieldLoginTypeAllowMultiple == '1' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ){ ?>
    <body class="site login">
    <?php } else if($setUserFieldLoginTypeAllowMultiple == '0' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ){ ?>
    <body class="site login">
    <?php } else { ?>
    <body class="site login corporate">
    <?php } ?>



    <script >
        /*
         * Parametros e Mensagens para a verificação do NIF em javascript / jquery validation
         */
        var virtualDeskNIFCheckOptions = {
            message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'); ?>"
        };

        var setUserFieldLoginType_JS = <?php echo $setUserFieldLoginType_JS ?>;
    </script>

    <link href="<?php echo $baseurl;?>/templates/virtualdesk/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />

    <div class="contentLogin">

        <!-- BEGIN LOGIN -->
        <form class="login-form" action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="form-login">
            <h3 class="form-title"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_TITLE'); ?></h3>
            <p class="hint" style="color: #000;"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_DESC'); ?></p>

            <!-- system messages -->
            <jdoc:include type="message" />

            <!-- alerts -->
            <div id="MainMessageAlertBlockSimple" class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span><?php echo $CampoForm_LoginFormAlert; ?>  </span>
            </div>

            <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
            </div>

            <div class="form-group first">
                <input class="form-control form-control-solid placeholder-no-fix" type="text" required autocomplete="off" placeholder="<?php echo $CampoForm_Username_Label; ?>" name="username" id="username" maxlength="<?php echo $CampoForm_Username_MaxLength; ?>" />
            </div>

            <div class="form-group">
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" name="password" " />
            </div>

            <?php if($configSetForgotPassword == '1' && $getAllowTextLogin!='allow') : ?>
                <p class="forgotPassword"><a href="<?php echo $this->baseurl.'/recuperacaosenha/?lang='.$language_tag; ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_FORGOTPASSWORD'); ?></a></p>
            <?php endif; ?>

            <?php if (count($twofactormethods) > 1) : ?>
                <div class="form-group">
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" name="secretkey" id="secretkey" title="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" />
                </div>
            <?php endif; ?>

            <div class="form-actions">

                <input type="submit" name="<?php echo JText::_('JLOGIN'); ?>" value ="<?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_LOGIN'); ?>" class="btn"/>

            </div>


            <?php if($configSetNewRegistration == '1' && $getAllowTextLogin!='allow' ) : ?>
                <div class="login-options">
                    <!-- BEGIN LINK  NEW USER REGISTRATION -->
                    <p><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_NEWREGISTRATION_1') . ' '; ?><a href="<?php echo $this->baseurl.'/novopromotor/?lang='.$language_tag; ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_NEWREGISTRATION_2'); ?></a></p>
                </div>
            <?php endif; ?>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('com_users',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('user.login',$setencrypt_forminputhidden); ?>"/>

            <input type="hidden" name="return" value="<?php echo base64_encode(JUri::base()); ?>" />
            <?php echo JHtml::_('form.token'); ?>
        </form>

        <div class="introLogin">
            <p><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_INTRO1');?></p>
            <p><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_COMERCIAL_INTRO2');?></p>
        </div>

    </div>

    <div id="cover-spin"></div>

    <!-- FOOTER -->
    <jdoc:include type="modules" name="debug" style="none" />
    <?php
    // CUSTOM JS DASHboard
    echo ("<!--[if lt IE 9]>");
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/respond.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/excanvas.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/ie8.fix.min.js');
    echo ("<![endif]-->");
    ?>

    </body>

</html>

