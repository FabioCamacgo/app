<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 *
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$obParam      = new VirtualDeskSiteParamsHelper();

$nomeAPP = $obParam->getParamsByTag('nomeAPP');
$versaoAPP = $obParam->getParamsByTag('versaoAPP');
$linkVersaoAPP = $obParam->getParamsByTag('linkVersaoAPP');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
$LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
$linkLogoCopyright = $obParam->getParamsByTag('linkLogoCopyright');
$logoCopyright = $obParam->getParamsByTag('logoCopyright');
$ligaLogoCopyright = $obParam->getParamsByTag('ligaLogoCopyright');
$ligaversaoSoftware = $obParam->getParamsByTag('ligaversaoSoftware');

/* load common headers for template: head, css, javascript */
require_once(dirname(__FILE__) . '/tpl_headers.php');

$app             = JFactory::getApplication();
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$sitemenulayout  = $templateParams->get('sitemenulayout');

$this->setGenerator('');

?>
<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
	echo ($this->direction == 'rtl' ? ' rtl' : '');

	// Dashboard - classes
    echo ('page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed');
?>">
	<!-- Body -->
    <div class="page-wrapper">

        <?php

        require_once(dirname(__FILE__) . '/tpl_body_header_top.php');
        ?>

        <div class="clearfix"> </div>


			<jdoc:include type="modules" name="banner" style="xhtml" />


                <div class="page-container">

                    <?php

                    require_once(dirname(__FILE__) . '/tpl_body_sidebar_menu.php');
                    ?>

                    <div class="page-content-wrapper">

                        <div class="page-content">



                            <!-- BEGIN PAGE BAR -->
                            <div class="page-bar">


                            </div>

                                <jdoc:include type="modules" name="position-3" style="xhtml" />
                                <jdoc:include type="modules" name="position-2" style="none" />
                                <jdoc:include type="message" />

                                <?php if ($this->params->get('displayComponentHomePage')) : ?>
                                    <jdoc:include type="component" />
                                <?php endif; ?>



                        </div>

                    </div>

                </div>


        <div class="page-footer">
            <div class="page-footer-inner">

            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>



        </div>
    </div>

    <div class="copyright">

        <div class="version">
            <?php
                if($ligaversaoSoftware == 1){
                    ?>
                        <a href="<?php echo $linkVersaoAPP;?>" target="_blank">
                            <?php echo $nomeAPP . ' ' . $versaoAPP;?>
                        </a>
                    <?php
                }
            ?>
        </div>

        <div class="brand">
            <span><?php echo $year = date("Y"); ?> ©</span>

            <a href="<?php echo $LinkCopyright;?>" target="_blank">
                <?php echo $copyrightAPP;?>
            </a>
        </div>

        <div class="owner">
            <?php
                if($ligaLogoCopyright == 1){
                    ?>
                        <a href="<?php echo $linkLogoCopyright;?>" target="_blank">
                            <img src="<?php echo $this->baseurl . $logoCopyright?>">
                        </a>
                    <?php
                }
            ?>
        </div>

    </div>


<?php
require_once(dirname(__FILE__) . '/tpl_footers.php');
?>

</body>
</html>