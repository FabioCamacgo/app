<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common headers for template: head, css, javascript
 */

defined('_JEXEC') or die;

//JLoader::register('VirtualDeskSiteContactUsFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactusfields.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;


$obParam      = new VirtualDeskSiteParamsHelper();

$imageBackground = $obParam->getParamsByTag('imgFundoAPP');
$logoInterface = $obParam->getParamsByTag('logoInterfaceBlack');
$versao = $obParam->getParamsByTag('versaoAPP');
$linkVersao = $obParam->getParamsByTag('linkVersaoAPP');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
$LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
$LinkInterface = $obParam->getParamsByTag('LinkInterface');
$LogoEntrada = $obParam->getParamsByTag('logoEntradaAPP');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Output as HTML5
$doc->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');


// Add JavaScript Frameworks
JHtml::_('jquery.framework',false);
JHtml::_('bootstrap.framework',false);

/* Problema de conflict com o $ no JQuery
 * tive de retirar "manualmente o carregamento anterior... sem carregar o bootstrap não conseguimos retirar
 * Solução: http://joomla.stackexchange.com/questions/97/disable-script-loading-in-head
 * Ref: https://docs.joomla.org/J3.2:Javascript_Frameworks
 * scripts to remove, customise as required
*/

unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/chosen.jquery.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-migrate.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/system/js/caption.js']);


/*
 * Problema do JCaption ao carregar
 * Solução: http://joomla.stackexchange.com/a/14867/9696
 *
 * Coloquei o IF porque nas tentativas de acesso ao user profile e ao colocar o override com o plugin dava erro nesta parte.
*/
if(array_key_exists('text/javascript', $doc->_script))
{
    $doc->_script['text/javascript'] = preg_replace('%jQuery\(window\).on\(\'load\',\s*function\(\)\s*\{\s*new\s*JCaption\(\'img.caption\'\)\;\s*\}\)\;\s*%', '', $doc->_script['text/javascript']);
}

// CUSTOM JS - dashboard
//BEGIN CORE PLUGINS
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/js/bootstrap.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/js.cookie.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.blockui.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');

// BEGIN PAGE LEVEL PLUGINS
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js');
//$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/js/select2.full.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/backstretch/jquery.backstretch.min.js');

// END PAGE LEVEL PLUGINS

//BEGIN THEME GLOBAL SCRIPTS
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/scripts/app.min.js');
//END THEME GLOBAL SCRIPTS

//BEGIN PAGE LEVEL SCRIPTS
//$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/scripts/login-5.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/scripts/login-5.js');
// END PAGE LEVEL SCRIPTS

$doc->addScriptVersion($this->baseurl . '/administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js');

// BEGIN THEME LAYOUT SCRIPTS
// END THEME LAYOUT SCRIPTS


// Add Stylesheets
//$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/template.css');

// Custom CSS - Dashboad
//BEGIN GLOBAL MANDATORY STYLES
$doc->addStyleSheet('//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/css/bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2-bootstrap.min.css');
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/components-rounded.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/plugins.min.css');
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/css/login-5.min.css');
// END PAGE LEVEL STYLES

//BEGIN THEME LAYOUT STYLES
//END THEME LAYOUT STYLES

// Crypt Inpout Hidden
$setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

// Logo file or site title param
if ($this->params->get('logoFile'))
{	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}

// JHtml::_('jquery.framework', false)
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $doc->language; ?>" dir="<?php echo $doc->direction; ?>">
<!--<![endif]-->

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <jdoc:include type="head" />

    <style>
        .user-login-5 .login-container > .login-content { margin-top: 50px; }
        .user-login-5 .login-logo.login-6 { top: 2.5em; left: 2.5em; }
        .user-login-5 .login-container > .login-content > .login-form { margin-top: 10px;  }
        .control-label {font-size: 14px; color: #4e5a64; text-align: right;  margin-bottom: 0; padding-top: 7px;}
        .help-block { margin-top: -20px;  margin-bottom: 15px;}
    </style>
</head>

<body class=" login">

<script>
    arrayVDPagesLoginBackGround = [
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg1.jpg",
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg2.jpg",
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg3.jpg"
    ];


    /*
     * Parametros e Mensagens para a verificação do NIF em javascript / jquery validation
     */
    var virtualDeskNIFCheckOptions = {
        message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'); ?>"
    };

    var setUserFieldLoginType_JS = <?php echo $setUserFieldLoginType_JS ?>;

</script>

<!-- Body -->
<!-- BEGIN : LOGIN PAGE 5-2 -->
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 login-container bs-reset">
            <div class="row">
                <div class="logo">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo $this->baseurl; ?>">
                                <img src="<?php echo $this->baseurl; ?>/<?php echo $LogoEntrada ?>"  alt="logo" class="logo-default"/>
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="login-content">

                <h1><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_TITLE'); ?></h1>
                <p><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_DESC'); ?></p>

                <!-- system messages -->
                <jdoc:include type="message" />

                <!-- alerts -->
                <div id="MainMessageAlertBlockSimple" class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span><?php echo $CampoForm_LoginFormAlert; ?>  </span>
                </div>

                <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                </div>

                <form class="login-form" action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="form-login">

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $CampoForm_Username_Label; ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo $CampoForm_Username_Label; ?>" name="username" id="username" maxlength="<?php echo $CampoForm_Username_MaxLength; ?>" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_LABEL'); ?></label>
                        <div class="col-md-9">
                            <input type="password" class="form-control placeholder-no-fix" autocomplete="off"  placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" name="password"  maxlength="25"/>
                        </div>
                    </div>

                    <?php if (count($twofactormethods) > 1) : ?>
                    <div class="form-group">
                        <label for="secretkey" class="control-label visible-ie8 visible-ie9"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?></label>
                        <input class="form-control form-control-solid placeholder-no-fix" type="text" name="secretkey" id="secretkey" title="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" />
                    </div>
                    <?php endif; ?>


                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <input type="submit" name="<?php echo JText::_('JLOGIN'); ?>" value ="<?php echo JText::_('JLOGIN'); ?>" class="btn green uppercase" />
                        </div>
                    </div>


                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('com_users',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('user.login',$setencrypt_forminputhidden); ?>"/>

                    <input type="hidden" name="return" value="<?php echo base64_encode(JUri::base()); ?>" />
                    <?php echo JHtml::_('form.token'); ?>
                </form>

                <div class="row">

                    <!-- BEGIN LINK  NEW USER REGISTRATION -->
                    <?php if($configSetNewRegistration == '1' ) : ?>
                        <p><a href="<?php echo$this->baseurl.'/newcorporateuser/?lang='.$language_tag; ?>"  class=""><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_NEWREGISTRATION'); ?></a></p>
                    <?php endif; ?>

                    <!-- BEGIN LINK  FORGOT PASSWORD -->
                    <?php if($configSetForgotPassword == '1') : ?>
                        <p><a href="<?php echo$this->baseurl.'/forgotpassword/?lang='.$language_tag; ?>"  class=""><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_FORGOTPASSWORD'); ?></a></p>
                    <?php endif; ?>

                    <!-- BEGIN LINK  LOGIN IN WITH TEXT TYPE -->
                    <?php if($setUserFieldLoginTypeAllowMultiple == '1' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ) : ?>
                        <p><a href="<?php echo$this->baseurl.'?allowtextlogin=allow'; ?>"  class=""><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_LOGINTEXTOPTION'); ?></a></p>
                    <?php endif; ?>

                </div>

            </div>

            <div class="copyright">

                <div class="col-md-4">
                    <div class="spaceTop"><a href="<?php echo $linkVersao;?>" class="versao" target="_blank">digitalGOV V.<?php echo $versao;?></a></div>
                </div>

                <div class="col-md-4">
                    <div class="contentCopyright">
                        <?php echo $year = date("Y"); ?> ©
                        <a href="<?php echo $LinkCopyright; ?>" target="_blank">
                            <?php echo $copyrightAPP; ?>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="logoInterface" style="margin-top:0;">
                        <a href="<?php echo $LinkInterface;?>" target="_blank">
                            <span class="prod">The Interface Projects <br><span class="connect">Governação Digital</span></span>
                            <img src="<?php echo $baseurl . $logoInterface;?>" alt="TIP" title=""/>
                        </a>
                    </div>
                </div>


            </div>


        </div>

        <div class="col-md-6 bs-reset">
            <img src="<?php echo $baseurl . $imageBackground; ?>" alt="imgBackground"/>
        </div>

    </div>
</div>

<div id="cover-spin"></div>

<!-- FOOTER -->
<jdoc:include type="modules" name="debug" style="none" />
<?php
// CUSTOM JS DASHboard
echo ("<!--[if lt IE 9]>");
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/respond.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/excanvas.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/ie8.fix.min.js');
echo ("<![endif]-->");

/*
* Tive de retirar o login-5.js para poder "customizar" a entrada com a validação, etc... passei para o ficheiro newregistration.js
* Devido a isso tive de colocar o código para as imagens de background no final deste ficheiro
*/
?>
<script>
    jQuery(document).ready(function() {
        // init background slide images
        jQuery('.login-bg').backstretch(arrayVDPagesLoginBackGround, {
                fade: 1000,
                duration: 8000
            }
        );
    });
</script>
</body>
</html>