<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common headers for template: head, css, javascript
 */

    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $user            = JFactory::getUser();
    $this->language  = $doc->language;
    $this->direction = $doc->direction;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $imageBackground = $obParam->getParamsByTag('imgFundoAPP');
    $logoInterface = $obParam->getParamsByTag('logoInterfaceBlack');
    $versao = $obParam->getParamsByTag('versaoAPP');
    $linkVersao = $obParam->getParamsByTag('linkVersaoAPP');
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
    $LinkInterface = $obParam->getParamsByTag('LinkInterface');
    $LogoEntrada = $obParam->getParamsByTag('logoEntradaAPP');
    $ligaLogoInterface = $obParam->getParamsByTag('ligaLogoInterface');
    $ligaversaoSoftware = $obParam->getParamsByTag('ligaversaoSoftware');
    $LogoEntradaCorporate = $obParam->getParamsByTag('LogoEntradaCorporate');
    $LogoCssFile = $obParam->getParamsByTag('cssFileGeral');
    $IconPasswordBalcaoOnline = $obParam->getParamsByTag('passwordBalcaoOnline');
    $IconLoginBalcaoOnline = $obParam->getParamsByTag('loginBalcaoOnline');
    $brasaoMunicipio = $obParam->getParamsByTag('brasaoMunicipio');
    $sliderLogin = $obParam->getParamsByTag('sliderLogin');
    $textoSliderLogin = $obParam->getParamsByTag('textoSliderLogin');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Output as HTML5
    $doc->setHtml5(true);

    // Getting params from template
    $params = $app->getTemplate(true)->params;

    // Detecting Active Variables
    $option   = $app->input->getCmd('option', '');
    $view     = $app->input->getCmd('view', '');
    $layout   = $app->input->getCmd('layout', '');
    $task     = $app->input->getCmd('task', '');
    $itemid   = $app->input->getCmd('Itemid', '');
    $sitename = $app->get('sitename');


    // Add JavaScript Frameworks
    JHtml::_('jquery.framework',false);
    JHtml::_('bootstrap.framework',false);

    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/chosen.jquery.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-migrate.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);
    unset($doc->_scripts[JURI::root(true) . '/media/system/js/caption.js']);

    if(array_key_exists('text/javascript', $doc->_script))
    {
        $doc->_script['text/javascript'] = preg_replace('%jQuery\(window\).on\(\'load\',\s*function\(\)\s*\{\s*new\s*JCaption\(\'img.caption\'\)\;\s*\}\)\;\s*%', '', $doc->_script['text/javascript']);
    }

    // CUSTOM JS - dashboard
    //BEGIN CORE PLUGINS
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/js/bootstrap.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/js.cookie.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.blockui.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');

    // BEGIN PAGE LEVEL PLUGINS
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js');
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js');
    // END PAGE LEVEL PLUGINS

    //BEGIN PAGE LEVEL SCRIPTS
    $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/scripts/login.js');
    // END PAGE LEVEL SCRIPTS
    $doc->addScriptVersion($this->baseurl . '/administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js');



    // Custom CSS - Dashboad
    //BEGIN GLOBAL MANDATORY STYLES
    $doc->addStyleSheet('//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/css/bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/' . $LogoCssFile);
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/components-rounded.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/plugins.min.css');
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/css/login.min.css');
    // END PAGE LEVEL STYLES

    //BEGIN THEME LAYOUT STYLES
    //END THEME LAYOUT STYLES



    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Logo file or site title param
    if ($this->params->get('logoFile'))
    {	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
    }
    elseif ($this->params->get('sitetitle'))
    {	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
    }
    else
    {	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
    }
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
    <!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
    <!--[if !IE]><!-->
    <html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>"   style="overflow-x:hidden;">
    <!--<![endif]-->
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
            <jdoc:include type="head" />
        </head>



        <?php
            if($setUserFieldLoginTypeAllowMultiple == '1' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ){
                ?>
                <body class="site login">
                <?php
            } else if($setUserFieldLoginTypeAllowMultiple == '0' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ){
                ?>
                <body class="site login">
                <?php
            } else {
                ?>
                <body class="site login corporate">
                <?php
            }
        ?>

        <script >
            /*
             * Parametros e Mensagens para a verificação do NIF em javascript / jquery validation
             */
            var virtualDeskNIFCheckOptions = {
                message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'); ?>"
            };

            var setUserFieldLoginType_JS = <?php echo $setUserFieldLoginType_JS ?>;
        </script>

            <div class="contentMain">

                <!-- BEGIN LOGO -->
                <div class="loginLogo">
                    <div class="sliderMotion">
                        <ul>
                            <li>
                                <a href="<?php echo $this->baseurl; ?>">
                                    <?php if(($setUserFieldLoginTypeAllowMultiple == '1' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ) || ($setUserFieldLoginTypeAllowMultiple == '0' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' )){ ?>
                                        <img src="<?php echo $this->baseurl; ?>/<?php echo $LogoEntrada ?>"  alt="logo" class="logo-default"/>
                                    <?php } else { ?>
                                        <img src="<?php echo $this->baseurl; ?>/<?php echo $LogoEntradaCorporate ?>"  alt="logo" class="logo-default"/>
                                    <?php } ?>
                                </a>
                            </li>

                            <?php
                                if(($setUserFieldLoginTypeAllowMultiple == '1' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ) || ($setUserFieldLoginTypeAllowMultiple == '0' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' )){
                                    if($sliderLogin == 1){

                                        if(!empty($textoSliderLogin)){
                                            $explodeTextSlider = explode(';;', $textoSliderLogin);

                                            $subtitulo = $explodeTextSlider[0];
                                            $titulo = $explodeTextSlider[1];
                                            $tag = $explodeTextSlider[2];
                                            $mensagem = $explodeTextSlider[3];

                                            ?>

                                            <li>
                                                <div class="subtitulo"><?php echo $subtitulo;?></div>
                                                <div class="titulo"><?php echo $titulo;?></div>
                                                <div class="tag"><?php echo $tag;?></div>
                                                <div class="mensagem"><?php echo $mensagem;?></div>
                                            </li>

                                            <?php
                                        }

                                    }
                                }

                            ?>
                        </ul>
                    </div>
                </div>


                <!-- END LOGO -->



                <!-- BEGIN LOGIN -->
                <div class="loginContent">
                    <!-- BEGIN LOGIN FORM -->

                    <?php
                        if(!empty($brasaoMunicipio)){
                            ?>
                            <a href="<?php echo $LinkCopyright;?>">
                                <div class="brasao">
                                    <img src="<?php echo $this->baseurl . '/' . $brasaoMunicipio;?>" alt="brasao"/>
                                </div>
                            </a>
                            <?php
                        }

                        $messages       = $app->getMessageQueue();
                        $messagesDanger = '';
                        $messagesDangerNumber = 0;
                        $messagesSucess = '';

                        if(!is_array($messages)) $messages = array();

                        foreach($messages as $chvMsg => $valMsg)
                        {
                            if($valMsg['type']=='warning' or $valMsg['type']=='error')
                            {
                                $messagesDanger .= '<div class="errorMessage"><i class="fa-lg fa fa-warning"></i>&nbsp;<span>'.$valMsg['message'].'</span></div>';
                                $messagesDangerNumber = $messagesDangerNumber + 1;
                            }
                            elseif ($valMsg['type']=='message')
                            {
                                $messagesSucess .= '<div class="successMessage"><i class="fa-lg fa fa-check"></i>&nbsp;<span>'.$valMsg['message'].'</span></div>';
                            }
                        }

                    ?>

                    <form class="login-form" action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="form-login">
                        <h3 class="form-title"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_TITLE'); ?></h3>

                        <!-- system messages -->
                        <jdoc:include type="message" />

                        <!-- alerts -->
                        <div id="MainMessageAlertBlockSimple" class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            <span><?php echo $CampoForm_LoginFormAlert; ?>  </span>
                        </div>

                        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                        </div>

                        <?php if (!empty($messagesDanger) && $messagesDangerNumber > 1) : ?>
                            <div id="MainMessageAlertBlock2Joomla" class="alert alert-danger fade in" >
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <?php echo $messagesDanger; ?>
                            </div>
                        <?php endif; ?>


                        <div class="form-group">
                            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                            <label class="control-label"><?php echo $CampoForm_Username_Label ?></label>
                            <div class="icon"><img src="<?php echo $this->baseurl . $IconLoginBalcaoOnline;?>" alt="loginImg"/></div>
                            <input class="form-control form-control-solid placeholder-no-fix" type="text" required autocomplete="off" name="username" id="username" maxlength="<?php echo $CampoForm_Username_MaxLength; ?>" />
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label>
                            <div class="icon"><img src="<?php echo $this->baseurl . $IconPasswordBalcaoOnline;?>" alt="passwordImg"/></div>
                            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" name="password" />
                        </div>

                        <?php if (count($twofactormethods) > 1) : ?>
                            <div class="form-group">
                                <label for="secretkey" class="control-label visible-ie8 visible-ie9"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?></label>
                                <input class="form-control form-control-solid placeholder-no-fix" type="text" name="secretkey" id="secretkey" title="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" />
                            </div>
                        <?php endif; ?>

                        <?php if($configSetForgotPassword == '1' && $getAllowTextLogin!='allow') : ?>
                            <p class="forgotPassword"><a href="<?php echo$this->baseurl.'/forgotpassword/?lang='.$language_tag; ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_FORGOTPASSWORD'); ?></a></p>
                        <?php endif; ?>

                        <div class="form-actions">

                            <input type="submit" name="<?php echo JText::_('JLOGIN'); ?>" value ="<?php echo JText::_('JLOGIN'); ?>" class="btn" />

                        </div>

                        <div class="login-options">

                            <!--<?php if($configSetForgotPassword == '1' && $getAllowTextLogin=='allow') : ?>
                                    <p class="corporate"><a href="<?php echo$this->baseurl.'/forgotpassword/?lang='.$language_tag; ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_FORGOTPASSWORD'); ?></a></p>
                                <?php endif; ?>-->

                            <?php if($setUserFieldLoginTypeAllowMultiple == '1' && $setUserFieldLoginType=="login_as_nif" && $getAllowTextLogin!='allow' ) : ?>
                                <p class="corporate"><a href="<?php echo$this->baseurl.'?allowtextlogin=allow'; ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_LOGINTEXTOPTION'); ?></a></p>
                            <?php endif; ?>

                            <?php if($configSetNewRegistration == '1' && $getAllowTextLogin!='allow' ) : ?>
                                <p class="newUser"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_NEWREGISTRATION_INTRO'); ?> <a href="<?php echo$this->baseurl.'/newcorporateuser/?lang='.$language_tag; ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGINFORM_NEWREGISTRATION'); ?></a></p>
                            <?php endif; ?>

                        </div>


                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('com_users',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('user.login',$setencrypt_forminputhidden); ?>"/>

                        <input type="hidden" name="return" value="<?php echo base64_encode(JUri::base()); ?>" />
                        <?php echo JHtml::_('form.token'); ?>
                    </form>
                    <!-- END LOGIN FORM -->

                </div>


            </div>


            <!-- FOOTER -->
            <jdoc:include type="modules" name="debug" style="none" />
            <?php
                // CUSTOM JS DASHboard
                echo ("<!--[if lt IE 9]>");
                $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/respond.min.js');
                $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/excanvas.min.js');
                $doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/ie8.fix.min.js');
                echo ("<![endif]-->");
            ?>

            <script>
                <?php
                    require_once (JPATH_SITE . '/templates/virtualdesk/tpl_offline_layout_center.js.php');
                ?>
            </script>

        </body>
    </html>
