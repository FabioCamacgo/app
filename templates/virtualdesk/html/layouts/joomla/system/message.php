<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteGeneralHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_general.php');

$msgList = $displayData['msgList'];

$currentUserSessionID = VirtualDeskSiteUserHelper::getUserSessionId();
if ((int)$currentUserSessionID <= 0) {
    $msgList = VirtualDeskSiteGeneralHelper::removeDisplayMsgListMessage($msgList , null, ['error','warning']);
}


$typeVDCss   = '';
$typeVDTitle = '';
$typeVDCss = '';

?>
<style>
    #system-message div.alert i {float: left; padding-right: 15px;}
    #system-message div.alert .alert-heading  {font-weight: 600;}

    #system-message .alert-default { background-color: #f5f8fd;  border-color: #e5e5e5;}
    #system-message .alert-default .alert-heading, #system-message .alert-default i {color: #1f78b5;}


</style>
<div id="system-message-container">
	<?php if (is_array($msgList) && !empty($msgList)) : ?>
		<div id="system-message">
			<?php foreach ($msgList as $type => $msgs) : ?>

                <?php
                switch ($type)
                {
                    case 'error':
                        $typeVDCss    = 'danger';
                        $typeVDTitle  = JText::_('COM_VIRTUALDESK_ALERT_ERROR_TITLE');
                        $typeVDIcon   = '<i class="fa-2x fa fa-times-circle"></i>';
                        break;
                    case 'warning':
                        $typeVDCss    = 'warning';
                        $typeVDTitle  = JText::_('COM_VIRTUALDESK_ALERT_WARNING_TITLE');
                        $typeVDIcon   = '<i class="fa-2x fa fa-warning"></i>';
                        break;
                    case 'info':
                        $typeVDCss    = 'info';
                        $typeVDTitle  = JText::_('COM_VIRTUALDESK_ALERT_INFO_TITLE');
                        $typeVDIcon   = '<i class="fa-2x fa fa-exclamation-circle"></i>';
                        break;
                    case 'success':
                        $typeVDCss    = 'success';
                        $typeVDTitle  = JText::_('COM_VIRTUALDESK_ALERT_SUCCESS_TITLE');
                        $typeVDIcon   = '<i class="fa-2x fa fa-check"></i>';
                        break;
                    default:
                        $typeVDCss    = 'default';
                        $typeVDTitle  = JText::_('COM_VIRTUALDESK_ALERT_DEFAULT_TITLE');
                        $typeVDIcon   = '<i class="fa-2x fa fa-exclamation"></i>';
                        break;
                }

                ?>

				<div class="alert alert-<?php echo $typeVDCss; ?>">
					<?php // This requires JS so we should add it trough JS. Progressive enhancement and stuff. ?>
					<a class="close" data-dismiss="alert">×</a>

					<?php if (!empty($msgs)) : ?>
                        <?php if (!empty($typeVDIcon)) echo $typeVDIcon; ?>
						<h4 class="alert-heading "><?php echo $typeVDTitle; ?></h4>

						<div>
							<?php foreach ($msgs as $msg) : ?>
								<div class="alert-message"><?php echo $msg; ?></div>
							<?php endforeach; ?>
						</div>

					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>
