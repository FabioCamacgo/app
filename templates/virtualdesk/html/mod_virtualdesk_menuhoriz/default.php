<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_virtualdesk_menuhoriz
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if (($tagId = $params->get('tag_id', '')))
{	$id = ' id="' . $tagId . '"';
}
?>

<!--start of menu-->
<li class=" menu<?php echo $class_sfx; ?>"<?php echo $id; ?>>


<?php
$CompLista          = sizeof($list);
$classMegaMenuColMd = '';
foreach ($list as $Indice => $item) {
    // Percorre a lista para assinalarr o nível do próximo elemento... de modo a podermos fechar quando o menu é "mega drop down"
if( ($Indice+1)<=($CompLista-1) ) {
    $list[$Indice]->NextLevel = $list[$Indice+1]->level;
}
else {
    $list[$Indice]->NextLevel = 1;
}

    // Assina o nivel do elemento anterior
    if( ($Indice==0) ) $list[$Indice]->PrevLevel = 1;
    if( ($Indice>=1) ) $list[$Indice]->PrevLevel = $list[$Indice-1]->level;
}


// search items do menu
$isMegaMenu = false;
foreach ($list as $i => &$item)
{
    // Só verifica permissões se o menu não for o inicial
    // O ÚLTIMO MENU tem de sempre o SAIR... por isso colocamos -1 em baixo para estr sempre disponível
   if( $i>0 && $i<= (sizeof($list)-1) ) {
        $vbHasAccess = $objCheckPerm->checkMenuAccess($item->id);
        if ($vbHasAccess === false) {
            // Avança para o próximo, não desenha este
            continue;
        }
    }

	$class = 'item-' . $item->id;
	if ($item->id == $default_id)
	{	$class .= 'start ';
	}

	if (($item->id == $active_id) || ($item->type == 'alias' && $item->params->get('aliasoptions') == $active_id))
	{	$class .= ' current ';
	}

	if (in_array($item->id, $path))
	{	$class .= ' active ';
	}
	elseif ($item->type == 'alias')
	{	$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{	$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{	$class .= ' alias-parent-active';
		}
	}

	if ($item->type == 'separator')
	{	$class .= ' heading';

	}


	if ($item->deeper && $item->level>1 && $isMegaMenu==false)
	{	//submenu que não esteja no nível 1
	    $class .= ' dropdown-submenu ';
	}
	elseif( $item->deeper && $item->level==1 )
    {
        // Verifica se tem a classe "mega-menu-dropdown" associado ao menu, caos contrário será um menu clássioc
        $posMegaMenuClass      = strpos($item->anchor_css, 'mega-menu-dropdown');
        $posMegaMenuFullClass  = strpos($item->anchor_css, 'mega-menu-full');
        $posMegaMenuColMdClass = strpos($item->anchor_css, 'mega-menu-col-md-');

        if($posMegaMenuClass!==false){
            // Retira do elemento e coloca no <li> ATUAL
            $item->anchor_css = str_replace('mega-menu-dropdown','',$item->anchor_css);
            $class .= ' mega-menu-dropdown ';
            if($posMegaMenuFullClass!==false) {
                $item->anchor_css = str_replace('mega-menu-full','',$item->anchor_css);
                $class .= ' mega-menu-full ';
            }

            if($posMegaMenuColMdClass!==false) {

                if($posMegaMenuColMdClass<strlen($item->anchor_css)){
                    $postMegaMenuColMdClassEnd = strpos($item->anchor_css," ",$posMegaMenuColMdClass+1);
                }
                else {
                    $postMegaMenuColMdClassEnd = strpos($item->anchor_css," ",$posMegaMenuColMdClass);
                }
                if($postMegaMenuColMdClassEnd===false) $postMegaMenuColMdClassEnd = strlen($item->anchor_css);
                $classMegaMenuColMdRemove = substr($item->anchor_css, $posMegaMenuColMdClass-18, $postMegaMenuColMdClassEnd);
                $classMegaMenuColMd       = substr($item->anchor_css, $posMegaMenuColMdClass-8, $postMegaMenuColMdClassEnd);
                $item->anchor_css = str_replace($classMegaMenuColMdRemove,'',$item->anchor_css);

            }
            else{
                $classMegaMenuColMd = 'col-md-12';
            }

            //Definimos a variável que vai identificar se o menu atual é do tipo MegaMenu ou do tipo Classic para os restantes subelementos
            $isMegaMenu = true;

        }
        else
        {   // Sub tipo de menu horizontal por defeito pasra o primeiro nível de elementos
            $class .= ' classic-menu-dropdown ';

            //Definimos a variável que vai identificar se o menu atual é do tipo MegaMenu ou do tipo Classic para os restantes subelementos
            $isMegaMenu         = false;
            $classMegaMenuColMd = '';
        }

    }

	if ($item->parent===true)
	{	$class .= ' parent';
	}


	// Se for um separador com megamenu e que não é um separador de inicio do 2º nivel
    if ($item->type == 'separator' && $isMegaMenu == true && $item->level>=2 && $item->PrevLevel!=1 && $item->NextLevel!=1)
    {   echo ('<!-- separator $isMegaMenu  $item->level>=2 && $item->PrevLevel!=1 && $item->NextLevel!=1 -->');
        echo ('</ul></div>');
        echo '<div class="'.$classMegaMenuColMd.'"><ul class="mega-menu-submenu">';
	}


    echo '<li class="' . $class . '">';


	switch ($item->type) :
		case 'separator':
		case 'component':
		case 'heading':
		case 'url':
			require JModuleHelper::getLayoutPath('mod_virtualdesk_menuhoriz', 'default_' . $item->type);
			break;
		default:
			require JModuleHelper::getLayoutPath('mod_virtualdesk_menuhoriz', 'default_url');
			break;
	endswitch;

	// The next item is deeper. Faz um submenu com o elemento <ul> se o próximo é um submenu...
	if ($item->deeper)
	{
        // elemento que agrega o item

        // Se for megamenu vai definir um elemmento com N colunas de acordo com os submenus que queremos
        if( $isMegaMenu == true && $item->level==1 ) {
           echo ('<!-- $isMegaMenu == true && $item->level==1 vai definir um elemmento com N colunas -->');
           echo '<ul class="dropdown-menu" style="min-width: 700px;">';
           echo '<li><div class="mega-menu-content"><div class="row"><div class="'.$classMegaMenuColMd.'"><ul class="mega-menu-submenu">';
        }
        elseif( $isMegaMenu == true && $item->level==2 ) {
            echo '';
        }
        elseif( $isMegaMenu == true && $item->level>2 ) {
            echo '';
        }
        else {
            echo '<ul class="dropdown-menu">';
        }

	}
	// The next item is shallower -> fim de nível...
	elseif ($item->shallower)
	{   echo '</li>';

        if( $isMegaMenu == true && $item->level==1) {
            echo ('<!-- $isMegaMenu == true && $item->level==1 Fecha o content Fecha o mega submenu-->');
            echo ('</ul></div></div>'); // Fecha o content
            echo ('</div></li></ul>'); // Fecha Mega Menu
        }
        elseif( $isMegaMenu == true && $item->NextLevel==1) {
            echo ('<!-- $isMegaMenu == true && $item->NextLevel Fecha o content Fecha o mega submenu-->');
            echo ('</ul></div></div>'); // Fecha o content
            echo ('</div></li></ul>');  // Fecha o mega submenu
        }
        elseif( $isMegaMenu == true && $item->NextLevel!=1) {
            echo ('<!--$isMegaMenu == true && $item->NextLevel!=1 -->');
        }
        else {
            echo ('<!-- str_repeat -->');
            echo str_repeat('</ul></li>', $item->level_diff);
        }

	}
	// The next item is on the same level.
	else
	{   echo '</li >';
	}
}
?>
</li>
