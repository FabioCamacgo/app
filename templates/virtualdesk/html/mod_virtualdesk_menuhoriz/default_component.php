<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_virtualdesk_menuhoriz
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$attributes = array();
$attributes['class'] = '';
$classIcon = '';
$classFA   = '';
$linktype  = '';

if ($item->anchor_title)
{
	$attributes['title'] = $item->anchor_title;
}

// Verifica se existe um icon- ou um fontawsome (fa-) no CSS associado ao item
if ($item->anchor_css)
{   // Verifica icon-xxx
    $posIcon = strpos($item->anchor_css, 'icon-');
    if($posIcon!==false){
        $postIconEnd =  strpos($item->anchor_css," ",$posIcon+1);
        if($postIconEnd===false) $postIconEnd = strlen($item->anchor_css);
        $classIcon = substr($item->anchor_css, $posIcon, $postIconEnd);
        $item->anchor_css = str_replace($classIcon,'',$item->anchor_css);
    }
    // Verifica fa-xxx
    $posFA = strpos($item->anchor_css, 'fa-');
    if($posFA!==false){
        $postFAEnd =  strpos($item->anchor_css," ",$posFA+1);
        if($postFAEnd===false) $postFAEnd = strlen($item->anchor_css);
        $classFA = substr($item->anchor_css, $posFA, $postFAEnd);
        $item->anchor_css = str_replace($classFA,'',$item->anchor_css);
    }

    $attributes['class'] = $item->anchor_css;
}

if ($item->anchor_rel)
{
	$attributes['rel'] = $item->anchor_rel;
}

// se tem icons
if($classIcon!='') $linktype = '<i class="'.$classIcon.'"></i>';

// se tem FontAwsome
if($classFA!='') $linktype = '<i class=" fa '.$classFA.'"></i>';

$linktype .= '<span class="title">'.$item->title.'</span>';

//$linktype = $item->title;

if ($item->menu_image)
{
	$linktype = JHtml::_('image', $item->menu_image, $item->title);

	if ($item->params->get('menu_text', 1))
	{
		$linktype .= '<span class="image-title">' . $item->title . '</span>';
	}
}

if ($item->browserNav == 1)
{
	$attributes['target'] = '_blank';
}
elseif ($item->browserNav == 2)
{
	$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes';

	$attributes['onclick'] = "window.open(this.href, 'targetWindow', '" . $options . "'); return false;";
}

echo JHtml::_('link', JFilterOutput::ampReplace(htmlspecialchars($item->flink)), $linktype, $attributes);
