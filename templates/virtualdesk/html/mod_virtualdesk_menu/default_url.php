<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_virtualdesk_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$attributes = array();
$attributes['class'] = '';
$classIcon = '';
$linktype  = '';

if ($item->anchor_title)
{	$attributes['title'] = $item->anchor_title;
}

if ($item->anchor_css)
{

    $posIcon = strpos($item->anchor_css, 'icon-');
    if($posIcon===false) $posIcon = strpos($item->anchor_css, 'fa-');
    if($posIcon!==false){
        $postIconEnd =  strpos($item->anchor_css," ",$posIcon+1);
        if($postIconEnd===false) $postIconEnd = strlen($item->anchor_css);
        $classIcon = substr($item->anchor_css, $posIcon, $postIconEnd);
        $item->anchor_css = str_replace($classIcon,'',$item->anchor_css);
    }
    $attributes['class'] = $item->anchor_css;
}

if ($item->anchor_rel)
{	$attributes['rel'] = $item->anchor_rel;
}

// se tem icons
if($classIcon!='') $linktype = '<i class="'.$classIcon.'"></i>';

$linktype .= '<span class="title">'.$item->title.'</span>';

//classes no <a>
$attributes['class'] .= ' nav-link ';

// se tem subniveis
if ($item->deeper)
{	$linktype    .=  ' <span class="arrow"></span>';
    $item->flink = 'javascript:;';
    $attributes['class'] .= ' nav-toggle ';
}


if ($item->menu_image)
{	$linktype = JHtml::_('image', $item->menu_image, $item->title);
	if ($item->params->get('menu_text', 1))
	{  $linktype .= '<span class="image-title">' . $item->title . '</span>';
	}
}

if ($item->browserNav == 1)
{   $attributes['target'] = '_blank';
}
elseif ($item->browserNav == 2)
{	$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,' . $params->get('window_open');
	$attributes['onclick'] = "window.open(this.href, 'targetWindow', '" . $options . "'); return false;";
}

echo JHtml::_('link', JFilterOutput::ampReplace(htmlspecialchars($item->flink, ENT_COMPAT, 'UTF-8')), $linktype, $attributes);
