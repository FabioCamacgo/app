<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_virtualdesk_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$title      = $item->anchor_title ? ' title="' . $item->anchor_title . '"' : '';
//$anchor_css = $item->anchor_css ? $item->anchor_css : '';

$linktype   = $item->title;

if ($item->menu_image)
{
	$linktype = JHtml::_('image', $item->menu_image, $item->title);

	if ($item->params->get('menu_text', 1))
	{
		$linktype .= '<span class="image-title">' . $item->title . '</span>';
	}
}


if ($item->anchor_css)
{
    $posIcon = strpos($item->anchor_css, 'icon-');
    if($posIcon===false) $posIcon = strpos($item->anchor_css, 'fa-');
    if($posIcon!==false){
        $postIconEnd =  strpos($item->anchor_css," ",$posIcon+1);
        if($postIconEnd===false) $postIconEnd = strlen($item->anchor_css);
        $classIcon = substr($item->anchor_css, $posIcon, $postIconEnd);
        $item->anchor_css = str_replace($classIcon,'',$item->anchor_css);
    }
    $anchor_css = $item->anchor_css;
}

// se tem icons
$classIconElem = '';
if($classIcon!='') $classIconElem = '<i class="'.$classIcon.'"></i>';
?>
<?php echo $classIconElem; ?>
<span class="nav-header <?php echo $anchor_css; ?>"<?php echo $title; ?>><?php echo $linktype; ?></span>
