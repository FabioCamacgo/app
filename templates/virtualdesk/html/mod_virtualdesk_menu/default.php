<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_virtualdesk_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();


$id = '';

if (($tagId = $params->get('tag_id', '')))
{	$id = ' id="' . $tagId . '"';
}
?>

<!--start of menu-->
<li class=" menu<?php echo $class_sfx; ?>"<?php echo $id; ?>>


<?php
// search items
foreach ($list as $i => &$item)
{
    // Só verifica permissões se o menu não for o inicial
    // O ÚLTIMO MENU tem de sempre o SAIR... por isso colocamos -1 em baixo para estr sempre disponível
    if( $i>0 && $i<= (sizeof($list)-1) ) {
        $vbHasAccess = $objCheckPerm->checkMenuAccess($item->id);
        if ($vbHasAccess === false) {
            // Avança para o próximo, não desenha este
            continue;
        }
    }


	$class = 'item-' . $item->id;
	if ($item->id == $default_id)
	{	$class .= 'start ';
	}

	if (($item->id == $active_id) || ($item->type == 'alias' && $item->params->get('aliasoptions') == $active_id))
	{	$class .= ' current ';
	}

	if (in_array($item->id, $path))
	{	$class .= ' active ';
	}
	elseif ($item->type == 'alias')
	{	$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{	$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{	$class .= ' alias-parent-active';
		}
	}

	if ($item->type == 'separator')
	{	$class .= ' heading';
	}

	if ($item->deeper)
	{	$class .= ' nav-item  deeper';
	}

	if ($item->parent)
	{	$class .= ' parent';
	}

	// elemento que agrega o item
    echo '<li class="' . $class . '">';

	switch ($item->type) :
		case 'separator':
		case 'component':
		case 'heading':
		case 'url':
			require JModuleHelper::getLayoutPath('mod_virtualdesk_menu', 'default_' . $item->type);
			break;
		default:
			require JModuleHelper::getLayoutPath('mod_virtualdesk_menu', 'default_url');
			break;
	endswitch;

	// The next item is deeper. Faz um submenu.
	if ($item->deeper)
	{   echo '<ul class="sub-menu"> ';
	}
	// The next item is shallower.
	elseif ($item->shallower)
	{   echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	else
	{   echo '</li>';
	}
}
?>
</li>
