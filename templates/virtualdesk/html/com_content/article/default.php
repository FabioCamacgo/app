<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$obParam           = new VirtualDeskSiteParamsHelper();
$artigosPermitidos = $obParam->getParamsByTag('artigosjoomlapermitidos');

$artigosPermitidosIDs = explode(',',$artigosPermitidos);


// Create shortcuts to some parameters.
$params  = $this->item->params;

if ( in_array($this->item->id,$artigosPermitidosIDs) !== true ) :
?>
<h1>ND</h1>
<?php
else:
?>
<div class="item-page<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="https://schema.org/Article">
    <meta itemprop="inLanguage" content="<?php echo ($this->item->language === '*') ? JFactory::getConfig()->get('language') : $this->item->language; ?>" />
    <?php if ($this->params->get('show_page_heading')) : ?>
    <div class="page-header">
        <h1> <?php echo $this->escape($this->params->get('page_heading')); ?> </h1>
    </div>
    <?php endif; ?>

    <div class="page-header">
        <?php if ($params->get('show_title')) : ?>
            <h2 itemprop="headline">
                <?php echo $this->escape($this->item->title); ?>
            </h2>
        <?php endif; ?>
    </div>

    <div itemprop="articleBody">
        <?php echo $this->item->text; ?>
    </div>
</div>
<?php endif; ?>