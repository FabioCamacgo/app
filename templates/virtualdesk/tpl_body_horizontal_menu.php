<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common
 */
defined('_JEXEC') or die;
?>
<!-- BEGIN MEGA MENU -->
<div class="hor-menu  hidden-sm hidden-xs">

    <ul class="nav navbar-nav">

    <!-- MODULE HORIZONTAL MENU-->
    <?php if ($this->countModules('horizontalmenu-top')) : ?>
        <jdoc:include type="modules" name="horizontalmenu-top" style="none" />
    <?php endif; ?>


<!--    <ul class="nav navbar-nav">-->
        


    </ul>
</div>
<!-- END MEGA MENU -->


<!-- BEGIN HEADER SEARCH BOX -->
<!--<form class="search-form" action="extra_search.html" method="GET">-->
<!--    <div class="input-group">-->
<!--        <input type="text" class="form-control" placeholder="Search..." name="query">-->
<!--        <span class="input-group-btn">-->
<!--                                <a href="javascript:;" class="btn submit">-->
<!--                                    <i class="icon-magnifier"></i>-->
<!--                                </a>-->
<!--                            </span>-->
<!--    </div>-->
<!--</form>-->
<!-- END HEADER SEARCH BOX -->