<?php
/**
 * @package     VirtualDesk
 * @subpackage  Templates.VirtualDesk
 * load common headers for template: head, css, javascript
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Output as HTML5
$doc->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');


// Add JavaScript Frameworks
JHtml::_('jquery.framework',false);
JHtml::_('bootstrap.framework',false);

/* Problema de conflict com o $ no JQuery
 * tive de retirar "manualmente o carregamento anterior... sem carregar o bootstrap não conseguimos retirar
 * Solução: http://joomla.stackexchange.com/questions/97/disable-script-loading-in-head
 * Ref: https://docs.joomla.org/J3.2:Javascript_Frameworks
 * scripts to remove, customise as required
*/

unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/chosen.jquery.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-noconflict.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/jquery-migrate.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/jui/js/bootstrap.min.js']);
unset($doc->_scripts[JURI::root(true) . '/media/system/js/caption.js']);


/*
 * Problema do JCaption ao carregar
 * Solução: http://joomla.stackexchange.com/a/14867/9696
 *
 * Coloquei o IF porque nas tentativas de acesso ao user profile e ao colocar o override com o plugin dava erro nesta parte.
*/
if(array_key_exists('text/javascript', $doc->_script))
{
    $doc->_script['text/javascript'] = preg_replace('%jQuery\(window\).on\(\'load\',\s*function\(\)\s*\{\s*new\s*JCaption\(\'img.caption\'\)\;\s*\}\)\;\s*%', '', $doc->_script['text/javascript']);
}

// CUSTOM JS - dashboard
//BEGIN CORE PLUGINS
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/js/bootstrap.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/js.cookie.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery.blockui.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');

// BEGIN PAGE LEVEL PLUGINS
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js');
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/js/select2.full.min.js');

// END PAGE LEVEL PLUGINS

//BEGIN THEME GLOBAL SCRIPTS
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/scripts/app.min.js');
//END THEME GLOBAL SCRIPTS

//BEGIN PAGE LEVEL SCRIPTS
$doc->addScriptVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/scripts/login.min.js');
// END PAGE LEVEL SCRIPTS

// BEGIN THEME LAYOUT SCRIPTS
// END THEME LAYOUT SCRIPTS


// Add Stylesheets
//$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/css/template.css');

// Custom CSS - Dashboad
//BEGIN GLOBAL MANDATORY STYLES
$doc->addStyleSheet('//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap/css/bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/plugins/select2/css/select2-bootstrap.min.css');
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/components-rounded.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/global/css/plugins.min.css');
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$doc->addStyleSheetVersion($this->baseurl . '/templates/' . $this->template . '/assets/pages/css/login.min.css');
// END PAGE LEVEL STYLES

//BEGIN THEME LAYOUT STYLES
//END THEME LAYOUT STYLES

// Logo file or site title param
if ($this->params->get('logoFile'))
{	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}

// JHtml::_('jquery.framework', false)
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<!--<![endif]-->
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<jdoc:include type="head" />
</head>