<?php

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');

// Import filesystem libraries. Perhaps not necessary, but does not hurt
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class VirtualDeskSitePluginsHelper {

    private $rotas  = array();
    private $objURI;
    private $setencrypt_urlqueryparams;

    private $pathLayouts = JPATH_SITE . DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'system'.DIRECTORY_SEPARATOR.'virtualdesk'.DIRECTORY_SEPARATOR.'layouts';

    public function __construct() {
        $this->objURI = new stdClass();
        $this->setencrypt_urlqueryparams = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_urlqueryparams');

        $this->rotas[] = '/newcorporateuser/';
        $this->rotas[] = '/forgotpassword/';
        $this->rotas[] = '/novopromotor/';
        $this->rotas[] = '/recuperacaosenha/';
        $this->rotas[] = '/uploadfoto/';
        $this->rotas[] = '/novoalerta/';
        $this->rotas[] = '/ultimasocorrencias/';
        $this->rotas[] = '/sliderocorrencias/';
        $this->rotas[] = '/alerta_FP_Pesquisa/';
        $this->rotas[] = '/formpesquisa/';
        $this->rotas[] = '/pesquisa/';
        $this->rotas[] = '/historico/';
        $this->rotas[] = '/alertfile/dl/';
        $this->rotas[] = '/comunicarLeitura/';
        $this->rotas[] = '/novoConsumidor/';
        $this->rotas[] = '/validaLeituras/';
        $this->rotas[] = '/enviaLeitura/';
        $this->rotas[] = '/leiturasEnviadas/';
        $this->rotas[] = '/adesaocontrato/';
        $this->rotas[] = '/alteracaocontrato/';
        $this->rotas[] = '/novaproposta/';
        $this->rotas[] = '/recuperarpassword/';
        $this->rotas[] = '/registarutilizador/';
        $this->rotas[] = '/submeterevento/';
        $this->rotas[] = '/novoevento/';
        $this->rotas[] = '/listacategorias/';
        $this->rotas[] = '/tickereventos/';
        $this->rotas[] = '/listaEventosFP/';
        $this->rotas[] = '/totalEventos/';
        $this->rotas[] = '/detalheEventos/';
        $this->rotas[] = '/filtroEventos/';
        $this->rotas[] = '/filtroEventosFP/';
        $this->rotas[] = '/eventfile/dl/';
        $this->rotas[] = '/registoAssociativismo/';
        $this->rotas[] = '/associativismofile/dl/';
        $this->rotas[] = '/listaCatAssociativismo/';
        $this->rotas[] = '/eventosFPAssociativismo/';
        $this->rotas[] = '/filtroAssociacoes/';
        $this->rotas[] = '/detalheAssociacoes/';
        $this->rotas[] = '/tickerAssociacoes/';
        $this->rotas[] = '/listaEventosAssociacoes/';
        $this->rotas[] = '/novaempresa/';
        $this->rotas[] = '/updateduserprofile/';
        $this->rotas[] = '/onAjaxVD/';
        $this->rotas[] = '/registoEmpresas/';
        $this->rotas[] = '/listacategoriasEmpresas/';
        $this->rotas[] = '/listaEmpresas/';
        $this->rotas[] = '/detalheEmpresas/';
        $this->rotas[] = '/listaSubcatConstrucao/';
        $this->rotas[] = '/filtroFP/';
        $this->rotas[] = '/contador/';
        $this->rotas[] = '/totalEmpresas/';
        $this->rotas[] = '/empresafile/dl/';
        $this->rotas[] = '/empresalogo/dl/';
        $this->rotas[] = '/listaAlojamento/';
        $this->rotas[] = '/cleanSafe/';
        $this->rotas[] = '/cinemaItaliano/';
        $this->rotas[] = '/tickerEmprego/';
        $this->rotas[] = '/ofertasEmprego/';
        $this->rotas[] = '/novaOferta/';
        $this->rotas[] = '/detalheEmprego/';
        $this->rotas[] = '/tickerfestas/';
        $this->rotas[] = '/simuladortaxas/';
        $this->rotas[] = '/remocaoVeiculos/dl/';
        $this->rotas[] = '/remocaoVeiculos/';
        $this->rotas[] = '/corteArvores/dl/';
        $this->rotas[] = '/corteArvores/';
        $this->rotas[] = '/limpezaTerrenos/dl/';
        $this->rotas[] = '/limpezaTerrenos/';
        $this->rotas[] = '/limpezaUrbana/dl/';
        $this->rotas[] = '/limpezaUrbana/';
        $this->rotas[] = '/cemiterios/';
        $this->rotas[] = '/queimadas/';
        $this->rotas[] = '/queimadas/dl/';
        $this->rotas[] = '/anunciarEvento/dl/';
        $this->rotas[] = '/anunciarEventoCartaz/dl/';
        $this->rotas[] = '/anunciarEventoGaleria/dl/';
        $this->rotas[] = '/anunciarEventoPrecario/dl/';
        $this->rotas[] = '/anunciarEvento/';
        $this->rotas[] = '/topEventos/';
        $this->rotas[] = '/proximosEventos/';
        $this->rotas[] = '/estouComVontade/';
        $this->rotas[] = '/historicoEventos/';
        $this->rotas[] = '/pagDetalheEvento/';
        $this->rotas[] = '/infoPromotor/';
        $this->rotas[] = '/listaVotacao/';
        $this->rotas[] = '/enviaVotacao/';
        $this->rotas[] = '/eventosRelacionadosPromotor/';
        $this->rotas[] = '/agendaMunicipal/';
        $this->rotas[] = '/mes_a_mes/';
        $this->rotas[] = '/listaCatsSubCats/';
        $this->rotas[] = '/listaEventos/';
        $this->rotas[] = '/pesquisarvagenda/';
        $this->rotas[] = '/ticker_V_agenda/';
        $this->rotas[] = '/sliderGaleria/';
        $this->rotas[] = '/patrocinadorLogo/dl/';
        $this->rotas[] = '/filtroEuropa/';
        $this->rotas[] = '/novoPrograma/dl/';
        $this->rotas[] = '/novoPrograma/';
        $this->rotas[] = '/filtroFPEuropa/';
        $this->rotas[] = '/detalheEuropa/dl/';
        $this->rotas[] = '/detalheEuropa/';
        $this->rotas[] = '/contadorProjetosMedia/';
        $this->rotas[] = '/contadorEntidadesCultura/';
        $this->rotas[] = '/financiamentoCultura/';
        $this->rotas[] = '/financiamentoMedia/';
        $this->rotas[] = '/MadeiraMagazine_FiltroFP/';
        $this->rotas[] = '/MadeiraMagazine_ondeFicarFiltroFP/';
        $this->rotas[] = '/MadeiraMagazine_filtroPercursosPedestres/';
        $this->rotas[] = '/MadeiraMagazine_detalhePercursosPedestres/';
        $this->rotas[] = '/MadeiraMagazine_descricaoPercursosPedestres/';
        $this->rotas[] = '/MadeiraMagazine_detalheParte2PercursosPedestres/';
        $this->rotas[] = '/MadeiraMagazine_patrocinadorPercursosPedestres/';
        $this->rotas[] = '/faleConnosco/';
        $this->rotas[] = '/concelhosName/';
        $this->rotas[] = '/detalheConcelho/';
        $this->rotas[] = '/novaOcorrenciaAlertarPT/';
        $this->rotas[] = '/menuAppAlertarPT/';
        $this->rotas[] = '/listaConcFregs/';
        $this->rotas[] = '/alertarPTOcorrencias/dl/';
        $this->rotas[] = '/faleConnosco/dl/';
        $this->rotas[] = '/eventoCheckNIFPromotor/';
        $this->rotas[] = '/politicaPrivacidade/';
        $this->rotas[] = '/faleConnoscoGovernment/';
        $this->rotas[] = '/faleConnoscoGovernment/dl/';
        $this->rotas[] = '/percursospedestresGaleria/dl/';
        $this->rotas[] = '/percursospedestresCapa/dl/';
        $this->rotas[] = '/percursospedestresGPX/dl/';
        $this->rotas[] = '/percursospedestresKML/dl/';
        $this->rotas[] = '/patrocinadoresLogo/dl/';
        $this->rotas[] = '/noticiasFP/';
        $this->rotas[] = '/noticias/dl/';
        $this->rotas[] = '/noticiasGaleria/dl/';
        $this->rotas[] = '/noticiasDetalhe/';
        $this->rotas[] = '/noticiasFiltro/';
        $this->rotas[] = '/noticiasTicker/';
        $this->rotas[] = '/documentos/dl/';
        $this->rotas[] = '/documentosTree/';
        $this->rotas[] = '/documentosByCat/';
        $this->rotas[] = '/documentosContent/';
        $this->rotas[] = '/diretorioservicosCapa/dl/';
        $this->rotas[] = '/diretorioservicosLogo/dl/';
        $this->rotas[] = '/diretorioservicosGaleria/dl/';
        $this->rotas[] = '/vAgendaEmpresasCapa/dl/';
        $this->rotas[] = '/vAgendaEmpresasLogo/dl/';
        $this->rotas[] = '/vAgendaEmpresasGaleria/dl/';
        $this->rotas[] = '/multimediaFP/';
        $this->rotas[] = '/multimediaCapa/dl/';
        $this->rotas[] = '/listMultimedia/';

    }

    public function setURIObj($objsURI) {
        $this->objURI = $objsURI;
    }

    public function checkIfUrlHasPluginRoute($uriPath)
    {
        // Verifica se alguma das rotas está presente no url... caso contrário nem carrega o objecto de permissões
        // poupando pesquisas à base de dados
        $vbContemRota = false;
        $vsRotaFound  = '';
        foreach ($this->rotas as $rota) {
            //if (strstr($string, $url)) { // mine version
            if (strpos($uriPath, $rota) !== false) {
                $vbContemRota = true;
                $vsRotaFound  = $rota;
                break;
            }
        }

        // Sai sem processar mais nada pois não encontrou a rota
        if($vbContemRota===false) return(true);

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPluginEnabled(false);


        switch ($vsRotaFound) {

            case '/newcorporateuser/' :
                    /**
                     * NEW REGISTRATION
                     */
                    $endUriPath    = substr($uriPath, -18);
                    if( $endUriPath == $vsRotaFound )
                    {
                        // Verifica se o plugin está enabled
                        $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                        if($vbPluginIsEnabled===false) return '';

                        // apresentar o formulário se não tiver nenhum token/captcha associado
                        $layout = new JLayoutFile('newregistration', $this->pathLayouts);
                        $data['aaa'] = 'bbb';
                        $html = $layout->render($data);
                        return $html;
                    }
                break;

            case '/forgotpassword/' :
                /**
                 * FORGOT PASSWORD
                 */
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('forgotpassword', $this->pathLayouts);
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/novopromotor/' :
                /**
                 * NEW REGISTRATION
                 */
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    // Verifica se o plugin está enabled
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    // apresentar o formulário se não tiver nenhum token/captcha associado
                    $layout = new JLayoutFile('novopromotor', $this->pathLayouts);
                    $data   = array();
                    $html = $layout->render($data);
                    return $html;
                }
                break;

            case '/recuperacaosenha/' :
                /**
                 * FORGOT PASSWORD
                 */
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('recuperacaosenha', $this->pathLayouts);
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/uploadfoto/' :
                /**
                 * FORGOT PASSWORD
                 */
                $endUriPath    = substr($uriPath, -12);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('uploadfoto', $this->pathLayouts);
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            /*___________________________________  Simulador Taxas  ___________________________________________*/

            case '/simuladortaxas/' :
                /**
                 * FORGOT PASSWORD
                 */
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('simuladortaxas', $this->pathLayouts . DIRECTORY_SEPARATOR .'/SimuladorTaxas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            /*___________________________________  Festas  ___________________________________________*/

            case '/tickerfestas/' :
                /**
                 * FORGOT PASSWORD
                 */
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('tickerfestas', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Festas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            /*___________________________________  Bolsa Emprego  ___________________________________________*/


            case '/tickerEmprego/' :
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('tickerEmprego', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Emprego');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/ofertasEmprego/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('ofertasEmprego', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Emprego');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/detalheEmprego/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('detalheEmprego', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Emprego');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/novaOferta/' :
                $endUriPath    = substr($uriPath, -12);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('novaOferta', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Emprego');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            /*_______________________________________  Interface Government  _____________________________________________*/

            case '/faleConnoscoGovernment/' :
                $endUriPath    = substr($uriPath, -24);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('faleConnoscoGovernment', $this->pathLayouts . DIRECTORY_SEPARATOR .'/interfaceGovernment');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/faleConnoscoGovernment/dl/' :
                /**
                 * Download Ficheiros do Alerta Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -27);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteGovernmentFaleConnoscoFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/interfaceGovernemt/virtualdesksite_FaleConnosco_files.php');
                    $objAlertFile = new VirtualDeskSiteGovernmentFaleConnoscoFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


             /*_______________________________________  ALERTAR.PT  _____________________________________________*/

            case '/faleConnosco/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('faleConnosco', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlertarPT');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/concelhosName/' :
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('concelhosName', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlertarPT');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/detalheConcelho/' :
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('detalheConcelho', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlertarPT');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/novaOcorrenciaAlertarPT/' :
                $endUriPath    = substr($uriPath, -25);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('novaOcorrenciaAlertarPT', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlertarPT');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;



            case '/menuAppAlertarPT/' :
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('menuAppAlertarPT', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlertarPT');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/alertarPTOcorrencias/dl/' :
                /**
                 * Download Ficheiros do Alerta Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -25);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteAlertarPTNovaOcorrenciaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AlertarPT/virtualdesksite_AlertarPT_novaOcorrencia_files.php');
                    $objAlertFile = new VirtualDeskSiteAlertarPTNovaOcorrenciaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/faleConnosco/dl/' :
                /**
                 * Download Ficheiros do Alerta Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteAlertarPTFaleConnoscoFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AlertarPT/virtualdesksite_AlertarPT_FaleConnosco_files.php');
                    $objAlertFile = new VirtualDeskSiteAlertarPTNovaOcorrenciaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/listaConcFregs/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaConcFregs', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlertarPT');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;




            /*___________________________________  Alojamento Local  ___________________________________________*/


            case '/listaAlojamento/' :
                /**
                 * Nova Empresa
                 */
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaAlojamento', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlojamentoLocal');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/cleanSafe/' :
                $endUriPath    = substr($uriPath, -11);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('cleanSafe', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AlojamentoLocal');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;



            /*___________________________________  Cinema Italiano  ___________________________________________*/


            case '/cinemaItaliano/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('cinemaItaliano', $this->pathLayouts . DIRECTORY_SEPARATOR .'/cinemaItaliano');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            /*___________________________________  Empresas  ___________________________________________*/

            case '/registoEmpresas/' :
                /**
                 * Nova Empresa
                 */
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('registo', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/listacategoriasEmpresas/' :
                /**
                 * Lista Categorias
                 */
                $endUriPath    = substr($uriPath, -25);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaCategorias', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/listaEmpresas/' :
                /**
                 * Lista Empresas
                 */
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaEmpresas', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/detalheEmpresas/' :
                /**
                 * Lista Empresas
                 */
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('detalheEmpresas', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/filtroFP/' :
                /**
                 * Lista Empresas
                 */
                $endUriPath    = substr($uriPath, -10);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('filtroFPEmpresas', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/contador/' :
                /**
                 * Lista Empresas
                 */
                $endUriPath    = substr($uriPath, -10);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('contador', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/totalEmpresas/' :
                /**
                 * Lista Empresas
                 */
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('totalEmpresas', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/listaSubcatConstrucao/' :
                /**
                 * Lista Categorias
                 */
                $endUriPath    = substr($uriPath, -23);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaSubCatConstrucao', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Empresas');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/empresafile/dl/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteEmpresasFilesHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_empresas_files.php');
                    $objAlertFile = new VirtualDeskSiteEmpresasFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/empresalogo/dl/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteEmpresasLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_logos_files.php');
                    $objAlertFile = new VirtualDeskSiteEmpresasLogosFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            /*___________________________________  Alerts  ___________________________________________*/

            case '/novoalerta/' :
                /**
                 * Novo Alerta
                 */
                $endUriPath    = substr($uriPath, -12);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alerta_form', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Alerta');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/ultimasocorrencias/' :
                /**
                 * Ultimas Ocorrências
                 */
                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alerta_ultimasOcorrencias', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Alerta');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/sliderocorrencias/' :
                /**
                 * Slider Ultimas Ocorrências
                 */
                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alerta_slideOcorrencias', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Alerta');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/alerta_FP_Pesquisa/' :
                /**
                 * Slider Ultimas Ocorrências
                 */
                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alerta_FP_Pesquisa', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Alerta');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/formpesquisa/' :

                 $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alerta_formPesquisar', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Alerta');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/pesquisa/' :
                /**
                 * Pesquisa
                 */
                $endUriPath    = substr($uriPath, -10);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alerta_pesquisa', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Alerta');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/historico/' :
                /**
                 * Historico
                 */
                $endUriPath    = substr($uriPath, -11);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alerta_historico', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Alerta');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/alertfile/dl/' :
                /**
                 * Download Ficheiros do Alerta Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteAlertsFilesHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_alerts_files.php');
                    $objAlertFile = new VirtualDeskSiteAlertsFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
             break;


            /*___________________________________  Água e Ambiente ___________________________________________*/


            case '/comunicarLeitura/' :
                /**
                 * Comunicar Leitura
                 */

                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Agua_ComunicarLeitura', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/novoConsumidor/' :
                /**
                 * Registo APP
                 */
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('novoConsumidor', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/validaLeituras/' :
                /**
                 * Valida Leituras
                 */
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Agua_validaLeituras', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/enviaLeitura/' :
                /**
                 * Envia Leituras
                 */
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Agua_validaLeiturasAPP', $this->pathLayouts);
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/leiturasEnviadas/' :
                /**
                 *  Leituras Enviadas
                 */
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Agua_historicoleiturasAPP', $this->pathLayouts);
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/adesaocontrato/' :

                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('novoContrato', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/alteracaocontrato/' :

                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('alteracaoContrato', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/remocaoVeiculos/' :

                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('remocaoVeiculos', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/remocaoVeiculos/dl/' :

                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteRemocaoVeiculosFilesHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_remocaoVeiculos_files.php');
                    $objAlertFile = new VirtualDeskSiteRemocaoVeiculosFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            case '/corteArvores/' :

                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('corteArvores', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/corteArvores/dl/' :

                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteCorteArvoresFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_corteArvores_files.php');
                    $objAlertFile = new VirtualDeskSiteCorteArvoresFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            case '/limpezaTerrenos/' :

                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('limpezaTerrenos', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/limpezaTerrenos/dl/' :

                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteLimpezaTerrenosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_limpezaTerrenos_files.php');
                    $objAlertFile = new VirtualDeskSiteLimpezaTerrenosFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            case '/limpezaUrbana/' :

                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('limpezaUrbana', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/limpezaUrbana/dl/' :

                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteLimpezaUrbanaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_limpezaUrbana_files.php');
                    $objAlertFile = new VirtualDeskSiteLimpezaUrbanaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            case '/cemiterios/' :

                $endUriPath    = substr($uriPath, -12);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('cemiterios', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/queimadas/' :

                $endUriPath    = substr($uriPath, -11);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('queimadas', $this->pathLayouts . DIRECTORY_SEPARATOR .'/AguaAmbiente');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            case '/queimadas/dl/' :

                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteQueimadasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_queimadas_files.php');
                    $objAlertFile = new VirtualDeskSiteLimpezaTerrenosFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            /*___________________________________  ORÇAMENTO PARTICIPATIVO  ___________________________________________*/

            case '/novaproposta/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('OP_submeteProposta', $this->pathLayouts);
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            /*___________________________________  CULTURA  ___________________________________________*/


            case '/listacategorias/' :
                /**
                 * Lista de Categorias
                 */
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_listaCategorias', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/tickereventos/' :
                /**
                 * Newsticker eventos
                 */
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_tickerEventos', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;



            case '/submeterevento/' :
                /**
                 * Submeter Evento
                 */
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_submeterEvento', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/novoevento/' :
                /**
                 * Novo Evento
                 */
                $endUriPath    = substr($uriPath, -12);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_NovoEvento', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/totalEventos/' :
                /**
                 * Novo Evento
                 */
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_TotalEventos', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/listaEventosFP/' :
                /**
                 * Lista Eventos FP
                 */
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_EventosFP', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/detalheEventos/' :
                /**
                 * Detalhe Evento
                 */
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_EventosDetalhe', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/filtroEventos/' :
                /**
                 * Filtro Evento
                 */
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_FiltroEventos', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/filtroEventosFP/' :
                /**
                 * Filtro Evento
                 */
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_FiltroEventosFP', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/eventfile/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteCulturaFilesHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_cultura_files.php');
                    $objAlertFile = new VirtualDeskSiteCulturaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/eventoCheckNIFPromotor/' :
                /**
                * Verifica se o NIF de um promotor já existe... se já existir, deixa
                */
                $endUriPath = substr($uriPath, -24);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    $jinput   = JFactory::getApplication()->input;
                    $paramNIF = $jinput->get('n','');

                    $data = array('nifExiste'=>0);
                    if(strlen($paramNIF)==9) {
                        $nifExiste = VirtualDeskSiteCulturaHelper::getNIF($paramNIF);
                        if(!empty($nifExiste)) {
                            if ((int)$nifExiste[0]['nif'] !== (int)$paramNIF ) {
                                $data = array('nifExiste'=>0);
                            }
                            else {
                                $data = array('nifExiste'=>1);
                            }
                        }
                    }

                    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
                    $obParam              = new VirtualDeskSiteParamsHelper();
                    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
                    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

                    echo json_encode($data);
                    exit();
                }
                break;


            case '/recuperarpassword/' :
                /**
                 * Recuperar Password
                 */
                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_forgotPassword', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;


            case '/registarutilizador/' :
                /**
                 * Novo Utilizador
                 */
                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Cultura_novoUtilizador', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Cultura');
                    $data   = array();
                    $html   = $layout->render($data);
                    return $html;
                }
                break;

            /*___________________________________  Associativismo  ___________________________________________*/

            case '/registoAssociativismo/' :
                $endUriPath    = substr($uriPath, -23);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('Registo', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Associativismo');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/listaCatAssociativismo/' :
                $endUriPath    = substr($uriPath, -24);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaCategorias', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Associativismo');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/eventosFPAssociativismo/' :
                $endUriPath    = substr($uriPath, -25);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('EventosFP', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Associativismo');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/associativismofile/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -23);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteAssociativismoFilesHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_associativismo_files.php');
                    $objAlertFile = new VirtualDeskSiteAssociativismoFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/filtroAssociacoes/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('filtroAssociacoes', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Associativismo');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/detalheAssociacoes/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('detalheAssociacoes', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Associativismo');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/tickerAssociacoes/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('tickerAssociacoes', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Associativismo');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/listaEventosAssociacoes/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -25);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaEventosAssociacoes', $this->pathLayouts . DIRECTORY_SEPARATOR .'/Associativismo');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;



            /*___________________________________  Vila Natal  ___________________________________________*/

            case '/novaempresa/' :
                /**
                 * Registar Nova Empresa
                 */
                $endUriPath    = substr($uriPath, -13);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('NovaEmpresa', $this->pathLayouts . DIRECTORY_SEPARATOR . '/VilaNatal');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;



            /*___________________________________  V-Agenda  ___________________________________________*/

            case '/anunciarEvento/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('anunciarEvento', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/anunciarEvento/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
                    $objAlertFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


        case '/anunciarEventoCartaz/dl/' :
            /**
             * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
             */
            $endUriPath    = substr($uriPath, -25);
            if( $endUriPath == $vsRotaFound )
            {
                $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                if($vbPluginIsEnabled===false) return '';

                // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                $this->setURIVarsInApp();

                JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');
                $objAlertFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                $objAlertFile->downloadWithNoSession();

                exit();
            }
            break;


        case '/patrocinadorLogo/dl/' :
            /**
             * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
             */
            $endUriPath    = substr($uriPath, -21);
            if( $endUriPath == $vsRotaFound )
            {
                $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                if($vbPluginIsEnabled===false) return '';

                // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                $this->setURIVarsInApp();

                JLoader::register('VirtualDeskSiteVAgendaPatrocinadoresLogosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_patrocinadores_logos.php');
                $objAlertFile = new VirtualDeskSiteVAgendaPatrocinadoresLogosHelper();
                $objAlertFile->downloadWithNoSession();

                exit();
            }
            break;

            case '/anunciarEventoGaleria/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -26);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
                    $objAlertFile = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/anunciarEventoPrecario/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -27);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteVAgendaPrecarioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_precario_files.php');
                    $objAlertFile = new VirtualDeskSiteVAgendaPrecarioFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            case '/topEventos/' :
                $endUriPath    = substr($uriPath, -12);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('topEventos', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/proximosEventos/' :
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('proximosEventos', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/listaCatsSubCats/' :
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaCatsSubCats', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/listaEventos/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaEventos', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/pesquisarvagenda/' :
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('pesquisarvagenda', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/ticker_V_agenda/' :
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('ticker_V_agenda', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/sliderGaleria/' :
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('sliderGaleria', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/estouComVontade/' :
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('estouComVontade', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/historicoEventos/' :
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('historicoEventos', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/pagDetalheEvento/' :
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('pagDetalheEvento', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/infoPromotor/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('infoPromotor', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/eventosRelacionadosPromotor/' :
                $endUriPath    = substr($uriPath, -29);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('eventosRelacionadosPromotor', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/agendaMunicipal/' :
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('agendaMunicipal', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/mes_a_mes/' :
                $endUriPath    = substr($uriPath, -11);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('mes_a_mes', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/vAgendaEmpresasCapa/dl/' :

                $endUriPath    = substr($uriPath, -24);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteVAgendaEmpresasFotoCapaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_capa_files.php');
                    $objAlertFile = new VirtualDeskSiteVAgendaEmpresasFotoCapaHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/vAgendaEmpresasLogo/dl/' :

                $endUriPath    = substr($uriPath, -24);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteVAgendaEmpresasLogosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_logos_files.php');
                    $objAlertFile = new VirtualDeskSiteVAgendaEmpresasLogosHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/vAgendaEmpresasGaleria/dl/' :

                $endUriPath    = substr($uriPath, -27);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteVAgendaEmpresasGaleriaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_galeria_files.php');
                    $objAlertFile = new VirtualDeskSiteVAgendaEmpresasGaleriaHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/listaVotacao/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listaVotacao', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/enviaVotacao/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('enviaVotacao', $this->pathLayouts . DIRECTORY_SEPARATOR . '/v_agenda');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            /*_____________________________________  Multimédia  _____________________________________________*/


            case '/multimediaFP/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('multimediaFP', $this->pathLayouts . DIRECTORY_SEPARATOR . '/multimedia');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/listMultimedia/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('listMultimedia', $this->pathLayouts . DIRECTORY_SEPARATOR . '/multimedia');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/multimediaCapa/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteMultimediaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/multimedia/fotoCapa.php');
                    $objAlertFile = new VirtualDeskSiteMultimediaCapaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            /*___________________________________  Europa Criativa  ___________________________________________*/



            case '/filtroEuropa/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('filtroEuropa', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;



            case '/novoPrograma/' :
                $endUriPath    = substr($uriPath, -14);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('novoPrograma', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/novoPrograma/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteFormularioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/Formulario_files.php');
                    $objAlertFile = new VirtualDeskSiteFormularioFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            case '/filtroFPEuropa/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('filtroFPEuropa', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/detalheEuropa/' :
                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('detalheEuropa', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/detalheEuropa/dl/' :
                /**
                 * Download Ficheiros do Alerta Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -18);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteFormularioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/Formulario_files.php');
                    $objAlertFile = new VirtualDeskSiteFormularioFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;

            case '/contadorProjetosMedia/' :
                $endUriPath    = substr($uriPath, -23);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('contadorProjetosMedia', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/contadorEntidadesCultura/' :
                $endUriPath    = substr($uriPath, -26);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('contadorEntidadesCultura', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/financiamentoCultura/' :
                $endUriPath    = substr($uriPath, -22);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('financiamentoCultura', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/financiamentoMedia/' :
                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('financiamentoMedia', $this->pathLayouts . DIRECTORY_SEPARATOR . '/EuropaCriativa');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            /*__________________________________   Madeira Magazine   ____________________________________*/


            case '/MadeiraMagazine_FiltroFP/' :
                $endUriPath    = substr($uriPath, -26);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('MadeiraMagazine_FiltroFP', $this->pathLayouts . DIRECTORY_SEPARATOR . '/MadeiraMagazine');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;



            case '/MadeiraMagazine_ondeFicarFiltroFP/' :
                $endUriPath    = substr($uriPath, -35);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('MadeiraMagazine_ondeFicarFiltroFP', $this->pathLayouts . DIRECTORY_SEPARATOR . '/MadeiraMagazine');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/MadeiraMagazine_filtroPercursosPedestres/' :
                $endUriPath    = substr($uriPath, -42);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('MadeiraMagazine_filtroPercursosPedestres', $this->pathLayouts . DIRECTORY_SEPARATOR . '/MadeiraMagazine');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/MadeiraMagazine_detalhePercursosPedestres/' :
                $endUriPath    = substr($uriPath, -43);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('MadeiraMagazine_detalhePercursosPedestres', $this->pathLayouts . DIRECTORY_SEPARATOR . '/MadeiraMagazine');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/MadeiraMagazine_descricaoPercursosPedestres/' :
                $endUriPath    = substr($uriPath, -45);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('MadeiraMagazine_descricaoPercursosPedestres', $this->pathLayouts . DIRECTORY_SEPARATOR . '/MadeiraMagazine');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/MadeiraMagazine_detalheParte2PercursosPedestres/' :
                $endUriPath    = substr($uriPath, -49);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('MadeiraMagazine_detalheParte2PercursosPedestres', $this->pathLayouts . DIRECTORY_SEPARATOR . '/MadeiraMagazine');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            case '/MadeiraMagazine_patrocinadorPercursosPedestres/' :
                $endUriPath    = substr($uriPath, -48);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('MadeiraMagazine_patrocinadorPercursosPedestres', $this->pathLayouts . DIRECTORY_SEPARATOR . '/MadeiraMagazine');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


                /*________________________________________Percursos Pedestres________________________________*/

            case '/percursospedestresGaleria/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -30);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSitePercursosPedestresGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/galeria_files.php');
                    $objAlertFile = new VirtualDeskSitePercursosPedestresGaleriaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/percursospedestresCapa/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -27);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSitePercursosPedestresCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/fotoCapa_files.php');
                    $objAlertFile = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/percursospedestresGPX/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -26);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSitePercursosPedestresGPXFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/gpx_files.php');
                    $objAlertFile = new VirtualDeskSitePercursosPedestresGPXFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/percursospedestresKML/dl/' :
                /**
                 * Download Ficheiros do Eventos/Cultura Sem Utilizar a Sessão na App (link com verificação)
                 */
                $endUriPath    = substr($uriPath, -26);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSitePercursosPedestresKMLFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/kml_files.php');
                    $objAlertFile = new VirtualDeskSitePercursosPedestresKMLFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            /*________________________________________Patrocinadores________________________________*/



            case '/patrocinadoresLogo/dl/' :

                $endUriPath    = substr($uriPath, -23);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSitePatrocinadoresLogoFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/patrocinadores/logo_files.php');
                    $objAlertFile = new VirtualDeskSitePatrocinadoresLogoFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


                /*___________________________________  Notícias  ___________________________________________*/


            case '/noticiasFP/' :
                $endUriPath    = substr($uriPath, -12);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('noticiasFP', $this->pathLayouts . DIRECTORY_SEPARATOR . '/Noticias');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;



                case '/noticias/dl/' :

                    $endUriPath    = substr($uriPath, -13);
                    if( $endUriPath == $vsRotaFound )
                    {
                        $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                        if($vbPluginIsEnabled===false) return '';

                        // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                        $this->setURIVarsInApp();

                        JLoader::register('VirtualDeskSiteNoticiasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias_fotoCapa_files.php');
                        $objAlertFile = new VirtualDeskSiteNoticiasFilesHelper();
                        $objAlertFile->downloadWithNoSession();

                        exit();
                    }
                    break;


                case '/noticiasGaleria/dl/' :

                    $endUriPath    = substr($uriPath, -20);
                    if( $endUriPath == $vsRotaFound )
                    {
                        $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                        if($vbPluginIsEnabled===false) return '';

                        // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                        $this->setURIVarsInApp();

                        JLoader::register('VirtualDeskSiteNoticiasGaleriaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias_galeria_files.php');
                        $objAlertFile = new VirtualDeskSiteNoticiasGaleriaHelper();
                        $objAlertFile->downloadWithNoSession();

                        exit();
                    }
                    break;

            case '/noticiasDetalhe/' :
                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('noticiasDetalhe', $this->pathLayouts . DIRECTORY_SEPARATOR . '/Noticias');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/noticiasFiltro/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('noticiasFiltro', $this->pathLayouts . DIRECTORY_SEPARATOR . '/Noticias');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;

            case '/noticiasTicker/' :
                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('noticiasTicker', $this->pathLayouts . DIRECTORY_SEPARATOR . '/Noticias');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            /*________________________________________Documentos________________________________*/



            case '/documentos/dl/' :

                $endUriPath    = substr($uriPath, -15);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteDocumentosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/files.php');
                    $objAlertFile = new VirtualDeskSiteDocumentosFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;



            case '/documentosTree/' :

                $endUriPath    = substr($uriPath, -16);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('documentosTree', $this->pathLayouts . DIRECTORY_SEPARATOR . '/documentos');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;



            case '/documentosByCat/' :

                $endUriPath    = substr($uriPath, -17);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('documentosByCat', $this->pathLayouts . DIRECTORY_SEPARATOR . '/documentos');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;



            case '/documentosContent/' :

                $endUriPath    = substr($uriPath, -19);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('documentosContent', $this->pathLayouts . DIRECTORY_SEPARATOR . '/documentos');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;


            /*________________________________________ Diretorio de servicos ________________________________*/



            case '/diretorioservicosCapa/dl/' :

                $endUriPath    = substr($uriPath, -26);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteDiretorioServicosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_capa_files.php');
                    $objAlertFile = new VirtualDeskSiteDiretorioServicosFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/diretorioservicosLogo/dl/' :

                $endUriPath    = substr($uriPath, -26);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteDiretorioServicosLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_logos_files.php');
                    $objAlertFile = new VirtualDeskSiteDiretorioServicosLogosFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;


            case '/diretorioservicosGaleria/dl/' :

                $endUriPath    = substr($uriPath, -29);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    // Força as variáveis no JInput se houver encriptação no URL. No plugin foi necessário.
                    $this->setURIVarsInApp();

                    JLoader::register('VirtualDeskSiteDiretorioServicosGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_galeria_files.php');
                    $objAlertFile = new VirtualDeskSiteDiretorioServicosGaleriaFilesHelper();
                    $objAlertFile->downloadWithNoSession();

                    exit();
                }
                break;



                /*___________________________________  Geral  ___________________________________________*/



            case '/updateduserprofile/' :
                $endUriPath    = substr($uriPath, -20);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $app = JFactory::getApplication();

                    JLoader::register('VirtualDeskUserActivationHelper',JPATH_ADMINISTRATOR.'/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
                    VirtualDeskUserActivationHelper::loadLanguageFiles();

                    $redirect = JUri::root();
                    $app->enqueueMessage(JText::_('COM_VIRTUALDESK_PROFILE_SAVE_SUCCESS_NEED_TO_VALIDATE'),'success');
                    $app->redirect(JRoute::_($redirect, false));
                    exit();
                }
                break;


            case '/onAjaxVD/' :
                /** Ajax request
                 */
                $endUriPath    = substr($uriPath, -10);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(trim($vsRotaFound, '/'));
                    if($vbPluginIsEnabled===false) return '';

                    $jinput = JFactory::getApplication()->input;
                    $method = $jinput->get('m','');
                    $data ='';

                    switch($method)
                    {   case 'alerta_getsubcat':
                            JLoader::register('VirtualDeskSiteAlertaHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteAlertaHelper::getAlertaSubcategoria($idwebsitelist);
                            break;

                        case 'cultura_getfreg':
                            JLoader::register('VirtualDeskSiteCulturaHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteCulturaHelper::getCulturaFreguesia($idwebsitelist);
                            break;

                        case 'empresas_getcat':
                            JLoader::register('VirtualDeskSiteEmpresasHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_empresas.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteEmpresasHelper::getSubCategoria($idwebsitelist);
                            break;

                        case 'associativismo_getcat':
                            JLoader::register('VirtualDeskSiteAssociativismoHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_associativismo.php');
                            $idwebsitelist = $jinput->get('areaAtuacao',1);
                            $data = VirtualDeskSiteAssociativismoHelper::getSubAreasAtuacaoDepend($idwebsitelist);
                            break;

                        case 'aguaAmbiente_getconc':
                            JLoader::register('VirtualDeskSiteRemocaoVeiculosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_remocaoVeiculos.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteRemocaoVeiculosHelper::getFregReq($idwebsitelist);
                            break;

                        case 'corteArvores_getconc':
                            JLoader::register('VirtualDeskSiteCorteArvoresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_corteArvores.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteCorteArvoresHelper::getFregReq($idwebsitelist);
                            break;

                        case 'limpezaTerrenos_getconc':
                            JLoader::register('VirtualDeskSiteLimpezaTerrenosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_limpezaTerrenos.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteLimpezaTerrenosHelper::getFregReq($idwebsitelist);
                            break;

                        case 'limpezaUrbana_getconc':
                            JLoader::register('VirtualDeskSiteLimpezaUrbanaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_limpezaUrbana.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteLimpezaUrbanaHelper::getFregReq($idwebsitelist);
                            break;

                        case 'cemiterios_getconc':
                            JLoader::register('VirtualDeskSiteCemiteriosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_cemiterios.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteCemiteriosHelper::getFregReq($idwebsitelist);
                            break;

                        case 'queimadas_getconc':
                            JLoader::register('VirtualDeskSiteQueimadasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_queimadas.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteQueimadasHelper::getFregReq($idwebsitelist);
                            break;

                        case 'anunciarEvento_getcat':
                            JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteVAgendaHelper::getSubCat($idwebsitelist);
                            break;

                        case 'anunciarEvento_getfreg':
                            JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteVAgendaHelper::getFreg($idwebsitelist);
                            break;

                        case 'europa_getconc':
                            JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
                            $idwebsitelist = $jinput->get('distrito',1);
                            $data = VirtualDeskSiteFiltroHelper::getConcelhos($idwebsitelist);
                            break;

                        case 'europa_getlinha':
                            JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
                            $idwebsitelist = $jinput->get('programa',1);
                            $data = VirtualDeskSiteFiltroHelper::getLinhasFinanciamento($idwebsitelist);
                            break;

                        case 'europa_getlinhabyCat':
                            JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteFiltroHelper::getLinhasFinanciamentoByCat($idwebsitelist);
                            break;

                        case 'europa_getcatByLinha':
                            JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
                            $idwebsitelist = $jinput->get('linhaFinanciamento',1);
                            $idCategoria = VirtualDeskSiteFiltroHelper::getIDCategoriaApoio($idwebsitelist);
                            $data = VirtualDeskSiteFiltroHelper::getCategoriaApoio($idCategoria);
                            break;

                        case 'europa_getcategoria':
                            JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
                            $data = VirtualDeskSiteFiltroHelper::getCategoria();
                            break;

                        case 'europa_getprogramas':
                            JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
                            $idwebsitelist = $jinput->get('programa',1);
                            $data = VirtualDeskSiteFiltroHelper::getProgramaAJAX();
                            break;

                        case 'europa_getAno':
                            JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
                            $idwebsitelist = $jinput->get('ano',1);
                            $data = VirtualDeskSiteFiltroHelper::getAnoAJAX();
                            break;

                        case 'agenda_getsubcat':
                            JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteVAgendaHelper::getSubCat($idwebsitelist);
                            break;

                        case 'agenda_getfreg':
                            JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteVAgendaHelper::getFreg($idwebsitelist);
                            break;

                        case 'agenda_getfregPlugin':
                            JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteVAgendaHelper::getFregPlugin($idwebsitelist);
                            break;

                        case 'agenda_getidConc':
                            JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteVAgendaHelper::getDelimitacaoMapa($idwebsitelist);
                            break;

                        case 'agenda_duplicaevento':
                            JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
                            $idwebsitelist = $jinput->get('evento',1);
                            $dadosEvento = VirtualDeskSiteVAgendaHelper::DuplicaEvento($idwebsitelist);

                            foreach($dadosEvento as $rowWSL) :
                                $id =                       $rowWSL['id'];
                                $nif =                      $rowWSL['nif'];
                                $categoria =                $rowWSL['categoria'];
                                $subcategoria =             $rowWSL['subcategoria'];
                                $nome_evento =              $rowWSL['nome_evento'];
                                $descricao_evento =         $rowWSL['descricao_evento'];
                                $data_inicio =              $rowWSL['data_inicio'];
                                $ano_inicio =               $rowWSL['ano_inicio'];
                                $mes_inicio =               $rowWSL['mes_inicio'];
                                $data_fim =                 $rowWSL['data_fim'];
                                $ano_fim =                  $rowWSL['ano_fim'];
                                $mes_fim =                  $rowWSL['mes_fim'];
                                $hora_inicio =              $rowWSL['hora_inicio'];
                                $hora_fim =                 $rowWSL['hora_fim'];
                                $local_evento =             $rowWSL['local_evento'];
                                $distrito =                 $rowWSL['distrito'];
                                $concelho =                 $rowWSL['concelho'];
                                $freguesia =                $rowWSL['freguesia'];
                                $latitude =                 $rowWSL['latitude'];
                                $longitude =                $rowWSL['longitude'];
                                $tipo_evento =              $rowWSL['tipo_evento'];
                                $observacoes_precario =     $rowWSL['observacoes_precario'];
                                $facebook =                 $rowWSL['facebook'];
                                $instagram =                $rowWSL['instagram'];
                                $youtube =                  $rowWSL['youtube'];
                                $vimeo =                    $rowWSL['vimeo'];
                                $videosEvento =             $rowWSL['videosEvento'];
                                $catVideosEvento =          $rowWSL['catVideosEvento'];
                            endforeach;


                            /*Gerar Referencia UNICA de alerta*/
                            $refExiste = 1;

                            $refCat          = VirtualDeskSiteVAgendaHelper::getRefCat($categoria);
                            $splitDataInicio = explode("-", $data_inicio);
                            $splitDataFim    = explode("-", $data_fim);
                            $anoInicio       = $splitDataInicio[0];
                            $mesInicio       = $splitDataInicio[1];
                            $anoFim          = $splitDataFim[0];
                            $mesFim          = $splitDataFim[1];


                            $referencia = '';
                            while($refExiste == 1){
                                $refRandom       = VirtualDeskSiteVAgendaHelper::random_code();
                                $referencia = $refCat . $refRandom . $nif . $anoInicio . $mesInicio . $anoFim . $mesFim;
                                $checkREF = VirtualDeskSiteVAgendaHelper::CheckReferencia($referencia);
                                if((int)$checkREF == 0){
                                    $refExiste = 0;
                                }
                            }

                            $nomeEventoDuplicado = $nome_evento . '(2)';

                            VirtualDeskSiteVAgendaHelper::saveNewEvento($referencia, $nif, $nomeEventoDuplicado, $categoria, $subcategoria, $descricao_evento, $data_inicio, $ano_inicio, $mes_inicio, $hora_inicio, $data_fim, $ano_fim, $mes_fim, $hora_fim, $distrito, $concelho, $freguesia, $local_evento, $latitude, $longitude, $tipo_evento, $observacoes_precario, $facebook, $instagram, $youtube, $vimeo, $videosEvento, $catVideosEvento, $balcao=0);

                            break;

                        case 'alertarPT_getIdClicked':
                            JLoader::register('VirtualDeskSiteAlertarPTHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AlertarPT/virtualdesksite_AlertarPT.php');
                            $idwebsitelist = $jinput->get('id',1);
                            $data = VirtualDeskSiteAlertarPTHelper::getIdClicked($idwebsitelist);
                            break;

                        case 'alertarPT_getsubcat':
                            JLoader::register('VirtualDeskSiteAlertarPTNovaOcorrenciaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AlertarPT/virtualdesksite_AlertarPT_novaOcorrencia.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getSubcategoria($idwebsitelist);
                            break;

                        case 'alertarPT_getfreg':
                            JLoader::register('VirtualDeskSiteAlertarPTNovaOcorrenciaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AlertarPT/virtualdesksite_AlertarPT_novaOcorrencia.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getFreguesias($idwebsitelist);
                            break;

                        case 'alertarPT_getAssunto':
                            JLoader::register('VirtualDeskSiteAlertarPTHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AlertarPT/virtualdesksite_AlertarPT.php');
                            $idwebsitelist = $jinput->get('tipologia',1);
                            $data = VirtualDeskSiteAlertarPTHelper::getAssunto($idwebsitelist);
                            break;

                        case 'government_getAssunto':
                            JLoader::register('VirtualDeskSiteGovernmentFaleConnoscoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/interfaceGovernemt/virtualdesksite_faleConnosco.php');
                            $idwebsitelist = $jinput->get('tipologia',1);
                            $data = VirtualDeskSiteGovernmentFaleConnoscoHelper::getAssunto($idwebsitelist);
                            break;

                        case 'percursospedestres_getconcelho':
                            JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');
                            $idwebsitelist = $jinput->get('distrito',1);
                            $data = VirtualDeskSitePercursosPedestresHelper::getConcelho($idwebsitelist);
                            break;

                        case 'percursospedestres_getfreguesia':
                            JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSitePercursosPedestresHelper::getFreguesia($idwebsitelist);
                            break;

                        case 'noticias_duplicanoticia':
                            JLoader::register('VirtualDeskSiteNoticiasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias.php');
                            $idwebsitelist = $jinput->get('noticiaId',1);
                            $dadosNoticia = VirtualDeskSiteNoticiasHelper::DuplicaNoticia($idwebsitelist);

                            foreach($dadosNoticia as $rowWSL) :
                                $titulo =           $rowWSL['titulo'];
                                $noticia =          $rowWSL['noticia'];
                                $categoria =        $rowWSL['categoria'];
                                $facebook =         $rowWSL['facebook'];
                                $video =            $rowWSL['video'];
                                $data_publicacao =  $rowWSL['data_publicacao'];
                            endforeach;


                            /*Gerar Referencia UNICA de alerta*/
                            $refExiste = 1;

                            $referencia = '';
                            while($refExiste == 1){
                                $referencia = VirtualDeskSiteNoticiasHelper::random_code();
                                $checkREF = VirtualDeskSiteNoticiasHelper::CheckReferencia($referencia);
                                if((int)$checkREF == 0){
                                    $refExiste = 0;
                                }
                            }

                            $nomeNoticia = $titulo . ' (cópia)';

                            VirtualDeskSiteNoticiasHelper::saveNoticiaDuplicada($referencia, $nomeNoticia, $noticia, $categoria, $facebook, $video, $data_publicacao, $balcao=0);

                            break;

                        case 'noticias_novaVisualizacao':
                            JLoader::register('VirtualDeskSiteNoticiasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias.php');
                            $referencia = $jinput->get('refNews',1);
                            $search = VirtualDeskSiteNoticiasHelper::getViews($referencia);
                            foreach($search as $rowWSL) :
                                $id = $rowWSL['id'];
                                $visualizacoes = $rowWSL['visualizacoes'];
                            endforeach;

                            $newVisualizacao = $visualizacoes + 1;

                            VirtualDeskSiteNoticiasHelper::updateViews($newVisualizacao, $id);

                            break;

                        case 'documentos_categoriaLevel1':
                            JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
                            $idwebsitelist = $jinput->get('catLevel1',1);
                            $data = VirtualDeskSiteDocumentosHelper::getCategoria4DocNivel2($idwebsitelist);
                            break;

                        case 'documentos_categoriaLevel2':
                            JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
                            $idwebsitelist = $jinput->get('catLevel2',1);
                            $data = VirtualDeskSiteDocumentosHelper::getCategoria4DocNivel3($idwebsitelist);
                            break;

                        case 'documentos_categoriaLevel2Plugin':
                            JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteDocumentosHelper::getCategoriaNivel3Plugin($idwebsitelist);
                            break;

                        case 'documentos_categoriaLevel3':
                            JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
                            $idwebsitelist = $jinput->get('catLevel3',1);
                            $data = VirtualDeskSiteDocumentosHelper::getCategoria4DocNivel4($idwebsitelist);
                            break;

                        case 'documentos_novaVisualizacao':
                            JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
                            $referencia = $jinput->get('refDoc',1);
                            $search = VirtualDeskSiteDocumentosHelper::getViews($referencia);
                            foreach($search as $rowWSL) :
                                $id = $rowWSL['id'];
                                $visualizacoes = $rowWSL['visualizacoes'];
                            endforeach;

                            $newVisualizacao = $visualizacoes + 1;

                            VirtualDeskSiteDocumentosHelper::updateViews($newVisualizacao, $id);

                            break;

                        case 'agua_novoContador':
                            JLoader::register('VirtualDeskSiteAguaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agua.php');
                            JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
                            $contador = $jinput->get('contador',1);
                            $nif = $jinput->get('nif',1);
                            $contadorExiste = VirtualDeskSiteAguaHelper::contadorExiste($contador);
                            $obParam      = new VirtualDeskSiteParamsHelper();
                            $NumContadorMaxLenght = $obParam->getParamsByTag('NumContadorMaxLenght');

                            if($contador == 0 || $contador == '' || strlen($contador) > $NumContadorMaxLenght || strlen($contador) < 3){
                                print "2";
                            } else if($contadorExiste == 0 || $contadorExiste = '' || empty($contadorExiste)){
                                VirtualDeskSiteAguaHelper::saveNovoContador($contador, $nif);
                                print "1";
                            } else {
                                print "0";
                            }

                            break;

                        case 'diretorioservicos_getfreguesia':
                            JLoader::register('VirtualDeskSiteDiretorioServicosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteDiretorioServicosHelper::getFreguesia($idwebsitelist);
                            break;

                        case 'diretorioservicos_getsubcategoria':
                            JLoader::register('VirtualDeskSiteDiretorioServicosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteDiretorioServicosHelper::getSubcategoria($idwebsitelist);
                            break;

                        case 'v_agenda_empresas_getfreguesia':
                            JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
                            $idwebsitelist = $jinput->get('concelho',1);
                            $data = VirtualDeskSiteAgendaHelper::getFreguesia($idwebsitelist);
                            break;

                        case 'v_agenda_empresas_getsubcategoria':
                            JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
                            $idwebsitelist = $jinput->get('categoria',1);
                            $data = VirtualDeskSiteAgendaHelper::getSubcategoriaEmpresa($idwebsitelist);
                            break;

                        default:
                        break;
                    }

                    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
                    $obParam              = new VirtualDeskSiteParamsHelper();
                    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
                    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);
                    echo json_encode($data);
                    exit();
                }
                break;


            case '/politicaPrivacidade/' :
                /**
                 * Verifica se o NIF de um promotor já existe... se já existir, deixa
                 */
                $endUriPath = substr($uriPath, -21);
                if( $endUriPath == $vsRotaFound )
                {
                    $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled(str_replace('/', '', $vsRotaFound ));
                    if($vbPluginIsEnabled===false) return '';

                    $layout = new JLayoutFile('politicaPrivacidade', $this->pathLayouts . DIRECTORY_SEPARATOR . '/');
                    $data   = array();
                    $html   = $layout->render($data);
                    echo $html;
                    exit();
                }
                break;
        }

    }


    public function checkPluginIsEnabled($tagplugin)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPluginEnabled(false);

        // Verifica se o plugin está enabled
        $vbPluginIsEnabled = $objCheckPerm->checkPluginIsEnabled($tagplugin);
        if($vbPluginIsEnabled===false) return false;
        return (true);
    }


    public function getPluginLayoutAtivePath($tagplugin,$tagtipolayout)
    {
        if( empty($tagplugin) ||  empty($tagtipolayout) )  return false;

        try
        {

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('b.tagchave as tagchave', 'b.path as path'   ))
                ->from("#__virtualdesk_config_plugin as a")
                ->leftJoin("#__virtualdesk_config_plugin_layout as b ON a.id = b.idplugin")
                ->leftJoin("#__virtualdesk_config_plugin_layout_ativos as c ON c.idplugin = a.id  and c.idpluginlayout=b.id")
                ->leftJoin("#__virtualdesk_config_plugin_layout_tipo as d on d.id=c.idtipolayout")
                ->where( $db->quoteName('a.tagchave') . '="' . $db->escape($tagplugin) .'" and ' . $db->quoteName('d.tagchave') . '="' . $db->escape($tagtipolayout).'" and c.enabled=1 ')
            );

            $dataRes = $db->loadObject();

            $dataReturn = $dataRes->path;

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    // No caso dos plugins, havia um problema com a encriptação de variáveis
    // Tivemos de criar esta função que lê a URL Query entretanto colocadda no objeto URI
    // E força a colocação das varáveis da query já desincriptadas no Jinput do Joomla APP
    private function setURIVarsInApp() {
        if ($this->setencrypt_urlqueryparams === "1") {
            $uriQueryArray = array();
            $jinput = JFactory::getApplication()->input;
            $uriQueryStr = $this->objURI->getQuery();
            parse_str($uriQueryStr, $uriQueryArray);

            if (!is_array($uriQueryArray)) $uriQueryArray = array();
            foreach ($uriQueryArray as $key => $var) {
                $jinput->set($key, $var);
            }
        }
    }

}