<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.remember
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

// Import filesystem libraries. Perhaps not necessary, but does not hurt
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Joomla! System VirtualDesk Plugin
 *
 * @since  1.5
 */

class PlgSystemVirtualDesk extends JPlugin
{
    public function __construct(&$subject, $config = array()) {
        parent::__construct($subject, $config);
    }

    /**
	 * Application object.
	 *
	 * @var    JApplicationCms
	 * @since  3.2
	 */
	protected $app;

	/**
	 * VirtualDesk method to run onAfterInitialise
	 * @return  void
	 *
	 * @since   1.5
	 * @throws  InvalidArgumentException
	 */
	public function onAfterInitialise()
    {
        JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');

        // Get the application object.
        $app = JFactory::getApplication();

        // Make sure we are not in the administrator
        if ($app->isAdmin()) return;


        /**
         * Se o site não estiver offline, ntão dá erro e não avança...
         */
        $isSiteOffLine = JFactory::getApplication('site')->getCfg('offline');
        if ((string)$isSiteOffLine != "1") {
            echo("Site must be Offline...");
            exit();
        }


        $uri = JUri::getInstance();
        $uriPath = $uri->getPath();


        /**
         * Verifica se o pedido está codificado no url query params
         */
        if (JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_urlqueryparams') === "1") {
            $uriQueryEncoded = $uri->getvar('vdqs', '');
            if (!empty($uriQueryEncoded)) {

                JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
                $obVDCrypt = new VirtualDeskSiteCryptHelper();
                $obVDCrypt->checkURLParamEncoded();

            }
        }


        /**
         * Verifica se está configurado para encriptar o POST e nesse caso verificar o POST e/ou as vars do app do joomla ?
         */
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if ($setencrypt_forminputhidden === '1') {
            JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
            $obVDCrypt = new VirtualDeskSiteCryptHelper();
            $obVDCrypt->decryptAppPostedInput($setencrypt_forminputhidden);
        }


        /**
         * Plugin:  check if we are receiving an activation request or new registration
         */

        /**
         * ATIVAÇÃO
         */
        $endUriPath = substr($uriPath, -10);
        $uriQueryToken = $uri->getvar('token', '');

        if (($endUriPath == "/activate/") && (string)$uriQueryToken != '') {  //call model to activate the user defined ind the token
            JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
            VirtualDeskUserActivationHelper::loadLanguageFiles();
            $ResultActivation = VirtualDeskUserActivationHelper::checkActivationToken($uriQueryToken);

            if ($ResultActivation === false) return;

            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_USERACTIVATION_SUCCESS'), 'success');
            //$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()));

            //TODO Destroy session if exist
            //TODO verify if token is well formed, if not then send ERROR and register that ERROR+IP
            //TODO check if token is active, if not then send ERROR and register that ERROR+IP
            // tentar ter um helper que faça a gestão / registo de tentativas de activação
            // If everything is ok tehn we send a success messagem, or we send a warning or error messagem
            // As site is offline then
        }


        /**
        * Check if IP is not blocked in last 10 minutes
        */
        $ObjEvtLog       = new VirtualDeskLogHelper();
        $checkIPAuthFail = $ObjEvtLog->checkIPAuthFail();
         if ($checkIPAuthFail === false) {
             echo('<div style="text-align: center"><h3>ATENÇÃO</h3>');
             echo('<h3>Excedeu o máximo de tentativa falhadas.</h3>');
             echo('<h3>O seu IP está neste momento bloqueado.</h3>');
             echo('<h3>Volte a tentar dentro de alguns minutos.</h3></div>');
             exit();
         }


        /**
         * Verifica as várias ROTAS para os plugins e se estão ativos
         */
        $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
        $obCheckPlgEnable->setURIObj($uri);
        $html = $obCheckPlgEnable->checkIfUrlHasPluginRoute($uriPath);
        if ((string)$html !== '' && $html!== true && $html!== false) {
            echo $html;
            exit();
        }

    }

    public function onAfterRoute()
    {
        // Override de CONTROLLERS
        $app = JFactory::getApplication();
        if('com_users' == JRequest::getCMD('option') && !$app->isAdmin())
        {
          require_once(dirname(__FILE__) . '/override/override_users_controller.php');
          require_once(dirname(__FILE__) . '/override/override_users_controllers_profile.php');
          require_once(dirname(__FILE__) . '/override/override_users_controllers_profile_base_json.php');
          //require_once(dirname(__FILE__) . '/override/override_users_controllers_profile.json.php');
          require_once(dirname(__FILE__) . '/override/override_users_controllers_reset.php');
          require_once(dirname(__FILE__) . '/override/override_users_controllers_registration.php');
          require_once(dirname(__FILE__) . '/override/override_users_controllers_remind.php');
        }

        // Override de VIEWS
        JLoader::register('UsersViewProfile', dirname(__FILE__) . '/override/override_users_views_profile.php', true);
    }

    public function onUserAfterLogin()
    {
        JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
        JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
        JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

        /*
        * Carrega as permissões do utilizador logo na entrada da sessão e também os plugins que estão ENABLED
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        // No inicio da sessão carrega sempre da BD, depois utiliza o array na sessão para ser mais rápido
        $objCheckPerm->loadPermissionFromDB(1);
        $objCheckPerm->setPermissionToSession();

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $vdlog = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['iduser']        = VirtualDeskSiteUserHelper::getUserVDIdFromJoomlaID ($UserJoomlaID);
        $eventdata['idjos']         = $UserJoomlaID;
        $eventdata['title']         = '';
        $eventdata['desc']          = '';
        $eventdata['category']      = 'UserLogin';
        $eventdata['sessionuserid'] = JFactory::getUser()->id; // ifz assim para validar com o id anterior... têm de ser sempre iguais
        $vdlog->insertEventLog($eventdata);
    }

    public function onUserLogout()
    {

        JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
        JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
        JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();

        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->destroyPermissionToSession();

        // Foi alterado o Pedido do tipo Contact Us
        $vdlog = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['iduser']        = VirtualDeskSiteUserHelper::getUserVDIdFromJoomlaID ($UserJoomlaID);
        $eventdata['idjos']         = $UserJoomlaID;
        $eventdata['title']         = '';
        $eventdata['desc']          = '';
        $eventdata['category']      = 'UserLogout';
        $eventdata['sessionuserid'] = JFactory::getUser()->id; // ifz assim para validar com o id anterior... têm de ser sempre iguais
        $vdlog->insertEventLog($eventdata);

    }

    public function onUserLoginFailure(){

        JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
        $vdlog = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['request']       = serialize($_REQUEST);
        $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
        $vdlog->insertAuthFailLog($eventdata);
    }


    /*
       * Para aplicar depois da página carregada
       */
    public function onAfterRender()
    {
        // Get the application object.
        $app = JFactory::getApplication();

        // Make sure we are not in the administrator
        if ( $app->isAdmin() ) return;

        // Verifica se é para encryptar a query strinf dos urls "locais" da página atual
        if( JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_urlqueryparams') === '1' )
        {
            JLoader::register('VirtualDeskSiteCryptHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');

            $obVDCrypt = new VirtualDeskSiteCryptHelper();
            //$body = $this->setUrlQueryStringEncrypted();
            $body = $obVDCrypt->setUrlQueryStringEncrypted();

            JFactory::getApplication()->setBody($body);
        }
    }


    public function onAjaxteste()  {

        $app = JFactory::getApplication();


        //echo the data
        //echo json_encode(array('sent' => 1));
    }


    public function teste1()  {

        echo json_encode(array('sent' => 1));
        echo 'teste' . time();
    }


    public static function onAjaxSendMail()
    {
        //Get the app
        $app = JFactory::getApplication();

        $data = "test";

        //echo the data
        echo json_encode($data);

        //close the $app
        $app->close();
    }

}

class plgAjaxMyplugin extends JPlugin
{

    function onAjaxMyplugin()
    {

        $data = array("test");
        return $data;

    }
}




