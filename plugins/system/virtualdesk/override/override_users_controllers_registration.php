<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');

/**
 * Registration controller class for Users.
 *
 * @since  1.6
 */
class UsersControllerRegistration extends UsersController
{
	/**
	 * Method to activate a user.
	 *
	 * @return  boolean  True on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function activate()
	{
        $app         = JFactory::getApplication();

        $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
        $app->setHeader('status', 403, true);

        return false;

	}

	/**
	 * Method to register a user.
	 *
	 * @return  boolean  True on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function register()
	{

        $app         = JFactory::getApplication();

        $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
        $app->setHeader('status', 403, true);

        return false;

	}
}
