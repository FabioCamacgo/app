<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');

/**
 * Reset controller class for Users.
 *
 * @since  1.6
 */
class UsersControllerReset extends UsersController
{
	/**
	 * Method to request a password reset.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function request()
	{
        $app         = JFactory::getApplication();

        $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
        $app->setHeader('status', 403, true);

        return false;

	}

	/**
	 * Method to confirm the password request.
	 *
	 * @return  boolean
	 *
	 * @access	public
	 * @since   1.6
	 */
	public function confirm()
	{
        $app         = JFactory::getApplication();

        $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
        $app->setHeader('status', 403, true);

        return false;
	}

	/**
	 * Method to complete the password reset process.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function complete()
	{
        $app         = JFactory::getApplication();

        $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
        $app->setHeader('status', 403, true);

        return false;
	}
}
