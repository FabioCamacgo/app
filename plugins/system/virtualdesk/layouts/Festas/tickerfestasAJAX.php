<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('tickerfestas');
    if ($resPluginEnabled === false) exit();


    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Festas/breaking-news-ticker.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Festas/breaking-news-ticker.css' . $addcss_end;


    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
    echo $headScripts;

    $dataAtual = date("Y-m-d");
    $horaAtual = date('H:i');
    $ListaCartaz = VirtualDeskSiteFestasHelper::getCartazTicker($dataAtual);
    $numElem = 0;

    ?>
    <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="example">
        <div class="bn-news">
            <ul>
                <?php foreach($ListaCartaz as $row) :
                    if($numElem < 10) {
                        $data = $row['data'];
                        $hora = $row['hora'];
                        $evento = $row['evento'];
                        $id = $row['id'];
                        $pieces = explode("-", $data);
                        $mes = VirtualDeskSiteFestasHelper::getAbvMes($pieces[1]);
                        ?>
                        <li>
                            <div class="data">
                                <div class="dia"><?php echo $pieces[2]; ?></div>
                                <div class="mes"><?php echo $mes; ?></div>
                            </div>
                            <div class="evento">
                                <span class="hora"><?php echo $hora; ?></span> - <span
                                        class="titEvento"><?php echo $evento; ?></span>
                            </div>
                        </li>
                        <?php
                        $numItems = $numItems + 1;

                        if($numItems == 10){
                            break;
                        }
                    } else {
                        break;
                    }
                    ?>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="bn-controls">
            <button><span class="bn-arrow bn-prev"></span></button>
            <button><span class="bn-action"></span></button>
            <button><span class="bn-arrow bn-next"></span></button>
        </div>
    </div>
    <?php

echo $footerScripts;

?>

<script>
    jQuery('#example').breakingNews();
</script>
