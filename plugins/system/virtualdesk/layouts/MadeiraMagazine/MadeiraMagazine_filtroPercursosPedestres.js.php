<?php
    defined('_JEXEC') or die;
?>

var PageInitial = 1;
var totalPages = <?php echo $numberPages;?>;
var PageAtual = 1;
var TotalPercursos = <?php echo count($Percursos);?>;
var Topic = <?php echo $topic; ?>;

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

function cleanColor(){
    jQuery('#topic1').css('border-bottom','0px');
    jQuery('#topic1').css('color','#333');
    jQuery('#topic2').css('border-bottom','0px');
    jQuery('#topic2').css('color','#333');
    jQuery('#topic3').css('border-bottom','0px');
    jQuery('#topic3').css('color','#333');
    jQuery('#topic4').css('border-bottom','0px');
    jQuery('#topic4').css('color','#333');
    jQuery('#topic5').css('border-bottom','0px');
    jQuery('#topic5').css('color','#333');
    jQuery('#topic6').css('border-bottom','0px');
    jQuery('#topic6').css('color','#333');
}

function hidePagination(){
    var pagination = <?php echo $numberPages;?>;

    for(var i= 1; i <= pagination; i++){
        jQuery('.pagination .page' + i).removeClass("active");
        jQuery('.results .page' + i).removeClass("active");
    }
}

jQuery(document).ready(function(){

    ComponentsSelect2.init();

    document.getElementById('percLaurissilva').onclick = function() {
        if ( this.checked ) {
            document.getElementById("percLaurissilvaValue").value = 1;
            jQuery('#submit').click();
        } else {
            document.getElementById("percLaurissilvaValue").value = 2;
            jQuery('#submit').click();
        }
    };

    document.getElementById('costLaurissilva').onclick = function() {
        if ( this.checked ) {
            document.getElementById("costLaurissilvaValue").value = 1;
            jQuery('#submit').click();
        } else {
            document.getElementById("costLaurissilvaValue").value = 2;
            jQuery('#submit').click();
        }
    };

    document.getElementById('costSol').onclick = function() {
        if ( this.checked ) {
            document.getElementById("costSolValue").value = 1;
            jQuery('#submit').click();
        } else {
            document.getElementById("costSolValue").value = 2;
            jQuery('#submit').click();
        }
    };

    if(Topic == 1) {
        cleanColor();
        jQuery('#topic1').css('border-bottom','1px solid #69b5db');
        jQuery('#topic1').css('color','#69b5db');
    } else if(Topic == 2) {
        cleanColor();
        jQuery('#topic2').css('border-bottom','1px solid #69b5db');
        jQuery('#topic2').css('color','#69b5db');
    } else if(Topic == 3) {
        cleanColor();
        jQuery('#topic3').css('border-bottom','1px solid #69b5db');
        jQuery('#topic3').css('color','#69b5db');
    } else if(Topic == 4) {
        cleanColor();
        jQuery('#topic4').css('border-bottom','1px solid #69b5db');
        jQuery('#topic4').css('color','#69b5db');
    } else if(Topic == 5) {
        cleanColor();
        jQuery('#topic5').css('border-bottom','1px solid #69b5db');
        jQuery('#topic5').css('color','#69b5db');
    } else if(Topic == 6) {
        cleanColor();
        jQuery('#topic6').css('border-bottom','1px solid #69b5db');
        jQuery('#topic6').css('color','#69b5db');
    }

    document.getElementById('topic1').onclick = function(){
        document.getElementById("topic").value = 1;
        jQuery('#submit').click();
    };

    document.getElementById('topic2').onclick = function(){
        document.getElementById("topic").value = 2;
        jQuery('#submit').click();
    };

    document.getElementById('topic3').onclick = function(){
        document.getElementById("topic").value = 3;
        jQuery('#submit').click();
    };

    document.getElementById('topic4').onclick = function(){
        document.getElementById("topic").value = 4;
        jQuery('#submit').click();
    };

    document.getElementById('topic5').onclick = function(){
        document.getElementById("topic").value = 5;
        jQuery('#submit').click();
    };

    document.getElementById('topic6').onclick = function(){
        document.getElementById("topic").value = 6;
        jQuery('#submit').click();
    };

    document.getElementById('blocks').onclick = function(){
        jQuery('.topFilter #blocks path').css('fill','#69b5db');
        jQuery('.topFilter #list path').css('fill','#333');
        jQuery('.results .block').addClass("inBlock");
        jQuery('.results .block').removeClass("inList");
        jQuery('#listapercursospedestres .sideFilter').addClass("inBlock");
        jQuery('#listapercursospedestres .sideFilter').removeClass("inList");
    };

    document.getElementById('list').onclick = function(){
        jQuery('.topFilter #blocks path').css('fill','#333');
        jQuery('.topFilter #list path').css('fill','#69b5db');
        jQuery('.results .block').addClass("inList");
        jQuery('.results .block').removeClass("inBlock");
        jQuery('#listapercursospedestres .sideFilter').addClass("inList");
        jQuery('#listapercursospedestres .sideFilter').removeClass("inBlock");
    };

    jQuery('.pagination .page' + PageInitial).addClass("active");
    jQuery('.results .page' + PageInitial).addClass("active");

    if(TotalPercursos == 0){
        jQuery('.topFilter .pagination #previous').css('z-index','-9');
        jQuery('.topFilter .pagination #next').css('z-index','-9');
    } else if(PageInitial == 1){
        if(totalPages == 1){
            jQuery('.topFilter .pagination #next').css('z-index','-9');
        }
        jQuery('.topFilter .pagination #previous').css('z-index','-9');
    } else if(PageInitial == totalPages){
        jQuery('.topFilter .pagination #next').css('z-index','-9');
    } else {
        jQuery('.topFilter .pagination #previous').css('z-index','0');
        jQuery('.topFilter .pagination #next').css('z-index','0');
    }

    document.getElementById('next').onclick = function(){
        PageAtual = PageAtual + 1;
        jQuery('.topFilter .pagination #previous').css('z-index','0');

        if(PageAtual < totalPages) {
            hidePagination();
            jQuery('.pagination .page' + PageAtual).addClass("active");
            jQuery('.results .page' + PageAtual).addClass("active");
        } else if(PageAtual == totalPages){
            hidePagination();
            jQuery('.topFilter .pagination #next').css('z-index','-9');
            jQuery('.pagination .page' + PageAtual).addClass("active");
            jQuery('.results .page' + PageAtual).addClass("active");
        } else {
            jQuery('.topFilter .pagination #next').css('z-index','-9');
        }
    };

    document.getElementById('previous').onclick = function(){
        PageAtual = PageAtual - 1;

        jQuery('.topFilter .pagination #next').css('z-index','0');

        if(PageAtual > 1) {
            hidePagination();
            jQuery('.pagination .page' + PageAtual).addClass("active");
            jQuery('.results .page' + PageAtual).addClass("active");
        } else if(PageAtual == 1){
            hidePagination();
            jQuery('.topFilter .pagination #previous').css('z-index','-9');
            jQuery('.pagination .page' + PageAtual).addClass("active");
            jQuery('.results .page' + PageAtual).addClass("active");
        } else {
            jQuery('.topFilter .pagination #previous').css('z-index','-9');
        }
    };

    jQuery('#concelho').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#paisagem').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#zonamento').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#vertigens').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#tipologia').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#terreno').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#sentido').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#duracao').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#distancia').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#find').click(function() {
        jQuery('#submit').click();
    });

    jQuery(document).on('keypress',function(e) {
        if(e.which == 13) {
            event.preventDefault();
            jQuery('#submit').click();
        }
    });

});