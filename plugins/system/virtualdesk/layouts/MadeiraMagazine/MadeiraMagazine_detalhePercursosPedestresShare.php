<?php
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();
    $imageDetailPercursosPedestres = $obParam->getParamsByTag('imageDetailPercursosPedestres');

    $imgCapa = MMFiltroPercursosPedestresHelper::getImgCapa($referencia);

    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag){
        case 'pt-PT':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresPT');
            break;
        case 'en-EN':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresEN');
            break;
        case 'fr-FR':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresFR');
            break;
        case 'de-DE':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresDE');
            break;
        default:
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresPT');
            break;
    }


    $piecesAlias = explode(" ", $percurso);
    for($i=0; $i<count($piecesAlias); $i++){
        if($i == 0){
            $alias = $piecesAlias[$i];
        } else {
            $alias .= '_' . $piecesAlias[$i];
        }
    }

?>

<meta property="og:url"                content="" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="<?php echo $percurso;?>" />
<meta property="og:description"        content="<?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao);?> " />

<?php

    if(int($imgCapa) == 0){

        $LinkParaAImagem = JUri::base() . $imageDetailPercursosPedestres;

    } else {
        $objEventFile = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
        $FileList21Html = '';
        foreach ($arFileList as $rowFile) {
            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
            $LinkParaAImagem = $rowFile->guestlink;
        }
    }
?>

<meta property="og:image" content="<?php echo $LinkParaAImagem; ?>" >


</head>

<body>


<script type="text/javascript">
    window.location.href = "<?php echo $linkDetalhe. '?' . $alias . '&ref=' . $ref; ?>";
</script>
</body>


