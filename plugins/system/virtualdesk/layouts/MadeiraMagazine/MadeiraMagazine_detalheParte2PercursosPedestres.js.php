<?php
    defined('_JEXEC') or die;
?>

jQuery( document ).ready(function() {

    var mapaPercurso = L.map('mapaPercurso',{scrollWheelZoom:false}).setView([43.64701, -79.39425], 15);;
    serviceMap = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });

    var el05 = L.control.elevation();
    el05.addTo(mapaPercurso);

    var gpxLine =new L.GPX("<?php echo $ficheiroGPX;?>>", {
        async: true,
        marker_options: {
            startIconUrl: '<?php echo $baseurl;?>/images/madeiraMagazine/leaflet/start.png',
            endIconUrl: '<?php echo $baseurl;?>/images/madeiraMagazine/leaflet/end.png',
            shadowUrl: '<?php echo $baseurl;?>/images/madeiraMagazine/leaflet/pin-shadow.png'
        }
    });

    gpxLine.on('loaded', function(e) {
        mapaPercurso.fitBounds(e.target.getBounds());
    });


    gpxLine.on("addline",function(e){
        el05.addData(e.line);
    });

    gpxLine.addTo(mapaPercurso);

    mapaPercurso.addLayer(serviceMap);


// Call the getContainer routine.
    var htmlObject = el05.getContainer();

    // Get the desired parent node.
    var a = document.getElementById('elevation');

    // Finally append that node to the new parent, recursively searching out and re-parenting nodes.
    function setParent(el, newParent)
    {
        newParent.appendChild(el);
    }
    setParent(htmlObject, a);

});
