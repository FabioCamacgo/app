<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSitePercursosPedestresCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/fotoCapa_files.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('MadeiraMagazine_detalhePercursosPedestres');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;

    $link = $jinput->get('link','', 'string');
    $piecesLinkShare = explode("&fbclid=", $link);
    $piecesLink = explode("&ref=", $piecesLinkShare[0]);
    $piece = $piecesLink[1];
    $ref = str_rot13($piece);

    $obParam      = new VirtualDeskSiteParamsHelper();
    $imageDetailPercursosPedestres = $obParam->getParamsByTag('imageDetailPercursosPedestres');
    $linkDefaultPP = $obParam->getParamsByTag('linkDefaultPP');

    if(empty($ref)){
        ?>
        <script>
            var DefaultPath = '<?php echo $linkDefaultPP; ?>';

            window.location=DefaultPath;
        </script>
        <?php
    } else {
        //LOCAL SCRIPTS
        $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

        //GLOBAL SCRIPTS
        $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


        //BEGIN GLOBAL MANDATORY STYLES
        $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
        $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
        $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

        //END GLOBAL MANDATORY STYLES

        //BEGIN THEME GLOBAL STYLES
        $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
        //END THEME GLOBAL STYLES

        // BEGIN PAGE LEVEL STYLES
        $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

        // CUSTOM JS DASHboard
        $footerScripts  = '<!--[if lt IE 9]>';
        $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
        $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
        $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
        $footerScripts .= '<![endif]-->';

        echo $headCSS;

        $Detalhe = MMFiltroPercursosPedestresHelper::getDetalhePercurso($ref);

        foreach($Detalhe as $rowWSL) :
            $referencia = $ref;
            $percurso = $rowWSL['percurso'];
            $descricao = $rowWSL['descricao'];
            $creditosFotoCapa = $rowWSL['creditosFotoCapa'];
            $linkCreditos = $rowWSL['linkCreditos'];
            $inicio_freguesia = $rowWSL['inicio_freguesia'];
            $fim_freguesia = $rowWSL['fim_freguesia'];
            $distancia = $rowWSL['distancia'];
            $duracao = $rowWSL['duracao'];
            $terreno = $rowWSL['terreno'];
            $sentido = $rowWSL['sentido'];
            $vertigens = $rowWSL['vertigens'];
            $altitude_max = $rowWSL['altitude_max'];
            $altitude_min = $rowWSL['altitude_min'];
            $dificuldade = $rowWSL['dificuldade'];
        endforeach;

        $imgCapa = MMFiltroPercursosPedestresHelper::getImgCapa($referencia);

        if((int)$isVDAjax2Share===1) {
            require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/MadeiraMagazine_detalhePercursosPedestresShare.php');
        } else {
            ?>
            <div class="topSection">


                <div class="nameRoute">
                    <div class="contentRoute">
                        <h3 class="leftContentRoute">
                            <?php echo $percurso; ?>
                        </h3>
                    </div>
                </div>

                <?php
                if(!empty($creditosFotoCapa)){
                    if(!empty($linkCreditos)){
                        $creditos = '<a href="' . $linkCreditos . '" target="_blank">' . $creditosFotoCapa . '</a>';
                    } else {
                        $creditos = $creditosFotoCapa;
                    }

                    ?>
                    <div class="credits">
                        <span class="textCredit">
                            <?php echo '<span class="tagCredit">' . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_CREDITS') . '</span>:' . ' ' . $creditos; ?>
                        </span>
                        <?php
                        echo file_get_contents("images/madeiraMagazine/svg/information.svg");
                        ?>
                    </div>
                    <?php
                }
                ?>

                <?php
                if((int)$imgCapa == 0){
                    ?>
                    <div class="mainphoto">
                        <img src="<?php echo JUri::base() . $imageDetailPercursosPedestres;?>" alt="<?php echo $percurso;?>"/>
                    </div>
                    <?php
                } else {
                    $objEventFile = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                        ?>
                        <div class="mainphoto">
                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $percurso;?>"/>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>

            <div class="techSheet">

                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_INICIO_FIM');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Inicio_Fim.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($inicio_freguesia)){
                            $inicio_freguesia = '-';
                        }

                        if(empty($fim_freguesia)){
                            $fim_freguesia = '-';
                        }

                        echo $inicio_freguesia . ' / ' . $fim_freguesia;
                        ?>
                    </div>
                </div>



                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_DISTANCIA');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Distancia.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($distancia)) {
                            echo '-';
                        } else {
                            echo $distancia . 'Km';
                        }
                        ?>
                    </div>
                </div>

                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_DURACAO');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Duracao.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($duracao)){
                            echo '-';
                        } else{
                            $piecesDuracao = explode(",", $duracao);
                            if(empty($piecesDuracao[1])){
                                echo $piecesDuracao[0] . 'h';
                            } else {
                                echo $piecesDuracao[0] . 'h' . $piecesDuracao[1] . 'm';
                            }
                        }
                        ?>
                    </div>
                </div>

                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_TERRENO');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Terreno.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($terreno)) {
                            echo '-';
                        } else {
                            echo $terreno;
                        }
                        ?>
                    </div>
                </div>

                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_SENTIDO');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Sentido.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($sentido)) {
                            echo '-';
                        } else {
                            echo $sentido;
                        }
                        ?>
                    </div>
                </div>

                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_VERTIGENS');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Vertigens.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($vertigens)) {
                            echo '-';
                        } else {
                            echo $vertigens;
                        }
                        ?>
                    </div>
                </div>

                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_ALTITUDE');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Altitude.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($altitude_max)){
                            $altitude_max = '-';
                        } else {
                            $altitude_max = $altitude_max . 'm';
                        }

                        if(empty($altitude_min)){
                            $altitude_min = '-';
                        } else {
                            $altitude_min = $altitude_min . 'm';
                        }

                        echo $altitude_max . ' / ' . $altitude_min;
                        ?>
                    </div>
                </div>

                <div class="techItem">
                    <h4>
                        <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA_DIFICULDADE');?>
                    </h4>

                    <div class="icon">
                        <?php echo file_get_contents("images/madeiraMagazine/svg/fichatecnica/Dificuldade.svg");?>
                    </div>

                    <div class="value">
                        <?php
                        if(empty($dificuldade)) {
                            echo '-';
                        } else {
                            echo $dificuldade;
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }

        echo $headScripts;
        echo $footerScripts;
        echo $localScripts;
    }

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/MadeiraMagazine/MadeiraMagazine_detalhePercursosPedestres.js.php');
    ?>
</script>
