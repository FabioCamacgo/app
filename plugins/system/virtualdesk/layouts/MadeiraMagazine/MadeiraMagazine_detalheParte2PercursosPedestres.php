<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $jinput = JFactory::getApplication()->input;

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('MMFiltroPercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/filtroPercursosPedestres.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGPXFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/gpx_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresKMLFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/kml_files.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('MadeiraMagazine_detalheParte2PercursosPedestres');
    if ($resPluginEnabled === false) exit();

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';

    $leafletScripts  = $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/leaflet.js' . $addscript_end;
    $leafletScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/leaflet-0.7.2.js' . $addscript_end;
    $leafletScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/d3.v3.min.js' . $addscript_end;
    $leafletScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/leaflet-elevation/leaflet.elevation-0.0.4.min.js' . $addscript_end;
    $leafletScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/leaflet-gpx/gpx.js' . $addscript_end;

    $leafletCSS = $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/leaflet.css'. $addcss_end;
    $leafletCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/leaflet-0.7.2.css'. $addcss_end;
    $leafletCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/leaflet/leaflet-elevation/leaflet.elevation-0.0.4.css'. $addcss_end;

    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;

    $jinput = JFactory::getApplication('site')->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag){
        case 'pt-PT':
            $fileLangSufix = 'pt_PT';
            break;
        case 'en-EN':
            $fileLangSufix = 'en-EN';
            break;
        case 'fr-FR':
            $fileLangSufix = 'fr-FR';
            break;
        case 'de-DE':
            $fileLangSufix = 'de-DE';
            break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    $obPlg      = new VirtualDeskSitePluginsHelper();

    if($isVDAjaxReq !=1){
        echo (JText::_('COM_VIRTUALDESK_LOADACTIVELAYOUT_ERROR'));
        exit();
    } else {

        $jinput = JFactory::getApplication()->input;

        $link = $jinput->get('link','', 'string');
        $piecesLinkShare = explode("&fbclid=", $link);
        $piecesLink = explode("&ref=", $piecesLinkShare[0]);
        $piece = $piecesLink[1];
        $ref = str_rot13($piece);

        if(!empty($ref)){
            $DetalheParte2 = MMFiltroPercursosPedestresHelper::getDetalheParte2Percurso($ref, $fileLangSufix);
            $KMLFile = MMFiltroPercursosPedestresHelper::getKML($ref);
            $GPXFile = MMFiltroPercursosPedestresHelper::getGPX($ref);

            $objEventFile = new VirtualDeskSitePercursosPedestresGPXFilesHelper();
            $arFileList = $objEventFile->getFileGuestLinkByRefId($ref);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile) {
                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                $ficheiroGPX = $rowFile->guestlink;
            }

            $objEventFile = new VirtualDeskSitePercursosPedestresKMLFilesHelper();
            $arFileList = $objEventFile->getFileGuestLinkByRefId($ref);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile) {
                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                $ficheiroKML = $rowFile->guestlink;
            }

            foreach($DetalheParte2 as $rowWSL) :
                $destaques = $rowWSL['destaques'];
                $inicio_freguesia = $rowWSL['inicio_freguesia'];
                $fim_freguesia = $rowWSL['fim_freguesia'];
                $inicio_sitio = $rowWSL['inicio_sitio'];
                $fim_sitio = $rowWSL['fim_sitio'];
                $hastags = $rowWSL['hastags'];
                $altitude_max = $rowWSL['altitude_max'];
                $altitude_min = $rowWSL['altitude_min'];
                $subida_acumulado = $rowWSL['subida_acumulado'];
                $descida_acumulado = $rowWSL['descida_acumulado'];
            endforeach;

            echo $leafletCSS;
            echo $leafletScripts;

            ?>

            <div class="content">
                <div class="leftColumn">
                    <?php
                    if(!empty($destaques)){
                        $destaque = explode(';;', $destaques);

                        ?>
                        <div class="highlights">
                            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUES');?></h4>
                            <ol>
                                <?php
                                for($i=0; $i<count($destaque); $i++){
                                    ?>
                                    <li><?php echo $destaque[$i];?></li>
                                    <?php
                                }
                                ?>
                            </ol>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="startPoint">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PTO_PARTIDA');?></h4>
                        <p><?php echo '<b>' . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGUESIA') . ':</b> ' . $inicio_freguesia?></p>
                        <p><?php echo '<b>' . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIO') . ':</b> ' . $inicio_sitio?></p>
                    </div>

                    <div class="endPoint">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PTO_CHEGADA');?></h4>
                        <p><?php echo '<b>' . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGUESIA') . ':</b> ' . $fim_freguesia?></p>
                        <p><?php echo '<b>' . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIO') . ':</b> ' . $fim_sitio?></p>
                    </div>

                    <?php
                    if(!empty($hastags)){
                        $hastag = explode(';;', $hastags);

                        ?>
                        <div class="hastags">
                            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_HASTAGS');?></h4>
                            <ol>
                                <?php
                                for($i=0; $i<count($hastag); $i++){
                                    ?>
                                    <li><?php echo $hastag[$i];?></li>
                                    <?php
                                }
                                ?>
                            </ol>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div class="rightColumn">
                    <div class="w60">
                        <div id="mapaPercurso" style="height:350px;"></div>

                        <div class="technicalData">
                            <div id="elevation"></div>
                            <div class="downloads">

                                <?php
                                if((int)$KMLFile > 0){
                                    ?>
                                    <a href="<?php echo $ficheiroKML;?>"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_OBTER_KML');?></a>
                                    <?php
                                }
                                ?>

                                <?php
                                if((int)$GPXFile > 0){
                                    ?>
                                    <a href="<?php echo $ficheiroGPX;?>"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_OBTER_GPX');?></a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    }

?>

<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/MadeiraMagazine/MadeiraMagazine_detalheParte2PercursosPedestres.js.php');
    ?>
</script>

