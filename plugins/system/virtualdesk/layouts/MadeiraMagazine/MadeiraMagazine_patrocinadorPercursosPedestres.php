<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $jinput = JFactory::getApplication()->input;

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('MMFiltroPercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/filtroPercursosPedestres.php');
    JLoader::register('VirtualDeskSitePatrocinadoresLogoFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/patrocinadores/logo_files.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('MadeiraMagazine_patrocinadorPercursosPedestres');
    if ($resPluginEnabled === false) exit();

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';


    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;


    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag){
        case 'pt-PT':
            $fileLangSufix = 'pt_PT';
            break;
        case 'en-EN':
            $fileLangSufix = 'en-EN';
            break;
        case 'fr-FR':
            $fileLangSufix = 'fr-FR';
            break;
        case 'de-DE':
            $fileLangSufix = 'de-DE';
            break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    $obPlg      = new VirtualDeskSitePluginsHelper();

    if($isVDAjaxReq !=1){
        echo (JText::_('COM_VIRTUALDESK_LOADACTIVELAYOUT_ERROR'));
        exit();
    } else {

        $jinput = JFactory::getApplication()->input;

        $link = $jinput->get('link','', 'string');
        $piecesLinkShare = explode("&fbclid=", $link);
        $piecesLink = explode("&ref=", $piecesLinkShare[0]);
        $piece = $piecesLink[1];
        $ref = str_rot13($piece);

        if(!empty($ref)){
            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoEditorTexto = $obParam->getParamsByTag('logoEditorTexto');
            $linkEditorTexto = $obParam->getParamsByTag('linkEditorTexto');
            $nomeEditorTexto = $obParam->getParamsByTag('nomeEditorTexto');

            $idPatrocinador = MMFiltroPercursosPedestresHelper::getIDPatrocinador($ref);
            $DadosPatrocinador = MMFiltroPercursosPedestresHelper::getPatrocinadorPercurso($idPatrocinador);

            foreach($DadosPatrocinador as $rowWSL) :
                $referencia = $rowWSL['referencia'];
                $nome = $rowWSL['nome'];
                $url = $rowWSL['url'];
            endforeach;

            $Logo = MMFiltroPercursosPedestresHelper::getLogoPatrocinador($referencia);

            ?>

            <div class="editor">
                <div class="logo">
                    <a href="<?php echo $linkEditorTexto;?>>" target="_blank">
                        <img src="<?php echo $baseurl . $logoEditorTexto;?>" alt="logo_Editor"/>
                    </a>
                </div>

                <div class="name">
                    <p><?php echo $nomeEditorTexto;?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_CONTEUDO_TEXT');?></p>
                </div>
            </div>


            <?php

            if($idPatrocinador != 0){
                ?>

                <div class="sponsor">
                    <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_CONTEUDO_PATROCINADOR');?></h4>

                    <a href="<?php echo $url; ?>" target="_blank">
                        <?php
                        if((int)$Logo != 0) {
                            $objEventFile = new VirtualDeskSitePatrocinadoresLogoFilesHelper();
                            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                            $FileList21Html = '';
                            foreach ($arFileList as $rowFile) {
                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                ?>
                                <div class="logo">
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo 'sponsor'; ?>" title="<?php echo $nome; ?>"/>
                                </div>
                                <?php
                            }
                        } else {
                            echo '<div>' . $nome . '</div>';
                        }
                        ?>
                    </a>
                </div>

                <?php
            }
        }
    }

?>

