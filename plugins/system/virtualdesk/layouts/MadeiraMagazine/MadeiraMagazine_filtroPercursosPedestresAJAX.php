<?php
    $jinput = JFactory::getApplication()->input;

    $link = $jinput->get('link');
    $concelho = $jinput->get('concelho');
    $paisagem = $jinput->get('paisagem');
    $zonamento = $jinput->get('zonamento');
    $vertigens = $jinput->get('vertigens');
    $tipologia = $jinput->get('tipologia');
    $terreno = $jinput->get('terreno');
    $sentido = $jinput->get('sentido');
    $duracao = $jinput->get('duracao');
    $distancia = $jinput->get('distancia');
    $percurso = $jinput->get('percurso','', 'string');
    $percLaurissilvaValue = $jinput->get('percLaurissilvaValue');
    $costLaurissilvaValue = $jinput->get('costLaurissilvaValue');
    $costSolValue = $jinput->get('costSolValue');
    $topic = $jinput->get('topic');
    if(empty($topic)){
        $topic = 6;
    }

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSitePercursosPedestresCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/fotoCapa_files.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('MadeiraMagazine_filtroPercursosPedestres');
    if ($resPluginEnabled === false) exit();


    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $distrito = $obParam->getParamsByTag('distritoPP_MMG');
    $numPercursosPagina = $obParam->getParamsByTag('numPercursosPagina');
    $imageFilterPercursosPedestres = $obParam->getParamsByTag('imageFilterPercursosPedestres');

    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag){
        case 'pt-PT':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresPT');
            break;
        case 'en-EN':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresEN');
            break;
        case 'fr-FR':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresFR');
            break;
        case 'de-DE':
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresDE');
            break;
        default:
            $linkDetalhe = $obParam->getParamsByTag('detalhePercursosPedestresPT');
            break;
    }

    if(!empty($topic) || !empty($costSolValue) || !empty($costLaurissilvaValue) || !empty($percLaurissilvaValue) || !empty($percurso) || !empty($tipologia) || !empty($terreno) || !empty($sentido) || !empty($duracao) || !empty($distancia) || !empty($vertigens) || !empty($zonamento) || !empty($paisagem) || !empty($concelho)){
        $Percursos = MMFiltroPercursosPedestresHelper::getPercursosFiltered($topic, $costSolValue, $costLaurissilvaValue, $percLaurissilvaValue, $percurso, $tipologia, $terreno, $sentido, $duracao, $distancia, $vertigens, $zonamento, $paisagem, $concelho);
    } else {
        $Percursos = MMFiltroPercursosPedestresHelper::getPercursosAll();
    }

    $divideBlocks = count($Percursos) / $numPercursosPagina;
    $pieces = explode(".", $divideBlocks);
    $beginNumber = 1;
    $endNumber = $numPercursosPagina;
    $endRoute = 0;

    if(empty($pieces[1]) || $pieces[1] == 0){
        $numberPages = $pieces[0];
    } else {
        $numberPages = $pieces[0] + 1;
    }
?>

<div class="sideFilter inBlock">
    <form class="lateral" action="" method="post" class="login-form" enctype="multipart/form-data" >

        <!--   Percurso  -->
        <div class="form-group" id="search">
            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOMEPERCURSO');?></h4>
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="percurso" id="percurso" maxlength="250" value="<?php echo $percurso ?>"/>
            <div id="find">
                <?php
                    echo file_get_contents("images/madeiraMagazine/svg/search.svg");
                ?>
            </div>
        </div>


        <input type="hidden" class="form-control" autocomplete="off" placeholder="" name="topic" id="topic" value="<?php echo $topic; ?>"/>

        <!--   Distancia  -->
        <div class="form-group">
            <?php $Distancia = MMFiltroPercursosPedestresHelper::getDistancia()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FILTRODISTANCIA'); ?></h4>

            <select name="distancia" value="<?php echo $distancia; ?>" id="distancia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($distancia)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Distancia as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['distancia_min'] . 'Km - ' . $rowWSL['distancia_max'] . 'Km'; ?></option>
                    <?php endforeach;
                } else {
                    $distancisSelect = MMFiltroPercursosPedestresHelper::getDistanciaSelect($distancia);
                    foreach($distancisSelect as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['distancia_min'] . 'Km - ' . $rowWSL['distancia_max'] . 'Km'; ?></option>
                    <?php endforeach; ?>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeDistancia = MMFiltroPercursosPedestresHelper::excludeDistancia($distancia)?>
                    <?php foreach($ExcludeDistancia as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['distancia_min'] . 'Km - ' . $rowWSL['distancia_max'] . 'Km'; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

        <!--   Duracao  -->
        <div class="form-group">
            <?php $Duracao = MMFiltroPercursosPedestresHelper::getDuracao()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO'); ?></h4>

            <select name="duracao" value="<?php echo $duracao; ?>" id="duracao" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($duracao)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Duracao as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['duracao_min'] . 'h - ' . $rowWSL['duracao_max'] . 'h'; ?></option>
                    <?php endforeach;
                } else {
                    $duracaoSelect = MMFiltroPercursosPedestresHelper::getDuracaoSelect($duracao);
                    foreach($duracaoSelect as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['duracao_min'] . 'h - ' . $rowWSL['duracao_max'] . 'h'; ?></option>
                    <?php endforeach; ?>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeDuracao = MMFiltroPercursosPedestresHelper::excludeDuracao($duracao)?>
                    <?php foreach($ExcludeDuracao as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['duracao_min'] . 'h - ' . $rowWSL['duracao_max'] . 'h'; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

        <!--   Sentido  -->
        <div class="form-group">
            <?php $Sentido = MMFiltroPercursosPedestresHelper::getSentido()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO'); ?></h4>

            <select name="sentido" value="<?php echo $sentido; ?>" id="sentido" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($sentido)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Sentido as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['sentido']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $sentido; ?>"><?php echo MMFiltroPercursosPedestresHelper::getSentidoSelect($sentido) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeSentido = MMFiltroPercursosPedestresHelper::excludeSentido($sentido)?>
                    <?php foreach($ExcludeSentido as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['sentido']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

        <!--   Terreno  -->
        <div class="form-group">
            <?php $Terreno = MMFiltroPercursosPedestresHelper::getTerreno()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO'); ?></h4>

            <select name="terreno" value="<?php echo $terreno; ?>" id="terreno" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($terreno)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Terreno as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['terreno']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $terreno; ?>"><?php echo MMFiltroPercursosPedestresHelper::getTerrenoSelect($terreno) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeTerreno = MMFiltroPercursosPedestresHelper::excludeTerreno($terreno)?>
                    <?php foreach($ExcludeTerreno as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['terreno']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

        <!--   Tipologia  -->
        <div class="form-group">
            <?php $Tipologia = MMFiltroPercursosPedestresHelper::getTipologia()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA'); ?></h4>

            <select name="tipologia" value="<?php echo $tipologia; ?>" id="tipologia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($tipologia)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Tipologia as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['tipologia']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $tipologia; ?>"><?php echo MMFiltroPercursosPedestresHelper::getTipologiaSelect($tipologia) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeTipologia = MMFiltroPercursosPedestresHelper::excludeTipologia($tipologia)?>
                    <?php foreach($ExcludeTipologia as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['tipologia']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

        <!--   Vertigens  -->
        <div class="form-group">
            <?php $Vertigens = MMFiltroPercursosPedestresHelper::getVertigens()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS'); ?></h4>

            <select name="vertigens" value="<?php echo $vertigens; ?>" id="vertigens" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($vertigens)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Vertigens as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['vertigens']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $vertigens; ?>"><?php echo MMFiltroPercursosPedestresHelper::getVertigensSelect($vertigens) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeVertigens = MMFiltroPercursosPedestresHelper::excludeVertigens($vertigens)?>
                    <?php foreach($ExcludeVertigens as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['vertigens']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

        <!--   Zonamento  -->
        <div class="form-group">
            <?php $Zonamento = MMFiltroPercursosPedestresHelper::getZonamento()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO'); ?></h4>

            <select name="zonamento" value="<?php echo $zonamento; ?>" id="zonamento" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($zonamento)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Zonamento as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['zonamento']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $zonamento; ?>"><?php echo MMFiltroPercursosPedestresHelper::getZonamentoSelect($zonamento) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeZonamento = MMFiltroPercursosPedestresHelper::excludeZonamento($zonamento)?>
                    <?php foreach($ExcludeZonamento as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['zonamento']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

        <!--   Variações  -->
        <div class="form-group">

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VARIACOES'); ?></h4>

            <div>
                <input type="checkbox" name="percLaurissilva" id="percLaurissilva" value="<?php echo $percLaurissilvaValue; ?>" <?php if ($percLaurissilvaValue == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PERCLAURISSILVA'); ?>
                <input type="hidden" required id="percLaurissilvaValue" name="percLaurissilvaValue" value="<?php echo $percLaurissilvaValue; ?>">
            </div>

            <div>
                <input type="checkbox" name="costLaurissilva" id="costLaurissilva" value="<?php echo $costLaurissilvaValue; ?>" <?php if ($costLaurissilvaValue == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTALAURISSILVA'); ?>
                <input type="hidden" required id="costLaurissilvaValue" name="costLaurissilvaValue" value="<?php echo $costLaurissilvaValue; ?>">
            </div>

            <div>
                <input type="checkbox" name="costSol" id="costSol" value="<?php echo $costSolValue; ?>" <?php if ($costSolValue == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTASOL'); ?>
                <input type="hidden" required id="costSolValue" name="costSolValue" value="<?php echo $costSolValue; ?>">
            </div>
        </div>

        <!--   Paisagem  -->
        <div class="form-group">
            <?php $Paisagem = MMFiltroPercursosPedestresHelper::getPaisagem()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM'); ?></h4>

            <select name="paisagem" value="<?php echo $paisagem; ?>" id="paisagem" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($paisagem)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Paisagem as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['paisagem']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $paisagem; ?>"><?php echo MMFiltroPercursosPedestresHelper::getPaisagemSelect($paisagem) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludePaisagem = MMFiltroPercursosPedestresHelper::excludePaisagem($paisagem)?>
                    <?php foreach($ExcludePaisagem as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['paisagem']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>

        <!--   Concelho  -->
        <div class="form-group">
            <?php $Concelho = MMFiltroPercursosPedestresHelper::getConcelho($distrito)?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_CONCELHO'); ?></h4>

            <select name="concelho" value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($concelho)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Concelho as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['concelho']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $concelho; ?>"><?php echo MMFiltroPercursosPedestresHelper::getConcelhoSelect($distrito, $concelho) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeConcelho = MMFiltroPercursosPedestresHelper::excludeConcelho($distrito, $concelho)?>
                    <?php foreach($ExcludeConcelho as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['concelho']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>

    </form>
</div>

<div class="pagecontent">
    <div class="topFilter">
        <form class="top" action="" method="post" class="login-form" enctype="multipart/form-data" >
            <span class="filter"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VIEW');?></span>
            <span id="topic1" class="filtertopic"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FILTER_TOPIC_1');?></span>
            <span id="topic2" class="filtertopic"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FILTER_TOPIC_2');?></span>
            <span id="topic3" class="filtertopic"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FILTER_TOPIC_3');?></span>
            <span id="topic4" class="filtertopic"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FILTER_TOPIC_4');?></span>
            <span id="topic5" class="filtertopic"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FILTER_TOPIC_5');?></span>
            <span id="topic6" class="filtertopic"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FILTER_TOPIC_6');?></span>

            <div class="pagination">

                <div id="previous">
                    <?php
                        echo file_get_contents("images/madeiraMagazine/svg/before.svg");
                    ?>
                </div>

                <?php
                for($i=0; $i < $numberPages; $i++){
                    if($i == 0){
                        $beginNumber = 1;
                        if(count($Percursos) < $numPercursosPagina) {
                            $endNumber = count($Percursos);
                        } else {
                            $endNumber = $numPercursosPagina;
                        }
                    } else if($i > 0 && $i < ($numberPages - 1)){
                        $beginNumber = $endNumber + 1;
                        $endNumber = $endNumber + $numPercursosPagina;
                    } else {
                        $beginNumber = $endNumber + 1;
                        $endNumber = count($Percursos);
                    }
                    ?>

                    <div class="page page<?php echo $i+1;?>">
                        <?php echo JText::sprintf('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAGINATION',$beginNumber, $endNumber, count($Percursos));?>
                    </div>

                    <?php
                }
                ?>

                <div id="next">
                    <?php
                    echo file_get_contents("images/madeiraMagazine/svg/next.svg");
                    ?>
                </div>
            </div>


            <div id="blocks">
                <?php
                echo file_get_contents("images/madeiraMagazine/svg/block.svg");
                ?>
            </div>

            <div id="list">
                <?php
                echo file_get_contents("images/madeiraMagazine/svg/list.svg");
                ?>
            </div>
        </form>
    </div>

    <div class="results">
        <?php
            if(count($Percursos) == 0) {
                echo '<h3 class="noresults">' . JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_NORESULTS') . '</h3>';
            } else {
                for($p=0; $p < $numberPages; $p++){
                    if($p == 0){
                        $beginRoute = 0;
                        $endRoute = $numPercursosPagina;
                    } else if($p > 0 && $p < ($numberPages - 1)){
                        $beginRoute = $endRoute;
                        $endRoute = $endRoute + $numPercursosPagina;
                    } else {
                        $beginRoute = $endRoute;
                        $endRoute = count($Percursos) - 1;
                    }
                    ?>

                    <div class="contentList page page<?php echo $p+1;?>">
                        <?php
                            $output = array_slice($Percursos, $beginRoute, $numPercursosPagina);

                            foreach($output as $rowWSL) :
                                $id = $rowWSL['id'];
                                $referencia = $rowWSL['referencia'];
                                $percurso = $rowWSL['percurso'];
                                $concelho = $rowWSL['concelho'];
                                $percurso_laurissilva = $rowWSL['percurso_laurissilva'];
                                $costa_laurissilva = $rowWSL['costa_laurissilva'];
                                $costa_sol = $rowWSL['costa_sol'];

                                $piecesAlias = explode(" ", $percurso);
                                for($i=0; $i<count($piecesAlias); $i++){
                                    if($i == 0){
                                        $alias = $piecesAlias[$i];
                                    } else {
                                        $alias .= '_' . $piecesAlias[$i];
                                    }
                                }

                                $ref = str_rot13($referencia);

                                ?>
                                <div class="block inBlock">

                                    <a href="<?php echo $linkDetalhe. '?' . $alias . '&ref=' . $ref; ?>">
                                        <?php
                                            $imgCapa = MMFiltroPercursosPedestresHelper::getImgCapa($referencia);
                                            $imgAlt = MMFiltroPercursosPedestresHelper::random_code();

                                            if((int)$imgCapa == 0){
                                                ?>
                                                    <div class="blockImage" style="background-image:URL('<?php echo JUri::base() . $imageFilterPercursosPedestres;?>'); background-size: cover; background-position: center; background-repeat: no-repeat;"></div>
                                                <?php
                                            } else {
                                                $objEventFile = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
                                                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                                $FileList21Html = '';
                                                foreach ($arFileList as $rowFile) {
                                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                    ?>
                                                    <div class="blockImage" style="background-image:URL('<?php echo $rowFile->guestlink; ?>'); background-size: cover; background-position: center; background-repeat: no-repeat;"></div>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </a>

                                    <div class="blockContent">
                                        <a href="<?php echo $linkDetalhe. '?' . $alias . '&ref=' . $ref; ?>">
                                            <h4><?php echo $percurso;?></h4>
                                        </a>

                                        <p>
                                            <?php echo file_get_contents("images/madeiraMagazine/svg/placemark.svg") . ' ' . $concelho; ?>
                                        </p>

                                        <a href="<?php echo $linkDetalhe. '?' . $alias . '&ref=' . $ref; ?>" class="explore">
                                            <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PERCLAURISSILVA_EXPLORE');?>
                                        </a>

                                        <?php
                                        if($percurso_laurissilva == 1 || $costa_laurissilva == 1 || $costa_sol == 1){
                                            ?>
                                            <div class="tags">
                                                <?php
                                                if($percurso_laurissilva == 1){
                                                    ?>
                                                    <span class="tag pLaurissilva">
                                                                    <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PERCLAURISSILVA_LISTAPLUGIN');?>
                                                                </span>
                                                    <?php
                                                }

                                                if($costa_laurissilva == 1){
                                                    ?>
                                                    <span class="tag cLaurissilva">
                                                                    <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTALAURISSILVA_LISTAPLUGIN');?>
                                                                </span>
                                                    <?php
                                                }

                                                if($costa_sol == 1){
                                                    ?>
                                                    <span class="tag cSol">
                                                                    <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTASOL_LISTAPLUGIN');?>
                                                                </span>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                            <?php
                            endforeach;
                        ?>
                    </div>
                    <?php
                }
            }
        ?>
    </div>
</div>




<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/MadeiraMagazine/MadeiraMagazine_filtroPercursosPedestres.js.php');
    ?>
</script>
