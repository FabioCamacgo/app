<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('MadeiraMagazine_ondeFicarFiltroFP');
    if ($resPluginEnabled === false) exit();


    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/MadeiraMagazine/MadeiraMagazine_ondeFicarFiltroFP.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;


?>


<div class="filtroOndeFicar">
    <form action="" method="post" class="login-form" enctype="multipart/form-data" >

        <!--   Local  -->
        <div class="form-group" id="localidade">
            <?php $Concelhos = VirtualDeskSiteMMOndeFicarFiltroHelper::getConcelhos()?>
            <div class="col-md-3">
                <?php echo JText::_('COM_VIRTUALDESK_MADEIRAMAGAZINE_ONDEFICAR_LOCALIDADE'); ?>
            </div>
            <div class="col-md-9">
                <select name="concelho" value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($concelho)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_MADEIRAMAGAZINE_ONDEFICAR_LOCALIDADE_INSIDE'); ?></option>
                        <?php foreach($Concelhos as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['concelho']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteMMOndeFicarFiltroHelper::getConcelhosSelect($concelho) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeConcelhos = VirtualDeskSiteMMOndeFicarFiltroHelper::excludeConcelhos($concelho)?>
                        <?php foreach($ExcludeConcelhos as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['concelho']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   CATEGORIAS  -->
        <div class="form-group" id="categoriasOndeFicar">
            <?php $CatOndeFicar = VirtualDeskSiteMMOndeFicarFiltroHelper::getCatOndeFicar()?>
            <div class="col-md-3">
                <?php echo JText::_('COM_VIRTUALDESK_MADEIRAMAGAZINE_ONDEFICAR'); ?>
            </div>
            <div class="col-md-9">
                <select name="catOndeFicar" value="<?php echo $catOndeFicar; ?>" id="catOndeFicar" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($catOndeFicar)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_MADEIRAMAGAZINE_ONDEFICAR_INSIDE'); ?></option>
                        <?php foreach($CatOndeFicar as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['name']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $catOndeFicar; ?>"><?php echo VirtualDeskSiteMMOndeFicarFiltroHelper::getCatOndeFicarSelect($catOndeFicar) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeCatOndeFicar = VirtualDeskSiteMMOndeFicarFiltroHelper::excludeCatOndeFicar($catOndeFicar)?>
                        <?php foreach($ExcludeCatOndeFicar as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['name']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
        </div>

    </form>
</div>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/MadeiraMagazine/MadeiraMagazine_ondeFicarFiltroFP.js.php');
    ?>
</script>