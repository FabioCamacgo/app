<?php
defined('_JEXEC') or die;

?>
function dropError(){
    jQuery('.backgroundErro').css('display','block');
}

function CollapseForm2(){

    var IDofDivWithForm = "new_alerta";

    document.getElementById(IDofDivWithForm).style.display = "none";

    jQuery('#btPost').css('visibility','hidden');
}

var MapsGoogle = function () {

    var mapMarker = function () {

        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });


        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconpath
        });

        map.setZoom(13);

        var everythingElse = [[-85.1054596961173,-180],[85.1054596961173,-180],[85.1054596961173,180],[-85.1054596961173,180],[-85.1054596961173,0]];

        var path = [[32.70325194126669,-17.13814096710318],
            [32.70147774524857,-17.13695831162798],
            [32.69995411247207,-17.13474469233536],
            [32.69893411170317,-17.13163556089005],
            [32.69687395571367,-17.12943719406816],
            [32.69366547545879,-17.12667983195218],
            [32.69214520965476,-17.12407853150978],
            [32.6918315557128,-17.12220973156305],
            [32.69113495889658,-17.12065434532456],
            [32.68817364820082,-17.11894370399893],
            [32.68676940266594,-17.11646465496893],
            [32.68663156154839,-17.11575220322811],
            [32.68508096273629,-17.11348402482469],
            [32.68314685846605,-17.11043164802486],
            [32.6814262275714,-17.10874406161285],
            [32.67960948894879,-17.10545425336425],
            [32.67837309225074,-17.10424798206541],
            [32.67865020854089,-17.10188801568033],
            [32.67887354713483,-17.10155762933463],
            [32.67909821495305,-17.10047165164765],
            [32.67960892642832,-17.09896913835208],
            [32.68006898915424,-17.09786249735334],
            [32.67943769063579,-17.09738478603199],
            [32.67909771568275,-17.09512892210956],
            [32.67972318686986,-17.09662110949],
            [32.68041782751283,-17.09674432731266],
            [32.6806522344457,-17.09603960519518],
            [32.68059342179142,-17.09562196487771],
            [32.68059329128901,-17.09457333008629],
            [32.68001061823549,-17.09321590364904],
            [32.67921019919422,-17.09141981337976],
            [32.67842709584783,-17.08945913142301],
            [32.67859336742423,-17.08896250102592],
            [32.67909686395519,-17.0877326102334],
            [32.67955037720746,-17.08670137771317],
            [32.68000974563691,-17.08662790967778],
            [32.68059207374994,-17.08583582387978],
            [32.68160383265835,-17.08608953529455],
            [32.68239625279082,-17.08510052585929],
            [32.68301750427597,-17.08535270007861],
            [32.6835217188933,-17.08454007625584],
            [32.68468134044328,-17.08462657705112],
            [32.68669379653326,-17.08389381142645],
            [32.68961840263629,-17.08226658720567],
            [32.69284547149272,-17.08180307640767],
            [32.69422396641406,-17.08037858385251],
            [32.69633388896689,-17.0797752233159],
            [32.69845760516908,-17.07924776453219],
            [32.70002364139621,-17.07803815168834],
            [32.70203366001988,-17.0764271558117],
            [32.70392888809101,-17.07589866421613],
            [32.70643334951454,-17.0746586682269],
            [32.70795431460744,-17.07257163045328],
            [32.71044190587646,-17.07193720431897],
            [32.71173175144993,-17.07061395529314],
            [32.71293313190375,-17.07059781662478],
            [32.71441069668395,-17.0685795837757],
            [32.71722977661648,-17.06549664977057],
            [32.71789169887732,-17.06572058773583],
            [32.71910356333482,-17.06642846737611],
            [32.71978852944614,-17.06719721743855],
            [32.71978891710235,-17.06794664081],
            [32.72304797523042,-17.06640758386783],
            [32.72665646744655,-17.06586909908592],
            [32.72790860249188,-17.06369268866851],
            [32.73268041800831,-17.0629216222728],
            [32.73388861462564,-17.06161727139308],
            [32.73565474158186,-17.05829652272587],
            [32.73458736602129,-17.05693299039739],
            [32.73511780222839,-17.05566600750634],
            [32.73783989918633,-17.05339232665619],
            [32.7402952598399,-17.05280140662531],
            [32.74611755881939,-17.04697975408501],
            [32.75624462950872,-17.05556708374865],
            [32.75624598380874,-17.0576925740875],
            [32.7705460574154,-17.07584885186186],
            [32.75898375068859,-17.09389517890802],
            [32.75484499163305,-17.09387564390165],
            [32.74658033868933,-17.0995597986565],
            [32.74349037982515,-17.1041992985907],
            [32.74787367107984,-17.11201547500711],
            [32.7459695008978,-17.11526276523328],
            [32.74054531165816,-17.11764608470608],
            [32.73958536452275,-17.11675134777904],
            [32.7355268233447,-17.11814067250346],
            [32.73070655123487,-17.11904260371574],
            [32.7287506280643,-17.12033437548597],
            [32.72748074370818,-17.12124946767959],
            [32.72732572370585,-17.12316354872576],
            [32.72717064578463,-17.12461595908481],
            [32.72577956955475,-17.12476302518585],
            [32.7250229326968,-17.12717412575072],
            [32.72338779779498,-17.12843348332556],
            [32.72353627905476,-17.13002245976161],
            [32.72487616291662,-17.1309471304673],
            [32.72295185563439,-17.13096551317183],
            [32.72194290907414,-17.1324294583741],
            [32.72081030908055,-17.13328334305649],
            [32.71901662829293,-17.13464899739699],
            [32.71714756815212,-17.13666071042617],
            [32.71533954039568,-17.13840055058039],
            [32.7118921314049,-17.1374247411969],
            [32.710598131351,-17.13851857475014],
            [32.70711596092104,-17.1399289206211],
            [32.70478273501011,-17.14016869137661],
            [32.70315302758685,-17.14056905797101],
            [32.70325194126669,-17.13814096710318]
        ];


        polygon1 = map.drawPolygon({
            paths:[everythingElse, path],
            strokeColor: '#e1e1e1',
            strokeOpacity: 1,
            strokeWeight: 2,
            fillColor: '#8a8a8a',
            fillOpacity: 0.5
        });

        GMaps.on('click', map.map, function(event) {

            map.removeMarkers();

            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            jQuery('#coordenadas').val(lat + ',' + lng);

            map.addMarker({
                lat: lat,
                lng: lng,
                //title: 'Marker #' + index,
                icon: iconpath,
                infoWindow: {
                    content : ''
                }

            });
        });

    }

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };

}();

var LatToSet = 32.687081455983055;
var LongToSet = -17.098311633676424;

var AlertaNew = function() {

    var handleAlertaNew = function() {

        jQuery('#new_alerta').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                console.log('com erros...');

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                //form.submit();
                console.log('submeter...');
                var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                MainMessageBlock.find('span').html('');
                MainMessageBlock.hide();
                vdFormSubmitByAjax.sendByAjax();
            }
        });

        jQuery('#new-alerta').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-alerta').validate().form()) {
                    //jQuery('#new-alerta').submit();
                    console.log('submeter...');
                    vdFormSubmitByAjax.sendByAjax();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleAlertaNew();
        },
        handleAlertaNew : handleAlertaNew
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        //jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload").fileuploader({

            changeInput: ' ',
            limit: 3,
            fileMaxSize: 3,
            extensions: ['jpg', 'jpeg', 'png', 'pdf'],
            enableApi: true,
            addMore: true,
            theme: 'thumbnails',
            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = jQuery.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = jQuery.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - null
            upload: {
                // upload URL {String}
                url: setVDCurrentRelativePath , // definido no php

                // upload data {null, Object}
                // you can also change this Object in beforeSend callback
                // example: { option_1: '1', option_2: '2' }
                data:  null ,

                // upload type {String}
                // for more details http://api.jquery.com/jquery.ajax/
                type: 'POST',

                // upload enctype {String}
                // for more details http://api.jquery.com/jquery.ajax/
                enctype: 'multipart/form-data',

                // auto-start file upload {Boolean}
                // if false, you can use the API methods - item.upload.send() to trigger upload for each file
                // if false, you can use the upload button - check custom file name example
                start: false, // true,

                // upload the files synchron {Boolean}
                synchron: true,

                // upload large files in chunks {false, Number} set file chunk size in MB as Number (ex: 4)
                chunk: false,

                // Callback fired before uploading a file by returning false, you can prevent the upload
                beforeSend: function(item, listEl, parentEl, newInputEl, inputEl) {
                    // example:
                    // here you can extend the upload data

                    item.upload.data.isVDAjaxReqFileUpload = '1';
                    item.upload.data.VDAjaxReqProcRefId    = jQuery("#VDAjaxReqProcRefId").val();

                    return true;
                },

                // Callback fired if the upload succeeds
                // we will add by default a success icon and fadeOut the progressbar
                onSuccess: function(data, item, listEl, parentEl, newInputEl, inputEl, textStatus, jqXHR) {
                    item.html.find('.fileuploader-action-remove').addClass('fileuploader-action-success');

                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },

                // Callback fired if the upload failed
                // we will set by default the progressbar to 0% and if it wasn't cancelled, we will add a retry button
                onError: function(item, listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus, errorThrown) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }

                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },

                onProgress: function(data, item, listEl, parentEl, newInputEl, inputEl) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },

                // Callback fired after all files were uploaded
                onComplete: function(listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus) {
                    // callback will go here

                    vdOnFileUploadLoadComplete(); // está definida no módulo do site se for invocado por ajax
                }
            }

            ,// by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

var vdAlertaHelperObj = function () {

    var vdAlertaData = '';

    var setAlertaDataByAjax = function (data) {
        vdAlertaData = data;
    };

    var getAlertaDataByAjax = function () {
        return vdAlertaData;
    };

    return {
        setAlertaDataByAjax  :  setAlertaDataByAjax,
        getAlertaDataByAjax  :  getAlertaDataByAjax
    };

}();

var vdFormSubmitByAjax = function () {

    var sendByAjax = function () {
        var VDgReCapResponse = grecaptcha.getResponse();
        var novaOcorrencia = jQuery(".novaOcorrencia");
        var loader = jQuery(".loader");
        novaOcorrencia.hide();
        jQuery('#btPost').hide();
        loader.show();
        jQuery.ajax({
            type: 'POST',
            url: '<?php echo JUri::base() . 'novoalerta/'; ?>',
            data: { submitForm: jQuery('#btPost').val(), categoria: jQuery('#categoria').val(), vdmenumain: jQuery('#vdmenumain').val(), nome: jQuery('#nome').val(), email: jQuery('#email').val(), emailConf: jQuery('#emailConf').val(), fiscalid: jQuery('#fiscalid').val(), freguesias: jQuery('#freguesias').val(), telefone: jQuery('#telefone').val(), ptosrefs: jQuery('#ptosrefs').val(), morada: jQuery('#morada').val(), sitio: jQuery('#sitio').val(), descricao: jQuery('#descricao').val(), upload: jQuery('#upload').val(), coordenadas: jQuery('#coordenadas').val(), veracid2: jQuery('#veracid2').val(), politica2: jQuery('#politica2').val(), isVDAjaxReq: 1, VDRecap: VDgReCapResponse },
            success: function(response) {
                var data = response;

                var nFirstPos = data.search("__i__");
                var nLastPos  = data.search("__e__");
                var RefOccrrentToSend= data.substring(nFirstPos+5,nLastPos);

                var vdApiFileUploader = jQuery.fileuploader.getInstance('#fileupload');

                if(vdApiFileUploader.isEmpty()) {
                    novaOcorrencia.empty();
                    novaOcorrencia.hide();
                    novaOcorrencia.append(data);
                    loader.hide();
                    jQuery('#novaOcorrencia .g-content').css('border','1px solid rgba(224, 224, 224, 0.38)');
                    novaOcorrencia.fadeIn();
                    jQuery('#btPost').fadeIn();

                }
                else {

                    if(RefOccrrentToSend.length > 10){
                        var d = new Date();
                        jQuery("#VDAjaxReqProcRefId").val(RefOccrrentToSend);

                        vdAlertaHelperObj.setAlertaDataByAjax(data);

                        // start the upload of the files
                        vdApiFileUploader.uploadStart();
                    } else {
                        // existe ficheiros, mas sem a refid, submetemos apenas o form
                        novaOcorrencia.empty();
                        novaOcorrencia.hide();
                        novaOcorrencia.append(data);
                        loader.hide();
                        jQuery('#novaOcorrencia .g-content').css('border','1px solid rgba(224, 224, 224, 0.38)');
                        novaOcorrencia.fadeIn();
                        jQuery('#btPost').fadeIn();
                    }
                }

            } ,
            error: function(xhr, status, error) {
                //ToDo: Tratar Erros de gravação da ocorrência
                //var err = eval("(" + xhr.responseText + ")");
                var err = 'Erro ao Gravar:' + error;
                alert(err);
            }


        });

    };

    return {
        sendByAjax  :  sendByAjax

    };

}();


function vdOnFileUploadLoadComplete ()
{
    var novaOcorrencia = jQuery(".novaOcorrencia");
    var loader = jQuery(".loader");
    var data = vdAlertaHelperObj.getAlertaDataByAjax();
    console.log('File batch upload complete');
    novaOcorrencia.empty();
    novaOcorrencia.hide();
    novaOcorrencia.append(data);
    loader.hide();
    jQuery('#novaOcorrencia .g-content').css('border','1px solid rgba(224, 224, 224, 0.38)');
    novaOcorrencia.fadeIn();
    jQuery('#btPost').fadeIn();

    vdAlertaHelperObj.setAlertaDataByAjax('');
}



/*
* Inicialização
*/
jQuery(document).ready(function() {

    AlertaNew.init();

    setTimeout(function(){

        var InputLatLong = jQuery('#coordenadas').val();

        if( InputLatLong!=undefined && InputLatLong!="") {
            var ArrayLatLong = InputLatLong.split(",");
            if(ArrayLatLong.length == 2) {
                LatToSet  =  ArrayLatLong[0];
                LongToSet =  ArrayLatLong[1];

            }
        }

        if( jQuery('#gmap_marker').length ) {
            MapsGoogle.init()
        }



        /*Erro Alerta*/
        jQuery( "#fechaErro" ).click(function() {
            jQuery('.backgroundErro').css('display','none');
        });

        // Select 2
        ComponentsSelect2.init();

        ComponentFileUploader.init();

        jQuery("#categoria").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

            var pathArray = window.location.pathname.split('/');

            var secondLevelLocation = pathArray[1];

            var categoria = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

            if(categoria == null || categoria == '') {

                var select = jQuery('#vdmenumain');
                select.find('option').remove();
                select.prop('disabled', true);

            } else {

                var dataString = "categoria="+categoria; /* STORE THAT TO A DATA STRING */

                App.blockUI({ target:'#blocoMenuMain',  animate: true});

                jQuery.ajax({
                    url: websitepath,
                    data:'m=alerta_getsubcat&categoria=' + categoria ,

                    success: function(output) {

                        var select = jQuery('#vdmenumain');
                        select.find('option').remove();

                        if (output == null || output == '') {
                            select.prop('disabled', true);
                        }
                        else {
                            select.prop('disabled', false);
                            select.append(jQuery('<option>'));
                            jQuery.each(JSON.parse(output), function (i, obj) {
                                select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                            });
                        }

                        // hide loader
                        App.unblockUI('#blocoMenuMain');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //alert(xhr.status +  ' ' + thrownError);
                        // hide loader
                        select.find('option').remove();
                        select.prop('disabled', true);

                       App.unblockUI('#blocoMenuMain');
                    }
                });
            }


        });

    }, 0);

    jQuery("#btPost").on("click", function(event) {
        //jQuery('#new_alerta').validate();
        AlertaNew.handleAlertaNew();
        console.log('validate');
    });

    var novaOcorrencia = jQuery(".novaOcorrencia");
    var loader = jQuery(".loader");
    novaOcorrencia.show();
    jQuery('#btPost').show();
    loader.hide();

});