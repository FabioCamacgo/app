<?php
    defined('_JEXEC') or die;
    defined('JPATH_BASE') or die;

    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    $conf = JFactory::getConfig();

    $host = $conf->get('host');
    $user = $conf->get('user');
    $password = $conf->get('password');
    $database = $conf->get('db');
    $dbprefix = $conf->get('dbprefix');


    $table = VirtualDeskSiteAlertaHelper::getOcorrenciasEspera($dbprefix);

    $primaryKey = 'Id_alerta';

    $columns = array(
        array( 'db' => 'dataCriacao',	                'dt' => 0 ),
        array( 'db' => 'categoria',                     'dt' => 1 ),
        array( 'db' => 'subcategoria',                  'dt' => 2 ),
        array( 'db' => 'descricao',		                'dt' => 3 ),
        array( 'db' => 'local',	                        'dt' => 4 ),
        array( 'db' => 'freguesia',	                    'dt' => 5 ),
        array( 'db' => 'estado',	                    'dt' => 6 ),
        array( 'db' => 'latitude',	                    'dt' => 7 ),
        array( 'db' => 'longitude',	                    'dt' => 8 )
    );

    $sql_details = array(
        'user' => $user,
        'pass' => $password,
        'db'   => $database,
        'host' => $host
    );

    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

    $dataFinal = json_encode($data,JSON_UNESCAPED_UNICODE);

    echo $dataFinal;
?>