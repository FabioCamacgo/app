<?php

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('novoalerta');
if ($resPluginEnabled === false) exit();


//GLOBAL SCRIPTS
$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/layouts/layout/scripts/layout.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/moment.min.js' . $addscript_end;

$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

$headScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;

$headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;


//BEGIN GLOBAL MANDATORY STYLES
//$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS  = '';
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/css/portfolio.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/css/cubeportfolio.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/layouts/layout/css/layout.min.css'. $addcss_end;
//$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/layouts/layout/css/themes/darkblue.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/layouts/layout/css/custom.min.css'. $addcss_end;

//Fileuploader
$headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;


// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
}


echo $headCSS;

$obParam      = new VirtualDeskSiteParamsHelper();

$linkPolitica = $obParam->getParamsByTag('linkPoliticaAlerta');
$idConcelhoAlerta = $obParam->getParamsByTag('alertaConcelho');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
$nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
$emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
$contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
$dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
$LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
$moradaMunicipio = $obParam->getParamsByTag('moradaMunicipio');
$codPostalMunicipio = $obParam->getParamsByTag('codPostalMunicipio');
$espera = $obParam->getParamsByTag('espera');

?>

<style>
    .fileuploader-popup {z-index: 99999 !important;}

    .novaOcorrencia{display:none;}
    #btPost{display:none;}

    .page-content-wrapper .page-content {margin-left: 0;}


    .lds-ellipsis {
        display: inline-block;
        position: relative;
        width: 64px;
        height: 64px;
    }

    .lds-ellipsis div {
        position: absolute;
        top: 27px;
        width: 11px;
        height: 11px;
        border-radius: 50%;
        background: #0b306b;
        animation-timing-function: cubic-bezier(0, 1, 1, 0);
    }

    .lds-ellipsis div:nth-child(1) {
        left: 6px;
        animation: lds-ellipsis1 0.6s infinite;
    }

    .lds-ellipsis div:nth-child(2) {
        left: 6px;
        animation: lds-ellipsis2 0.6s infinite;
    }

    .lds-ellipsis div:nth-child(3) {
        left: 26px;
        animation: lds-ellipsis2 0.6s infinite;
    }

    .lds-ellipsis div:nth-child(4) {
        left: 45px;
        animation: lds-ellipsis3 0.6s infinite;
    }

    @keyframes lds-ellipsis1 {
        0% {
            transform: scale(0);
        }
        100% {
            transform: scale(1);
        }
    }

    @keyframes lds-ellipsis3 {
        0% {
            transform: scale(1);
        }
        100% {
            transform: scale(0);
        }
    }

    @keyframes lds-ellipsis2 {
        0% {
            transform: translate(0, 0);
        }
        100% {
            transform: translate(19px, 0);
        }
    }
    .loader, .loader2{
        text-align: center;
    }

</style>

<?php

if (isset($_POST['submitForm'])) {
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $email2 = $_POST['emailConf'];
    $freguesias = $_POST['freguesias'];
    $fiscalid = $_POST['fiscalid'];
    $telefone = $_POST['telefone'];
    $ptosrefs= $_POST['ptosrefs'];
    $morada = $_POST['morada'];
    $sitio =$_POST['sitio'];
    $descricao = $_POST['descricao'];
    $categoria = $_POST['categoria'];
    $subcategoria = $_POST['vdmenumain'];
    $coordenadas = $_POST['coordenadas'];
    $splitCoord = explode(",", $coordenadas);
    $lat = $splitCoord[0];
    $long = $splitCoord[1];
    $veracid2 = $_POST['veracid2'];
    $politica2 = $_POST['politica2'];

    if($categoria == 'Escolher opção'){
        $categoria = '';
    }

    if($subcategoria == 'Escolher opção'){
        $subcategoria = '';
    }

    /*Valida Categoria*/
    if($categoria == Null){
        $errCategoria = 1;
    }

    /*Valida Subcategoria*/
    if($subcategoria == Null){
        $errSubcategoria = 1;
    }

    /*Valida Descrição*/
    if($descricao == Null){
        $errDescricao = 1;
    } else if(is_numeric($descricao)){
        $errDescricao = 2;
    }

    /*Valida Freguesia*/
    if($freguesias == Null){
        $errFreguesia = 1;
    }

    /*Valida Sítio*/
    if($sitio == Null){
        $errSitio = 1;
    } else if(is_numeric($sitio)){
        $errSitio = 2;
    }

    /*Valida Morada*/
    if($morada == Null){
        $errMorada = 1;
    } else if(is_numeric($morada)){
        $errMorada = 2;
    }

    /*Valida Pontos de Referência*/
    if($ptosrefs == Null){
        $errPtosrefs = 1;
    } else if(is_numeric($ptosrefs)){
        $errPtosrefs = 2;
    }

    /*Valida Nome*/
    if($nome == Null){
        $errNome = 1;
    }else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$nome)) {
        $errNome = 2;
    }

    /*Valida Telefone*/
    $TelefoneSplit=str_split($telefone);
    if($telefone == Null){
        $errTelefone = 1;
    } else if(!is_numeric($telefone) || strlen($telefone) != 9){
        $errTelefone = 2;
    } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
        $errTelefone = 2;
    } else if($TelefoneSplit[0] == 9){
        if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
            $errTelefone = 2;
        }
    }

    /*Valida Email*/

    $ExisteMail = VirtualDeskSiteAlertaHelper::seeEmailExiste($email);
    if($email == Null){
        $errEmail = 1;
    } else if($email2 == Null || $email != $email2){
        $errEmail = 2;
    }

    /*Valida Nif*/
    $nif=trim($fiscalid);
    $ignoreFirst=true;
    if (!is_numeric($nif) || strlen($nif)!=9) {
        $errNif = 1;
    } else {
        $nifSplit=str_split($nif);
        if (
            in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9))
            ||
            $ignoreFirst
        ) {
            $checkDigit=0;
            for($i=0; $i<8; $i++) {
                $checkDigit+=$nifSplit[$i]*(10-$i-1);
            }
            $checkDigit=11-($checkDigit % 11);
            if($checkDigit>=10) $checkDigit=0;
            if ($checkDigit==$nifSplit[8]) {

            } else {
                $errNif = 1;
            }
        } else {
            $errNif = 1;
        }
    }



    // dynamic_recaptcha_1
    $captcha                  = JCaptcha::getInstance($captcha_plugin);
    $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

    $errRecaptcha = 1;
    if((string)$recaptcha_response_field !='') {
        $jinput->set('g-recaptcha-response', $recaptcha_response_field);
        $resRecaptchaAnswer = $captcha->checkAnswer($recaptcha_response_field);
        $errRecaptcha = 0;
        if (!$resRecaptchaAnswer) {
            $errRecaptcha = 1;
        }
    }

    if($politica2 != 0 && $veracid2 != 0 && $errCategoria == 0 && $errSubcategoria == 0 && $errDescricao == 0 && $errFreguesia == 0 && $errSitio == 0 && $errMorada == 0 && $ptosrefs == 0 && $errNome == 0 && $errTelefone == 0 && $errEmail == 0 && $errNif == 0 && $errRecaptcha==0){


        /*Gerar Referencia UNICA de alerta*/
        $refExiste = 1;

        $refCat = VirtualDeskSiteAlertaHelper::getReferencia($categoria);
        $year = substr(date("Y"), -2);

        while($refExiste == 1){
            $refRandom = VirtualDeskSiteAlertaHelper::random_code();
            $referencia = $indicativoConcelho . '-' . $refCat . strtoupper($refRandom) . '-' . $year;
            $checkREF = VirtualDeskSiteAlertaHelper::CheckReferencia($referencia);
            if((int)$checkREF == 0){
                $refExiste = 0;
            }
        }
        /*END Gerar Referencia UNICA de alerta*/

        /*Save Ocorrencia*/
        $resSaveOcorrencia = VirtualDeskSiteAlertaHelper::saveNewAlerta($categoria, $referencia, $subcategoria, $descricao, $idConcelhoAlerta, $freguesias, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long);
        /*END Save Ocorrencia*/

        if($resSaveOcorrencia==true) :?>
            <style>
                #btPost{display:none !important;}
            </style>
            <div class="sucesso">
                <p>
                <div class="sucessIcon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10" stroke-width="2"/><path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg></div>
                <?php echo JText::_('COM_VIRTUALDESK_ALERTA_SUCESSO'); ?>
                </p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SUCESSO1'); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SUCESSO2'); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SUCESSO3'); ?></p>
                <p><?php echo JText::sprintf('COM_VIRTUALDESK_ALERTA_SUCESSO4',$nomeMunicipio); ?></p>
                <p><?php echo JText::sprintf('COM_VIRTUALDESK_ALERTA_SUCESSO5',$copyrightAPP); ?></p>
            </div>

            <?php

            /*Send Email*/

            $catName = VirtualDeskSiteAlertaHelper::getCatName($categoria);
            $subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
            $fregName = VirtualDeskSiteAlertaHelper::getFregName($freguesias);

            VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);

            /*END Send Email*/

            // ToDo : encriptar a referencia???
            echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');


            exit();

            // ELSE
            // Todo : Se der erro na gravação retorna erro para o ajax
        endif;

        //

    } else { ?>
        <script>
            dropError();
        </script>
        <div class="backgroundErro">
            <div class="erro" style="display:block;">
                <button id="fechaErro"><svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                        <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                            M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                            c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                            c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                            c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                            c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                            c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                            c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                            C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                    </svg></button>

                <h2>
                    <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTA_AVISO'); ?>
                </h2>

                <ol>
                    <?php
                    if($errCategoria == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_CATEGORIA_1') . '</li>';
                    }else{
                        $errCategoria = 0;
                    }

                    if($errSubcategoria == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_SUBCATEGORIA_1') . '</li>';
                    }else{
                        $errSubcategoria = 0;
                    }

                    if($errDescricao == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_DESCRICAO_1') . '</li>';
                    } else if($errDescricao == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_DESCRICAO_2') . '</li>';
                    }else{
                        $errDescricao = 0;
                    }

                    if($errFreguesia == 1) {
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_FREGUESIA_1') . '</li>';
                    }else{
                        $errFreguesia = 0;
                    }

                    if($errSitio == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_SITIO_1') . '</li>';
                    } else if($errSitio == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_SITIO_2') . '</li>';
                    }else{
                        $errSitio = 0;
                    }

                    if($errMorada == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_MORADA_1') . '</li>';
                    } else if($errMorada == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_MORADA_2') . '</li>';
                    }else{
                        $errMorada = 0;
                    }

                    if($errPtosrefs == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_PTOSREF_1') . '</li>';
                    } else if($errPtosrefs == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_PTOSREF_2') . '</li>';
                    }else{
                        $errPtosrefs = 0;
                    }

                    if($errNome == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_NOME_1') . '</li>';
                    } else if($errNome == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_NOME_2') . '</li>';
                    }else{
                        $errNome = 0;
                    }

                    if($errTelefone == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_TELEFONE_1') . '</li>';
                    } else if($errTelefone == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_TELEFONE_2') . '</li>';
                    }else{
                        $errTelefone = 0;
                    }

                    if($errEmail == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_1') . '</li>';
                    } else if($errEmail == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_2') . '</li>';
                    } else if($errEmail == 3){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_3') . '</li>';
                    } else if($errEmail == 4){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_4') . '</li>';
                    }else{
                        $errEmail = 0;
                    }

                    if($errNif == 1) {
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_NIF_1') . '</li>';
                    }else{
                        $errNif = 0;
                    }

                    if($veracid2 == 0) {
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_VERACIDADE') . '</li>';
                    }

                    if($politica2 == 0) {
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_POLITICA') . '</li>';
                    }

                    if($errRecaptcha == 1) {
                        echo '<li>Erro na verificação Não Sou um Robô (Recaptcha)</li>';
                    }else{
                        $errRecaptcha = 0;
                    }
                    ?>
                </ol>
            </div>
        </div>
    <?php }


}

?>

<div class="clearfix"> </div>

<div class="container">
<div class="page-container">
    <div class="page-content-wrapper">
        <div class="page-content" >

        <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php  ?></span>
            </div>
        </div>


            <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
            </div>

            <script>
                var MessageAlert = new function() {
                    this.getRequiredMissed = function (nErrors) {
                        if (nErrors == null)
                        { return(''); }
                        if(nErrors==1)
                        { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                        else  if(nErrors>1)
                        { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                            return (msg.replace("%s",nErrors) );
                        }
                    };
                }
            </script>

        <div class="portlet-body form">

            <div class="loader">
                <div class="lds-ellipsis">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>

            <div class="novaOcorrencia">

                <form id="new_alerta" action="" method="post" class="alerta-form" enctype="multipart/form-data" >
                    <legend class="section"><h3><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TITLE'); ?></h3></legend>

                    <!--   CATEGORIA   -->
                    <div class="form-group" id="cat">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL'); ?><span class="required">*</span></label>
                        <?php $categorias = VirtualDeskSiteAlertaHelper::getCategorias($fileLangSufix, $idConcelhoAlerta)?>
                        <div class="col-md-9">
                            <select name="categoria" required value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($categoria)){
                                    ?>
                                    <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                    <?php foreach($categorias as $rowStatus) : ?>
                                        <option value="<?php echo $rowStatus['ID_categoria']; ?>"
                                        ><?php echo $rowStatus['name_PT']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteAlertaHelper::getCatSelect($categoria, $idConcelhoAlerta) ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeCat = VirtualDeskSiteAlertaHelper::excludeCat($categoria, $idConcelhoAlerta)?>
                                    <?php foreach($ExcludeCat as $rowStatus) : ?>
                                        <option value="<?php echo $rowStatus['ID_categoria']; ?>"
                                        ><?php echo $rowStatus['name_PT']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <!--   SUBCATEGORIA   -->
                    <?php
                    // Carrega menusec se o id de menumain estiver definido.
                    $ListaDeMenuMain = array();
                    if(!empty($categoria)) {
                        if( (int) $categoria > 0) $ListaDeMenuMain = VirtualDeskSiteAlertaHelper::getAlertaSubcategoria($categoria, $idConcelhoAlerta);
                    }
                    ?>
                    <div id="blocoMenuMain" class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_SUBCATEGORY_LABEL' ); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <select name="vdmenumain" id="vdmenumain" value="<?php echo $subcategoria;?>" required
                                <?php
                                if(empty($categoria)) {
                                    echo 'disabled';
                                }
                                ?>
                                    class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($subcategoria)){
                                    ?>
                                    <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                    <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                                        <option value="<?php echo $rowMM['id']; ?>"
                                        ><?php echo $rowMM['name']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $subcategoria; ?>"><?php echo VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria, $idConcelhoAlerta);?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeSubCat = VirtualDeskSiteAlertaHelper::excludesubCat($categoria, $subcategoria, $idConcelhoAlerta);?>
                                    <?php foreach($ExcludeSubCat as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['name']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>

                        </div>
                    </div>


                    <!--   DESCRIÇAO   -->
                    <div class="form-group" id="desc">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DESCRIPTION'); ?></label>
                        <div class="col-md-9">
                                    <textarea required class="form-control" rows="7"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_DESCRIPTION_LABEL'); ?>"
                                              name="descricao" id="descricao" maxlength="2000"><?php echo $descricao; ?></textarea>
                        </div>
                    </div>


                    <!--   FREGUESIAS   -->
                    <div class="form-group" id="freg">
                        <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_FREGUESIAS_LABEL') ?><span class="required">*</span></label>
                        <?php $AlertaFreguesias = VirtualDeskSiteAlertaHelper::getAlertaFreguesia($idConcelhoAlerta)?>
                        <div class="col-md-9">
                            <select name="freguesias" value="<?php echo $freguesias; ?>" id="freguesias" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($freguesias)){
                                    ?>
                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                    <?php foreach($AlertaFreguesias as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['freguesia']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $freguesias; ?>"><?php echo VirtualDeskSiteAlertaHelper::getFregName($freguesias) ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeFreg = VirtualDeskSiteAlertaHelper::excludeFreguesia($idConcelhoAlerta, $freguesias)?>
                                    <?php foreach($ExcludeFreg as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                        ><?php echo $rowWSL['freguesia']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <!--   SÍTIO   -->
                    <div class="form-group" id="place">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SITIO'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_SITIO_LABEL'); ?>"
                                   name="sitio" id="sitio" maxlength="40" value="<?php echo $sitio; ?>"/>
                        </div>
                    </div>


                    <!--   MORADA   -->
                    <div class="form-group" id="address">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MORADA'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_MORADA_LABEL'); ?>"
                                   name="morada" id="morada" maxlength="100" value="<?php echo $morada; ?>"/>
                        </div>
                    </div>


                    <!--   PONTOS DE REFERÊNCIA   -->
                    <div class="form-group" id="ptosRef">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_REFS'); ?><span class="required">*</span></label>
                        <div class="col-md-9"><textarea required class="form-control" rows="5" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_REFS_LABEL'); ?>"
                                                        name="ptosrefs" id="ptosrefs" maxlength="2000"><?php echo htmlentities($ptosrefs); ?></textarea>
                        </div>
                    </div>


                    <!--   MAPA   -->
                    <div class="form-group" id="mapa">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MAPA'); ?></label>
                        <div class="col-md-9">
                            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                            <input type="hidden" required class="form-control"
                                   name="coordenadas" id="coordenadas"
                                   value="<?php echo htmlentities($this->data->$coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
                        </div>
                    </div>


                    <!--   Upload Imagens   -->
                    <div class="form-group" id="uploadField">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_UPLOAD'); ?></label>
                        <div class="col-md-9">
                            <div class="file-loading">
                                <input type="file" name="fileupload[]" id="fileupload" multiple>
                            </div>
                            <div id="errorBlock" class="help-block"></div>
                            <input type="hidden" id="VDAjaxReqProcRefId">

                        </div>
                    </div>



                    <legend><h3 class="dadosPessoais"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DADOSPESSOAIS'); ?></h3></legend>


                    <!--   NOME COMPLETO   -->
                    <div class="form-group" id="name">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_NOME'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_NOME_LABEL'); ?>"
                                   name="nome" id="nome" value="<?php echo htmlentities($nome, ENT_QUOTES, 'UTF-8'); ?>" maxlength="40"/>
                        </div>
                    </div>

                    <!--   EMAIL   -->

                    <div class="form-group" id="mail">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_LABEL'); ?>"
                                   name="email" id="email" value="<?php echo $email; ?>"/>
                        </div>
                    </div>


                    <!--   VERIFICAÇÃO DE EMAIL   -->
                    <div class="form-group" id="mail2">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_REPEAT_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_REPEAT_LABEL'); ?>"
                                   name="emailConf" id="emailConf" value="<?php echo $email2; ?>"/>
                        </div>
                    </div>

                    <!--   NIF   -->
                    <div class="form-group" id="nif">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_FISCALID_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_FISCALID_LABEL'); ?>"
                                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                        </div>
                    </div>

                    <!--   TELEFONE  -->
                    <div class="form-group" id="telef">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TELEFONE_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_TELEFONE_LABEL'); ?>"
                                   name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                        </div>
                    </div>



                    <legend><h3 class="dadosPessoais"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_VERIFICACOES'); ?></h3></legend>

                    <!--   Veracidade -->
                    <div class="form-group" id="Veracidade">

                        <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_VERACIDADE'); ?><span class="required">*</span></label>

                        <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
                    </div>

                    <script>

                        document.getElementById('veracid').onclick = function() {
                            // access properties using this keyword
                            if ( this.checked ) {
                                document.getElementById("veracid2").value = 1;
                            } else {
                                document.getElementById("veracid2").value = 0;
                            }
                        };
                    </script>

                    <!--   Politica de privacidade  -->
                    <div class="form-group" id="PoliticaPrivacidade">

                        <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
                        <label class="col-md-3 control-label">Tomei conhecimento das condições editadas em  <a href="<?php echo $linkPolitica; ?>" target="_blank">Privacidade, Termos, RGPD e Cookies</a> e concordo com as mesmas. <span class="required">*</span></label>

                        <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
                    </div>

                    <script>

                        document.getElementById('politica').onclick = function() {
                            // access properties using this keyword
                            if ( this.checked ) {
                                document.getElementById("politica2").value = 1;
                            } else {
                                document.getElementById("politica2").value = 0;
                            }
                        };
                    </script>


                    <?php if ($captcha_plugin!='0') : ?>
                        <div class="form-group" >
                            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                            $field_id = 'dynamic_recaptcha_1';
                            print $captcha->display($field_id, $field_id, 'g-recaptcha');
                            ?>
                            <input type="hidden" class="form-control-feedback"  name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                        </div>
                    <?php endif; ?>



                    <div class="form-actions" method="post">
                        <input type="submit" name="submitForm" id="btPost" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
                    </div>

                    <!--<?php echo JHtml::_('form.token'); ?>-->

                </form>

            </div>


        </div>
    </div>

        </div>
        </div>
</div>
</div>

<?php echo $headScripts; ?>
<?php echo $footerScripts; ?>
<?php echo $recaptchaScripts; ?>


<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'novoalerta/'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
    var iconpath = '<?php echo JUri::base() . 'images/cmps/alerta/Espera.png'; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Alerta/alerta_formAPP.js.php');
    ?>
</script>