<?php
defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$obParam      = new VirtualDeskSiteParamsHelper();

$pinMapa = $obParam->getParamsByTag('pinMapa');
$pathDelimitacaoMapa = $obParam->getParamsByTag('pathDelimitacaoMapa');
?>

var MapsGoogle = function () {

    var mapMarker = function () {

        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });


        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });

        map.setZoom(13);

        GMaps.on('click', map.map, function(event) {

            map.removeMarkers();

            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            jQuery('#coordenadas').val(lat + ',' + lng);

            map.addMarker({
                lat: lat,
                lng: lng,
                //title: 'Marker #' + index,
                icon: iconPath,
                infoWindow: {
                content : ''
                }

            });
        });

        <?php require_once (JPATH_SITE . $pathDelimitacaoMapa); ?>

    }

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };

}();


var LatToSet = 32.70889142068046;
var LongToSet = -17.09796831092251;

/*
* Inicialização
*/

jQuery(document).ready(function() {

    setTimeout(function(){
        var InputLatLong = jQuery('#coordenadas').val();


        if( InputLatLong!=undefined && InputLatLong!="") {
            var ArrayLatLong = InputLatLong.split(",");
            if(ArrayLatLong.length == 2) {
                LatToSet  =  ArrayLatLong[0];
                LongToSet =  ArrayLatLong[1];

            }
        }

        if( jQuery('#gmap_marker').length ) {
            MapsGoogle.init()
        }
    }, 800);

});