<?php
defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$obParam      = new VirtualDeskSiteParamsHelper();
$concluida       = $obParam->getParamsByTag('concluida');
$analise       = $obParam->getParamsByTag('analise');
$espera       = $obParam->getParamsByTag('espera');
$pathDelimitacaoMapa = $obParam->getParamsByTag('pathDelimitacaoMapa');

?>

var iconPathEspera = '<?php echo JUri::base() . $espera; ?>';
var iconPathAnalise = '<?php echo JUri::base() . $analise; ?>';
var iconPathConcluido = '<?php echo JUri::base() . $concluida; ?>';

var MapsGoogle = function () {

    // Default
    var LatToSet = 32.701070;
    var LongToSet =   -17.104355;

    var mapMarker = function () {

        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });

        <?php foreach($AlertaAbertosMaps4Manager as $rowMarker) : ?>
        <?php if(!empty($rowMarker['lat']) && !empty($rowMarker['long'])) : ?>

        map.addMarker({
            lat: <?php echo $rowMarker['lat']; ?>,
            lng: <?php echo $rowMarker['long']; ?>,
            title:'<?php echo $rowMarker['cat'] ." | ". $rowMarker['estado']  ; ?>',
            icon:
            <?php switch((int)$rowMarker['idestado']) {
                case 1:
                    echo 'iconPathEspera';
                    break;
                case 2:
                    echo 'iconPathAnalise';
                    break;
                case 3:
                    echo 'iconPathConcluido';
                    break;}
            ?> ,
            infoWindow: {
                content : '<?php echo 'Cat: '. $rowMarker['cat'] ."<br>Est: ". $rowMarker['estado']  ; ?>'
            }
        });
        <?php endif; ?>
        <?php endforeach; ?>

        map.setZoom(13);

        <?php require_once (JPATH_SITE . $pathDelimitacaoMapa); ?>

    };

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };

}();



/*
* Inicialização
*/
jQuery(document).ready(function() {

    if( jQuery('#gmap_marker').length ) {
        MapsGoogle.init()
    }

});