<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('historico');
    if ($resPluginEnabled === false) exit();


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/myscripts.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

    $DataTCSS      = $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.css'. $addcss_end;
    $DataTCSS     .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'. $addcss_end;

    $DataTScripts1 = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $DataTScripts2 = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $DataTScripts3 = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;

    echo $headCSS;
    echo $DataTCSS;

?>



<div id="FP" class="contentHist">

    <!--  Historico de Ocorrencias por NIF  -->

    <div class="headhistorico"><h2><?php echo JText::_('COM_VIRTUALDESK_HISTORICO_TITULO'); ?></h2></div>

    <table class="table table-striped table-bordered table-hover order-column" id="tabela_historico_Ocorrencias">
        <thead>
        <tr>
            <th><?php echo JText::_('COM_VIRTUALDESK_HISTORICO_DATAOCORRENCIA'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_HISTORICO_REFERENCIA'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_HISTORICO_CATEGORY'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_HISTORICO_SUBCATEGORY'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_HISTORICO_FREGUESIA'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_HISTORICO_ESTADO'); ?></th>
        </tr>
        </thead>
    </table>

    <script type="text/javascript">
        var TableDatatablesManaged = function () {

            var initTable2 = function () {

                var table = jQuery('#tabela_historico_Ocorrencias');


                // begin first table
                var oTable = table.dataTable({

                    // set the initial value
                    "processing": true,
                    "serverSide": true,
                    "ajax": "<?php echo $baseurl.'historico/?VDAjaxReq2DataTables=1&ref='. $refNoGET; ?>",
                    "oLanguage": {
                        "sProcessing": "Aguarde enquanto os dados s&atilde;o carregados ...",
                        "sLengthMenu": "Mostrar _MENU_ registos por p&aacute;gina",
                        "sZeroRecords": "Nenhum registo correspondente ao crit&eacute;rio encontrado",
                        "sInfoEmtpy": "Exibindo 0 a 0 de 0 registos",
                        "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registos",
                        "sInfoFiltered": "",
                        "sSearch": "Procurar",
                        "oPaginate": {
                            "sFirst":    "Primeiro",
                            "sPrevious": "Anterior",
                            "sNext":     "Pr&oacute;ximo",
                            "sLast":     "Último"
                        }
                    },

                    "columnDefs": [
                        {
                            "className": "dt-left",
                            'targets': [1,2,3,4,5], // column index (start from 0)
                            'orderable': false, // set orderable false for selected columns
                            //"targets": [2]
                        },

                        {
                            "targets": 0,
                            "data": 0,
                            "render": function ( data, type, row, meta) {
                                //console.log('data=' + data);
                                //console.log('row=' + row[0]);

                                var retVal = row[0];

                                return (retVal);
                            }
                        }

                    ],
                    "order": [[0, "desc"]],
                });

            }


            return {

                //main function to initiate the module
                init: function () {
                    if (!jQuery().dataTable) {
                        return;
                    }
                    initTable2();
                }
            };
        }();


        jQuery(document).ready(function() {

            jQuery.when(
                jQuery.getScript('<?php echo  $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js'; ?>'),
                jQuery.getScript('<?php echo  $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js'; ?>'),
                jQuery.getScript('<?php echo  $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'; ?>'),
                jQuery.Deferred(function( deferred ){
                    jQuery( deferred.resolve );
                })
            ).done(function(){
                //place your code here, the scripts are all loaded
                TableDatatablesManaged.init();
            });

        });

    </script>

</div>


<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
    echo $DataTScripts1;
    echo $DataTScripts2;
    echo $DataTScripts3;
?>
