var Alertanovo = function() {

    var handleAlerta = function() {

        jQuery('#new_alerta').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new_alerta').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new_alerta').validate().form()) {
                    jQuery('#new_alerta').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleAlerta();
        }
    };

}();


/*
 * Inicialização do Select 2
 */
var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater').each(function(){
                //console.log('a')
                jQuery(this).repeater({

                    show: function () {

                        if(checkIfFileLimtiHasReached()===true)
                        {
                            jQuery('a.mt-repeater-add').hide();
                            var AlertMsg = jQuery('#FileListNumReachedAlert').clone();
                            jQuery('#FileListNumReachedAlertShow').html(AlertMsg);
                            jQuery('#FileListNumReachedAlertShow > div').show();
                            jQuery('#vd-filelist-repeater').find('div.mt-repeater-item.vd-filelist-repeater-item').last().remove();

                        }
                        else
                        {
                            jQuery(this).slideDown();
                        }
                    },

                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            jQuery(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });
        }

    };

}();




function checkIfFileLimtiHasReached()
{

    var NumFiles  = Number(jQuery('#vd-filelist').find('.vd-filelist-item').size());
    var NumRepeat = Number(jQuery('#vd-filelist-repeater').find('.vd-filelist-repeater-item').size());
    var SumNumFilesNumRepeat = Number(NumFiles+NumRepeat);

    console.log(param_contactus_filelimitnum +' - '+ NumFiles +' , '+ NumRepeat + ' - ' + SumNumFilesNumRepeat);

    if( SumNumFilesNumRepeat <= param_contactus_filelimitnum ){
        return(false)
    }
    return(true);
}



/*
* Inicialização da validação
*/

jQuery(document).ready(function() {


    // Validações
    Alertanovo.init();

    // Select 2
    ComponentsSelect2.init();

    // Repeater
    FormRepeater.init();


    jQuery('.vd-button-delete').click(function(evt) {
        evt.preventDefault();
        var sa_title = jQuery(this).data('title');
        var sa_message = jQuery(this).data('message');
        var sa_type = jQuery(this).data('type');
        var sa_allowOutsideClick = jQuery(this).data('allow-outside-click');
        var sa_showConfirmButton = jQuery(this).data('show-confirm-button');
        var sa_showCancelButton = jQuery(this).data('show-cancel-button');
        var sa_closeOnConfirm = jQuery(this).data('close-on-confirm');
        var sa_closeOnCancel = jQuery(this).data('close-on-cancel');
        var sa_confirmButtonText = jQuery(this).data('confirm-button-text');
        var sa_cancelButtonText = jQuery(this).data('cancel-button-text');
        var sa_confirmButtonClass = jQuery(this).data('confirm-button-class');
        var sa_cancelButtonClass = jQuery(this).data('cancel-button-class');

        var vd_url_delete = jQuery(this).data('vd-url-delete');

        swal({  title: sa_title,
                text: sa_message,
                type: sa_type,
                allowOutsideClick: sa_allowOutsideClick,
                showConfirmButton: sa_showConfirmButton,
                showCancelButton: sa_showCancelButton,
                confirmButtonClass: sa_confirmButtonClass,
                cancelButtonClass: sa_cancelButtonClass,
                closeOnConfirm: sa_closeOnConfirm,
                closeOnCancel: sa_closeOnCancel,
                confirmButtonText: sa_confirmButtonText,
                cancelButtonText: sa_cancelButtonText
            },
            function(isConfirm){
                if (isConfirm){
                    window.location.href = vd_url_delete;
                }
            });
    });




    jQuery("#vdwebsitelist").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */
        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var categoria = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(categoria == null || categoria == '') {

            var select = jQuery('#vdmenumain');
            select.find('option').remove();
            select.prop('disabled', true);
            select.append(jQuery('<option>').text('Escolher Opção'));

        } else {
            var dataString = "categoria="+categoria; /* STORE THAT TO A DATA STRING */

            // show loader
            App.unblockUI({ target:'#blocoMenuMain',  animate: true});

            jQuery.ajax({
                url: websitepath,
                data:'m=alerta_getsubCat&categoria=' + categoria ,

                success: function(output) {

                    var select = jQuery('#vdmenumain');
                    select.find('option').remove();

                    if (output == null || output=='') {
                        select.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    //App.unblockUI('#blocoMenuMain');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoMenuMain');
                }
            });
        }

    });




    jQuery("#vdmenumain").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */
        var vdmenumain = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */
        var dataString = "vdmenumain="+vdmenumain; /* STORE THAT TO A DATA STRING */

        // show loader
        App.blockUI({ target:'#blocoMenuSec',  animate: true});
        App.blockUI({ target:'#blocoMenuSec2',  animate: true});

        jQuery.ajax({
            url: 'index.php',
            data:'option=com_virtualdesk&task=contactus.getmenusecajax&vdmenumain=' + vdmenumain,
            success: function(output) {

                var select = jQuery('#vdmenusec');
                select.find('option').remove();

                if (output == null || output=='') {
                    select.prop('disabled', true);
                }
                else {
                    select.prop('disabled', false);
                    select.append(jQuery('<option>'));
                    jQuery.each(JSON.parse(output), function (i, obj) {
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                    });
                }

                var select03 = jQuery('#vdmenusec2');
                select03.find('option').remove();
                select03.prop('disabled', true);

                // hide loader
                App.unblockUI('#blocoMenuSec');
                App.unblockUI('#blocoMenuSec2');

            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status +  ' ' + thrownError);
                // hide loader
                select.find('option').remove();
                select.prop('disabled', true);
                App.unblockUI('#blocoMenuSec');
                App.unblockUI('#blocoMenuSec2');
            }
        });

    });


    jQuery("#vdmenusec").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */
        var vdmenusec = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */
        var dataString = "vdmenusec="+vdmenusec; /* STORE THAT TO A DATA STRING */

        // show loader
        App.blockUI({ target:'#blocoMenuSec2',  animate: true});

        jQuery.ajax({
            url: 'index.php',
            data:'option=com_virtualdesk&task=contactus.getmenusec2ajax&vdmenusec=' + vdmenusec,
            success: function(output) {

                var select = jQuery('#vdmenusec2');
                select.find('option').remove();

                if (output == null || output=='') {
                    select.prop('disabled', true);
                }
                else {
                    select.prop('disabled', false);
                    select.append(jQuery('<option>'));
                    jQuery.each(JSON.parse(output), function (i, obj) {
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                    });
                }

                // hide loader
                App.unblockUI('#blocoMenuSec2');

            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status +  ' ' + thrownError);
                // hide loader
                select.find('option').remove();
                select.prop('disabled', true);
                App.unblockUI('#blocoMenuSec2');
            }
        });

    });






});