<?php
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
$obParam              = new VirtualDeskSiteParamsHelper();
$arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('ultimasocorrencias');
if ($resPluginEnabled === false) exit();


//GLOBAL SCRIPTS
$headScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;

//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

$DataTCSS      = $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.css'. $addcss_end;
$DataTCSS     .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'. $addcss_end;

echo $headCSS;
echo $DataTCSS;


?>

<!-- Conteúdo da Página -->
<div id="UltimasOcorrencias" class="content">

    <!--<div class="headhistorico"><h2><?php echo JText::_('COM_VIRTUALDESK_ALERTA_ULTIMASOCORRENCIAS_TITULO'); ?></h2></div>-->


    <div class="m-datatable m-datatable--default m-datatable--brand">

        <table class="table table-striped table-bordered table-hover" id="tabela_ocorrencias_espera">
            <thead>
            <tr>
                <th><?php echo JText::_('COM_VIRTUALDESK_TABELA_DATAOCORRENCIA'); ?></th>
                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL'); ?></th>
                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SUBCATEGORY_LABEL'); ?></th>
                <th><?php echo JText::_('COM_VIRTUALDESK_TABELA_OCORRENCIA'); ?></th>
                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MORADA'); ?></th>
                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_FREGUESIAS_LABEL'); ?></th>
                <th><?php echo JText::_('COM_VIRTUALDESK_TABELA_ESTADO'); ?></th>
            </tr>
            </thead>
        </table>

        <script type="text/javascript">
            var TableDatatablesManaged = function () {

                var initTable2 = function () {

                    var table = jQuery('#tabela_ocorrencias_espera');


                    // begin first table
                    var oTable = table.dataTable({

                        // set the initial value
                        "processing": true,
                        "serverSide": true,
                        "dom": 'frltip',
                        "oLanguage": {
                            "sProcessing": "Aguarde enquanto os dados s&atilde;o carregados ...",
                            "sLengthMenu": "Mostrar _MENU_ registos por p&aacute;gina",
                            "sZeroRecords": "Nenhum registo correspondente ao crit&eacute;rio encontrado",
                            "sInfoEmtpy": "Exibindo 0 a 0 de 0 registos",
                            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registos",
                            "sInfoFiltered": "",
                            "sSearch": "Procurar",
                            "oPaginate": {
                                "sFirst":    "Primeiro",
                                "sPrevious": "Anterior",
                                "sNext":     "Pr&oacute;ximo",
                                "sLast":     "Último"
                            }
                        },
                        "columnDefs": [
                            {
                                "className": "dt-left",
                                'targets': [1,2,3,4,5,6], // column index (start from 0)
                                'orderable': false, // set orderable false for selected columns
                            },

                            {
                                "targets": 0,
                                "data": 0,
                                "render": function ( data, type, row, meta) {

                                    var retVal = row[0];

                                    return (retVal);
                                }
                            }

                        ],
                        "order": [[0, "desc"]],
                        "ajax": "<?php echo $baseurl.'ultimasocorrencias/?VDAjaxReq2DataTables=1'; ?>",

                    });


                }


                return {

                    //main function to initiate the module
                    init: function () {
                        if (!jQuery().dataTable) {
                            return;
                        }
                        initTable2();
                    }
                };
            }();


            jQuery(document).ready(function() {

                jQuery.when(
                    jQuery.getScript('<?php echo  $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js'; ?>'),
                    jQuery.getScript('<?php echo  $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js'; ?>'),
                    jQuery.getScript('<?php echo  $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'; ?>'),
                    jQuery.Deferred(function( deferred ){
                        jQuery( deferred.resolve );
                    })
                ).done(function(){
                    TableDatatablesManaged.init();
                });

            });

        </script>
    </div>

    <?php
    $AlertaAbertosMaps4Manager = VirtualDeskSiteAlertaHelper::getAlertaAbertosMaps4Plugin (10);
    ?>

    <div class="w100">
        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>
    </div>

</div>


<!-- END Conteúdo da Página -->

<?php
echo $headScripts;
echo $footerScripts;
echo ('<script>');
require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Alerta/alerta_abertos_maps4manager.js.php');
echo ('</script>');
?>

