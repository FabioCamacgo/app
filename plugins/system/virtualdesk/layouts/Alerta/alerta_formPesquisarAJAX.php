<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('formpesquisa');
    if ($resPluginEnabled === false) exit();

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $espera       = $obParam->getParamsByTag('espera');
    $corEspera = $obParam->getParamsByTag('corEspera');
    $corAnalise = $obParam->getParamsByTag('corAnalise');
    $corConcluido = $obParam->getParamsByTag('corConcluido');



    $link = $jinput->get('link','' ,'string');

    $ref = explode("?ref=", $link);

    $PesquisaReferencia = base64_decode($ref[1]);


    if(isset($_POST['Pesquisar'])){
        $PesquisaReferencia = $_POST['PesquisaReferencia'];
    }

?>


<div class="search-container">
    <form action="" method="post">
        <input type="text" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_TITLE_PESQUISAR');?>" id="PesquisaReferencia" name="PesquisaReferencia" value = "<?php echo $PesquisaReferencia;?>">
        <input type="submit" id="Pesquisar" name="Pesquisar" value="Pesquisar" style="display:none;">
    </form>
</div>


<?php
    if(!empty($PesquisaReferencia)){
        if(preg_match('/^[  a-zA-Z]+/', $PesquisaReferencia)){


            $ProcuraResultados = VirtualDeskSiteAlertaHelper::getresultados($PesquisaReferencia);

            if((int)$ProcuraResultados == 0){
                ?>
                    <div class="warning">
                        <img src="<?php echo JUri::base() . 'images/cmps/alerta/warning.png'; ?>" alt="warning" title="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_AVISO');?>"/>

                        <?php echo JText::_('COM_VIRTUALDESK_ALERTA_PESQUISA_VAZIO'); ?>
                    </div>
                <?php

             } else {
                foreach ($ProcuraResultados as $rowStatus) :?>

                    <?php $coordenadas = $rowStatus['latitude'] . ',' . $rowStatus['longitude'];?>

                    <div class="headpesquisa">
                        <div class="w66">
                            <?php echo JText::_('COM_VIRTUALDESK_PESQUISA_OCORRENCIA'); ?> <span class="refer"><?php echo $rowStatus['codOco'];?></span>
                        </div>
                        <div class="w33">
                            <form action="" class="seeHistorico" method="post">
                                <input type="hidden" id="userNIF" name="userNIF" value="<?php echo $rowStatus['nif'];?>">
                                <a href="<?php echo $nomeMenu;?>?ref=<?php echo base64_encode($PesquisaReferencia); ?>" class="verhistorico"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_VERHISTORICO'); ?></a>
                            </form>

                        </div>

                    </div>

                    <div class="corpoResultado">

                        <div class="w100">

                            <div class="dadosOcorrencia">

                                <h2><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_SECCAO_DADOS'); ?></h2>

                                <div class="w50">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_CATEGORIA'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['categoria'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {
                                            echo $rowStatus['categoria'];
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="w50">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_SUBCATEGORIA'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['subcategoria'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {
                                            echo $rowStatus['subcategoria'];
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="w100">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_DESCRICAO'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['descricao'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {
                                            echo $rowStatus['descricao'];
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="w100">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_OBSERVACOES'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['observacoes'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {
                                            echo $rowStatus['observacoes'];
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="w50">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_DATA_OCORRENCIA'); ?></div>
                                    <div class="result">

                                        <?php
                                        $pieces = explode("-", $rowStatus['data_criacao']);
                                        ?>
                                        <td><?php echo $pieces[2] . '-' . $pieces[1] . '-' . $pieces[0] ?></td>
                                    </div>
                                </div>

                                <div class="w50">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_ESTADO'); ?></div>
                                    <div class="result" style="margin-top: 4px;">

                                        <?php
                                        $getIdEstado = VirtualDeskSiteAlertaHelper::getIdEstado($rowStatus['estado']);

                                        if($getIdEstado == 1){
                                            $colorEstado = $corEspera;
                                        } else if($getIdEstado == 2){
                                            $colorEstado = $corAnalise;
                                        } else if($getIdEstado == 3){
                                            $colorEstado = $corConcluido;
                                        }
                                        ?>
                                        <span style="background:<?php echo $colorEstado;?>; padding: 4px 10px; color: #fff; border-radius: 3px;"><?php echo $rowStatus['estado']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="w100">
                            <div class="dadosLocalizacao">

                                <h2><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_SECCAO_LOCALIZACAO'); ?></h2>


                                <div class="w50">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_FREGUESIA'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['freguesia'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {
                                            echo $rowStatus['freguesia'];
                                        }
                                        ?>
                                    </div>
                                </div>


                                <div class="w50">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_GPS'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['latitude']) || empty($rowStatus['longitude'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {?>
                                            <div class="result1"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_LATITUDE'); ?> : <span> <?php echo $rowStatus['latitude']; ?></span></div>
                                            <div class="result2"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_LONGITUDE'); ?> : <span> <?php echo $rowStatus['longitude']; ?></span></div>
                                        <?php } ?>

                                    </div>
                                </div>

                                <div class="w100">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_LOCAL'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['local'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {
                                            echo $rowStatus['local'];
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="w100">
                                    <div class="item"><?php echo JText::_('COM_VIRTUALDESK_PESQUISA_PONTOS_REFERENCIA'); ?></div>
                                    <div class="result">
                                        <?php
                                        if(empty($rowStatus['pontos_referencia'])){
                                            echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                                        } else {
                                            echo $rowStatus['pontos_referencia'];
                                        }
                                        ?>
                                    </div>
                                </div>


                                <?php
                                    if(!empty($rowStatus['latitude']) || !empty($rowStatus['longitude'])){
                                        ?>
                                        <div class="w100">
                                            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                            <input type="hidden" required class="form-control"
                                                   name="coordenadas" id="coordenadas"
                                                   value="<?php echo htmlentities($coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>

                    </div>
                <?php endforeach;
            }
        } else{
            ?>
            <div class="warning"> <img src="<?php echo JUri::base() . $warning; ?>" alt="warning" title="<?php echo JTEXT_('COM_VIRTUALDESK_ALERTA_AVISO');?>"/>
                <?php echo JText::_('COM_VIRTUALDESK_ALERTA_PESQUISA_VAZIO'); ?>
            </div>
            <?php
        }
    }
?>

<?php
    echo $headScripts;
    echo $footerScripts;
?>

<script>
    var iconPath = '<?php echo JUri::base() . $espera; ?>';

    <?php
        if(!empty($rowStatus['latitude']) || !empty($rowStatus['longitude'])){
            require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Alerta/edit_maps.js.php');
        }
    ?>
</script>