<?php
    defined('_JEXEC') or die;
    defined('JPATH_BASE') or die;

    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');

    $conf = JFactory::getConfig();

    $host = $conf->get('host');
    $user = $conf->get('user');
    $password = $conf->get('password');
    $database = $conf->get('db');
    $dbprefix = $conf->get('dbprefix');

    $table = VirtualDeskSiteAlertaHelper::gethistorico($nifUser, $dbprefix);

    $primaryKey = 'data_criacao';

    $columns = array(
        array( 'db' => 'data_criacao',      'dt' => 0 ),
        array( 'db' => 'codOco',            'dt' => 1 ),
        array( 'db' => 'categoria',         'dt' => 2 ),
        array( 'db' => 'subcategoria',      'dt' => 3 ),
        array( 'db' => 'freguesia',         'dt' => 4 ),
        array( 'db' => 'estado',            'dt' => 5 )
    );

    $sql_details = array(
        'user' => $user,
        'pass' => $password,
        'db'   => $database,
        'host' => $host
    );

    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

    $dataFinal = json_encode($data,JSON_UNESCAPED_UNICODE);

    echo $dataFinal;
?>