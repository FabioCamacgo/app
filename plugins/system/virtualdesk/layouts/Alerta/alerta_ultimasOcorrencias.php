<?php

defined('JPATH_BASE') or die;
defined('_JEXEC') or die;


JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
$obParam              = new VirtualDeskSiteParamsHelper();
$arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

$jinput = JFactory::getApplication()->input;

$isVDAjaxReq = $jinput->get('isVDAjaxReq');
$isVDAjaxReq2DataTables = $jinput->get('VDAjaxReq2DataTables','',string);

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('ultimasocorrencias');
if ($resPluginEnabled === false) exit();

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$config          = JFactory::getConfig();
$sitename        = $config->get('sitename');
$sitedescription = $config->get('sitedescription');
$sitetitle       = $config->get('sitetitle');
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$logoFile        = $templateParams->get('logoFile');
$fluidContainer  = $config->get('fluidContainer');
$page_heading    = $config->get('page_heading');
$baseurl         = JUri::base();
$rooturl         = JUri::root();
$captcha_plugin  = JFactory::getConfig()->get('captcha');
$templateName    = 'virtualdesk';
$labelseparator = ' : ';
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';

$obParam      = new VirtualDeskSiteParamsHelper();
$imgPath       = $obParam->getParamsByTag('warning');
$concluida       = $obParam->getParamsByTag('concluida');
$analise       = $obParam->getParamsByTag('analise');
$espera       = $obParam->getParamsByTag('espera');


// Idiomas
$lang = JFactory::getLanguage();
$extension = 'com_virtualdesk';
$base_dir = JPATH_SITE;
//$language_tag = $lang->getTag(); // loads the current language-tag
$jinput = JFactory::getApplication('site')->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}


$obPlg      = new VirtualDeskSitePluginsHelper();

if( $isVDAjaxReq2DataTables == '1' ) {
    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ultimasOcorrenciasAJAX2DataTables.php');
    exit();
}


if($isVDAjaxReq !=1){
    $layoutPath = $obPlg->getPluginLayoutAtivePath('ultimasocorrencias','form');
    require_once(JPATH_SITE . $layoutPath);

} else {
    $layoutPath = $obPlg->getPluginLayoutAtivePath('ultimasocorrencias','formajax');
    require_once(JPATH_SITE . $layoutPath);
}

if((string)$layoutPath=='') {
    // Se n達o carregar o caminho do layout sai com erro...
    echo (JText::_('COM_VIRTUALDESK_LOADACTIVELAYOUT_ERROR'));
    exit();
}
require_once(JPATH_SITE . $layoutPath);

?>


