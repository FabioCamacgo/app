<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('sliderocorrencias');
    if ($resPluginEnabled === false) exit();


    $obParam      = new VirtualDeskSiteParamsHelper();

    $corEspera = $obParam->getParamsByTag('corEspera');
    $corAnalise = $obParam->getParamsByTag('corAnalise');
    $corConcluido = $obParam->getParamsByTag('corConcluido');
    $alertaWebsiteName = $obParam->getParamsByTag('alertaWebsiteName');
    $alertaConcelho = $obParam->getParamsByTag('alertaConcelho');
    $nomeConcelho = VirtualDeskSiteAlertaHelper::getConcelhoName($alertaConcelho);
    $Tickermensagem1 = $obParam->getParamsByTag('Tickermensagem1');
    $Tickermensagem2 = $obParam->getParamsByTag('Tickermensagem2');
    $Tickermensagem3 = $obParam->getParamsByTag('Tickermensagem3');

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Alerta/breaking-news-ticker.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Alerta/breaking-news-ticker.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Alerta/alerta_slideOcorrencias.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
    echo $headScripts;


    $ListaOcorrencias = VirtualDeskSiteAlertaHelper::getUltimasOcorrenciasSlider($fileLangSufix);
    $numOcorr = 1;
    $colorBall = 0;

    if(count($ListaOcorrencias)< 5){
        ?>
            <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="example">
                <div class="bn-news">
                    <ul>
                        <li><img src="<?php echo JUri::base() . 'images/cmps/alerta/Pin.png';?>" title="<?php echo $alertaWebsiteName;?> - <?php echo $nomeConcelho;?>"/><?php echo $Tickermensagem1; ?></li>
                        <li><?php echo $Tickermensagem2; ?></li>
                        <li><?php echo $Tickermensagem3; ?></li>
                    </ul>
                </div>
                <div class="bn-controls">
                    <button><span class="bn-arrow bn-prev"></span></button>
                    <button><span class="bn-action"></span></button>
                    <button><span class="bn-arrow bn-next"></span></button>
                </div>
            </div>
        <?php
    } else {
        ?>
            <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="example">
                <div class="bn-news">
                    <ul>
                        <?php foreach($ListaOcorrencias as $row) :
                            if($numOcorr < 20){
                                if($row['estado'] == 1){
                                    $colorBall = $corEspera;
                                    $title = JText::_('COM_VIRTUALDESK_ALERTA_TITLE_ESPERA');
                                } else if($row['estado'] == 2){
                                    $colorBall = $corAnalise;
                                    $title = JText::_('COM_VIRTUALDESK_ALERTA_TITLE_ANALISE');
                                } else if($row['estado'] == 3){
                                    $colorBall = $corConcluido;
                                    $title = JText::_('COM_VIRTUALDESK_ALERTA_TITLE_CONCLUIDO');
                                } ?>
                                <li>
                                    <div class="ball" style="background:<?php echo $colorBall; ?>" title="<?php echo $title; ?>"></div>
                                    <div class="ocor" title="<?php echo $title; ?>">
                                        <?php echo $row['categoria'] . ' - ' . $row['subcategoria'] . ' - ' . $row['freguesia']; ?>
                                    </div>
                                </li>
                                <?php $numOcorr = $numOcorr + 1;
                            } else{
                                break;
                            }?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="bn-controls">
                    <button><span class="bn-arrow bn-prev"></span></button>
                    <button><span class="bn-action"></span></button>
                    <button><span class="bn-arrow bn-next"></span></button>
                </div>
            </div>
        <?php
    }


    echo $footerScripts;
?>

<script>
    jQuery('#example').breakingNews();
</script>
