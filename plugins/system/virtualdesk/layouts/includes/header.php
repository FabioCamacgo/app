<?php
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();

    $nomeMunicipio       = $obParam->getParamsByTag('nomeMunicipio');
?>

<!--  MENU PRINCIPAL   -->

<div id="menuContent">
    <a href="http://localhost/app/" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Login</div></a>
    <a href="http://localhost/app/novoalerta/" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Nova Ocorrência</div></a>
    <a href="http://localhost/app/ultimasocorrencias/" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Últimas Ocorrências</div></a>
    <a href="" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Menu 1</div></a>
    <a href="" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Menu 2</div></a>
    <a href="" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Menu 3</div></a>
    <a href="" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Menu 4</div></a>
    <a href="" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Menu 5</div></a>
    <a href="" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Menu 6</div></a>
    <a href="" title=""><div><span class="out"><img src="<?php echo $imgPath; ?>dotBlack.png" alt="list" title="Estilo"/></span><span class="hov"><img src="<?php echo $imgPath; ?>dotSol.png" alt="list" title="Estilo"/></span> Menu 7</div></a>
</div>

<!--  END MENU PRINCIPAL   -->

<!-- BEGIN LOGO -->
<div class="logo">
    <div class="row">
        <div class="col-md-3">
            <?php if ($logoFile) : ?>
                <a href="<?php echo $baseurl; ?>/">
                    <img src="<?php echo $logoFile ?>"  alt="logo" title="Portal Municipal da <?php echo $nomeMunicipio;?>" class="logo-default"/>
                </a>
            <?php endif; ?>
        </div>

        <div class="col-md-6">
            <div class="search-container">
                <form action="/app/pesquisa/" method="post">
                    <input type="text" placeholder="Pesquisar ocorrência.." name="Pesquisar">
                    <button type="submit" name="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>

        <div class="col-md-3 pull-right">
            <button id="menu">
                <img src="<?php echo $imgPath; ?>menu.png" alt="MenuTop" title="Menu"/>
            </button>

            <button id="closeMainMenu">
                <img src="<?php echo $imgPath; ?>close.png" alt="closeMenu" title="Close"/>
            </button>
        </div>

    </div>
</div>


<!-- END LOGO -->