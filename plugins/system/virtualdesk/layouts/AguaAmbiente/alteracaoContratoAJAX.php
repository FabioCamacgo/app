<?php

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('alteracaocontrato');
if ($resPluginEnabled === false) exit();

?>



<form id="formAlteracaoMoradaAgua" action="" method="post">

    <legend class="titulo"><h3><?php echo 'Identificação do titular do contrato' ?></h3></legend>

    <section id="dadosPessoais">
        <div class="form-group">
            <label for="field_nomeConsumidor"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NOME_CONSUMIDOR') ?></label>
            <input type="text" id="field_nomeConsumidor" class="form-control select2 select2-nosearch" required name="field_nomeConsumidor" value="<?php echo $NomeConsumidor; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_numCliente"><?php echo 'Número de Cliente' ?></label>
            <input type="text" id="field_numCliente" class="form-control select2 select2-nosearch" required name="field_numCliente" max="9" value="<?php echo $NumCliente; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_localConsumo"><?php echo 'Código do local do consumo' ?></label>
            <input type="text" id="field_localConsumo" class="form-control select2 select2-nosearch" required name="field_localConsumo" max="9" value="<?php echo $LocalConsumo; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_email"><?php echo 'Email do cliente' ?></label>
            <input type="text" id="field_email" class="form-control select2 select2-nosearch" required name="field_email" value="<?php echo $Email; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_email2"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_EMAIL_CONSUMIDOR_CONFIRMACAO') ?></label>
            <input type="text" id="field_email2" class="form-control select2 select2-nosearch" required name="field_email2" value="<?php echo $Email2; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_nif"><?php echo 'Nif do cliente' ?></label>
            <input type="text" id="field_nif" class="form-control select2 select2-nosearch" required name="field_nif" value="<?php echo $Nif; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_telefone"><?php echo 'Telefone do cliente' ?></label>
            <input type="text" id="field_telefone" class="form-control select2 select2-nosearch" required name="field_telefone" max="9" value="<?php echo $Telefone; ?>"/>
        </div>
    </section>


    <legend class="titulo"><h3><?php echo 'Dados a alterar' ?></h3></legend>

    <section id="alteracaoDados">
        <div class="form-group">
            <label for="field_tipoAlteracao"><?php echo JText::_('Tipo de Alteração') ?></label>
            <div class="input-group ">
                <select name="field_tipoAlteracao" id="field_tipoAlteracao" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <option></option>
                    <option value="1">Alterar morada de envio de correspondência</option>
                    <option value="2">Corrigir local de consumo</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="field_distrito"><?php echo 'Distrito' ?></label>
            <input type="text" id="field_distrito" class="form-control select2 select2-nosearch" required name="field_distrito" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_concelho"><?php echo 'Concelho' ?></label>
            <input type="text" id="field_concelho" class="form-control select2 select2-nosearch" required name="field_concelho" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_freguesia"><?php echo 'Freguesia' ?></label>
            <input type="text" id="field_freguesia" class="form-control select2 select2-nosearch" required name="field_freguesia" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_rua"><?php echo 'Rua / Av. / Lugar' ?></label>
            <input type="text" id="field_rua" class="form-control select2 select2-nosearch" required name="field_rua" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_numero"><?php echo 'Número / Lote' ?></label>
            <input type="text" id="field_numero" class="form-control select2 select2-nosearch" required name="field_numero" value="<?php echo $Numero; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_andar"><?php echo 'Andar' ?></label>
            <input type="text" id="field_andar" class="form-control select2 select2-nosearch" required name="field_andar" value="<?php echo $Andar; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_codPostal"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CODIGOPOSTAL_CONSUMIDOR') ?></label>
            <input type="text" id="field_codPostal" class="form-control select2 select2-nosearch" required name="field_codPostal" value="<?php echo $CodPostal; ?>"/>
        </div>
    </section>

    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6LfwsgcUAAAAAPpGifbSkLj_1Dk7RnZ76W1vN_RB" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
            <input type="hidden" name="hiddenRecaptcha" id="hiddenRecaptcha" class="form-control d-none" data-recaptcha="true" required data-error="Please complete the Captcha">
            <div class="help-block with-errors"></div>
        </div>
    <?php endif; ?>

    <div style="display:none;"><input type="submit" name="alterarMorada" id="alterarMorada" value="Alterar Morada"></div>

</form>

