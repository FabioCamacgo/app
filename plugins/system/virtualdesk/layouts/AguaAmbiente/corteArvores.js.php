<?php
    defined('_JEXEC') or die;
?>

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater').each(function(){
                //console.log('a')
                jQuery(this).repeater({

                    show: function () {

                        if(checkIfFileLimtiHasReached()===true)
                        {
                            jQuery('a.mt-repeater-add').hide();
                            var AlertMsg = jQuery('#FileListNumReachedAlert').clone();
                            jQuery('#FileListNumReachedAlertShow').html(AlertMsg);
                            jQuery('#FileListNumReachedAlertShow > div').show();
                            jQuery('#vd-filelist-repeater').find('div.mt-repeater-item.vd-filelist-repeater-item').last().remove();

                        }
                        else
                        {
                            jQuery(this).slideDown();
                        }
                    },

                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            jQuery(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });
        }

    };

}();

function dropError(){
    jQuery('.backgroundErro').css('display','block');
}

var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 4,

            // if null - has no limits
            // example: 3
            fileMaxSize: 5,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png', 'pdf'],

            enableApi: true,


            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - null
            upload: {
                // upload URL {String}
                url: setVDCurrentRelativePath , // definido no php

                // upload data {null, Object}
                // you can also change this Object in beforeSend callback
                // example: { option_1: '1', option_2: '2' }
                data:  null ,

                // upload type {String}
                // for more details http://api.jquery.com/jquery.ajax/
                type: 'POST',

                // upload enctype {String}
                // for more details http://api.jquery.com/jquery.ajax/
                enctype: 'multipart/form-data',

                // auto-start file upload {Boolean}
                // if false, you can use the API methods - item.upload.send() to trigger upload for each file
                // if false, you can use the upload button - check custom file name example
                start: false, // true,

                // upload the files synchron {Boolean}
                synchron: true,

                // upload large files in chunks {false, Number} set file chunk size in MB as Number (ex: 4)
                chunk: false,

                // Callback fired before uploading a file by returning false, you can prevent the upload
                beforeSend: function(item, listEl, parentEl, newInputEl, inputEl) {
                    // example:
                    // here you can extend the upload data

                    item.upload.data.isVDAjaxReqFileUpload = '1';
                    item.upload.data.VDAjaxReqProcRefId    = jQuery("#VDAjaxReqProcRefId").val();

                    return true;
                },

                // Callback fired if the upload succeeds
                // we will add by default a success icon and fadeOut the progressbar
                onSuccess: function(data, item, listEl, parentEl, newInputEl, inputEl, textStatus, jqXHR) {
                    item.html.find('.fileuploader-action-remove').addClass('fileuploader-action-success');

                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },

                // Callback fired if the upload failed
                // we will set by default the progressbar to 0% and if it wasn't cancelled, we will add a retry button
                onError: function(item, listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus, errorThrown) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }

                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },

                onProgress: function(data, item, listEl, parentEl, newInputEl, inputEl) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },

                // Callback fired after all files were uploaded
                onComplete: function(listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus) {
                    // callback will go here

                    vdOnFileUploadLoadComplete(); // está definida no módulo do site se for invocado por ajax
                }
            }

            ,// by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

var vdAlertaHelperObj = function () {

    var vdAlertaData = '';

    var setAlertaDataByAjax = function (data) {
        vdAlertaData = data;
    };

    var getAlertaDataByAjax = function () {
        return vdAlertaData;
    };

    return {
        setAlertaDataByAjax  :  setAlertaDataByAjax,
        getAlertaDataByAjax  :  getAlertaDataByAjax
    };

}();


/*
* Inicialização da validação
*/

jQuery(document).ready(function() {

    // Select 2
    ComponentsSelect2.init();

    ComponentFileUploader.init();

    // Repeater
    FormRepeater.init();

    jQuery( "#fechaErro" ).click(function() {
        jQuery('.backgroundErro').css('display','none');
    });

    jQuery('.date-picker1').datepicker({autoclose:true});
    jQuery('.date-picker1').on('hide', function (e) { e.preventDefault(); });

    jQuery('#m_timepicker_2, #m_timepicker_3').timepicker({
        showMeridian: false
    });

    var concelhoPost = jQuery("#concelho").val(); /* GET THE VALUE OF THE SELECTED DATA */

    if(concelhoPost != 0){
        var select = jQuery('#vdmenumain');
        select.prop('disabled', false);
    }

    jQuery("#concelho").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */



        var pathArray = window.location.pathname.split('/');
        var secondLevelLocation = pathArray[1];



        var concelho = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(concelho == 0) {
            var select = jQuery('#vdmenumain');
            select.find('option').remove();
            select.prop('disabled', true);

            App.unblockUI('#blocoMenuMain');

        } else {

            var dataString = "concelho="+concelho; /* STORE THAT TO A DATA STRING */

            // show loader
            App.blockUI({ target:'#blocoMenuMain',  animate: true});

            jQuery.ajax({
                url: websitepath,
                data:'m=corteArvores_getconc&concelho=' + concelho ,

                success: function(output) {

                    var select = jQuery('#vdmenumain');
                    select.find('option').remove();

                    if (output == null || output=='') {
                        select.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoMenuMain');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoMenuMain');
                }
            });
        }

    });


});