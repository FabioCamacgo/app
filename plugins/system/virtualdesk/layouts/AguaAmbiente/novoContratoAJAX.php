<?php

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('adesaocontrato');
if ($resPluginEnabled === false) exit();


?>

<form id="formNovoContratoAgua" action="" method="post">

    <legend class="titulo"><h3><?php echo 'Identificação do titular do contrato' ?></h3></legend>

    <section id="dadosPessoais">
        <div class="form-group">
            <label for="field_nomeConsumidor"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NOME_CONSUMIDOR') ?></label>
            <input type="text" id="field_nomeConsumidor" class="form-control select2 select2-nosearch" required name="field_nomeConsumidor" value="<?php echo $NomeConsumidor; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_email"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_EMAIL_CONSUMIDOR') ?></label>
            <input type="text" id="field_email" class="form-control select2 select2-nosearch" required name="field_email" value="<?php echo $Email; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_email2"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_EMAIL_CONSUMIDOR_CONFIRMACAO') ?></label>
            <input type="text" id="field_email2" class="form-control select2 select2-nosearch" required name="field_email2" value="<?php echo $Email2; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_nif"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NIF_CONSUMIDOR') ?></label>
            <input type="text" id="field_nif" class="form-control select2 select2-nosearch" required name="field_nif" value="<?php echo $Nif; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_telefone"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_TELEFONE_CONSUMIDOR') ?></label>
            <input type="text" id="field_telefone" class="form-control select2 select2-nosearch" required name="field_telefone" max="9" value="<?php echo $Telefone; ?>"/>
        </div>
    </section>




    <legend class="titulo"><h3><?php echo 'Localização do prédio para colocação do contador' ?></h3></legend>


    <section id="localizacaoPredio">
    <div class="form-group">
        <label for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_FREGUESIA_CONSUMIDOR') ?></label>
        <?php $Freguesias = VirtualDeskSiteAguaHelper::getFreguesias('4')?>
        <div class="input-group ">
            <select name="field_freguesia" value="<?php echo $Freguesia; ?>" id="field_freguesia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <option></option>
                <?php foreach($Freguesias as $rowStatus) : ?>
                    <option value="<?php echo $rowStatus['id_freguesia']; ?>"
                    ><?php echo $rowStatus['freguesia']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="field_rua"><?php echo 'Rua / Av. / Lugar' ?></label>
        <input type="text" id="field_rua" class="form-control select2 select2-nosearch" required name="field_rua" value="<?php echo $Rua; ?>"/>
    </div>

    <div class="form-group">
        <label for="field_numero"><?php echo 'Número / Lote' ?></label>
        <input type="text" id="field_numero" class="form-control select2 select2-nosearch" required name="field_numero" value="<?php echo $Numero; ?>"/>
    </div>

    <div class="form-group">
        <label for="field_andar"><?php echo 'Andar' ?></label>
        <input type="text" id="field_andar" class="form-control select2 select2-nosearch" required name="field_andar" value="<?php echo $Andar; ?>"/>
    </div>

    <div class="form-group">
        <label for="field_NumQuartos"><?php echo 'Nº de quartos da habitação' ?></label>
        <input type="text" id="field_NumQuartos" class="form-control select2 select2-nosearch" required name="field_NumQuartos" value="<?php echo $NumQuartos; ?>"/>
    </div>

    <div class="form-group">
        <label for="field_codPostal"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CODIGOPOSTAL_CONSUMIDOR') ?></label>
        <input type="text" id="field_codPostal" class="form-control select2 select2-nosearch" required name="field_codPostal" value="<?php echo $CodPostal; ?>"/>
    </div>
</section>


    <legend class="titulo"><h3><?php echo 'Morada para envio da correspondência' ?></h3></legend>

    <section id="moradaCorrespondencia">
    <div class="form-group">
        <label for="field_correspondencia"><?php echo 'Escolha onde deseja receber a correspondência' ?></label>
        <div class="input-group ">
            <select name="field_correspondencia" id="field_correspondencia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <option></option>
                <option value="1">Morada do prédio</option>
                <option value="2">Outra Morada</option>
            </select>
        </div>
    </div>

    <section id="moradaCorrespondenciaHide">
        <div class="form-group">
            <label for="field_distritoCorrespondencia"><?php echo 'Distrito' ?></label>
            <input type="text" id="field_distritoCorrespondencia" class="form-control select2 select2-nosearch" required name="field_distritoCorrespondencia" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_concelhoCorrespondencia"><?php echo 'Concelho' ?></label>
            <input type="text" id="field_concelhoCorrespondencia" class="form-control select2 select2-nosearch" required name="field_concelhoCorrespondencia" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_freguesiaCorrespondencia"><?php echo 'Freguesia' ?></label>
            <input type="text" id="field_freguesiaCorrespondencia" class="form-control select2 select2-nosearch" required name="field_freguesiaCorrespondencia" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_ruaCorrespondencia"><?php echo 'Rua / Av. / Lugar' ?></label>
            <input type="text" id="field_ruaCorrespondencia" class="form-control select2 select2-nosearch" required name="field_ruaCorrespondencia" value="<?php echo $Rua; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_numeroCorrespondencia"><?php echo 'Número / Lote' ?></label>
            <input type="text" id="field_numeroCorrespondencia" class="form-control select2 select2-nosearch" required name="field_ruaCorrespondenciaCorrespondencia" value="<?php echo $Numero; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_andarCorrespondencia"><?php echo 'Andar' ?></label>
            <input type="text" id="field_andarCorrespondencia" class="form-control select2 select2-nosearch" required name="field_andarCorrespondencia" value="<?php echo $Andar; ?>"/>
        </div>

        <div class="form-group">
            <label for="field_codPostalCorrespondencia"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CODIGOPOSTAL_CONSUMIDOR') ?></label>
            <input type="text" id="field_codPostalCorrespondencia" class="form-control select2 select2-nosearch" required name="field_codPostalCorrespondencia" value="<?php echo $CodPostal; ?>"/>
        </div>
    </section>
</section>


    <legend class="titulo"><h3><?php echo 'Outras Informações' ?></h3></legend>


    <section id="outrasInformacoes">
    <div class="form-group">
        <label for="field_tipoContador"><?php echo JText::_('Tipo de Contador') ?></label>
        <div class="input-group ">
            <select name="field_tipoContador" id="field_tipoContador" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <option></option>
                <option value="1">Doméstico</option>
                <option value="2">Provisório</option>
                <option value="3">Serviços Públicos</option>
                <option value="4">Instituições Particulares</option>
                <option value="5">Comércio / Indústria</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="field_uploads"><?php echo JText::_('Anexe os documentos que ache pertinentes') ?></label>
        <input type="file" id="field_uploads" name="field_uploads" accept="image/png, image/jpeg, application/pdf" />
    </div>
</section>


    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6LfwsgcUAAAAAPpGifbSkLj_1Dk7RnZ76W1vN_RB" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
            <input type="hidden" name="hiddenRecaptcha" id="hiddenRecaptcha" class="form-control d-none" data-recaptcha="true" required data-error="Please complete the Captcha">
            <div class="help-block with-errors"></div>
        </div>
    <?php endif; ?>

    <div style="display:none;"><input type="submit" name="enviarContrato" id="enviarContrato" value="Enviar Contrato"></div>

</form>