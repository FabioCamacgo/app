<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('novoConsumidor');
    if ($resPluginEnabled === false) exit();


    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AguaAmbiente/novoConsumidor.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;
    }

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $concelhoParam = $obParam->getParamsByTag('novoConsumidorConcelho');
    $linkPolitica = $obParam->getParamsByTag('linkPolPrivNovoConsumidor');
    $codigoPostalTextMaxLenght = $obParam->getParamsByTag('codigoPostalTextMaxLenght');
    $codigoPostal2MaxLenght = $obParam->getParamsByTag('codigoPostal2MaxLenght');
    $codigoPostal1MaxLenght = $obParam->getParamsByTag('codigoPostal1MaxLenght');
    $MoradaMaxLenght = $obParam->getParamsByTag('MoradaMaxLenght');
    $TelefoneMaxLenght = $obParam->getParamsByTag('TelefoneMaxLenght');
    $NifMaxLenght = $obParam->getParamsByTag('NifMaxLenght');
    $NomeConsumidorMaxLenght = $obParam->getParamsByTag('NomeConsumidorMaxLenght');
    $NumContadorMaxLenght = $obParam->getParamsByTag('NumContadorMaxLenght');
    $NumConsumidorMaxLenght = $obParam->getParamsByTag('NumConsumidorMaxLenght');
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
    $linkDefaultLeituras = $obParam->getParamsByTag('linkDefaultLeituras');

    if (isset($_POST['submitForm'])) {
        $numConsumidor = $_POST['numConsumidor'];
        $numContador = $_POST['numContador'];
        $nomeConsumidor = $_POST['nomeConsumidor'];
        $email = $_POST['email'];
        $email2 = $_POST['email2'];
        $nif = $_POST['nif'];
        $telefone = $_POST['telefone'];
        $morada = $_POST['morada'];
        $freg = $_POST['freg'];
        $codPostal4 = $_POST['codPostal4'];
        $codPostal3 = $_POST['codPostal3'];
        $codPostalText = $_POST['codPostalText'];
        $veracid2 = $_POST['veracid2'];
        $politica2 = $_POST['politica2'];

        $CodPostal = $codPostal4 . '-' . $codPostal3 . ' ' . $codPostalText;

        $existeConsumidor = VirtualDeskSiteAguaHelper::checkConsumidor($numConsumidor);
        $existeContador = VirtualDeskSiteAguaHelper::checkContador($numContador);
        $existeNIF = VirtualDeskSiteAguaHelper::checkNIF($nif);

        $validacaoNumeroConsumidor = VirtualDeskSiteAguaHelper::validaNumeroConsumidor($numConsumidor, $NumConsumidorMaxLenght, count($existeConsumidor));
        $validacaoNumeroContador = VirtualDeskSiteAguaHelper::validaNumeroContador($numContador, $NumContadorMaxLenght, count($existeContador));
        $validacaoNome = VirtualDeskSiteAguaHelper::validaNome($nomeConsumidor);
        $validacaoEmail = VirtualDeskSiteAguaHelper::validaEmail($email, $email2);
        $validacaoNIF = VirtualDeskSiteAguaHelper::validaNif($nif, $NifMaxLenght, count($existeNIF));
        $validacaoTelefone = VirtualDeskSiteAguaHelper::validaTelefone($telefone, $TelefoneMaxLenght);
        $validacaoMorada = VirtualDeskSiteAguaHelper::validaMorada($morada);
        $validacaoFreg = VirtualDeskSiteAguaHelper::validaFreguesia($freg);
        $validacaoCodigoPostal = VirtualDeskSiteAguaHelper::validaCodigoPostal($codPostal4, $codPostal3, $codPostalText, $codigoPostal2MaxLenght, $codigoPostal1MaxLenght);

        // dynamic_recaptcha_1
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        $errRecaptcha = 1;
        if((string)$recaptcha_response_field !='') {
            $jinput->set('g-recaptcha-response', $recaptcha_response_field);
            $resRecaptchaAnswer = $captcha->checkAnswer($recaptcha_response_field);
            $errRecaptcha = 0;
            if (!$resRecaptchaAnswer) {
                $errRecaptcha = 1;
            }
        }


        if($validacaoNumeroConsumidor == 0 && $validacaoNumeroContador == 0 && $validacaoNome == 0 && $validacaoEmail == 0 && $validacaoNIF == 0 && $validacaoTelefone == 0 && $validacaoMorada == 0 && $validacaoFreg == 0 && $validacaoCodigoPostal == 0 && $politica2 != 0 && $veracid2 != 0 && $errRecaptcha == 0) {

            $saveRegisto = VirtualDeskSiteAguaHelper::guardaRegisto($numConsumidor, $nomeConsumidor, $nif, $email, $telefone, $morada, $concelhoParam, $freg, $CodPostal);
            $saveContador = VirtualDeskSiteAguaHelper::guardaContador($numContador, $nif);

            if ($saveRegisto == true && $saveContador == true) {
                ?>
                    <style>
                        #registar{ display:none !important;}
                    </style>

                    <div class="message">
                        <div class="messageContent">
                            <p>
                                <div class="sucessIcon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10" stroke-width="2"/><path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg></div>
                                <?php echo JText::_('COM_VIRTUALDESK_AGUA_REGISTO_CONSUMIDOR_SUCESSO'); ?>
                            </p>
                            <p><?php echo JText::_('COM_VIRTUALDESK_AGUA_REGISTO_CONSUMIDOR_SUCESSO1'); ?></p>
                            <p><?php echo JText::_('COM_VIRTUALDESK_AGUA_REGISTO_CONSUMIDOR_SUCESSO2'); ?></p>
                            <p><?php echo JText::sprintf('COM_VIRTUALDESK_AGUA_REGISTO_CONSUMIDOR_SUCESSO3',$nomeMunicipio); ?></p>
                            <p><?php echo JText::sprintf('COM_VIRTUALDESK_AGUA_REGISTO_CONSUMIDOR_SUCESSO4',$copyrightAPP); ?></p>
                        </div>
                    </div>
                <?php

                $nomeFreguesia = VirtualDeskSiteAguaHelper::getnomeFreguesia($freg);

                VirtualDeskSiteAguaHelper::SendRegistoEmail($numConsumidor, $numContador, $nomeConsumidor, $email, $nif, $telefone, $morada, $nomeFreguesia, $CodPostal);

                ?>
                    <script>
                        setTimeout(function(){
                            window.location.href = '<?php echo $linkDefaultLeituras;?>';
                        }, 3000);
                    </script>
                <?php
                exit();

            } else {
                ?>
                    <div class="message"><?php echo JText::_('COM_VIRTUALDESK_AGUA_REGISTO_CONSUMIDOR_ERRO'); ?></div>
                <?php
            }

        } else {
            ?>
            <script>
                dropError();
            </script>

            <div class="backgroundErro">
                <div class="erro" style="display:block;">
                    <button id="fechaErro">
                        <svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                            <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                                                                                M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                                                                c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                                                                c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                                                                c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                                                                c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                                                                c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                                                                c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                                                                C42.996,43.384,41.986,44.001,40.361,44.02z">

                            </path>
                        </svg>
                    </button>

                    <h2>
                        <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                        <?php echo JText::_('COM_VIRTUALDESK_AGUA_AVISO'); ?>
                    </h2>

                    <ol>
                        <?php

                            if($validacaoNIF == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NIF_INVALIDO') . '</li>';
                            } else if($validacaoNIF == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NIF_EXISTE') . '</li>';
                            }  else {
                                $validacaoNIF = 0;
                            }

                            if($validacaoNumeroContador == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_CONTADOR') . '</li>';
                            } else if($validacaoNumeroContador == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CONTADOR_INVALIDO') . '</li>';
                            } else if($validacaoNumeroContador == 3) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CONTADOR_EXISTE') . '</li>';
                            } else {
                                $validacaoNumeroContador = 0;
                            }

                            if($validacaoNome == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_NOME') . '</li>';
                            } else if($validacaoNome == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NOME_INVALIDO') . '</li>';
                            }  else {
                                $validacaoNome = 0;
                            }

                            if($validacaoNumeroConsumidor == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_CONSUMIDOR') . '</li>';
                            } else if($validacaoNumeroConsumidor == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CONSUMIDOR_INVALIDO') . '</li>';
                            } else if($validacaoNumeroConsumidor == 3) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CONSUMIDOR_EXISTE') . '</li>';
                            } else {
                                $validacaoNumeroConsumidor = 0;
                            }

                            if($validacaoTelefone == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_TELEFONE') . '</li>';
                            } else if($validacaoTelefone == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_TELEFONE_INVALIDO') . '</li>';
                            }  else {
                                $validacaoTelefone = 0;
                            }

                            if($validacaoEmail == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_EMAIL') . '</li>';
                            } else if($validacaoEmail == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_EMAIL_INVALIDO') . '</li>';
                            } else if($validacaoEmail == 3) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_EMAIL_NAO_CORRESPONDE') . '</li>';
                            } else {
                                $validacaoEmail = 0;
                            }

                            if($validacaoMorada == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_MORADA') . '</li>';
                            } else {
                                $validacaoMorada = 0;
                            }

                            if($validacaoFreg == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_FREGUESIA') . '</li>';
                            } else {
                                $validacaoFreg = 0;
                            }

                            if($validacaoCodigoPostal == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CODIGOPOSTAL_INVALIDO') . '</li>';
                            } else if($validacaoCodigoPostal == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_SEM_CODIGOPOSTAL') . '</li>';
                            } else {
                                $validacaoCodigoPostal = 0;
                            }

                            if($veracid2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK__AGUA_NOVO_REGISTO_ERRO_VERACIDADE') . '</li>';
                            }

                            if($politica2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK__AGUA_NOVO_REGISTO_ERRO_POLITICA') . '</li>';
                            }

                            if($errRecaptcha == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_CONSUMIDOR_CAPTCHA') . '</li>';
                            }else{
                                $errRecaptcha = 0;
                            }

                        ?>
                    </ol>
                </div>
            </div>
        <?php
        }

    }


?>

<form id="new_consumidor" action="" method="post" class="consumidor-form" enctype="multipart/form-data" >

    <div class="section">

        <div class="form-group" id="nifConsumidor">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NIF_CONSUMIDOR') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" name="nif" id="nif" class="form-control" maxlength="9" value="<?php echo $nif; ?>"/>
            </div>
        </div>

        <div class="form-group" id="numberContador">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NUMERO_CONTADOR') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" name="numContador" id="numContador" class="form-control" maxlength="<?php echo $NumContadorMaxLenght;?>" value="<?php echo $numContador; ?>" />
            </div>
        </div>

        <div class="form-group" id="nameConsumidor">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NOME_CONSUMIDOR') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" name="nomeConsumidor" id="nomeConsumidor" class="form-control" value="<?php echo $nomeConsumidor; ?>" maxlength="<?php echo $NomeConsumidorMaxLenght;?>"/>
            </div>
        </div>

        <div class="form-group" id="numberConsumidor">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NUMERO_CONSUMIDOR') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" name="numConsumidor" id="numConsumidor" class="form-control" maxlength="<?php echo $NumConsumidorMaxLenght;?>" value="<?php echo $numConsumidor; ?>"/>
            </div>
        </div>

        <div class="form-group" id="telefoneConsumidor">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_TELEFONE_CONSUMIDOR') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" name="telefone" id="telefone" class="form-control" maxlength="<?php echo $TelefoneMaxLenght;?>" value="<?php echo $telefone; ?>"/>
            </div>
        </div>

        <div class="form-group" id="mailConsumidor">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_EMAIL_CONSUMIDOR') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" name="email" id="email" class="form-control" value="<?php echo $email; ?>"/>
            </div>
        </div>

        <div class="form-group" id="confMail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_EMAIL_CONSUMIDOR_CONFIRMACAO') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" name="email2" id="email2" class="form-control" value="<?php echo $email2; ?>"/>
            </div>
        </div>

        <div class="form-group" id="moradaConsumidor">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_MORADA_CONSUMIDOR') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" id="morada" class="form-control" name="morada" value="<?php echo $morada; ?>" maxlength="<?php echo $MoradaMaxLenght;?>" />
            </div>
        </div>

        <div class="form-group" id="fregConsumidor">
            <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_FREGUESIA_CONSUMIDOR') ?><span class="required">*</span></label>
            <?php $Freguesia = VirtualDeskSiteAguaHelper::getFreguesia($concelhoParam)?>
            <div class="col-md-9">
                <select name="freg" value="<?php echo $freg; ?>" id="freg" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($freg)){
                        ?>
                        <option value="">Escolher Op&ccedil;&atilde;o</option>
                        <?php foreach($Freguesia as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $freg; ?>"><?php echo VirtualDeskSiteAguaHelper::getFregSelect($freg) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFreg = VirtualDeskSiteAguaHelper::excludeFreguesia($concelhoParam, $freg)?>
                        <?php foreach($ExcludeFreg as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group" id="codPostal">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CODIGOPOSTAL_CONSUMIDOR'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="codPostal4" id="codPostal4" maxlength="<?php echo $codigoPostal1MaxLenght;?>" value="<?php echo $codPostal4; ?>"/>
                <?php echo '-'; ?>
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="codPostal3" id="codPostal3" maxlength="<?php echo $codigoPostal2MaxLenght;?>" value="<?php echo $codPostal3; ?>"/>
                <?php echo ' '; ?>
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="codPostalText" id="codPostalText" maxlength="<?php echo $codigoPostalTextMaxLenght;?>" value="<?php echo $codPostalText; ?>"/>
            </div>
        </div>
    </div>


    <div class="section second">

        <!--   Veracidade -->
        <div class="form-group" id="Veracidade">

            <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_VERACIDADE'); ?><span class="required">*</span></label>

            <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
        </div>


        <!--   Politica de privacidade  -->
        <div class="form-group" id="PoliticaPrivacidade">

            <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
            <label class="col-md-3 control-label">Declaro que li e aceito a <a href="<?php echo $linkPolitica; ?>" target="_blank">Pol&iacute;tica de Privacidade</a>. <span class="required">*</span></label>

            <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
        </div>

        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group" >
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>


        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
        </div>

    </div>

</form>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
?>

<script>

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AguaAmbiente/novoConsumidor.js.php');
    ?>

</script>
