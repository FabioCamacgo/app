<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('limpezaUrbana');
    if ($resPluginEnabled === false) exit();

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AguaAmbiente/maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AguaAmbiente/edit_maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    //FileUploader
    $headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    //Fileuploader
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;
    }

    echo $headCSS;

    ?>
        <style>
            .fileuploader-popup {z-index: 99999 !important;}
        </style>
    <?php

$obParam      = new VirtualDeskSiteParamsHelper();

    $concelhoParam = $obParam->getParamsByTag('limpezaUrbanaConcelho');
    $indConcelho = $obParam->getParamsByTag('limpezaUrbanaIndConcelho');
    $linkPolitica = $obParam->getParamsByTag('linkPolPrivlimpezaUrbana');
    $pinMapa = $obParam->getParamsByTag('pinMapa');
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');


    if (isset($_POST['submitForm'])) {
        $nome = $_POST['nome'];
        $morada = $_POST['morada'];
        $concelho = $_POST['concelho'];
        $fregReq = $_POST['vdmenumain'];
        if (!empty($fregReq)) {
            $idSelectFreg = VirtualDeskSiteLimpezaUrbanaHelper::getFregSelectID($fregReq);
        }
        $localidade = $_POST['localidade'];
        $codPostal4 = $_POST['codPostal4'];
        $codPostal3 = $_POST['codPostal3'];
        $codPostalText = $_POST['codPostalText'];
        $codPostal = $codPostal4 . '-' . $codPostal3 . ' ' . $codPostalText;
        $cc = $_POST['cc'];
        $validadeCC = $_POST['validadeCC'];
        $telefone = $_POST['telefone'];
        $fiscalid = $_POST['fiscalid'];
        $email = $_POST['email'];
        $confemail = $_POST['confemail'];
        $freg = $_POST['freg'];
        $local = $_POST['local'];
        $motivo = $_POST['motivo'];
        $coordenadas = $_POST['coordenadas'];
        $splitCoord = explode(",", $coordenadas);
        $lat = $splitCoord[0];
        $long = $splitCoord[1];
        $veracid2 = $_POST['veracid2'];
        $politica2 = $_POST['politica2'];
        $dataAtual = date("Y-m-d");

        $validacaoNome = VirtualDeskSiteLimpezaUrbanaHelper::validaNome($nome);
        $validacaoMorada = VirtualDeskSiteLimpezaUrbanaHelper::validaMorada($morada);
        $validacaoConcelho = VirtualDeskSiteLimpezaUrbanaHelper::validaConcelho($concelho);
        $validacaoFregReq = VirtualDeskSiteLimpezaUrbanaHelper::validaFreguesia($fregReq);
        $validacaoLocalidade = VirtualDeskSiteLimpezaUrbanaHelper::validaLocalidade($localidade);
        $validacaoCodigoPostal = VirtualDeskSiteLimpezaUrbanaHelper::validaCodigoPostal($codPostal4, $codPostal3, $codPostalText);
        $validacaoCC = VirtualDeskSiteLimpezaUrbanaHelper::validaCC($cc);
        $validacaoValidadeCC = VirtualDeskSiteLimpezaUrbanaHelper::validaValidadeCC($validadeCC, $dataAtual);
        $validacaoTelefone = VirtualDeskSiteLimpezaUrbanaHelper::validaTelefone($telefone);
        $validacaoNIF = VirtualDeskSiteLimpezaUrbanaHelper::validaNif($fiscalid);
        $validacaoEmail = VirtualDeskSiteLimpezaUrbanaHelper::validaEmail($email, $confemail);
        $validacaoFreg = VirtualDeskSiteLimpezaUrbanaHelper::validaFreguesia($freg);
        $validacaoLocal = VirtualDeskSiteLimpezaUrbanaHelper::validaMorada($local);


        // dynamic_recaptcha_1
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        // dynamic_recaptcha_1
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        $errRecaptcha = 1;
        if((string)$recaptcha_response_field !='') {
            $jinput->set('g-recaptcha-response', $recaptcha_response_field);
            $resRecaptchaAnswer = $captcha->checkAnswer($recaptcha_response_field);
            $errRecaptcha = 0;
            if (!$resRecaptchaAnswer) {
                $errRecaptcha = 1;
            }
        }

        if($validacaoNome == 0 && $validacaoMorada == 0 && $validacaoConcelho == 0 && $validacaoFregReq == 0 && $validacaoLocalidade == 0 && $validacaoCodigoPostal == 0 && $validacaoCC == 0 && $validacaoValidadeCC == 0 && $validacaoTelefone == 0 && $validacaoNIF == 0 && $validacaoEmail == 0 && $validacaoFreg == 0 && $validacaoLocal == 0 && $validacaoDataLimpeza == 0 && $politica2 != 0 && $veracid2 != 0 && $errRecaptcha == 0) {
            $random = VirtualDeskSiteLimpezaUrbanaHelper::random_code();
            $referencia = 'LU-' . $indConcelho . '-' . $fiscalid . '-' . $random;

            $idFreg = VirtualDeskSiteLimpezaUrbanaHelper::getFregSelectID($fregReq);

            $savePedido = VirtualDeskSiteLimpezaUrbanaHelper::SaveNovoPedido($referencia, $nome, $morada, $concelho, $idFreg, $localidade, $codPostal, $cc, $validadeCC, $telefone, $fiscalid, $email, $freg, $local, $motivo, $lat, $long, $dataAtual);


            if ($savePedido == true) {
                ?>
                <style>
                    #submit{ display:none !important;}
                </style>

                <div class="message">
                    <div class="messageContent">
                        <p>
                            <div class="sucessIcon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10" stroke-width="2"/><path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg></div>
                            <?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_SUCESSO'); ?>
                        </p>
                        <p><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_SUCESSO1'); ?></p>
                        <p><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_SUCESSO2'); ?></p>
                        <p><?php echo JText::sprintf('COM_VIRTUALDESK_LIMPEZAURBANA_SUCESSO3',$nomeMunicipio); ?></p>
                        <p><?php echo JText::sprintf('COM_VIRTUALDESK_LIMPEZAURBANA_SUCESSO4',$copyrightAPP); ?></p>
                    </div>
                </div>
                <?php

                echo('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');

                exit();

            } else {
                ?>
                <div class="message"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO'); ?></div>
                <?php
            }

        }  else {
            ?>
                <script>
                    dropError();
                </script>

                <div class="backgroundErro">
                    <div class="erro" style="display:block;">
                        <button id="fechaErro">
                            <svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                                <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                                                                        M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                                                        c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                                                        c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                                                        c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                                                        c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                                                        c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                                                        c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                                                        C42.996,43.384,41.986,44.001,40.361,44.02z">

                                </path>
                            </svg>
                        </button>

                        <h2>
                            <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                            <?php echo JText::_('COM_VIRTUALDESK_AGUA_AVISO'); ?>
                        </h2>

                        <ol>
                            <?php

                            if($validacaoNome == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_NOME1') . '</li>';
                            } else if($validacaoNome == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_NOME2') . '</li>';
                            } else {
                                $validacaoNome = 0;
                            }

                            if($validacaoMorada == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_MORADA1') . '</li>';
                            } else {
                                $validacaoMorada = 0;
                            }

                            if($validacaoConcelho == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_CONCELHO') . '</li>';
                            } else {
                                $validacaoConcelho = 0;
                            }

                            if($validacaoFregReq == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_FREGUESIAREQ') . '</li>';
                            } else {
                                $validacaoFregReq = 0;
                            }

                            if($validacaoCodigoPostal == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_CODIGOPOSTAL') . '</li>';
                            } else {
                                $validacaoCodigoPostal = 0;
                            }

                            if($validacaoLocalidade == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_LOCALIDADE') . '</li>';
                            } else {
                                $validacaoLocalidade = 0;
                            }

                            if($validacaoCC == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_CC1') . '</li>';
                            } else if($validacaoCC == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_CC2') . '</li>';
                            } else {
                                $validacaoCC = 0;
                            }

                            if($validacaoValidadeCC == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_VALIDADECC1') . '</li>';
                            } else if($validacaoValidadeCC == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_VALIDADECC2') . '</li>';
                            } else {
                                $validacaoValidadeCC = 0;
                            }

                            if($validacaoTelefone == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_TELEFONE1') . '</li>';
                            } else if($validacaoTelefone == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_TELEFONE2') . '</li>';
                            } else {
                                $validacaoTelefone = 0;
                            }

                            if($validacaoNIF == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_NIF1') . '</li>';
                            } else {
                                $validacaoNIF = 0;
                            }

                            if($validacaoEmail == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_EMAIL1') . '</li>';
                            } else if($validacaoEmail == 2) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_EMAIL2') . '</li>';
                            } else if($validacaoEmail == 3) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_EMAIL3') . '</li>';
                            } else {
                                $validacaoEmail = 0;
                            }

                            if($validacaoFreg == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_FREGUESIA1') . '</li>';
                            } else {
                                $validacaoFreg = 0;
                            }

                            if($validacaoLocal == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_LOCAL1') . '</li>';
                            } else {
                                $validacaoLocal = 0;
                            }

                            if($veracid2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_VERACIDADE') . '</li>';
                            }

                            if($politica2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_ERRO_POLITICA') . '</li>';
                            }

                            if($errRecaptcha == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_CONSUMIDOR_CAPTCHA') . '</li>';
                            }else{
                                $errRecaptcha = 0;
                            }
                            ?>
                        </ol>
                    </div>
                </div>
            <?php
        }


    }
?>

<form id="new_limpezaUrbana" action="" method="post" class="limpezaUrbana-form" enctype="multipart/form-data" >

    <div class="section">

        <h3><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_IDENTIFICACAOREQUERENTE'); ?></h3>

        <!--   NOME COMPLETO   -->
        <div class="form-group" id="name">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_NOME'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder=""
                       name="nome" id="nome" value="<?php echo htmlentities($nome, ENT_QUOTES, 'UTF-8'); ?>" maxlength="150"/>
            </div>
        </div>


        <!--   Morada  -->
        <div class="form-group" id="address">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_MORADA'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" placeholder=""
                       name="morada" id="morada" value="<?php echo htmlentities($morada, ENT_QUOTES, 'UTF-8'); ?>" maxlength="500"/>
            </div>
        </div>


        <!--   Concelho  -->
        <div class="form-group" id="conc">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_CONCELHO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <?php $concelhos = VirtualDeskSiteLimpezaUrbanaHelper::getConcelhos()?>
                <div class="input-group ">
                    <select name="concelho" required value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($concelho)){
                            ?>
                            <option value="0">Escolher Op&ccedil;&atilde;o</option>
                            <?php foreach($concelhos as $rowStatus) : ?>
                                <option value="<?php echo $rowStatus['id']; ?>"
                                    <?php
                                    if(!empty($this->data->categoria)) {
                                        if( (int)($rowStatus['id'] == (int) $this->data->idwebsitelist) ) echo 'selected';
                                    }
                                    ?>
                                ><?php echo $rowStatus['concelho']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteLimpezaUrbanaHelper::getConcSelect($concelho) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeConc = VirtualDeskSiteLimpezaUrbanaHelper::excludeconcelho($concelho)?>
                            <?php foreach($ExcludeConc as $rowStatus) : ?>
                                <option value="<?php echo $rowStatus['id']; ?>"
                                    <?php
                                    if(!empty($this->data->categoria)) {
                                        if( (int)($rowStatus['id'] == (int) $this->data->idwebsitelist) ) echo 'selected';
                                    }
                                    ?>
                                ><?php echo $rowStatus['concelho']; ?></option>
                            <?php endforeach;
                        }
                        ?>

                    </select>
                </div>
            </div>
        </div>

        <!--   Freguesia   -->
        <?php
        // Carrega menusec se o id de menumain estiver definido.
        $ListaDeMenuMain = array();
        if(!empty($this->data->concelho)) {
            if( (int) $this->data->concelho > 0) $ListaDeMenuMain = VirtualDeskSiteLimpezaUrbanaHelper::getFregReq($this->data->concelho);
        }

        ?>
        <div id="blocoMenuMain" class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_LIMPEZAURBANA_FREGUESIA' ); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <select name="vdmenumain" id="vdmenumain" value ="<?php echo $fregReq;?>" required
                    <?php
                    if(!empty($this->data->concelho)) {
                        if ((int)$this->data->concelho <= 0) echo 'disabled';
                    }
                    else {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">

                    <?php
                    if(empty($fregReq)){
                        ?>
                        <option value="">Escolher Op&ccedil;&atilde;o</option>
                        <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                                <?php
                                if(!empty($this->data->vdmenumain)) {
                                    if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                                }
                                ?>
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $idSelectFreg; ?>"><?php echo VirtualDeskSiteLimpezaUrbanaHelper::getFregSelect($idSelectFreg) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFregReq = VirtualDeskSiteLimpezaUrbanaHelper::getFregReq2($concelho, $idSelectFreg)?>
                        <?php foreach($ExcludeFregReq as $rowStatus) : ?>
                            <option value="<?php echo $rowStatus['id']; ?>"
                                <?php
                                if(!empty($this->data->categoria)) {
                                    if( (int)($rowStatus['id'] == (int) $this->data->idwebsitelist) ) echo 'selected';
                                }
                                ?>
                            ><?php echo $rowStatus['name']; ?></option>
                        <?php endforeach;
                    }
                    ?>


                </select>

            </div>
        </div>


        <!--   Localidade  -->
        <div class="form-group" id="localida">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_LOCALIDADE'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="localidade" id="localidade" maxlength="150" value="<?php echo $localidade; ?>"/>
            </div>
        </div>


        <!--   CÓDIGO POSTAL  -->
        <div class="form-group" id="codPostal">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_CODIGOPOSTAL'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="codPostal4" id="codPostal4" maxlength="4" value="<?php echo $codPostal4; ?>"/>
                <?php echo '-'; ?>
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="codPostal3" id="codPostal3" maxlength="3" value="<?php echo $codPostal3; ?>"/>
                <?php echo ' '; ?>
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="codPostalText" id="codPostalText" maxlength="150" value="<?php echo $codPostalText; ?>"/>
            </div>
        </div>


        <!--   CC  -->
        <div class="form-group" id="cartaoCidadao">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_CC'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder=""
                       name="cc" id="cc" maxlength="8" value="<?php echo $cc; ?>"/>
            </div>
        </div>


        <!--   Validade CC  -->
        <div class="form-group" id="valCC">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_VALIDADECC'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" readonly name="validadeCC" id="validadeCC" value="<?php echo $validadeCC; ?>">
                    <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>

        <!--   NIF   -->
        <div class="form-group" id="nif">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_FISCALID'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder=""
                       name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
            </div>
        </div>

        <!--   TELEFONE  -->
        <div class="form-group" id="telef">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_TELEFONE'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder=""
                       name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
            </div>
        </div>

        <!--   EMAIL   -->
        <div class="form-group" id="mail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_EMAIL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder=""
                       name="email" id="email" value="<?php echo $email; ?>" maxlength="500"/>
            </div>
        </div>


        <!--   Confirmacao Email   -->
        <div class="form-group" id="conf_mail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_CONFEMAIL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder=""
                       name="confemail" id="confemail" value="<?php echo $confemail; ?>" maxlength="500"/>
            </div>
        </div>
    </div>


    <div class="section second">

        <h3><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_REQUERER'); ?></h3>

        <!--   Local   -->
        <div class="form-group" id="localLimpezaUrbana">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_LOCAL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="" name="local" id="local" value="<?php echo $local; ?>" maxlength="500"/>
            </div>
        </div>

        <!--   FREGUESIAS   -->
        <div class="form-group" id="fregLimpezaUrbana">
            <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_FREGUESIAS') ?><span class="required">*</span></label>
            <?php $Freguesia = VirtualDeskSiteLimpezaUrbanaHelper::getFreguesia($concelhoParam)?>
            <div class="col-md-9">
                <select name="freg" value="<?php echo $freg; ?>" id="freg" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($freg)){
                        ?>
                        <option value="">Escolher Op&ccedil;&atilde;o</option>
                        <?php foreach($Freguesia as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $freg; ?>"><?php echo VirtualDeskSiteLimpezaUrbanaHelper::getFregSelect($freg) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFreg = VirtualDeskSiteLimpezaUrbanaHelper::excludeFreguesia($concelhoParam, $freg)?>
                        <?php foreach($ExcludeFreg as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Motivo   -->
        <div class="form-group" id="motivoLimpezaUrbana">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_MOTIVO'); ?></label>
            <div class="col-md-9">
                <textarea class="form-control" rows="5" placeholder="" name="motivo" id="motivo" maxlength="5000"><?php echo $motivo; ?></textarea>
            </div>
        </div>

        <!--   MAPA   -->
        <div class="form-group" id="mapa">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_MAPA'); ?></label>
            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

            <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                   value="<?php echo htmlentities($this->data->$coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>

        </div>

        <!--   Upload Imagens  -->
        <div class="form-group" id="uploadField">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_UPLOAD'); ?></label>
            <div class="col-md-9">
                <div class="file-loading">
                    <input type="file" name="fileupload[]" id="fileupload" multiple>
                </div>
                <div id="errorBlock" class="help-block"></div>
                <input type="hidden" id="VDAjaxReqProcRefId">

            </div>
        </div>

    </div>


    <div class="section second">

        <!--   Veracidade -->
        <div class="form-group" id="Veracidade">

            <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_LIMPEZAURBANA_VERACIDADE'); ?><span class="required">*</span></label>

            <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
        </div>

        <script>

            document.getElementById('veracid').onclick = function() {
                // access properties using this keyword
                if ( this.checked ) {
                    document.getElementById("veracid2").value = 1;
                } else {
                    document.getElementById("veracid2").value = 0;
                }
            };
        </script>

        <!--   Politica de privacidade  -->
        <div class="form-group" id="PoliticaPrivacidade">

            <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
            <label class="col-md-3 control-label">Declaro que li e aceito a <a href="<?php echo $linkPolitica; ?>" target="_blank">Pol&iacute;tica de Privacidade</a>. <span class="required">*</span></label>

            <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
        </div>

        <script>

            document.getElementById('politica').onclick = function() {
                // access properties using this keyword
                if ( this.checked ) {
                    document.getElementById("politica2").value = 1;
                } else {
                    document.getElementById("politica2").value = 0;
                }
            };
        </script>


        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group" >
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>

    </div>

    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
    </div>

</form>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;
?>

<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'limpezaUrbana/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
    var iconPath = '<?php echo JUri::base() . $pinMapa; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AguaAmbiente/limpezaUrbana.js.php');
    ?>

</script>
