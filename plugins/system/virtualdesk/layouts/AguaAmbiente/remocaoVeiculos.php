<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $jinput = JFactory::getApplication()->input;

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteRemocaoVeiculosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_remocaoVeiculos.php');
    JLoader::register('VirtualDeskSiteRemocaoVeiculosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_remocaoVeiculos_files.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('remocaoVeiculos');
    if ($resPluginEnabled === false) exit();


    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';


    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;
    //$language_tag = $lang->getTag(); // loads the current language-tag
    $jinput = JFactory::getApplication('site')->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        $fileLangSufixV2 ='pt';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }


    // EST  A RECEBER UM TESTE por AJAX ?

    $isVDAjaxReqTESTEPost= $jinput->get('isVDAjaxReqTESTEPost');
    if($isVDAjaxReqTESTEPost=="1") {
        $results = array( 'error' => '', 'data' => array()
        );

        echo json_encode($results);
        exit();
    }


    // EST  A RECEBER UM UPLOAD ?
    $isVDAjaxReqFileUpload = $jinput->get('isVDAjaxReqFileUpload');
    if($isVDAjaxReqFileUpload=="1") {

        $alertsFiles =  new VirtualDeskSiteRemocaoVeiculosFilesHelper();
        $alertsFiles->tagprocesso = 'Remocao_Veiculos';
        // ToDo: A referência do id de processo tem de vir encriptado sendo também encriptado na resposta desse Id pelo servidor...
        $alertsFiles->idprocesso = $jinput->get('VDAjaxReqProcRefId');


        $resFileSave = $alertsFiles->saveListFileByAjax('fileupload');

        $erroGravar = '';
        if($resFileSave!==true) $erroGravar = 'Erro ao gravar os ficheiros';

        $results = array(
            'error' => $erroGravar,
            'data' => array()
        );
        // Todo: tratamento de erros ao falhar a gravação de um ficheiro...
        echo json_encode($results);
        exit();
    }

    $obPlg      = new VirtualDeskSitePluginsHelper();

    if($isVDAjaxReq !=1){
        $layoutPath = $obPlg->getPluginLayoutAtivePath('remocaoVeiculos','form');
        require_once(JPATH_SITE . $layoutPath);
    } else {
        $layoutPath = $obPlg->getPluginLayoutAtivePath('remocaoVeiculos','formajax');
        require_once(JPATH_SITE . $layoutPath);
    }

    if((string)$layoutPath=='') {
        // Se não carregar o caminho do layout sai com erro...
        echo (JText::_('COM_VIRTUALDESK_LOADACTIVELAYOUT_ERROR'));
        exit();
    }
    require_once(JPATH_SITE . $layoutPath);

?>