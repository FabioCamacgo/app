<?php

?>

var contador = 0;

jQuery( document ).ready(function() {

    jQuery('#addnewContador').click(function() {
        jQuery('.contadoresSection .newContador').slideDown( "slow" );
    });

    document.getElementById('EnviarForm').onclick = function() {
        jQuery('#EnviarLeitura').click();
    };

    jQuery("#numberNewContador").change(function(){
        contador = jQuery(this).val();
    });

    jQuery('#addContador').click(function() {
        var nif = <?php echo $nifConsumidor;?>;
        jQuery.ajax({
            url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
            data:'m=agua_novoContador&contador=' + contador + '&nif=' + nif,

            success: function(output) {
                if(output == '1""'){
                    alert("Contador adicionado com sucesso.");
                    location.reload();
                } else if(output == '2""'){
                    alert("O contador indicado está incorreto.");
                } else {
                    alert("O contador já está registado nos nossos serviços!");
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });

    });

});
