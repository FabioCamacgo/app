<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('validaLeituras');
    if ($resPluginEnabled === false) exit();

    $link = $jinput->get('link','' ,'string');

    $ref = explode("ref=", $link);

    $search = base64_decode($ref[1]);

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

//BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
    $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
    $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');

    $nifConsumidor = $search;
    $dadosConsumidor = VirtualDeskSiteAguaHelper::getDadosConsumidor($nifConsumidor);
    $contadoresAssociados =  VirtualDeskSiteAguaHelper::getContadores($nifConsumidor);

    foreach($dadosConsumidor as $rowStatus) :
        $numConsumidor = $rowStatus['consumidor'];
        $zona = $rowStatus['zona'];
        $nome = $rowStatus['nome'];
    endforeach;

    if (isset($_POST['submitForm'])) {

        $newLeitura = $_POST['newLeitura'];
        $numContador = $_POST['numContador'];

        $ultimaLeitura = VirtualDeskSiteAguaHelper::getUltimaLeituras($nifConsumidor, $numContador);

        $year = date("Y");
        $month = date("m");

        if($month == 1){
            $month = 12;
        } else if($month != 11 || $month != 12){
            $month = '0' . ($month - 1);
        } else{
            $month = $month - 1;
        }

        $referencia = 'LA' . $numContador . $year . $month;
        $refExiste = VirtualDeskSiteAguaHelper::CheckReferencia($referencia);

        if($newLeitura == NULL || empty($newLeitura)){
            $errLeitura = 1;
        } else if (!is_numeric($newLeitura) || strlen($newLeitura) < 4) {
            $errLeitura = 2;
        }  else if($ultimaLeitura > $newLeitura) {
            $errLeitura = 3;
        } else if(!empty($refExiste)){
            $errLeitura = 4;
        } else {
            $errLeitura = 0;
        }

        if($numContador == NULL || empty($numContador)){
            $errContador = 1;
        } else {
            $errContador = 0;
        }



        // dynamic_recaptcha_1
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        $errRecaptcha = 1;
        if((string)$recaptcha_response_field !='') {
            $jinput->set('g-recaptcha-response', $recaptcha_response_field);
            $resRecaptchaAnswer = $captcha->checkAnswer($recaptcha_response_field);
            $errRecaptcha = 0;
            if (!$resRecaptchaAnswer) {
                $errRecaptcha = 1;
            }
        }

        if($errLeitura == 0 && $errContador == 0 && $errRecaptcha == 0){

            $saveLeitura =VirtualDeskSiteAguaHelper::guardaLeitura($referencia, $nifConsumidor, $numContador, $newLeitura);

            if($saveLeitura == true) { ?>

                <script>
                    gotop();
                </script>
                <div class="backgroundSuccess">
                    <div class="messageSuccess">
                        <button id="fechaSuccess">
                            <svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                                <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                                    M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                    c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                    c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                    c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                    c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                    c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                    c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                    C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                            </svg>
                        </button>
                        <?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVALEITURA_SUCESSO'); ?></div></div>
                <?php

                VirtualDeskSiteAguaHelper::SendLeituraAdminEmail($referencia);

                $mailConsumidor = VirtualDeskSiteAguaHelper::getMailConsumidor($nifConsumidor);
                $nameConsumidor = VirtualDeskSiteAguaHelper::getnomeConsumidorByNIF($nifConsumidor);

                VirtualDeskSiteAguaHelper::SendLeituraEmailPlugin($nameConsumidor, $mailConsumidor, $numContador, $newLeitura);

            }
        } else {
            ?>
            <script>
                dropError();
            </script>

            <div class="backgroundErro">
                <div class="erro" style="display:block;">
                    <button id="fechaErro"><svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                                <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                                    M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                    c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                    c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                    c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                    c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                    c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                    c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                    C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                                </svg></button>

                    <h2>
                        <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                        <?php echo JText::_('COM_VIRTUALDESK_AGUA_AVISO'); ?>
                    </h2>

                    <ol>
                        <?php
                            if($errContador == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_ERRO_CONTADOR') . '</li>';
                            } else {
                                $errContador = 0;
                            }

                            if($errLeitura == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_ERRO_LEITURA1') . '</li>';
                            }  if($errLeitura == 2){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_ERRO_LEITURA2') . '</li>';
                            } else if($errLeitura == 3){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_ERRO_LEITURA3') . '</li>';
                            } else if($errLeitura == 4){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_ERRO_LEITURA4') . '</li>';
                            } else {
                                $errLeitura = 0;
                            }

                            if($errRecaptcha == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_CONSUMIDOR_CAPTCHA') . '</li>';
                            }else{
                                $errRecaptcha = 0;
                            }
                        ?>
                    </ol>
                </div>
            </div>
            <?php
        }
    }

?>

<div class="section_Dados">
    <h2> <?php echo JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_DADOSCLIENTE_TITULO'); ?> </h2>

    <div class="info">

        <div class="w30">
            <div class="titulo"> <?php echo JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_DADOSCLIENTE_NIF_CONSUMIDOR'); ?> </div>
            <div class="dados"><?php echo $nifConsumidor; ?></div>
        </div>

        <div class="w30">
            <div class="titulo"> <?php echo JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_DADOSCLIENTE_NUMERO_CONSUMIDOR'); ?> </div>
            <div class="dados"><?php echo $numConsumidor; ?></div>
        </div>

        <div class="w40">
            <div class="titulo"> <?php echo JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_DADOSCLIENTE_CODIGOZONA'); ?> </div>
            <div class="dados"><?php echo $zona; ?></div>
        </div>

        <div class="w100">
            <div class="titulo"><?php echo JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_DADOSCLIENTE_NOME_CONSUMIDOR'); ?> </div>
            <div class="dados"><?php echo $nome; ?></div>
        </div>
    </div>

    <h2 class="newSection"><?php echo JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_DADOSCLIENTE_CONTADORESASSOCIADOS'); ?> </h2>

    <div class="contadoresSection">
        <div class="w70">
            <?php
            if(count($contadoresAssociados) == 0){
                echo JText::_('COM_VIRTUALDESK_AGUA_SEM_CONTADOR');
            } else {
                ?>
                <table>
                    <thead>
                        <tr>
                            <th><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NUM_CONTADOR'); ?></th>
                            <th><?php echo JText::_('COM_VIRTUALDESK_AGUA_DATAREGISTO'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($contadoresAssociados as $rowStatus) :
                                $dataCriacao = explode(' ', $rowStatus['data_criacao']);
                                $dataCriacaoExplode = explode('-', $dataCriacao[0]);
                                $dataCriacaoRevert = $dataCriacaoExplode[2] . '-' . $dataCriacaoExplode[1] . '-' . $dataCriacaoExplode[0];
                                ?>
                                <tr>
                                    <td><?php echo $rowStatus['contador'];?></td>
                                    <td><?php echo $dataCriacaoRevert;?></td>
                                </tr>
                            <?php
                            endforeach;
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>
        </div>

        <div class="w30">
            <button id="addnewContador"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVACONTADOR'); ?></button>
        </div>

        <div class="newContador">
            <form id="newContador"  action="" method="post">
                <div class="form-group" id="novoContador">
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NUMBERCONTADOR'); ?><span class="required">*</span></label>
                    <div class="col-md-9">
                        <input type="text" name="numberNewContador" id="numberNewContador" value="<?php echo $numberNewContador;?>" max-lenght="7">
                    </div>
                </div>
            </form>

            <button id="addContador"><?php echo JText::_('COM_VIRTUALDESK_AGUA_LEITURAS_DADOSCLIENTE_NOVOCONTADOR'); ?></button>
        </div>
    </div>

</div>


<?php
    if(count($contadoresAssociados) > 0){
        ?>
        <div class="section_novaLeitura">
            <h2><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVALEITURA'); ?></h2>

            <form id="formNovaLeitura"  action="" method="post">


                <div class="section">
                    <div class="form-group" id="numberContador">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NUMERO_CONTADOR') ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <?php
                            if(count($contadoresAssociados) == 1){
                                $numContador = $rowStatus['contador'];
                                ?>
                                <input type="text" name="numContador" readonly id="numContador" class="form-control" value="<?php echo $numContador; ?>" />
                                <?php
                            } else {
                                ?>
                                    <select name="numContador" value="<?php echo $numContador; ?>" id="numContador" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($numContador)){
                                            ?>
                                            <option value="">Escolher Op&ccedil;&atilde;o</option>
                                            <?php foreach($contadoresAssociados as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['contador']; ?>"
                                                ><?php echo $rowWSL['contador']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $numContador; ?>"><?php echo $numContador;?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeContador = VirtualDeskSiteAguaHelper::excludeContador($nifConsumidor, $numContador)?>
                                            <?php foreach($ExcludeContador as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['contador']; ?>"
                                                ><?php echo $rowWSL['contador']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                <?php
                            }
                            ?>


                        </div>
                    </div>


                    <div class="form-group" id="leitura">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVALEITURA'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" name="newLeitura" id="newLeitura" required value="<?php echo $newLeitura;?>" max-lenght="7">
                        </div>
                    </div>
                </div>

                <div class="section second">

                    <?php if ($captcha_plugin!='0') : ?>
                        <div class="form-group" >
                            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                            $field_id = 'dynamic_recaptcha_1';
                            print $captcha->display($field_id, $field_id, 'g-recaptcha');
                            ?>
                            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                        </div>
                    <?php endif; ?>

                </div>

                <button id="EnviarForm"><?php echo JText::_('COM_VIRTUALDESK_AGUA_ENVIARLEITURA');?></button>

                <div class="form-actions" method="post" style="display:none;">
                    <input type="submit" id="submitForm" name="submitForm" value="Enviar Leitura">
                    <input type="hidden" id="nifConsumidor" name="nifConsumidor" value="<?php echo $nifConsumidor;?>">
                </div>
            </form>

        </div>
        <?php
    } else {
        ?>
            <style>
                #Enviar{
                    display:none !important;
                }
            </style>
        <?php
    }
?>

<div class="section_historicoLeituras">

    <h2> <?php echo JText::_('COM_VIRTUALDESK_AGUA_HISTORICOLEITURA'); ?> </h2>

    <div class="section">
        <?php

        $Leituras = VirtualDeskSiteAguaHelper::getLeituras($nifConsumidor);

        if((int)$Leituras <=0){
            echo '<p>' . JText::_('COM_VIRTUALDESK_AGUA_HISTORICO_LEITURAS_VAZIO') . $nifConsumidor . '</p>';
        } else{ ?>
            <table>
                <thead>
                <tr>
                    <th valign="center"><?php echo JText::_('COM_VIRTUALDESK_AGUA_HISTORICO_LEITURAS_LEITURA') ?></th>
                    <th valign="center"><?php echo JText::_('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NUM_CONTADOR') ?></th>
                    <th valign="center"><?php echo JText::_('COM_VIRTUALDESK_AGUA_HISTORICO_LEITURAS_DATA') ?></th>
                    <th valign="center" style="text-align:center;"><?php echo JText::_('COM_VIRTUALDESK_AGUA_HISTORICO_LEITURAS_ESTADO') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($Leituras as $rowStatus) : ?>
                    <tr>
                        <td><?php echo $rowStatus['leitura']; ?></td>
                        <td><?php echo $rowStatus['num_contador']; ?></td>
                        <td>
                            <?php
                            $data = explode(" ", $rowStatus['data_leitura']);
                            $revertDataLeitura = explode("-", $data[0]);
                            $dataLeitura = $revertDataLeitura[2] . '-' . $revertDataLeitura[1] . '-' . $revertDataLeitura[0];
                            echo $dataLeitura;
                            ?>
                        </td>
                        <?php
                            if($rowStatus['idEstado'] == 1){
                                $class="analise";
                            } else if($rowStatus['idEstado'] == 2){
                                $class="valido";
                            } else if($rowStatus['idEstado'] == 3){
                                $class="rejeitado";
                            } else {
                                $class="analise";
                            }
                        ?>

                        <td style="text-align:center;"><?php echo '<span class="'. $class .'">' . $rowStatus['estado'] . '</span>'; ?></td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
            <?php
        } ?>
    </div>
</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AguaAmbiente/Agua.js.php');
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AguaAmbiente/EnviarLeitura.js.php');
    ?>
</script>
