var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater').each(function(){
                //console.log('a')
                jQuery(this).repeater({

                    show: function () {

                        if(checkIfFileLimtiHasReached()===true)
                        {
                            jQuery('a.mt-repeater-add').hide();
                            var AlertMsg = jQuery('#FileListNumReachedAlert').clone();
                            jQuery('#FileListNumReachedAlertShow').html(AlertMsg);
                            jQuery('#FileListNumReachedAlertShow > div').show();
                            jQuery('#vd-filelist-repeater').find('div.mt-repeater-item.vd-filelist-repeater-item').last().remove();

                        }
                        else
                        {
                            jQuery(this).slideDown();
                        }
                    },

                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            jQuery(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });
        }

    };

}();

function dropError(){
    jQuery('.backgroundErro').css('display','block');
}


function turnOption(){

    var valor = jQuery('#pedido').val();

    if(valor == 8 ){
        jQuery('#quais').css('display','inline-block');
    } else {
        jQuery('#quais').css('display','none');
    }
}

jQuery(document).ready(function() {

    // Select 2
    ComponentsSelect2.init();

    // Repeater
    FormRepeater.init();


    jQuery('#pedido').on('change', function() {
        var value = jQuery(this).val();
        if(value == 8){
            jQuery('#quais').css('display','inline-block');
        } else {
            jQuery('#quais').css('display','none');
        }
    });

    jQuery( "#fechaErro" ).click(function() {
        jQuery('.backgroundErro').css('display','none');
    });

    jQuery('.date-picker1').datepicker({autoclose:true});
    jQuery('.date-picker1').on('hide', function (e) { e.preventDefault(); });

    jQuery('#m_timepicker_2, #m_timepicker_3').timepicker({
        showMeridian: false
    });

    var concelhoPost = jQuery("#concelho").val(); /* GET THE VALUE OF THE SELECTED DATA */

    if(concelhoPost != 0){
        var select = jQuery('#vdmenumain');
        select.prop('disabled', false);
    }

    jQuery("#concelho").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */



        var pathArray = window.location.pathname.split('/');
        var secondLevelLocation = pathArray[1];



        var concelho = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(concelho == 0) {
            var select = jQuery('#vdmenumain');
            select.find('option').remove();
            select.prop('disabled', true);

            App.unblockUI('#blocoMenuMain');

        } else {

            var dataString = "concelho="+concelho; /* STORE THAT TO A DATA STRING */

            // show loader
            App.blockUI({ target:'#blocoMenuMain',  animate: true});

            jQuery.ajax({
                url: websitepath,
                data:'m=cemiterios_getconc&concelho=' + concelho ,

                success: function(output) {

                    var select = jQuery('#vdmenumain');
                    select.find('option').remove();

                    if (output == null || output=='') {
                        select.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoMenuMain');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoMenuMain');
                }
            });
        }

    });

});