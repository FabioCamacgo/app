<?php
    defined('_JEXEC') or die;
?>


var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload_foto").fileuploader({

            changeInput: ' ',
            limit: 1,
            fileMaxSize: 3,
            extensions: ['jpg', 'jpeg', 'png'],
            enableApi: true,
            addMore: false,
            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

/*
* Define estrutura da validação.
* A validação é iniciada depois no final deste ficheiro.
* Foi acrescentado antes da inicialização um método ( .addMethod("vdpasscheck"... ) que permite fazer a validação específica da password.
*/
var UploadFoto = function() {

    var handleRegister = function() {

        jQuery('#uploadFotoSlider').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                hiddenRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
            },

            messages: { // custom messages for radio buttons and checkboxes
                hiddenRecaptcha: {
                    required: jQuery('#hiddenRecaptcha').attr('messageValidation')
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    jQuery("#MainMessageAlertBlock").find('span').html(message);
                    jQuery("#MainMessageAlertBlock").show();
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
                    error.insertAfter(jQuery('#register_tnc_error'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                jQuery('#cover-spin').show(0);
                form[0].submit();
            }
        });

        jQuery('#uploadFotoSlider').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#uploadFotoSlider').validate().form()) {
                    jQuery('#uploadFotoSlider').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {

            handleRegister();
        }

    };

}();

/*
* Inicialização da validação
*/
jQuery(document).ready(function() {
    UploadFoto.init();

    ComponentFileUploader.init();
});
