<?php

defined('JPATH_BASE') or die;
defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;

$isVDAjaxReq = $jinput->get('isVDAjaxReq');

JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteVilaNatalHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_VilaNatal.php');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$config          = JFactory::getConfig();
$sitename        = $config->get('sitename');
$sitedescription = $config->get('sitedescription');
$sitetitle       = $config->get('sitetitle');
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$logoFile        = $templateParams->get('logoFile');
$fluidContainer  = $config->get('fluidContainer');
$page_heading    = $config->get('page_heading');
$baseurl         = JUri::base();
$rooturl         = JUri::root();
$captcha_plugin  = JFactory::getConfig()->get('captcha');
$templateName    = 'virtualdesk';
$labelseparator = ' : ';
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';


// Idiomas
$lang = JFactory::getLanguage();
$extension = 'com_virtualdesk';
$base_dir = JPATH_SITE;
//$language_tag = $lang->getTag(); // loads the current language-tag
$jinput = JFactory::getApplication('site')->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}


$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

//GLOBAL SCRIPTS
$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/VilaNatal/NovaEmpresa.js' . $addscript_end;

// PAGE LEVEL PLUGIN SCRIPTS
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;




// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';


$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
}

$obParam      = new VirtualDeskSiteParamsHelper();
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
$linkDirEmpresas = $obParam->getParamsByTag('linkDirEmpresas');
$linkRegistoDirEmpresas = $obParam->getParamsByTag('linkRegistoDirEmpresas');
$websiteVilaNatal = $obParam->getParamsByTag('websiteVilaNatal');
$nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

?>

<legend><h3 class="dadosEmpresa"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_DADOSEMPRESA'); ?></h3></legend>

<?php

if (isset($_POST['submitForm'])) {
    $nome = $_POST['nome'];
    $fiscalid = $_POST['fiscalid'];
    $horario = $_POST['horario'];
    $facebook = $_POST['facebook'];
    $instagramform = $_POST['instagramform'];
    $Website = $_POST['Website'];
    $check = $_POST['termsCondicoes'];


    /*Valida Nome*/
    if($nome == Null){
        $errNome = 1;
    }



    /*Valida Nif*/
    $nif=trim($fiscalid);
    $nifSplit2=str_split($nif);
    $ignoreFirst=true;
    $nifExiste = VirtualDeskSiteVilaNatalHelper::seeNIFExiste($fiscalid);
    //Verificamos se é numérico e tem comprimento 9
    if (!is_numeric($nif) || strlen($nif)!=9) {
        $errNif = 1;
    } else if($nifSplit2[0] != 5){
        $errNif = 1;
    } else if($nifExiste != 0){
        $errNif = 2;
    } else {
        $nifSplit=str_split($nif);
        //O primeiro digíto tem de ser 1, 2, 5, 6, 8 ou 9
        //Ou não, se optarmos por ignorar esta "regra"
        if (
            in_array($nifSplit[0], array(5))
            ||
            $ignoreFirst
        ) {
            //Calculamos o dígito de controlo
            $checkDigit=0;
            for($i=0; $i<8; $i++) {
                $checkDigit+=$nifSplit[$i]*(10-$i-1);
            }
            $checkDigit=11-($checkDigit % 11);
            //Se der 10 então o dígito de controlo tem de ser 0
            if($checkDigit>=10) $checkDigit=0;
            //Comparamos com o último dígito
            if ($checkDigit==$nifSplit[8]) {

            } else {
                $errNif = 1;
            }
        } else {
            $errNif = 1;
        }
    }

    /*Valida Horario*/
    if($horario == Null){
        $errHorario = 1;
    }

    /*Valida facebook*/
    if(is_numeric($facebook)){
        $errfacebook = 1;
    }

    /*Valida instagram*/
    if(is_numeric($instagramform)){
        $errInstagram = 1;
    }

    /*Valida website*/
    if(is_numeric($Website)){
        $errWebsite = 1;
    }


    if($check == 1 && $errNome == 0 && $errHorario == 0 && $errNif == 0 && $errfacebook == 0 && $errInstagram == 0 && $errWebsite == 0){

        if(empty($facebook)){
            $facebook = 'Não indicado';
        }

        if(empty($instagramform)){
            $instagramform = 'Não indicado';
        }

        if(empty($Website)){
            $Website = 'Não indicado';
        }

        /*Save Empresa*/
        $resSaveempresa = VirtualDeskSiteVilaNatalHelper::saveNewEmpresa($nome, $fiscalid, $horario, $facebook, $instagramform, $Website);
        /*END Save Empresa*/

        /*Send Email*/

        VirtualDeskSiteVilaNatalHelper::SendEmailRegisto($nome, $fiscalid, $horario, $facebook, $instagramform, $Website);
        /*END Send Email*/

        if($resSaveempresa==true) :?>

            <div class="sucesso">

                <p><?php echo JText::sprintf('COM_VIRTUALDESK_VILANATAL_SUCESSO_1'); ?></p>
                <p><?php echo JText::sprintf('COM_VIRTUALDESK_VILANATAL_SUCESSO_2'); ?></p>
                <p><?php echo JText::sprintf('COM_VIRTUALDESK_VILANATAL_SUCESSO_3',$copyrightAPP); ?></p>

            </div>

            <script>
                jQuery('#btPost').css('display','none');
            </script>

            <?php

            exit();
        endif;


    } else { ?>
        <script>
            dropError();
        </script>
        <div class="backgroundErro">
            <div class="erro" style="display:block;">
                <button id="fechaErro"><svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                    <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                        c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                        c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                        c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                        c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                        c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                        c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                        C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                </svg></button>

                <h2>
                    <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                    <?php echo JText::_('COM_VIRTUALDESK_VILANATAL_AVISO'); ?>
                </h2>

                <?php

                if($errNome == 1){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_NOME_1') . '</p>';
                } else if($errNome == 2){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_NOME_2') . '</p>';
                }else{
                    $errNome = 0;
                }

                if($errNif == 1) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_NIF_1') . '</p>';
                }else if($errNif == 2){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_NIF_2') . '</p>';
                }else{
                    $errNif = 0;
                }

                if($errHorario == 1) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_HORARIO_1') . '</p>';
                }else{
                    $errHorario = 0;
                }

                if($errfacebook == 1) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_FACEBOOK_1') . '</p>';
                }else{
                    $errfacebook = 0;
                }

                if($errInstagram == 1) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_INSTAGRAM_1') . '</p>';
                }else{
                    $errInstagram = 0;
                }

                if($errWebsite == 1) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_WEBSITE_1') . '</p>';
                }else{
                    $errWebsite = 0;
                }

                if($check == 0){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_VILANATAL_ERRO_TERMOSCONDICOES') . '</p>';
                }

                ?>

            </div>
        </div>
    <?php }


}

?>

<form id="new_empresa" action="/app/message/" method="post" class="empresa-form" enctype="multipart/form-data" >


    <!--   NOME DA EMPRESA   -->
    <div class="form-group" id="name">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_NOME_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" required class="form-control autocomplete="off"" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VILANATAL_NOME_LABEL'); ?>"
            name="nome" id="nome" value="<?php echo htmlentities($nome, ENT_QUOTES, 'UTF-8'); ?>" maxlength="60"/>
        </div>
    </div>


    <!--   NIPC   -->
    <div class="form-group" id="nipc">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_FISCALID_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_VILANATAL_FISCALID_LABEL'); ?>"
                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
        </div>
    </div>

    <!--   HORARIO DE FUNCIONAMENTO  -->
    <div class="form-group" id="horarioFuncionamento">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_HORARIO_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_VILANATAL_HORARIO_LABEL'); ?>"
                   name="horario" id="horario" value="<?php echo $horario; ?>"/>
        </div>
    </div>

    <!--   Facebook  -->
    <div class="form-group" id="FacebookLabel">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_FACEBOOK_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_VILANATAL_FACEBOOK_LABEL'); ?>"
                   name="facebook" id="facebook" value="<?php echo $facebook; ?>"/>
        </div>
    </div>

    <!--   Instagram  -->
    <div class="form-group" id="InstagramLabel">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_INSTAGRAM_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_VILANATAL_INSTAGRAM_LABEL'); ?>"
                   name="instagramform" id="instagramform" value="<?php echo $instagramform; ?>"/>
        </div>
    </div>

    <!--   WEBSITE  -->
    <div class="form-group" id="WebsiteLabel">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_WEBSITE_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_VILANATAL_WEBSITE_LABEL'); ?>"
                   name="Website" id="Website" value="<?php echo $Website; ?>"/>
        </div>
    </div>

    <div class="form-group" id="TermosCondicoes">

        <input type="checkbox" name="termsCondicoes2" id="termsCondicoes2" value="" required />
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_TERMOSCONDICOES_TITLE'); ?></label>
        <label class="texto">
            <ol>
                <li>O registo das empresas tem como finalidade informar os cidadãos acerca da oferta de comes e bebes do evento, assim como apoiar a promoção do setor empresarial local, designadamente o setor da restauração.</li>
                <li>As empresas que constam da lista inicial podem reformular os dados que lá estão bastando para o efeito, preencher o formulário para registo, como se de um novo registo se tratasse.  O registo é efetuado exclusivamente de forma online, no website <?php echo $websiteVilaNatal;?>, em formulário preparado para o devido efeito.</li>
                <li>As empresas que não estejam evidenciadas na lista inicial e queiram fazer parte do diretório da restauração Vila Natal, deverão igualmente submeter o seu registo de acordo com o disposto no ponto anterior.</li>
                <li>Os dados das empresas recolhidos pelo Município da <?php echo $nomeMunicipio; ?>, servem essencialmente para:
                    <ul>
                        <li>Informar os cidadãos acerca dos serviços de comes e bebes do evento;</li>
                        <li>Apoiar a promoção e a divulgação do setor da restauração;</li>
                        <li>Dar a conhecer informações personalizadas sobre o evento em questão;</li>
                        <li>Contactar as empresas para alguma dinâmica que se pretenda implementar no decorrer do evento;</li>
                        <li>Enviar informações sobre as atividades culturais e serviços municipais de interesse para as empresas locais.</li>
                    </ul>
                </li>
                <li>Os dados serão realizados mediante o consentimento das empresas no momento da submissão ao website <?php echo $websiteVilaNatal; ?>. Caso consinta, receberá comunicações do Município através de e-mail, podendo o consentimento para o tratamento de dados pessoais ser revogado em qualquer altura.</li>
            </ol>
        </label>
        <input type="hidden" id="termsCondicoes" name="termsCondicoes">
    </div>

    <div class="form-group" id="DiretorioEmpresas">

        <input type="checkbox" name="sim" id="sim" value="" required />
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_FAZERPARTEDIRETORIO'); ?></label>

        <div id="botoesDiretorio" style="display:none;">
            <a href="<?php echo $linkDirEmpresas;?>" target="_blank"><div><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_BOTOES_1'); ?></div></a>
            <a href="<?php echo $linkRegistoDirEmpresas;?>" target="_blank"><div><?php echo JText::_('COM_VIRTUALDESK_VILANATAL_BOTOES_2'); ?></div></a>
        </div>
    </div>

    <script>

        document.getElementById('termsCondicoes2').onclick = function() {
            // access properties using this keyword
            if ( this.checked ) {
                document.getElementById("termsCondicoes").value = 1;
            } else {
                document.getElementById("termsCondicoes").value = 0;
            }
        };

        document.getElementById('sim').onclick = function() {
            if ( this.checked ) {
                jQuery('#botoesDiretorio').css('display','block');
            } else {
                jQuery('#botoesDiretorio').css('display','none');
            }
        };
    </script>


    <!--<?php if ($captcha_plugin!='0') : ?>
            <div class="form-group" >
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
        $field_id = 'dynamic_recaptcha_1';
        print $captcha->display($field_id, $field_id, 'g-recaptcha');
        ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>-->



    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
    </div>

    <!--<input type="hidden" name="novoalerta" value="novoalerta">-->
    <!--<?php echo JHtml::_('form.token'); ?>-->



</form>





<?php echo $headScripts; ?>
<?php echo $footerScripts; ?>
<?php echo $recaptchaScripts; ?>
<?php echo $localScripts;?>


