<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listacategorias');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;


    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;


    $procuraCategorias = VirtualDeskSiteCulturaHelper::getCategorias();

    foreach($procuraCategorias as $rowWSL) :
        $idCat = $rowWSL['id'];
        $nameCat = $rowWSL['categoria'];

        $nospecial = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $nameCat);
        $string = str_replace(' / ', '', $nospecial);
        $string2 = str_replace(' ', '_', $string);
        ?>
            <div class="wideCat">
                <a href="<?php echo $nomeSite . $menu;?>?cat=<?php echo base64_encode($idCat);?>">
                    <div class="nomeCat">
                        <h3>
                            <div class="borda"></div>
                            <div class="nome"><?php echo $nameCat; ?></div>
                        </h3>
                    </div>

                    <div class="image">
                        <?php
                            $imagemCat = VirtualDeskSiteCulturaHelper::getImagemCategorias($idCat);
                        ?>
                        <img src="<?php echo $nomeSite. $noimage . $imagemCat;?>" alt="<?php echo $string2; ?>"/>
                    </div>
                </a>
            </div>

        <?php

    endforeach;

    ?>
        <div class="wideCat all">
            <a href="<?php echo $nomeSite . $menu;?>">
                <div class="nomeCat">
                    <h3>
                        <div class="borda"></div>
                        <div class="nome"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_ALL_CATS'); ?></div>
                    </h3>
                </div>

                <div class="background">
                    <div class="icon">
                        <svg enable-background="new 0 0 500 500" version="1.1" viewBox="0 0 500 500" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g><g><rect height="27.4" width="177.9" x="234.3" y="229.5"/></g><g><polygon points="124.8,289.9 87.7,252.8 107.1,233.5 124.8,251.2 169.9,206.1 189.2,225.5   "/></g><g><rect height="27.4" width="177.9" x="234.3" y="120"/></g><g><polygon points="124.8,180.4 87.7,143.3 107.1,124 124.8,141.7 169.9,96.6 189.2,116   "/></g><g><rect height="27.4" width="177.9" x="234.3" y="339"/></g><g><polygon points="124.8,399.4 87.7,362.4 107.1,343 124.8,360.7 169.9,315.6 189.2,335   "/></g></g></svg>
                    </div>
                </div>
            </a>
        </div>

    <?php




    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;

?>