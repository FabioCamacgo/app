<?php

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaEventosFP');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;


    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');


    $dataAtual = date("Y-m-d");
    $temImagem = 0;
    $numItems = 0;
    $totalItems = 8;

    $procuraEventos = VirtualDeskSiteCulturaHelper::getEventos('2', $dataAtual);

    if((int)$procuraEventos < 4){

        $eventosAnteriores = VirtualDeskSiteCulturaHelper::getEventosAnteriores('2', $dataAtual);

        foreach($eventosAnteriores as $rowWSL) :
            $dataInicio = $rowWSL['data_inicio'];
            $dataFim = $rowWSL['data_fim'];
            $temImagem = 0;

            $dataOK = VirtualDeskSiteCulturaHelper::compareDate($dataFim, $dataAtual);

            $id = $rowWSL['id_evento'];
            $referencia = $rowWSL['referencia_evento'];
            $categoria = $rowWSL['categoria'];
            $nomeEvento = $rowWSL['nome_evento'];
            $descricaoEvento = $rowWSL['desc_evento'];
            $fregEvento = $rowWSL['freguesia_evento'];
            $fregName = VirtualDeskSiteCulturaHelper::getNameFreg($fregEvento);
            $ArrStartDate = explode("-",$dataInicio);
            $month = $ArrStartDate[1];
            $title = 'Agenda ' . $nomeMunicipio . ' - '.$nomeEvento;

            $monthEventos = VirtualDeskSiteCulturaHelper::getNameMonth($month);

            $imgEventos = VirtualDeskSiteCulturaHelper::getImgEventos($referencia);

            $name = str_replace(' ', '_', $nomeEvento);
            $code = $id . '_' . $name;

            ?>
            <div class="w33">
                <div class="headItem">
                    <div class="date">
                        <div class="month">
                            <?php echo $monthEventos; ?>
                        </div>
                        <div class="day">
                            <?php echo $ArrStartDate[2]; ?>
                        </div>
                    </div>

                    <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">
                        <div class="image">
                            <?php
                            if((int)$imgEventos == 0){

                                $idCat = VirtualDeskSiteCulturaHelper::getIdCat($categoria);
                                $semimagem = VirtualDeskSiteCulturaHelper::getImagemGeral($idCat);
                                ?>
                                <img src="<?php echo $nomeSite . $image . $semimagem;?>" alt="<?php echo $nomeEvento; ?>"/>
                                <?php
                            } else {
                                $alt=$nomeEvento;
                                $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
                                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                $FileList21Html = '';
                                foreach ($arFileList as $rowFile) {

                                    if($temImagem == 0){
                                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                        ?>
                                        <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>">
                                        <?php
                                        $temImagem = 1;
                                    }
                                }
                            }
                            $numItems = $numItems + 1;

                            ?>

                        </div>
                    </a>

                    <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">
                        <div class="titleEvent">
                            <?php echo $nomeEvento; ?>
                        </div>
                    </a>

                    <div class="catEvent">
                        <div class="borda"></div>
                        <div class="text">
                            <?php echo $categoria ?>
                        </div>
                    </div>

                    <div class="fregEvent">
                        <div class="icon">
                            <svg aria-hidden="true" class="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"/></svg>
                        </div>
                        <div class="text">
                            <?php echo $fregName; ?>
                        </div>
                    </div>


                    <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">

                        <div class="sabermais">
                            <button id="saberMais">
                                <div class="borda"></div>
                                <input type="submit" name="sabermais" id="sabermais" value="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_SABERMAIS'); ?>">
                            </button>

                        </div>
                    </a>

                </div>


            </div>
            <?php
        endforeach;


    } else{
        foreach($procuraEventos as $rowWSL) :
            $dataInicio = $rowWSL['data_inicio'];
            $dataFim = $rowWSL['data_fim'];
            $temImagem = 0;

            $dataOK = VirtualDeskSiteCulturaHelper::compareDate($dataFim, $dataAtual);

            if($dataOK == true){
                $id = $rowWSL['id_evento'];
                $referencia = $rowWSL['referencia_evento'];
                $categoria = $rowWSL['categoria'];
                $nomeEvento = $rowWSL['nome_evento'];
                $descricaoEvento = $rowWSL['desc_evento'];
                $fregEvento = $rowWSL['freguesia_evento'];
                $fregName = VirtualDeskSiteCulturaHelper::getNameFreg($fregEvento);
                $ArrStartDate = explode("-",$dataInicio);
                $month = $ArrStartDate[1];
                $title = 'Agenda ' . $nomeMunicipio . ' - '.$nomeEvento;

                $monthEventos = VirtualDeskSiteCulturaHelper::getNameMonth($month);

                $imgEventos = VirtualDeskSiteCulturaHelper::getImgEventos($referencia);

                $name = str_replace(' ', '_', $nomeEvento);
                $code = $id . '_' . $name;

                ?>
                <div class="w33">
                    <div class="headItem">
                        <div class="date">
                            <div class="month">
                                <?php echo $monthEventos; ?>
                            </div>
                            <div class="day">
                                <?php echo $ArrStartDate[2]; ?>
                            </div>
                        </div>

                        <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">
                            <div class="image">
                                <?php
                                    if((int)$imgEventos == 0){

                                        $idCat = VirtualDeskSiteCulturaHelper::getIdCat($categoria);
                                        $semimagem = VirtualDeskSiteCulturaHelper::getImagemGeral($idCat);
                                        ?>
                                        <img src="<?php echo $nomeSite . $image . $semimagem;?>" alt="<?php echo $nomeEvento; ?>"/>
                                        <?php
                                    } else {
                                        $alt=$nomeEvento;
                                        $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {

                                            if($temImagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>">
                                                <?php
                                                $temImagem = 1;
                                            }
                                        }
                                    }
                                    $numItems = $numItems + 1;

                                    ?>

                            </div>
                        </a>

                        <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">
                            <div class="titleEvent">
                                <?php echo $nomeEvento; ?>
                            </div>
                        </a>

                        <div class="catEvent">
                            <div class="borda"></div>
                            <div class="text">
                                <?php echo $categoria ?>
                            </div>
                        </div>

                        <div class="fregEvent">
                            <div class="icon">
                                <svg aria-hidden="true" class="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"/></svg>
                            </div>
                            <div class="text">
                                <?php echo $fregName; ?>
                            </div>
                        </div>


                        <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">

                            <div class="sabermais">
                                <button id="saberMais">
                                    <div class="borda"></div>
                                    <input type="submit" name="sabermais" id="sabermais" value="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_SABERMAIS'); ?>">
                                </button>

                            </div>
                        </a>

                    </div>


                </div>
                <?php
            }
        endforeach;
    }


echo $headScripts;
echo $footerScripts;
echo $recaptchaScripts;
echo $localScripts;

?>

