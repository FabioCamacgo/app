<?php
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroEventosFP');
    if ($resPluginEnabled === false) exit();


    $obParam      = new VirtualDeskSiteParamsHelper();
    $concelhoCultura = $obParam->getParamsByTag('concelhoCultura');

    if (isset($_POST['submitForm'])) {
        $nomePost = $_POST['nome'];
        $categoriaPost = $_POST['categoria'];
        $freguesiaPost = $_POST['freguesia'];
        $mesPost = $_POST['mes'];
        $anoPost = $_POST['Ano'];

    }
?>


<form id="ProcuraEvento" action="" method="post" class="login-form" enctype="multipart/form-data" >


    <div class="visibleBlock">
        <!--   Nome  -->
        <div class="form-group" id="nomeSearch">
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_NOMEEVENTO'); ?>"
                       name="nome" id="nome" value="<?php echo $nomePost; ?>"/>
            </div>
        </div>
    </div>


    <div class="invisibleBlock">
        <!--   Categoria  -->
        <div class="form-group" id="catSearch">
            <?php $Categorias = VirtualDeskSiteCulturaHelper::getCategoria()?>
            <div class="col-md-9">
                <select name="categoria" value="<?php echo $categoriaPost; ?>" id="categoria" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($categoriaPost)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_CATEGORIA'); ?></option>
                        <?php foreach($Categorias as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['categoria']; ?></option>
                        <?php endforeach;
                    } else{ ?>
                        <option value="<?php echo $categoriaPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getCatSelect($categoriaPost) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeCategoria = VirtualDeskSiteCulturaHelper::excludeCat($categoriaPost)?>
                        <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['categoria']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Freguesia  -->
        <div class="form-group" id="fregSearch">
            <?php $Freguesias = VirtualDeskSiteCulturaHelper::getFreguesia($concelhoCultura)?>
            <div class="col-md-9">
                <select name="freguesia" value="<?php echo $freguesiaPost; ?>" id="freguesia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($freguesiaPost)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_FREGUESIA'); ?></option>
                        <?php foreach($Freguesias as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $freguesiaPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getFregSelect($freguesiaPost) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFreguesia = VirtualDeskSiteCulturaHelper::excludeFreguesia($concelhoCultura, $freguesiaPost)?>
                        <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Ano  -->
        <div class="form-group" id="anoField">
            <?php $Anos = VirtualDeskSiteCulturaHelper::getAnos()?>
            <div class="col-md-9">
                <select name="Ano" value="<?php echo $anoPost; ?>" id="Ano" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($anoPost)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_ANO'); ?></option>
                        <?php foreach($Anos as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['Ano']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $anoPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getAnoSelect($anoPost) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeMes = VirtualDeskSiteCulturaHelper::excludeAno($anoPost)?>
                        <?php foreach($ExcludeMes as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['Ano']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Mes  -->
        <div class="form-group" id="month">
            <?php $Meses = VirtualDeskSiteCulturaHelper::getMeses()?>
            <div class="col-md-9">
                <select name="mes" value="<?php echo $mesPost; ?>" id="mes" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($mesPost)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_MES'); ?></option>
                        <?php foreach($Meses as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['mes']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $mesPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getMesSelect($mesPost) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeMes = VirtualDeskSiteCulturaHelper::excludeMes($mesPost)?>
                        <?php foreach($ExcludeMes as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['mes']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>






    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
    </div>

</form>


<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Cultura/Cultura_FiltroEventosFP.js.php');
    ?>
</script>



