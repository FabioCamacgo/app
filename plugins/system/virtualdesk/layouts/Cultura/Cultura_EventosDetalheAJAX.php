<?php

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheEventos');
    if ($resPluginEnabled === false) exit();

    $obParam      = new VirtualDeskSiteParamsHelper();
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');

    $jinput = JFactory::getApplication()->input;

    $link = $jinput->get('link','' ,'string');

    $ref = explode("name=", $link);

    $nomeEvento = base64_decode ($ref[1]);
    $info = explode("/", $nomeEvento);
    $id= $info[0];
    $infoEvento = VirtualDeskSiteCulturaHelper::getInfoEvento($id);
    foreach($infoEvento as $rowWSL) :
        $nome = $rowWSL['nome_evento'];
        $promotor = $rowWSL['promotor'];
        $referencia = $rowWSL['referencia_evento'];
        $coOrganizacao = $rowWSL['coOrganizacao'];
        $categoria = $rowWSL['categoria'];

        $descricaoEvento = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML( $rowWSL['desc_evento']);

        $fregEvento = $rowWSL['freguesia'];
        $localEvento = $rowWSL['local_evento'];
        $inicioEvento = $rowWSL['data_inicio'];
        $horaInicioEvento = $rowWSL['hora_inicio'];
        $fimEvento = $rowWSL['data_fim'];
        $horaFimEvento = $rowWSL['hora_fim'];
        $layout = $rowWSL['layout'];
        $website = $rowWSL['website'];
        $facebook = $rowWSL['facebook'];
        $instagram = $rowWSL['instagram'];
        $coordenadas[$count++] = $rowWSL['latitude'] . ',' . $rowWSL['longitude'] . ',' . $rowWSL['referencia_evento'];
        $latitude = $rowWSL['latitude'];
        $longitude = $rowWSL['longitude'];
        $observacoes = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML( $rowWSL['observacoes']);
    endforeach;


    if(empty($latitude) || empty($longitude)){
        $latitude = 32.681042557387805;
        $longitude = -17.097659319920133;
    }

    $imgEventos = VirtualDeskSiteCulturaHelper::getImgEventos($referencia);

    $obParam      = new VirtualDeskSiteParamsHelper();
    $goBack = $obParam->getParamsByTag('goBack');

    ?>

<?php if((int)$isVDAjax2Share!=1) : ?>
    <a href="<?php echo $goBack;?>" class="goBackFilter">
        <svg baseProfile="tiny" height="24px" version="1.2" viewBox="0 0 24 24" width="24px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g><path d="M19.164,19.547c-1.641-2.5-3.669-3.285-6.164-3.484v1.437c0,0.534-0.208,1.036-0.586,1.414   c-0.756,0.756-2.077,0.751-2.823,0.005l-6.293-6.207C3.107,12.523,3,12.268,3,11.999s0.107-0.524,0.298-0.712l6.288-6.203   c0.754-0.755,2.073-0.756,2.829,0.001C12.792,5.463,13,5.965,13,6.499v1.704c4.619,0.933,8,4.997,8,9.796v1   c0,0.442-0.29,0.832-0.714,0.958c-0.095,0.027-0.19,0.042-0.286,0.042C19.669,19.999,19.354,19.834,19.164,19.547z M12.023,14.011   c2.207,0.056,4.638,0.394,6.758,2.121c-0.768-3.216-3.477-5.702-6.893-6.08C11.384,9.996,11,10,11,10V6.503l-5.576,5.496l5.576,5.5   V14C11,14,11.738,14.01,12.023,14.011z"/></g></svg>
        <?php echo JText::_('COM_VIRTUALDESK_CULTURA_GOBACK');?>
    </a>
<?php endif; ?>

    <h2 class="nomeEvento">
        <div class="borda"></div>
        <div class="text"><?php echo $nome; ?></div>
    </h2>

<?php
if((int)$isVDAjax2Share===1) {
    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_EventosDetalheShare.php');
}
else {
    if ($layout == 1) {
        require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_EventosDetalheLayout1.php');
    } else {
        require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_EventosDetalheLayout2.php');
    }
}
?>

<script>
    var Pinpath = '<?php echo JUri::base() . 'plugins/system/virtualdesk/layouts/includes/Espera.png'; ?>';
</script>
