var MapsGoogle = function () {

    var mapBasic = function () {
        new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });
    }

    return {
        //main function to initiate map samples
        init: function () {
            mapBasic();
        }

    };

}();

jQuery(document).ready(function() {
    MapsGoogle.init();
});