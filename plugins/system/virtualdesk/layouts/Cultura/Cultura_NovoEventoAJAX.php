

<?php

if (isset($_POST['submitForm'])) {
    $fiscalid = $_POST['fiscalid'];
    $password = $_POST['password'];

    /*Valida Nif*/
    $nif=trim($fiscalid);
    $ignoreFirst=true;
    //Verificamos se é numérico e tem comprimento 9
    if (!is_numeric($nif) || strlen($nif)!=9) {
        $errNif = 1;
    } else if((int)$nifExiste != 0){
        $errNif = 2;
    }  else {
        $nifSplit=str_split($nif);
        //O primeiro digíto tem de ser 1, 2, 5, 6, 8 ou 9
        //Ou não, se optarmos por ignorar esta "regra"
        if (
            in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9))
            ||
            $ignoreFirst
        ) {
            //Calculamos o dígito de controlo
            $checkDigit=0;
            for($i=0; $i<8; $i++) {
                $checkDigit+=$nifSplit[$i]*(10-$i-1);
            }
            $checkDigit=11-($checkDigit % 11);
            //Se der 10 então o dígito de controlo tem de ser 0
            if($checkDigit>=10) $checkDigit=0;
            //Comparamos com o último dígito
            if ($checkDigit==$nifSplit[8]) {

            } else {
                $errNif = 1;
            }
        } else {
            $errNif = 1;
        }
    }


    /*Valida Password*/

    $login = VirtualDeskSiteCulturaHelper::checkLogin($fiscalid);
    $pass = base64_decode($login);

    if($password != $pass){
        $errPassword = 1;
    }



    // Valida Captcha
    $captcha                  = JCaptcha::getInstance($captcha_plugin);
    $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');
    $jinput->set('g-recaptcha-response',$recaptcha_response_field);
    $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
    $errRecaptcha = 0;
    if (!$resRecaptchaAnswer) {
        $errRecaptcha = 1;
    }


    if($errNif == 0 && $errPassword == 0 && $errRecaptcha == 0){


        $estado = VirtualDeskSiteCulturaHelper::checkEstado($fiscalid);

        if($estado == 0){ ?>

            <div class="message"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LOGIN_ERRO'); ?></div>

        <?php } else {

            //require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_NovoEvento.php');
        }


        ?>
        <script>

            //jQuery(window).load(function(event) {
            event.preventDefault();
            var url = '/novoevento/',
                LoginContainer = jQuery(".LoginContainer");
            var data;

            var fiscalid = '#' + <?php echo $fiscalid; ?>;

            jQuery.ajax({
                type: 'POST',
                url: url ,
                success: function(response) {
                    window.location.href = '/novo-evento?fiscalid='+ fiscalid;
                }
            });
            //});





            jQuery('#login').css('display','none');
            jQuery('#registo').css('display','none');
            jQuery('#recoverPassword').css('display','none');
        </script>
        <?php
        exit();

    } else { ?>

        <script>
            dropError();
        </script>
        <div class="backgroundErro">
            <div class="erro" style="display:block;">
                <button id="fechaErro"><svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                        <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                            M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                            c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                            c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                            c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                            c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                            c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                            c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                            C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                    </svg></button>

                <h2>
                    <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18"
                         role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                    <?php echo JText::_('COM_VIRTUALDESK_CULTURA_AVISO'); ?>
                </h2>

                <?php
                if($errNif == 1){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_LOGIN_NIF_1') . '</p>';
                } else if ($errNif == 2){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_LOGIN_NIF_2') . '</p>';
                } else {
                    $errNif = 0;
                }

                if($errPassword == 1){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_LOGIN_PASSWORD_1') . '</p>';
                } else {
                    $errPassword = 0;
                }

                if($errRecaptcha == 1){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_LOGIN_CAPTCHA_1') . '</p>';
                } else {
                    $errRecaptcha = 0;
                }
                ?>
            </div>
        </div>
    <?php }

}
?>

<form id="LoginEvento" action="" method="post" class="login-form" enctype="multipart/form-data" >

    <!--   NIF   -->
    <div class="form-group" id="nif">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?>"
                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
        </div>
    </div>


    <!--   PASSWORD  -->
    <div class="form-group" id="pass">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PASSWORD'); ?></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_PASSWORD'); ?>"
                   name="password" id="password" value="<?php echo htmlentities($password, ENT_QUOTES, 'UTF-8'); ?>" maxlength="20"/>
        </div>
    </div>


    <div style="height:75px;">
        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group">
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>
    </div>



    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_LOGIN'); ?>">
    </div>


</form>
