<?php
$code = $id . '_' . $name;
?>

<meta property="og:url"                content="" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="<?php echo $nome;?>" />
<meta property="og:description"        content="<?php echo $descricaoEvento;?> " />

<script type="text/javascript">
    window.location.href = "<?php echo $obParam->getParamsByTag('CulturaLinkSite') ?>/menu/detalhe-evento?name=<?php echo base64_encode($code);?>";
</script>


<?php
$LinkParaAImagem = '';
if((int)$imgEventos == 0) {
    $idCat = VirtualDeskSiteCulturaHelper::getIdCat($categoria);
    $semimagemVertical = VirtualDeskSiteCulturaHelper::getImagemVertical($idCat);

    $LinkParaAImagem = $nomeSite . $vertical . $semimagemVertical;

} else {
    $alt=$nome;
    $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
    $FileList21Html = '';
    foreach ($arFileList as $rowFile) {
        if($temImagem == 0) {
            $LinkParaAImagem = $rowFile->guestlink;
        }
    }
}
?>



<meta property="og:image" content="<?php echo $LinkParaAImagem; ?>" >

</head>

<body>

<div class="layout2">

    <div class="w30">
        <div class="imagem">
            <?php
            if((int)$imgEventos == 0){
                $idCat = VirtualDeskSiteCulturaHelper::getIdCat($categoria);
                $semimagemVertical = VirtualDeskSiteCulturaHelper::getImagemVertical($idCat);
                ?>
                <img src="<?php echo $nomeSite . $vertical . $semimagemVertical;?>" alt="<?php echo $nome; ?>"/>
                <?php
            } else {
                $alt=$nome;
                $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                $FileList21Html = '';
                foreach ($arFileList as $rowFile) {
                    if($temImagem == 0){
                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                        ?>
                        <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>">
                        <?php
                        $temImagem = 1;
                    }
                }
            }
            ?>
        </div>
    </div>


    <div class="w70">
        <div class="detalhes normal">

            <div class="w50">
                <div class="catEvent">
                    <div class="w10" title="Categoria">
                        <div class="borda"></div>
                    </div>
                    <div class="w90">
                        <div class="text">
                            <?php echo $categoria ?>
                        </div>
                    </div>
                </div>


                <div class="placeEvento">
                    <div class="w90">
                        <div class="freguesia"><?php echo $fregEvento; ?></div>
                        <div class="local"><?php echo $localEvento; ?></div>
                    </div>
                </div>
            </div>

            <div class="w50">
                <div class="date">

                    <div class="w90">
                        <?php
                        $dateInicio = explode("-", $inicioEvento);
                        $dateFim = explode("-", $fimEvento);
                        ?>

                        <div class="diaMes">
                            <?php
                            if($dateInicio[2] == $dateFim[2] && $dateInicio[1] == $dateFim[1]){
                                echo $dateInicio[2] . '.' . $dateInicio[1];
                            } else{
                                echo $dateInicio[2] . '.' . $dateInicio[1] . ' - ' . $dateFim[2] . '.' . $dateFim[1];
                            }
                            ?>
                        </div>

                        <div class="ano">
                            <?php
                            if($dateInicio[0] == $dateFim[0]){
                                echo $dateInicio[0];
                            } else {
                                echo $dateInicio[0] . '-' . $dateFim[0];
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="promotor">
                    <div class="w90">
                        <div class="text">
                            <?php echo $promotor ?>
                        </div>
                    </div>
                </div>

            </div>

        </div>






    </div>