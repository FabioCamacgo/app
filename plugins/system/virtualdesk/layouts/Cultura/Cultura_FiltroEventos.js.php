<?php
defined('_JEXEC') or die;
?>


var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(window).scroll(function(){
    if (jQuery(this).scrollTop() > 145) {
        jQuery('#ListaEventos .w30').css('position','fixed');
        jQuery('#ListaEventos .w30').css('width','282.19px');
        jQuery('#ListaEventos .w30').css('top','145px');
        jQuery('#peOustide').css('position','fixed');
        jQuery('#peOustide').css('width','282.19px');
        jQuery('#peOustide').css('top','395px');
    } else {
        jQuery('#ListaEventos .w30').css('position','relative');
        jQuery('#ListaEventos .w30').css('width','282.19px');
        jQuery('#ListaEventos .w30').css('top','auto');
        jQuery('#peOustide').css('position','relative');
        jQuery('#peOustide').css('width','282.19px');
        jQuery('#peOustide').css('top','auto');
    }
});


jQuery(document).ready(function(){
    ComponentsSelect2.init();

    jQuery("#openFilter").click(function(){
        jQuery("#ListaEventos .w30").css('left','0');
        jQuery("#peOustide").css('left','20px');
    });

    jQuery("#goBackList").click(function(){
        jQuery("#ListaEventos .w30").css('left','-100%');
        jQuery("#peOustide").css('left','-100%');
    });
});

