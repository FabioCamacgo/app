<?php

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

/* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
$obParam              = new VirtualDeskSiteParamsHelper();
$arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('submeterevento');
if ($resPluginEnabled === false) exit();


//LOCAL SCRIPTS

//$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;

//GLOBAL SCRIPTS
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;


//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css' . $addcss_end;

//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

//$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.min.css'. $addcss_end;

//Fileuploader
$headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
}

echo $headCSS;

$obParam      = new VirtualDeskSiteParamsHelper();
$concelhoCultura = $obParam->getParamsByTag('concelhoCultura');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
$nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
$linkPoliticaCultura = $obParam->getParamsByTag('linkPoliticaCultura');

?>

<style>
    .fileuploader-popup {z-index: 99999 !important;}
</style>
<?php

if (isset($_POST['submitForm'])) {
    $nifIntro = $_POST['nifIntro'];
    $fiscalid = $_POST['fiscalid'];
    $confEmail = $_POST['confEmail'];
    $email = $_POST['email'];
    $coOrganizacao = $_POST['coOrganizacao'];
    $nomePromotor = $_POST['nomePromotor'];
    $categoria = $_POST['categoria'];
    $nomeEvento = $_POST['nomeEvento'];
    $descricao = $_POST['descricao'];
    $freguesia = $_POST['freguesia'];
    $localEvento = $_POST['localEvento'];
    $dataInicio = $_POST['dataInicio'];
    $dataFim = $_POST['dataFim'];
    $horaInicio = $_POST['horaInicio'];
    $horaFim = $_POST['horaFim'];
    $websiteEvento = $_POST['websiteEvento'];
    $facebookEvento = $_POST['facebookEvento'];
    $instagramEvento = $_POST['instagramEvento'];
    $observacoes = $_POST['observacoes'];
    $coordenadas = $_POST['coordenadas'];
    $splitCoord = explode(",", $coordenadas);
    $lat = $splitCoord[0];
    $long = $splitCoord[1];
    $layout = $_POST['layout'];
    $politica2 = $_POST['politica2'];
    $veracid2 = $_POST['veracid2'];


    if(!empty($dataInicio) && !empty($dataFim)){
        $dataInicioFormated = VirtualDeskSiteCulturaHelper::revertDataOrder($dataInicio);
        $dataFimFormated = VirtualDeskSiteCulturaHelper::revertDataOrder($dataFim);
        $currentDate= date("Y-m-d");
        $currentYear = date("Y");
        $currentMonth = date("m");
        $currentDay = date("d");
    }

    if(empty($dataInicio)){
        $errDataInicio = 1;
    }

    if(empty($dataFim)){
        $errDataFim = 1;
    }


    /*Valida Nif*/
    $nif=trim($fiscalid);
    $ignoreFirst=true;

    $nifExiste = VirtualDeskSiteCulturaHelper::getNIF($nif);

    if(strlen($nif)!=9){

    } else if((int)$nifExiste == 0){

        /*Valida Nome Promotor*/
        if($nomePromotor == Null){
            $errNomePromotor = 1;
        } else if(is_numeric($nomePromotor)){
            $errNomePromotor = 2;
        }

        $conta = "/^[a-zA-Z0-9\._-]+@";
        $domino = "[a-zA-Z0-9\._-]+.[.]";
        $extensao = "([a-zA-Z]{2,4})$/";
        $pattern = $conta . $domino . $extensao;
        $ExisteMail = VirtualDeskSiteCulturaHelper::seeEmailExiste($email);
        if($email == Null){
            $errEmail = 1;
        } else if(!preg_match($pattern, $email)) {
            $errEmail = 2;
        } else if((int)$ExisteMail > 0){
            $errEmail = 3;
        } else if($email != $confEmail){
            $errEmail = 4;
        }

    } else {
        $errNomePromotor = 0;
        $errEmail = 0;
    }

    if (!is_numeric($nif) || strlen($nif)!=9) {
        $errNif = 1;
    }else {
        $nifSplit=str_split($nif);
        if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
            $checkDigit=0;
            for($i=0; $i<8; $i++) {
                $checkDigit+=$nifSplit[$i]*(10-$i-1);
            }
            $checkDigit=11-($checkDigit % 11);

            if($checkDigit>=10) $checkDigit=0;

            if ($checkDigit==$nifSplit[8]) {

            } else {
                $errNif = 1;
            }
        } else {
            $errNif = 1;
        }
    }

    /*Valida Layout*/
    if($layout == Null){
        $errLayout = 1;
    }

    /*Valida Categoria*/
    if($categoria == Null){
        $errCategoria = 1;
    }

    /*Valida Nome Evento*/
    if($nomeEvento == Null){
        $errNome = 1;
    }

    /*Valida Descricao*/
    if($descricao == Null){
        $errDescricao = 1;
    }

    /*Valida Freguesia*/
    if($freguesia == Null){
        $errFreguesia = 1;
    }

    /*Valida Local Evento*/
    if($localEvento == Null){
        $errLocal = 1;
    }

    /*Valida CoOrganização*/
    if($coOrganizacao != 1 && $coOrganizacao != 2){
        $errCoOrganizacao = 1;
    }

    /*Validações Recaptcha*/
    $captcha                  = JCaptcha::getInstance($captcha_plugin);
    $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

    $errRecaptcha = 1;
    if((string)$recaptcha_response_field !='') {
        $jinput->set('g-recaptcha-response',$recaptcha_response_field);
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        $errRecaptcha = 0;
        if (!$resRecaptchaAnswer) {
            $errRecaptcha = 1;
        }
    }

    if($errCoOrganizacao == 0 && $veracid2 == 1 && $politica2 == 1 && $errDataInicio == 0 && $errDataFim == 0 && $errEmail == 0 && $errNomePromotor == 0 && $errLayout == 0 && $errNif == 0 && $errCategoria == 0 && $errNome == 0 && $errDescricao == 0 && $errFreguesia == 0 && $errLocal == 0 && $errFreguesia == 0 && $errRecaptcha == 0){

        $splitData = explode("-", $dataInicioFormated);
        $splitData[0];
        $splitData[1];
        $splitData[2];

        $splitData2 = explode("-", $dataFimFormated);
        $splitData2[0];
        $splitData2[1];
        $splitData2[2];


        $referencia = VirtualDeskSiteCulturaHelper::RefEvent($fiscalid, $splitData[2], $splitData[1], $splitData2[2], $splitData2[1]);

        $idMonth = VirtualDeskSiteCulturaHelper::getIdMonth($splitData[1]);

        $idAno = $splitData[0];

        $db = JFactory::getDbo();
        if(!empty($descricao)) $descricao  = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao));


        $saveEvento = VirtualDeskSiteCulturaHelper::SaveNovoEvento($fiscalid, $coOrganizacao, $categoria, $referencia, $nomeEvento, $descricao, $freguesia, $localEvento, $dataInicioFormated, $horaInicio, $dataFimFormated, $horaFim, $idMonth, $idAno, $lat, $long, $websiteEvento, $facebookEvento, $instagramEvento, $observacoes, $layout);

        if((int)$nifExiste == 0){

            $email = $_POST['email'];
            $nomePromotor = $_POST['nomePromotor'];

            $resSaveRegisto = VirtualDeskSiteCulturaHelper::SaveNovoRegistoV2($fiscalid, $nomePromotor, $email);
        } else {
            $resSaveRegisto = true;
        }

        if($saveEvento == true && $resSaveRegisto == true) { ?>

            <div class="sucesso">

                <div class="sucessIcon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10" stroke-width="2"/><path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg></div>

                <p><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_SUCESSO'); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_SUCESSO1'); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_SUCESSO3'); ?></p>
                <p><?php echo JText::sprintf('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_SUCESSO4',$nomeMunicipio); ?></p>
                <p><?php echo JText::sprintf('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_SUCESSO5',$copyrightAPP); ?></p>
            </div>

            <script>
                setTimeout(function(){
                    jQuery('#registo').css('display','none');
                    jQuery('#DivBotaoSubmeterEvento').css('display','none');
                    jQuery('#submitEvento').css('display','none');
                }, 500);

            </script>

            <?php

            echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');

            $emailUser = VirtualDeskSiteCulturaHelper::getUserEmail($fiscalid);
            $nameUser = VirtualDeskSiteCulturaHelper::getUserName($fiscalid);
            $freguesiaName = VirtualDeskSiteCulturaHelper::getNameFreg($freguesia);
            $catName = VirtualDeskSiteCulturaHelper::getCatSelect($categoria);
            $StartDate = $splitData[2] . '-' . $splitData[1] . '-' . $splitData[0];
            $EndDate = $splitData2[2] . '-' . $splitData2[1] . '-' . $splitData2[0];
            $descEmail = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML ($db->escape($descricao));

            if(!empty($websiteEvento)){
                $web = $websiteEvento;
            } else {
                $web = JText::_('COM_VIRTUALDESK_CULTURA_MAIL_VAZIO');
            }

            if(!empty($facebookEvento)){
                $fac = $facebookEvento;
            } else {
                $fac = JText::_('COM_VIRTUALDESK_CULTURA_MAIL_VAZIO');
            }

            if(!empty($instagramEvento)){
                $inst = $instagramEvento;
            } else {
                $inst = JText::_('COM_VIRTUALDESK_CULTURA_MAIL_VAZIO');
            }

            VirtualDeskSiteCulturaHelper::SendMailClientEvento($nameUser, $nomeEvento, $catName, $emailUser, $freguesiaName, $localEvento, $StartDate, $EndDate, $descEmail, $web, $fac, $inst, $nomeMunicipio);
            VirtualDeskSiteCulturaHelper::SendMailAdminEvento($nomeMunicipio, $nameUser, $referencia);

            exit();

        } else { ?>

            <div class="message"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO'); ?></div>

        <?php }


    } else {
        ?>
        <script>
            dropError();
        </script>

        <div class="backgroundErro">
            <div class="erro" style="display:block;">
                <button id="fechaErro">
                    <svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                                <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                            c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                            c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                            c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                            c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                            c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                            c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                            C42.996,43.384,41.986,44.001,40.361,44.02z">
                                </path>
                            </svg>
                </button>

                <h2>
                    <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18"
                         role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                    <?php echo JText::_('COM_VIRTUALDESK_CULTURA_AVISO'); ?>
                </h2>

                <?php

                if($errNif == 2){
                    echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NIF_2') . '</p>';
                } else if($errNif == 3){
                    echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NIF_3') . '</p>';
                } else {
                    if($errNif == 1){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NIF_1') . '</p>';
                    } else{
                        $errNif = 0;
                    }


                    if(strlen($nif)==9){
                        if($errNomePromotor == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NOME_1') . '</p>';
                        } else if($errNomePromotor == 2){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NOME_2') . '</p>';
                        } else if($errNomePromotor == 3){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NOME_3') . '</p>';
                        } else{
                            $errNomePromotor = 0;
                        }

                        if($errEmail == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_1') . '</p>';
                        } else if($errEmail == 2){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_2') . '</p>';
                        } else if($errEmail == 3){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_3') . '</p>';
                        } else if($errEmail == 4){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_4') . '</p>';
                        } else{
                            $errEmail = 0;
                        }

                        if($errCoOrganizacao == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::sprintf('COM_VIRTUALDESK_CULTURA_ERRO_COORGANIZACAO', $copyrightAPP) . '</p>';
                        }

                        if($errCategoria == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_CATEGORIA_1') . '</p>';
                        } else{
                            $errCategoria = 0;
                        }

                        if($errNome == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NOME_1') . '</p>';
                        } else if($errNome == 2){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NOME_2') . '</p>';
                        } else if($errNome == 3){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NOME_3') . '</p>';
                        } else{
                            $errNome = 0;
                        }

                        if($errDescricao == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_DESCRICAO_1') . '</p>';
                        } else{
                            $errDescricao = 0;
                        }

                        if($errFreguesia == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_FREGUESIA_1') . '</p>';
                        } else{
                            $errFreguesia = 0;
                        }

                        if($errLocal == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_LOCAL_1') . '</p>';
                        } else if($errLocal == 2){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_LOCAL_2') . '</p>';
                        } else if($errLocal == 3){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_LOCAL_3') . '</p>';
                        } else{
                            $errLocal = 0;
                        }

                        if($errDataInicio == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_DATAINICIO') . '</p>';
                        } else {
                            $errDataInicio = 0;
                        }

                        if($errDataFim == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_DATAFIM') . '</p>';
                        } else {
                            $errDataFim = 0;
                        }

                        if($errLayout == 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_LAYOUT_1') . '</p>';
                        } else {
                            $errLayout = 0;
                        }

                        if($veracid2 != 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_VERACIDADE_1') . '</p>';
                        }

                        if($politica2 != 1){
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_POLITICA_1') . '</p>';
                        }

                        if($errRecaptcha == 1) {
                            echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_CAPTCHA') . '</p>';
                        }else{
                            $errRecaptcha = 0;
                        }
                    }
                }

                ?>

            </div>
        </div>
        <?php
    }

}

?>



<form id="CriaEvento" action="" method="post" class="login-form" enctype="multipart/form-data" >

    <h2><?php echo JText::_('COM_VIRTUALDESK_CULTURA_TITLE_NOVOEVENTO'); ?></h2>


    <!--   NIF Associação  -->
    <div class="form-group" id="nif">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>

            <input type="hidden" id="nifIntro" name="nifIntro" value="<?php echo $nifIntro; ?>">
        </div>
    </div>


    <div class="section UserNifExiste" style="display: none;">
        <p><i class="fa fa-check font-green-jungle"></i> <?php echo JText::_('COM_VIRTUALDESK_CULTURA_PROMOTOR_NIFEXISTE'); ?></p>
    </div>

    <div class="section UserNifNaoExiste" style="display: none;">
        <p><i class="fa fa-warning font-red-pink"></i> <?php echo JText::_('COM_VIRTUALDESK_CULTURA_PROMOTOR_NIFNAOEXISTE'); ?></p>
    </div>

    <div class="section UserNifInvalido" style="display: none;">
        <p><i class="fa fa-warning font-red-pink"></i> <?php echo JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NIF_1'); ?></p>
    </div>




    <div class="section User">


        <h3><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INFO_PROMOTOR'); ?></h3>

        <!--   NOME   -->
        <div class="form-group" id="nomeGroup">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOME_LABEL'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="email" id="nomePromotor" maxlength="100" value="<?php echo $nomePromotor; ?>"/>
            </div>
        </div>


        <!--   EMAIL  -->
        <div class="form-group" id="mail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_EMAIL'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" name="email" id="email" value="<?php echo htmlentities($email, ENT_QUOTES, 'UTF-8'); ?>" />
            </div>
        </div>


        <!--   CONFIRMAÇÃO DE EMAIL  -->
        <div class="form-group" id="confmail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CONFEMAIL'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" name="confEmail" id="confEmail" value="<?php echo htmlentities($confEmail, ENT_QUOTES, 'UTF-8'); ?>" />
            </div>
        </div>
    </div>


    <!-- Coorganizador -->
    <div class="form-group" id="coOrganizador">
        <label class="col-md-3 control-label"><?php echo JText::sprintf('COM_VIRTUALDESK_CULTURA_COORGANIZACAO', $copyrightAPP); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="radio" name="radioval" id="sim" value="sim" <?php if (isset($_POST["submitForm"]) && $_POST['coOrganizacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo 'Sim'; ?>
            <input type="radio" name="radioval" id="nao" value="nao" <?php if (isset($_POST["submitForm"]) && $_POST['coOrganizacao'] == 2) { echo 'checked="checked"'; } ?>> <?php echo 'Não'; ?>
        </div>

        <input type="hidden" id="coOrganizacao" name="coOrganizacao" value="<?php echo $coOrganizacao; ?>">
    </div>


    <?php
        if($coOrganizacao == 1){
            ?>
            <script>
                document.getElementById('sim').onclick = function() {
                    document.getElementById("coOrganizacao").value = 1;
                };

                document.getElementById('nao').onclick = function() {
                    document.getElementById("coOrganizacao").value = 2;
                };

            </script>
        <?php
        } else{
            ?>
                <script>
                    document.getElementById('sim').onclick = function() {
                        document.getElementById("coOrganizacao").value = 1;
                    };

                    document.getElementById('nao').onclick = function() {
                        document.getElementById("coOrganizacao").value = 2;
                    };

                </script>

            <?php
        }
    ?>


    <div class="section Evento">

        <h3><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INFO_EVENTO'); ?></h3>


        <!--   Nome do evento  -->
        <div class="form-group" id="eventName">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOMEEVENTO_LABEL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="nomeEvento" id="nomeEvento" maxlength="100" value="<?php echo $nomeEvento; ?>"/>
            </div>
        </div>


        <!--  Descrição Evento  -->
        <div class="form-group" id="desc">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DESCRICAO_LABEL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control wysihtml5" rows="5" name="descricao" id="descricao" maxlength="4000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao);  ?></textarea>
            </div>
        </div>


        <!--   Categoria  -->
        <div class="form-group" id="cat">
            <label class="col-md-3 control-label" for="field_categoria"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CATEGORIA_LABEL') ?><span class="required">*</span></label>
            <?php $Categorias = VirtualDeskSiteCulturaHelper::getCategoria()?>
            <div class="col-md-9">
                <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($categoria)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_OPTION'); ?></option>
                        <?php foreach($Categorias as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['categoria']; ?></option>
                        <?php endforeach;
                    } else{ ?>
                        <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteCulturaHelper::getCatSelect($categoria) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeCategoria = VirtualDeskSiteCulturaHelper::excludeCat($categoria)?>
                        <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['categoria']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Freguesia  -->
        <div class="form-group" id="freg">
            <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FREGUESIAS_LABEL') ?><span class="required">*</span></label>
            <?php $Freguesias = VirtualDeskSiteCulturaHelper::getFreguesia($concelhoCultura)?>
            <div class="col-md-9">
                <select name="freguesia" value="<?php echo $freguesia; ?>" id="freguesia" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($freguesia)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_OPTION'); ?></option>
                        <?php foreach($Freguesias as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteCulturaHelper::getFregSelect($freguesia) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFreguesia = VirtualDeskSiteCulturaHelper::excludeFreguesia($concelhoCultura, $freguesia)?>
                        <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--  Local do evento  -->
        <div class="form-group" id="eventPlace">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LOCALEVENTO_LABEL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="localEvento" id="localEvento" maxlength="100" value="<?php echo $localEvento; ?>"/>
            </div>
        </div>



        <!--   Data de inicio  -->
        <div class="form-group" id="eventStart">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAINICIO_LABEL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" name="dataInicio" id="dataInicio" value="<?php echo $dataInicio; ?>">
                    <span class="input-group-btn">
                        <button class="btn default" disabled="disabled" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>


        <!--   Hora de inicio  -->
        <div class="form-group" id="eventStartHour">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAINICIO_LABEL'); ?></label>
            <div class="col-md-9">
                <div class="input-group timepicker">
                    <input class="form-control m-input" name="horaInicio" id="m_timepicker_2" value="<?php echo $horaInicio;?>" type="text" />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="la la-clock-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!--   Data de fim  -->
        <div class="form-group" id="eventEnd">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAFIM_LABEL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" name="dataFim" id="dataFim" value="<?php echo $dataFim; ?>">
                    <span class="input-group-btn">
                        <button class="btn default" disabled="disabled" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>




        <!--   Hora de fim  -->
        <div class="form-group" id="eventEndHour">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAFIM_LABEL'); ?></label>
            <div class="col-md-9">
                <div class="input-group timepicker">
                    <input class="form-control m-input" name="horaFim" id="m_timepicker_3" value="<?php echo $horaFim;?>" type="text" />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="la la-clock-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--  website  -->
        <div class="form-group" id="eventwebsite">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_WEBSITEEVENTO_LABEL'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="websiteEvento" id="websiteEvento" maxlength="250" value="<?php echo $websiteEvento; ?>"/>
            </div>
        </div>


        <!--  Facebook  -->
        <div class="form-group" id="eventfacebook">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FACEBOOKEVENTO_LABEL'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="facebookEvento" id="facebookEvento" maxlength="250" value="<?php echo $facebookEvento; ?>"/>
            </div>
        </div>


        <!--  Instagram  -->
        <div class="form-group" id="eventinstagram">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INSTAGRAMEVENTO_LABEL'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="instagramEvento" id="instagramEvento" maxlength="250" value="<?php echo $instagramEvento; ?>"/>
            </div>
        </div>


        <!--   MAPA   -->
        <div class="form-group" id="mapa">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_MAPA'); ?></label>
            <div class="col-md-9">
                <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                <input type="hidden" required class="form-control"
                       name="coordenadas" id="coordenadas"
                       value="<?php echo htmlentities($this->data->$coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
            </div>
        </div>



        <!--   Upload Imagens   -->
        <div class="form-group" id="uploadField">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_UPLOAD'); ?>
                <span class="informationButton">
                    <svg data-name="Layer 1" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg"><path d="M64,32A32,32,0,1,0,32,64,32,32,0,0,0,64,32Zm-5.57,0A26.43,26.43,0,1,1,32,5.57,26.45,26.45,0,0,1,58.43,32Z" data-name="&lt;Compound Path&gt;"/><rect height="6.96" width="6.96" x="28.52" y="12.19"/><polygon points="35.48 44.39 35.48 30.85 35.48 23.89 28.88 23.89 25.65 23.89 25.65 30.85 28.88 30.85 28.88 44.39 25.65 44.39 25.65 51.35 28.88 51.35 35.48 51.35 38.35 51.35 38.35 44.39 35.48 44.39"/></svg>
                </span>

                <div class="information">
                    <?php echo JText::_('COM_VIRTUALDESK_CULTURA_UPLOAD_SIZE_TITLE'); ?>
                    <ul>
                        <li>
                            <svg height="512px" style="enable-background:new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" ><polygon points="160,115.4 180.7,96 352,256 180.7,416 160,396.7 310.5,256 "/></svg>
                            <?php echo JText::_('COM_VIRTUALDESK_CULTURA_UPLOAD_SIZE_HORIZONTAL'); ?>
                        </li>
                        <li>
                            <svg height="512px" style="enable-background:new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" ><polygon points="160,115.4 180.7,96 352,256 180.7,416 160,396.7 310.5,256 "/></svg>
                            <?php echo JText::_('COM_VIRTUALDESK_CULTURA_UPLOAD_SIZE_VERTICAL'); ?>
                        </li>
                    </ul>
                </div>

            </label>
            <div class="col-md-9">
                <div class="file-loading"> <!-- disabled="disabled" -->
                    <input type="file"  name="fileupload[]" id="fileupload" multiple>
                </div>
                <div id="errorBlock" class="help-block"></div>
                <input type="hidden" id="VDAjaxReqProcRefId">

            </div>
        </div>



        <!--   Layout  -->
        <div class="form-group" id="lay">
            <label class="col-md-3 control-label" for="field_layout"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LAYOUT') ?><span class="required">*</span></label>
            <?php $Layout = VirtualDeskSiteCulturaHelper::getLayout()?>
            <div class="col-md-9">
                <select name="layout" value="<?php echo $layout; ?>" id="layout" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($layout)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_OPTION'); ?></option>
                        <?php foreach($Layout as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['layout']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $layout; ?>"><?php echo VirtualDeskSiteCulturaHelper::getLayoutSelect($layout) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeLayout = VirtualDeskSiteCulturaHelper::excludeLayout($layout)?>
                        <?php foreach($ExcludeLayout as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['layout']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>




        <!--  OBSERVAÇÕES  -->
        <div class="form-group" id="obs">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_OBSERVACOES'); ?></label>
            <div class="col-md-9">
                <textarea required class="form-control wysihtml5" rows="5" name="observacoes" id="observacoes" maxlength="500"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($observacoes); ?></textarea>
            </div>
        </div>


        <!--   Veracidade -->
        <div class="form-group" id="Veracidade">

            <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_VERACIDADE'); ?><span class="required">*</span></label>

            <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
        </div>

        <script>

            document.getElementById('veracid').onclick = function() {
                // access properties using this keyword
                if ( this.checked ) {
                    document.getElementById("veracid2").value = 1;
                } else {
                    document.getElementById("veracid2").value = 0;
                }
            };
        </script>

        <!--   Politica de privacidade -->
        <div class="form-group" id="PoliticaPrivacidade">

            <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
            <label class="col-md-3 control-label">Declaro que li e aceito a <a href="<?php echo $linkPoliticaCultura;?>" target="_blank">Pol&iacute;tica de Privacidade</a>. <span class="required">*</span></label>

            <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
        </div>

        <script>

            document.getElementById('politica').onclick = function() {
                // access properties using this keyword
                if ( this.checked ) {
                    document.getElementById("politica2").value = 1;
                } else {
                    document.getElementById("politica2").value = 0;
                }
            };
        </script>


        <!--   Verificação de Segurança  -->
        <div style="height:75px;">
            <?php if ($captcha_plugin!='0') : ?>
                <div class="form-group">
                    <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                    $field_id = 'dynamic_recaptcha_1';
                    print $captcha->display($field_id, $field_id, 'g-recaptcha');
                    ?>
                    <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                </div>
            <?php endif; ?>
        </div>


        <div id="DivBotaoSubmeterEvento" class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="Submeter Evento">
        </div>

    </div>

</form>


<?php
echo $headScripts;
echo $footerScripts;
echo $recaptchaScripts;
echo $localScripts;

$EncontrouNif = (int)VirtualDeskSiteCulturaHelper::getNIF($nif);

?>

<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'submeterevento/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
    var iconpath = '<?php echo JUri::base() . 'plugins/system/virtualdesk/layouts/includes/Espera.png'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Cultura/Cultura_submeterEvento.js.php');
    ?>
</script>


