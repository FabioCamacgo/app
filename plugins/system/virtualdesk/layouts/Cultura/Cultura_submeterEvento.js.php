<?php
defined('_JEXEC') or die;

$pathDelimitacaoMapa = $obParam->getParamsByTag('pathDelimitacaoMapa');
?>

function dropError(){
    jQuery('.backgroundErro').css('display','block');
}

function turnDown(){
    jQuery('#categoria').prop("disabled", true);
    jQuery('#nomeEvento').prop("disabled", true);
    jQuery('#freguesia').prop("disabled", true);
    jQuery('#localEvento').prop("disabled", true);
    jQuery('#dataInicio').prop("disabled", true);
    jQuery('#eventStart .btn').prop("disabled", true);
    jQuery('#m_timepicker_2').prop("disabled", true);
    jQuery('#dataFim').prop("disabled", true);
    jQuery('#eventEnd .btn').prop("disabled", true);
    jQuery('#m_timepicker_3').prop("disabled", true);
    jQuery('#websiteEvento').prop("disabled", true);
    jQuery('#facebookEvento').prop("disabled", true);
    jQuery('#instagramEvento').prop("disabled", true);
    jQuery('#gmap_marker').prop("disabled", true);
    jQuery('#coOrganizador').prop("disabled", true);
    jQuery('#sim').prop("disabled", true);
    jQuery('#nao').prop("disabled", true);
    jQuery('.wysihtml5-editor').prop("disabled", true);

    var elemFileUploader = jQuery('#fileupload');
    var apiFileUploader  = jQuery.fileuploader.getInstance(elemFileUploader);
    apiFileUploader.disable();

    jQuery('#veracid').prop("disabled", true);
    jQuery('#politica').prop("disabled", true);
    jQuery('#layout').prop("disabled", true);
    jQuery('.section.Evento').css('opacity','0.3');
    jQuery('#coOrganizador').css('opacity','0.3');

}

function turnUp(){
    jQuery('#categoria').prop("disabled", false);
    jQuery('#nomeEvento').prop("disabled", false);
    jQuery('#freguesia').prop("disabled", false);
    jQuery('#localEvento').prop("disabled", false);
    jQuery('#dataInicio').prop("disabled", false);
    jQuery('#eventStart .btn').prop("disabled", false);
    jQuery('#m_timepicker_2').prop("disabled", false);
    jQuery('#dataFim').prop("disabled", false);
    jQuery('#eventEnd .btn').prop("disabled", false);
    jQuery('#m_timepicker_3').prop("disabled", false);
    jQuery('#websiteEvento').prop("disabled", false);
    jQuery('#facebookEvento').prop("disabled", false);
    jQuery('#instagramEvento').prop("disabled", false);
    jQuery('#gmap_marker').prop("disabled", false);
    jQuery('#coOrganizador').prop("disabled", false);
    jQuery('#sim').prop("disabled", false);
    jQuery('#nao').prop("disabled", false);
    jQuery('.wysihtml5-editor').prop("disabled", false);

    var elemFileUploader = jQuery('#fileupload');
    var apiFileUploader  = jQuery.fileuploader.getInstance(elemFileUploader);
    apiFileUploader.enable();

    jQuery('#veracid').prop("disabled", false);
    jQuery('#politica').prop("disabled", false);
    jQuery('#layout').prop("disabled", false);
    jQuery('.section.Evento').css('opacity','1');
    jQuery('#coOrganizador').css('opacity','1');

}

function checkFiscalIdPromotor()
{
    document.getElementById("nifIntro").value = document.getElementById("fiscalid").value;
    if(document.getElementById("nifIntro").value.length == 9){

        // Chedk NIF
        var virtualDeskNIFCheckOptions = {
            message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NIF_1'); ?>"
        };
        var resNIFCheckJS = jQuery().virtualDeskNIFCheck(document.getElementById("nifIntro").value,virtualDeskNIFCheckOptions );
        if(resNIFCheckJS.return!==true) {
            jQuery('.section.UserNifInvalido').css('display','inline-block');
            jQuery('.section.UserNifExiste').hide();
            jQuery('.section.UserNifNaoExiste').hide();
            return(false);
        }

        // Check Nif Exists
        jQuery.ajax({
            type: 'GET',
            url: '<?php echo JUri::base() . 'eventoCheckNIFPromotor/'; ?>',
            data: {  n :  document.getElementById("nifIntro").value },
            success: function(response) {
                var d = JSON.parse(response);

                if(d.nifExiste == 1) {
                    jQuery('.section.User').css('display','none');
                    jQuery('.section.UserNifExiste').css('display','inline-block');
                    jQuery('.section.UserNifNaoExiste').hide();
                    jQuery('.section.UserNifInvalido').hide();
                }
                else {
                    jQuery('.section.User').css('display','block');
                    jQuery('.section.UserNifExiste').hide();
                    jQuery('.section.UserNifNaoExiste').css('display','inline-block');
                    jQuery('.section.UserNifInvalido').hide();
                }
            },
            error: function(xhr, status, error) {
                var err = 'Erro ao Check NIF:' + error;
            }
        });

       turnUp();

    } else {
        jQuery('.section.User').css('display','none');
        jQuery('.section.UserNifExiste').hide();
        jQuery('.section.UserNifNaoExiste').hide();
        jQuery('.section.UserNifInvalido').hide();

        turnDown();
    }

}

var MapsGoogle = function () {

    var mapMarker = function () {

        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });


        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconpath
        });

        map.setZoom(14);

        GMaps.on('click', map.map, function(event) {

            map.removeMarkers();

            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            jQuery('#coordenadas').val(lat + ',' + lng);

            map.addMarker({
                lat: lat,
                lng: lng,
                //title: 'Marker #' + index,
                icon: iconpath,
                infoWindow: {
                    content : ''
                }

            });
        });

        <?php require_once (JPATH_SITE . $pathDelimitacaoMapa); ?>

    }

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };

}();

<?php require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps_default_Cultura.js.php'); ?>

var ComponentsEditors = function () {

    var handleWysihtml5 = function () {
        if (!wysihtml5.browser.supported()) {
            //return;
        }

        jQuery('.wysihtml5').wysihtml5({
            toolbar: {
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false, //Button to change color of font
                "blockquote": false, //Blockquote
                "size": 'sm' //default: none, other options are xs, sm, lg
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();

        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 1,

            // if null - has no limits
            // example: 3
            fileMaxSize: 5,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png', 'pdf'],

            enableApi: true,


            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = jQuery.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = jQuery.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - null
            upload: {
                // upload URL {String}
                url: setVDCurrentRelativePath , // definido no php

                // upload data {null, Object}
                // you can also change this Object in beforeSend callback
                // example: { option_1: '1', option_2: '2' }
                data:  null ,

                // upload type {String}
                // for more details http://api.jquery.com/jquery.ajax/
                type: 'POST',

                // upload enctype {String}
                // for more details http://api.jquery.com/jquery.ajax/
                enctype: 'multipart/form-data',

                // auto-start file upload {Boolean}
                // if false, you can use the API methods - item.upload.send() to trigger upload for each file
                // if false, you can use the upload button - check custom file name example
                start: false, // true,

                // upload the files synchron {Boolean}
                synchron: true,

                // upload large files in chunks {false, Number} set file chunk size in MB as Number (ex: 4)
                chunk: false,

                // Callback fired before uploading a file by returning false, you can prevent the upload
                beforeSend: function(item, listEl, parentEl, newInputEl, inputEl) {
                    // example:
                    // here you can extend the upload data

                    item.upload.data.isVDAjaxReqFileUpload = '1';
                    item.upload.data.VDAjaxReqProcRefId    = jQuery("#VDAjaxReqProcRefId").val();

                    return true;
                },

                // Callback fired if the upload succeeds
                // we will add by default a success icon and fadeOut the progressbar
                onSuccess: function(data, item, listEl, parentEl, newInputEl, inputEl, textStatus, jqXHR) {
                    item.html.find('.fileuploader-action-remove').addClass('fileuploader-action-success');

                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },

                // Callback fired if the upload failed
                // we will set by default the progressbar to 0% and if it wasn't cancelled, we will add a retry button
                onError: function(item, listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus, errorThrown) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }

                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },

                onProgress: function(data, item, listEl, parentEl, newInputEl, inputEl) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },

                // Callback fired after all files were uploaded
                onComplete: function(listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus) {
                    // callback will go here

                    vdOnFileUploadLoadComplete(); // está definida no módulo do site se for invocado por ajax
                }
            }

            ,// by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

var vdAlertaHelperObj = function () {

    var vdAlertaData = '';

    var setAlertaDataByAjax = function (data) {
        vdAlertaData = data;
    };

    var getAlertaDataByAjax = function () {
        return vdAlertaData;
    };

    return {
        setAlertaDataByAjax  :  setAlertaDataByAjax,
        getAlertaDataByAjax  :  getAlertaDataByAjax
    };

}();

jQuery(document).ready(function(){

    var InputLatLong = jQuery('#coordenadas').val();

    if( InputLatLong!=undefined && InputLatLong!="") {
        var ArrayLatLong = InputLatLong.split(",");
        if(ArrayLatLong.length == 2) {
            LatToSet  =  ArrayLatLong[0];
            LongToSet =  ArrayLatLong[1];

        }
    }

    if( jQuery('#gmap_marker').length ) {
        MapsGoogle.init()
    }

    jQuery('.date-picker1').datepicker({autoclose:true});
    jQuery('.date-picker1').on('hide', function (e) { e.preventDefault(); });

    jQuery('#m_timepicker_2, #m_timepicker_3').timepicker({
        showMeridian: false
    });

    jQuery('.informationButton').hover(
        function() {
            jQuery('.information').fadeIn("slow");
        }, function() {
            jQuery('.information').fadeOut("slow");
        }
    );

   ComponentsSelect2.init();

   ComponentFileUploader.init();

   jQuery('#fiscalid').on('input',function(e) {
        // access properties using this keyword
        checkFiscalIdPromotor()
   });

   jQuery('#Next').css('display','block');

   jQuery( "#fechaErro" ).click(function() {
        jQuery('.backgroundErro').css('display','none');
    });

   checkFiscalIdPromotor();

   setTimeout(function(){
        ComponentsEditors.init();
    }, 1000);


});

