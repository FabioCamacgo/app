<?php
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroEventos');
if ($resPluginEnabled === false) exit();

$listaEventos = VirtualDeskSiteCulturaHelper::getEventsFiltered($nomePost, $categoriaPost, $freguesiaPost, $mesPost, $anoPost, $dataAtual);

if((int)$listaEventos == 0){
    ?><div class="noresults">
        <div class="icon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M13,17h-2v-2h2V17z M13,13h-2V7h2V13z"/><g><path d="M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4 M12,2C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10s10-4.5,10-10   C22,6.5,17.5,2,12,2L12,2z"/></g></svg></div>
    <?php echo JText::_('COM_VIRTUALDESK_CULTURA_SEM_RESULTADOS'); ?> </div> <?php
}else{
    foreach($listaEventos as $rowWSL) :
        ?>
        <div class="item">
            <?php
            $id = $rowWSL['id_evento'];
            $referencia = $rowWSL['referencia_evento'];
            $categoria = $rowWSL['categoria'];
            $nomeEvento = $rowWSL['nome_evento'];
            $descEvento = $rowWSL['desc_evento'];
            $freguesia = $rowWSL['freguesia'];
            $dataInicio = $rowWSL['data_inicio'];
            $ano = $rowWSL['Ano'];

            $imgEventos = VirtualDeskSiteCulturaHelper::getImgEventos($referencia);
            $ArrStartDate = explode("-",$dataInicio);
            $month = $ArrStartDate[1];
            $monthEventos = VirtualDeskSiteCulturaHelper::getNameMonth($month);
            $temImagem = 0;

            $name = str_replace(' ', '_', $nomeEvento);
            $code = $id . '/' . $name;
            ?>

            <div class="w50 image">
                <div class="date">
                    <div class="month">
                        <?php echo $monthEventos; ?>
                    </div>
                    <div class="day">
                        <?php echo $ArrStartDate[2]; ?>
                    </div>
                </div>

                <div class="ano"><?php echo $ano ?></div>

                <div class="imagem">
                    <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">
                        <?php
                        if((int)$imgEventos == 0){
                            $idCat = VirtualDeskSiteCulturaHelper::getIdCat($categoria);
                            $semimagem = VirtualDeskSiteCulturaHelper::getImagemGeral($idCat);
                            ?>
                            <img src="<?php echo $nomeSite . $imagem . $semimagem;?>" loading="lazy" alt="<?php echo $nomeEvento; ?>"/>
                            <?php
                        } else {
                            $alt=$nomeEvento;
                            $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
                            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                            $FileList21Html = '';
                            foreach ($arFileList as $rowFile) {
                                if($temImagem == 0){
                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                    ?>
                                    <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $alt; ?>">
                                    <?php
                                    $temImagem = 1;
                                }
                            }
                        }
                        ?>
                    </a>
                </div>
            </div>

            <div class="w50">
                <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">
                    <div class="titleEvent">
                        <?php echo $nomeEvento; ?>
                    </div>
                </a>

                <div class="catEvent">
                    <div class="borda"></div>
                    <div class="text">
                        <?php echo $categoria ?>
                    </div>
                </div>

                <div class="fregEvent">
                    <div class="icon">
                        <svg aria-hidden="true" class="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"/></svg>
                    </div>
                    <div class="text">
                        <?php echo $freguesia; ?>
                    </div>
                </div>



                <a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>">

                    <div class="sabermais">
                        <button id="saberMais">
                            <div class="borda"></div>
                            <input type="submit" name="sabermais" id="sabermais" value="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_SABERMAIS'); ?>">
                        </button>

                    </div>
                </a>
            </div>


        </div>
    <?php
    endforeach;
}
?>