<?php
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

/* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
$obParam              = new VirtualDeskSiteParamsHelper();
$arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroEventos');
if ($resPluginEnabled === false) exit();

$dataAtual = date("Y-m-d");

$obParam      = new VirtualDeskSiteParamsHelper();
$concelhoCultura = $obParam->getParamsByTag('concelhoCultura');


$link = $jinput->get('link','' ,'string');
$ref = explode("cat=", $link);
$nomeEvento = base64_decode($ref[1]);
$info = explode("/", $nomeEvento);
$id= $info[0];

//GLOBAL SCRIPTS
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js?v2' . $addscript_end;

//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';



echo $headCSS;


?>

<div id="ListaEventos">

    <div class="w70">
        <?php
        if (isset($_POST['submitForm'])) {
            $nomePost = $_POST['nome'];
            $categoriaPost = $_POST['categoria'];
            $freguesiaPost = $_POST['freguesia'];
            $mesPost = $_POST['mes'];
            $anoPost = $_POST['Ano'];

            if(empty($nomePost) && empty($categoriaPost) && empty($freguesiaPost) && empty($mesPost) && empty($anoPost)){
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_FiltroEventosALL.php');
            } else{
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_FiltroEventosFILTERED.php');
            }

        } else if($id !='') {
            $categoriaPost = $id;
            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_FiltroEventosFILTERED.php');
        } else {
            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Cultura_FiltroEventosALL.php');
        }
        ?>
    </div>


    <div class="w30">
        <form id="ProcuraEvento" action="" method="post" class="login-form" enctype="multipart/form-data" >

            <div id="goBackList">
                <svg baseProfile="tiny" height="24px" version="1.2" viewBox="0 0 24 24" width="24px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g><path d="M19.164,19.547c-1.641-2.5-3.669-3.285-6.164-3.484v1.437c0,0.534-0.208,1.036-0.586,1.414   c-0.756,0.756-2.077,0.751-2.823,0.005l-6.293-6.207C3.107,12.523,3,12.268,3,11.999s0.107-0.524,0.298-0.712l6.288-6.203   c0.754-0.755,2.073-0.756,2.829,0.001C12.792,5.463,13,5.965,13,6.499v1.704c4.619,0.933,8,4.997,8,9.796v1   c0,0.442-0.29,0.832-0.714,0.958c-0.095,0.027-0.19,0.042-0.286,0.042C19.669,19.999,19.354,19.834,19.164,19.547z M12.023,14.011   c2.207,0.056,4.638,0.394,6.758,2.121c-0.768-3.216-3.477-5.702-6.893-6.08C11.384,9.996,11,10,11,10V6.503l-5.576,5.496l5.576,5.5   V14C11,14,11.738,14.01,12.023,14.011z"/></g></svg>
                <?php echo JText::_('COM_VIRTUALDESK_CULTURA_OUT_FILTER');?>
            </div>


            <!--   Nome  -->
            <div class="form-group" id="nomeSearch">
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_NOMEEVENTO'); ?>"
                           name="nome" id="nome" value="<?php echo $nomePost; ?>"/>
                </div>
            </div>


            <!--   Categoria  -->
            <div class="form-group" id="catSearch">
                <?php $Categorias = VirtualDeskSiteCulturaHelper::getCategoria()?>
                <div class="col-md-9">
                    <select name="categoria" value="<?php echo $categoriaPost; ?>" id="categoria" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($categoriaPost)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_CATEGORIA'); ?></option>
                            <?php foreach($Categorias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        } else{ ?>
                            <option value="<?php echo $categoriaPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getCatSelect($categoriaPost) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeCategoria = VirtualDeskSiteCulturaHelper::excludeCat($categoriaPost)?>
                            <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>

            <!--   Freguesia  -->
            <div class="form-group" id="fregSearch">
                <?php $Freguesias = VirtualDeskSiteCulturaHelper::getFreguesia($concelhoCultura)?>
                <div class="col-md-9">
                    <select name="freguesia" value="<?php echo $freguesiaPost; ?>" id="freguesia" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($freguesiaPost)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_FREGUESIA'); ?></option>
                            <?php foreach($Freguesias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['freguesia']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $freguesiaPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getFregSelect($freguesiaPost) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeFreguesia = VirtualDeskSiteCulturaHelper::excludeFreguesia($concelhoCultura, $freguesiaPost)?>
                            <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['freguesia']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>


            <!--   Ano  -->
            <div class="form-group" id="anoField">
                <?php $Anos = VirtualDeskSiteCulturaHelper::getAnos()?>
                <div class="col-md-9">
                    <select name="Ano" value="<?php echo $anoPost; ?>" id="Ano" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($anoPost)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_ANO'); ?></option>
                            <?php foreach($Anos as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['Ano']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $anoPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getAnoSelect($anoPost) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeMes = VirtualDeskSiteCulturaHelper::excludeAno($anoPost)?>
                            <?php foreach($ExcludeMes as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['Ano']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>

            <!--   Mes  -->
            <div class="form-group" id="month">
                <?php $Meses = VirtualDeskSiteCulturaHelper::getMeses()?>
                <div class="col-md-9">
                    <select name="mes" value="<?php echo $mesPost; ?>" id="mes" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($mesPost)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_PESQUISA_MES'); ?></option>
                            <?php foreach($Meses as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['mes']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $mesPost; ?>"><?php echo VirtualDeskSiteCulturaHelper::getMesSelect($mesPost) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeMes = VirtualDeskSiteCulturaHelper::excludeMes($mesPost)?>
                            <?php foreach($ExcludeMes as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['mes']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>




            <div class="form-actions" method="post" style="display:none;">
                <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
            </div>

        </form>
    </div>

</div>

<?php
echo $headScripts;
echo $footerScripts;
?>

<script>
    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Cultura/Cultura_FiltroEventos.js.php');
    ?>
</script>



