

<div class="layout2">

    <div class="w30">
        <div class="imagem">
            <?php
            if((int)$imgEventos == 0){
                $idCat = VirtualDeskSiteCulturaHelper::getIdCat($categoria);
                $semimagemVertical = VirtualDeskSiteCulturaHelper::getImagemVertical($idCat);
                ?>
                <img src="<?php echo $nomeSite . $vertical . $semimagemVertical;?>" alt="<?php echo $nome; ?>"/>
                <?php
            } else {
                $alt=$nome;
                $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                $FileList21Html = '';
                foreach ($arFileList as $rowFile) {
                    if($temImagem == 0){
                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                        ?>
                        <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>">
                        <?php
                        $temImagem = 1;
                    }
                }
            }
            ?>
        </div>
    </div>


    <div class="w70">
        <div class="detalhes normal">
            <div>
                <button id="detalhes">
                    <div class="borda"></div>
                    <div class="name"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_BUTTON_DETALHES'); ?></div>
                </button>
            </div>


            <div class="w50">
                <div class="catEvent">
                    <div class="w10" title="Categoria">
                        <div class="borda"></div>
                    </div>

                    <div class="w90">
                        <div class="text">
                            <?php echo $categoria ?>
                        </div>
                    </div>
                </div>


                <div class="placeEvento">
                    <div class="w10" title="Local">
                        <svg aria-hidden="true" role="img" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"/></svg>
                    </div>

                    <div class="w90">
                        <div class="freguesia"><?php echo $fregEvento; ?></div>
                        <div class="local"><?php echo $localEvento; ?></div>
                    </div>
                </div>

                <div class="website" title="Online">
                    <div class="w10">
                        <svg x="0px" y="0px" width="464.772px" height="539px" viewBox="0 0 464.772 539" enable-background="new 0 0 464.772 539" xml:space="preserve">
                            <g>
                                <path fill="#0B1C2A" d="M442.153,220.59L37.39,15.318c-5.306-2.949-12.607-1.365-16.245,3.379
                                    c-1.782,2.229-2.475,4.924-1.917,7.493l81.171,455.192c1.142,5.272,7.217,8.488,13.573,7.109c2.972-0.644,5.715-2.372,7.463-4.729
                                    l99.099-133.936l124.831,163.49c6.941,8.538,21.351,10.368,32.1,3.793c0,0,0,0-0.03-0.138l37.475-28.49
                                    c10.74-6.58,10.797-22.684,3.962-31.394L294.374,293.997l142.778-51.642c6.25-1.211,11.081-10.064,10.069-15.367
                                    C446.8,224.393,444.865,221.98,442.153,220.59z"/>
                            </g>
                        </svg>
                    </div>

                    <div class="w90">
                        <?php
                            if(empty($website) && empty($facebook) && empty($instagram)){
                                echo '-';
                            } else{
                                if(!empty($website)){
                                    ?>
                                    <div><a href="<?php echo $website; ?>" title="Website - <?php echo $nome; ?>" target="_blank"><?php echo $website; ?></a></div>
                                    <?php
                                }

                                if(!empty($facebook)){
                                    ?>
                                    <div><a href="<?php echo $facebook; ?>" title="Facebook - <?php echo $nome; ?>" target="_blank"><?php echo 'Ver facebook'; ?></a></div>
                                    <?php
                                }

                                if(!empty($instagram)){
                                    ?>
                                    <div><a href="<?php echo $instagram; ?>" title="Instagram - <?php echo $nome; ?>" target="_blank"><?php echo 'Ver instagram'; ?></a></div>
                                    <?php
                                }

                            }

                        ?>
                    </div>
                </div>

            </div>

            <div class="w50">
                <div class="date">
                    <div class="w10" title="Data">
                        <svg x="0px" y="0px" width="533px" height="539px" viewBox="0 0 533 539" enable-background="new 0 0 533 539" xml:space="preserve">
                    <g>
                        <path fill="#0B1C2A" d="M491.475,58.259h-60.761V38.77c0-10.414-8.354-18.76-18.767-18.76H356.27
                            c-10.406,0-18.759,8.346-18.759,18.76v20.09l-137.744,1.215V38.77c0-10.414-8.476-18.76-18.882-18.76H125.2
                            c-10.284,0-18.759,8.346-18.759,18.76v20.09H45.679c-10.291,0-18.759,8.353-18.759,18.766v416.984
                            c0,10.291,8.469,18.76,18.759,18.76h445.795c10.407,0,18.76-8.469,18.76-18.76V77.018
                            C510.234,66.604,501.882,58.259,491.475,58.259z M410.739,423.195v-35.102h34.98L410.739,423.195z M472.716,351.175H391.98
                            c-10.414,0-18.883,8.476-18.883,18.767v79.521H64.438V212.223h408.277V351.175z"/>
                    </g>
                </svg>
                    </div>

                    <div class="w90">
                        <?php
                        $dateInicio = explode("-", $inicioEvento);
                        $dateFim = explode("-", $fimEvento);
                        ?>

                        <div class="diaMes">
                            <?php
                            if($dateInicio[2] == $dateFim[2] && $dateInicio[1] == $dateFim[1]){
                                echo $dateInicio[2] . ' ' . VirtualDeskSiteCulturaHelper::getNameMonth($dateInicio[1]);
                            } else{
                                echo $dateInicio[2] . ' ' . VirtualDeskSiteCulturaHelper::getNameMonth($dateInicio[1]) . ' - ' . $dateFim[2] . ' ' . VirtualDeskSiteCulturaHelper::getNameMonth($dateFim[1]);
                            }
                            ?>
                        </div>

                        <div class="ano">
                            <?php
                            if($dateInicio[0] == $dateFim[0]){
                                echo $dateInicio[0];
                            } else {
                                echo $dateInicio[0] . '-' . $dateFim[0];
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="hora">
                    <div class="w10" title="Hora">
                        <svg height="20px" version="1.1" viewBox="0 0 20 20" width="20px"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1"><g fill="#000000" fill-opacity="0.9" id="Icons-Device" transform="translate(-44.000000, 0.000000)"><g id="access-time" transform="translate(44.000000, 0.000000)"><path d="M10,0 C4.5,0 0,4.5 0,10 C0,15.5 4.5,20 10,20 C15.5,20 20,15.5 20,10 C20,4.5 15.5,0 10,0 L10,0 Z M10,18 C5.6,18 2,14.4 2,10 C2,5.6 5.6,2 10,2 C14.4,2 18,5.6 18,10 C18,14.4 14.4,18 10,18 Z M10.5,5 L9,5 L9,11 L14.2,14.2 L15,12.9 L10.5,10.2 L10.5,5 Z"/></g></g></g></svg>
                    </div>

                    <div class="w90">
                        <?php
                        if(empty($horaInicioEvento) || empty($horaFimEvento)){
                            echo '-';
                        } else {
                            echo $horaInicioEvento . '-' . $horaFimEvento;
                        }
                        ?>
                    </div>
                </div>

                <div class="promotor">
                    <div class="w10" title="Promotor">
                        <svg enable-background="new 0 0 32 32" height="32px" id="Layer_1" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M22.417,14.836c-1.209,2.763-3.846,5.074-6.403,5.074c-3.122,0-5.39-2.284-6.599-5.046   c-7.031,3.642-6.145,12.859-6.145,12.859c0,1.262,0.994,1.445,2.162,1.445h10.581h10.565c1.17,0,2.167-0.184,2.167-1.445   C28.746,27.723,29.447,18.479,22.417,14.836z" fill="#0B1C2A"/>
                                <path d="M16.013,18.412c3.521,0,6.32-5.04,6.32-9.204c0-4.165-2.854-7.541-6.375-7.541   c-3.521,0-6.376,3.376-6.376,7.541C9.582,13.373,12.491,18.412,16.013,18.412z" fill="#0B1C2A"/></g></svg>
                    </div>

                    <div class="w90">
                        <?php
                            if(empty($promotor)){
                                echo '-';
                            } else {
                                echo $promotor;
                            }
                            if($coOrganizacao == 1){
                                echo '<div class="local">Coorganização: ' . $copyrightAPP . '</div>';
                            }
                         ?>
                    </div>
                </div>

            </div>

        </div>

        <div class="info">
            <div class="controls">
                <button id="descricao">
                    <div class="borda"></div>
                    <div class="name"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_BUTTON_DESCRICAO'); ?></div>
                </button>

                <?php
                if(!empty($latitude) || !empty($longitude)){
                    ?>
                    <button id="mapa">
                        <div class="borda"></div>
                        <div class="name"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_BUTTON_MAPA'); ?></div>
                    </button>
                    <?php
                }
                ?>
            </div>

            <div id="contentDescricao">
                <?php echo $descricaoEvento; ?>
            </div>

            <?php
            if(!empty($latitude) || !empty($longitude)){
                ?>
                <div id="contentMapa">
                    <script type="text/javascript">

                        jArrayPendente = <?php echo json_encode($coordenadas ); ?>;


                    </script>
                    <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden; width:100%; height:300px;"></div>
                </div>
                <?php
            }
            ?>
        </div>

        <?php
        if(!empty($observacoes)){
            ?>
            <div class="info second">
                <button class="obs"">
                <div class="borda"></div>
                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_BUTTON_OBSERVACOES'); ?></div>
                </button>

                <div class="contentOBS">
                    <?php echo $observacoes; ?>
                </div>

            </div>
            <?php
        }
        ?>

        <?php
            if(count($imgEventos)>1){
                ?>
                <div class="info second">
                    <button class="obs"">
                    <div class="borda"></div>
                    <div class="name"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_BUTTON_GALERIA'); ?></div>
                    </button>

                    <div class="contentGAL">
                        <?php
                            $alt=$nome;
                            $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
                            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                            $FileList21Html = '';
                            foreach ($arFileList as $rowFile) {
                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                ?>
                                <div class="item">
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>">
                                </div>
                                <?php
                            }
                        ?>
                    </div>

                </div>
                <?php
            }
        ?>

        <div class="detalhes mobile">
            <div>
                <button id="detalhes">
                    <div class="borda"></div>
                    <div class="name"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_BUTTON_DETALHES'); ?></div>
                </button>
            </div>


            <div class="w50">
                <div class="catEvent">
                    <div class="w10" title="Categoria">
                        <div class="borda"></div>
                    </div>

                    <div class="w90">
                        <div class="text">
                            <?php echo $categoria ?>
                        </div>
                    </div>
                </div>


                <div class="placeEvento">
                    <div class="w10" title="Local">
                        <svg aria-hidden="true" role="img" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"/></svg>
                    </div>

                    <div class="w90">
                        <div class="freguesia"><?php echo $fregEvento; ?></div>
                        <div class="local"><?php echo $localEvento; ?></div>
                    </div>
                </div>

                <div class="date">
                    <div class="w10" title="Data">
                        <svg x="0px" y="0px" width="533px" height="539px" viewBox="0 0 533 539" enable-background="new 0 0 533 539" xml:space="preserve">
                            <g>
                                <path fill="#0B1C2A" d="M491.475,58.259h-60.761V38.77c0-10.414-8.354-18.76-18.767-18.76H356.27
                                    c-10.406,0-18.759,8.346-18.759,18.76v20.09l-137.744,1.215V38.77c0-10.414-8.476-18.76-18.882-18.76H125.2
                                    c-10.284,0-18.759,8.346-18.759,18.76v20.09H45.679c-10.291,0-18.759,8.353-18.759,18.766v416.984
                                    c0,10.291,8.469,18.76,18.759,18.76h445.795c10.407,0,18.76-8.469,18.76-18.76V77.018
                                    C510.234,66.604,501.882,58.259,491.475,58.259z M410.739,423.195v-35.102h34.98L410.739,423.195z M472.716,351.175H391.98
                                    c-10.414,0-18.883,8.476-18.883,18.767v79.521H64.438V212.223h408.277V351.175z"/>
                            </g>
                        </svg>
                    </div>

                    <div class="w90">
                        <?php
                        $dateInicio = explode("-", $inicioEvento);
                        $dateFim = explode("-", $fimEvento);
                        ?>

                        <div class="diaMes">
                            <?php
                            if($dateInicio[2] == $dateFim[2] && $dateInicio[1] == $dateFim[1]){
                                echo $dateInicio[2] . '.' . $dateInicio[1];
                            } else{
                                echo $dateInicio[2] . '.' . $dateInicio[1] . ' - ' . $dateFim[2] . '.' . $dateFim[1];
                            }
                            ?>
                        </div>

                        <div class="ano">
                            <?php
                            if($dateInicio[0] == $dateFim[0]){
                                echo $dateInicio[0];
                            } else {
                                echo $dateInicio[0] . '-' . $dateFim[0];
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="hora">
                    <div class="w10" title="Hora">
                        <svg height="20px" version="1.1" viewBox="0 0 20 20" width="20px"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1"><g fill="#000000" fill-opacity="0.9" id="Icons-Device" transform="translate(-44.000000, 0.000000)"><g id="access-time" transform="translate(44.000000, 0.000000)"><path d="M10,0 C4.5,0 0,4.5 0,10 C0,15.5 4.5,20 10,20 C15.5,20 20,15.5 20,10 C20,4.5 15.5,0 10,0 L10,0 Z M10,18 C5.6,18 2,14.4 2,10 C2,5.6 5.6,2 10,2 C14.4,2 18,5.6 18,10 C18,14.4 14.4,18 10,18 Z M10.5,5 L9,5 L9,11 L14.2,14.2 L15,12.9 L10.5,10.2 L10.5,5 Z"/></g></g></g></svg>
                    </div>

                    <div class="w90">
                        <?php
                        if(empty($horaInicioEvento) || empty($horaFimEvento)){
                            echo '-';
                        } else {
                            echo $horaInicioEvento . '-' . $horaFimEvento;
                        }
                        ?>
                    </div>
                </div>

                <div class="promotor">
                    <div class="w10" title="Promotor">
                        <svg enable-background="new 0 0 32 32" height="32px" id="Layer_1" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M22.417,14.836c-1.209,2.763-3.846,5.074-6.403,5.074c-3.122,0-5.39-2.284-6.599-5.046   c-7.031,3.642-6.145,12.859-6.145,12.859c0,1.262,0.994,1.445,2.162,1.445h10.581h10.565c1.17,0,2.167-0.184,2.167-1.445   C28.746,27.723,29.447,18.479,22.417,14.836z" fill="#0B1C2A"/>
                                <path d="M16.013,18.412c3.521,0,6.32-5.04,6.32-9.204c0-4.165-2.854-7.541-6.375-7.541   c-3.521,0-6.376,3.376-6.376,7.541C9.582,13.373,12.491,18.412,16.013,18.412z" fill="#0B1C2A"/></g></svg>
                    </div>

                    <div class="w90">
                        <?php
                            if(empty($promotor)){
                                echo '-';
                            } else {
                                echo $promotor;
                            }
                            if($coOrganizacao == 1){
                                echo '<div class="local">Coorganização: ' . $copyrightAPP . '</div>';
                            }
                        ?>
                    </div>
                </div>

                <div class="website" title="Website">
                    <div class="w10">
                        <svg x="0px" y="0px" width="464.772px" height="539px" viewBox="0 0 464.772 539" enable-background="new 0 0 464.772 539" xml:space="preserve">
                            <g>
                                <path fill="#0B1C2A" d="M442.153,220.59L37.39,15.318c-5.306-2.949-12.607-1.365-16.245,3.379
                                    c-1.782,2.229-2.475,4.924-1.917,7.493l81.171,455.192c1.142,5.272,7.217,8.488,13.573,7.109c2.972-0.644,5.715-2.372,7.463-4.729
                                    l99.099-133.936l124.831,163.49c6.941,8.538,21.351,10.368,32.1,3.793c0,0,0,0-0.03-0.138l37.475-28.49
                                    c10.74-6.58,10.797-22.684,3.962-31.394L294.374,293.997l142.778-51.642c6.25-1.211,11.081-10.064,10.069-15.367
                                    C446.8,224.393,444.865,221.98,442.153,220.59z"/>
                            </g>
                        </svg>
                    </div>

                    <div class="w90">
                        <?php
                            if(empty($website) && empty($facebook) && empty($instagram)){
                                echo '-';
                            } else{
                                if(!empty($website)){
                                    ?>
                                    <a href="<?php echo $website; ?>" title="Website - <?php echo $nome; ?>" target="_blank"><?php echo $website; ?></a>
                                    <?php
                                }

                                if(!empty($facebook)){
                                    ?>
                                    <a href="<?php echo $facebook; ?>" title="Facebook - <?php echo $nome; ?>" target="_blank"><?php echo 'Ver facebook'; ?></a>
                                    <?php
                                }

                                if(!empty($instagram)){
                                    ?>
                                    <a href="<?php echo $instagram; ?>" title="Instagram - <?php echo $nome; ?>" target="_blank"><?php echo 'Ver instagram'; ?></a>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </div>

            </div>

        </div>

    </div>



</div>