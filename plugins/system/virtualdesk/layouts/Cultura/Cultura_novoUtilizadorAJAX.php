<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('registarutilizador');
    if ($resPluginEnabled === false) exit();



    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Cultura/novoUtilizador.js' . $addscript_end;


    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
    $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
    $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
    $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');


    if (isset($_POST['submitForm'])) {
        $nome = $_POST['nome'];
        $fiscalid = $_POST['fiscalid'];
        $telefone = $_POST['telefone'];
        $email = $_POST['email'];
        $confEmail = $_POST['confEmail'];
        $morada = $_POST['morada'];
        $concelho = $_POST['concelho'];
        $freguesia = $_POST['vdmenumain'];
        $codPostal4 = $_POST['codPostal4'];
        $codPostal3 = $_POST['codPostal3'];
        $nomeConcelho = VirtualDeskSiteCulturaHelper::getConcName($concelho);
        $codPostal = $codPostal4 . '-' . $codPostal3 . ' ' . $nomeConcelho;
        $websitePromotor = $_POST['websitePromotor'];
        $facebookPromotor = $_POST['facebookPromotor'];
        $instagramPromotor = $_POST['instagramPromotor'];
        $politica2 = $_POST['politica2'];
        $veracid2 = $_POST['veracid2'];



        /*Valida Nome*/
        if($nome == Null){
            $errNome = 1;
        } else if(is_numeric($nome)){
            $errNome = 2;
        } /*else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$nome)) {
            $errNome = 3;
        }*/

        /*Valida Nif*/
        $nif=trim($fiscalid);
        $ignoreFirst=true;
        $nifExiste = VirtualDeskSiteCulturaHelper::getNIF($nif);
        //Verificamos se é numérico e tem comprimento 9
        if (!is_numeric($nif) || strlen($nif)!=9) {
            $errNif = 1;
        } else if((int)$nifExiste > 0){
            $errNif = 2;
        } else {
            $nifSplit=str_split($nif);
            //O primeiro digíto tem de ser 1, 2, 5, 6, 8 ou 9
            //Ou não, se optarmos por ignorar esta "regra"
            if (
                in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9))
                ||
                $ignoreFirst
            ) {
                //Calculamos o dígito de controlo
                $checkDigit=0;
                for($i=0; $i<8; $i++) {
                    $checkDigit+=$nifSplit[$i]*(10-$i-1);
                }
                $checkDigit=11-($checkDigit % 11);
                //Se der 10 então o dígito de controlo tem de ser 0
                if($checkDigit>=10) $checkDigit=0;
                //Comparamos com o último dígito
                if ($checkDigit==$nifSplit[8]) {

                } else {
                    $errNif = 1;
                }
            } else {
                $errNif = 1;
            }
        }

        /*Valida Telefone*/
        $TelefoneSplit=str_split($telefone);
        if($telefone == Null){
            $errTelefone = 1;
        } else if(!is_numeric($telefone) || strlen($telefone) != 9){
            $errTelefone = 2;
        } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
            $errTelefone = 2;
        } else if($TelefoneSplit[0] == 9){
            if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
                $errTelefone = 2;
            }
        }

        /*Valida Email*/
        $conta = "/^[a-zA-Z0-9\._-]+@";
        $domino = "[a-zA-Z0-9\._-]+.[.]";
        $extensao = "([a-zA-Z]{2,4})$/";
        $pattern = $conta . $domino . $extensao;
        $ExisteMail = VirtualDeskSiteCulturaHelper::seeEmailExiste($email);
        if($email == Null){
            $errEmail = 1;
        } else if(!preg_match($pattern, $email)) {
            $errEmail = 2;
        } else if((int)$ExisteMail > 0){
            $errEmail = 3;
        } else if($email != $confEmail){
            $errEmail = 4;
        }


        /*Valida Morada*/
        if($morada == Null){
            $errMorada = 1;
        }

        /*Valida Concelho*/
        if($concelho == 'Escolher Opção'){
            $errConcelho = 1;
        }


        /*Valida Freguesia*/
        if($freguesia == Null){
            $errFreguesia = 1;
        } else if($freguesia == 'Escolher Opção'){
            $errFreguesia = 1;
        }


        /*Valida Codigo Postal*/
        if($codPostal4 == Null || $codPostal3 == Null){
            $errCodigoPostal = 1;
        } else if(!is_numeric($codPostal4) || !is_numeric($codPostal3)){
            $errCodigoPostal = 2;
        }


        // Valida recaptcha
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');
        $jinput->set('g-recaptcha-response',$recaptcha_response_field);
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        $errRecaptcha = 0;
        if (!$resRecaptchaAnswer) {
            $errRecaptcha = 1;
        }

        if($veracid2 == 1 && $politica2 == 1 && $errNome == 0 && $errNif == 0 && $errTelefone == 0 && $errEmail == 0 && $errMorada == 0 && $errConcelho == 0 && $errFreguesia == 0 && $errCodigoPostal == 0 && $errRecaptcha == 0){


            $resSaveRegisto = VirtualDeskSiteCulturaHelper::SaveNovoRegisto($nome, $fiscalid, $telefone, $email, $morada, $concelho, $freguesia, $codPostal, $websitePromotor, $facebookPromotor, $instagramPromotor);

            if($resSaveRegisto == true) { ?>

                <div class="message"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOREGISTO_SUCESSO'); ?></div>

                <script>
                    jQuery('#NovoUser').css('display','none');
                </script>

                <?php

                //VirtualDeskSiteCulturaHelper::SendMailAdmin($contactoTelefCopyrightEmail, $dominioMunicipio, $emailCopyrightGeral, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $nome, $fiscalid, $email2);

                //$freguesiaName = VirtualDeskSiteCulturaHelper::getFregName($freguesia);

                //VirtualDeskSiteCulturaHelper::SendMailClient($contactoTelefCopyrightEmail, $dominioMunicipio, $emailCopyrightGeral, $LinkCopyright, $nome, $fiscalid, $telefone, $email, $password, $morada, $freguesiaName, $codPostal, $copyrightAPP, $nomeMunicipio);

                exit();

            } else { ?>

                <div class="message"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOREGISTO_ERRO'); ?></div>

            <?php }

        } else { ?>

            <script>
                dropError();
            </script>
            <div class="backgroundErro">
                <div class="erro" style="display:block;">
                    <button id="fechaErro"><svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                        <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                            M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                            c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                            c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                            c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                            c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                            c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                            c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                            C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                    </svg></button>

                    <h2>
                        <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18"
                             role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                        <?php echo JText::_('COM_VIRTUALDESK_CULTURA_AVISO'); ?>
                    </h2>

                    <?php

                    if($errNome == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NOME_1') . '</p>';
                    } else if($errNome == 2){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NOME_2') . '</p>';
                    } else if($errNome == 3){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NOME_3') . '</p>';
                    } else{
                        $errNome = 0;
                    }


                    if($errNif == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NIF_1') . '</p>';
                    } else if($errNif == 2){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_NIF_2') . '</p>';
                    } else {
                        $errNif = 0;
                    }

                    if($errTelefone == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_TELEFONE_1') . '</p>';
                    } else if($errTelefone == 2){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_TELEFONE_2') . '</p>';
                    } else{
                        $errTelefone = 0;
                    }


                    if($errEmail == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_1') . '</p>';
                    } else if($errEmail == 2){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_2') . '</p>';
                    } else if($errEmail == 3){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_3') . '</p>';
                    } else if($errEmail == 4){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_EMAIL_4') . '</p>';
                    } else{
                        $errEmail = 0;
                    }


                    if($errConcelho == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_CONCELHO_1') . '</p>';
                    } else{
                        $errConcelho = 0;
                    }


                    if($errFreguesia == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_FREGUESIA_1') . '</p>';
                    } else{
                        $errFreguesia = 0;
                    }


                    if($errMorada == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_MORADA_1') . '</p>';
                    } else if($errMorada == 2){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_MORADA_2') . '</p>';
                    } else{
                        $errMorada = 0;
                    }


                    if($errCodigoPostal == 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_CODIGOPOSTAL_1') . '</p>';
                    } else if($errCodigoPostal == 2){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_CODIGOPOSTAL_2') . '</p>';
                    } else{
                        $errCodigoPostal = 0;
                    }

                    if($veracid2 != 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_VERACIDADE_1') . '</p>';
                    }

                    if($politica2 != 1){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_POLITICA_1') . '</p>';
                    }


                    if($errRecaptcha == 1) {
                        echo '<p>' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_CAPTCHA') . '</p>';
                    }else{
                        $errRecaptcha = 0;
                    }

                    ?>

                </div>
            </div>

        <?php }


    }
?>

<form id="novoRegisto" action="" method="post" class="novoRegisto-form" enctype="multipart/form-data" >

    <!--   NOME   -->
    <div class="form-group" id="nomeGroup">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOME_LABEL'); ?> <span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOME_LABEL'); ?>"
                   name="nome" id="nome" maxlength="100" value="<?php echo $nome; ?>"/>
        </div>
    </div>


    <!--   NIF   -->
    <div class="form-group" id="nif">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?> <span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?>"
                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
        </div>
    </div>


    <!--   TELEFONE   -->
    <div class="form-group" id="phone">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_TELEFONE_LABEL'); ?> <span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_TELEFONE_LABEL'); ?>"
                   name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
        </div>
    </div>


    <!--   EMAIL  -->
    <div class="form-group" id="mail">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_EMAIL'); ?> <span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_EMAIL'); ?>"
                   name="email" id="email" value="<?php echo htmlentities($email, ENT_QUOTES, 'UTF-8'); ?>" />
        </div>
    </div>


    <!--   CONFIRMAÇÃO DE EMAIL  -->
    <div class="form-group" id="confmail">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CONFEMAIL'); ?> <span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_CONFEMAIL'); ?>"
                   name="confEmail" id="confEmail" value="<?php echo htmlentities($confEmail, ENT_QUOTES, 'UTF-8'); ?>" />
        </div>
    </div>



    <!--   Concelho   -->
    <div class="form-group" id="cat">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CONCELHO'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <?php $concelhos = VirtualDeskSiteCulturaHelper::getConcelhos()?>
            <div class="input-group ">
                <select name="concelho" required value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <option>Escolher Op&ccedil;&atilde;o</option>
                    <?php foreach($concelhos as $rowStatus) : ?>
                        <option value="<?php echo $rowStatus['id']; ?>"
                            <?php
                            if(!empty($this->data->concelho)) {
                                if( (int)($rowStatus['id'] == (int) $this->data->idwebsitelist) ) echo 'selected';
                            }
                            ?>
                        ><?php echo $rowStatus['concelho']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>


    <!--   Freguesia   -->
    <?php
    // Carrega menusec se o id de menumain estiver definido.
    $ListaDeMenuMain = array();
    if(!empty($this->data->concelho)) {
        if( (int) $this->data->concelho > 0) $ListaDeMenuMain = VirtualDeskSiteCulturaHelper::getCulturaFreguesia($this->data->concelho);
    }

    ?>
    <div id="blocoMenuMain" class="form-group">
        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_FREGUESIAS_LABEL' ); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <select name="vdmenumain" id="vdmenumain" required
                <?php
                if(!empty($this->data->concelho)) {
                    if ((int)$this->data->concelho <= 0 || empty($ListaDeMenuMain)) echo 'disabled';
                }
                else {
                    echo 'disabled';
                }
                ?>
                    class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                <option>Escolher Op&ccedil;&atilde;o</option>
                <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                    <option value="<?php echo $rowMM['id']; ?>"
                        <?php
                        if(!empty($this->data->vdmenumain)) {
                            if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                        }
                        ?>
                    ><?php echo $rowMM['name']; ?></option>
                <?php endforeach; ?>
            </select>

        </div>
    </div>


    <!--   MORADA  -->
    <div class="form-group" id="address">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_MORADA_LABEL'); ?> <span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_MORADA_LABEL'); ?>"
                   name="morada" id="morada" maxlength="100" value="<?php echo $morada; ?>"/>
        </div>
    </div>


    <!--   CÓDIGO POSTAL  -->
    <div class="form-group" id="codPostal">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CODIGOPOSTAL_LABEL'); ?> <span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'XXXX'; ?>"
                   name="codPostal4" id="codPostal4" maxlength="4" value="<?php echo $codPostal4; ?>"/>
            <?php echo '-'; ?>
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'YYY'; ?>"
                   name="codPostal3" id="codPostal3" maxlength="3" value="<?php echo $codPostal3; ?>"/>
        </div>
    </div>


    <!--  website  -->
    <div class="form-group" id="promotorWebsite">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_WEBSITEPROMOTOR'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_WEBSITEPROMOTOR'); ?>"
                   name="websitePromotor" id="websitePromotor" maxlength="200" value="<?php echo $websitePromotor; ?>"/>
        </div>
    </div>


    <!--  Facebook  -->
    <div class="form-group" id="promotorFacebook">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FACEBOOKPROMOTOR'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_FACEBOOKPROMOTOR'); ?>"
                   name="facebookPromotor" id="facebookPromotor" maxlength="200" value="<?php echo $facebookPromotor; ?>"/>
        </div>
    </div>


    <!--  Instagram  -->
    <div class="form-group" id="promotorInstagram">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INSTAGRAMPROMOTOR'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_INSTAGRAMPROMOTOR'); ?>"
                   name="instagramPromotor" id="instagramPromotor" maxlength="200" value="<?php echo $instagramPromotor; ?>"/>
        </div>
    </div>

    <!--   Veracidade -->
    <div class="form-group" id="Veracidade">

        <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?>/>
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_VERACIDADE'); ?><span class="required">*</span></label>

        <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
    </div>

    <script>

        document.getElementById('veracid').onclick = function() {
            // access properties using this keyword
            if ( this.checked ) {
                document.getElementById("veracid2").value = 1;
            } else {
                document.getElementById("veracid2").value = 0;
            }
        };
    </script>

    <!--   Politica de privacidade -->
    <div class="form-group" id="PoliticaPrivacidade">

        <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
        <label class="col-md-3 control-label">Declaro que li e aceito a <a href="#" target="_blank">Pol&iacute;tica de Privacidade</a>. <span class="required">*</span></label>

        <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
    </div>

    <script>

        document.getElementById('politica').onclick = function() {
            // access properties using this keyword
            if ( this.checked ) {
                document.getElementById("politica2").value = 1;
            } else {
                document.getElementById("politica2").value = 0;
            }
        };
    </script>


    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group" >
            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
            $field_id = 'dynamic_recaptcha_1';
            print $captcha->display($field_id, $field_id, 'g-recaptcha');
            ?>
            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
        </div>
    <?php endif; ?>


    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_RECUPERARPASSWORD'); ?>">
    </div>


</form>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;

?>

<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'submeterevento/'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
</script>