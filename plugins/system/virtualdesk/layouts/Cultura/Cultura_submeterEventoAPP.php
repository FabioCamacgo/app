<?php

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('submeterevento');
if ($resPluginEnabled === false) exit();

//LOCAL SCRIPTS
$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js' . $addscript_end;

//GLOBAL SCRIPTS
$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Cultura/cultura.js' . $addscript_end;

// PAGE LEVEL PLUGIN SCRIPTS

$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

//FileInput***
$headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/plugins/sortable.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/fileinput.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/locales/pt.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/fa/theme.js' . $addscript_end;
$headScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;


//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css' . $addcss_end;

//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

//FileInput***
$headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/css/fileinput.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.css' . $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
}

echo $headCSS;

$obParam      = new VirtualDeskSiteParamsHelper();
$nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
?>



<h2 class="titulo">
    <div class="borda"></div>
    <?php echo JText::_('COM_VIRTUALDESK_CULTURA_TITULO_NOVOEVENTO'); ?>
</h2>

<?php

if (isset($_POST['submitForm'])) {
    $fiscalid = $_POST['fiscalid'];
    $categoria = $_POST['categoria'];
    $nomeEvento = $_POST['nomeEvento'];
    $descricao = $_POST['descricao'];
    $freguesia = $_POST['freguesia'];
    $localEvento = $_POST['localEvento'];
    $dataInicio = $_POST['dataInicio'];
    $dataFim = $_POST['dataFim'];
    $horaInicio = $_POST['horaInicio'];
    $horaFim = $_POST['horaFim'];

    $currentDate= date("d-m-Y");
    $currentYear = date("Y");
    $currentMonth = date("m");
    $currentDay = date("d");

    /*Valida Nif*/
    $nif=trim($fiscalid);
    $ignoreFirst=true;
    $nifExiste = VirtualDeskSiteCulturaHelper::getNIF($nif);
    $nifAutorizado = VirtualDeskSiteCulturaHelper::getEstadoNIF($nif);
    if (!is_numeric($nif) || strlen($nif)!=9) {
        $errNif = 1;
    } else if((int)$nifExiste == 0){
        $errNif = 2;
    } else if($nifAutorizado != 2){
        $errNif = 3;
    } else {
        $nifSplit=str_split($nif);
        if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
            $checkDigit=0;
            for($i=0; $i<8; $i++) {
                $checkDigit+=$nifSplit[$i]*(10-$i-1);
            }
            $checkDigit=11-($checkDigit % 11);

            if($checkDigit>=10) $checkDigit=0;

            if ($checkDigit==$nifSplit[8]) {

            } else {
                $errNif = 1;
            }
        } else {
            $errNif = 1;
        }
    }

    /*Valida Categoria*/
    if($categoria == Null){
        $errCategoria = 1;
    }

    /*Valida Nome Evento*/
    if($nomeEvento == Null){
        $errNome = 1;
    }/* else if(is_numeric($nomeEvento)){
            $errNome = 2;
        } else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$nomeEvento)) {
            $errNome = 3;
        }*/

    /*Valida Descricao*/
    if($descricao == Null){
        $errDescricao = 1;
    }

    /*Valida Freguesia*/
    if($freguesia == Null){
        $errFreguesia = 1;
    }

    /*Valida Local Evento*/
    if($localEvento == Null){
        $errLocal = 1;
    } else if(is_numeric($localEvento)){
        $errLocal = 2;
    } else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$localEvento)) {
        $errLocal = 3;
    }




    // Valida recaptcha
    $captcha                  = JCaptcha::getInstance($captcha_plugin);
    $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');
    $jinput->set('g-recaptcha-response',$recaptcha_response_field);
    $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
    $errRecaptcha = 0;
    if (!$resRecaptchaAnswer) {
        $errRecaptcha = 1;
    }



    if($errNif == 0 && $errCategoria == 0 && $errNome == 0 && $errDescricao == 0 && $errFreguesia == 0 && $errLocal == 0 && $errFreguesia == 0 && $errRecaptcha == 0){

        $referencia = VirtualDeskSiteCulturaHelper::RefEvent($fiscalid, $startDay, $startMonth, $endDay, $endMonth);

        $idMonth = VirtualDeskSiteCulturaHelper::getIdMonth($startMonth);

        //$saveEvento = VirtualDeskSiteCulturaHelper::SaveNovoEvento($fiscalid, $categoria, $referencia, $nomeEvento, $descricao, $freguesia, $localEvento, $dataInicio, $dataFim, $idMonth);


        if($saveEvento == true) { ?>

            <div class="message"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_SUCESSO'); ?></div>

            <script>
                jQuery('#submitEvento').css('display','none');
                jQuery('#registo').css('display','none');
            </script>

            <?php

            echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');



            $freguesiaName = VirtualDeskSiteCulturaHelper::getFregName($freguesia);

            //$categoriaName = VirtualDeskSiteCulturaHelper::getCatName($categoria);

            $emailUser = VirtualDeskSiteCulturaHelper::getUserEmail($fiscalid);

            $nameUser = VirtualDeskSiteCulturaHelper::getUserName($fiscalid);

            //VirtualDeskSiteCulturaHelper::SendMailAdminEvento($nomeMunicipio, $nomeEvento, $nameUser, $email2);

            //VirtualDeskSiteCulturaHelper::SendMailClientEvento($nameUser, $nomeEvento, $descricao, $emailUser, $freguesiaName, $localEvento, $eventStart, $eventEnd);

            exit();

        } else { ?>

            <div class="message"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO'); ?></div>

        <?php }


    } else {
        ?>
        <script>
            dropError();
        </script>

        <div class="backgroundErro">
            <div class="erro" style="display:block;">
                <button id="fechaErro">
                    <svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                                <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                            c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                            c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                            c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                            c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                            c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                            c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                            C42.996,43.384,41.986,44.001,40.361,44.02z">
                                </path>
                            </svg>
                </button>

                <h2>
                    <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18"
                         role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                    <?php echo JText::_('COM_VIRTUALDESK_CULTURA_AVISO'); ?>
                </h2>

                <?php

                if($errNif == 2){
                    echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NIF_2') . '</p>';
                } else if($errNif == 3){
                    echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NIF_3') . '</p>';
                } else {
                    if($errNif == 1){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NIF_1') . '</p>';
                    } else{
                        $errNif = 0;
                    }


                    if($errCategoria == 1){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_CATEGORIA_1') . '</p>';
                    } else{
                        $errCategoria = 0;
                    }

                    if($errNome == 1){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NOME_1') . '</p>';
                    } else if($errNome == 2){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NOME_2') . '</p>';
                    } else if($errNome == 3){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_NOME_3') . '</p>';
                    } else{
                        $errNome = 0;
                    }

                    if($errDescricao == 1){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_DESCRICAO_1') . '</p>';
                    } else{
                        $errDescricao = 0;
                    }

                    if($errFreguesia == 1){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_FREGUESIA_1') . '</p>';
                    } else{
                        $errFreguesia = 0;
                    }

                    if($errLocal == 1){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_LOCAL_1') . '</p>';
                    } else if($errLocal == 2){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_LOCAL_2') . '</p>';
                    } else if($errLocal == 3){
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_NOVOEVENTO_ERRO_LOCAL_3') . '</p>';
                    } else{
                        $errLocal = 0;
                    }


                    if($errRecaptcha == 1) {
                        echo '<p><svg aria-hidden="true" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zm-248 50c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg> ' . JText::_('COM_VIRTUALDESK_CULTURA_ERRO_CAPTCHA') . '</p>';
                    }else{
                        $errRecaptcha = 0;
                    }
                }

                ?>

            </div>
        </div>
        <?php
    }

}
?>

<form id="CriaEvento" action="" method="post" class="login-form" enctype="multipart/form-data" >

    <!--   NIF Associação  -->
    <div class="form-group" id="nif">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_FISCALID_LABEL'); ?>"
                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
        </div>
    </div>



    <!--   Categoria  -->
    <div class="form-group" id="cat">
        <label class="col-md-3 control-label" for="field_categoria"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CATEGORIA_LABEL') ?></label>
        <?php $Categorias = VirtualDeskSiteCulturaHelper::getCategoria()?>
        <div class="col-md-9">
            <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($categoria)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CATEGORIA_LABEL'); ?></option>
                    <?php foreach($Categorias as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['categoria']; ?></option>
                    <?php endforeach;
                } else{ ?>
                    <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteCulturaHelper::getCatSelect($categoria) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeCategoria = VirtualDeskSiteCulturaHelper::excludeCat($categoria)?>
                    <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['categoria']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>


    <!--   Nome do evento  -->
    <div class="form-group" id="eventName">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOMEEVENTO_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOMEEVENTO_LABEL'); ?>"
                   name="nomeEvento" id="nomeEvento" maxlength="100" value="<?php echo $nomeEvento; ?>"/>
        </div>
    </div>


    <!--  Descrição Evento  -->
    <div class="form-group" id="desc">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DESCRICAO_LABEL'); ?></label>
        <div class="col-md-9">
                    <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_DESCRICAO_LABEL'); ?>"
                              name="descricao" id="descricao" maxlength="250"><?php echo $descricao; ?></textarea>
        </div>
    </div>


    <!--   Freguesia  -->
    <div class="form-group" id="freg">
        <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FREGUESIAS_LABEL') ?></label>
        <?php $Freguesias = VirtualDeskSiteCulturaHelper::getFreguesia('4')?>
        <div class="col-md-9">
            <select name="freguesia" value="<?php echo $freguesia; ?>" id="freguesia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($freguesia)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FREGUESIAS_LABEL'); ?></option>
                    <?php foreach($Freguesias as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                        ><?php echo $rowWSL['freguesia']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteCulturaHelper::getFregSelect($freguesia) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeFreguesia = VirtualDeskSiteCulturaHelper::excludeFreguesia('4', $freguesia)?>
                    <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                        ><?php echo $rowWSL['freguesia']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>


    <!--  Local do evento  -->
    <div class="form-group" id="eventPlace">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LOCALEVENTO_LABEL'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_LOCALEVENTO_LABEL'); ?>"
                   name="localEvento" id="localEvento" maxlength="100" value="<?php echo $localEvento; ?>"/>
        </div>
    </div>



    <!--   Data de inicio  -->
    <div class="form-group" id="eventStart">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAINICIO_LABEL'); ?></label>
        <div class="col-md-9">
            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                <input type="text" readonly name="dataInicio" id="dataInicio" value="<?php echo $dataInicio; ?>">
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>


    <!--   Hora de inicio  -->
    <div class="form-group" id="eventStartHour">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAINICIO_LABEL'); ?></label>
        <div class="col-md-9">
            <div class="input-group timepicker">
                <input class="form-control m-input" name="horaInicio" id="m_timepicker_2" type="text" />
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="la la-clock-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        #m_timepicker_2{display:block !important}
    </style>



    <!--   Data de fim  -->
    <div class="form-group" id="eventEnd">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAFIM_LABEL'); ?></label>
        <div class="col-md-9">
            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                <input type="text" readonly name="dataFim" id="dataFim" value="<?php echo $dataFim; ?>">
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>




    <!--   Hora de fim  -->
    <div class="form-group" id="eventEndHour">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAFIM_LABEL'); ?></label>
        <div class="col-md-9">
            <div class="input-group timepicker">
                <input class="form-control m-input" name="horaFim" id="m_timepicker_3" value="<?php echo $horaFim; ?>" type="text" />
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="la la-clock-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        #m_timepicker_3{display:block !important}
    </style>


    <!--   Upload Imagens   -->
    <div class="form-group" id="uploadField">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_UPLOAD'); ?></label>
        <div class="col-md-9">

            <div class="file-loading">
                <input type="file" name="fileupload[]" id="fileupload" multiple>
            </div>
            <div id="errorBlock" class="help-block"></div>
            <input type="hidden" id="VDAjaxReqProcRefId">

        </div>
    </div>


    <!--   Verificação de Segurança  -->
    <div style="height:75px;">
        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group">
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>
    </div>


    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="Submeter Evento">
    </div>

</form>


<?php
echo $headScripts;
echo $footerScripts;
echo $recaptchaScripts;
echo $localScripts;
?>


<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'submeterevento/'; ?>';
</script>
