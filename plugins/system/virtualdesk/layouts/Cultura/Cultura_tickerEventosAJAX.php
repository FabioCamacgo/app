<style>
    #acontece .date{
        width: 50px;
        display: inline-block;
        vertical-align: top;
        height: 50px;
        padding: 5px;
        margin-top: 5px;
    }

    #acontece .month{
        text-align: center;
        height: 20px;
        line-height: 20px;
        font-size: 12px;
        color: #001b2b;
        font-weight: bold;
        font-family: "Open Sans",sans-serif;
    }

    #acontece .day {
        font-size: 20px;
        text-align: center;
        height: 20px;
        line-height: 20px;
        color: #001b2b;
        font-weight: bold;
        font-family: "Open Sans",sans-serif;
    }

    #acontece .link{
        display: inline-block;
        color: #001b2b;
        font-weight: bold;
        font-family: "Open Sans",sans-serif;
    }

    .bn-news ul li a {
        color: #001b2b;
        font-family: "Open Sans",sans-serif;
    }
</style>

<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('tickereventos');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Cultura/ticker.js' . $addscript_end;


    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Cultura/breaking-news-ticker.min.js' . $addscript_end;
    //END GLOBAL SCRIPTS

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Cultura/breaking-news-ticker.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
    echo $headScripts;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $TickerCultura1 = $obParam->getParamsByTag('TickerCultura1');
    $TickerCultura2 = $obParam->getParamsByTag('TickerCultura2');
    $TickerCultura3 = $obParam->getParamsByTag('TickerCultura3');
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');


    $dataAtual = date("Y-m-d");

    $listaEventos = VirtualDeskSiteCulturaHelper::getEventosTicker('2', $dataAtual);

    if((int)$listaEventos == 0){
        ?>
            <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="example">
                <div class="bn-news">
                    <ul style="list-style:none;">
                        <li><div class="link"><?php echo $TickerCultura1; ?></div></li>
                        <li><div class="link"><?php echo $TickerCultura2; ?></div></li>
                        <li><div class="link"><?php echo $TickerCultura3; ?></div></li>
                        <li><div class="link"><?php echo $TickerCultura1; ?></div></li>
                        <li><div class="link"><?php echo $TickerCultura2; ?></div></li>
                        <li><div class="link"><?php echo $TickerCultura3; ?></div></li>
                    </ul>
                </div>
            </div>
        <?php
    }else{

        if((int)$listaEventos < 4){
                $eventosAnteriores = VirtualDeskSiteCulturaHelper::getEventosTickerAnteriores('2');
            ?>
            <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="example">
                <div class="bn-news">
                    <ul>
                        <?php
                        foreach($eventosAnteriores as $rowWSL) :
                            $dataInicio = $rowWSL['data_inicio'];
                            $dataFim = $rowWSL['data_fim'];
                            $id = $rowWSL['id_evento'];
                            $categoria = $rowWSL['categoria'];
                            $nomeEvento = $rowWSL['nome_evento'];
                            $ArrStartDate = explode("-",$dataInicio);
                            $month = $ArrStartDate[1];
                            $title = 'Agenda ' . $nomeMunicipio . ' - '.$nomeEvento;
                            $monthEventos = VirtualDeskSiteCulturaHelper::getNameMonth($month);
                            $name = str_replace(' ', '_', $nomeEvento);
                            $code = $id . '_' . $name;

                            ?>

                            <li>
                                <div class="date">
                                    <div class="month">
                                        <?php echo $monthEventos; ?>
                                    </div>
                                    <div class="day">
                                        <?php echo $ArrStartDate[2]; ?>
                                    </div>
                                </div>
                                <div class="link"><a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>"><?php echo $nomeEvento; ?></a></div>
                            </li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                </div>
                <div class="bn-controls" style="visibility:hidden">
                    <button><span class="bn-arrow bn-prev"></span></button>
                    <button><span class="bn-action"></span></button>
                    <button><span class="bn-arrow bn-next"></span></button>
                </div>
            </div>
            <?php
        } else {
            ?>
                <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="example">
                    <div class="bn-news">
                        <ul>
                            <?php
                            foreach($listaEventos as $rowWSL) :
                                $dataInicio = $rowWSL['data_inicio'];
                                $dataFim = $rowWSL['data_fim'];
                                $dataOK = VirtualDeskSiteCulturaHelper::compareDate($dataFim, $dataAtual);
                                if($dataOK == true){
                                    $id = $rowWSL['id_evento'];
                                    $categoria = $rowWSL['categoria'];
                                    $nomeEvento = $rowWSL['nome_evento'];
                                    $ArrStartDate = explode("-",$dataInicio);
                                    $month = $ArrStartDate[1];
                                    $title = 'Agenda ' . $nomeMunicipio . ' - '.$nomeEvento;
                                    $monthEventos = VirtualDeskSiteCulturaHelper::getNameMonth($month);
                                    $name = str_replace(' ', '_', $nomeEvento);
                                    $code = $id . '_' . $name;

                                    ?>

                                    <li>
                                        <div class="date">
                                            <div class="month">
                                                <?php echo $monthEventos; ?>
                                            </div>
                                            <div class="day">
                                                <?php echo $ArrStartDate[2]; ?>
                                            </div>
                                        </div>
                                        <div class="link"><a href="<?php echo $nomeSite . $menu;?>?<?php echo $name;?>&name=<?php echo base64_encode($code);?>"><?php echo $nomeEvento; ?></a></div>
                                    </li>
                                    <?php
                                }
                            endforeach;
                            ?>
                        </ul>
                    </div>
                    <div class="bn-controls" style="visibility:hidden">
                        <button><span class="bn-arrow bn-prev"></span></button>
                        <button><span class="bn-action"></span></button>
                        <button><span class="bn-arrow bn-next"></span></button>
                    </div>
                </div>
            <?php
        }

    }

    echo $footerScripts;
?>

<script>
    jQuery('#example').breakingNews();
</script>