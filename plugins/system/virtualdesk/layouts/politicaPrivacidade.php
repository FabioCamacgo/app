<?php
    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('politicaPrivacidade');
    if ($resPluginEnabled === false) exit();

    $obParam      = new VirtualDeskSiteParamsHelper();
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
    $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
    $maildpo = $obParam->getParamsByTag('maildpo');
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
    $codPostalMunicipio = $obParam->getParamsByTag('codPostalMunicipio');
    $moradaMunicipio = $obParam->getParamsByTag('moradaMunicipio');
    $linkApp = $obParam->getParamsByTag('linkApp');
    $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
    $LogoCssFile = $obParam->getParamsByTag('cssFileGeral');
    $Logo = $obParam->getParamsByTag('logoInteriorAPP');
    $brasaoMunicipio = $obParam->getParamsByTag('brasaoMunicipio');

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';

    // Idiomas
    $lang      = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir  = JPATH_SITE;
    $jinput    = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload    = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);

    $templateName  = 'virtualdesk';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/' . $LogoCssFile . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login-5.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $doc->language; ?>" dir="<?php echo $doc->direction; ?>">


    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php echo $headCSS; ?>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-boxed">
        <div class="page-wrapper">

            <div class="politicaprivacidade">
                <!-- BEGIN CONTAINER -->
                <div class="page-container">

                    <meta itemprop="inLanguage" content="pt-PT">


                    <div class="header">
                        <a href="<?php echo $baseurl;?>" class="mainlogo">
                            <img src="<?php echo $baseurl . $Logo;?>" alt="mainlogo"/>
                        </a>

                        <a href="<?php echo $LinkCopyright;?>" target="_blank" class="mainplatform">
                            <img src="<?php echo $baseurl . $brasaoMunicipio;?>" alt="mainplatform"/>
                        </a>
                    </div>

                    <div class="textBody">
                        <h2 class="titulo">Privacidade, Termos, RGPD e Cookies</h2>

                        <p>O presente Portal da <?php echo $nomeMunicipio;?> insere-se em contexto da modernização administrativa levada a cabo pela <?php echo $copyrightAPP;?>, com o claro intuito de ir ao encontro de uma melhor prestação dos serviços municipais, proporcionando, entre diversos aspetos, uma clara vantagem para todos os cidadãos que recorrem e necessitam dos serviços municipais.</p>

                        <p>A <?php echo $copyrightAPP;?>, pessoa coletiva número 511 235 461, situada <?php echo $moradaMunicipio;?>, <?php echo $codPostalMunicipio;?>, respeita, acima de tudo, a sua privacidade e agradece a sua confiança.</p>

                        <p>O objetivo da presente política de privacidade é esclarecer e fazer cumprir com os princípios de tratamento e direitos dos titulares de acordo sendo que a privacidade e a proteção dos dados pessoais representam um firme compromisso para o Município da <?php echo $nomeMunicipio;?> que atua no cumprimento das suas obrigações legais, em particular as que resultam da aplicação do novo Regulamento Geral de Proteção de Dados (RGPD), <a href="https://www.cm-pontadosol.pt/pt/viver/camara/documentos/informacoes-institucionais/ds12-participacao-cidadania-e-comunicacoes/privacidade-e-dados/756-regulamento-2016-679-rgpd-protecao-de-dados/file" target="_blank">Regulamento 2016/679, de 27 de Abril de 2016</a> (“RGPD”) e da Lei de Proteção de Dados, <a href="https://dre.pt/web/guest/pesquisa/-/search/123815982/details/maximized" target="_blank">Lei 58/2019, de 8 de Agosto</a>.</p>

                        <p>Com a presente política de privacidade de utilização do Portal Municipal, pretendemos explicar-lhe, entre diversos aspetos, as seguintes situações:</p>

                        <ul>
                            <li>principais aspetos da política de privacidade e RGPD;</li>
                            <li>registos, tratamentos e categorias;</li>
                            <li>direitos, pressupostos e segurança;</li>
                            <li>com quem os partilhamos os seus dados;</li>
                            <li>durante quanto tempo conservamos os seus dados;</li>
                            <li>quais as formas de exercer os seus direitos.</li>
                        </ul>

                        <p>Esta finalidade objetiva de serviço público e a sua ligação aos serviços digitais, insere-se, portanto, na inequívoca assunção de prestação dos serviços e políticas municipais de administração local desenvolvidas pela <?php echo $copyrightAPP;?>.</p>

                        <p>A <?php echo $copyrightAPP;?> solicita que todos os utilizadores dos serviços digitais do presente Portal Municipal, leiam atentamente todos os termos e condições de utilização, uma vez que os mesmos regulam o acesso e utilização pessoal de cada um dos websites inserido no contexto direto do Portal Municipal da <?php echo $nomeMunicipio;?>.</p>

                        <p>Qualquer utilizador que navegue, interaja ou aceda a qualquer plataforma e funcionalidade integrante do Portal Municipal da <?php echo $nomeMunicipio;?>, aceita tacitamente os termos e condições constantes das presentes <span class="bold">Condições Gerais de Utilização</span>. Não obstante, as interações que envolvam a disponibilização de dados pessoais são realizadas mediante expressa aprovação por parte do utilizador estando prevista sempre o consentimento objetivo da ação envolvendo a aceitação dos mesmos.</p>

                        <p>Em caso de não concordar com os termos abaixo descritos, aconselhamos que nos sejam reportados as situações para o email <span id="cloak6e769a4cee308a20bbe24932fe4c1cea"><a href="mailto:<?php echo $maildpo;?>"><?php echo $maildpo;?></a></span><script type="text/javascript">
                                document.getElementById('cloak6e769a4cee308a20bbe24932fe4c1cea').innerHTML = '';
                                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                                var path = 'hr' + 'ef' + '=';
                                var addy6e769a4cee308a20bbe24932fe4c1cea = 'dp&#111;' + '&#64;';
                                addy6e769a4cee308a20bbe24932fe4c1cea = addy6e769a4cee308a20bbe24932fe4c1cea + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';
                                var addy_text6e769a4cee308a20bbe24932fe4c1cea = 'dp&#111;' + '&#64;' + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';document.getElementById('cloak6e769a4cee308a20bbe24932fe4c1cea').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy6e769a4cee308a20bbe24932fe4c1cea + '\'>'+addy_text6e769a4cee308a20bbe24932fe4c1cea+'<\/a>';
                            </script> da <?php echo $copyrightAPP;?> e que até não utilize os endereços e serviços até contacto institucional sobre as matérias expostas. </p>

                        <p>A <?php echo $copyrightAPP;?> pode alterar ou modificar em qualquer altura, e sem aviso prévio, os conteúdos e condições dos websites que pertencem ao Portal Municipal da <?php echo $nomeMunicipio;?>, incluindo serviços e conteúdos. De modo a garantir uma maior consciência dos seus direitos e deveres enquanto Utilizador , recomendamos que consulte estes termos e condições de utilização sempre que aceder a qualquer plataforma web do presente Portal Municipal, denominada de “Sol Digital”.</p>

                        <p>Ainda no presente enquadramento das condições gerais de utilização dos serviços e uso das informações constantes no domínio <?php echo $dominioMunicipio;?>, bem como em todos os subdomínios e marcas digitais pertencentes à <?php echo $copyrightAPP;?>, o utilizador  assume a responsabilidade pela utilização segundo as normas presentes nos termos e condições e de acordo com a legislação em vigor em Portugal. O utilizador  aceita igualmente que não utilizará as plataformas do Sol Digital para produzir ou disseminar informações, imagens, produtos ou materiais ofensivos, ilegais, maliciosos, ou qualquer outra ação que viole os direitos dos cidadãos ou empresas.</p>


                        <h3 class="topic">Quais são os principais aspetos do Regulamento Geral sobre a Proteção de Dados (RGPD) de que as administrações públicas devem cumprir?</h3>


                        <p>As administrações públicas estão sujeitas às regras do RGPD sempre que efetuam o tratamento de dados pessoais relacionados com um cidadão. </p>

                        <p>A maior parte dos dados pessoais detidos pela administração pública são habitualmente tratados com base numa obrigação jurídica ou na medida do necessário para realizar tarefas por motivos de interesse público ou no exercício de autoridade pública de que está investida.</p>

                        <p>Aquando do tratamento dos dados pessoais, as administrações públicas devem respeitar os princípios fundamentais, nomeadamente:</p>

                        <ul>
                            <li>tratamento equitativo e lícito;</li>
                            <li>limitação da finalidade;</li>
                            <li>minimização dos dados e conservação dos dados.</li>
                        </ul>


                        <p>Caso os dados sejam tratados com base no disposto na lei, tais disposições devem já assegurar o respeito destes princípios (p. ex., os tipos de dados, o período de conservação e as medidas de salvaguarda adequadas).</p>

                        <p>Antes de efetuar o tratamento de dados pessoais, os indivíduos devem ser informados sobre o tratamento, nomeadamente as respetivas finalidades, os tipos de dados recolhidos, os destinatários dos dados e os seus direitos em matéria de proteção de dados.</p>

                        <p>As administrações públicas têm a obrigação de nomear um encarregado da proteção de dados (EPD), que poderão assim partilhar os seus serviços ou subcontratar esta tarefa a um EPD externo.</p>

                        <p>Caso os dados pessoais detidos sejam divulgados acidental ou ilicitamente a destinatários não autorizados, fiquem temporariamente indisponíveis ou sejam alterados, a violação deve ser notificada à autoridade de proteção de dados (APD) sem demora injustificada e, o mais tardar, no prazo de 72 horas após ter tido conhecimento do ocorrido. As administrações públicas também terão de informar os indivíduos sobre a violação de dados.</p>


                        <h3 class="topic">Proteger os seus dados pessoais</h3>


                        <p>Através desta Política, o Município da <?php echo $nomeMunicipio;?> reconhece a importância da segurança dos dados pessoais que trata e assegura a proteção da privacidade dos respetivos titulares sem prejudicar o objeto e concretização plena das diferentes áreas em que atua.</p>

                        <p>Nesta Política, o Município da <?php echo $nomeMunicipio;?> presta ainda informação sobre as regras, os princípios e as boas práticas que observa no âmbito do tratamento dos dados pessoais que lhe são confiados, em conformidade com o Regulamento Geral sobre a Proteção de Dados (RGPD) e demais legislação aplicável, e sobre os meios que os titulares dos dados têm ao seu dispor para exercício dos respetivos direitos.</p>


                        <h3 class="topic">Responsável pelo tratamento do Município da <?php echo $nomeMunicipio;?></h3>

                        <p>No âmbito da atividade que desenvolve nas suas diferentes áreas de atuação, de acordo com a Estabelece o quadro de competências, resultante do regime jurídico das autarquias locais, Lei n.º 75/2013, de 12 de Setembro, assim como o regime jurídico de funcionamento, dos órgãos dos municípios e das freguesias, Lei n.º169/99, de 18 de Setembro, o Município da <?php echo $nomeMunicipio;?>, é a entidade responsável pelo tratamento de dados pessoais, podendo ser contactada através do seguinte endereço de e-mail: <span id="cloakbe4edea5f76284a6fed083b9aae0e8fd"><a href="mailto:<?php echo $maildpo;?>"><?php echo $maildpo;?></a></span><script type="text/javascript">
                                document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML = '';
                                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                                var path = 'hr' + 'ef' + '=';
                                var addybe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;';
                                addybe4edea5f76284a6fed083b9aae0e8fd = addybe4edea5f76284a6fed083b9aae0e8fd + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';
                                var addy_textbe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;' + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybe4edea5f76284a6fed083b9aae0e8fd + '\'>'+addy_textbe4edea5f76284a6fed083b9aae0e8fd+'<\/a>';
                            </script></p>


                        <h3 class="topic">Encarregado de Proteção de Dados</h3>

                        <p>Atendendo à obrigação legal que resulta da alínea a) do n.º 1 do artigo 37.º do RGPD, o Município da <?php echo $nomeMunicipio;?> designou um Encarregado de Proteção de Dados, responsável por garantir, entre outros aspetos, a conformidade das atividades de tratamento e proteção de dados pessoais sob a responsabilidade do Município da <?php echo $nomeMunicipio;?>, de acordo com a legislação aplicável e com a presente Política.</p>

                        <p>Assim, os titulares de dados pessoais, caso o pretendam, podem endereçar uma comunicação ao Encarregado da Proteção de Dados, relativamente a assuntos relacionados com o tratamento de dados pessoais, utilizando, para o efeito, o seguinte email: <span id="cloakbe4edea5f76284a6fed083b9aae0e8fd"><a href="mailto:<?php echo $maildpo;?>"><?php echo $maildpo;?></a></span><script type="text/javascript">
                                document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML = '';
                                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                                var path = 'hr' + 'ef' + '=';
                                var addybe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;';
                                addybe4edea5f76284a6fed083b9aae0e8fd = addybe4edea5f76284a6fed083b9aae0e8fd + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';
                                var addy_textbe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;' + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybe4edea5f76284a6fed083b9aae0e8fd + '\'>'+addy_textbe4edea5f76284a6fed083b9aae0e8fd+'<\/a>';
                            </script></p>


                        <h3 class="subtitulo first">Fundamentação para tratamento de dados pessoais</h3>

                        <p>O Município da <?php echo $nomeMunicipio;?> apenas trata dados pessoais sempre que se verifique, pelo menos, uma das seguintes situações:</p>

                        <h4 class="topic">Consentimento do titular</h4>

                        <p>Quando o titular dos dados tiver dado o seu consentimento para o tratamento dos seus dados pessoais, para uma ou mais finalidades específicas, mediante expresso consentimento, que indique uma manifestação de vontade livre, específica, informada e inequívoca de que o titular consente no tratamento dos seus dados. O consentimento poderá ser obtido por quaisquer meios (incluindo eletrónico), conservando o Município da <?php echo $nomeMunicipio;?> um registo do mesmo, como forma de poder comprovar que o titular deu o seu consentimento para o tratamento dos seus dados pessoais.</p>

                        <p>O titular dos dados tem o direito de retirar o seu consentimento a qualquer momento, sendo que a retirada do consentimento não compromete a licitude do tratamento efetuado com base no consentimento previamente dado.</p>


                        <h4 class="topic">Execução de contrato ou diligências pré-contratuais</h4>

                        <p>Quando o tratamento for necessário para a execução de um contrato no qual o titular dos dados é parte, ou para diligências pré-contratuais a pedido do titular.</p>

                        <p>Nesta situação, enquadram-se, a título exemplificativo, o tratamento de dados pessoais dos trabalhadores do Município da <?php echo $nomeMunicipio;?> no âmbito da gestão da relação laboral estabelecida.</p>

                        <h4 class="topic">Cumprimento de obrigação legal</h4>

                        <p>Quando o tratamento for necessário para o cumprimento de uma obrigação jurídica/legal. Nesta situação, enquadra-se, por exemplo, o tratamento de dados pessoais para cumprimento do dever de identificação e diligência a que o Município da <?php echo $nomeMunicipio;?> está obrigado:</p>

                        <ul>
                            <li>Nas obrigações declarativas a realizar para a Caixa Geral de Aposentações, para a Segurança Social, para a Administração Tributária e para a Direção Geral das Autarquias Locais (“DGAL”);</li>
                            <li>Nos processos de licenciamento industrial decorrentes das responsabilidades que resultam do Sistema da Indústria Responsável (SIR), (Decreto-Lei n.º 73/2015, de 11 de maio);</li>
                            <li>Nos processos de licenciamento urbanístico, que resultam do Regime Jurídico da Urbanização e da Edificação, Decreto-Lei n.º 555/99 de 16 de Dezembro;</li>
                            <li>Nos processos administrativos para obtenção de licenças, de acordo com o respetivo Regulamento Municipal;</li>
                            <li>Nos termos da recolha, registo e atualização de bases de dados da Rede de Defesa da Floresta Contra Incêndios, que resultam do regime jurídico do Sistema Nacional de Defesa da Floresta contra Incêndios (Decreto-Lei n.º 124/2006, de 28 de junho);</li>
                            <li>Nos termos da recolha dos dados a remeter para a DGAL para registo nacional de guardas noturnos, (Lei n.º 105/2015, de 25 de agosto);</li>
                            <li>Nos termos dos dados recolhidos nos programas de estágio profissionais na Administração Local, (Decreto-Lei n.º 18/2010, de 19 de março);</li>
                            <li>Nos termos da lei de combate ao branqueamento de capitais e ao financiamento do terrorismo (Lei n.º 83/2017, de 18 de agosto).</li>
                        </ul>

                        <h4 class="topic">Interesses vitais</h4>

                        <p>Quando o tratamento for necessário para a defesa de interesses vitais do titular dos dados ou de outra pessoa singular.</p>

                        <h4 class="topic">Interesse público/autoridade pública</h4>

                        <p>Quando o tratamento for necessário ao exercício de funções de interesse público ou ao exercício da autoridade pública da responsabilidade do Município da <?php echo $nomeMunicipio;?>. Por exemplo, na aplicação de contraordenações no âmbito das funções que detém.</p>

                        <h4 class="topic">Interesse legítimo</h4>

                        <p>Quando o tratamento for necessário para efeito dos interesses legítimos prosseguidos pelo Município da <?php echo $nomeMunicipio;?> ou por terceiros, exceto se prevalecerem os interesses ou direitos e liberdades fundamentais do titular que exijam a proteção dos dados pessoais.</p>


                        <h3 class="subtitulo first">O que são dados pessoais?</h3>

                        <p><span class="bold">Dados pessoais</span> são qualquer informação, de qualquer natureza e em qualquer suporte (ex: som ou imagem), relativa a uma pessoa singular identificada ou identificável (designado por “titular dos dados”). É considerada identificável a pessoa singular que possa ser identificada direta ou indiretamente, designadamente, através de um nome, de um número de identificação, de um dado de localização, de um identificador eletrónico ou de outros elementos específicos da identidade física, fisiológica, genética, mental, económica, cultural ou social dessa pessoa singular.</p>

                        <p><span class="bold">Dados sensíveis</span> são todos os dados pessoais que estão sujeitos a condições de tratamento específicas e que de alguma maneira revelam enquadramentos genéticos, biométricos, religiosos, políticos ou étnicos.</p>


                        <h3 class="subtitulo first">Categorias de dados pessoais tratados</h3>

                        <p>O Município da <?php echo $nomeMunicipio;?> trata dados pessoais de diferente natureza e sensibilidade, bem como da finalidade associada ao tratamento desses dados, como sejam, a título exemplificativo:</p>

                        <ul>
                            <li><span class="bold">Dados pessoais de identificação:</span> nome, data de nascimento, local de nascimento, sexo, nacionalidade, morada, número de telefone, habilitações profissionais, e-mail, número de identificação civil e/ou passaporte, número de contribuinte, número de carta de condução e número de segurança social;</li>
                            <li><span class="bold">Situação familiar:</span> estado civil, nome do cônjuge, filhos ou pessoas dependentes e/ou qualquer outra informação necessária para determinar os complementos salariais;</li>
                            <li><span class="bold">Atividade profissional:</span> horário, local de trabalho, data de admissão, cargo, categoria profissional e duração da experiência na categoria, nível salarial, tipologia do vínculo contratual e certificado(s) de qualificação profissional;</li>
                            <li><span class="bold">Informações financeiras:</span> remuneração, remunerações suplementares, variáveis ou montantes fixos, subsídios, férias, assiduidade, licenças, ou outras informações relacionadas com remunerações suplementares, montante ou taxas de contribuições obrigatórias ou facultativas, métodos de pagamento, nome do banco e número da conta bancária (NIB ou IBAN), declaração de compatibilidade de funções (quando aplicável);</li>
                            <li><span class="bold">Categorias especiais de dados pessoais:</span> Grau de incapacidade do funcionário e/ou de qualquer membro do seu agregado familiar, possível incapacidade temporária como resultado de acidentes de trabalho ou doenças profissionais e baixas por doença.</li>
                        </ul>


                        <h3 class="subtitulo first">Registo de tratamento de dados</h3>


                        <p>O Município da <?php echo $nomeMunicipio;?> possui um registo de tratamento de dados, no qual estão identificados:</p>

                        <ul>
                            <li>O nome e os contactos do responsável pelo tratamento e, sendo caso disso, de qualquer responsável conjunto pelo tratamento, do representante do responsável pelo tratamento e do encarregado da proteção de dados; As finalidades do tratamento dos dados;</li>
                            <li>A descrição das categorias de titulares de dados e das categorias de dados pessoais;</li>
                            <li>Os prazos previstos para o apagamento das diferentes categorias de dados;</li>
                            <li>As medidas técnicas e organizativas no domínio da segurança implementada para assegurar pseudonimização e a cifragem dos dados pessoais e a capacidade de assegurar a confidencialidade, integridade, disponibilidade e resiliência permanentes dos sistemas e dos serviços de tratamento.</li>
                        </ul>


                        <h3 class="subtitulo first">Finalidades de tratamento dos dados pessoais</h3>

                        <p>Considerando a diversidade das suas áreas de atuação, o Município da <?php echo $nomeMunicipio;?> trata dados pessoais para as seguintes finalidades:</p>

                        <ul>
                            <li><span class="bold">Dados financeiros:</span> Gestão de cobranças/faturação; gestão de pagamentos; receção e tratamento de propostas apresentadas em procedimentos aquisitivos; execução de contratos estabelecidos com fornecedores;</li>
                            <li><span class="bold">Dados pessoais na área do Património, Aprovisionamento e Contratação:</span> Gestão de procedimentos de contratação pública; acompanhar a execução física dos contratos de fornecimentos de bens e serviços;</li>
                            <li><span class="bold">Procedimentos administrativos:</span> Elaboração dos contratos, de direito público ou de direito privado, previstos legalmente, instruindo e praticando os inerentes procedimentos técnico administrativos; apoio técnico a processos de licenciamento; contencioso; contraordenações; execuções. Receção e tratamento de pedidos de suporte informático; Desenvolvimento de novas soluções informáticas; Gestão da Rede e dados do município;</li>
                            <li><span class="bold">Área dos Recursos Humanos:</span> Recrutamento e seleção de recursos humanos; gestão de recursos humanos (assiduidade e gestão de horários); processamento salarial; avaliação de desempenho; promoção da segurança, higiene e saúde no trabalho; atribuição de benefícios sociais aos trabalhadores;</li>
                            <li><span class="bold">Na área do planeamento e Gestão Urbanística:</span> Processos de licenciamento urbanísticos, industriais, outros; editais; reabilitação urbana; processos de vistorias, de contraordenação; informação geográfica. Organização de mapas de controlo de obras e gestão de trabalhadores; controlo físico de acessos; alarme de instalações. Gestão de viaturas e máquinas. CCTV e alarmística;</li>
                            <li><span class="bold">Na área da divisão de Desporto, Educação e Cultura:</span> iniciativas sociais; organização de torneios; convites para eventos e atividades culturais e desportivas; dinamização de ações para desenvolvimento do turismo; no âmbito da gestão dos espaços públicos e culturais do município;</li>
                            <li><span class="bold">Na área do ambiente:</span> gestão de recolha de resíduos; licenciamento; registo animal; pedidos de adoção de animais;</li>
                            <li><span class="bold">Disponibilização de dados na comunicação do município:</span> Divulgação de comunicações internas e externas; gestão de redes sociais; organização de eventos oficiais.</li>
                        </ul>


                        <h3 class="subtitulo first">De que forma são recolhidos os dados pessoais?</h3>

                        <p>O Município da <?php echo $nomeMunicipio;?> pode recolher dados de forma direta (i.e., diretamente junto do titular dos dados) ou de forma indireta (i.e., através de terceiros). A recolha pode ser feita através dos seguintes canais:</p>

                        <ul>
                            <li><span class="bold">Recolha direta:</span> presencialmente, por telefone, por e-mail, através do seu websites e através da área de formação;</li>
                            <li><span class="bold">Recolha indireta:</span> através dos seus associados e/ou terceiros.</li>
                        </ul>


                        <h3 class="subtitulo first">A minha informação é partilhada com terceiros?</h3>

                        <p>Os seus dados pessoais poderão ainda ser acedidos pelas empresas subcontratadas pela <?php echo $copyrightAPP; ?>, para a gestão e manutenção dos sistemas e/ou gestão dos conteúdos, designadamente os serviços de manutenção de sistemas informáticos e de auditoria.</p>


                        <h4 class="topic">Subcontratantes</h4>
                        <p></p>
                        <ul>
                            <li>O Município da <?php echo $nomeMunicipio;?> poderá recorrer a outras entidades por si contratadas (subcontratantes), para, em nome do Município da <?php echo $nomeMunicipio;?>, e de acordo com as instruções dadas por esta, procederem ao tratamento dos dados do titular, em estrito cumprimento do disposto no RGPD, na legislação nacional em matéria de proteção de dados pessoais e na presente Política;</li>
                            <li>Os subcontratantes não poderão transmitir os dados do titular a outras entidades sem que o Município da <?php echo $nomeMunicipio;?> tenha dado, previamente e por escrito, autorização para tal, estando também impedidos de contratar outras entidades sem autorização prévia do Município da <?php echo $nomeMunicipio;?>;</li>
                            <li>O Município da <?php echo $nomeMunicipio;?> compromete-se a assegurar que estes subcontratantes serão apenas entidades que apresentem garantias suficientes de execução das medidas técnicas e organizativas adequadas, de forma a assegurar a privacidade dos dados dos titulares e a defesa dos seus direitos;</li>
                            <li>Todos os subcontratantes ficam vinculados ao Município da <?php echo $nomeMunicipio;?> através de um contrato escrito no qual são regulados, e que resultam de procedimentos de aquisição ao abrigo do Código dos Contratos Públicos, Decreto-Lei n.º 18/2008, de 29 de janeiro, alterado e republicado pelo Decreto-Lei n.º 111-. B/2017, de 31 de agosto, que inclui, , o objeto e a duração do tratamento, a natureza e finalidade do tratamento, o tipo de dados pessoais, as categorias dos titulares dos dados, os direitos e obrigações das partes, incluindo o dever de confidencialidade, e as medidas de segurança a implementar.</li>
                        </ul>

                        <h4 class="topic">Terceiros</h4>

                        <p>O Município da <?php echo $nomeMunicipio;?> poderá ainda transmitir dados a terceiros, designadamente, entidades às quais os dados tenham de ser comunicados de acordo com a legislação aplicável, como, por exemplo, a Autoridade Tributária, a Segurança Social, Caixa Geral de Aposentações, Direção Geral das Autarquias Locais, entidades seguradoras, entre outras.</p>


                        <h3 class="subtitulo first">Quais são e como posso acionar os meus direitos?</h3>

                        <p>Para os serviços que necessitam dos registos e dados pessoais, poderá o utilizador aceder à sua área pessoal e alterar os dados de perfil e/ou de identificação pessoal, bem como solicitar o encerramento da sua área pessoal de registos.</p>

                        <p>Todavia, deverão ser mantidos pela <?php echo $copyrightAPP; ?> todos os registos de dados que legalmente os serviços municipais estão obrigados a manter aquando da submissão de formulários e/ou de requerimentos efetuados.</p>

                        <p>Face à gestão dos seus dados pessoais na <?php echo $copyrightAPP; ?>, os utilizadores dispõem dos seguintes direitos:</p>

                        <h4 class="topic">Direito de acesso</h4>

                        <p>Uma vez subscrevendo, o utilizador terá acesso à sua área pessoal e consequentemente acesso aos registos/requerimentos efetuados por si, através dos serviços online proporcionados no Portal Municipal da <?php echo $nomeMunicipio;?>. Neste sentido, poderá aceder aos seus dados pessoais e às informações previstas na lei.</p>

                        <h4 class="topic">Direito de retificação</h4>

                        <p>Os titulares de área pessoal têm o direito de retificar os dados pessoais podendo efetuar a retificação de forma autónoma e mediante as condições de segurança definidas nos fluxos do software de gestão utilizado, designadamente, através da confirmação das retificações através do email pessoal (indicado aquando da subscrição).</p>

                        <h4 class="topic">Direito à eliminação dos dados pessoais</h4>

                        <p>A <?php echo $copyrightAPP; ?> tem a obrigação de apagar os dados pessoais do utilizador sempre que este requerer tal enquadramento.</p>

                        <p>Não obstante, os mesmos só poderão ser eliminados pela <?php echo $copyrightAPP; ?> quando estiver em causa as seguintes situações:</p>

                        <ul>
                            <li>Quando os dados pessoais deixaram de ser necessários para a finalidade que motivou a sua recolha ou tratamento;</li>
                            <li>Quando o utilizador pretende eliminar a sua área pessoal, sem prejuízo de salvaguarda das informações previstas na lei;</li>
                            <li>Quando o utilizador anulou o consentimento outrora subscrito aquando da submissão de dados;</li>
                            <li>Quando o utilizador se opõe ao tratamento estatístico e de perfil, não existindo interesse legítimo e objetivo que justifique o tratamento dos dados.</li>
                        </ul>

                        <p>A eliminação da conta pessoal do Balcão do Munícipe Online, impede o usufruto da utilização do Portal Municipal da <?php echo $nomeMunicipio;?> e dos demais serviços digitais que requerem autenticação através da conta pelo utilizador.</p>

                        <p>A eliminação da conta pessoal inviabilizará ainda a prestação da informação eventualmente solicitada e quaisquer consultas, acessos, gestão ou solicitações de serviços, prestações ou bens através do Portal Municipal da <?php echo $nomeMunicipio;?>. A eliminação da conta não significa, necessariamente, a eliminação de dados pessoais, os quais são conservados por período previsto para o devido efeito.</p>

                        <h4 class="topic">Direito à limitação do tratamento estatístico dos dados</h4>

                        <p>Os titulares dos dados pessoais inscritos no Portal Municipal da <?php echo $nomeMunicipio;?> têm o direito à limitação do tratamento estatístico dos dados, designadamente quando:</p>

                        <ul>
                            <li>Contradizer a precisão dos dados pessoais no espaço temporal que permita a <?php echo $copyrightAPP; ?> verificar a sua precisão;</li>
                            <li>O titular dos dados solicitar a limitação da sua utilização face à personalização efetuada em função do perfil do utilizador;</li>
                            <li>A <?php echo $copyrightAPP; ?> não necessitar de manter os dados pessoais para fins legais;</li>
                            <li>A finalidade institucional prevalece sobre os do titular dos dados.</li>
                        </ul>

                        <h4 class="topic">Direito de portabilidade dos dados</h4>

                        <p>O Portal Municipal da <?php echo $nomeMunicipio;?>, designadamente na área pessoal do utilizador, permite que o mesmo aceda ao perfil e aos dados pessoais que lhe digam respeito e que eventualmente tenha subscrito.</p>

                        <h4 class="topic">Direito de oposição</h4>

                        <p>O titular tem o direito de se opor a qualquer momento, por motivos relacionados com a sua situação particular, ao tratamento dos dados pessoais que lhe digam respeito que assente no exercício de interesses legítimos prosseguidos ou quando o tratamento for efetuado para fins que não sejam aqueles para os quais os dados pessoais foram recolhidos. Dispõe ainda do direito de apresentar reclamação junto da <a href="https://www.cnpd.pt/" target="_blank">Comissão Nacional de Proteção de Dados</a> (CNPD).</p>

                        <h3 class="subtitulo first">Durante quanto tempo ficam os meus dados conservados na base de dados da Câmara Municipal?</h3>

                        <p>Os dados pessoais são conservados apenas durante o período de tempo necessário para a realização das finalidades para as quais são tratados.</p>

                        <p>O Município da <?php echo $nomeMunicipio;?> cumprirá os prazos máximos de conservação legalmente estabelecidos sem prejuízo, os dados poderão ser conservados por períodos mais longos, para cumprimento de finalidades distintas que possam subsistir, como, por exemplo, o exercício de um direito num processo judicial, fins de arquivo de interesse público, fins de investigação científica ou histórica ou fins estatísticos, aplicando o Município da <?php echo $nomeMunicipio;?> as medidas técnicas e organizativas adequadas.</p>

                        <p>Sem prejuízo dos ajustamentos efetuados pelo utilizador (atualização, correção ou eliminação de dados), os dados pessoais analisados no círculo da presente política de termos de utilização, serão conservados enquanto a conta pessoal do utilizador no balcão municipal online permanecer ativa, bem como enquanto estiver pendente e/ou em execução, qualquer requerimento, serviço ou ação solicitada através do Portal Municipal da <?php echo $nomeMunicipio;?>. O período de conservação dos dados pessoais é de 5 anos após a eliminação da mesma.</p>

                        <h3 class="subtitulo first">Violação de dados</h3>

                        <p>Em caso de violação de dados pessoais, e na medida em que tal violação seja suscetível de resultar num risco elevado para os direitos e liberdades do titular, o Encarregado de Proteção de Dados do Município da <?php echo $nomeMunicipio;?> notificará a autoridade de controlo nacional dessa violação, bem como comunicará a violação ao titular dos dados, até 72 horas após ter tido conhecimento da mesma.</p>

                        <p>Nos termos do RGPD, a comunicação ao titular não é exigida nos seguintes casos:</p>

                        <ul>
                            <li>Caso o Município da <?php echo $nomeMunicipio;?> tenha aplicado medidas de proteção adequadas, tanto técnicas como organizativas, e essas medidas tenham sido aplicadas aos dados pessoais afetados pela violação de dados pessoais, especialmente medidas que tornem os dados pessoais incompreensíveis para qualquer pessoa não autorizada a aceder a esses dados, tais como a cifragem;</li>
                            <li>Caso o Município da <?php echo $nomeMunicipio;?> tenha tomado medidas subsequentes que assegurem que o elevado risco para os direitos e liberdades do titular já não é suscetível de se concretizar;</li>
                            <li>Se a comunicação ao titular implique um esforço desproporcionado para o Município da <?php echo $nomeMunicipio;?>, caso em que esta fará uma comunicação pública ou tomará uma medida semelhante através da qual o titular será informado.</li>
                        </ul>

                        <p>Qualquer violação de dados pessoais, cujo tratamento seja da responsabilidade do Município da <?php echo $nomeMunicipio;?>, poderá ser reportada para o endereço de email: <span id="cloakbe4edea5f76284a6fed083b9aae0e8fd"><a href="mailto:<?php echo $maildpo;?>"><?php echo $maildpo;?></a></span><script type="text/javascript">
                                document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML = '';
                                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                                var path = 'hr' + 'ef' + '=';
                                var addybe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;';
                                addybe4edea5f76284a6fed083b9aae0e8fd = addybe4edea5f76284a6fed083b9aae0e8fd + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';
                                var addy_textbe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;' + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybe4edea5f76284a6fed083b9aae0e8fd + '\'>'+addy_textbe4edea5f76284a6fed083b9aae0e8fd+'<\/a>';
                            </script></p>

                        <h3 class="subtitulo first">Posso anular os meus consentimentos?</h3>

                        <p>O titular dos dados tem sempre o direito de anular o consentimento outrora consentido.</p>

                        <p>Não obstante, mesmo que o utilizador prevaleça o direito de anular os consentimentos previamente acionados, caso os dados sujeitos a consentimento sejam legalmente necessários para o tratamento de dados processuais por parte da Câmara Municipal (requerimentos, certidões, etc.), a legitimidade processual da assunção dos dados submetidos não pode ser comprometida.</p>

                        <p>Caso pretenda retirar o seu consentimento, pode contactar-nos através de carta, por telefone ou através do endereço e-mail <span id="cloakbe4edea5f76284a6fed083b9aae0e8fd"><a href="mailto:<?php echo $maildpo;?>"><?php echo $maildpo;?></a></span><script type="text/javascript">
                                document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML = '';
                                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                                var path = 'hr' + 'ef' + '=';
                                var addybe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;';
                                addybe4edea5f76284a6fed083b9aae0e8fd = addybe4edea5f76284a6fed083b9aae0e8fd + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';
                                var addy_textbe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;' + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybe4edea5f76284a6fed083b9aae0e8fd + '\'>'+addy_textbe4edea5f76284a6fed083b9aae0e8fd+'<\/a>';
                            </script></p>

                        <h3 class="subtitulo first">Medidas de segurança</h3>

                        <p>Tendo em conta o princípio da proporcionalidade e adequabilidade, da segurança, os custos de aplicação e a natureza, o âmbito, o contexto e as finalidades do tratamento, bem como os riscos de probabilidade, o Município da <?php echo $nomeMunicipio;?> aplica medidas de segurança, técnicas e organizativas, adequadas, para assegurar um nível de segurança dos dados pessoais adequado ao risco, como, por exemplo:</p>

                        <ul>
                            <li>Utilização de firewall e sistemas de deteção de intrusão nos seus sistemas de informação;</li>
                            <li>Aplicação de procedimentos de controlo de acessos, com recurso a perfis de acesso diferenciado e com base no princípio da necessidade de saber;</li>
                            <li>Registo de ações efetuadas sobre os sistemas de informação que contenham dados pessoais (login);</li>
                            <li>Execução de um plano de backups;</li>
                            <li>Proteção antisspam de receção e envio de emails corporativos;</li>
                            <li>Instalação, manutenção e gestão dos sistemas de antivírus e de firewall nos computadores do Município da <?php echo $nomeMunicipio;?>;</li>
                            <li>Pseudonimização de dados pessoais;</li>
                            <li>Controlo de acessos às instalações físicas dos equipamentos do Município da <?php echo $nomeMunicipio;?>;</li>
                            <li>Sistema de deteção automática de incêndio e de deteção de intrusão;</li>
                            <li>Execução de ações de formação e/ou sensibilização em segurança da informação e proteção de dados.</li>
                        </ul>

                        <h2 class="titulo second">Termos e condições dos serviços online</h2>

                        <p>No âmbito dos pressupostos da <a href="https://www.cm-pontadosol.pt/pt/viver/camara/modernizacao-administrativa-e-informativa" target="_blank">modernização administrativa e informativa</a> bem como nas considerações de infoinclusão e acessibilidades proporcionados aos munícipes, empresas e investidores do concelho, a Câmara Municipal da <?php echo $nomeMunicipio;?> tem paulatinamente desenvolvido <span class="bold">serviços online</span> onde se pretende agilizar os processos e agregar condições para potenciar territorialmente o Concelho.</p>

                        <p>Relativamente aos dados pessoais solicitados de forma online, os mesmos seguem a orientação e pressupostos dos dados solicitados de forma presencial relativamente aos serviços municipais prestados pela Autarquia. O tratamento dos dados pessoais é necessário para o cumprimento de todos os processos que sejam formalizados junto da Câmara Municipal, fazendo cumprir a tramitação de identificação processual dos assuntos camarários.</p>

                        <p>Caso o utilizador pretenda beneficiar dos serviços online ou da sua área pessoal de registos processuais e pessoais, terá de aceitar expressamente a recolha dos dados pessoais nos formulários de adesão, bem como a aceitação dos Termos de Utilização dos serviços online do Portal Municipal.</p>

                        <p>O utilizador está no direito de não facultar os dados, sendo que, no efeito, não poderá beneficiar dos serviços online/digitais proporcionados pela <?php echo $copyrightAPP; ?>. No caso de pretender usufruir dos serviços, a disponibilização dos dados pessoais é essencial sendo necessário igualmente o tratamento dos dados pessoais para a execução do contrato celebrado com a <?php echo $copyrightAPP; ?>.</p>

                        <h3 class="subtitulo first">Utilização de dados para informação processual e divulgação de atividades municipais</h3>

                        <p>A <?php echo $copyrightAPP; ?>, no normal tratamento dos dados pessoais submetidos pelo utilizador, poderá enviar informações acerca dos fluxos processuais submetidos.</p>

                        <p>Poderão igualmente ser enviados pela Câmara Municipal, no estreito cumprimento das suas ações e serviços municipais, informações acerca das atividades sociais, educacionais, ambientais, urbanísticas, culturais, desportivas e demais serviços prestados aos cidadãos, sendo que as mesmas dependem sempre da aceitação expressa por parte do titular dos dados.</p>

                        <p>O tratamento de dados será sempre realizado com o consentimento do utilizador, expresso no momento da adesão dos serviços online ou área pessoal do munícipe. As comunicações serão sempre efetuadas através de e-mail. O consentimento para o tratamento de dados pessoais para os efeitos previstos neste ponto, podem ser revogados em qualquer altura através da área pessoal do Balcão Online Municipal (plataforma brevemente disponível) ou mediante exposição de intenção através do endereço de email: <span id="cloakbe4edea5f76284a6fed083b9aae0e8fd"><a href="mailto:<?php echo $maildpo;?>"><?php echo $maildpo;?></a></span><script type="text/javascript">
                                document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML = '';
                                var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                                var path = 'hr' + 'ef' + '=';
                                var addybe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;';
                                addybe4edea5f76284a6fed083b9aae0e8fd = addybe4edea5f76284a6fed083b9aae0e8fd + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';
                                var addy_textbe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;' + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybe4edea5f76284a6fed083b9aae0e8fd + '\'>'+addy_textbe4edea5f76284a6fed083b9aae0e8fd+'<\/a>';
                            </script></p>

                        <h3 class="subtitulo first">Sol Digital</h3>

                        <p>As plataformas digitais que compõem a área <a href="https://www.cm-pontadosol.pt/pt/viver/participacao-e-cidadania/sol-digital" target="_blank">Sol Digital</a> da <?php echo $copyrightAPP; ?>, proporcionam serviços online diferenciados em função das áreas de atuação municipal, designadamente:</p>

                        <ul>
                            <li>Balcão Municipal Online;</li>
                            <li>A Minha Rua | Alertas e Ocorrências da Ponta do Sol;</li>
                            <li>Orçamento Participativo;</li>
                            <li>Portal do Urbanismo;</li>
                            <li>Água e Ambiente;</li>
                            <li>Diretório de Empresas;</li>
                            <li>Banco de Terras;</li>
                            <li>Bolsa de emprego;</li>
                            <li>Associativismo;</li>
                            <li>Cultura;</li>
                            <li>Proponho para o Município.</li>
                        </ul>

                        <p>O tratamento de dados pessoais para cada uma das áreas de intervenção seguem os critérios standards de identificação do utilizador, com a incidência das políticas e procedimentos de proteção de dados definidas na política de privacidade, com a finalidade de permitir a identificação e persecução dos serviços e ações municipais.</p>

                        <p>Em algumas das plataformas e serviços online proporcionados aos cidadãos, são solicitados alguns dados pessoais, designadamente:</p>

                        <ul>
                            <li>NIF;</li>
                            <li>Nome completo;</li>
                            <li>Número de identificação civil;</li>
                            <li>Endereço de residência;</li>
                            <li>Endereço de email;</li>
                            <li>Número de telemóvel.</li>
                        </ul>

                        <h2 class="titulo second">Política de Cookies</h2>

                        <h3 class="subtitulo first">Como e para que efeito é que a <?php echo $copyrightAPP; ?> usa cookies e tecnologias semelhantes?</h3>

                        <h4 class="topic">Condições gerais dos cookies</h4>

                        <p>Os cookies são ficheiros de texto com informação enviado por um local de Internet que fica alojado no computador do utilizador. Os cookies contêm um número que permite identificar o computador do utilizado pelo endereço de IP.</p>

                        <p>Os cookies utilizados no Portal Municipal da <?php echo $nomeMunicipio;?>, estão essencialmente orientados para:</p>

                        <ul>
                            <li>agilizar a navegabilidade do utilizador;</li>
                            <li>recolher dados estatísticos para melhorar a performance do Portal Municipal;</li>
                            <li>prestar serviço mais eficiente ao utilizador;</li>
                            <li>mensurar a distribuição geográfica dos acessos ao Portal;</li>
                            <li>avaliar a performance das informações publicadas no Portal Municipal e aferir o alcance das mesmas junto do público-alvo;</li>
                            <li>apoiar o processo de tomada de decisão face às políticas de comunicação da implementação das medidas de administração local da <?php echo $copyrightAPP; ?>;</li>
                            <li>analisar o comportamento do utilizador com o intuito de personalizar e aprimorar a sua experiência na compreensão e interpretação das medidas de governação local, nomeadamente o lugar de acesso, o tempo de conexão, o tipo de dispositivo (fixo ou móvel), o sistema operativo, o navegador utilizado, (browser), as páginas visitadas e os números de cliques;</li>
                            <li>analisar e direcionar a navegação pelas preferências do utilizador;</li>
                            <li>melhorar o design e utilidade do Portal Municipal.</li>
                        </ul>

                        <p>Existem várias tipologias de cookies no Portal Municipal da <?php echo $nomeMunicipio;?>: as internas e as externas sendo que apenas podem ser controladas as cookies criadas pelo Portal Municipal.</p>

                        <p>Está ao alcance do utilizador do Portal Municipal da <?php echo $nomeMunicipio;?>, gerir e controlar os seus cookies ao nível do navegador, pois ao desativar os cookies, pode impedir o uso de determinadas funções.</p>

                        <p>O Portal Municipal, por exemplo, utiliza o Google Analytics para mensurar o tráfego no Portal, sendo que a Google tem sua própria Política de Privacidade as condições de controlo das mesmas.</p>

                        <p>Caso não pretenda que as suas visitas sejam detetadas pelo Google Analytics, aceda a <a href="https://tools.google.com/dlpage/gaoptout" target="_blank">http://tools.google.com/dlpage/gaoptout.</a></p>

                        <p>O Portal Municipal da <?php echo $nomeMunicipio;?> utiliza cookies para as seguintes finalidades:</p>

                        <ul>
                            <li><span class="bold">Cookies necessários:</span> utilizadas essencialmente que o utilizador possa usar o login de acesso ao Balcão Municipal Online da <?php echo $nomeMunicipio;?>, sendo que estas cookies não recolhem nenhuma informação pessoal.</li>
                            <li><span class="bold">Cookies de sessão:</span> utilizadas de forma temporária, em função da sua navegação, permanecendo no browser até sair do Portal Municipal. A informação recolhida permite, por exemplo, identificar anomalias e facultar uma melhor experiência de navegação.</li>
                            <li><span class="bold">Cookies de funcionalidade:</span> utilizadas para agilizar a navegação do utilizador, como por exemplo, lembrar os dados pessoais no preenchimento de formulários.</li>
                            <li><span class="bold">Cookies analíticos:</span> são utilizados para monitorizar o uso e o desempenho do Portal Municipal assim como dos serviços online e informação publicada.</li>
                            <li><span class="bold">Cookies de publicidade institucional:</span> servem para facultar informações municipais relevantes, personalizadas em função do perfil do utilizador e de acordo com as suas preferências.</li>
                        </ul>

                        <h4 class="topic">Autorização para o uso de Cookies</h4>

                        <p>Ao aceder ao Portal Municipal da <?php echo $nomeMunicipio;?>, o mesmo solicita ao utilizador o respetivo consentimento para a utilização de cookies assim como para os termos de utilização.</p>

                        <p>Ao dar consentimento, o utilizador está a melhorar a experiência e navegabilidade no Portal Municipal da <?php echo $nomeMunicipio;?> e declara a sua concordância ao uso de cookies podendo, no entanto, não aceitar. cancelar ou recusar a utilização.</p>

                        <p>O consentimento ao uso de cookies insere-se no âmbito dos cookies de funcionalidade, sendo que recordamos que o uso de cookies pode ser definido nas preferências do seu navegador (browser) designadamente nas opções de privacidade.</p>

                        <p>Nesta medida, sugerimos que consulte as definições do seu navegador ou que visite as páginas web do respetivo fornecedor, designadamente:</p>

                        <ul>
                            <li><span class="bold">Chrome:</span> <a href="https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=en" target="_blank">https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=en</a></li>
                            <li><span class="bold">Firefox:</span> <a href="https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer" target="_blank">https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer</a></li>
                            <li><span class="bold">Internet Explorer:</span> <a href="https://support.microsoft.com/pt-pt/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank"> https://support.microsoft.com/pt-pt/help/17442/windows-internet-explorer-delete-manage-cookies</a></li>
                            <li><span class="bold">Opera:</span> <a href="https://help.opera.com/en/latest/web-preferences/" target="_blank">https://help.opera.com/en/latest/web-preferences/</a></li>
                            <li><span class="bold">Safari:</span> <a href="https://support.apple.com/pt-pt/guide/safari/sfri11471/mac" target="_blank">https://support.apple.com/pt-pt/guide/safari/sfri11471/mac</a></li>
                        </ul>

                        <p>Poderá ainda remover os cookies instaladas no seu navegador ou computador, sendo que para o efeito poderá consultar o presente link <a href="https://www.allaboutcookies.org/" target="_blank">www.allaboutcookies.org.</a></p>

                        <h4 class="topic">Uso de cookies das newsletters</h4>

                        <p>As newsletters do Portal Municipal da <?php echo $nomeMunicipio;?> registam igualmente dados pessoais que são guardados para fins de gestão da tipologia de assuntos/categorias pretendidos pelo utilizador.</p>

                        <p>Os mesmos têm finalidade de informar os contornos das newsletters enviadas e requeridas bem como apresentar dados estatísticos acerca da receção/leitura das mesmas. O utilizador tem a possibilidade de cancelar a subscrição efetuada sendo os seus dados automaticamente eliminados da base de dados da gestão das newsletters (nome e email).</p>

                        <p>Tem alguma dúvida?</p>

                        <p>Se permanecer com alguma dúvida relativamente ao tratamento dos seus dados pessoais, ou pretender exercer algum dos seus direitos, por favor contacte-nos:</p>

                        <ul>
                            <li><span class="bold">Por telefone:</span> 291 972 106</li>
                            <li><span class="bold">Por e-mail:</span> <span id="cloakbe4edea5f76284a6fed083b9aae0e8fd"><a href="mailto:<?php echo $maildpo;?>"><?php echo $maildpo;?></a></span><script type="text/javascript">
                                    document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML = '';
                                    var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                                    var path = 'hr' + 'ef' + '=';
                                    var addybe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;';
                                    addybe4edea5f76284a6fed083b9aae0e8fd = addybe4edea5f76284a6fed083b9aae0e8fd + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';
                                    var addy_textbe4edea5f76284a6fed083b9aae0e8fd = 'dp&#111;' + '&#64;' + 'cm-p&#111;nt&#97;d&#111;s&#111;l' + '&#46;' + 'pt';document.getElementById('cloakbe4edea5f76284a6fed083b9aae0e8fd').innerHTML += '<a ' + path + '\'' + prefix + ':' + addybe4edea5f76284a6fed083b9aae0e8fd + '\'>'+addy_textbe4edea5f76284a6fed083b9aae0e8fd+'<\/a>';
                                </script></p></li>
                        </ul>

                        <p>Ocasionalmente, a <?php echo $copyrightAPP; ?> atualizará a presente Política de Privacidade. Solicitamos-lhe que reveja periodicamente este documento para se manter atualizado.</p>

                        <p><span class="bold">Última atualização:</span> fevereiro de 2020</p>



                    </div>

                    <div class="copyrightBar">
                        <a href="<?php echo $LinkCopyright;?>" target="_blank">
                            <?php echo $copyrightAPP;?>
                        </a>
                    </div>

                    <?php echo $headScripts; ?>
                    <?php echo $footerScripts;?>

                </div>
            </div>
        </div>
    </body>

</html>
