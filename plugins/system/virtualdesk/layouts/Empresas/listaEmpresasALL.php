<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $baseurl = JUri::base();

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaEmpresas');
    if ($resPluginEnabled === false) exit();

    $obParam      = new VirtualDeskSiteParamsHelper();
    $imagensListaSubCats = $obParam->getParamsByTag('imagensListaSubCats');
    $imagensListaCats = $obParam->getParamsByTag('imagensListaCats');
    $imagensIcons = $obParam->getParamsByTag('imagensIcons');


    $listaEmpresas = VirtualDeskSiteEmpresasHelper::getempresasFilter();


    foreach($listaEmpresas as $rowWSL) :

        $id = $rowWSL['id'];
        $referencia = $rowWSL['referencia'];
        $designacao = $rowWSL['designacao'];
        $categoria = $rowWSL['categoria'];
        $subcategoria = $rowWSL['subcategoria'];
        $freguesia = $rowWSL['freguesia'];
        $imgEmpresa = VirtualDeskSiteEmpresasHelper::getImgEmpresa($referencia);
        $temImagem = 0;
        $idCat = VirtualDeskSiteEmpresasHelper::getIdCat($categoria);
        $name = str_replace(' ', '_', $designacao);
        $code = $id . '_' . $name;

        ?>
            <div class="empresa">
                <div class="imagem">
                    <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" title="<?php echo $designacao;?>">
                        <?php
                            if((int)$imgEmpresa == 0){
                                if($idCat == 4){
                                    $idSubCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);
                                    ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats . $idSubCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                    <?php
                                } else if($idCat == 16 || $idCat == 5 ){/*Categorias sem banner*/
                                    $idSubCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);
                                    if($idSubCat == 73 || $idSubCat == 75 || $idSubCat == 76 || $idSubCat == 79 || $idSubCat == 38 || $idSubCat == 39 || $idSubCat == 40 || $idSubCat == 41){
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats . $idSubCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats;?>banner.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    }
                                } else if($idCat == 3 || $idCat == 8 || $idCat == 11 || $idCat == 12 || $idCat == 13 || $idCat == 14 || $idCat == 15){
                                    $idSubCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);
                                    if($idSubCat == 16 || $idSubCat == 22 || $idSubCat == 23 || $idSubCat == 26 || $idSubCat == 47 || $idSubCat == 48 || $idSubCat == 49 || $idSubCat == 50 || $idSubCat == 55 || $idSubCat == 57 || $idSubCat == 58 || $idSubCat == 60 || $idSubCat == 61 || $idSubCat == 63 || $idSubCat == 64 || $idSubCat == 67 || $idSubCat == 72){
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats . $idSubCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaCats . $idCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    }
                                } else if($idCat == 7){
                                    $idSubCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);
                                    if($numImage == 0){
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats; ?>odd.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                        $numImage = 1;
                                    }else {
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats;?>even.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                        $numImage = 0;
                                    }
                                } else {
                                    ?>
                                        <img src="<?php echo $baseurl . $imagensListaCats . $idCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                    <?php
                                }
                            } else {
                                $objEventFile = new VirtualDeskSiteEmpresasFilesHelper();
                                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                $FileList21Html = '';
                                foreach ($arFileList as $rowFile) {
                                    if($temImagem == 0){
                                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                        ?>
                                        <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>">
                                        <?php
                                        $temImagem = 1;
                                    }
                                }
                            }
                        ?>
                    </a>
                </div>
                <div class="dados">
                    <div class="titulo">
                        <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" title="<?php echo $designacao;?>">
                            <h3 class="titleEvent">
                                <?php echo $designacao; ?>
                            </h3>
                        </a>
                    </div>

                    <div class="info">
                        <div class="cat">
                            <div class="content">
                                <div class="icon">
                                    <img src="<?php echo $baseurl .$imagensIcons; ?>Categoria.png" alt="iconCategoria" title="Categoria"/>
                                </div>
                                <div class="text">
                                    <?php echo $categoria; ?>
                                </div>
                            </div>
                        </div>

                        <div class="subcat">
                            <div class="content">
                                <div class="icon">
                                    <img src="<?php echo $baseurl .$imagensIcons; ?>Subcategoria.png" alt="iconSubcategoria" title="Subcategoria"/>
                                </div>
                                <div class="text">
                                    <?php echo $subcategoria; ?>
                                </div>
                            </div>
                        </div>

                        <?php
                            if(!empty($freguesia)){
                                ?>
                                    <div class="freg">
                                        <div class="content">
                                            <div class="icon">
                                                <img src="<?php echo $baseurl .$imagensIcons; ?>Freguesia.png" alt="iconFreguesia" title="Freguesia"/>
                                            </div>
                                            <div class="text">
                                                <?php echo $freguesia; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>

                    <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" title="Saiba mais">
                        <div class="sabermais">
                            <button class="botaoSaberMais">
                                <div class="borda"></div>
                                <input type="submit" name="sabermais" id="sabermais" value="<?php echo JText::_('COM_VIRTUALDESK_EMPRESA_SABERMAIS'); ?>">
                            </button>
                        </div>
                    </a>
                </div>
            </div>
        <?php

    endforeach;
?>