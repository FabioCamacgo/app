<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroFP');
    if ($resPluginEnabled === false) exit();



    //LOCAL SCRIPTS
    //$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Empresas/listaEmpresas.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
    $obParam      = new VirtualDeskSiteParamsHelper();
    $concelhoEmpresas = $obParam->getParamsByTag('concelhoEmpresasGOV');

    if (isset($_POST['submitForm'])){
        $nomePost = $_POST['nomePost'];
        $cat = $_POST['categoria'];
        $subCcat = $_POST['vdmenumain'];
        $freg = $_POST['freguesia'];

        if($cat == 2){
            ?>
            <script>
                window.location.href = "<?php echo $nomeSiteAssoc;?>";
            </script>
            <?php
        } else {
            ?>
            <script>
                window.location.href = "lista-de-empresas?cat=<?php echo $cat;?>&subcat=<?php echo $subCcat;?>&freg=<?php echo $freg;?>&name=<?php echo $nomePost;?>";
            </script>
            <?php
        }
    }

?>

    <form id="ProcuraEmpresa" action="" method="post" class="login-form" enctype="multipart/form-data" >
        <!--   Categoria   -->
        <div class="form-group" id="cat">
            <div class="col-md-9">
                <?php $categorias = VirtualDeskSiteEmpresasHelper::getCategorias()?>
                <div class="input-group ">
                    <select name="categoria" value="<?php echo $cat; ?>" id="categoria" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($cat)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_CATEGORIA'); ?></option>
                            <?php foreach($categorias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $cat; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getCatName($cat) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeCategoria = VirtualDeskSiteEmpresasHelper::excludeCategoria($cat)?>
                            <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>


        <!--   SubCategoria   -->
        <?php
        // Carrega menusec se o id de menumain estiver definido.
        $ListaDeMenuMain = array();
        if(!empty($cat)) {
            if( (int) $cat > 0) $ListaDeMenuMain = VirtualDeskSiteEmpresasHelper::getSubCategoria($cat);
        }

        ?>
        <div id="blocoMenuMain" class="form-group">
            <div class="col-md-9">
                <select name="vdmenumain" id="vdmenumain"
                    <?php
                    if(empty($cat)) {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($subCcat)){
                        ?>
                        <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EMPRESA_SUBCATEGORIA' ); ?></option>
                        <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                                <?php
                                if(!empty($this->data->vdmenumain)) {
                                    if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                                }
                                ?>
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach; ?>
                        <?php
                    } else {
                        ?>
                        <option value="<?php echo $subCcat; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getSubCatName($subCcat) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeSubCategoria = VirtualDeskSiteEmpresasHelper::excludeSubCategoria($cat, $subCcat)?>
                        <?php foreach($ExcludeSubCategoria as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['subcategoria']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>

            </div>
        </div>



        <!--   Freguesia   -->
        <div class="form-group" id="freg">
            <?php $Freguesias = VirtualDeskSiteEmpresasHelper::getFreguesia($concelhoEmpresas)?>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="freguesia" value="<?php echo $freg; ?>" id="freguesia" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($freg)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FREGUESIA'); ?></option>
                            <?php foreach($Freguesias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                ><?php echo $rowWSL['freguesia']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $freg; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getFregName($freg) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeFreguesia = VirtualDeskSiteEmpresasHelper::excludeFreguesia('4', $freg)?>
                            <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                ><?php echo $rowWSL['freguesia']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <!--   Pesquisa Livre  -->
        <div class="form-group" id="nomeSearch">
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_PESQUISALIVRE'); ?>"
                       name="nomePost" id="nomePost" value="<?php echo $nomePost; ?>"/>
            </div>
        </div>

        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
        </div>




    </form>


<?php

echo $headScripts;
echo $footerScripts;
echo $localScripts;

?>
<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
</script>