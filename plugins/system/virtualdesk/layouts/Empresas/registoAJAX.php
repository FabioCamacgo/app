<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('registoEmpresas');
    if ($resPluginEnabled === false) exit();



    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Empresas/maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Empresas/edit_maps.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Empresas/registo.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS

    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //FileInput***
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/plugins/sortable.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/fileinput.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/locales/pt.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/fa/theme.js' . $addscript_end;
    $headScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    //FileInput***
    $headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/css/fileinput.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $concelhoEmpresas = $obParam->getParamsByTag('concelhoEmpresasGOV');

    if (isset($_POST['submitForm'])) {
        $nomeEmpresa = $_POST['nomeEmpresa'];
        $designacaoComercial = $_POST['designacaoComercial'];
        $apresentacao = $_POST['apresentacao'];
        $categoria = $_POST['categoria'];
        $subcategoria = $_POST['vdmenumain'];
        $morada = $_POST['morada'];
        $coordenadas = $_POST['coordenadas'];
        $splitCoord = explode(",", $coordenadas);
        $lat = $splitCoord[0];
        $long = $splitCoord[1];
        $codPostal4 = $_POST['codPostal4'];
        $codPostal3 = $_POST['codPostal3'];
        $codPostalText = $_POST['codPostalText'];
        $codPostal = $codPostal4 . '-' . $codPostal3 . ' ' . $codPostalText;
        $freguesia = $_POST['freguesia'];
        $fiscalid = $_POST['fiscalid'];
        $telefone = $_POST['telefone'];
        $email = $_POST['email'];
        $confEmail = $_POST['confEmail'];
        $website = $_POST['website'];
        $facebook = $_POST['facebook'];
        $instagram = $_POST['instagram'];
        $nomeResponsavel = $_POST['nomeResponsavel'];
        $cargoResponsavel = $_POST['cargoResponsavel'];
        $telefoneResponsavel = $_POST['telefoneResponsavel'];
        $emailResponsavel = $_POST['emailResponsavel'];
        $confEmailResponsavel = $_POST['confEmailResponsavel'];
        $politica2 = $_POST['politica2'];
        $veracid2 = $_POST['veracid2'];
        $nregisto = $_POST['nregisto'];
        $capacidade = $_POST['capacidade'];
        $unidades = $_POST['unidades'];
        $tipologia = $_POST['tipologia'];
        $nQuartos = $_POST['nQuartos'];
        $nUtentes = $_POST['nUtentes'];
        $nCamas = $_POST['nCamas'];


        // Valida recaptcha
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');
        $jinput->set('g-recaptcha-response',$recaptcha_response_field);
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        $errRecaptcha = 0;
        if (!$resRecaptchaAnswer) {
            $errRecaptcha = 1;
        }


        $random = VirtualDeskSiteEmpresasHelper::random_code();

        $referencia = 'EMP-PSOL-' . $fiscalid . '-' . $random;

        $saveEmpresa = VirtualDeskSiteEmpresasHelper::SaveNovaEmpresa($referencia, $nomeEmpresa, $designacaoComercial, $apresentacao, $categoria, $subcategoria, $morada, $lat, $long, $codPostal, $freguesia, $fiscalid, $telefone, $email, $website, $facebook, $instagram, $nomeResponsavel, $cargoResponsavel, $telefoneResponsavel, $emailResponsavel, $nregisto, $capacidade, $unidades, $tipologia, $nQuartos, $nUtentes, $nCamas);

        if($saveEmpresa == true) { ?>


            <div class="message"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_NOVOEMPRESA_SUCESSO'); ?></div>

            <?php

            echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');

            exit();

        }else { ?>

            <div class="message"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOVOEMPRESA_ERRO'); ?></div>

        <?php }
    }


?>

<form id="novaEmpresa" action="" method="post" class="novaEmpresa-form" enctype="multipart/form-data" >


    <h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_DADOSEMPRESA'); ?></h3>

    <!--   NOME EMPRESA   -->
    <div class="form-group" id="nomeGroup">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_NOMEEMPRESA'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESA_NOMEEMPRESA_INSIDE'); ?>"
                   name="nomeEmpresa" id="nomeEmpresa" maxlength="150" value="<?php echo $nomeEmpresa; ?>"/>
        </div>
    </div>
    <!--   DESIGNAÇÃO COMERCIAL   -->
    <div class="form-group" id="comGroup">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_DESIGNACAOCOMERCIAL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESA_DESIGNACAOCOMERCIAL_INSIDE'); ?>"
                   name="designacaoComercial" id="designacaoComercial" maxlength="150" value="<?php echo $designacaoComercial; ?>"/>
        </div>
    </div>

    <!--  Apresentacao da Empresa  -->
    <div class="form-group" id="apr">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_APRESENTACAO'); ?></label>
        <div class="col-md-9">
            <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESA_APRESENTACAO_INSIDE'); ?>"
                      name="apresentacao" id="apresentacao" maxlength="2000"><?php echo $apresentacao; ?></textarea>
        </div>
    </div>


    <!--   Categoria   -->
    <div class="form-group" id="cat">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_CATEGORIA'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <?php $categorias = VirtualDeskSiteEmpresasHelper::getCategorias()?>
            <div class="input-group ">
                <select name="categoria" required value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <option>Identificar categoria da empresa nas opções definidas</option>
                    <?php foreach($categorias as $rowStatus) : ?>
                        <option value="<?php echo $rowStatus['id']; ?>"
                            <?php
                            if(!empty($this->data->categoria)) {
                                if( (int)($rowStatus['id'] == (int) $this->data->idwebsitelist) ) echo 'selected';
                            }
                            ?>
                        ><?php echo $rowStatus['categoria']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>


    <!--   SubCategoria   -->
    <?php
    // Carrega menusec se o id de menumain estiver definido.
    $ListaDeMenuMain = array();
    if(!empty($this->data->categoria)) {
        if( (int) $this->data->categoria > 0) $ListaDeMenuMain = VirtualDeskSiteEmpresasHelper::getSubCategoria($this->data->categoria);
    }

    ?>
    <div id="blocoMenuMain" class="form-group">
        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_EMPRESA_SUBCATEGORIA' ); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <select name="vdmenumain" id="vdmenumain" required
                <?php
                if(!empty($this->data->categoria)) {
                    if ((int)$this->data->categoria <= 0 || empty($ListaDeMenuMain)) echo 'disabled';
                }
                else {
                    echo 'disabled';
                }
                ?>
                    class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                <option>Identificar subcategoria da empresa nas opções definidas</option>
                <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                    <option value="<?php echo $rowMM['id']; ?>"
                        <?php
                        if(!empty($this->data->vdmenumain)) {
                            if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                        }
                        ?>
                    ><?php echo $rowMM['name']; ?></option>
                <?php endforeach; ?>
            </select>

        </div>
    </div>


    <div id="Alojamento" style="display:none;">

        <h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_DADOSALOJAMENTO'); ?></h3>

        <!--   Numero de Registo  -->
        <div class="form-group" id="numeroRegisto">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NREGISTO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NREGISTO'); ?>"
                       name="nregisto" id="nregisto" maxlength="20" value="<?php echo $nregisto; ?>"/>
            </div>
        </div>


        <!--   Capacidade  -->
        <div class="form-group" id="capacidadeAlojamento" style="display:none;">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_CAPACIDADE'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_CAPACIDADE'); ?>"
                       name="capacidade" id="capacidade" maxlength="4" value="<?php echo $capacidade; ?>"/>
            </div>
        </div>

        <!--   Unidades de Alojamento  -->
        <div class="form-group" id="unidadesAlojamento" style="display:none;">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_UNIDADES'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_UNIDADES'); ?>"
                       name="unidades" id="unidades" maxlength="4" value="<?php echo $unidades; ?>"/>
            </div>
        </div>

        <!--   Tipo de Alojamento   -->
        <div class="form-group" id="tipologiaAlojamento">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_TIPOLOGIA'); ?><span class="required">*</span></label>
            <?php $Tipologia = VirtualDeskSiteEmpresasHelper::getTipologia()?>
            <div class="col-md-9">
                <select name="tipologia" value="<?php echo $tipologia; ?>" id="tipologia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($tipologia)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_TIPOLOGIA'); ?></option>
                        <?php foreach($Tipologia as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['tipologia']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $tipologia; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getTipologiaName($tipologia) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeTipologia = VirtualDeskSiteEmpresasHelper::excludeTipologia($tipologia)?>
                        <?php foreach($ExcludeTipologia as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['tipologia']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>

        <!--   Numero Quartos  -->
        <div class="form-group" id="numeroQuartos" style="display:none;">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NQUARTOS'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NQUARTOS'); ?>"
                       name="nQuartos" id="nQuartos" maxlength="5" value="<?php echo $nQuartos; ?>"/>
            </div>
        </div>

        <!--   Numero Utentes  -->
        <div class="form-group" id="numeroUtentes" style="display:none;">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NUTENTES'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NUTENTES'); ?>"
                       name="nUtentes" id="nUtentes" maxlength="5" value="<?php echo $nUtentes; ?>"/>
            </div>
        </div>

        <!--   Numero Camas  -->
        <div class="form-group" id="numeroCamas" style="display:none;">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NCAMAS'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NCAMAS'); ?>"
                       name="nCamas" id="nCamas" maxlength="5" value="<?php echo $nCamas; ?>"/>
            </div>
        </div>





    </div>


    <!--   MORADA  -->
    <div class="form-group" id="address">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_MORADA'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_MORADA_INSIDE'); ?>"
                   name="morada" id="morada" maxlength="200" value="<?php echo $morada; ?>"/>
        </div>
    </div>


    <!--   MAPA   -->
    <div class="form-group" id="mapa">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_MAPA'); ?></label>
        <div class="col-md-9">
            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

            <input type="hidden" required class="form-control"
                   name="coordenadas" id="coordenadas"
                   value="<?php echo htmlentities($this->data->$coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
        </div>
    </div>


    <!--   CÓDIGO POSTAL  -->
    <div class="form-group" id="codPostal">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CODIGOPOSTAL_LABEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'XXXX'; ?>"
                   name="codPostal4" id="codPostal4" maxlength="4" value="<?php echo $codPostal4; ?>"/>
            <?php echo '-'; ?>
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'YYY'; ?>"
                   name="codPostal3" id="codPostal3" maxlength="3" value="<?php echo $codPostal3; ?>"/>
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'Localidade'; ?>"
                   name="codPostalText" id="codPostalText" maxlength="90" value="<?php echo $codPostalText; ?>"/>
        </div>
    </div>



    <!--   Freguesia   -->
    <div class="form-group" id="freg">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FREGUESIA'); ?><span class="required">*</span></label>
        <?php $Freguesias = VirtualDeskSiteEmpresasHelper::getFreguesia($concelhoEmpresas)?>
        <div class="col-md-9">
            <select name="freguesia" value="<?php echo $freguesia; ?>" id="freguesia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($freguesia)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FREGUESIA_INSIDE'); ?></option>
                    <?php foreach($Freguesias as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                        ><?php echo $rowWSL['freguesia']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getFregName($freguesia) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeFreguesia = VirtualDeskSiteEmpresasHelper::excludeFreguesia($concelhoEmpresas, $freguesia)?>
                    <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                        ><?php echo $rowWSL['freguesia']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>


    <!--   NIF/NIPC   -->
    <div class="form-group" id="nif">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FISCALID'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FISCALID_INSIDE'); ?>"
                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
        </div>
    </div>


    <!--   Telefone Principal   -->
    <div class="form-group" id="phone">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_TELEFONE'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_TELEFONE_INSIDE'); ?>"
                   name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
        </div>
    </div>


    <!--   EMAIL  -->
    <div class="form-group" id="mail">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_EMAIL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_EMAIL_INSIDE'); ?>"
                   name="email" id="email" maxlength="150" value="<?php echo htmlentities($email, ENT_QUOTES, 'UTF-8'); ?>" />
        </div>
    </div>


    <!--   CONFIRMAÇÃO DE EMAIL  -->
    <div class="form-group" id="confmail">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_CONFEMAIL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_CONFEMAIL_INSIDE'); ?>"
                   name="confEmail" id="confEmail" maxlength="150" value="<?php echo htmlentities($confEmail, ENT_QUOTES, 'UTF-8'); ?>" />
        </div>
    </div>



    <!--  website  -->
    <div class="form-group" id="empwebsite">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_WEBSITE'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_WEBSITE_INSIDE'); ?>"
                   name="website" id="website" maxlength="200" value="<?php echo $website; ?>"/>
        </div>
    </div>


    <!--  Facebook  -->
    <div class="form-group" id="empfacebook">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FACEBOOK'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FACEBOOK_INSIDE'); ?>"
                   name="facebook" id="facebook" maxlength="200" value="<?php echo $facebook; ?>"/>
        </div>
    </div>


    <!--  Instagram  -->
    <div class="form-group" id="empinstagram">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_INSTAGRAM'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_INSTAGRAM_INSIDE'); ?>"
                   name="instagram" id="instagram" maxlength="200" value="<?php echo $instagram; ?>"/>
        </div>
    </div>


    <!--   Upload Imagens   -->
   <div class="form-group" id="uploadField">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_UPLOAD'); ?></label>
        <div class="col-md-9">

            <div class="file-loading">
                <input type="file" name="fileupload[]" id="fileupload" multiple>
            </div>
            <div id="errorBlock" class="help-block"></div>
            <input type="hidden" id="VDAjaxReqProcRefId">

        </div>
    </div>



    <h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_DADOSRESPONSAVEL'); ?></h3>


    <!--   NOME  Responsável -->
    <div class="form-group" id="nomeRespGroup">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_NOMERESPONSAVEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESA_NOMERESPONSAVEL_INSIDE'); ?>"
                   name="nomeResponsavel" id="nomeResponsavel" maxlength="150" value="<?php echo $nomeResponsavel; ?>"/>
        </div>
    </div>

    <!--   Cargo Responsável   -->
    <div class="form-group" id="cargoGroup">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_CARGORESPONSAVEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESA_CARGORESPONSAVEL_INSIDE'); ?>"
                   name="cargoResponsavel" id="cargoResponsavel" maxlength="150" value="<?php echo $cargoResponsavel; ?>"/>
        </div>
    </div>

    <!--   Telefone Responsável   -->
    <div class="form-group" id="phoneResp">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_TELEFONERESPONSAVEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_TELEFONERESPONSAVEL_INSIDE'); ?>"
                   name="telefoneResponsavel" id="telefoneResponsavel" maxlength="9" value="<?php echo $telefoneResponsavel; ?>"/>
        </div>
    </div>


    <!--   EMAIL Responsável -->
    <div class="form-group" id="mailResp">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_EMAILRESPONSAVEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_EMAILRESPONSAVEL_INSIDE'); ?>"
                   name="emailResponsavel" id="emailResponsavel" maxlength="150" value="<?php echo htmlentities($emailResponsavel, ENT_QUOTES, 'UTF-8'); ?>" />
        </div>
    </div>


    <!--   CONFIRMAÇÃO DE EMAIL  Responsável-->
    <div class="form-group" id="confmailResp">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_CONFEMAILRESPONSAVEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_CONFEMAILRESPONSAVEL_INSIDE'); ?>"
                   name="confEmailResponsavel" id="confEmailResponsavel" maxlength="150" value="<?php echo htmlentities($confEmailResponsavel, ENT_QUOTES, 'UTF-8'); ?>" />
        </div>
    </div>



    <!--   Veracidade -->
    <div class="form-group" id="Veracidade">

        <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_VERACIDADE'); ?><span class="required">*</span></label>

        <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
    </div>

    <script>

        document.getElementById('veracid').onclick = function() {
            // access properties using this keyword
            if ( this.checked ) {
                document.getElementById("veracid2").value = 1;
            } else {
                document.getElementById("veracid2").value = 0;
            }
        };
    </script>

    <!--   Politica de privacidade -->
    <div class="form-group" id="PoliticaPrivacidade">

        <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
        <label class="col-md-3 control-label">Declaro que li e aceito a <a href="#" target="_blank">Pol&iacute;tica de Privacidade</a>. <span class="required">*</span></label>

        <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
    </div>

    <script>

        document.getElementById('politica').onclick = function() {
            // access properties using this keyword
            if ( this.checked ) {
                document.getElementById("politica2").value = 1;
            } else {
                document.getElementById("politica2").value = 0;
            }
        };
    </script>


    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group" >
            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
            $field_id = 'dynamic_recaptcha_1';
            print $captcha->display($field_id, $field_id, 'g-recaptcha');
            ?>
            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
        </div>
    <?php endif; ?>


    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_REGISTAR'); ?>">
    </div>


</form>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;

?>

<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'registoEmpresas/'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
    var iconpath = '<?php echo JUri::base() . 'plugins/system/virtualdesk/layouts/includes/Espera.png'; ?>';
</script>
