<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listacategoriasEmpresas');
    if ($resPluginEnabled === false) exit();

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;


    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
    $imagensCat = $obParam->getParamsByTag('imagensCat');

    $procuraCategorias = VirtualDeskSiteEmpresasHelper::getCategorias();

    foreach($procuraCategorias as $rowWSL) :
        $idCat = $rowWSL['id'];
        $nameCat = $rowWSL['categoria'];

        $nospecial = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $nameCat);
        $string = str_replace(' / ', '', $nospecial);
        $string2 = str_replace(' ', '_', $string);

        ?>
        <div class="wideCat">
            <?php
                if($idCat == 2){
                    ?>
                        <a href="<?php echo $nomeSiteAssoc;?>" target="_blank" title="Empresas <?php echo $nomeMunicipio;?> - <?php echo $nameCat;?>">
                            <div class="nomeCat">
                                <h3>
                                    <div class="nome"><?php echo $nameCat; ?></div>
                                </h3>
                            </div>

                            <div class="image">
                                <img src="<?php echo $baseurl . $imagensCat . $idCat; ?>.jpg" alt="<?php echo $string2; ?>" title="<?php echo $nameCat; ?> - Empresas <?php echo $nomeMunicipio;?>"/>
                            </div>
                        </a>
                    <?php
                } else {
                    ?>
                        <a href="<?php echo $nomeSite . $menu;?>?cat=<?php echo $idCat;?>" title="Empresas <?php echo $nomeMunicipio;?> - <?php echo $nameCat;?>">
                            <div class="nomeCat">
                                <h3>
                                    <div class="nome"><?php echo $nameCat; ?></div>
                                </h3>
                            </div>

                            <div class="image">
                                <img src="<?php echo $baseurl . $imagensCat . $idCat; ?>.jpg" alt="<?php echo $string2; ?>" title="<?php echo $nameCat; ?> - Empresas <?php echo $nomeMunicipio;?>"/>
                            </div>
                        </a>
                    <?php
                }
            ?>
        </div>
    <?php

    endforeach;

    echo $footerScripts;

?>