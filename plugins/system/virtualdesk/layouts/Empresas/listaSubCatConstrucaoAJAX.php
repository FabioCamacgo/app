<?php

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaSubcatConstrucao');
if ($resPluginEnabled === false) exit();

//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;


//END GLOBAL MANDATORY STYLES


//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES


// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';


echo $headCSS;

$obParam      = new VirtualDeskSiteParamsHelper();
$nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
$imagensConstrucao = $obParam->getParamsByTag('imagensConstrucao');

$procuraSubCategorias = VirtualDeskSiteEmpresasHelper::getSubCategoria('4');

foreach($procuraSubCategorias as $rowWSL) :
    $idSubCat = $rowWSL['id'];
    $nameSubCat = $rowWSL['name'];

    $nospecial = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $nameSubCat);
    $string = str_replace(' / ', '', $nospecial);
    $string2 = str_replace(' ', '_', $string);

    ?>
    <div class="wideSubCat">
        <a href="<?php echo $nomeSite . $menu;?>?cat=4&subcat=<?php echo $idSubCat;?>" title="<?php echo $nameSubCat;?> - Empresas <?php echo $nomeMunicipio; ?>">
            <div class="nomeSubCat">
                <h3>
                    <div class="nome"><?php echo $nameSubCat; ?></div>
                </h3>
            </div>

            <div class="image">
                <img src="<?php echo $baseurl . $imagensConstrucao . $idSubCat; ?>.jpg" alt="<?php echo $string2; ?>" title="<?php echo $nameSubCat; ?> - Empresas <?php echo $nomeMunicipio; ?>"/>
            </div>
        </a>
    </div>

<?php

endforeach;

echo $footerScripts;

?>