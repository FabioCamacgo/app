<?php

    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('contador');
    if ($resPluginEnabled === false) exit();

    echo 'app';

?>