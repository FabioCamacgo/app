<?php

    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('contador');
    if ($resPluginEnabled === false) exit();

    $totalEmpresas = VirtualDeskSiteEmpresasHelper::getTotal();
    $totalOndeFicar = VirtualDeskSiteEmpresasHelper::getTotalFicar();
    $totalOndeComer = VirtualDeskSiteEmpresasHelper::getTotalComer();
    $totalOndeVestir = VirtualDeskSiteEmpresasHelper::getTotalVestir();
    $totalConstrucao = VirtualDeskSiteEmpresasHelper::getTotalConstrucao();
    $totalAgricultura = VirtualDeskSiteEmpresasHelper::getTotalAgricultura();

?>



<div class="wide total">
    <div class="number"><?php echo count($totalEmpresas);?></div>
    <div class="text"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_TOTAL'); ?></div>
</div>

<div class="wide">
    <div class="number"><?php echo count($totalOndeFicar);?></div>
    <div class="text"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_ONDEFICAR'); ?></div>
</div>

<div class="wide">
    <div class="number"><?php echo count($totalOndeComer);?></div>
    <div class="text"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_ONDECOMER'); ?></div>
</div>

<div class="wide">
    <div class="number"><?php echo count($totalOndeVestir);?></div>
    <div class="text"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_ONDEVESTIR'); ?></div>
</div>

<div class="wide">
    <div class="number"><?php echo count($totalConstrucao);?></div>
    <div class="text"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_CONSTRUCAO'); ?></div>
</div>

<div class="wide">
    <div class="number"><?php echo count($totalAgricultura);?></div>
    <div class="text"><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_AGRICULTURA'); ?></div>
</div>

