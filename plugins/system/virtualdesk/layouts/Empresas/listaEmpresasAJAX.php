<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaEmpresas');
    if ($resPluginEnabled === false) exit();


    $link = $jinput->get('link','' ,'string');
    $ref = explode("?cat=", $link);

    if(strpos($ref[1], '&freg=') !== false){
        $getCat = explode("&subcat=", $ref[1]);
        $getSubCat = explode("&freg=", $getCat[1]);
        $getFreg = explode("&name=", $getSubCat[1]);
        $idCatLink = $getCat[0];
        $idSubCatLink = $getSubCat[0];
        $idFregLink = $getFreg[0];
        if(strpos($getFreg[1], '%20') !== false){
            $idNameLink = str_replace('%20', ' ', $getFreg[1]);
        } else {
            $idNameLink = $getFreg[1];
        }
    } else if (strpos($ref[1], '&subcat=') !== false) {
        $ref2 = explode("&subcat=", $ref[1]);

        $idCatLink = $ref2[0];
        $idSubCatLink = $ref2[1];
    } else {
        $idCatLink = $ref[1];
    }

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Empresas/listaEmpresas.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $concelhoEmpresas = $obParam->getParamsByTag('concelhoEmpresasGOV');

?>

    <div class="w70">
        <?php
        if (isset($_POST['submitForm'])) {
            $nomePost = $_POST['nomePost'];
            $cat = $_POST['categoria'];
            $subCcat = $_POST['vdmenumain'];
            $freg = $_POST['freguesia'];

            if(empty($nomePost) && empty($cat) && empty($subCcat) && empty($freg)){
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEmpresasALL.php');
            } else if($cat == 2){
                //header('Location: ' . $nomeSiteAssoc);
                ?>
                    <script>
                        <?php
                            echo "window.location.href = '$nomeSiteAssoc';";
                        ?>
                    </script>
                <?php
            } else{
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEmpresasFILTERED.php');
            }

        } else if($idCatLink !='') {
            if($idCatLink == 2){
                ?>
                    <script>
                        window.location.href = "<?php echo $nomeSiteAssoc;?>";
                    </script>
                <?php
            } else {
                if($idSubCatLink !=''){
                    $subCcat = $idSubCatLink;
                }
                if($idFregLink !=''){
                    $freg = $idFregLink;
                }
                if($idNameLink !=''){
                    $nomePost = $idNameLink;
                }
                $cat = $idCatLink;
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEmpresasFILTERED.php');
            }

        } else if($idFregLink !='') {
            if($idSubCatLink !=''){
                $subCcat = $idSubCatLink;
            }
            if($idCatLink !=''){
                $cat = $idCatLink;
            }
            if($idNameLink !=''){
                $nomePost = $idNameLink;
            }
            $freg = $idFregLink;
            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEmpresasFILTERED.php');

        } else if($idNameLink !='') {
            if($idSubCatLink !=''){
                $subCcat = $idSubCatLink;
            }
            if($idCatLink !=''){
                $cat = $idCatLink;
            }
            if($idFregLink !=''){
                $freg = $idFregLink;
            }
            $nomePost = $idNameLink;
            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEmpresasFILTERED.php');
        } else {
            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEmpresasALL.php');
        }
        ?>
    </div>



    <div class="w30">

        <form id="ProcuraEmpresa" action="" method="post" class="login-form" enctype="multipart/form-data" >

            <div class="total"><?php echo 'Nº de Empresas: <div>' . count($listaEmpresas) . '</div>';?></div>

            <!--   Categoria   -->
            <div class="form-group" id="cat">
                <div class="col-md-9">
                    <?php $categorias = VirtualDeskSiteEmpresasHelper::getCategorias()?>
                    <div class="input-group ">
                        <select name="categoria" value="<?php echo $cat; ?>" id="categoria" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                            <?php
                                if(empty($cat)){
                                    ?>
                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESA_CATEGORIA'); ?></option>
                                    <?php foreach($categorias as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['categoria']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $cat; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getCatName($cat) ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeCategoria = VirtualDeskSiteEmpresasHelper::excludeCategoria($cat)?>
                                    <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['categoria']; ?></option>
                                    <?php endforeach;
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <!--   SubCategoria   -->
            <?php
            // Carrega menusec se o id de menumain estiver definido.
            $ListaDeMenuMain = array();
            if(!empty($cat)) {
                if( (int) $cat > 0) $ListaDeMenuMain = VirtualDeskSiteEmpresasHelper::getSubCategoria($cat);
            }

            ?>
            <div id="blocoMenuMain" class="form-group">
                <div class="col-md-9">
                    <select name="vdmenumain" id="vdmenumain"
                        <?php
                        if(empty($cat)) {
                            echo 'disabled';
                        }
                        ?>
                            class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                        <?php
                            if(empty($subCcat)){
                                ?>
                                    <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EMPRESA_SUBCATEGORIA' ); ?></option>
                                    <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                                        <option value="<?php echo $rowMM['id']; ?>"
                                            <?php
                                            if(!empty($this->data->vdmenumain)) {
                                                if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                                            }
                                            ?>
                                        ><?php echo $rowMM['name']; ?></option>
                                    <?php endforeach; ?>
                                <?php
                            } else {
                                ?>
                                    <option value="<?php echo $subCcat; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getSubCatName($subCcat) ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeSubCategoria = VirtualDeskSiteEmpresasHelper::excludeSubCategoria($cat, $subCcat)?>
                                    <?php foreach($ExcludeSubCategoria as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['subcategoria']; ?></option>
                                    <?php endforeach;
                            }
                        ?>
                    </select>

                </div>
            </div>



            <!--   Freguesia   -->
            <div class="form-group" id="freg">
                <?php $Freguesias = VirtualDeskSiteEmpresasHelper::getFreguesia($concelhoEmpresas)?>
                <div class="col-md-9">
                    <div class="input-group ">
                        <select name="freguesia" value="<?php echo $freg; ?>" id="freguesia" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                            <?php
                            if(empty($freg)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FREGUESIA'); ?></option>
                                <?php foreach($Freguesias as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                    ><?php echo $rowWSL['freguesia']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $freg; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getFregName($freg) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php $ExcludeFreguesia = VirtualDeskSiteEmpresasHelper::excludeFreguesia('4', $freg)?>
                                <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                    ><?php echo $rowWSL['freguesia']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <!--   Pesquisa Livre  -->
            <div class="form-group" id="nomeSearch">
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_PESQUISALIVRE'); ?>"
                           name="nomePost" id="nomePost" value="<?php echo $nomePost; ?>"/>
                </div>
            </div>

            <div class="form-actions" method="post" style="display:none;">
                <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
            </div>


        </form>
    </div>


<?php

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
</script>

