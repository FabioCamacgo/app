<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheEmpresas');
    if ($resPluginEnabled === false) exit();


    //LOCAL SCRIPTS
    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Empresas/maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Empresas/edit_maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $baseurl = JUri::base();

    $obParam      = new VirtualDeskSiteParamsHelper();
    $imagensListaSubCats = $obParam->getParamsByTag('imagensListaSubCats');
    $imagensListaCats = $obParam->getParamsByTag('imagensListaCats');
    $imagensIcons = $obParam->getParamsByTag('imagensIcons');

    $link = $jinput->get('link','' ,'string');
    $ref = explode("=", $link);
    $nomeEmpresa = $ref[1];
    $info = explode("_", $ref[1]);
    $idEmpresa = $info[0];

    $infoEmpresa = VirtualDeskSiteEmpresasHelper::getInfoEmpresa($idEmpresa);

    foreach($infoEmpresa as $rowWSL) :
        $id = $rowWSL['id'];
        $referencia = $rowWSL['referencia'];
        $nome = $rowWSL['designacao'];
        $apresentacao = $rowWSL['apresentacao'];
        $categoria = $rowWSL['categoria'];
        $subcategoria = $rowWSL['subcategoria'];
        $morada = $rowWSL['morada'];
        $latitude = $rowWSL['latitude'];
        $longitude = $rowWSL['longitude'];
        $freguesia = $rowWSL['freguesia'];
        $telefone = $rowWSL['telefone'];
        $website = $rowWSL['website'];
        $facebook = $rowWSL['facebook'];
        $instagram = $rowWSL['instagram'];
        $imgEmpresa = VirtualDeskSiteEmpresasHelper::getImgEmpresa($referencia);
        $temImagem = 0;
        $idCat = VirtualDeskSiteEmpresasHelper::getIdCat($categoria);
        $name = str_replace(' ', '_', $designacao);
        $code = $id . '_' . $name;
        $coordenadas = $latitude . ',' . $longitude;

        ?>

            <div class="detalheEmpresa">

                <div class="w70">
                    <h2 class="title"><?php echo $nome;?></h2>
                </div>


                <div class="w70">

                    <div class="imagem">
                        <?php
                            if((int)$imgEmpresa == 0){
                                if($idCat == 4){
                                    $idSubCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);
                                    ?>
                                    <img src="<?php echo $baseurl . $imagensListaSubCats . $idSubCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $nome; ?>"/>
                                    <?php
                                } else if($idCat == 16 || $idCat == 5){   /*Categorias sem banner*/
                                    $idSubCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);
                                    if($idSubCat == 73 || $idSubCat == 75 || $idSubCat == 76 || $idSubCat == 79 || $idSubCat == 38 || $idSubCat == 39 || $idSubCat == 40 || $idSubCat == 41){
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats . $idSubCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    } else {
                                        ?>
                                        <img src="<?php echo $baseurl . $imagensListaSubCats; ?>banner.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    }

                                } else if($idCat == 3 || $idCat == 8 || $idCat == 11 || $idCat == 12 || $idCat == 13 || $idCat == 14 || $idCat == 15){
                                    $idSubCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);
                                    if($idSubCat == 16 || $idSubCat == 22 || $idSubCat == 23 || $idSubCat == 26 || $idSubCat == 47 || $idSubCat == 48 || $idSubCat == 49 || $idSubCat == 50 || $idSubCat == 55 || $idSubCat == 57 || $idSubCat == 58 || $idSubCat == 60 || $idSubCat == 61 || $idSubCat == 63 || $idSubCat == 64 || $idSubCat == 67 || $idSubCat == 72){
                                        ?>
                                            <img src="<?php echo $baseurl . $imagensListaSubCats . $idSubCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    } else {
                                        ?>
                                            <img src="<?php echo $baseurl . $imagensListaCats . $idCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <img src="<?php echo $baseurl . $imagensListaCats . $idCat; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $nome; ?>"/>
                                    <?php
                                }
                            } else {
                                $objEventFile = new VirtualDeskSiteEmpresasFilesHelper();
                                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                $FileList21Html = '';
                                foreach ($arFileList as $rowFile) {
                                    if($temImagem == 0){
                                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                        ?>
                                        <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $code; ?>" title="<?php echo $nome; ?>">
                                        <?php
                                        $temImagem = 1;
                                    }
                                }
                            }
                        ?>
                    </div>

                    <div class="descritivo">
                        <h3>Apresentação da empresa</h3>


                            <?php
                                $idSUBCat = VirtualDeskSiteEmpresasHelper::getIdSubCat($idCat, $subcategoria);

                                if($idSUBCat == 80 || $idSUBCat == 82 || $idSUBCat == 83 || $idSUBCat == 84 || $idSUBCat == 85 || $idSUBCat == 86 || $idSUBCat == 87){
                                    ?>
                                        <div class="alojLocal">
                                            <?php
                                                $detalheGeral = VirtualDeskSiteEmpresasHelper::getInfoGeral($id);
                                                foreach ($detalheGeral as $rowDetail) {
                                                    $nRegisto = $rowDetail['nRegisto_Alojamento'];
                                                    $capacidade = $rowDetail['capacidade_Alojamento'];
                                                    $unidades = $rowDetail['unidades_Alojamento'];
                                                }
                                            ?>
                                            <div class="w33">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconNRegisto" title="Número de Registo"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $nRegisto; ?>
                                                </div>
                                            </div>

                                            <div class="w33">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconCapacidade" title="Capacidade de Alojamento"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $capacidade; ?>
                                                </div>
                                            </div>

                                            <div class="w33">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconUnidades" title="Unidades de Alojamento"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $unidades; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                } else if ($idSUBCat == 81){
                                    ?>
                                        <div class="alojLocal">
                                            <?php
                                                $detalheAL = VirtualDeskSiteEmpresasHelper::getInfoAL($id);
                                                foreach ($detalheAL as $rowDetail2) {
                                                    $nRegisto = $rowDetail2['nRegisto_Alojamento'];
                                                    $tipo = $rowDetail2['tipologia'];
                                                    $nQuartos = $rowDetail2['nQuartos_Alojamento'];
                                                    $nPessoas = $rowDetail2['nUtentes_Alojamento'];
                                                    $nCamas = $rowDetail2['nCamas_Alojamento'];
                                                }

                                            ?>
                                            <div class="w20">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconNRegisto" title="Número de Registo"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $nRegisto; ?>
                                                </div>
                                            </div>

                                            <div class="w20">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconTipologia" title="Tipologia"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $tipo; ?>
                                                </div>
                                            </div>

                                            <div class="w20">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconNQuartos" title="Número de Quartos"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $nQuartos; ?>
                                                </div>
                                            </div>

                                            <div class="w20">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconNPessoas" title="Número de Pessoas"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $nPessoas; ?>
                                                </div>
                                            </div>

                                            <div class="w20">
                                                <div class="icon">
                                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconNCamas" title="Número de Camas"/>
                                                </div>
                                                <div class="text">
                                                    <?php echo $nCamas; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }

                            $lines = explode(". ", $apresentacao); // or use PHP PHP_EOL constant
                            if ( !empty($lines) ) {
                                for ($i = 0; $i < count($lines); ++$i) {
                                    $newstring = substr(strtolower($lines[$i]), -3);
                                    $newstring2 = substr(strtolower($lines[$i]), -2);
                                    if($newstring != 'dra' && $newstring != 'sra' && $newstring != 'drª' && $newstring != 'srª' && $newstring2 != 'sr' && $newstring2 != 'dr' && $newstring2 != 'av' && $newstring2 != 'nº' && $newstring2 != 'bl' && $newstring2 != 'dt' && $newstring != 'esq' && $newstring != 'soc'){
                                        echo '<p>'. trim( $lines[$i] ) .'.</p>';
                                    } else {
                                        echo '<p>'. trim( $lines[$i] ) .'. ' . trim( $lines[$i + 1] ) . '.</p>';
                                        $i = $i + 1;
                                    }
                                }
                            }
                        ?></div>
                </div>

                <div class="w30">

                    <div class="info">
                        <div class="block1">
                            <div class="item">
                                <div class="icon">
                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Categoria.png" alt="iconCategoria" title="Categoria"/>
                                </div>
                                <div class="text">
                                    <?php echo $categoria; ?>
                                </div>
                            </div>
                            <div class="item">
                                <div class="icon">
                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Subcategoria.png" alt="iconSubcategoria" title="Subcategoria"/>
                                </div>
                                <div class="text">
                                    <?php echo $subcategoria; ?>
                                </div>
                            </div>
                            <div class="item">
                                <div class="icon">
                                    <img src="<?php echo $baseurl . $imagensIcons; ?>Freguesia.png" alt="iconFreguesia" title="Freguesia"/>
                                </div>
                                <div class="text">
                                    <?php echo $freguesia; ?>
                                </div>
                            </div>
                        </div>

                        <div class="block2">

                            <?php
                            if(empty($morada)){
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Morada.png" alt="iconMorada" title="Morada"/>
                                        </div>
                                        <div class="text">
                                            -
                                        </div>
                                    </div>
                                <?php
                            } else {
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Morada.png" alt="iconMorada" title="Morada"/>
                                        </div>
                                        <div class="text">
                                            <?php echo $morada; ?>
                                        </div>
                                    </div>
                                <?php
                            }
                            ?>

                            <?php
                            if(empty($telefone) || $telefone == 0){
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Contacto.png" alt="iconContacto" title="Contacto"/>
                                        </div>
                                        <div class="text">
                                            -
                                        </div>
                                    </div>
                                <?php
                            } else {
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Contacto.png" alt="iconContacto" title="Contacto"/>
                                        </div>
                                        <div class="text">
                                            <?php echo $telefone; ?>
                                        </div>
                                    </div>
                                <?php
                            }
                            ?>

                        </div>

                        <div class="block3">
                            <?php
                            if(empty($website)){
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Web.png" alt="iconWebsite" title="Website"/>
                                        </div>
                                        <div class="text">
                                            -
                                        </div>
                                    </div>
                                <?php
                            } else {
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Web.png" alt="iconWebsite" title="Website"/>
                                        </div>
                                        <div class="text">
                                            <a href="<?php echo $website; ?>">Website</a>
                                        </div>
                                    </div>
                                <?php
                            }
                            ?>

                            <?php
                            if(empty($facebook)){
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Face.png" alt="iconFacebook" title="Facebook"/>
                                        </div>
                                        <div class="text">
                                            -
                                        </div>
                                    </div>
                                <?php
                            } else {
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Face.png" alt="iconFacebook" title="Facebook"/>
                                        </div>
                                        <div class="text">
                                            <a href="<?php echo $facebook; ?>">Facebook</a>
                                        </div>
                                    </div>
                                <?php
                            }
                            ?>

                            <?php
                            if(empty($instagram)){
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Insta.png" alt="iconInstagram" title="Instagram"/>
                                        </div>
                                        <div class="text">
                                            -
                                        </div>
                                    </div>
                                <?php
                            } else {
                                ?>
                                    <div class="item">
                                        <div class="icon">
                                            <img src="<?php echo $baseurl . $imagensIcons; ?>Insta.png" alt="iconInstagram" title="Instagram"/>
                                        </div>
                                        <div class="text">
                                            <a href="<?php echo $instagram; ?>">Instagram</a>
                                        </div>
                                    </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>


                    <div class="mapa">
                        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                        <?php
                            if(!empty($latitude) || !empty($longitude)){
                                ?>
                                    <input type="hidden" required class="form-control"
                                       name="coordenadas" id="coordenadas"
                                       value="<?php echo htmlentities($coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
                                <?php
                            }
                        ?>

                    </div>

                </div>

            </div>
        <?php
    endforeach;

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;
?>

<script>
    var iconpath = '<?php echo JUri::base() . 'plugins/system/virtualdesk/layouts/includes/Espera.png'; ?>';
</script>
