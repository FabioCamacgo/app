var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater').each(function(){
                //console.log('a')
                jQuery(this).repeater({

                    show: function () {

                        if(checkIfFileLimtiHasReached()===true)
                        {
                            jQuery('a.mt-repeater-add').hide();
                            var AlertMsg = jQuery('#FileListNumReachedAlert').clone();
                            jQuery('#FileListNumReachedAlertShow').html(AlertMsg);
                            jQuery('#FileListNumReachedAlertShow > div').show();
                            jQuery('#vd-filelist-repeater').find('div.mt-repeater-item.vd-filelist-repeater-item').last().remove();

                        }
                        else
                        {
                            jQuery(this).slideDown();
                        }
                    },

                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            jQuery(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });
        }

    };

}();


function checkIfFileLimtiHasReached()
{

    var NumFiles  = Number(jQuery('#vd-filelist').find('.vd-filelist-item').size());
    var NumRepeat = Number(jQuery('#vd-filelist-repeater').find('.vd-filelist-repeater-item').size());
    var SumNumFilesNumRepeat = Number(NumFiles+NumRepeat);

    console.log(param_contactus_filelimitnum +' - '+ NumFiles +' , '+ NumRepeat + ' - ' + SumNumFilesNumRepeat);

    if( SumNumFilesNumRepeat <= param_contactus_filelimitnum ){
        return(false)
    }
    return(true);
}


jQuery(document).ready(function(){


    // Select 2
    ComponentsSelect2.init();

    // Repeater
    FormRepeater.init();


    jQuery("#vdmenumain").change(function(){

        var subcategoria = jQuery(this).val();

        if(subcategoria == 81){
            jQuery('#capacidadeAlojamento').css('display','none');
            jQuery('#unidadesAlojamento').css('display','none');
            jQuery('#tipologiaAlojamento').css('display','inline-block');
            jQuery('#numeroQuartos').css('display','inline-block');
            jQuery('#numeroUtentes').css('display','inline-block');
            jQuery('#numeroCamas').css('display','inline-block');
        } else if(subcategoria == 80 || subcategoria == 82 || subcategoria == 83 || subcategoria == 84 || subcategoria == 86 || subcategoria == 85 || subcategoria == 87){
            jQuery('#capacidadeAlojamento').css('display','inline-block');
            jQuery('#unidadesAlojamento').css('display','inline-block');
            jQuery('#tipologiaAlojamento').css('display','none');
            jQuery('#numeroQuartos').css('display','none');
            jQuery('#numeroUtentes').css('display','none');
            jQuery('#numeroCamas').css('display','none');
        } else{
            jQuery('#capacidadeAlojamento').css('display','none');
            jQuery('#unidadesAlojamento').css('display','none');
            jQuery('#tipologiaAlojamento').css('display','none');
            jQuery('#numeroQuartos').css('display','none');
            jQuery('#numeroUtentes').css('display','none');
            jQuery('#numeroCamas').css('display','none');
        }

    });

    jQuery("#categoria").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */



        var pathArray = window.location.pathname.split('/');
        var secondLevelLocation = pathArray[1];



        var categoria = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(categoria == 1){
            jQuery('#Alojamento').css('display','block');
            jQuery('#capacidadeAlojamento').css('display','none');
            jQuery('#unidadesAlojamento').css('display','none');
            jQuery('#tipologiaAlojamento').css('display','none');
            jQuery('#numeroQuartos').css('display','none');
            jQuery('#numeroUtentes').css('display','none');
            jQuery('#numeroCamas').css('display','none');
        } else {
            jQuery('#Alojamento').css('display','none');
            jQuery('#capacidadeAlojamento').css('display','none');
            jQuery('#tipologiaAlojamento').css('display','none');
            jQuery('#unidadesAlojamento').css('display','none');
            jQuery('#numeroQuartos').css('display','none');
            jQuery('#numeroUtentes').css('display','none');
            jQuery('#numeroCamas').css('display','none');
        }
        var dataString = "categoria="+categoria; /* STORE THAT TO A DATA STRING */

        // show loader
        App.blockUI({ target:'#blocoMenuMain',  animate: true});

        jQuery.ajax({
            url: websitepath,
            data:'m=empresas_getcat&categoria=' + categoria ,

            success: function(output) {

                var select = jQuery('#vdmenumain');
                select.find('option').remove();

                if (output == null || output=='') {
                    select.prop('disabled', true);
                }
                else {
                    select.prop('disabled', false);
                    select.append(jQuery('<option>'));
                    jQuery.each(JSON.parse(output), function (i, obj) {
                        //console.log('i=' + i);
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                    });
                }

                // hide loader
                App.unblockUI('#blocoMenuMain');

            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status +  ' ' + thrownError);
                // hide loader
                select.find('option').remove();
                select.prop('disabled', true);

                App.unblockUI('#blocoMenuMain');
            }
        });

    });


// FileUpLoad
    jQuery("#fileupload").fileinput({
        theme: 'fa',
        language: fileLangSufixV2,
        showPreview: true,
        allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        elErrorContainer: '#errorBlock',
        uploadUrl : setVDCurrentRelativePath , // definido no php
        maxFileCount: 3,
        uploadAsync: true,
        showUpload: false, // hide upload button
        showBrowse: true,
        showRemove: false,
        showCaption: false, //
        browseOnZoneClick: true,
        retryErrorUploads : false,
        /*uploadExtraData: {
             isVDAjaxReqFileUpload: "1"
        },*/

        // Enviar referência do projecto para associar os ficheiros
        uploadExtraData:  function() {  // callback example
            var out = {}, key, i = 0;

            out['isVDAjaxReqFileUpload'] = isVDAjaxReqFileUpload;
            out['VDAjaxReqProcRefId'] = jQuery("#VDAjaxReqProcRefId").val();
            /*$('.kv-input:visible').each(function() {
                $el = $(this);
                key = $el.hasClass('kv-new') ? 'new_' + i : 'init_' + i;
                out[key] = $el.val();
                i++;
            });*/
            return out;},

        fileActionSettings: {
            showRemove: true,
            showUpload: false,
            showZoom: true,
            showDrag: false,
            showDownload: false,
            uploadRetryIcon: false,
            indicatorNew: '',
            indicatorNewTitle: ''
        }
    });

    /*jQuery("#btPost").on("click", function(event) {
        //jQuery('#fileupload').fileinput('upload');
    });*/


    jQuery('#fileupload').on('fileuploaderror', function(event, data, msg) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File upload error');
        // get message
        alert(msg);
    });

    jQuery('#fileupload').on('filebatchuploadsuccess', function(event, data) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File batch upload success');
    });

    jQuery('#fileupload').on('filebatchuploaderror', function(event, data, msg) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File batch upload error');
        // get message
        alert(msg);
    });


    jQuery('#fileupload').on('filebatchuploadcomplete', function(event, files, extra) {
        console.log('File batch upload complete');
    });

    var isVDAjaxReqFileUpload = '1';
    var VDAjaxReqProcRefId = 0;


    jQuery("#btPostFile").on("click", function(event) {

        jQuery.ajax({
            type: 'POST',
            url: setVDCurrentRelativePath,
            data: {  isVDAjaxReqTESTEPost: 1 },
            success: function(response) {
                console.log(' response ok...');
                var data = response;

                var nFirstPos = data.search("__i__");
                var nLastPos  = data.search("__e__");

                var RefOccrrentToSend = data.substring(nFirstPos+5,nLastPos);

                // debugger;
                // Se correu bem a gravação da ocorrência... vai tentar enviar os ficheiros

                jQuery('html, body').animate({
                    scrollTop: jQuery("#uploadField").offset().top
                }, 2000);

                // Só envia ficheiros se tiver algum seleccionado
                var files = jQuery('#fileupload').fileinput('getFileStack'); // returns file list selected
                var files2 = jQuery('#fileupload').val(); // returns file list selected

                if(files.length>0 && files2!='') {
                    var d = new Date();
                    VDAjaxReqProcRefId = RefOccrrentToSend; // d.getTime();  // Para teste

                    jQuery('#fileupload').fileinput('upload');

                    //ToDo: Tratar Erros de gravação dos ficheiros com on('fileuploaderror'  ... .on('filebatchuploaderror'

                }

            },
            error: function(xhr, status, error) {

                //ToDo: Tratar Erros de gravação da ocorrência

                //debugger;
                //var err = eval("(" + xhr.responseText + ")");
                var err = 'Erro ao Gravar:' + error;
                alert(err);
            }

        });
    });


});