<?php


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroEuropa');
    if ($resPluginEnabled === false) exit();


    $link = $jinput->get('link','' ,'string');

    $ref = explode("?item=", $link);
    $ref1 = explode("&prog=", $ref[1]);
    $ref2 = explode("&finan=", $ref1[1]);
    $ref3 = explode("&ano=", $ref2[1]);

    $getItem = base64_decode($ref1[0]);

    $item = str_replace('%20', ' ', $getItem);

    $progFiltro = $ref2[0];
    $finan = $ref3[0];
    $anoFiltro = $ref3[1];


    // POR AJAX para o EXTERIOR as scripts JS têm de ser carregadas no módulo de origem
    // é um problema a resolver

    //LOCAL SCRIPTS
    //$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    //$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    //$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' . $addscript_end;
    //$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    //$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js?v2' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/EuropaCriativa/filtroEuropa.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    //$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/EuropaCriativa/filtroEuropa.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $linkDetalhe = $obParam->getParamsByTag('linkDetalheProgramas');

?>

<div class="lista">
    <?php
        if (isset($_POST['submitForm'])) {

            $pesquisaLivre = $_POST['pesquisaLivre'];
            $prog = $_POST['programa'];
            $categoria = $_POST['categoria'];
            $linhaFinanciamento = $_POST['linhaFinanciamento'];
            $ano = $_POST['ano'];
            $dataInicio = $_POST['dataInicio'];
            $dataFim = $_POST['dataFim'];
            $tipologiaID = $_POST['tipologia'];

            if($prog == '2'){
                ?>
                    <script>
                        jQuery('#cat').css('display','block');
                        jQuery('#tip').css('display','none');
                    </script>
                <?php
            } else if($prog == '1'){
                ?>
                    <script>
                        jQuery('#cat').css('display','none');
                        jQuery('#tip').css('display','block');
                    </script>
                <?php
            } else {
                ?>
                <script>
                        jQuery('#cat').css('display','none');
                        jQuery('#tip').css('display','none');
                    </script>
                <?php

                $linhaFinanciamento = '';
                $categoria = '';
            }

            if(empty($pesquisaLivre) && empty($prog) && empty($categoria) && empty($ano) && empty($dataInicio) && empty($dataFim) && empty($tipologiaID)){
                require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/listaProjetosALL.php');
            } else {
                require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/listaProjetosFiltered.php');
            }

        } else if($item !='' || $progFiltro != '' || $finan != '' || $anoFiltro != ''){
            if($item !=''){
                $pesquisaLivre = $item;
            }

            if($progFiltro !=''){
                $prog = $progFiltro;
            }

            if($finan !=''){
                $linhaFinanciamento = $finan;
            }

            if($anoFiltro !=''){
                $ano = $anoFiltro;
            }

            if($prog == '2'){
                    ?>
                <script>
                    jQuery('#cat').css('display','block');
                    jQuery('#tip').css('display','none');
                </script>
            <?php
            } else if($prog == '1'){
            ?>
                <script>
                    jQuery('#cat').css('display','none');
                    jQuery('#tip').css('display','block');
                </script>
            <?php
            } else {
            ?>
                <script>
                    jQuery('#cat').css('display','none');
                    jQuery('#tip').css('display','none');
                </script>
                <?php
            }

            require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/listaProjetosFiltered.php');

        } else {
            require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/listaProjetosALL.php');
        }
    ?>
</div>


<div class="filtro">

    <div class="title">
        <?php echo JText::_('COM_VIRTUALDESK_EUROPA_FILTRO'); ?>
    </div>

    <span class="cleanFilter"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CLEANFILTER'); ?></span>

    <form id="projetosApoiados" action="" method="post" class="login-form" enctype="multipart/form-data" >

        <!--   Pesquisa Livre  -->
        <div class="form-group" id="nomeSearch">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PESQUISALIVRE'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_PESQUISALIVRE'); ?>"
                       name="pesquisaLivre" id="pesquisaLivre" value="<?php echo $pesquisaLivre; ?>"/>
            </div>
        </div>


        <!--   Programa   -->
        <div class="form-group" id="prog">
            <?php $Programa = VirtualDeskSiteFiltroHelper::getPrograma()?>
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PROGRAMA'); ?></label>
            <div class="col-md-9">
                <select name="programa" value="<?php echo $prog; ?>" id="programa" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($prog)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PROGRAMA'); ?></option>
                        <?php foreach($Programa as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['programa']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $prog; ?>"><?php echo VirtualDeskSiteFiltroHelper::getProgramaName($prog) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludePrograma = VirtualDeskSiteFiltroHelper::excludePrograma($prog)?>
                        <?php foreach($ExcludePrograma as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['programa']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Categoria -->
        <div class="form-group" id="cat" style="display:none;">
            <?php $Categoria = VirtualDeskSiteFiltroHelper::getCategoria()?>
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CATEGORIA'); ?></label>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($categoria)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CATEGORIA'); ?></option>
                            <?php foreach($Categoria as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteFiltroHelper::getCategoriaName($categoria) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeCategoria = VirtualDeskSiteFiltroHelper::excludeCategoria($categoria)?>
                            <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>





        <!--   Linhas Financiamento   -->
        <?php
        // Carrega menusec se o id de menumain estiver definido.
        $ListaFinanciamento = array();
        if(!empty($prog)) {
            if( (int) $prog > 0) $ListaFinanciamento = VirtualDeskSiteFiltroHelper::getLinhasFinanciamento($prog);
        }

        ?>
        <div id="blocoLinhasFinanciamento" class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO'); ?></label>
            <div class="col-md-9">
                <select name="linhaFinanciamento" id="linhaFinanciamento" value="<?php echo $linhaFinanciamento; ?>"
                    <?php
                    if(empty($prog)) {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($linhaFinanciamento)){
                        ?>
                        <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO' ); ?></option>
                        <?php foreach($ListaFinanciamento as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                                <?php
                                if(!empty($this->data->linhaFinanciamento)) {
                                    if($this->data->linhaFinanciamento == $rowMM['id']) echo 'selected';
                                }
                                ?>
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach; ?>
                        <?php
                    } else {
                        ?>
                        <option value="<?php echo $linhaFinanciamento; ?>"><?php echo VirtualDeskSiteFiltroHelper::getLinhasFinanciamentoName($linhaFinanciamento) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeLinhasFinanciamento = VirtualDeskSiteFiltroHelper::excludeLinhasFinanciamento($prog, $linhaFinanciamento)?>
                        <?php foreach($ExcludeLinhasFinanciamento as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['linha']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>

            </div>
        </div>


        <!--   Tipologia   -->
        <div class="form-group" id="tip" style="display:none;">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA'); ?></label>
            <?php $Tipologia = VirtualDeskSiteFiltroHelper::getTipologia()?>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="tipologia" value="<?php echo $tipologiaID; ?>" id="tipologia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($tipologiaID)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA'); ?></option>
                            <?php foreach($Tipologia as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['tipologia']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $tipologiaID; ?>"><?php echo VirtualDeskSiteFiltroHelper::getTipologiaName($tipologiaID) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeTipologia = VirtualDeskSiteFiltroHelper::excludeTipologia($tipologiaID)?>
                            <?php foreach($ExcludeTipologia as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['tipologia']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <!--   Ano   -->
        <div class="form-group" id="year">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ANO'); ?></label>
            <?php $AnoProjeto = VirtualDeskSiteFiltroHelper::getAno()?>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($ano)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ANO'); ?></option>
                            <?php foreach($AnoProjeto as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['ano']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $ano; ?>"><?php echo VirtualDeskSiteFiltroHelper::getAnoName($ano) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeAno = VirtualDeskSiteFiltroHelper::excludeAno($ano)?>
                            <?php foreach($ExcludeAno as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['ano']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>


        <!--   Distrito   -->
        <!--<div class="form-group" id="district">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DISTRITOS'); ?></label>
            <?php $Distritos = VirtualDeskSiteFiltroHelper::getDistritos()?>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="distrito" value="<?php echo $distPost; ?>" id="distrito" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($distPost)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DISTRITOS'); ?></option>
                            <?php foreach($Distritos as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['distrito']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $distPost; ?>"><?php echo VirtualDeskSiteFiltroHelper::getDistritosName($distPost) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeDistritos = VirtualDeskSiteFiltroHelper::excludeDistritos($distPost)?>
                            <?php foreach($ExcludeDistritos as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['distrito']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>-->





        <!--   Concelho   -->
        <!--<?php

        $ListaDeMenuMain = array();
        if(!empty($distPost)) {
            if( (int) $distPost > 0) $ListaDeMenuMain = VirtualDeskSiteFiltroHelper::getConcelhos($distPost);
        }

        ?>
        <div id="blocoMenuMain" class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CONCELHO'); ?></label>
            <div class="col-md-9">
                <select name="vdmenumain" id="vdmenumain" value="<?php echo $concelhos; ?>"
                    <?php
                    if(empty($distPost)) {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($concelhos)){
                        ?>
                        <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EUROPA_CONCELHO' ); ?></option>
                        <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                                <?php
                                if(!empty($this->data->vdmenumain)) {
                                    if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                                }
                                ?>
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach; ?>
                        <?php
                    } else {
                        ?>
                        <option value="<?php echo $concelhos; ?>"><?php echo VirtualDeskSiteFiltroHelper::getConcelhoName($concelhos) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeConcelho = VirtualDeskSiteFiltroHelper::excludeConcelho($distPost, $concelhos)?>
                        <?php foreach($ExcludeConcelho as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['concelho']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>

            </div>
        </div>-->


        <!--   Data de inicio  -->
        <div class="form-group" id="projStart">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAINICIO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" placeholder="dd-mm-aaaa" name="dataInicio" id="dataInicio" value="<?php echo $dataInicio; ?>">
                    <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>


        <!--   Data de fim  -->
        <div class="form-group" id="projEnd">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAFIM'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" placeholder="dd-mm-aaaa" name="dataFim" id="dataFim" value="<?php echo $dataFim; ?>">
                    <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>


        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
        </div>

    </form>
</div>


<!--<div id="showFilter">
    <svg height="32px" style="enable-background:new 0 0 32 32;" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" ><g transform="translate(576 192)"><path d="M-544.88-165.121l-7.342-7.342c-1.095,1.701-2.541,3.148-4.242,4.242l7.343,7.342c1.172,1.172,3.071,1.172,4.241,0   C-543.707-162.048-543.707-163.947-544.88-165.121z"/><path d="M-552-180c0-6.627-5.373-12-12-12s-12,5.373-12,12s5.373,12,12,12S-552-173.373-552-180z M-564-171c-4.964,0-9-4.036-9-9   c0-4.963,4.036-9,9-9c4.963,0,9,4.037,9,9C-555-175.036-559.037-171-564-171z"/><path d="M-571-180h2c0-2.757,2.242-5,5-5v-2C-567.86-187-571-183.858-571-180z"/></g></svg>
</div>-->

<div id="showFilter">
    <div id="nav-icon3">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

<div id="hideFilter">
    <svg enable-background="new 0 0 32 32" height="32px" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" ><path d="M17.459,16.014l8.239-8.194c0.395-0.391,0.395-1.024,0-1.414c-0.394-0.391-1.034-0.391-1.428,0  l-8.232,8.187L7.73,6.284c-0.394-0.395-1.034-0.395-1.428,0c-0.394,0.396-0.394,1.037,0,1.432l8.302,8.303l-8.332,8.286  c-0.394,0.391-0.394,1.024,0,1.414c0.394,0.391,1.034,0.391,1.428,0l8.325-8.279l8.275,8.276c0.394,0.395,1.034,0.395,1.428,0  c0.394-0.396,0.394-1.037,0-1.432L17.459,16.014z" fill="#121313" id="Close"/><g/><g/><g/><g/><g/><g/></svg>
</div>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/filtroEuropa.js.php');
    ?>
</script>