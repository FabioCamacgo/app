<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();

    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroEuropa');
    if ($resPluginEnabled === false) exit();


    $listaProjetos = VirtualDeskSiteFiltroHelper::getProjetosFilter();

    ?>
        <div class="resultados">
            <h2><?php echo 'Foram encontrados <span>' . count($listaProjetos) . '</span> projetos apoiados.';?></h2>
        </div>
    <?php
    foreach($listaProjetos as $rowWSL) :
        $id = $rowWSL['id'];
        $ref = $rowWSL['referencia'];
        $projeto = $rowWSL['projeto'];
        $designacao = $rowWSL['designacao'];
        $programa = $rowWSL['programa'];
        $entidade = $rowWSL['entidade'];
        $lider = $rowWSL['lider'];
        $pais_lider = $rowWSL['pais_lider'];
        $ano_Apoio = $rowWSL['ano_Apoio'];
        $linha_Financiamento = $rowWSL['linha_Financiamento'];
        $tipologia = $rowWSL['tipologia'];
        $link = $rowWSL['link'];
        $data_Inicio = $rowWSL['data_Inicio'];
        $data_Fim = $rowWSL['data_Fim'];
        $call = $rowWSL['call'];

        $idProg = VirtualDeskSiteFiltroHelper::getIDProg($programa);

        if($idProg == 1) {
            $splittipologia = explode(",", $tipologia);
            $class = 'cultura';

            if(count($splittipologia) == 1){
                if($splittipologia[0] == 'NULL'){
                    $textTipologia = JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                } else {
                    $textTipologia = VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[0]);
                }
            } else {
                for($i=0; $i < count($splittipologia); $i = $i + 1) {
                    if($i == 0){
                        $textTipologia = VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[0]);
                    } else {
                        $textTipologia .= ', ' . VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[$i]);
                    }
                }
            }

            $corHover = $obParam->getParamsByTag('corFundoBlocosCultura');
        } else if($idProg == 2){
            $corHover = $obParam->getParamsByTag('corFundoBlocosMedia');
            $class = 'media';
        }

        ?>

            <div class="projeto <?php echo $class;?>">
                <div class="titulo">
                    <a href="<?php echo $linkDetalhe;?>?ref=<?php echo base64_encode($ref); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_SAIBA_MAIS');?>">
                        <h3>
                            <?php echo $projeto; ?>
                        </h3>
                    </a>
                </div>


                <div class="content">
                    <div class="programa item">
                        <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PROGRAMA');?></h3>
                        <p>
                            <?php
                            if(empty($programa) || $programa == ''){
                                echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                            } else {
                                echo $programa;
                            }
                            ?>
                        </p>
                    </div>

                    <div class="linha item">
                        <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO');?></h3>
                        <p>
                            <?php
                            if(empty($linha_Financiamento) || $linha_Financiamento == ''){
                                echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                            } else {
                                echo $linha_Financiamento;
                            }
                            ?>
                        </p>
                    </div>

                    <div class="item">
                        <?php

                            if($idProg == 1){
                                ?>
                                    <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA');?></h3>
                                    <p>
                                        <?php
                                        if(empty($tipologia) || $tipologia == ''){
                                            echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                        } else {
                                            echo $textTipologia;
                                        }
                                        ?>
                                    </p>
                                <?php
                            } else if($idProg == 2){
                                $idcat = VirtualDeskSiteFiltroHelper::getIDcat($linha_Financiamento);
                                $catApoio = VirtualDeskSiteFiltroHelper::getCatApoio($idcat);

                                ?>
                                    <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CATEGORIA');?></h3>
                                    <p>
                                        <?php
                                        if(empty($catApoio) || $catApoio == ''){
                                            echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                        } else {
                                            echo $catApoio;
                                        }
                                        ?>
                                    </p>
                                <?php
                            }
                        ?>
                    </div>


                    <div class="ano item">
                        <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ANO');?></h3>
                        <p>
                            <?php
                                if(empty($ano_Apoio) || $ano_Apoio == ''){
                                    echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                } else {
                                    echo $ano_Apoio;
                                }
                            ?>
                        </p>
                    </div>


                    <?php
                    if($idProg == 1) {
                        ?>
                            <div class="entidade item">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER');?></h3>
                                <p>
                                    <?php
                                    if(empty($lider) || $lider == '' || $lider == 'NULL'){
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else {
                                        echo $lider;
                                    }
                                    ?>
                                </p>
                            </div>



                            <div class="data item">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DATAPROJETO');?></h3>
                                <p>
                                    <?php


                                    if(!empty($data_Inicio) || $data_Inicio != ''){
                                        $splitDataInicio = explode("-", $data_Inicio);
                                        $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];
                                    }

                                    if(!empty($data_Fim) || $data_Fim != ''){
                                        $splitDataFim = explode("-", $data_Fim);
                                        $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];
                                    }


                                    if($data_Inicio == '--' && $data_Fim == '--'){
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else if($data_Inicio == '--' && $data_Fim == 'Em curso'){
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else if($data_Inicio != '--' && $data_Fim == 'Em curso'){
                                        echo $DataInicioFormated . ' / ' . $data_Fim;
                                    } else {
                                        echo $DataInicioFormated . ' / ' . $DataFimFormated;
                                    }
                                    ?>
                                </p>
                            </div>
                        <?php
                        }
                    ?>

                    <a href="<?php echo $linkDetalhe;?>?ref=<?php echo base64_encode($ref); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_SAIBA_MAIS');?>">
                        <div class="sabermais">
                            <button class="botaoSaberMais" style="background:<?php echo $corHover;?>; border-color:<?php echo $corHover;?>;"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_SABERMAIS'); ?></button>
                        </div>
                    </a>

                </div>
            </div>
        <?php
    endforeach;
?>