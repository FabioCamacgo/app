<?php
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();

    $imgPrograma = VirtualDeskSiteDetalheHelper::getImgPrograma($referencia);

    $temImagem2 = 0;

    $idProg = VirtualDeskSiteDetalheHelper::getIDProg($programa);

?>

<meta property="og:url"                content="" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="<?php echo $projeto;?>" />
<meta property="og:description"        content="<?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($designacao);?> " />

<?php
    if(count($imgPrograma) == 0){

        $imgCultura = $obParam->getParamsByTag('ImagemCulturaHorizontal');
        $imgMedia = $obParam->getParamsByTag('ImagemMediaHorizontal');


        if($idProg == 1){
            $LinkParaAImagem = JUri::base() . 'images/EuropaCriativa/cultura_horizontal.jpg';
        } else if($idProg == 2){
            $LinkParaAImagem = JUri::base() . 'images/EuropaCriativa/media_horizontal.jpg';
        }
    } else {
        $objEventFile = new VirtualDeskSiteFormularioFilesHelper();
        $arFileList = $objEventFile->getFileGuestLinkByRefIdDETALHE($referencia);
        $FileList21Html = '';
        foreach ($arFileList as $rowFile) {
            if($temImagem2 == 0){
                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                $LinkParaAImagem = $rowFile->guestlink;
                $temImagem2 = 1;
            }

        }
    }
?>

<meta property="og:image" content="<?php echo $LinkParaAImagem; ?>" >


</head>

<body>
    <div class="vertical">

        <!--<div class="w65">
            <div class="imagemPrograma">
                <?php
                if(count($imgPrograma) == 0){

                    $imgCultura = $obParam->getParamsByTag('ImagemCulturaHorizontal');
                    $imgMedia = $obParam->getParamsByTag('ImagemMediaHorizontal');
                    echo 'aaaaa' . $idProg;

                    if($idProg == 1){
                        ?>
                        <img src="<?php echo JUri::base();?>images/EuropaCriativa/<?php echo $imgCultura;?>" alt="imgDetalhe" title="<?php echo $projeto; ?>"/>
                        <?php
                    } else if($idProg == 2){
                        ?>
                        <img src="<?php echo JUri::base();?>images/EuropaCriativa/<?php echo $imgMedia;?>" alt="imgDetalhe" title="<?php echo $projeto; ?>"/>
                        <?php
                    }
                } else {
                    $objEventFile = new VirtualDeskSiteFormularioFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefIdDETALHE($referencia);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        if($temImagem2 == 0){
                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                            ?>
                            <img src="<?php echo $rowFile->guestlink; ?>" alt="imgDetalhe" title="<?php echo $projeto; ?>">
                            <?php
                            $temImagem2 = 1;
                        }
                    }
                }
                ?>
            </div>

        </div>


        <div class="w65">
            <div class="descricao">
                <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($designacao); ?>
            </div>
        </div>-->


    </div>

    <script type="text/javascript">
        window.location.href = "<?php echo $obParam->getParamsByTag('EuropaCriativaLinkSiteDiretorio') ?>/detalhe?ref=<?php echo base64_encode($referencia);?>";
    </script>
</body>


