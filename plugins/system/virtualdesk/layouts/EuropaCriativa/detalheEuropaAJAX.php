<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheEuropa');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;
    //$headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/EuropaCriativa/detalheEuropa.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $link = $jinput->get('link','' ,'string');
    $ref = explode("?ref=", $link);

    $referencia = base64_decode($ref[1]);

    $listaInfo = VirtualDeskSiteDetalheHelper::getInfoProjeto($referencia);

    foreach($listaInfo as $rowWSL) :
        $projeto = $rowWSL['projeto'];
        $designacao = $rowWSL['designacao'];
        $programa = $rowWSL['programa'];
        $entidade = $rowWSL['entidade'];
        $lider = $rowWSL['lider'];
        $pais_lider = $rowWSL['pais_lider'];
        $ano_Apoio = $rowWSL['ano_Apoio'];
        $linha_Financiamento = $rowWSL['linha_Financiamento'];
        $distrito = $rowWSL['distrito'];
        $concelho = $rowWSL['concelho'];
        $tipologia = $rowWSL['tipologia'];
        $link = $rowWSL['link'];
        $data_Inicio = $rowWSL['data_Inicio'];
        $data_Fim = $rowWSL['data_Fim'];
        $call = $rowWSL['call'];
        $valorFinanciamento = $rowWSL['valorFinanciamento'];
        $layout = $rowWSL['layout'];


        if((int)$isVDAjax2Share===1) {
            require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/detalheShare.php');
        } else {

            ?>
                <div class="w65">
                    <h2 class="vdTituloDetalhe"><?php echo $projeto; ?></h2>
                </div>

                <div class="w35">

                </div>
            <?php

            if($layout == 1){
                require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/detalheEuropaVertical.php');
            } else if($layout == 2){
                require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/detalheEuropaHorizontal.php');
            }
        }



    endforeach;

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/detalheEuropa.js.php');
    ?>
</script>
