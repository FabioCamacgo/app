<?php
    defined('_JEXEC') or die;

?>

jQuery(document).ready(function(){
    jQuery(window).resize(function() {
        if (jQuery(window).width() < 959) {
            jQuery('.mobileVertical').css('display','block');
            jQuery('.normalVertical').css('display','none');
        }
        else {
            jQuery('.mobileVertical').css('display','none');
            jQuery('.normalVertical').css('display','block');
        }
    });

    if (jQuery(window).width() < 959) {
        jQuery('.mobileVertical').css('display','block');
        jQuery('.normalVertical').css('display','none');
    }
    else {
        jQuery('.mobileVertical').css('display','none');
        jQuery('.normalVertical').css('display','block');
    }
});
