<?php

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
$obParam              = new VirtualDeskSiteParamsHelper();
$arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroFPEuropa');
if ($resPluginEnabled === false) exit();


//LOCAL SCRIPTS
//$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

//GLOBAL SCRIPTS
//$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
//$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
//$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts = $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;


//BEGIN GLOBAL MANDATORY STYLES
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/EuropaCriativa/filtroFPEuropa.css' . $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';


echo $headCSS;

if (isset($_POST['submitForm'])){
    $it1 = $_POST['pesquisaLivre'];
    $it2 = $_POST['programa'];
    $it3 = $_POST['linhaFinanciamento'];
    $it4 = $_POST['ano'];

    ?>
    <script>
        window.location.href = "<?php echo $paginaFiltro;?>?item=<?php echo base64_encode($it1);?>&prog=<?php echo $it2;?>&finan=<?php echo $it3;?>&ano=<?php echo $it4;?>";
    </script>
    <?php

}

?>


<div class="filtro">
    <form id="projetosApoiados" action="" method="post" class="login-form" enctype="multipart/form-data" >

        <div class="visibleBlock">
            <div class="form-group" id="nomeSearch">
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_PESQUISALIVRE'); ?>"
                           name="pesquisaLivre" id="pesquisaLivre" value="<?php echo $pesquisaLivre; ?>"/>
                </div>
            </div>
        </div>


        <div class="invisibleBlock">

            <!--   Programa  -->
            <div class="form-group" id="prog">
                <?php $Programa = VirtualDeskSiteFiltroFPHelper::getPrograma()?>
                <div class="col-md-9">
                    <select name="programa" value="<?php echo $programa; ?>" id="programa" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($programa)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PROGRAMA'); ?></option>
                            <?php foreach($Programa as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['programa']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $programa; ?>"><?php echo VirtualDeskSiteFiltroFPHelper::getProgSelect($programa) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludePrograma = VirtualDeskSiteFiltroFPHelper::excludePrograma($programa)?>
                            <?php foreach($ExcludePrograma as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['programa']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>


            <!--   Linhas Financiamento   -->
            <?php
            // Carrega menusec se o id de menumain estiver definido.
            $ListaFinanciamento = array();
            if(!empty($programa)) {
                if( (int) $programa > 0) $ListaFinanciamento = VirtualDeskSiteFiltroFPHelper::getLinhasFinanciamento($programa);
            }

            ?>
            <div id="blocoLinhasFinanciamento" class="form-group">
                <div class="col-md-9">
                    <select name="linhaFinanciamento" id="linhaFinanciamento" value="<?php echo $linhaFinanciamento;?>"
                        <?php
                        if(empty($programa)) {
                            echo 'disabled';
                        }
                        ?>
                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($linhaFinanciamento)){
                            ?>
                            <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO' ); ?></option>
                            <?php foreach($ListaFinanciamento as $rowMM) : ?>
                                <option value="<?php echo $rowMM['id']; ?>"
                                    <?php
                                    if(!empty($this->data->linhaFinanciamento)) {
                                        if($this->data->linhaFinanciamento == $rowMM['id']) echo 'selected';
                                    }
                                    ?>
                                ><?php echo $rowMM['name']; ?></option>
                            <?php endforeach; ?>
                            <?php
                        } else {
                            ?>
                            <option value="<?php echo $linhaFinanciamento; ?>"><?php echo VirtualDeskSiteFiltroFPHelper::getLinhasFinanciamentoName($linhaFinanciamento) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeLinhasFinanciamento = VirtualDeskSiteFiltroFPHelper::excludeLinhasFinanciamento($programa, $linhaFinanciamento)?>
                            <?php foreach($ExcludeLinhasFinanciamento as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['name']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>

                </div>
            </div>


            <!--   Ano   -->
            <div class="form-group" id="year">
                <?php $AnoProjeto = VirtualDeskSiteFiltroFPHelper::getAno()?>
                <div class="col-md-9">
                    <div class="input-group ">
                        <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                            <?php
                            if(empty($ano)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ANO'); ?></option>
                                <?php foreach($AnoProjeto as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"
                                    ><?php echo $rowWSL['ano']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $ano; ?>"><?php echo VirtualDeskSiteFiltroFPHelper::getAnoName($ano) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php $ExcludeAno = VirtualDeskSiteFiltroFPHelper::excludeAno($ano)?>
                                <?php foreach($ExcludeAno as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"
                                    ><?php echo $rowWSL['ano']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="form-actions" method="post" style="display:none;">
                <input type="submit" name="submitForm" id="submitForm" value="Submeter Projeto">
            </div>

    </form>
</div>

<?php

echo $headScripts;
echo $footerScripts;
echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/filtroFPEuropa.js.php');
    ?>
</script>