<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $jinput = JFactory::getApplication()->input;

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    $isVDAjax2Share = $jinput->get('isVDAjax2Share');

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteDetalheHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/detalhe.php');
    JLoader::register('VirtualDeskSiteFormularioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/Formulario_files.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheEuropa');
    if ($resPluginEnabled === false) exit();

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';


    // EST  A RECEBER UM TESTE por AJAX ?

    $isVDAjaxReqTESTEPost= $jinput->get('isVDAjaxReqTESTEPost');
    if($isVDAjaxReqTESTEPost=="1") {
        $results = array( 'error' => '', 'data' => array()
        );

        echo json_encode($results);
        exit();
    }



    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;

    $jinput = JFactory::getApplication('site')->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag){
        case 'pt-PT':
            $fileLangSufix = 'pt_PT';
            break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    if((int)$isVDAjax2Share===1) echo('<html><head>');

    $obPlg      = new VirtualDeskSitePluginsHelper();

    if($isVDAjaxReq !=1){
        $layoutPath = $obPlg->getPluginLayoutAtivePath('detalheEuropa','form');
        require_once(JPATH_SITE . $layoutPath);
    } else {
        $layoutPath = $obPlg->getPluginLayoutAtivePath('detalheEuropa','formajax');
        require_once(JPATH_SITE . $layoutPath);
    }

    if((string)$layoutPath=='') {
        // Se não carregar o caminho do layout sai com erro...
        echo (JText::_('COM_VIRTUALDESK_LOADACTIVELAYOUT_ERROR'));
        exit();
    }
    require_once(JPATH_SITE . $layoutPath);


    if((int)$isVDAjax2Share===1) echo('</body></html>');
?>
