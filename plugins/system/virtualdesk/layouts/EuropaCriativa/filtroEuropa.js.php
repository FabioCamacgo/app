<?php
defined('_JEXEC') or die;
?>

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery.noConflict();
jQuery(document).ready(function($){

    // executes when complete page is fully loaded, including all frames, objects and images
    setTimeout(function(){

        jQuery('.date-picker1').datepicker({autoclose:true});
        jQuery('.date-picker1').on('hide', function (e) { e.preventDefault(); });
        jQuery('#dataFim').attr('readonly',false);
        jQuery('#dataInicio').attr('readonly',false);


        jQuery('#showFilter').click(function(){
            jQuery('.itemid-712 .lista').css('margin-left','325px');
            jQuery('.itemid-712 .lista').css('-webkit-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 .lista').css('-moz-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 .lista').css('-o-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 .lista').css('transition','all 0.25s ease-in-out');

            jQuery('.itemid-712 .filtro').css('left','0');
            jQuery('.itemid-712 .filtro').css('-webkit-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 .filtro').css('-moz-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 .filtro').css('-o-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 .filtro').css('transition','all 0.25s ease-in-out');

            jQuery('.itemid-712 #submit').css('left','0');
            jQuery('.itemid-712 #submit').css('-webkit-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 #submit').css('-moz-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 #submit').css('-o-transition','all 0.25s ease-in-out');
            jQuery('.itemid-712 #submit').css('transition','all 0.25s ease-in-out');

            jQuery('#showFilter').css('left','300px');
            jQuery('#showFilter').css('opacity','0');
            jQuery('#showFilter').css('z-index','0');
            jQuery('#showFilter').css('-webkit-transition','all 0.25s ease-in-out');
            jQuery('#showFilter').css('-moz-transition','all 0.25s ease-in-out');
            jQuery('#showFilter').css('-o-transition','all 0.25s ease-in-out');
            jQuery('#showFilter').css('transition','all 0.25s ease-in-out');

            jQuery('#hideFilter').css('left','300px');
            jQuery('#hideFilter').css('opacity','1');
            jQuery('#hideFilter').css('z-index','999');
            jQuery('#hideFilter').css('-webkit-transition','all 0.25s ease-in-out');
            jQuery('#hideFilter').css('-moz-transition','all 0.25s ease-in-out');
            jQuery('#hideFilter').css('-o-transition','all 0.25s ease-in-out');
            jQuery('#hideFilter').css('transition','all 0.25s ease-in-out');

        });

        if (jQuery(window).width() < 1200) {
            jQuery('.itemid-712 #submit').click(function(){
                jQuery('.itemid-712 .lista').css('margin-left','0');

                jQuery('.itemid-712 .filtro').css('left','-305px');

                jQuery('.itemid-712 #submit').css('left','-305px');

                jQuery('#showFilter').css('left','0');
                jQuery('#showFilter').css('opacity','1');
                jQuery('#showFilter').css('z-index','999');

                jQuery('#hideFilter').css('left','0');
                jQuery('#hideFilter').css('opacity','0');
                jQuery('#hideFilter').css('z-index','0');

            });
        }


        jQuery('.itemid-712 #hideFilter').click(function(){
            jQuery('.itemid-712 .lista').css('margin-left','0');

            jQuery('.itemid-712 .filtro').css('left','-305px');

            jQuery('.itemid-712 #submit').css('left','-305px');

            jQuery('#showFilter').css('left','0');
            jQuery('#showFilter').css('opacity','1');
            jQuery('#showFilter').css('z-index','999');

            jQuery('#hideFilter').css('left','0');
            jQuery('#hideFilter').css('opacity','0');
            jQuery('#hideFilter').css('z-index','0');

        });

        var auxiliar = jQuery("#programa").val(); /* GET THE VALUE OF THE SELECTED DATA */

        var auxBit = 0;

        if(auxiliar != '') {
            auxBit = 1;
            var x = jQuery("#projetosApoiados").height();
            var altura = x + 95;
        } else {
            var x = jQuery("#projetosApoiados").height();
            var altura = x + 95;
        }

        jQuery('#submit').css('margin-top',altura);

        ComponentsSelect2.init();

        jQuery("#programa").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

            var pathArray = window.location.pathname.split('/');

            var secondLevelLocation = pathArray[1];

            var programa = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

            if(programa != 1 && programa != 2) {
                auxBit = 0;
                var select = jQuery('#linhaFinanciamento');
                select.find('option').remove();
                select.prop('disabled', true);
                select.append(jQuery('<option>').text('Linhas de financiamento'));

            } if(programa == 2){
                jQuery("#tipologia").removeAttr('value');
                jQuery("#tipologia").empty();

                var x = jQuery("#projetosApoiados").height();

                if(auxBit == 0){
                    var altura = x + 153;
                    auxBit = 1;
                } else {
                    var altura = x + 80;
                }


                jQuery('#submit').css('margin-top',altura);

                var dataString = "programa="+programa; /* STORE THAT TO A DATA STRING */

                // show loader
                App.blockUI({ target:'#blocoLinhasFinanciamento',  animate: true});

                jQuery.ajax({
                    url: websitepath,
                    data:'m=europa_getlinha&programa=' + programa ,

                    success: function(output) {

                        var select = jQuery('#linhaFinanciamento');
                        select.find('option').remove();

                        if (output == null || output=='') {
                            select.prop('disabled', true);
                        }
                        else {
                            select.prop('disabled', false);
                            select.append(jQuery('<option>').text('Todas as linhas de financiamento').attr('value', ''));
                            jQuery.each(JSON.parse(output), function (i, obj) {
                                //console.log('i=' + i);
                                select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                            });
                        }

                        // hide loader
                        App.unblockUI('#blocoLinhasFinanciamento');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //alert(xhr.status +  ' ' + thrownError);
                        // hide loader
                        select.find('option').remove();
                        select.prop('disabled', true);

                        App.unblockUI('#blocoLinhasFinanciamento');
                    }
                });

            } else {

                var x = jQuery("#projetosApoiados").height();

                if(auxBit == 0){
                    var altura = x + 153;
                    auxBit = 1;
                } else {
                    var altura = x + 80;
                }

                jQuery('#submit').css('margin-top',altura);

                var dataString = "programa="+programa; /* STORE THAT TO A DATA STRING */

                // show loader
                jQuery.noConflict();
                (function( $ ) {
                    $(function() {
                        // More code using $ as alias to jQuery
                        App.blockUI({ target:'#blocoLinhasFinanciamento',  animate: true});
                    });
                })(jQuery);



                jQuery.ajax({
                    url: websitepath,
                    data:'m=europa_getlinha&programa=' + programa ,

                    success: function(output) {

                        var select = jQuery('#linhaFinanciamento');
                        select.find('option').remove();

                        if (output == null || output=='') {
                            select.prop('disabled', true);
                        }
                        else {
                            select.prop('disabled', false);
                            select.append(jQuery('<option>').text('Todas as linhas de financiamento').attr('value', ''));
                            jQuery.each(JSON.parse(output), function (i, obj) {
                                //console.log('i=' + i);
                                select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                            });
                        }

                        // hide loader
                        App.unblockUI('#blocoLinhasFinanciamento');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //alert(xhr.status +  ' ' + thrownError);
                        // hide loader
                        select.find('option').remove();
                        select.prop('disabled', true);

                        App.unblockUI('#blocoLinhasFinanciamento');
                    }
                });
            }

        });

        jQuery('#programa').on('change', function() {
            var value = jQuery(this).val();
            if(value == '2'){
                jQuery('#cat').css('display','block');
                jQuery('#tip').css('display','none');
            } else if(value == '1'){
                jQuery('#cat').css('display','none');
                jQuery('#tip').css('display','block');
            } else {
                auxBit = 0;
                jQuery('#cat').css('display','none');
                jQuery('#tip').css('display','none');
                jQuery('#submit').css('margin-top','540px');
            }
        });


        jQuery( ".filtro .cleanFilter" ).on( "click", function() {
            auxBit = 0;
            jQuery('#cat').css('display','none');
            jQuery('#tip').css('display','none');

            var x = jQuery("#projetosApoiados").height();
            var altura = x + 80;
            jQuery('#submit').css('margin-top',altura);


            jQuery("#pesquisaLivre").removeAttr('value');
            jQuery("#pesquisaLivre").val('');


            jQuery("#programa").removeAttr('value');
            jQuery("#programa").empty();
            jQuery.ajax({
                url: websitepath,
                data:'m=europa_getprogramas&programa=1' ,

                success: function(output) {

                    var select = jQuery('#programa');
                    select.find('option').remove();

                    select.prop('disabled', false);
                    select.append(jQuery('<option>').text('Subprograma').attr('value', ''));
                    jQuery.each(JSON.parse(output), function (i, obj) {
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                    });


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    select.find('option').remove();
                    select.prop('disabled', true);

                }
            });




            jQuery("#ano").removeAttr('value');
            jQuery("#ano").empty();
            jQuery.ajax({
                url: websitepath,
                data:'m=europa_getAno&ano=1' ,

                success: function(output) {

                    var selectAno = jQuery('#ano');
                    selectAno.find('option').remove();

                    selectAno.prop('disabled', false);
                    selectAno.append(jQuery('<option>').text('Ano de apoio').attr('value', ''));
                    jQuery.each(JSON.parse(output), function (i, obj) {
                        selectAno.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                    });


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    selectAno.find('option').remove();
                    selectAno.prop('disabled', true);

                }
            });


            jQuery("#dataInicio").removeAttr('value');
            jQuery("#dataInicio").val('');
            jQuery("#dataFim").removeAttr('value');
            jQuery("#dataFim").val('');

            var select = jQuery('#linhaFinanciamento');
            select.find('option').remove();
            select.prop('disabled', true);
        });


    }, 500);

});