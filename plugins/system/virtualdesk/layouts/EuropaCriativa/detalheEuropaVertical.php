<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheEuropa');
    if ($resPluginEnabled === false) exit();

    $obParam      = new VirtualDeskSiteParamsHelper();

    $linkFiltro = $obParam->getParamsByTag('linkFiltroProgramas');

    $imgPrograma = VirtualDeskSiteDetalheHelper::getImgPrograma($referencia);
    $temImagem = 0;

    $idProg = VirtualDeskSiteDetalheHelper::getIDProg($programa);


    $parceiros = VirtualDeskSiteDetalheHelper::getParceirosPrograma($referencia);

    if($idProg == 1) {
        $splittipologia = explode("|", $tipologia);

        if(count($splittipologia) == 1){
            if($splittipologia[0] == 'NULL'){
                $textTipologia = JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
            } else {
                $textTipologia = VirtualDeskSiteDetalheHelper::getTextTipologia($splittipologia[0]);
            }
        } else {
            for($i=0; $i < count($splittipologia); $i = $i + 1) {
                if($i == 0){
                    $textTipologia = VirtualDeskSiteDetalheHelper::getTextTipologia($splittipologia[0]);
                } else {
                    $textTipologia .= ', ' . VirtualDeskSiteDetalheHelper::getTextTipologia($splittipologia[$i]);
                }
            }
        }

        $corCultura = $obParam->getParamsByTag('corFundoBlocosCultura');
        $corTabelaCultura = $obParam->getParamsByTag('corTabelaCultura');

        $colorBox = $corCultura;
        $colorTableLine = $corTabelaCultura;

    } else if($idProg == 2) {
        $idcat = VirtualDeskSiteDetalheHelper::getIDcat($linha_Financiamento);
        $catApoio = VirtualDeskSiteDetalheHelper::getCatApoio($idcat);

        $corMedia = $obParam->getParamsByTag('corFundoBlocosMedia');
        $corTabelaMedia = $obParam->getParamsByTag('corTabelaMedia');

        $colorBox = $corMedia;
        $colorTableLine = $corTabelaMedia;
    }

?>

<style>
    table tbody tr:nth-child(even){
        background:<?php echo $colorTableLine;?>;
    }
</style>

<div class="vertical">

    <div class="mobileVertical">
        <?php $temImagem = 0; ?>
        <div class="w35">
            <div class="imagemPrograma">
                <?php
                if(count($imgPrograma) == 0){

                    $imgCultura = $obParam->getParamsByTag('ImagemCulturaVertical');
                    $imgMedia = $obParam->getParamsByTag('ImagemMediaVertical');


                    if($idProg == 1){
                        ?>
                        <img src="<?php echo JUri::base();?>images/EuropaCriativa/<?php echo $imgCultura;?>" alt="imgDetalhe" title="<?php echo $projeto; ?>"/>
                        <?php
                    } else if($idProg == 2){
                        ?>
                        <img src="<?php echo JUri::base();?>images/EuropaCriativa/<?php echo $imgMedia;?>" alt="imgDetalhe" title="<?php echo $projeto; ?>"/>
                        <?php
                    }
                } else {
                    $objEventFile = new VirtualDeskSiteFormularioFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefIdDETALHE($referencia);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        if($temImagem == 0){
                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                            ?>
                            <img src="<?php echo $rowFile->guestlink; ?>" alt="imgDetalhe" title="<?php echo $projeto; ?>">
                            <?php
                            $temImagem = 1;
                        }
                    }
                }
                ?>
            </div>
        </div>

        <div class="w100">
            <div class="box" style="background:<?php echo $colorBox;?>">
                <?php
                if(!empty($programa)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PROGRAMA'); ?></div>
                        <div class="info"><?php echo $programa;?></div>
                    </div>
                    <?php
                }

                if($lider != 'NULL' && !empty($pais_lider)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER'); ?></div>
                        <div class="info"><?php echo $lider . ' (' . VirtualDeskSiteDetalheHelper::getPaisLider($pais_lider) . ')' ;?></div>
                    </div>
                    <?php
                }

                if(!empty($entidade)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ENTIDADE'); ?></div>
                        <div class="info"><?php echo $entidade;?></div>
                    </div>
                    <?php
                }

                if(!empty($linha_Financiamento)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO'); ?></div>
                        <div class="info"><?php echo $linha_Financiamento;?></div>
                    </div>
                    <?php
                }

                if(!empty($call)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_CALL'); ?></div>
                        <div class="info"><?php echo $call;?></div>
                    </div>
                    <?php
                }

                if(!empty($valorFinanciamento)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_VALOR'); ?></div>
                        <?php
                        $pieces = explode(",", $valorFinanciamento);

                        if(empty($pieces[1])){
                            ?>
                            <div class="info"><?php echo number_format($pieces[0], 2, '.', ' ') . ' €';?></div>
                            <?php
                        } else {
                            ?>
                            <div class="info"><?php echo number_format($pieces[0], 0, '.', ' ') . ',' . $pieces[1] . ' €';?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }

                if($idProg == 1) {

                    if (!empty($data_Inicio) || !empty($data_Fim)) {
                        ?>
                        <div class="w25">
                            <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DATAPROJETO'); ?></div>
                            <div class="info">

                                <?php

                                if (!empty($data_Inicio) || $data_Inicio != '') {
                                    $splitDataInicio = explode("-", $data_Inicio);
                                    $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];
                                }

                                if (!empty($data_Fim) || $data_Fim != '') {
                                    $splitDataFim = explode("-", $data_Fim);
                                    $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];
                                }


                                if($data_Inicio == '--' && $data_Fim == '--'){
                                    echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                } else if($data_Inicio == '--' && $data_Fim == 'Em curso'){
                                    echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                } else if($data_Inicio != '--' && $data_Fim == 'Em curso'){
                                    echo $DataInicioFormated . ' / ' . $data_Fim;
                                } else {
                                    echo $DataInicioFormated . ' / ' . $DataFimFormated;
                                }

                                ?>
                            </div>
                        </div>
                        <?php
                    }
                }

                if(!empty($ano_Apoio)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ANO'); ?></div>
                        <div class="info"><?php echo $ano_Apoio;?></div>
                    </div>
                    <?php
                }

                if($idProg == 1){
                    if(!empty($tipologia)){
                        ?>
                        <div class="w25">
                            <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA'); ?></div>
                            <div class="info"><?php echo $textTipologia;?></div>
                        </div>
                        <?php
                    }
                }

                if($idProg == 2){
                    if(!empty($linha_Financiamento)) {
                        ?>
                        <div class="w25">
                            <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CATEGORIA'); ?></div>
                            <div class="info"><?php echo $catApoio; ?></div>
                        </div>
                        <?php
                    }
                }

                if(!empty($link)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LINK'); ?></div>
                        <div class="info"><a href="<?php echo $link;?>" title="<?php echo $link;?>" target="_blank"><?php echo substr($link,0,30); ?></a></div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

        <div class="w65">
            <div class="descricao">
                <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($designacao); ?>
            </div>
        </div>

        <?php
        if(count($parceiros) > 0){
            ?>
            <div class="w65" style="vertical-align:middle;">
                <h3 class="titleParceiros"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LISTAPARCEIROS') ?></h3>
                <table>
                    <thead>
                    <tr style="background:<?php echo $colorBox;?>">
                        <td><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PARCEIRO') ?></td>
                        <td><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PAIS') ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($parceiros as $rowWSL) :
                        ?>
                        <tr>
                            <td><?php echo $rowWSL['parceiro'];?></td>
                            <td><?php echo $rowWSL['pais'];?></td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
            <?php
        }
        ?>
    </div>

    <div class="normalVertical">
        <?php $temImagem = 0; ?>
        <div class="w65">
            <div class="descricao">
                <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($designacao); ?>
            </div>
        </div>

        <div class="w35">
            <div class="imagemPrograma">
                <?php
                if(count($imgPrograma) == 0){

                    $imgCultura = $obParam->getParamsByTag('ImagemCulturaVertical');
                    $imgMedia = $obParam->getParamsByTag('ImagemMediaVertical');


                    if($idProg == 1){
                        ?>
                        <img src="<?php echo JUri::base();?>images/EuropaCriativa/<?php echo $imgCultura;?>" alt="imgDetalhe" title="<?php echo $projeto; ?>"/>
                        <?php
                    } else if($idProg == 2){
                        ?>
                        <img src="<?php echo JUri::base();?>images/EuropaCriativa/<?php echo $imgMedia;?>" alt="imgDetalhe" title="<?php echo $projeto; ?>"/>
                        <?php
                    }
                } else {
                    $objEventFile = new VirtualDeskSiteFormularioFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefIdDETALHE($referencia);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        if($temImagem == 0){
                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                            ?>
                            <img src="<?php echo $rowFile->guestlink; ?>" alt="imgDetalhe" title="<?php echo $projeto; ?>">
                            <?php
                            $temImagem = 1;
                        }
                    }
                }
                ?>
            </div>
        </div>

        <div class="w100">
            <div class="box" style="background:<?php echo $colorBox;?>">
                <?php
                if(!empty($programa)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PROGRAMA'); ?></div>
                        <div class="info"><?php echo $programa;?></div>
                    </div>
                    <?php
                }

                if($lider != 'NULL' && !empty($pais_lider)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER'); ?></div>
                        <div class="info"><?php echo $lider . ' (' . VirtualDeskSiteDetalheHelper::getPaisLider($pais_lider) . ')' ;?></div>
                    </div>
                    <?php
                }

                if(!empty($entidade)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ENTIDADE'); ?></div>
                        <div class="info"><?php echo $entidade;?></div>
                    </div>
                    <?php
                }

                if(!empty($linha_Financiamento)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO'); ?></div>
                        <div class="info"><?php echo $linha_Financiamento;?></div>
                    </div>
                    <?php
                }

                if(!empty($call)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_CALL'); ?></div>
                        <div class="info"><?php echo $call;?></div>
                    </div>
                    <?php
                }

                if(!empty($valorFinanciamento)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_VALOR'); ?></div>
                        <?php
                        $pieces = explode(",", $valorFinanciamento);

                        if(empty($pieces[1])){
                            ?>
                            <div class="info"><?php echo number_format($pieces[0], 2, '.', ' ') . ' €';?></div>
                            <?php
                        } else {
                            ?>
                            <div class="info"><?php echo number_format($pieces[0], 0, '.', ' ') . ',' . $pieces[1] . ' €';?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }

                if($idProg == 1) {

                    if (!empty($data_Inicio) || !empty($data_Fim)) {
                        ?>
                        <div class="w25">
                            <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DATAPROJETO'); ?></div>
                            <div class="info">

                                <?php

                                if (!empty($data_Inicio) || $data_Inicio != '') {
                                    $splitDataInicio = explode("-", $data_Inicio);
                                    $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];
                                }

                                if (!empty($data_Fim) || $data_Fim != '') {
                                    $splitDataFim = explode("-", $data_Fim);
                                    $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];
                                }


                                if($data_Inicio == '--' && $data_Fim == '--'){
                                    echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                } else if($data_Inicio == '--' && $data_Fim == 'Em curso'){
                                    echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                } else if($data_Inicio != '--' && $data_Fim == 'Em curso'){
                                    echo $DataInicioFormated . ' / ' . $data_Fim;
                                } else {
                                    echo $DataInicioFormated . ' / ' . $DataFimFormated;
                                }

                                ?>
                            </div>
                        </div>
                        <?php
                    }
                }

                if(!empty($ano_Apoio)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ANO'); ?></div>
                        <div class="info"><?php echo $ano_Apoio;?></div>
                    </div>
                    <?php
                }

                if($idProg == 1){
                    if(!empty($tipologia)){
                        ?>
                        <div class="w25">
                            <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA'); ?></div>
                            <div class="info"><?php echo $textTipologia;?></div>
                        </div>
                        <?php
                    }
                }

                if($idProg == 2){
                    if(!empty($linha_Financiamento)) {
                        ?>
                        <div class="w25">
                            <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CATEGORIA'); ?></div>
                            <div class="info"><?php echo $catApoio; ?></div>
                        </div>
                        <?php
                    }
                }

                if(!empty($link)){
                    ?>
                    <div class="w25">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LINK'); ?></div>
                        <div class="info"><a href="<?php echo $link;?>" title="<?php echo $link;?>" target="_blank"><?php echo substr($link,0,30); ?></a></div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

        <?php
            if(count($parceiros) > 0){
                ?>
                    <div class="w65" style="vertical-align:middle;">
                        <h3 class="titleParceiros"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LISTAPARCEIROS') ?></h3>
                        <table>
                            <thead>
                            <tr style="background:<?php echo $colorBox;?>">
                                <td><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PARCEIRO') ?></td>
                                <td><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PAIS') ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($parceiros as $rowWSL) :
                                ?>
                                <tr>
                                    <td><?php echo $rowWSL['parceiro'];?></td>
                                    <td><?php echo $rowWSL['pais'];?></td>
                                </tr>
                            <?php
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                <?php
            }
        ?>

    </div>

    <div class="w35" style="vertical-align:middle;">
        <a href="<?php echo $linkFiltro;?>">
            <div class="goback" style="background:<?php echo $colorBox;?>; border-color:<?php echo $colorBox;?>;" title="<?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_GOBACKTITLE') ?>"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_GOBACK') ?></div>
        </a>
    </div>
</div>
