<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('novoPrograma');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    //FileUploader
    $headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/plugins/system/virtualdesk/layouts/EuropaCriativa/novoPrograma.css' . $addcss_end;

    //Fileuploader
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

    ?>
        <style>
            .fileuploader-popup {z-index: 99999 !important;}
        </style>
    <?php

    if (isset($_POST['submitForm'])) {
        $nomeProjeto = $_POST['nomeProjeto'];
        $descricao = $_POST['descricao'];
        $programa = $_POST['programa'];
        $entidade = $_POST['entidade'];
        $lider = $_POST['lider'];
        $pais = $_POST['pais'];
        $ano = $_POST['ano'];
        $linhaFinanciamento = $_POST['linhaFinanciamento'];
        $escala = $_POST['escala'];
        $distrito = $_POST['distrito'];
        $concelhos = $_POST['vdmenumain'];
        $link = $_POST['link'];
        $call = $_POST['call'];
        $dataInicio = $_POST['dataInicio'];
        $dataFim = $_POST['dataFim'];
        $tipologia = $_POST['tipologia'];
        $idsTipologia = 0;
        $dataAtual = date("Y-m-d");
        $ProgramaParceiro = $_POST['ProgramaParceiro'];
        $valor = $_POST['valor'];
        $layout = $_POST['layout'];

        $ref = explode('{"ParceiroInput":', $ProgramaParceiro);
        $ref2 = explode(']}', $ref[1]);

        $arrayParceiros = $ref2[0] . ']';

        $noPartner = str_replace('"parceiro":', "", $arrayParceiros);

        $noPais = str_replace('"paisParceiro":', "", $noPartner);

        $nochavetas = str_replace('},{', ",", $noPais);

        $noarrayIni = str_replace('[{', "", $nochavetas);

        $noarrayFim = str_replace('}]', "", $noarrayIni);

        $ParceirosBD = str_replace('"', "", $noarrayFim);


        /*if(!empty($dataInicio) && !empty($dataFim)){
            $errDatas = VirtualDeskSiteFormularioHelper::ValidacaoDatas($dataInicio, $dataFim);
        }*/

        if(empty($programa)){
            $errPrograma=1;
        }

        if($linhaFinanciamento == 15){
            if(empty($escala)){
                $errEscala=1;
            }
        }

        if($programa == '1'){
            ?>
                <script>jQuery('.blocoFormCultura').css('display','block');</script>
            <?php

            if(empty($tipologia['0'])){
                //$errTipologia=1;
                $idsTipologia='NULL';
            }else {
                $idsTipologia = implode(',',$tipologia);
            }
        } else {
            ?>
                <script>jQuery('.blocoFormCultura').css('display','none');</script>
            <?php
        }


        if(empty($layout)){
            $errLayout=1;
        }

        // Valida recaptcha
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');
        $jinput->set('g-recaptcha-response',$recaptcha_response_field);
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        $errRecaptcha = 0;
        if (!$resRecaptchaAnswer) {
            $errRecaptcha = 1;
        }


        if($errPrograma == 0 && $errDatas == 0 && $errTipologia == 0 && $errEscala == 0 && $errLayout == 0 && $errRecaptcha == 0){

            $random = VirtualDeskSiteFormularioHelper::random_code();
            $indicativoPrograma = VirtualDeskSiteFormularioHelper::getIndicativo($programa);

            $referencia = $indicativoPrograma . '_' . $random;

            $splitDataInicio = explode("-", $dataInicio);
            $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];


            $splitDataFim = explode("-", $dataFim);
            $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];


            if($DataFimFormated == '--'){
                $DataFimBD = 'Em curso';
            } else {
                $DataFimBD = $DataFimFormated;
            }


            if($programa != '1'){
                $lider = 'NULL';
                $pais = 'NULL';
                $idsTipologia = 'NULL';
            }

            if($linhaFinanciamento != 15){
                $escala = '';
            }


            $idCat = VirtualDeskSiteFormularioHelper::getCat($linhaFinanciamento);

            $saveProj = VirtualDeskSiteFormularioHelper::saveProjeto($referencia, $nomeProjeto, $descricao, $programa, $entidade, $lider, $pais, $ano, $linhaFinanciamento, $escala, $distrito, $concelhos, $idsTipologia, $idCat, $link, $DataInicioFormated, $DataFimBD, $call, $valor, $layout, $dataAtual);

            if ($saveProj == true) {
                ?>

                <style>
                    #submit{ display:none !important;}
                </style>

                <div class="message">
                    <div class="messageContent">
                        <p>
                            <div class="sucessIcon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10" stroke-width="2"/><path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg></div>
                            <?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_SUCESSO'); ?>
                        </p>
                        <p><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_SUCESSO1'); ?></p>
                        <p><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_SUCESSO2'); ?></p>
                        <p><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_SUCESSO3'); ?></p>
                        <p><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_SUCESSO4'); ?></p>
                    </div>
                </div>
                <?php

                if ($programa == 1) {
                    $refParceiros = explode(',', $ParceirosBD);

                    for($i=0; $i < count($refParceiros); $i = $i + 2){
                        VirtualDeskSiteFormularioHelper::saveParceiros($referencia, $refParceiros[$i], $refParceiros[$i + 1]);
                    }
                }

                echo('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');

                exit();

            } else { ?>

                <div class="message"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ERRO'); ?></div>

            <?php }

        } else {
            ?>
                <script>
                    dropError();
                </script>

                <div class="backgroundErro">
                    <div class="erro" style="display:block;">
                        <button id="fechaErro">
                            <svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                                <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                            c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                            c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                            c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                            c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                            c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                            c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                            C42.996,43.384,41.986,44.001,40.361,44.02z">
                                </path>
                            </svg>
                        </button>

                        <h2>
                            <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18"
                                 role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                            <?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_AVISO'); ?>
                        </h2>

                        <ol>
                            <?php

                                if($errPrograma == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ERRO_PROGRAMA') . '</li>';
                                }else{
                                    $errPrograma = 0;
                                }

                                if($errDatas == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ERRO_DATAS') . '</li>';
                                }else{
                                    $errDatas = 0;
                                }

                                if($errEscala == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ERRO_ESCALA') . '</li>';
                                }else{
                                    $errEscala = 0;
                                }

                                if($errTipologia == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ERRO_TIPOLOGIA') . '</li>';
                                }else{
                                    $errTipologia = 0;
                                }

                                if($errLayout == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ERRO_LAYOUT') . '</li>';
                                }else{
                                    $errLayout = 0;
                                }

                                if($errRecaptcha == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ERRO_CAPTCHA') . '</li>';
                                }else{
                                    $errRecaptcha = 0;
                                }
                            ?>
                        </ol>

                    </div>
                </div>
            <?php
        }



    }

?>

<div class="formulario">
    <form id="novoProjetoApoiado" action="" method="post" class="login-form" enctype="multipart/form-data" >

        <!--   Nome do projeto  -->
        <div class="form-group" id="projName">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_NOMEPROJETO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="" name="nomeProjeto" id="nomeProjeto" maxlength="500" value="<?php echo $nomeProjeto; ?>"/>
            </div>
        </div>


        <!--  Descrição projeto  -->
        <div class="form-group" id="desc">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DESIGNACAO_LABEL'); ?></label>
            <div class="col-md-9">
                    <textarea class="form-control" rows="5"  placeholder="" name="descricao" id="descricao" maxlength="2000"><?php echo $descricao; ?></textarea>
            </div>
        </div>

        <!--   Programa  -->
        <div class="form-group" id="prog">
            <label class="col-md-3 control-label" for="field_programa"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PROGRAMA') ?><span class="required">*</span></label>
            <?php $Programa = VirtualDeskSiteFormularioHelper::getPrograma()?>
            <div class="col-md-9">
                <select name="programa" value="<?php echo $programa; ?>" id="programa" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($programa)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                        <?php foreach($Programa as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['programa']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $programa; ?>"><?php echo VirtualDeskSiteFormularioHelper::getProgSelect($programa) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludePrograma = VirtualDeskSiteFormularioHelper::excludePrograma($programa)?>
                        <?php foreach($ExcludePrograma as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['programa']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Nome da Entidade  -->
        <div class="form-group" id="entName">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ENTIDADE'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="" name="entidade" id="entidade" maxlength="500" value="<?php echo $entidade; ?>"/>
            </div>
        </div>

        <!--   Campos específicos para cultura  -->
        <div class="blocoFormCultura">

            <!--   Líder  -->
            <div class="form-group" id="liderName">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="lider" id="lider" maxlength="500" value="<?php echo $lider; ?>"/>
                </div>
            </div>

            <!--   País Lider  -->
            <div class="form-group" id="paisLider">
                <label class="col-md-3 control-label" for="field_pais"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PAIS') ?><span class="required">*</span></label>
                <?php $Pais = VirtualDeskSiteFormularioHelper::getPais()?>
                <div class="col-md-9">
                    <select name="pais" value="<?php echo $pais; ?>" id="pais" class="form-control select2 select2-search" >
                        <?php
                        if(empty($pais)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                            <?php foreach($Pais as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['pais']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $pais; ?>"><?php echo VirtualDeskSiteFormularioHelper::getPaisSelect($pais) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludePais = VirtualDeskSiteFormularioHelper::excludePais($pais)?>
                            <?php foreach($ExcludePais as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['pais']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>



            <div  class="mt-repeater">
                <div data-repeater-list="ParceiroInput">
                    <div data-repeater-item class="mt-repeater-item ">

                        <div class="mt-repeater-input ParceiroInput ParceiroInput-new" data-provides="ParceiroInput">
                            <div class="input-group input-xlarge">
                                <div class="form-control uneditable-input input-fixed input-large" data-trigger="ParceiroInput">

                                    <div class="form-group" id="parceiroName">
                                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PARCEIRO'); ?><span class="required">*</span></label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" autocomplete="off" placeholder="" name="parceiro" maxlength="500" value="<?php echo $parceiro1; ?>"/>
                                        </div>
                                    </div>

                                    <!--   País Parceiro  -->
                                    <div class="form-group" id="paisPartner">
                                        <label class="col-md-3 control-label" for="field_pais"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PAISPARCEIRO') ?><span class="required">*</span></label>
                                        <?php $Pais = VirtualDeskSiteFormularioHelper::getPais()?>
                                        <div class="col-md-9">
                                            <select name="paisParceiro" value="<?php echo $paisParceiro; ?>" class="form-control select2 select2-search" >
                                                <?php
                                                if(empty($paisParceiro)){
                                                    ?>
                                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                    <?php foreach($Pais as $rowWSL) : ?>
                                                        <option value="<?php echo $rowWSL['id']; ?>"
                                                        ><?php echo $rowWSL['pais']; ?></option>
                                                    <?php endforeach;
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $paisParceiro; ?>"><?php echo VirtualDeskSiteFormularioHelper::getPaisSelect($paisParceiro) ?></option>
                                                    <option value=""><?php echo '-'; ?></option>
                                                    <?php $ExcludePais = VirtualDeskSiteFormularioHelper::excludePais($paisParceiro)?>
                                                    <?php foreach($ExcludePais as $rowWSL) : ?>
                                                        <option value="<?php echo $rowWSL['id']; ?>"
                                                        ><?php echo $rowWSL['pais']; ?></option>
                                                    <?php endforeach;
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <a href="javascript:;" class="btn btn-danger mt-repeater-delete ParceiroInput-exists" data-repeater-delete data-dismiss="ParceiroInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>
                            </div>
                        </div>

                        <div class="mt-repeater-input">
                        </div>

                    </div>
                </div>

                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                    <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

            </div>






        </div>


        <!--   Ano atribuição -->
        <div class="form-group" id="year">
            <label class="col-md-3 control-label" for="field_ano"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ANO') ?><span class="required">*</span></label>
            <?php $Ano = VirtualDeskSiteFormularioHelper::getAno()?>
            <div class="col-md-9">
                <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($ano)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                        <?php foreach($Ano as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['ano']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $ano; ?>"><?php echo VirtualDeskSiteFormularioHelper::getAnoSelect($ano) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeAno = VirtualDeskSiteFormularioHelper::excludeAno($ano)?>
                        <?php foreach($ExcludeAno as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['ano']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Linhas Financiamento   -->
        <?php
        // Carrega menusec se o id de menumain estiver definido.
        $ListaFinanciamento = array();
        if(!empty($programa)) {
            if( (int) $programa > 0) $ListaFinanciamento = VirtualDeskSiteFormularioHelper::getLinhasFinanciamento($programa);
        }

        ?>
        <div id="blocoLinhasFinanciamento" class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <select name="linhaFinanciamento" id="linhaFinanciamento" value="<?php echo $linhaFinanciamento;?>"
                    <?php
                    if(empty($programa)) {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($linhaFinanciamento)){
                        ?>
                        <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EUROPA_OPCAO' ); ?></option>
                        <?php foreach($ListaFinanciamento as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                                <?php
                                if(!empty($this->data->linhaFinanciamento)) {
                                    if($this->data->linhaFinanciamento == $rowMM['id']) echo 'selected';
                                }
                                ?>
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach; ?>
                        <?php
                    } else {
                        ?>
                        <option value="<?php echo $linhaFinanciamento; ?>"><?php echo VirtualDeskSiteFormularioHelper::getLinhasFinanciamentoName($linhaFinanciamento) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeLinhasFinanciamento = VirtualDeskSiteFormularioHelper::excludeLinhasFinanciamento($programa, $linhaFinanciamento)?>
                        <?php foreach($ExcludeLinhasFinanciamento as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['name']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>

            </div>
        </div>



        <div class="blocoEscala">

            <div class="form-group" id="blocoEscalasCooperacao">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ESCALA') ?><span class="required">*</span></label>
                <?php $ListaEscala = VirtualDeskSiteFormularioHelper::getListaEscala()?>
                <div class="col-md-9">
                    <select name="escala" value="<?php echo $escala; ?>" id="escala" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($escala)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                            <?php foreach($ListaEscala as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['nome']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $escala; ?>"><?php echo VirtualDeskSiteFormularioHelper::getEscalaName($escala) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeEscala = VirtualDeskSiteFormularioHelper::excludeEscala($escala)?>
                            <?php foreach($ExcludeEscala as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['nome']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>


        </div>


        <!--   Distrito   -->
        <!--<div class="form-group" id="district">
            <?php $Distritos = VirtualDeskSiteFormularioHelper::getDistritos()?>
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DISTRITOS'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="distrito" value="<?php echo $distrito; ?>" id="distrito" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($distrito)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                            <?php foreach($Distritos as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['distrito']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $distrito; ?>"><?php echo VirtualDeskSiteFormularioHelper::getDistritosName($distrito) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeDistritos = VirtualDeskSiteFormularioHelper::excludeDistritos($distrito)?>
                            <?php foreach($ExcludeDistritos as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['distrito']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>-->


        <!--   Concelho   -->
        <!--<?php

        $ListaDeMenuMain = array();
        if(!empty($distrito)) {
            if( (int) $distrito > 0) $ListaDeMenuMain = VirtualDeskSiteFormularioHelper::getConcelhos($distrito);
        }

        ?>
        <div id="blocoMenuMain" class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CONCELHO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <select name="vdmenumain" id="vdmenumain" value="<?php echo $concelhos;?>"
                    <?php
                    if(empty($distrito)) {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($concelhos)){
                        ?>
                        <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EUROPA_OPCAO' ); ?></option>
                        <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                                <?php
                                if(!empty($this->data->vdmenumain)) {
                                    if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                                }
                                ?>
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach; ?>
                        <?php
                    } else {
                        ?>
                        <option value="<?php echo $concelhos; ?>"><?php echo VirtualDeskSiteFormularioHelper::getConcelhoName($concelhos) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeConcelho = VirtualDeskSiteFormularioHelper::excludeConcelho($distrito, $concelhos)?>
                        <?php foreach($ExcludeConcelho as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['name']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>

            </div>
        </div>-->


        <!--   URL DO PROJETO  -->
        <div class="form-group" id="linkProj">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LINK'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="" name="link" id="link" maxlength="1000" value="<?php echo $link; ?>"/>
            </div>
        </div>


        <!--   CALL DO PROJETO  -->
        <div class="form-group" id="callProj">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_CALL'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="" name="call" id="call" maxlength="100" value="<?php echo $call; ?>"/>
            </div>
        </div>


        <!--   VALOR FINANCIAMENTO  -->
        <div class="form-group" id="valorProj">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_VALOR'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="" name="valor" id="valor" maxlength="30" value="<?php echo $valor; ?>"/>
                <span>€</span>
            </div>
        </div>


        <!--   Layout Detalhe -->
        <div class="form-group" id="layoutDetalhe">
            <label class="col-md-3 control-label" for="field_ano"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LAYOUT') ?><span class="required">*</span></label>
            <?php $Layout = VirtualDeskSiteFormularioHelper::getLayout()?>
            <div class="col-md-9">
                <select name="layout" value="<?php echo $layout; ?>" id="layout" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($layout)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                        <?php foreach($Layout as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['layout']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $layout; ?>"><?php echo VirtualDeskSiteFormularioHelper::getLayoutSelect($layout) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeLayout = VirtualDeskSiteFormularioHelper::excludeLayout($layout)?>
                        <?php foreach($ExcludeLayout as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['layout']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   Data de inicio  -->
        <div class="form-group" id="projStart">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAINICIO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" placeholder="dd-mm-aaaa" name="dataInicio" id="dataInicio" value="<?php echo $dataInicio; ?>">
                    <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>


        <!--   Data de fim  -->
        <div class="form-group" id="projEnd">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAFIM'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" placeholder="dd-mm-aaaa" name="dataFim" id="dataFim" value="<?php echo $dataFim; ?>">
                    <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
                </div>
            </div>
        </div>


        <div class="blocoFormCultura">
            <!--   Tipologia   -->
            <div class="form-group" id="tipo">
                <?php $TIPOLOGIAS = VirtualDeskSiteFormularioHelper::getTipologia()?>
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <div class="input-group ">
                        <select name="tipologia[]" value="<?php echo $tipologia; ?>" id="tipologia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true" multiple="multiple">
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                            <?php foreach ($TIPOLOGIAS as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['tipologia']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <!--   Upload Imagens  -->
        <div class="form-group" id="uploadField">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_UPLOAD'); ?></label>
            <div class="col-md-9">
                <div class="file-loading">
                    <input type="file" name="fileupload[]" id="fileupload" multiple>
                </div>
                <div id="errorBlock" class="help-block"></div>
                <input type="hidden" id="VDAjaxReqProcRefId">

            </div>
        </div>

        <div style="height:75px;">
            <?php if ($captcha_plugin!='0') : ?>
                <div class="form-group">
                    <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                    $field_id = 'dynamic_recaptcha_1';
                    print $captcha->display($field_id, $field_id, 'g-recaptcha');
                    ?>
                    <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                </div>
            <?php endif; ?>
        </div>


        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="Submeter Projeto">
        </div>

    </form>
</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;
?>

<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'novoPrograma/'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/EuropaCriativa/novoPrograma.js.php');
    ?>
</script>