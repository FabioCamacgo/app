<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();

    //$noresults = 0;

    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroEuropa');
    if ($resPluginEnabled === false) exit();

    if(!empty($dataInicio)){
        $splitDataInicio = explode("-", $dataInicio);
        $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];
    } else {
        $DataInicioFormated = '';
    }

    if(!empty($dataFim)){
        $splitDataFim = explode("-", $dataFim);
        $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];
    } else {
        $DataFimFormated = '';
    }

    /*if(!empty($pesquisaLivre)){
        $listaRefsParceiros = VirtualDeskSiteFiltroHelper::getRefs($pesquisaLivre);
    }*/

    $listaProjetosFiltered = VirtualDeskSiteFiltroHelper::getProjetosFiltered($pesquisaLivre, $prog, $linhaFinanciamento, $ano, $DataInicioFormated, $DataFimFormated, $tipologiaID, $categoria);

    //$listaPartners = '';

    /*if(count($listaRefsParceiros)>0){
        $number = 1;
        foreach($listaRefsParceiros as $row) :
            $idPartner = $row['id'];
            $refPartner = $row['referencia'];
            $listaProjetosFilteredPartner = VirtualDeskSiteFiltroHelper::getProjetosFilteredPartner($refPartner, $prog, $linhaFinanciamento, $ano, $DataInicioFormated, $DataFimFormated, $tipologiaID, $categoria);


            if( strpos( $listaPartners, $listaProjetosFilteredPartner ) !== false) {

            } else {
                if($number == 1){
                    $listaPartners = $listaProjetosFilteredPartner;
                    $number = 2;
                } else {
                    $listaPartners .= ',' . $listaProjetosFilteredPartner;
                }
            }

        endforeach;

        $pieces2 = explode(",", $listaPartners);
    }*/

    if(count($listaProjetosFiltered) == 0){
        ?>
            <div class="semresultados">
                <div class="icon">
                    <svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M13,17h-2v-2h2V17z M13,13h-2V7h2V13z"/><g><path d="M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4 M12,2C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10s10-4.5,10-10   C22,6.5,17.5,2,12,2L12,2z"/></g></svg>
                </div>
                <h2><?php echo JText::_('COM_VIRTUALDESK_EUROPA_SEMRESULTADOS');?></h2>
            </div>
        <?php
    } else {
        /*if(!empty($pesquisaLivre)){
            $numberElements = count($pieces2) + count($listaProjetosFiltered);
        } else {
            $numberElements = count($listaProjetosFiltered);
        }*/

        ?>

        <div class="resultados">
            <h2><?php echo 'Foram encontrados <span>' . count($listaProjetosFiltered) . '</span> projetos apoiados.';?></h2>
        </div>

        <?php
            foreach($listaProjetosFiltered as $rowWSL) :
            $id = $rowWSL['id'];
            $ref = $rowWSL['referencia'];
            $projeto = $rowWSL['projeto'];
            $designacao = $rowWSL['designacao'];
            $programa = $rowWSL['programa'];
            $entidade = $rowWSL['entidade'];
            $lider = $rowWSL['lider'];
            $pais_lider = $rowWSL['pais_lider'];
            $ano_Apoio = $rowWSL['ano_Apoio'];
            $linha_Financiamento = $rowWSL['linha_Financiamento'];
            $tipologia = $rowWSL['tipologia'];
            $link = $rowWSL['link'];
            $data_Inicio = $rowWSL['data_Inicio'];
            $data_Fim = $rowWSL['data_Fim'];
            $call = $rowWSL['call'];

            $idProg = VirtualDeskSiteFiltroHelper::getIDProg($programa);

            if($idProg == 1) {
                $splittipologia = explode(",", $tipologia);
                $class = 'cultura';

                if(count($splittipologia) == 1){
                    if($splittipologia[0] == 'NULL'){
                        $textTipologia = JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                    } else {
                        $textTipologia = VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[0]);
                    }
                } else {
                    for($i=0; $i < count($splittipologia); $i = $i + 1) {
                        if($i == 0){
                            $textTipologia = VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[0]);
                        } else {
                            $textTipologia .= ', ' . VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[$i]);
                        }
                    }
                }

                $corHover = $obParam->getParamsByTag('corFundoBlocosCultura');

            } else if($idProg == 2){
                $corHover = $obParam->getParamsByTag('corFundoBlocosMedia');
                $class = 'media';
            }


            ?>
            <div class="projeto <?php echo $class;?>">
                <div class="titulo">
                    <a href="<?php echo $linkDetalhe;?>?ref=<?php echo base64_encode($ref); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_SAIBA_MAIS');?>">
                        <h3>
                            <?php echo $projeto; ?>
                        </h3>
                    </a>
                </div>


                <div class="content">
                    <div class="programa item">
                        <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PROGRAMA');?></h3>
                        <p>
                            <?php
                            if(empty($programa) || $programa == ''){
                                echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                            } else {
                                echo $programa;
                            }
                            ?>
                        </p>
                    </div>

                    <div class="linha item">
                        <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO');?></h3>
                        <p>
                            <?php
                            if(empty($linha_Financiamento) || $linha_Financiamento == ''){
                                echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                            } else {
                                echo $linha_Financiamento;
                            }
                            ?>
                        </p>
                    </div>

                    <div class="item">
                        <?php

                        if($idProg == 1){
                            ?>
                            <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA');?></h3>
                            <p>
                                <?php
                                if(empty($tipologia) || $tipologia == ''){
                                    echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                } else {
                                    echo $textTipologia;
                                }
                                ?>
                            </p>
                            <?php
                        } else if($idProg == 2){
                            $idcat = VirtualDeskSiteFiltroHelper::getIDcat($linha_Financiamento);
                            $catApoio = VirtualDeskSiteFiltroHelper::getCatApoio($idcat);

                            ?>
                            <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CATEGORIA');?></h3>
                            <p>
                                <?php
                                if(empty($catApoio) || $catApoio == ''){
                                    echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                } else {
                                    echo $catApoio;
                                }
                                ?>
                            </p>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="ano item">
                        <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ANO');?></h3>
                        <p>
                            <?php
                            if(empty($ano_Apoio) || $ano_Apoio == ''){
                                echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                            } else {
                                echo $ano_Apoio;
                            }
                            ?>
                        </p>
                    </div>

                    <?php
                    if($idProg == 1) {
                        ?>

                            <div class="entidade item">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER');?></h3>
                                <p>
                                    <?php
                                    if(empty($lider) || $lider == '' || $lider == 'NULL'){
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else {
                                        echo $lider;
                                    }
                                    ?>
                                </p>
                            </div>


                            <div class="data item">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DATAPROJETO');?></h3>
                                <p>
                                    <?php


                                    if(!empty($data_Inicio) || $data_Inicio != ''){
                                        $splitDataInicio = explode("-", $data_Inicio);
                                        $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];
                                    }

                                    if(!empty($data_Fim) || $data_Fim != ''){
                                        $splitDataFim = explode("-", $data_Fim);
                                        $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];
                                    }


                                    if($data_Inicio == '--' && $data_Fim == '--'){
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else if($data_Inicio == '--' && $data_Fim == 'Em curso'){
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else if($data_Inicio != '--' && $data_Fim == 'Em curso'){
                                        echo $DataInicioFormated . ' / ' . $data_Fim;
                                    } else {
                                        echo $DataInicioFormated . ' / ' . $DataFimFormated;
                                    }

                                    ?>
                                </p>
                            </div>
                            <?php
                        }
                    ?>



                    <a href="<?php echo $linkDetalhe;?>?ref=<?php echo base64_encode($ref); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_SAIBA_MAIS');?>">
                        <div class="sabermais">
                            <button class="botaoSaberMais" style="background:<?php echo $corHover;?>; border-color:<?php echo $corHover;?>;"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_SABERMAIS'); ?></button>
                        </div>
                    </a>

                </div>
            </div>
        <?php
        endforeach;


        /*if(!empty($listaPartners)){

            $pieces = explode(",", $listaPartners);

            $arrayProjetos = array();

            for($i=0; $i<count($pieces); $i++) {
                $getItem = VirtualDeskSiteFiltroHelper::getItemFromPartner($pieces[$i]);

                $id = $getItem[0]['id'];
                $ref = $getItem[0]['referencia'];
                $projeto = $getItem[0]['projeto'];
                $programa = $getItem[0]['programa'];
                $lider = $getItem[0]['lider'];
                $ano_Apoio = $getItem[0]['ano_Apoio'];
                $linha_Financiamento = $getItem[0]['linha_Financiamento'];
                $tipologia = $getItem[$i]['tipologia'];
                $data_Inicio = $getItem[0]['data_Inicio'];
                $data_Fim = $getItem[0]['data_Fim'];

                $idLinha = VirtualDeskSiteFiltroHelper::getIdLinha($linha_Financiamento);

                //, $prog, $linhaFinanciamento, $ano, $DataInicioFormated, $DataFimFormated, $tipologiaID, $categoria

                $idProg = VirtualDeskSiteFiltroHelper::getIDProg($programa);

                if(empty($prog) || $prog == $idProg){
                    $p = 1;
                } else {
                    $p = 0;
                }

                if(empty($linhaFinanciamento) || $idLinha == $linhaFinanciamento){
                    $l = 1;
                } else {
                    $l = 0;
                }


                if($p == 1 && $l == 1) {

                    if ($idProg == 1) {
                        $splittipologia = explode(",", $tipologia);
                        $class = 'cultura';

                        if (count($splittipologia) == 1) {
                            if ($splittipologia[0] == 'NULL') {
                                $textTipologia = JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                            } else {
                                $textTipologia = VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[0]);
                            }
                        } else {
                            for ($i = 0; $i < count($splittipologia); $i = $i + 1) {
                                if ($i == 0) {
                                    $textTipologia = VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[0]);
                                } else {
                                    $textTipologia .= ', ' . VirtualDeskSiteFiltroHelper::getTextTipologia($splittipologia[$i]);
                                }
                            }
                        }

                        $corHover = $obParam->getParamsByTag('corFundoBlocosCultura');

                    } else if ($idProg == 2) {
                        $corHover = $obParam->getParamsByTag('corFundoBlocosMedia');
                        $class = 'media';
                    }

                    ?>
                    <div class="projeto <?php echo $class; ?>">
                        <div class="titulo">
                            <a href="<?php echo $linkDetalhe; ?>?ref=<?php echo base64_encode($ref); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_SAIBA_MAIS'); ?>">
                                <h3>
                                    <?php echo $projeto; ?>
                                </h3>
                            </a>
                        </div>


                        <div class="content">
                            <div class="programa item">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_PROGRAMA'); ?></h3>
                                <p>
                                    <?php
                                    if (empty($programa) || $programa == '') {
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else {
                                        echo $programa;
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="linha item">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO'); ?></h3>
                                <p>
                                    <?php
                                    if (empty($linha_Financiamento) || $linha_Financiamento == '') {
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else {
                                        echo $linha_Financiamento;
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="item">
                                <?php

                                if ($idProg == 1) {
                                    ?>
                                    <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA'); ?></h3>
                                    <p>
                                        <?php
                                        if (empty($tipologia) || $tipologia == '') {
                                            echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                        } else {
                                            echo $textTipologia;
                                        }
                                        ?>
                                    </p>
                                    <?php
                                } else if ($idProg == 2) {
                                    $idcat = VirtualDeskSiteFiltroHelper::getIDcat($linha_Financiamento);
                                    $catApoio = VirtualDeskSiteFiltroHelper::getCatApoio($idcat);

                                    ?>
                                    <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_CATEGORIA'); ?></h3>
                                    <p>
                                        <?php
                                        if (empty($catApoio) || $catApoio == '') {
                                            echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                        } else {
                                            echo $catApoio;
                                        }
                                        ?>
                                    </p>
                                    <?php
                                }
                                ?>
                            </div>

                            <div class="ano item">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ANO'); ?></h3>
                                <p>
                                    <?php
                                    if (empty($ano_Apoio) || $ano_Apoio == '') {
                                        echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                    } else {
                                        echo $ano_Apoio;
                                    }
                                    ?>
                                </p>
                            </div>

                            <?php
                            if ($idProg == 1) {
                                ?>

                                <div class="entidade item">
                                    <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER'); ?></h3>
                                    <p>
                                        <?php
                                        if (empty($lider) || $lider == '' || $lider == 'NULL') {
                                            echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                        } else {
                                            echo $lider;
                                        }
                                        ?>
                                    </p>
                                </div>


                                <div class="data item">
                                    <h3><?php echo JText::_('COM_VIRTUALDESK_EUROPA_DATAPROJETO'); ?></h3>
                                    <p>
                                        <?php


                                        if (!empty($data_Inicio) || $data_Inicio != '') {
                                            $splitDataInicio = explode("-", $data_Inicio);
                                            $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];
                                        }

                                        if (!empty($data_Fim) || $data_Fim != '') {
                                            $splitDataFim = explode("-", $data_Fim);
                                            $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];
                                        }


                                        if (empty($data_Inicio) && empty($data_Fim)) {
                                            echo JText::_('COM_VIRTUALDESK_EUROPA_NAOINDICADO');
                                        } else if (empty($data_Inicio) && !empty($data_Fim)) {
                                            echo ' - / ' . $DataFimFormated;
                                        } else if (!empty($data_Inicio) && empty($data_Fim)) {
                                            echo $DataInicioFormated . ' / -';
                                        } else {
                                            echo $DataInicioFormated . ' / ' . $DataFimFormated;
                                        }
                                        ?>
                                    </p>
                                </div>
                                <?php
                            }
                            ?>


                            <a href="<?php echo $linkDetalhe; ?>?ref=<?php echo base64_encode($ref); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_EUROPA_SAIBA_MAIS'); ?>">
                                <div class="sabermais">
                                    <button class="botaoSaberMais"
                                            style="background:<?php echo $corHover; ?>; border-color:<?php echo $corHover; ?>;"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_SABERMAIS'); ?></button>
                                </div>
                            </a>

                        </div>
                    </div>
                    <?php
                    $arrayProjetos[$i] = 'true';
                } else {
                    $arrayProjetos[$i] = 'false';
                }

            }

            if (in_array("true", $arrayProjetos)) {

            } else {
                if(count($listaProjetosFiltered) == 0){
                    ?>
                    <style>
                        .resultados{display:none;}
                    </style>


                    <div class="semresultados">
                        <div class="icon">
                            <svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M13,17h-2v-2h2V17z M13,13h-2V7h2V13z"/><g><path d="M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4 M12,2C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10s10-4.5,10-10   C22,6.5,17.5,2,12,2L12,2z"/></g></svg>
                        </div>
                        <h2><?php echo JText::_('COM_VIRTUALDESK_EUROPA_SEMRESULTADOS');?></h2>
                    </div>
                    <?php
                }
            }


        }*/

    }

?>




