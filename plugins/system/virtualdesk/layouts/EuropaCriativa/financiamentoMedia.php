<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $jinput = JFactory::getApplication()->input;

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFiltroHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/filtro.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('financiamentoMedia');
    if ($resPluginEnabled === false) exit();

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';
    $valor = 0;
    $decimas = 0;

    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;

    $jinput = JFactory::getApplication('site')->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag){
        case 'pt-PT':
            $fileLangSufix = 'pt_PT';
            break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }


    $listaValor = VirtualDeskSiteFiltroHelper::getValorMedia();


    foreach($listaValor as $rowWSL) :
        $valorFinanciamento = $rowWSL['valorFinanciamento'];

        $pieces = explode(",", $valorFinanciamento);

        if(empty($pieces[1])){
            $valor = $valor + $valorFinanciamento;
        } else {
            $valor = $valor + $pieces[0];
            $decimas = $decimas + $pieces[1];
        }

    endforeach;

    if(strlen($decimas) > 2){

        $rest = substr($decimas, -2);

        $rest2 = $decimas / 100;

        $pieces2 = explode('.', $rest2);

        $valor = $valor + $pieces2[0];

    }

    if ($decimas == 0) {
        echo number_format($valor, 0, '', ' ') . ',00';
    } else {
        echo number_format($valor, 0, '', ' ') . ',' . $pieces2[1];
    }



?>

