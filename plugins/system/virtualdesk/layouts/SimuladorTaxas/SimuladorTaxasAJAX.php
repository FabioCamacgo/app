<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('simuladortaxas');
    if ($resPluginEnabled === false) exit();

    $headScripts = $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/SimuladorTaxas/simuladorTaxas.js' . $addscript_end;

?>


<div class="simulador">

    <?php
        if (isset($_POST['submitForm'])) {
            $area = $_POST['area'];
            $tip = 1;
        } else if (isset($_POST['submitInfo'])) {
            $tip = 2;
            $area = $_POST['area'];
            $sepTemp = $_POST['sepTemp'];
            $sepPerp = $_POST['sepPerp'];
            $corpos = $_POST['corpos'];
            $ossadas = $_POST['ossadas'];
            $imuGavetoes = $_POST['imuGavetoes'];
            $imuOssarios = $_POST['imuOssarios'];
            $exuOssada = $_POST['exuOssada'];
            $transCemiterio = $_POST['transCemiterio'];
            $transConcelho = $_POST['transConcelho'];
            $transOutConcelho = $_POST['transOutConcelho'];
            $colLapides = $_POST['colLapides'];
            $terJazigos = $_POST['terJazigos'];
            $obrasJazigos = $_POST['obrasJazigos'];
            $reconstrucaoJazigos = $_POST['reconstrucaoJazigos'];
            $revestimentoPerpetuas = $_POST['revestimentoPerpetuas'];
            $revestimentoTemporarias = $_POST['revestimentoTemporarias'];
            $utilizacaoCapela = $_POST['utilizacaoCapela'];
            $averbamentosAlvara = $_POST['averbamentosAlvara'];
            $total = 0;
        }
    ?>


   <form id="simula" action="" method="post" class="novaEmpresa-form" enctype="multipart/form-data" >
        <div id="contentTema">
            <div id="opcoes">
                <div class="form-group">
                    <?php $areasSimulador = VirtualDeskSiteSimuladorHelper::getAreas()?>
                    <div class="col-md-9">
                        <select name="area" value="<?php echo $area; ?>" id="area" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                            <?php
                            if(empty($area)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_SIMULADOR_AREAS_INSIDE'); ?></option>
                                <?php foreach($areasSimulador as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"
                                    ><?php echo $rowWSL['tema']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $area; ?>"><?php echo VirtualDeskSiteSimuladorHelper::getAreaName($area) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php $ExcludeArea = VirtualDeskSiteSimuladorHelper::excludeArea($area)?>
                                <?php foreach($ExcludeArea as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"
                                    ><?php echo $rowWSL['tema']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-actions" method="post" style="display:none;">
                    <input type="submit" name="submitForm" id="submitForm" value="Simular">
                </div>
            </div>
        </div>
    </form>

    <?php
        if($tip == 1){
            if(!empty($area)){
                if($area == 11){
                    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'SimuladorTaxasCemiterios.php');
                    ?>
                        <style>
                            #TaxSimular{display:inline-block;}
                        </style>
                    <?php
                } else {
                    ?>
                        <style>
                            #TaxSimular{display:none !important;}
                        </style>
                    <?php
                }
            }
        } else if($tip == 2){
            if($area == 11) {
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'SimuladorTaxasCemiterios.php');
                ?>
                <style>
                    #TaxSimular {
                        display: inline-block;
                    }
                </style>
                <?php
            }
        }

    ?>

</div>

<?php echo $headScripts;?>