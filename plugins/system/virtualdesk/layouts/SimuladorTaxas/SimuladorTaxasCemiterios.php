<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('simuladortaxas');
    if ($resPluginEnabled === false) exit();


?>

<div class="contentSimulador">

    <h2><span class="capitulo">Capítulo XII</span> - <span class="bold">Taxas Relativas a cemitérios</span></h2>

    <form id="simulaCemiterios" action="" method="post" class="novaEmpresa-form" enctype="multipart/form-data" >
        <h3>Artigo 83º - <span class="bold">Imunação, exumação e trasladação</span></h3>

        <div class="header">
            <div class="w60"></div>
            <div class="w20">Quantidade</div>
            <div class="w20">Valor</div>
        </div>

        <div class="contentBody">
            <div class="w100 level1">1- Imunação em covais:</div>

            <div class="w60 level2">a) Sepulturas temporárias</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="sepTemp" id="sepTemp" value="<?php echo $sepTemp; ?>"/>
            </div>
            <div class="w20 valor">50.00€</div>

            <div class="w60 level2">b) Sepulturas perpétuas</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="sepPerp" id="sepPerp" value="<?php echo $sepPerp; ?>"/>
            </div>
            <div class="w20 valor">100.00€</div>

            <!-- -------------------------------- -->

            <div class="w100 level1">2- Imunação em jazigo particular:</div>

            <div class="w60 level2">a) Corpos</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="corpos" id="corpos" value="<?php echo $corpos; ?>"/>
            </div>
            <div class="w20 valor">100.00€</div>

            <div class="w60 level2">b) Ossadas</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="ossadas" id="ossadas" value="<?php echo $ossadas; ?>"/>
            </div>
            <div class="w20 valor">40.00€</div>

            <!-- -------------------------------- -->

            <div class="w60 level1">3- Imunação em gavetões, pelo prazo de imunação</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="imuGavetoes" id="imuGavetoes" value="<?php echo $imuGavetoes; ?>"/>
            </div>
            <div class="w20 valor">250.00€</div>

            <!-- -------------------------------- -->

            <div class="w60 level1">4- Imunação em ossários, por ano</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="imuOssarios" id="imuOssarios" value="<?php echo $imuOssarios; ?>"/>
            </div>
            <div class="w20 valor">30.00€</div>

            <!-- -------------------------------- -->

            <div class="w60 level1">5- Exumação, por cada ossada</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="exuOssada" id="exuOssada" value="<?php echo $exuOssada; ?>"/>
            </div>
            <div class="w20 valor">40.00€</div>

            <!-- -------------------------------- -->

            <div class="w100 level1">6- Trasladação, por cada ossada:</div>

            <div class="w60 level2">a) Dentro do próprio cemitério</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="transCemiterio" id="transCemiterio" value="<?php echo $transCemiterio; ?>"/>
            </div>
            <div class="w20 valor">10.00€</div>

            <div class="w60 level2">b) Transporte de cadáveres dentro do concelho</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="transConcelho" id="transConcelho" value="<?php echo $transConcelho; ?>"/>
            </div>
            <div class="w20 valor">20.00€</div>

            <div class="w60 level2">c) Transporte de cadáveres de e para fora do concelho</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="transOutConcelho" id="transOutConcelho" value="<?php echo $transOutConcelho; ?>"/>
            </div>
            <div class="w20 valor">50.00€</div>

            <!-- -------------------------------- -->

            <div class="w60 level1">7- Colocação de lápides em campas</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="colLapides" id="colLapides" value="<?php echo $colLapides; ?>"/>
            </div>
            <div class="w20 valor">150.00€</div>
        </div>




        <h3>Artigo 84º - <span class="bold">Concessão de terrenos</span></h3>

        <div class="header">
            <div class="w60"></div>
            <div class="w20">Quantidade</div>
            <div class="w20">Valor</div>
        </div>

        <div class="contentBody">
            <div class="w100 level1">1- Para jazigos:</div>

            <div class="w60 level2">a) Os primeiros 3m²</div>
            <div class="w20"></div>
            <div class="w20 valor">3500.00€</div>

            <div class="w60 level2">b) Por cada m² ou fração suplementar</div>
            <div class="w20"></div>
            <div class="w20 valor">1000.00€</div>

            <div class="w60 level2">Número de m² que necessita</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="terJazigos" id="terJazigos" value="<?php echo $terJazigos; ?>"/>
            </div>
            <div class="w20"></div>
        </div>



        <h3>Artigo 85º - <span class="bold">Obras em jazigos e sepulturas</span></h3>

        <div class="header">
            <div class="w60"></div>
            <div class="w20">Quantidade</div>
            <div class="w20">Valor</div>
        </div>

        <div class="contentBody">
            <div class="w100 level1">1- Construção e obras de alteração de jazigos particulares:</div>

            <div class="w60 level2">a) Até 5m² (por m²)</div>
            <div class="w20"></div>
            <div class="w20 valor">5.00€</div>

            <div class="w60 level2">b) A partir dos 6m² (por m²)</div>
            <div class="w20"></div>
            <div class="w20 valor">7.50€</div>

            <div class="w60 level2">Número de m² que necessita</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="obrasJazigos" id="obrasJazigos" value="<?php echo $obrasJazigos; ?>"/>
            </div>
            <div class="w20"></div>

            <!-- -------------------------------- -->

            <div class="w60 level1">2- Reconstrução de jazigos</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="reconstrucaoJazigos" id="reconstrucaoJazigos" value="<?php echo $reconstrucaoJazigos; ?>"/>
            </div>
            <div class="w20 valor">30.00€</div>

            <!-- -------------------------------- -->

            <div class="w100 level1">3- Revestimentos de sepulturas:</div>

            <div class="w60 level2">a) Perpétuas</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="revestimentoPerpetuas" id="revestimentoPerpetuas" value="<?php echo $revestimentoPerpetuas; ?>"/>
            </div>
            <div class="w20 valor">50.00€</div>

            <div class="w60 level2">b) Temporárias</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="revestimentoTemporarias" id="revestimentoTemporarias" value="<?php echo $revestimentoTemporarias; ?>"/>
            </div>
            <div class="w20 valor">25.00€</div>
        </div>


        <h3>Artigo 86º - <span class="bold">Outros serviços</span></h3>

        <div class="header">
            <div class="w60"></div>
            <div class="w20">Quantidade</div>
            <div class="w20">Valor</div>
        </div>


        <div class="contentBody">
            <div class="w60 level1">1- Utilização da capela por cada período de 24 horas ou fração</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="utilizacaoCapela" id="utilizacaoCapela" value="<?php echo $utilizacaoCapela; ?>"/>
            </div>
            <div class="w20 valor">20.00€</div>

            <!-- -------------------------------- -->

            <div class="w60 level1">2- Averbamentos em alvarás de concessão de terrenos em nome do novo proprietário</div>
            <div class="w20">
                <input type="text" class="form-control" autocomplete="off" placeholder="" maxlength="5" name="averbamentosAlvara" id="averbamentosAlvara" value="<?php echo $averbamentosAlvara; ?>"/>
            </div>
            <div class="w20 valor">50.00€</div>
        </div>

        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitInfo" id="submitInfo" value="Calcular">
        </div>
    </form>

</div>


<?php
    if(!empty($sepTemp) && !is_numeric($sepTemp)){
        $errSepTemp = 1;
    } else if (strpos($sepTemp, '.') !== false) {
        $errSepTemp = 1;
    } else {
        $errSepTemp = 0;
    }

    if(!empty($sepPerp) && !is_numeric($sepPerp)){
        $errSepPerp = 1;
    } else if (strpos($sepPerp, '.') !== false) {
        $errSepPerp = 1;
    } else {
        $errSepPerp = 0;
    }

    if(!empty($corpos) && !is_numeric($corpos)){
        $errCorpos = 1;
    } else if (strpos($corpos, '.') !== false) {
        $errCorpos = 1;
    } else {
        $errCorpos = 0;
    }

    if(!empty($ossadas) && !is_numeric($ossadas)){
        $errOssadas = 1;
    } else if (strpos($ossadas, '.') !== false) {
        $errOssadas = 1;
    } else {
        $errOssadas = 0;
    }

    if(!empty($imuGavetoes) && !is_numeric($imuGavetoes)){
        $errImuGavetoes = 1;
    } else if (strpos($imuGavetoes, '.') !== false) {
        $errImuGavetoes = 1;
    } else {
        $errImuGavetoes = 0;
    }

    if(!empty($imuOssarios) && !is_numeric($imuOssarios)){
        $errImuOssarios = 1;
    } else if (strpos($imuOssarios, '.') !== false) {
        $errImuOssarios = 1;
    } else {
        $errImuOssarios = 0;
    }

    if(!empty($exuOssada) && !is_numeric($exuOssada)){
        $errExuOssada = 1;
    } else if (strpos($exuOssada, '.') !== false) {
        $errExuOssada = 1;
    } else {
        $errExuOssada = 0;
    }

    if(!empty($transCemiterio) && !is_numeric($transCemiterio)){
        $errTransCemiterio = 1;
    } else if (strpos($transCemiterio, '.') !== false) {
        $errTransCemiterio = 1;
    } else {
        $errTransCemiterio = 0;
    }

    if(!empty($transConcelho) && !is_numeric($transConcelho)){
        $errTransConcelho = 1;
    } else if (strpos($transConcelho, '.') !== false) {
        $errTransConcelho = 1;
    } else {
        $errTransConcelho = 0;
    }

    if(!empty($transOutConcelho) && !is_numeric($transOutConcelho)){
        $errTransOutConcelho = 1;
    } else if (strpos($transOutConcelho, '.') !== false) {
        $errTransOutConcelho = 1;
    } else {
        $errTransOutConcelho = 0;
    }

    if(!empty($colLapides) && !is_numeric($colLapides)){
        $errColLapides = 1;
    } else if (strpos($colLapides, '.') !== false) {
        $errColLapides = 1;
    } else {
        $errColLapides = 0;
    }

    if(!empty($terJazigos)){
        $text = str_replace(',', '.', $terJazigos);
        if(!is_numeric($text)){
            $errTerJazigos = 1;
        } else {
            $errTerJazigos = 0;
        }
    } else {
        $errTerJazigos = 0;
    }

    if(!empty($obrasJazigos)){
        $text2 = str_replace(',', '.', $obrasJazigos);
        if(!is_numeric($text2)){
            $errObrasJazigos = 1;
        } else {
            $errObrasJazigos = 0;
        }
    } else {
        $errObrasJazigos = 0;
    }

    if(!empty($reconstrucaoJazigos) && !is_numeric($reconstrucaoJazigos)){
        $errReconstrucaoJazigos = 1;
    } else if (strpos($reconstrucaoJazigos, '.') !== false) {
        $errReconstrucaoJazigos = 1;
    } else {
        $errReconstrucaoJazigos = 0;
    }

    if(!empty($revestimentoPerpetuas) && !is_numeric($revestimentoPerpetuas)){
        $errRevestimentoPerpetuas = 1;
    } else if (strpos($revestimentoPerpetuas, '.') !== false) {
        $errRevestimentoPerpetuas = 1;
    } else {
        $errRevestimentoPerpetuas = 0;
    }

    if(!empty($revestimentoTemporarias) && !is_numeric($revestimentoTemporarias)){
        $errRevestimentoTemporarias = 1;
    } else if (strpos($revestimentoTemporarias, '.') !== false) {
        $errRevestimentoTemporarias = 1;
    } else {
        $errRevestimentoTemporarias = 0;
    }

    if(!empty($utilizacaoCapela) && !is_numeric($utilizacaoCapela)){
        $errUtilizacaoCapela = 1;
    } else if (strpos($utilizacaoCapela, '.') !== false) {
        $errUtilizacaoCapela = 1;
    } else {
        $errUtilizacaoCapela = 0;
    }

    if(!empty($averbamentosAlvara) && !is_numeric($averbamentosAlvara)){
        $errAverbamentosAlvara = 1;
    } else if (strpos($averbamentosAlvara, '.') !== false) {
        $errAverbamentosAlvara = 1;
    } else {
        $errAverbamentosAlvara = 0;
    }

    if($errSepTemp == 0 && $errSepPerp == 0 && $errCorpos == 0 && $errOssadas == 0 && $errImuGavetoes == 0 && $errImuOssarios == 0 && $errExuOssada == 0 && $errTransCemiterio == 0 && $errTransConcelho == 0 && $errTransOutConcelho == 0 && $errColLapides == 0 && $errTerJazigos == 0 && $errObrasJazigos == 0 && $errReconstrucaoJazigos == 0 && $errRevestimentoPerpetuas == 0 && $errRevestimentoTemporarias == 0 && $errUtilizacaoCapela == 0 && $errAverbamentosAlvara == 0){

        if(!empty($sepTemp)){
            $var = 'sepTemp';
            $precoSepTemp = VirtualDeskSiteSimuladorHelper::getPreco($var);
            $calc = $sepTemp * $precoSepTemp;
            $total = $calc;
        }

        if(!empty($sepPerp)){
            $var2 = 'sepPerp';
            $precoSepPerp = VirtualDeskSiteSimuladorHelper::getPreco($var2);
            $calc2 = $sepPerp * $precoSepPerp;
            $total = $total + $calc2;
        }

        if(!empty($corpos)){
            $var3 = 'corpos';
            $precoCorpos = VirtualDeskSiteSimuladorHelper::getPreco($var3);
            $calc3 = $corpos * $precoCorpos;
            $total = $total + $calc3;
        }

        if(!empty($ossadas)){
            $var4 = 'ossadas';
            $precoOssadas = VirtualDeskSiteSimuladorHelper::getPreco($var4);
            $calc4 = $ossadas * $precoOssadas;
            $total = $total + $calc4;
        }

        if(!empty($imuGavetoes)){
            $var5 = 'imuGavetoes';
            $precoImuGavetoes = VirtualDeskSiteSimuladorHelper::getPreco($var5);
            $calc5 = $imuGavetoes * $precoImuGavetoes;
            $total = $total + $calc5;
        }

        if(!empty($imuOssarios)){
            $var6 = 'imuOssarios';
            $precoImuOssarios = VirtualDeskSiteSimuladorHelper::getPreco($var6);
            $calc6 = $imuOssarios * $precoImuOssarios;
            $total = $total + $calc6;
        }

        if(!empty($exuOssada)){
            $var7 = 'exuOssada';
            $precoExuOssada = VirtualDeskSiteSimuladorHelper::getPreco($var7);
            $calc7 = $exuOssada * $precoExuOssada;
            $total = $total + $calc7;
        }

        if(!empty($transCemiterio)){
            $var8 = 'transCemiterio';
            $precoTransCemiterio = VirtualDeskSiteSimuladorHelper::getPreco($var8);
            $calc8 = $transCemiterio * $precoTransCemiterio;
            $total = $total + $calc8;
        }

        if(!empty($transConcelho)){
            $var9 = 'transConcelho';
            $precoTransConcelho = VirtualDeskSiteSimuladorHelper::getPreco($var9);
            $calc9 = $transConcelho * $precoTransConcelho;
            $total = $total + $calc9;
        }

        if(!empty($transOutConcelho)){
            $var10 = 'transOutConcelho';
            $precoTransOutConcelho = VirtualDeskSiteSimuladorHelper::getPreco($var10);
            $calc10 = $transOutConcelho * $precoTransOutConcelho;
            $total = $total + $calc10;
        }

        if(!empty($colLapides)){
            $var11 = 'colLapides';
            $precoColLapides = VirtualDeskSiteSimuladorHelper::getPreco($var11);
            $calc11 = $colLapides * $precoColLapides;
            $total = $total + $calc11;
        }

        if(!empty($terJazigos)){
            $var12 = 'terJazigos';
            $var13 = 'terJazigos2';
            $precoTerJazigos = VirtualDeskSiteSimuladorHelper::getPreco($var12);
            $precoTerJazigos2 = VirtualDeskSiteSimuladorHelper::getPreco($var13);

            if($terJazigos < 4){
                $total = $total + $precoTerJazigos;
            } else {
                $missM2 = $terJazigos - 3;
                $calc12 = $missM2 * $precoTerJazigos2;
                $total = $total + $calc12 + $precoTerJazigos;
            }
        }

        if(!empty($obrasJazigos)){
            $var14 = 'obrasJazigos';
            $var15 = 'obrasJazigos2';
            $precoObrasJazigos = VirtualDeskSiteSimuladorHelper::getPreco($var14);
            $precoObrasJazigos2 = VirtualDeskSiteSimuladorHelper::getPreco($var15);

            if($obrasJazigos < 6){
                $calc13 = $obrasJazigos * $precoObrasJazigos;
                $total = $total + $calc13;
            } else {
                $calc14 = $obrasJazigos * $precoObrasJazigos2;
                $total = $total + $calc14;
            }
        }

        if(!empty($reconstrucaoJazigos)){
            $var16 = 'reconstrucaoJazigos';
            $precoReconstrucaoJazigos = VirtualDeskSiteSimuladorHelper::getPreco($var16);
            $calc15 = $reconstrucaoJazigos * $precoReconstrucaoJazigos;
            $total = $total + $calc15;
        }

        if(!empty($revestimentoPerpetuas)){
            $var17 = 'revestimentoPerpetuas';
            $precoRevestimentoPerpetuas = VirtualDeskSiteSimuladorHelper::getPreco($var17);
            $calc16 = $revestimentoPerpetuas * $precoRevestimentoPerpetuas;
            $total = $total + $calc16;
        }

        if(!empty($revestimentoTemporarias)){
            $var18 = 'revestimentoTemporarias';
            $precoRevestimentoTemporarias = VirtualDeskSiteSimuladorHelper::getPreco($var18);
            $calc17 = $revestimentoTemporarias * $precoRevestimentoTemporarias;
            $total = $total + $calc17;
        }

        if(!empty($utilizacaoCapela)){
            $var19 = 'utilizacaoCapela';
            $precoUtilizacaoCapela = VirtualDeskSiteSimuladorHelper::getPreco($var19);
            $calc18 = $utilizacaoCapela * $precoUtilizacaoCapela;
            $total = $total + $calc18;
        }

        if(!empty($averbamentosAlvara)){
            $var20 = 'averbamentosAlvara';
            $precoAverbamentosAlvara = VirtualDeskSiteSimuladorHelper::getPreco($var20);
            $calc19 = $averbamentosAlvara * $precoAverbamentosAlvara;
            $total = $total + $calc19;
        }

    } else {
        ?>
            <div class="backgroundErro" style="display:block;">
                <div class="erro">
                <button id="fechaErro">
                    <svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                                <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                                            c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                                            c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                                            c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                                            c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                                            c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                                            c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                                            C42.996,43.384,41.986,44.001,40.361,44.02z">
                                </path>
                            </svg>
                </button>
                <ol>
                    <?php if($errSepTemp == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_SEPTEMP'); ?></li>
                    <?php }

                    if($errSepPerp == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_SEPPERP'); ?></li>
                    <?php }

                    if($errCorpos == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_CORPOS'); ?></li>
                    <?php }

                    if($errOssadas == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_OSSADAS'); ?></li>
                    <?php }

                    if($errImuGavetoes == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_IMUGAVETOES'); ?></li>
                    <?php }

                    if($errImuOssarios == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_IMUOSSARIOS'); ?></li>
                    <?php }

                    if($errExuOssada == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_EXUOSSADA'); ?></li>
                    <?php }

                    if($errTransCemiterio == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_TRANSCEMITERIO'); ?></li>
                    <?php }

                    if($errTransConcelho == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_TRANSCONCELHO'); ?></li>
                    <?php }

                    if($errTransOutConcelho == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_TRANSOUTCONCELHO'); ?></li>
                    <?php }

                    if($errColLapides == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_COLLAPIDES'); ?></li>
                    <?php }

                    if($errTerJazigos == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_TERJAZIGOS'); ?></li>
                    <?php }

                    if($errObrasJazigos == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_OBRASJAZIGOS'); ?></li>
                    <?php }

                    if($errReconstrucaoJazigos == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_RECONSJAZIGOS'); ?></li>
                    <?php }

                    if($errRevestimentoPerpetuas == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_REVESTPERPETUAS'); ?></li>
                    <?php }

                    if($errRevestimentoTemporarias == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_REVESTEMPORARIAS'); ?></li>
                    <?php }

                    if($errUtilizacaoCapela == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_UTILCAPELA'); ?></li>
                    <?php }

                    if($errAverbamentosAlvara == 1){ ?>
                        <li><?php echo JText::_('COM_VIRTUALDESK_SIMULADORTAXAS_AVERBALVARA'); ?></li>
                    <?php } ?>
                </ol>
            </div>
            </div>
        <?php
    }

?>

<div class="boxTotal">
    <div class="w60 total">Total</div>
    <div class="w20"><div class="box"><?php echo number_format($total,2) . ' €'; ?></div></div>
    <div class="w20 valor"></div>
</div>
