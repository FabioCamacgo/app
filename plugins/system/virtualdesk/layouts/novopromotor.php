<?php
    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    //$this->language  = $doc->language;
    //$this->direction = $doc->direction;
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';

    // Se não estiver ativo o registo de novos utilizadores devemos parar o processamento
    $configSetNewRegistration = JComponentHelper::getParams('com_virtualdesk')->get('setnewregistration');
    if($configSetNewRegistration == '0' or empty($configSetNewRegistration) ) die;

    // Verifica qual o layout a apresentar
    $setregistrationlayout = JComponentHelper::getParams('com_virtualdesk')->get('setregistrationlayout');

    // Parametros da Password Chyeck de modo a podermos utilizar na validação javascript
    $passwordcheck_length      = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_length', 8);
    $passwordcheck_no_name     = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_name');
    $passwordcheck_no_email    = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_email');
    $passwordcheck_types_azmin = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmin');
    $passwordcheck_types_azmai = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmai');
    $passwordcheck_types_num   = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_num');
    $passwordcheck_types_special = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_special');

    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;
    //$language_tag = $lang->getTag(); // loads the current language-tag
    $jinput = JFactory::getApplication('site')->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);

    $obParam      = new VirtualDeskSiteParamsHelper();

    $cssFile = $obParam->getParamsByTag('cssFileGeral');
    $logosbrandpartner = $obParam->getParamsByTag('logosbrandpartner');
    $linkbrandpartner = $obParam->getParamsByTag('linkbrandpartner');
    $logowebsiteComercial = $obParam->getParamsByTag('logowebsiteComercial');
    $linkwebsiteComercial = $obParam->getParamsByTag('linkwebsiteComercial');
    $logoEntradaAPP = $obParam->getParamsByTag('logoEntradaAPP');
    $tabuladoresComercial = $obParam->getParamsByTag('tabuladoresComercial');
    $imgFundoAPP = $obParam->getParamsByTag('imgFundoAPP');
    $explodeBrandPartnerLogo = explode(';;', $logosbrandpartner);
    $explodeBrandPartnerLink = explode(';;', $linkbrandpartner);

    $resCheckFormSubmit = VirtualDeskSiteUserHelper::checkNewUserRegistrationFormData();
    $arrayMessages = array();
    $data          = array(); // inicializa array com dados
    $resNewUser    = false;  // poor defeito... não criou ainda o utilizdor...
    if($resCheckFormSubmit) {

        if ($captcha_plugin!='0') {
            $captcha                  = JCaptcha::getInstance($captcha_plugin);
            $recaptcha_response_field = $jinput->get('g-recaptcha-response', '', 'string');
            $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
            if (!$resRecaptchaAnswer) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'), 'error');
            } else {
                $model = JModelLegacy::getInstance('Profile', 'VirtualDeskModel');
                $data  = $model->getNewRegistrationPostedFormData();

                $resDataValid = $model->validateNewRegistrationBeforeSave($data);

                if($resDataValid===true){
                    $resNewUser =  VirtualDeskSiteUserHelper::saveNewUserRegistration($data);
                }
            }
        }
    } else {
        $data = VirtualDeskSiteUserHelper::getNewRegistrationCleanPostedData();
    }

    $data = VirtualDeskSiteUserHelper::getNewRegistrationSetIfNullPostData($data);


    $ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
    $arUserFieldsConfig = $ObjUserFields->getUserFields();
    $arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();

    ?>

    <link rel="stylesheet" href="<?php echo $baseurl;?>templates/virtualdesk/<?php echo $cssFile;?>">

    <div class="barraSuperior">
        <div class="content">
            <?php
                for($i=0; $i<count($explodeBrandPartnerLogo); $i++){
                    $indice = $i + 1;
                    ?>
                    <a href="<?php echo $explodeBrandPartnerLink[$i];?>" target="_blank">
                        <div class="botao brandpartner<?php echo $indice;?>">
                            <img src="<?php echo $baseurl . 'images/v_agenda/brandpartner/' . $explodeBrandPartnerLogo[$i];?>" alt="brandpartner<?php echo $indice;?>"/>
                        </div>
                    </a>
                    <?php
                }
            ?>
            <a href="<?php echo $linkwebsiteComercial;?>" class="logoWebsite">
                <img src="<?php echo $baseurl . $logowebsiteComercial;?>" alt="website"/>
            </a>
        </div>
    </div>

    <div class="fundo" style="background-image: URL('..<?php echo $imgFundoAPP;?>');"></div>

    <div class="workSpace">
        <a href="<?php echo $baseurl;?>" class="logo">
            <img src="<?php echo $baseurl . $logoEntradaAPP;?>" alt="logo_plataforma"/>
        </a>

        <div class="tabs">
            <?php
            $explodeTabs = explode(';;', $tabuladoresComercial);

            for($i=0; $i<count($explodeTabs);$i++){
                $index = $i + 1;

                if($i == 1){
                    ?>
                    <div class="tab active" id="tab<?php echo $index;?>" >
                    <?php
                } else {
                    ?>
                    <div class="tab" id="tab<?php echo $index;?>">
                        <?php
                }

                echo $explodeTabs[$i];?>
                </div>
                <?php
            }
                ?>
        </div>

        <div class="tabContent2 active">
            <?php
                $setregistrationlayout = 'novopromotor_center.php';

                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR. $setregistrationlayout);
            ?>
        </div>


    </div>
