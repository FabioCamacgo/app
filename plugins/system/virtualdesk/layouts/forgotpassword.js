/*
* Define estrutura da validação.
* A validação é iniciada depois no final deste ficheiro.
* Foi acrescentado antes da inicialização um método ( .addMethod("vdpasscheck"... ) que permite fazer a validação específica da password.
*/
var ForgotPassword = function() {

    var handleRegister = function() {

        jQuery('#member-registration').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                hiddenRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
            },

            messages: { // custom messages for radio buttons and checkboxes
                hiddenRecaptcha: {
                    required: jQuery('#hiddenRecaptcha').attr('messageValidation')
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   

                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = MessageAlert.getRequiredMissed(errors);
                        jQuery("#MainMessageAlertBlock").find('span').html(message);
                        jQuery("#MainMessageAlertBlock").show();
                    }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
                    error.insertAfter(jQuery('#register_tnc_error'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                jQuery('#cover-spin').show(0);
                form[0].submit();
            }
        });

        jQuery('#member-registration').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#member-registration').validate().form()) {
                    jQuery('#member-registration').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {

            handleRegister();
        }

    };

}();

/*
* Inicialização da validação
*/
jQuery(document).ready(function() {
    ForgotPassword.init();
});