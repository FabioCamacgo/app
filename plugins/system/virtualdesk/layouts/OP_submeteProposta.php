<?php

defined('JPATH_BASE') or die;
defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;

$isVDAjaxReq = $jinput->get('isVDAjaxReq');

JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteOPHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_op.php');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$config          = JFactory::getConfig();
$sitename        = $config->get('sitename');
$sitedescription = $config->get('sitedescription');
$sitetitle       = $config->get('sitetitle');
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$logoFile        = $templateParams->get('logoFile');
$fluidContainer  = $config->get('fluidContainer');
$page_heading    = $config->get('page_heading');
$baseurl         = JUri::base();
$rooturl         = JUri::root();
$captcha_plugin  = JFactory::getConfig()->get('captcha');
$templateName    = 'virtualdesk';
$labelseparator = ' : ';
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';


// Idiomas
$lang = JFactory::getLanguage();
$extension = 'com_virtualdesk';
$base_dir = JPATH_SITE;
//$language_tag = $lang->getTag(); // loads the current language-tag
$jinput = JFactory::getApplication('site')->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}



//$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

//GLOBAL SCRIPTS
$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'plugins\system\virtualdesk\layouts\OP.js' . $addscript_end;

// PAGE LEVEL PLUGIN SCRIPTS
//$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
//$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
//$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
//$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

//$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;
//$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
}

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
    $moradaMunicipio = $obParam->getParamsByTag('moradaMunicipio');
    $codPostalMunicipio = $obParam->getParamsByTag('codPostalMunicipio');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
    $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
    $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');

    if($isVDAjaxReq != 1){
        /*work inside app*/
    } else{


?>

        <div id="submeteProposta">

        <?php

        $politica = 0;

        if (isset($_POST['saveproposta'])) {
            $Nome = $_POST['field_nome'];
            $Email = $_POST['field_email'];
            $Email2 = $_POST['field_email2'];
            $Nif = $_POST['field_nif'];
            $Telefone = $_POST['field_telefone'];
            $Morada = $_POST['field_morada'];
            $Freguesia = $_POST['field_freguesia'];
            $CodPostal = $_POST['field_codPostal'];
            $nomeProposta = $_POST['field_nomeProposta'];
            $descricaoProposta = $_POST['field_descricaoProposta'];
            $freguesiaProposta = $_POST['field_freguesiaProposta'];
            $politica = $_POST['politicaPrivacidade'];
            if(empty($_POST['politicaPrivacidade'])){
                echo 'cccc ' . $politica;
            }
            $erro = 0;


            ?>
            <div class="erros">

                <button id="fechaErro"><svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                    <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                        c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                        c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                        c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                        c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                        c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                        c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                        C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                </svg></button>


                <h2>
                    <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                    <?php echo JText::_('COM_VIRTUALDESK_OP_AVISO'); ?>
                </h2>

                <?php

                /*Validação do nome*/
                if($Nome == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_NOME_VAZIO') . '</p>';
                    $erro = $erro + 1;
                }else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$Nome)) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_NOME_INVALIDO') . '</p>';
                    $erro = $erro + 1;
                }


                /*Validação Email*/

                $ExisteMail = VirtualDeskSiteOPHelper::seeEmailExiste($Email);
                if($Email == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_EMAIL_VAZIO') . '</p>';
                    $erro = $erro + 1;
                } else if($Email2 == Null || $Email != $Email2){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_EMAIL_NAO_CORRESPONDE') . '</p>';
                    $erro = $erro + 1;
                } else if((int)$ExisteMail > 1){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_EMAIL_EXISTE') . '</p>';
                    $erro = $erro + 1;
                } else if((int)$ExisteMail == 1){
                    $ExisteMailNIF = VirtualDeskSiteOPHelper::seeEmailExisteNIF($Email);
                    if($ExisteMailNIF != $Nif){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_EMAIL_EXISTE') . '</p>';
                        $erro = $erro + 1;
                    }
                }


                /*Validação Nif*/
                $nif=trim($Nif);
                $ignoreFirst=true;
                //Verificamos se é numérico e tem comprimento 9
                if (!is_numeric($nif) || strlen($nif)!=9) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_NIF_INVALIDO') . '</p>';
                    $erro = $erro + 1;
                } else{
                    $nifSplit=str_split($nif);
                    //O primeiro digíto tem de ser 1, 2, 5, 6, 8 ou 9
                    //Ou não, se optarmos por ignorar esta "regra"
                    if (
                        in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9))
                        ||
                        $ignoreFirst
                    ) {
                        //Calculamos o dígito de controlo
                        $checkDigit=0;
                        for($i=0; $i<8; $i++) {
                            $checkDigit+=$nifSplit[$i]*(10-$i-1);
                        }
                        $checkDigit=11-($checkDigit % 11);
                        //Se der 10 então o dígito de controlo tem de ser 0
                        if($checkDigit>=10) $checkDigit=0;
                        //Comparamos com o último dígito
                        if ($checkDigit==$nifSplit[8]) {

                        } else {
                            echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_NIF_INVALIDO') . '</p>';
                            $erro = $erro + 1;
                        }
                    } else {
                        echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_NIF_INVALIDO') . '</p>';
                        $erro = $erro + 1;
                    }
                }


                /*Validação Telefone*/
                $TelefoneSplit=str_split($Telefone);
                if($Telefone == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_TELEFONE_VAZIO') . '</p>';
                    $erro = $erro + 1;
                } else if(!is_numeric($Telefone) || strlen($Telefone) != 9){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_TELEFONE_INVALIDO') . '</p>';
                    $erro = $erro + 1;
                } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_TELEFONE_INVALIDO') . '</p>';
                    $erro = $erro + 1;
                } else if($TelefoneSplit[0] == 9){
                    if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
                        echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_TELEFONE_INVALIDO') . '</p>';
                        $erro = $erro + 1;
                    }
                }


                /*Validação Morada*/
                if($Morada == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_MORADA_VAZIO') . '</p>';
                    $erro = $erro + 1;
                } else if(is_numeric($Morada)){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_MORADA_INVALIDO') . '</p>';
                    $erro = $erro + 1;
                }


                /*Validação Freguesia*/
                if($Freguesia == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_FREGUESIA_VAZIO') . '</p>';
                    $erro = $erro + 1;
                }


                /*Validação Código Postal*/
                if($CodPostal == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_CODPOSTAL_VAZIO') . '</p>';
                    $erro = $erro + 1;
                } else if (!preg_match('/[0-9]{4}-[0-9]{3}/', $CodPostal)) {
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_CODPOSTAL_INVALIDO') . '</p>';
                    $erro = $erro + 1;
                }


                /*Validação Nome da Proposta*/
                if($nomeProposta == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_NOMEPROPOSTA_VAZIO') . '</p>';
                    $erro = $erro + 1;
                }


                /*Validação Descricao da Proposta*/
                if($descricaoProposta == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_DESCRICAOPROPOSTA_VAZIO') . '</p>';
                    $erro = $erro + 1;
                } else if(is_numeric($descricaoProposta)){
                    $erro = $erro + 1;
                }


                /*Validação Freguesia*/
                if($freguesiaProposta == Null){
                    echo '<p>' . JText::_('COM_VIRTUALDESK_OP_ERRO_FREGUESIAPROPOSTA_VAZIO') . '</p>';
                    $erro = $erro + 1;
                }

            ?>
            </div>


            <?php
            if($erro != 0){
                ?>
                <script type="text/javascript">
                    dropError();
                </script>
                <?php
            } else{
                ?>
                <script type="text/javascript">
                    jQuery( document ).ready(function() {
                        jQuery('#submeteProposta .erros').css('display','none');
                        jQuery('#enviarProposta').css('display','none');
                        var IDofDivWithForm = "novaProposta";

                        document.getElementById(IDofDivWithForm).style.display = "none";
                    })
                </script>

                <!--  Mensagem de sucesso -->
                <div class="sucesso">
                    <?php echo '<p>' . JText::sprintf('COM_VIRTUALDESK_OP_SUCESSO',$copyrightAPP) . '</p>'; ?>
                </div>

                <!--Enviar Email -->
                <?php

                $ExisteNif = VirtualDeskSiteOPHelper::seeNifExiste($Nif);

                if((int)$ExisteNif == 0){
                    VirtualDeskSiteOPHelper::guardaDadosPessoais($Nome, $Email, $Nif, $Telefone, $Morada, $Freguesia, $CodPostal);
                }



                $refExiste = 1;
                $year = substr(date("Y"), -2);

                while($refExiste == 1){
                    $refRandom = VirtualDeskSiteOPHelper::random_code();
                    $referencia = 'op' . $refRandom . $year;
                    $checkREF = VirtualDeskSiteOPHelper::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                $estado = 1;

                VirtualDeskSiteOPHelper::guardaDadosProposta($nomeProposta, $descricaoProposta, $freguesiaProposta, $Nif, $referencia, $estado);

                $fregName = VirtualDeskSiteOPHelper::getNomeFreguesia($Freguesia);
                $fregPropName = VirtualDeskSiteOPHelper::getNomeFreguesia($freguesiaProposta);
                VirtualDeskSiteOPHelper::SendEmailUser($contactoTelefCopyrightEmail, $dominioMunicipio, $moradaMunicipio, $codPostalMunicipio, $LinkCopyright, $emailCopyrightGeral, $Nome, $Email, $Nif, $Telefone, $Morada, $fregName, $CodPostal, $nomeProposta, $descricaoProposta, $fregPropName, $nomeMunicipio, $copyrightAPP);

            }
        }
        ?>

        <form id="novaProposta" method="post">

            <legend><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_SECTION_DADOSPESSOAIS') ?></legend>

            <fieldset id="personalData">
                <div class="form-group">
                    <label for="field_nome"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_NOME') ?></label>
                    <input type="text" id="field_nome" class="form-control select2 select2-nosearch" required name="field_nome" value="<?php echo htmlentities($Nome,ENT_QUOTES,'UTF-8'); ?> "/>
                </div>

                <div class="form-group">
                    <label for="field_email3"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_EMAIL') ?></label>
                    <input type="text" id="field_email" class="form-control select2 select2-nosearch" required name="field_email" value="<?php echo htmlentities($Email, ENT_QUOTES, 'UTF-8'); ?>"/>
                </div>

                <div class="form-group">
                    <label for="field_email2"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_EMAIL_CONF') ?></label>
                    <input type="text" id="field_email2" class="form-control select2 select2-nosearch" required name="field_email2" value="<?php echo htmlentities($Email2, ENT_QUOTES, 'UTF-8'); ?>"/>
                </div>

                <div class="form-group">
                    <label for="field_nif"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_NIF') ?></label>
                    <input type="text" id="field_nif" class="form-control select2 select2-nosearch" required name="field_nif" value="<?php echo htmlentities($Nif, ENT_QUOTES, 'UTF-8'); ?>"/>
                </div>

                <div class="form-group">
                    <label for="field_telefone"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_TELEFONE') ?></label>
                    <input type="text" id="field_telefone" class="form-control select2 select2-nosearch" required name="field_telefone" max="9" value="<?php echo htmlentities($Telefone, ENT_QUOTES, 'UTF-8'); ?>"/>
                </div>

                <div class="form-group">
                    <label for="field_morada"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_MORADA') ?></label>
                    <input type="text" id="field_morada" class="form-control select2 select2-nosearch" required name="field_morada" value="<?php echo htmlentities($Morada, ENT_QUOTES, 'UTF-8'); ?>"/>
                </div>

                <div class="form-group">
                    <label for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_FREGUESIA') ?></label>
                    <?php $Freguesias = VirtualDeskSiteOPHelper::getFreguesias('4')?>
                    <div class="input-group ">
                        <select name="field_freguesia" value="<?php echo $Freguesia; ?>" id="field_freguesia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                            <option></option>
                            <?php foreach($Freguesias as $rowStatus) : ?>
                                <option value="<?php echo $rowStatus['id_freguesia']; ?>"
                                ><?php echo $rowStatus['freguesia']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field_codPostal"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_CODIGOPOSTAL') ?></label>
                    <input type="text" id="field_codPostal" class="form-control select2 select2-nosearch" required name="field_codPostal"
                           value="<?php echo htmlentities($CodPostal, ENT_QUOTES, 'UTF-8'); ?>"/>
                </div>
            </fieldset>


            <legend><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_SECTION_DADOSPROPOSTA') ?></legend>

            <fieldset id="proposalData">

                <div class="form-group">
                    <label for="field_nomeProposta"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_NOMEPROPOSTA') ?></label>
                    <input type="text" id="field_nomeProposta" class="form-control select2 select2-nosearch" required name="field_nomeProposta"
                           value="<?php echo htmlentities($nomeProposta, ENT_QUOTES, 'UTF-8'); ?>"/>
                </div>


                <div class="form-group">
                    <label for="field_descricaoProposta"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_DESCPROPOSTA') ?></label>
                    <textarea required class="form-control select2 select2-nosearch" rows="5" name="field_descricaoProposta" id="field_descricaoProposta" maxlength="500"
                              value=""><?php echo htmlentities($descricaoProposta,ENT_QUOTES,'UTF-8'); ?></textarea>
                </div>

                <div class="form-group">
                    <label for="field_freguesiaProposta"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_FREGUESIA') ?></label>
                    <?php $Freguesias = VirtualDeskSiteOPHelper::getFreguesias('4')?>
                    <div class="input-group ">
                        <select name="field_freguesiaProposta" value="<?php echo $freguesiaProposta; ?>" id="field_freguesiaProposta" required class="form-control select2 select2-nosearch" tabindex="-1"
                                aria-hidden="true">
                            <option></option>
                            <?php foreach($Freguesias as $rowStatus) : ?>
                                <option value="<?php echo $rowStatus['id_freguesia']; ?>"
                                ><?php echo $rowStatus['freguesia']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field_uploadDocumentos"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_DOCUMENTOS') ?></label>
                    <input type="file" name="field_uploadDocumentos" id="field_uploadDocumentos" accept="application/pdf" />
                </div>

                <div class="form-group">
                    <input type="checkbox" required id="politicaPrivacidade" name="politicaPrivacidade" value="politicaPrivacidade">
                    <label for="politicaPrivacidade"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_TERMOSCONDICOES_INTRO') ?> </label>
                    <div id="buttonTermosCondicoes"><?php echo JText::_('COM_VIRTUALDESK_OP_FORM_TERMOSCONDICOES_BUTTON') ?></div>


                    <script type="text/javascript">
                        jQuery( document ).ready(function() {
                            jQuery( "#buttonTermosCondicoes" ).click(function() {
                                jQuery("#TermosCondicoes").css('display','block');
                                jQuery("html").css('overflow-y','hidden');
                            });

                            jQuery( "#fechaTermosCondicoes" ).click(function() {
                                jQuery('#TermosCondicoes').css('display','none');
                                jQuery("html").css('overflow-y','scroll');
                            });
                        })
                    </script>

                    <div id="TermosCondicoes">
                        <div class="overlay">
                            <div class="box">
                                <div id="fechaTermosCondicoes"><svg version="1.2" baseProfile="tiny" id="Cinza" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="53px" height="53px" viewBox="0 0 53 53" xml:space="preserve">
                    <path fill-rule="evenodd" fill="none" stroke="#9B9B9B" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                        M40.361,44.02c-2.743,0.231-4.286-2.144-5.68-3.896c-2.141-2.691-4.753-4.867-6.954-7.45c-0.958-1.123-1.608-0.955-2.537,0.007
                        c-2.89,2.994-5.877,5.896-8.769,8.89c-1.793,1.855-3.788,3.535-6.348,1.938c-2.1-1.312-1.453-4.1,1.199-6.736
                        c2.729-2.711,5.35-5.537,8.187-8.128c1.688-1.543,2.01-2.516,0.081-4.224c-3.197-2.835-6.043-6.061-9.126-9.029
                        c-1.779-1.714-2.129-3.586-0.47-5.324c1.716-1.799,3.693-1.316,5.367,0.341c3.118,3.086,6.287,6.126,9.273,9.336
                        c1.375,1.478,2.272,1.654,3.697,0.093c2.927-3.202,6.001-6.269,9.045-9.362c1.172-1.191,2.556-2.02,4.303-1.422
                        c1.453,0.495,2.079,1.743,2.433,3.138c0.277,1.091,0.059,1.824-0.908,2.713c-3.495,3.219-6.729,6.72-10.186,9.982
                        c-1.425,1.345-1.107,2.133,0.145,3.334c3.32,3.181,6.526,6.481,9.748,9.763c1.147,1.168,1.53,2.524,0.764,4.103
                        C42.996,43.384,41.986,44.001,40.361,44.02z"></path>
                </svg></div>

                                <h2><?php echo JText::_('COM_VIRTUALDESK_OP_TERMOSCONDICOES_TITULO'); ?></h2>

                                <div class="linha"></div>

                                <?php echo JText::_('COM_VIRTUALDESK_OP_TERMOSCONDICOES'); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>


            <?php if ($captcha_plugin!='0') : ?>
                <div class="form-group" >
                    <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                    $field_id = 'dynamic_recaptcha_1';
                    print $captcha->display($field_id, $field_id, 'g-recaptcha');
                    ?>
                    <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                </div>
            <?php endif; ?>


            <div style="display:none;"><input type="submit" name="saveproposta" id="saveproposta" value="Enviar Proposta"></div>
        </form>

    </div>

<?php
    }
?>

<?php echo $headScripts; ?>
<?php echo $footerScripts; ?>
<?php echo $recaptchaScripts; ?>
<?php echo $localScripts;?>


