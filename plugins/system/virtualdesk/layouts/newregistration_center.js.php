<?php
    defined('_JEXEC') or die;
?>

var slideInicio = 1;

var NewRegistration = function() {

    var handleRegister = function() {

        jQuery('#member-registration').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                hiddenRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },

                password1: {
                    vdpasscheck: ['', 'password1',''],
                    required: true
                },

                password2: {
                    vdpasscheck: ['', 'password2','password1'],
                    required: true
                },

                email2: {
                    equalTo: "#email1"
                },

                fiscalid: {
                    vdnifcheck: ['', 'fiscalid']
                },
                // o campo de login pode ser NIF, tem outra varíavle que permite retorna a validação true se não for fo tipo NIF
                login: {
                    vdnifcheck: ['', 'login'],
                    required: true
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
                hiddenRecaptcha: {
                    required: jQuery('#hiddenRecaptcha').attr('messageValidation')
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    jQuery("#MainMessageAlertBlock").find('span').html(message);
                    jQuery("#MainMessageAlertBlock").show();
                }
                // else {
                //     $("#").hide();
                // }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.attr("type") === "checkbox") { // insert checkbox errors after the container
                    error.insertAfter(element.closest('div.form-group').find('div.errorLabel'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                // form[0].submit();
                jQuery('#cover-spin').show(0);
                form.submit();
            }
        });

        jQuery('#member-registration').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#member-registration').validate().form()) {
                    jQuery('#member-registration').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {

            handleRegister();
        }

    };

}();

(function($) {
    $.fn.fadeImages = function(options) {
        var opt = $.extend({
            time: 2000, //动画间隔时间
            fade: 1000, //淡入淡出的动画时间
            dots: false, //是否启用图片按钮
            arrows: false, //上一张，下一张
            inicio: slideInicio,
            complete: function() {} //淡入完成后的回调函数
        }, options);
        var t = parseInt(opt.time),
            f = parseInt(opt.fade),
            d = opt.dots,
            i = opt.inicio,
            j = 0,
            l, m, set, me, cb = opt.complete,
            a = opt.arrows;
        m = $(this).find("ul li");
        m.hide();
        me = $(this);
        l = m.length;

        // 如果没有一张图片
        if (l <= 0) {
            return false;
        }
        // 如果开启底部按钮
        if (d) {
            $(this).append("<div id='dots'></div>");
            for (j = 0; j < l; j++) {
                $(this).find("#dots").append("<a>" + (j + 1) + "</a>");
            }
            $(this).find("#dots a").eq(0).addClass("active");
            // 底部按钮点击切换
            $(this).on("click", "#dots a", function(event) {
                event.preventDefault();
                clearTimeout(set);
                i = $(this).index();
                var mapID = i + 1;
                dots(i);
                show(i);
            });

        }
        // 如果开启ARROW
        if (a) {
            $(this).append("<a href='#' class='arrow prev'><img src='<?php echo $baseurl;?>images/v_agenda/icons/seta2.png' alt='prev'/></a><a href='#' class='arrow next'><img src='<?php echo $baseurl;?>images/v_agenda/icons/seta3.png' alt='next'/></a>");
            $(this).on("click", ".arrow.prev", function(event) {
                event.preventDefault();
                jQuery('html').css('overflow-y', 'hidden');
                clearTimeout(set);
                i = me.find(".curr").index() - 1;
                if (i <= -1) {
                    i = l - 1;
                }
                dots(i);
                show(i);

            });
            $(this).on("click", ".arrow.next", function(event) {
                event.preventDefault();
                jQuery('html').css('overflow-y', 'hidden');
                clearTimeout(set);
                i = me.find(".curr").index() + 1;
                if (i >= l) {
                    i = 0;
                }
                dots(i);
                show(i);
            });
        }
        // 初始化
        show(0);
        play(0);
        // 图片切换
        function show(i) {
            m.eq(i).siblings().css("z-index", 1).removeClass("curr").stop(true, true).css('display','none');
            m.eq(i).addClass("curr").css("z-index", 2).stop(true, true).fadeIn(f, cb);
        }

        //逗点切换
        function dots(i) {
            me.find("#dots a").eq(i).addClass("active").siblings().removeClass();
        }

        //图片自动播放函数
        function play(i) {
            if (i >= l - 1) {
                i = -1;
            }
            set = setTimeout(function() {
                show(i);
                dots(i);
                play(i)
            }, t + f);
            i++;

            return i;
        }

        jQuery('.btn.credits').hover(function() {
            i = me.find(".curr").index();
            jQuery('.credito.credito' + i).css('display','block');
            clearTimeout(set);
        }, function() {
            i = me.find(".curr").index();
            jQuery('.credito.credito' + i).css('display','none');
            play(i);
        });


        //鼠标经过停止与播放
        /*me.hover(function() {
            i = me.find(".curr").index();
            clearTimeout(set);
        }, function() {
            i = me.find(".curr").index();
            play(i);
        });*/

        return this;
    }
}(jQuery));


jQuery( window ).load(function() {
    var alturaEcra = jQuery( window ).height();
    var larguraLogo = jQuery('.sliderMotion').width();
    jQuery('.loginLogo').css('height',alturaEcra);
    jQuery('.loginContent').css('min-height',alturaEcra);
});


jQuery( window ).resize(function() {
    var alturaEcra = jQuery( window ).height();
    var larguraLogo = jQuery('.sliderMotion').width();
    jQuery('.loginLogo').css('height',alturaEcra);
    jQuery('.loginContent').css('min-height',alturaEcra);
});

jQuery(document).ready(function() {

    var altura = jQuery(window).height();

    jQuery('.site.login').css('height',altura);

    jQuery.validator.addMethod("vdpasscheck", function(value, element, params) {
        var password1      = null;
        var password2      = null;
        var resPassCheckJS = null;

        if(params[1]!='') password1 =  jQuery('#'+params[1]);
        if(params[2]!='') password2 =  jQuery('#'+params[2]);

        if( password2 === null )
        {
            resPassCheckJS = jQuery().virtualDeskPasswordCheck(password1.val(),'',virtualDeskPassCheckOptions);
        }
        else
        {
            resPassCheckJS = jQuery().virtualDeskPasswordCheck(password1.val(),password2.val(),virtualDeskPassCheckOptions);
        }

        params[0] = resPassCheckJS.message;
        return resPassCheckJS.return ;
    }, jQuery.validator.format(" {0} "));


    jQuery.validator.addMethod("vdnifcheck", function(value, element, params) {
        // Verifica se o campo atual é o username/login e não for do tipo NIF então sai
        // valida para as restantes situações: o campo é do tipo nif e não é de´login/username ou é login e é do tipo NIF
        if(setUserFieldLoginTypeNIF_JS<1 && params[1]=='login') return true;

        var NIFElem = null;
        if(params[1]!='') NIFElem =  jQuery('#'+params[1]);
        // Se o campo não estiver preenchido não deve dar erro de NIF
        if(NIFElem.val()=='' || NIFElem.val()==undefined) return true;

        var resNIFCheckJS = jQuery().virtualDeskNIFCheck(NIFElem.val(),virtualDeskNIFCheckOptions);
        params[0] = resNIFCheckJS.message;
        return resNIFCheckJS.return ;
    }, jQuery.validator.format(" {0} "));


    NewRegistration.init();

    jQuery(".sliderMotion").fadeImages({
        // interval
        time: 5000,
        // animation speed
        fade: 0,
        inicio: slideInicio,
        // callback
        complete:function() {}
    });

    jQuery(".make-switch").bootstrapSwitch();

});