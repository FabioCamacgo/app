<?php
defined('JPATH_BASE') or die;
defined('_JEXEC') or die;


JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');


 /* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('forgotpassword');
if ($resPluginEnabled === false) exit();



$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$config          = JFactory::getConfig();
$sitename        = $config->get('sitename');
$sitedescription = $config->get('sitedescription');
$sitetitle       = $config->get('sitetitle');
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$logoFile        = $templateParams->get('logoFile');
$fluidContainer  = $config->get('fluidContainer');
$page_heading    = $config->get('page_heading');
$baseurl         = JUri::base();
$rooturl         = JUri::root();
$captcha_plugin  = JFactory::getConfig()->get('captcha');
$templateName    = 'virtualdesk';

// Se não estiver ativo o pedido de nova senha devemos parar o processamento
$configSetForgotPassword = JComponentHelper::getParams('com_virtualdesk')->get('setforgotpassword');
if($configSetForgotPassword == '0' or empty($configSetForgotPassword) ) die;

// Verifica qual o layout a apresentar
$setforgotpasswordlayout = JComponentHelper::getParams('com_virtualdesk')->get('setforgotpasswordlayout');

// Idiomas
$lang      = JFactory::getLanguage();
$extension = 'com_virtualdesk';
$base_dir  = JPATH_SITE;
$jinput    = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$reload    = true;
$lang->load($extension, $base_dir, $language_tag, $reload);

//// TODO TESTE EMAIL ACTIVACÇÂO !!!!!!!!!!!!!!!!!!
/*
//$emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation_email.html');
//
//$BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_TITLE','Virtual Desk');
//$BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_GREETING','nunocosta@faceconsulting.pt');
//$BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_MESSAGE','Virtual Desk');
//$BODY_ACTIVATE_DESC = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_ACTIVATE_DESC');
//$BODY_ACTIVATE_LINK = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_ACTIVATE_LINK','http://localhost/dasfsdfsdfsdfsdfsdfsdfsdfds');
//$BODY_FOOTER        = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_FOOTER','VD - Virtual Desk','nunocosta@faceconsulting.pt');
//
//$body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
//$body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
//$body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
//$body      = str_replace("%BODY_ACTIVATE_DESC", $BODY_ACTIVATE_DESC, $body);
//$body      = str_replace("%BODY_ACTIVATE_LINK", $BODY_ACTIVATE_LINK, $body);
//$body      = str_replace("%BODY_FOOTER",$BODY_FOOTER, $body);
//
//// Send the password reset request email.
//$newActivationEmail = JFactory::getMailer();
//$newActivationEmail->Encoding = 'base64';
//$newActivationEmail->isHtml(true);
//$newActivationEmail->setBody($body);
//$newActivationEmail->addReplyTo('nunocosta2004@gmail.com');
//$newActivationEmail->setSender('nunocosta2004@gmail.com');
//$newActivationEmail->addRecipient('nunocosta@faceconsulting.pt');
//$newActivationEmail->setSubject('VD subject');
//$return = $newActivationEmail->send();
*/
//// TODO TESTE !!!!!!!!!!!!!!!!!!


// Verifica se foi realizado um post dos dados
$resCheckFormSubmit = VirtualDeskSiteUserHelper::checkForgotPasswordFormData();
$arrayMessages = array();
$data          = array(); // inicializa array com dados
$resNewUser    = false;  // poor defeito... não criou ainda o utilizdor...
if($resCheckFormSubmit)
{
    // Se sim começa o processo de criação e activação de um novo utilizador
    // antes de tudo valida o recaptha ... se der erro não continua o processo
    if ($captcha_plugin!='0') {
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('g-recaptcha-response', '', 'string');
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        if (!$resRecaptchaAnswer) {

            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_CAPTCHA'), 'error');
        }
        else
        { // Pode carregar e validar os dados submetidos
          $model = JModelLegacy::getInstance('Profile', 'VirtualDeskModel');
          $data  = $model->getForgotPasswordPostedFormData();
          //validação dos dados submetidos
          $resDataValid = $model->validateForgotPasswordBeforeSave($data);

        // Só passa o seguinte se não tiver erros. Se tiver erros serão apresentados no bloco no inicio do formulário (processado no include em baixo)
        if($resDataValid===true){
            //Gravação do utilizador
            $resNewUser =  VirtualDeskSiteUserHelper::saveForgotPasswordRequest($data);
            // A mensagem de sucesso já foi colocada no método anterior... utiliza depois esta variável para colocar o botão "Proceed to Login"
            }
        }
    }
}
else
{ // não foi feita uma submissão de dados ou ocorreu alguma erro... limpa objecto $data
  $data = VirtualDeskSiteUserHelper::getForgotPasswordCleanPostedData();
}

// Para garantir que não dá uma exception no php... inicializo o $data
$data = VirtualDeskSiteUserHelper::getForgotPasswordSetIfNullPostData($data);

// Logo file or site title param
if (empty($logoFile)) {
    $logoFile = $baseurl . '/templates/' . $templateName . '/images/logo/logo_virtualdesk_100.png';
}
else {
    $logoFile = $baseurl . $logoFile;
}

if((string)$setforgotpasswordlayout==''){
    $setforgotpasswordlayout = 'forgotpassword_center.php';
}
else {
    $setforgotpasswordlayout .= '.php';
}
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . $setforgotpasswordlayout);
?>