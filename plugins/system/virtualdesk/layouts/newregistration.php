<?php
defined('JPATH_BASE') or die;
defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');


$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$config          = JFactory::getConfig();
$sitename        = $config->get('sitename');
$sitedescription = $config->get('sitedescription');
$sitetitle       = $config->get('sitetitle');
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$logoFile        = $templateParams->get('logoFile');
$fluidContainer  = $config->get('fluidContainer');
$page_heading    = $config->get('page_heading');
$baseurl         = JUri::base();
$rooturl         = JUri::root();
//$this->language  = $doc->language;
//$this->direction = $doc->direction;
$captcha_plugin  = JFactory::getConfig()->get('captcha');
$templateName    = 'virtualdesk';

// Se não estiver ativo o registo de novos utilizadores devemos parar o processamento
$configSetNewRegistration = JComponentHelper::getParams('com_virtualdesk')->get('setnewregistration');
if($configSetNewRegistration == '0' or empty($configSetNewRegistration) ) die;

// Verifica qual o layout a apresentar
$setregistrationlayout = JComponentHelper::getParams('com_virtualdesk')->get('setregistrationlayout');

// Parametros da Password Chyeck de modo a podermos utilizar na validação javascript
$passwordcheck_length      = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_length', 8);
$passwordcheck_no_name     = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_name');
$passwordcheck_no_email    = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_email');
$passwordcheck_types_azmin = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmin');
$passwordcheck_types_azmai = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmai');
$passwordcheck_types_num   = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_num');
$passwordcheck_types_special = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_special');


// Idiomas
$lang = JFactory::getLanguage();
$extension = 'com_virtualdesk';
$base_dir = JPATH_SITE;
//$language_tag = $lang->getTag(); // loads the current language-tag
$jinput = JFactory::getApplication('site')->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);

//// TODO TESTE EMAIL ACTIVACÇÂO !!!!!!!!!!!!!!!!!!
//$emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation_email.html');
//
//$BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_TITLE','Virtual Desk');
//$BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_GREETING','nunocosta@faceconsulting.pt');
//$BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_MESSAGE','Virtual Desk');
//$BODY_ACTIVATE_DESC = JText::_('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_ACTIVATE_DESC');
//$BODY_ACTIVATE_LINK = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_ACTIVATE_LINK','http://localhost/dasfsdfsdfsdfsdfsdfsdfsdfds');
//$BODY_ACTIVATE_LINK_LABEL = JText::_('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_ACTIVATE_LINK_LABEL');
//$BODY_FOOTER        = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_FOOTER','VD - Virtual Desk','nunocosta@faceconsulting.pt');
//
//$body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
//$body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
//$body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
//$body      = str_replace("%BODY_ACTIVATE_DESC", $BODY_ACTIVATE_DESC, $body);
//$body      = str_replace("%BODY_ACTIVATE_LINK", $BODY_ACTIVATE_LINK, $body);
//$body      = str_replace("%BODY_ACTIVATE_LABELLINK", $BODY_ACTIVATE_LINK_LABEL, $body);
//$body      = str_replace("%BODY_FOOTER",$BODY_FOOTER, $body);
//
//// Send the password reset request email.
//$newActivationEmail = JFactory::getMailer();
//$newActivationEmail->Encoding = 'base64';
//$newActivationEmail->isHtml(true);
//$newActivationEmail->setBody($body);
//$newActivationEmail->addReplyTo('nunocosta@faceconsulting.pt');
//$newActivationEmail->setSender('nunocosta@faceconsulting.pt');
//$newActivationEmail->addRecipient('nunocosta@faceconsulting.pt');
//$newActivationEmail->setSubject('VD subject');
//$return = $newActivationEmail->send();
//
//exit();
//// TODO TESTE !!!!!!!!!!!!!!!!!!


// Verifica se foi realizado um post dos dados para registar o novo utilizador
$resCheckFormSubmit = VirtualDeskSiteUserHelper::checkNewUserRegistrationFormData();
$arrayMessages = array();
$data          = array(); // inicializa array com dados
$resNewUser    = false;  // poor defeito... não criou ainda o utilizdor...
if($resCheckFormSubmit)
{
    // Se sim começa o processo de criação e activação de um novo utilizador
    // antes de tudo valida o recaptha ... se der erro não continua o processo
    if ($captcha_plugin!='0') {
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('g-recaptcha-response', '', 'string');
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        if (!$resRecaptchaAnswer) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'), 'error');
        }
        else
        { // Pode carregar e validar os dados submetidos
          $model = JModelLegacy::getInstance('Profile', 'VirtualDeskModel');
          $data  = $model->getNewRegistrationPostedFormData();
          //validação dos dados submetidos
          $resDataValid = $model->validateNewRegistrationBeforeSave($data);

        // Só passa o seguinte se não tiver erros. Se tiver erros serão apresentados no bloco no inicio do formulário (processado no include em baixo)
        if($resDataValid===true){
            //Gravação do utilizador
            $resNewUser =  VirtualDeskSiteUserHelper::saveNewUserRegistration($data);
            // A mensagem de sucesso já foi colocada no método anterior... utiliza depois esta variável para colocar o botão "Proceed to Login"
            }
        }
    }
}
else
{ // não foi feita uma submissão de dados ou ocorreu alguma erro... limpa objecto $data
  $data = VirtualDeskSiteUserHelper::getNewRegistrationCleanPostedData();
}

// Para garantir que não dá uma exception no php... inicializo o $data
$data = VirtualDeskSiteUserHelper::getNewRegistrationSetIfNullPostData($data);

// Verifica se o campo de login é do tipo Text (livre) ou do tipo NIF
//$setUserFieldLoginType = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
//if($setUserFieldLoginType=="login_as_nif") {
//    $CampoForm_Username_Label = JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL');
//    $setUserFieldLoginTypeNIF = true;
//    $CampoForm_Username_MaxLength = "9" ;
//    $setUserFieldLoginTypeNIF_JS = 1;
//}
//else {
//    $CampoForm_Username_Label = JText::_('COM_VIRTUALDESK_USER_LOGIN_LABEL');
//    $setUserFieldLoginTypeNIF = false;
//    $CampoForm_Username_MaxLength = "250" ;
//    $setUserFieldLoginTypeNIF_JS = -1;
//}

//require_once(JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
$ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
$arUserFieldsConfig = $ObjUserFields->getUserFields();
$arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();

// Logo file or site title param
if (empty($logoFile))
{	$logoFile = $baseurl . '/templates/' . $templateName . '/images/logo/logo_virtualdesk_100.png'; }
else
{ $logoFile = $baseurl . $logoFile;}

if((string)$setregistrationlayout==''){
$setregistrationlayout = 'newregistration_center.php';
}
else {
$setregistrationlayout .= '.php';
}
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR. $setregistrationlayout);
?>