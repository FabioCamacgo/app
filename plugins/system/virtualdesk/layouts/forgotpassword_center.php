<?php
    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $templateName  = 'virtualdesk';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';

    $obParam      = new VirtualDeskSiteParamsHelper();

    $imageBackground = $obParam->getParamsByTag('imgFundoAPP');
    $logoInterface = $obParam->getParamsByTag('logoInterfaceBlack');
    $versao = $obParam->getParamsByTag('versaoAPP');
    $linkVersao = $obParam->getParamsByTag('linkVersaoAPP');
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
    $LinkInterface = $obParam->getParamsByTag('LinkInterface');
    $LogoEntrada = $obParam->getParamsByTag('logoEntradaAPP');
    $ligaLogoInterface = $obParam->getParamsByTag('ligaLogoInterface');
    $ligaversaoSoftware = $obParam->getParamsByTag('ligaversaoSoftware');
    $LogoEntradaCorporate = $obParam->getParamsByTag('LogoEntradaCorporate');
    $LogoCssFile = $obParam->getParamsByTag('cssFileGeral');
    $IconPasswordBalcaoOnline = $obParam->getParamsByTag('passwordBalcaoOnline');
    $IconLoginBalcaoOnline = $obParam->getParamsByTag('loginBalcaoOnline');
    $brasaoMunicipio = $obParam->getParamsByTag('brasaoMunicipio');
    $sliderLogin = $obParam->getParamsByTag('sliderLogin');
    $textoSliderLogin = $obParam->getParamsByTag('textoSliderLogin');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
            $fileLangSufix = 'pt_PT';
        break;
      default:
            $fileLangSufix = substr($language_tag, 0, 2);
        break;
    }

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/' . $LogoCssFile . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;


    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts  = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini.'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag.$addscript_end;;
    }

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $doc->language; ?>" dir="<?php echo $doc->direction; ?>">
<!--<![endif]-->

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php echo $headCSS; ?>
    </head>


    <body class="site login">

        <div class="contentMain">

            <div class="loginLogo">
                <div class="sliderMotion">
                    <ul>
                        <li>
                            <a href="<?php echo $baseurl; ?>">
                                <img src="<?php echo $baseurl; ?>/<?php echo $LogoEntrada ?>"  alt="logo" class="logo-default"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="loginContent">
                <?php
                    if(!empty($brasaoMunicipio)){
                        ?>
                        <a href="<?php echo $LinkCopyright;?>">
                            <div class="brasao">
                                <img src="<?php echo $baseurl . '/' . $brasaoMunicipio;?>" alt="brasao"/>
                            </div>
                        </a>
                        <?php
                    }

                    $messages       = $app->getMessageQueue();
                    $messagesDanger = '';
                    $messagesSucess = '';
                    if(!is_array($messages)) $messages = array();
                    foreach($messages as $chvMsg => $valMsg)
                    {
                        if($valMsg['type']=='warning' or $valMsg['type']=='error')
                        {
                            $messagesDanger .= '<i class="fa-lg fa fa-warning"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                        }
                        elseif ($valMsg['type']=='message')
                        {
                            $messagesSucess .= '<i class="fa-lg fa fa-check"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                        }
                    }
                    // Apenas mais uma verificação para que não surja a mensgen de sucesso se deu erro na criação do utilizador
                    // a variável vem do script forgotpassword.php
                    if($resNewUser!==true) $messagesSucess = '';

                ?>

                <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                </div>

                <?php if (!empty($messagesDanger)) : ?>
                    <div id="MainMessageAlertBlock2Joomla" class="alert alert-danger fade in" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php echo $messagesDanger; ?>
                    </div>
                <?php endif; ?>


                <?php if (!empty($messagesSucess)) : ?>
                    <div id="MainMessageSucessBlock" class="alert alert-success fade in" >
                        <h4 class="alert-heading"><?php echo $messagesSucess; ?></h4>
                        <div class="text-center"><a class="btn blue" style="margin-top:25px;" href="<?php echo JRoute::_('index.php'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?></a></div>
                    </div>
                <?php endif; ?>

                <script>
                    var MessageAlert = new function() {
                        this.getRequiredMissed = function (nErrors) {
                            if (nErrors == null)
                            { return(''); }
                            if(nErrors==1)
                            { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                            else  if(nErrors>1)
                            { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                                return (msg.replace("%s",nErrors) );
                            }
                        };
                    }
                </script>

                <form <?php if(!empty($messagesSucess) && empty($messagesDanger)) echo 'style="display:none;" ';?>  id="recover-password" action="<?php echo JRoute::_('?token&captcha&lang='.$language_tag); ?>" method="post"  class="register-form" enctype="multipart/form-data" >

                    <h3 class="form-title"><?php echo JText::_('COM_VIRTUALDESK_FORMS_FORGOTPASSWORD_TITLE'); ?></h3>

                    <p class="hint"> <?php echo JText::_('COM_VIRTUALDESK_FORMS_FORGOTPASSWORD_DESC'); ?></p>

                    <div class="form-group">
                        <label for="email1" class="control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_LABEL'); ?></label>
                        <div class="icon"><img src="<?php echo $baseurl . $IconLoginBalcaoOnline;?>" alt="loginImg"/></div>
                        <input type="email" class="form-control placeholder-no-fix" autocomplete="off" required name="email1" id="email1" maxlength="250" value="<?php echo htmlentities($data['email1'], ENT_QUOTES, 'UTF-8'); ?>">
                    </div>

                    <?php if ($captcha_plugin!='0') : ?>
                        <div class="form-group" >
                            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                            $field_id = 'dynamic_recaptcha_1';
                            print $captcha->display($field_id, $field_id, 'g-recaptcha');
                            ?>
                            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                        </div>
                    <?php endif; ?>

                    <div class="form-actions">
                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span></button>
                        <a class="btn default" href="<?php echo JRoute::_('index.php'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                    </div>
                    <input type="hidden" name="newForgotPassword" value="newForgotPassword">
                    <?php echo JHtml::_('form.token'); ?>

                </form>
            </div>

        </div>

        <div id="cover-spin"></div>

        <?php
            echo $headScripts;
            echo $recaptchaScripts;
            echo $footerScripts;
        ?>

        <script>
            <?php
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/forgotpassword_center.js.php');
            ?>
        </script>

    </body>

</html>