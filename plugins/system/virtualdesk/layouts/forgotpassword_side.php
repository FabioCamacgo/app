<?php
defined('JPATH_BASE') or die;
defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$templateName  = 'virtualdesk';
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';

$obParam      = new VirtualDeskSiteParamsHelper();

$imageBackground = $obParam->getParamsByTag('imgFundoAPP');
$logoInterface = $obParam->getParamsByTag('logoInterfaceBlack');
$versao = $obParam->getParamsByTag('versaoAPP');
$linkVersao = $obParam->getParamsByTag('linkVersaoAPP');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
$LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
$LinkInterface = $obParam->getParamsByTag('LinkInterface');
$LogoEntrada = $obParam->getParamsByTag('logoEntradaAPP');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}


//GLOBAL SCRIPTS
$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;

// PAGE LEVEL PLUGIN SCRIPTS
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/backstretch/jquery.backstretch.min.js' . $addscript_end;
/*
* Tive de retirar o login-5.js para poder "customizar" a entrada com a validação, etc... passei para o ficheiro forgotpassword.js
* Devido a isso tive de colocar o código para as imagens de background no final deste ficheiro
*/
//$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/login-5.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$headScripts .= $addscript_ini .$baseurl . 'plugins/system/virtualdesk/layouts/forgotpassword.js' . $addscript_end;

//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login-5.min.css'. $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;


// Recaptha - utilização do plugin interno do joomla
// check params for recpacha
//$captcha_plugin  = JFactory::getConfig()->get('captcha');
$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts  = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini.'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag.$addscript_end;;
}



?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $doc->language; ?>" dir="<?php echo $doc->direction; ?>">
<!--<![endif]-->

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <?php echo $headCSS; ?>

    <style>
        .user-login-5 .login-container > .login-content { margin-top: 50px; }
        .user-login-5 .login-logo.login-6 { top: 2.5em; left: 2.5em; }
        .user-login-5 .login-container > .login-content > .login-form { margin-top: 10px;  }
        .control-label {font-size: 14px; color: #4e5a64; text-align: right;  margin-bottom: 0; padding-top: 7px;}
        .help-block { margin-top: -20px;  margin-bottom: 15px;}
    </style>
</head>

<body class=" login">

<script>
    arrayVDPagesLoginBackGround = [
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg1.jpg",
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg2.jpg",
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg3.jpg"
    ];
</script>

<!-- Body -->
<!-- BEGIN : LOGIN PAGE 5-2 -->
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 login-container bs-reset">
            <div class="row">
                <!-- BEGIN LOGO -->
                <div class="logo">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo $baseurl; ?>">
                                <img src="<?php echo $baseurl; ?>/<?php echo $LogoEntrada ?>"  alt="logo" class="logo-default"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="login-content">

                <?php
                // Verifica mensagens de erro/sucesso do joomla/php
                $messages       = $app->getMessageQueue();
                $messagesDanger = '';
                $messagesSucess = '';
                if(!is_array($messages)) $messages = array();
                foreach($messages as $chvMsg => $valMsg)
                {
                    if($valMsg['type']=='warning' or $valMsg['type']=='error')
                    {
                        $messagesDanger .= '<i class="fa-lg fa fa-warning"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                    }
                    elseif ($valMsg['type']=='message')
                    {
                        $messagesSucess .= '<i class="fa-lg fa fa-check"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                    }
                }
                // Apenas mais uma verificação para que não surja a mensgen de sucesso se deu erro na criação do utilizador
                // a variável vem do script forgotpassword.php
                if($resNewUser!==true) $messagesSucess = '';

                ?>


                <h1><?php echo JText::_('COM_VIRTUALDESK_FORMS_FORGOTPASSWORD_TITLE'); ?></h1>
                <p><?php echo JText::_('COM_VIRTUALDESK_FORMS_FORGOTPASSWORD_DESC'); ?></p>


                <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                </div>

                <?php if (!empty($messagesDanger)) : ?>
                    <!-- bloco para mensagens de erro do joomla/php -->
                    <div id="MainMessageAlertBlock2Joomla" class="alert alert-danger fade in" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php echo $messagesDanger; ?>
                    </div>
                <?php endif; ?>


                <?php
                // bloco para mensagens de sucesso do joomla/php
                if (!empty($messagesSucess)) : ?>
                    <div id="MainMessageSucessBlock" class="alert alert-success fade in" >
                        <h4 class="alert-heading"><?php echo $messagesSucess; ?></h4>
                        <div class="text-center"><a class="btn blue" style="margin-top:25px;" href="<?php echo JRoute::_('index.php'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?></a></div>
                    </div>
                <?php endif; ?>

                <script>
                    <!-- Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (forgotpassword.js) -->
                    var MessageAlert = new function() {
                        this.getRequiredMissed = function (nErrors) {
                            if (nErrors == null)
                            { return(''); }
                            if(nErrors==1)
                            { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                            else  if(nErrors>1)
                            { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                                return (msg.replace("%s",nErrors) );
                            }
                        };
                    }
                </script>

                <form <?php if(!empty($messagesSucess) && empty($messagesDanger)) echo 'style="display:none;" ';?>  id="member-registration" action="<?php echo JRoute::_('?token&captcha&lang='.$language_tag); ?>" method="post"  class="register-form login-form" enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_LABEL'); ?></label>
                        <div class="col-md-9">
                        <input type="email" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_LABEL'); ?>" name="email1" id="email1" maxlength="250" value="<?php echo htmlentities($data['email1'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6 text-left">
                            <?php if ($captcha_plugin!='0') : ?>
                                <div class="form-group" >
                                    <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                                    $field_id = 'dynamic_recaptcha_1';
                                    print $captcha->display($field_id, $field_id, 'g-recaptcha');
                                    ?>
                                    <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="col-sm-6 text-right">
                            <div style="margin-top: 40px;">
                            <button type="submit" class="btn blue"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span></button>
                            <a class="btn default" href="<?php echo JRoute::_('index.php'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="newForgotPassword" value="newForgotPassword">
                    <?php echo JHtml::_('form.token'); ?>

                </form>
            </div>

            <div class="copyright">

                <div class="col-md-4">
                    <div class="spaceTop"><a href="<?php echo $linkVersao;?>" class="versao" target="_blank">digitalGOV V.<?php echo $versao;?></a></div>
                </div>

                <div class="col-md-4">
                    <div class="contentCopyright">
                        <?php echo $year = date("Y"); ?> ©
                        <a href="<?php echo $LinkCopyright;?>" target="_blank">
                            <?php echo $copyrightAPP; ?>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="logoInterface" style="margin-top:0;">
                        <a href="<?php echo $LinkInterface;?>" target="_blank">
                            <span class="prod">The Interface Projects <br><span class="connect">Governação Digital</span></span>
                            <img src="<?php echo $baseurl . $logoInterface;?>" alt="TIP" title=""/>
                        </a>
                    </div>
                </div>


            </div>

        </div>

        <div class="col-md-6 bs-reset">
            <img src="<?php echo $baseurl . $imageBackground; ?>" alt="imgBackground"/>
        </div>

    </div>
</div>

<div id="cover-spin"></div>

<?php echo $headScripts; ?>
<?php echo $recaptchaScripts; ?>
<?php echo $footerScripts; ?>

<script>
    jQuery(document).ready(function() {
        // init background slide images
        jQuery('.login-bg').backstretch(arrayVDPagesLoginBackGround, {
                fade: 1000,
                duration: 8000
            }
        );
    });
</script>

</body>
</html>