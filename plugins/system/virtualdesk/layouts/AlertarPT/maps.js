var MapsGoogle = function () {

    var mapBasic = function () {
        new GMaps({
            div: '#gmap_marker',
            lat: 39.21558173996407,
            lng: -8.208079057293048
        });
    }

    return {
        //main function to initiate map samples
        init: function () {
            mapBasic();
        }

    };

}();

jQuery(document).ready(function() {
    MapsGoogle.init();
});