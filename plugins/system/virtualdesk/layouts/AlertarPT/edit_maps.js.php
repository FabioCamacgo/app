<?php
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteAlertarPTNovaOcorrenciaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AlertarPT/virtualdesksite_AlertarPT_novaOcorrencia.php');

    $jinput = JFactory::getApplication()->input;
    $link = $jinput->get('link','' ,'string');
    $ref = explode("mun=", $link);

    if(empty($ref)){
        $idURL = 0;
    } else {
        if (strpos($ref[1], '&tipo=') !== false) {
            $explodeCat = explode("&tipo=", $ref[1]);
            $idURL = base64_decode($explodeCat[0]);
            $cat =  base64_decode($explodeCat[1]);
        } else {
            $idURL = base64_decode($ref[1]);
        }
    }

?>

var MapsGoogle = function () {

    var mapMarker = function () {

        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });


        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });

        map.setZoom(13);

        var everythingElse = [[-85.1054596961173,-180],[85.1054596961173,-180],[85.1054596961173,180],[-85.1054596961173,180],[-85.1054596961173,0]];

        <?php
            if($idURL == 105) {
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_105.js.php');
            } else if($idURL == 59){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_59.js.php');
            } else if($idURL == 222){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_222.js.php');
            } else if($idURL == 203){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_203.js.php');
            } else if($idURL == 57){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_57.js.php');
            } else if($idURL == 212){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_212.js.php');
            } else if($idURL == 244){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_244.js.php');
            } else if($idURL == 235){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_235.js.php');
            } else if($idURL == 133){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_133.js.php');
            } else if($idURL == 230){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_230.js.php');
            } else if($idURL == 133){
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_133.js.php');
            } else if($idURL == 213) {
                require_once(JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/delimitacoesConcelhos/conc_213.js.php');
            }
        ?>

        GMaps.on('click', map.map, function(event) {

            map.removeMarkers();

            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            jQuery('#coordenadas').val(lat + ',' + lng);

            map.addMarker({
                lat: lat,
                lng: lng,
                title: 'Marker #' + index,
                icon: iconPath,
                infoWindow: {
                    content : ''
                }

            });
        });

    }

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };

}();

<?php
    if($idURL == 0) {
        ?>
            var LatToSet = '39.21558173996407';
            var LongToSet = '-8.208079057293048';
        <?php
    } else {
        ?>
            var LatToSet = <?php echo VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getLatitude($idURL); ?>;
            var LongToSet = <?php echo VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getLongitude($idURL); ?>;
        <?php
    }
?>



/*
* InicializaÃ§Ã£o
*/

jQuery(document).ready(function() {


    var InputLatLong = jQuery('#coordenadas').val();


    if( InputLatLong!=undefined && InputLatLong!="") {
        var ArrayLatLong = InputLatLong.split(",");
        if(ArrayLatLong.length == 2) {
            LatToSet  =  ArrayLatLong[0];
            LongToSet =  ArrayLatLong[1];

        }
    }

    if( jQuery('#gmap_marker').length ) {
        MapsGoogle.init()
    }

});
