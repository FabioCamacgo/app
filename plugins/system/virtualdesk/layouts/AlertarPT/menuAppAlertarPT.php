<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $jinput = JFactory::getApplication()->input;
    $link = $jinput->get('link','' ,'string');
    $ref = explode("mun=", $link);

    if (strpos($ref[1], '&tipo=') !== false) {
        $explodeCat = explode("&tipo=", $ref[1]);
        $idURL = base64_decode($explodeCat[0]);
    } else {
        $idURL = base64_decode($ref[1]);
    }

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('menuAppAlertarPT');
    if ($resPluginEnabled === false) exit();


    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';
?>

<ul>
    <li><a href="submeter-ocorrencia?mun=<?php echo base64_encode($idURL);?>">Submeter ocorrência</a></li>
    <li><a href="fale-connosco?mun=<?php echo base64_encode($idURL);?>">Solicitar demoALERTAR.PT</a></li>
    <li><a href="fale-connosco?mun=<?php echo base64_encode($idURL);?>">Requerer orçamento</a></li>
    <li><a href="precos">Preços</a></li>
    <li><a href="fale-connosco?mun=<?php echo base64_encode($idURL);?>">Fale connosco</a></li>
</ul>
