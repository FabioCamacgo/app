<?php


    if(!empty($idURL)){
        $concelho = $idURL;
        $nameConcelho = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getConcName($idURL);

        ?>
            <script>
                document.getElementById("concelho").disabled = true;
            </script>
        <?php
    }


    if(!empty($cat)){
        $categoria = $cat;
    }

    if (isset($_POST['submitForm'])) {
        $categoria = $_POST['categoria'];
        $subcategoria = $_POST['subcategoria'];
        $descricao = $_POST['descricao'];
        $concelho = $_POST['concelho'];
        $freguesia = $_POST['freguesia'];
        $sitio = $_POST['sitio'];
        $morada = $_POST['morada'];
        $ptosrefs = $_POST['ptosrefs'];
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $emailConf = $_POST['emailConf'];
        $fiscalid = $_POST['fiscalid'];
        $telefone = $_POST['telefone'];
        $veracid2 = $_POST['veracid2'];
        $politica2 = $_POST['politica2'];
        $coordenadas = $_POST['coordenadas'];
        $splitCoord = explode(",", $coordenadas);
        $lat = $splitCoord[0];
        $long = $splitCoord[1];
        $dataAtual = date("Y-m-d");


        if($categoria == 'Escolher opção'){
            $categoria = '';
        }

        if($subcategoria == 'Escolher opção'){
            $subcategoria = '';
        }

        if($concelho == 'Escolher opção'){
            $concelho = '';
        }

        if($freguesia == 'Escolher opção'){
            $freguesia = '';
        }

        $errCategoria = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaSelect($categoria);
        $errSubcategoria = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaSelect($subcategoria);
        $errConcelho = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaSelect($concelho);
        $errFreguesia = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaSelect($freguesia);
        $errSitio = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaText($sitio);
        $errMorada = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaMorada($morada);
        $errNome = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaText($nome);
        $errEmail = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaEmail($email, $emailConf);
        $errNif = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaNIF($fiscalid);
        $errTelefone = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::validaTelefone($telefone);


        /*Validações Recaptcha*/
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        $errRecaptcha = 1;
        if((string)$recaptcha_response_field !='') {
            $jinput->set('g-recaptcha-response',$recaptcha_response_field);
            $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
            $errRecaptcha = 0;
            if (!$resRecaptchaAnswer) {
                $errRecaptcha = 1;
            }
        }

        if($errRecaptcha == 0 && $politica2 != 0 && $veracid2 != 0 && $errCategoria == 0 && $errSubcategoria == 0 && $errConcelho == 0 && $errFreguesia == 0 && $errSitio == 0 && $errMorada == 0 && $errNome == 0 && $errEmail == 0 && $errNif == 0 && $errTelefone == 0) {

            /*Gerar Referencia UNICA de alerta*/
            $refExiste = 1;

            $refCat = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getReferencia($categoria);
            $year = substr(date("Y"), -2);

            while($refExiste == 1){
                $refRandom = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::random_code();
                $referencia = $fiscalid . '-' . $refCat . strtoupper($refRandom) . '-' . $year;
                $checkREF = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            /*END Gerar Referencia UNICA de alerta*/


            $resSaveOcorrenciaMun = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::saveNewOcorrencia($referencia, $categoria, $subcategoria, $descricao, $concelho, $freguesia, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, $dataAtual);


            if($resSaveOcorrenciaMun==true) {
                ?>
                <style>
                    #btPost, #new_ocorrencia{
                        display: none !important;
                    }
                </style>

                <div class="sucesso">
                    <p>
                    <div class="sucessIcon">
                        <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/success.svg");
                        ?>
                    </div>
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_SUCESSO'); ?>
                    </p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_SUCESSO1'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_SUCESSO2'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_SUCESSO3'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_SUCESSO4'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_SUCESSO5') . '<span>' . $nameConcelho . JText::_('COM_VIRTUALDESK_ALERTARPT_SUCESSO6') . '</span>'; ?></p>
                </div>

                <?php

                echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');

                /*Send Email*/

                $catName = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getCatSelect($categoria);
                $subcatName = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getSubCatName($subcategoria);
                $fregName = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getFregName($freguesia);

                VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::SendEmailOcorrencia($referencia, $catName, $subcatName, $descricao, $concelho, $nameConcelho, $fregName, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, $dataAtual);
                VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::SendEmailOcorrenciaAdmin($referencia, $catName, $subcatName, $descricao, $concelho, $nameConcelho, $fregName, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, $dataAtual);

                /*END Send Email*/


                //exit();

            } else {
                ?>
                <div class="sucesso">
                    <p>
                    <div class="errorIcon">
                        <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/error.svg");
                        ?>
                    </div>
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_1'); ?>
                    </p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_2'); ?></p>
                </div>
                <?php

                exit();
            }

        } else {
            ?>
            <script>
                dropError();
            </script>

            <div class="backgroundErro">
                <div class="erro" style="display:block;">
                    <button id="fechaErro">
                        <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/closeMenu.svg");
                        ?>
                    </button>

                    <h2>
                        <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                        <?php echo JText::_('COM_VIRTUALDESK_ALERTA_AVISO'); ?>
                    </h2>

                    <ol>
                        <?php
                        if($errCategoria == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_CATEGORIA_1') . '</li>';
                        }else{
                            $errCategoria = 0;
                        }

                        if($errSubcategoria == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_SUBCATEGORIA_1') . '</li>';
                        }else{
                            $errSubcategoria = 0;
                        }

                        if($errConcelho == 1) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_CONCELHO_1') . '</li>';
                        }else{
                            $errConcelho = 0;
                        }

                        if($errFreguesia == 1) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_FREGUESIA_1') . '</li>';
                        }else{
                            $errFreguesia = 0;
                        }

                        if($errSitio == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_SITIO_1') . '</li>';
                        } else if($errSitio == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_SITIO_2') . '</li>';
                        }else{
                            $errSitio = 0;
                        }

                        if($errMorada == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_MORADA_1') . '</li>';
                        } else if($errMorada == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_MORADA_2') . '</li>';
                        }else{
                            $errMorada = 0;
                        }

                        if($errNome == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_NOME_1') . '</li>';
                        } else if($errNome == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_NOME_2') . '</li>';
                        }else{
                            $errNome = 0;
                        }

                        if($errTelefone == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_TELEFONE_1') . '</li>';
                        } else if($errTelefone == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_TELEFONE_2') . '</li>';
                        }else{
                            $errTelefone = 0;
                        }

                        if($errEmail == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_1') . '</li>';
                        } else if($errEmail == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_2') . '</li>';
                        } else if($errEmail == 3){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_3') . '</li>';
                        } else if($errEmail == 4){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_EMAIL_4') . '</li>';
                        }else{
                            $errEmail = 0;
                        }

                        if($errNif == 1) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_NIF_1') . '</li>';
                        }else{
                            $errNif = 0;
                        }

                        if($veracid2 == 0) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_VERACIDADE') . '</li>';
                        }

                        if($politica2 == 0) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTA_ERRO_POLITICA') . '</li>';
                        }

                        if($errRecaptcha == 1) {
                            echo '<li>Erro na verificação Não Sou um Robô (Recaptcha)</li>';
                        }else{
                            $errRecaptcha = 0;
                        }


                        ?>
                    </ol>

                </div>
            </div>
            <?php
        }

    }
?>


<form id="new_ocorrencia" action="/app/message/" method="post" class="ocorrencia-form" enctype="multipart/form-data" >

    <legend class="section"><h2><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TITLE'); ?></h2></legend>


    <!--   CATEGORIA   -->
    <div class="form-group" id="cat">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL'); ?><span class="required">*</span></label>
        <?php $categorias = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getCategorias()?>
        <div class="col-md-9">
            <select name="categoria" required value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($categoria)){
                    ?>
                    <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                    <?php foreach($categorias as $rowStatus) : ?>
                        <option value="<?php echo $rowStatus['id']; ?>"
                        ><?php echo $rowStatus['categoria']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getCatSelect($categoria) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeCat = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::excludeCat($categoria)?>
                    <?php foreach($ExcludeCat as $rowStatus) : ?>
                        <option value="<?php echo $rowStatus['id']; ?>"
                        ><?php echo $rowStatus['categoria']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>



    <!--   SUBCATEGORIA   -->
    <?php
    $ListaDeMenuMain = array();
    if(!empty($categoria)) {
        if( (int) $categoria > 0) $ListaDeMenuMain = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getSubcategoria($categoria);
    }
    ?>
    <div id="blocoSubCat" class="form-group">
        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_SUBCATEGORY_LABEL' ); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <select name="subcategoria" id="subcategoria" value="<?php echo $subcategoria;?>" required
                <?php
                if(empty($categoria)) {
                    echo 'disabled';
                }
                ?>
                    class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($subcategoria)){
                    ?>
                    <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                    <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                        <option value="<?php echo $rowMM['id']; ?>"
                        ><?php echo $rowMM['name']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $subcategoria; ?>"><?php echo VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getSubCatName($subcategoria);?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeSubCat = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::excludesubCat($categoria, $subcategoria);?>
                    <?php foreach($ExcludeSubCat as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['name']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>
    </div>



    <!--   DESCRIÇAO   -->
    <div class="form-group" id="desc">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DESCRIPTION'); ?></label>
        <div class="col-md-9">
            <textarea required class="form-control" rows="7" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_DESCRIPTION_LABEL'); ?>"
                      name="descricao" id="descricao" maxlength="5000"><?php echo $descricao; ?></textarea>
        </div>
    </div>



    <!--   CONCELHO   -->
    <div class="form-group" id="conc">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CONCELHO'); ?><span class="required">*</span></label>
        <?php $concelhos = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getConcelho()?>
        <div class="col-md-9">
            <select name="concelho" required value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getConcName($concelho) ?></option>
            </select>
        </div>
    </div>


    <!--   Freguesia   -->
    <?php
    $ListaDeFreguesias = array();
    if(!empty($concelho)) {
        if( (int) $concelho > 0) $ListaDeFreguesias = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getFreguesias($concelho);
    }
    ?>
    <div id="blocoFreg" class="form-group">
        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_FREGUESIA' ); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <select name="freguesia" id="freguesia" value="<?php echo $freguesia;?>" required
                <?php
                if(empty($concelho)) {
                    echo 'disabled';
                }
                ?>
                    class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($freguesia)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                    <?php foreach($ListaDeFreguesias as $rowMM) : ?>
                        <option value="<?php echo $rowMM['id']; ?>"
                        ><?php echo $rowMM['name']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getFregName($freguesia);?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeSubCat = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::excludeFreg($concelho, $freguesia);?>
                    <?php foreach($ExcludeSubCat as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['name']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>
    </div>


    <!--   SÍTIO   -->
    <div class="form-group" id="place">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SITIO'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_SITIO_LABEL'); ?>"
                   name="sitio" id="sitio" maxlength="250" value="<?php echo $sitio; ?>"/>
        </div>
    </div>


    <!--   MORADA   -->
    <div class="form-group" id="address">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MORADA'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_MORADA_LABEL'); ?>"
                   name="morada" id="morada" maxlength="250" value="<?php echo $morada; ?>"/>
        </div>
    </div>


    <!--   PONTOS DE REFERÊNCIA   -->
    <div class="form-group" id="ptosRef">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_REFS'); ?></label>
        <div class="col-md-9"><textarea required class="form-control" rows="7" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_REFS_LABEL'); ?>"
                                        name="ptosrefs" id="ptosrefs" maxlength="5000"><?php echo htmlentities($ptosrefs); ?></textarea>
        </div>
    </div>


    <!--   MAPA   -->
    <div class="form-group" id="mapa">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MAPA'); ?></label>
        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

        <input type="hidden" required class="form-control"
               name="coordenadas" id="coordenadas"
               value="<?php echo htmlentities($this->data->$coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>

    </div>


    <!--   Upload Imagens   -->
    <div class="form-group" id="uploadField">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_UPLOAD'); ?></label>
        <div class="col-md-9">

            <div class="file-loading">
                <input type="file" name="fileupload[]" id="fileupload" multiple>
            </div>
            <div id="errorBlock" class="help-block"></div>
            <input type="hidden" id="VDAjaxReqProcRefId">

        </div>
    </div>



    <legend class="section2"><h2 class="dadosPessoais"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DADOSPESSOAIS'); ?></h2></legend>


    <!--   NOME COMPLETO   -->
    <div class="form-group" id="name">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_NOME'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_NOME_LABEL'); ?>"
                   name="nome" id="nome" value="<?php echo htmlentities($nome, ENT_QUOTES, 'UTF-8'); ?>" maxlength="250"/>
        </div>
    </div>

    <!--   EMAIL   -->

    <div class="form-group" id="mail">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_LABEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_LABEL'); ?>"
                   name="email" id="email" value="<?php echo $email; ?>"/>
        </div>
    </div>


    <!--   VERIFICAÇÃO DE EMAIL   -->
    <div class="form-group" id="mail2">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_REPEAT_LABEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_REPEAT_LABEL'); ?>"
                   name="emailConf" id="emailConf" value="<?php echo $emailConf; ?>"/>
        </div>
    </div>

    <!--   NIF   -->
    <div class="form-group" id="nif">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_FISCALID_LABEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_FISCALID_LABEL'); ?>"
                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
        </div>
    </div>

    <!--   TELEFONE  -->
    <div class="form-group" id="telef">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TELEFONE_LABEL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_TELEFONE_LABEL'); ?>"
                   name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
        </div>
    </div>



    <legend class="section2"><h2 class="dadosPessoais"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TERMOS'); ?></h2></legend>

    <!--   Veracidade -->
    <div class="form-group" id="Veracidade">

        <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_VERACIDADE'); ?><span class="required">*</span></label>

        <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
    </div>

    <script>

        document.getElementById('veracid').onclick = function() {
            // access properties using this keyword
            if ( this.checked ) {
                document.getElementById("veracid2").value = 1;
            } else {
                document.getElementById("veracid2").value = 0;
            }
        };
    </script>

    <!--   Politica de privacidade  -->
    <div class="form-group" id="PoliticaPrivacidade">

        <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
        <label class="col-md-3 control-label">Tomei conhecimento das condições editadas em  <a href="privacidade-termos-rgpd-e-cookies" target="_blank">Privacidade, Termos, RGPD e Cookies</a> e concordo com as mesmas. <span class="required">*</span></label>

        <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
    </div>

    <script>

        document.getElementById('politica').onclick = function() {
            // access properties using this keyword
            if ( this.checked ) {
                document.getElementById("politica2").value = 1;
            } else {
                document.getElementById("politica2").value = 0;
            }
        };
    </script>


    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group" >
            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
            $field_id = 'dynamic_recaptcha_1';
            print $captcha->display($field_id, $field_id, 'g-recaptcha');
            ?>
            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
        </div>
    <?php endif; ?>



    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
    </div>

</form>