<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheConcelho');
    if ($resPluginEnabled === false) exit();


    $headScripts = $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AlertarPT/breaking-news-ticker.min.js' . $addscript_end;

    $headCSS = $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AlertarPT/breaking-news-ticker.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
    echo $headScripts;


    $jinput = JFactory::getApplication()->input;

    $link = $jinput->get('link','' ,'string');

    $ref = explode("mun=", $link);

    $id = base64_decode($ref[1]);
    $name = VirtualDeskSiteAlertarPTHelper::getNameConcelho($id);
    $color = VirtualDeskSiteAlertarPTHelper::getColorConcelho($id);
    $freguesias = VirtualDeskSiteAlertarPTHelper::getFreguesias($id);
    $indice = 0;
    $stringFreg = '';

    if(empty($id)){
        ?>
            <script>
                window.location.href = "https://alertar.pt/";
            </script>
        <?php

        exit();
    }

    $image = JUri::base() . 'images/Alertar.PT/Pin.png';

    foreach($freguesias as $rowWSL) :
        $freguesia = $rowWSL['freguesia'];

        if($indice == 0) {
            $stringFreg = '<img src="' . $image . '" alt="brasao"/>' . $freguesia;
            $indice = 1;
        } else {
            $stringFreg .= ', <img src="' . $image . '" alt="brasao"/>' . $freguesia;
        }
    endforeach;



?>



<div class="menuContent">

    <div class="contentBarra">
        <div class="brasao">
            <img src="<?php echo JUri::base()?>images/Brasoes/concelhos/c_<?php echo $id; ?>.png"/>
        </div>

        <div class="text">
            <h2><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TITLE');?></h2>
            <h3><?php echo $name; ?> <span><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_RESOLVE');?></span></h3>
        </div>


        <div class="closeMenu">
            <?php
            echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/closeMenu.svg");
            ?>
        </div>

    </div>




    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(1);?>">
        <div class="w25 catAgua">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Agua.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_1');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(2);?>">
        <div class="w25 catFlorestacao">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Florestacao.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_2');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(3);?>">
        <div class="w25 catLimpezaUrbana">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_LimpezaUrbana.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_3');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(4);?>">
        <div class="w25 catMateriais">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Materiais.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_4');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(5);?>">
        <div class="w25 catPasseios">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Passeios.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_5');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(6);?>">
        <div class="w25 catPredios">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Predios.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_6');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(7);?>">
        <div class="w25 catRecolhaLixo">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Recolha_Lixo.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_7');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(8);?>">
        <div class="w25 catEsgotos">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Rede_Esgotos.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_8');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(9);?>">
        <div class="w25 catSaude">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Saude.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_9');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(10);?>">
        <div class="w25 catTransito">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Transito.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_10');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(11);?>">
        <div class="w25 catFiscalizacao">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Fiscalizacao.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_11');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(12);?>">
        <div class="w25 catObras">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Obras.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_12');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(13);?>">
        <div class="w25 catIluminacao">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Iluminacao_Publica.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_13');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(14);?>">
        <div class="w25 catSeguranca">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Seguranca.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_14');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(15);?>">
        <div class="w25 catEquipamentos">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Equipamentos.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_15');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(16);?>">
        <div class="w25 catAcessibilidades">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Acessibilidades.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_16');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(17);?>">
        <div class="w25 catHabitacao">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Habitacao.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_17');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(18);?>">
        <div class="w25 catSocial">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Social.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_18');?></div>
            </div>
        </div>
    </a>

    <!--   -->

    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>&tipo=<?php echo base64_encode(19);?>">
        <div class="w25 catAvaliacao">
            <div class="block">
                <div class="icon">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_avaliacao.svg");
                    ?>
                </div>

                <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_19');?></div>
            </div>
        </div>
    </a>
</div>





<div class="menuBar" style="border-bottom:1px solid #ddd">

    <a href="https://alertar.pt/" title="Alertar.PT">
        <div class="logoMain">
            <img src="<?php echo JUri::base();?>images/Alertar.PT/Mapa.png" alt="mapaFP"/>
        </div>
    </a>

    <div class="botaoCat">
        <svg height="295px" version="1.1" viewBox="0 0 295 295" width="295px" xmlns="http://www.w3.org/2000/svg"><desc/><defs/>
            <g fill="none" fill-rule="evenodd" stroke="none" stroke-width="1">
                <g fill-rule="nonzero">
                    <path d="M147.421,0 C66.133,0 0,66.133 0,147.421 C0,188.389 17.259,227.846 47.351,255.676 C49.784,257.926 53.58,257.777 55.83,255.345 C58.08,252.911 57.932,249.116 55.498,246.866 C27.854,221.3 12,185.054 12,147.421 C12,72.75 72.75,12 147.421,12 C222.092,12 282.842,72.75 282.842,147.421 C282.842,222.092 222.092,282.842 147.421,282.842 C144.108,282.842 141.421,285.529 141.421,288.842 C141.421,292.155 144.108,294.842 147.421,294.842 C228.71,294.842 294.842,228.709 294.842,147.421 C294.842,66.133 228.71,0 147.421,0 Z" fill="#333"/>
                    <path d="M84.185,90.185 L210.658,90.185 C213.971,90.185 216.658,87.498 216.658,84.185 C216.658,80.872 213.971,78.185 210.658,78.185 L84.185,78.185 C80.872,78.185 78.185,80.872 78.185,84.185 C78.185,87.498 80.872,90.185 84.185,90.185 Z" fill="#333"/>
                    <path d="M84.185,153.421 L210.658,153.421 C213.971,153.421 216.658,150.734 216.658,147.421 C216.658,144.108 213.971,141.421 210.658,141.421 L84.185,141.421 C80.872,141.421 78.185,144.108 78.185,147.421 C78.185,150.734 80.872,153.421 84.185,153.421 Z" fill="#333"/>
                    <path d="M216.658,210.658 C216.658,207.345 213.971,204.658 210.658,204.658 L84.185,204.658 C80.872,204.658 78.185,207.345 78.185,210.658 C78.185,213.971 80.872,216.658 84.185,216.658 L210.658,216.658 C213.971,216.658 216.658,213.971 216.658,210.658 Z" fill="#333"/>
                </g>
            </g>
        </svg>
    </div>

    <div class="botaoMenu">
        <?php
            echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/iconMenu.svg");
        ?>
    </div>

</div>


<div class="w65">
    <div class="brasao">
        <img src="<?php echo JUri::base()?>images/Brasoes/concelhos/c_<?php echo $id; ?>.png" alt="brasao"/>
    </div>
    <div class="text">
        <h2><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TITLE');?></h2>
        <h3><?php echo $name; ?> <span><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_RESOLVE');?></span></h3>

        <div class="block">
            <div class="pin">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="126.939px" height="180.028px" viewBox="0 0 126.939 180.028" enable-background="new 0 0 126.939 180.028" xml:space="preserve">
                <path fill="<?php echo $color;?>" d="M63.46,2.939c-32.292,0-58.47,26.186-58.47,58.489c0,32.293,37.266,90.636,58.47,115.66
                    c21.159-25.024,58.489-83.367,58.489-115.66C121.949,29.119,95.76,2.939,63.46,2.939z M63.46,88.481
                    c-15.97,0-28.9-12.95-28.9-28.906c0-15.94,12.94-28.897,28.9-28.897c15.966,0,28.896,12.957,28.896,28.897
                    C92.356,75.531,79.426,88.481,63.46,88.481z"/>
            </svg>
            </div>
            <div class="info">
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_INFO_1');?></span></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_INFO_2');?></span></p>
            </div>
        </div>

    </div>
</div>


<div class="w35">
    <a href="submeter-ocorrencia?mun=<?php echo base64_encode($id);?>" class="first" style="background:<?php echo $color;?>; border-color:<?php echo $color;?>;"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_NEW_OCORRENCIA2');?></a>
    <a href="fale-connosco?mun=<?php echo base64_encode($id);?>" style="background:#fff; border-color:#212121;"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO');?></a>
</div>





<div class="alertarTicker" style="border-top:1px solid #ddd">
    <div class="topic" style="background:#e1e1e1; color:#333;"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT'); ?></div>
    <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="ticker">
        <div class="bn-news">
            <ul>
                <li><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TICKER_1') . $name; ?></li>
                <li><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TICKER_2'); ?></li>
                <li><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TICKER_3') . $stringFreg;?></li>
                <li><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TICKER_1') . $name; ?></li>
                <li><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TICKER_2'); ?></li>
                <li><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TICKER_3') . $stringFreg;?></li>
            </ul>
        </div>
        <div class="bn-controls">
            <button><span class="bn-arrow bn-prev"></span></button>
            <button><span class="bn-action"></span></button>
            <button><span class="bn-arrow bn-next"></span></button>
        </div>
    </div>
</div>


<?php
    echo $footerScripts;
?>


<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/detalheConcelho.js.php');
    ?>

    jQuery('#ticker').breakingNews();

</script>

<style>
    .bn-controls{
        opacity:0;
        z-index:-999;
    }
</style>