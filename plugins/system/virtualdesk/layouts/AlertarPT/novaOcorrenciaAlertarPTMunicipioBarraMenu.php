<?php
    $nomeConcelho = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getConcName($idURL);
?>

<div class="barraMenu">
    <div class="g-content">
        <a href="municipio?mun=<?php echo base64_encode($idURL);?>">
            <div class="brasao">
                <img src="<?php echo JUri::base()?>images/Brasoes/concelhos/c_<?php echo $idURL; ?>.png" alt="brasao"/>
            </div>

            <div class="text">
                <h2><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TITLE');?></h2>
                <h3><?php echo $nomeConcelho; ?> <span><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_RESOLVE');?></span></h3>
            </div>
        </a>

        <div class="openMenuApp">
            <?php
                echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/iconMenu.svg");
            ?>
        </div>


        <a href="https://alertar.pt/" title="Alertar.PT">
            <div class="logoMain">
                <img src="<?php echo JUri::base();?>images/Alertar.PT/Mapa.png" alt="mapaFP"/>
            </div>
        </a>

    </div>
</div>