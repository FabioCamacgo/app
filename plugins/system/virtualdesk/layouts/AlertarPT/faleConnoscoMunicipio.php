<?php

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    if(!empty($idURL)) {
        $concelho = $idURL;
        $nameConcelho = VirtualDeskSiteAlertarPTNovaOcorrenciaHelper::getConcName($idURL);
    }


    $numberRadio = 0;
    if (isset($_POST['submitForm'])) {
        $tipoContacto = $_POST['tipoContacto'];
        $assunto = $_POST['assunto'];
        $projeto = $_POST['projeto'];
        $urlProjeto = $_POST['urlProjeto'];
        $espAssunto = $_POST['espAssunto'];
        $populacao = $_POST['populacao'];
        $cat_1_Hidden = $_POST['cat_1_Hidden'];
        $cat_2_Hidden = $_POST['cat_2_Hidden'];
        $cat_3_Hidden = $_POST['cat_3_Hidden'];
        $cat_4_Hidden = $_POST['cat_4_Hidden'];
        $cat_5_Hidden = $_POST['cat_5_Hidden'];
        $cat_6_Hidden = $_POST['cat_6_Hidden'];
        $cat_7_Hidden = $_POST['cat_7_Hidden'];
        $cat_8_Hidden = $_POST['cat_8_Hidden'];
        $cat_9_Hidden = $_POST['cat_9_Hidden'];
        $cat_10_Hidden = $_POST['cat_10_Hidden'];
        $cat_11_Hidden = $_POST['cat_11_Hidden'];
        $cat_12_Hidden = $_POST['cat_12_Hidden'];
        $cat_13_Hidden = $_POST['cat_13_Hidden'];
        $cat_14_Hidden = $_POST['cat_14_Hidden'];
        $cat_15_Hidden = $_POST['cat_15_Hidden'];
        $cat_16_Hidden = $_POST['cat_16_Hidden'];
        $cat_17_Hidden = $_POST['cat_17_Hidden'];
        $cat_18_Hidden = $_POST['cat_18_Hidden'];
        $cat_19_Hidden = $_POST['cat_19_Hidden'];
        $cat_20_Hidden = $_POST['cat_20_Hidden'];
        $outraCat = $_POST['outraCat'];
        $numTecnicos = $_POST['numTecnicos'];
        $marcaDesignacao = $_POST['marcaDesignacao'];
        $marcaMunicipal = $_POST['marcaMunicipal'];
        $outroNomeProjeto = $_POST['outroNomeProjeto'];
        $obsGerais = $_POST['obsGerais'];
        $parceria = $_POST['parceria'];
        $assuntoReuniao = $_POST['assuntoReuniao'];
        $dataReuniao = $_POST['dataReuniao'];
        $horaReuniao = $_POST['m_timepicker_3'];
        $nomeReuniao = $_POST['nomeReuniao'];
        $refVagaTrabalho = $_POST['refVagaTrabalho'];
        $nomeCand = $_POST['nomeCand'];
        $emailCand = $_POST['emailCand'];
        $emailConfCand = $_POST['emailConfCand'];
        $apresentacaoVaga = $_POST['apresentacaoVaga'];
        $nomeCV = $_POST['nomeCV'];
        $emailCV = $_POST['emailCV'];
        $emailConfCV = $_POST['emailConfCV'];
        $apresentacaoCV = $_POST['apresentacaoCV'];
        $apresentacao = $_POST['apresentacao'];
        $outroAssunto = $_POST['outroAssunto'];
        $propAlertar = $_POST['propAlertar'];
        $concelho = $_POST['concelho'];
        $fiscalid = $_POST['fiscalid'];
        $email = $_POST['email'];
        $emailConf = $_POST['emailConf'];
        $nome = $_POST['nome'];
        $cargo = $_POST['cargo'];
        $dataAtual = date("Y-m-d");
        $piecesData = explode("-", $dataReuniao);
        $invertDataReuniao = $piecesData[2] . '-' . $piecesData[1] . '-' . $piecesData[0];


        /*Validações*/

        $errTipoContato = VirtualDeskSiteAlertarPTHelper::validaRadioInput($tipoContacto);

        /*Elogios e Sugestões*/
        if($tipoContacto != 5) {
            $errAssunto = VirtualDeskSiteAlertarPTHelper::validaSelect($assunto);
        } else {
            $errAssunto = 0;
        }

        /*Validações Ticket e alteraçao de modulos*/
        if(($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2)){
            $errProjeto = VirtualDeskSiteAlertarPTHelper::validaText2($projeto);
            $errUrl = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($urlProjeto);
            $errEspAssunto = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($espAssunto);
        } else {
            $errProjeto = 0;
            $errUrl = 0;
            $errEspAssunto = 0;
            $projeto = '';
            $urlProjeto = '';
            $espAssunto = '';
        }

        /*Validações Orçamento*/
        if(($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 2 && $assunto == 8)){
            $errPopulacao = VirtualDeskSiteAlertarPTHelper::validaNumberMandatory($populacao);
            $errCategorias = VirtualDeskSiteAlertarPTHelper::validaCategoriasEscolhidas($cat_1_Hidden, $cat_2_Hidden, $cat_3_Hidden, $cat_4_Hidden, $cat_5_Hidden, $cat_6_Hidden, $cat_7_Hidden, $cat_8_Hidden, $cat_9_Hidden, $cat_10_Hidden, $cat_11_Hidden, $cat_12_Hidden, $cat_13_Hidden, $cat_14_Hidden, $cat_15_Hidden, $cat_16_Hidden, $cat_17_Hidden, $cat_18_Hidden, $cat_19_Hidden, $cat_20_Hidden);
            if($cat_20_Hidden == 1){
                $errOutrasCat = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($outraCat);
            } else {
                $errOutrasCat = 0;
                $outraCat = '';
            }
            $errNumTecnicos = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($numTecnicos);
            $errMarcaDesignacao = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($marcaDesignacao);
            if($marcaDesignacao == 2) {
                $errMarcaMunicipal = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($marcaMunicipal);
            } else {
                $errMarcaMunicipal = 0;
                $marcaMunicipal = '';
            }
            if($marcaMunicipal == 99){
                $errOutraMarcaMunicipal = VirtualDeskSiteAlertarPTHelper::validaText2($outroNomeProjeto);
            } else {
                $errOutraMarcaMunicipal = 0;
                $outroNomeProjeto = '';
            }
        } else {
            $errPopulacao = 0;
            $errCategorias = 0;
            $errOutrasCat = 0;
            $errNumTecnicos = 0;
            $errMarcaDesignacao = 0;
            $errMarcaMunicipal = 0;
            $errOutraMarcaMunicipal = 0;
            $populacao = '';
            $outraCat = '';
            $numTecnicos = '';
            $marcaDesignacao = '';
            $marcaMunicipal = '';
            $outroNomeProjeto = '';
        }

        /*Validações Parceria*/
        if(($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 3 && $assunto == 16)){
            $errParceria = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($parceria);
        } else {
            $errParceria = 0;
            $parceria = '';
        }

        /*Validações Reunião*/
        if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)){
            $errAssuntoReuniao = VirtualDeskSiteAlertarPTHelper::validaText2($assuntoReuniao);
            $errDataReuniao = VirtualDeskSiteAlertarPTHelper::validaDataReuniao($dataReuniao, $invertDataReuniao, $dataAtual);
            //$errHoraReuniao = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($horaReuniao);
            $errNomeReuniao = VirtualDeskSiteAlertarPTHelper::validaText2($nomeReuniao);
        } else {
            $errAssuntoReuniao = 0;
            $errDataReuniao = 0;
            $errHoraReuniao = 0;
            $errNomeReuniao = 0;
            $assuntoReuniao = '';
            $dataReuniao = '';
            $horaReuniao = '';
            $nomeReuniao = '';
        }

        /*Validações Outro Assunto*/
        if(($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 13)){
            $errOutroAssunto = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($outroAssunto);
        } else {
            $errOutroAssunto = 0;
            $outroAssunto = '';
        }

        /*Validações Vaga Trabalho*/
        if($tipoContacto == 3 && $assunto == 15){
            $errRefVagaTrabalho = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($refVagaTrabalho);
            $errAprVagaTrabalho = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($apresentacaoVaga);
            $errNomeCand = VirtualDeskSiteAlertarPTHelper::validaText($nomeCand);
            $errEmailCand = VirtualDeskSiteAlertarPTHelper::validaEmail($emailCand, $emailConfCand);
        } else {
            $errRefVagaTrabalho = 0;
            $errAprVagaTrabalho = 0;
            $errNomeCand = 0;
            $errEmailCand = 0;
            $refVagaTrabalho = '';
            $apresentacaoVaga = '';
            $nomeCand = '';
            $emailCand = '';
        }

        /*Validações Enviar CV*/
        if($tipoContacto == 3 && $assunto == 14){
            $errAprCV = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($apresentacaoCV);
            $errNomeCV = VirtualDeskSiteAlertarPTHelper::validaText($nomeCV);
            $errEmailCV = VirtualDeskSiteAlertarPTHelper::validaEmail($emailCV, $emailConfCV);
        } else {
            $errAprCV = 0;
            $errNomeCV = 0;
            $errEmailCV = 0;
            $apresentacaoCV = '';
            $nomeCV = '';
            $emailCV = '';
            $emailConfCV = '';
        }

        /*Validações Brainstorming / Conferências / Workshops*/
        if(($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18) || ($tipoContacto == 4 && $assunto == 19)){
            $errAprBrainstorming = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($apresentacao);
        } else {
            $errAprBrainstorming = 0;
            $apresentacao = '';
        }

        /*Validações Elogios e Sugestões*/
        if($tipoContacto == 5){
            $errPropAlertar = VirtualDeskSiteAlertarPTHelper::validaNotEmpty($propAlertar);
        } else {
            $errPropAlertar = 0;
            $propAlertar = '';
        }

        /*Validações Dados Pessoais*/
        if($tipoContacto == 1 || $tipoContacto == 2 || $tipoContacto == 4 || $tipoContacto == 5 || ($tipoContacto == 3 && $assunto == 16)){
            $errConcelho = VirtualDeskSiteAlertarPTHelper::validaSelect($concelho);
            $errNif = VirtualDeskSiteAlertarPTHelper::validaNIF($fiscalid);
            $errEmail = VirtualDeskSiteAlertarPTHelper::validaEmail($email, $emailConf);
            $errNome = VirtualDeskSiteAlertarPTHelper::validaText($nome);
            $errCargo = VirtualDeskSiteAlertarPTHelper::validaText($cargo);
        } else {
            $errConcelho = 0;
            $errNif = 0;
            $errEmail = 0;
            $errNome = 0;
            $errCargo = 0;
            $concelho = '';
            $fiscalid = '';
            $email = '';
            $nome = '';
            $cargo = '';
        }

        /*Validações Recaptcha*/
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        $errRecaptcha = 1;
        if((string)$recaptcha_response_field !='') {
            $jinput->set('g-recaptcha-response',$recaptcha_response_field);
            $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
            $errRecaptcha = 0;
            if (!$resRecaptchaAnswer) {
                $errRecaptcha = 1;
            }
        }


        if($errRecaptcha == 0 && $errPropAlertar == 0 && $errNomeCV == 0 && $errEmailCV == 0 && $errAprCV == 0 && $errTipoContato == 0 && $errAssunto == 0 && $errProjeto == 0 && $errUrl == 0 && $errEspAssunto == 0 && $errPopulacao == 0 && $errCategorias == 0 && $errOutrasCat == 0 && $errNumTecnicos == 0 && $errMarcaDesignacao == 0 && $errMarcaMunicipal == 0 && $errOutraMarcaMunicipal == 0 && $errParceria == 0 && $errAssuntoReuniao == 0 && $errDataReuniao == 0 && $errHoraReuniao == 0 && $errNomeReuniao == 0 && $errOutroAssunto == 0 && $errRefVagaTrabalho == 0 && $errNomeCand == 0 && $errEmailCand == 0 && $errAprVagaTrabalho == 0 && $errAprBrainstorming == 0 && $errConcelho == 0 && $errNif == 0 && $errEmail == 0 && $errNome == 0 && $errCargo == 0){
            $dataEntrada = date("Y-m-d");

            $website = 'Alertar.PT';

            $random = VirtualDeskSiteAlertarPTHelper::getRandom(7);

            $idExterno = 'dGov-Alertar' . $random;


            if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)){
                $pieces = explode("-", $dataReuniao);
                $invertDataReuniao = $pieces['2'] . '-' . $pieces['1'] . '-' . $pieces['0'];
            } else {
                $invertDataReuniao = '';
            }

            if($tipoContacto == 5) {
                $assunto = '';
            }

            if(($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 2 && $assunto == 8)){
                $catEscolhidas = VirtualDeskSiteAlertarPTHelper::concatCatEscolhidas($cat_1_Hidden, $cat_2_Hidden, $cat_3_Hidden, $cat_4_Hidden, $cat_5_Hidden, $cat_6_Hidden, $cat_7_Hidden, $cat_8_Hidden, $cat_9_Hidden, $cat_10_Hidden, $cat_11_Hidden, $cat_12_Hidden, $cat_13_Hidden, $cat_14_Hidden, $cat_15_Hidden, $cat_16_Hidden, $cat_17_Hidden, $cat_18_Hidden, $cat_19_Hidden, $cat_20_Hidden);
            } else {
                $catEscolhidas = '';
            }

            /*Save BD*/
            $saveRegisto = VirtualDeskSiteAlertarPTHelper::saveBD($idExterno, $website, $tipoContacto, $assunto, $projeto, $urlProjeto, $espAssunto, $populacao, $catEscolhidas, $outraCat, $numTecnicos, $marcaDesignacao, $marcaMunicipal, $outroNomeProjeto, $obsGerais, $parceria, $assuntoReuniao, $invertDataReuniao, $horaReuniao, $nomeReuniao, $refVagaTrabalho, $nomeCand, $emailCand, $apresentacaoVaga, $nomeCV, $emailCV, $apresentacaoCV, $apresentacao, $outroAssunto, $propAlertar, $concelho, $fiscalid, $email, $nome, $cargo, $dataAtual);

            if($saveRegisto==true) {
                ?>
                <style>
                    #enviarPedido {
                        display: none !important;
                    }
                </style>

                <div class="sucesso">
                    <p>
                    <div class="sucessIcon">
                        <svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve"
                             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline
                                clip-rule="evenodd" fill="none" fill-rule="evenodd"
                                points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10"
                                stroke-width="2"/>
                            <path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg>
                    </div>
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_SUCESSO'); ?>
                    </p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_SUCESSO1'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_SUCESSO2'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_SUCESSO3'); ?></p>
                </div>

                <?php

                /*EMAILS*/

                /*Email Elogios e sugestões*/
                if($tipoContacto == 5){
                    VirtualDeskSiteAlertarPTHelper::SendEmailElogiosSugestões($propAlertar, $email, $nome);
                }

                /*Email Ticket e Alteraçao de modulo*/
                if(($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2)){
                    $nomeAssunto = VirtualDeskSiteAlertarPTHelper::getAssuntoName($assunto);
                    VirtualDeskSiteAlertarPTHelper::SendEmailTicket($nomeAssunto, $projeto, $urlProjeto, $espAssunto, $email, $nome);
                }

                /*Email Orçamento*/
                if(($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 2 && $assunto == 8)) {

                    $piecesCat = explode(",", $catEscolhidas);

                    for($i = 0; $i < count($piecesCat); $i++){
                        if($i == 0){
                            if($piecesCat[$i] != 20) {
                                $textCatEscolhidas = JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_' . $piecesCat[$i] );
                            }
                        } else {
                            if($piecesCat[$i] != 20) {
                                $textCatEscolhidas .= ', ' . JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_' . $piecesCat[$i] );
                            }
                        }
                    }

                    if(!empty($outraCat)){
                        $textCatEscolhidas .= ', ' . $outraCat;
                    }

                    $nameNumTecnicos = VirtualDeskSiteAlertarPTHelper::getNameNumtecnicos($numTecnicos);

                    $textMarcaDesignacao = VirtualDeskSiteAlertarPTHelper::getNameDesignacao($marcaDesignacao);

                    if ($marcaDesignacao == 2) {
                        if($marcaMunicipal == 99){
                            $textMarcaMunicipal = $outroNomeProjeto;
                        } else {
                            $textMarcaMunicipal = VirtualDeskSiteAlertarPTHelper::getNameMarcaMunicipal($marcaMunicipal);
                        }
                    } else {
                        $textMarcaMunicipal = JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_VAZIO');
                    }

                    VirtualDeskSiteAlertarPTHelper::SendEmailOrcamento($populacao, $nameNumTecnicos, $textCatEscolhidas, $textMarcaDesignacao, $textMarcaMunicipal, $email, $nome);
                }

                /*Email Parceria*/
                if(($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 3 && $assunto == 16)){
                    VirtualDeskSiteAlertarPTHelper::SendEmailParceria($parceria, $email, $nome);
                }

                /*Email Reunião*/
                if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)){
                    VirtualDeskSiteAlertarPTHelper::SendEmailReuniao($assuntoReuniao, $dataReuniao, $horaReuniao, $nomeReuniao, $email, $nome);
                }

                /*Email Convite Contratação Pública*/
                if(($tipoContacto == 1 && $assunto == 3) || ($tipoContacto == 2 && $assunto == 9)){
                    VirtualDeskSiteAlertarPTHelper::SendEmailCCP($email);
                }

                /*Email Requisicao / Compromisso */
                if(($tipoContacto == 1 && $assunto == 21) || ($tipoContacto == 2 && $assunto == 22)){
                    VirtualDeskSiteAlertarPTHelper::SendEmailCompromisso($email, $nome);
                }

                /*Email Outro Assunto*/
                if(($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 13)){
                    VirtualDeskSiteAlertarPTHelper::SendEmailOutroAssunto($outroAssunto, $email, $nome);
                }

                /*Email Solicitar Demonstração*/
                if($tipoContacto == 2 && $assunto == 11){
                    VirtualDeskSiteAlertarPTHelper::SendEmailDemonstracao($email, $nome);
                }

                /*Email Vaga Trabalho*/
                if($tipoContacto == 3 && $assunto == 15){
                    VirtualDeskSiteAlertarPTHelper::SendEmailVagaTrabalho($refVagaTrabalho, $apresentacaoVaga, $nomeCand, $emailCand);
                }

                /*Email Enviar CV*/
                if($tipoContacto == 3 && $assunto == 14){
                    VirtualDeskSiteAlertarPTHelper::SendEmailNovoCV($apresentacaoCV, $nomeCV, $emailCV);
                }

                /*Email  Brainstorming / Conferências / Workshops*/
                if(($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18) || ($tipoContacto == 4 && $assunto == 19)){
                    VirtualDeskSiteAlertarPTHelper::SendEmailBrainstorming($apresentacao, $email, $nome, $assunto);
                }

                $nomeAssunto = VirtualDeskSiteAlertarPTHelper::getAssuntoName($assunto);

                VirtualDeskSiteAlertarPTHelper::SendEmailAdmin($nomeAssunto);

                echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $idExterno . '__e__"/>');

                exit();

            } else {
                ?>
                <style>
                    #enviarPedido {
                        display: none !important;
                    }
                </style>

                <div class="sucesso">
                    <p>
                    <div class="errorIcon">
                        <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/error.svg");
                        ?>
                    </div>
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_1'); ?>
                    </p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_2'); ?></p>
                </div>

                <?php

                exit();
            }

        } else {
            ?>
            <script>
                dropError();
            </script>

            <div class="backgroundErro">
                <div class="erro" style="display:block;">
                    <button id="fechaErro">
                        <?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/closeMenu.svg");?>
                    </button>

                    <h3>
                        <?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/error.svg");?>
                        <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_AVISO'); ?>
                    </h3>

                    <ol>
                        <?php

                        if($errTipoContato == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_TIPOCONTATO') . '</li>';
                        } else {
                            $errTipoContato = 0;
                        }

                        if($errAssunto == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ASSUNTO') . '</li>';
                        } else {
                            $errAssunto = 0;
                        }

                        if($errProjeto == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_PROJETO_1') . '</li>';
                        } else if($errProjeto == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_PROJETO_2') . '</li>';
                        } else{
                            $errProjeto = 0;
                        }

                        if($errUrl == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_URL') . '</li>';
                        } else{
                            $errUrl = 0;
                        }

                        if($errEspAssunto == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ESPECIFICACAO_ASSUNTO') . '</li>';
                        } else{
                            $errEspAssunto = 0;
                        }

                        if($errPopulacao == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_POPULACAO_1') . '</li>';
                        } else if($errPopulacao == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_POPULACAO_2') . '</li>';
                        } else {
                            $errPopulacao = 0;
                        }

                        if($errCategorias == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_CATEGORIAS') . '</li>';
                        } else {
                            $errCategorias = 0;
                        }

                        if($errOutrasCat == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_OUTRAS_CATEGORIAS') . '</li>';
                        } else {
                            $errOutrasCat = 0;
                        }

                        if($errNumTecnicos == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NUM_TECNICOS') . '</li>';
                        } else {
                            $errNumTecnicos = 0;
                        }

                        if($errMarcaDesignacao == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_MARCA_DESIGNACAO') . '</li>';
                        } else {
                            $errMarcaDesignacao = 0;
                        }

                        if($errMarcaMunicipal == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_MARCA_MUNICIPAL') . '</li>';
                        } else {
                            $errMarcaMunicipal = 0;
                        }

                        if($errOutraMarcaMunicipal == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_OUTRA_MARCA_MUNICIPAL_1') . '</li>';
                        } else if($errOutraMarcaMunicipal == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_OUTRA_MARCA_MUNICIPAL_2') . '</li>';
                        } else {
                            $errOutraMarcaMunicipal = 0;
                        }

                        if($errParceria == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_PARCERIA') . '</li>';
                        } else {
                            $errParceria = 0;
                        }

                        if($errAssuntoReuniao == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ASSUNTO_REUNIAO_1') . '</li>';
                        } else if($errAssuntoReuniao == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ASSUNTO_REUNIAO_2') . '</li>';
                        } else {
                            $errAssuntoReuniao = 0;
                        }

                        if($errDataReuniao == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_DATA_REUNIAO') . '</li>';
                        } else if($errDataReuniao == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_DATA_REUNIAO_2') . '</li>';
                        } else {
                            $errDataReuniao = 0;
                        }

                        if($errHoraReuniao == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_HORA_REUNIAO') . '</li>';
                        } else {
                            $errHoraReuniao = 0;
                        }

                        if($errNomeReuniao == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_REUNIAO_1') . '</li>';
                        } else if($errNomeReuniao == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_REUNIAO_2') . '</li>';
                        } else {
                            $errNomeReuniao = 0;
                        }

                        if($errOutroAssunto == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_OUTRO_ASSUNTO') . '</li>';
                        } else {
                            $errOutroAssunto = 0;
                        }

                        if($errRefVagaTrabalho == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_REF_VAGA_TRABALHO') . '</li>';
                        } else {
                            $errRefVagaTrabalho = 0;
                        }

                        if($errNomeCand == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECANDIDATO_1') . '</li>';
                        } else if($errNomeCand == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECANDIDATO_2') . '</li>';
                        } else {
                            $errNomeCand = 0;
                        }

                        if($errEmailCand == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCANDIDATO_1') . '</li>';
                        } else if($errEmailCand == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCANDIDATO_2') . '</li>';
                        } else {
                            $errEmailCand = 0;
                        }

                        if($errAprVagaTrabalho == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_APRESENTACAO_VAGA_TRABALHO') . '</li>';
                        } else {
                            $errAprVagaTrabalho = 0;
                        }

                        if($errNomeCV == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECV_1') . '</li>';
                        } else if($errNomeCV == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECV_2') . '</li>';
                        } else {
                            $errNomeCV = 0;
                        }

                        if($errEmailCV == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCV_1') . '</li>';
                        } else if($errEmailCV == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCV_2') . '</li>';
                        } else {
                            $errEmailCV = 0;
                        }

                        if($errAprCV == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_APRESENTACAO_CV') . '</li>';
                        } else {
                            $errAprCV = 0;
                        }

                        if($errPropAlertar == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_PROPONHO') . '</li>';
                        } else {
                            $errPropAlertar = 0;
                        }

                        if($errAprBrainstorming == 1){
                            if($assunto == 17) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_APRESENTACAO_BRAINSTORMING_1') . '</li>';
                            } else if($assunto == 18) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_APRESENTACAO_BRAINSTORMING_2') . '</li>';
                            }
                        } else {
                            $errAprBrainstorming = 0;
                        }

                        if($errConcelho == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_CONCELHO') . '</li>';
                        } else {
                            $errConcelho = 0;
                        }

                        if($errNif == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NIF') . '</li>';
                        } else {
                            $errNif = 0;
                        }

                        if($errEmail == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAIL_1') . '</li>';
                        } else if($errEmail == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAIL_2') . '</li>';
                        } else {
                            $errEmail = 0;
                        }

                        if($errNome == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_1') . '</li>';
                        } else if($errNome == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_2') . '</li>';
                        } else {
                            $errNome = 0;
                        }

                        if($errCargo == 1){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_CARGO_1') . '</li>';
                        } else if($errCargo == 2){
                            echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_CARGO_2') . '</li>';
                        } else {
                            $errCargo = 0;
                        }

                        if($errRecaptcha == 1) {
                            echo '<li>Erro na verificação Não Sou um Robô (Recaptcha)</li>';
                        }else{
                            $errRecaptcha = 0;
                        }
                        ?>
                    </ol>
                </div>
            </div>
            <?php
        }
    }

?>

<legend class="section"><h2><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TITLE_FALE_CONNOSCO'); ?></h2></legend>

<form id="talk" action="/app/message/" method="post" class="talk-form" enctype="multipart/form-data" >

    <div class="form-group" id="tipo">
        <label class="col-md-3 control-label"></label>

        <?php $Tipologia = VirtualDeskSiteAlertarPTHelper::getTipoFaleConnosco();?>

        <div class="col-md-9">

            <?php
            foreach($Tipologia as $rowWSL) : ?>

                <span><input type="radio" name="radioval" id="<?php echo $rowWSL['idValue']; ?>" value="<?php echo $rowWSL['id']; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['tipoContacto'] == $rowWSL['id']) { echo 'checked="checked"'; } ?>> <?php echo $rowWSL['tipologia']; ?></span>

                <?php
                $numberRadio = $numberRadio + 1;
                if($numberRadio == 1){
                    ?>
                    <span class="separator">|</span>
                    <?php
                }
            endforeach; ?>

        </div>

        <input type="hidden" id="tipoContacto" name="tipoContacto" value="<?php echo $tipoContacto; ?>">
    </div>

    <!--   Assunto-->
    <?php
    $ListaDeMenuMain = array();
    if(!empty($tipoContacto)) {
        if( (int) $tipoContacto > 0) $ListaDeMenuMain = VirtualDeskSiteAlertarPTHelper::getAssunto($tipoContacto);
    }
    ?>
    <div id="blocoSubCat" class="form-group">
        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTAPT_FALECONNOSCO_ASSUNTO' ); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <select name="assunto" id="assunto" value="<?php echo $assunto;?>" required
                <?php
                if(empty($tipoContacto)) {
                    echo 'disabled';
                }
                ?>
                    class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($assunto)){
                    ?>
                    <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                    <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                        <option value="<?php echo $rowMM['id']; ?>"
                        ><?php echo $rowMM['name']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $assunto; ?>"><?php echo VirtualDeskSiteAlertarPTHelper::getAssuntoName($assunto);?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeAssunto = VirtualDeskSiteAlertarPTHelper::excludeAssunto($tipoContacto, $assunto);?>
                    <?php foreach($ExcludeAssunto as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['name']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>

        </div>
    </div>

    <div id="anomalia"
        <?php if(($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2)) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }

        ?>
    >

        <h3 id="titleAnomalia"
            <?php if($assunto == 1) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }

            ?>
        ><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ANOMALIA'); ?></h3>

        <h3 id="titleUpgrade"
            <?php if($assunto == 2) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }

            ?>
        ><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_UPGRADE'); ?></h3>

        <!--   Projeto   -->
        <div class="form-group" id="proj">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PROJETO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="projeto" id="projeto" maxlength="250" value="<?php echo $projeto; ?>"/>
            </div>
        </div>


        <!--   URL   -->
        <div class="form-group" id="urlProj">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_URL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="urlProjeto" id="urlProjeto" maxlength="250" value="<?php echo $urlProjeto; ?>"/>
            </div>
        </div>

        <!--   Especificação do assunto   -->
        <div class="form-group" id="esp">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ESPECIFICACAO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="espAssunto" id="espAssunto" maxlength="2000"><?php echo $espAssunto; ?></textarea>
            </div>
        </div>
    </div>



    <div id="orcamento"
        <?php if(($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 2 && $assunto == 8)) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >

        <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ORCAMENTO'); ?></h3>

        <!--  População local  -->
        <div class="form-group" id="populacaoLocal">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_POPULACAOLOCAL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="populacao" id="populacao" maxlength="15" value="<?php echo $populacao; ?>"/>
            </div>
        </div>

        <!--   Categorias -->
        <div id="blocoCat" class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_FALECONNOSCO_CATEGORIAS'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="cat">
                    <input type="checkbox" name="cat1" id="cat1" value="<?php echo $cat_1_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_1_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Agua.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_1');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat2" id="cat2" value="<?php echo $cat_2_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_2_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Florestacao.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_2');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat3" id="cat3" value="<?php echo $cat_3_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_3_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_LimpezaUrbana.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_3');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat4" id="cat4" value="<?php echo $cat_4_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_4_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Materiais.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_4');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat5" id="cat5" value="<?php echo $cat_5_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_5_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Passeios.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_5');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat6" id="cat6" value="<?php echo $cat_6_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_6_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Predios.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_6');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat7" id="cat7" value="<?php echo $cat_7_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_7_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Recolha_Lixo.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_7');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat8" id="cat8" value="<?php echo $cat_8_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_8_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Rede_Esgotos.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_8');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat9" id="cat9" value="<?php echo $cat_9_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_9_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Saude.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_9');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat10" id="cat10" value="<?php echo $cat_10_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_10_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Transito.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_10');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat11" id="cat11" value="<?php echo $cat_11_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_11_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Fiscalizacao.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_11');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat12" id="cat12" value="<?php echo $cat_12_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_12_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Obras.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_12');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat13" id="cat13" value="<?php echo $cat_13_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_13_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Iluminacao_Publica.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_13');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat14" id="cat14" value="<?php echo $cat_14_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_14_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Seguranca.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_14');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat15" id="cat15" value="<?php echo $cat_15_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_15_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Equipamentos.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_15');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat16" id="cat16" value="<?php echo $cat_16_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_16_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Acessibilidades.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_16');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat17" id="cat17" value="<?php echo $cat_17_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_17_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Habitacao.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_17');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat18" id="cat18" value="<?php echo $cat_18_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_18_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_Social.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_18');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat19" id="cat19" value="<?php echo $cat_19_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_19_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="icon"><?php echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/Alertar_avaliacao.svg");?></div>
                        <div class="text"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_19');?></div>
                    </label>
                </div>

                <div class="cat">
                    <input type="checkbox" name="cat20" id="cat20" value="<?php echo $cat_20_Hidden; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['cat_20_Hidden'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="control-label">
                        <div class="nameCat"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CAT_20');?></div>
                    </label>
                </div>


                <input type="hidden" id="cat_1_Hidden" name="cat_1_Hidden" value="<?php echo $cat_1_Hidden; ?>">
                <input type="hidden" id="cat_2_Hidden" name="cat_2_Hidden" value="<?php echo $cat_2_Hidden; ?>">
                <input type="hidden" id="cat_3_Hidden" name="cat_3_Hidden" value="<?php echo $cat_3_Hidden; ?>">
                <input type="hidden" id="cat_4_Hidden" name="cat_4_Hidden" value="<?php echo $cat_4_Hidden; ?>">
                <input type="hidden" id="cat_5_Hidden" name="cat_5_Hidden" value="<?php echo $cat_5_Hidden; ?>">
                <input type="hidden" id="cat_6_Hidden" name="cat_6_Hidden" value="<?php echo $cat_6_Hidden; ?>">
                <input type="hidden" id="cat_7_Hidden" name="cat_7_Hidden" value="<?php echo $cat_7_Hidden; ?>">
                <input type="hidden" id="cat_8_Hidden" name="cat_8_Hidden" value="<?php echo $cat_8_Hidden; ?>">
                <input type="hidden" id="cat_9_Hidden" name="cat_9_Hidden" value="<?php echo $cat_9_Hidden; ?>">
                <input type="hidden" id="cat_10_Hidden" name="cat_10_Hidden" value="<?php echo $cat_10_Hidden; ?>">
                <input type="hidden" id="cat_11_Hidden" name="cat_11_Hidden" value="<?php echo $cat_11_Hidden; ?>">
                <input type="hidden" id="cat_12_Hidden" name="cat_12_Hidden" value="<?php echo $cat_12_Hidden; ?>">
                <input type="hidden" id="cat_13_Hidden" name="cat_13_Hidden" value="<?php echo $cat_13_Hidden; ?>">
                <input type="hidden" id="cat_14_Hidden" name="cat_14_Hidden" value="<?php echo $cat_14_Hidden; ?>">
                <input type="hidden" id="cat_15_Hidden" name="cat_15_Hidden" value="<?php echo $cat_15_Hidden; ?>">
                <input type="hidden" id="cat_16_Hidden" name="cat_16_Hidden" value="<?php echo $cat_16_Hidden; ?>">
                <input type="hidden" id="cat_17_Hidden" name="cat_17_Hidden" value="<?php echo $cat_17_Hidden; ?>">
                <input type="hidden" id="cat_18_Hidden" name="cat_18_Hidden" value="<?php echo $cat_18_Hidden; ?>">
                <input type="hidden" id="cat_19_Hidden" name="cat_19_Hidden" value="<?php echo $cat_19_Hidden; ?>">
                <input type="hidden" id="cat_20_Hidden" name="cat_20_Hidden" value="<?php echo $cat_20_Hidden; ?>">
            </div>
        </div>

        <!--   Outras categorias   -->
        <div class="form-group" id="outrasCat"
            <?php if($cat_20_Hidden == 1) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_OUTRASCAT'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="outraCat" id="outraCat" maxlength="2000"><?php echo $outraCat; ?></textarea>
            </div>
        </div>

        <div class="form-group" id="numTecs">
            <label class="col-md-3 control-label">
                <?php
                echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_NUMTECNICOS');
                echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/information.svg");
                ?>
            </label>

            <div class="infoNumTecs">
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_INFONUMTECS1');?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_INFONUMTECS2');?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_INFONUMTECS3');?></p>
            </div>

            <?php $NumeroTecnicos = VirtualDeskSiteAlertarPTHelper::getNumtecnicos();?>

            <div class="col-md-9">

                <?php foreach($NumeroTecnicos as $rowWSL) : ?>

                    <span><input type="radio" name="radioval2" id="tec_<?php echo $rowWSL['id']; ?>" value="<?php echo $rowWSL['id']; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['numTecnicos'] == $rowWSL['id']) { echo 'checked="checked"'; } ?>> <?php echo $rowWSL['numTecnicos']; ?></span>

                <?php endforeach; ?>

            </div>

            <input type="hidden" id="numTecnicos" name="numTecnicos" value="<?php echo $numTecnicos; ?>">
        </div>

        <div class="form-group" id="marcaDesignacao">

            <label class="col-md-3 control-label">
                <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_MARCADESIGNACAO');?>
            </label>

            <?php $Marca = VirtualDeskSiteAlertarPTHelper::getMarcas();?>

            <div class="col-md-9">

                <?php foreach($Marca as $rowWSL) : ?>
                    <div class="w25">
                        <input type="radio" name="radioval3" id="marca_<?php echo $rowWSL['id']; ?>" value="<?php echo $rowWSL['id']; ?>"
                            <?php if (isset($_POST["submitForm"]) && $_POST['marcaDesignacao'] == $rowWSL['id']) { echo 'checked="checked"'; } ?>> <?php echo '<label><p>' . $rowWSL['marca'] . '</p> <p> EX: <span class="url">' . $rowWSL['exemplo'] . '</span></p></label>'; ?>
                    </div>
                <?php endforeach; ?>

            </div>

            <input type="hidden" id="marcaDesignacao" name="marcaDesignacao" value="<?php echo $marcaDesignacao; ?>">
        </div>

        <div class="form-group" id="nomesProjeto"
            <?php if($marcaDesignacao == 2) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <label class="col-md-3 control-label">
                <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_MARCAMUNICIPAL');?>
            </label>

            <?php $MarcaMunicipal = VirtualDeskSiteAlertarPTHelper::getMarcaMunicipal();?>

            <div class="col-md-9">

                <?php foreach($MarcaMunicipal as $rowWSL) : ?>
                    <div class="w25">
                        <input type="radio" name="radioval4" id="marcaMunicipal_<?php echo $rowWSL['id']; ?>" value="<?php echo $rowWSL['id']; ?>"
                            <?php if (isset($_POST["submitForm"]) && $_POST['marcaMunicipal'] == $rowWSL['id']) { echo 'checked="checked"'; } ?>> <?php echo '<label><p>' . $rowWSL['marca'] . '</p> <p> EX: <span class="url">' . $rowWSL['exemplo'] . '</span></p></label>'; ?>
                    </div>
                <?php endforeach; ?>

                <div class="w25"><input type="radio" name="radioval4" id="marcaMunicipal_outro" value="99" <?php if (isset($_POST["submitForm"]) && $_POST['marcaMunicipal'] == 99) { echo 'checked="checked"'; } ?>> <?php echo '<label><p>Outro</p></label>'; ?></div>

            </div>

            <input type="hidden" id="marcaMunicipal" name="marcaMunicipal" value="<?php echo $marcaMunicipal; ?>">
        </div>

        <div class="form-group" id="outroNomesProjeto"
            <?php if($marcaMunicipal == 99 && $marcaDesignacao == 2) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_OUTRONOMEPROJETO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <span>www</span>
                <input type="text" class="form-control" autocomplete="off" required name="outroNomeProjeto" id="outroNomeProjeto" maxlength="100" value="<?php echo $outroNomeProjeto; ?>"/>
                <span>.pt</span>
            </div>

        </div>

        <div class="form-group" id="observacoesGerais">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_OBSERVACOESGERAIS'); ?></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="obsGerais" id="obsGerais" maxlength="2000"><?php echo $obsGerais; ?></textarea>
            </div>
        </div>

    </div>

    <div id="parcerias"
        <?php if(($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 3 && $assunto == 16)) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >
        <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PARCERIA_TITLE'); ?></h3>


        <!--   Especificação da parceria   -->
        <div class="form-group" id="parc">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PARCERIA'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="parceria" id="parceria" maxlength="2000"><?php echo $parceria; ?></textarea>
            </div>
        </div>
    </div>

    <div id="reuniao"
        <?php if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >

        <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_REUNIAO'); ?></h3>


        <!--   Assunto   -->
        <div class="form-group" id="assReuniao">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ASSUNTO_REUNIAO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="assuntoReuniao" id="assuntoReuniao" maxlength="250" value="<?php echo $assuntoReuniao; ?>"/>
            </div>
        </div>


        <!--   Data de reuniao  -->
        <div class="form-group" id="reuData">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_DATAREUNIAO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                    <input type="text" placeholder="dd-mm-aaaa" name="dataReuniao" id="dataReuniao" value="<?php echo $dataReuniao; ?>">
                    <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>

        <!--   Hora de reuniao  -->
        <div class="form-group" id="reuHour">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_HORAREUNIAO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="input-group timepicker">
                    <input class="form-control m-input" name="m_timepicker_3" id="m_timepicker_3" value="<?php echo $horaReuniao;?>" type="text" />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="la la-clock-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--   Nome reuniao   -->
        <div class="form-group" id="nameReuniao">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_NOME_REUNIAO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="nomeReuniao" id="nomeReuniao" maxlength="250" value="<?php echo $nomeReuniao; ?>"/>
            </div>
        </div>
    </div>

    <div id="vagaTrabalho"
        <?php if($tipoContacto == 3 && $assunto == 15) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >

        <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_VAGA'); ?></h3>

        <!--   Referencia   -->
        <div class="form-group" id="refTrabalho">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_REFERENCIA'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="refVagaTrabalho" id="refVagaTrabalho" maxlength="100" value="<?php echo $refVagaTrabalho; ?>"/>
            </div>
        </div>

        <!--   Nome Candidato   -->
        <div class="form-group" id="noneCand">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_NOMECANDIDATO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="nomeCand" id="nomeCand" maxlength="250" value="<?php echo $nomeCand; ?>"/>
            </div>
        </div>

        <!--   Email Candidato   -->
        <div class="form-group" id="mailCand">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_EMAILCANDIDATO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="emailCand" id="emailCand" maxlength="250" value="<?php echo $emailCand; ?>"/>
            </div>
        </div>

        <!--   VERIFICAÇÃO DE EMAIL   -->
        <div class="form-group" id="mailCand2">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_EMAIL_REPEAT'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="emailConfCand" id="emailConfCand" value="<?php echo $emailConfCand; ?>"/>
            </div>
        </div>

        <!--   Apresentacao  -->
        <div class="form-group" id="apre">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_APRESENTACAO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="apresentacaoVaga" id="apresentacaoVaga" maxlength="2000"><?php echo $apresentacaoVaga; ?></textarea>
            </div>
        </div>

    </div>

    <div id="enviarCV"
        <?php if($tipoContacto == 3 && $assunto == 14) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >

        <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ENVIARCV'); ?></h3>


        <!--   Nome CV   -->
        <div class="form-group" id="nomeEnvioCV">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_NOMECV'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="nomeCV" id="nomeCV" maxlength="250" value="<?php echo $nomeCV; ?>"/>
            </div>
        </div>

        <!--   Email CV   -->
        <div class="form-group" id="mailCV">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_EMAILCV'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="emailCV" id="emailCV" maxlength="250" value="<?php echo $emailCV; ?>"/>
            </div>
        </div>

        <!--   VERIFICAÇÃO DE EMAIL   -->
        <div class="form-group" id="mailCV2">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_EMAIL_REPEAT'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="emailConfCV" id="emailConfCV" value="<?php echo $emailConfCV; ?>"/>
            </div>
        </div>

        <!--   Apresentacao  -->
        <div class="form-group" id="apreCV">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_APRESENTACAO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="apresentacaoCV" id="apresentacaoCV" maxlength="2000"><?php echo $apresentacaoCV; ?></textarea>
            </div>
        </div>

    </div>

    <div id="parceriaBloco"
        <?php if(($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18) || ($tipoContacto == 4 && $assunto == 19)) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >

        <div class="form-group" id="conv">
            <label id="labelProjeto" class="col-md-3 control-label"<?php if($assunto == 17) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
            >
                <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PROJETO_TITLE'); ?></label>

            <label id="labelBrainstorming" class="col-md-3 control-label"<?php if($assunto == 18) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
            >

                <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_BRAINSSTORMING_TITLE'); ?></label>

            <label id="labelConferencia" class="col-md-3 control-label"<?php if($assunto == 19) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
            >

                <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_CONFERENCIA_TITLE'); ?></label>



            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="apresentacao" id="apresentacao" maxlength="2000"><?php echo $apresentacao; ?></textarea>
            </div>
        </div>

    </div>

    <div id="outro"
        <?php if(($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 13)) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >
        <div class="form-group" id="outroAss">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_OUTRO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="outroAssunto" id="outroAssunto" maxlength="2000"><?php echo $outroAssunto; ?></textarea>
            </div>
        </div>

    </div>

    <div id="elogiosSugestoes"
        <?php if($tipoContacto == 5) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >
        <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ELOGIOS_SUGESTOES'); ?></h3>

        <div class="intro">
            <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ELOGIOS_SUGESTOES_INTRO'); ?></p>
            <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ELOGIOS_SUGESTOES_INTRO_2'); ?></p>
        </div>


        <div class="form-group" id="proponho">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PROPONHO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <textarea required class="form-control" rows="7" name="propAlertar" id="propAlertar" maxlength="2000"><?php echo $propAlertar; ?></textarea>
            </div>
        </div>

    </div>

    <div id="enviarFicheiros"
        <?php if(($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2) || ($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 8) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 2 && $assunto == 12) || ($tipoContacto == 2 && $assunto == 13) || ($tipoContacto == 3 && $assunto == 14) || ($tipoContacto == 3 && $assunto == 16) || ($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18)) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >

        <div class="form-group" id="uploadField">
            <?php
            if($assunto == 13 || $assunto == 14){
                ?>
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_UPLOAD_CV'); ?></label>
                <?php
            } else {
                ?>
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_UPLOAD'); ?></label>
                <?php
            }
            ?>

            <div class="col-md-9">

                <div class="file-loading">
                    <input type="file" name="fileupload[]" id="fileupload" multiple>
                </div>
                <div id="errorBlock" class="help-block"></div>
                <input type="hidden" id="VDAjaxReqProcRefId">

            </div>
        </div>
    </div>

    <div id="dadosPessoais"
        <?php if(($tipoContacto == 1 && !empty($assunto) && $assunto != 'Escolher opção') || ($tipoContacto == 2 && !empty($assunto) && $assunto != 'Escolher opção') || ($tipoContacto == 4 && !empty($assunto) && $assunto != 'Escolher opção')) {
            ?>
            style="display:block";
            <?php
        } else {
            ?>
            style="display:none";
            <?php
        }
        ?>
    >
        <legend class="section2"><h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_DADOSPESSOAIS'); ?></h3></legend>

        <!--   Concelho  -->
        <div class="form-group" id="conc">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_CONCELHO_FALECONNOSCO_LABEL') ?><span class="required">*</span></label>
            <?php $Concelhos = VirtualDeskSiteAlertarPTHelper::getConcelho()?>
            <div class="col-md-9">
                <select name="concelho" disabled value="<?php echo $concelho; ?>" id="concelho" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($concelho)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_OPCAO'); ?></option>
                        <?php foreach($Concelhos as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['concelho']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteAlertarPTHelper::getConcelhoName($concelho) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeConcelho = VirtualDeskSiteAlertarPTHelper::excludeConcelho($concelho)?>
                        <?php foreach($ExcludeConcelho as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['concelho']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>

        <!--   NIF   -->
        <div class="form-group" id="nif">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_NIF'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
            </div>
        </div>

        <!--   EMAIL   -->
        <div class="form-group" id="mail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_EMAIL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="email" id="email" value="<?php echo $email; ?>"/>
            </div>
        </div>

        <!--   VERIFICAÇÃO DE EMAIL   -->
        <div class="form-group" id="mail2">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_EMAIL_REPEAT'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="emailConf" id="emailConf" value="<?php echo $emailConf; ?>"/>
            </div>
        </div>

        <!--   NOME  -->
        <div class="form-group" id="name">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_NOME'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" name="nome" id="nome" value="<?php echo htmlentities($nome, ENT_QUOTES, 'UTF-8'); ?>" maxlength="250"/>
            </div>
        </div>

        <!--   Cargo  -->
        <div class="form-group" id="carg">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_CARGO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="cargo" id="cargo" maxlength="250" value="<?php echo $cargo; ?>"/>
            </div>
        </div>
    </div>

    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group" id="captcha">
            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
            $field_id = 'dynamic_recaptcha_1';
            print $captcha->display($field_id, $field_id, 'g-recaptcha');
            ?>
            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
        </div>
    <?php endif; ?>

    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
    </div>

</form>
