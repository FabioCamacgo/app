<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('novaOcorrenciaAlertarPT');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;
    $link = $jinput->get('link','' ,'string');
    $ref = explode("mun=", $link);

    if (strpos($ref[1], '&tipo=') !== false) {
        $explodeCat = explode("&tipo=", $ref[1]);
        $idURL = base64_decode($explodeCat[0]);
        $cat =  base64_decode($explodeCat[1]);
    } else {
        $idURL = base64_decode($ref[1]);
    }

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AlertarPT/maps.js' . $addscript_end;
    //$localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AlertarPT/edit_maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    //FileUploader
    $headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;


    $headScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    //Fileuploader
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $espera       = $obParam->getParamsByTag('espera');

?>

<style>
    .fileuploader-popup {z-index: 99999 !important;}
</style>



<?php

    if(empty($idURL)){
        require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'novaOcorrenciaAlertarPTGeral.php');
        ?>
            <script>
                jQuery('.itemid-1404 #g-navigation').fadeIn('slow');
                jQuery('.itemid-1404 #iconMenu').fadeIn('slow');
                jQuery('.itemid-1404 #BarraCopyright').fadeIn('slow');
            </script>
        <?php
    } else {
        require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'novaOcorrenciaAlertarPTMunicipioBarraMenu.php');
        require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'novaOcorrenciaAlertarPTMunicipio.php');
        ?>
            <script>
                jQuery('.itemid-1404 #BarraCopyright').fadeIn('slow');
            </script>
        <style>
            .itemid-1404 #g-navigation, .itemid-1404 #iconMenu{
                display:none !important;
            }
        </style>
        <?php
    }

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;
?>


<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'novaOcorrenciaAlertarPT/'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
    var iconPath = '<?php echo JUri::base() . $espera; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/novaOcorrenciaAlertarPT.js.php');
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/edit_maps.js.php');
    ?>
</script>
