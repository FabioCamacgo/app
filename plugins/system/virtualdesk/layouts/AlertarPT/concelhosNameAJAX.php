<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('concelhosName');
    if ($resPluginEnabled === false) exit();

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    //echo $headCSS;

    if (isset($_POST['submitForm'])) {
        $valorID = $_POST['valorID'];
    } else {
        $valorID = 0;
    }

?>

<form action="" method="post" enctype="multipart/form-data">
    <input type="hidden" id="valorID" name="valorID" value="<?php echo $valorID; ?>">

    <?php if(!empty($valorID)){?>
        <div class="alertarConcelho">
            <div class="brasao">
                <img src="<?php echo JUri::base()?>images/Brasoes/concelhos/c_<?php echo $valorID; ?>.png"/>
            </div>

            <div class="info">
                <h2><?php echo VirtualDeskSiteAlertarPTHelper::getNameConcelho($valorID); ?></h2>

                <a href="municipio?mun=<?php echo base64_encode($valorID);?>" class="newOcorrencia" target="_blank"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_NEW_OCORRENCIA'); ?></a>
            </div>
        </div>
    <?php } else { ?>

        <div class="alertarConcelho">
                <h2 class="generic"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_ESCOLHER_GOVERNACAO'); ?></h2>

                <div class="icon generic">
                    <?php
                        echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/setaMapa.svg");
                    ?>
                </div>
        </div>

    <?php }?>

    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="aa">
    </div>
</form>

<?php
    echo $footerScripts;
?>


<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlertarPT/concelhosName.js.php');
    ?>
</script>