<?php
    defined('_JEXEC') or die;
?>

jQuery(document).ready(function() {


    jQuery( ".itemid-1432 .menuBar .botaoMenu" ).click(function() {
        jQuery("#menuApp").css('right','0');
        jQuery("body").css('overflow-y','hidden');
        jQuery("#g-header").css('opacity','0.2');
        jQuery("#g-showcase").css('opacity','0.2');
        jQuery("#g-above").css('opacity','0.2');
        jQuery("#g-copyright").css('opacity','0.2');
        jQuery("#g-container-5234").css('opacity','0.2');
    });

    jQuery(".closeMenu").click(function(){
        var largura = jQuery(window).width();

        if(largura < 470){
            jQuery("#menuApp").css('right','-65%');
        } else {
            jQuery("#menuApp").css('right','-50%');
        }

        setTimeout(function() {
            jQuery("body").css('overflow-y','scroll');
            jQuery("#g-header").css('opacity','1');
            jQuery("#g-showcase").css('opacity','1');
            jQuery("#g-above").css('opacity','1');
            jQuery("#g-copyright").css('opacity','1');
            jQuery("#g-container-5234").css('opacity','1');
        }, 200);
    });


    jQuery(".itemid-1432 .menuBar .botaoCat svg").click(function() {
        jQuery('.itemid-1432 .menuContent').slideDown('slow');

        setTimeout(function(){
            jQuery('.itemid-1432 .menuContent .w25.catAgua .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catFlorestacao .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catLimpezaUrbana .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catMateriais .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catPasseios .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .contentBarra').fadeIn('slow');
        }, 300);

        setTimeout(function(){
            jQuery('.itemid-1432 .menuContent .w25.catPredios .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catRecolhaLixo .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catEsgotos .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catSaude .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catTransito .block').slideDown('slow');
        }, 600);

        setTimeout(function(){
            jQuery('.itemid-1432 .menuContent .w25.catFiscalizacao .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catObras .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catIluminacao .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catSeguranca .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catEquipamentos .block').slideDown('slow');
        }, 900);

        setTimeout(function(){
            jQuery('.itemid-1432 .menuContent .w25.catAcessibilidades .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catHabitacao .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catSocial .block').slideDown('slow');
            jQuery('.itemid-1432 .menuContent .w25.catAvaliacao .block').slideDown('slow');
        }, 1200);

    });


    jQuery(".itemid-1432 .closeMenu").click(function() {
        jQuery('.itemid-1432 .menuContent .w25.catAcessibilidades .block').fadeOut('slow');
        jQuery('.itemid-1432 .menuContent .w25.catHabitacao .block').fadeOut('slow');
        jQuery('.itemid-1432 .menuContent .w25.catSocial .block').fadeOut('slow');
        jQuery('.itemid-1432 .menuContent .w25.catAvaliacao .block').fadeOut('slow');

        setTimeout(function(){
            jQuery('.itemid-1432 .menuContent .w25.catFiscalizacao .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catObras .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catIluminacao .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catSeguranca .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catEquipamentos .block').fadeOut('slow');
        }, 100);

        setTimeout(function(){
            jQuery('.itemid-1432 .menuContent .w25.catPredios .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catRecolhaLixo .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catEsgotos .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catSaude .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catTransito .block').fadeOut('slow');
        }, 300);

        setTimeout(function(){
            jQuery('.itemid-1432 .menuContent .w25.catAgua .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catFlorestacao .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catLimpezaUrbana .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catMateriais .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .w25.catPasseios .block').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent .contentBarra').fadeOut('slow');
            jQuery('.itemid-1432 .menuContent').slideUp('slow');
        }, 500);



    });

});
