<?php

?>


var idLevel1 = <?php echo $level;?>;

function newLevel(level, position){
    idLevel1 = level;
    manageTreeMenu(position);
}

function manageTreeMenu(item){
    if(idLevel1 == 1){
        removeAtiveLevel1();
        ativateLevel1(item);
    } else if(idLevel1 == 2){
        removeAtiveLevel2();
        ativateLevel2(item);
    } else if(idLevel1 == 3){
        removeAtiveLevel3();
        ativateLevel3(item);
    }

}


var ComponentFileUploadervPlg = function() {

    var checkFileUploader = function(RefID) {

        //debugger;

        console.log(RefID);
        //console.log(jQuery('#' + RefID).attr('previewJS'));

        var previewJS = jQuery('#' + RefID).attr('previewJS');
        //console.log(previewJS);

        //var objPreviewJS = JSON.parse(JSON.stringify(previewJS));
        //console.log(objPreviewJS);

        var objPreviewJS = eval('[' + previewJS + ']');
        console.log(objPreviewJS);

        jQuery('#' + RefID).fileuploader({

            changeInput: ' ',
            limit: 1,

            fileMaxSize: 1,

            extensions: ['jpg', 'jpeg', 'png','pdf'],

            enableApi: true,

            addMore: false,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +

                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +

                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: null,
                onItemRemove: null
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));
                plusInput.on('click', function() {
                    api.open();
                });
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            , files: objPreviewJS

        });


    };



    return {
        //main function to initiate the module
        init: function(RefID) {
            checkFileUploader(RefID);
        }

    };
}();


var iFrameDocPreview =  function() {

    var ShowiFrameDocPreview = function(iFrameID) {
        console.log('iFrameID=' + iFrameID);

        var currIframe  = jQuery('iframe#'+iFrameID);

        if(currIframe.length>0) {
           var url2Src = currIframe.attr('url2Src');
            currIframe.attr('src',url2Src);
            currIframe.show();
        }


        return('');
    }

    return {
        //main function to initiate the module
        ShowiFrameDocPreview : function(iFrameID) { ShowiFrameDocPreview(iFrameID); }
    };

}();


jQuery( window ).resize(function() {

    var alturaTreeList = jQuery('.TreeDocs').height();
    var alturaContentList = jQuery('.contentDocs').height();

    if(alturaTreeList > alturaContentList){
        jQuery('.TreeDocs').css('min-height',alturaTreeList);
        jQuery('.contentDocs').css('min-height',alturaTreeList);
    } else {
        jQuery('.TreeDocs').css('min-height',alturaContentList);
        jQuery('.contentDocs').css('min-height',alturaContentList);
    }
});


jQuery( document ).ready(function() {

    var alturaTreeList = jQuery('.TreeDocs').height();
    var alturaContentList = jQuery('.contentDocs').height();

    if(alturaTreeList > alturaContentList){
        jQuery('.TreeDocs').css('min-height',alturaTreeList);
        jQuery('.contentDocs').css('min-height',alturaTreeList);
    } else {
        jQuery('.TreeDocs').css('min-height',alturaContentList);
        jQuery('.contentDocs').css('min-height',alturaContentList);
    }

    jQuery('.content-docs .item:nth-child(1)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('0');
    });

    jQuery('.content-docs .item:nth-child(2)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('1');
    });

    jQuery('.content-docs .item:nth-child(3)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('2');
    });

    jQuery('.content-docs .item:nth-child(4)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('3');
    });

    jQuery('.content-docs .item:nth-child(5)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('4');
    });

    jQuery('.content-docs .item:nth-child(6)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('5');
    });

    jQuery('.content-docs .item:nth-child(7)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('6');
    });

    jQuery('.content-docs .item:nth-child(8)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('7');
    });

    jQuery('.content-docs .item:nth-child(9)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('8');
    });

    jQuery('.content-docs .item:nth-child(10)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('9');
    });

    jQuery('.content-docs .item:nth-child(11)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('10');
    });

    jQuery('.content-docs .item:nth-child(12)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('11');
    });

    jQuery('.content-docs .item:nth-child(13)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('12');
    });

    jQuery('.content-docs .item:nth-child(14)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('13');
    });

    jQuery('.content-docs .item:nth-child(15)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('14');
    });

    jQuery('.content-docs .item:nth-child(16)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('15');
    });

    jQuery('.content-docs .item:nth-child(17)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('16');
    });

    jQuery('.content-docs .item:nth-child(18)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('17');
    });

    jQuery('.content-docs .item:nth-child(19)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('18');
    });

    jQuery('.content-docs .item:nth-child(20)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('19');
    });

    jQuery('.content-docs .item:nth-child(21)').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
        manageTreeMenu('20');
    });




    jQuery('.content-docs .docItem a').click(function() {
        var refDoc = jQuery(this).attr('value');

        jQuery.ajax({
            url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
            data:'m=documentos_novaVisualizacao&refDoc=' + refDoc ,

            success: function(output) {

            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    });


});