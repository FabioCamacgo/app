<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('documentosTree');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;
    $link = $jinput->get('link');
    $idCatLevel1 = '';
    $idCatLevel2 = '';
    $idCatLevel3 = '';
    $idCatLevel4 = '';

    $svgComSubnivel = $obParam->getParamsByTag('svgComSubnivel');
    $svgNoSubnivel = $obParam->getParamsByTag('svgNoSubnivel');

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

?>

<div class="tree-list">
    <?php
        $catLevel1 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Plugin();
        $numberCats = count($catLevel1);

    foreach($catLevel1 as $rowWSL) :
        $idCatLevel1 = $rowWSL['id'];
        $nameCatLevel1 = $rowWSL['categoria'];
        $catLevel2 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Plugin($idCatLevel1);
        $numberCatsLevel2 = count($catLevel2);
        ?>
            <div class="catLevel1">
                <?php
                    if(count($catLevel2) > 0) {
                        echo '<span class="level1" style="cursor:pointer;">' . file_get_contents($svgComSubnivel) . '</span>';
                    } else {
                        echo '<span>' . file_get_contents($svgNoSubnivel) . '</span>';
                    }

                ?>
                <span class="name" value="<?php echo base64_encode( $nameCatLevel1 . '&level1=' . $idCatLevel1);?>"><?php echo $nameCatLevel1;?></span>

                <?php
                    if(count($catLevel2) > 0){
                        ?>
                            <div class="blockCat2">
                                <?php
                                    foreach ($catLevel2 as $rowWSL2) :
                                        $idCatLevel2 = $rowWSL2['id'];
                                        $nameCatLevel2 = $rowWSL2['categoria'];
                                        $catLevel3 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Plugin($idCatLevel2);
                                        $numberCatsLevel3 = count($catLevel3);
                                        ?>
                                        <div class="catLevel2">
                                            <?php
                                                if(count($catLevel3) > 0) {
                                                    echo '<span style="cursor:pointer;">' . file_get_contents($svgComSubnivel) . '</span>';
                                                } else {
                                                    echo '<span>' . file_get_contents($svgNoSubnivel) . '</span>';
                                                }
                                            ?>

                                            <span class="name" value="<?php echo base64_encode($nameCatLevel2 . '&level1=' . $idCatLevel1 . '&level2=' . $idCatLevel2); ?>"><?php echo $nameCatLevel2; ?></span>

                                            <?php
                                                if(count($catLevel3) > 0) {
                                                    ?>
                                                    <div class="blockCat3">
                                                        <?php
                                                            foreach ($catLevel3 as $rowWSL3) :
                                                                $idCatLevel3 = $rowWSL3['id'];
                                                                $nameCatLevel3 = $rowWSL3['categoria'];
                                                                $catLevel4 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel4Plugin($idCatLevel3);
                                                                $numberCatsLevel4 = count($catLevel4);
                                                                ?>
                                                                <div class="catLevel3">
                                                                    <?php
                                                                        if(count($catLevel4) > 0) {
                                                                            echo '<span style="cursor:pointer;">' . file_get_contents($svgComSubnivel) . '</span>';
                                                                        } else {
                                                                            echo '<span>' . file_get_contents($svgNoSubnivel) . '</span>';
                                                                        }
                                                                    ?>

                                                                    <span class="name" value="<?php echo base64_encode($nameCatLevel3 . '&level2=' . $idCatLevel2 . '&level3=' . $idCatLevel3); ?>"><?php echo $nameCatLevel3; ?></span>

                                                                    <?php
                                                                        if(count($catLevel4) > 0) {
                                                                            ?>
                                                                            <div class="blockCat4">
                                                                                <?php
                                                                                foreach ($catLevel4 as $rowWSL4) :
                                                                                    $idCatLevel4 = $rowWSL4['id'];
                                                                                    $nameCatLevel4 = $rowWSL4['categoria'];

                                                                                    ?>
                                                                                    <div class="catLevel4">
                                                                                        <?php echo '<span>' . file_get_contents($svgNoSubnivel) . '</span>';?>
                                                                                        <span class="name" value="<?php echo base64_encode($nameCatLevel4 . '&level3=' . $idCatLevel3 . '&level4=' . $idCatLevel4); ?>"><?php echo $nameCatLevel4; ?></span>
                                                                                    </div>
                                                                                <?php
                                                                                endforeach;
                                                                                ?>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                    ?>
                                                                </div>
                                                                <?php

                                                            endforeach;
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                            ?>

                                        </div>

                                        <?php

                                    endforeach;
                                ?>
                            </div>
                        <?php
                    }
                ?>
        </div>
    <?php
    endforeach;
     ?>
</div>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/documentos/documentosTree.js.php');
    ?>
</script>
