<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('documentosByCat');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;
    $link = $jinput->get('link','', 'string');
    $maincat = $jinput->get('maincat');
    $documento = $jinput->get('documento','', 'string');
    $categoria = $jinput->get('categoria');
    $subcategoria = $jinput->get('subcategoria');
    $urlParam = $jinput->get('urlParam');

    if(!empty($link)){
        if(empty($categoria)){
            $explodeLink = explode('?cat=', $link);
            $categoria = $explodeLink[1];
            $subcategoria = '';
        }
    }

    if(!empty($urlParam)){

        $level4Value = explode('&level4=', base64_decode($urlParam));
        if(!empty($level4Value[1])){
            $idCatLevel4Link = $level4Value[1];
        }

        $level3Value = explode('&level3=', $level4Value[0]);
        if(!empty($level3Value[1])){
            $subcategoria = $level3Value[1];
        } else {
            $subcategoria = '';
        }

        $level2Value = explode('&level2=', $level3Value[0]);
        if(!empty($level2Value[1])){
            $categoria = $level2Value[1];
        }
    }

    if(empty($categoria)){
        $subcategoria = '';
    }


    //LOCAL SCRIPTS
    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $linkAPPDocumentos = $obParam->getParamsByTag('linkAPPDocumentos');
    $svgCategoria = $obParam->getParamsByTag('svgCategoria');

    if(!empty($documento) || !empty($categoria) || !empty($subcategoria)){

        if(empty($documento)){
            if(!empty($subcategoria)){
                $docsCats = VirtualDeskSiteDocumentosHelper::getDocsCatsLevel4byCat($subcategoria);
                $levelAnterior = '&level3=';
                $levelAtual = '&level4=';
            } else if(!empty($categoria)){
                $docsCats = VirtualDeskSiteDocumentosHelper::getDocsCatsLevel3byCat($categoria);
                $levelAnterior = '&level2=';
                $levelAtual = '&level3=';
            }
        } else {
            $docsCats = 0;
        }

        $docsFiltered = VirtualDeskSiteDocumentosHelper::getDocsbyCatFiltered($documento, $categoria, $subcategoria, $maincat);

    } else {
        $docsFiltered = VirtualDeskSiteDocumentosHelper::getDocsbyCat($maincat);
        $docsCats = VirtualDeskSiteDocumentosHelper::getDocsCatsbyCat($maincat);
        $levelAnterior = '&level1=';
        $levelAtual = '&level2=';
    }

?>

<div class="showMobileBar">
    <div id="showMobile"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_PESQUISAAVANCADA');?></div>
</div>


<div class="filterDocs">

    <form class="lateral" id="filterDocs" action="" method="post" class="login-form" enctype="multipart/form-data" >

        <input type="hidden" class="form-control" autocomplete="off" placeholder="" name="urlParam" id="urlParam" value="<?php echo $urlParam; ?>"/>

        <!--   Nome Documento  -->
        <div class="form-group" id="search">
            <h4><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_PESQUISA');?></h4>
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="documento" id="documento" maxlength="250" value="<?php echo $documento ?>"/>
            <div id="find">
                <?php
                    echo file_get_contents("images/documentos/svg/search.svg");
                ?>
            </div>
        </div>

        <!--   Categoria  -->
        <div class="form-group">

            <h4><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA'); ?></h4>

            <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($categoria)){
                    $Categorias = VirtualDeskSiteDocumentosHelper::getCatLevel2ByCat($maincat);
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Categorias as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['categoria']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteDocumentosHelper::getCatLevel2SelectByCat($categoria) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php
                       $ExcludeCategoria = VirtualDeskSiteDocumentosHelper::excludeCatLevel2ByCat($maincat, $categoria);
                    ?>
                    <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['name']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>


        <?php
        $ListaSubcategorias = array();
        if(!empty($categoria)) {
            if( (int) $categoria > 0) $ListaSubcategorias = VirtualDeskSiteDocumentosHelper::getCatLevel3ByCat($categoria);
        }
        ?>
        <div id="blocoSubcategoria" class="form-group half right">
            <h4><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_SUBCATEGORIA'); ?></h4>
            <div class="value col-md-12">
                <select name="subcategoria" id="subcategoria" value="<?php echo $subcategoria;?>"
                    <?php
                    if(empty($categoria)) {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($subcategoria)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ESCOLHAOPCAO'); ?></option>
                        <?php foreach($ListaSubcategorias as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $subcategoria; ?>"><?php echo VirtualDeskSiteDocumentosHelper::getCatLevel3ByCatName($subcategoria);?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeSubcategoria = VirtualDeskSiteDocumentosHelper::excludeCatLevel3ByCat($categoria, $subcategoria);?>
                        <?php foreach($ExcludeSubcategoria as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['name']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>

            </div>
        </div>

        <div id="hideMobile"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_VOLTAR');?></div>

    </form>
</div>

<div class="docs">

    <?php

    if(count($docsCats)>0){
        echo '<div class="catsDocs">';
        foreach($docsCats as $rowWSL) :
            $idCat = $rowWSL['id'];
            $categoria = $rowWSL['categoria'];
            $idCatAnterior = $rowWSL['id_nivel1'];
            ?>
            <div class="item" value="<?php echo base64_encode( $categoria . $levelAnterior . $idCatAnterior . $levelAtual . $idCat);?>">
                <?php
                    echo '<div class="icon">' . file_get_contents($svgCategoria) . '</div>';
                    echo '<div class="catName">' . $categoria . '</div>';
                ?>
            </div>
            <?php
        endforeach;
        echo '</div>';
    } else {
        if(count($docsFiltered) > 0) {
            echo '<div class="documents">';

            foreach($docsFiltered as $rowWSL) :
                $referencia = $rowWSL['referencia'];
                $nome = $rowWSL['nome'];
                $visualizacoes = $rowWSL['visualizacoes'];
                $data_criacao = $rowWSL['data_criacao'];
                $data_alteracao = $rowWSL['data_alteracao'];
                $dataSubstr = substr($data_alteracao, 0, 10);
                $explodeDataSubst = explode('-', $dataSubstr);
                $dataPublicacao = $explodeDataSubst[2] . '-' . $explodeDataSubst[1] . '-' . $explodeDataSubst[0];

                $objEventFile = new VirtualDeskSiteDocumentosFilesHelper();
                $arFileList = $objEventFile->getFileGuestLinkByRefId_And_PreviewJS($referencia);
                $FileList21Html = '';
                foreach ($arFileList as $rowFile) {
                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, true) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                }

                ?>

                <div class="docItem">

                    <?php switch ($rowFile->type) :
                    case 'application/pdf':
                        ?>
                        <div>
                            <a href="<?php echo JUri::root().'templates/virtualdesk/js/pdfjs/web/viewer.html?file=' .urlencode($rowFile->guestlink); ?>" target="_blank" value="<?php echo $referencia; ?>">
                                <div class="docName"><?php echo $nome;?></div>
                            </a>
                        </div>

                        <div>
                            <iframe id="iFrm<?php echo $referencia; ?>" src="" url2Src="<?php echo JUri::root().'templates/virtualdesk/js/pdfjs/web/viewer.html?file=' .urlencode($rowFile->guestlink); ?>"
                                    style="display:none;" width='800px' height='350px' ></iframe>
                        </div>


                        <?  break; ?>

                        <iframe src="https://docs.google.com/gview?url=<?php echo $linkAPPDocumentos;?>/documentos/dl/%3Frefid%3D88z0wzajsy04wck4404w%26bname%3D475b5a5ce472687f43d4abb0fe2f75ae%26cname%3Da896e47f64232fe7132125d976b2bf55579dd709e7d04265b5e0c031c238cfb9c9447e921e32015658aff9f4b576cdb9b84a4744adef24a2399af4fc94340be3&embedded=true"
                                width='400px' height='600px'
                                frameborder="0">
                        </iframe>
                    <?php
                    // Visualizar WORD EXCEL ou PPT no google docs
                    case 'application/msword':
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    case 'application/vnd.ms-excel':
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    // case 'application/pdf':  // TESTE

                    ?>
                        <div>
                            <a href="https://docs.google.com/viewerng/viewer?url=<?php echo urlencode($rowFile->guestlink); ?>" target="_blank" value="<?php echo $referencia; ?>">
                                <div class="docName"><?php echo $nome;?></div>
                            </a>
                        </div>
                        <div>
                            <a href="https://docs.google.com/viewerng/viewer?url=<?php echo urlencode($rowFile->guestlink); ?>" target="_blank" value="<?php echo $referencia; ?>">
                                Abrir noutra janela
                            </a>
                        </div>

                    <?  break; ?>


                    <?php
                    case 'image/jpg':
                    // Visualizar uma imagem
                    ?>
                        <div class="docName"><?php echo $nome;?></div>
                        <div class="file-loading">
                            <input type="file" name="fileupload" id="fileupload<?php echo $referencia; ?>" previewJS="<?php echo $rowFile->previewjs; ?>" />
                        </div>
                        <script>
                            jQuery(document).ready(function() {
                                setTimeout(function () {
                                    ComponentFileUploadervPlg.init('fileupload<?php echo $referencia; ?>');
                                }, 500);
                            });
                        </script>
                    <?  break; ?>


                    <?  default: ?>
                        <a href="<?php echo $rowFile->guestlink; ?>" value="<?php echo $referencia; ?>">
                            <div class="docName"><?php echo $nome;?></div>
                        </a>

                    <? endswitch; ?>


                    <div class="docVisual"><?php echo $visualizacoes . ' ' . JText::_('COM_VIRTUALDESK_DOCUMENTOS_VISUALIZACOES');?></div>
                    <div class="docPub"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_PUBLICADO') . $dataPublicacao;?></div>
                </div>
            <?php
            endforeach;
            echo '</div>';
        } else {
            ?>
            <h3 class="noResults"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_SEMRESULTADOS');?></h3>
            <?php
        }
    }

    ?>
</div>


<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/documentos/documentosByCat.js.php');
    ?>
</script>
