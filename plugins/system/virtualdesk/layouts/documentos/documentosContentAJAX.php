<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('documentosContent');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;
    $link = $jinput->get('link');
    $urlParam = $jinput->get('urlParam');
    $level = '';
    $numberResults = 0;

    if(empty($urlParam)){
        $level4Value = explode('&level4=', base64_decode($link));
        if(!empty($level4Value[1])){
            $idCatLevel4Link = $level4Value[1];
        } else {
            $idCatLevel4Link = '';
        }

        $level3Value = explode('&level3=', $level4Value[0]);
        if(!empty($level3Value[1])){
            $idCatLevel3Link = $level3Value[1];
        } else {
            $idCatLevel3Link = '';
        }

        $level2Value = explode('&level2=', $level3Value[0]);
        if(!empty($level2Value[1])){
            $idCatLevel2Link = $level2Value[1];
        } else {
            $idCatLevel2Link = '';
        }

        $level1Value = explode('&level1=', $level2Value[0]);
        if(!empty($level1Value[1])){
            $idCatLevel1Link = $level1Value[1];
        } else {
            $idCatLevel1Link = '';
        }
    } else {
        $level4Value = explode('&level4=', base64_decode($urlParam));
        if(!empty($level4Value[1])){
            $idCatLevel4Link = $level4Value[1];
        } else {
            $idCatLevel4Link = '';
        }

        $level3Value = explode('&level3=', $level4Value[0]);
        if(!empty($level3Value[1])){
            $idCatLevel3Link = $level3Value[1];
        } else {
            $idCatLevel3Link = '';
        }

        $level2Value = explode('&level2=', $level3Value[0]);
        if(!empty($level2Value[1])){
            $idCatLevel2Link = $level2Value[1];
        } else {
            $idCatLevel2Link = '';
        }

        $level1Value = explode('&level1=', $level2Value[0]);
        if(!empty($level1Value[1])){
            $idCatLevel1Link = $level1Value[1];
        } else {
            $idCatLevel1Link = '';
        }
    }

    $obParam      = new VirtualDeskSiteParamsHelper();
    $svgCategoria = $obParam->getParamsByTag('svgCategoria');
    $linkAPPDocumentos = $obParam->getParamsByTag('linkAPPDocumentos');
    $tituloEntradaDocumentos = $obParam->getParamsByTag('tituloEntradaDocumentos');

    //LOCAL SCRIPTS
    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    //GLOBAL SCRIPTS

    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;




//BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;


//END GLOBAL MANDATORY STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

?>

<div class="content-docs">

    <form class="searchDocs" action="" method="post" class="login-form" enctype="multipart/form-data" >
        <!--Formulário de pesquisa-->
        <input type="hidden" class="form-control" autocomplete="off" placeholder="" name="urlParam" id="urlParam" value="<?php echo $urlParam; ?>"/>
    </form>

    <?php
    if(!empty($idCatLevel1Link) || !empty($idCatLevel2Link) || !empty($idCatLevel3Link) || !empty($idCatLevel4Link)) {

        if(!empty($idCatLevel1Link) && empty($idCatLevel2Link) && empty($idCatLevel3Link) && empty($idCatLevel4Link)){
            $catsFiltered = VirtualDeskSiteDocumentosHelper::getCatsFilteredLevel1($idCatLevel1Link);
            $levelAnterior = '&level1=';
            $levelAtual = '&level2=';
            $level = 2;
        } else if(!empty($idCatLevel1Link) && !empty($idCatLevel2Link) && empty($idCatLevel3Link) && empty($idCatLevel4Link)){
            $catsFiltered = VirtualDeskSiteDocumentosHelper::getCatsFilteredLevel2($idCatLevel2Link);
            $levelAnterior = '&level2=';
            $levelAtual = '&level3=';
            $level = 3;
        } else if(empty($idCatLevel1Link) && !empty($idCatLevel2Link) && !empty($idCatLevel3Link) && empty($idCatLevel4Link)){
            $catsFiltered = VirtualDeskSiteDocumentosHelper::getCatsFilteredLevel3($idCatLevel3Link);
            $levelAnterior = '&level3=';
            $levelAtual = '&level4=';
            $level = 4;
            $idCatLevel1Link = VirtualDeskSiteDocumentosHelper::getIdCatLevel1($idCatLevel2Link);
        } else if(empty($idCatLevel1Link) && empty($idCatLevel2Link) && !empty($idCatLevel3Link) && !empty($idCatLevel4Link)){
            $level = 0;
            $idCatLevel2Link = VirtualDeskSiteDocumentosHelper::getIdCatLevel2($idCatLevel3Link);
            $idCatLevel1Link = VirtualDeskSiteDocumentosHelper::getIdCatLevel1($idCatLevel2Link);
        } else {
            $level = 0;
        }

        $docsFiltered = VirtualDeskSiteDocumentosHelper::getDocsFiltered($idCatLevel1Link, $idCatLevel2Link, $idCatLevel3Link, $idCatLevel4Link);

        /*Lista documentos*/
        if(count($docsFiltered) > 0) {
            echo '<div class="documents">';
                if(!empty($idCatLevel4Link)){
                    $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Select($idCatLevel3Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel4Select($idCatLevel4Link);
                } else if(!empty($idCatLevel3Link)){
                    $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Select($idCatLevel3Link);
                } else if(!empty($idCatLevel2Link)){
                    $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link);
                } else if(!empty($idCatLevel1Link)){
                    $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link);
                }

                echo '<h2>' . $catName . '</h2>';

                foreach($docsFiltered as $rowWSL) :
                    $referencia = $rowWSL['referencia'];
                    $nome = $rowWSL['nome'];
                    $visualizacoes = $rowWSL['visualizacoes'];
                    $data_criacao = $rowWSL['data_criacao'];
                    $data_alteracao = $rowWSL['data_alteracao'];
                    $dataSubstr = substr($data_alteracao, 0, 10);
                    $explodeDataSubst = explode('-', $dataSubstr);
                    $dataPublicacao = $explodeDataSubst[2] . '-' . $explodeDataSubst[1] . '-' . $explodeDataSubst[0];

                    $objEventFile = new VirtualDeskSiteDocumentosFilesHelper();
                    #$arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                    $arFileList = $objEventFile->getFileGuestLinkByRefId_And_PreviewJS($referencia);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, true) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                    }

                    ?>

                    <div class="docItem">

                        <?php switch ($rowFile->type) :
                            // Visualizar o PDF com iframe OU só primeira página OU icon + link para visualizador
                            case 'application/pdf':
                        ?>
                            <div>
                                <a href="<?php echo JUri::root().'templates/virtualdesk/js/pdfjs/web/viewer.html?file=' .urlencode($rowFile->guestlink); ?>" target="_blank" value="<?php echo $referencia; ?>">
                                <div class="docName"><?php echo $nome;?></div>
                                </a>
                            </div>
                           <!-- <div>
                                <a href="<?php echo JUri::root().'templates/virtualdesk/js/pdfjs/web/viewer.html?file=' .urlencode($rowFile->guestlink); ?>" target="_blank" value="<?php echo $referencia; ?>">
                                   Abrir noutra janela
                                </a>
                            </div>
                            <div>
                                <button  onclick="iFrameDocPreview.ShowiFrameDocPreview('iFrm<?php echo $referencia; ?>'); ">
                                    Abrir Visualização Aqui
                                </button>

                            </div>-->


                            <!--

                            <div>
                                <iframe id="iFrm<?php echo $referencia; ?>" src="" url2Src="<?php echo JUri::root().'templates/virtualdesk/js/pdfjs/web/viewer.html?file=' .urlencode($rowFile->guestlink); ?>"
                                        style="display:none;" width='800px' height='350px' ></iframe>
                            </div>
                            -->



                        <?  break; ?>

                            <iframe src="https://docs.google.com/gview?url=<?php echo $linkAPPDocumentos;?>/documentos/dl/%3Frefid%3D88z0wzajsy04wck4404w%26bname%3D475b5a5ce472687f43d4abb0fe2f75ae%26cname%3Da896e47f64232fe7132125d976b2bf55579dd709e7d04265b5e0c031c238cfb9c9447e921e32015658aff9f4b576cdb9b84a4744adef24a2399af4fc94340be3&embedded=true"
                                    width='400px' height='600px'
                                    frameborder="0">
                            </iframe>
                        <?php
                            // Visualizar WORD EXCEL ou PPT no google docs
                            case 'application/msword':
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            case 'application/vnd.ms-excel':
                            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                            // case 'application/pdf':  // TESTE

                        ?>
                                <div>
                                    <a href="https://docs.google.com/viewerng/viewer?url=<?php echo urlencode($rowFile->guestlink); ?>" target="_blank" value="<?php echo $referencia; ?>">
                                        <div class="docName"><?php echo $nome;?></div>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://docs.google.com/viewerng/viewer?url=<?php echo urlencode($rowFile->guestlink); ?>" target="_blank" value="<?php echo $referencia; ?>">
                                        Abrir noutra janela
                                    </a>
                                </div>
                               <!-- <div>
                                    <iframe src ="https://docs.google.com/gview?url=<?php echo urlencode($rowFile->guestlink); ?>"
                                            width='800px' height='350px' ></iframe>
                                </div>
                               -->

                        <?  break; ?>


                        <?php
                            case 'image/jpg':
                           // Visualizar uma imagem
                        ?>
                            <div class="docName"><?php echo $nome;?></div>
                            <div class="file-loading">
                                <input type="file" name="fileupload" id="fileupload<?php echo $referencia; ?>" previewJS="<?php echo $rowFile->previewjs; ?>" />
                            </div>
                            <script>
                                jQuery(document).ready(function() {
                                    setTimeout(function () {
                                        ComponentFileUploadervPlg.init('fileupload<?php echo $referencia; ?>');
                                    }, 500);
                                });
                            </script>
                        <?  break; ?>


                        <?  default: ?>
                        <a href="<?php echo $rowFile->guestlink; ?>" value="<?php echo $referencia; ?>">
                            <div class="docName"><?php echo $nome;?></div>
                        </a>

                        <? endswitch; ?>


                        <div class="docVisual"><?php echo $visualizacoes . ' ' . JText::_('COM_VIRTUALDESK_DOCUMENTOS_VISUALIZACOES');?></div>
                        <div class="docPub"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_PUBLICADO') . $dataPublicacao;?></div>
                    </div>
                <?php
                endforeach;
            echo '</div>';
        }


        /*Lista de Categorias*/

        $numberResults = count($catsFiltered);
        if($numberResults > 0) {
            echo '<div class="cats">';

            if(!empty($idCatLevel4Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Select($idCatLevel3Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel4Select($idCatLevel4Link);
            } else if(!empty($idCatLevel3Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Select($idCatLevel3Link);
            } else if(!empty($idCatLevel2Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link);
            } else if(!empty($idCatLevel1Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link);
            }

            echo '<h2>' . JText::_('COM_VIRTUALDESK_DOCUMENTOS_SUBCATEGORIAS_PLUGIN') . ' - ' . $catName . '</h2>';

            foreach($catsFiltered as $rowWSL) :
                $idCatLevel = $rowWSL['id'];
                $nameCatLevel = $rowWSL['categoria'];
                $idCatLevelAnterior = $rowWSL['nivel_anterior'];
                ?>
                <div class="item" value="<?php echo base64_encode( $nameCatLevel . $levelAnterior . $idCatLevelAnterior . $levelAtual . $idCatLevel);?>">
                    <?php
                    echo '<div class="icon">' . file_get_contents($svgCategoria) . '</div>';
                    echo '<div class="catName">' . $nameCatLevel . '</div>';
                    ?>
                </div>
            <?php
            endforeach;
            echo '</div>';
        }

        /*Sem Docs nem Categorias*/
        if(count($docsFiltered) == 0 && count($catsFiltered) == 0){
            if(!empty($idCatLevel4Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Select($idCatLevel3Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel4Select($idCatLevel4Link);
            } else if(!empty($idCatLevel3Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Select($idCatLevel3Link);
            } else if(!empty($idCatLevel2Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link) . ' - ' . VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($idCatLevel2Link);
            } else if(!empty($idCatLevel1Link)){
                $catName = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($idCatLevel1Link);
            }

            echo '<h2>' . $catName . '</h2>';
        }

    } else {

        $catLevel1 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Plugin();
        $level = 1;

        echo '<div class="cats">';
        echo '<h2>' . $tituloEntradaDocumentos . '</h2>';
        foreach($catLevel1 as $rowWSL) :
            $idCatLevel1 = $rowWSL['id'];
            $nameCatLevel1 = $rowWSL['categoria'];
            ?>
            <div class="item" value="<?php echo base64_encode( $nameCatLevel1 . '&level1=' . $idCatLevel1);?>">
                <?php
                echo '<div class="icon">' . file_get_contents($svgCategoria) . '</div>';
                echo '<div class="catName">' . $nameCatLevel1 . '</div>';
                ?>
            </div>
        <?php
        endforeach;
        echo '</div>';
    }
    ?>
</div>


<?php

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/documentos/documentosContent.js.php');
    ?>
</script>
