<?php


?>

var numbercats = <?php echo $numberCats;?>;
var numbercatsLevel2 = <?php echo $numberCatsLevel2;?>;
var numbercatsLevel3 = <?php echo $numberCatsLevel3;?>;

function removeAtiveLevel1(){
    for(var i=1; i<=numbercats; i++){
        jQuery('.tree-list .catLevel1:nth-child(' + i + ')').removeClass('active');
        jQuery('.tree-list .catLevel2').removeClass('active');
        jQuery('.tree-list .catLevel3').removeClass('active');
        jQuery('.tree-list .catLevel4').removeClass('active');
        jQuery('.tree-list .catLevel1:nth-child(' + i + ') .blockCat2').css('display','none');
        jQuery('.tree-list .catLevel1 .blockCat3').css('display','none');
        jQuery('.tree-list .catLevel1 .blockCat4').css('display','none');
        jQuery('.tree-list .catLevel1:nth-child(' + i + ') svg').css('-ms-transform', 'rotate(0deg)');
        jQuery('.tree-list .catLevel1:nth-child(' + i + ') svg').css('transform', 'rotate(0deg)');
    }
}

function removeSmothAtiveLevel1(value){
    jQuery('.tree-list .catLevel1:nth-child(' + value + ')').removeClass('active');
    jQuery('.tree-list .catLevel2').removeClass('active');
    jQuery('.tree-list .catLevel3').removeClass('active');
    jQuery('.tree-list .catLevel4').removeClass('active');
    jQuery('.tree-list .catLevel1:nth-child(' + value + ') .blockCat2').slideUp( "slow" );
    jQuery('.tree-list .catLevel1 .blockCat3').css('display','none');
    jQuery('.tree-list .catLevel1 .blockCat4').css('display','none');
    jQuery('.tree-list .catLevel1:nth-child(' + i + ') svg').css('-ms-transform', 'rotate(0deg)');
    jQuery('.tree-list .catLevel1:nth-child(' + i + ') svg').css('transform', 'rotate(0deg)');
}

function ativateLevel1(catLevel1){
    jQuery('.tree-list .catLevel1:nth-child(' + catLevel1 + ')').addClass('active');
    jQuery('.tree-list .catLevel1:nth-child(' + catLevel1 + ') .blockCat2').slideDown( "slow" );
    jQuery('.tree-list .catLevel1:nth-child(' + catLevel1 + ') .level1 svg').css('-ms-transform', 'rotate(90deg)');
    jQuery('.tree-list .catLevel1:nth-child(' + catLevel1 + ') .level1 svg').css('transform', 'rotate(90deg)');
    jQuery('.tree-list .catLevel2 svg').css('-ms-transform', 'rotate(0deg)');
    jQuery('.tree-list .catLevel2 svg').css('transform', 'rotate(0deg)');
}

function removeAtiveLevel2(){
    for(var i=1; i<=numbercatsLevel2; i++){
        jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + i + ')').removeClass('active');
        jQuery('.tree-list .catLevel3').removeClass('active');
        jQuery('.tree-list .catLevel4').removeClass('active');
        jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + i + ') .blockCat3').css('display','none');
        jQuery('.tree-list .catLevel1 .blockCat4').css('display','none');
        jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + i + ') svg').css('-ms-transform', 'rotate(0deg)');
        jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + i + ') svg').css('transform', 'rotate(0deg)');
    }
}

function removeSmothAtiveLevel2(value){
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + value + ')').removeClass('active');
    jQuery('.tree-list .catLevel3').removeClass('active');
    jQuery('.tree-list .catLevel4').removeClass('active');
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + value + ') .blockCat3').slideUp( "slow" );
    jQuery('.tree-list .catLevel1 .blockCat4').css('display','none');
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + value + ') svg').css('-ms-transform', 'rotate(0deg)');
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + value + ') svg').css('transform', 'rotate(0deg)');
}

function ativateLevel2(catLevel2){
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + catLevel2 + ')').addClass('active');
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + catLevel2 + ') .blockCat3').slideDown( "slow" );
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + catLevel2 + ') svg').css('-ms-transform', 'rotate(90deg)');
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(' + catLevel2 + ') svg').css('transform', 'rotate(90deg)');
    jQuery('.tree-list .catLevel3 svg').css('-ms-transform', 'rotate(0deg)');
    jQuery('.tree-list .catLevel3 svg').css('transform', 'rotate(0deg)');
}

function removeAtiveLevel3(){
    jQuery('.tree-list .blockCat3 .catLevel3').removeClass('active');
    jQuery('.tree-list .blockCat3 .catLevel3 .blockCat4').css('display','none');
    jQuery('.tree-list .blockCat3 .catLevel3 svg').css('-ms-transform', 'rotate(0deg)');
    jQuery('.tree-list .blockCat3 .catLevel3 svg').css('transform', 'rotate(0deg)');
}

function ativateLevel3(catLevel3){
    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(' + catLevel3 + ')').addClass('active');
    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(' + catLevel3 + ') .blockCat4').slideDown( "slow" );
    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(' + catLevel3 + ') svg').css('-ms-transform', 'rotate(90deg)');
    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(' + catLevel3 + ') svg').css('transform', 'rotate(90deg)');
    jQuery('.tree-list .catLevel4 svg').css('-ms-transform', 'rotate(0deg)');
    jQuery('.tree-list .catLevel4 svg').css('transform', 'rotate(0deg)');
}


function openCatDetail(valueCat){
    document.getElementById("urlParam").value = valueCat;
    jQuery('#submit').click();
}

jQuery( document ).ready(function() {

    /*Abre e Fecha Menu*/
    /*Nível 1*/

    jQuery('.tree-list .catLevel1:nth-child(1) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(1) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(1)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('1');
            } else {
                removeAtiveLevel1();
                ativateLevel1(1);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(2) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(2) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(2)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('2');
            } else {
                removeAtiveLevel1();
                ativateLevel1(2);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(3) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(3) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(3)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('3');
            } else {
                removeAtiveLevel1();
                ativateLevel1(3);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(4) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(4) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(4)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('4');
            } else {
                removeAtiveLevel1();
                ativateLevel1(4);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(5) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(5) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(5)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('5');
            } else {
                removeAtiveLevel1();
                ativateLevel1(5);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(6) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(6) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(6)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('6');
            } else {
                removeAtiveLevel1();
                ativateLevel1(6);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(7) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(7) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(7)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('7');
            } else {
                removeAtiveLevel1();
                ativateLevel1(7);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(8) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(8) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(8)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('8');
            } else {
                removeAtiveLevel1();
                ativateLevel1(8);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(9) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(9) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(9)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('9');
            } else {
                removeAtiveLevel1();
                ativateLevel1(9);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(10) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(10) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(10)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('10');
            } else {
                removeAtiveLevel1();
                ativateLevel1(10);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(11) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(11) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(11)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('11');
            } else {
                removeAtiveLevel1();
                ativateLevel1(11);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(12) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(12) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(12)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('12');
            } else {
                removeAtiveLevel1();
                ativateLevel1(12);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(13) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(13) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(13)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('13');
            } else {
                removeAtiveLevel1();
                ativateLevel1(13);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(14) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(14) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(14)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('14');
            } else {
                removeAtiveLevel1();
                ativateLevel1(14);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(15) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(15) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(15)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('15');
            } else {
                removeAtiveLevel1();
                ativateLevel1(15);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(16) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(16) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(16)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('16');
            } else {
                removeAtiveLevel1();
                ativateLevel1(16);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(17) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(17) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(17)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('17');
            } else {
                removeAtiveLevel1();
                ativateLevel1(17);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(18) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(18) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(18)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('18');
            } else {
                removeAtiveLevel1();
                ativateLevel1(18);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(19) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(19) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(19)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('19');
            } else {
                removeAtiveLevel1();
                ativateLevel1(19);
            }
        }
    });

    jQuery('.tree-list .catLevel1:nth-child(20) .level1 svg').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(20) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(20)" ).hasClass( "active" )){
                removeSmothAtiveLevel1('20');
            } else {
                removeAtiveLevel1();
                ativateLevel1(20);
            }
        }
    });



    jQuery('.tree-list .catLevel1:nth-child(1) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(1) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(1)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','1');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(2) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(2) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(2)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','2');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(3) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(3) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(3)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','3');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(4) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(4) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(4)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','4');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(5) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(5) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(5)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','5');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(6) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(6) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(6)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','6');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(7) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(7) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(7)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','7');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(8) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(8) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(8)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','8');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(9) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(9) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(9)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','9');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(10) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(10) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(10)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','10');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(11) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(11) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(11)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','11');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(12) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(12) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(12)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','12');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(13) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(13) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(13)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','13');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(14) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(14) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(14)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','14');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(15) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(15) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(15)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','15');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(16) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(16) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(16)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','16');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(17) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(17) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(17)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','17');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(18) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(18) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(18)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','18');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(19) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(19) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(19)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','19');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .catLevel1:nth-child(20) .name').click(function() {
        if(jQuery(".tree-list .catLevel1:nth-child(20) .blockCat2").length){
            if(jQuery(".tree-list .catLevel1:nth-child(20)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('1','20');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });



    /*Nível 2*/
    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(1) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(1) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(1)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('1');
            } else {
                removeAtiveLevel2();
                ativateLevel2(1);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(2) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(2) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(2)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('2');
            } else {
                removeAtiveLevel2();
                ativateLevel2(2);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(3) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(3) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(3)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('3');
            } else {
                removeAtiveLevel2();
                ativateLevel2(3);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(4) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(4) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(4)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('4');
            } else {
                removeAtiveLevel2();
                ativateLevel2(4);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(5) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(5) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(5)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('5');
            } else {
                removeAtiveLevel2();
                ativateLevel2(5);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(6) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(6) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(6)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('6');
            } else {
                removeAtiveLevel2();
                ativateLevel2(6);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(7) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(7) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(7)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('7');
            } else {
                removeAtiveLevel2();
                ativateLevel2(7);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(8) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(8) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(8)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('8');
            } else {
                removeAtiveLevel2();
                ativateLevel2(8);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(9) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(9) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(9)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('9');
            } else {
                removeAtiveLevel2();
                ativateLevel2(9);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(10) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(10) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(10)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('10');
            } else {
                removeAtiveLevel2();
                ativateLevel2(10);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(11) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(11) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(11)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('11');
            } else {
                removeAtiveLevel2();
                ativateLevel2(11);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(12) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(12) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(12)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('12');
            } else {
                removeAtiveLevel2();
                ativateLevel2(12);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(13) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(13) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(13)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('13');
            } else {
                removeAtiveLevel2();
                ativateLevel2(13);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(14) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(14) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(14)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('14');
            } else {
                removeAtiveLevel2();
                ativateLevel2(14);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(15) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(15) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(15)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('15');
            } else {
                removeAtiveLevel2();
                ativateLevel2(15);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(16) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(16) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(16)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('16');
            } else {
                removeAtiveLevel2();
                ativateLevel2(16);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(17) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(17) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(17)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('17');
            } else {
                removeAtiveLevel2();
                ativateLevel2(17);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(18) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(18) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(18)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('18');
            } else {
                removeAtiveLevel2();
                ativateLevel2(18);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(19) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(19) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(19)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('19');
            } else {
                removeAtiveLevel2();
                ativateLevel2(19);
            }
        }
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(20) svg').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(20) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(20)" ).hasClass( "active" )){
                removeSmothAtiveLevel2('20');
            } else {
                removeAtiveLevel2();
                ativateLevel2(20);
            }
        }
    });



    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(1) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(1) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(1)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','1');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(2) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(2) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(2)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','2');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(3) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(3) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(3)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','3');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(4) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(4) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(4)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','4');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(5) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(5) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(5)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','5');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(6) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(6) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(6)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','6');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(7) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(7) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(7)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','7');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(8) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(8) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(8)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','8');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(9) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(9) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(9)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','9');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(10) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(10) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(10)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','1');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(11) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(11) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(11)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','1');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(12) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(12) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(12)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','12');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(13) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(13) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(13)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','13');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(14) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(14) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(14)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','14');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(15) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(15) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(15)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','15');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(16) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(16) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(16)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','16');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(17) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(17) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(17)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','17');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(18) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(18) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(18)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','18');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(19) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(19) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(19)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','19');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat2 .catLevel2:nth-child(20) .name').click(function() {
        if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(20) .blockCat3").length){
            if(jQuery(".tree-list .blockCat2 .catLevel2:nth-child(20)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('2','20');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });



    /*Nível 3*/
    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(1) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(1) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(1)" ).hasClass( "active" )){

            } else {
                removeAtiveLevel3();
                ativateLevel3(1);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(2) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(2) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(2)" ).hasClass( "active" )){

            } else {
                removeAtiveLevel3();
                ativateLevel3(2);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(3) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(3) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(3)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(3);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(4) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(4) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(4)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(4);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(5) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(5) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(5)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(5);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(6) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(6) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(6)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(6);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(7) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(7) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(7)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(7);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(8) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(8) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(8)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(8);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(9) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(9) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(9)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(9);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(10) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(10) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(10)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(10);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(11) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(11) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(11)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(11);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(12) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(12) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(12)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(12);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(13) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(13) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(13)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(13);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(14) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(14) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(14)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(14);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(15) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(15) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(15)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(15);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(16) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(16) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(16)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(16);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(17) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(17) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(17)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(17);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(18) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(18) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(18)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(18);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(19) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(19) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(19)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(19);
            }
        }
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(20) svg').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(20) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(20)" ).hasClass( "active" )){
                //Do nothing
            } else {
                removeAtiveLevel3();
                ativateLevel3(20);
            }
        }
    });



    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(1) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(1) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(1)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','1');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(2) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(2) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(2)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','2');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(3) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(3) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(3)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','3');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(4) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(4) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(4)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','4');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(5) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(5) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(5)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','5');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(6) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(6) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(6)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','6');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(7) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(7) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(7)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','7');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(8) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(8) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(8)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','8');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(9) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(9) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(9)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','9');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(10) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(10) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(10)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','10');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(11) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(11) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(11)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','11');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(12) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(12) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(12)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','12');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(13) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(13) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(13)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','13');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(14) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(14) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(14)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','14');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(15) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(15) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(15)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','15');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(16) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(16) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(16)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','16');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(17) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(17) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(17)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','17');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(18) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(18) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(18)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','18');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(19) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(19) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(19)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','19');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

    jQuery('.tree-list .blockCat3 .catLevel3:nth-child(20) .name').click(function() {
        if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(20) .blockCat4").length){
            if(jQuery(".tree-list .blockCat3 .catLevel3:nth-child(20)" ).hasClass( "active" )){
                //Do nothing
            } else {
                newLevel('3','20');
            }
        }
        var valueCat = jQuery(this).attr('value');
        openCatDetail(valueCat);
    });

});
