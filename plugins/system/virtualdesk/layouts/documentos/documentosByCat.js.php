<?php
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
?>


var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


jQuery(document).ready(function() {

    ComponentsSelect2.init();

    jQuery("#categoria").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        jQuery('#urlParam').attr('value', 0);

        var categoria = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(categoria == null || categoria == '') {

            var select = jQuery('#subcategoria');
            select.find('option').remove();
            select.prop('disabled', true);

        } else {

            var select = jQuery('#subcategoria');
            select.find('option').remove();
            select.prop('disabled', true);

            var dataString = "categoria="+categoria; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoSubcategoria',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=documentos_categoriaLevel2Plugin&categoria=' + categoria ,

                success: function(output) {

                    var select = jQuery('#subcategoria');
                    select.find('option').remove();

                    if (output == null || output == '') {
                        select.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoSubcategoria');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoSubcategoria');
                }
            });
        }


    });

    jQuery('#categoria').on('change', function() {
        var select = jQuery('#subcategoria');
        select.find('option').remove();
        select.prop('disabled', true);

        jQuery('#submit').click();
    });

    jQuery('#subcategoria').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('.Docs .docs .item').click(function() {
        document.getElementById("urlParam").value = jQuery(this).attr('value');
        jQuery('#submit').click();
    });

    jQuery(document).on('keypress',function(e) {
        if(e.which == 13) {
            event.preventDefault();
            jQuery('#submit').click();
        }
    });

    jQuery('.Docs .docs .docItem a').click(function() {
        var refDoc = jQuery(this).attr('value');

        jQuery.ajax({
            url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
            data:'m=documentos_novaVisualizacao&refDoc=' + refDoc ,

            success: function(output) {

            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    });

    jQuery('#showMobile').click(function() {
        jQuery('.Docs .filterDocs').css('left','0');
        jQuery('.Docs .filterDocs').css('-webkit-transition','all 1s ease-in-out');
        jQuery('.Docs .filterDocs').css('-moz-transition','all 1s ease-in-out');
        jQuery('.Docs .filterDocs').css('-o-transition','all 1s ease-in-out');
        jQuery('.Docs .filterDocs').css('transition','all 1s ease-in-out');
    });

    jQuery('#hideMobile').click(function() {
        jQuery('.Docs .filterDocs').css('left','-100%');
        jQuery('.Docs .filterDocs').css('-webkit-transition','all 1s ease-in-out');
        jQuery('.Docs .filterDocs').css('-moz-transition','all 1s ease-in-out');
        jQuery('.Docs .filterDocs').css('-o-transition','all 1s ease-in-out');
        jQuery('.Docs .filterDocs').css('transition','all 1s ease-in-out');
    });

});