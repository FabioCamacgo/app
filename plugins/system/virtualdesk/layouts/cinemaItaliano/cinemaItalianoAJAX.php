<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('cinemaItaliano');
    if ($resPluginEnabled === false) exit();


    //GLOBAL SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = '';
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $linkPolPrivacidade = $obParam->getParamsByTag('linkPoliticaCleanSafe');
    $idConcelho = $obParam->getParamsByTag('concelhoClean&Safe');
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
    $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
    $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
    $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
    $moradaMunicipio = $obParam->getParamsByTag('moradaMunicipio');
    $codPostalMunicipio = $obParam->getParamsByTag('codPostalMunicipio');
    $logosendmailImage = $obParam->getParamsByTag('LogoEmailRegistoAPP');


    if (isset($_POST['submitForm'])) {
        $filme = $_POST['filme'];
        $numBilhetes = $_POST['numBilhetes'];
        $residente = $_POST['residente'];
        $nameReq = $_POST['nameReq'];
        $emailReq = $_POST['emailReq'];
        $emailConf = $_POST['emailConf'];
        $veracid2 = $_POST['veracid2'];
        $politica2 = $_POST['politica2'];

        if(empty($filme)){
            $errFilme = 1;
        } else {
            $errFilme = 0;
        }

        if($numBilhetes != 1 && $numBilhetes != 2){
            $errNumBilhetes = 1;
        } else {
            $errNumBilhetes = 0;
        }

        if($residente != 1 && $residente != 2){
            $errResidente = 1;
        } else {
            $errResidente = 0;
        }

        $errNomeReq = VirtualDeskSiteCinemaItalianoHelper::validaNome($nameReq);
        $errEmail = VirtualDeskSiteCinemaItalianoHelper::validaEmail($emailReq, $emailConf);

        // dynamic_recaptcha_1
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        $errRecaptcha = 1;
        if((string)$recaptcha_response_field !='') {
            $jinput->set('g-recaptcha-response', $recaptcha_response_field);
            $resRecaptchaAnswer = $captcha->checkAnswer($recaptcha_response_field);
            $errRecaptcha = 0;
            if (!$resRecaptchaAnswer) {
                $errRecaptcha = 1;
            }
        }

        if($errFilme == 0 && $errNumBilhetes == 0 && $errResidente == 0 && $errNomeReq == 0 && $errEmail == 0 &&  $politica2 == 1 && $veracid2 == 1 && $errRecaptcha == 0){

            $refExiste = 1;

            while($refExiste == 1){
                $referencia = VirtualDeskSiteCinemaItalianoHelper::random_code();
                $checkREF = VirtualDeskSiteCinemaItalianoHelper::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }

            $resSaveForm = VirtualDeskSiteCinemaItalianoHelper::saveNewForm($referencia, $filme, $numBilhetes, $residente, $nameReq, $emailReq);

            if($resSaveForm==true) :?>
                <style>
                    .itemid-1476 #submitForm{display:none !important;}
                </style>

                <div class="sucesso">

                    <div class="sucessIcon">
                        <?php echo file_get_contents("images/cmps/svg/success.svg"); ?>
                    </div>

                    <p><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_SUCESSO'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_SUCESSO2'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_SUCESSO3'); ?></p>
                    <p><?php echo JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_SUCESSO4',$nomeMunicipio); ?></p>
                    <p><?php echo JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_SUCESSO5',$copyrightAPP); ?></p>
                </div>

                <?php

                /*Send Email*/


                VirtualDeskSiteCinemaItalianoHelper::SendEmailAdmin($referencia, $filme, $numBilhetes, $residente, $nameReq, $emailReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio);
                VirtualDeskSiteCinemaItalianoHelper::SendEmailUser($referencia, $filme, $numBilhetes, $residente, $nameReq, $emailReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio);

                /*END Send Email*/

                exit();
            endif;

            exit();
        } else {
            ?>
            <script>
                dropError();
            </script>

            <div class="backgroundErro">
                <div class="erro" style="display:block;">
                    <button class="close">
                        <?php echo file_get_contents("images/cmps/svg/close.svg"); ?>
                    </button>

                    <h3>
                        <?php echo file_get_contents("images/cmps/svg/warning.svg") . ' ' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_AVISO'); ?>
                    </h3>

                    <ol>
                        <?php

                            if($errFilme == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_FILME') . '</li>';
                            } else {
                                $errFilme = 0;
                            }

                            if($errNumBilhetes == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_NUM_BILHETES') . '</li>';
                            } else {
                                $errNumBilhetes = 0;
                            }

                            if($errResidente == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_RESIDENTE') . '</li>';
                            } else {
                                $errResidente = 0;
                            }

                            if($errNomeReq == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_NOME_REQUERENTE_1') . '</li>';
                            } else if($errNomeReq == 2){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_NOME_REQUERENTE_2') . '</li>';
                            } else {
                                $errNomeReq = 0;
                            }

                            if($errEmail == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_EMAIL_REQUERENTE_1') . '</li>';
                            } else if($errEmail == 2){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_EMAIL_REQUERENTE_2') . '</li>';
                            } else {
                                $errEmail = 0;
                            }

                            if($veracid2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_VERACIDADE') . '</li>';
                            }

                            if($politica2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_POLITICA') . '</li>';
                            }

                            if($errRecaptcha == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_CINEMAITALIANO_ERRO_CAPTCHA') . '</li>';
                            } else {
                                $errRecaptcha = 0;
                            }
                        ?>
                    </ol>

                </div>
            </div>

            <?php
        }

    }

?>

<form id="new_cinemaItaliano" action="" method="post" class="cinemaItaliano-form" enctype="multipart/form-data" >

    <div class="block">

        <legend class="section"><h3><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_INFORMACAO_FILME'); ?></h3></legend>

        <div class="form-group half">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_FILME') ?><span class="required">*</span></label>
            <?php $Filmes = VirtualDeskSiteCinemaItalianoHelper::getFilmes()?>
            <div class="col-md-9">
                <select name="filme" value="<?php echo $filme; ?>" id="filme" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($filme)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_OPCAO'); ?></option>
                        <?php foreach($Filmes as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['filme'] . ' <span>(' . $rowWSL['data_filme'] .  ' - ' . $rowWSL['tipo'] .'</span>)'; ?></option>
                        <?php endforeach;
                    } else {
                        $NameFilme = VirtualDeskSiteCinemaItalianoHelper::getFilmeName($filme);
                        foreach($NameFilme as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['filme'] . ' <span>(' . $rowWSL['data_filme'] .  ' - ' . $rowWSL['tipo'] .'</span>)'; ?></option>
                        <?php endforeach;
                        ?>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFilme = VirtualDeskSiteCinemaItalianoHelper::excludeFilme($filme)?>
                        <?php foreach($ExcludeFilme as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['filme'] . ' <span>(' . $rowWSL['data_filme'] .  ' - ' . $rowWSL['tipo'] .'</span>)'; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group half tickets">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_NUM_BILHETES'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="radio" name="radiovalTickets" id="um" value="um" <?php if (isset($_POST["submitForm"]) && $_POST['numBilhetes'] == 1) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_UM_BILHETE'); ?>
                <input type="radio" name="radiovalTickets" id="dois" value="dois" <?php if (isset($_POST["submitForm"]) && $_POST['numBilhetes'] == 2) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_DOIS_BILHETES'); ?>
            </div>

            <input type="hidden" id="numBilhetes" name="numBilhetes" value="<?php echo $numBilhetes; ?>">
        </div>

    </div>

    <div class="block">

        <legend class="section"><h3><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_IDENTIFICACAO_REQUERENTE'); ?></h3></legend>

        <div class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_RELACAOCONCELHO'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="radio" name="radioval" id="sim" value="sim" <?php if (isset($_POST["submitForm"]) && $_POST['residente'] == 1) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_RESIDENTE_CONCELHO'); ?>
                <input type="radio" name="radioval" id="nao" value="nao" <?php if (isset($_POST["submitForm"]) && $_POST['residente'] == 2) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_NAO_RESIDENTE'); ?>
            </div>

            <input type="hidden" id="residente" name="residente" value="<?php echo $residente; ?>">
        </div>

        <div class="form-group nomeReq">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_NOMEREQUERENTE'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" name="nameReq" id="nameReq" value="<?php echo htmlentities($nameReq, ENT_QUOTES, 'UTF-8'); ?>" maxlength="250"/>
            </div>
        </div>

        <div class="form-group half">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="emailReq" id="emailReq" value="<?php echo $emailReq; ?>"/>
            </div>
        </div>

        <div class="form-group half">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL_REPEAT'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="emailConf" id="emailConf" value="<?php echo $emailConf; ?>"/>
            </div>
        </div>
    </div>

    <div class="block">
        <legend class="section"><h3><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_VERIFICACOES'); ?></h3></legend>

        <div class="form-group" id="Veracidade">

            <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CINEMAITALIANO_VERACIDADE'); ?><span class="required">*</span></label>

            <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
        </div>

        <div class="form-group" id="PoliticaPrivacidade">

            <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
            <label class="col-md-3 control-label"><?php echo JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_POLITICAPRIVACIDADE',$linkPolPrivacidade); ?><span class="required">*</span></label>

            <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
        </div>
    </div>



    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group" >
            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
            $field_id = 'dynamic_recaptcha_1';
            print $captcha->display($field_id, $field_id, 'g-recaptcha');
            ?>
            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
        </div>
    <?php endif; ?>



    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
    </div>

</form>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/cinemaItaliano/cinemaItaliano.js.php');
    ?>
</script>