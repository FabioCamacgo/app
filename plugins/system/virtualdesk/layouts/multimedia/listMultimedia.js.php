<?php
    defined('_JEXEC') or die;
?>

var idClicked = '';

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {

    // Select 2
    ComponentsSelect2.init();

    jQuery('#categoria').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#ano').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#find').click(function() {
        jQuery('#submit').click();
    });

    jQuery(document).on('keypress',function(e) {
        if(e.which == 13) {
            event.preventDefault();
            jQuery('#submit').click();
        }
    });

    jQuery(".videoItem .overlay").on( "click", function() {
        idClicked = jQuery(this).attr( "id" );

        jQuery('#popup' + idClicked).css('display', 'flex');
        jQuery('#popup' + idClicked + ' iframe').addClass("youtube-iframe");
        jQuery('html').css('overflow-y', 'hidden');
        jQuery('#g-container-main').css('z-index','9');
        jQuery('#g-container-main').css('position','fixed');
    });

    jQuery("#closeVideo svg").click(function() {
        jQuery('#popup' + idClicked).css('display', 'none');
        //jQuery('#player' + idClicked).stopVideo();
        jQuery('.youtube-iframe').each(function(index) {
            jQuery(this).attr('src', jQuery(this).attr('src'));
            return false;
        });
        idClicked = '';
        jQuery('html').css('overflow-y', 'scroll');
        jQuery('#g-container-main').css('z-index','1');
        jQuery('#g-container-main').css('position','relative');
    });

});