<?php

?>

var idClicked = '';

jQuery( document ).ready(function() {
    jQuery(".videoItem .overlay").on( "click", function() {
        idClicked = jQuery(this).attr( "id" );

        jQuery('#popup' + idClicked).css('display', 'flex');
        jQuery('#popup' + idClicked + ' iframe').addClass("youtube-iframe");
        jQuery('html').css('overflow-y', 'hidden');
        jQuery('#g-container-showcase').css('z-index','9');
    });

    jQuery("#closeVideo svg").click(function() {
        jQuery('#popup' + idClicked).css('display', 'none');
        //jQuery('#player' + idClicked).stopVideo();
        jQuery('.youtube-iframe').each(function(index) {
            jQuery(this).attr('src', jQuery(this).attr('src'));
            return false;
        });
        idClicked = '';
        jQuery('html').css('overflow-y', 'scroll');
        jQuery('#g-container-showcase').css('z-index','1');
    });
});


