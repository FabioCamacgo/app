<?php
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listMultimedia');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;


    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $pesquisaMultimedia = $obParam->getParamsByTag('pesquisaMultimedia');
    $botaoPlay = $obParam->getParamsByTag('botaoPlay');
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $botaoFechar = $obParam->getParamsByTag('botaoFechar');

    if(!empty($pesquisaLivre) || !empty($categoria) || !empty($ano)){
        $listVideos = VirtualDeskSiteMultimediaHelper::getListVideosFiltered($pesquisaLivre, $categoria, $ano);
    } else {
        $listVideos = VirtualDeskSiteMultimediaHelper::getListVideos();
    }

    $i = 0;

?>

<div class="searchBar">
    <div class="searchBarContent">
        <form class="results" action="" method="post" enctype="multipart/form-data" >

            <div class="form-group">
                <h3><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_PESQUISALIVRE');?></h3>
                <input type="text" class="form-control" autocomplete="off" placeholder="" name="pesquisaLivre" id="pesquisaLivre" maxlength="250" value="<?php echo $pesquisaLivre ?>"/>
                <div id="find">
                    <?php
                        echo file_get_contents($pesquisaMultimedia);
                    ?>
                </div>
            </div>


            <div class="form-group">

                <h3><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_CATEGORIA_FILTER'); ?></h3>

                <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    $Categorias = VirtualDeskSiteMultimediaHelper::getCategoria();
                    if(empty($categoria)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_ESCOLHAOPCAO'); ?></option>
                        <?php foreach($Categorias as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['categoria']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteMultimediaHelper::getCategoriaSelect($categoria) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php
                        $ExcludeCategoria = VirtualDeskSiteMultimediaHelper::excludeCategoria($categoria);
                        foreach($ExcludeCategoria as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['categoria']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>


            <div class="form-group">

                <h3><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_ANO_FILTER'); ?></h3>

                <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    $Anos = VirtualDeskSiteMultimediaHelper::getAnos();
                    if(empty($ano)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_ESCOLHAOPCAO'); ?></option>
                        <?php foreach($Anos as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['ano']; ?>"><?php echo $rowWSL['ano']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $ano; ?>"><?php echo $ano; ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php
                        $ExcludeAno = VirtualDeskSiteMultimediaHelper::excludeAno($ano);
                        foreach($ExcludeAno as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['ano']; ?>"><?php echo $rowWSL['ano']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </form>
    </div>
</div>


<div class="resultsContent">
    <?php
        if(count($listVideos) == 0){
            echo '<h3 class="noResults">' . JText::_('COM_VIRTUALDESK_MULTIMEDIA_SEMVIDEOS') . '</h3>';
        } else {
            foreach ($listVideos as $row):
                $i = $i + 1;
                $referencia = $row['referencia'];
                $link = $row['link'];
                $nome = $row['nome'];

                ?>

                <div class="videoItem">
                    <div id="item<?php echo $i;?>" class="overlay" title="<?php echo $titlePlataforma . ' - ' . $nome;?>">
                        <?php echo file_get_contents($botaoPlay);?>
                    </div>
                    <?php
                        if (strpos($link, 'youtube') !== false) {

                            $youtubeCode = $obParam->getParamsByTag('youtubeCode');

                            if (strpos($link, 'watch?v=') !== false) {
                                $explodeLink = explode("watch?v=", $link);
                                $idVideo = explode('&', $explodeLink[1]);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($link, 'embed') !== false) {
                                $explodeLink = explode("embed/", $link);
                                $idVideo = explode('?', $explodeLink[1]);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }
                        }

                        if (strpos($link, 'youtu.be') !== false) {
                            $explodeLink = explode("youtu.be/", $link);
                            $idVideo = explode('?', $explodeLink[1]);

                            $youtubeCode = $obParam->getParamsByTag('youtubeCode');
                            ?>
                            <div class="video">
                                <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <?php
                        }

                        if (strpos($link, 'facebook') !== false){

                            $explodeLink = explode("facebook.com/", $link);
                            $idVideo = $explodeLink[1];
                            $facebookCode = $obParam->getParamsByTag('facebookCode');

                            ?>

                            <div class="video">
                                <iframe src="<?php echo $facebookCode . urlencode($link);?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <?php
                        }

                        if (strpos($link, 'vimeo') !== false){
                            $vimeoCode = $obParam->getParamsByTag('vimeoCode');

                            if (strpos($link, 'player.vimeo.com/video/') !== false) {
                                $explodeLink = explode("player.vimeo.com/video/", $link);

                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($link, '/vimeo.com/') !== false) {
                                $explodeLink = explode("/vimeo.com/", $link);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }
                        }
                    ?>
                    <h5><?php echo $nome;?></h5>
                </div>


                <div class="seeVideo" id="popupitem<?php echo $i;?>">
                    <div id="closeVideo">
                        <?php echo file_get_contents($botaoFechar);?>
                    </div>
                    <?php
                        if (strpos($link, 'youtube') !== false) {

                            $youtubeCode = $obParam->getParamsByTag('youtubeCode');

                            if (strpos($link, 'watch?v=') !== false) {
                                $explodeLink = explode("watch?v=", $link);
                                $idVideo = explode('&', $explodeLink[1]);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($link, 'embed') !== false) {
                                $explodeLink = explode("embed/", $link);
                                $idVideo = explode('?', $explodeLink[1]);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }
                        }

                        if (strpos($link, 'youtu.be') !== false) {
                            $explodeLink = explode("youtu.be/", $link);
                            $idVideo = explode('?', $explodeLink[1]);

                            $youtubeCode = $obParam->getParamsByTag('youtubeCode');
                            ?>
                            <div class="video">
                                <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <?php
                        }

                        if (strpos($link, 'facebook') !== false){

                            $explodeLink = explode("facebook.com/", $link);
                            $idVideo = $explodeLink[1];
                            $facebookCode = $obParam->getParamsByTag('facebookCode');

                            ?>

                            <div class="video">
                                <iframe src="<?php echo $facebookCode . urlencode($link);?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <?php
                        }

                        if (strpos($link, 'vimeo') !== false){
                            $vimeoCode = $obParam->getParamsByTag('vimeoCode');

                            if (strpos($link, 'player.vimeo.com/video/') !== false) {
                                $explodeLink = explode("player.vimeo.com/video/", $link);

                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($link, '/vimeo.com/') !== false) {
                                $explodeLink = explode("/vimeo.com/", $link);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }
                        }
                    ?>
                </div>

                <?php
            endforeach;
        }
    ?>
</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/multimedia/listMultimedia.js.php');
    ?>
</script>
