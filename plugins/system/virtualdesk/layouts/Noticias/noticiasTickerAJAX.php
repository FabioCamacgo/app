<?php
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('noticiasTicker');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;


    //LOCAL SCRIPTS

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Noticias/breaking-news-ticker.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Noticias/breaking-news-ticker.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $idCatAmbiente = $obParam->getParamsByTag('idCatAmbiente');
    $idCatAcontece = $obParam->getParamsByTag('idCatAcontece');

    if(empty($cats[0]) || $cats[0] == '') {
        $Noticia = VirtualDeskSiteNoticiasHelper::getNoticiasTicker($limitNews);
    } else {
        $Noticia = VirtualDeskSiteNoticiasHelper::getNoticiasTickerByCat($cats[0], $cats[1], $cats[2], $cats[3], $cats[4], $limitNews);
    }
?>

<div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="newsTicker">
    <div class="bn-news">
        <ul>
            <?php
                foreach($Noticia as $rowWSL) :
                    $referencia = $rowWSL['referencia'];
                    $titulo = $rowWSL['titulo'];
                    $data_publicacao = $rowWSL['data_publicacao'];
                    $categoria = $rowWSL['categoria'];
                    $catName = $rowWSL['catName'];
                    $alt = str_replace(' ', '_', $titulo);
                    $alt2 = str_replace('#', '', $alt);
                    $arrData = explode('-', $data_publicacao);

                    if($categoria == $idCatAcontece){
                        $detalhe = $linkAcontece;
                        $class = str_replace(' ', '_', $catName);
                    } else {
                        $detalhe = $linkGeral;
                        if(!empty($idCatAmbiente)){
                            $catsAmbiente = explode(',', $idCatAmbiente);
                            for($i=0;$i<count($catsAmbiente);$i++){
                                if($categoria == $catsAmbiente[$i]){
                                    $class = 'ambiente';
                                    break;
                                } else {
                                    $class = 'geral';
                                }
                            }
                        } else {
                            $class = 'geral';
                        }
                    }

                    ?>
                    <li>
                        <div class="date <?php echo $class;?>">
                            <div class="day">
                                <?php echo $arrData[2]; ?>
                            </div>
                            <div class="month">
                                <?php echo VirtualDeskSiteNoticiasHelper::getMonthName($arrData[1]); ?>
                            </div>
                        </div>
                        <div class="link">
                            <a href="<?php echo $detalhe; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>" title="<?php echo $titulo;?>"><?php echo $titulo;?></a>
                        </div>
                    </li>
                    <?php

                endforeach;
            ?>
        </ul>
    </div>
    <div class="bn-controls" style="visibility:hidden;">
        <button><span class="bn-arrow bn-prev"></span></button>
        <button><span class="bn-action"></span></button>
        <button><span class="bn-arrow bn-next"></span></button>
    </div>
</div>

<?php

    echo $headScripts;
    echo $footerScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    jQuery('#newsTicker').breakingNews();
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Noticias/noticiasTicker.js.php');
    ?>
</script>
