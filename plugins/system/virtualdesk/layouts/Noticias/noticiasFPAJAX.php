<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('noticiasFP');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;


    $verTodasNoticias = $obParam->getParamsByTag('verTodasNoticias');
    $idCatAcontece = $obParam->getParamsByTag('idCatAcontece');
    $detalheNoticias = $linkGeral;
    $detalheAcontece = $linkAcontece;

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    if(empty($cats[0]) || $cats[0] == ''){
        /*Notícias de todas as CATS*/

        $newsNumber = 0;

        if(empty($exludedcats[0]) || $exludedcats[0] == ''){
            $cat1 = 0;
            $cat2 = 0;
            $cat3 = 0;
            $cat4 = 0;
            $cat5 = 0;
        } else {
            $cat1 = $exludedcats[0];
            $cat2 = $exludedcats[1];
            $cat3 = $exludedcats[2];
            $cat4 = $exludedcats[3];
            $cat5 = $exludedcats[4];
        }
        $Noticia = VirtualDeskSiteNoticiasHelper::getNoticias($cat1, $cat2, $cat3, $cat4, $cat5, $limitBlocks);
        $NoticiaList = VirtualDeskSiteNoticiasHelper::getNoticias($cat1, $cat2, $cat3, $cat4, $cat5, 25);

        ?>
            <div class="newsBlocks">
                <?php
                    foreach($Noticia as $rowWSL) :
                        $referencia = $rowWSL['referencia'];
                        $titulo = $rowWSL['titulo'];
                        $noticia = $rowWSL['noticia'];
                        $categoria = $rowWSL['categoria'];
                        $data_publicacao = $rowWSL['data_publicacao'];
                        $alt = str_replace(' ', '_', $titulo);
                        $alt2 = str_replace('#', '', $alt);
                        $decodedText = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($noticia);

                        $explodeData = explode('-', $data_publicacao);
                        $newData = $explodeData[2] . '-' . $explodeData[1] . '-' . $explodeData[0];

                        $imgNoticia = VirtualDeskSiteNoticiasHelper::getImgNoticia($referencia);

                        ?>

                        <div class="item">
                            <a href="<?php echo $detalheNoticias; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>" title="<?php echo $titulo;?>">
                                <div class="itemImage">
                                    <?php
                                    if((int)$imgNoticia == 0){
                                        ?>
                                        <img src="<?php echo JUri::base();?>images/cmm/logo_azul_e_branco.png" alt="<?php echo $alt2;?>"/>
                                        <?php
                                    } else {
                                        $objEventFile = new VirtualDeskSiteNoticiasFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt2;?>"/>
                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                            </a>

                            <div class="itemContent">

                                <div class="itemInfo">
                                    <div class="date"><?php echo $newData;?></div>
                                    <div class="cat"><?php echo $categoria;?></div>
                                </div>

                                <a href="<?php echo $detalheNoticias; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>">
                                    <h4><?php echo $titulo;?></h4>
                                </a>

                                <?php
                                    $noBR = str_replace('<br>', '', $decodedText);
                                    $noLink = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $noBR);
                                    $noLink2 = str_replace('</a>', '', $noLink);
                                    $noBold = str_replace('<b>', '', $noLink2);
                                    $noBold2 = str_replace('</b>', '', $noBold);
                                    $nopp = str_replace('<p>', '', $noBold2);
                                    $nopp2 = str_replace('</p>', '', $nopp);
                                    $nohh = str_replace('<h3>', '', $nopp2);
                                    $nohh2 = str_replace('</h3>', '', $nohh);
                                    $nostrong = str_replace('<strong>', '', $nohh2);
                                    $nostrong2 = str_replace('</strong>', '', $nostrong);

                                    $text = explode(" ", $nostrong2);

                                    for($i=0; $i<9; $i++){
                                        if($i==0){
                                            $description = $text[$i];
                                        } else {
                                            $description = $description . ' ' . $text[$i];
                                        }
                                    }

                                ?>

                                <div class="itemDescription"><?php echo $description . '...';?></div>

                            </div>
                        </div>

                    <?php

                    endforeach;
                ?>
            </div>

            <div class="newsList">
                <ul>
                    <?php
                        foreach($NoticiaList as $rowWSL) :
                            if($newsNumber == $limitBlocks){
                                $referencia = $rowWSL['referencia'];
                                $titulo = $rowWSL['titulo'];
                                $alt = str_replace(' ', '_', $titulo);
                                ?>
                                <li><a href="<?php echo $detalheNoticias; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>"><?php echo $titulo;?></a></li>
                                <?php
                            } else {
                                $newsNumber = $newsNumber + 1;
                            }
                        endforeach;
                    ?>
                </ul>

                <a href="<?php echo $verTodasNoticias;?>" class="seeAll">Ver todas as notícias</a>
            </div>

        <?php

    } else {
        /*Noticias de Cat Especifica*/

        $Noticia = VirtualDeskSiteNoticiasHelper::getNoticiasByCat($cats[0], $cats[1], $cats[2], $cats[3], $cats[4], $limitBlocks);
        ?>

        <div class="newsBlocks">
            <?php

                foreach($Noticia as $rowWSL) :
                    $referencia = $rowWSL['referencia'];
                    $titulo = $rowWSL['titulo'];
                    $noticia = $rowWSL['noticia'];
                    $categoria = $rowWSL['categoria'];
                    $idCat = $rowWSL['idCat'];
                    $data_publicacao = $rowWSL['data_publicacao'];
                    $alt = str_replace(' ', '_', $titulo);
                    $alt2 = str_replace('#', '', $alt);
                    $decodedText = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($noticia);

                    $explodeData = explode('-', $data_publicacao);
                    $newData = $explodeData[2] . '-' . $explodeData[1] . '-' . $explodeData[0];

                    $imgNoticia = VirtualDeskSiteNoticiasHelper::getImgNoticia($referencia);

                    if($idCat == $idCatAcontece){
                        $detalheNoticia = $detalheAcontece;
                    } else {
                        $detalheNoticia = $detalheNoticias;
                    }

                    ?>
                        <div class="item">
                            <a href="<?php echo $detalheNoticia; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>" title="<?php echo $titulo;?>">
                                <div class="itemImage">
                                    <?php
                                        if((int)$imgNoticia == 0){
                                            ?>
                                                <img src="<?php echo JUri::base();?>images/cmm/logo_azul_e_branco.png" alt="<?php echo $alt2;?>"/>
                                            <?php
                                        } else {
                                            $objEventFile = new VirtualDeskSiteNoticiasFilesHelper();
                                            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                            $FileList21Html = '';
                                            foreach ($arFileList as $rowFile) {
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt2;?>"/>
                                                <?php
                                            }
                                        }
                                    ?>

                                </div>
                            </a>
                            <div class="itemContent">

                                <div class="itemInfo">
                                    <div class="date"><?php echo $newData;?></div>
                                    <div class="cat"><?php echo $categoria;?></div>
                                </div>

                                <a href="<?php echo $detalheNoticia; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>">
                                    <h4><?php echo $titulo;?></h4>
                                </a>

                                <?php
                                    $noBR = str_replace('<br>', '', $decodedText);
                                    $noLink = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $noBR);
                                    $noLink2 = str_replace('</a>', '', $noLink);
                                    $noBold = str_replace('<b>', '', $noLink2);
                                    $noBold2 = str_replace('</b>', '', $noBold);
                                    $nopp = str_replace('<p>', '', $noBold2);
                                    $nopp2 = str_replace('</p>', ' ', $nopp);
                                    $nohh = str_replace('<h3>', '', $nopp2);
                                    $nohh2 = str_replace('</h3>', '', $nohh);
                                    $nostrong = str_replace('<strong>', '', $nohh2);
                                    $nostrong2 = str_replace('</strong>', '', $nostrong);


                                    $text = explode(" ", $nostrong2);

                                    for($i=0; $i<15; $i++){
                                        if($i==0){
                                            $description = $text[$i];
                                        } else {
                                            $description = $description . ' ' . $text[$i];
                                        }
                                    }

                                ?>

                                <div class="itemDescription"><?php echo $description . '...';?></div>

                            </div>
                        </div>
                    <?php

                endforeach;

            ?>
        </div>
        <?php
    }

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Noticias/noticiasFP.js.php');
    ?>
</script>
