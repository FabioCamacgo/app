<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('noticiasFiltro');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;

    $link = $jinput->get('link');
    $noticia = $jinput->get('noticia','', 'string');
    $mes = $jinput->get('mes');
    $ano = $jinput->get('ano');
    $categoria = $jinput->get('categoria');


    $obParam      = new VirtualDeskSiteParamsHelper();

    if(!empty($cats[0]) || $cats[0] != ''){
        $detalheNoticias = $linkAcontece;
    } else {
        $detalheNoticias = $linkGeral;
    }

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    //$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    if(!empty($noticia) || !empty($mes) || !empty($ano) || !empty($categoria)){
        if(!empty($cats[0]) || $cats[0] != ''){
            $Categorias = VirtualDeskSiteNoticiasHelper::getCategoriaPluginByCat($cats[0], $cats[1], $cats[2], $cats[3], $cats[4]);
            $listaNoticias = VirtualDeskSiteNoticiasHelper::getNoticiasContentByCatFiltered($noticia, $mes, $ano, $categoria, $cats[0], $cats[1], $cats[2], $cats[3], $cats[4], $limit);
        } else {
            $Categorias = VirtualDeskSiteNoticiasHelper::getCategoriaPlugin($exludedcats[0], $exludedcats[1], $exludedcats[2], $exludedcats[3], $exludedcats[4]);
            $listaNoticias = VirtualDeskSiteNoticiasHelper::getNoticiasContentFiltered($noticia, $mes, $ano, $categoria, $exludedcats[0], $exludedcats[1], $exludedcats[2], $exludedcats[3], $exludedcats[4], $limit);
        }

    } else {
        if(!empty($cats[0]) || $cats[0] != ''){
            $Categorias = VirtualDeskSiteNoticiasHelper::getCategoriaPluginByCat($cats[0], $cats[1], $cats[2], $cats[3], $cats[4]);
            $listaNoticias = VirtualDeskSiteNoticiasHelper::getNoticiasContentByCat($cats[0], $cats[1], $cats[2], $cats[3], $cats[4], $limit);
        } else {
            $Categorias = VirtualDeskSiteNoticiasHelper::getCategoriaPlugin($exludedcats[0], $exludedcats[1], $exludedcats[2], $exludedcats[3], $exludedcats[4]);
            $listaNoticias = VirtualDeskSiteNoticiasHelper::getNoticiasContent($exludedcats[0], $exludedcats[1], $exludedcats[2], $exludedcats[3], $exludedcats[4], $limit);
        }
    }

?>

<div class="sideFilter inBlock">
    <form class="lateral" action="" method="post" class="login-form" enctype="multipart/form-data" >

        <!--   Noticia  -->
        <div class="form-group" id="search">
            <h4><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_PESQUISA');?></h4>
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="noticia" id="noticia" maxlength="250" value="<?php echo $noticia ?>"/>
            <div id="find">
                <?php
                echo file_get_contents("images/noticias/svg/search.svg");
                ?>
            </div>
        </div>


        <?php
            if(empty($cats[0]) || !empty($cats[1])){
                ?>
                <!--   Categoria  -->
                <div class="form-group">

                    <h4><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_CATEGORIA'); ?></h4>

                    <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($categoria)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_ESCOLHAOPCAO'); ?></option>
                            <?php foreach($Categorias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteNoticiasHelper::getCategoriaSelect($categoria) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php
                            if(!empty($cats[0]) || $cats[0] != ''){
                                $ExcludeCategoria = VirtualDeskSiteNoticiasHelper::excludeCategoriaPluginByCat($cats[0], $cats[1], $cats[2], $cats[3], $cats[4], $categoria);
                            } else {
                                $ExcludeCategoria = VirtualDeskSiteNoticiasHelper::excludeCategoriaPlugin($exludedcats[0], $exludedcats[1], $exludedcats[2], $exludedcats[3], $exludedcats[4], $categoria);
                            }
                            ?>
                            <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['categoria']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
                <?php
            }
        ?>


        <!--   Ano  -->
        <div class="form-group">
            <?php $Anos = VirtualDeskSiteNoticiasHelper::getAnoList()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_ANO'); ?></h4>

            <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($ano)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Anos as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['ano']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $ano; ?>"><?php echo VirtualDeskSiteNoticiasHelper::getAnoListSelect($ano) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeAno = VirtualDeskSiteNoticiasHelper::excludeAnoList($ano)?>
                    <?php foreach($ExcludeAno as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['ano']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>


        <!--   Mes  -->
        <div class="form-group">
            <?php $Meses = VirtualDeskSiteNoticiasHelper::getMesList()?>

            <h4><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_MESES'); ?></h4>

            <select name="mes" value="<?php echo $mes; ?>" id="mes" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($mes)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Meses as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['mes']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $mes; ?>"><?php echo VirtualDeskSiteNoticiasHelper::getMesListSelect($mes) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeMes = VirtualDeskSiteNoticiasHelper::excludeMesList($mes)?>
                    <?php foreach($ExcludeMes as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['mes']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>

        <div class="closeSearch"><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_GOBACK');?></div>

    </form>
</div>

<div class="pagecontent">

    <div class="mobile">
        <div class="searchButton"><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_PESQUISA_AVANCADA');?></div>
    </div>


    <div class="newsBlocks">
        <?php
        if(count($listaNoticias) == 0) {
            echo '<h3 class="noresults">' . JText::_('COM_VIRTUALDESK_NOTICIAS_NORESULTS') . '</h3>';
        } else {
            foreach($listaNoticias as $rowWSL) :
                $referencia = $rowWSL['referencia'];
                $titulo = $rowWSL['titulo'];
                $noticia = $rowWSL['noticia'];
                $categoria = $rowWSL['categoria'];
                $data_publicacao = $rowWSL['data_publicacao'];
                $alt = str_replace(' ', '_', $titulo);
                $alt2 = str_replace('#', '', $alt);
                $decodedText = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($noticia);

                $explodeData = explode('-', $data_publicacao);
                $newData = $explodeData[2] . '-' . $explodeData[1] . '-' . $explodeData[0];

                $imgNoticia = VirtualDeskSiteNoticiasHelper::getImgNoticia($referencia);

                ?>

                <div class="item">
                    <a href="<?php echo $detalheNoticias; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>" title="<?php echo $titulo;?>">
                        <div class="itemImage">
                            <?php
                            if((int)$imgNoticia == 0){
                                ?>
                                <img src="<?php echo JUri::base();?>images/cmm/logo_azul_e_branco.png" alt="<?php echo $alt2;?>"/>
                                <?php
                            } else {
                                $objEventFile = new VirtualDeskSiteNoticiasFilesHelper();
                                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                $FileList21Html = '';
                                foreach ($arFileList as $rowFile) {
                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                    ?>
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt2;?>"/>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </a>

                    <div class="itemContent">

                        <div class="itemInfo">
                            <div class="date"><?php echo $newData;?></div>
                            <div class="cat"><?php echo $categoria;?></div>
                        </div>

                        <a href="<?php echo $detalheNoticias; ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>">
                            <h4><?php echo $titulo;?></h4>
                        </a>


                        <?php
                            $noBR = str_replace('<br>', '', $decodedText);
                            $noLink = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $noBR);
                            $noLink2 = str_replace('</a>', '', $noLink);
                            $noBold = str_replace('<b>', '', $noLink2);
                            $noBold2 = str_replace('</b>', '', $noBold);
                            $nopp = str_replace('<p>', ' ', $noBold2);
                            $nopp2 = str_replace('</p>', '', $nopp);
                            $nohh = str_replace('<h3>', '', $nopp2);
                            $nohh2 = str_replace('</h3>', '', $nohh);
                            $nostrong = str_replace('<strong>', '', $nohh2);
                            $nostrong2 = str_replace('</strong>', '', $nostrong);

                            $text = explode(" ", $nostrong2);

                            for($i=0; $i<15; $i++){
                                if($i==0){
                                    $description = $text[$i];
                                } else {
                                    $description = $description . ' ' . $text[$i];
                                }
                            }

                        ?>

                        <div class="itemDescription"><?php echo $description . '...';?></div>

                    </div>
                </div>
            <?php
            endforeach;
        }
        ?>
    </div>
</div>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Noticias/filtro.js.php');
    ?>
</script>


