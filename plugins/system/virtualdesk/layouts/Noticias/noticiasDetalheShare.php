<?php
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();
    $idCatAcontece = $obParam->getParamsByTag('idCatAcontece');

    $imgCapa = VirtualDeskSiteNoticiasHelper::getImgNoticia($referencia);

    if($idCategoria == $idCatAcontece){
        $linkPaginaDetalhe = $obParam->getParamsByTag('linkPartilhaAcontece');
    } else {
        $linkPaginaDetalhe = $obParam->getParamsByTag('linkPartilhaNoticias');
    }

    $alt = str_replace(' ', '_', $titulo);
    $alt2 = str_replace('#', '', $alt);

?>

<head>
    <meta property="og:url"                content="" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="<?php echo $titulo;?>" />
    <meta property="og:description"        content="<?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descritivo);?> " />

    <?php

        $objEventFile = new VirtualDeskSiteNoticiasFilesHelper();
        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
        $FileList21Html = '';
        foreach ($arFileList as $rowFile) {
            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
            $LinkParaAImagem = $rowFile->guestlink;
        }

    ?>

    <meta property="og:image" content="<?php echo $LinkParaAImagem; ?>" >

</head>

<body>


<script type="text/javascript">
    window.location.href = "<?php echo $linkPaginaDetalhe ?>?item=<?php echo $alt2;?>&ref=<?php echo base64_encode($referencia);?>";
</script>
</body>


