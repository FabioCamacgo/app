<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('noticiasDetalhe');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;

    $obParam      = new VirtualDeskSiteParamsHelper();
    //$detalheNoticias = $obParam->getParamsByTag('detalheNoticias');
    $linkDefaultNoticias = $obParam->getParamsByTag('defaultPageNoticias');
    //$detalheAcontece = $obParam->getParamsByTag('detalheAcontece');
    $idAcontece = $obParam->getParamsByTag('idCatAcontece');
    $layoutClipping = $obParam->getParamsByTag('layoutClipping');
    $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');

    if(empty($link)){
        ?>
        <script>
            var DefaultPath = '<?php echo $linkDefaultNoticias; ?>';

            window.location=DefaultPath;
        </script>
        <?php
    } else {

        //LOCAL SCRIPTS
        $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

        //GLOBAL SCRIPTS
        $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
        $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


        //BEGIN GLOBAL MANDATORY STYLES
        $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
        $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
        $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

        //END GLOBAL MANDATORY STYLES

        //BEGIN THEME GLOBAL STYLES
        $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
        //END THEME GLOBAL STYLES

        // BEGIN PAGE LEVEL STYLES
        $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

        // CUSTOM JS DASHboard
        $footerScripts  = '<!--[if lt IE 9]>';
        $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
        $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
        $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
        $footerScripts .= '<![endif]-->';

        echo $headCSS;

        $refEncoded = explode('&ref=', $link);
        $referencia = base64_decode($refEncoded[1]);

        $imgCapa = VirtualDeskSiteNoticiasHelper::getImgNoticia($referencia);
        $imgGaleria = VirtualDeskSiteNoticiasHelper::getImgGaleria($referencia);
        $noticiasDetail = VirtualDeskSiteNoticiasHelper::getNoticiasDetailPlugin($referencia);

        foreach ($noticiasDetail as $row):
            $titulo = $row['titulo'];
            $descritivo = $row['noticia'];
            $categoria = $row['categoria'];
            $idCategoria = $row['idCategoria'];
            $facebook = $row['facebook'];
            $video = $row['video'];
        endforeach;

        $alt = str_replace(' ','_',$titulo);


        if((int)$isVDAjax2Share===1) {
            require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Noticias/noticiasDetalheShare.php');
        } else {

            ?>

            <div class="page-header">
                <h2 itemprop="headline" class="articletitle"><?php echo $titulo; ?></h2>
            </div>

            <dl class="article-info muted">
                <dt class="article-info-term">
                    <?php echo $categoria; ?>
                </dt>
            </dl>


            <?php
                if ($layoutClipping == $idCategoria) { /*Apenas para clipping*/
                    ?>

                    <div itemprop="articleBody">
                        <?php
                        $objEventFile = new VirtualDeskSiteNoticiasGaleriaHelper();
                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                        $FileList21Html = '';
                        foreach ($arFileList as $rowFile) {
                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                            ?>
                            <p>
                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>" itemprop="image"/>
                            </p>
                            <?php
                        }
                        ?>

                    </div>

                    <?php
                } else {
                    if (count($imgGaleria) == 0 && empty($video)) {
                        $objEventFile = new VirtualDeskSiteNoticiasFilesHelper();
                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                        $FileList21Html = '';
                        foreach ($arFileList as $rowFile) {
                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                            ?>
                            <div class="pull-none item-image">
                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>" itemprop="image"/>
                            </div>
                            <?php
                        }
                    }

                    ?>

                    <div itemprop="articleBody">
                        <?php
                        echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descritivo);
                        if (!empty($facebook)) {
                            ?>
                            <p><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_VERMAIS') . ' <a href="' . $facebook . '" target="_blank">aqui</a>'; ?></p>
                            <?php
                        }
                        ?>

                    </div>

                    <?php
                    if (count($imgGaleria) > 0) {
                        $objEventFile = new VirtualDeskSiteNoticiasGaleriaHelper();
                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                        $FileList21Html = '';

                        if (count($imgGaleria) == 1 || count($imgGaleria) == 2 || count($imgGaleria) == 4) {
                            ?>
                            <div class="multipleImages50">
                                <?php
                                foreach ($arFileList as $rowFile) {
                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                    ?>
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>"
                                         title="<?php echo $titulo; ?>"/>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="multipleImages">
                                <?php
                                foreach ($arFileList as $rowFile) {
                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                    ?>
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $alt; ?>"
                                         title="<?php echo $titulo; ?>"/>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                    }


                    if(!empty($video)){
                        if (strpos($video, 'youtube') !== false) {

                            $youtubeCode = $obParam->getParamsByTag('youtubeCode');

                            if (strpos($video, 'watch?v=') !== false) {
                                $explodeLink = explode("watch?v=", $video);
                                $idVideo = explode('&', $explodeLink[1]);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($video, 'embed') !== false) {
                                $explodeLink = explode("embed/", $video);
                                $idVideo = explode('?', $explodeLink[1]);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }
                        }

                        if (strpos($video, 'youtu.be') !== false) {
                            $explodeLink = explode("youtu.be/", $video);
                            $idVideo = explode('?', $explodeLink[1]);

                            $youtubeCode = $obParam->getParamsByTag('youtubeCode');
                            ?>
                            <div class="video">
                                <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <?php
                        }

                        if (strpos($video, 'facebook') !== false){

                            $explodeLink = explode("facebook.com/", $video);
                            $idVideo = $explodeLink[1];
                            $facebookCode = $obParam->getParamsByTag('facebookCode');


                            ?>
                            <!--<div class="video">
                                <iframe src="<?php echo $facebookCode . urlencode($video);?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>-->


                            <div id="fb-root"></div>
                            <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>

                            <!-- Your embedded video player code -->
                            <div class="fb-video" data-href="<?php echo $video;?>" data-width="500" data-show-text="false">

                            </div>



                            <?php
                        }

                        if (strpos($video, 'vimeo') !== false){
                            $vimeoCode = $obParam->getParamsByTag('vimeoCode');

                            if (strpos($video, 'player.vimeo.com/video/') !== false) {
                                $explodeLink = explode("player.vimeo.com/video/", $video);

                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($video, '/vimeo.com/') !== false) {
                                $explodeLink = explode("/vimeo.com/", $video);
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }
                        }

                    }
                }

            ?>
            <div class="navigateArticle">
                <?php
                    $noticiasDetailByCatID = VirtualDeskSiteNoticiasHelper::getNoticiasDetailByCatID($idCategoria);

                    if($idCategoria == $idAcontece){
                        $detalhe = $detalheAcontece;
                    } else {
                        $detalhe = $detalheGeral;
                    }

                    $refs = array();
                    foreach ($noticiasDetailByCatID as $row):
                        array_push($refs, $row['referencia']);
                    endforeach;

                    for($i=0; $i<count($refs); $i++){
                        if( $refs[$i] == $referencia){
                            $refAnterior = $refs[$i + 1];
                            $refSeguinte = $refs[$i - 1];
                            break;
                        }
                    }

                    if(!empty($refSeguinte)){
                        $newsTitleSeguinte = VirtualDeskSiteNoticiasHelper::getNewsName($refSeguinte);
                        $altSeguinte = str_replace(' ', '_', $newsTitleSeguinte);
                        $altSeguinte2 = str_replace('#', '', $altSeguinte);
                        ?>
                        <a href="https://<?php echo $detalhe; ?>?item=<?php echo $altSeguinte2;?>&ref=<?php echo base64_encode($refSeguinte);?>" title="<?php echo $newsTitleSeguinte; ?>">
                            <span><?php echo file_get_contents('images/svg/anterior.svg');?></span>
                            <span><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_ANTERIOR'); ?></span>
                        </a>
                        <?php
                    }

                    if(!empty($refAnterior)){
                        $newsTitleAnterior = VirtualDeskSiteNoticiasHelper::getNewsName($refAnterior);
                        $altAnterior = str_replace(' ', '_', $newsTitleAnterior);
                        $altAnterior2 = str_replace('#', '', $altAnterior);
                        ?>
                            <a href="https://<?php echo $detalhe; ?>?item=<?php echo $altAnterior2;?>&ref=<?php echo base64_encode($refAnterior);?>" title="<?php echo $newsTitleAnterior; ?>">
                                <span><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_SEGUINTE'); ?></span>
                                <span><?php echo file_get_contents('images/svg/seguinte.svg');?></span>
                            </a>
                        <?php
                    }
                ?>
            </div>
            <?php

        }
        ?>

        <?php
            echo $headScripts;
            echo $footerScripts;
            echo $localScripts;
    }

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/Noticias/noticiasDetalhe.js.php');
    ?>
</script>
