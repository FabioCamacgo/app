<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('tickerAssociacoes');
    if ($resPluginEnabled === false) exit();


    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/breaking-news-ticker.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/breaking-news-ticker.css' . $addcss_end;


    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
    echo $headScripts;


$ListaAssociacoes = VirtualDeskSiteAssociativismoHelper::getAssociacoesTicker();



    ?>
        <div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="example">
            <div class="bn-news">
                <ul>
                    <?php foreach($ListaAssociacoes as $row) :
                        $assCultura = 0;
                        $assSocial = 0;
                        $assDesporto = 0;
                        $assEducacao = 0;
                        $areasAtuacao = $row['areasAtuacao'];
                        $nome = $row['nome'];
                        $id = $row['id'];

                        $name = str_replace(' ', '_', $nome);
                        $code = $id . '_' . $name;
                        $pieces = explode(",", $areasAtuacao);

                        for($i = 0; $i < count($pieces); $i++){
                            if($pieces[$i] == 1){
                                $assCultura = 1;
                            } else if($pieces[$i] == 2){
                                $assDesporto = 1;
                            } elseif($pieces[$i] == 3){
                                $assSocial = 1;
                            } elseif($pieces[$i] == 4){
                                $assEducacao = 1;
                            }
                        }

                    ?>
                        <li>
                            <div class="imagens" title="<?php echo $nome; ?>">
                                <?php
                                    if($assCultura == 1){
                                        ?>
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Cultura.png" alt="Cultura" title="Associativismo - Cultura"/>
                                        <?php
                                    }
                                    if($assDesporto == 1){
                                        ?>
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Desporto.png" alt="Desporto" title="Associativismo - Desporto"/>
                                        <?php
                                    }
                                    if($assSocial == 1){
                                        ?>
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Social.png" alt="Social" title="Associativismo - Social"/>
                                        <?php
                                    }
                                    if($assEducacao == 1){
                                        ?>
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Educacao.png" alt="Educacao" title="Associativismo - Educação"/>
                                        <?php
                                    }
                                ?>
                            </div>
                            <div class="associacao" title="<?php echo $nome; ?>">
                                <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code; ?>">
                                    <?php echo $nome;?>
                                </a>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="bn-controls">
                <button><span class="bn-arrow bn-prev"></span></button>
                <button><span class="bn-action"></span></button>
                <button><span class="bn-arrow bn-next"></span></button>
            </div>
        </div>
    <?php

    echo $footerScripts;

?>

<script>
    jQuery('#example').breakingNews();
</script>
