<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheAssociacoes');
    if ($resPluginEnabled === false) exit();

    $link = $jinput->get('link','' ,'string');
    $ref = explode("?name=", $link);
    $getId = explode("_", $ref[1]);
    $id = $getId[0];

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/edit_maps.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/filtroAssociacoes.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;


    $getDetalhes = VirtualDeskSiteAssociativismoHelper::getDetalhes($id);

    foreach($getDetalhes as $rowWSL) :
        $referencia = $rowWSL['referencia'];
        $nome = $rowWSL['nome'];
        $apresentacao = $rowWSL['apresentacao'];
        $morada = $rowWSL['morada'];
        $codpostal = $rowWSL['codpostal'];
        $freguesia = $rowWSL['freguesia'];
        $email = $rowWSL['email'];
        $telefone = $rowWSL['telefone'];
        $website = $rowWSL['website'];
        $facebook = $rowWSL['facebook'];
        $instagram = $rowWSL['instagram'];
        $coordenadas = $rowWSL['coordenadas'];
        $areasAtuacao = $rowWSL['areasAtuacao'];
        $areasCultura = $rowWSL['areasCultura'];
        $outraCultura = $rowWSL['outraCultura'];
        $areasDesporto = $rowWSL['areasDesporto'];
        $outraDesporto = $rowWSL['outraDesporto'];
        $areasSocial = $rowWSL['areasSocial'];
        $outraSocial = $rowWSL['outraSocial'];
        $areasEducacao = $rowWSL['areasEducacao'];
        $outraEducacao = $rowWSL['outraEducacao'];

        $imgAssoc = VirtualDeskSiteAssociativismoHelper::getImgAssoc($referencia);
        $temImagem = 0;
        $name = str_replace(' ', '_', $nome);
        $assCultura = 0;
        $assSocial = 0;
        $assDesporto = 0;
        $assEducacao = 0;
        $resSubAreaCult = '';
        $resSubAreaDesp = '';
        $resSubAreaSoc = '';
        $resSubAreaEdu = '';


        $areasACT = explode(",", $areasAtuacao);

        for($i = 0; $i < count($areasACT); $i++){
            if($areasACT[$i] == 1){
                $assCultura = 1;
            } else if($areasACT[$i] == 2){
                $assDesporto = 1;
            } elseif($areasACT[$i] == 3){
                $assSocial = 1;
            } elseif($areasACT[$i] == 4){
                $assEducacao = 1;
            }
        }

        if(($assCultura == 1 && !empty($areasCultura)) || ($assCultura == 1 && !empty($outraCultura))){
            $subAreasCult = explode(",", $areasCultura);
            $resOutraCultura = str_replace('; ', ', ', $outraCultura);
            $resOutraCulturaExplode = explode(",", $resOutraCultura);
            for($c = 0; $c < count($subAreasCult); $c++){
                $resultado = VirtualDeskSiteAssociativismoHelper::getSubArea($subAreasCult[$c]);
                if(!empty($subAreasCult[$c])){
                    $resSubAreaCult .= '<div class="sub"><div class="ball Cultura"></div>' . $resultado . '</div>';
                }
            }

            if(!empty($outraCultura)){
                for($ce = 0; $ce < count($resOutraCulturaExplode); $ce++){
                    $resSubAreaCult .= '<div class="sub"><div class="ball Cultura"></div>' . $resOutraCulturaExplode[$ce] . '</div>';
                }
            }
        }

        if(($assDesporto == 1 && !empty($areasDesporto)) || ($assDesporto == 1 && !empty($outraDesporto))){
            $subAreasDesp = explode(",", $areasDesporto);
            $resOutraDesporto = str_replace('; ', ', ', $outraDesporto);
            $subAreasDespExplode = explode(",", $resOutraDesporto);
            for($d = 0; $d < count($subAreasDesp); $d++){
                $resultado2 = VirtualDeskSiteAssociativismoHelper::getSubArea($subAreasDesp[$d]);
                if(!empty($subAreasDesp[$d])){
                    $resSubAreaDesp .= '<div class="sub"><div class="ball Desporto"></div>' . $resultado2 . '</div>';
                }

            }

            if(!empty($outraDesporto)){
                for($de = 0; $de < count($subAreasDespExplode); $de++) {
                    $resSubAreaDesp .= '<div class="sub"><div class="ball Desporto"></div>' . $subAreasDespExplode[$de] . '</div>';
                }
            }
        }

        if(($assSocial == 1 && !empty($areasSocial)) || ($assSocial == 1 && !empty($outraSocial))){
            $subAreasSoc = explode(",", $areasSocial);
            $resOutraSoc = str_replace('; ', ', ', $outraSocial);
            $resOutraSocExplode = explode(",", $resOutraSoc);
            for($s = 0; $s < count($subAreasSoc); $s++){
                $resultado3 = VirtualDeskSiteAssociativismoHelper::getSubArea($subAreasSoc[$s]);
                if(!empty($subAreasSoc[$s])){
                    $resSubAreaSoc .= '<div class="sub"><div class="ball Social"></div>' . $resultado3 . '</div>';
                }
            }

            if(!empty($outraSocial)){
                for($se = 0; $se < count($resOutraSocExplode); $se++){
                    $resSubAreaSoc .= '<div class="sub"><div class="ball Social"></div>' . $resOutraSocExplode[$se] . '</div>';
                }
            }
        }

        if(($assEducacao == 1 && !empty($areasEducacao)) || ($assEducacao == 1 && !empty($outraEducacao))){
            $subAreasEdu = explode(",", $areasEducacao);
            $resOutraEdu = str_replace('; ', ', ', $outraEducacao);
            $resOutraEduExplode = explode(",", $resOutraEdu);
            for($e = 0; $e < count($subAreasEdu); $e++){
                $resultado4 = VirtualDeskSiteAssociativismoHelper::getSubArea($subAreasEdu[$e]);
                if(!empty($subAreasEdu[$e])){
                    $resSubAreaEdu .= '<div class="sub"><div class="ball Educacao"></div>' . $resultado4 . '</div>';
                }
            }

            if(!empty($outraEducacao)){
                for($ee = 0; $ee < count($resOutraEduExplode); $ee++){
                    $resSubAreaEdu .= '<div class="sub"><div class="ball Educacao"></div>' . $resOutraEduExplode[$ee] . '</div>';
                }
            }
        }


        $textoApresentacao = explode(". ", $apresentacao);

        ?>
            <div class="detalheAssociacao">
                <div class="logo">
                    <?php
                        if((int)$imgAssoc == 0){
                            ?>
                            <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/associacoes/Associativismo.jpg" alt="<?php echo $name; ?>" title="<?php echo $nome; ?>"/>
                            <?php
                        } else {
                            $objEventFile = new VirtualDeskSiteAssociativismoFilesHelper();
                            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                            $FileList21Html = '';
                            foreach ($arFileList as $rowFile) {
                                if($temImagem == 0){
                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                    ?>
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $code; ?>" title="<?php echo $nome; ?>">
                                    <?php
                                    $temImagem = 1;
                                }
                            }
                        }
                    ?>
                </div>

                <div class="nome">
                    <h2><?php echo $nome; ?></h2>
                </div>

                <?php
                    if(!empty($apresentacao)){
                        ?>
                            <h3 class="titleApresentacao"> <?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_APRESENTACAO');?> </h3>
                            <div class="apresentacao">
                                <?php

                                    if ( !empty($textoApresentacao) ) {
                                        for ($i = 0; $i < count($textoApresentacao); ++$i) {
                                            $newstring = substr(strtolower($textoApresentacao[$i]), -3);
                                            $newstring2 = substr(strtolower($textoApresentacao[$i]), -2);
                                            if($newstring != 'dra' && $newstring != 'sra' && $newstring != 'drª' && $newstring != 'srª' && $newstring2 != 'sr' && $newstring2 != 'dr' && $newstring2 != 'av' && $newstring2 != 'nº' && $newstring2 != 'bl' && $newstring2 != 'dt' && $newstring != 'esq' && $newstring != 'soc'){
                                                echo '<p>'. trim( $textoApresentacao[$i] ) .'.</p>';
                                            } else {
                                                echo '<p>'. trim( $textoApresentacao[$i] ) .'. ' . trim( $textoApresentacao[$i + 1] ) . '.</p>';
                                                $i = $i + 1;
                                            }
                                        }
                                    }

                                ?>
                            </div>
                        <?php
                    }
                ?>

                <div class="outrasInfo">

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_MORADA'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($morada)){
                                echo $morada;
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CODIGOPOSTAL'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($codpostal)){
                                echo $codpostal;
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FREGUESIAS'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($freguesia)){
                                echo $freguesia;
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_WEBSITE'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($website)){
                                ?> <a href="<?php echo $website; ?>" target="_blank"><?php echo substr($website, 0, 30) . '...'; ?></a> <?php
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FACEBOOK'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($facebook)){
                                ?> <a href="<?php echo $facebook; ?>" target="_blank"><?php echo substr($facebook, 0, 30) . '...'; ?></a> <?php
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_INSTAGRAM'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($instagram)){
                                ?> <a href="<?php echo $instagram; ?>" target="_blank"><?php echo substr($instagram, 0, 30) . '...'; ?></a> <?php
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_TELEF'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($telefone)){
                                echo $telefone;
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <div class="w33">
                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_MAIL'); ?></div>
                        <div class="content">
                            <?php
                            if(!empty($email)){
                                echo $email;
                            } else {
                                echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO');
                            }
                            ?>
                        </div>
                    </div>

                    <?php
                        if(!empty($coordenadas)){
                            ?>
                                <div class="mapa">
                                    <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                    <input type="hidden" required class="form-control"
                                           name="coordenadas" id="coordenadas"
                                           value="<?php echo htmlentities($coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
                                </div>
                            <?php
                        }
                    ?>

                </div>

                <div class="areasAtuacao">
                    <h3> <?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_AREASATUA');?> </h3>

                    <?php
                        if($assCultura == 1){
                            ?>
                                <div class="block">
                                    <div class="w25">
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Cultura.png" alt="Cultura" title="Associativismo - Cultura"/>
                                    </div>
                                    <div class="w75">
                                        <?php
                                            if(!empty($resSubAreaCult)){
                                                echo $resSubAreaCult;
                                            } else {
                                                echo '<div class="sub"><div class="ball Cultura"></div>' . JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO') . '</div>';
                                            }
                                        ?>
                                    </div>
                                </div>
                            <?php
                        }
                        if($assDesporto == 1){
                            ?>
                                <div class="block">
                                    <div class="w25">
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Desporto.png" alt="Desporto" title="Associativismo - Desporto"/>
                                    </div>
                                    <div class="w75">
                                        <?php
                                             if(!empty($resSubAreaDesp)){
                                                echo $resSubAreaDesp;
                                             } else {
                                                 echo '<div class="sub"><div class="ball Desporto"></div>' . JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO') . '</div>';
                                             }
                                        ?>
                                    </div>
                                </div>
                            <?php
                        }
                        if($assSocial == 1){
                            ?>
                                <div class="block">
                                    <div class="w25">
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Social.png" alt="Social" title="Associativismo - Social"/>
                                    </div>
                                    <div class="w75">
                                        <?php
                                            if(!empty($resSubAreaSoc)){
                                                echo $resSubAreaSoc;
                                            } else {
                                                echo '<div class="sub"><div class="ball Social"></div>' . JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO') . '</div>';
                                            }
                                        ?>
                                    </div>
                                </div>
                            <?php
                        }
                        if($assEducacao == 1){
                            ?>
                                <div class="block">
                                    <div class="w25">
                                        <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Educacao.png" alt="Educacao" title="Associativismo - Educação"/>
                                    </div>
                                    <div class="w75">
                                        <?php
                                            if(!empty($resSubAreaEdu)){
                                                echo $resSubAreaEdu;
                                            } else {
                                                echo '<div class="sub"><div class="ball Educacao"></div>' . JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NAOINDICADO') . '</div>';
                                            }
                                        ?>
                                    </div>
                                </div>
                            <?php
                        }
                    ?>
                </div>

            </div>
        <?php
    endforeach;


?>


<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
</script>
