<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaEventosAssociacoes');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
    ?>

    <div class="resultadosEventos">
        <?php
        if (isset($_POST['submitForm'])) {
            $nomePost = $_POST['nomePost'];
            $promotor = $_POST['promotor'];
            $ano = $_POST['ano'];

            if(empty($nomePost) && empty($promotor) && empty($ano)){
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEventosALL.php');
            } else {
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEventosFiltered.php');
            }
        } else {
            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaEventosALL.php');
        }
        ?>
    </div>


    <div class="filtroEventos">
        <div class="content">
            <form id="ProcuraEventos" action="" method="post" class="login-form" enctype="multipart/form-data" >

                <!--   Pesquisa Livre  -->
                <div class="form-group" id="nomeSearch">
                    <div class="col-md-9">
                        <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_PESQUISALIVRE'); ?>"
                               name="nomePost" id="nomePost" value="<?php echo $nomePost; ?>"/>
                    </div>
                </div>


                <!--   Promotor -->
                <div class="form-group" id="prom">
                    <?php $Promotores = VirtualDeskSiteAssociativismoHelper::getPromotores()?>
                    <div class="col-md-9">
                        <div class="input-group ">
                            <select name="promotor" value="<?php echo $promotor; ?>" id="promotor" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($promotor)){
                                    ?>
                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_PROMOTOR'); ?></option>
                                    <?php foreach($Promotores as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['nome']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $promotor; ?>"><?php echo VirtualDeskSiteAssociativismoHelper::getPromSelect($promotor) ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludePromotor = VirtualDeskSiteAssociativismoHelper::excludePromotor($promotor)?>
                                    <?php foreach($ExcludePromotor as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['nome']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>



                <!--   Anos -->
                <div class="form-group" id="year">
                    <?php $Anos = VirtualDeskSiteAssociativismoHelper::getAnos()?>
                    <div class="col-md-9">
                        <div class="input-group ">
                            <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($ano)){
                                    ?>
                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ANO'); ?></option>
                                    <?php foreach($Anos as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['Ano']; ?>"
                                        ><?php echo $rowWSL['Ano']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $ano; ?>"><?php echo $ano; ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeAno = VirtualDeskSiteAssociativismoHelper::excludeAno($ano)?>
                                    <?php foreach($ExcludeAno as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['Ano']; ?>"
                                        ><?php echo $rowWSL['Ano']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>





                <div class="form-actions" method="post" style="display:none;">
                    <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
                </div>

            </form>
        </div>

    </div>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
</script>
