function dropError(){
    jQuery('.backgroundErro').css('display','block');
}

function checkIfFileLimtiHasReached()
{

    var NumFiles  = Number(jQuery('#vd-filelist').find('.vd-filelist-item').size());
    var NumRepeat = Number(jQuery('#vd-filelist-repeater').find('.vd-filelist-repeater-item').size());
    var SumNumFilesNumRepeat = Number(NumFiles+NumRepeat);

    console.log(param_contactus_filelimitnum +' - '+ NumFiles +' , '+ NumRepeat + ' - ' + SumNumFilesNumRepeat);

    if( SumNumFilesNumRepeat <= param_contactus_filelimitnum ){
        return(false)
    }
    return(true);
}


function shortcut(valor, elemento){
    if(document.getElementById(elemento).value == valor){
        document.getElementById(elemento).value = 0;
    } else {
        document.getElementById(elemento).value = valor;
    }
}

jQuery(document).ready(function(){

    document.getElementById('areaCultura').onclick = function() {
        if(document.getElementById("areaCulturaHide").value == 1){
            document.getElementById("areaCulturaHide").value = 0;
            jQuery("#blockCultura").css('display','none');
        } else {
            document.getElementById("areaCulturaHide").value = 1;
            jQuery("#blockCultura").css('display','block');
        }
    };

    /*SUB CULTURA*/
    document.getElementById('folclore').onclick = function() {
        shortcut(1, 'folcloreHide');
    };

    document.getElementById('artes').onclick = function() {
        shortcut(2, 'artesHide');
    };

    document.getElementById('danca').onclick = function() {
        shortcut(3, 'dancaHide');
    };

    document.getElementById('feiras').onclick = function() {
        shortcut(4, 'feirasHide');
    };

    document.getElementById('literatura').onclick = function() {
        shortcut(5, 'literaturaHide');
    };

    document.getElementById('musica').onclick = function() {
        shortcut(6, 'musicaHide');
    };

    document.getElementById('teatro').onclick = function() {
        shortcut(7, 'teatroHide');
    };

    document.getElementById('outra').onclick = function() {
        if(document.getElementById("outraHide").value == 100){
            document.getElementById("outraHide").value = 0;
            document.getElementById("outraCulturaGroup").hide();
        } else {
            document.getElementById("outraHide").value = 100;
            document.getElementById("outraCulturaGroup").show();
        }
    };




    document.getElementById('areaDesporto').onclick = function() {
        if(document.getElementById("areaDesportoHide").value == 2){
            document.getElementById("areaDesportoHide").value = 0;
            jQuery("#blockDesporto").css('display','none');
        } else {
            document.getElementById("areaDesportoHide").value = 2;
            jQuery("#blockDesporto").css('display','block');
        }
    };

    /*SUB DESPORTO*/
    document.getElementById('andebol').onclick = function() {
        shortcut(9, 'andebolHide');
    };

    document.getElementById('artMarciais').onclick = function() {
        shortcut(10, 'artMarciaisHide');
    };

    document.getElementById('atletismo').onclick = function() {
        shortcut(11, 'atletismoHide');
    };

    document.getElementById('automobilismo').onclick = function() {
        shortcut(12, 'automobilismoHide');
    };

    document.getElementById('basquet').onclick = function() {
        shortcut(13, 'basquetHide');
    };

    document.getElementById('ciclismo').onclick = function() {
        shortcut(14, 'ciclismoHide');
    };

    document.getElementById('difRed').onclick = function() {
        shortcut(15, 'difRedHide');
    };

    document.getElementById('futebol').onclick = function() {
        shortcut(16, 'futebolHide');
    };

    document.getElementById('hoquei').onclick = function() {
        shortcut(17, 'hoqueiHide');
    };

    document.getElementById('ginastica').onclick = function() {
        shortcut(18, 'ginasticaHide');
    };

    document.getElementById('natacao').onclick = function() {
        shortcut(19, 'natacaoHide');
    };

    document.getElementById('patinagem').onclick = function() {
        shortcut(20, 'patinagemHide');
    };

    document.getElementById('tenMesa').onclick = function() {
        shortcut(21, 'tenMesaHide');
    };

    document.getElementById('natur').onclick = function() {
        shortcut(22, 'naturHide');
    };

    document.getElementById('outraDesp').onclick = function() {
        if(document.getElementById("outraDespHide").value == 200){
            document.getElementById("outraDespHide").value = 0;
            document.getElementById("outraDesportoGroup").hide();
        } else {
            document.getElementById("outraDespHide").value = 200;
            document.getElementById("outraDesportoGroup").show();
        }
    };





    document.getElementById('areaSocial').onclick = function() {
        if(document.getElementById("areaSocialHide").value == 3){
            document.getElementById("areaSocialHide").value = 0;
            jQuery("#blockSocial").css('display','none');
        } else {
            document.getElementById("areaSocialHide").value = 3;
            jQuery("#blockSocial").css('display','block');
        }
    };


    /*SUB Social*/
    document.getElementById('tercIdade').onclick = function() {
        shortcut(24, 'tercIdadeHide');
    };

    document.getElementById('portDef').onclick = function() {
        shortcut(25, 'portDefHide');
    };

    document.getElementById('apJuv').onclick = function() {
        shortcut(26, 'apJuvHide');
    };

    document.getElementById('refDom').onclick = function() {
        shortcut(27, 'refDomHide');
    };

    document.getElementById('viagens').onclick = function() {
        shortcut(28, 'viagensHide');
    };

    document.getElementById('outraSoc').onclick = function() {
        if(document.getElementById("outraSocHide").value == 300){
            document.getElementById("outraSocHide").value = 0;
            document.getElementById("outraSocialGroup").hide();
        } else {
            document.getElementById("outraSocHide").value = 300;
            document.getElementById("outraSocialGroup").show();
        }
    };





    document.getElementById('areaEducacao').onclick = function() {
        if(document.getElementById("areaEducacaoHide").value == 4){
            document.getElementById("areaEducacaoHide").value = 0;
            jQuery("#blockEducacao").css('display','none');
        } else {
            document.getElementById("areaEducacaoHide").value = 4;
            jQuery("#blockEducacao").css('display','block');
        }
    };


    /*SUB Social*/
    document.getElementById('assPais').onclick = function() {
        shortcut(30, 'assPaisHide');
    };

    document.getElementById('assEst').onclick = function() {
        shortcut(31, 'assEstHide');
    };

    document.getElementById('biblioteca').onclick = function() {
        shortcut(32, 'bibliotecaHide');
    };

    document.getElementById('atl').onclick = function() {
        shortcut(33, 'atlHide');
    };

    document.getElementById('intercambio').onclick = function() {
        shortcut(34, 'intercambioHide');
    };

    document.getElementById('outraEdu').onclick = function() {
        if(document.getElementById("outraEduHide").value == 400){
            document.getElementById("outraEduHide").value = 0;
            document.getElementById("outraEducacaoGroup").hide();
        } else {
            document.getElementById("outraEduHide").value = 400;
            document.getElementById("outraEducacaoGroup").show();
        }
    };




    jQuery( "#fechaErro" ).click(function() {
        jQuery('.backgroundErro').css('display','none');
    });

    // FileUpLoad
    jQuery("#fileupload").fileinput({
        theme: 'fa',
        language: fileLangSufixV2,
        showPreview: true,
        allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        elErrorContainer: '#errorBlock',
        uploadUrl : setVDCurrentRelativePath , // definido no php
        maxFileCount: 3,
        uploadAsync: true,
        showUpload: false, // hide upload button
        showBrowse: true,
        showRemove: false,
        showCaption: false, //
        browseOnZoneClick: true,
        retryErrorUploads : false,


        // Enviar referência do projecto para associar os ficheiros
        uploadExtraData:  function() {  // callback example
            var out = {}, key, i = 0;

            out['isVDAjaxReqFileUpload'] = isVDAjaxReqFileUpload;
            out['VDAjaxReqProcRefId'] = jQuery("#VDAjaxReqProcRefId").val();
            return out;},

        fileActionSettings: {
            showRemove: true,
            showUpload: false,
            showZoom: true,
            showDrag: false,
            showDownload: false,
            uploadRetryIcon: false,
            indicatorNew: '',
            indicatorNewTitle: ''
        }
    });


    jQuery('#fileupload').on('fileuploaderror', function(event, data, msg) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File upload error');
        // get message
        alert(msg);
    });

    jQuery('#fileupload').on('filebatchuploadsuccess', function(event, data) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File batch upload success');
    });

    jQuery('#fileupload').on('filebatchuploaderror', function(event, data, msg) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File batch upload error');
        // get message
        alert(msg);
    });


    jQuery('#fileupload').on('filebatchuploadcomplete', function(event, files, extra) {
        console.log('File batch upload complete');
    });

    var isVDAjaxReqFileUpload = '1';
    var VDAjaxReqProcRefId = 0;


    jQuery("#btPostFile").on("click", function(event) {

        jQuery.ajax({
            type: 'POST',
            url: setVDCurrentRelativePath,
            data: {  isVDAjaxReqTESTEPost: 1 },
            success: function(response) {
                console.log(' response ok...');
                var data = response;

                var nFirstPos = data.search("__i__");
                var nLastPos  = data.search("__e__");

                var RefOccrrentToSend = data.substring(nFirstPos+5,nLastPos);

                // debugger;
                // Se correu bem a gravação da ocorrência... vai tentar enviar os ficheiros

                jQuery('html, body').animate({
                    scrollTop: jQuery("#uploadField").offset().top
                }, 2000);

                // Só envia ficheiros se tiver algum seleccionado
                var files = jQuery('#fileupload').fileinput('getFileStack'); // returns file list selected
                var files2 = jQuery('#fileupload').val(); // returns file list selected

                if(files.length>0 && files2!='') {
                    var d = new Date();
                    VDAjaxReqProcRefId = RefOccrrentToSend; // d.getTime();  // Para teste

                    jQuery('#fileupload').fileinput('upload');

                    //ToDo: Tratar Erros de gravação dos ficheiros com on('fileuploaderror'  ... .on('filebatchuploaderror'

                }

            },
            error: function(xhr, status, error) {

                //ToDo: Tratar Erros de gravação da ocorrência

                //debugger;
                //var err = eval("(" + xhr.responseText + ")");
                var err = 'Erro ao Gravar:' + error;
                alert(err);
            }

        });
    });


});