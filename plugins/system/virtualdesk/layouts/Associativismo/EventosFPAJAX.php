<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('eventosFPAssociativismo');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    //FileInput***
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/plugins/sortable.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/fileinput.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/locales/pt.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/fa/theme.js' . $addscript_end;
    $headScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    //FileInput***
    $headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/css/fileinput.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $procuraAssociacoes = VirtualDeskSiteAssociativismoHelper::getAssociacoes();

    foreach($procuraAssociacoes as $rowWSL) :
        $nif .= $rowWSL['nif'] . '_';
    endforeach;

    $pieces = explode("_", $nif);
    $total = count($pieces) + 1;
    $limite = 0;

    for($i = 0; $i < $total; $i++){
        $value = $pieces[$i];

        $procuraEventos = VirtualDeskSiteAssociativismoHelper::getEventos($value);

        foreach($procuraEventos as $rowWSL) :
            if($limite <6){
                $id = $rowWSL['id_evento'];
                $referencia = $rowWSL['referencia_evento'];
                $categoria = $rowWSL['categoria'];
                $nomeEvento = $rowWSL['nome_evento'];
                $descricaoEvento = $rowWSL['desc_evento'];
                $fregEvento = $rowWSL['freguesia'];
                $dataInicio = $rowWSL['data_inicio'];
                $dataFim = $rowWSL['data_fim'];
                $ArrStartDate = explode("-",$dataInicio);
                $month = $ArrStartDate[1];

                $name = str_replace(' ', '_', $nomeEvento);
                $code = $id . '_' . $name;
                $temImagem = 0;

                $monthEventos = VirtualDeskSiteAssociativismoHelper::getNameMonth($month);
                $imgEventos = VirtualDeskSiteAssociativismoHelper::getImgEventos($referencia);

                ?>
                <div class="w33">
                    <div class="headItem">
                        <div class="date">
                            <div class="month">
                                <?php echo $monthEventos; ?>
                            </div>
                            <div class="day">
                                <?php echo $ArrStartDate[2]; ?>
                            </div>
                        </div>

                        <div class="ano">
                            <?php echo $ArrStartDate[0]; ?>
                        </div>
                        <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" target="_blank" title="Associativismo <?php echo $concelho; ?> - <?php echo $nomeEvento;?>">
                            <div class="image">
                                <?php
                                if((int)$imgEventos == 0){
                                    $idCat = VirtualDeskSiteAssociativismoHelper::getIdCat($categoria);
                                    $semimagem = VirtualDeskSiteAssociativismoHelper::getImagemGeral($idCat);
                                    ?>
                                    <img src="<?php echo $nomeSite . $image . $semimagem;?>" alt="<?php echo $nomeEvento; ?>" title="<?php echo $title; ?>"/>
                                    <?php
                                } else {
                                    $objEventFile = new VirtualDeskSiteCulturaFilesHelper();
                                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                    $FileList21Html = '';
                                    foreach ($arFileList as $rowFile) {

                                        if($temImagem == 0){
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $name; ?>" title="Associativismo <?php echo $concelho; ?> - <?php echo $nomeEvento;?>">
                                            <?php
                                            $temImagem = 1;
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </a>
                    </div>

                    <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" target="_blank" title="Associativismo <?php echo $concelho; ?> - <?php echo $nomeEvento;?>">
                        <div class="titleEvent">
                            <?php echo $nomeEvento; ?>
                        </div>
                    </a>

                    <div class="catEvent">
                        <div class="borda"></div>
                        <div class="text">
                            <?php echo $categoria ?>
                        </div>
                    </div>

                    <div class="fregEvent">
                        <div class="icon">
                            <svg aria-hidden="true" class="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" viewBox="0 0 384 512"><path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"/></svg>
                        </div>
                        <div class="text">
                            <?php echo $fregEvento; ?>
                        </div>
                    </div>


                    <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" target="_blank" title="Associativismo <?php echo $concelho; ?> - <?php echo $nomeEvento;?>">

                        <div class="sabermais">
                            <button id="saberMais">
                                <div class="borda"></div>
                                <input type="submit" name="sabermais" id="sabermais" value="<?php echo JText::_('COM_VIRTUALDESK_CULTURA_SABERMAIS'); ?>">
                            </button>

                        </div>
                    </a>

                </div>
                <?php
            }
            $limite = $limite + 1;
        endforeach;
    }


    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
</script>
