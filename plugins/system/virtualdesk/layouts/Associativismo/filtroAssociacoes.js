var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


jQuery(document).ready(function(){

    // Select 2
    ComponentsSelect2.init();


    jQuery("#areaAtuacao").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var areaAtuacao = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        var dataString = "areaAtuacao="+areaAtuacao; /* STORE THAT TO A DATA STRING */

        // show loader
        App.blockUI({ target:'#blocoMenuMain',  animate: true});

        jQuery.ajax({
            url: websitepath,
            data:'m=associativismo_getcat&areaAtuacao=' + areaAtuacao ,

            success: function(output) {

                var select = jQuery('#vdmenumain');
                select.find('option').remove();

                if (output == null || output=='') {
                    select.prop('disabled', true);
                }
                else {
                    select.prop('disabled', false);
                    select.append(jQuery('<option>'));
                    jQuery.each(JSON.parse(output), function (i, obj) {
                        //console.log('i=' + i);
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                    });
                }

                // hide loader
                App.unblockUI('#blocoMenuMain');

            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status +  ' ' + thrownError);
                // hide loader
                select.find('option').remove();
                select.prop('disabled', true);

                App.unblockUI('#blocoMenuMain');
            }
        });

    });

});