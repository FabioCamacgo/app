
<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('registoAssociativismo');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/edit_maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/associativismo.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    //FileInput***
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/plugins/sortable.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/fileinput.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/js/locales/pt.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/fa/theme.js' . $addscript_end;
    $headScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    //FileInput***
    $headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/css/fileinput.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/assets/fileinput/themes/explorer-fa/theme.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;




if (isset($_POST['submitForm'])) {

    $nome = $_POST['nome'];
    $apresentacao = $_POST['apresentacao'];
    $fiscalid = $_POST['fiscalid'];
    $niss = $_POST['niss'];
    $morada = $_POST['morada'];
    $codPostal4 = $_POST['codPostal4'];
    $codPostal3 = $_POST['codPostal3'];
    $codPostalText = $_POST['codPostalText'];
    $codPostal = $codPostal4 . '-' . $codPostal3 . ' ' . $codPostalText;
    $freguesia = $_POST['freguesia'];
    $email = $_POST['email'];
    $confEmail = $_POST['confEmail'];
    $telefone = $_POST['telefone'];
    $website = $_POST['website'];
    $facebook = $_POST['facebook'];
    $instagram = $_POST['instagram'];
    $areaCulturaHide = $_POST['areaCulturaHide'];
    $areaDesportoHide = $_POST['areaDesportoHide'];
    $areaSocialHide = $_POST['areaSocialHide'];
    $areaEducacaoHide = $_POST['areaEducacaoHide'];
    $coordenadas = $_POST['coordenadas'];
    $folcloreHide = $_POST['folcloreHide'];
    $artesHide = $_POST['artesHide'];
    $dancaHide = $_POST['dancaHide'];
    $feirasHide = $_POST['feirasHide'];
    $literaturaHide = $_POST['literaturaHide'];
    $musicaHide = $_POST['musicaHide'];
    $teatroHide = $_POST['teatroHide'];
    $outraHide = $_POST['outraHide'];
    $outraCultura = $_POST['outraCultura'];
    $descCultura = $_POST['descCultura'];
    $mailCultura = $_POST['mailCultura'];
    $confEmailCultura = $_POST['confEmailCultura'];
    $andebolHide = $_POST['andebolHide'];
    $artMarciaisHide = $_POST['artMarciaisHide'];
    $atletismoHide = $_POST['atletismoHide'];
    $automobilismoHide = $_POST['automobilismoHide'];
    $basquetHide = $_POST['basquetHide'];
    $ciclismoHide = $_POST['ciclismoHide'];
    $difRedHide = $_POST['difRedHide'];
    $futebolHide = $_POST['futebolHide'];
    $hoqueiHide = $_POST['hoqueiHide'];
    $ginasticaHide = $_POST['ginasticaHide'];
    $natacaoHide = $_POST['natacaoHide'];
    $patinagemHide = $_POST['patinagemHide'];
    $tenMesaHide = $_POST['tenMesaHide'];
    $naturHide = $_POST['naturHide'];
    $outraDespHide = $_POST['outraDespHide'];
    $outraDesporto = $_POST['outraDesporto'];
    $descDesporto = $_POST['descDesporto'];
    $mailDesporto = $_POST['mailDesporto'];
    $confEmailDesporto = $_POST['confEmailDesporto'];
    $tercIdadeHide = $_POST['tercIdadeHide'];
    $portDefHide = $_POST['portDefHide'];
    $apJuvHide = $_POST['apJuvHide'];
    $refDomHide = $_POST['refDomHide'];
    $viagensHide = $_POST['viagensHide'];
    $outraSocHide = $_POST['outraSocHide'];
    $outraSocial = $_POST['outraSocial'];
    $descSocial = $_POST['descSocial'];
    $mailSocial = $_POST['mailSocial'];
    $confEmailSocial = $_POST['confEmailSocial'];
    $assPaisHide = $_POST['assPaisHide'];
    $assEstHide = $_POST['assEstHide'];
    $bibliotecaHide = $_POST['bibliotecaHide'];
    $atlHide = $_POST['atlHide'];
    $intercambioHide = $_POST['intercambioHide'];
    $outraEduHide = $_POST['outraEduHide'];
    $outraEducacao = $_POST['outraEducacao'];
    $descEducacao = $_POST['descEducacao'];
    $mailEducacao = $_POST['mailEducacao'];
    $confEmailEducacao = $_POST['confEmailEducacao'];

    $choseAreas = VirtualDeskSiteAssociativismoHelper::ConcatAreas($areaCulturaHide, $areaDesportoHide, $areaSocialHide, $areaEducacaoHide);
    $choseCultura = VirtualDeskSiteAssociativismoHelper::ConcatCultura($folcloreHide, $artesHide, $dancaHide, $feirasHide, $literaturaHide, $musicaHide, $teatroHide);
    $choseDesporto = VirtualDeskSiteAssociativismoHelper::ConcatDesporto($andebolHide, $artMarciaisHide, $atletismoHide, $automobilismoHide, $basquetHide, $ciclismoHide, $difRedHide, $futebolHide, $hoqueiHide, $ginasticaHide, $natacaoHide, $patinagemHide, $tenMesaHide, $naturHide);
    $choseSocial = VirtualDeskSiteAssociativismoHelper::ConcatSocial($tercIdadeHide, $portDefHide, $apJuvHide, $refDomHide, $viagensHide);
    $choseEducacao = VirtualDeskSiteAssociativismoHelper::ConcatEducacao($assPaisHide, $assEstHide, $bibliotecaHide, $atlHide, $intercambioHide);

    if(!empty($fiscalid)){
        $iscultura = VirtualDeskSiteAssociativismoHelper::getAssocCultura($fiscalid);

        if(empty($iscultura)){
            VirtualDeskSiteAssociativismoHelper::SaveUserCultura($fiscalid, $email, $nome, $morada, $freguesia, $codPostal, $telefone, $website, $facebook);
        }

        $referencia = 'ASSOC-PSOL-' . $fiscalid;

        VirtualDeskSiteAssociativismoHelper::SaveUserAssociativismo($referencia, $nome, $apresentacao, $fiscalid, $niss, $morada, $codPostal, $freguesia, $email, $telefone, $website, $facebook, $instagram, $coordenadas, $choseAreas, $choseCultura, $outraCultura, $descCultura, $mailCultura, $choseDesporto, $outraDesporto, $descDesporto, $mailDesporto, $choseSocial, $outraSocial, $descSocial, $mailSocial, $choseEducacao, $outraEducacao, $descEducacao, $mailEducacao);

        echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');

        echo 'ok';

        exit();
    }

}

?>

    <form id="novoRegisto" action="" method="post" class="novoRegisto-form" enctype="multipart/form-data" >

        <h3><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DADOSASSOCIACAO'); ?></h3>


        <!--   NOME   -->
        <div class="form-group" id="nomeGroup">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NOME'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NOME'); ?>"
                       name="nome" id="nome" maxlength="250" value="<?php echo $nome; ?>"/>
            </div>
        </div>



        <!--  APRESENTAÇÃO  -->
        <div class="form-group" id="apres">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_APRESENTACAO'); ?></label>
            <div class="col-md-9">
                    <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_APRESENTACAO'); ?>"
                              name="apresentacao" id="apresentacao" maxlength="2000"><?php echo $apresentacao; ?></textarea>
            </div>
        </div>


        <!--   NIF   -->
        <div class="form-group" id="nif">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FISCALID'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FISCALID'); ?>"
                       name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
            </div>
        </div>



        <!--   NISS   -->
        <div class="form-group" id="nissGroup">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NISS'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NISS'); ?>"
                       name="niss" id="niss" maxlength="11" value="<?php echo $niss; ?>"/>
            </div>
        </div>



        <!--   MORADA  -->
        <div class="form-group" id="address">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_MORADA'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_MORADA'); ?>"
                       name="morada" id="morada" maxlength="250" value="<?php echo $morada; ?>"/>
            </div>
        </div>


        <!--   CÓDIGO POSTAL  -->
        <div class="form-group" id="codPostal">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CODIGOPOSTAL'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'XXXX'; ?>"
                       name="codPostal4" id="codPostal4" maxlength="4" value="<?php echo $codPostal4; ?>"/>
                <?php echo '-'; ?>
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'YYY'; ?>"
                       name="codPostal3" id="codPostal3" maxlength="3" value="<?php echo $codPostal3; ?>"/>
                <?php echo ' '; ?>
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo 'Localidade'; ?>"
                       name="codPostalText" id="codPostalText" maxlength="75" value="<?php echo $codPostalText; ?>"/>
            </div>
        </div>



        <!--   FREGUESIA  -->
        <div class="form-group" id="freg">
            <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FREGUESIAS') ?><span class="required">*</span></label>
            <?php $Freguesias = VirtualDeskSiteAssociativismoHelper::getFreguesia('4')?>
            <div class="col-md-9">
                <select name="freguesia" value="<?php echo $freguesia; ?>" id="freguesia" required class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($freguesia)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FREGUESIAS'); ?></option>
                        <?php foreach($Freguesias as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteAssociativismoHelper::getFregSelect($freguesia) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFreguesia = VirtualDeskSiteAssociativismoHelper::excludeFreguesia('4', $freguesia)?>
                        <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>


        <!--   MAPA   -->
        <div class="form-group" id="mapa">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_MAPA'); ?></label>
            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

            <input type="hidden" required class="form-control"
                   name="coordenadas" id="coordenadas"
                   value="<?php echo htmlentities($this->data->$coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>

        </div>


        <!--   EMAIL  -->
        <div class="form-group" id="mail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL'); ?>"
                       name="email" id="email" maxlength="250" value="<?php echo htmlentities($email, ENT_QUOTES, 'UTF-8'); ?>" />
            </div>
        </div>


        <!--   CONFIRMAÇÃO DE EMAIL  -->
        <div class="form-group" id="confmail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?> <span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?>"
                       name="confEmail" id="confEmail" maxlength="250" value="<?php echo htmlentities($confEmail, ENT_QUOTES, 'UTF-8'); ?>" />
            </div>
        </div>


        <!--  TELEFONE  -->
        <div class="form-group" id="phone">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_TELEFONE'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_TELEFONE'); ?>"
                       name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone ?>"/>
            </div>
        </div>


        <!--  WEBSITE  -->
        <div class="form-group" id="assocWebsite">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_WEBSITE'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_WEBSITE'); ?>"
                       name="website" id="website" maxlength="250" value="<?php echo $website ?>"/>
            </div>
        </div>


        <!--  FACEBOOK  -->
        <div class="form-group" id="asscFacebook">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FACEBOOK'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FACEBOOK'); ?>"
                       name="facebook" id="facebook" maxlength="250" value="<?php echo $facebook; ?>"/>
            </div>
        </div>


        <!--  INSTAGRAM  -->
        <div class="form-group" id="assocInstagram">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_INSTAGRAM'); ?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_INSTAGRAM'); ?>"
                       name="instagram" id="instagram" maxlength="250" value="<?php echo $instagram; ?>"/>
            </div>
        </div>



        <h3><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_AREAS_INTERVENCAO_SECCAO'); ?></h3>


        <div class="form-group" id="arInt">
            <label class="col-md-3 control-label" for="field_areasIntervencao"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_AREAS_INTERVENCAO') ?><span class="required">*</span></label>
            <div class="col-md-9">
                <div class="checkItem">
                    <input type="checkbox" name="areaCultura" id="areaCultura" value="<?php echo $areaCulturaHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['areaCulturaHide'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CULTURA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="areaDesporto" id="areaDesporto" value="<?php echo $areaDesportoHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['areaDesportoHide'] == 2) { echo 'checked="checked"'; } ?> />
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESPORTO'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="areaSocial" id="areaSocial" value="<?php echo $areaSocialHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['areaSocialHide'] == 3) { echo 'checked="checked"'; } ?> />
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_SOCIAL'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="areaEducacao" id="areaEducacao" value="<?php echo $areaEducacaoHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['areaEducacaoHide'] == 4) { echo 'checked="checked"'; } ?> />
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EDUCACAO'); ?></label>
                </div>

                <input type="hidden" id="areaCulturaHide" name="areaCulturaHide" value="<?php echo $areaCulturaHide; ?>">
                <input type="hidden" id="areaDesportoHide" name="areaDesportoHide" value="<?php echo $areaDesportoHide; ?>">
                <input type="hidden" id="areaSocialHide" name="areaSocialHide" value="<?php echo $areaSocialHide; ?>">
                <input type="hidden" id="areaEducacaoHide" name="areaEducacaoHide" value="<?php echo $areaEducacaoHide; ?>">

            </div>
        </div>



        <div class="form-group" id="blockCultura">
            <h3><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CULTURA'); ?></h3>
            <?php if($areaCulturaHide == 1){
                ?>
                    <style>
                        #blockCultura{display:block;}
                    </style>
                <?php
            } else {
                ?>
                    <style>
                        #blockCultura{display:none;}
                    </style>
                 <?php
            }
            ?>

            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_SUBAREAS_CULTURA'); ?></label>
            <div class="subareas">
                <div class="checkItem">
                    <input type="checkbox" name="folclore" id="folclore" value="<?php echo $folcloreHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['folcloreHide'] == 1) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FOLCLORE'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="artes" id="artes" value="<?php echo $artesHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['artesHide'] == 2) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ARTES'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="danca" id="danca" value="<?php echo $dancaHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['dancaHide'] == 3) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DANCA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="feiras" id="feiras" value="<?php echo $feirasHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['feirasHide'] == 4) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FEIRAS'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="literatura" id="literatura" value="<?php echo $literaturaHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['literaturaHide'] == 5) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_LITERATURA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="musica" id="musica" value="<?php echo $musicaHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['musicaHide'] == 6) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_MUSICA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="teatro" id="teatro" value="<?php echo $teatroHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['teatroHide'] == 7) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_TEATRO'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="outra" id="outra" value="<?php echo $outraHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['outraHide'] == 100) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_OUTRA'); ?></label>
                </div>
            </div>


            <div class="form-group" id="outraCulturaGroup">
                <?php if($outraHide == 100){ ?>
                    <style>
                        #outraCulturaGroup{display:block;}
                    </style>
                <?php
                } else {
                ?>
                    <style>
                        #outraCulturaGroup{display:none;}
                    </style>
                <?php
                }
                ?>

                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?>"
                           name="outraCultura" id="outraCultura" maxlength="100" value="<?php echo $outraCultura; ?>"/>
                </div>
            </div>

            <!--  DESCRICAO AREA CULTURA  -->
            <div class="form-group" id="descAreaCultura">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_CULTURA'); ?></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_CULTURA'); ?>"
                              name="descCultura" id="descCultura" maxlength="2000"><?php echo $descCultura; ?></textarea>
                </div>
            </div>

            <!--   EMAIL RESPONSAVEL CULTURA -->
            <div class="form-group" id="mailRespCultura">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_CULTURA'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_CULTURA'); ?>"
                           name="mailCultura" id="mailCultura" maxlength="250" value="<?php echo htmlentities($mailCultura, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>


            <!--   CONFIRMAÇÃO DE EMAIL  -->
            <div class="form-group" id="confmailCultura">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?>"
                           name="confEmailCultura" id="confEmailCultura" maxlength="250" value="<?php echo htmlentities($confEmailCultura, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>

            <input type="hidden" id="folcloreHide" name="folcloreHide" value="<?php echo $folcloreHide; ?>">
            <input type="hidden" id="artesHide" name="artesHide" value="<?php echo $artesHide; ?>">
            <input type="hidden" id="dancaHide" name="dancaHide" value="<?php echo $dancaHide; ?>">
            <input type="hidden" id="feirasHide" name="feirasHide" value="<?php echo $feirasHide; ?>">
            <input type="hidden" id="literaturaHide" name="literaturaHide" value="<?php echo $literaturaHide; ?>">
            <input type="hidden" id="musicaHide" name="musicaHide" value="<?php echo $musicaHide; ?>">
            <input type="hidden" id="teatroHide" name="teatroHide" value="<?php echo $teatroHide; ?>">
            <input type="hidden" id="outraHide" name="outraHide" value="<?php echo $outraHide; ?>">

        </div>

        <div class="form-group" id="blockDesporto">
            <h3><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESPORTO'); ?></h3>
            <?php if($areaDesportoHide == 2){
                ?>
                    <style>
                        #blockDesporto{display:block;}
                    </style>
                <?php
            } else {
                ?>
                    <style>
                        #blockDesporto{display:none;}
                    </style>
                <?php
            }
            ?>

            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_SUBAREAS_DESPORTO'); ?></label>
            <div class="subareas">
                <div class="checkItem">
                    <input type="checkbox" name="andebol" id="andebol" value="<?php echo $andebolHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['andebolHide'] == 9) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ANDEBOL'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="artMarciais" id="artMarciais" value="<?php echo $artMarciaisHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['artMarciaisHide'] == 10) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ARTESMARCIAIS'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="atletismo" id="atletismo" value="<?php echo $atletismoHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['atletismoHide'] == 11) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ATLETISMO'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="automobilismo" id="automobilismo" value="<?php echo $automobilismoHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['automobilismoHide'] == 12) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_AUTOMOBILISMO'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="basquet" id="basquet" value="<?php echo $basquetHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['basquetHide'] == 13) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_BASQUET'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="ciclismo" id="ciclismo" value="<?php echo $ciclismoHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['ciclismoHide'] == 14) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CICLISMO'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="difRed" id="difRed" value="<?php echo $difRedHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['difRedHide'] == 15) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DIFMOTORAS'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="futebol" id="futebol" value="<?php echo $futebolHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['futebolHide'] == 16) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FUTEBOL'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="hoquei" id="hoquei" value="<?php echo $hoqueiHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['hoqueiHide'] == 17) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_HOQUEI'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="ginastica" id="ginastica" value="<?php echo $ginasticaHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['ginasticaHide'] == 18) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_GINASTICA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="natacao" id="natacao" value="<?php echo $natacaoHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['natacaoHide'] == 19) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NATACAO'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="patinagem" id="patinagem" value="<?php echo $patinagemHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['patinagemHide'] == 20) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_PATINAGEM'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="tenMesa" id="tenMesa" value="<?php echo $tenMesaHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['tenMesaHide'] == 21) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_TENISMESA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="natur" id="natur" value="<?php echo $naturHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['naturHide'] == 22) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_NATUREZA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="outraDesp" id="outraDesp" value="<?php echo $outraDespHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['outraDespHide'] == 200) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_OUTRA'); ?></label>
                </div>
            </div>


            <div class="form-group" id="outraDesportoGroup">
                <?php if($outraDespHide == 200){ ?>
                    <style>
                        #outraDesportoGroup{display:block;}
                    </style>
                    <?php
                } else {
                    ?>
                    <style>
                        #outraDesportoGroup{display:none;}
                    </style>
                    <?php
                }
                ?>

                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?>"
                           name="outraDesporto" id="outraDesporto" maxlength="100" value="<?php echo $outraDesporto; ?>"/>
                </div>
            </div>

            <!--  DESCRICAO AREA CULTURA  -->
            <div class="form-group" id="descAreaDesporto">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_DESPORTO'); ?></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_DESPORTO'); ?>"
                              name="descDesporto" id="descDesporto" maxlength="2000"><?php echo $descDesporto; ?></textarea>
                </div>
            </div>

            <!--   EMAIL RESPONSAVEL CULTURA -->
            <div class="form-group" id="mailRespDesporto">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_DESPORTO'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_DESPORTO'); ?>"
                           name="mailDesporto" id="mailDesporto" maxlength="250" value="<?php echo htmlentities($mailDesporto, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>


            <!--   CONFIRMAÇÃO DE EMAIL  -->
            <div class="form-group" id="confmailDesporto">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?>"
                           name="confEmailDesporto" id="confEmailDesporto" maxlength="250" value="<?php echo htmlentities($confEmailDesporto, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>


            <input type="hidden" id="andebolHide" name="andebolHide" value="<?php echo $andebolHide; ?>">
            <input type="hidden" id="artMarciaisHide" name="artMarciaisHide" value="<?php echo $artMarciaisHide; ?>">
            <input type="hidden" id="atletismoHide" name="atletismoHide" value="<?php echo $atletismoHide; ?>">
            <input type="hidden" id="automobilismoHide" name="automobilismoHide" value="<?php echo $automobilismoHide; ?>">
            <input type="hidden" id="basquetHide" name="basquetHide" value="<?php echo $basquetHide; ?>">
            <input type="hidden" id="ciclismoHide" name="ciclismoHide" value="<?php echo $ciclismoHide; ?>">
            <input type="hidden" id="difRedHide" name="difRedHide" value="<?php echo $difRedHide; ?>">
            <input type="hidden" id="futebolHide" name="futebolHide" value="<?php echo $futebolHide; ?>">
            <input type="hidden" id="hoqueiHide" name="hoqueiHide" value="<?php echo $hoqueiHide; ?>">
            <input type="hidden" id="ginasticaHide" name="ginasticaHide" value="<?php echo $ginasticaHide; ?>">
            <input type="hidden" id="natacaoHide" name="natacaoHide" value="<?php echo $natacaoHide; ?>">
            <input type="hidden" id="patinagemHide" name="patinagemHide" value="<?php echo $patinagemHide; ?>">
            <input type="hidden" id="tenMesaHide" name="tenMesaHide" value="<?php echo $tenMesaHide; ?>">
            <input type="hidden" id="naturHide" name="naturHide" value="<?php echo $naturHide; ?>">
            <input type="hidden" id="outraDespHide" name="outraDespHide" value="<?php echo $outraDespHide; ?>">

        </div>

        <div class="form-group" id="blockSocial">
            <h3><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_SOCIAL'); ?></h3>
            <?php if($areaSocialHide == 3){
                ?>
                <style>
                    #blockSocial{display:block;}
                </style>
                <?php
            } else {
                ?>
                <style>
                    #blockSocial{display:none;}
                </style>
                <?php
            }
            ?>

            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_SUBAREAS_SOCIAL'); ?></label>
            <div class="subareas">
                <div class="checkItem">
                    <input type="checkbox" name="tercIdade" id="tercIdade" value="<?php echo $tercIdadeHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['tercIdadeHide'] == 24) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_TERCIDADE'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="portDef" id="portDef" value="<?php echo $portDefHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['portDefHide'] == 25) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_PORTDEF'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="apJuv" id="apJuv" value="<?php echo $apJuvHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['apJuvHide'] == 26) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_APJUV'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="refDom" id="refDom" value="<?php echo $refDomHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['refDomHide'] == 27) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_REFDOM'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="viagens" id="viagens" value="<?php echo $viagensHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['viagensHide'] == 28) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_VIAGENS'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="outraSoc" id="outraSoc" value="<?php echo $outraSocHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['outraSocHide'] == 300) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_OUTRASOCIAL'); ?></label>
                </div>
            </div>

            <div class="form-group" id="outraSocialGroup">
                <?php if($outraSocHide == 300){ ?>
                    <style>
                        #outraSocialGroup{display:block;}
                    </style>
                    <?php
                } else {
                    ?>
                    <style>
                        #outraSocialGroup{display:none;}
                    </style>
                    <?php
                }
                ?>

                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?>"
                           name="outraSocial" id="outraSocial" maxlength="100" value="<?php echo $outraSocial; ?>"/>
                </div>
            </div>

            <!--  DESCRICAO AREA SOCIAL  -->
            <div class="form-group" id="descAreaSocial">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_SOCIAL'); ?></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_SOCIAL'); ?>"
                              name="descSocial" id="descSocial" maxlength="2000"><?php echo $descSocial; ?></textarea>
                </div>
            </div>

            <!--   EMAIL RESPONSAVEL CULTURA -->
            <div class="form-group" id="mailRespSocial">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_SOCIAL'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_SOCIAL'); ?>"
                           name="mailSocial" id="mailSocial" maxlength="250" value="<?php echo htmlentities($mailSocial, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>


            <!--   CONFIRMAÇÃO DE EMAIL  -->
            <div class="form-group" id="confmailSocial">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?>"
                           name="confEmailSocial" id="confEmailSocial" maxlength="250" value="<?php echo htmlentities($confEmailSocial, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>

            <input type="hidden" id="tercIdadeHide" name="tercIdadeHide" value="<?php echo $tercIdadeHide; ?>">
            <input type="hidden" id="portDefHide" name="portDefHide" value="<?php echo $portDefHide; ?>">
            <input type="hidden" id="apJuvHide" name="apJuvHide" value="<?php echo $apJuvHide; ?>">
            <input type="hidden" id="refDomHide" name="refDomHide" value="<?php echo $refDomHide; ?>">
            <input type="hidden" id="viagensHide" name="viagensHide" value="<?php echo $viagensHide; ?>">
            <input type="hidden" id="outraSocHide" name="outraSocHide" value="<?php echo $outraSocHide; ?>">

        </div>

        <div class="form-group" id="blockEducacao">
            <h3><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EDUCACAO'); ?></h3>
            <?php if($areaEducacaoHide == 4){
                ?>
                <style>
                    #blockEducacao{display:block;}
                </style>
                <?php
            } else {
                ?>
                <style>
                    #blockEducacao{display:none;}
                </style>
                <?php
            }
            ?>

            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_SUBAREAS_EDUCACAO'); ?></label>
            <div class="subareas">
                <div class="checkItem">
                    <input type="checkbox" name="assPais" id="assPais" value="<?php echo $assPaisHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['assPaisHide'] == 30) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ASSPAIS'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="assEst" id="assEst" value="<?php echo $assEstHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['assEstHide'] == 31) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ASSEST'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="biblioteca" id="biblioteca" value="<?php echo $bibliotecaHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['bibliotecaHide'] == 32) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_BIBLIOTECA'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="atl" id="atl" value="<?php echo $atlHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['atlHide'] == 33) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_ATL'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="intercambio" id="intercambio" value="<?php echo $intercambioHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['intercambioHide'] == 34) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_INTERCAMBIO'); ?></label>
                </div>

                <div class="checkItem">
                    <input type="checkbox" name="outraEdu" id="outraEdu" value="<?php echo $outraEduHide; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['outraEduHide'] == 400) { echo 'checked="checked"'; } ?>/>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_OUTRASOCIAL'); ?></label>
                </div>
            </div>

            <div class="form-group" id="outraEducacaoGroup">
                <?php if($outraEduHide == 400){ ?>
                    <style>
                        #outraEducacaoGroup{display:block;}
                    </style>
                    <?php
                } else {
                    ?>
                    <style>
                        #outraEducacaoGroup{display:none;}
                    </style>
                    <?php
                }
                ?>

                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_QUAL'); ?>"
                           name="outraEducacao" id="outraEducacao" maxlength="100" value="<?php echo $outraEducacao; ?>"/>
                </div>
            </div>

            <!--  DESCRICAO AREA EDUCACAO  -->
            <div class="form-group" id="descAreaEducacao">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_EDUCACAO'); ?></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_DESCRICAO_EDUCACAO'); ?>"
                              name="descEducacao" id="descEducacao" maxlength="2000"><?php echo $descEducacao; ?></textarea>
                </div>
            </div>

            <!--   EMAIL RESPONSAVEL EDUCACAO -->
            <div class="form-group" id="mailRespEducacao">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_EDUCACAO'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_EMAIL_RESP_EDUCACAO'); ?>"
                           name="mailEducacao" id="mailEducacao" maxlength="250" value="<?php echo htmlentities($mailEducacao, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>


            <!--   CONFIRMAÇÃO DE EMAIL  -->
            <div class="form-group" id="confmailEducacao">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?> <span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_CONFEMAIL'); ?>"
                           name="confEmailEducacao" id="confEmailEducacao" maxlength="250" value="<?php echo htmlentities($confEmailEducacao, ENT_QUOTES, 'UTF-8'); ?>" />
                </div>
            </div>


            <input type="hidden" id="assPaisHide" name="assPaisHide" value="<?php echo $assPaisHide; ?>">
            <input type="hidden" id="assEstHide" name="assEstHide" value="<?php echo $assEstHide; ?>">
            <input type="hidden" id="bibliotecaHide" name="bibliotecaHide" value="<?php echo $bibliotecaHide; ?>">
            <input type="hidden" id="atlHide" name="atlHide" value="<?php echo $atlHide; ?>">
            <input type="hidden" id="intercambioHide" name="intercambioHide" value="<?php echo $intercambioHide; ?>">
            <input type="hidden" id="outraEduHide" name="outraEduHide" value="<?php echo $outraEduHide; ?>">

        </div>


        <!--   Upload Imagens   -->
        <div class="form-group" id="uploadField">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_UPLOAD'); ?></label>
            <div class="col-md-9">

                <div class="file-loading">
                    <input type="file" name="fileupload[]" id="fileupload" multiple>
                </div>
                <div id="errorBlock" class="help-block"></div>
                <input type="hidden" id="VDAjaxReqProcRefId">

            </div>
        </div>


        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group" >
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>


    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_REGISTO'); ?>">
    </div>



</form>


<?php

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;
?>

<script>
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
</script>
