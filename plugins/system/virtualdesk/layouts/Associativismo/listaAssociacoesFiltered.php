<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroAssociacoes');
    if ($resPluginEnabled === false) exit();

    $listaAssociacoes = VirtualDeskSiteAssociativismoHelper::getAssociacoesFiltered($nomePost, $freguesia);


    foreach($listaAssociacoes as $rowWSL) :
        $id = $rowWSL['id'];
        $referencia = $rowWSL['referencia'];
        $nome = $rowWSL['nome'];
        $areasAtuacao = $rowWSL['areasAtuacao'];
        $areasCultura = $rowWSL['areasCultura'];
        $areasDesporto = $rowWSL['areasDesporto'];
        $areasSocial = $rowWSL['areasSocial'];
        $areasEducacao = $rowWSL['areasEducacao'];
        $filtroArea = 0;
        $filtroSubArea = 0;
        $pieces2 = explode(",", $areasAtuacao);
        $temImagem = 0;
        $assCultura = 0;
        $assSocial = 0;
        $assDesporto = 0;
        $assEducacao = 0;
        $name = str_replace(' ', '_', $nome);
        $code = $id . '_' . $name;

        for($i = 0; $i < count($pieces2); $i++){
            if($pieces2[$i] == 1){
                $assCultura = 1;
            } else if($pieces2[$i] == 2){
                $assDesporto = 1;
            } elseif($pieces2[$i] == 3){
                $assSocial = 1;
            } elseif($pieces2[$i] == 4){
                $assEducacao = 1;
            }
        }

        $imgAssoc = VirtualDeskSiteAssociativismoHelper::getImgAssoc($referencia);

        if(!empty($areaAtuacao)){
            for($i = 0; $i < count($pieces2); $i++){
                if($pieces2[$i] == $areaAtuacao){
                    $filtroArea = $filtroArea + 1;
                }
            }
        } else {
            $filtroArea = 1;
        }

        if(!empty($subAreaAtuacao)){
            if($areaAtuacao == 1){
                $pieces3 = explode(",", $areasCultura);
                for($c = 0; $c < count($pieces3); $c++){
                    if($pieces3[$c] == $subAreaAtuacao){
                        $filtroSubArea = $filtroSubArea + 1;
                    }
                }
            } else if($areaAtuacao == 2){
                $pieces3 = explode(",", $areasDesporto);
                for($d = 0; $d < count($pieces3); $d++){
                    if($pieces3[$d] == $subAreaAtuacao){
                        $filtroSubArea = $filtroSubArea + 1;
                    }
                }
            } else if($areaAtuacao == 3){
                $pieces3 = explode(",", $areasSocial);
                for($s = 0; $s < count($pieces3); $s++){
                    if($pieces3[$s] == $subAreaAtuacao){
                        $filtroSubArea = $filtroSubArea + 1;
                    }
                }
            } else if($areaAtuacao == 4){
                $pieces3 = explode(",", $areasEducacao);
                for($e = 0; $e < count($pieces3); $e++){
                    if($pieces3[$e] == $subAreaAtuacao){
                        $filtroSubArea = $filtroSubArea + 1;
                    }
                }
            }
        } else {
            $filtroSubArea = 1;
        }

        if($filtroArea != 0 && $filtroSubArea != 0){
            ?>
            <div class="w33">
                <a href="<?php echo $nomeSite;?>menu/detalhe-associacoes?name=<?php echo $code;?>" title="<?php echo $nome;?>">
                    <div class="imagem">
                        <?php
                        if((int)$imgAssoc == 0){
                            ?>
                            <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/associacoes/Associativismo.jpg" alt="<?php echo $code; ?>" title="<?php echo $nome; ?>"/>
                            <?php
                        } else {
                            $objEventFile = new VirtualDeskSiteAssociativismoFilesHelper();
                            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                            $FileList21Html = '';
                            foreach ($arFileList as $rowFile) {
                                if($temImagem == 0){
                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                    ?>
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $code; ?>" title="<?php echo $nome; ?>">
                                    <?php
                                    $temImagem = 1;
                                }
                            }
                        }
                        ?>
                    </div>
                </a>
                <div class="dados">
                    <div class="titulo">
                        <a href="<?php echo $nomeSite;?>menu/detalhe-associacoes?name=<?php echo $code;?>" title="<?php echo $nome;?>">
                            <h3 class="titleAssoc">
                                <?php echo $nome;?>
                            </h3>
                        </a>
                    </div>
                    <div class="info">
                        <div class="areasAtuacao">
                            <?php
                            if($assCultura == 1){
                                ?>
                                <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Cultura.png" alt="Cultura" title="Associativismo - Cultura"/>
                                <?php
                            }
                            if($assDesporto == 1){
                                ?>
                                <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Desporto.png" alt="Desporto" title="Associativismo - Desporto"/>
                                <?php
                            }
                            if($assSocial == 1){
                                ?>
                                <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Social.png" alt="Social" title="Associativismo - Social"/>
                                <?php
                            }
                            if($assEducacao == 1){
                                ?>
                                <img src="<?php echo JUri::base(); ?>plugins/system/virtualdesk/layouts/Associativismo/Imagens/Icons/Educacao.png" alt="Educacao" title="Associativismo - Educação"/>
                                <?php
                            }
                            ?>
                        </div>

                        <a href="<?php echo $nomeSite;?>menu/detalhe-associacoes?name=<?php echo $code;?>" title="Saiba mais">
                            <div class="sabermais">
                                <button class="botaoSaberMais">
                                    <input type="submit" name="sabermais" id="sabermais" value="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_SABERMAIS'); ?>">
                                </button>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
            <?php
        }


    endforeach;
?>