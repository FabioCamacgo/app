<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('filtroAssociacoes');
    if ($resPluginEnabled === false) exit();

    $link = $jinput->get('link','' ,'string');
    $ref = explode("?cat=", $link);

    $ref2 = explode("&area=", $ref[1]);
    $idCatLink = $ref2[0];
    $idSubCatLink = $ref2[1];


    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Associativismo/filtroAssociacoes.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;
?>

    <div class="resultados">
        <?php
            if (isset($_POST['submitForm'])) {
                $nomePost = $_POST['nomePost'];
                $areaAtuacao = $_POST['areaAtuacao'];
                $subAreaAtuacao = $_POST['vdmenumain'];
                $freguesia = $_POST['freguesia'];

                if(empty($nomePost) && empty($areaAtuacao) && empty($subAreaAtuacao) && empty($freguesia)){
                    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaAssociacoesALL.php');
                } else {
                    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaAssociacoesFiltered.php');
                }
            } else if($idCatLink !='') {
                $areaAtuacao = $idCatLink;
                if($idSubCatLink != ''){
                    $subAreaAtuacao = $idSubCatLink;
                }
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaAssociacoesFiltered.php');
            } else {
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaAssociacoesALL.php');
            }
        ?>
    </div>


    <div class="filtro">
        <div class="content">
            <form id="ProcuraAssociacao" action="" method="post" class="login-form" enctype="multipart/form-data" >

                <!--   Pesquisa Livre  -->
                <div class="form-group" id="nomeSearch">
                    <div class="col-md-9">
                        <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_PESQUISALIVRE'); ?>"
                               name="nomePost" id="nomePost" value="<?php echo $nomePost; ?>"/>
                    </div>
                </div>


                <!--   Area de Atuacao   -->
                <div class="form-group" id="area">
                    <div class="col-md-9">
                        <?php $getAreaAtuacao = VirtualDeskSiteAssociativismoHelper::getAreas()?>
                        <div class="input-group ">
                            <select name="areaAtuacao" value="<?php echo $areaAtuacao; ?>" id="areaAtuacao" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($areaAtuacao)){
                                    ?>
                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_AREAATUACAO'); ?></option>
                                    <?php foreach($getAreaAtuacao as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['area']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $areaAtuacao; ?>"><?php echo VirtualDeskSiteAssociativismoHelper::getAreaAtuacaoName($areaAtuacao) ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeAreaAtuacao = VirtualDeskSiteAssociativismoHelper::excludeAreaAtuacao($areaAtuacao)?>
                                    <?php foreach($ExcludeAreaAtuacao as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['area']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <!--   SubAreas Atuacao   -->
                <?php
                // Carrega menusec se o id de menumain estiver definido.
                $ListaDeMenuMain = array();
                if(!empty($areaAtuacao)) {
                    if( (int) $areaAtuacao > 0) $ListaDeMenuMain = VirtualDeskSiteAssociativismoHelper::getSubAreasAtuacaoDepend($areaAtuacao);
                }

                ?>
                <div id="blocoMenuMain" class="form-group">
                    <div class="col-md-9">
                        <select name="vdmenumain" id="vdmenumain"
                            <?php
                            if(empty($areaAtuacao)) {
                                echo 'disabled';
                            }
                            ?>
                                class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                            <?php
                            if(empty($subAreaAtuacao)){
                                ?>
                                <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_ASSOCIATIVISMO_SUBAREAATUACAO' ); ?></option>
                                <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                                    <option value="<?php echo $rowMM['id']; ?>"
                                        <?php
                                        if(!empty($this->data->vdmenumain)) {
                                            if($this->data->vdmenumain == $rowMM['id']) echo 'selected';
                                        }
                                        ?>
                                    ><?php echo $rowMM['name']; ?></option>
                                <?php endforeach; ?>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $subAreaAtuacao; ?>"><?php echo VirtualDeskSiteAssociativismoHelper::getSubAreaName($subAreaAtuacao) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php $ExcludeSubAreaAtuacao = VirtualDeskSiteAssociativismoHelper::excludeSubArea($areaAtuacao, $subAreaAtuacao)?>
                                <?php foreach($ExcludeSubAreaAtuacao as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"
                                    ><?php echo $rowWSL['subarea']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>

                    </div>
                </div>


                <!--   Freguesia   -->
                <div class="form-group" id="freg">
                    <?php $Freguesias = VirtualDeskSiteAssociativismoHelper::getFreguesia('4')?>
                    <div class="col-md-9">
                        <div class="input-group ">
                            <select name="freguesia" value="<?php echo $freguesia; ?>" id="freguesia" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($freguesia)){
                                    ?>
                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_ASSOCIATIVISMO_FREGUESIA'); ?></option>
                                    <?php foreach($Freguesias as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                        ><?php echo $rowWSL['freguesia']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteAssociativismoHelper::getFregSelect($freguesia) ?></option>
                                    <option value=""><?php echo '-'; ?></option>
                                    <?php $ExcludeFreguesia = VirtualDeskSiteAssociativismoHelper::excludeFreguesia('4', $freguesia)?>
                                    <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                        ><?php echo $rowWSL['freguesia']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-actions" method="post" style="display:none;">
                    <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
                </div>

            </form>
        </div>

    </div>

<?php

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
</script>
