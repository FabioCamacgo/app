<?php
defined('JPATH_BASE') or die;
defined('_JEXEC') or die;

$templateName  = 'virtualdesk';
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';
$imgPath       = '../templates/virtualdesk/images/plataforma/';
// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}



$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

//GLOBAL SCRIPTS
$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/myscripts.js' . $addscript_end;

// PAGE LEVEL PLUGIN SCRIPTS
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/NovoAlerta.js' . $addscript_end;

//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/css/bottomMenu.css' . $addcss_end;
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

// Recaptha - utilização do plugin interno do joomla
// check params for recpacha
//$captcha_plugin  = JFactory::getConfig()->get('captcha');
$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts  = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini.'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag.$addscript_end;;
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $doc->language; ?>" dir="<?php echo $doc->direction; ?>">
<!--<![endif]-->
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php echo $headCSS; ?>
    </head>

    <body style="width:100%">


    <!-- Body -->

        <?php
            require_once(JPATH_SITE.'/plugins/system/virtualdesk/layouts/includes/header.php');
        ?>

    <!-- Conteúdo da Página -->
        <div id="FP" class="content" style="width: 1200px; margin: 160px auto 70px auto;">

            <?php

                $categoria = $_POST["idwebsitelist"];
                $subcategoria = $_POST["vdmenumain"];
                $descricao = $_POST["descricao"];
                $freguesia = $_POST["freguesias"];
                $sitio = $_POST["sitio"];
                $morada = $_POST["morada"];
                $pontosReferencia = $_POST["refs"];
                $nome = $_POST["nome"];
                $nif = $_POST["fiscalid"];
                $email1 = $_POST["email1"];
                $email2 = $_POST["email2"];
                $telefone = $_POST["telefone"];
                $refCode = '';

                $refBD = VirtualDeskSiteAlertaHelper::getReferencia($categoria);

                foreach($refBD as $rowMM) :
                    $refCode = $rowMM['Ref'];
                endforeach;

                $year = date("Y");
                $ano = str_split($year);
                $code = VirtualDeskSiteAlertaHelper::random_code();

                $referencia = $refCode . $code . '-' . $ano[2] . $ano[3] ;

                $seExiste = VirtualDeskSiteAlertaHelper::CheckReferencia($referencia);
                if((int)$seExiste > 0){
                    VirtualDeskSiteAlertaHelper::getReferencia($categoria);
                }

                $subcatID = VirtualDeskSiteAlertaHelper::GetSubCatID($subcategoria);
                foreach($subcatID as $rowMM) :
                    $subcatID = $rowMM['Id_subcategoria'];
                endforeach;


                VirtualDeskSiteAlertaHelper::saveNewAlerta($categoria, $referencia, $subcatID, $descricao, $freguesia, $sitio, $morada, $pontosReferencia, $nome, $nif, $email1, $telefone);



                /*Mostra os dados submetidos */

            ?>


            <h1> <img src="<?php echo $imgPath; ?>sucess.png" alt="sucess" title="Sucesso"/> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_INFO_TITLE'); ?></h1>

            <P><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_INFO'); ?></P>

            <div class="headpesquisa">
                <div class="w100">
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRAUSER'); ?>
                </div>
            </div>

            <div class="corpoResultado">

                <div class="w100">

                    <div class="dadosOcorrencia">

                        <h2><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRADADOS'); ?></h2>

                        <div class="w33">
                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_CATEGORIA'); ?></div>
                                <div class="result">
                                    <?php echo $categoria; ?>
                                </div>
                            </div>

                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_SUBCATEGORIA'); ?></div>
                                <div class="result">
                                    <?php echo $subcategoria; ?>
                                </div>
                            </div>
                        </div>


                        <div class="w33">
                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_FREGUESIA'); ?></div>
                                <div class="result">
                                    <?php echo $freguesia; ?>
                                </div>
                            </div>

                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_SITIO'); ?></div>
                                <div class="result">
                                    <?php echo $sitio; ?>
                                </div>
                            </div>
                        </div>

                        <div class="w33">
                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_MORADA'); ?></div>
                                <div class="result">
                                    <?php echo $morada; ?>
                                </div>
                            </div>

                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_PONTOSREFERENCIA'); ?></div>
                                <div class="result">
                                    <?php echo $pontosReferencia; ?>
                                </div>
                            </div>
                        </div>

                        <div class="w100">
                            <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_DESCRICAO'); ?></div>
                            <div class="result">
                                <?php echo $descricao; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w100">
                    <div class="dadosPessoais">

                        <h2><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SECCAO_DADOSPESSOAIS'); ?></h2>

                        <div class="w25">
                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_NOME'); ?></div>
                                <div class="result">
                                    <?php echo $nome; ?>
                                </div>
                            </div>
                        </div>

                        <div class="w25">
                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_NIF'); ?></div>
                                <div class="result">
                                    <?php echo $nif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="w25">
                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_EMAIL'); ?></div>
                                <div class="result">
                                    <?php echo $email1; ?>
                                </div>
                            </div>
                        </div>

                        <div class="w25">
                            <div class="w100">
                                <div class="item"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MOSTRA_TELEFONE'); ?></div>
                                <div class="result">
                                    <?php echo $telefone; ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <?php
                /*Enviar email....   Ver se funciona quando a plataforma está online*/

                /*$to = $email1; // this is your Email address
                $from = "fabio.camacho@faceconsulting.pt"; // this is the sender's Email address
                $first_name = $nome;
                $last_name = $nif;
                $subject = "Form submission";
                $subject2 = "Copy of your form submission";
                $message = $first_name . " " . $last_name . " wrote the following:" . "\n\n" . $descricao;
                $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $descricao;

                $headers = "From:" . $from;
                $headers2 = "From:" . $to;
                mail($to,$subject,$message,$headers);
                mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
                echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
                */

            ?>

        </div>


    <!-- END Conteúdo da Página -->

        <?php
            //require_once(JPATH_SITE.'/plugins/system/virtualdesk/layouts/includes/footer.php');
        ?>


    <!-- END FOOTER -->

        <?php echo $headScripts; ?>
        <?php echo $recaptchaScripts; ?>
        <?php echo $footerScripts; ?>
        <?php echo $localScripts;?>
    </body>
</html>