<?php
    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $templateName  = 'virtualdesk';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;

    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;

    // Recaptha - utilização do plugin interno do joomla
    // check params for recpacha
    //$captcha_plugin  = JFactory::getConfig()->get('captcha');
    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts  = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini.'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag.$addscript_end;;
    }

?>

<html lang="<?php echo $doc->language; ?>" dir="<?php echo $doc->direction; ?>">


    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <?php echo $headCSS; ?>
    </head>


    <body>

        <div class="contentUploadFoto">

            <!-- BEGIN FORM -->
            <div class="content">
                <?php
                    // Verifica mensagens de erro/sucesso do joomla/php
                    $messages       = $app->getMessageQueue();
                    $messagesDanger = '';
                    $messagesSucess = '';
                    if(!is_array($messages)) $messages = array();
                    foreach($messages as $chvMsg => $valMsg)
                    {
                        if($valMsg['type']=='warning' or $valMsg['type']=='error')
                        {
                            $messagesDanger .= '<i class="fa-lg fa fa-warning"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                        }
                        elseif ($valMsg['type']=='message')
                        {
                            $messagesSucess .= '<i class="fa-lg fa fa-check"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                        }
                    }

                    if($resNewUser!==true) $messagesSucess = '';

                ?>

                <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                </div>

                <?php if (!empty($messagesDanger)) : ?>
                    <div id="MainMessageAlertBlock2Joomla" class="alert alert-danger fade in" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php echo $messagesDanger; ?>
                    </div>
                <?php endif; ?>


                <?php
                if (!empty($messagesSucess)) : ?>
                    <div id="MainMessageSucessBlock" class="alert alert-success fade in" >
                        <h4 class="alert-heading"><?php echo $messagesSucess; ?></h4>
                        <div class="text-center"><a class="btn blue" style="margin-top:25px;" href="<?php echo JRoute::_('index.php'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?></a></div>
                    </div>
                <?php endif; ?>

                <script>
                    var MessageAlert = new function() {
                        this.getRequiredMissed = function (nErrors) {
                            if (nErrors == null)
                            { return(''); }
                            if(nErrors==1)
                            { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                            else  if(nErrors>1)
                            { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                                return (msg.replace("%s",nErrors) );
                            }
                        };
                    }
                </script>

                <form <?php if(!empty($messagesSucess) && empty($messagesDanger)) echo 'style="display:none;" ';?>  id="uploadFotoSlider" action="<?php echo JRoute::_('?token&captcha&lang='.$language_tag); ?>" method="post"  class="uploadFoto-form" enctype="multipart/form-data" >

                    <h3><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_TITLE'); ?></h3>

                    <div class="uploadData">
                        <div class="form-group" style="margin-top:0;">
                            <label class="control-label visible-ie8 visible-ie9"><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_NOME_LABEL'); ?></label>
                            <input type="text" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_NOME_LABEL'); ?>" name="nome" id="nome" maxlength="250" value="<?php echo htmlentities($data['nome'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>

                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9"><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_EMAIL_LABEL'); ?></label>
                            <input type="email" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_EMAIL_LABEL'); ?>" name="email" id="email" maxlength="250" value="<?php echo htmlentities($data['email'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>

                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9"><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_NOMEPROPRIETARIO_LABEL'); ?></label>
                            <input type="text" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_NOMEPROPRIETARIO_LABEL'); ?>" name="donoFoto" id="donoFoto" maxlength="250" value="<?php echo htmlentities($data['donoFoto'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>

                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9"><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_NOMEFOTO_LABEL'); ?></label>
                            <input type="text" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_NOMEFOTO_LABEL'); ?>" name="nomeFoto" id="nomeFoto" maxlength="500" value="<?php echo htmlentities($data['nomeFoto'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>
                    </div>

                    <div class="uploadFoto">
                        <div class="form-group" id="uploadFieldFoto">
                            <div class="col-md-12">
                                <div class="file-loading">
                                    <input type="file" name="fileupload_foto[]" id="fileupload_foto" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group checkBox">
                        <div><?php echo JText::_( 'COM_VIRTUALDESK_USER_VERACIDADEDADOS_LABEL' ); ?></div>
                        <input type="checkbox" name="veracidadedados" id="veracidadedados" required class="make-switch form-control" value="1"
                               data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                            <?php if((int)$this->data->veracidadedados>0)  echo 'checked="checked"' ?>
                        />
                        <div class="row errorLabel"></div>
                    </div>


                    <div class="form-group checkBox">
                        <div><?php  echo JText::sprintf( 'COM_VIRTUALDESK_USER_POLITICAPRIVACIDADE_LABEL', JUri::root() ); ?></div>
                        <input type="checkbox" name="politicaprivacidade" id="politicaprivacidade" required class="make-switch form-control" value="1"
                            <?php echo $arUserFieldsConfig['UserField_POLITICAPRIVACIDADE_Required']; ?>
                               data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                            <?php if((int)$this->data->politicaprivacidade>0)  echo 'checked="checked"' ?>
                        />
                        <div class="row errorLabel" ></div>

                    </div>

                    <?php if ($captcha_plugin!='0') : ?>
                        <div class="form-group" >
                            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                            $field_id = 'dynamic_recaptcha_1';
                            print $captcha->display($field_id, $field_id, 'g-recaptcha');
                            ?>
                            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                        </div>
                    <?php endif; ?>

                    <div class="form-actions">
                        <button type="submit" class="btn submit"><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_SUBMIT'); ?></button>
                        <a class="btn cancel" href="<?php echo JRoute::_('index.php'); ?>"><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_CANCEL'); ?></a>
                    </div>
                    <input type="hidden" name="newUploadFoto" value="newUploadFoto">
                    <?php echo JHtml::_('form.token'); ?>

                </form>

                <div class="introUpload">
                    <p><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_INTRO1');?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_UPLOADFOTO_INTRO2');?></p>
                </div>
            </div>
            <!-- END FORM -->
        </div>

        <div id="cover-spin"></div>

        <?php
            echo $headScripts;
            echo $recaptchaScripts;
            echo $footerScripts;
        ?>

        <script>
            <?php
                require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/uploadfoto.js.php');
            ?>
        </script>
    </body>
</html>