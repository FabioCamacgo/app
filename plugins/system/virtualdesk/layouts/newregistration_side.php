<?php

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

defined('JPATH_BASE') or die;
defined('_JEXEC') or die;

$templateName  = 'virtualdesk';
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';

$obParam      = new VirtualDeskSiteParamsHelper();

$imageBackground = $obParam->getParamsByTag('imgFundoAPP');
$logoInterface = $obParam->getParamsByTag('logoInterfaceBlack');
$versao = $obParam->getParamsByTag('versaoAPP');
$linkVersao = $obParam->getParamsByTag('linkVersaoAPP');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
$LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
$LinkInterface = $obParam->getParamsByTag('LinkInterface');
$LogoEntrada = $obParam->getParamsByTag('logoEntradaAPP');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}


//GLOBAL SCRIPTS
$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;

// PAGE LEVEL PLUGIN SCRIPTS
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/backstretch/jquery.backstretch.min.js' . $addscript_end;
/*
* Tive de retirar o login-5.js para poder "customizar" a entrada com a validação, etc... passei para o ficheiro newregistration.js
* Devido a isso tive de colocar o código para as imagens de background no final deste ficheiro
*/
//$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/login-5.min.js' . $addscript_end;
$headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$headScripts .= $addscript_ini .$baseurl . 'plugins/system/virtualdesk/layouts/newregistration.js' . $addscript_end;

//BEGIN GLOBAL MANDATORY STYLES
$headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
//END GLOBAL MANDATORY STYLES

//BEGIN PAGE LEVEL PLUGIN STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
//END PAGE LEVEL PLUGIN STYLES

//BEGIN THEME GLOBAL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
//END THEME GLOBAL STYLES

// BEGIN PAGE LEVEL STYLES
$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login-5.min.css'. $addcss_end;

// CUSTOM JS DASHboard
$footerScripts  = '<!--[if lt IE 9]>';
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
$footerScripts .= '<![endif]-->';

$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;
$footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;


// Recaptha - utilização do plugin interno do joomla
// check params for recpacha
//$captcha_plugin  = JFactory::getConfig()->get('captcha');
$recaptchaScripts = '';
if ($captcha_plugin != '0') {
    // Load callback first for browser compatibility
    $recaptchaScripts  = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
    $recaptchaScripts .= $addscript_ini.'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag.$addscript_end;;
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?php echo $doc->language; ?>" dir="<?php echo $doc->direction; ?>">
<!--<![endif]-->

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <?php echo $headCSS; ?>

    <style>
        .user-login-5 .login-container > .login-content { margin-top: 50px; }
        .user-login-5 .login-logo.login-6 { top: 1.5em; left: 4em; }
        .user-login-5 .login-container > .login-content > .login-form { margin-top: 10px;  }
        .control-label {font-size: 14px; color: #4e5a64; text-align: right;  margin-bottom: 0; padding-top: 7px;}
        .help-block { margin-top: -20px;  margin-bottom: 15px;}
    </style>
</head>

<body class=" login">

<script>
    arrayVDPagesLoginBackGround = [
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg1.jpg",
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg2.jpg",
        "<?php echo $baseurl . 'templates/' . $templateName ?>/images/login/bg3.jpg"
    ];

    <?php
    /*
     * Parametros e Mensagens para a verificação da password em javascript / jquery validation
     */
    ?>
    var virtualDeskPassCheckOptions = {
        // check min length
        m_minLength       :<?php echo $passwordcheck_length ?>
        ,message_MinLength : "<?php echo str_replace('%s','{0}',JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_LENGTH')); ?>"
        // check login in pass
        ,p_Username        :<?php echo $passwordcheck_no_name ?>
        ,p_UsernameElem    : 'login'
        ,message_Username  : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_NAME'); ?>"
        // check email in pass
        ,p_Email           :<?php echo $passwordcheck_no_name ?>
        ,p_EmailElem       : 'email1'
        ,message_Email     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_EMAIL'); ?>"
        //check cchar
        ,message_chartype  : "<?php echo str_replace('%s','{0}',JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE')); ?>"
        // check letras minusculas
        ,p_AZMin           :<?php echo $passwordcheck_types_azmin ?>
        ,message_AZMin     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_LOWERCASELETTER'); ?>"
        // check letras minusculas
        ,p_AZMai           :<?php echo $passwordcheck_types_azmai ?>
        ,message_AZMai     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_UPPERCASELETTER'); ?>"
        // check números
        ,p_Num           :<?php echo $passwordcheck_types_num ?>
        ,message_Num     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_NUMBER'); ?>"
        // check números
        ,p_SpecialChar           :<?php echo $passwordcheck_types_special ?>
        ,message_SpecialChar     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_SPECIALCHARACTER'); ?>"
        // check espaços
        ,message_Spaces     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_STARTSENDS'); ?>"
        // check pass1 e pass2
        ,message_pass1pass2     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_PASS1PASS2'); ?>"

    };

    // js para identificar se o login é do tipo nif
    var setUserFieldLoginTypeNIF_JS = <?php echo $arUserFieldLoginConfig['setUserFieldLoginTypeNIF_JS']; ?>;

    var virtualDeskNIFCheckOptions = {
        message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'); ?>"
    };
</script>

<!-- Body -->
<!-- BEGIN : LOGIN PAGE 5-2 -->
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 login-container bs-reset">
            <div class="row">
                <!-- BEGIN LOGO -->
                <div class="logo">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="<?php echo $baseurl; ?>">
                                <img src="<?php echo $baseurl; ?>/<?php echo $LogoEntrada ?>"  alt="logo" class="logo-default"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="login-content">

                <?php
                // Verifica mensagens de erro/sucesso do joomla/php
                $messages       = $app->getMessageQueue();
                $messagesDanger = '';
                $messagesSucess = '';
                if(!is_array($messages)) $messages = array();
                foreach($messages as $chvMsg => $valMsg)
                {
                    if($valMsg['type']=='warning' or $valMsg['type']=='error')
                    {
                        $messagesDanger .= '<i class="fa-lg fa fa-warning"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                    }
                    elseif ($valMsg['type']=='message')
                    {
                        $messagesSucess .= '<i class="fa-lg fa fa-check"></i>&nbsp;<span>'.$valMsg['message'].'</span>';
                    }
                }
                // Apenas mais uma verificação para que não surja a mensgen de sucesso se deu erro na criação do utilizador
                // a variável vem do script newregistraion.php
                if($resNewUser!==true) $messagesSucess = '';

                ?>


                <h1><?php echo JText::_('COM_VIRTUALDESK_FORMS_REGISTRATION_TITLE'); ?></h1>
                <p><?php echo JText::_('COM_VIRTUALDESK_FORMS_REGISTRATION_DESC'); ?></p>


                <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                </div>

                <?php if (!empty($messagesDanger)) : ?>
                    <!-- bloco para mensagens de erro do joomla/php -->
                    <div id="MainMessageAlertBlock2Joomla" class="alert alert-danger fade in" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <?php echo $messagesDanger; ?>
                    </div>
                <?php endif; ?>


                <?php
                // bloco para mensagens de sucesso do joomla/php
                if (!empty($messagesSucess)) : ?>
                    <div id="MainMessageSucessBlock" class="alert alert-success fade in" >
                        <h4 class="alert-heading"><?php echo $messagesSucess; ?></h4>
                        <div class="text-center"><a class="btn blue" style="margin-top:25px;" href="<?php echo JRoute::_('index.php'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?>"><?php echo JText::_('COM_VIRTUALDESK_LOGIN_FORM'); ?></a></div>
                    </div>
                <?php endif; ?>

                <script>
                    <!-- Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js) -->
                    var MessageAlert = new function() {
                        this.getRequiredMissed = function (nErrors) {
                            if (nErrors == null)
                            { return(''); }
                            if(nErrors==1)
                            { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                            else  if(nErrors>1)
                            { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                                return (msg.replace("%s",nErrors) );
                            }
                        };
                    }
                </script>

                <form <?php if(!empty($messagesSucess) && empty($messagesDanger)) echo 'style="display:none;" ';?>  id="member-registration" action="<?php echo JRoute::_('?token&captcha&lang='.$language_tag); ?>" method="post"  class="register-form login-form" enctype="multipart/form-data" >

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $arUserFieldLoginConfig['CampoForm_Username_Label']; ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo $arUserFieldLoginConfig['CampoForm_Username_Label']; ?>" name="login" id="login" maxlength="<?php echo $arUserFieldLoginConfig['CampoForm_Username_MaxLength']; ?>" value="<?php echo htmlentities($data['login'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_LABEL'); ?></label>
                        <div class="col-md-9">
                        <input type="password" class="form-control placeholder-no-fix" autocomplete="off"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_LABEL'); ?>" name="password1" id="password1" maxlength="25"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_REPEAT_LABEL'); ?></label>
                        <div class="col-md-9">
                        <input type="password" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_REPEAT_LABEL'); ?>" name="password2" id="password2" maxlength="25"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_LABEL'); ?></label>
                        <div class="col-md-9">
                        <input type="email" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_LABEL'); ?>" name="email1" id="email1" maxlength="250" value="<?php echo htmlentities($data['email1'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_REPEAT_LABEL'); ?></label>
                        <div class="col-md-9">
                        <input type="email" class="form-control placeholder-no-fix" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_REPEAT_LABEL'); ?>" name="email2" id="email2"   maxlength="250" value="<?php echo htmlentities($data['email2'], ENT_QUOTES, 'UTF-8'); ?>">
                        </div>
                    </div>



                    <?php
                    // Apresenta o campo apenas se o login não for do tipo NIF E se o campo NIF estiver ativo na configuração para esta view
                    if($arUserFieldLoginConfig['setUserFieldLoginTypeNIF']==false && $arUserFieldsConfig['UserField_NIF_Registration']===true) : ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" autocomplete="off"  <?php echo $arUserFieldsConfig['UserField_NIF_Required']; ?> placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL'); ?>" name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo htmlentities($data['fiscalid'], ENT_QUOTES, 'UTF-8'); ?>">
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php foreach ($ObjUserFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                        <?php if($arUserFieldsConfig['UserField_' . $valConfFieldNames . '_Registration' ]===true) : ?>
                            <?php if ($valConfFieldNames == 'NIF') : ?>

                            <?php elseif ($valConfFieldNames == 'CIVILID') : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                               placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL'); ?>"
                                               name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                               maxlength="10"
                                               value="<?php echo htmlentities($data[$keyConfFieldNames], ENT_QUOTES, 'UTF-8'); ?>"/>
                                    </div>
                                </div>

                            <?php elseif ($valConfFieldNames == 'ADDRESS') : ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                    <div class="col-md-9">
                                            <textarea <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control" rows="3"
                                                                                                                                        placeholder="<?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?>"
                                                                                                                                        name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                                                                                                                        maxlength="250">
                                                      <?php echo htmlentities($data[$keyConfFieldNames], ENT_QUOTES, 'UTF-8'); ?></textarea>
                                    </div>
                                </div>

                            <?php elseif ($valConfFieldNames == 'VERACIDADEDADOS') : ?>

                            <?php elseif ($valConfFieldNames == 'POLITICAPRIVACIDADE') : ?>

                            <?php else: ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                               placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL'); ?>"
                                               name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                               value="<?php echo htmlentities($data[$keyConfFieldNames], ENT_QUOTES, 'UTF-8'); ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                    <?php endforeach; ?>



                    <div class="row">
                        <?php if (!empty($ObjUserFields->arConfFieldNames ['veracidadedados'])) : ?>
                            <div class="form-group">
                                <div class="col-md-3 " style="padding: 0;text-align: center">
                                    <input type="checkbox" name="veracidadedados" id="veracidadedados" class="make-switch form-control" value="1"
                                        <?php echo $arUserFieldsConfig['UserField_VERACIDADEDADOS_Required']; ?>
                                           data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                                        <?php if((int)$this->data->veracidadedados>0)  echo 'checked="checked"' ?>
                                    />
                                </div>
                                <label class="col-md-9 " ><?php echo JText::_( 'COM_VIRTUALDESK_USER_VERACIDADEDADOS_LABEL' ); ?></label>
                                <div class="row errorLabel"></div>
                            </div>
                        <?php endif; ?>
                    </div>


                    <div class="row">
                        <?php if (!empty($ObjUserFields->arConfFieldNames ['politicaprivacidade'])) : ?>
                            <div class="form-group">
                                <div class="col-md-3 " style="padding: 0;text-align: center">
                                    <input type="checkbox" name="politicaprivacidade" id="politicaprivacidade" class="make-switch form-control" value="1"
                                        <?php echo $arUserFieldsConfig['UserField_POLITICAPRIVACIDADE_Required']; ?>
                                           data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                                        <?php if((int)$this->data->politicaprivacidade>0)  echo 'checked="checked"' ?>
                                    />
                                </div>
                                <label class="col-md-9 "  ><?php  echo JText::sprintf( 'COM_VIRTUALDESK_USER_POLITICAPRIVACIDADE_LABEL', JUri::root() ); ?></label>
                                <div class="row errorLabel" ></div>

                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="row">

                    </div>

                    <?php if ($captcha_plugin!='0') : ?>
                        <div class="form-group" >
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-9 text-left">
                            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                            $field_id = 'dynamic_recaptcha_1';
                            print $captcha->display($field_id, $field_id, 'g-recaptcha');
                            ?>
                            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                           </div>
                        </div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <button type="submit" class="btn blue"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span></button>
                            <a class="btn default" href="<?php echo JRoute::_('index.php'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                    <input type="hidden" name="newRegSubmit" value="newRegSubmit">
                    <?php echo JHtml::_('form.token'); ?>


                </form>
            </div>

            <div class="copyright">

                <div class="col-md-4">
                    <div class="spaceTop"><a href="<?php echo $linkVersao;?>" class="versao" target="_blank">digitalGOV V.<?php echo $versao;?></a></div>
                </div>

                <div class="col-md-4">
                    <div class="contentCopyright">
                        <?php echo $year = date("Y"); ?> ©
                        <a href="<?php echo $LinkCopyright;?>" target="_blank">
                            <?php echo $copyrightAPP; ?>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="logoInterface" style="margin-top:0;">
                        <a href="<?php echo $LinkInterface;?>" target="_blank">
                            <span class="prod">The Interface Projects <br><span class="connect">Governação Digital</span></span>
                            <img src="<?php echo $baseurl . $logoInterface;?>" alt="TIP" title=""/>
                        </a>
                    </div>
                </div>


            </div>


        </div>

        <div class="col-md-6 bs-reset">
            <img src="<?php echo $baseurl . $imageBackground; ?>" alt="imgBackground"/>
        </div>

    </div>
</div>

<?php echo $headScripts; ?>
<?php echo $recaptchaScripts; ?>
<?php echo $footerScripts; ?>
<script>
    jQuery(document).ready(function() {

        // init background slide images
        jQuery('.login-bg').backstretch(arrayVDPagesLoginBackGround, {
                fade: 1000,
                duration: 8000
            }
        );
    });
</script>


</body>
</html>












