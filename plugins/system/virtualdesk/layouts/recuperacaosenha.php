<?php
    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('recuperacaosenha');
    if ($resPluginEnabled === false) exit();

    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';

    // Se não estiver ativo o pedido de nova senha devemos parar o processamento
    $configSetForgotPassword = JComponentHelper::getParams('com_virtualdesk')->get('setforgotpassword');
    if($configSetForgotPassword == '0' or empty($configSetForgotPassword) ) die;

    // Verifica qual o layout a apresentar
    $setforgotpasswordlayout = JComponentHelper::getParams('com_virtualdesk')->get('setforgotpasswordlayout');

    // Idiomas
    $lang      = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir  = JPATH_SITE;
    $jinput    = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload    = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);

    // Verifica se foi realizado um post dos dados
    $resCheckFormSubmit = VirtualDeskSiteUserHelper::checkForgotPasswordFormData();
    $arrayMessages = array();
    $data          = array(); // inicializa array com dados
    $resNewUser    = false;  // poor defeito... não criou ainda o utilizdor...
    if($resCheckFormSubmit)
    {
        // Se sim começa o processo de criação e activação de um novo utilizador
        // antes de tudo valida o recaptha ... se der erro não continua o processo
        if ($captcha_plugin!='0') {
            $captcha                  = JCaptcha::getInstance($captcha_plugin);
            $recaptcha_response_field = $jinput->get('g-recaptcha-response', '', 'string');
            $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
            if (!$resRecaptchaAnswer) {

                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_CAPTCHA'), 'error');
            }
            else
            { // Pode carregar e validar os dados submetidos
                $model = JModelLegacy::getInstance('Profile', 'VirtualDeskModel');
                $data  = $model->getForgotPasswordPostedFormData();
                //validação dos dados submetidos
                $resDataValid = $model->validateForgotPasswordBeforeSave($data);

                // Só passa o seguinte se não tiver erros. Se tiver erros serão apresentados no bloco no inicio do formulário (processado no include em baixo)
                if($resDataValid===true){
                    //Gravação do utilizador
                    $resNewUser =  VirtualDeskSiteUserHelper::saveForgotPasswordRequest($data);
                    // A mensagem de sucesso já foi colocada no método anterior... utiliza depois esta variável para colocar o botão "Proceed to Login"
                }
            }
        }
    }
    else
    { // não foi feita uma submissão de dados ou ocorreu alguma erro... limpa objecto $data
        $data = VirtualDeskSiteUserHelper::getForgotPasswordCleanPostedData();
    }

    $obParam      = new VirtualDeskSiteParamsHelper();

    $cssFile = $obParam->getParamsByTag('cssFileGeral');
    $logosbrandpartner = $obParam->getParamsByTag('logosbrandpartner');
    $linkbrandpartner = $obParam->getParamsByTag('linkbrandpartner');
    $logowebsiteComercial = $obParam->getParamsByTag('logowebsiteComercial');
    $linkwebsiteComercial = $obParam->getParamsByTag('linkwebsiteComercial');
    $logoEntradaAPP = $obParam->getParamsByTag('logoEntradaAPP');
    $imgFundoAPP = $obParam->getParamsByTag('imgFundoAPP');
    $explodeBrandPartnerLogo = explode(';;', $logosbrandpartner);
    $explodeBrandPartnerLink = explode(';;', $linkbrandpartner);
    ?>

    <link rel="stylesheet" href="<?php echo $baseurl;?>templates/virtualdesk/<?php echo $cssFile;?>">

    <div class="barraSuperior">
        <div class="content">

            <?php
                for($i=0; $i<count($explodeBrandPartnerLogo); $i++){
                    $indice = $i + 1;
                    ?>
                    <a href="<?php echo $explodeBrandPartnerLink[$i];?>" target="_blank">
                        <div class="botao brandpartner<?php echo $indice;?>">
                            <img src="<?php echo $baseurl . 'images/v_agenda/brandpartner/' . $explodeBrandPartnerLogo[$i];?>" alt="brandpartner<?php echo $indice;?>"/>
                        </div>
                    </a>
                    <?php
                }
            ?>

            <a href="<?php echo $linkwebsiteComercial;?>" class="logoWebsite">
                <img src="<?php echo $baseurl . $logowebsiteComercial;?>" alt="website"/>
            </a>

        </div>
    </div>

    <div class="fundo" style="background-image: URL('..<?php echo $imgFundoAPP;?>');"></div>

    <div class="workSpace">

        <a href="<?php echo $baseurl;?>" class="logo">
            <img src="<?php echo $baseurl . $logoEntradaAPP;?>" alt="logo_plataforma"/>
        </a>

        <?php

            // Para garantir que não dá uma exception no php... inicializo o $data
            $data = VirtualDeskSiteUserHelper::getForgotPasswordSetIfNullPostData($data);

            $setforgotpasswordlayout = 'recuperacaosenha_center.php';

            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . $setforgotpasswordlayout);
        ?>
    </div>
