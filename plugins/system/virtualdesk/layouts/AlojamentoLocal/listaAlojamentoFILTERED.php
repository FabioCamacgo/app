<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaAlojamento');
    if ($resPluginEnabled === false) exit();

    $listaAlojamentoFiltered = VirtualDeskSiteAlojamentoHelper::getAlojamentosFiltered($categoria, $subcategoria, $state, $nomePost, $tipo, $freg);

    foreach($listaAlojamentoFiltered as $rowWSL) :
        $id = $rowWSL['id'];
        $referencia = $rowWSL['referencia'];
        $designacao = $rowWSL['designacao'];
        $freguesia = $rowWSL['freguesia'];
        $tipologiaAL = $rowWSL['tipologia'];
        $nQuartos = $rowWSL['nQuartos_Alojamento'];
        $nUtentes = $rowWSL['nUtentes_Alojamento'];
        $nCamas = $rowWSL['nCamas_Alojamento'];
        $name = str_replace(' ', '_', $designacao);
        $code = $id . '_' . $name;
        $temImagem = 0;
        $imgAL = VirtualDeskSiteAlojamentoHelper::getImgAL($referencia);
        ?>
        <div class="alojamento">
            <div class="imagem">
                <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" title="<?php echo $designacao;?>" target="_blank">
                    <?php
                    if((int)$imgAL == 0){
                        ?>
                        <img src="<?php echo $pathAPP; ?>plugins/system/virtualdesk/layouts/Empresas/imagensEmpresas/categoria_<?php echo $categoria; ?>.jpg" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>"/>
                        <?php
                    } else {
                        $objEventFile = new VirtualDeskSiteEmpresasFilesHelper();
                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                        $FileList21Html = '';
                        foreach ($arFileList as $rowFile) {
                            if($temImagem == 0){
                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                ?>
                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $code; ?>" title="<?php echo $designacao; ?>">
                                <?php
                                $temImagem = 1;
                            }
                        }
                    }
                    ?>
                </a>
            </div>
            <div class="dados">
                <div class="titulo">
                    <a href="" title="<?php echo $designacao;?>">
                        <h3 class="titleAL">
                            <?php echo $designacao; ?>
                        </h3>
                    </a>
                </div>
                <div class="info">
                    <div class="tipologia">
                        <div class="content">
                            <div class="icon">
                                <img src="<?php echo $pathAPP; ?>plugins/system/virtualdesk/layouts/AlojamentoLocal/Icons/tipologia.png" alt="iconTipologia" title="Tipologia"/>
                            </div>
                            <div class="text">
                                <?php echo $tipologiaAL; ?>
                            </div>
                        </div>

                        <div class="Freg">
                            <div class="icon">
                                <img src="<?php echo $pathAPP; ?>plugins/system/virtualdesk/layouts/AlojamentoLocal/Icons/Freguesia.png" alt="iconFreguesia" title="Freguesia"/>
                            </div>
                            <div class="text">
                                <?php echo $freguesia; ?>
                            </div>
                        </div>
                    </div>
                    <div class="defAl">
                        <div class="content">
                            <div class="def1">
                                <div class="icon">
                                    <img src="<?php echo $pathAPP; ?>plugins/system/virtualdesk/layouts/AlojamentoLocal/Icons/quartos.png" alt="iconQuartos" title="Número de quartos"/>
                                </div>
                                <div class="text">
                                    <?php
                                    if(empty($nQuartos)){
                                        echo '-';
                                    } else {
                                        echo $nQuartos;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="def2">
                                <div class="icon">
                                    <img src="<?php echo $pathAPP; ?>plugins/system/virtualdesk/layouts/AlojamentoLocal/Icons/pessoas.png" alt="iconPessoas" title="Número de pessoas"/>
                                </div>
                                <div class="text">
                                    <?php
                                    if(empty($nUtentes)){
                                        echo '-';
                                    } else {
                                        echo $nUtentes;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="def3">
                                <div class="icon">
                                    <img src="<?php echo $pathAPP; ?>plugins/system/virtualdesk/layouts/AlojamentoLocal/Icons/camas.png" alt="iconCamas" title="Número de camas"/>
                                </div>
                                <div class="text">
                                    <?php
                                    if(empty($nCamas)){
                                        echo '-';
                                    } else {
                                        echo $nCamas;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="<?php echo $nomeSite . $menu;?>?name=<?php echo $code;?>" title="Saiba mais" target="_blank">
                        <div class="sabermais">
                            <button class="botaoSaberMais">
                                <div class="borda"></div>
                                <input type="submit" name="sabermais" id="sabermais" value="<?php echo JText::_('COM_VIRTUALDESK_EMPRESA_SABERMAIS'); ?>">
                            </button>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <?php
    endforeach;

?>