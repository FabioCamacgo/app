<?php

defined('JPATH_BASE') or die;
defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;

$isVDAjaxReq = $jinput->get('isVDAjaxReq');
$categoria = $jinput->get('categoria');
$subcategoria = $jinput->get('subcategoria');
$state = $jinput->get('state');
$pathAPP  = $jinput->get('pathAPP', '', 'STR');
$nomeSite = $jinput->get('nomeSite', '', 'STR');
$menu = $jinput->get('menu', '', 'STR');

JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteAlojamentoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alojamento.php');
JLoader::register('VirtualDeskSiteEmpresasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_files.php');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

/* Check if Plugin is Enabled ? */
$obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
$resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaAlojamento');
if ($resPluginEnabled === false) exit();

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$config          = JFactory::getConfig();
$sitename        = $config->get('sitename');
$sitedescription = $config->get('sitedescription');
$sitetitle       = $config->get('sitetitle');
$template        = $app->getTemplate(true);
$templateParams  = $template->params;
$logoFile        = $templateParams->get('logoFile');
$fluidContainer  = $config->get('fluidContainer');
$page_heading    = $config->get('page_heading');
$baseurl         = JUri::base();
$rooturl         = JUri::root();
$captcha_plugin  = JFactory::getConfig()->get('captcha');
$templateName    = 'virtualdesk';
$labelseparator = ' : ';
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';


// Idiomas
$lang = JFactory::getLanguage();
$extension = 'com_virtualdesk';
$base_dir = JPATH_SITE;

$jinput = JFactory::getApplication('site')->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$reload = true;
$lang->load($extension, $base_dir, $language_tag, $reload);
switch($language_tag){
    case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        $fileLangSufixV2 ='pt';
        break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}



$obPlg      = new VirtualDeskSitePluginsHelper();

if($isVDAjaxReq !=1){
    $layoutPath = $obPlg->getPluginLayoutAtivePath('listaAlojamento','form');
    require_once(JPATH_SITE . $layoutPath);
} else {
    $layoutPath = $obPlg->getPluginLayoutAtivePath('listaAlojamento','formajax');
    require_once(JPATH_SITE . $layoutPath);
}



?>
