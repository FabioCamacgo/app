<?php


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaAlojamento');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/AlojamentoLocal/listaAlojamento.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $idConcelho = $obParam->getParamsByTag('concelhoClean&Safe');

?>

<div class="w70">
    <?php
        if (isset($_POST['submitForm'])) {
            $nomePost = $_POST['nomePost'];
            $tipo = $_POST['tipologia'];
            $freg = $_POST['freguesia'];

            if(empty($nomePost) && empty($tipo) && empty($freg)){
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaAlojamentoALL.php');
            } else{
                require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaAlojamentoFILTERED.php');
            }
        } else {
            require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'listaAlojamentoALL.php');
        }

    ?>
</div>

<div class="w30">
    <form id="ListaAlojamento" action="" method="post" class="login-form" enctype="multipart/form-data" >

        <!--   Tipologia   -->
        <div class="form-group" id="tipo">
            <?php $Tipologias = VirtualDeskSiteAlojamentoHelper::getTipologia()?>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="tipologia" value="<?php echo $tipo; ?>" id="tipologia" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($tipo)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTO_TIPOLOGIA'); ?></option>
                            <?php foreach($Tipologias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['tipologia']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $tipo; ?>"><?php echo VirtualDeskSiteAlojamentoHelper::getTipName($tipo) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeTipologia = VirtualDeskSiteAlojamentoHelper::excludeTipologia($tipo)?>
                            <?php foreach($ExcludeTipologia as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['tipologia']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>


        <!--   Freguesia   -->
        <div class="form-group" id="freg">
            <?php $Freguesias = VirtualDeskSiteAlojamentoHelper::getFreguesia($idConcelho)?>
            <div class="col-md-9">
                <div class="input-group ">
                    <select name="freguesia" value="<?php echo $freg; ?>" id="freguesia" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($freg)){
                            ?>
                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_FREGUESIA'); ?></option>
                            <?php foreach($Freguesias as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['freguesia']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $freg; ?>"><?php echo VirtualDeskSiteAlojamentoHelper::getFregName($freg) ?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeFreguesia = VirtualDeskSiteAlojamentoHelper::excludeFreguesia($idConcelho, $freg)?>
                            <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['freguesia']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <!--   Pesquisa Livre  -->
        <div class="form-group" id="nomeSearch">
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_PESQUISALIVRE'); ?>"
                       name="nomePost" id="nomePost" value="<?php echo $nomePost; ?>"/>
            </div>
        </div>

        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="Pesquisar">
        </div>


    </form>
</div>



<?php

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;

?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
</script>
