<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('cleanSafe');
    if ($resPluginEnabled === false) exit();


    //GLOBAL SCRIPTS
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;


    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = '';
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $footerScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $linkPolPrivacidade = $obParam->getParamsByTag('linkPoliticaCleanSafe');
    $idConcelho = $obParam->getParamsByTag('concelhoClean&Safe');
    $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
    $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
    $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
    $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
    $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
    $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
    $moradaMunicipio = $obParam->getParamsByTag('moradaMunicipio');
    $codPostalMunicipio = $obParam->getParamsByTag('codPostalMunicipio');
    $logosendmailImage = $obParam->getParamsByTag('logoMailCleanSafe');

    if (isset($_POST['submitForm'])) {
        $name = $_POST['name'];
        $number = $_POST['number'];
        $freguesia = $_POST['freguesia'];
        $personType = $_POST['personType'];
        $nameReq = $_POST['nameReq'];
        $emailReq = $_POST['emailReq'];
        $emailConf = $_POST['emailConf'];
        $nif = $_POST['nif'];
        $telefoneReq = $_POST['telefoneReq'];
        $veracid2 = $_POST['veracid2'];
        $politica2 = $_POST['politica2'];


        if(empty($name)){
            $errNomeAL = 1;
        } else {
            $errNomeAL = 0;
        }

        $errNumeroAL = VirtualDeskSiteAlojamentoHelper::validaNumber($number);

        if(empty($freguesia)){
            $errFreguesia = 1;
        } else {
            $errFreguesia = 0;
        }

        if($personType != 1 && $personType != 2 && $personType !=3){
            $errPersonType = 1;
        } else {
            $errPersonType = 0;
        }

        $errNomeReq = VirtualDeskSiteAlojamentoHelper::validaNome($nameReq);
        $errEmail = VirtualDeskSiteAlojamentoHelper::validaEmail($emailReq, $emailConf);
        $errNif = VirtualDeskSiteAlojamentoHelper::validaNif($nif);
        $errTelefone = VirtualDeskSiteAlojamentoHelper::validaTelefone($telefoneReq);

        // dynamic_recaptcha_1
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

        $errRecaptcha = 1;
        if((string)$recaptcha_response_field !='') {
            $jinput->set('g-recaptcha-response', $recaptcha_response_field);
            $resRecaptchaAnswer = $captcha->checkAnswer($recaptcha_response_field);
            $errRecaptcha = 0;
            if (!$resRecaptchaAnswer) {
                $errRecaptcha = 1;
            }
        }

        if($errNomeAL == 0 && $errNumeroAL == 0 && $errFreguesia == 0 && $errPersonType == 0 && $errNomeReq == 0 && $errEmail == 0 && $errNif == '' && $errTelefone == 0 && $politica2 == 1 && $veracid2 == 1 && $errRecaptcha == 0){
            $refExiste = 1;

            while($refExiste == 1){
                $referencia = VirtualDeskSiteAlojamentoHelper::random_code();
                $checkREF = VirtualDeskSiteAlojamentoHelper::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }

            $numeroAL = $number . '/AL';

            $resSaveForm = VirtualDeskSiteAlojamentoHelper::saveNewForm($referencia, $name, $numeroAL, $personType, $freguesia, $nameReq, $emailReq, $nif, $telefoneReq);

            if($resSaveForm==true) :?>
                <style>
                    .itemid-1461 #submitForm{display:none !important;}
                </style>

                <div class="sucesso">

                    <div class="sucessIcon">
                        <?php echo file_get_contents("images/cmps/svg/success.svg"); ?>
                    </div>

                    <p><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_SUCESSO'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_SUCESSO2'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_SUCESSO3'); ?></p>
                    <p><?php echo JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_SUCESSO4',$nomeMunicipio); ?></p>
                    <p><?php echo JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_SUCESSO5',$copyrightAPP); ?></p>
                </div>

                <?php

                /*Send Email*/

                $reqType = VirtualDeskSiteAlojamentoHelper::getReqType($personType);
                $fregName = VirtualDeskSiteAlojamentoHelper::getFregName($freguesia);

                VirtualDeskSiteAlojamentoHelper::SendEmailAdmin($referencia, $name, $numeroAL, $reqType, $fregName, $nameReq, $emailReq, $nif, $telefoneReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio);
                VirtualDeskSiteAlojamentoHelper::SendEmailUser($referencia, $name, $numeroAL, $reqType, $fregName, $nameReq, $emailReq, $nif, $telefoneReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio);

                /*END Send Email*/



                exit();
            endif;

        } else {
            ?>
            <script>
                dropError();
            </script>

            <div class="backgroundErro">
                <div class="erro" style="display:block;">
                    <button class="close">
                        <?php echo file_get_contents("images/cmps/svg/close.svg"); ?>
                    </button>

                    <h3>
                        <?php echo file_get_contents("images/cmps/svg/warning.svg") . ' ' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_AVISO'); ?>
                    </h3>

                    <ol>
                        <?php
                            if($errNomeAL == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_NOMEAL') . '</li>';
                            } else {
                                $errNomeAL = 0;
                            }

                            if($errNumeroAL == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_NUMEROAL_1') . '</li>';
                            } else if($errNumeroAL == 2){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_NUMEROAL_2') . '</li>';
                            } else {
                                $errNumeroAL = 0;
                            }

                            if($errFreguesia == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_FREGUESIA') . '</li>';
                            } else {
                                $errFreguesia = 0;
                            }

                            if($errPersonType == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_TIPO_REQUERENTE') . '</li>';
                            } else {
                                $errPersonType = 0;
                            }

                            if($errNomeReq == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_NOME_REQUERENTE_1') . '</li>';
                            } else if($errNomeReq == 2){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_NOME_REQUERENTE_2') . '</li>';
                            } else {
                                $errNomeReq = 0;
                            }

                            if($errEmail == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_EMAIL_REQUERENTE_1') . '</li>';
                            } else if($errEmail == 2){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_EMAIL_REQUERENTE_2') . '</li>';
                            } else {
                                $errEmail = 0;
                            }

                            if($errNif == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_NIF_REQUERENTE') . '</li>';
                            } else {
                                $errNif = 0;
                            }

                            if($errTelefone == 1){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_TELEFONE_REQUERENTE_1') . '</li>';
                            } else if($errTelefone == 2){
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_TELEFONE_REQUERENTE_2') . '</li>';
                            } else {
                                $errTelefone = 0;
                            }

                            if($veracid2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_VERACIDADE') . '</li>';
                            }

                            if($politica2 == 0) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_POLITICA') . '</li>';
                            }

                            if($errRecaptcha == 1) {
                                echo '<li>' . JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_ERRO_CAPTCHA') . '</li>';
                            } else {
                                $errRecaptcha = 0;
                            }
                        ?>
                    </ol>

                </div>
            </div>
            <?php
        }

    }

?>

<form id="new_cleanSafe" action="" method="post" class="cleanSafe-form" enctype="multipart/form-data" >

    <div class="block">

        <legend class="section"><h3><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_IDENTIFICACAO_AL'); ?></h3></legend>

        <div class="form-group" id="field-name">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NAME'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" name="name" id="name" maxlength="250" value="<?php echo $name; ?>"/>
            </div>
        </div>

        <div class="form-group" id="field-number">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NUMBER'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NUMBER_PLACEHOLDER');?>" name="number" id="number" maxlength="50" value="<?php echo $number; ?>"/> <span>/AL</span>
            </div>
        </div>

        <div class="form-group" id="field-freg">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_FREGUESIA') ?><span class="required">*</span></label>
            <?php $Freguesias = VirtualDeskSiteAlojamentoHelper::getFreguesia($idConcelho)?>
            <div class="col-md-9">
                <select name="freguesia" value="<?php echo $freguesia; ?>" id="freguesia" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($freguesia)){
                        ?>
                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_OPCAO'); ?></option>
                        <?php foreach($Freguesias as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteAlojamentoHelper::getFregName($freguesia) ?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeFreg = VirtualDeskSiteAlojamentoHelper::excludeFreguesia($idConcelho, $freguesia)?>
                        <?php foreach($ExcludeFreg as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['freguesia']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group" id="field-person">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_PERSONTYPE'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="radio" name="radioval" id="type1" value="type1" <?php if (isset($_POST["submitForm"]) && $_POST['personType'] == 1) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_PERSONTYPE_TYPE1'); ?>
                <input type="radio" name="radioval" id="type2" value="type2" <?php if (isset($_POST["submitForm"]) && $_POST['personType'] == 2) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_PERSONTYPE_TYPE2'); ?>
                <input type="radio" name="radioval" id="type3" value="type3" <?php if (isset($_POST["submitForm"]) && $_POST['personType'] == 3) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_PERSONTYPE_TYPE3'); ?>
            </div>

            <input type="hidden" id="personType" name="personType" value="<?php echo $personType; ?>">
        </div>

    </div>

    <div class="block">

        <legend class="section"><h3><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_IDENTIFICACAO_REQUERENTE'); ?></h3></legend>

        <div class="form-group" id="field-nameReq">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NOMEREQUERENTE'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" required class="form-control" name="nameReq" id="nameReq" value="<?php echo htmlentities($nameReq, ENT_QUOTES, 'UTF-8'); ?>" maxlength="250"/>
            </div>
        </div>

        <div class="form-group" id="field-mail">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="emailReq" id="emailReq" value="<?php echo $emailReq; ?>"/>
            </div>
        </div>

        <div class="form-group" id="field-mailConf">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL_REPEAT'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="emailConf" id="emailConf" value="<?php echo $emailConf; ?>"/>
            </div>
        </div>

        <div class="form-group" id="field-nif">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_FISCALID'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="nif" id="nif" maxlength="9" value="<?php echo $nif; ?>"/>
            </div>
        </div>

        <div class="form-group" id="field-telef">
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_TELEFONE'); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <input type="text" class="form-control" autocomplete="off" required name="telefoneReq" id="telefoneReq" maxlength="9" value="<?php echo $telefoneReq; ?>"/>
            </div>
        </div>

    </div>

    <div class="block">
        <legend class="section"><h3><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_VERIFICACOES'); ?></h3></legend>

        <div class="form-group" id="Veracidade">

            <input type="checkbox" name="veracid" id="veracid" value="<?php echo $veracid2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['veracid2'] == 1) { echo 'checked="checked"'; } ?> />
            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALOJAMENTOLOCAL_VERACIDADE'); ?><span class="required">*</span></label>

            <input type="hidden" id="veracid2" name="veracid2" value="<?php echo $veracid2; ?>">
        </div>

        <div class="form-group" id="PoliticaPrivacidade">

            <input type="checkbox" name="politica" id="politica" value="<?php echo $politica2; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['politica2'] == 1) { echo 'checked="checked"'; } ?>/>
            <label class="col-md-3 control-label"><?php echo JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_POLITICAPRIVACIDADE',$linkPolPrivacidade); ?><span class="required">*</span></label>

            <input type="hidden" id="politica2" name="politica2" value="<?php echo $politica2; ?>">
        </div>
    </div>



    <?php if ($captcha_plugin!='0') : ?>
        <div class="form-group" >
            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
            $field_id = 'dynamic_recaptcha_1';
            print $captcha->display($field_id, $field_id, 'g-recaptcha');
            ?>
            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
        </div>
    <?php endif; ?>



    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
    </div>

</form>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/AlojamentoLocal/cleanSafe.js.php');
    ?>
</script>
