<?php
    defined('_JEXEC') or die;
?>

function dropError(){
    jQuery('.backgroundErro').css('display','block');
}

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        //jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery( document ).ready(function() {

    ComponentsSelect2.init();

    document.getElementById('type1').onclick = function() {
        document.getElementById("personType").value = 1;
    };

    document.getElementById('type2').onclick = function() {
        document.getElementById("personType").value = 2;
    };

    document.getElementById('type3').onclick = function() {
        document.getElementById("personType").value = 3;
    };

    document.getElementById('veracid').onclick = function() {
        if ( this.checked ) {
            document.getElementById("veracid2").value = 1;
        } else {
            document.getElementById("veracid2").value = 0;
        }
    };

    document.getElementById('politica').onclick = function() {
        // access properties using this keyword
        if ( this.checked ) {
            document.getElementById("politica2").value = 1;
        } else {
            document.getElementById("politica2").value = 0;
        }
    };

    jQuery(document).on('keypress',function(e) {
        if(e.which == 13) {
            event.preventDefault();
            jQuery('#submitForm').click();
        }
    });

    jQuery(".close").click(function() {
        jQuery('.backgroundErro').css('display','none');
    });



});



