<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('faleConnoscoGovernment');
    if ($resPluginEnabled === false) exit();


    //GLOBAL SCRIPTS
    $headScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    $headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    //BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;
    //END PAGE LEVEL STYLES

    //BEGIN FILE UPLOAD STYLES
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;
    //END FILE UPLOAD STYLES

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }


    echo $headCSS;

if (isset($_POST['submitForm'])) {
    $tipoContacto = $_POST['tipoContacto'];
    $assunto = $_POST['assunto'];
    $projeto = $_POST['projeto'];
    $urlProjeto = $_POST['urlProjeto'];
    $espAssunto = $_POST['espAssunto'];
    $observacoesOrcamentoField = $_POST['observacoesOrcamentoField'];
    $parceria = $_POST['parceria'];
    $assuntoReuniao = $_POST['assuntoReuniao'];
    $dataReuniao = $_POST['dataReuniao'];
    $horaReuniao = $_POST['m_timepicker_3'];
    $nomeReuniao = $_POST['nomeReuniao'];
    $refVagaTrabalho = $_POST['refVagaTrabalho'];
    $nomeCand = $_POST['nomeCand'];
    $emailCand = $_POST['emailCand'];
    $emailConfCand = $_POST['emailConfCand'];
    $apresentacaoVaga = $_POST['apresentacaoVaga'];
    $nomeCV = $_POST['nomeCV'];
    $emailCV = $_POST['emailCV'];
    $emailConfCV = $_POST['emailConfCV'];
    $apresentacaoCV = $_POST['apresentacaoCV'];
    $apresentacao = $_POST['apresentacao'];
    $outroAssunto = $_POST['outroAssunto'];
    $propAlertar = $_POST['propAlertar'];
    $email = $_POST['email'];
    $emailConf = $_POST['emailConf'];
    $nome = $_POST['nome'];
    $dataAtual = date("Y-m-d");
    $piecesData = explode("-", $dataReuniao);
    $invertDataReuniao = $piecesData[2] . '-' . $piecesData[1] . '-' . $piecesData[0];


    /*Validações*/

    $errTipoContato = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaRadioInput($tipoContacto);

    /*Elogios e Sugestões*/
    if($tipoContacto != 5) {
        $errAssunto = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaSelect($assunto);
    } else {
        $errAssunto = 0;
    }

    /*Validações Ticket e alteraçao de modulos*/
    if(($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2)){
        $errProjeto = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaText2($projeto);
        $errUrl = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($urlProjeto);
        $errEspAssunto = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($espAssunto);
    } else {
        $errProjeto = 0;
        $errUrl = 0;
        $errEspAssunto = 0;
        $projeto = '';
        $urlProjeto = '';
        $espAssunto = '';
    }

    /*Validações Orçamento*/
    if(($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 2 && $assunto == 8)){
        $errOrcamento = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($observacoesOrcamentoField);
    } else {
        $errOrcamento = 0;
        $observacoesOrcamentoField = '';
    }

    /*Validações Parceria*/
    if(($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 3 && $assunto == 16)){
        $errParceria = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($parceria);
    } else {
        $errParceria = 0;
        $parceria = '';
    }

    /*Validações Reunião*/
    if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)){
        $errAssuntoReuniao = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaText2($assuntoReuniao);
        $errDataReuniao = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaDataReuniao($dataReuniao, $invertDataReuniao, $dataAtual);
        $errNomeReuniao = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaText2($nomeReuniao);
    } else {
        $errAssuntoReuniao = 0;
        $errDataReuniao = 0;
        $errHoraReuniao = 0;
        $errNomeReuniao = 0;
        $assuntoReuniao = '';
        $dataReuniao = '';
        $horaReuniao = '';
        $nomeReuniao = '';
    }

    /*Validações Outro Assunto*/
    if(($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 13)){
        $errOutroAssunto = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($outroAssunto);
    } else {
        $errOutroAssunto = 0;
        $outroAssunto = '';
    }

    /*Validações Vaga Trabalho*/
    if($tipoContacto == 3 && $assunto == 15){
        $errRefVagaTrabalho = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($refVagaTrabalho);
        $errAprVagaTrabalho = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($apresentacaoVaga);
        $errNomeCand = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaText($nomeCand);
        $errEmailCand = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaEmail($emailCand, $emailConfCand);
    } else {
        $errRefVagaTrabalho = 0;
        $errAprVagaTrabalho = 0;
        $errNomeCand = 0;
        $errEmailCand = 0;
        $refVagaTrabalho = '';
        $apresentacaoVaga = '';
        $nomeCand = '';
        $emailCand = '';
    }

    /*Validações Enviar CV*/
    if($tipoContacto == 3 && $assunto == 14){
        $errAprCV = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($apresentacaoCV);
        $errNomeCV = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaText($nomeCV);
        $errEmailCV = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaEmail($emailCV, $emailConfCV);
    } else {
        $errAprCV = 0;
        $errNomeCV = 0;
        $errEmailCV = 0;
        $apresentacaoCV = '';
        $nomeCV = '';
        $emailCV = '';
        $emailConfCV = '';
    }

    /*Validações Brainstorming / Conferências / Workshops*/
    if(($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18) || ($tipoContacto == 4 && $assunto == 19)){
        $errAprBrainstorming = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($apresentacao);
    } else {
        $errAprBrainstorming = 0;
        $apresentacao = '';
    }

    /*Validações Elogios e Sugestões*/
    if($tipoContacto == 5){
        $errPropAlertar = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaNotEmpty($propAlertar);
    } else {
        $errPropAlertar = 0;
        $propAlertar = '';
    }

    /*Validações Dados Pessoais*/
    if($tipoContacto == 1 || $tipoContacto == 2 || $tipoContacto == 4 || $tipoContacto == 5 || ($tipoContacto == 3 && $assunto == 16)){
        $errEmail = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaEmail($email, $emailConf);
        $errNome = VirtualDeskSiteGovernmentFaleConnoscoHelper::validaText($nome);
    } else {
        $errEmail = 0;
        $errNome = 0;
        $email = '';
        $nome = '';
    }

    /*Validações Recaptcha*/
    $captcha                  = JCaptcha::getInstance($captcha_plugin);
    $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');

    $errRecaptcha = 1;
    if((string)$recaptcha_response_field !='') {
        $jinput->set('g-recaptcha-response',$recaptcha_response_field);
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        $errRecaptcha = 0;
        if (!$resRecaptchaAnswer) {
            $errRecaptcha = 1;
        }
    }


    if($errRecaptcha == 0 && $errPropAlertar == 0 && $errNomeCV == 0 && $errEmailCV == 0 && $errAprCV == 0 && $errTipoContato == 0 && $errAssunto == 0 && $errProjeto == 0 && $errUrl == 0 && $errEspAssunto == 0 && $errParceria == 0 && $errAssuntoReuniao == 0 && $errDataReuniao == 0 && $errHoraReuniao == 0 && $errNomeReuniao == 0 && $errOutroAssunto == 0 && $errRefVagaTrabalho == 0 && $errNomeCand == 0 && $errEmailCand == 0 && $errAprVagaTrabalho == 0 && $errAprBrainstorming == 0 && $errEmail == 0 && $errNome == 0){
        $dataEntrada = date("Y-m-d");

        $website = 'Interface Government';

        $random = VirtualDeskSiteGovernmentFaleConnoscoHelper::getRandom(7);

        $idExterno = 'TIPG' . $random;


        if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)){
            $pieces = explode("-", $dataReuniao);
            $invertDataReuniao = $pieces['2'] . '-' . $pieces['1'] . '-' . $pieces['0'];
        } else {
            $invertDataReuniao = '';
        }

        if($tipoContacto == 5) {
            $assunto = '';
        }

        /*Save BD*/
        $saveRegisto = VirtualDeskSiteGovernmentFaleConnoscoHelper::saveBD($idExterno, $website, $tipoContacto, $assunto, $projeto, $urlProjeto, $espAssunto, $observacoesOrcamentoField, $parceria, $assuntoReuniao, $invertDataReuniao, $horaReuniao, $nomeReuniao, $refVagaTrabalho, $nomeCand, $emailCand, $apresentacaoVaga, $nomeCV, $emailCV, $apresentacaoCV, $apresentacao, $outroAssunto, $propAlertar, $concelho, $email, $nome, $dataAtual);

        if($saveRegisto==true) {
            ?>
            <style>
                #btPost {
                    display: none !important;
                }
            </style>

            <div class="sucesso">
                <p>
                <div class="sucessIcon">
                    <svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline
                            clip-rule="evenodd" fill="none" fill-rule="evenodd"
                            points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10"
                            stroke-width="2"/>
                        <path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg>
                </div>
                <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_SUCESSO'); ?>
                </p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_SUCESSO1'); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_SUCESSO2'); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_SUCESSO3'); ?></p>
            </div>

            <?php

            /*EMAILS*/

            /*Email Elogios e sugestões*/
            if($tipoContacto == 5){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailElogiosSugestões($propAlertar, $email, $nome);
            }

            /*Email Ticket e Alteraçao de modulo*/
            if(($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2)){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailTicket($assunto, $projeto, $urlProjeto, $espAssunto, $email, $nome);
            }

            /*Email Orçamento*/
            if(($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 2 && $assunto == 8)) {
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailOrcamento($observacoesOrcamentoField, $email, $nome);
            }

            /*Email Parceria*/
            if(($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 3 && $assunto == 16)){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailParceria($parceria, $email, $nome);
            }

            /*Email Reunião*/
            if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailReuniao($assuntoReuniao, $dataReuniao, $horaReuniao, $nomeReuniao, $email, $nome);
            }

            /*Email Convite Contratação Pública*/
            if(($tipoContacto == 1 && $assunto == 3) || ($tipoContacto == 2 && $assunto == 9)){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailCCP($email);
            }

            /*Email Requisicao / Compromisso */
            if(($tipoContacto == 1 && $assunto == 21) || ($tipoContacto == 2 && $assunto == 22)){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailCompromisso($email, $nome);
            }

            /*Email Outro Assunto*/
            if(($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 13)){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailOutroAssunto($outroAssunto, $email, $nome);
            }

            /*Email Vaga Trabalho*/
            if($tipoContacto == 3 && $assunto == 15){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailVagaTrabalho($refVagaTrabalho, $apresentacaoVaga, $nomeCand, $emailCand);
            }

                /*Email Enviar CV*/
            if($tipoContacto == 3 && $assunto == 14){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailNovoCV($apresentacaoCV, $nomeCV, $emailCV);
            }

            /*Email  Brainstorming / Conferências / Projeto*/
            if(($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18) || ($tipoContacto == 4 && $assunto == 19)){
                VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailBrainstorming($apresentacao, $email, $nome, $assunto);
            }

            if($tipoContacto == 5) {
                $nomeAssunto = JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ELOGIOS_SUGESTOES');
            } else {
                $nomeAssunto = VirtualDeskSiteGovernmentFaleConnoscoHelper::getAssuntoName($assunto);
            }

            VirtualDeskSiteGovernmentFaleConnoscoHelper::SendEmailAdmin($nomeAssunto);

            echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $idExterno . '__e__"/>');

            exit();

        } else {
            ?>
            <style>
                #enviarPedido {
                    display: none !important;
                }
            </style>

            <div class="sucesso">
                <p>
                <div class="errorIcon">
                    <?php
                    echo file_get_contents(JUri::base() . "images/Alertar.PT/svg/error.svg");
                    ?>
                </div>
                <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_1'); ?>
                </p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_2'); ?></p>
            </div>

            <?php

            exit();
        }

    } else {
        ?>
        <script>
            dropError();
        </script>

        <div class="backgroundErro">
            <div class="erro" style="display:block;">
                <button id="fechaErro">
                    <?php echo file_get_contents(JUri::base() . "images/svg/closeMenu.svg");?>
                </button>

                <h3>
                    <?php echo file_get_contents(JUri::base() . "images/svg/error.svg");?>
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_AVISO'); ?>
                </h3>

                <ol>
                    <?php

                    if($errTipoContato == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_TIPOCONTATO') . '</li>';
                    } else {
                        $errTipoContato = 0;
                    }

                    if($errAssunto == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ASSUNTO') . '</li>';
                    } else {
                        $errAssunto = 0;
                    }

                    if($errProjeto == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_PROJETO_1') . '</li>';
                    } else if($errProjeto == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_PROJETO_2') . '</li>';
                    } else{
                        $errProjeto = 0;
                    }

                    if($errUrl == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_URL') . '</li>';
                    } else{
                        $errUrl = 0;
                    }

                    if($errEspAssunto == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ESPECIFICACAO_ASSUNTO') . '</li>';
                    } else{
                        $errEspAssunto = 0;
                    }

                    if($errOrcamento == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_ERRO_PEDIDOORCAMENTO') . '</li>';
                    } else {
                        $errOrcamento = 0;
                    }

                    if($errParceria == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_PARCERIA') . '</li>';
                    } else {
                        $errParceria = 0;
                    }

                    if($errAssuntoReuniao == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ASSUNTO_REUNIAO_1') . '</li>';
                    } else if($errAssuntoReuniao == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_ASSUNTO_REUNIAO_2') . '</li>';
                    } else {
                        $errAssuntoReuniao = 0;
                    }

                    if($errDataReuniao == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_DATA_REUNIAO') . '</li>';
                    } else if($errDataReuniao == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_DATA_REUNIAO_2') . '</li>';
                    } else {
                        $errDataReuniao = 0;
                    }

                    if($errHoraReuniao == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_HORA_REUNIAO') . '</li>';
                    } else {
                        $errHoraReuniao = 0;
                    }

                    if($errNomeReuniao == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_REUNIAO_1') . '</li>';
                    } else if($errNomeReuniao == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_REUNIAO_2') . '</li>';
                    } else {
                        $errNomeReuniao = 0;
                    }

                    if($errOutroAssunto == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_OUTRO_ASSUNTO') . '</li>';
                    } else {
                        $errOutroAssunto = 0;
                    }

                    if($errRefVagaTrabalho == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_REF_VAGA_TRABALHO') . '</li>';
                    } else {
                        $errRefVagaTrabalho = 0;
                    }

                    if($errNomeCand == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECANDIDATO_1') . '</li>';
                    } else if($errNomeCand == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECANDIDATO_2') . '</li>';
                    } else {
                        $errNomeCand = 0;
                    }

                    if($errEmailCand == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCANDIDATO_1') . '</li>';
                    } else if($errEmailCand == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCANDIDATO_2') . '</li>';
                    } else {
                        $errEmailCand = 0;
                    }

                    if($errAprVagaTrabalho == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_APRESENTACAO_VAGA_TRABALHO') . '</li>';
                    } else {
                        $errAprVagaTrabalho = 0;
                    }

                    if($errNomeCV == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECV_1') . '</li>';
                    } else if($errNomeCV == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOMECV_2') . '</li>';
                    } else {
                        $errNomeCV = 0;
                    }

                    if($errEmailCV == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCV_1') . '</li>';
                    } else if($errEmailCV == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAILCV_2') . '</li>';
                    } else {
                        $errEmailCV = 0;
                    }

                    if($errAprCV == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_APRESENTACAO_CV') . '</li>';
                    } else {
                        $errAprCV = 0;
                    }

                    if($errPropAlertar == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_ERRO_PROPONHO') . '</li>';
                    } else {
                        $errPropAlertar = 0;
                    }

                    if($errAprBrainstorming == 1){
                        if($assunto == 17) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_ERRO_PROJETO') . '</li>';
                        } else if($assunto == 18) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_ERRO_BRAINSTORMING') . '</li>';
                        } else if($assunto == 19) {
                            echo '<li>' . JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_ERRO_CONFERENCIAS') . '</li>';
                        }
                    } else {
                        $errAprBrainstorming = 0;
                    }

                    if($errNome == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_1') . '</li>';
                    } else if($errNome == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_NOME_2') . '</li>';
                    } else {
                        $errNome = 0;
                    }

                    if($errEmail == 1){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_ERRO_EMAIL_1') . '</li>';
                    } else if($errEmail == 2){
                        echo '<li>' . JText::_('COM_VIRTUALDESK_ALERTARPT_ERRO_EMAIL_2') . '</li>';
                    } else {
                        $errEmail = 0;
                    }

                    if($errRecaptcha == 1) {
                        echo '<li>Erro na verificação Não Sou um Robô (Recaptcha)</li>';
                    }else{
                        $errRecaptcha = 0;
                    }
                    ?>
                </ol>
            </div>
        </div>
        <?php
    }
}

    ?>

    <legend class="section"><h2><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_TITLE_FALE_CONNOSCO'); ?></h2></legend>

    <form id="talk" action="/app/message/" method="post" class="talk-form" enctype="multipart/form-data" >

        <div class="form-group" id="tipo">
            <label class="col-md-3 control-label"></label>

            <?php $Tipologia = VirtualDeskSiteGovernmentFaleConnoscoHelper::getTipoFaleConnosco();?>

            <div class="col-md-9">

                <?php
                foreach($Tipologia as $rowWSL) : ?>

                    <span><input type="radio" name="radioval" id="<?php echo $rowWSL['idValue']; ?>" value="<?php echo $rowWSL['id']; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['tipoContacto'] == $rowWSL['id']) { echo 'checked="checked"'; } ?>> <?php echo $rowWSL['tipologia']; ?></span>

                    <?php
                    $numberRadio = $numberRadio + 1;
                    if($numberRadio == 2){
                        ?>
                        <span class="separator">|</span>
                        <?php
                    }
                endforeach; ?>

            </div>

            <input type="hidden" id="tipoContacto" name="tipoContacto" value="<?php echo $tipoContacto; ?>">
        </div>

        <!--   Assunto-->
        <?php
        $ListaDeMenuMain = array();
        if(!empty($tipoContacto)) {
            if( (int) $tipoContacto > 0) $ListaDeMenuMain = VirtualDeskSiteGovernmentFaleConnoscoHelper::getAssunto($tipoContacto);
        }
        ?>
        <div id="blocoSubCat" class="form-group">
            <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTAPT_FALECONNOSCO_ASSUNTO' ); ?><span class="required">*</span></label>
            <div class="col-md-9">
                <select name="assunto" id="assunto" value="<?php echo $assunto;?>" required
                    <?php
                    if(empty($tipoContacto)) {
                        echo 'disabled';
                    }
                    ?>
                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                    <?php
                    if(empty($assunto)){
                        ?>
                        <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                        <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                            <option value="<?php echo $rowMM['id']; ?>"
                            ><?php echo $rowMM['name']; ?></option>
                        <?php endforeach;
                    } else {
                        ?>
                        <option value="<?php echo $assunto; ?>"><?php echo VirtualDeskSiteGovernmentFaleConnoscoHelper::getAssuntoName($assunto);?></option>
                        <option value=""><?php echo '-'; ?></option>
                        <?php $ExcludeAssunto = VirtualDeskSiteGovernmentFaleConnoscoHelper::excludeAssunto($tipoContacto, $assunto);?>
                        <?php foreach($ExcludeAssunto as $rowWSL) : ?>
                            <option value="<?php echo $rowWSL['id']; ?>"
                            ><?php echo $rowWSL['name']; ?></option>
                        <?php endforeach;
                    }
                    ?>
                </select>

            </div>
        </div>

        <div id="anomalia"
            <?php if(($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2)) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }

            ?>
        >

            <h3 id="titleAnomalia"
                <?php if($assunto == 1) {
                    ?>
                    style="display:block";
                    <?php
                } else {
                    ?>
                    style="display:none";
                    <?php
                }

                ?>
            ><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ANOMALIA'); ?></h3>

            <h3 id="titleUpgrade"
                <?php if($assunto == 2) {
                    ?>
                    style="display:block";
                    <?php
                } else {
                    ?>
                    style="display:none";
                    <?php
                }

                ?>
            ><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_UPGRADE'); ?></h3>

            <!--   Projeto   -->
            <div class="form-group" id="proj">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PROJETO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="projeto" id="projeto" maxlength="250" value="<?php echo $projeto; ?>"/>
                </div>
            </div>


            <!--   URL   -->
            <div class="form-group" id="urlProj">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_URL'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="urlProjeto" id="urlProjeto" maxlength="250" value="<?php echo $urlProjeto; ?>"/>
                </div>
            </div>

            <!--   Especificação do assunto   -->
            <div class="form-group" id="esp">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ESPECIFICACAO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="espAssunto" id="espAssunto" maxlength="2000"><?php echo $espAssunto; ?></textarea>
                </div>
            </div>
        </div>

        <div id="orcamento"
            <?php if(($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 2 && $assunto == 8)) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ORCAMENTO'); ?></h3>

            <div class="form-group" id="observacoesOrcamento">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_PEDIDOORCAMENTO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="observacoesOrcamentoField" id="observacoesOrcamentoField" maxlength="2000"><?php echo $observacoesOrcamentoField; ?></textarea>
                </div>
            </div>

        </div>

        <div id="parcerias"
            <?php if(($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 3 && $assunto == 16)) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >
            <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PARCERIA_TITLE'); ?></h3>


            <!--   Especificação da parceria   -->
            <div class="form-group" id="parc">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PARCERIA'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="parceria" id="parceria" maxlength="2000"><?php echo $parceria; ?></textarea>
                </div>
            </div>
        </div>

        <div id="reuniao"
            <?php if(($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 2 && $assunto == 12)) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_REUNIAO'); ?></h3>


            <!--   Assunto   -->
            <div class="form-group" id="assReuniao">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ASSUNTO_REUNIAO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="assuntoReuniao" id="assuntoReuniao" maxlength="250" value="<?php echo $assuntoReuniao; ?>"/>
                </div>
            </div>


            <!--   Data de reuniao  -->
            <div class="form-group" id="reuData">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_DATAREUNIAO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                        <input type="text" placeholder="dd-mm-aaaa" name="dataReuniao" id="dataReuniao" value="<?php echo $dataReuniao; ?>">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <!--   Hora de reuniao  -->
            <div class="form-group" id="reuHour">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_HORAREUNIAO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <div class="input-group timepicker">
                        <input class="form-control m-input" name="m_timepicker_3" id="m_timepicker_3" value="<?php echo $horaReuniao;?>" type="text" />
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <i class="la la-clock-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--   Nome reuniao   -->
            <div class="form-group" id="nameReuniao">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_NOME_REUNIAO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="nomeReuniao" id="nomeReuniao" maxlength="250" value="<?php echo $nomeReuniao; ?>"/>
                </div>
            </div>
        </div>

        <div id="vagaTrabalho"
            <?php if($tipoContacto == 3 && $assunto == 15) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_VAGA'); ?></h3>

            <!--   Referencia   -->
            <div class="form-group" id="refTrabalho">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_REFERENCIA'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="refVagaTrabalho" id="refVagaTrabalho" maxlength="100" value="<?php echo $refVagaTrabalho; ?>"/>
                </div>
            </div>

            <!--   Nome Candidato   -->
            <div class="form-group" id="noneCand">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_NOMECANDIDATO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="nomeCand" id="nomeCand" maxlength="250" value="<?php echo $nomeCand; ?>"/>
                </div>
            </div>

            <!--   Email Candidato   -->
            <div class="form-group" id="mailCand">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_EMAILCANDIDATO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="emailCand" id="emailCand" maxlength="250" value="<?php echo $emailCand; ?>"/>
                </div>
            </div>

            <!--   VERIFICAÇÃO DE EMAIL   -->
            <div class="form-group" id="mailCand2">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_EMAIL_REPEAT'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" required name="emailConfCand" id="emailConfCand" value="<?php echo $emailConfCand; ?>"/>
                </div>
            </div>

            <!--   Apresentacao  -->
            <div class="form-group" id="apre">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_APRESENTACAO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="apresentacaoVaga" id="apresentacaoVaga" maxlength="2000"><?php echo $apresentacaoVaga; ?></textarea>
                </div>
            </div>

        </div>

        <div id="enviarCV"
            <?php if($tipoContacto == 3 && $assunto == 14) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ENVIARCV'); ?></h3>


            <!--   Nome CV   -->
            <div class="form-group" id="nomeEnvioCV">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_NOMECV'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="nomeCV" id="nomeCV" maxlength="250" value="<?php echo $nomeCV; ?>"/>
                </div>
            </div>

            <!--   Email CV   -->
            <div class="form-group" id="mailCV">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_EMAILCV'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" autocomplete="off" required name="emailCV" id="emailCV" maxlength="250" value="<?php echo $emailCV; ?>"/>
                </div>
            </div>

            <!--   VERIFICAÇÃO DE EMAIL   -->
            <div class="form-group" id="mailCV2">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_EMAIL_REPEAT'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" required name="emailConfCV" id="emailConfCV" value="<?php echo $emailConfCV; ?>"/>
                </div>
            </div>

            <!--   Apresentacao  -->
            <div class="form-group" id="apreCV">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_APRESENTACAO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="apresentacaoCV" id="apresentacaoCV" maxlength="2000"><?php echo $apresentacaoCV; ?></textarea>
                </div>
            </div>

        </div>

        <div id="parceriaBloco"
            <?php if(($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18) || ($tipoContacto == 4 && $assunto == 19)) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <div class="form-group" id="conv">
                <label id="labelProjeto" class="col-md-3 control-label"<?php if($assunto == 17) {
                    ?>
                    style="display:block";
                    <?php
                } else {
                    ?>
                    style="display:none";
                    <?php
                }
                ?>
                >
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_PROJETO_TITLE'); ?><span class="required">*</span></label>

                <label id="labelBrainstorming" class="col-md-3 control-label"<?php if($assunto == 18) {
                    ?>
                    style="display:block";
                    <?php
                } else {
                    ?>
                    style="display:none";
                    <?php
                }
                ?>
                >

                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_BRAINSSTORMING_TITLE'); ?><span class="required">*</span></label>

                <label id="labelConferencia" class="col-md-3 control-label"<?php if($assunto == 19) {
                    ?>
                    style="display:block";
                    <?php
                } else {
                    ?>
                    style="display:none";
                    <?php
                }
                ?>
                >

                    <?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_CONFERENCIA_TITLE'); ?><span class="required">*</span></label>



                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="apresentacao" id="apresentacao" maxlength="2000"><?php echo $apresentacao; ?></textarea>
                </div>
            </div>

        </div>

        <div id="outro"
            <?php if(($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 13)) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >
            <div class="form-group" id="outroAss">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_OUTRO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="outroAssunto" id="outroAssunto" maxlength="2000"><?php echo $outroAssunto; ?></textarea>
                </div>
            </div>

        </div>

        <div id="elogiosSugestoes"
            <?php if($tipoContacto == 5) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >
            <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ELOGIOS_SUGESTOES'); ?></h3>

            <div class="intro">
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ELOGIOS_SUGESTOES_INTRO'); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_ELOGIOS_SUGESTOES_INTRO_2'); ?></p>
            </div>


            <div class="form-group" id="proponho">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_PROPONHO'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <textarea required class="form-control" rows="7" name="propAlertar" id="propAlertar" maxlength="2000"><?php echo $propAlertar; ?></textarea>
                </div>
            </div>

        </div>

        <div id="enviarFicheiros"
            <?php if(($tipoContacto == 1 && $assunto == 21) || ($tipoContacto == 2 && $assunto == 22) || ($tipoContacto == 1 && $assunto == 1) || ($tipoContacto == 1 && $assunto == 2) || ($tipoContacto == 1 && $assunto == 4) || ($tipoContacto == 1 && $assunto == 5) || ($tipoContacto == 1 && $assunto == 6) || ($tipoContacto == 1 && $assunto == 7) || ($tipoContacto == 2 && $assunto == 8) || ($tipoContacto == 2 && $assunto == 10) || ($tipoContacto == 2 && $assunto == 12) || ($tipoContacto == 2 && $assunto == 13) || ($tipoContacto == 3 && $assunto == 14) || ($tipoContacto == 3 && $assunto == 16) || ($tipoContacto == 4 && $assunto == 17) || ($tipoContacto == 4 && $assunto == 18)) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >

            <div class="form-group" id="uploadField">
                <?php
                if($assunto == 13 || $assunto == 14){
                    ?>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_UPLOAD_CV'); ?></label>
                    <?php
                } else {
                    ?>
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_UPLOAD'); ?></label>
                    <?php
                }
                ?>

                <div class="col-md-9">

                    <div class="file-loading">
                        <input type="file" name="fileupload[]" id="fileupload" multiple>
                    </div>
                    <div id="errorBlock" class="help-block"></div>
                    <input type="hidden" id="VDAjaxReqProcRefId">

                </div>
            </div>
        </div>

        <div id="dadosPessoais"
            <?php if(($tipoContacto == 1 && !empty($assunto) && $assunto != 'Escolher opção') || ($tipoContacto == 2 && !empty($assunto) && $assunto != 'Escolher opção') || ($tipoContacto == 4 && !empty($assunto) && $assunto != 'Escolher opção')) {
                ?>
                style="display:block";
                <?php
            } else {
                ?>
                style="display:none";
                <?php
            }
            ?>
        >
            <legend class="section2"><h3><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO_DADOSPESSOAIS'); ?></h3></legend>

            <!--   NOME  -->
            <div class="form-group" id="name">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_NOME'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" required class="form-control" name="nome" id="nome" value="<?php echo htmlentities($nome, ENT_QUOTES, 'UTF-8'); ?>" maxlength="250"/>
                </div>
            </div>

            <!--   EMAIL   -->
            <div class="form-group" id="mail">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_FALECONNNOSCOGOVERNMENT_EMAIL'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" required name="email" id="email" value="<?php echo $email; ?>"/>
                </div>
            </div>

            <!--   VERIFICAÇÃO DE EMAIL   -->
            <div class="form-group" id="mail2">
                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTAPT_EMAIL_REPEAT'); ?><span class="required">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" required name="emailConf" id="emailConf" value="<?php echo $emailConf; ?>"/>
                </div>
            </div>
        </div>

        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group" id="captcha">
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>

        <div class="form-actions" method="post" style="display:none;">
            <input type="submit" name="submitForm" id="submitForm" value="<?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?>">
        </div>

    </form>



    <?php

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;

?>


<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'faleConnoscoGovernment/'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';

    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/interfaceGovernment/faleConnoscoGovernment.js.php');
    ?>
</script>
