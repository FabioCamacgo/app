<?php
defined('_JEXEC') or die;
?>

function dropError(){
    jQuery('.backgroundErro').css('display','block');
}

/*
 * Inicialização do Select 2
 */
var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

/*
 * Inicialização do FILE UPLOAD
 */
var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 4,

            // if null - has no limits
            // example: 3
            fileMaxSize: 5,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png', 'pdf'],

            enableApi: true,


            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = jQuery.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = jQuery.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - null
            upload: {
                // upload URL {String}
                url: setVDCurrentRelativePath , // definido no php

                // upload data {null, Object}
                // you can also change this Object in beforeSend callback
                // example: { option_1: '1', option_2: '2' }
                data:  null ,

                // upload type {String}
                // for more details http://api.jquery.com/jquery.ajax/
                type: 'POST',

                // upload enctype {String}
                // for more details http://api.jquery.com/jquery.ajax/
                enctype: 'multipart/form-data',

                // auto-start file upload {Boolean}
                // if false, you can use the API methods - item.upload.send() to trigger upload for each file
                // if false, you can use the upload button - check custom file name example
                start: false, // true,

                // upload the files synchron {Boolean}
                synchron: true,

                // upload large files in chunks {false, Number} set file chunk size in MB as Number (ex: 4)
                chunk: false,

                // Callback fired before uploading a file by returning false, you can prevent the upload
                beforeSend: function(item, listEl, parentEl, newInputEl, inputEl) {
                    // example:
                    // here you can extend the upload data

                    item.upload.data.isVDAjaxReqFileUpload = '1';
                    item.upload.data.VDAjaxReqProcRefId    = jQuery("#VDAjaxReqProcRefId").val();

                    return true;
                },

                // Callback fired if the upload succeeds
                // we will add by default a success icon and fadeOut the progressbar
                onSuccess: function(data, item, listEl, parentEl, newInputEl, inputEl, textStatus, jqXHR) {
                    item.html.find('.fileuploader-action-remove').addClass('fileuploader-action-success');

                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);
                },

                // Callback fired if the upload failed
                // we will set by default the progressbar to 0% and if it wasn't cancelled, we will add a retry button
                onError: function(item, listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus, errorThrown) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }

                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },

                onProgress: function(data, item, listEl, parentEl, newInputEl, inputEl) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },

                // Callback fired after all files were uploaded
                onComplete: function(listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus) {
                    // callback will go here

                    vdOnFileUploadLoadComplete(); // está definida no módulo do site se for invocado por ajax
                }
            }

            ,// by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

var vdAlertaHelperObj = function () {

    var vdAlertaData = '';

    var setAlertaDataByAjax = function (data) {
        vdAlertaData = data;
    };

    var getAlertaDataByAjax = function () {
        return vdAlertaData;
    };

    return {
        setAlertaDataByAjax  :  setAlertaDataByAjax,
        getAlertaDataByAjax  :  getAlertaDataByAjax
    };

}();

function getAssuntoByTipo($tipologia){
    var pathArray = window.location.pathname.split('/');

    var secondLevelLocation = pathArray[1];

    var tipologia = jQuery('#tipoContacto').val(); /* GET THE VALUE OF THE SELECTED DATA */

    var dataString = "tipologia="+tipologia; /* STORE THAT TO A DATA STRING */

    App.blockUI({ target:'#blocoSubCat',  animate: true});

    jQuery.ajax({
        url: websitepath,
        data:'m=government_getAssunto&tipologia=' + tipologia ,

        success: function(output) {

            var select = jQuery('#assunto');
            select.find('option').remove();

            if (output == null || output == '') {
                select.prop('disabled', true);
            }
            else {
                select.prop('disabled', false);
                select.append(jQuery('<option>Escolher opção</option>'));
                jQuery.each(JSON.parse(output), function (i, obj) {
                    select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                });
            }

            // hide loader
            App.unblockUI('#blocoSubCat');

        },
        error: function (xhr, ajaxOptions, thrownError) {
            select.find('option').remove();
            select.prop('disabled', true);

            App.unblockUI('#blocoSubCat');
        }
    });

}

function hideblocks(){
    document.getElementById("anomalia").style.display = 'none';
    document.getElementById("orcamento").style.display = 'none';
    document.getElementById("parcerias").style.display = 'none';
    document.getElementById("reuniao").style.display = 'none';
    document.getElementById("vagaTrabalho").style.display = 'none';
    document.getElementById("enviarCV").style.display = 'none';
    document.getElementById("parceriaBloco").style.display = 'none';
    document.getElementById("outro").style.display = 'none';
    document.getElementById("elogiosSugestoes").style.display = 'none';


}

jQuery(document).ready(function() {

    jQuery(".talk-form #numTecs svg").hover(function(){
        jQuery('.infoNumTecs').fadeIn('slow');
    }, function(){
        jQuery('.infoNumTecs').fadeOut('slow');
    });

    jQuery('.date-picker1').datepicker({autoclose:true});
    jQuery('.date-picker1').on('hide', function (e) { e.preventDefault(); });
    jQuery('#dataReuniao').attr('readonly',false);

    jQuery('#m_timepicker_3').timepicker({
        showMeridian: false
    });

    jQuery('#m_timepicker_3').on('hide', function (e) { e.preventDefault(); });

    ComponentsSelect2.init();

    ComponentFileUploader.init();

    var tipoContactoOnload = jQuery('#tipoContacto').val();
    var assuntoOnload = jQuery('#assunto').val();

    document.getElementById('Cliente').onclick = function() {
        if(document.getElementById("tipoContacto").value != 1){
            hideblocks();
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'none';
            document.getElementById("blocoSubCat").style.display = 'block';
            document.getElementById("tipoContacto").value = 1;
            getAssuntoByTipo('1');
        }
    };

    document.getElementById('NovoCliente').onclick = function() {
        if(document.getElementById("tipoContacto").value != 2) {
            hideblocks();
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'none';
            document.getElementById("blocoSubCat").style.display = 'block';
            document.getElementById("tipoContacto").value = 2;
            getAssuntoByTipo('2');
        }
    };

    document.getElementById('Carreira').onclick = function() {
        if(document.getElementById("tipoContacto").value != 3) {
            hideblocks();
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'none';
            document.getElementById("blocoSubCat").style.display = 'block';
            document.getElementById("tipoContacto").value = 3;
            getAssuntoByTipo('3');
        }
    };

    document.getElementById('ParceriaTipo').onclick = function() {
        if(document.getElementById("tipoContacto").value != 4) {
            hideblocks();
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'none';
            document.getElementById("blocoSubCat").style.display = 'block';
            document.getElementById("tipoContacto").value = 4;
            getAssuntoByTipo('4');
        }
    };

    document.getElementById('Elogios').onclick = function() {
        if(document.getElementById("tipoContacto").value != 5) {
            hideblocks();
            document.getElementById("blocoSubCat").style.display = 'none';
            document.getElementById("elogiosSugestoes").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'block';
            document.getElementById("dadosPessoais").style.display = 'block';
            document.getElementById("tipoContacto").value = 5;
        }
    };

    if(tipoContactoOnload == 5){
        document.getElementById("blocoSubCat").style.display = 'none';
        document.getElementById("elogiosSugestoes").style.display = 'block';
        document.getElementById("enviarFicheiros").style.display = 'block';
        document.getElementById("dadosPessoais").style.display = 'block';
        document.getElementById("tipoContacto").value = 5;
    }

    if((tipoContactoOnload == 1 && assuntoOnload == 4) || (tipoContactoOnload == 2 && assuntoOnload == 8)){

    }

    jQuery('#assunto').on('change', function() {
        var tipo = document.getElementById("tipoContacto").value;
        var ass = document.getElementById("assunto").value;

        if((tipo == 1 && ass == 1) || (tipo == 1 && ass == 2)){
            hideblocks();
            document.getElementById("anomalia").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'block';
            document.getElementById("dadosPessoais").style.display = 'block';

            if(ass == 1){
                document.getElementById("titleAnomalia").style.display = 'block';
                document.getElementById("titleUpgrade").style.display = 'none';
            } else if(ass == 2){
                document.getElementById("titleAnomalia").style.display = 'none';
                document.getElementById("titleUpgrade").style.display = 'block';
            }
        }

        if((tipo == 1 && ass == 21) || (tipo == 2 && ass == 22)){
            hideblocks();
            document.getElementById("enviarFicheiros").style.display = 'block';
            document.getElementById("dadosPessoais").style.display = 'block';
        }

        if((tipo == 1 && ass == 3) || (tipo == 2 && ass == 9)){
            hideblocks();
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'block';
        }

        if((tipo == 1 && ass == 4) || (tipo == 2 && ass == 8)){
            hideblocks();
            document.getElementById("orcamento").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'block';
            document.getElementById("dadosPessoais").style.display = 'block';

        }

        if((tipo == 1 && ass == 5) || (tipo == 2 && ass == 10) || (tipo == 3 && ass == 16)){
            hideblocks();
            document.getElementById("parcerias").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'block';
            document.getElementById("dadosPessoais").style.display = 'block';
        }

        if((tipo == 1 && ass == 6) || (tipo == 2 && ass == 12)){
            hideblocks();
            document.getElementById("reuniao").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'block';
            document.getElementById("dadosPessoais").style.display = 'block';
        }

        if(tipo == 3 && ass == 15){
            hideblocks();
            document.getElementById("vagaTrabalho").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'none';
        }

        if(tipo == 3 && ass == 14){
            hideblocks();
            document.getElementById("enviarCV").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'block';
        }

        if((tipo == 4 && ass == 17) || (tipo == 4 && ass == 18) || (tipo == 4 && ass == 19)){
            hideblocks();
            document.getElementById("parceriaBloco").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'block';
        }

        if((tipo == 4 && ass == 'Escolher opção') || (tipo == 4 && ass == '') || (tipo == 3 && ass == 'Escolher opção') || (tipo == 3 && ass == '') || (tipo == 2 && ass == 'Escolher opção') || (tipo == 2 && ass == '') || (tipo == 1 && ass == 'Escolher opção') || (tipo == 1 && ass == '')){
            hideblocks();
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'none';
        }

        if((tipo == 4 && ass == 17)){
            document.getElementById("labelProjeto").style.display = 'block';
            document.getElementById("labelBrainstorming").style.display = 'none';
            document.getElementById("labelConferencia").style.display = 'none';
        }

        if((tipo == 4 && ass == 18)){
            document.getElementById("labelProjeto").style.display = 'none';
            document.getElementById("labelBrainstorming").style.display = 'block';
            document.getElementById("labelConferencia").style.display = 'none';
        }

        if((tipo == 4 && ass == 19)){
            document.getElementById("labelProjeto").style.display = 'none';
            document.getElementById("labelBrainstorming").style.display = 'none';
            document.getElementById("labelConferencia").style.display = 'block';
        }

        if((tipo == 2 && ass == 11)){
            hideblocks();
            document.getElementById("outro").style.display = 'none';
            document.getElementById("enviarFicheiros").style.display = 'none';
            document.getElementById("dadosPessoais").style.display = 'block';
        }

        if((tipo == 1 && ass == 7) || (tipo == 2 && ass == 13)){
            hideblocks();
            document.getElementById("outro").style.display = 'block';
            document.getElementById("enviarFicheiros").style.display = 'block';
            document.getElementById("dadosPessoais").style.display = 'block';
        }

    });

    jQuery( "#fechaErro" ).click(function() {
        jQuery('.backgroundErro').css('display','none');
    });

    jQuery( ".openMenuApp" ).click(function() {
        jQuery("#menuApp").css('right','0');
        jQuery("body").css('overflow-y','hidden');
        jQuery("#g-header").css('opacity','0.2');
        jQuery("#g-showcase").css('opacity','0.2');
        jQuery("#g-above").css('opacity','0.2');
        jQuery("#g-copyright").css('opacity','0.2');
        jQuery("#g-mainbar").css('opacity','0.2');
        jQuery("#g-navigation .logo img").css('opacity','0.2');
    });

    jQuery(".closeMenu").click(function(){

        var largura = jQuery(window).width();

        if(largura < 470){
            jQuery("#menuApp").css('right','-65%');
        } else {
            jQuery("#menuApp").css('right','-50%');
        }

        setTimeout(function() {
            jQuery("body").css('overflow-y','scroll');
            jQuery("#g-header").css('opacity','1');
            jQuery("#g-showcase").css('opacity','1');
            jQuery("#g-above").css('opacity','1');
            jQuery("#g-copyright").css('opacity','1');
            jQuery("#g-mainbar").css('opacity','1');
            jQuery("#g-navigation .logo img").css('opacity','1');
        }, 200);
    });

});