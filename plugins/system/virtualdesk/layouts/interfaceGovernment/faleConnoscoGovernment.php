<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    /* Set PHP Headers por causa do CORS - Cross Origin dos pedidos Ajax */
    $obParam              = new VirtualDeskSiteParamsHelper();
    $arHeaderAjaxPHPCORS  = $obParam->getParamsMultiLinhaByTag('AjaxPHPHeaderAccessControlAllowOrigin');
    VirtualDeskSiteGeneralHelper::setHeaderPHPByList($arHeaderAjaxPHPCORS);

    $jinput = JFactory::getApplication()->input;

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteGovernmentFaleConnoscoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/interfaceGovernemt/virtualdesksite_faleConnosco.php');
    JLoader::register('VirtualDeskSiteGovernmentFaleConnoscoFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/interfaceGovernemt/virtualdesksite_FaleConnosco_files.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('faleConnoscoGovernment');
    if ($resPluginEnabled === false) exit();


    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';


    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;
    //$language_tag = $lang->getTag(); // loads the current language-tag
    $jinput = JFactory::getApplication('site')->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        $fileLangSufixV2 ='pt';
        break;
        default:
            $fileLangSufix   = substr($language_tag, 0, 2);
            $fileLangSufixV2 = substr($language_tag, 0, 2);
            break;
    }


    // ESTÁ A RECEBER UM UPLOAD ?
    $isVDAjaxReqFileUpload = $jinput->get('isVDAjaxReqFileUpload');
    if($isVDAjaxReqFileUpload=="1") {

        $alertsFiles =  new VirtualDeskSiteGovernmentFaleConnoscoFilesHelper();
        $alertsFiles->tagprocesso = 'FALECONNOSCOGOVERNMENT_AJAX';
        $alertsFiles->idprocesso = $jinput->get('VDAjaxReqProcRefId');

        $resFileSave = $alertsFiles->saveListFileByAjax('fileupload');

        $erroGravar = '';
        if($resFileSave!==true) $erroGravar = 'Erro ao gravar os ficheiros';

        $results = array(
            'error' => $erroGravar,
            'data' => array()
        );
        // Todo: tratamento de erros ao falhar a gravação de um ficheiro...
        echo json_encode($results);
        exit();
    }

    /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
    $obPlg      = new VirtualDeskSitePluginsHelper();

    if($isVDAjaxReq !=1){
        $layoutPath = $obPlg->getPluginLayoutAtivePath('faleConnoscoGovernment','form');
    } else {
        $layoutPath = $obPlg->getPluginLayoutAtivePath('faleConnoscoGovernment','formajax');
    }

    if((string)$layoutPath=='') {
        // Se não carregar o caminho do layout sai com erro...
        echo (JText::_('COM_VIRTUALDESK_LOADACTIVELAYOUT_ERROR'));
        exit();
    }
    require_once(JPATH_SITE . $layoutPath);
?>