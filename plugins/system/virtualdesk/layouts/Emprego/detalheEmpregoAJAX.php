<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('detalheEmprego');
    if ($resPluginEnabled === false) exit();

    $jinput = JFactory::getApplication()->input;

    $link = $jinput->get('link','' ,'string');
    $ref = explode("_id=", $link);

    $referencia = $ref[1];

    $dadosOferta = VirtualDeskSiteEmpregoHelper::getDadosOferta($referencia);

    foreach($dadosOferta as $rowWSL) :
        $nome_oferta = $rowWSL['nome_oferta'];
        $freguesia = $rowWSL['freguesia'];
        $habilitacoes = $rowWSL['habilitacoes'];
        $formProfissional = $rowWSL['formProfissional'];
        $expAnterior = $rowWSL['expAnterior'];
        $tempo_minimo_exp = $rowWSL['tempo_minimo_exp'];
        $cartaConducao = $rowWSL['cartaConducao'];
        $tipo_carta_conducao = $rowWSL['tipo_carta_conducao'];
        $transp_proprio = $rowWSL['transp_proprio'];
        $tipo_contrato = $rowWSL['tipo_contrato'];
        $reg_horario = $rowWSL['reg_horario'];
        $horas_dia = $rowWSL['horas_dia'];
        $hora_inicio = $rowWSL['hora_inicio'];
        $hora_fim = $rowWSL['hora_fim'];
        $descanso_semanal = $rowWSL['descanso_semanal'];
        $remuneracao = $rowWSL['remuneracao'];
        $subs_alimentacao = $rowWSL['subs_alimentacao'];
        $perfil_candidato = $rowWSL['perfil_candidato'];


        $obParam      = new VirtualDeskSiteParamsHelper();

        $idFreguesia = VirtualDeskSiteEmpregoHelper::getIdFreg($freguesia);
        $bannerGeral = $obParam->getParamsByTag('bannerGeral');
        $bannerFreg1 = $obParam->getParamsByTag('bannerFreg1');
        $bannerFreg2 = $obParam->getParamsByTag('bannerFreg2');
        $bannerFreg3 = $obParam->getParamsByTag('bannerFreg3');


        if($idFreguesia == 10) {
            $imagemOferta = $bannerFreg3;
        } else if ($idFreguesia == 24) {
            $imagemOferta = $bannerFreg2;
        } else if ($idFreguesia == 29) {
            $imagemOferta = $bannerFreg1;
        } else {
            $imagemOferta = $bannerGeral;
        }


        ?>
            <div class="detalhe">
                <div class="headerOferta">

                    <div class="banner">
                        <img src="<?php echo JUri::base() . $imagemOferta; ?>" alt="freg<?php echo $idFreguesia;?>" title=""/>
                    </div>

                    <div class="info">
                        <h2><?php echo $nome_oferta;?></h2>

                        <div class="w50">
                            <div class="freg"><?php echo $freguesia; ?></div>
                            <div class="ref"><span>ID da oferta</span><?php echo $referencia; ?></div>
                        </div>

                        <div class="w50">
                            <a href=""><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_CANDIDATESE'); ?></a>
                            <a href=""><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_ENVIARAMIGO'); ?></a>
                        </div>

                    </div>

                </div>

                <div class="blockCondicoesRequeridas">

                    <h3><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_REQUERIDAS');?></h3>

                    <div class="w50">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_HABESCOLARES');?></h4>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_HABMINIMAS');?></div>
                        <div class="result"><?php echo $habilitacoes; ?></div>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_FORMPROF');?></div>
                        <div class="result"><?php echo $formProfissional; ?></div>

                    </div>

                    <div class="w50">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_OUTROS');?></h4>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_CARTACONDUCAO');?></div>
                        <div class="result"><?php echo $cartaConducao; ?></div>

                        <?php
                            $idcarta = VirtualDeskSiteEmpregoHelper::getIdCarta($cartaConducao);

                            if($idcarta == 1){
                                $tipoCarta = explode(",", $tipo_carta_conducao);

                                for($i = 0; $i < count($tipoCarta); $i++){
                                    if($i == 0) {
                                        $textTipoCarta = VirtualDeskSiteEmpregoHelper::getTipoCarta($tipoCarta[$i]);
                                    } else {
                                        $textTipoCarta .= ', ' . VirtualDeskSiteEmpregoHelper::getTipoCarta($tipoCarta[$i]);
                                    }
                                }
                                ?>

                                <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_TIPOCARTACONDUCAO');?></div>
                                <div class="result"><?php echo $textTipoCarta; ?></div>

                                <?php
                            }
                        ?>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_TRANSPROP');?></div>
                        <div class="result"><?php echo $transp_proprio; ?></div>
                    </div>

                    <div class="w50">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_EXPPROF');?></h4>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_EXPPROF');?></div>
                        <div class="result"><?php echo $expAnterior; ?></div>

                    </div>

                    <?php
                        if(!empty($perfil_candidato)){
                            ?>
                                <div class="w100">
                                    <h4><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_PERFCAND');?></h4>
                                    <div class="result"><?php echo $perfil_candidato; ?></div>
                                </div>
                            <?php
                        }
                    ?>


                </div>

                <div class="blockCondicoesOferecidas">

                    <h3><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_OFERECIDAS');?></h3>

                    <div class="w50">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_CONTTRAB');?></h4>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_TIPOCONTRATO');?></div>
                        <div class="result"><?php echo $tipo_contrato; ?></div>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_REGCONTRATO');?></div>
                        <div class="result"><?php echo $reg_horario; ?></div>

                    </div>

                    <div class="w50">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_HORTRAB');?></h4>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_NUMHORAS');?></div>
                        <div class="result"><?php echo $horas_dia; ?></div>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_HORAINICIO');?></div>
                        <div class="result"><?php echo $hora_inicio; ?></div>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_HORAFIM');?></div>
                        <div class="result"><?php echo $hora_fim; ?></div>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_DESCANSO');?></div>
                        <div class="result"><?php echo $descanso_semanal; ?></div>

                    </div>

                    <div class="w50">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_REMSUBS');?></h4>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_REMBASE');?></div>
                        <div class="result"><?php echo $remuneracao . '€ / ' . JText::_('COM_VIRTUALDESK_EMPREGO_MES') ; ?></div>

                        <div class="topic"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_SUBALIM');?></div>
                        <div class="result"><?php echo $subs_alimentacao . '€ / ' . JText::_('COM_VIRTUALDESK_EMPREGO_DIA');?></div>

                    </div>

                </div>

            </div>

        <?php

    endforeach;

?>