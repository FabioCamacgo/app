<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('novaOferta');
    if ($resPluginEnabled === false) exit();

?>


<form id="novaOfertaForm" action="" method="post" class="login-form" enctype="multipart/form-data" >

    <!--   NIF Empresa  -->
    <div class="form-group" id="nif">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_EMPREGO_FISCALID'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" required placeholder="<?php echo JText::_('COM_VIRTUALDESK_EMPREGO_FISCALID'); ?>"
                   name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
        </div>
    </div>





    <!--   Verificação de Segurança  -->
    <div style="height:75px;">
        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group">
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>
    </div>


    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm2" id="submitForm2" value="Submeter Oferta">
    </div>
</form>
