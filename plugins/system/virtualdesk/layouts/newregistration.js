/*
* Define estrutura da validação.
* A validação é iniciada depois no final deste ficheiro.
* Foi acrescentado antes da inicialização um método ( .addMethod("vdpasscheck"... ) que permite fazer a validação específica da password.
*/
var NewRegistration = function() {

    var handleRegister = function() {

        jQuery('#member-registration').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                hiddenRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },

                password1: {
                     vdpasscheck: ['', 'password1',''],
                     required: true
                     },

                password2: {
                    vdpasscheck: ['', 'password2','password1'],
                    required: true
                },

                email2: {
                    equalTo: "#email1"
                },

                fiscalid: {
                    vdnifcheck: ['', 'fiscalid']
                },
                // o campo de login pode ser NIF, tem outra varíavle que permite retorna a validação true se não for fo tipo NIF
                login: {
                    vdnifcheck: ['', 'login'],
                    required: true
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
                hiddenRecaptcha: {
                    required: jQuery('#hiddenRecaptcha').attr('messageValidation')
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   

                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = MessageAlert.getRequiredMissed(errors);
                        jQuery("#MainMessageAlertBlock").find('span').html(message);
                        jQuery("#MainMessageAlertBlock").show();
                    }
                    // else {
                    //     $("#").hide();
                    // }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.attr("type") === "checkbox") { // insert checkbox errors after the container
                    error.insertAfter(element.closest('div.form-group').find('div.errorLabel'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                // form[0].submit();
                jQuery('#cover-spin').show(0);
                form.submit();
            }
        });

        jQuery('#member-registration').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#member-registration').validate().form()) {
                    jQuery('#member-registration').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {

            handleRegister();
        }

    };

}();

/*
* Inicialização da validação
*/

jQuery(document).ready(function() {

    var altura = jQuery(window).height();

    jQuery('.site.login').css('height',altura);

    jQuery.validator.addMethod("vdpasscheck", function(value, element, params) {
        var password1      = null;
        var password2      = null;
        var resPassCheckJS = null;

        if(params[1]!='') password1 =  jQuery('#'+params[1]);
        if(params[2]!='') password2 =  jQuery('#'+params[2]);

        if( password2 === null )
        {
            resPassCheckJS = jQuery().virtualDeskPasswordCheck(password1.val(),'',virtualDeskPassCheckOptions);
        }
        else
        {
            resPassCheckJS = jQuery().virtualDeskPasswordCheck(password1.val(),password2.val(),virtualDeskPassCheckOptions);
        }

        params[0] = resPassCheckJS.message;
        return resPassCheckJS.return ;
    }, jQuery.validator.format(" {0} "));


    jQuery.validator.addMethod("vdnifcheck", function(value, element, params) {
         // Verifica se o campo atual é o username/login e não for do tipo NIF então sai
        // valida para as restantes situações: o campo é do tipo nif e não é de´login/username ou é login e é do tipo NIF
        if(setUserFieldLoginTypeNIF_JS<1 && params[1]=='login') return true;

        var NIFElem = null;
        if(params[1]!='') NIFElem =  jQuery('#'+params[1]);
        // Se o campo não estiver preenchido não deve dar erro de NIF
        if(NIFElem.val()=='' || NIFElem.val()==undefined) return true;

        var resNIFCheckJS = jQuery().virtualDeskNIFCheck(NIFElem.val(),virtualDeskNIFCheckOptions);
        params[0] = resNIFCheckJS.message;
        return resNIFCheckJS.return ;
    }, jQuery.validator.format(" {0} "));


    NewRegistration.init();


    jQuery(".make-switch").bootstrapSwitch();

});