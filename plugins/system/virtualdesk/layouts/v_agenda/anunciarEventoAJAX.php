<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('anunciarEvento');
    if ($resPluginEnabled === false) exit();

    //LOCAL SCRIPTS
    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/v_agenda/maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/v_agenda/edit_maps.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    // PAGE LEVEL PLUGIN SCRIPTS

    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //FileUploader
    $headScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/v_agenda/anunciarEvento.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    //Fileuploader
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css' . $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        // Load callback first for browser compatibility
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

?>

<style>
    .fileuploader-popup {z-index: 99999 !important;}
    #m_timepicker_3{display:block !important}
    #m_timepicker_2{display:block !important}
</style>


<?php

    $obParam      = new VirtualDeskSiteParamsHelper();

    $linkApp = $obParam->getParamsByTag('linkRegistoApp');

    if (isset($_POST['submitForm'])) {

        $nif = $_POST['nif'];
        $email = $_POST['email'];
        $categoria = $_POST['categoria'];
        $subcategoria = $_POST['subcategoria'];
        $nomeEvento = $_POST['nomeEvento'];
        $descricao = $_POST['descricao'];
        $dataInicio = $_POST['dataInicio'];
        $horaInicio = $_POST['m_timepicker_2'];
        $dataFim = $_POST['dataFim'];
        $horaFim = $_POST['m_timepicker_3'];
        $concelho = $_POST['concelho'];
        $freguesia = $_POST['freguesia'];
        $localEvento = $_POST['localEvento'];
        $coordenadas = $_POST['coordenadas'];
        $splitCoord = explode(",", $coordenadas);
        $lat = $splitCoord[0];
        $long = $splitCoord[1];
        $facebook = $_POST['facebook'];
        $instagram = $_POST['instagram'];
        $youtube = $_POST['youtube'];
        $vimeo = $_POST['vimeo'];
        $precoEvento = $_POST['precoEvento'];
        $dataAtual = date("Y-m-d");
        $horaAtual = date('H:i');
        $layout = $_POST['layout'];

        $dataInicioFormated = VirtualDeskSiteVAgendaHelper::revertDataOrder($dataInicio);
        $dataFimFormated = VirtualDeskSiteVAgendaHelper::revertDataOrder($dataFim);
        $idSelectSubCat = $subcategoria; //VirtualDeskSiteVAgendaHelper::getSubCatSelectID($subcategoria);
        $idSelectFreg = VirtualDeskSiteVAgendaHelper::getFregSelectID($freguesia);

        $validacaoNIF = VirtualDeskSiteVAgendaHelper::validaNif($nif);
        $mailExiste = VirtualDeskSiteVAgendaHelper::seeEmailExiste($email, $nif);
        $numEmail = count($mailExiste);
        $validacaoEmail = VirtualDeskSiteVAgendaHelper::validaEmail($email);
        if(!empty($nif) && !empty($email)){
            $seeNifExiste = VirtualDeskSiteVAgendaHelper::seeNifExiste($nif);
            if(count($seeNifExiste) == 1){
                $getMail = VirtualDeskSiteVAgendaHelper::getEmail($nif);

                if($getMail != $email){
                    $nifMail = 1;
                } else {
                    $nifMail = 0;
                }
            }
        }

        $validacaoCategoria = VirtualDeskSiteVAgendaHelper::validaCategoria($categoria);
        if($categoria != 1) {
            $validacaoSubcategoria = VirtualDeskSiteVAgendaHelper::validaSubcategoria($subcategoria);
        } else {
            $validacaoSubcategoria = 0;
        }
        $validacaoNomeEvento = VirtualDeskSiteVAgendaHelper::validaNomeEvento($nomeEvento);
        $validacaoDataInicio = VirtualDeskSiteVAgendaHelper::validaDataInicio($dataInicioFormated, $dataAtual);
        $validacaoHoraInicio = VirtualDeskSiteVAgendaHelper::validaHoraInicio($horaInicio, $horaAtual, $dataInicioFormated, $dataAtual);
        $validacaoDataFim = VirtualDeskSiteVAgendaHelper::validaDataFim($dataFimFormated, $dataInicioFormated, $dataAtual);
        $validacaoHoraFim = VirtualDeskSiteVAgendaHelper::validaHoraFim($horaFim, $horaInicio, $horaAtual, $dataFimFormated, $dataInicioFormated, $dataAtual);
        $validacaoConcelho = VirtualDeskSiteVAgendaHelper::validaConcelho($concelho);
        $validacaoFreguesia = VirtualDeskSiteVAgendaHelper::validaFreguesia($freguesia);
        $validacaoLocal = VirtualDeskSiteVAgendaHelper::validaLocal($localEvento);
        $validacaoPrecoEvento = VirtualDeskSiteVAgendaHelper::validaPrecoEvento($precoEvento);
        $validacaolayout = VirtualDeskSiteVAgendaHelper::validaFreguesia($layout);


        // Valida recaptcha
        $captcha                  = JCaptcha::getInstance($captcha_plugin);
        $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');
        $jinput->set('g-recaptcha-response',$recaptcha_response_field);
        $resRecaptchaAnswer       = $captcha->checkAnswer($recaptcha_response_field);
        $errRecaptcha = 0;
        if (!$resRecaptchaAnswer) {
            $errRecaptcha = 1;
        }


        if($validacaolayout == 0 && $validacaoNIF == 0 && $validacaoEmail == 0 && $numEmail == 0 && $nifMail == 0 && $validacaoCategoria == 0 && $validacaoSubcategoria == 0 && $validacaoNomeEvento == 0 && $validacaoDataInicio == 0 && $validacaoHoraInicio == 0 && $validacaoDataFim == 0 && $validacaoHoraFim == 0 && $validacaoConcelho == 0 && $validacaoFreguesia == 0 && $validacaoLocal == 0 && $validacaoPrecoEvento == 0 && $errRecaptcha == 0){
            $refRandom = VirtualDeskSiteVAgendaHelper::random_code();

            $refCat = VirtualDeskSiteVAgendaHelper::getRefCat($categoria);
            $splitDataInicio = explode("-", $dataInicio);
            $splitDataFim = explode("-", $dataFim);
            $anoInicio = $splitDataInicio[2];
            $mesInicio = $splitDataInicio[1];
            $anoFim = $splitDataFim[2];
            $mesFim = $splitDataFim[1];

            $getIdFreg = VirtualDeskSiteVAgendaHelper::getFregSelectID($freguesia);

            $seeNifExi = VirtualDeskSiteVAgendaHelper::seeNifExiste($nif);
            if(count($seeNifExi) == 0){
                $obsExiste = 1;
            } else {
                $obsExiste = 2;
            }

            $distrito = 20;

            $referencia = $refCat . $refRandom . $nif . $anoInicio . $mesInicio . $anoFim . $mesFim;
            $resSaveEvento = VirtualDeskSiteVAgendaHelper::saveNewEvento($referencia, $nif, $obsExiste, $categoria, $subcategoria, $nomeEvento, $descricao, $dataInicioFormated, $anoInicio, $mesInicio, $horaInicio, $dataFimFormated, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $getIdFreg, $localEvento, $lat, $long, $facebook, $instagram, $youtube, $vimeo, $precoEvento, $layout, $dataAtual);

            if($resSaveEvento==true){
                ?>
                    <style>
                        #submit{ display:none !important;}
                    </style>

                    <div class="sucesso">
                        <?php
                            if($obsExiste == 1){
                                ?>
                                    <div class="w65">
                                        <p>
                                        <div class="sucessIcon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10" stroke-width="2"/><path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg></div>
                                        <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO1'); ?>
                                        </p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO2'); ?></p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO3'); ?></p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO4') . ' <a href=' . $linkApp . '>aqui</a>.'; ?></p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO5'); ?></p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO6'); ?></p>
                                    </div>

                                    <div class="w35">
                                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PUBLICAR'); ?></h3>
                                        <a href="<?php echo $linkApp; ?>"><div><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_REGISTAR'); ?></div></a>
                                    </div>
                                <?php
                            } else {
                                ?>
                                    <div class="w100">
                                        <p>
                                            <div class="sucessIcon"><svg enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polyline clip-rule="evenodd" fill="none" fill-rule="evenodd" points="  21.2,5.6 11.2,15.2 6.8,10.8 " stroke="#000000" stroke-miterlimit="10" stroke-width="2"/><path d="M19.9,13c-0.5,3.9-3.9,7-7.9,7c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8c1.4,0,2.7,0.4,3.9,1l1.5-1.5C15.8,2.6,14,2,12,2  C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.2,0,9.4-3.9,9.9-9H19.9z"/></svg></div>
                                            <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO1'); ?>
                                        </p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO2'); ?></p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO3'); ?></p>
                                        <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SUCESSO6'); ?></p>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>
                <?php
                echo ('<input type="hidden" id="idReturnedRefOcorrencia" value="__i__' . $referencia . '__e__"/>');
                exit();
            }

        } else {


            ?>
                <script>
                    dropError();
                </script>

                <div class="backgroundErro">
                    <div class="erro" style="display:block;">
                        <button id="fechaErro">
                            <svg style="enable-background:new 0 0 24 24;" version="1.1" viewBox="0 0 24 24" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g/><g><g><path d="M12,10c1.1,0,2-0.9,2-2V4c0-1.1-0.9-2-2-2s-2,0.9-2,2v4C10,9.1,10.9,10,12,10z"/><path d="M19.1,4.9L19.1,4.9c-0.3-0.3-0.6-0.4-1.1-0.4c-0.8,0-1.5,0.7-1.5,1.5c0,0.4,0.2,0.8,0.4,1.1l0,0c0,0,0,0,0,0c0,0,0,0,0,0    c1.3,1.3,2,3,2,4.9c0,3.9-3.1,7-7,7s-7-3.1-7-7c0-1.9,0.8-3.7,2.1-4.9l0,0C7.3,6.8,7.5,6.4,7.5,6c0-0.8-0.7-1.5-1.5-1.5    c-0.4,0-0.8,0.2-1.1,0.4l0,0C3.1,6.7,2,9.2,2,12c0,5.5,4.5,10,10,10s10-4.5,10-10C22,9.2,20.9,6.7,19.1,4.9z"/></g></g></svg>
                        </button>

                        <h2>
                            <svg aria-hidden="true" data-prefix="fas" data-icon="exclamation-triangle" class="svg-inline--fa fa-exclamation-triangle fa-w-18"
                                 role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>
                            <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ERROS'); ?>
                        </h2>

                        <ol>
                            <?php
                                if($validacaoNIF == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_NIF_1') . '</li>';
                                } else if($validacaoNIF == 2) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_NIF_2') . '</li>';
                                } else {
                                    $validacaoNIF = 0;
                                }

                                if($nifMail != 0){
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_NIFMAIL') . '</li>';
                                } else {
                                    $nifMail = 0;
                                }

                                if($validacaoEmail == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_EMAIL_1') . '</li>';
                                } else if($validacaoEmail == 2) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_EMAIL_2') . '</li>';
                                } else {
                                    $validacaoEmail = 0;
                                }

                                if($numEmail != 0){
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_EMAIL_3') . '</li>';
                                } else {
                                    $numEmail = 0;
                                }

                                if($validacaoCategoria == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_CATEGORIA') . '</li>';
                                } else {
                                    $validacaoCategoria = 0;
                                }

                                if($validacaoSubcategoria == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_SUBCATEGORIA') . '</li>';
                                } else {
                                    $validacaoSubcategoria = 0;
                                }

                                if($validacaoNomeEvento == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_NOMEEVENTO') . '</li>';
                                } else {
                                    $validacaoNomeEvento = 0;
                                }

                                if($validacaoDataInicio == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_DATAINICIO_1') . '</li>';
                                } else if($validacaoDataInicio == 2) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_DATAINICIO_2') . '</li>';
                                } else {
                                    $validacaoDataInicio = 0;
                                }

                                if($validacaoHoraInicio == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_HORAINICIO_1') . '</li>';
                                } else if($validacaoHoraInicio == 2) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_HORAINICIO_2') . '</li>';
                                } else {
                                    $validacaoHoraInicio = 0;
                                }

                                if($validacaoDataFim == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_DATAFIM_1') . '</li>';
                                } else if($validacaoDataFim == 2) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_DATAFIM_2') . '</li>';
                                } else {
                                    $validacaoDataFim = 0;
                                }

                                if($validacaoHoraFim == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_HORAFIM_1') . '</li>';
                                } else if($validacaoHoraFim == 2) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_HORAFIM_2') . '</li>';
                                } else {
                                    $validacaoHoraFim = 0;
                                }

                                if($validacaoConcelho == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_CONCELHO') . '</li>';
                                } else {
                                    $validacaoConcelho = 0;
                                }

                                if($validacaoFreguesia == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_FREGUESIA') . '</li>';
                                } else {
                                    $validacaoFreguesia = 0;
                                }

                                if($validacaoLocal == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_LOCAL') . '</li>';
                                } else {
                                    $validacaoLocal = 0;
                                }

                                if($validacaoPrecoEvento == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_ERRO_PRECO') . '</li>';
                                } else {
                                    $validacaoPrecoEvento = 0;
                                }

                                if($validacaolayout == 1) {
                                    echo '<li>Indique o layout da foto de detalhe</li>';
                                } else {
                                    $validacaolayout = 0;
                                }

                                if($errRecaptcha == 1) {
                                    echo '<li>' . JText::_('COM_VIRTUALDESK_AGUA_CONSUMIDOR_CAPTCHA') . '</li>';
                                }else{
                                    $errRecaptcha = 0;
                                }
                            ?>
                        </ol>
                    </div>
                </div>
            <?php
        }

    }
?>

<form id="anunciarEvento" action="" method="post" class="login-form" enctype="multipart/form-data" >

    <!--   NIF Promotor  -->
    <div class="form-group" id="fiscalid">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NIF'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="nif" id="nif" maxlength="9" value="<?php echo $nif; ?>"/>
        </div>
    </div>


    <!--   Email Promotor  -->
    <div class="form-group" id="mail">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMAIL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="email" id="email" maxlength="250" value="<?php echo $email; ?>"/>
        </div>
    </div>

    <!--   Categoria  -->
    <div class="form-group" id="cat">
        <label class="col-md-3 control-label" for="field_categoria"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CATEGORIA') ?><span class="required">*</span></label>
        <?php $Categorias = VirtualDeskSiteVAgendaHelper::getCategoria()?>
        <div class="col-md-9">
            <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($categoria)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Categorias as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['categoria']; ?></option>
                    <?php endforeach;
                } else{ ?>
                    <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getCatSelect($categoria) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeCategoria = VirtualDeskSiteVAgendaHelper::excludeCat($categoria)?>
                    <?php foreach($ExcludeCategoria as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['categoria']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>

    <?php
    // Carrega menusec se o id de menumain estiver definido.
    $ListaDeMenuMain = array();
    if(!empty($this->data->categoria)) {
        if( (int) $this->data->categoria > 0) $ListaDeMenuMain = VirtualDeskSiteVAgendaHelper::getSubCat($this->data->categoria);
    }

    ?>
    <div id="blocoSubCat" class="form-group">
        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_SUBCATEGORIA' ); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <select name="subcategoria" id="subcategoria" value ="<?php echo $subcategoria;?>"
                <?php
                if(!empty($this->data->categoria)) {
                    if ((int)$this->data->categoria <= 0) echo 'disabled';
                }
                else {
                    echo 'disabled';
                }
                ?>
                    class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">

                <?php
                if(empty($subcategoria)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                        <option value="<?php echo $rowMM['id']; ?>"
                            <?php
                            if(!empty($this->data->subcategoria)) {
                                if($this->data->subcategoria == $rowMM['id']) echo 'selected';
                            }
                            ?>
                        ><?php echo $rowMM['name']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $idSelectSubCat; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getSubCatSelect($idSelectSubCat) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeSubCat = VirtualDeskSiteVAgendaHelper::getSubCatMissing($categoria, $idSelectSubCat)?>
                    <?php foreach($ExcludeSubCat as $rowStatus) : ?>
                        <option value="<?php echo $rowStatus['id']; ?>"
                            <?php
                            if(!empty($this->data->categoria)) {
                                if( (int)($rowStatus['id'] == (int) $this->data->idwebsitelist) ) echo 'selected';
                            }
                            ?>
                        ><?php echo $rowStatus['name']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>


    <!--   Nome do evento  -->
    <div class="form-group" id="eventName">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NOMEEVENTO'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="nomeEvento" id="nomeEvento" maxlength="250" value="<?php echo $nomeEvento; ?>"/>
        </div>
    </div>


    <!--  Descrição Evento  -->
    <div class="form-group" id="desc">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DESCRICAO'); ?></label>
        <div class="col-md-9">
            <textarea class="form-control" rows="10"  placeholder="" name="descricao" id="descricao" maxlength="5000"><?php echo $descricao; ?></textarea>
        </div>
    </div>


    <!--   Data de inicio  -->
    <div class="form-group" id="eventStart">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAINICIO'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                <input type="text" readonly name="dataInicio" id="dataInicio" value="<?php echo $dataInicio; ?>">
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>


    <!--   Hora de inicio  -->
    <div class="form-group" id="eventStartHour">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAINICIO'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <div class="input-group timepicker">
                <input class="form-control m-input" name="horaInicio" id="m_timepicker_2" value="<?php echo $horaInicio;?>" type="text" />
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="la la-clock-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--   Data de fim  -->
    <div class="form-group" id="eventEnd">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAFIM'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                <input type="text" readonly name="dataFim" id="dataFim" value="<?php echo $dataFim; ?>">
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>


    <!--   Hora de fim  -->
    <div class="form-group" id="eventEndHour">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAFIM'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <div class="input-group timepicker">
                <input class="form-control m-input" name="horaFim" id="m_timepicker_3" value="<?php echo $horaFim;?>" type="text" />
                <div class="input-group-append">
                    <div class="input-group-text">
                        <i class="la la-clock-o"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--   Concelho  -->
    <div class="form-group" id="conc">
        <label class="col-md-3 control-label" for="field_concelho"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CONCELHO') ?><span class="required">*</span></label>
        <?php $Concelhos = VirtualDeskSiteVAgendaHelper::getConcelho()?>
        <div class="col-md-9">
            <select name="concelho" value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($concelho)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Concelhos as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['concelho']; ?></option>
                    <?php endforeach;
                } else{ ?>
                    <option value="<?php echo $concelho ?>"><?php echo VirtualDeskSiteVAgendaHelper::getConcSelect($concelho) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeConcelho = VirtualDeskSiteVAgendaHelper::excludeConc($concelho)?>
                    <?php foreach($ExcludeConcelho as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['concelho']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>

    <!--   Freguesia  -->
    <?php
    // Carrega menusec se o id de menumain estiver definido.
    $ListaDeMenuMain = array();
    if(!empty($this->data->concelho)) {
        if( (int) $this->data->concelho > 0) $ListaDeMenuMain = VirtualDeskSiteVAgendaHelper::getFreg($this->data->concelho);
    }

    ?>
    <div id="blocoFreg" class="form-group">
        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_FREGUESIA' ); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <select name="freguesia" id="freguesia" value ="<?php echo $freguesia;?>"
                <?php
                if(!empty($this->data->concelho)) {
                    if ((int)$this->data->concelho <= 0) echo 'disabled';
                }
                else {
                    echo 'disabled';
                }
                ?>
                    class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">

                <?php
                if(empty($freguesia)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                        <option value="<?php echo $rowMM['id']; ?>"
                            <?php
                            if(!empty($this->data->freguesia)) {
                                if($this->data->freguesia == $rowMM['id']) echo 'selected';
                            }
                            ?>
                        ><?php echo $rowMM['name']; ?></option>
                    <?php endforeach;
                } else {
                    ?>
                    <option value="<?php echo $idSelectSubCat; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getFregSelect($idSelectFreg) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeFregReq = VirtualDeskSiteVAgendaHelper::getFregMissing($concelho, $idSelectFreg)?>
                    <?php foreach($ExcludeFregReq as $rowStatus) : ?>
                        <option value="<?php echo $rowStatus['id']; ?>"
                            <?php
                            if(!empty($this->data->concelho)) {
                                if( (int)($rowStatus['id'] == (int) $this->data->idwebsitelist) ) echo 'selected';
                            }
                            ?>
                        ><?php echo $rowStatus['name']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>


    <!--  Local do evento  -->
    <div class="form-group" id="eventPlace">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_LOCAL'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="localEvento" id="localEvento" maxlength="250" value="<?php echo $localEvento; ?>"/>
        </div>
    </div>


    <!--   MAPA   -->
    <div class="form-group" id="mapa">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MAPA'); ?></label>
        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

        <input type="hidden" class="form-control" name="coordenadas" id="coordenadas" value="<?php echo htmlentities($this->data->$coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
    </div>

    <!--   Upload Imagem Capa  -->
    <div class="form-group" id="uploadField">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FOTO_CAPA'); ?></label>
        <div class="col-md-9">
            <div class="file-loading">
                <input type="file" name="fileupload[]" id="fileupload" multiple>
            </div>
            <div id="errorBlock" class="help-block"></div>
            <input type="hidden" id="VDAjaxReqProcRefId">
        </div>
    </div>


    <!--   Upload Cartaz  -->
    <div class="form-group" id="uploadFieldCartaz">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CARTAZ'); ?></label>
        <div class="col-md-9">
            <div class="file-loading">
                <input type="file" name="fileuploadCartaz[]" id="fileuploadCartaz" multiple>
            </div>
            <div id="errorBlock" class="help-block"></div>
            <input type="hidden" id="VDAjaxReqProcRefIdCartaz">
        </div>
    </div>


    <!--   Upload Galeria  -->
    <div class="form-group" id="uploadFieldGaleria">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_GALERIA'); ?></label>
        <div class="col-md-9">
            <div class="file-loading">
                <input type="file" name="fileuploadGaleria[]" id="fileuploadGaleria" multiple>
            </div>
            <div id="errorBlock" class="help-block"></div>
            <input type="hidden" id="VDAjaxReqProcRefIdGaleria">
        </div>
    </div>


    <!--  Facebook  -->
    <div class="form-group" id="face">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FACEBOOK'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="facebook" id="facebook" maxlength="200" value="<?php echo $facebook; ?>"/>
        </div>
    </div>


    <!--  Instagram  -->
    <div class="form-group" id="insta">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_INSTAGRAM'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="instagram" id="instagram" maxlength="200" value="<?php echo $instagram; ?>"/>
        </div>
    </div>


    <!--  Youtube  -->
    <div class="form-group" id="yout">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_YOUTUBE'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="youtube" id="youtube" maxlength="200" value="<?php echo $youtube; ?>"/>
        </div>
    </div>


    <!--  Vimeo  -->
    <div class="form-group" id="vim">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VIMEO'); ?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="vimeo" id="vimeo" maxlength="200" value="<?php echo $vimeo; ?>"/>
        </div>
    </div>


    <div class="form-group" id="eventPreco">
        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PRECOEVENTO'); ?><span class="required">*</span></label>
        <div class="col-md-9">
            <input type="radio" name="radioval" id="Gratuito" value="Gratuito" <?php if (isset($_POST["submitForm"]) && $_POST['precoEvento'] == 2) { echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_GRATUITO'); ?>
            <input type="radio" name="radioval" id="Pago" value="Pago" <?php if (isset($_POST["submitForm"]) && $_POST['precoEvento'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PAGO'); ?>
        </div>

        <input type="hidden" id="precoEvento" name="precoEvento" value="<?php echo $precoEvento; ?>">
    </div>

    <script>
        document.getElementById('Pago').onclick = function() {
            document.getElementById("precoEvento").value = 1;
        };

        document.getElementById('Gratuito').onclick = function() {
            document.getElementById("precoEvento").value = 2;
        };
    </script>

    <!--   Layout  -->
    <div class="form-group" id="lay">
        <label class="col-md-3 control-label" for="field_concelho">Layout Foto Detalhe<span class="required">*</span></label>
        <?php $Layouts = VirtualDeskSiteVAgendaHelper::getLayout()?>
        <div class="col-md-9">
            <select name="layout" value="<?php echo $layout; ?>" id="layout" class="form-control select2 select2-nosearch" tabindex="-1" aria-hidden="true">
                <?php
                if(empty($layout)){
                    ?>
                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                    <?php foreach($Layouts as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['layout']; ?></option>
                    <?php endforeach;
                } else{ ?>
                    <option value="<?php echo $layout ?>"><?php echo VirtualDeskSiteVAgendaHelper::getLayoutSelect($layout) ?></option>
                    <option value=""><?php echo '-'; ?></option>
                    <?php $ExcludeLayout = VirtualDeskSiteVAgendaHelper::excludelayout($layout)?>
                    <?php foreach($ExcludeLayout as $rowWSL) : ?>
                        <option value="<?php echo $rowWSL['id']; ?>"
                        ><?php echo $rowWSL['layout']; ?></option>
                    <?php endforeach;
                }
                ?>
            </select>
        </div>
    </div>


    <div style="height:75px;">
        <?php if ($captcha_plugin!='0') : ?>
            <div class="form-group">
                <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'dynamic_recaptcha_1';
                print $captcha->display($field_id, $field_id, 'g-recaptcha');
                ?>
                <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
            </div>
        <?php endif; ?>
    </div>


    <div class="form-actions" method="post" style="display:none;">
        <input type="submit" name="submitForm" id="submitForm" value="Submeter Evento">
    </div>
</form>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;
?>

<script>
    var setVDCurrentRelativePath = '<?php echo JUri::base() . 'anunciarEvento/'; ?>';
    var fileLangSufixV2 = '<?php echo $fileLangSufixV2; ?>';
    var iconpath = '<?php echo JUri::base() . 'images/v_agenda/Pin.png'; ?>';
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';

    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/anunciarEvento.js.php');
    ?>
</script>
