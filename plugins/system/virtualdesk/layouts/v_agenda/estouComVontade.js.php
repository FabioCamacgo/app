<?php
    defined('_JEXEC') or die;
?>

jQuery(document).ready(function() {

    var indexMenu = <?php echo $indexMenu;?>;
    var loadedPage = <?php echo $loadedPage;?>;

    if(loadedPage == 0){
        var ref = jQuery('.barraTemas .tema:nth-child(' + indexMenu + ')').attr("value");
        document.getElementById("refTema").value = ref;
        jQuery('.barraTemas .tema:nth-child(' + indexMenu + ')').addClass("active");
        jQuery('#submit').click();
    }

    jQuery('.tema').on('click', function() {
        var ref = jQuery(this).attr("value");
        document.getElementById("refTema").value = ref;
        var index = jQuery(this).index() + 1;
        document.getElementById("indexMenu").value = index;
        jQuery('#submit').click();
    });

    jQuery('.barraTemas .tema:nth-child(' + indexMenu + ')').addClass("active");

});
