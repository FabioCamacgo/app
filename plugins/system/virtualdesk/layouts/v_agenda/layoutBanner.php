<div class="layoutBanner">

    <div class="w35">
        <div class="dataSheet">
            <div class="box">
                <div class="topic">
                    <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPIC_DETALHES'); ?>
                </div>

                <div class="item">
                    <div class="itemIcon">
                        <?php echo file_get_contents("images/v_agenda/icons/svg/Pin.svg");?>
                    </div>

                    <div class="itemText">
                        <?php
                            if(empty($concelho) && empty($freguesia)){
                                echo '<p> - </p>';
                            } else if(!empty($concelho) && !empty($freguesia)){
                                echo '<p>' . $concelho . ' | ' . $freguesia . '</p>';
                            } else if(!empty($concelho) && empty($freguesia)){
                                echo '<p>' . $concelho . '</p>';
                            } else if(empty($concelho) && !empty($freguesia)){
                                echo '<p>' . $freguesia . '</p>';
                            }

                            if(!empty($local_evento)){
                                echo '<p>' . $local_evento . '</p>';
                            }
                        ?>
                    </div>
                </div>



                <div class="item">
                    <div class="itemIcon">
                        <?php echo file_get_contents("images/v_agenda/icons/categoria/iconcat_" . $id_categoria . ".svg");?>
                    </div>

                    <div class="itemText">
                        <?php
                            if(empty($categoria) && empty($subcategoria)){
                                echo '<p> - </p>';
                            } else if(!empty($categoria) && !empty($subcategoria)){
                                echo '<p>' . $categoria . '</p>';
                                echo '<p>' . $subcategoria . '</p>';
                            } else if(!empty($categoria) && empty($subcategoria)){
                                echo '<p>' . $categoria . '</p>';
                            } else if(empty($categoria) && !empty($subcategoria)){
                                echo '<p>' . $subcategoria . '</p>';
                            }
                        ?>
                    </div>
                </div>



                <div class="item">
                    <div class="itemIcon">
                        <?php echo file_get_contents("images/v_agenda/icons/svg/Diretor.svg");?>
                    </div>

                    <div class="itemText">
                        <?php
                            if(!empty($nome)) {
                                echo '<p>' . $nome . '</p>';
                            } else {
                                echo '<p> - </p>';
                            }
                        ?>
                    </div>
                </div>



                <div class="item">
                    <div class="itemIcon">
                        <?php echo file_get_contents("images/v_agenda/icons/svg/Mail.svg");?>
                    </div>

                    <div class="itemText">
                        <?php
                            if(!empty($email)) {
                                echo '<p>' . $email . '</p>';
                            } else {
                                echo '<p> - </p>';
                            }
                        ?>
                    </div>
                </div>



                <div class="item">
                    <div class="itemIcon">
                        <?php echo file_get_contents("images/v_agenda/icons/svg/face.svg");?>
                    </div>

                    <div class="itemText">
                        <?php
                            if(!empty($facebook)){
                                $facebookLink = VirtualDeskSiteVAgendaHelper::convertLink($facebook);
                                ?>
                                <p><a href="<?php echo $facebookLink;?>" target="_blank"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VER_FACEBOOK');?></a></p>
                                <?php
                            } else {
                                echo '<p> - </p>';
                            }
                        ?>

                    </div>
                </div>



                <div class="item">
                    <div class="itemIcon">
                        <?php echo file_get_contents("images/v_agenda/icons/svg/Insta.svg");?>
                    </div>

                    <div class="itemText">
                        <?php
                            if(!empty($instagram)){
                                $instagramLink = VirtualDeskSiteVAgendaHelper::convertLink($instagram);
                                ?>
                                <p><a href="<?php echo $instagramLink;?>" target="_blank"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VER_INSTAGRAM');?></a></p>
                                <?php
                            } else {
                                echo '<p> - </p>';
                            }
                        ?>

                    </div>
                </div>



                <div class="item">
                    <div class="itemIcon">
                        <?php echo file_get_contents("images/v_agenda/icons/svg/bilhete.svg");?>
                    </div>

                    <div class="itemText">
                        <?php
                            if($tipo_evento == 1){
                                echo '<p>' . JText::_('COM_VIRTUALDESK_VAGENDA_EVENTO_PAGO') . '</p>';
                            } else if($tipo_evento == 2){
                                echo '<p>' . JText::_('COM_VIRTUALDESK_VAGENDA_EVENTO_GRATUITO') . '</p>';
                            } else {
                                echo '<p> - </p>';
                            }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="w65">
        <div class="mainPhoto">
            <?php
            if($top_Evento == 1){
                ?>
                <div class="etiqueta topEvento">
                    <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1'); ?></div>
                    <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2'); ?></div>
                </div>
                <?php
            }

            if($evento_premium == 1){
                ?>
                <div class="etiqueta eventoPremium">
                    <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1'); ?></div>
                    <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2'); ?></div>
                </div>
                <?php
            }

            ?>

            <div class="image">
                <?php
                if((int)$imgCapa > 0){
                    $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                        ?>
                        <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameAlt; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                        <?php
                    }
                } else {
                    ?>
                    <img src="<?php echo $caminhoSemImagem . $idSubcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameAlt; ?>" title="<?php echo $title; ?>"/>
                    <?php
                }
                ?>
            </div>
        </div>

        <?php
            if(!empty($descricao_evento)){
                ?>
                <div class="description">
                    <div class="topic">
                        <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPIC_DESCRICAO'); ?>
                    </div>

                    <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao_evento);?>

                </div>
                <?php
            }
        ?>

        <div class="iconsBar">
            <div class="infoEvent">
                <div class="votes">
                        <span class="icon">
                            <?php echo file_get_contents("images/v_agenda/icons/svg/coracao.svg");?>
                        </span>

                    <span class="text"><?php echo ' (' . $votos . ' ' . JText::_('COM_VIRTUALDESK_VAGENDA_VOTOS') . ')' ;?></span>
                </div>

                <div class="views">
                        <span class="icon">
                            <?php echo file_get_contents("images/v_agenda/icons/svg/Visualizacoes.svg");?>
                        </span>

                    <span class="text"><?php echo ' (' . $newVisualizações . ' ' . JText::_('COM_VIRTUALDESK_VAGENDA_VISUALIZACOES') . ')' ;?></span>
                </div>
            </div>

            <div class="shareEvent">
                <button id="shareFacebook" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SHARE_FACEBOOK'); ?>">
                    <?php echo file_get_contents("images/v_agenda/icons/svg/face.svg");?>
                </button>
                <div class="separator"></div>
                <button id="shareTwitter" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SHARE_TWITTER'); ?>">
                    <?php echo file_get_contents("images/v_agenda/icons/svg/Twiter.svg");?>
                </button>
                <div class="separator"></div>
                <button id="shareWhatsapp" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SHARE_WHATSAPP'); ?>">
                    <?php echo file_get_contents("images/v_agenda/icons/svg/whatsapp.svg");?>
                </button>
                <div class="separator"></div>
                <button id="shareEmail" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SHARE_EMAIL'); ?>">
                    <?php echo file_get_contents("images/v_agenda/icons/svg/Mail.svg");?>
                </button>
            </div>
        </div>

        <?php
            if(count($imgGaleria)>0){
                ?>

                <div class="galeria">
                    <div class="topic">
                        <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPIC_GALERIA'); ?>
                    </div>

                    <?php
                    $objEventFile = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                    $FileList21Html = '';
                    $image = 0;
                    foreach ($arFileList as $rowFile) {
                        $image = $image + 1;
                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                        ?>
                        <div id="galeryImage<?php echo $image;?>" value="<?php echo $image;?>" class="image">
                            <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameAlt . $image; ?>">
                        </div>
                        <?php
                    }
                    ?>

                </div>

                <?php
            }


            if(!empty($videosEvento)){
                $explodeEventos = explode(';;' , $videosEvento);
                ?>

                <div class="multimedia">
                    <div class="topic">
                        <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPIC_MULTIMEDIA'); ?>
                    </div>

                    <?php
                    for($i=0; $i<count($explodeEventos); $i++){
                        ?>
                        <div class="videoItem">
                            <div id="item<?php echo $i;?>" class="overlay" title="<?php echo $titlePlataforma . ' - ' . $nome_evento;?>">
                                <?php echo file_get_contents($botaoPlay);?>
                            </div>
                            <?php
                            if (strpos($explodeEventos[$i], 'youtube') !== false) {

                                $youtubeCode = $obParam->getParamsByTag('youtubeCode');

                                if (strpos($explodeEventos[$i], 'watch?v=') !== false) {
                                    $explodeLink = explode("watch?v=", $explodeEventos[$i]);
                                    $idVideo = explode('&', $explodeLink[1]);
                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }

                                if (strpos($explodeEventos[$i], 'embed') !== false) {
                                    $explodeLink = explode("embed/", $explodeEventos[$i]);
                                    $idVideo = explode('?', $explodeLink[1]);
                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }
                            }

                            if (strpos($explodeEventos[$i], 'youtu.be') !== false) {
                                $explodeLink = explode("youtu.be/", $explodeEventos[$i]);
                                $idVideo = explode('?', $explodeLink[1]);

                                $youtubeCode = $obParam->getParamsByTag('youtubeCode');
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($explodeEventos[$i], 'facebook') !== false){

                                $explodeLink = explode("facebook.com/", $explodeEventos[$i]);
                                $idVideo = $explodeLink[1];
                                $facebookCode = $obParam->getParamsByTag('facebookCode');

                                ?>

                                <div class="video">
                                    <iframe src="<?php echo $facebookCode . urlencode($explodeEventos[$i]);?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($explodeEventos[$i], 'vimeo') !== false){
                                $vimeoCode = $obParam->getParamsByTag('vimeoCode');

                                if (strpos($explodeEventos[$i], 'player.vimeo.com/video/') !== false) {
                                    $explodeLink = explode("player.vimeo.com/video/", $explodeEventos[$i]);

                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }

                                if (strpos($explodeEventos[$i], '/vimeo.com/') !== false) {
                                    $explodeLink = explode("/vimeo.com/", $explodeEventos[$i]);
                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>


                        <div class="seeVideo" id="popupitem<?php echo $i;?>">
                            <div id="closeVideo">
                                <?php echo file_get_contents($botaoFechar);?>
                            </div>
                            <?php
                            if (strpos($explodeEventos[$i], 'youtube') !== false) {

                                $youtubeCode = $obParam->getParamsByTag('youtubeCode');

                                if (strpos($explodeEventos[$i], 'watch?v=') !== false) {
                                    $explodeLink = explode("watch?v=", $explodeEventos[$i]);
                                    $idVideo = explode('&', $explodeLink[1]);
                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }

                                if (strpos($explodeEventos[$i], 'embed') !== false) {
                                    $explodeLink = explode("embed/", $explodeEventos[$i]);
                                    $idVideo = explode('?', $explodeLink[1]);
                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }
                            }

                            if (strpos($explodeEventos[$i], 'youtu.be') !== false) {
                                $explodeLink = explode("youtu.be/", $explodeEventos[$i]);
                                $idVideo = explode('?', $explodeLink[1]);

                                $youtubeCode = $obParam->getParamsByTag('youtubeCode');
                                ?>
                                <div class="video">
                                    <iframe src="<?php echo $youtubeCode . $idVideo[0] . '?rel=0';?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($explodeEventos[$i], 'facebook') !== false){

                                $explodeLink = explode("facebook.com/", $explodeEventos[$i]);
                                $idVideo = $explodeLink[1];
                                $facebookCode = $obParam->getParamsByTag('facebookCode');

                                ?>

                                <div class="video">
                                    <iframe src="<?php echo $facebookCode . urlencode($explodeEventos[$i]);?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <?php
                            }

                            if (strpos($explodeEventos[$i], 'vimeo') !== false){
                                $vimeoCode = $obParam->getParamsByTag('vimeoCode');

                                if (strpos($explodeEventos[$i], 'player.vimeo.com/video/') !== false) {
                                    $explodeLink = explode("player.vimeo.com/video/", $explodeEventos[$i]);

                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }

                                if (strpos($explodeEventos[$i], '/vimeo.com/') !== false) {
                                    $explodeLink = explode("/vimeo.com/", $explodeEventos[$i]);
                                    ?>
                                    <div class="video">
                                        <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            <?php
        }
        ?>

    </div>

</div>