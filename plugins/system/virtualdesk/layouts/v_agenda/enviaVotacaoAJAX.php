<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
    JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('enviaVotacao');
    if ($resPluginEnabled === false) exit();

    $dataAtual = date("Y-m-d");
    $anoAtual = date("Y");
    $mesAtual = date("n");

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');
    $distritoVAgenda = $obParam->getParamsByTag('distritoVagenda');
    $botaoFecharVotacao = $obParam->getParamsByTag('botaoFecharVotacao');
    $listaVotacao = $obParam->getParamsByTag('listaVotacao');

    $linkToken = explode('?token=', $link);

    if(!empty($linkToken[1])){

        $linkDecoded = base64_decode($linkToken[1]);
        $refEventByActivation = explode('&refEvent=', $linkDecoded);
        $nameEventByActivation = explode('&nameEvent=', $refEventByActivation[0]);
        $refvoteByActivation = explode('ref=', $nameEventByActivation[0]);
        $nameEventoURL = str_replace(' ', '_', $nameEventByActivation);

        $refCodeEventByActivation = $refEventByActivation[1];
        $nameCodeEventByActivation = $nameEventByActivation[1];
        $refCodeVoteByActivation = $refvoteByActivation[1];

        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($refCodeEventByActivation);
        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($refCodeEventByActivation);

        VirtualDeskSiteVAgendaHelper::validaVoto($refCodeVoteByActivation);

        $votos = count(VirtualDeskSiteVAgendaHelper::getNumvotos($refCodeEventByActivation));

        VirtualDeskSiteVAgendaHelper::updateNumVotos($refCodeEventByActivation, $votos);

        ?>

        <div class="votacao">

            <div class="closeContent">
                <button id="closeVotacao" value="0"><?php echo file_get_contents($botaoFecharVotacao);?></button>
            </div>


            <div class="imageVotacao">
                <?php
                if((int)$imgCartaz > 0){
                    $imagem = 0;
                    $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefId($refCodeEventByActivation);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        if($imagem == 0){
                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                            ?>
                            <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                            <?php
                            $imagem = 1;
                        }
                    }
                } else if((int)$imgCapa > 0){
                    $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                    $arFileList = $objEventFile->getFileGuestLinkByRefId($refCodeEventByActivation);
                    $FileList21Html = '';
                    foreach ($arFileList as $rowFile) {
                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                        ?>
                        <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                        <?php
                    }
                } else {
                    ?>
                    <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameEventoURL;?>"/>
                    <?php
                }
                ?>
            </div>

            <div class="infoVotacao">

                <h3><?php echo $nameCodeEventByActivation;?></h3>

                <div class="sucesso">
                    <div class="icon"><?php echo file_get_contents('images/v_agenda/icons/svg/success.svg');?></div>
                    <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_SUCESSO_AFTER_ATIVATION'); ?></p>
                    <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_SUCESSO_AFTER_ATIVATION2'); ?></p>
                </div>

            </div>
        </div>

        <?php

    } else {
        $linkRef = explode('?ref=', $link);

        if(empty($linkRef[1])){
            ?>
            <script>
                window.location.href = '<?php echo $listaVotacao;?>';
            </script>
            <?php

        } else {

            $ref = base64_decode($linkRef[1]);
            $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($ref);
            $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($ref);
            $infoEvento = VirtualDeskSiteVAgendaHelper::getInfoEvento($ref);

            foreach($infoEvento as $rowWSL) :
                $nome_evento = $rowWSL['nome_evento'];
                $subcategoria = $rowWSL['subcategoria'];
            endforeach;

            $nameEventoURL = str_replace(' ', '_', $nome_evento);

            ?>

            <div class="votacao">

                <div class="closeContent">
                    <button id="closeVotacao" value="0"><?php echo file_get_contents($botaoFecharVotacao);?></button>
                </div>


                <div class="imageVotacao">
                    <?php
                    if((int)$imgCartaz > 0){
                        $imagem = 0;
                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                        $arFileList = $objEventFile->getFileGuestLinkByRefId($ref);
                        $FileList21Html = '';
                        foreach ($arFileList as $rowFile) {
                            if($imagem == 0){
                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                ?>
                                <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                                <?php
                                $imagem = 1;
                            }
                        }
                    } else if((int)$imgCapa > 0){
                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                        $arFileList = $objEventFile->getFileGuestLinkByRefId($ref);
                        $FileList21Html = '';
                        foreach ($arFileList as $rowFile) {
                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                            ?>
                            <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                            <?php
                        }
                    } else {
                        ?>
                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameEventoURL;?>"/>
                        <?php
                    }
                    ?>
                </div>

                <div class="infoVotacao">

                    <h3><?php echo $nome_evento;?></h3>

                    <?php
                        if (isset($_POST['submitForm'])) {

                            $email = $_POST['email'];

                            //valida Email
                            $errMail = VirtualDeskSiteVAgendaHelper::validaEmailVotacao($email, $ref);

                            if(!empty($VDRecap)){
                                // Valida recaptcha
                                $captcha = JCaptcha::getInstance($captcha_plugin);
                                $recaptcha_response_field = $jinput->get('VDRecap', '', 'string');
                                $jinput->set('g-recaptcha-response', $recaptcha_response_field);
                                $resRecaptchaAnswer = $captcha->checkAnswer($recaptcha_response_field);
                                $errRecaptcha = 0;
                                if (!$resRecaptchaAnswer) {
                                    $errRecaptcha = 1;
                                }
                            } else {
                                $errRecaptcha = 1;
                            }

                            if($errRecaptcha == 0 && $errMail == 0){

                                /*Gerar Referencia UNICA de alerta*/
                                $refExiste = 1;

                                while($refExiste == 1){
                                    $refVotacao = VirtualDeskSiteVAgendaHelper::random_code_Votacao();
                                    $checkREF = VirtualDeskSiteVAgendaHelper::CheckReferenciaVotacao($refVotacao);
                                    if((int)$checkREF == 0){
                                        $refExiste = 0;
                                    }
                                }

                                $resSaveVotacao = VirtualDeskSiteVAgendaHelper::saveNewVotacao($refVotacao, $ref, $email);

                            if($resSaveVotacao == true) :?>
                                <div class="sucesso">
                                    <div class="icon"><?php echo file_get_contents('images/v_agenda/icons/svg/success.svg');?></div>
                                    <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_SUCESSO1'); ?></p>
                                    <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_SUCESSO2'); ?></p>
                                    <p><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_SUCESSO3'); ?></p>
                                </div>

                            <?php

                            $token = base64_encode('ref=' . $refVotacao . '&nameEvent=' . $nome_evento . '&refEvent=' . $ref);

                            VirtualDeskSiteVAgendaHelper::SendEmailVotacao($refVotacao, $email, $nome_evento, $token);

                            endif;

                            } else {
                            ?>
                                <script>
                                    dropError();
                                </script>

                                <div class="backgroundErro">
                                    <div class="erro" style="display:block;">
                                        <button id="fechaErro">
                                            <?php echo file_get_contents('images/v_agenda/icons/svg/close.svg');?>
                                        </button>
                                    </div>

                                    <div class="header">
                                        <?php echo file_get_contents('images/v_agenda/icons/svg/warning.svg');?>

                                        <h4>
                                            <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_AVISO'); ?>
                                        </h4>
                                    </div>

                                    <ol>
                                        <?php
                                            if($errMail == 1){
                                                echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_ERRO_EMAIL_1') . '</li>';
                                            } else if($errMail == 2){
                                                echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_ERRO_EMAIL_2') . '</li>';
                                            } else if($errMail == 3){
                                                echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_ERRO_EMAIL_3') . '</li>';
                                            } else {
                                                $errMail = 0;
                                            }

                                            if($errRecaptcha == 1) {
                                                echo '<li>' . JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_ERRO_CAPTCHA') . '</li>';
                                            }else{
                                                $errRecaptcha = 0;
                                            }
                                        ?>
                                    </ol>
                                </div>

                                <?php
                            }
                        }


                        if($resSaveVotacao != true){
                            ?>

                            <form id="vote" action="" method="post" class="login-form" enctype="multipart/form-data">

                                <div class="form-group" id="mailUser">
                                    <h4><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL');?></h4>
                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="email" id="email" maxlength="250" value="<?php echo $email ?>"/>
                                </div>

                                <div style="height:75px;">
                                    <?php if ($captcha_plugin!='0') : ?>
                                        <div class="form-group" >
                                            <?php $captcha = JCaptcha::getInstance($captcha_plugin);
                                            $field_id = 'dynamic_recaptcha_1';
                                            print $captcha->display($field_id, $field_id, 'g-recaptcha');
                                            ?>
                                            <input type="hidden" class="form-control-feedback" required name="hiddenRecaptcha" id="hiddenRecaptcha" messageValidation="<?php echo JText::_('COM_VIRTUALDESK_USER_RECAPTCHA_CHECKVALID'); ?>">
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div class="form-actions" method="post" style="display:none;">
                                    <input type="submit" name="submitForm" id="submitForm" value="votar">
                                </div>

                                <div class="voteButton"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VOTACAO_BOTAO');?></div>

                            </form>

                            <?php
                        }

                    ?>

                </div>
            </div>

            <?php
        }
    }

    echo $headScripts;
    echo $footerScripts;
    echo $recaptchaScripts;
    echo $localScripts;
?>

<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/enviaVotacao.js.php');
    ?>
</script>


