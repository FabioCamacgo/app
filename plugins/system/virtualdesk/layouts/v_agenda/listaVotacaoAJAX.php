<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaVotacao');
    if ($resPluginEnabled === false) exit();

    $dataAtual = date("Y-m-d");
    $anoAtual = date("Y");
    $mesAtual = date("n");

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $recaptchaScripts = '';
    if ($captcha_plugin != '0') {
        $recaptchaScripts = $addscript_ini . $rooturl . 'media/plg_captcha_recaptcha/js/recaptcha.min.js' . $addscript_end;
        $recaptchaScripts .= $addscript_ini . 'https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=' . $language_tag . $addscript_end;;
    }

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');
    $distritoVAgenda = $obParam->getParamsByTag('distritoVagenda');
    $botaoFecharVotacao = $obParam->getParamsByTag('botaoFecharVotacao');


    if(!empty($pesquisaLivre) || !empty($categoria) || !empty($concelho)){
        $eventos = VirtualDeskSiteVAgendaHelper::getEventosVotacaoFiltered($pesquisaLivre, $categoria, $concelho, $anoPesquisa, $cats);
    } else {
        $eventos = VirtualDeskSiteVAgendaHelper::getEventosVotacao($anoPesquisa, $cats);
    }

    ?>


        <div class="searchBar">
            <div class="searchBarContent">
                <form class="results" action="" method="post" enctype="multipart/form-data" >

                    <div class="form-group" id="search">
                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PESQUISALIVRE');?></h3>
                        <input type="text" class="form-control" autocomplete="off" placeholder="" name="pesquisaLivre" id="pesquisaLivre" maxlength="250" value="<?php echo $pesquisaLivre ?>"/>
                        <div id="find">
                            <?php
                            echo file_get_contents("images/v_agenda/icons/svg/Lupa.svg");
                            ?>
                        </div>
                    </div>


                    <div class="form-group" id="cat">

                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CATEGORIA_VOTACAO'); ?></h3>

                        <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                            <?php
                            $Categorias = VirtualDeskSiteVAgendaHelper::getCategoriaVotacao($cats);
                            if(empty($categoria)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($Categorias as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['categoria']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getCatSelectVotacao($categoria) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php
                                $ExcludeCategoria = VirtualDeskSiteVAgendaHelper::excludeCatVotacao($categoria, $cats);
                                foreach($ExcludeCategoria as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['categoria']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>
                    </div>


                    <div class="form-group" id="conc">

                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CONCELHO_VOTACAO'); ?></h3>

                        <select name="concelho" value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                            <?php
                            $Concelhos = VirtualDeskSiteVAgendaHelper::getConcelho($distritoVAgenda);
                            if(empty($concelho)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($Concelhos as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['concelho']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getConcSelect($distritoVAgenda, $concelho) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php
                                $ExcludeConcelho = VirtualDeskSiteVAgendaHelper::excludeConc($distritoVAgenda, $concelho);
                                foreach($ExcludeConcelho as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['concelho']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>
                    </div>


                    <input id="refClicked" name="refClicked" type="hidden" value="<?php echo $refClicked;?>>">
                    <input id="refClicked" name="refClicked" type="hidden" value="<?php echo $refClicked;?>>">

                </form>
            </div>
        </div>


        <div class="resultsVotacao">

            <?php
                if(count($eventos) == 0){
                    echo '<h3 class="noResults">' . JText::_('COM_VIRTUALDESK_VAGENDA_SEMRESULTADOS') . '</h3>';
                } else {
                    foreach($eventos as $rowWSL) :
                        $referencia = $rowWSL['referencia'];
                        $nomeEvento = $rowWSL['nome_evento'];
                        $subcategoria = $rowWSL['subcategoria'];
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);
                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $nameEventoURL = str_replace(' ', '_', $nomeEvento);

                        ?>

                        <div class="item">
                            <div class="image">
                                <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img value="<?php echo base64_encode($referencia); ?>" src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nomeEvento;?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img value="<?php echo base64_encode($referencia); ?>" src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nomeEvento;?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img value="<?php echo base64_encode($referencia); ?>" src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $title; ?>"/>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>

                    <?php

                    endforeach;
                }
            ?>
        </div>
    <?php
        echo $headScripts;
        echo $footerScripts;
        echo $localScripts;
    ?>

    <script>
        <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/listaVotacao.js.php');
        ?>
    </script>


