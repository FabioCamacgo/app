<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('historicoEventos');
    if ($resPluginEnabled === false) exit();

    $dataAtual = date("Y-m-d");
    $anoAtual = date("Y");
    $mesAtual = date("n");

    $jinput = JFactory::getApplication()->input;
    $anoPesquisa = $jinput->get('anoPesquisa');
    $mesPesquisa = $jinput->get('mesPesquisa');

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');
    $pagListaEventos = $obParam->getParamsByTag('pagListaEventos');

    $anos = VirtualDeskSiteVAgendaHelper::getDistinctYear($anoAtual);
    $meses = VirtualDeskSiteVAgendaHelper::getDistinctMes();

    if(empty($anoPesquisa)){
        $anoPesquisa = $anoAtual;
    }

    if(empty($mesPesquisa)){
        $mesPesquisa = $mesAtual;
    }

    $eventos = VirtualDeskSiteVAgendaHelper::getEventosHistorico($anoPesquisa, $mesPesquisa);

?>

<div class="searchBar">
    <div class="searchBarContent">
        <div class="years">
            <?php
                foreach($anos as $rowWSL) :
                    ?>
                    <button class="year" value="<?php echo $rowWSL['ano_inicio'];?>">
                        <?php echo $rowWSL['ano_inicio'];?>
                    </button>
                    <?php
                endforeach;
            ?>
        </div>

        <div class="months">
            <?php
                foreach($meses as $rowWSL) :
                    ?>
                    <button class="month" value="<?php echo $rowWSL['mes_inicio'];?>">
                        <?php echo VirtualDeskSiteVAgendaHelper::getNameMonth($rowWSL['mes_inicio']);?>
                    </button>
                    <?php
                endforeach;
            ?>
        </div>
    </div>
</div>

<div class="resultsHistoric">
    <form class="results" action="" method="post" enctype="multipart/form-data" >

        <input type="hidden" class="form-control" autocomplete="off" placeholder="" name="mesPesquisa" id="mesPesquisa" value="<?php echo $mesPesquisa; ?>"/>
        <input type="hidden" class="form-control" autocomplete="off" placeholder="" name="anoPesquisa" id="anoPesquisa" value="<?php echo $anoPesquisa; ?>"/>

    </form>

    <?php
        if(count($eventos) == 0){
            echo '<h3 class="noResults">' . JText::_('COM_VIRTUALDESK_VAGENDA_SEMRESULTADOS') . '</h3>';
        } else {
            foreach($eventos as $rowWSL) :
                $referencia = $rowWSL['referencia'];
                $idCategoria = $rowWSL['idCategoria'];
                $categoria = $rowWSL['categoria'];
                $nomeEvento = $rowWSL['nome_evento'];
                $dataInicio = $rowWSL['data_inicio'];
                $dataFim = $rowWSL['data_fim'];
                $idConcelho = $rowWSL['idConcelho'];
                $concelho = $rowWSL['concelho'];
                $freguesia = $rowWSL['freguesia'];
                $idFreguesia = $rowWSL['idFreguesia'];
                $idSubcategoria = $rowWSL['idSubcategoria'];
                $mes_inicio = $rowWSL['mes_inicio'];
                $mes_fim = $rowWSL['mes_fim'];
                $evento_premium = $rowWSL['evento_premium'];
                $top_Evento = $rowWSL['top_Evento'];

                $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);
                $monthInicioName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio);
                $monthFimName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim);
                $showNome = mb_substr($nomeEvento, 0, 30);
                $nameEventoURL = str_replace(' ', '_', $nomeEvento);
                $refEncoded = base64_encode($referencia);
                $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $idCategoria . '&estouvontade=0';
                $refSearchConc = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';
                $refSearchFreg = 'freguesia=' . $idFreguesia . '&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';

                ?>

                <div class="item">
                    <?php
                        if($top_Evento == 1){
                            ?>
                            <div class="etiqueta topEvento">
                                <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                            </div>
                            <?php
                        }

                        if($evento_premium == 1){
                            ?>
                            <div class="etiqueta eventoPremium">
                                <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                            </div>
                            <?php
                        }
                    ?>

                    <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nomeEvento;?>">
                        <div class="image">
                            <?php
                                if((int)$imgCapa > 0){
                                    $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                    $FileList21Html = '';
                                    foreach ($arFileList as $rowFile) {
                                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                        ?>
                                        <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nomeEvento;?>">
                                        <?php
                                    }
                                } else if((int)$imgCartaz > 0){
                                    $imagem = 0;
                                    $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                    $FileList21Html = '';
                                    foreach ($arFileList as $rowFile) {
                                        if($imagem == 0){
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nomeEvento;?>">
                                            <?php
                                            $imagem = 1;
                                        }
                                    }
                                } else {
                                    ?>
                                    <img src="<?php echo $caminhoSemImagem . $idSubcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $title; ?>"/>
                                    <?php
                                }
                            ?>
                        </div>
                    </a>

                    <div class="box">
                        <div class="info">
                            <div class="w30">
                                <?php

                                $splitData = explode("-", $dataInicio);
                                $splitDataFim = explode("-", $dataFim);

                                if($dataInicio == $dataFim || $dataFim == '0000-00-00' || empty($dataFim)) {
                                    ?>
                                    <div class="date">
                                        <div class="day"><?php echo $splitData[2]; ?></div>
                                        <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                        <div class="year"><?php echo $splitData[0]; ?></div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="date">
                                        <div class="day"><?php echo $splitData[2]; ?></div>
                                        <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                        <div class="year"><?php echo $splitData[0]; ?></div>
                                    </div>
                                    <div class="separator"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ATE'); ?></div>
                                    <div class="date">
                                        <div class="day"><?php echo $splitDataFim[2]; ?></div>
                                        <div class="month"><?php echo $monthFimName . '.'; ?></div>
                                        <div class="year"><?php echo $splitDataFim[0]; ?></div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <div class="w70">
                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nomeEvento;?>">
                                    <div class="titleEvent">
                                        <?php echo $showNome;
                                        if(strlen($nomeEvento)>30){
                                            echo '...';
                                        }

                                        ?>
                                    </div>
                                </a>

                                <div class="catEvent">
                                    <div class="icon">
                                        <?php echo file_get_contents('images/v_agenda/icons/svg/etiqueta.svg'); ?>
                                    </div>
                                    <div class="text">
                                        <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $categoria ?></a>
                                    </div>
                                </div>

                                <?php
                                    if(!empty($concelho) || !empty($freguesia)){
                                        ?>
                                        <div class="fregEvent">
                                            <div class="icon">
                                                <?php echo file_get_contents('images/v_agenda/icons/svg/pin2.svg'); ?>
                                            </div>
                                            <div class="text">
                                                <?php
                                                    if(!empty($concelho) && !empty($freguesia)){
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a> | <a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    } else if(!empty($concelho) && empty($freguesia)){
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a>';
                                                    } else if(empty($concelho) && !empty($freguesia)){
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>

                            </div>
                        </div>


                        <div class="more">
                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?> - <?php echo $nomeEvento;?>">
                                <div class="Botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?></div>
                            </a>
                        </div>

                    </div>

                </div>

                <?php
            endforeach;
        }
    ?>

</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/historicoEventos.js.php');
    ?>
</script>
