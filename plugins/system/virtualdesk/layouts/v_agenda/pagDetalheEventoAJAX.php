<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('pagDetalheEvento');
    if ($resPluginEnabled === false) exit();


    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //GLOBAL SCRIPTS

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $botaoPlay = $obParam->getParamsByTag('botaoPlay');
    $botaoFechar = $obParam->getParamsByTag('botaoFechar');
    $newLink = str_replace("%3D","",$link);
    $explodedLink = explode('&ref', $newLink);
    $explodeFaceLink = explode('&fbclid=', $explodedLink[1]);
    $referencia = base64_decode($explodeFaceLink[0]);

    $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
    $imgGaleria = VirtualDeskSiteVAgendaHelper::getImgGaleria($referencia);
    $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);
    $detalheEvento = VirtualDeskSiteVAgendaHelper::getDetalheEvento($referencia);

    foreach($detalheEvento as $rowWSL) :
        $id                 = $rowWSL['id'];
        $id_categoria       = $rowWSL['id_categoria'];
        $categoria          = $rowWSL['categoria'];
        $idSubcategoria     = $rowWSL['idSubcategoria'];
        $subcategoria       = $rowWSL['subcategoria'];
        $nome_evento        = $rowWSL['nome_evento'];
        $data_inicio        = $rowWSL['data_inicio'];
        $data_fim           = $rowWSL['data_fim'];
        $concelho           = $rowWSL['concelho'];
        $freguesia          = $rowWSL['freguesia'];
        $mes_inicio         = $rowWSL['mes_inicio'];
        $mes_fim            = $rowWSL['mes_fim'];
        $nif                = $rowWSL['nif'];
        $descricao_evento   = $rowWSL['descricao_evento'];
        $local_evento       = $rowWSL['local_evento'];
        $latitude           = $rowWSL['latitude'];
        $longitude          = $rowWSL['longitude'];
        $tipo_evento        = $rowWSL['tipo_evento'];
        $facebook           = $rowWSL['facebook'];
        $instagram          = $rowWSL['instagram'];
        $youtube            = $rowWSL['youtube'];
        $vimeo              = $rowWSL['vimeo'];
        $evento_premium     = $rowWSL['evento_premium'];
        $top_Evento         = $rowWSL['top_Evento'];
        $votos              = $rowWSL['votos'];
        $visualizacoes      = $rowWSL['visualizacoes'];
        $videosEvento       = $rowWSL['videosEvento'];
    endforeach;

    $monthInicioName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio);
    $monthFimName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim);
    $nameAlt = str_replace(' ', '_', $nome_evento);
    $infoPromotor = VirtualDeskSiteVAgendaHelper::getInfoPromotor($nif);
    $newVisualizações = $visualizacoes + 1;

    VirtualDeskSiteVAgendaHelper::saveNewVisualizacoes($id, $newVisualizações);

    foreach($infoPromotor as $rowWSL) :
        $nome   = $rowWSL['nome'];
        $email  = $rowWSL['email'];
    endforeach;


    if((int)$isVDAjax2Share===1) {
        $temImagem = 0;
        if((int)$imgCapa > 0){
            $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile) {
                if($temImagem == 0) {
                    $LinkParaAImagem = $rowFile->guestlink;
                    $temImagem = 1;
                }
            }
        } else if((int)$imgCartaz > 0){
            $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile) {
                if($temImagem == 0) {
                    $LinkParaAImagem = $rowFile->guestlink;
                    $temImagem = 1;
                }
            }
        } else {
            ?>
            <img src="<?php echo $caminhoSemImagem . $idSubcategoria . '.jpg';?>" alt="<?php echo $nameAlt; ?>" title="<?php echo $title; ?>"/>
            <?php
        }

        ?>
                <meta property="og:url"                 content="<?php echo $obParam->getParamsByTag('linkDetalheEventoVAgenda') . '?' . $nameAlt . '&ref=' . $explodeFaceLink[0];?>" />
                <meta property="og:type"                content="article" />
                <meta property="og:title"               content="<?php echo $nome_evento;?>" />
                <meta property="og:description"         content="<?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao_evento);?> " />
                <meta property="og:image"               content="<?php echo $LinkParaAImagem; ?>" >
            </head>

            <script type="text/javascript">
                window.location.href = "<?php echo $obParam->getParamsByTag('linkDetalheEventoVAgenda') . '?' . $nameAlt . '&ref=' . $explodeFaceLink[0];?>";
            </script>
        <?php
    } else {
        if((int)$imgGaleria > 0) {
            ?>

            <div class="galeriaEvento">
                <div class="content">

                    <a href="" class="logoGalery">
                        <img src="<?php echo $baseurl;?>images/v_agenda/logo_V_Agenda.png" alt="main_logo"/>
                    </a>

                    <div class="closeGalery">
                        <?php
                        echo file_get_contents("images/v_agenda/icons/svg/close.svg");
                        ?>
                    </div>

                    <div class="sliderMotion">
                        <ul>
                            <?php
                            $objEventFile = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
                            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                            $FileList21Html = '';
                            foreach ($arFileList as $rowFile) {
                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                ?>
                                <li>
                                    <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameAlt; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>

                </div>
            </div>
            <?php

        }

        ?>


        <div class="eventInfo">

            <div class="header">
                <?php
                if($data_inicio == $data_fim || $data_fim == '0000-00-00' || empty($data_fim)) {
                    $splitData = explode("-", $data_inicio);
                    ?>
                    <div class="singleDate">
                        <div class="boxData">
                            <div class="date">
                                <div class="day"><?php echo $splitData[2]; ?></div>
                                <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                <div class="year"><?php echo $splitData[0]; ?></div>
                            </div>
                        </div>

                        <div class="title">
                            <h2><?php echo $nome_evento . ' <div></div>';?></h2>
                        </div>
                    </div>
                    <?php
                } else {
                    $splitData = explode("-", $data_inicio);
                    $splitDataFim = explode("-", $data_fim);
                    ?>
                    <div class="doubleDate">
                        <div class="boxData">
                            <div class="date">
                                <div class="day"><?php echo $splitData[2]; ?></div>
                                <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                <div class="year"><?php echo $splitData[0]; ?></div>
                            </div>
                            <div class="separator"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ATE'); ?></div>
                            <div class="date">
                                <div class="day"><?php echo $splitDataFim[2]; ?></div>
                                <div class="month"><?php echo $monthFimName . '.'; ?></div>
                                <div class="year"><?php echo $splitDataFim[0]; ?></div>
                            </div>
                        </div>

                        <div class="title">
                            <h2><?php echo $nome_evento . ' <div></div>';?></h2>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

            <?php

                if($sizeWindow < 767){
                    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/layoutMobile.php');
                } else if((int)$imgCapa > 0){
                    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/layoutBanner.php');
                } else {
                    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/layoutCartaz.php');
                }
             ?>

            <?php
                echo $headScripts;
                echo $footerScripts;
                echo $localScripts;
            ?>
        </div>

        <script>
            <?php
                require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/pagDetalheEvento.js.php');
            ?>
        </script>

        <?php

    }

?>