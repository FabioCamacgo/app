var MapsGoogle = function () {

    var mapBasic = function () {
        new GMaps({
            div: '#gmap_marker',
            lat: 32.73588409867888,
            lng: -16.98417663574219
        });
    }

    return {
        //main function to initiate map samples
        init: function () {
            mapBasic();
        }

    };

}();

jQuery(document).ready(function() {
    MapsGoogle.init();
});