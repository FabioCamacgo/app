<?php
    defined('_JEXEC') or die;
?>

function removeAtiveYears(){
    jQuery('.searchBarContent .years button').removeClass("active");
}

function removeAtiveMonths(){
    jQuery('.searchBarContent .months button').removeClass("active");
}

function manageMonthAtual(anoAtivo, mes){
    var anoatual = <?php echo $anoAtual;?>;
    var mesatual = <?php echo $mesAtual;?>;

    if(anoAtivo == anoatual){

        jQuery('.searchBarContent .years button:nth-last-child(1)').addClass("active");

        for(var i = 1; i<13; i++){
            if(jQuery('.searchBarContent .months button:nth-child(' + i + ')').val() > mesatual){
                jQuery('.searchBarContent .months button:nth-child(' + i + ')').addClass("inactive");
            }

            if(mes == i){
                jQuery('.searchBarContent .months button:nth-child(' + i + ')').addClass("active");
            }

            if(jQuery('.searchBarContent .months button:nth-child(' + i + ')').hasClass("inactive")){
                jQuery('.searchBarContent .months button:nth-child(' + i + ')').removeClass("active");
            }
        }
        if(mes > mesatual){
            jQuery('.searchBarContent .months button:nth-child(' + mesatual + ')').addClass("active");
        }

    } else {
        jQuery('.searchBarContent .months button').removeClass("inactive");
        jQuery('.searchBarContent .months button:nth-child(' + mes + ')').addClass("active");
    }
}

jQuery( document ).ready(function() {
    var anoatual = <?php echo $anoAtual;?>;
    var mesatual = <?php echo $mesAtual;?>;
    var anoAtivo = <?php echo $anoPesquisa;?>;
    var mesAtivo = <?php echo $mesPesquisa;?>;
    var numAnos = jQuery(".searchBarContent .years > button").size();

    manageMonthAtual(anoAtivo, mesAtivo);

    for(i=1; i<=numAnos; i++){
        if(jQuery('.searchBarContent .years button:nth-child(' + i + ')').val() == anoAtivo){
            jQuery('.searchBarContent .years button:nth-child(' + i + ')').addClass("active");
        }
    }

    jQuery('.searchBarContent .years button').click(function() {
        var value = jQuery(this).val();
        var activeMonth = jQuery('.searchBar .months .month.active').val();
        anoAtivo = value;

        removeAtiveYears();
        removeAtiveMonths();
        manageMonthAtual(anoAtivo, activeMonth);

        jQuery(this).addClass("active");

        document.getElementById("anoPesquisa").value = value;
        jQuery('#submit').click();

    });

    jQuery('.searchBarContent .months button').click(function() {
        if(jQuery(this).hasClass("inactive")){

        } else {
            removeAtiveMonths();
            jQuery(this).addClass("active");

            var valueMonth  = jQuery(this).val();

            document.getElementById("mesPesquisa").value = valueMonth;
            jQuery('#submit').click();
        }

    });

    jQuery('.searchBarContent .months button.inactive').css('cursor','initial');

});