<?php
    defined('_JEXEC') or die;
?>

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        jQuery.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {

    ComponentsSelect2.init();


    jQuery('#categoria').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#estouComVontade').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#concelho').on('change', function() {

        document.getElementById("freguesia").value = 0;

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var concelho = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(concelho == null || concelho == '') {

            var select = jQuery('#freguesia');
            select.find('option').remove();
            select.prop('disabled', true);

        } else {

            var dataString = "concelho="+concelho; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoFreg',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=agenda_getfregPlugin&concelho=' + concelho ,

                success: function(output) {

                    var select = jQuery('#freguesia_evento');
                    select.find('option').remove();

                    if (output == null || output == '') {
                        select.prop('disabled', true);
                        console.log(output);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoFreg');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoFreg');
                }
            });
        }

        jQuery('#submit').click();
    });

    jQuery('#freguesia').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#ano').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#mes').on('change', function() {
        jQuery('#submit').click();
    });

    jQuery('#find').click(function() {
        jQuery('#submit').click();
    });

    jQuery(document).on('keypress',function(e) {
        if(e.which == 13) {
            event.preventDefault();
            jQuery('#submit').click();
        }
    });




});