<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('mes_a_mes');
    if ($resPluginEnabled === false) exit();

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $obParam      = new VirtualDeskSiteParamsHelper();
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $pagListaEventos = $obParam->getParamsByTag('pagListaEventos');

    $dataAtual = date("Y-m-d");

    if(empty($mes) || $mes == 0){
        $mesAtual = date("m");
        if($mesAtual[0] == 0){
            $mes = $mesAtual[1];
        } else {
            $mes = $mesAtual;
        }
    }

    $refSearch = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=' . $mes . '&cat=0&estouvontade=0';
    $eventos =  VirtualDeskSiteVAgendaHelper::getEventosMesAMes($mes);

?>

<form class="mesamesform" action="" method="post" enctype="multipart/form-data" >
    <input id="mes" name="mes" type="hidden" value="<?php echo $mes;?>">
</form>

<div class="monthBar">

    <div class="monthIntro">
        <?php
            if($mes == 1){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JAN_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_JAN');?></div>
                <?php
            } else if($mes == 2){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_FEV_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_FEV');?></div>
                <?php
            } else if($mes == 3){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAR_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_MAR');?></div>
                <?php
            } else if($mes == 4){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_ABR_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_ABR');?></div>
                <?php
            } else if($mes == 5){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAI_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_MAI');?></div>
                <?php
            } else if($mes == 6){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUN_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_JUN');?></div>
                <?php
            } else if($mes == 7){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUL_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_JUL');?></div>
                <?php
            } else if($mes == 8){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_AGO_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_AGO');?></div>
                <?php
            } else if($mes == 9){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_SET_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_SET');?></div>
                <?php
            } else if($mes == 10){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_OUT_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_OUT');?></div>
                <?php
            } else if($mes == 11){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_NOV_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_NOV');?></div>
                <?php
            } else if($mes == 12){
                ?>
                    <h5><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DEZ_NAME');?> <span class="ball"></span></h5>
                    <div class="description"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DESCRIPTION_DEZ');?></div>
                <?php
            }
        ?>
    </div>

    <div class="monthButtons">
        <div class="button month1" value="1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JAN');?></div>
        <div class="button month2" value="2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_FEV');?></div>
        <div class="button month3" value="3"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAR');?></div>
        <div class="button month4" value="4"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_ABR');?></div>
        <div class="button month5" value="5"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAI');?></div>
        <div class="button month6" value="6"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUN');?></div>
        <div class="button month7" value="7"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUL');?></div>
        <div class="button month8" value="8"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_AGO');?></div>
        <div class="button month9" value="9"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_SET');?></div>
        <div class="button month10" value="10"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_OUT');?></div>
        <div class="button month11" value="11"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_NOV');?></div>
        <div class="button month12" value="12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DEZ');?></div>
    </div>
</div>

<div class="monthContent">
    <?php

        if(count($eventos) == 0){
            echo '<h5>' . JText::_('COM_VIRTUALDESK_VAGENDA_SEMRESULTADOS') . '</h5>';
        } else {
            foreach ($eventos as $rowWsl):
                $referencia = $rowWsl['referencia'];
                $nome_evento = $rowWsl['nome_evento'];
                $data_inicio = $rowWsl['data_inicio'];
                $mes_inicio = $rowWsl['mes_inicio'];
                $data_fim = $rowWsl['data_fim'];
                $mes_fim = $rowWsl['mes_fim'];
                $idCategoria = $rowWsl['idCategoria'];
                $categoria = $rowWsl['categoria'];
                $idConcelho = $rowWsl['idConcelho'];
                $concelho = $rowWsl['concelho'];
                $freguesia = $rowWsl['freguesia'];
                $idFreguesia = $rowWsl['idFreguesia'];
                $idSubcategoria = $rowWsl['idSubcategoria'];

                $showNome = mb_substr($nome_evento, 0, 30);
                $nameEventoURL = str_replace(' ', '_', $nome_evento);
                $refEncoded = base64_encode($referencia);

                $monthInicioName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio);
                $monthFimName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim);

                $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $idCategoria . '&estouvontade=0';
                $refSearchConc = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';
                $refSearchFreg = 'freguesia=' . $idFreguesia . '&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';

                ?>

                <div class="item">
                    <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                        <div class="image">
                            <?php
                                if((int)$imgCapa > 0){
                                    $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                    $FileList21Html = '';
                                    foreach ($arFileList as $rowFile) {
                                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                        ?>
                                        <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                                        <?php
                                    }
                                } else if((int)$imgCartaz > 0){
                                    $imagem = 0;
                                    $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                    $FileList21Html = '';
                                    foreach ($arFileList as $rowFile) {
                                        if($imagem == 0){
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                            $imagem = 1;
                                        }
                                    }
                                } else {
                                    ?>
                                    <img src="<?php echo $caminhoSemImagem . $idSubcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>"/>
                                    <?php
                                }
                            ?>
                        </div>
                    </a>

                    <div class="box">
                        <div class="info">
                            <div class="w30">
                                <?php

                                    $splitData = explode("-", $data_inicio);
                                    $splitDataFim = explode("-", $data_fim);

                                    if($data_inicio == $data_fim || $data_fim == '0000-00-00' || empty($data_fim)) {
                                        ?>
                                        <div class="date">
                                            <div class="day"><?php echo $splitData[2]; ?></div>
                                            <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                            <div class="year"><?php echo $splitData[0]; ?></div>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="date">
                                            <div class="day"><?php echo $splitData[2]; ?></div>
                                            <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                            <div class="year"><?php echo $splitData[0]; ?></div>
                                        </div>
                                        <div class="separator"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ATE'); ?></div>
                                        <div class="date">
                                            <div class="day"><?php echo $splitDataFim[2]; ?></div>
                                            <div class="month"><?php echo $monthFimName . '.'; ?></div>
                                            <div class="year"><?php echo $splitDataFim[0]; ?></div>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>

                            <div class="w70">
                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php echo $showNome;
                                        if(strlen($nome_evento)>30){
                                            echo '...';
                                        }

                                        ?>
                                    </div>
                                </a>

                                <div class="catEvent">
                                    <div class="icon">
                                        <?php echo file_get_contents('images/v_agenda/icons/svg/etiqueta.svg'); ?>
                                    </div>
                                    <div class="text">
                                        <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $categoria ?></a>
                                    </div>
                                </div>

                                <?php
                                    if(!empty($concelho) || !empty($freguesia)) {
                                        ?>
                                        <div class="fregEvent">
                                            <div class="icon">
                                                <?php echo file_get_contents('images/v_agenda/icons/svg/pin2.svg'); ?>
                                            </div>
                                            <div class="text">
                                                <?php
                                                    if (!empty($concelho) && !empty($freguesia)) {
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a> | <a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    } else if (!empty($concelho) && empty($freguesia)) {
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a>';
                                                    } else if (empty($concelho) && !empty($freguesia)) {
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>

                            </div>
                        </div>


                        <div class="more">
                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?> - <?php echo $nome_evento;?>">
                                <div class="Botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?></div>
                            </a>
                        </div>

                    </div>
                </div>

            <?php
            endforeach;
            ?>
            <a class="verOutros" href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch);?>"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VERMAISEVENTOS');?></a>
            <?php
        }
    ?>
</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/mes_a_mes.js.php');
    ?>
</script>