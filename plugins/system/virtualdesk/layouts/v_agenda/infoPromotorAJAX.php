<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('infoPromotor');
    if ($resPluginEnabled === false) exit();

    $explodedLink = explode('&ref', $link);
    $referencia = base64_decode($explodedLink[1]);

    $nifPromotor = VirtualDeskSiteVAgendaHelper::getNifPromotor($referencia);
    $eventName = VirtualDeskSiteVAgendaHelper::getEventName($referencia);

    if(!empty($nifPromotor) && $nifPromotor != 0){
        $infoPromotor = VirtualDeskSiteVAgendaHelper::getInfoPromotor($nifPromotor);

        foreach($infoPromotor as $rowWSL) :
            $nome       = $rowWSL['nome'];
            $email      = $rowWSL['email'];
        endforeach;
        ?>

        <div class="info1">
            <div class="w65">
                <h3><?php echo $nome . ' <div></div>';?></h3>
                <div><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PROMOTOREVENTO') . ' "' . $eventName . '"';?></div>
            </div>

            <div class="w35">
                <a href="" target="_blank" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PROMOTOR_VERFACEBOOK');?>">
                    <?php echo file_get_contents("images/v_agenda/icons/svg/face.svg");?>
                </a>

                <a href="" target="_blank" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PROMOTOR_VERTWITTER');?>">
                    <?php echo file_get_contents("images/v_agenda/icons/svg/Twiter.svg");?>
                </a>
            </div>
        </div>

        <a href="<?php echo $email;?>" target="_blank">
            <div class="info2">
                <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ENTRARCONTACTO'); ?>
            </div>
        </a>


        <?php
    } else {
        ?>
            <style>
                .infoPromotor{
                    display:none;
                }
            </style>
        <?php
    }

?>