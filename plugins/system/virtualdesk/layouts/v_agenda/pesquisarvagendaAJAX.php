<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('pesquisarvagenda');
    if ($resPluginEnabled === false) exit();

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;
    //END GLOBAL MANDATORY STYLES

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $pagListaEventos = $obParam->getParamsByTag('pagListaEventos');

    if(!empty($pesqHidden)){
        $refSearch = 'freguesia=0&pesquisaLivre=' . $pesqHidden . '&beginDate=0&concelho=0&mes=0&cat=0&estouvontade=0';
        $linkReencaminha = $pagListaEventos . '?' . base64_encode($refSearch);
        ?>
            <script>
                var reencaminhaLink = '<?php echo $linkReencaminha;?>';
                reencaminha(reencaminhaLink);
            </script>
        <?php
    }
?>

<form class="results" action="" method="post" enctype="multipart/form-data" >

    <div class="form-group" id="searchForm">
        <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PLUGINPESQUISA_PLACEHOLDER');?>" name="pesquisa" id="pesquisa" maxlength="250" value="<?php echo $pesqHidden ?>"/>
        <div id="findPesquisa">
            <?php
                echo file_get_contents("images/v_agenda/icons/svg/Lupa.svg");
            ?>
        </div>

        <input type="hidden" id="pesqHidden" name="pesqHidden" value="<?php echo $pesqHidden;?>">
    </div>

</form>


<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/pesquisarvagenda.js.php');
    ?>
</script>


