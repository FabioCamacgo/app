<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('proximosEventos');
    if ($resPluginEnabled === false) exit();

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //GLOBAL SCRIPTS
    $headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addcss_end;


    //END GLOBAL MANDATORY STYLES


    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $ProximosEventosPubPosition = $obParam->getParamsByTag('ProximosEventosPubPosition');

    $dataAtual = date("Y-m-d");
    $temImagem = 0;
    $numItems = 0;
    $contaItens = 1;
    $totalItemsProximos = $obParam->getParamsByTag('ProximosEventosNumBlocos');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');
    $pagListaEventos = $obParam->getParamsByTag('pagListaEventos');
    $refSearch = 'pesquisaLivre=0&beginDate= ' . $dataAtual . '&concelho=0&mes=0&cat=0&estouvontade=0';

    $procuraProximosEventos = VirtualDeskSiteVAgendaHelper::getProximosEventos('2', '2', $dataAtual, $totalItemsProximos);

?>

<div class="proximos">

    <?php
    if((int)$procuraProximosEventos == 0){
        ?>
        <h5 class="noResults"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PROXIMOSEVENTOS_VAZIO');?></h5>
        <?php
    } else{
        foreach($procuraProximosEventos as $rowWSL) :
            if($numItems < $totalItemsProximos){
                $id = $rowWSL['id'];
                $referencia = $rowWSL['referencia'];
                $idCategoria = $rowWSL['idCategoria'];
                $categoria = $rowWSL['categoria'];
                $idSubcategoria = $rowWSL['idSubcategoria'];
                $nome_evento = $rowWSL['nome_evento'];
                $data_inicio = $rowWSL['data_inicio'];
                $monthInicio = $rowWSL['mes_inicio'];
                $data_fim = $rowWSL['data_fim'];
                $monthFim = $rowWSL['mes_fim'];
                $idConcelho = $rowWSL['idConcelho'];
                $concelho = $rowWSL['concelho'];
                $freguesia = $rowWSL['freguesia'];
                $idFreguesia = $rowWSL['idFreguesia'];

                $showNome = mb_substr($nome_evento, 0, 30);
                $nameEventoURL = str_replace(' ', '_', $nome_evento);
                $refEncoded = base64_encode($referencia);

                $monthInicioName = VirtualDeskSiteVAgendaHelper::getNameMonth($monthInicio);
                $monthFimName = VirtualDeskSiteVAgendaHelper::getNameMonth($monthFim);

                $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $idCategoria . '&estouvontade=0';
                $refSearchConc = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';
                $refSearchFreg = 'freguesia=' . $idFreguesia . '&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';

                if($contaItens == $ProximosEventosPubPosition){
                    ?>
                    <div class="w25 pub">
                        <div class="pubContent">
                            <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ANUNCIEAQUI'); ?>
                        </div>
                    </div>
                    <?php

                    $numItems = $numItems + 1;

                    if($numItems == $totalItemsProximos){
                        exit();
                    }
                }

                ?>
                <div class="w25">
                    <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                        <div class="image">
                            <?php

                            if((int)$imgCapa > 0){
                                $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                $FileList21Html = '';
                                foreach ($arFileList as $rowFile) {
                                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                    ?>
                                    <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                                    <?php
                                }
                            } else if((int)$imgCartaz > 0){
                                $imagem = 0;
                                $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                $FileList21Html = '';
                                foreach ($arFileList as $rowFile) {
                                    if($imagem == 0){
                                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                        ?>
                                        <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>">
                                        <?php
                                        $imagem = 1;
                                    }
                                }
                            } else {
                                ?>
                                <img src="<?php echo $caminhoSemImagem . $idSubcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>"/>
                                <?php
                            }

                            ?>

                        </div>
                    </a>

                    <div class="box">
                        <div class="info">
                            <div class="w30">
                                <?php

                                $splitData = explode("-", $data_inicio);
                                $splitDataFim = explode("-", $data_fim);

                                if($data_inicio == $data_fim || $data_fim == '0000-00-00' || empty($data_fim)) {
                                    ?>
                                    <div class="date">
                                        <div class="day"><?php echo $splitData[2]; ?></div>
                                        <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                        <div class="year"><?php echo $splitData[0]; ?></div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="date">
                                        <div class="day"><?php echo $splitData[2]; ?></div>
                                        <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                        <div class="year"><?php echo $splitData[0]; ?></div>
                                    </div>
                                    <div class="separator"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ATE'); ?></div>
                                    <div class="date">
                                        <div class="day"><?php echo $splitDataFim[2]; ?></div>
                                        <div class="month"><?php echo $monthFimName . '.'; ?></div>
                                        <div class="year"><?php echo $splitDataFim[0]; ?></div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <div class="w70">
                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php echo $showNome;
                                        if(strlen($nome_evento)>30){
                                            echo '...';
                                        }

                                        ?>
                                    </div>
                                </a>

                                <div class="catEvent">
                                    <div class="icon">
                                        <?php echo file_get_contents('images/v_agenda/icons/svg/etiqueta.svg'); ?>
                                    </div>
                                    <div class="text">
                                        <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $categoria ?></a>
                                    </div>
                                </div>

                                <?php
                                    if(!empty($concelho) || !empty($freguesia)) {
                                        ?>
                                        <div class="fregEvent">
                                            <div class="icon">
                                                <?php echo file_get_contents('images/v_agenda/icons/svg/pin2.svg'); ?>
                                            </div>
                                            <div class="text">
                                                <?php
                                                    if (!empty($concelho) && !empty($freguesia)) {
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a> | <a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    } else if (!empty($concelho) && empty($freguesia)) {
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a>';
                                                    } else if (empty($concelho) && !empty($freguesia)) {
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>

                            </div>
                        </div>


                        <div class="more">
                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?> - <?php echo $nome_evento;?>">
                                <div class="Botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?></div>
                            </a>
                        </div>

                    </div>

                </div>
                <?php

                $numItems = $numItems + 1;
                $contaItens = $contaItens + 1;
            } else {
                break;
            }
        endforeach;
        ?>
        <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PROXIMOS_EVENTOS');?></a>
        <?php
    }
    ?>
</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>
