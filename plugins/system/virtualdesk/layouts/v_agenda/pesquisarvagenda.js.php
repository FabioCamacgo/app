<?php
    defined('_JEXEC') or die;

?>

function reencaminha(reencaminhaLink){
    window.location.href = reencaminhaLink;
}

jQuery(document).ready(function() {

    jQuery('#findPesquisa').click(function() {
        var text = jQuery('#pesquisa').val();
        document.getElementById("pesqHidden").value = text;
        jQuery('#submitPesquisar').click();
    });

    jQuery(document).on('keypress',function(e) {
        if(e.which == 13) {
            event.preventDefault();
            var text = jQuery('#pesquisa').val();
            document.getElementById("pesqHidden").value = text;
            jQuery('#submitPesquisar').click();
        }
    });


});