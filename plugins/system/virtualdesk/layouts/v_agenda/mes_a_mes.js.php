<?php
    defined('_JEXEC') or die;
?>

jQuery(document).ready(function() {

    var mesAtual = <?php echo $mes;?>;
    jQuery('.monthButtons .month').removeClass("active");
    jQuery('.monthButtons .month' + mesAtual).addClass("active");

    jQuery('.monthButtons .button').click(function() {
        var ref = jQuery(this).attr("value");
        document.getElementById("mes").value = ref;
        jQuery('.monthButtons .month').removeClass("active");
        jQuery(this).addClass("active");
        jQuery('#submit').click();
    });

});
