<?php

    $pinMapa = $obParam->getParamsByTag('pinMapa');

    if(empty($latitude) || empty($longitude)){
        $lat = 0;
        $long = 0;
    } else {
        $lat = $latitude;
        $long = $longitude;
    }


?>

var iconPath = '<?php echo JUri::base() . $pinMapa; ?>';
var LatGetBD = <?php echo $lat;?>;
var LongGetBD = <?php echo $long;?>;
var LatToSet = 32.70889142068046;
var LongToSet = -17.09796831092251;
var Lat = '';
var Long = '';
var LatPin = '';
var LongPin = '';
var idClicked = '';

if(LatGetBD == 0){
    Lat = LatToSet;
    LatPin = '';
} else {
    Lat = LatGetBD;
    LatPin = LatGetBD;
}

if(LongGetBD == 0){
    Long = LongToSet;
    LongPin = '';
} else {
    Long = LongGetBD;
    LongPin = LatGetBD;
}

var MapsGoogle = function () {
    var mapMarker = function () {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: Lat,
            lng: Long
        });
        map.addMarker({
            lat: LatPin,
            lng: LongPin,
            title: '',
            icon: iconPath
        });
        map.setZoom(12);
    }
    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };
}();

var slideInicio = 1;

(function($) {
    $.fn.fadeImages = function(options) {
        var opt = $.extend({
            time: 2000, //动画间隔时间
            fade: 1000, //淡入淡出的动画时间
            dots: false, //是否启用图片按钮
            arrows: true, //上一张，下一张
            inicio: slideInicio,
            complete: function() {} //淡入完成后的回调函数
        }, options);
        var t = parseInt(opt.time),
            f = parseInt(opt.fade),
            d = opt.dots,
            i = opt.inicio,
            j = 0,
            l, m, set, me, cb = opt.complete,
            a = opt.arrows;
        m = $(this).find("ul li");
        m.hide();
        me = $(this);
        l = m.length;

        // 如果没有一张图片
        if (l <= 0) {
            return false;
        }
        // 如果开启底部按钮
        if (d) {
            $(this).append("<div id='dots'></div>");
            for (j = 0; j < l; j++) {
                $(this).find("#dots").append("<a>" + (j + 1) + "</a>");
            }
            $(this).find("#dots a").eq(0).addClass("active");
            // 底部按钮点击切换
            $(this).on("click", "#dots a", function(event) {
                event.preventDefault();
                clearTimeout(set);
                i = $(this).index();
                var mapID = i + 1;
                dots(i);
                show(i);
            });

        }
        // 如果开启ARROW
        if (a) {
            $(this).append("<a href='#' class='arrow prev'><img src='<?php echo $baseurl;?>images/v_agenda/icons/seta2.png' alt='prev'/></a><a href='#' class='arrow next'><img src='<?php echo $baseurl;?>images/v_agenda/icons/seta3.png' alt='next'/></a>");
            $(this).on("click", ".arrow.prev", function(event) {
                event.preventDefault();
                jQuery('html').css('overflow-y', 'hidden');
                clearTimeout(set);
                i = me.find(".curr").index() - 1;
                if (i <= -1) {
                    i = l - 1;
                }
                dots(i);
                show(i);

            });
            $(this).on("click", ".arrow.next", function(event) {
                event.preventDefault();
                jQuery('html').css('overflow-y', 'hidden');
                clearTimeout(set);
                i = me.find(".curr").index() + 1;
                if (i >= l) {
                    i = 0;
                }
                dots(i);
                show(i);
            });
        }
        // 初始化
        show(0);
        play(0);
        // 图片切换
        function show(i) {
            m.eq(i).siblings().css("z-index", 1).removeClass("curr").stop(true, true).css('display','none');
            m.eq(i).addClass("curr").css("z-index", 2).stop(true, true).fadeIn(f, cb);
        }

        //逗点切换
        function dots(i) {
            me.find("#dots a").eq(i).addClass("active").siblings().removeClass();
        }

        //图片自动播放函数
        function play(i) {
            if (i >= l - 1) {
                i = -1;
            }
            set = setTimeout(function() {
                show(i);
                dots(i);
                play(i)
            }, t + f);
            i++;

            return i;
        }


        //鼠标经过停止与播放
        /*me.hover(function() {
            i = me.find(".curr").index();
            clearTimeout(set);
        }, function() {
            i = me.find(".curr").index();
            play(i);
        });*/

        return this;
    }
}(jQuery));

jQuery( window ).resize(function() {
    var alturaEcraGaleria = jQuery( window ).height();
    var alturaTopGaleria = 220;
    var alturaImageGaleria = alturaEcraGaleria - alturaTopGaleria;
    var positionSeta = alturaImageGaleria / 2;

    jQuery('.galeriaEvento .sliderMotion .arrow.next').css('margin-top', -positionSeta);
    jQuery('.galeriaEvento .sliderMotion .arrow.prev').css('margin-top', -positionSeta);

    jQuery('.galeriaEvento .sliderMotion img').css('max-height',alturaImageGaleria);
});


jQuery(document).ready(function() {
    //MapsGoogle.init();

    jQuery("button#shareFacebook").click(function() {
        jQuery( ".a2a_svg.a2a_s__default.a2a_s_facebook" ).click();
    });

    jQuery("button#shareTwitter").click(function() {
        jQuery( ".a2a_svg.a2a_s__default.a2a_s_twitter" ).click();
    });

    jQuery("button#shareWhatsapp").click(function() {
        jQuery( ".a2a_svg.a2a_s__default.a2a_s_whatsapp" ).click();
    });

    jQuery("button#shareEmail").click(function() {
        jQuery( ".a2a_svg.a2a_s__default.a2a_s_email" ).click();
    });


    jQuery(".videoItem .overlay").on( "click", function() {
        idClicked = jQuery(this).attr( "id" );

        jQuery('#popup' + idClicked).css('display', 'flex');
        jQuery('#popup' + idClicked + ' iframe').addClass("youtube-iframe");
        jQuery('html').css('overflow-y', 'hidden');
        jQuery('#g-container-main').css('z-index','9');
        jQuery('#g-container-main').css('position','fixed');
    });

    jQuery("#closeVideo svg").click(function() {
        jQuery('#popup' + idClicked).css('display', 'none');
        //jQuery('#player' + idClicked).stopVideo();
        jQuery('.youtube-iframe').each(function(index) {
            jQuery(this).attr('src', jQuery(this).attr('src'));
            return false;
        });
        idClicked = '';
        jQuery('html').css('overflow-y', 'scroll');
        jQuery('#g-container-main').css('z-index','1');
        jQuery('#g-container-main').css('position','relative');
    });


    jQuery("#DetalheEvento .galeria .image").click(function() {
        slideInicio = jQuery(this).attr('value')
        jQuery(".galeriaEvento .sliderMotion li").removeClass("curr");
        jQuery(".galeriaEvento .sliderMotion li").css('display','none');
        jQuery(".galeriaEvento .sliderMotion li").css('z-index','0');
        jQuery(".galeriaEvento .sliderMotion li:nth-child(" + slideInicio + ")").addClass("curr");
        jQuery(".galeriaEvento .sliderMotion li:nth-child(" + slideInicio + ")").css('display','list-item');
        jQuery(".galeriaEvento .sliderMotion li:nth-child(" + slideInicio + ")").css('z-index','2');
        var alturaEcraGaleria = jQuery( window ).height();
        var alturaTopGaleria = 220;
        var alturaImageGaleria = alturaEcraGaleria - alturaTopGaleria;
        var positionSeta = alturaImageGaleria / 2;

        jQuery('.galeriaEvento .sliderMotion .arrow.next').css('margin-top', -positionSeta);
        jQuery('.galeriaEvento .sliderMotion .arrow.prev').css('margin-top', -positionSeta);

        jQuery('.galeriaEvento .sliderMotion img').css('max-height',alturaImageGaleria);

        jQuery('html').css('overflow-y', 'hidden');
        jQuery("#g-container-top").css('display','none');
        jQuery(".galeriaEvento").css('display','block');
        jQuery("#g-container-navigation").css('z-index','-9');
    });

    jQuery(".sliderMotion").fadeImages({
        // interval
        time: 10000,
        // animation speed
        fade: 1000,
        inicio: slideInicio,
        // callback
        complete:function() {}
    });

    jQuery(".galeriaEvento .closeGalery").click(function() {
        jQuery(".galeriaEvento").css('display','none');
        jQuery("html").css('overflow-y','scroll');
        jQuery("#g-container-top").css('display','block');
        jQuery("#g-container-navigation").css('z-index','2');
    });


});