<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaCatsSubCats');
    if ($resPluginEnabled === false) exit();

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $obParam      = new VirtualDeskSiteParamsHelper();
    $pagListaEventos = $obParam->getParamsByTag('pagListaEventos');

    $procuraCategorias = VirtualDeskSiteVAgendaHelper::getCategoria();

    foreach($procuraCategorias as $rowWSL) :
        $idCat = $rowWSL['id'];
        $nameCat = $rowWSL['categoria'];

        $procuraSubcategorias = VirtualDeskSiteVAgendaHelper::getSubCat($idCat);

        $refSearch = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $idCat. '&estouvontade=0';

        ?>
            <div class="w20">
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch);?>">
                    <h3>
                        <div class="iconCat">
                            <?php echo file_get_contents("images/v_agenda/icons/categoria/iconcat_" . $idCat . ".svg"); ?>
                        </div>

                        <div class="titleCat">
                            <?php echo $nameCat; ?>
                        </div>
                    </h3>
                </a>

                <!--<ul>
                    <?php
                        foreach($procuraSubcategorias as $rowSL) :
                            $nameSubcat = $rowSL['name'];
                            ?>
                                <li>
                                    <a href="">
                                        <div class="ball"></div>
                                        <div class="text">
                                            <?php echo $nameSubcat; ?>
                                        </div>
                                    </a>

                                </li>
                            <?php
                        endforeach;
                    ?>
                </ul>-->
            </div>
        <?php


    endforeach;

    echo $footerScripts;
    echo $headScripts;

?>