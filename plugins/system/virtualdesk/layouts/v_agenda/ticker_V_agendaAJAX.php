<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('ticker_V_agenda');
    if ($resPluginEnabled === false) exit();

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;

    //GLOBAL SCRIPTS
    //$headScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.min.js' . $addscript_end;
    //$headScripts .= $addscript_ini . $baseurl . 'plugins/system/virtualdesk/layouts/v_agenda/breaking-news-ticker.min.js' . $addscript_end;
    $headScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/js/bootstrap.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/js.cookie.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName. '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
    $headScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/app.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;

    //BEGIN THEME GLOBAL STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/components-rounded.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'plugins/system/virtualdesk/layouts/Noticias/breaking-news-ticker.css' . $addcss_end;
    //END THEME GLOBAL STYLES

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';


    echo $headCSS;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');

    $dataAtual = date("Y-m-d");
    $numItems = 0;
    $contaItens = 1;

    $tickerEventos = VirtualDeskSiteVAgendaHelper::getTickerEventos('2', $dataAtual);

?>

<div class="breaking-news-ticker bn-effect-scroll bn-direction-ltr" id="newsTicker">
    <div class="bn-news">
        <ul>
            <?php
                foreach($tickerEventos as $rowWSL) :
                    $referencia = $rowWSL['referencia'];
                    $idCategoria = $rowWSL['idCategoria'];
                    $idSubcategoria = $rowWSL['idSubcategoria'];
                    $nome_evento = $rowWSL['nome_evento'];
                    $data_inicio = $rowWSL['data_inicio'];
                    $mes_inicio = $rowWSL['mes_inicio'];
                    $data_fim = $rowWSL['data_fim'];
                    $mes_fim = $rowWSL['mes_fim'];

                    $monthInicioName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio);
                    $monthFimName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim);
                    $nameEventoURL = str_replace(' ', '_', $nome_evento);
                    $refEncoded = base64_encode($referencia);

                    ?>

                    <li>
                        <div class="eventData">
                            <?php
                                if($data_inicio == $data_fim || $data_fim == '0000-00-00' || empty($data_fim)) {
                                    $splitData = explode("-", $data_inicio);
                                    ?>
                                    <div class="singleDate">
                                        <div class="boxData">
                                            <div class="date">
                                                <div class="day"><?php echo $splitData[2]; ?></div>
                                                <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    $splitData = explode("-", $data_inicio);
                                    $splitDataFim = explode("-", $data_fim);
                                    ?>
                                    <div class="doubleDate">
                                        <div class="boxData">
                                            <div class="date">
                                                <div class="day"><?php echo $splitData[2]; ?></div>
                                                <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                            </div>
                                            <div class="separator"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ATE'); ?></div>
                                            <div class="date">
                                                <div class="day"><?php echo $splitDataFim[2]; ?></div>
                                                <div class="month"><?php echo $monthFimName . '.'; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            ?>
                        </div>

                        <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>"><?php echo $nome_evento;?></a>
                    </li>

                    <?php
                endforeach;
            ?>
        </ul>
    </div>

    <div class="bn-controls" style="visibility:hidden;">
        <button><span class="bn-arrow bn-prev"></span></button>
        <button><span class="bn-action"></span></button>
        <button><span class="bn-arrow bn-next"></span></button>
    </div>
</div>


<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    var websitepath = '<?php echo JUri::base() . 'onAjaxVD/'; ?>';
    jQuery('#newsTicker').breakingNews();
    <?php
    require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/ticker_V_agenda.js.php');
    ?>
</script>
