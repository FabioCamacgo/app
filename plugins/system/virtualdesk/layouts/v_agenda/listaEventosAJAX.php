<?php
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('listaEventos');
    if ($resPluginEnabled === false) exit();

    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

    //BEGIN GLOBAL MANDATORY STYLES
    $headCSS  = $addcss_ini . '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addcss_end;

    //END GLOBAL MANDATORY STYLES

    //BEGIN PAGE LEVEL PLUGIN STYLES
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2.min.css' . $addcss_end;
    $headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/select2/css/select2-bootstrap.min.css' . $addcss_end;
    //END PAGE LEVEL PLUGIN STYLES

    //BEGIN THEME GLOBAL STYLES
    //$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/global/css/plugins.min.css'. $addcss_end;
    //END THEME GLOBAL STYLES

    // BEGIN PAGE LEVEL STYLES
    //$headCSS .= $addcss_ini . $baseurl . '/templates/' . $templateName . '/assets/pages/css/login.min.css'. $addcss_end;

    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $obParam      = new VirtualDeskSiteParamsHelper();
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $distritoVagenda = $obParam->getParamsByTag('distritoVagenda');
    $pagListaEventos = $obParam->getParamsByTag('pagListaEventos');
    $showTitle = 0;

    if($reloadPage == 0){
        $linkDecoded = base64_decode($link);
        $explodeParam1 = explode('&estouvontade=', $linkDecoded);
        $explodeParam2 = explode('&cat=', $explodeParam1[0]);
        $explodeParam3 = explode('&mes=', $explodeParam2[0]);
        $explodeParam4 = explode('&concelho=', $explodeParam3[0]);
        $explodeParam5 = explode('&beginDate=', $explodeParam4[0]);
        $explodeParam6 = explode('&pesquisaLivre=', $explodeParam5[0]);
        $explodeParam7 = explode('freguesia=', $explodeParam6[0]);

        $estouComVontade = $explodeParam1[1];
        $categoria = $explodeParam2[1];
        $mes = $explodeParam3[1];
        $concelho = $explodeParam4[1];
        $data_inicio = $explodeParam5[1];
        $pesquisaLivre = $explodeParam6[1];
        $freguesia = $explodeParam7[1];


        if(!empty($estouComVontade) && $estouComVontade != 0){
            $filterType = 1;
            $proximos = 1;
            if($pesquisaLivre == 0){
                $pesquisaLivre = '';
            }
        } else if(!empty($categoria) && $categoria != 0){
            $filterType = 2;
            $proximos = 0;
            if($pesquisaLivre == 0){
                $pesquisaLivre = '';
            }
        } else if(!empty($mes) && $mes != 0){
            $filterType = 3;
            $proximos = 0;
            if($pesquisaLivre == 0){
                $pesquisaLivre = '';
            }
        } else if(!empty($concelho) && $concelho != 0){
            $filterType = 4;
            $proximos = 0;
            if($pesquisaLivre == 0){
                $pesquisaLivre = '';
            }
        } else if(!empty($data_inicio) && $data_inicio != 0){
            $filterType = 5;
            $proximos = 1;
            if($pesquisaLivre == 0){
                $pesquisaLivre = '';
            }
        } else if(!empty($pesquisaLivre)){
            $filterType = 6;
            $proximos = 0;
        } else {
            $filterType = 7;
            $proximos = 1;
        }
    } else {
        $estouComVontade = $jinput->get('estouComVontade');
        $categoria = $jinput->get('categoria');
        $mes = $jinput->get('mes');
        $ano = $jinput->get('ano');
        $concelho = $jinput->get('concelho');
        $freguesia = $jinput->get('freguesia');
        $data_inicio = $jinput->get('data_inicio','', 'string');
        $filterType = $jinput->get('filterType');
        $proximos = $jinput->get('proximos');
        $pesquisaLivre = $jinput->get('pesquisaLivre','', 'string');
        $reloadPage = $jinput->get('reloadPage');

        if(empty($pesquisaLivre) && empty($categoria) && empty($concelho) && empty($estouComVontade) && $reloadPage == 1){
            $data_inicio = date("Y-m-d");
            $proximos = 1;
            $filterType = 5;
        }
    }

    echo $headCSS;
    $reloadPage = 1;
    $anoTitle = 0;
    $anoAtual = date("Y");

    if($freguesia == 0) {
        $freguesia = '';
    }

    if($filterType == 1){

        $catsTema = VirtualDeskSiteVAgendaHelper::getCatsTema($estouComVontade);
        $subCatsTema = VirtualDeskSiteVAgendaHelper::getSubCatsTema($estouComVontade);
        $data_inicio = date("Y-m-d");

        if(!empty($catsTema)){
            $cats = explode('|', $catsTema);
            $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListVontadeByCat($cats, $data_inicio, $pesquisaLivre, $concelho, $freguesia, 500);
        } else if(!empty($subCatsTema)){
            $subCats = explode('|', $subCatsTema);
            $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListVontadeBySubCat($subCats, $data_inicio, $pesquisaLivre, $concelho, $freguesia, 500);
        } else {
            $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListVontadeAll($data_inicio, $pesquisaLivre, $concelho, $freguesia, 500);
        }

    } else if($filterType == 2){
        $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListByCategoria($categoria, $concelho, $freguesia, $ano, $pesquisaLivre);
    } else if($filterType == 3){
        $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListByMes($categoria, $concelho, $freguesia, $ano, $mes, $pesquisaLivre);
    } else if($filterType == 4){
        $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListByConcelho($categoria, $ano, $concelho, $freguesia, $pesquisaLivre);
    } else if($filterType == 5){
        $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListByData($data_inicio, $categoria, $concelho, $freguesia, $pesquisaLivre);
    } else if($filterType == 6){
        $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListByPesquisa($categoria, $ano, $concelho, $freguesia, $pesquisaLivre);
    } else {
        $dataAtual = date("Y-m-d");
        $listaEventos = VirtualDeskSiteVAgendaHelper::getEventsListByData($dataAtual, $categoria, $concelho, $freguesia, $pesquisaLivre);
    }
?>

<div class="sideFilter">

    <form class="results" action="" method="post" enctype="multipart/form-data">

        <div class="form-group" id="search">
            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PESQUISALIVRE_LISTA');?></h3>
            <input type="text" class="form-control" autocomplete="off" placeholder="" name="pesquisaLivre" id="pesquisaLivre" maxlength="250" value="<?php echo $pesquisaLivre ?>"/>
            <div id="find">
                <?php
                    echo file_get_contents("images/v_agenda/icons/svg/Lupa.svg");
                ?>
            </div>
        </div>

        <?php
            if($filterType == 2 || $filterType == 3 || $filterType == 4 || $filterType == 5 || $filterType == 6 || $filterType == 7){
                ?>
                <div class="form-group" id="cat">

                    <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CATEGORIA_LISTA'); ?></h3>

                    <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                            $Categorias = VirtualDeskSiteVAgendaHelper::getCategoria();
                            if(empty($categoria)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($Categorias as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['categoria']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getCatSelect($categoria) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php
                                $ExcludeCategoria = VirtualDeskSiteVAgendaHelper::excludeCat($categoria);
                                foreach($ExcludeCategoria as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['categoria']; ?></option>
                                <?php endforeach;
                            }
                        ?>
                    </select>
                </div>
                <?php
            }
        ?>

        <?php
            if($filterType == 1){
                ?>
                <div class="form-group" id="vontade">

                    <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESTOUCOMVONTADE_LISTA'); ?></h3>

                    <select name="estouComVontade" value="<?php echo $estouComVontade; ?>" id="estouComVontade" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                            $estouVontade = VirtualDeskSiteVAgendaHelper::getEstouComVontade();
                            if(empty($estouComVontade)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($estouVontade as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['nome']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $estouComVontade; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getEstouComVontadeSelect($estouComVontade) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php
                                $ExcludeEstouComVontade = VirtualDeskSiteVAgendaHelper::excludeEstouComVontade($estouComVontade);
                                foreach($ExcludeEstouComVontade as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['nome']; ?></option>
                                <?php endforeach;
                            }
                        ?>
                    </select>
                </div>
                <?php
            }
        ?>

        <?php
            if($filterType == 1 ||  $filterType == 2 ||  $filterType == 3 || $filterType == 4 ||  $filterType == 5 || $filterType == 6 || $filterType == 7){
                ?>
                <div class="form-group" id="conc">

                    <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CONCELHO_LISTA'); ?></h3>

                    <select name="concelho" value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                            $Concelhos = VirtualDeskSiteVAgendaHelper::getConcelho($distritoVagenda);
                            if(empty($concelho)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($Concelhos as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['concelho']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getConcSelect($distritoVagenda, $concelho) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php
                                $ExcludeConcelho = VirtualDeskSiteVAgendaHelper::excludeConc($distritoVagenda, $concelho);
                                foreach($ExcludeConcelho as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"><?php echo $rowWSL['concelho']; ?></option>
                                <?php endforeach;
                            }
                        ?>
                    </select>
                </div>


                <?php
                // Carrega menusec se o id de menumain estiver definido.
                $ListaDeMenuMain = array();
                if(!empty($concelho)) {
                    if( (int) $concelho > 0) $ListaDeFregs = VirtualDeskSiteVAgendaHelper::getFregPlugin($concelho);
                }
                ?>
                <div class="form-group" id="blocoFreg">
                    <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FREGUESIA_LISTA') ?></h3>
                    <select name="freguesia" id="freguesia" value="<?php echo $freguesia;?>"
                        <?php
                        if(empty($concelho)) {
                            echo 'disabled';
                        }
                        ?>
                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                        if(empty($freguesia)){
                            ?>
                            <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                            <?php foreach($ListaDeFregs as $rowMM) : ?>
                                <option value="<?php echo $rowMM['id']; ?>"
                                ><?php echo $rowMM['name']; ?></option>
                            <?php endforeach;
                        } else {
                            ?>
                            <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getFregSelect($freguesia);?></option>
                            <option value=""><?php echo '-'; ?></option>
                            <?php $ExcludeFreg = VirtualDeskSiteVAgendaHelper::getFregMissing($concelho, $freguesia);?>
                            <?php foreach($ExcludeFreg as $rowWSL) : ?>
                                <option value="<?php echo $rowWSL['id']; ?>"
                                ><?php echo $rowWSL['name']; ?></option>
                            <?php endforeach;
                        }
                        ?>
                    </select>
                </div>

                <?php
            }
        ?>

        <?php
            if($filterType == 2 || $filterType == 3 || $filterType == 4 || $filterType == 7){
                ?>
                <div class="form-group" id="year">

                    <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ANO_LISTA'); ?></h3>

                    <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                            $Anos = VirtualDeskSiteVAgendaHelper::getAnoList($anoAtual);
                            if(empty($ano)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($Anos as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['ano']; ?>"><?php echo $rowWSL['ano']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $ano; ?>"><?php echo $ano ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php
                                $ExcludeAno = VirtualDeskSiteVAgendaHelper::excludeAnoList($anoAtual, $ano);
                                foreach($ExcludeAno as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['ano']; ?>"><?php echo $rowWSL['ano']; ?></option>
                                <?php endforeach;
                            }
                        ?>
                    </select>
                </div>
                <?php
            }
        ?>

        <?php
            if($filterType == 3){
                ?>
                <div class="form-group" id="month">

                    <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MES_LISTA'); ?></h3>

                    <select name="mes" value="<?php echo $mes; ?>" id="mes" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                        <?php
                            $Meses = VirtualDeskSiteVAgendaHelper::getMesList();
                            if(empty($mes)){
                                ?>
                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($Meses as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['mes']; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getMonthNameList($rowWSL['mes']); ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $mes; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getMonthNameList($mes); ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php
                                $ExcludeMes = VirtualDeskSiteVAgendaHelper::excludeMesList($mes);
                                foreach($ExcludeMes as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['mes']; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getMonthNameList($rowWSL['mes']); ?></option>
                                <?php endforeach;
                            }
                        ?>
                    </select>
                </div>
                <?php
            }
        ?>

        <input type="hidden" id="reloadPage" name="reloadPage" value="<?php echo $reloadPage; ?>">
        <input type="hidden" id="data_inicio" name="data_inicio" value="<?php echo $data_inicio; ?>">
        <input type="hidden" id="filterType" name="filterType" value="<?php echo $filterType; ?>">
        <input type="hidden" id="proximos" name="proximos" value="<?php echo $proximos; ?>">

    </form>

</div>

<div class="contentFilter">
    <?php
        if((int)$listaEventos == 0){
            echo '<h3 class="noResults">' . JText::_('COM_VIRTUALDESK_VAGENDA_PESQUISA_VAZIO') . '</h3>';
        } else {
            foreach ($listaEventos as $rowWSL):
                $referencia = $rowWSL['referencia'];
                $idCategoria = $rowWSL['idCategoria'];
                $categoria = $rowWSL['categoria'];
                $idSubcategoria = $rowWSL['idSubcategoria'];
                $subcategoria = $rowWSL['subcategoria'];
                $nome_evento = $rowWSL['nome_evento'];
                $data_inicio = $rowWSL['data_inicio'];
                $ano_inicio = $rowWSL['ano_inicio'];
                $mes_inicio = $rowWSL['mes_inicio'];
                $data_fim = $rowWSL['data_fim'];
                $mes_fim = $rowWSL['mes_fim'];
                $idConcelho = $rowWSL['idConcelho'];
                $concelho = $rowWSL['concelho'];
                $freguesia = $rowWSL['freguesia'];
                $idFreguesia = $rowWSL['idFreguesia'];
                $evento_premium = $rowWSL['evento_premium'];
                $top_Evento = $rowWSL['top_Evento'];

                $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);
                $monthInicioName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio);
                $monthFimName = VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim);
                $showNome = mb_substr($nome_evento, 0, 30);
                $nameEventoURL = str_replace(' ', '_', $nome_evento);
                $refEncoded = base64_encode($referencia);
                $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $idCategoria . '&estouvontade=0';
                $refSearchConc = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';
                $refSearchFreg = 'freguesia=' . $idFreguesia . '&pesquisaLivre=0&beginDate=0&concelho=' . $idConcelho . '&mes=0&cat=0&estouvontade=0';

                if($proximos != 1){
                    if($anoTitle != $ano_inicio){
                        $anoTitle = $ano_inicio;
                        ?>
                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOS_DE') . ' ' . $anoTitle;?></h3>
                        <?php
                    }
                } else {
                    if($anoTitle != $ano_inicio){
                        $anoTitle = $ano_inicio;
                        if($showTitle == 0) {
                            ?>
                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOS_LISTA');?></h3>
                            <?php
                            $showTitle = 1;
                        }

                    }
                }


                ?>

                <div class="item">
                    <?php
                        if($top_Evento == 1){
                            ?>
                            <div class="etiqueta topEvento">
                                <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                            </div>
                            <?php
                        }

                        if($evento_premium == 1){
                            ?>
                            <div class="etiqueta eventoPremium">
                                <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                            </div>
                            <?php
                        }
                    ?>

                    <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                        <div class="image">
                            <?php
                                if((int)$imgCapa > 0){
                                    $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                    $FileList21Html = '';
                                    foreach ($arFileList as $rowFile) {
                                        $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                        ?>
                                        <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                        <?php
                                    }
                                } else if((int)$imgCartaz > 0){
                                    $imagem = 0;
                                    $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                    $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                    $FileList21Html = '';
                                    foreach ($arFileList as $rowFile) {
                                        if($imagem == 0){
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                            <?php
                                            $imagem = 1;
                                        }
                                    }
                                } else {
                                    ?>
                                    <img src="<?php echo $caminhoSemImagem . $idSubcategoria . '.jpg';?>" loading="lazy" alt="<?php echo $nameEventoURL; ?>" title="<?php echo $title; ?>"/>
                                    <?php
                                }
                            ?>
                        </div>
                    </a>

                    <div class="box">
                        <div class="info">
                            <div class="w30">
                                <?php
                                    $splitData = explode("-", $data_inicio);
                                    $splitDataFim = explode("-", $data_fim);

                                    if($data_inicio == $data_fim || $data_fim == '0000-00-00' || $data_fim == '') {
                                        ?>
                                        <div class="date">
                                            <div class="day"><?php echo $splitData[2]; ?></div>
                                            <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                            <div class="year"><?php echo $splitData[0]; ?></div>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class="date">
                                            <div class="day"><?php echo $splitData[2]; ?></div>
                                            <div class="month"><?php echo $monthInicioName . '.'; ?></div>
                                            <div class="year"><?php echo $splitData[0]; ?></div>
                                        </div>
                                        <div class="separator"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ATE'); ?></div>
                                        <div class="date">
                                            <div class="day"><?php echo $splitDataFim[2]; ?></div>
                                            <div class="month"><?php echo $monthFimName . '.'; ?></div>
                                            <div class="year"><?php echo $splitDataFim[0]; ?></div>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>

                            <div class="w70">
                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="catEvent">
                                    <div class="icon">
                                        <?php echo file_get_contents('images/v_agenda/icons/svg/etiqueta.svg'); ?>
                                    </div>
                                    <div class="text">
                                        <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $categoria ?></a>
                                    </div>
                                </div>

                                <?php
                                    if(!empty($concelho) || !empty($freguesia)){
                                        ?>
                                        <div class="fregEvent">
                                            <div class="icon">
                                                <?php echo file_get_contents('images/v_agenda/icons/svg/pin2.svg'); ?>
                                            </div>
                                            <div class="text">
                                                <?php
                                                    if(!empty($concelho) && !empty($freguesia)){
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a> | <a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    } else if(!empty($concelho) && empty($freguesia)){
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchConc) . '">' . $concelho . '</a>';
                                                    } else if(empty($concelho) && !empty($freguesia)){
                                                        echo '<a href="' . $pagListaEventos . '?' . base64_encode($refSearchFreg) . '">' . $freguesia . '</a>';
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>

                        <div class="more">
                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?> - <?php echo $nome_evento;?>">
                                <div class="Botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_SABERMAIS'); ?></div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
        }
    ?>
</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/listaEventos.js.php');
    ?>
</script>