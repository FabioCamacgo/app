<?php
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    $obParam      = new VirtualDeskSiteParamsHelper();
    $listaVotacao = $obParam->getParamsByTag('listaVotacao');

?>

function dropError(){
    jQuery('.backgroundErro').css('display','block');
}

jQuery(document).ready(function() {

    jQuery("#closeVotacao svg").on( "click", function() {
        window.location.href = '<?php echo $listaVotacao;?>';
    });

    jQuery("#fechaErro").click(function() {
        jQuery('.backgroundErro').css('display','none');
    });

    jQuery('.voteButton').click(function() {
        jQuery('#submit').click();
    });

});