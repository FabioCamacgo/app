<?php

    defined('JPATH_BASE') or die;
    defined('_JEXEC') or die;

    $jinput = JFactory::getApplication()->input;

    $isVDAjaxReq = $jinput->get('isVDAjaxReq');

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
    JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');
    JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
    JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_virtualdesk/models/', 'VirtualDeskModel');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('anunciarEvento');
    if ($resPluginEnabled === false) exit();


    $app             = JFactory::getApplication();
    $doc             = JFactory::getDocument();
    $config          = JFactory::getConfig();
    $sitename        = $config->get('sitename');
    $sitedescription = $config->get('sitedescription');
    $sitetitle       = $config->get('sitetitle');
    $template        = $app->getTemplate(true);
    $templateParams  = $template->params;
    $logoFile        = $templateParams->get('logoFile');
    $fluidContainer  = $config->get('fluidContainer');
    $page_heading    = $config->get('page_heading');
    $baseurl         = JUri::base();
    $rooturl         = JUri::root();
    $captcha_plugin  = JFactory::getConfig()->get('captcha');
    $templateName    = 'virtualdesk';
    $labelseparator = ' : ';
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';

    // Idiomas
    $lang = JFactory::getLanguage();
    $extension = 'com_virtualdesk';
    $base_dir = JPATH_SITE;
    //$language_tag = $lang->getTag(); // loads the current language-tag
    $jinput = JFactory::getApplication('site')->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');
    $reload = true;
    $lang->load($extension, $base_dir, $language_tag, $reload);
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        $fileLangSufixV2 ='pt';
        break;
        default:
            $fileLangSufix   = substr($language_tag, 0, 2);
            $fileLangSufixV2 = substr($language_tag, 0, 2);
            break;
    }


    // ESTÁ A RECEBER UM TESTE por AJAX ?

    $isVDAjaxReqTESTEPost= $jinput->get('isVDAjaxReqTESTEPost');
    if($isVDAjaxReqTESTEPost=="1") {
        $results = array( 'error' => '', 'data' => array()
        );

        echo json_encode($results);
        exit();
    }


    // ESTÁ A RECEBER UM UPLOAD ?
    $isVDAjaxReqFileUpload = $jinput->get('isVDAjaxReqFileUpload');
    if($isVDAjaxReqFileUpload=="1") {

        /*Foto Capa*/
        $alertsFiles =  new VirtualDeskSiteVAgendaCapaFilesHelper();
        $alertsFiles->tagprocesso = 'V_Agenda_Capa';
        $alertsFiles->idprocesso = $jinput->get('VDAjaxReqProcRefId');

        $resFileSave = $alertsFiles->saveListFileByAjax('fileupload');

        $erroGravar = '';
        if($resFileSave!==true) $erroGravar = 'Erro ao gravar os ficheiros';

        $results = array(
            'error' => $erroGravar,
            'data' => array()
        );


        /*Foto Cartaz*/
        $cartazFiles =  new VirtualDeskSiteVAgendaCartazFilesHelper();
        $cartazFiles->tagprocesso = 'V_Agenda_Cartaz';
        $cartazFiles->idprocesso = $jinput->get('VDAjaxReqProcRefId');

        $resFileCartazSave = $cartazFiles->saveListFileByAjax('fileuploadCartaz');

        $erroGravarCartaz = '';

        if($resFileCartazSave!==true) $erroGravarCartaz = 'Erro ao gravar os ficheiros';

        $resultsCartaz = array(
            'error' => $erroGravarCartaz,
            'data' => array()
        );


        /*Foto Galeria*/
        $galeriaFiles =  new VirtualDeskSiteVAgendaGaleriaFilesHelper();
        $galeriaFiles->tagprocesso = 'V_Agenda_Galeria';
        $galeriaFiles->idprocesso = $jinput->get('VDAjaxReqProcRefId');

        $resFileGaleriaSave = $galeriaFiles->saveListFileByAjax('fileuploadGaleria');

        $erroGravarGaleria = '';

        if($resFileGaleriaSave!==true) $erroGravarGaleria = 'Erro ao gravar os ficheiros';

        $resultsGaleria = array(
            'error' => $erroGravarGaleria,
            'data' => array()
        );

        echo json_encode($results);
        echo json_encode($resultsCartaz);
        echo json_encode($resultsGaleria);
        exit();
    }



    /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
    $obPlg      = new VirtualDeskSitePluginsHelper();

    if($isVDAjaxReq !=1){
        $layoutPath = $obPlg->getPluginLayoutAtivePath('anunciarEvento','form');
    } else {
        $layoutPath = $obPlg->getPluginLayoutAtivePath('anunciarEvento','formajax');
    }

    if((string)$layoutPath=='') {
        // Se não carregar o caminho do layout sai com erro...
        echo (JText::_('COM_VIRTUALDESK_LOADACTIVELAYOUT_ERROR'));
        exit();
    }
    require_once(JPATH_SITE . $layoutPath);
?>