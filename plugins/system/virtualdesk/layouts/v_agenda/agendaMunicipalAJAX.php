<?php

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /* Check if Plugin is Enabled ? */
    $obCheckPlgEnable = new VirtualDeskSitePluginsHelper();
    $resPluginEnabled = $obCheckPlgEnable->checkPluginIsEnabled('agendaMunicipal');
    if ($resPluginEnabled === false) exit();


    // CUSTOM JS DASHboard
    $footerScripts  = '<!--[if lt IE 9]>';
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/respond.min.js'  . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/excanvas.min.js' . $addscript_end;
    $footerScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ie8.fix.min.js' . $addscript_end;
    $footerScripts .= '<![endif]-->';

    $obParam      = new VirtualDeskSiteParamsHelper();
    $linkDetalheEventoVAgenda = $obParam->getParamsByTag('linkDetalheEventoVAgenda');
    $titlePlataforma = $obParam->getParamsByTag('nomePlataformaV_Agenda');
    $limitAgendaMunicipal = $obParam->getParamsByTag('limitAgendaMunicipal');
    $caminhoSemImagem = $obParam->getParamsByTag('caminhoSemImagem');
    $pagListaEventos = $obParam->getParamsByTag('pagListaEventos');

    $dataAtual = date("Y-m-d");

?>

<div class="map">
    <?php echo file_get_contents('images/v_agenda/Mapas/SVG/Madeira_Concelhos.svg');?>
</div>

<div class="sliderMotion agendaPremium">
    <ul>
        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/funchal.png" alt="logo_funchal"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho1 = 105;
                    $refSearch1 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho1 . '&mes=0&cat=0&estouvontade=0';
                    $agendaFunchal = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho1, $limitAgendaMunicipal);

                    foreach ($agendaFunchal as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>

                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch1);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>

        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/camaradelobos.png" alt="logo_camaradelobos"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho2 = 59;
                    $refSearch2 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho2 . '&mes=0&cat=0&estouvontade=0';
                    $agendaCamLobos = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho2, $limitAgendaMunicipal);

                    foreach ($agendaCamLobos as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch2);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>

        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/ribeirabrava.png" alt="logo_ribeirabrava"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho3 = 222;
                    $refSearch3 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho3 . '&mes=0&cat=0&estouvontade=0';
                    $agendaRibBrava = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho3, $limitAgendaMunicipal);

                    foreach ($agendaRibBrava as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch3);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/pontadosol.png" alt="logo_pontadosol"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho4 = 203;
                    $refSearch4 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho4 . '&mes=0&cat=0&estouvontade=0';
                    $agendaPontaSol = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho4, $limitAgendaMunicipal);

                    foreach ($agendaPontaSol as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch4);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/calheta.png" alt="logo_calheta"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho5 = 57;
                    $refSearch5 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho5 . '&mes=0&cat=0&estouvontade=0';
                    $agendaCalheta = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho5, $limitAgendaMunicipal);

                    foreach ($agendaCalheta as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch5);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/portomoniz.png" alt="logo_portomoniz"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho6 = 212;
                    $refSearch6 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho6 . '&mes=0&cat=0&estouvontade=0';
                    $agendaPortoMoniz = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho6, $limitAgendaMunicipal);

                    foreach ($agendaPortoMoniz as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch6);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/saovicente.png" alt="logo_saovicente"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho7 = 244;
                    $refSearch7 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho7 . '&mes=0&cat=0&estouvontade=0';
                    $agendaSaoVicente = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho7, $limitAgendaMunicipal);

                    foreach ($agendaSaoVicente as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch7);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/santana.png" alt="logo_santana"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho8 = 235;
                    $refSearch8 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho8 . '&mes=0&cat=0&estouvontade=0';
                    $agendaSantana = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho8, $limitAgendaMunicipal);

                    foreach ($agendaSantana as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch8);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/machico.png" alt="logo_machico"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho9 = 133;
                    $refSearch9 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho9 . '&mes=0&cat=0&estouvontade=0';
                    $agendaMachico = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho9, $limitAgendaMunicipal);

                    foreach ($agendaMachico as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch9);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/santacruz.png" alt="logo_santacruz"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho10 = 230;
                    $refSearch10 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho10 . '&mes=0&cat=0&estouvontade=0';
                    $agendaSantaCruz = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho10, $limitAgendaMunicipal);

                    foreach ($agendaSantaCruz as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch10);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>


        <li>
            <div class="logo">
                <img src="<?php echo $baseurl;?>images/v_agenda/agendaMunicipal/portosanto.png" alt="logo_portosanto"/>
            </div>

            <div class="contentBackground">
                <div class="contentEtiqueta">
                    <img src="<?php echo $baseurl;?>images/v_agenda/Etiqueta_Premium.png" alt="etiqueta_premium"/>
                </div>
            </div>
            <div class="content">
                <?php
                    $concelho11 = 213;
                    $refSearch11 = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=' . $concelho11 . '&mes=0&cat=0&estouvontade=0';
                    $agendaPortoSanto = VirtualDeskSiteVAgendaHelper::getAgendaEvents($concelho11, $limitAgendaMunicipal);

                    foreach ($agendaPortoSanto as $rowWsl):
                        $id = $rowWsl['id'];
                        $referencia = $rowWsl['referencia'];
                        $nome_evento = $rowWsl['nome_evento'];
                        $categoria = $rowWsl['categoria'];
                        $catName = $rowWsl['CatName'];
                        $subcategoria = $rowWsl['subcategoria'];
                        $subcatName = $rowWsl['subcatName'];
                        $mes_inicio = $rowWsl['mes_inicio'];
                        $mes_fim = $rowWsl['mes_fim'];
                        $evento_premium = $rowWsl['evento_premium'];
                        $top_Evento = $rowWsl['top_Evento'];

                        $showNome = mb_substr($nome_evento, 0, 30);
                        $nameEventoURL = str_replace(' ', '_', $nome_evento);
                        $refEncoded = base64_encode($referencia);
                        $refSearchCat = 'freguesia=0&pesquisaLivre=0&beginDate=0&concelho=0&mes=0&cat=' . $categoria . '&estouvontade=0';

                        $imgCapa = VirtualDeskSiteVAgendaHelper::getImgCapa($referencia);
                        $imgCartaz = VirtualDeskSiteVAgendaHelper::getImgCartaz($referencia);

                        ?>
                        <div class="item">

                            <?php
                                if($evento_premium == 1){
                                    ?>
                                    <div class="etiqueta eventoPremium">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EVENTOPREMIUM2');?></div>
                                    </div>
                                    <?php
                                }

                                if($top_Evento == 1){
                                    ?>
                                    <div class="etiqueta topEvento">
                                        <div class="word1"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO1');?></div>
                                        <div class="word2"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TOPEVENTO2');?></div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                <div class="image">
                                    <?php
                                    if((int)$imgCartaz > 0){
                                        $imagem = 0;
                                        $objEventFile = new VirtualDeskSiteVAgendaCartazFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            if($imagem == 0){
                                                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                                ?>
                                                <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                                <?php
                                                $imagem = 1;
                                            }
                                        }
                                    } else if((int)$imgCapa > 0){
                                        $objEventFile = new VirtualDeskSiteVAgendaCapaFilesHelper();
                                        $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                                        $FileList21Html = '';
                                        foreach ($arFileList as $rowFile) {
                                            $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                                            ?>
                                            <img src="<?php echo $rowFile->guestlink; ?>" alt="<?php echo $nameEventoURL; ?>">
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <img src="<?php echo $caminhoSemImagem . $subcategoria . '.jpg';?>" alt="<?php echo $nameEventoURL; ?>"/>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </a>

                            <div class="infoBox">

                                <a href="<?php echo $linkDetalheEventoVAgenda . '?' . $nameEventoURL . '&ref=' . $refEncoded;?>" title="<?php echo $titlePlataforma; ?> - <?php echo $nome_evento;?>">
                                    <div class="titleEvent">
                                        <?php
                                            echo $showNome;
                                            if(strlen($nome_evento)>30){
                                                echo '...';
                                            }
                                        ?>
                                    </div>
                                </a>

                                <div class="monthEvent">
                                    <?php
                                        if($mes_inicio == $mes_fim){
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '.';
                                        } else {
                                            echo VirtualDeskSiteVAgendaHelper::getNameMonth($mes_inicio) . '. / ' . VirtualDeskSiteVAgendaHelper::getNameMonth($mes_fim) . '.';
                                        }
                                    ?>
                                </div>

                                <div class="infoEvent">
                                    <div class="catEvent">
                                        <div class="icon">
                                            <?php echo file_get_contents('images/v_agenda/icons/categoria/iconcat_' . $categoria . '.svg'); ?>
                                        </div>
                                        <div class="text">
                                            <p><a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearchCat);?>"><?php echo $catName;?></a></p>
                                            <?php echo '<p>' . $subcatName . '</p>'; ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php
                    endforeach;
                ?>
                <a href="<?php echo $pagListaEventos . '?' . base64_encode($refSearch11);?>" class="botao"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_AGENDAMUNICIPAL_VERMAIS'); ?></a>
            </div>
        </li>

    </ul>
</div>

<?php
    echo $headScripts;
    echo $footerScripts;
    echo $localScripts;
?>

<script>
    <?php
        require_once (JPATH_SITE . '/plugins/system/virtualdesk/layouts/v_agenda/agendaMunicipal.js.php');
    ?>
</script>