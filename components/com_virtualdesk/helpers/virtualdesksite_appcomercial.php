<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskTableUploadsAppComercial', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/uploads_appcomercial.php');
JLoader::register('VirtualDeskTablePermGroupsUsers', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_groupsusers.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');



/**
 * VirtualDesk User helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteAppComercialHelper extends VirtualDeskUserHelper
{

    public static function checkUploadFotoFormData()
    {
        // Check for request forgeries.
        if(!JSession::checkToken()) return(false);
        $app          = JFactory::getApplication();
        $newRegSubmit = $app->input->get('newUploadFoto',null, 'STRING');
        if((string)$newRegSubmit != 'newUploadFoto') return(false);
        return(true);

    }

    public static function CheckReferencia($cod){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('referencia')
            ->from("#__virtualdesk_uploads_appcomercial")
            ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getUploadFotoCleanPostedData(){
        $data = array();
        $data['nome'] =  null;
        $data['email'] =  null;
        $data['donoFoto'] =  null;
        $data['nomeFoto'] =  null;
        $data['veracidadedados'] =  null;
        $data['politicaprivacidade'] =  null;
        return($data);
    }

    public static function saveNewUpload($data){
        $nome                   = $data['nome'];
        $email                  = $data['email'];
        $donoFoto               = $data['donoFoto'];
        $nomeFoto               = $data['nomeFoto'];
        $veracidadedados        = $data['veracidadedados'];
        $politicaprivacidade    = $data['politicaprivacidade'];

        $referencia = '';
        while($refExiste == 1){

            $referencia = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 20);
            $checkREF = self::CheckReferencia($referencia);
            if((int)$checkREF == 0){
                $refExiste = 0;
            }
        }

        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);
        $columns = array('referencia','nome','email','proprietario','nome_foto','estado');
        $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nome)), $db->quote($db->escape($email)), $db->quote($db->escape($donoFoto)), $db->quote($db->escape($nomeFoto)), $db->quote($db->escape($veracidadedados)), $db->quote($db->escape($politicaprivacidade)), $db->quote($db->escape('1')));
        $query
            ->insert($db->quoteName('#__virtualdesk_uploads_appcomercial'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean) $db->execute();

        return($result);

    }

}