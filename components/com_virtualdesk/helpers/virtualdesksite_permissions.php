<?php
/**
 * @package     Joomla.Site.VirtualDesk
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');


/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSitePermissionsHelper
{
    protected $cmpModuloTag = 'modulo_tagchave';
    protected $cmpModuloId  = 'modulo_id';
    protected $cmpTipoTag   = 'tipo_tagchave';
    protected $cmpTipoId    = 'tipo_id';
    protected $cmpActionTag = 'action_tagchave';
    protected $cmpActionId  = 'action_id';

    protected $cmpPluginTag = 'plugin_tagchave';
    protected $cmpPluginEnabled = 'plugin_enabled';

    protected $cmpPermCreate  = 'pcreate';
    protected $cmpPermRead    = 'pread';
    protected $cmpPermUpdate  = 'pupdate';
    protected $cmpPermDelete  = 'pdelete';
    protected $cmpPermReadAll = 'preadall';



    public $bitPermCreate  = 'c';
    public $bitPermRead    = 'r';
    public $bitPermUpdate  = 'u';
    public $bitPermDelete  = 'd';
    public $bitPermReadAll = 'rAll';
    public $bitPermSourceG = 'sG';  // para indicar que a linha de permissões veio por GRUPO
    public $bitPermSourceU = 'sU';  // para indicar que a linha de permissões veio por UTILIZADOR
    public $bitPermSourceGName = 'sGn';

    public $bitPluginEnabled  = 'e'; // se estiver a 1 o plugin está enabled

    public $bitPermCreateSrc  = 'cSrc'; // PAra efeitos de detalhe do carregamento da spermissões, para saber se a permissão veio do Grupo ou do User
    public $bitPermReadSrc    = 'rSrc';
    public $bitPermUpdateSrc  = 'uSrc';
    public $bitPermDeleteSrc  = 'dSrc';
    public $bitPermReadAllSrc = 'rAllSrc';

    protected $tagView        = 'view';
    protected $tagLayout      = 'layout';
    protected $tagReport      = 'report';
    protected $tagCRUD        = 'crud';
    protected $tagMenus       = 'menus';
    protected $tagFunction    = 'function';

    protected $bitFromDB      = 'db'; // carrega por defeito da base de dados (mais para testes), caso contrário utiliza a sessão.

    protected $arPerm       = array();
    protected $arPlugin     = array();


    function __construct() {

        $this->bitFromDB = JComponentHelper::getParams('com_virtualdesk')->get('permvd_check_userperm_from_session');
    }


    /**
     *
     * @param $Modulo
     * @param $Action
     * @return mixed
     */
    public function loadPermission($bitDebug=false , $idUserToLoad=-1)
    {

        if($this->bitFromDB==='db') {
            $this->loadPermissionFromDB($bitDebug , $idUserToLoad);
        }
        else {
            $this->loadPermissionFromSession();
        }
    }


    public function loadPluginEnabled($bitDebug=false)
    {
        // por agora não faz sentido termos a opção de colocar os plugins na sessão, porque vamos verificar a maior parte quando não têm sessão (user externos)
        $this->loadPluginEnabledFromDB($bitDebug );

        /*if($this->bitFromDB==='db') {
            $this->loadPluginEnabledFromDB($bitDebug , $idUserToLoad);
        }
        else {
            $this->loadPluginEnabledFromSession();
        }*/
    }


    public function loadModuleEnabledByTag($Tag='')
    {
        try
        {
            $db = JFactory::getDbo();

            $db->setQuery("Select enabled From #__virtualdesk_perm_modulo Where tagchave ='" . $db->escape($Tag). "'");
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /**
     *
     * @param $Modulo
     * @param $Action
     * @return mixed
     */
    public function loadPermissionFromDB($bitDebug=false , $idUserToLoad=-1)
    {
        // Current Session User...
        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();

        if((int)$idUserToLoad>0) $UserJoomlaID = $idUserToLoad;

        // Carregar os Grupos do Utilizador atual
        $arGroupsFromUser = $this->loadGroupsFromUser($UserJoomlaID);
        $GroupsFromUser = implode(",", $arGroupsFromUser);

        // Carregar carregamento de permissões dos grupos associados ao GRUPO
        $dataReturnGroups = $this->loadGroupsActionsTipoModules($GroupsFromUser);

        // Transforma a lista com subarray subobjectos por grupo
        foreach ($dataReturnGroups as $rowGroups) {
            // se não existe o módulo nas permissões adiciona
            $currModuleTag = $rowGroups[$this->cmpModuloTag];
            if (!array_key_exists($currModuleTag,  $this->arPerm)) {
                $this->arPerm[$currModuleTag] = array();
            }

            // Se não existe o tipo nas permissões adiciona
            $currTipoTag = $rowGroups[$this->cmpTipoTag];
            if (!array_key_exists($currTipoTag,  $this->arPerm[$currModuleTag])) {
                $this->arPerm[$currModuleTag][$currTipoTag] = array();
            }

            // Se não existe a action nas permissões adiciona
            $currActionTag= $rowGroups[$this->cmpActionTag];
            // Neste caso adioiona sempre a Action, pode sobrepor depois a tag de User relativamente à tag Grupo
            if (!array_key_exists($currActionTag,  $this->arPerm[$currModuleTag][$currTipoTag])) {
                $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag] = array();
            }

            // Permissões
            $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermCreate]  = $rowGroups[$this->cmpPermCreate];
            if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermCreateSrc] = $this->bitPermSourceG;

            $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermRead]    = $rowGroups[$this->cmpPermRead];
            if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermReadSrc] = $this->bitPermSourceG;

            $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermUpdate]  = $rowGroups[$this->cmpPermUpdate];
            if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermUpdateSrc] = $this->bitPermSourceG;

            $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermDelete]  = $rowGroups[$this->cmpPermDelete];
            if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermDeleteSrc] = $this->bitPermSourceG;

            $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermReadAll] = $rowGroups[$this->cmpPermReadAll];
            if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermReadAllSrc] = $this->bitPermSourceG;

            $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermSourceG] = 1; // para indicar que a linha de permissões veio por GRUPO
            if($bitDebug===true) {
                if(empty($this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermSourceGName])) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermSourceGName] = '';
                $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermSourceGName] .= $rowGroups['groups_nome'].' ';
            }

        }

        // Percorrer o carregamento de permissões por utilizador e criar estrutura
        // ATENçÃO, as permissões do utilizador sobrepõem-se aos do grupo se existiram permissão na mesma "action"+"módulo"+"tipo"

        $dataReturnUser = $this->loadUsersActionsTipoModules($UserJoomlaID);

        // a estrutura deve reflectir  "Módulo->tipo->action-> permissões"
        // uma estrutura em array é mais rápido que objecto com MODULO -> TIPO -> ACTION ->bits -> USERS + GRUPOS

          // Transforma a lista com subarray subobjectos por utilizador
        foreach ($dataReturnUser as $rowUser) {
            // se não existe o módulo nas permissões adiciona
            $currModuleTag = $rowUser[$this->cmpModuloTag];
            if (!array_key_exists($currModuleTag,  $this->arPerm)) {
                $this->arPerm[$currModuleTag] = array();
            }

            // Se não existe o tipo nas permissões adiciona
            $currTipoTag = $rowUser[$this->cmpTipoTag];
            if (!array_key_exists($currTipoTag,  $this->arPerm[$currModuleTag])) {
                $this->arPerm[$currModuleTag][$currTipoTag] = array();
            }

            // Se não existe a action nas permissões adiciona
            $currActionTag= $rowUser[$this->cmpActionTag];
            // Neste caso adioiona sempre a Action, pode sobrepor depois a tag de User relativamente à tag Grupo
            if (!array_key_exists($currActionTag,  $this->arPerm[$currModuleTag][$currTipoTag])) {
                $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag] = array();
            }

            // Permissões: Vai sobrepor as permissões do utilizar que estiverem definidas com 1 ,. Se o grupo estiver a 1 não sobrepõem o 0 do utilizador
            //
            if(!empty($this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermCreate])) {
                if ((int)$this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermCreate] !== 1) {
                    $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermCreate] = $rowUser[$this->cmpPermCreate];
                    if ($bitDebug === true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermCreateSrc] = $this->bitPermSourceU;
                }
            }
            if(!empty($this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermRead])) {
                if((int)$this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermRead]!==1)   {
                    $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermRead]    = $rowUser[$this->cmpPermRead];
                    if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermReadSrc] = $this->bitPermSourceU;
                }
            }
            if(!empty($this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermUpdate])) {
                if ((int)$this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermUpdate] !== 1) {
                    $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermUpdate] = $rowUser[$this->cmpPermUpdate];
                    if ($bitDebug === true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermUpdateSrc] = $this->bitPermSourceU;
                }
            }
            if(!empty($this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermDelete])) {
                if((int)$this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermDelete]!==1) {
                    $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermDelete]  = $rowUser[$this->cmpPermDelete];
                    if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermDeleteSrc] = $this->bitPermSourceU;
                }
            }
            if(!empty($this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermDelete])) {
                if((int)$this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermReadAll]!==1) {
                    $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermReadAll] = $rowUser[$this->cmpPermReadAll];
                    if($bitDebug===true) $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermReadAllSrc] = $this->bitPermSourceU;
                }
            }
            $this->arPerm[$currModuleTag][$currTipoTag][$currActionTag][$this->bitPermSourceU] = 1; // para indicar que a linha de permissões veio por UTILIZADOR

        }

        //return($this->arPerm);
        return(true);
    }


    public function loadPluginEnabledFromDB($bitDebug=false)
    {
        // Current Session User...
        //$UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();

        //if((int)$idUserToLoad>0) $UserJoomlaID = $idUserToLoad;

        $dataReturn = $this->loadPluginEnableList();

        // Transforma a lista com subarray subobjectos por grupo
        foreach ($dataReturn as $rowEnabled) {
            // se não existe o plugin nas permissões adiciona
            $currPluginTag = $rowEnabled[$this->cmpPluginTag];
            if (!array_key_exists($currPluginTag,  $this->arPlugin)) {
                $this->arPlugin[$currPluginTag] = array();
            }

           $this->arPlugin[$currPluginTag][$this->bitPluginEnabled]  = $rowEnabled[$this->cmpPluginEnabled];
        }

        return(true);
    }



    /**
     * Carrega
     * @return mixed
     */
    public function setPermissionToSession()
    {
        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        $JSession     = JFactory::getSession();
        $JSession->set('VDPerm',$this->arPerm,'VirtualDesk');
        return(true);
    }

    /**
     * Carrega
     * @return mixed
     */
    public function destroyPermissionToSession()
    {
        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        #if( empty($UserJoomlaID) )  return false;
        #if( (int)$UserJoomlaID<=0 )  return false;
        $arPerm = array();
        $JSession     = JFactory::getSession();
        $JSession->set('VDPerm',$arPerm,'VirtualDesk');
        return(true);
    }


/* por agora não faz sentido termos a opção de colocar os plugins na sessão, porque vamos verificar a maior parte quando não têm sessão (user externos)
    public function setPluginEnabledToSession()
    {
        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        $JSession     = JFactory::getSession();
        $JSession->set('VDPlugInEnabled',$this->arPlugin,'VirtualDesk');
        return(true);
    }
*/

    /**
     * Carrega
     * @return mixed
    */
    public function loadPermissionFromSession()
    {
        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        $JSession     = JFactory::getSession();
        $arPerm = $JSession->get('VDPerm',array(),'VirtualDesk');
        $this->arPerm = $arPerm;
        return(true);
    }


    /* por agora não faz sentido termos a opção de colocar os plugins na sessão, porque vamos verificar a maior parte quando não têm sessão (user externos)
        public function loadPluginEnabledFromSession()
        {
            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            $JSession     = JFactory::getSession();
            $arPlugin = $JSession->get('VDPlugInEnabled',array(),'VirtualDesk');
            $this->arPlugin = $arPlugin;
            return(true);
        }
  */

        public function getPermArray()
        {
            return($this->arPerm);
        }

        public function getPluginArray()
        {
            return($this->arPlugin);
        }


        /**
         *
         * @param $Modulo
         * @param $Action
         * @return boolean
         */
    public function checkPluginIsEnabled($PlugInTag)
    {  $isEnabled= false;
       if( !isset($this->arPlugin[$PlugInTag][$this->bitPluginEnabled]) ) return($isEnabled);
       if( $this->arPlugin[$PlugInTag][$this->bitPluginEnabled] === "1") $isEnabled = true;
       return $isEnabled;
    }

    public function loadGroupsFromUser($UserSessionId=-1)
    {
        if( empty($UserSessionId) )  return false;
        if( (int)$UserSessionId<=0 )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('distinct idpermgroup'))
            ->from(" #__virtualdesk_perm_groupsusers as groupsusers")
            ->where($db->quoteName('groupsusers.iduserjos').'='.$db->escape($UserSessionId) )
        );
        $dataReturn = $db->loadColumn();

        return($dataReturn);
    }


    public function checkSessionUserInVDAdmin()
    {
        $groupVDAdminId = JComponentHelper::getParams('com_virtualdesk')->get('permvd_joomlagroup_vdadmin');
        $IsInGroup = false;

        $userSessId = VirtualDeskSiteUserHelper::getUserSessionId();
        $groups = JAccess::getGroupsByUser($userSessId);

        if( in_array($groupVDAdminId,$groups) ===true) $IsInGroup = true;
        return $IsInGroup;
    }


    /*
    * Verifica se o utilizador está no grupo Admin da APP
    * Por exemplo para poder aceder às listas e outros ecrã especificos dos Managers ou Admins
    */
    public function checkSessionUserInAppAdmin()
    {
        $groupAppAdminId   = JComponentHelper::getParams('com_virtualdesk')->get('permvd_appgroup_vdadmin');
        $IsInGroup = false;

        $userSessId = VirtualDeskSiteUserHelper::getUserSessionId();

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('distinct idpermgroup'))
            ->from(" #__virtualdesk_perm_groupsusers as groupsusers")
            ->where($db->quoteName('groupsusers.iduserjos').'='.$db->escape($userSessId).' and idpermgroup in ('. $groupAppAdminId .')' )
        );
        $dataReturn = $db->loadColumn();

        if( !is_array($dataReturn) || (empty($dataReturn)) ) return $IsInGroup;
        if( ((int)$dataReturn[0] > 0 ) ) $IsInGroup = true;
        return $IsInGroup;
    }


    /*
    * Verifica se o utilizador está no grupo Admin ou Manager da APP
    * Por exemplo para poder aceder às listas e outros ecrã especificos dos Managers ou Admins
    */
    public function checkSessionUserInAppAdminOrManager()
    {
        $groupAppAdminId   = JComponentHelper::getParams('com_virtualdesk')->get('permvd_appgroup_vdadmin');
        $groupAppManagerId = JComponentHelper::getParams('com_virtualdesk')->get('permvd_appgroup_vdmanager');
        $IsInGroup = false;

        $userSessId = VirtualDeskSiteUserHelper::getUserSessionId();

        $db1 = JFactory::getDBO();
        $db1->setQuery($db1->getQuery(true)
            ->select(array('distinct id'))
            ->from(" #__virtualdesk_perm_groups")
            ->where(' isManager=1 and id>4 ')
        );
        $dataGroupInManager = $db1->loadAssocList();
        $groupExtraManager  = '';
        if(!empty($dataGroupInManager)) {
            foreach($dataGroupInManager as $key => $val) {
                $groupExtraManager .= ',' . $val['id'];
            }
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('distinct idpermgroup'))
            ->from(" #__virtualdesk_perm_groupsusers as groupsusers")
            ->where($db->quoteName('groupsusers.iduserjos').'='.$db->escape($userSessId).' and idpermgroup in ('. $groupAppAdminId.','.$groupAppManagerId . $groupExtraManager.')' )
        );
        $dataReturn = $db->loadColumn();

        if( !is_array($dataReturn) || (empty($dataReturn)) ) return $IsInGroup;
        if( ((int)$dataReturn[0] > 0 ) ) $IsInGroup = true;
        return $IsInGroup;
    }


    /**
     *
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkViewAccess($Modulo)
    {  $HasAccess = false;
       if( !isset($this->arPerm[$Modulo][$this->tagView][$this->tagView][$this->bitPermRead]) ) return($HasAccess);
       if( $this->arPerm[$Modulo][$this->tagView][$this->tagView][$this->bitPermRead] === "1") $HasAccess = true;
       return $HasAccess;
    }


    /**
    *
    * @param $Modulo
    * @param $Action
    * @return boolean
    */
    public function checkLayoutAccess($Modulo, $Action)
    {  $HasAccess = false;
       if( !isset($this->arPerm[$Modulo][$this->tagLayout][$Action][$this->bitPermRead]) ) return($HasAccess);
       if( $this->arPerm[$Modulo][$this->tagLayout][$Action][$this->bitPermRead] === "1") $HasAccess = true;
       return $HasAccess;
    }


    /**
     *
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkReportAccess($Modulo, $Report)
    {  $HasAccess = false;
       if( !isset($this->arPerm[$Modulo][$this->tagReport][$Report][$this->bitPermRead]) ) return($HasAccess);
       if( $this->arPerm[$Modulo][$this->tagReport][$Report][$this->bitPermRead] === "1") $HasAccess = true;
       return $HasAccess;
    }


    /**
     *Verifica se o utilizador tem permissão ao detalhe do registos para criar um NOVO (new)
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkDetailAddNewAccess($Modulo)
    {  $HasAccess = false;
       if( !isset($this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermCreate]) ) return($HasAccess);
       if( $this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermCreate] === "1") $HasAccess = true;
       return $HasAccess;
    }


    /**
     * Verifica se o utilizador tem permissão ao detalhe do registos para aceder ao View Detail (DEFAULT)
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkDetailReadAccess($Modulo)
    {  $HasAccess = false;
        if( !isset($this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermRead]) ) return($HasAccess);
        if( $this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermRead] === "1") $HasAccess = true;
        return $HasAccess;
    }


    /**
     * Verifica se o utilizador tem permissão ao detalhe do registos para aceder ao View Detail (DEFAULT)
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkDetailEditAccess($Modulo)
    {  $HasAccess = false;
        if( !isset($this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermUpdate]) ) return($HasAccess);
        if( $this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermUpdate] === "1") $HasAccess = true;
        return $HasAccess;
    }


    /**
     * Verifica se o utilizador tem permissão ao detalhe do registos para aceder ao View Detail (DEFAULT)
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkDetailDeleteAccess($Modulo)
    {  $HasAccess = false;
        if( !isset($this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermDelete]) ) return($HasAccess);
        if( $this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermDelete] === "1") $HasAccess = true;
        return $HasAccess;
    }


    /**
     * Verifica se o utilizador tem permissão ao detalhe do registos para aceder ao View Detail (DEFAULT)
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkMenuAccess($MenuId)
    {  $HasAccess = false;
        $MenuId = (string)$MenuId;
        if( !isset($this->arPerm[$this->tagMenus][$this->tagMenus][$MenuId][$this->bitPermRead]) ) return($HasAccess);
        if( $this->arPerm[$this->tagMenus][$this->tagMenus][$MenuId][$this->bitPermRead] === "1") $HasAccess = true;
        return $HasAccess;
    }


    /**
     * Verifica se o utilizador tem permissão à opçºão ReadAll, ou seja, vê todos os registos, não filtrando na lista nem no carregamento do detalhe
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkReadAllAccess($Modulo)
    {  $HasAccess = false;
        if( !isset($this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermReadAll]) ) return($HasAccess);
        if( $this->arPerm[$Modulo][$this->tagCRUD][$this->tagCRUD][$this->bitPermReadAll] === "1") $HasAccess = true;
        return $HasAccess;
    }

    /**
     * Verifica se o utilizador tem permissão à opçºão ReadAll, ou seja, vê todos os registos, não filtrando na lista nem no carregamento do detalhe
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkReadAllLayoutAccess($Modulo,$Action)
    {
        $HasAccess = false;
        if( !isset($this->arPerm[$Modulo][$this->tagLayout][$Action][$this->bitPermReadAll]) ) return($HasAccess);
        if( $this->arPerm[$Modulo][$this->tagLayout][$Action][$this->bitPermReadAll] === "1") $HasAccess = true;
        return $HasAccess;
    }

    /**
     *
     * @param $Modulo
     * @param $Action
     * @return boolean
     */
    public function checkFunctionAccess($Modulo, $Action)
    {  $HasAccess = false;
        if( !isset($this->arPerm[$Modulo][$this->tagFunction][$Action][$this->bitPermRead]) ) return($HasAccess);
        if( $this->arPerm[$Modulo][$this->tagFunction][$Action][$this->bitPermRead] === "1") $HasAccess = true;
        return $HasAccess;
    }


    /**
     * Carregar tipo de actions a partir dos módulos
     *
     */
    public function loadModulesTipoActions ()
    {
        // SQL
        /* SELECT
             modulo.id AS modulo_id
            ,modulo.tagchave AS modulo_tagchave
            ,modulo.nome AS modulo_nome
            ,tipo.id AS tipo_id
            ,tipo.tagchave AS tipo_tagchave
            ,tipo.nome AS tipo_nome
            ,action.id AS action_id
            ,action.tagchave AS action_tagchave
            ,action.nome AS action_nome
            FROM gtonq_virtualdesk_perm_modulo as modulo
            LEFT  JOIN gtonq_virtualdesk_perm_tipomodulo as tipomodulo ON modulo.id = tipomodulo.idpermmodulo
            LEFT  JOIN gtonq_virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id
            LEFT  JOIN gtonq_virtualdesk_perm_action as action ON tipomodulo.id = action.idpermtipomodulo
        */

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'modulo.id AS modulo_id'  , 'modulo.tagchave AS modulo_tagchave'
            ,'modulo.nome AS modulo_nome' ,'tipo.id AS tipo_id'
            ,'tipo.tagchave AS tipo_tagchave'
            ,'tipo.nome AS tipo_nome'
            ,'action.id AS action_id'
            ,'action.tagchave AS action_tagchave' ,'action.nome AS action_nome'
            ))
            ->join('LEFT', '#__virtualdesk_perm_tipomodulo as tipomodulo ON modulo.id = tipomodulo.idpermmodulo')
            ->join('LEFT', '#__virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id')
            ->join('LEFT', '#__virtualdesk_perm_action as action ON tipomodulo.id = action.idpermtipomodulo')
            ->from("#__virtualdesk_perm_modulo as modulo")
            ->order(' modulo_tagchave, tipo_tagchave')
        );

        // Dump the query
        //echo $db->getQuery()->dump();

        $dataReturn = $db->loadObjectList();

        return($dataReturn);


    }


    /**
     * Carregar actions/permissões a partir dos módulos
     *
     */
    public function loadModulesTipoActionsForEditing ($UserId)
    {
        /*SELECT
gtonq_virtualdesk_perm_modulo.tagchave AS modulo_tagchave,
        gtonq_virtualdesk_perm_modulo.nome AS modulo_nome,
        gtonq_virtualdesk_perm_modulo.tagchave AS modulo_tagchave,
        gtonq_virtualdesk_perm_tipo.id AS tipo_id,
        gtonq_virtualdesk_perm_tipo.tagchave AS tipo_tagchave,
        gtonq_virtualdesk_perm_tipo.nome AS tipo_nome,
        gtonq_virtualdesk_perm_action.id AS action_id,
        gtonq_virtualdesk_perm_action.tagchave AS action_tagchave,
        gtonq_virtualdesk_perm_action.descricao AS action_descricao,
        gtonq_virtualdesk_perm_action.nome AS action_nome,
        uactions.permread,
        uactions.permcreate,
        uactions.permupdate,
        uactions.permdelete,
        uactions.permreadall,
        uactions.iduserjos
        FROM
        gtonq_virtualdesk_perm_modulo
        LEFT JOIN gtonq_virtualdesk_perm_tipomodulo ON gtonq_virtualdesk_perm_modulo.id = gtonq_virtualdesk_perm_tipomodulo.idpermmodulo
        LEFT JOIN gtonq_virtualdesk_perm_tipo ON gtonq_virtualdesk_perm_tipomodulo.idpermtipo = gtonq_virtualdesk_perm_tipo.id
        LEFT OUTER JOIN gtonq_virtualdesk_perm_action ON gtonq_virtualdesk_perm_tipomodulo.id = gtonq_virtualdesk_perm_action.idpermtipomodulo
        LEFT OUTER JOIN
             ( SELECT * FROM gtonq_virtualdesk_perm_usersaction where gtonq_virtualdesk_perm_usersaction.iduserjos = 40) as uactions
         ON uactions.idpermaction = gtonq_virtualdesk_perm_action.id
order by modulo_tagchave, tipo_tagchave, action_tagchave
        */

        if( empty($UserId) )  return false;
        if( (int)$UserId<=0 )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'modulo.id AS modulo_id', 'modulo.tagchave AS modulo_tagchave' , 'modulo.nome AS modulo_nome' , 'modulo.tagchave AS modulo_tagchave'
            ,'tipo.id AS tipo_id' , 'tipo.tagchave AS tipo_tagchave' , 'tipo.nome AS tipo_nome'
            ,'action.id AS action_id' , 'action.tagchave AS action_tagchave' , 'action.nome AS action_nome' , 'action.descricao AS action_descricao'
            ,'uactions.id as uactions_id', 'uactions.permread as pread','uactions.permcreate as pcreate','uactions.permupdate as pupdate','uactions.permdelete as pdelete','uactions.permreadall as preadall','uactions.iduserjos'

            ))
            ->join('LEFT', '#__virtualdesk_perm_tipomodulo as tipomodulo ON tipomodulo.idpermmodulo = modulo.id')
            ->join('LEFT', '#__virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id')
            ->join('LEFT OUTER', '#__virtualdesk_perm_action as action ON tipomodulo.id = action.idpermtipomodulo')
            ->join('LEFT OUTER', ' (SELECT * FROM #__virtualdesk_perm_usersaction where #__virtualdesk_perm_usersaction.iduser = '.$db->escape($UserId).' ) as uactions ON uactions.idpermaction = action.id ')
            ->from("#__virtualdesk_perm_modulo as modulo")
            ->order('modulo_tagchave, tipo_tagchave, action_tagchave' )
        );


        // Dump the query
        //echo $db->getQuery()->dump();

        $dataReturn = $db->loadObjectList();

        return($dataReturn);


     }



    public function loadModulesTipoActionsGrupoForEditing ($GrupoUserId)
    {
        /*SELECT
gtonq_virtualdesk_perm_modulo.tagchave AS modulo_tagchave,
        gtonq_virtualdesk_perm_modulo.nome AS modulo_nome,
        gtonq_virtualdesk_perm_modulo.tagchave AS modulo_tagchave,
        gtonq_virtualdesk_perm_tipo.id AS tipo_id,
        gtonq_virtualdesk_perm_tipo.tagchave AS tipo_tagchave,
        gtonq_virtualdesk_perm_tipo.nome AS tipo_nome,
        gtonq_virtualdesk_perm_action.id AS action_id,
        gtonq_virtualdesk_perm_action.tagchave AS action_tagchave,
        gtonq_virtualdesk_perm_action.descricao AS action_descricao,
        gtonq_virtualdesk_perm_action.nome AS action_nome,
        uactions.permread,
        uactions.permcreate,
        uactions.permupdate,
        uactions.permdelete,
        uactions.permreadall,
        uactions.iduserjos
        FROM
        gtonq_virtualdesk_perm_modulo
        LEFT JOIN gtonq_virtualdesk_perm_tipomodulo ON gtonq_virtualdesk_perm_modulo.id = gtonq_virtualdesk_perm_tipomodulo.idpermmodulo
        LEFT JOIN gtonq_virtualdesk_perm_tipo ON gtonq_virtualdesk_perm_tipomodulo.idpermtipo = gtonq_virtualdesk_perm_tipo.id
        LEFT OUTER JOIN gtonq_virtualdesk_perm_action ON gtonq_virtualdesk_perm_tipomodulo.id = gtonq_virtualdesk_perm_action.idpermtipomodulo
        LEFT OUTER JOIN
             ( SELECT * FROM gtonq_virtualdesk_perm_usersaction where gtonq_virtualdesk_perm_usersaction.iduserjos = 40) as uactions
         ON uactions.idpermaction = gtonq_virtualdesk_perm_action.id
order by modulo_tagchave, tipo_tagchave, action_tagchave
        */

        if( empty($GrupoUserId) )  return false;
        if( (int)$GrupoUserId<=0 )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'modulo.id AS modulo_id', 'modulo.tagchave AS modulo_tagchave' , 'modulo.nome AS modulo_nome' , 'modulo.tagchave AS modulo_tagchave'
            ,'tipo.id AS tipo_id' , 'tipo.tagchave AS tipo_tagchave' , 'tipo.nome AS tipo_nome'
            ,'action.id AS action_id' , 'action.tagchave AS action_tagchave' , 'action.nome AS action_nome' , 'action.descricao AS action_descricao'
            ,'gactions.id as gactions_id', 'gactions.permread as pread','gactions.permcreate as pcreate','gactions.permupdate as pupdate','gactions.permdelete as pdelete','gactions.permreadall as preadall','gactions.idpermgroup'
            ))
            ->join('LEFT', '#__virtualdesk_perm_tipomodulo as tipomodulo ON tipomodulo.idpermmodulo = modulo.id')
            ->join('LEFT', '#__virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id')
            ->join('LEFT OUTER', '#__virtualdesk_perm_action as action ON tipomodulo.id = action.idpermtipomodulo')
            ->join('LEFT OUTER', ' (SELECT * FROM #__virtualdesk_perm_groupsaction where #__virtualdesk_perm_groupsaction.idpermgroup = '.$db->escape($GrupoUserId).' ) as gactions ON gactions.idpermaction = action.id ')
            ->from("#__virtualdesk_perm_modulo as modulo")
            ->order('modulo_tagchave, tipo_tagchave, action_tagchave' )
        );

        // Dump the query
        //echo $db->getQuery()->dump();

        $dataReturn = $db->loadObjectList();
        return($dataReturn);
    }



    /**
     * Carregar permissões a partir dos módulos, e que grupos estão associados
     *
     */
    private function loadModulesTipoActionsByGroups ()
    {
        /*SELECT
        gtonq_virtualdesk_perm_modulo.id AS modulo_id,
        gtonq_virtualdesk_perm_modulo.nome AS modulo_nome,
        gtonq_virtualdesk_perm_modulo.tagchave AS modulo_tagchave,
        gtonq_virtualdesk_perm_tipo.id AS tipo_id,
        gtonq_virtualdesk_perm_tipo.tagchave AS tipo_tagchave,
        gtonq_virtualdesk_perm_tipo.nome AS tipo_nome,
        gtonq_virtualdesk_perm_action.id AS action_id,
        gtonq_virtualdesk_perm_action.tagchave AS action_tagchave,
        gtonq_virtualdesk_perm_action.descricao AS action_descricao,
        gtonq_virtualdesk_perm_action.nome AS action_nome,
        gtonq_virtualdesk_perm_groups.id as group_id,
        gtonq_virtualdesk_perm_groups.nome as groups_nome,
        gtonq_virtualdesk_perm_groupsaction.id as groupsaction_id,
        gtonq_virtualdesk_perm_groupsaction.permread as groupsaction_permread,
        gtonq_virtualdesk_perm_groupsaction.permcreate as groupsaction_permcreate,
        gtonq_virtualdesk_perm_groupsaction.permupdate as groupsaction_permupdate,
        gtonq_virtualdesk_perm_groupsaction.permdelete as groupsaction_permdelete,
        gtonq_virtualdesk_perm_groupsaction.permreadall as groupsaction_permreadall
        FROM
        gtonq_virtualdesk_perm_modulo
        LEFT JOIN gtonq_virtualdesk_perm_tipomodulo ON gtonq_virtualdesk_perm_modulo.id = gtonq_virtualdesk_perm_tipomodulo.idpermmodulo
        LEFT JOIN gtonq_virtualdesk_perm_tipo ON gtonq_virtualdesk_perm_tipomodulo.idpermtipo = gtonq_virtualdesk_perm_tipo.id
        LEFT JOIN gtonq_virtualdesk_perm_action ON gtonq_virtualdesk_perm_tipomodulo.id = gtonq_virtualdesk_perm_action.idpermtipomodulo
        LEFT JOIN gtonq_virtualdesk_perm_groupsaction ON gtonq_virtualdesk_perm_groupsaction.idpermaction = gtonq_virtualdesk_perm_action.id AND gtonq_virtualdesk_perm_groupsaction.idpermaction = gtonq_virtualdesk_perm_action.id
        LEFT JOIN gtonq_virtualdesk_perm_groups ON gtonq_virtualdesk_perm_groups.id = gtonq_virtualdesk_perm_groupsaction.idpermgroup
                */
    }


    /**
    * Carregar permissões a partir de 1 grupo
    *
    */
    private function loadGroupsActionsTipoModules ($GroupID)
    {

                /*SELECT
        gtonq_virtualdesk_perm_groups.id as group_id,
        gtonq_virtualdesk_perm_groups.nome as groups_nome,
        gtonq_virtualdesk_perm_action.id AS action_id,
        gtonq_virtualdesk_perm_action.tagchave AS action_tagchave,
        gtonq_virtualdesk_perm_action.descricao AS action_descricao,
        gtonq_virtualdesk_perm_action.nome AS action_nome,
        gtonq_virtualdesk_perm_groupsaction.id as groupsaction_id,
        gtonq_virtualdesk_perm_groupsaction.permread as groupsaction_permread,
        gtonq_virtualdesk_perm_groupsaction.permcreate as groupsaction_permcreate,
        gtonq_virtualdesk_perm_groupsaction.permupdate as groupsaction_permupdate,
        gtonq_virtualdesk_perm_groupsaction.permdelete as groupsaction_permdelete,
        gtonq_virtualdesk_perm_groupsaction.permreadall as groupsaction_permreadall,
        gtonq_virtualdesk_perm_tipomodulo.id AS tipomodulo_id,
        gtonq_virtualdesk_perm_tipomodulo.idpermtipo AS tipomodulo_idpermtipo,
        gtonq_virtualdesk_perm_tipo.id AS tipo_id,
        gtonq_virtualdesk_perm_tipo.tagchave AS tipo_tagchave,
        gtonq_virtualdesk_perm_tipo.nome AS tipo_nome,
        gtonq_virtualdesk_perm_modulo.id AS modulo_id,
        gtonq_virtualdesk_perm_modulo.nome AS modulo_nome,
        gtonq_virtualdesk_perm_modulo.tagchave AS modulo_tagchave

        FROM
        gtonq_virtualdesk_perm_groups
        LEFT JOIN gtonq_virtualdesk_perm_groupsaction ON gtonq_virtualdesk_perm_groups.id = gtonq_virtualdesk_perm_groupsaction.idpermgroup
        LEFT JOIN gtonq_virtualdesk_perm_action ON gtonq_virtualdesk_perm_groupsaction.idpermaction = gtonq_virtualdesk_perm_action.id
        LEFT OUTER JOIN gtonq_virtualdesk_perm_tipomodulo ON gtonq_virtualdesk_perm_action.idpermtipomodulo = gtonq_virtualdesk_perm_tipomodulo.id
        LEFT JOIN gtonq_virtualdesk_perm_tipo ON gtonq_virtualdesk_perm_tipomodulo.idpermtipo = gtonq_virtualdesk_perm_tipo.id
        LEFT JOIN gtonq_virtualdesk_perm_modulo ON gtonq_virtualdesk_perm_tipomodulo.idpermmodulo = gtonq_virtualdesk_perm_modulo.id

        */

        if( empty($GroupID) )  return false;
        // if( (int)$GroupID<=0 )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'groups.id as group_id'  , 'groups.nome as groups_nome'
                           ,'action.id AS action_id' , 'action.tagchave AS action_tagchave'
                           ,'tipo.id AS tipo_id'     , 'tipo.tagchave AS tipo_tagchave'
                           ,'modulo.id AS modulo_id' , 'modulo.tagchave AS modulo_tagchave'
                           ,'groupsaction.id as groupsaction_id' , 'groupsaction.permread as pread' , 'groupsaction.permcreate as pcreate'
                           ,'groupsaction.permdelete as pdelete' ,'groupsaction.permupdate as pupdate' , 'groupsaction.permreadall as preadall'

            ))
            ->join('LEFT', '#__virtualdesk_perm_groupsaction as groupsaction ON groups.id = groupsaction.idpermgroup')
            ->join('LEFT', '#__virtualdesk_perm_action as action ON groupsaction.idpermaction = action.id')
            ->join('LEFT OUTER', '#__virtualdesk_perm_tipomodulo as tipomodulo ON action.idpermtipomodulo = tipomodulo.id')
            ->join('LEFT', '#__virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id')
            ->join('LEFT', '#__virtualdesk_perm_modulo as modulo ON tipomodulo.idpermmodulo = modulo.id')
            ->from("#__virtualdesk_perm_groups as groups")
            ->where($db->quoteName('groups.id').' in ('.$db->escape($GroupID).') AND modulo.enabled=1' )
        );

        //echo $db->getQuery();

        $dataReturn = $db->loadAssocList();

        return($dataReturn);


    }


    /**
     * Carregar permissões a partir de 1 utilizador (Id do joomla
     *
     */
    private function loadUsersActionsTipoModules ($UserJosId)
    {

        /*
         *
         *SELECT
    gtonq_virtualdesk_users.id as users_id,
    gtonq_virtualdesk_users.name as users_nome,
    gtonq_virtualdesk_perm_action.id AS action_id,
    gtonq_virtualdesk_perm_action.tagchave AS action_tagchave,
    gtonq_virtualdesk_perm_action.descricao AS action_descricao,
    gtonq_virtualdesk_perm_action.nome AS action_nome,
    gtonq_virtualdesk_perm_usersaction.id as usersaction_id,
    gtonq_virtualdesk_perm_usersaction.permread as usersaction_permread,
    gtonq_virtualdesk_perm_usersaction.permcreate as usersaction_permcreate,
    gtonq_virtualdesk_perm_usersaction.permupdate as usersaction_permupdate,
    gtonq_virtualdesk_perm_usersaction.permdelete as usersaction_permdelete,
    gtonq_virtualdesk_perm_usersaction.permreadall as usersaction_permreadall,
    gtonq_virtualdesk_perm_tipomodulo.id AS tipomodulo_id,
    gtonq_virtualdesk_perm_tipomodulo.idpermtipo AS tipomodulo_idpermtipo,
    gtonq_virtualdesk_perm_tipo.id AS tipo_id,
    gtonq_virtualdesk_perm_tipo.tagchave AS tipo_tagchave,
    gtonq_virtualdesk_perm_tipo.nome AS tipo_nome,
    gtonq_virtualdesk_perm_modulo.id AS modulo_id,
    gtonq_virtualdesk_perm_modulo.nome AS modulo_nome,
    gtonq_virtualdesk_perm_modulo.tagchave AS modulo_tagchave

    FROM
    gtonq_virtualdesk_users
    LEFT JOIN gtonq_virtualdesk_perm_usersaction ON gtonq_virtualdesk_users.id = gtonq_virtualdesk_perm_usersaction.iduser
    LEFT JOIN gtonq_virtualdesk_perm_action ON gtonq_virtualdesk_perm_usersaction.idpermaction = gtonq_virtualdesk_perm_action.id
    LEFT OUTER JOIN gtonq_virtualdesk_perm_tipomodulo ON gtonq_virtualdesk_perm_action.idpermtipomodulo = gtonq_virtualdesk_perm_tipomodulo.id
    LEFT JOIN gtonq_virtualdesk_perm_tipo ON gtonq_virtualdesk_perm_tipomodulo.idpermtipo = gtonq_virtualdesk_perm_tipo.id
    LEFT JOIN gtonq_virtualdesk_perm_modulo ON gtonq_virtualdesk_perm_tipomodulo.idpermmodulo = gtonq_virtualdesk_perm_modulo.id

         */


        if( empty($UserJosId) )  return false;
        if( (int)$UserJosId<=0 )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'users.id as user_id'  , 'users.name as users_name'
            ,'action.id AS action_id' , 'action.tagchave AS action_tagchave'
            ,'tipo.id AS tipo_id'     , 'tipo.tagchave AS tipo_tagchave'
            ,'modulo.id AS modulo_id' , 'modulo.tagchave AS modulo_tagchave'
            ,'usersaction.id as usersaction_id'  , 'usersaction.permread as pread'    , 'usersaction.permcreate as pcreate'
            ,'usersaction.permdelete as pdelete' ,'usersaction.permupdate as pupdate' , 'usersaction.permreadall as preadall'

            ))
            ->join('LEFT', '#__virtualdesk_perm_usersaction as usersaction ON users.id = usersaction.iduser')
            ->join('LEFT', '#__virtualdesk_perm_action as action ON usersaction.idpermaction = action.id')
            ->join('LEFT OUTER', '#__virtualdesk_perm_tipomodulo as tipomodulo ON action.idpermtipomodulo = tipomodulo.id')
            ->join('LEFT', '#__virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id')
            ->join('LEFT', '#__virtualdesk_perm_modulo as modulo ON tipomodulo.idpermmodulo = modulo.id')
            ->from("#__virtualdesk_users as users")
            ->where($db->quoteName('users.idjos').'='.$db->escape($UserJosId).' AND modulo.enabled=1 ' )
        );

       // echo $db->getQuery();

        $dataReturn = $db->loadAssocList();

        return($dataReturn);
    }



    public function loadGroupsActionsTipoModulesFullObject ($GroupID)
    {

        if( empty($GroupID) )  return false;
        if( (int)$GroupID<=0 )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'groups.id as group_id'  , 'groups.nome as groups_nome'
            ,'action.id AS action_id' , 'action.tagchave AS action_tagchave', 'action.nome AS action_nome'
            ,'tipo.id AS tipo_id'     , 'tipo.tagchave AS tipo_tagchave', 'tipo.nome AS tipo_nome'
            ,'modulo.id AS modulo_id' , 'modulo.tagchave AS modulo_tagchave', 'modulo.nome AS modulo_nome'
            ,'groupsaction.id as groupsaction_id' , 'groupsaction.permread as pread' , 'groupsaction.permcreate as pcreate'
            ,'groupsaction.permdelete as pdelete' ,'groupsaction.permupdate as pupdate' , 'groupsaction.permreadall as preadall'

            ))
            ->join('LEFT', '#__virtualdesk_perm_groupsaction as groupsaction ON groups.id = groupsaction.idpermgroup')
            ->join('LEFT', '#__virtualdesk_perm_action as action ON groupsaction.idpermaction = action.id')
            ->join('LEFT OUTER', '#__virtualdesk_perm_tipomodulo as tipomodulo ON action.idpermtipomodulo = tipomodulo.id')
            ->join('LEFT', '#__virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id')
            ->join('LEFT', '#__virtualdesk_perm_modulo as modulo ON tipomodulo.idpermmodulo = modulo.id')
            ->from("#__virtualdesk_perm_groups as groups")
            ->where($db->quoteName('groups.id').' in ('.$db->escape($GroupID).')' )
        );
        $dataReturn = $db->loadObjectList();

        return($dataReturn);


    }


    public function loadUsersActionsTipoModulesFullObject ($UserJosId)
    {


        if( empty($UserJosId) )  return false;
        if( (int)$UserJosId<=0 )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'users.id as user_id'  , 'users.name as users_name'
            ,'action.id AS action_id' , 'action.tagchave AS action_tagchave', 'action.nome AS action_nome'
            ,'tipo.id AS tipo_id'     , 'tipo.tagchave AS tipo_tagchave', 'tipo.nome AS tipo_nome'
            ,'modulo.id AS modulo_id' , 'modulo.tagchave AS modulo_tagchave', 'modulo.nome AS modulo_nome'
            ,'usersaction.id as usersaction_id'  , 'usersaction.permread as pread'    , 'usersaction.permcreate as pcreate'
            ,'usersaction.permdelete as pdelete' ,'usersaction.permupdate as pupdate' , 'usersaction.permreadall as preadall'

            ))
            ->join('LEFT', '#__virtualdesk_perm_usersaction as usersaction ON users.id = usersaction.iduser')
            ->join('LEFT', '#__virtualdesk_perm_action as action ON usersaction.idpermaction = action.id')
            ->join('LEFT OUTER', '#__virtualdesk_perm_tipomodulo as tipomodulo ON action.idpermtipomodulo = tipomodulo.id')
            ->join('LEFT', '#__virtualdesk_perm_tipo as tipo ON tipomodulo.idpermtipo = tipo.id')
            ->join('LEFT', '#__virtualdesk_perm_modulo as modulo ON tipomodulo.idpermmodulo = modulo.id')
            ->from("#__virtualdesk_users as users")
            ->where($db->quoteName('users.idjos').'='.$db->escape($UserJosId) )
        );


        // Dump the query
        //echo $db->getQuery()->dump();

        $dataReturn = $db->loadObjectList();

        return($dataReturn);
    }


    /**
     * Carregar permissões a partir de 1 grupo
     *
     */
    private function loadPluginEnableList ()
    {

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array( 'a.id as id','a.tagchave AS plugin_tagchave','a.enabled AS plugin_enabled'))
            ->from("#__virtualdesk_config_plugin as a")
            ->where('enabled=1')
        );
        $dataReturn = $db->loadAssocList();

        return($dataReturn);


    }


    public function getGroupEmailNameByIdArray($ar_idgroup)
    {
        if( !is_array($ar_idgroup) ) return false;
        if( empty($ar_idgroup) ) return false;
        $setGroupsIds = implode(',',$ar_idgroup);

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nome as name , email')
            ->from("#__virtualdesk_perm_groups")
            ->where($db->quoteName('id') . " in (" . $db->escape($setGroupsIds).')')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    /* Devolve objecto com o nome e mail do Grupo.
       E também com o nome email de todos os utilizadores.
    */
    public function getGrupoOrUsersEmail2Obj ($IdPermGrupo)
    {

        if( empty($IdPermGrupo) )  return false;

        try
        {
            $ObjNomeEmails = array();

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('nome','email'))
                ->from("#__virtualdesk_perm_groups")
                ->where( $db->quoteName('id') . '=' . $db->escape($IdPermGrupo) )
            );
            $dataGrupo = $db->loadObject();

            $db2 = JFactory::getDBO();
            $db2->setQuery($db->getQuery(true)
                ->select(array('a.email as email', 'a.name as nome' ))
                ->from("#__virtualdesk_perm_groups as b")
                ->join("LEFT OUTER", ' #__virtualdesk_perm_groupsusers as c ON c.idpermgroup=b.id' )
                ->join("LEFT OUTER", ' #__virtualdesk_users as a ON a.id=c.iduser' )
                ->where( $db->quoteName('b.id') . '=' . $db->escape($IdPermGrupo).' AND c.iduser>0' )
            );
            $dataGrupoUsers = $db2->loadObjectList();

            $ObjNomeEmails = new stdClass();
            $ObjNomeEmails->grupo = $dataGrupo;
            $ObjNomeEmails->users = $dataGrupoUsers;

            return($ObjNomeEmails);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

}
