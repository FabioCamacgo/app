<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

class VirtualDeskSiteTransitoHelper
{
    const tagchaveModulo = 'transito';

    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.addnewcargadescarga4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewcartaoestacionamento4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewmobilidadereduzida4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewcolocacaosinalizacao4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewaverbamentoalteracaotitular4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewaverbamentosubstituicaoveiculo4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnew2vialicencataxi4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewlicencataxi4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewautrizacaocargadescarga4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewautorizacaocirculacaoviascondicionadas4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewalteracaotransito4user.transito.data', null);
        $app->setUserState('com_virtualdesk.addnewcondicionamentotransito4user.transito.data', null);
    }
}
?>