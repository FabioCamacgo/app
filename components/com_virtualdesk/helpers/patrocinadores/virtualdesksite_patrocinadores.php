<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSitePatrocinadoresLogoFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/patrocinadores/logo_files.php');
    JLoader::register('VirtualDeskTablePatrocinadores', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/patrocinadores.php');
    JLoader::register('VirtualDeskTablePatrocinadoresEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/patrocinadoresEstadoHistorico.php');

    class VirtualDeskSitePatrocinadoresHelper
    {
        const tagchaveModulo = 'patrocinadores';


        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_Patrocinadores_Logo as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
        }


        public static function CheckReferencia($cod){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_Patrocinadores")
                ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        /* Cria novo Evento para o ecrã/permissões do MANAGER */
        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('patrocinadores');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('patrocinadores', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $nomepatrocinador = $data['nomepatrocinador'];
                $urlpatrocinador = $data['urlpatrocinador'];

                $urlHttps = explode("https://", $urlpatrocinador);
                if(count($urlHttps) == 1){
                    $hasLink = 0;
                    $urlHttp = explode("http://", $urlpatrocinador);
                    if(count($urlHttp) == 1){
                        $hasLink = 0;
                    } else {
                        $hasLink = 1;
                    }
                } else {
                    $hasLink = 1;
                }

                if($hasLink == 0){
                    $url = 'https://' . $urlpatrocinador;
                } else {
                    $url = $urlpatrocinador;
                }

                $dataAtual = date("Y-m-d");
                $horaAtual = date('H:i');


                /*Gerar Referencia UNICA de alerta*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $refRandom       = self::random_code();
                    $referencia = 'Patr' . $refRandom;
                    $checkREF = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $resSavePatrocinador = self::saveNewPatrocinador($referencia, $nomepatrocinador, $url);

                if (empty($resSavePatrocinador)) {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSitePatrocinadoresLogoFilesHelper();
                $objCapaFiles->tagprocesso   = 'PATROCINADORES_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_logo');

                if ($resFileSaveCapa===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Patrocinadores_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED') . 'ref=' . $referencia;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewPatrocinador($referencia, $nomepatrocinador, $urlpatrocinador, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','nome','url','estado');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nomepatrocinador)), $db->quote($db->escape($urlpatrocinador)), $db->quote($db->escape('2')));
            $query
                ->insert($db->quoteName('#__virtualdesk_Patrocinadores'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name_pt','estado as name_en') )
                    ->from("#__virtualdesk_Patrocinadores_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $rowName
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-pendente';
                break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }


        public static function getPatrocinadoresList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('patrocinadores', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('patrocinadores','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=patrocinadores&layout=edit4manager&patrocinadores_id=');

                    $table  = " ( SELECT a.id as id, a.id as patrocinadores_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.nome as nome, a.url as url";
                    $table .= " , IFNULL(b.estado, ' ') as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_Patrocinadores as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Patrocinadores_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'codigo',    'dt' => 0 ),
                        array( 'db' => 'nome',      'dt' => 1 ),
                        array( 'db' => 'url',       'dt' => 2 ),
                        array( 'db' => 'estado',    'dt' => 3 ),
                        array( 'db' => 'dummy',     'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',  'dt' => 5 ),
                        array( 'db' => 'id',        'dt' => 6 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstado(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Patrocinadores_Estado")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstadoSelect($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('estado')
                ->from("#__virtualdesk_Patrocinadores_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($id) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeEstado($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Patrocinadores_Estado")
                ->where($db->quoteName('id_estado') . "!='" . $db->escape($id) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getPatrocinadoresDetail4Manager ($IdPatrocinadores)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('patrocinadores');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('patrocinadores', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('patrocinadores'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('patrocinadores','edit4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdPatrocinadores) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as patrocinadores_id', 'a.referencia as codigo', 'a.nome as nome', 'a.url as url',
                        "IFNULL(b.estado,'') as estado", 'a.estado as idestado'))
                    ->join('LEFT', '#__virtualdesk_Patrocinadores_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Patrocinadores as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdPatrocinadores)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('patrocinadores');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('patrocinadores', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $patrocinadores_id = $data['patrocinadores_id'];
            if((int) $patrocinadores_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTablePatrocinadores($db);
            $Table->load(array('id'=>$patrocinadores_id));

            $db->transactionStart();

            try {

                $referencia          = $Table->referencia;
                $obParam             = new VirtualDeskSiteParamsHelper();

                $nome = null;
                $url = null;
                $estado = null;

                if(array_key_exists('nomepatrocinador',$data))      $nomepatrocinador = $data['nomepatrocinador'];
                if(array_key_exists('urlpatrocinador',$data))       $urlpatrocinador = $data['urlpatrocinador'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];

                $urlHttps = explode("https://", $urlpatrocinador);
                if(count($urlHttps) == 1){
                    $hasLink = 0;
                    $urlHttp = explode("http://", $urlpatrocinador);
                    if(count($urlHttp) == 1){
                        $hasLink = 0;
                    } else {
                        $hasLink = 1;
                    }
                } else {
                    $hasLink = 1;
                }

                if($hasLink == 0){
                    $url = 'https://' . $urlpatrocinador;
                } else {
                    $url = $urlpatrocinador;
                }

                /* BEGIN Save Patrocinador */
                $resSavePatrocinador = self::saveEditedPatrocinador4Manager($patrocinadores_id, $nomepatrocinador, $url, $estado);
                /* END Save Patrocinador */

                if (empty($resSavePatrocinador)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objLogoFiles                = new VirtualDeskSitePatrocinadoresLogoFilesHelper();
                $listFileLogo2Eliminar       = $objLogoFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_logo');
                $resFileLogoDelete           = $objLogoFiles->deleteFiles ($referencia , $listFileLogo2Eliminar);
                if ($resFileLogoDelete==false && !empty($listFileLogo2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o logo', 'error');
                }


                // Insere os NOVOS ficheiros
                /* FILES */
                $objLogoInFiles                = new VirtualDeskSitePatrocinadoresLogoFilesHelper();
                $objLogoInFiles->tagprocesso   = 'PATROCINADORES_LOGO_POST';
                $objLogoInFiles->idprocesso    = $referencia;
                $resFileSaveLogo             = $objLogoInFiles->saveListFileByPOST('fileupload_logo');


                if ($resFileSaveLogo===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Patrocinadores_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED') . 'ref=' . $referencia;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedPatrocinador4Manager($patrocinadores_id, $nome, $url, $estado){
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('patrocinadores');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('patrocinadores', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($nome))  array_push($fields, 'nome="'.$db->escape($nome).'"');
            if(!is_null($url))  array_push($fields, 'url="'.$db->escape($url).'"');
            if(!is_null($estado))  array_push($fields, 'estado="'.$db->escape($estado).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Patrocinadores')
                ->set($fields)
                ->where(' id = '.$db->escape($patrocinadores_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.patrocinadores.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.patrocinadores.data', null);
            $app->setUserState('com_virtualdesk.edit4user.patrocinadores.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.patrocinadores.data', null);

        }
    }

?>