<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskTableEnterprise', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/enterprise.php');
JLoader::register('VirtualDeskTableEnterpriseFile', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/enterprise_file.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('VirtualDeskTableEnterpriseAreaAct', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/enterprise_areaact.php');
//JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');

/**
 * VirtualDesk Enterprise helper class.
 * @since  1.6
 */
class VirtualDeskSiteEnterpriseHelper
{

    public static function getEnterpriseListCount ($UserJoomlaID)
    {
        if( empty($UserJoomlaID) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id'))
                ->from("#__virtualdesk_enterprise")
                ->where($db->quoteName('iduserjos').'='.$db->escape($UserJoomlaID) )
            );
            $db->execute();
            $num_rows = $db->getNumRows();

            return($num_rows);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEnterpriseList ($UserJoomlaID)
    {
        if( empty($UserJoomlaID) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as enterprise_id','iduser','iduserjos','nipc','firmname','name','email','address','postalcode','enterpriseavatar','phone1','phone2','website','facebook','freguesia','localidade','areaact'))
                ->from("#__virtualdesk_enterprise")
                ->where($db->quoteName('iduserjos').'='.$db->escape($UserJoomlaID) )
            );
            $dataReturn = $db->loadObjectList();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEnterpriseDetail ($UserJoomlaID, $IdEnterprise)
    {
        if( empty($UserJoomlaID) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as enterprise_id','iduser','iduserjos','nipc','firmname','name','email','address','postalcode','enterpriseavatar','phone1','phone2','website','facebook','freguesia','localidade','areaact'))
                ->from("#__virtualdesk_enterprise")
                ->where($db->quoteName('iduserjos') . '=' . $db->escape($UserJoomlaID) . ' AND ' . $db->quoteName('id') . '=' . $db->escape($IdEnterprise))
            );



            $dataReturn = $db->loadObject();

            // comparar dados serializados com explode/implode com dados carregados têm de ser iguais
            $ComparaSerializeAreaAct = self::getEnterpriseAreaActListSelected ($UserJoomlaID, $IdEnterprise);
            asort($ComparaSerializeAreaAct);
            $ComparaSerializeAreaAct = implode('|',$ComparaSerializeAreaAct);

            if($ComparaSerializeAreaAct!= $dataReturn->areaact )  return false;

            $FieldSerializaAreaAct   = explode('|',$dataReturn->areaact);
            if (!empty($dataReturn->areaact)) $dataReturn->areaact = $FieldSerializaAreaAct;

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEnterpriseDetailRawData ($UserJoomlaID, $IdEnterprise)
    {
        if( empty($UserJoomlaID) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as enterprise_id','iduser','iduserjos','nipc','firmname','name','email','address','postalcode','enterpriseavatar','phone1','phone2','website','facebook','freguesia','localidade','areaact'))
                ->from("#__virtualdesk_enterprise")
                ->where($db->quoteName('iduserjos') . '=' . $db->escape($UserJoomlaID) . ' AND ' . $db->quoteName('id') . '=' . $db->escape($IdEnterprise))
            );
            $dataReturn = $db->loadObject();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Carrega lista de TODAS áreas de atuação para serem apresentadas
    */
    public static function getEnterpriseAreaActListAllByGroup ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id as id','a.idgroup as idgroup','a.name_pt as name_pt','a.name_en as name_en','b.groupname_pt as groupname_pt','b.groupname_en as groupname_en') )
                ->join('LEFT', '#__virtualdesk_areaact_group AS b ON b.id = a.idgroup')
                ->from("#__virtualdesk_areaact as a")
                ->order('a.idgroup')
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                // Add Area Act Group
                $rowGroupName   = '';
                $rowAreaActName = '';

                if (empty($response[$row->idgroup])) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowGroupName = $row->groupname_pt;
                    }
                    else {
                        $rowGroupName = $row->groupname_en;
                    }
                $response[$row->idgroup]['group'] = array(
                    'idgroup' => $row->idgroup,
                    'groupname' => $rowGroupName,
                );
                }

               // Add Area Act
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowAreaActName = $row->name_pt;
                }
                else {
                    $rowAreaActName = $row->name_en;
                }

              $response[$row->idgroup]['rows'][] =  array (
                'id' => $row->id,
                'name' => $rowAreaActName
                );
            }

            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Carrega lista das áreas de atuação associadas a uma empresa
    */
    public static function getEnterpriseAreaActListSelected ($UserJoomlaID, $IdEnterprise)
    {
        if( empty($UserJoomlaID) )  return false;
        if( empty($IdEnterprise) )  return false;
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','identerprise','idareaact') )
                ->from("#__virtualdesk_enterprise_areaact")
                ->where($db->quoteName('identerprise') . '=' . $db->escape($IdEnterprise))
            );
            $dataReturn = $db->loadAssocList();
            $dataList = array();
            foreach( $dataReturn as $keyList )
            {
                $dataList[] = $keyList['idareaact'];
            }

            return($dataList);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
   * Carrega lista das áreas de atuação associadas a uma empresa
   */
    public static function getEnterpriseAreaActListSelectName ($data, $lang)
    {
        if( empty($data) )  return false;
        if( empty($data->enterprise_id) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.identerprise','a.idareaact','b.name_pt as name_pt','b.name_en as name_en') )
                ->join('LEFT', '#__virtualdesk_areaact AS b ON b.id = a.idareaact')
                ->from("#__virtualdesk_enterprise_areaact as a")
                ->where($db->quoteName('identerprise') . '=' . $db->escape($data->enterprise_id))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                // Add Area Act Group
                $rowAreaActName = '';
                // Add Area Act
                if( empty($lang) or ($lang='pt_PT') ) {
                    $response[] = $row->name_pt;
                }
                else {
                    $response[] = $row->name_en;
                }
            }

            return($response);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }





    /*
    * Check if the NIPC exists for another user (in Joomla and in VD)
    */
    public static function checkIfNIPCisDuplicate($UserJoomlaID, $NIPC)
    {
        if((int)$UserJoomlaID <=0 ) return false;
        if((string)$NIPC=='') return false;

        // Verifica se o username existe noutro utilizador no VirtualDesk users...
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('iduserjos'))
            ->from("#__virtualdesk_enterprise")
            ->where($db->quoteName('nipc').'=\''.$db->escape($NIPC).'\' AND '. $db->quoteName('iduserjos').'<>'.$db->escape($UserJoomlaID) .' Limit 1' )
        );
        $isDuplicate = $db->loadResult();

        if((int)$isDuplicate > 0 && (int)$isDuplicate<>(int)$UserJoomlaID) return true;

        return false;
    }


    /*
    * Faz a gravação de uma empresa de um utilizador (update)
    */
    public static function updateEnterprise($UserJoomlaID, $UserVDId, $data)
    {
        $IdEnterprise = $data['enterprise_id'];
        if((int) $IdEnterprise <=0 ) return false;
        $data['id'] = $IdEnterprise;
        unset($data['enterprise_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $TableEnterprise = new VirtualDeskTableEnterprise($db);
        $TableEnterprise->load(array('id'=>$IdEnterprise, 'iduser'=>$UserVDId, 'iduserjos'=>$UserJoomlaID));
        if((integer)$TableEnterprise->iduserjos !== (integer)$UserJoomlaID ) return false;

        if( !empty($data['email']) )  $data['email'] = JStringPunycode::emailToPunycode($data['email']);



        // UPDATE: Avatar
        // Se tem um ficheiro de logo associado então...  Retira o "files" do $data , faz o upload da foto e coloca no bind
        $AvatarPreviousFileName = '';
        $avatarFileName         = VirtualDeskSiteFileUploadHelper::saveGenericAvatarFileToFolder($UserJoomlaID, $IdEnterprise, $data['enterpriseavatar']);
        if($avatarFileName === false) {
            $data['enterpriseavatar'] = '';
            return false;
        }
        else if((string)$avatarFileName != '') {
            $AvatarPreviousFileName = $TableEnterprise->enterpriseavatar;
        }
        unset($data['enterpriseavatar']);
        if((string)$avatarFileName != '')  $data['enterpriseavatar'] =  $avatarFileName;

        $dataReturnAreaAct = self::saveEnterpriseAreaAct ($UserJoomlaID, $IdEnterprise, $data);
        if($dataReturnAreaAct===false) return false;
        if( $dataReturnAreaAct!==true && !empty((string)$dataReturnAreaAct) ) $data['areaact'] = $dataReturnAreaAct;


        //Poder anexar vários documentos ou imagens a um pedido
        // Se tem um ou mais ficheiros então...
        if ( !empty($data['attachment']) ) {
            $FileNameList = VirtualDeskSiteFileUploadHelper::saveFileListToFolder ( $UserJoomlaID , $data['attachment'] , JComponentHelper::getParams('com_virtualdesk')->get('enterprise_filefolder') );
            if($FileNameList === false) {
                $data['attachment'] = '';
                return false;
            }
            else if (!empty($FileNameList)) {
                // Vai colocar os ficheiros associados ao
                $resFiles2Table = self::saveFileListToTable($FileNameList, $data);
                if($resFiles2Table === false) {
                    $data['attachment'] = '';
                    return false;
                }
            }
        }
        unset($data['attachment']);


        // Verifica se é para atualizar ficheiros nome/descrição: atualiza na base de dados
        if ( !empty($data['attachmentUPDATE']) ) {
            // Apaga da BD
            $FileNameList = self::updateFileListFromTable ($UserJoomlaID, $data['attachmentUPDATE'], $data );
            if($FileNameList === false) {
                $data['attachmentUPDATE'] = '';
                return false;
            }
        }
        unset($data['attachmentUPDATE']);


        // Verifica se é para eliminar ficheiros: depois de apagar da base de dados deve apagar do servidor
        if ( !empty($data['attachmentDELETE']) ) {
            // Apaga da BD
            $FileNameList = self::removeFileListFromTable ($UserJoomlaID, $data['attachmentDELETE'], $data );
            if($FileNameList === false) {
                $data['attachmentDELETE'] = '';
                return false;
            }
        }
        unset($data['attachmentDELETE']);



        // Bind the data.
        if (!$TableEnterprise->bind($data)) return false;

        // Store the data.
        if (!$TableEnterprise->save($data)) return false;


        // Se correu tudo bem e alterou o ficheiro da foto/avatar pode eliminar o ficheiro anterior
        if( $AvatarPreviousFileName!=='') VirtualDeskSiteFileUploadHelper::deleteUserAvatarFile ($AvatarPreviousFileName);

        return true;
    }


    /*
    * Faz a gravação de uma empresa de um utilizador (update)
    */
    public static function createEnterprise($UserJoomlaID, $UserVDId, $data)
    {

        $db    = JFactory::getDbo();
        $TableEnterprise = new VirtualDeskTableEnterprise($db);

        if (empty((integer)$UserJoomlaID) ) return false;
        if (empty((integer)$UserVDId) ) return false;
        if (empty($data) ) return false;

        if( !empty($data['email']) )  $data['email'] = JStringPunycode::emailToPunycode($data['email']);

        // Associa ao novo registo os Ids do utilizador
        $data['iduser']    = $UserVDId;
        $data['iduserjos'] = $UserJoomlaID;


        // UPDATE: Avatar
        // Se tem um ficheiro de logo associado então...  Retira o "files" do $data , faz o upload da foto e coloca no bind
        $AvatarPreviousFileName = '';
        $avatarFileName         = VirtualDeskSiteFileUploadHelper::saveGenericAvatarFileToFolder($UserJoomlaID, rand(1,100), $data['enterpriseavatar']);
        if($avatarFileName === false) {
            $data['enterpriseavatar'] = '';
            return false;
        }
        else if((string)$avatarFileName != '') {
            $AvatarPreviousFileName = $TableEnterprise->enterpriseavatar;
        }
        unset($data['enterpriseavatar']);
        if((string)$avatarFileName != '')  $data['enterpriseavatar'] =  $avatarFileName;


        //Poder anexar vários documentos ou imagens a um pedido
        // Se tem um ou mais ficheiros então...
        if ( !empty($data['attachment']) ) {
            $FileNameList = VirtualDeskSiteFileUploadHelper::saveFileListToFolder ( $UserJoomlaID , $data['attachment'] , JComponentHelper::getParams('com_virtualdesk')->get('enterprise_filefolder') );
            if($FileNameList === false) {
                $data['attachment'] = '';
                return false;
            }
            else if (!empty($FileNameList)) {
                // Vai colocar os ficheiros associados ao
                $resFiles2Table = self::saveFileListToTable($FileNameList, $data);
                if($resFiles2Table === false) {
                    $data['attachment'] = '';
                    return false;
                }
            }
        }
        unset($data['attachment']);


        // Verifica se é para eliminar ficheiros: depois de apagar da base de dados deve apagar do servidor
        if ( !empty($data['attachmentUPDATE']) ) {
            // Apaga da BD
            $FileNameList = self::updateFileListFromTable ($UserJoomlaID, $data['attachmentUPDATE'], $data );
            if($FileNameList === false) {
                $data['attachmentUPDATE'] = '';
                return false;
            }
        }
        unset($data['attachmentUPDATE']);


        // Verifica se é para eliminar ficheiros: depois de apagar da base de dados deve apagar do servidor
        if ( !empty($data['attachmentDELETE']) ) {
            // Apaga da BD
            $FileNameList = self::removeFileListFromTable ($UserJoomlaID, $data['attachmentDELETE'], $data );
            if($FileNameList === false) {
                $data['attachmentDELETE'] = '';
                return false;
            }
        }
        unset($data['attachmentDELETE']);


        // Bind the data.
        if (!$TableEnterprise->bind($data)) return false;
        // Store the data.
        if (!$TableEnterprise->save($data)) return false;

        $GetCreateEnterpriseId = $TableEnterprise->id;
        if( empty($GetCreateEnterpriseId) ) return false;

        $dataReturnAreaAct = self::saveEnterpriseAreaAct ($UserJoomlaID, $GetCreateEnterpriseId , $data);
        if($dataReturnAreaAct===false) return false;
        unset($data);

        // Grava novamente apenas a areaact...
        if($dataReturnAreaAct !==true ) $data['areaact'] = $dataReturnAreaAct;

        // Bind the data.
        if (!$TableEnterprise->bind($data)) return false;
        // Store the data.
        if (!$TableEnterprise->save($data)) return false;

        // Se correu tudo bem e alterou o ficheiro da foto/avatar pode eliminar o ficheiro anterior
        if( $AvatarPreviousFileName!=='') VirtualDeskSiteFileUploadHelper::deleteUserAvatarFile ($AvatarPreviousFileName);

        return true;
    }



    /*
    * Carrega lista das áreas de atuação associadas a uma empresa
    */
    public static function saveEnterpriseAreaAct ($UserJoomlaID, $IdEnterprise, $data)
    {
        if( empty($UserJoomlaID) )  return false;
        if( empty($IdEnterprise) )  return false;
        if( !array_key_exists('areaact',$data) )  return true;  // não foram alterados os dados...

        $dataReturn = "";
        try
        {
            $db = JFactory::getDBO();

            if(!empty($data['areaact']))
            {   $queryDel = $db->getQuery(true);
                $conditions = array(
                    $db->quoteName('identerprise') . ' = ' . $db->escape($IdEnterprise)
                );
                $queryDel->delete($db->quoteName('#__virtualdesk_enterprise_areaact'));
                $queryDel->where($conditions);
                $db->setQuery($queryDel);
                if (!$db->execute()) return false;

                // Create a new query object.
                $queryIns = $db->getQuery(true);
                // Insert columns.
                $columns = array('identerprise', 'idareaact');
                // Insert values.
                $queryIns
                    ->insert($db->quoteName('#__virtualdesk_enterprise_areaact'))
                    ->columns($db->quoteName($columns));
                foreach($data['areaact'] as $keyDataArea )
                {
                    $values = $IdEnterprise.',' . $keyDataArea;
                    $queryIns->values($values);
                }

                // Set the query using our newly populated query object and execute it.
                $db->setQuery($queryIns);

                // Dump the query
                //echo $db->getQuery()->dump();

                if (!$db->execute()) return false;

                asort($data['areaact']);

                $dataReturn = implode('|', $data['areaact']);
            }
            else
            {   // não tem nenhuma escolha... se carregou dados das AreasAct então devem ser eliminados
                if(empty($dataReturn)) {
                    $query = $db->getQuery(true);
                    $conditions = array(
                        $db->quoteName('identerprise') . ' = ' . $db->escape($IdEnterprise)
                    );
                    $query->delete($db->quoteName('#__virtualdesk_enterprise_areaact'));
                    $query->where($conditions);
                    $db->setQuery($query);

                    if (!$db->execute()) return false;
                }
            }

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Carrega lista ficheiros de um registo
    */
    public static function getEnterpriseFileListById ($UserJoomlaID, $EnterpriseId)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.identerprise','a.desc','a.basename','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->join('LEFT', '#__virtualdesk_enterprise AS b ON b.id = a.identerprise')
                ->from("#__virtualdesk_enterprise_file AS a")
                ->where($db->quoteName('a.identerprise') . '=' . $db->escape($EnterpriseId) . ' AND ' . $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID) )
            );


            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
   * Carrega ficheiro associado a um registo
   */
    public static function getEnterpriseFileNameById ($UserJoomlaID, $EnterpriseId, $basename)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.identerprise','a.basename','a.desc','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->join('LEFT', '#__virtualdesk_enterprise AS b ON b.id = a.identerprise')
                ->from("#__virtualdesk_enterprise_file AS a")
                ->where($db->quoteName('a.identerprise') . '=' . $db->escape($EnterpriseId)
                    . ' AND ' . $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID)
                    . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
            );


            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /**
     * Carrega os ficheiros associados ao registo e apresenta de acordo com o tipo de ficheiro o icon
     * O ideal será escolher entre uma disposição em lista ou em tabela para os ficheiros associados
     * Apresenta o icon ou o preview se for imagem, o nome, o tamanho e a data ( de modificação)
     */
    public static function getEnterpriseFilesLayout ($data)
    {
        if(empty($data->enterprise_id)) return false;
        // carregar a lista de ficheiros, com os dados e os icons respectivos
        $userReqFileList = self::getEnterpriseFileListById (VirtualDeskSiteUserHelper::getUserSessionId(), $data->enterprise_id);
        $userReqFileList = VirtualDeskSiteFileUploadHelper::setFileListIconsOrPreview ($userReqFileList);
        return($userReqFileList);
    }



    public static function saveFileListToTable ($FileList, $dataSent )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';

        foreach ($FileList as $rowFile)
        {
            $IdEnterprise = $dataSent['id'];
            if((int) $IdEnterprise <=0 ) return false;
            $data['identerprise'] = $IdEnterprise;
            $data['filename']    = $rowFile['filename'];
            $data['basename']    = $rowFile['basename'];
            $data['ext']         = $rowFile['ext'];
            $data['type']        = $rowFile['type'];
            $data['size']        = $rowFile['size'];
            $data['desc']        = $rowFile['desc'];
            $data['filepath']    = $rowFile['filepath'];

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $TableFile = new VirtualDeskTableEnterpriseFile($db);

            // Bind the data.
            if (!$TableFile->bind($data)) return false;

            // Store the data.
            if (!$TableFile->save($data)) return false;



        }

        return true;
    }



    public static function removeFileListFromTable ($UserJoomlaID, $FileList, $dataSent )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';

        foreach ($FileList as $basename)
        {
            $IdEnterprise = $dataSent['id'];
            if((int) $IdEnterprise <=0 ) return false;

            try
            {   $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('a.id','a.basename','a.filename','a.filepath') )
                    ->join('LEFT', '#__virtualdesk_enterprise AS b ON b.id = a.identerprise')
                    ->from("#__virtualdesk_enterprise_file AS a")
                    ->where($db->quoteName('a.identerprise') . '=' . $db->escape($IdEnterprise)
                        . ' AND ' . $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID)
                        . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
                );
                $dataReturn = $db->loadObject();

                if($dataReturn->basename == $basename) {
                    $db = JFactory::getDBO();
                    $TableFile = new VirtualDeskTableEnterpriseFile($db);
                    $resDelete = $TableFile->delete($dataReturn->id);
                    if (!$resDelete ) return($resDelete);
                    $resDelete = VirtualDeskSiteFileUploadHelper::deleteFileFromFolder($dataReturn->filepath, $dataReturn->filename);
                    if (!$resDelete ) return($resDelete);
                }
                else {
                    return false;
                }

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }
        return true;
    }


    public static function updateFileListFromTable ($UserJoomlaID, $FileList, $dataSent )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';

        foreach ($FileList as $basename => $filenameupdate)
        {
            $IdEnterprise = $dataSent['id'];
            if((int) $IdEnterprise <=0 ) return false;

            try
            {   $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('a.id','a.basename','a.filename','a.filepath') )
                    ->join('LEFT', '#__virtualdesk_enterprise AS b ON b.id = a.identerprise')
                    ->from("#__virtualdesk_enterprise_file AS a")
                    ->where($db->quoteName('a.identerprise') . '=' . $db->escape($IdEnterprise)
                        . ' AND ' . $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID)
                        . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
                );
                $dataReturn = $db->loadObject();

                // Dump the query
                //echo $db->getQuery()->dump();

                if($dataReturn->basename == $basename) {
                    $db = JFactory::getDBO();
                    $data = array();
                    $data['id']   = $dataReturn->id;
                    $data['desc'] = $filenameupdate;

                    // Carrega dados atuais na base de dados
                    $db    = JFactory::getDbo();
                    $TableFile = new VirtualDeskTableEnterpriseFile($db);
                    // Bind the data.
                    if (!$TableFile->bind($data)) return false;
                    // Store the data.
                    if (!$TableFile->save($data)) return false;
                }
                else {
                    return false;
                }

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }
        return true;
    }



}