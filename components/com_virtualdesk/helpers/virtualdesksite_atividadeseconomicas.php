<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

class VirtualDeskSiteAtividadesEconomicasHelper
{
    const tagchaveModulo = 'atividadeseconomicas';

    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.addnew2vialicencaguardanoturno4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewatribuicaoespaco4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewautorizacaoatividades4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewcandidaturalicenca4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewcessacaoatividade4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewcriacaoextincaoservico4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewdesistenciaespaco4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewfixacaoareasatuacao4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewlicenciamentoarlivre4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewlicenciamentorecintodiversao4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewlicenciamentorecintoimprovisado4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewlicenciamentorecintointenerante4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewprovaanualcumprimento4user.atividadeseconomicas.data', null);
        $app->setUserState('com_virtualdesk.addnewrenovacaolicencaatividade4user.atividadeseconomicas.data', null);
    }
}
?>