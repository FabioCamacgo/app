<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskTableContactUs', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/contactus.php');
//JLoader::register('VirtualDeskTableContactUsFile', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/contactus_file.php');
//JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
//JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/**
 * VirtualDesk ContactUs helper class.
 * @since  1.6
 */
class VirtualDeskSiteContactUsReportsHelper
{


    /*
    * $vbForDataTables: se for true, vai carrega o JSON para os datatables
    */
    public function getContactUsReportResume ($UserJoomlaID)
    {

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkReportAccess('contactus','resume');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_pt';
            }
            else {
                $CatName = 'name_en';
            }


                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                $table  = " ( SELECT COUNT(Distinct a.id) as Conta, ";
                $table .= " IFNULL(b.".$CatName.", ' ') as estado, IFNULL(a.idestado, '0')  as idestado";
                $table .= " FROM ".$dbprefix."virtualdesk_contactus as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_contactus_estado AS b  ON b.id = a.idestado ";
                $table .= " GROUP BY a.idestado ) temp ";

                $primaryKey = 'idestado';

                $columns = array(
                    array( 'db' => 'estado',      'dt' => 0 ),
                    array( 'db' => 'Conta',       'dt' => 1 ),
                    array( 'db' => 'idestado',    'dt' => 2 ),
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getContactUsPedidosPorCategoria ($UserJoomlaID)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkReportAccess('contactus','resume');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_pt';
            }
            else {
                $CatName = 'name_en';
            }


            $conf = JFactory::getConfig();

            $host = $conf->get('host');
            $user = $conf->get('user');
            $password = $conf->get('password');
            $database = $conf->get('db');
            $dbprefix = $conf->get('dbprefix');

            $table  = " ( SELECT COUNT(Distinct a.id) as Conta, ";
            $table .= " IFNULL(b.".$CatName.", ' ') as categoria, IFNULL(a.idcategory, '0')  as idcategory";
            $table .= " FROM ".$dbprefix."virtualdesk_contactus as a ";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_contactus_category AS b  ON b.id = a.idcategory ";
            $table .= " GROUP BY a.idcategory ) temp ";

            $primaryKey = 'idcategory';

            $columns = array(

                array( 'db' => 'categoria',  'dt' => 0 ),
                array( 'db' => 'Conta',      'dt' => 1 ),
                array( 'db' => 'idcategory', 'dt' => 2 ),
            );

             $sql_details = array(
                'user' => $user,
                'pass' => $password,
                'db'   => $database,
                'host' => $host
            );

            $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

            return $data;

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
        *
        */
    public function getContactUsDashboard ($UserJoomlaID, $setLimitSQL=-1)
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte dos relatórios


        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_pt';
            }
            else {
                $CatName = 'name_en';
            }

            $conf = JFactory::getConfig();

            $host = $conf->get('host');
            $user = $conf->get('user');
            $password = $conf->get('password');
            $database = $conf->get('db');
            $dbprefix = $conf->get('dbprefix');


            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.id) as npedidos, YEAR(a.created) as year , MONTH(a.created) as mes ";
            $query .= " FROM #__virtualdesk_contactus as a ";
            $query .= " LEFT JOIN #__virtualdesk_contactus_category AS b  ON b.id = a.idcategory ";
            $query .= " GROUP BY YEAR(a.created), MONTH(a.created)";


            $db->setQuery($query);

            $dataReturn = $db->loadObjectList();

            return($dataReturn);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getContactUsTotaisPorCategoria ($UserJoomlaID, $setLimitSQL=-1)
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte dos relatórios


        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_pt';
            }
            else {
                $CatName = 'name_en';
            }

            $conf = JFactory::getConfig();

            $host = $conf->get('host');
            $user = $conf->get('user');
            $password = $conf->get('password');
            $database = $conf->get('db');
            $dbprefix = $conf->get('dbprefix');


            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.id) as npedidos, IFNULL(b.".$CatName.", ' ') as categoria, IFNULL(a.idcategory, '0') as idcategory ";
            $query .= " FROM #__virtualdesk_contactus as a ";
            $query .= " LEFT JOIN #__virtualdesk_contactus_category AS b  ON b.id = a.idcategory ";
            $query .= " GROUP BY a.idcategory";

            $db->setQuery($query);

            $dataReturn = $db->loadObjectList();

            return($dataReturn);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getContactUsTotaisPorEstado ($UserJoomlaID, $setLimitSQL=-1)
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte dos relatórios


        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_pt';
            }
            else {
                $CatName = 'name_en';
            }

            $conf = JFactory::getConfig();

            $host = $conf->get('host');
            $user = $conf->get('user');
            $password = $conf->get('password');
            $database = $conf->get('db');
            $dbprefix = $conf->get('dbprefix');


            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.id) as npedidos, IFNULL(b.".$CatName.", ' ') as estado, IFNULL(a.idestado, '0') as idestado ";
            $query .= " FROM #__virtualdesk_contactus as a ";
            $query .= " LEFT JOIN #__virtualdesk_contactus_estado AS b  ON b.id = a.idestado ";
            $query .= " GROUP BY a.idestado";

            $db->setQuery($query);

            $dataReturn = $db->loadObjectList();

            return($dataReturn);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


}