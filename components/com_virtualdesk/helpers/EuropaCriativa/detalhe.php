<?php

    class VirtualDeskSiteDetalheHelper{

        public static function getInfoProjeto($referencia){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.projeto, a.designacao, b.programa as programa, a.entidade, a.lider, a.pais_lider, c.ano as ano_Apoio, d.linha as linha_Financiamento, e.distrito as distrito, f.concelho as concelho, a.tipologia, a.link, a.data_Inicio, a.data_Fim, a.call, a.valorFinanciamento, a.layout'))
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_programa AS b ON b.id = a.programa')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_anos AS c ON c.id = a.ano_Apoio')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_linhasFinanciamento AS d ON d.id = a.linha_Financiamento')
                ->join('LEFT', '#__virtualdesk_digitalGov_distritos AS e ON e.id = a.distrito')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS f ON f.id = a.concelho')
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
                ->order('a.data_Fim DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getImgPrograma($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_EuropaCriativa_files")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getIDProg($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('programa') . "='" . $db->escape($programa) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getTextTipologia($tipologia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('tipologia')
                ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                ->where($db->quoteName('id') . "='" . $db->escape($tipologia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getIDcat($linha){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('linha') . "='" . $db->escape($linha) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getCatApoio($idcat){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_EuropaCriativa_categorias")
                ->where($db->quoteName('id') . "='" . $db->escape($idcat) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getPaisLider($idPais){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('indicativo')
                ->from("#__virtualdesk_EuropaCriativa_paises")
                ->where($db->quoteName('id') . "='" . $db->escape($idPais) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getParceirosPrograma($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.parceiro, b.pais as pais'))
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_paises AS b ON b.id = a.pais')
                ->from("#__virtualdesk_EuropaCriativa_Parceiros as a")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
                ->order('a.parceiro ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

    }

?>