<?php

    class VirtualDeskSiteFiltroHelper{

        public static function getValorMedia(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, valorFinanciamento'))
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados")
                ->where($db->quoteName('programa') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getValorCultura(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, valorFinanciamento'))
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados")
                ->where($db->quoteName('programa') . "='" . $db->escape('1') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getNumProjetosMedia(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, valorFinanciamento'))
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados")
                ->where($db->quoteName('programa') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAllEntidadesCultura(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, entidade'))
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados")
                ->where($db->quoteName('programa') . "='" . $db->escape('1') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAno(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnoAJAX(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano as name'))
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->order('name DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getAnoName($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('ano')
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->where($db->quoteName('id') . "='" . $db->escape($ano) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeAno($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->where($db->quoteName('id') . "!='" . $db->escape($ano) . "'")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistritos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distrito'))
                ->from("#__virtualdesk_digitalGov_distritos")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistritosName($distrito){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('distrito')
                ->from("#__virtualdesk_digitalGov_distritos")
                ->where($db->quoteName('id') . "='" . $db->escape($distrito) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeDistritos($distrito){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distrito'))
                ->from("#__virtualdesk_digitalGov_distritos")
                ->where($db->quoteName('id') . "!='" . $db->escape($distrito) . "'")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelhos($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('concelho ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho as name'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getConcelhoName($concelhos){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelhos) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeConcelho($distrito, $concelhos){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distrito) . "'")
                ->where($db->quoteName('concelho') . "!='" . $db->escape($concelhos) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPrograma(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, programa'))
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->order('programa ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getProgramaAJAX(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, programa as name'))
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getProgramaName($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('programa')
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('id') . "='" . $db->escape($programa) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getIDProg($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('programa') . "='" . $db->escape($programa) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludePrograma($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, programa'))
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('id') . "!='" . $db->escape($programa) . "'")
                ->order('programa ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCategoria(/*$onAjaxVD=0*/)
        {
            /*if($onAjaxVD=0) {*/
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria'))
                    ->from("#__virtualdesk_EuropaCriativa_categorias")
                    ->order('categoria ASC')
                );
                $data = $db->loadAssocList();
            /*} else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name'))
                    ->from("#__virtualdesk_EuropaCriativa_categorias")
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }*/
            return ($data);
        }

        public static function getCategoriaName($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_EuropaCriativa_categorias")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeCategoria($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_EuropaCriativa_categorias")
                ->where($db->quoteName('id') . "!='" . $db->escape($categoria) . "'")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getLinhasFinanciamento($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('programa') . '=' . $db->escape($idwebsitelist))
                    ->order('linha ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha as name'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('programa') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getLinhasFinanciamentoByCat($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('categoria') . '=' . $db->escape($idwebsitelist))
                    ->order('linha ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha as name'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('categoria') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getLinhasFinanciamentoName($linhaFinanciamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('linha')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('id') . "='" . $db->escape($linhaFinanciamento) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeLinhasFinanciamento($programa, $linhaFinanciamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, linha'))
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('programa') . "='" . $db->escape($programa) . "'")
                ->where($db->quoteName('linha') . "!='" . $db->escape($linhaFinanciamento) . "'")
                ->order('linha ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getIDCategoriaApoio($idwebsitelist){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('id') . "='" . $db->escape($idwebsitelist) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getCategoriaApoio($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria'))
                    ->from("#__virtualdesk_EuropaCriativa_categorias")
                    ->where($db->quoteName('id') . '=' . $db->escape($idwebsitelist))
                    ->order('linha ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name'))
                    ->from("#__virtualdesk_EuropaCriativa_categorias")
                    ->where($db->quoteName('id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getTipologia()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tipologia'))
                ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                ->order('tipologia ASC')
            );
            $data = $db->loadAssocList();

            return ($data);
        }

        public static function getTipologiaName($tipologia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('tipologia')
                ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                ->where($db->quoteName('id') . "='" . $db->escape($tipologia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeTipologia($tipologia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tipologia'))
                ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                ->where($db->quoteName('id') . "!='" . $db->escape($tipologia) . "'")
                ->order('tipologia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getProjetosFilter(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.projeto, a.designacao, b.programa as programa, a.entidade, a.lider, a.pais_lider, a.ano_Apoio as id_ano, c.ano as ano_Apoio, d.linha as linha_Financiamento, e.distrito as distrito, f.concelho as concelho, a.tipologia, a.link, a.data_Inicio, a.data_Fim, a.call'))
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_programa AS b ON b.id = a.programa')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_anos AS c ON c.id = a.ano_Apoio')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_linhasFinanciamento AS d ON d.id = a.linha_Financiamento')
                ->join('LEFT', '#__virtualdesk_digitalGov_distritos AS e ON e.id = a.distrito')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS f ON f.id = a.concelho')
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('ano_Apoio DESC, projeto ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getIDcat($linha){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('linha') . "='" . $db->escape($linha) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getIdLinha($linha_Financiamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('linha') . "='" . $db->escape($linha_Financiamento) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getCatApoio($idcat){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_EuropaCriativa_categorias")
                ->where($db->quoteName('id') . "='" . $db->escape($idcat) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getTextTipologia($tipologia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('tipologia')
                ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                ->where($db->quoteName('id') . "='" . $db->escape($tipologia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getRefs($pesquisaLivre){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','referencia'))
                ->from("#__virtualdesk_EuropaCriativa_Parceiros")
                ->where ($db->quoteName('parceiro') . "LIKE '" . $db->escape('%' . $pesquisaLivre .'%') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getItemFromPartner($ref){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.projeto, a.designacao, b.programa as programa, a.entidade, a.lider, a.pais_lider, c.ano as ano_Apoio, d.linha as linha_Financiamento, a.tipologia, a.link, a.data_Inicio, a.data_Fim, a.call'))
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_programa AS b ON b.id = a.programa')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_anos AS c ON c.id = a.ano_Apoio')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_linhasFinanciamento AS d ON d.id = a.linha_Financiamento')
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                ->where($db->quoteName('a.referencia') . "='" . $db->escape($ref) . "'")
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getProjetosFilteredPartner($item, $progFiltro, $finan, $anoFiltro, $DataInicioFormated, $DataFimFormated, $tipologiaID, $categoria){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('0') . "'";

            if(!empty($progFiltro)){
                $where .= 'AND' . $db->quoteName('a.programa') . "='" . $db->escape($progFiltro) . "'";
            }

            if(!empty($finan)){
                $where .= 'AND' . $db->quoteName('a.linha_Financiamento') . "='" . $db->escape($finan) . "'";
            }

            if(!empty($anoFiltro)){
                $where .= 'AND' . $db->quoteName('a.ano_Apoio') . "='" . $db->escape($anoFiltro) . "'";
            }

            if(!empty($tipologiaID) && $tipologiaID != 'NULL'){
                $where .= 'AND' . $db->quoteName('a.tipologia') . "LIKE'" . $db->escape($tipologiaID .'%') . "'";
            }

            if(!empty($categoria) && $categoria != '0'){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($DataInicioFormated) && !empty($DataFimFormated)){
                $where .= 'AND' . $db->quoteName('a.data_Inicio') . ">='" . $db->escape($DataInicioFormated) . "'";
                $where .= 'AND' . $db->quoteName('a.data_Fim') . "<='" . $db->escape($DataFimFormated) . "'";
            } else if(!empty($DataInicioFormated) && empty($DataFimFormated)){
                $where .= 'AND' . $db->quoteName('a.data_Inicio') . ">='" . $db->escape($DataInicioFormated) . "'";
            } else if(empty($DataInicioFormated) && !empty($DataFimFormated)){
                $where .= 'AND' . $db->quoteName('a.data_Fim') . "<='" . $db->escape($DataFimFormated) . "'";
            }

            if(!empty($item)){
                $where .= 'AND' . $db->quoteName('a.referencia') . "='" . $db->escape($item) . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select('a.referencia')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_programa AS b ON b.id = a.programa')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_anos AS c ON c.id = a.ano_Apoio')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_linhasFinanciamento AS d ON d.id = a.linha_Financiamento')
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                ->where($where)
                ->order('a.data_Fim DESC')
            );

            $data = $db->loadResult();
            return ($data);
        }



        public static function getProjetosFiltered($item, $progFiltro, $finan, $anoFiltro, $DataInicioFormated, $DataFimFormated, $tipologiaID, $categoria){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            if(!empty($progFiltro)){
                $where .= 'AND' . $db->quoteName('a.programa') . "='" . $db->escape($progFiltro) . "'";
            }

            if(!empty($finan)){
                $where .= 'AND' . $db->quoteName('a.linha_Financiamento') . "='" . $db->escape($finan) . "'";
            }

            if(!empty($anoFiltro)){
                $where .= 'AND' . $db->quoteName('a.ano_Apoio') . "='" . $db->escape($anoFiltro) . "'";
            }

            if(!empty($tipologiaID) && $tipologiaID != 'NULL'){
                $where .= 'AND' . $db->quoteName('a.tipologia') . "LIKE'" . $db->escape($tipologiaID .'%') . "'";
            }

            if(!empty($categoria) && $categoria != '0'){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($DataInicioFormated) && !empty($DataFimFormated)){
                $where .= 'AND' . $db->quoteName('a.data_Inicio') . ">='" . $db->escape($DataInicioFormated) . "'";
                $where .= 'AND' . $db->quoteName('a.data_Fim') . "<='" . $db->escape($DataFimFormated) . "'";
            } else if(!empty($DataInicioFormated) && empty($DataFimFormated)){
                $where .= 'AND' . $db->quoteName('a.data_Inicio') . ">='" . $db->escape($DataInicioFormated) . "'";
            } else if(empty($DataInicioFormated) && !empty($DataFimFormated)){
                $where .= 'AND' . $db->quoteName('a.data_Fim') . "<='" . $db->escape($DataFimFormated) . "'";
            }

            if(!empty($item)){
                $nome2 = '%' . $item . '%';

                $where2 = $db->quoteName('a.projeto') . "LIKE '" . $db->escape($nome2) . "'";
                $where3 = $db->quoteName('a.designacao') . "LIKE '" . $db->escape($nome2) . "'";
                $where4 = $db->quoteName('a.entidade') . "LIKE '" . $db->escape($nome2) . "'";
                $where5 = $db->quoteName('a.lider') . "LIKE '" . $db->escape($nome2) . "'";

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.projeto, a.designacao, b.programa as programa, a.entidade, a.lider, a.pais_lider, c.ano as ano_Apoio, d.linha as linha_Financiamento, a.tipologia, a.link, a.data_Inicio, a.data_Fim, a.call'))
                    ->join('LEFT', '#__virtualdesk_EuropaCriativa_programa AS b ON b.id = a.programa')
                    ->join('LEFT', '#__virtualdesk_EuropaCriativa_anos AS c ON c.id = a.ano_Apoio')
                    ->join('LEFT', '#__virtualdesk_EuropaCriativa_linhasFinanciamento AS d ON d.id = a.linha_Financiamento')
                    ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                    ->where($where . 'AND' . $where2)
                    ->orWhere($where . 'AND' . $where3)
                    ->orWhere($where . 'AND' . $where4)
                    ->orWhere($where . 'AND' . $where5)
                    ->order('ano_Apoio DESC, projeto ASC')
                );
            } else {

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.projeto, a.designacao, b.programa as programa, a.entidade, a.lider, a.pais_lider, c.ano as ano_Apoio, d.linha as linha_Financiamento, a.tipologia, a.link, a.data_Inicio, a.data_Fim, a.call'))
                    ->join('LEFT', '#__virtualdesk_EuropaCriativa_programa AS b ON b.id = a.programa')
                    ->join('LEFT', '#__virtualdesk_EuropaCriativa_anos AS c ON c.id = a.ano_Apoio')
                    ->join('LEFT', '#__virtualdesk_EuropaCriativa_linhasFinanciamento AS d ON d.id = a.linha_Financiamento')
                    ->join('LEFT', '#__virtualdesk_digitalGov_distritos AS e ON e.id = a.distrito')
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS f ON f.id = a.concelho')
                    ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                    ->where($where)
                    ->order('ano_Apoio DESC, projeto ASC')
                );
            }

            $data = $db->loadAssocList();
            return ($data);
        }


    }

?>