<?php

    class VirtualDeskSiteFormularioHelper{

        public static function getPrograma(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, programa'))
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->order('programa ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludePrograma($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, programa'))
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('id') . "!='" . $db->escape($programa) . "'")
                ->order('programa ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getProgSelect($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('programa')
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('id') . "='" . $db->escape($programa) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getLayout(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, layout'))
                ->from("#__virtualdesk_EuropaCriativa_TipoLayout")
                ->order('layout ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeLayout($layout){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, layout'))
                ->from("#__virtualdesk_EuropaCriativa_TipoLayout")
                ->where($db->quoteName('id') . "!='" . $db->escape($layout) . "'")
                ->order('layout ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getLayoutSelect($layout){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('layout')
                ->from("#__virtualdesk_EuropaCriativa_TipoLayout")
                ->where($db->quoteName('id') . "='" . $db->escape($layout) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getPais(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, pais'))
                ->from("#__virtualdesk_EuropaCriativa_paises")
                ->order('pais ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludePais($pais){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, pais'))
                ->from("#__virtualdesk_EuropaCriativa_paises")
                ->where($db->quoteName('id') . "!='" . $db->escape($pais) . "'")
                ->order('pais ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getPaisSelect($pais){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('pais')
                ->from("#__virtualdesk_EuropaCriativa_paises")
                ->where($db->quoteName('id') . "='" . $db->escape($pais) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getAno(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeAno($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->where($db->quoteName('id') . "!='" . $db->escape($ano) . "'")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnoSelect($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('ano')
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->where($db->quoteName('id') . "='" . $db->escape($ano) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getLinhasFinanciamento($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('programa') . '=' . $db->escape($idwebsitelist))
                    ->order('linha ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha as name'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('programa') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getLinhasFinanciamentoName($linhaFinanciamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('linha')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('id') . "='" . $db->escape($linhaFinanciamento) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeLinhasFinanciamento($programa, $linhaFinanciamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, linha as name'))
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('programa') . "='" . $db->escape($programa) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($linhaFinanciamento) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getListaEscala(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, nome'))
                ->from("#__virtualdesk_EuropaCriativa_Escala")
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();

            return ($data);
        }


        public static function getEscalaName($escala){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome')
                ->from("#__virtualdesk_EuropaCriativa_Escala")
                ->where($db->quoteName('id') . "='" . $db->escape($escala) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeEscala($escala){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, nome'))
                ->from("#__virtualdesk_EuropaCriativa_Escala")
                ->where($db->quoteName('id') . "!='" . $db->escape($escala) . "'")
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDistritos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distrito'))
                ->from("#_virtualdesk_digitalGov_distritos")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistritosName($distrito){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('distrito')
                ->from("#__virtualdesk_digitalGov_distritos")
                ->where($db->quoteName('id') . "='" . $db->escape($distrito) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeDistritos($distrito){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distrito'))
                ->from("#__virtualdesk_digitalGov_distritos")
                ->where($db->quoteName('id') . "!='" . $db->escape($distrito) . "'")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelhos($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('concelho ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho as name'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getConcelhoName($concelhos){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelhos) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeConcelho($distrito, $concelhos){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho as name'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distrito) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($concelhos) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getTipologia(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tipologia'))
                ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                ->order('tipologia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getTipologiaName($tipo){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('tipologia')
                ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                ->where($db->quoteName('id') . "='" . $db->escape($tipo) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function ValidacaoDatas($dataInicio, $dataFim){
            $splitDataInicio = explode("-", $dataInicio);
            $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];

            $splitDataFim = explode("-", $dataFim);
            $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];

            if($DataFimFormated < $DataInicioFormated){
                return 1;
            } else {
                return 0;
            }

        }

        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
        }

        public static function getIndicativo($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('indicativo')
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('id') . "='" . $db->escape($programa) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getCat($linhaFinanciamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('id') . "='" . $db->escape($linhaFinanciamento) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function saveProjeto($referencia, $nomeProjeto, $descricao, $programa, $entidade, $lider, $pais, $ano, $linhaFinanciamento, $escala, $distrito, $concelhos, $idsTipologia, $idCat, $link, $dataInicio, $dataFim, $call, $valor, $layout, $dataAtual){
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','projeto','designacao','programa','entidade','lider','pais_lider','ano_Apoio','linha_Financiamento','escala','distrito','concelho','tipologia','categoria','link','data_Inicio','data_Fim','call','valorFinanciamento','layout','data_Criacao','estado');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nomeProjeto)), $db->quote($db->escape($descricao)), $db->quote($db->escape($programa)), $db->quote($db->escape($entidade)), $db->quote($db->escape($lider)), $db->quote($db->escape($pais)), $db->quote($db->escape($ano)), $db->quote($db->escape($linhaFinanciamento)), $db->quote($db->escape($escala)), $db->quote($db->escape($distrito)), $db->quote($db->escape($concelhos)), $db->quote($db->escape($idsTipologia)), $db->quote($db->escape($idCat)), $db->quote($db->escape($link)), $db->quote($db->escape($dataInicio)), $db->quote($db->escape($dataFim)), $db->quote($db->escape($call)), $db->quote($db->escape($valor)), $db->quote($db->escape($layout)),  $db->quote($db->escape($dataAtual)),  $db->quote($db->escape('1')));
            $query
                ->insert($db->quoteName('#__virtualdesk_EuropaCriativa_ProjetosApoiados'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);
        }


        public static function getIdProj($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados")
            ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function saveTipologiasV2($idProj, $tipologia){
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('id_projecto_apoiado','id_tipologia');
            $values = array($db->quote($db->escape($idProj)), $db->quote($db->escape($tipologia)));
            $query
                ->insert($db->quoteName('#__virtualdesk_EuropaCriativa_ProjetosApoiados_Tipologia'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);
        }



        public static function saveEditedProjeto4Manager($ciecdiretorio_id, $referencia, $nomeProjeto, $descricao, $programa, $linhaFinanciamento, $escala, $entidade, $ano, $lider, $pais, $link, $dataInicio, $dataFim, $valor, $layout, $tipologia, $call){
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('ciecdiretorio');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $dataAlteracao = date("Y-m-d");

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($nomeProjeto)) array_push($fields, 'projeto="'.$db->escape($nomeProjeto).'"');
            if(!is_null($descricao)) array_push($fields, 'designacao="'.$db->escape($descricao).'"');
            if(!is_null($entidade)) array_push($fields, 'entidade="'.$db->escape($entidade).'"');
            if(!is_null($ano)) array_push($fields, 'ano_Apoio='.$db->escape($ano));
            if(!is_null($programa)){
                array_push($fields, 'programa='.$db->escape($programa));

                if($programa == '2'){
                    if(!is_null($linhaFinanciamento)){

                        if($linhaFinanciamento == '15'){
                            $linhaLimpa = 0;
                            array_push($fields, 'linha_Financiamento='.$db->escape($linhaLimpa));
                        } else {
                            array_push($fields, 'linha_Financiamento='.$db->escape($linhaFinanciamento));
                        }
                    }

                    $categoria = self::getCat($linhaFinanciamento);

                    if(!is_null($categoria)) array_push($fields, 'categoria='.$db->escape($categoria));

                    $escalaLimpa = 0;
                    array_push($fields, 'escala='.$db->escape($escalaLimpa));

                    $noLider = 'NULL';
                    array_push($fields, 'lider="'.$db->escape($noLider).'"');

                    $noPaisLider = 'NULL';
                    array_push($fields, 'pais_lider="'.$db->escape($noPaisLider).'"');
                } else {
                    if(!is_null($linhaFinanciamento)){
                        array_push($fields, 'linha_Financiamento='.$db->escape($linhaFinanciamento));

                        if($linhaFinanciamento != '15'){
                            $escalaLimpa = 0;
                            array_push($fields, 'escala='.$db->escape($escalaLimpa));
                        } else {
                            if(!is_null($escala)) array_push($fields, 'escala='.$db->escape($escala));
                        }

                    }

                    if(!is_null($lider)) array_push($fields, 'lider="'.$db->escape($lider).'"');

                    if(!is_null($pais)) array_push($fields, 'pais_lider='.$db->escape($pais));

                    $cleanCategoria = 0;
                    array_push($fields, 'categoria='.$db->escape($cleanCategoria));
                }
            }

            if(!is_null($link)) array_push($fields, 'link="'.$db->escape($link).'"');
            if(!is_null($dataInicio)) array_push($fields, 'data_Inicio="'.$db->escape($dataInicio).'"');
            if(!is_null($dataFim)) array_push($fields, 'data_Fim="'.$db->escape($dataFim).'"');
            if(!is_null($valor)) array_push($fields, 'valorFinanciamento="'.$db->escape($valor).'"');
            if(!is_null($layout)) array_push($fields, 'layout='.$db->escape($layout));


            if(!is_null($tipologia)) array_push($fields, 'tipologia="'.$db->escape($tipologia).'"');

            if(!is_null($call)) array_push($fields, '`call`="'.$db->escape($call).'"');

            array_push($fields, 'data_Alteracao="'.$db->escape($dataAlteracao).'"');

            $query
                ->update('#__virtualdesk_EuropaCriativa_ProjetosApoiados')
                ->set($fields)
                ->where('id = '.$db->escape($ciecdiretorio_id));

            $db->setQuery($query);

            $result = (boolean) $db->execute();

            return($result);

        }



        public static function saveParceiros($referencia, $parceiro, $pais){
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','parceiro','pais');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($parceiro)), $db->quote($db->escape($pais)));
            $query
                ->insert($db->quoteName('#__virtualdesk_EuropaCriativa_Parceiros'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);
        }
    }

?>