<?php

    class VirtualDeskSiteFiltroFPHelper{

        public static function getAno(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getAnoName($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('ano')
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->where($db->quoteName('id') . "='" . $db->escape($ano) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeAno($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_EuropaCriativa_anos")
                ->where($db->quoteName('id') . "!='" . $db->escape($ano) . "'")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistritos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distrito'))
                ->from("#__virtualdesk_digitalGov_distritos")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistritosName($distrito){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('distrito')
                ->from("#__virtualdesk_digitalGov_distritos")
                ->where($db->quoteName('id') . "='" . $db->escape($distrito) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeDistritos($distrito){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distrito'))
                ->from("#__virtualdesk_digitalGov_distritos")
                ->where($db->quoteName('id') . "!='" . $db->escape($distrito) . "'")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelhos($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('concelho ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho as name'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getConcelhoName($concelhos){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelhos) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeConcelho($distrito, $concelhos){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distrito) . "'")
                ->where($db->quoteName('concelho') . "!='" . $db->escape($concelhos) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPrograma(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, programa'))
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->order('programa ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludePrograma($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, programa'))
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('id') . "!='" . $db->escape($programa) . "'")
                ->order('programa ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getProgSelect($programa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('programa')
                ->from("#__virtualdesk_EuropaCriativa_programa")
                ->where($db->quoteName('id') . "='" . $db->escape($programa) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getLinhasFinanciamento($idwebsitelist,$onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('programa') . '=' . $db->escape($idwebsitelist))
                    ->order('linha ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, linha as name'))
                    ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                    ->where($db->quoteName('programa') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getLinhasFinanciamentoName($linhaFinanciamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('linha')
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('id') . "='" . $db->escape($linhaFinanciamento) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeLinhasFinanciamento($programa, $linhaFinanciamento){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, linha as name'))
                ->from("#__virtualdesk_EuropaCriativa_linhasFinanciamento")
                ->where($db->quoteName('programa') . "='" . $db->escape($programa) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($linhaFinanciamento) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }
    }

?>