<?php
/**
 * @package     Joomla.Site.VirtualDesk
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskTableConfigPlugin', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/config_plugin.php');
JLoader::register('VirtualDeskTableConfigPluginLayout', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/config_plugin_layout.php');
JLoader::register('VirtualDeskTableConfigPluginLayoutAtivos', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/config_plugin_layout_ativos.php');
JLoader::register('VirtualDeskTableConfigParams', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/config_params.php');
JLoader::register('VirtualDeskTableConfigParamsTipo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/config_params_tipo.php');
JLoader::register('VirtualDeskTableConfigParamsGrupo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/config_params_grupo.php');


/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteConfigAdminHelper
{

    function __construct() {
    }

    public static function getPluginList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
         * Check Permissões - existe uma verificação extra do grupo VD ADMIN
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('configadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=configadmin&layout=pluginview&configadmin_plugin_id=');

                // Gerar Link para o detalhe...
                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.setPluginEnableByAjax&configadmin_plugin_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.getPluginEnableValByAjax&configadmin_plugin_id=');


                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";
                $table  = " ( SELECT a.id as id, a.nome as nome, a.tagchave as tagchave, a.enabled as enabled, a.created as created, a.modified as modified ";
                $table .= " , CONCAT('".$dummyHRef    ."', CAST(a.id as CHAR(10))) as dummy ";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_config_plugin as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'id',        'dt' => 0 ),
                    array( 'db' => 'nome',      'dt' => 1 ),
                    array( 'db' => 'tagchave',  'dt' => 2 ),
                    array( 'db' => 'enabled',   'dt' => 3 ),
                    array( 'db' => 'created',   'dt' => 4 ),
                    array( 'db' => 'modified',  'dt' => 5 ),
                    array( 'db' => 'dummy',     'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchSetHRef', 'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchGetHRef', 'dt' => 8 ,'formatter'=>'URL_ENCRYPT')
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPluginLayoutList ($vbForDataTables=false, $setLimit=-1,$IdPlugin, $InputTipoLayoutId=-1)
    {
        /*
         * Check Permissões - existe uma verificação extra do grupo VD ADMIN
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginview');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPlugin) )  return false;

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();
                $db   = JFactory::getDBO();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=configadmin&layout=pluginlayoutview&configadmin_plugin_id='.$db->escape($IdPlugin).'&configadmin_pluginlayout_id=');

                // Gerar Link para o detalhe...
                #$SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.setPluginEnableByAjax&configadmin_plugin_id=');
                #$SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.getPluginEnableValByAjax&configadmin_plugin_id=');

                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";
                $table  = " ( SELECT a.id as id, a.nome as nome, a.tagchave as tagchave,  a.path as path, a.created as created, a.modified as modified, c.nome as layouttipo ";
                $table .= " , CONCAT('".$dummyHRef    ."', CAST(a.id as CHAR(10))) as dummy ";
                #$table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                #$table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_config_plugin_layout as a ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."virtualdesk_config_plugin_layout_tipo as c ON c.id=a.idtipolayout";
                $table .= " WHERE a.idplugin=".$db->escape($IdPlugin);

                if($InputTipoLayoutId>0) {
                    $table .= " AND a.idtipolayout=".$db->escape($InputTipoLayoutId);
                }


                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'id',        'dt' => 0 ),
                    array( 'db' => 'nome',      'dt' => 1 ),
                    array( 'db' => 'tagchave',  'dt' => 2 ),
                    array( 'db' => 'path',      'dt' => 3 ),
                    array( 'db' => 'layouttipo',  'dt' => 4 ),
                    array( 'db' => 'modified',  'dt' => 5 ),
                    array( 'db' => 'dummy',     'dt' => 6 ,'formatter'=>'URL_ENCRYPT')
                    #array( 'db' => 'SwitchSetHRef', 'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                    #array( 'db' => 'SwitchGetHRef', 'dt' => 8 ,'formatter'=>'URL_ENCRYPT')
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }
            else {

                // Verifica se o email nos JosUsers
                $db = JFactory::getDBO();

                $ConditionTipo = '';
                if($InputTipoLayoutId>0) {
                    $ConditionTipo = " AND a.idtipolayout=".$db->escape($InputTipoLayoutId);
                }

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id, a.nome as text'))
                    ->from("#__virtualdesk_config_plugin_layout as a")
                    ->where('a.idplugin='.$db->escape($IdPlugin). $ConditionTipo )
                    ->order('nome ASC' )
                );
                $data = $db->loadAssocList();
                return($data);

            }

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPluginLayoutAtivosList ($vbForDataTables=false, $setLimit=-1,$IdPlugin)
    {
        /*
         * Check Permissões - existe uma verificação extra do grupo VD ADMIN
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginview');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPlugin) )  return false;

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();
                $db   = JFactory::getDBO();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.setLayoutAtivoEnableByAjax&configadmin_layoutativo_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.getLayoutAtivoEnableValByAjax&configadmin_layoutativo_id=');

                $table  = " ( SELECT a.id as idtipo, a.nome AS tipolayout, c.nome as nomeativos, b.id as idativos, b.enabled as enabled,  b.idplugin as idplugin, c.path as path, b.created as created, b.modified as modified ";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST( b.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST( b.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_config_plugin_layout_tipo as a ";
                $table .= " LEFT OUTER JOIN (select * from ".$dbprefix."virtualdesk_config_plugin_layout_ativos WHERE idplugin=".$db->escape($IdPlugin)." )as b ON b.idtipolayout=a.id";
                $table .= " LEFT OUTER JOIN (select * from ".$dbprefix."virtualdesk_config_plugin_layout WHERE idplugin=".$db->escape($IdPlugin)." ) as c ON c.idplugin=b.idplugin";
                $table .= " AND b.idpluginlayout=c.id";
                $table .= "  ) temp ";

                $primaryKey = 'idtipo';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'idtipo',       'dt' => 0 ),
                    array( 'db' => 'tipolayout',   'dt' => 1 ),
                    array( 'db' => 'nomeativos',   'dt' => 2 ),
                    array( 'db' => 'path',         'dt' => 3 ),
                    array( 'db' => 'enabled',      'dt' => 4 ),
                    array( 'db' => 'modified',     'dt' => 5 ),
                    array( 'db' => 'idativos',     'dt' => 6 ),
                    array( 'db' => 'SwitchSetHRef', 'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchGetHRef', 'dt' => 8 ,'formatter'=>'URL_ENCRYPT')


                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getParamsList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
         * Check Permissões - existe uma verificação extra do grupo VD ADMIN
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('configadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=configadmin&layout=paramview&configadmin_param_id=');

                // Gerar Link para o detalhe...
                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.setParamEnableByAjax&configadmin_param_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.getParamEnableValByAjax&configadmin_param_id=');

                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";
                $table  = " ( SELECT a.id as id, a.nome as nome, a.tagchave as tagchave, a.valor as valor, a.enabled as enabled ";
                $table .= " , b.nome as tipoparam , d.nome as moduloparam ,  c.nome as grupoparam ";

                $table .= " , CONCAT('".$dummyHRef    ."', CAST(a.id as CHAR(10))) as dummy ";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_config_params as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_config_params_tipo as b ON a.idtipoparam = b.id";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_config_params_grupo as c ON a.idgrupoparam = c.id";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_modulo as d ON a.idpermmodulo = d.id";
                $table .= "  ) temp ";


                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'id',             'dt' => 0 ),
                    array( 'db' => 'nome',           'dt' => 1 ),
                    array( 'db' => 'tagchave',       'dt' => 2 ),
                    array( 'db' => 'valor',          'dt' => 3 ),
                    array( 'db' => 'tipoparam',      'dt' => 4 ),
                    array( 'db' => 'moduloparam',    'dt' => 5 ),
                    array( 'db' => 'grupoparam',     'dt' => 6 ),
                    array( 'db' => 'enabled',        'dt' => 7 ),
                    array( 'db' => 'dummy',          'dt' => 8 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchSetHRef',  'dt' => 9 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchGetHRef',  'dt' => 10 ,'formatter'=>'URL_ENCRYPT')
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getParamsTipoList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
         * Check Permissões - existe uma verificação extra do grupo VD ADMIN
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('configadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=configadmin&layout=paramtipoview&configadmin_paramtipo_id=');

                // Gerar Link para o detalhe...
                #$SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.setParamEnableByAjax&configadmin_param_id=');
                #$SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.getParamEnableValByAjax&configadmin_param_id=');

                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";
                $table  = " ( SELECT a.id as id, a.nome as nome, a.tagchave as tagchave ";
                $table .= " , CONCAT('".$dummyHRef    ."', CAST(a.id as CHAR(10))) as dummy ";
                #$table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                #$table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_config_params_tipo as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'id',             'dt' => 0 ),
                    array( 'db' => 'nome',           'dt' => 1 ),
                    array( 'db' => 'tagchave',       'dt' => 2 ),
                    array( 'db' => 'dummy',          'dt' => 3 ,'formatter'=>'URL_ENCRYPT')
                    #array( 'db' => 'SwitchSetHRef',  'dt' => 9 ,'formatter'=>'URL_ENCRYPT'),
                    #array( 'db' => 'SwitchGetHRef',  'dt' => 10 ,'formatter'=>'URL_ENCRYPT')
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getParamsGrupoList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
         * Check Permissões - existe uma verificação extra do grupo VD ADMIN
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('configadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=configadmin&layout=paramgrupoview&configadmin_paramgrupo_id=');

                // Gerar Link para o detalhe...
                #$SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.setParamEnableByAjax&configadmin_param_id=');
                #$SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=configadmin.getParamEnableValByAjax&configadmin_param_id=');

                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";
                $table  = " ( SELECT a.id as id, a.nome as nome, a.tagchave as tagchave ";
                $table .= " , CONCAT('".$dummyHRef    ."', CAST(a.id as CHAR(10))) as dummy ";
                #$table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                #$table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_config_params_grupo as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'id',             'dt' => 0 ),
                    array( 'db' => 'nome',           'dt' => 1 ),
                    array( 'db' => 'tagchave',       'dt' => 2 ),
                    array( 'db' => 'dummy',          'dt' => 3 ,'formatter'=>'URL_ENCRYPT')
                    #array( 'db' => 'SwitchSetHRef',  'dt' => 9 ,'formatter'=>'URL_ENCRYPT'),
                    #array( 'db' => 'SwitchGetHRef',  'dt' => 10 ,'formatter'=>'URL_ENCRYPT')
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPluginDetail ($IdPlugin)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('configadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPlugin) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as configadmin_plugin_id', 'nome', 'tagchave as tagchave', 'enabled as enabled' , 'created', 'modified' ))
                ->from("#__virtualdesk_config_plugin")
                ->where( $db->quoteName('id') . '=' . $db->escape($IdPlugin) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function getPluginLayoutDetail ($IdPlugin, $IdPluginLayout)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('configadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPlugin) ||  empty($IdPluginLayout) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id' , 'a.idplugin as idplugin' , 'a.id as configadmin_pluginlayout_id' , 'a.idplugin as configadmin_plugin_id' , 'a.idtipolayout as idtipolayout'
                , 'b.nome as tiponome' , 'b.tagchave as tipotag' , 'a.nome as nome', 'a.tagchave as tagchave', 'a.path as path'
                ,'c.nome as pluginnome','a.descricao as descricao', 'a.created as created', 'a.modified as modified' ))
                ->from("#__virtualdesk_config_plugin_layout as a")
                ->leftJoin("#__virtualdesk_config_plugin_layout_tipo as b ON a.idtipolayout = b.id")
                ->leftJoin("#__virtualdesk_config_plugin as c ON a.idplugin = c.id")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdPluginLayout) .' and ' . $db->quoteName('a.idplugin') . '=' . $db->escape($IdPlugin))
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function getParamDetail ($IdParam)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('configadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdParam) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id  as id', 'a.id as configadmin_param_id', 'a.nome as nome', 'a.tagchave as tagchave', 'a.valor as valor', 'a.descricao as descricao', 'a.enabled as enabled' , 'a.created as created', 'a.modified as modified'
                              , 'a.idtipoparam as idtipoparam', 'a.idgrupoparam as idgrupoparam', 'a.idpermmodulo as idpermmodulo'
                              , 'b.nome as tipoparam', 'c.nome as grupoparam', 'd.nome as moduloparam', 'b.tagchave as tipoparamtagchave' ))
                ->from("#__virtualdesk_config_params a")
                ->leftJoin("#__virtualdesk_config_params_tipo as b ON a.idtipoparam = b.id")
                ->leftJoin("#__virtualdesk_config_params_grupo as c ON a.idgrupoparam = c.id")
                ->leftJoin("#__virtualdesk_perm_modulo as d ON a.idpermmodulo = d.id")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdParam) )
            );

           // echo $db->getQuery();

            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function getParamTipoDetail ($IdParamTipo)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('configadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramtipoview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdParamTipo) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as configadmin_paramtipo_id', 'nome', 'tagchave', 'created', 'modified' ))
                ->from("#__virtualdesk_config_params_tipo")
                ->where( $db->quoteName('id') . '=' . $db->escape($IdParamTipo) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function getParamGrupoDetail ($IdParamGrupo)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('configadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramgrupoview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdParamGrupo) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as configadmin_paramgrupo_id', 'nome', 'tagchave', 'created', 'modified' ))
                ->from("#__virtualdesk_config_params_grupo")
                ->where( $db->quoteName('id') . '=' . $db->escape($IdParamGrupo) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function updatePlugin($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $configadmin_plugin_id = $data['configadmin_plugin_id'];
        if((int) $configadmin_plugin_id <=0 ) return false;
        $data['id'] = $configadmin_plugin_id;
        unset($data['configadmin_plugin_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigPlugin($db);
        $Table->load(array('id'=>$configadmin_plugin_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updatePluginLayout($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $configadmin_pluginlayout_id = $data['configadmin_pluginlayout_id'];
        $configadmin_plugin_id       = $data['configadmin_plugin_id'];
        if((int) $configadmin_pluginlayout_id <=0 ) return false;
        if((int) $configadmin_plugin_id <=0 ) return false;
        $data['id']       = $configadmin_pluginlayout_id;
        $data['idplugin'] = $configadmin_plugin_id;
        unset($data['configadmin_pluginlayout_id']);
        unset($data['configadmin_plugin_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigPluginLayout($db);
        $Table->load(array('id'=>$configadmin_pluginlayout_id, 'idplugin'=>$configadmin_plugin_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGINLAYOUT_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGINLAYOUT_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGINLAYOUT_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updateParam($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $configadmin_param_id = $data['configadmin_param_id'];
        if((int) $configadmin_param_id <=0 ) return false;
        $data['id'] = $configadmin_param_id;
        unset($data['configadmin_param_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigParams($db);
        $Table->load(array('id'=>$configadmin_param_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            //if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PARAM_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PARAM_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PARAM_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updateParamTipo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramtipoedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $configadmin_paramtipo_id = $data['configadmin_paramtipo_id'];
        if((int) $configadmin_paramtipo_id <=0 ) return false;
        $data['id'] = $configadmin_paramtipo_id;
        unset($data['configadmin_paramtipo_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigParamsTipo($db);
        $Table->load(array('id'=>$configadmin_paramtipo_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updateParamGrupo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramgrupoedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $configadmin_paramgrupo_id = $data['configadmin_paramgrupo_id'];
        if((int) $configadmin_paramgrupo_id <=0 ) return false;
        $data['id'] = $configadmin_paramgrupo_id;
        unset($data['configadmin_paramgrupo_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigParamsGrupo($db);
        $Table->load(array('id'=>$configadmin_paramgrupo_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function setPluginEnableState($UserJoomlaID, $getInputPlugin_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_plugin_id = $getInputPlugin_id;
        if((int) $configadmin_plugin_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_plugin_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigPlugin($db);
        $Table->load(array('id'=>$configadmin_plugin_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function setPluginLayoutAtivo($UserJoomlaID, $getInputPlugin_id, $getInputLayout_id, $getInputTipoLayout_id)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_plugin_id     = $getInputPlugin_id;
        $configadmin_layout_id     = $getInputLayout_id;
        $configadmin_tipolayout_id = $getInputTipoLayout_id;

        if((int) $configadmin_plugin_id <=0 ) return false;
        if((int) $configadmin_layout_id <=0 ) return false;
        if((int) $configadmin_tipolayout_id <=0 ) return false;

        $data['idplugin']       = $configadmin_plugin_id;
        $data['idpluginlayout'] = $configadmin_layout_id;
        $data['idtipolayout']   = $configadmin_tipolayout_id;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigPluginLayoutAtivos($db);
        $Table->load(array('idplugin'=>$configadmin_plugin_id, 'idtipolayout'=>$configadmin_tipolayout_id));



        if((int)$Table->id > 0) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }
        else {
            $dateCreated = new DateTime();
            $data['created'] = $dateCreated->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function setParamEnableState($UserJoomlaID, $getInputParam_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_param_id = $getInputParam_id;
        if((int) $configadmin_param_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_param_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigParams($db);
        $Table->load(array('id'=>$configadmin_param_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PARAM_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PARAM_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PARAM_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function setLayoutAtivoEnableState($UserJoomlaID, $getInputLayoutAtivo_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_layoutativo_id = $getInputLayoutAtivo_id;
        if((int) $configadmin_layoutativo_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_layoutativo_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigPluginLayoutAtivos($db);
        $Table->load(array('id'=>$configadmin_layoutativo_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function checkPluginEnableVal($getInputPlugin_id)
    {
        if( empty($getInputPlugin_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select enabled From #__virtualdesk_config_plugin Where id =" . $db->escape($getInputPlugin_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function checkParamEnableVal($getInputParam_id)
    {
        if( empty($getInputParam_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select enabled From #__virtualdesk_config_params Where id =" . $db->escape($getInputParam_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function checkPluginLayoutPathVal($getInputPlugin_id,$getInputLayout_id)
    {
        if( empty($getInputPlugin_id) )  return false;
        if( empty($getInputLayout_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select path From #__virtualdesk_config_plugin_layout Where idplugin =" . $db->escape($getInputPlugin_id) . " AND id=" . $db->escape($getInputLayout_id) );
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function checkLayoutAtivoEnableVal($getInputLayoutAtivo_id)
    {
        if( empty($getInputLayoutAtivo_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select enabled From #__virtualdesk_config_plugin_layout_ativos Where id =" . $db->escape($getInputLayoutAtivo_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function createPlugin($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigPlugin($db);

        if (empty($data) ) return false;

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function createPluginLayout($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'pluginaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if (empty($data) ) return false;

        $configadmin_plugin_id       = $data['configadmin_plugin_id'];
        if((int) $configadmin_plugin_id <=0 ) return false;
        unset($data['configadmin_plugin_id']);
        $data['idplugin'] = $configadmin_plugin_id;

        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigPluginLayout($db);

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGINLAYOUT_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGINLAYOUT_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGINLAYOUT_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function createParam($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigParams($db);

        if (empty($data) ) return false;

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function createParamTipo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramtipoaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigParamsTipo($db);

        if (empty($data) ) return false;

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function createParamGrupo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('configadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('configadmin', 'paramgrupoaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableConfigParamsGrupo($db);

        if (empty($data) ) return false;

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function getParamTipoAllList($fileLangSufix)
    {
        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome, tagchave'))
            ->from("#__virtualdesk_config_params_tipo")
            ->order('nome ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getParamGrupoAllList($fileLangSufix)
    {
        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome, tagchave'))
            ->from("#__virtualdesk_config_params_grupo")
            ->order('nome ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getParamModuloAllList($fileLangSufix)
    {
        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome ,tagchave'))
            ->from("#__virtualdesk_perm_modulo")
            ->order('nome ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getPluginLayoutTipoAllList($fileLangSufix)
    {
        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome, tagchave'))
            ->from("#__virtualdesk_config_plugin_layout_tipo")
            ->order('nome ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getParamValor($paramtag)
    {
        if( empty($paramtag) )  return false;
        try
        {
            // Verifica se o email nos JosUsers
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, nome ,tagchave, valor, enabled'))
                ->from("#__virtualdesk_config_params as a")
                ->where( $db->quoteName("a.tagchave") . "='" . $db->escape($paramtag) ."' and enabled=1" )
            );
            // Dump the query
            //echo $db->getQuery()->dump();
            $dataReturn = $db->loadObject();

            return($dataReturn->valor);
        }
        catch (RuntimeException $e)
        {
            return false;
        }

    }



    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.pluginaddnew.configadmin.data', null);
        $app->setUserState('com_virtualdesk.pluginedit.configadmin.data',null);
        $app->setUserState('com_virtualdesk.pluginlayoutaddnew.configadmin.data', null);
        $app->setUserState('com_virtualdesk.pluginlayoutedit.configadmin.data',null);
        $app->setUserState('com_virtualdesk.paramaddnew.configadmin.data', null);
        $app->setUserState('com_virtualdesk.paramedit.configadmin.data',null);
        $app->setUserState('com_virtualdesk.paramtipoaddnew.configadmin.data', null);
        $app->setUserState('com_virtualdesk.paramtipoedit.configadmin.data',null);
        $app->setUserState('com_virtualdesk.paramgrupoaddnew.configadmin.data', null);
        $app->setUserState('com_virtualdesk.paramgrupoedit.configadmin.data',null);
        $app->setUserState('com_virtualdesk.edit.profile.data', null);
    }





}
