<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

class VirtualDeskSiteDireitosCidadaniaHelper
    {
        const tagchaveModulo = 'direitoscidadania';

        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnewagendamentoreuniao4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewapresentacaocontributos4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewconstituicaointeressado4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewconsultaprocesso4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewdesistenciapretensoes4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewelogio4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewexercicioreuniao4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewintervencaoassembleia4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewintervencaocamara4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewisencaotaxa4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewjuncaoelementos4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewjuncaoelementosprorrogacao4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewlicenciamentooperacoes4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewpagamentoprestacoes4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewpedidoesclarecimento4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewprojetoregulamentomunicipal4user.direitoscidadania.data', null);
            $app->setUserState('com_virtualdesk.addnewreembolso4user.direitoscidadania.data', null);
        }
    }
?>