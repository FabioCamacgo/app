<?php

    class VirtualDeskSiteAguaHelper
    {
        const tagchaveModulo = 'agua';

        public static function checkConsumidor($numconsumidor){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('consumidor') . "='" . $db->escape($numconsumidor) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function checkContador($numContador){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Agua_Contadores")
                ->where($db->quoteName('contador') . "='" . $db->escape($numContador) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function checkNIF($nif){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nif) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function checkEmail($email){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_consumidor, email'))
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('email') . "='" . $db->escape($email) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getFreguesias($freguesia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($freguesia))
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getFreguesia($concelho)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getFregSelect($freg){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freg) . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeFreguesia($concelho, $freg){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.freguesia as freguesia'))
                ->from("#__virtualdesk_digitalGov_freguesias as a")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($freg) . "'")
                ->order('a.id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDadosConsumidor($nifConsumidor){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('consumidor, nome, zona'))
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nifConsumidor) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getContadores($nifConsumidor){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, contador, data_criacao'))
                ->from("#__virtualdesk_Agua_Contadores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nifConsumidor) . "'")
                ->order('data_criacao DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeContador($nifConsumidor, $numContador){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, contador'))
                ->from("#__virtualdesk_Agua_Contadores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nifConsumidor) . "'")
                ->where($db->quoteName('contador') . "!='" . $db->escape($numContador) . "'")
                ->order('data_criacao DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getLeituras($nifConsumidor){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.leitura, a.num_contador, a.data_leitura, b.estado, a.estado as idEstado'))
                ->join('LEFT', '#__virtualdesk_Agua_Leituras_Estado AS b ON b.id_estado = a.estado')
                ->from("#__virtualdesk_Agua_Leituras as a")
                ->where($db->quoteName('a.nif') . "='" . $db->escape($nifConsumidor) . "'")
                ->order('a.data_leitura DESC')
                ->setLimit(50)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getUltimaLeituras($nifConsumidor, $numContador){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('leitura')
                ->from("#__virtualdesk_Agua_Leituras")
                ->where($db->quoteName('nif') . "='" . $db->escape($nifConsumidor) . "'")
                ->where($db->quoteName('num_contador') . "='" . $db->escape($numContador) . "'")
                ->order('data_leitura DESC')
                ->setLimit(1)
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function contadorExiste($contador){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Agua_Contadores")
                ->where($db->quoteName('contador') . "='" . $db->escape($contador) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function saveNovoContador($contador, $nif){

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('contador', 'nif', 'estado');
            $values = array($db->quote($contador), $db->quote($nif), $db->quote('2'));
            $query
                ->insert($db->quoteName('#__virtualdesk_Agua_Contadores'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);


        }

        public static function getEmail($num){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('consumidor') . '=' . $db->escape($num))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function guardaLeitura($referencia, $nifConsumidor, $numContador, $newLeitura){

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('ref_leitura', 'nif', 'num_contador', 'leitura', 'estado');
            $values = array($db->quote($referencia), $db->quote($nifConsumidor), $db->quote($numContador), $db->quote($newLeitura), $db->quote('1'));
            $query
                ->insert($db->quoteName('#__virtualdesk_Agua_Leituras'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);


        }

        public static function updateLeitura($referencias, $leitura){

            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $fields = array($db->quoteName('leitura') . "='" . $db->escape($leitura) . "'");

            $conditions = array(
                $db->quoteName('ref_leitura') . "='" . $db->escape($referencias) . "'");


            $query->update($db->quoteName('#__virtualdesk_Agua_Leituras'))->set($fields)->where($conditions);

            $db->setQuery($query);

            $db->execute();
        }

        public static function CheckReferencia($codi){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_leitura')
                ->from("#__virtualdesk_Agua_Leituras")
                ->where($db->quoteName('ref_leitura') . "='" . $db->escape($codi) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getnomeConsumidor($numconsumidor){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome')
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('consumidor') . "='" . $db->escape($numconsumidor) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getmonthName($month){
            if($month == 1){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_JANEIRO');
            } else if($month == 2){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_FEVEREIRO');
            } else if($month == 3){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_MARÇO');
            } else if($month == 4){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_ABRIL');
            } else if($month == 5){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_MAIO');
            } else if($month == 6){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_JUNHO');
            } else if($month == 7){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_JULHO');
            } else if($month == 8){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_AGOSTO');
            } else if($month == 9){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_SETEMBRO');
            } else if($month == 10){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_OUTUBRO');
            } else if($month == 11){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_NOVEMBRO');
            } else if($month == 12){
                $mes = JText::_('COM_VIRTUALDESK_AGUA_DEZEMBRO');
            }

            return $mes;
        }


        public static function getnomeFreguesia($freguesia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function validaTelefone($telefone, $TelefoneMaxLenght){
            if($telefone == Null){
                return (1);
            } else if($telefone != Null){
                $TelefoneSplit=str_split($telefone);
                if(!is_numeric($telefone) || strlen($telefone) != $TelefoneMaxLenght){
                    return (2);
                } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
                    return (2);
                } else if($TelefoneSplit[0] == 9){
                    if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
                        return (2);
                    }
                } else {
                    return (0);
                }
            }
        }


        public static function validaNumeroConsumidor($numConsumidor, $NumConsumidorMaxLenght, $existeConsumidor){
            if($numConsumidor == Null){
                return (1);
            } else if(!is_numeric($numConsumidor) || strlen($numConsumidor) > $NumConsumidorMaxLenght){
                return (2);
            } else if($existeConsumidor != 0){
                return (3);
            } else {
                return (0);
            }
        }


        public static function validaNumeroContador($numContador, $NumContadorMaxLenght, $existeContador){
            if($numContador == Null){
                return (1);
            } else if(!is_numeric($numContador) || strlen($numContador) > $NumContadorMaxLenght){
                return (2);
            } else if($existeContador != 0){
                return (3);
            } else {
                return (0);
            }
        }


        public static function validaFreguesia($freg){
            if($freg == Null){
                return (1);
            } else{
                return (0);
            }
        }


        public static function validaMorada($local){
            if($local == Null){
                return (1);
            } else{
                return (0);
            }
        }


        public static function validaCodigoPostal($codPostal4, $codPostal3, $codPostalText, $codigoPostal2MaxLenght, $codigoPostal1MaxLenght){
            if($codPostal4 == Null && $codPostal3 == Null && $codPostalText == Null){
                return (2);
            } else if($codPostal4 == Null || $codPostal3 == Null || $codPostalText == Null){
                return (1);
            } else if(!is_numeric($codPostal4) || !is_numeric($codPostal3) || is_numeric($codPostalText)){
                return (1);
            } else if(preg_match('([0-9])', $codPostalText)){
                return (1);
            } else if(strlen($codPostal4) != $codigoPostal1MaxLenght || strlen($codPostal3) != $codigoPostal2MaxLenght){
                return (1);
            } else{
                return (0);
            }
        }


        public static function validaNome($nome){
            if($nome == Null){
                return (1);
            }else if(is_numeric($nome)){
                return (2);
            } else if(preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $nome)){
                return (2);
            } else{
                return (0);
            }
        }


        public static function validaEmail($email, $confemail){
            $conta = "/^[a-zA-Z0-9\._-]+@";
            $domino = "[a-zA-Z0-9\._-]+.[.]";
            $extensao = "([a-zA-Z]{2,4})$/";
            $pattern = $conta . $domino . $extensao;
            if($email == Null){
                return (1);
            } else if(!preg_match($pattern, $email)) {
                return (2);
            } else if($email != $confemail){
                return (3);
            } else{
                return (0);
            }
        }


        public static function validaNif($fiscalid, $NifMaxLenght, $nifExiste){
            $nif=trim($fiscalid);
            $ignoreFirst=true;
            if (!is_numeric($nif) || strlen($nif)!= $NifMaxLenght) {
                return (1);
            } else if($nifExiste != 0){
                return (2);
            } else {
                $nifSplit=str_split($nif);
                if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);

                    if($checkDigit>=10) $checkDigit=0;

                    if ($checkDigit==$nifSplit[8]) {

                    } else {
                        return (1);
                    }
                } else {
                    return (1);
                }
            }
        }


        public static function SendLeituraEmail($copyrightAPP, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $email, $read, $nome, $monthName){

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_LeituraAgua_Email.html');


            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $email;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_TITULO');
            $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_TITULO');
            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_INTRO',$nome);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_CORPO', $monthName);
            $BODY_LEITURA       = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_CORPO_VALORLEITURA',$read);
            $BODY_COPYNAME       = JText::sprintf($copyrightAPP);
            $BODY_COPYMAIL       = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYTELE       = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYDOM       = JText::sprintf($dominioMunicipio);
            $BODY_COPYLINK       = JText::sprintf($LinkCopyright);


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_LEITURA", $BODY_LEITURA, $body);
            $body      = str_replace("%BODY_COPYNAME", $BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYMAIL", $BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYTELE", $BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYDOM", $BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYLINK", $BODY_COPYLINK, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo( $data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Agua_BannerEmail.png', "banner", "Agua_BannerEmail.png");
            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }


        public static function guardaRegisto($numConsumidor, $nomeConsumidor, $nif, $email, $telefone, $morada, $concelhoParam, $freg, $CodPostal){
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $columns = array('nif', 'consumidor', 'nome', 'email', 'telefone', 'morada', 'concelho', 'freguesia', 'codigo_postal', 'estado');
            $values = array($db->quote($db->escape($nif)), $db->quote($db->escape($numConsumidor)), $db->quote($db->escape($nomeConsumidor)), $db->quote($db->escape($email)), $db->quote($db->escape($telefone)), $db->quote($db->escape($morada)), $db->quote($db->escape($concelhoParam)), $db->quote($db->escape($freg)), $db->quote($db->escape($CodPostal)), $db->quote('2'));

            $query
                ->insert($db->quoteName('#__virtualdesk_Agua_Consumidores'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);
        }


        public static function guardaContador($numContador, $nif){
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $columns = array('contador', 'nif', 'estado');
            $values = array($db->quote($db->escape($numContador)), $db->quote($db->escape($nif)), $db->quote('2'));

            $query
                ->insert($db->quoteName('#__virtualdesk_Agua_Contadores'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);
        }


        public static function SendRegistoEmail($numConsumidor, $numContador, $nomeConsumidor, $email, $nif, $telefone, $morada, $freg, $CodPostal){

            $config = JFactory::getConfig();

            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('novoConsumidor','email2user');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }

            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $obParam      = new VirtualDeskSiteParamsHelper();
            $nomeCamara = $obParam->getParamsByTag('nomeCamaraAgua');
            $concelhoPSOL = $obParam->getParamsByTag('concelhoPSOL');
            $contactoCopyright = $obParam->getParamsByTag('contactoCopyrightEmail');
            $emailCopyright = $obParam->getParamsByTag('emailCopyrightEmail');
            $linkCopyright = $obParam->getParamsByTag('linkCopyrightEmail');
            $websiteCopyright = $obParam->getParamsByTag('websiteCopyrightEmail');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOCONSUMIDOR_EMAIL_TITULO',$concelhoPSOL);
            $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOCONSUMIDOR_EMAIL_TITULO',$concelhoPSOL);
            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOCONSUMIDOR_EMAIL_INTRO',$nomeConsumidor);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOCONSUMIDOR_EMAIL_CORPO',$concelhoPSOL);
            $BODY_SECONDLINE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOCONSUMIDOR_EMAIL_CORPO2',$nomeCamara);
            $BODY_THIRDLINE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOCONSUMIDOR_CORPO3');

            $BODY_NUMCONSUMIDOR_TITLE       = JText::sprintf('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NUM_CONSUMIDOR');
            $BODY_NUMCONSUMIDOR_VALUE       = JText::sprintf($numConsumidor);
            $BODY_NUMCONTADOR_TITLE       = JText::sprintf('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NUM_CONTADOR');
            $BODY_NUMCONTADOR_VALUE       = JText::sprintf($numContador);
            $BODY_NIF_TITLE       = JText::sprintf('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_NIF');
            $BODY_NIF_VALUE       = JText::sprintf($nif);
            $BODY_TELEFONE_TITLE       = JText::sprintf('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_TELEFONE');
            $BODY_TELEFONE_VALUE       = JText::sprintf($telefone);
            $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_MORADA');
            $BODY_MORADA_VALUE       = JText::sprintf($morada);
            $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_FREGUESIA');
            $BODY_FREGUESIA_VALUE       = JText::sprintf($freg);
            $BODY_CODPOSTAL_TITLE       = JText::sprintf('COM_VIRTUALDESK_AGUA_NOVO_REGISTO_CODPOSTAL');
            $BODY_CODPOSTAL_VALUE       = JText::sprintf($CodPostal);
            $BODY_NOMECAMARA_VALUE       = JText::sprintf($nomeCamara);
            $BODY_CONTACTOCOPYRIGHT_VALUE       = JText::sprintf($contactoCopyright);
            $BODY_EMAILCOPYRIGHT_VALUE       = JText::sprintf($emailCopyright);
            $BODY_LINKCOPYRIGHT_VALUE       = JText::sprintf($linkCopyright);
            $BODY_WEBSITECOPYRIGHT_VALUE       = JText::sprintf($websiteCopyright);


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_NUMCONSUMIDOR_TITLE",$BODY_NUMCONSUMIDOR_TITLE, $body);
            $body      = str_replace("%BODY_NUMCONSUMIDOR_VALUE",$BODY_NUMCONSUMIDOR_VALUE, $body);
            $body      = str_replace("%BODY_NUMCONTADOR_TITLE",$BODY_NUMCONTADOR_TITLE, $body);
            $body      = str_replace("%BODY_NUMCONTADOR_VALUE",$BODY_NUMCONTADOR_VALUE, $body);
            $body      = str_replace("%BODY_NIF_TITLE",$BODY_NIF_TITLE, $body);
            $body      = str_replace("%BODY_NIF_VALUE",$BODY_NIF_VALUE, $body);
            $body      = str_replace("%BODY_TELEFONE_TITLE",$BODY_TELEFONE_TITLE, $body);
            $body      = str_replace("%BODY_TELEFONE_VALUE",$BODY_TELEFONE_VALUE, $body);
            $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
            $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
            $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
            $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
            $body      = str_replace("%BODY_CODPOSTAL_TITLE",$BODY_CODPOSTAL_TITLE, $body);
            $body      = str_replace("%BODY_CODPOSTAL_VALUE",$BODY_CODPOSTAL_VALUE, $body);
            $body      = str_replace("%BODY_NOMECAMARA_VALUE",$BODY_NOMECAMARA_VALUE, $body);
            $body      = str_replace("%BODY_CONTACTOCOPYRIGHT_VALUE",$BODY_CONTACTOCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_EMAILCOPYRIGHT_VALUE",$BODY_EMAILCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_LINKCOPYRIGHT_VALUE",$BODY_LINKCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_WEBSITECOPYRIGHT_VALUE",$BODY_WEBSITECOPYRIGHT_VALUE, $body);


            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['fromname']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);

            $logosendmailImage = $obParam->getParamsByTag('logosendmailAguaAmbiente');
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;


        }


        public static function getMailConsumidor($nifConsumidor){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nifConsumidor) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getnomeConsumidorByNIF($nifConsumidor){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome')
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nifConsumidor) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function SendLeituraAdminEmail($referencia){

            $config = JFactory::getConfig();

            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('validaLeituras','email2admin');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }

            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $obParam      = new VirtualDeskSiteParamsHelper();
            $nomeCamara = $obParam->getParamsByTag('nomeCamaraAgua');
            $concelhoPSOL = $obParam->getParamsByTag('concelhoPSOL');
            $contactoCopyright = $obParam->getParamsByTag('contactoCopyrightEmail');
            $emailCopyright = $obParam->getParamsByTag('emailCopyrightEmail');
            $linkCopyright = $obParam->getParamsByTag('linkCopyrightEmail');
            $websiteCopyright = $obParam->getParamsByTag('websiteCopyrightEmail');
            $mailAdmin = $obParam->getParamsByTag('mailAdminLeituraAgua');
            $mailManager = $obParam->getParamsByTag('mailManagerLeituraAgua');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_TITULO');
            $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_TITULO');
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_CORPO', $referencia);
            $BODY_SECONDLINE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_CORPO2');


            $BODY_NOMECAMARA_VALUE       = JText::sprintf($nomeCamara);
            $BODY_CONTACTOCOPYRIGHT_VALUE       = JText::sprintf($contactoCopyright);
            $BODY_EMAILCOPYRIGHT_VALUE       = JText::sprintf($emailCopyright);
            $BODY_LINKCOPYRIGHT_VALUE       = JText::sprintf($linkCopyright);
            $BODY_WEBSITECOPYRIGHT_VALUE       = JText::sprintf($websiteCopyright);


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_NOMECAMARA_VALUE",$BODY_NOMECAMARA_VALUE, $body);
            $body      = str_replace("%BODY_CONTACTOCOPYRIGHT_VALUE",$BODY_CONTACTOCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_EMAILCOPYRIGHT_VALUE",$BODY_EMAILCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_LINKCOPYRIGHT_VALUE",$BODY_LINKCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_WEBSITECOPYRIGHT_VALUE",$BODY_WEBSITECOPYRIGHT_VALUE, $body);


            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['fromname']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($mailAdmin);
            $newActivationEmail->addRecipient($mailManager);
            $newActivationEmail->setSubject($data['sitename']);

            $logosendmailImage = $obParam->getParamsByTag('logosendmailAguaAmbiente');
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;


        }


        public static function SendLeituraEmailPlugin($nameConsumidor, $mailConsumidor, $numContador, $newLeitura){

            $config = JFactory::getConfig();

            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('validaLeituras','email2user');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }

            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $obParam      = new VirtualDeskSiteParamsHelper();
            $nomeCamara = $obParam->getParamsByTag('nomeCamaraAgua');
            $concelhoPSOL = $obParam->getParamsByTag('concelhoPSOL');
            $contactoCopyright = $obParam->getParamsByTag('contactoCopyrightEmail');
            $emailCopyright = $obParam->getParamsByTag('emailCopyrightEmail');
            $linkCopyright = $obParam->getParamsByTag('linkCopyrightEmail');
            $websiteCopyright = $obParam->getParamsByTag('websiteCopyrightEmail');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_TITULO');
            $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_TITULO');
            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_INTRO',$nameConsumidor);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_USER_CORPO');
            $BODY_SECONDLINE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_USER_CORPO2');
            $BODY_THIRDLINE       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_USER_CORPO3');
            $BODY_CONTADOR_NAME       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_USER_CONTADOR');
            $BODY_CONTADOR_VALUE       = JText::sprintf($numContador);
            $BODY_LEITURA_NAME       = JText::sprintf('COM_VIRTUALDESK_AGUAAMBIENTE_NOVOLEITURA_EMAIL_USER_LEITURA');
            $BODY_LEITURA_VALUE       = JText::sprintf($newLeitura);
            $BODY_NOMECAMARA_VALUE       = JText::sprintf($nomeCamara);
            $BODY_CONTACTOCOPYRIGHT_VALUE       = JText::sprintf($contactoCopyright);
            $BODY_EMAILCOPYRIGHT_VALUE       = JText::sprintf($emailCopyright);
            $BODY_LINKCOPYRIGHT_VALUE       = JText::sprintf($linkCopyright);
            $BODY_WEBSITECOPYRIGHT_VALUE       = JText::sprintf($websiteCopyright);


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body);
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_NOMECAMARA_VALUE",$BODY_NOMECAMARA_VALUE, $body);
            $body      = str_replace("%BODY_CONTACTOCOPYRIGHT_VALUE",$BODY_CONTACTOCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_EMAILCOPYRIGHT_VALUE",$BODY_EMAILCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_LINKCOPYRIGHT_VALUE",$BODY_LINKCOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_WEBSITECOPYRIGHT_VALUE",$BODY_WEBSITECOPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_CONTADOR_NAME",$BODY_CONTADOR_NAME, $body);
            $body      = str_replace("%BODY_CONTADOR_VALUE",$BODY_CONTADOR_VALUE, $body);
            $body      = str_replace("%BODY_LEITURA_NAME",$BODY_LEITURA_NAME, $body);
            $body      = str_replace("%BODY_LEITURA_VALUE",$BODY_LEITURA_VALUE, $body);


            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['fromname']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($mailConsumidor);
            $newActivationEmail->setSubject($data['sitename']);

            $logosendmailImage = $obParam->getParamsByTag('logosendmailAguaAmbiente');
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;


        }

    }

?>