<?php
/**
 * Created by PhpStorm.
 * User: Camacho
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskTableUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta.php');

    class VirtualDeskBackofficeAlertaHelper{


        public static function getAlertaDetail($userSessionID, $IdAlerta){
            //TODO REVER !!!!!!!!!!!!!
            /*
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, a.descricao, d.freguesia as freguesia, a.sitio, a.local, a.latitude, a.longitude, a.pontos_referencia, e.estado_PT as estado, a.data_criacao, a.data_alteracao, a.descritivoBO, a.observacoes, a.utilizador, a.nif, a.email, a.telefone'))
                ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                ->from("#__virtualdesk_alerta as a")
                ->where($db->quoteName('a.estado').'= 1 and Id_alerta = ' . $db->escape($IdAlerta) )
                ->order('  alerta_id DESC ')
            );
            $dataReturn = $db->loadObject();
            return ($dataReturn);
             */
            return false;
        }


        public static function getAlertaList($userSessionID){
            //TODO REVER !!!!!!!!!!!!!
            /*
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.estado_PT as estado, a.data_criacao'))
                ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                ->from("#__virtualdesk_alerta as a")
                ->where($db->quoteName('a.estado').'= 1' )
                ->order('  alerta_id DESC ')
            );
            $dataReturn = $db->loadObjectList();
            return ($dataReturn);
            */
            return false;
        }



        public static function updateUserAlerta($UserJoomlaID, $IdAlerta, $data)
        {
            //TODO REVER !!!!!!!!!!!!!
            /*
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            $fields = array(
                $db->quoteName('descritivoBO') . ' = ' . $db->quote($data['descritivoBO']),
                $db->quoteName('observacoes') . ' = ' . $db->quote($data['observacoes'])
            );


            $conditions = array(
                $db->quoteName('Id_alerta') . ' = ' . $db->quote($data['alerta_id'])
            );

            $query->update($db->quoteName('#__virtualdesk_alerta'))->set($fields)->where($conditions);

            $db->setQuery($query);

            $result = $db->execute();

            return ($result);
            */
            return false;
        }



        public static function delete($UserJoomlaID, $data)
        {
             //TODO REVER !!!!!!!!!!!!!
            /*
            $IdAlerta = $data['Id_alerta'];
            if((int) $IdAlerta <=0 ) return false;
            $data['id'] = $IdAlerta;
            unset($data['Id_alerta']);

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $TableAlerta = new VirtualDeskTableAlerta($db);
            $TableAlerta->load(array('id'=>$IdAlerta));
            //if((integer)$TableAlerta->iduserjos !== (integer)$UserJoomlaID ) return false;

            // Eliimina ficheiros associados ao pedido
            //if(!self::deleteFilesAssociatedInRequest ($IdAlerta )) return (false);

            // elimina area act
            $queryDel   = $db->getQuery(true);
            $conditions = array( $db->quoteName('Id_alerta') . ' = ' . $db->escape($IdAlerta)   );
            $queryDel->delete($db->quoteName('#__virtualdesk_alerta'));
            $queryDel->where($conditions);
            $db->setQuery($queryDel);
            $db->execute();

            if (!$TableAlerta->delete($IdAlerta)) return false;

            // Foi alterado o Pedido do tipo Contact Us
            //$vdlog = new VirtualDeskLogHelper();
            //$eventdata                  = array();
            //$eventdata['iduser']        = $UserVDId;
            //$eventdata['idjos']         = $UserJoomlaID;
            //$eventdata['title']         = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_DELETED_TITLE');
            //$eventdata['desc']          = self::getEventLogMailDesc ($TableAlerta, $eventdata['title'] );
            // $eventdata['category']      = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_CAT');
            //$eventdata['sessionuserid'] = JFactory::getUser()->id;
            //$vdlog->insertEventLog($eventdata);

            return true;
            */
            return false;
        }

    }
?>