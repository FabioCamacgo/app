<?php
/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


class VirtualDeskSiteAlertaFieldsHelper
{
    var $arConfFieldNames = array();
    var $arAlertaFieldsConfig     = array();
    var $arEmptyFieldsError           = array();

    // public function VirtualDeskSiteAlertaFieldsHelper()
    public function __construct()
    {

        $this->arConfFieldNames =  array(
            'type'=>'TYPE',
            'category'=>'CATEGORY',
            'subcategory'=>'SUBCATEGORY',
            'status'=>'STATUS',
            'ctlanguage'=>'CTLANGUAGE',
            'vdwebsitelist'=>'VDWEBSITELIST',
            'vdmenumain'=>'VDMENUMAIN',
            'vdmenusec'=>'VDMENUSEC',
            'vdmenusec2'=>'VDMENUSEC2',
            'areaact'=>'AREAACT',
            'obs'=>'OBS',
            'attachment'=>'ATTACHMENT'
        );

        $this->setFields();
    }


    public function setFields()
    {
        foreach($this->arConfFieldNames as $key => $val)
        {
            // Field : visible
            $this->arAlertaFieldsConfig['AlertaField_' . $val]  = false;
            if(JComponentHelper::getParams('com_virtualdesk')->get('alertafield_' . $key) == 1 ) $this->arAlertaFieldsConfig['AlertaField_' . $val] = true;

            // Field: obrigatório
            $this->arAlertaFieldsConfig['AlertaField_' . $val. '_Required']    = "";
            if(JComponentHelper::getParams('com_virtualdesk')->get('alertafield_'.$key.'_required') == 1 ) $this->arAlertaFieldsConfig['AlertaField_' . $val. '_Required'] = 'required';
        }

    }


    public function getFields()
    {
        return $this->arAlertaFieldsConfig;
    }


    /*
    * Verifica se os campos recebidos se estão vazios e nesse caso não inicializa o array de dados.
    * Ou se são obrigatórios nesse caso não podem vir vazios.
    *
    */
    public function checkCamposEmpty($arCamposGet, $data)
    {
        // ToDO FALTA IMPLEMENTAR

        foreach($this->arConfFieldNames as $key => $val)
        {
            if ($this->arAlertaFieldsConfig['AlertaField_' . $val]) {
                if ($this->arAlertaFieldsConfig['AlertaField_' . $val . '_Required']) {
                    if (!empty($arCamposGet[$key])) {
                        // não está vazio
                        $data[$key] = $arCamposGet[$key];
                    }
                    else {
                        // é obrigatório e está vazio... Marca como estando com erro
                        $this->arEmptyFieldsError[$key] = true;
                    }
                } else { // Se não é obrigatório pode ir vazio
                    $data[$key] = $arCamposGet[$key];
                }
            }
        }

        return($data);
    }


    public function checkRequiredEmptyFields()
    {
        $msg ="";

        foreach($this->arConfFieldNames as $key => $val) {
            if( $this->arEmptyFieldsError[$key] )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_CONTACTUS_' . $val . '_LABEL');
        }

        return($msg);
    }






}