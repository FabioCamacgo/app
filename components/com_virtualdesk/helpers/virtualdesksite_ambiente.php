<?php
    /**
     * Created by PhpStorm.
     * User:
     * Date: 04/09/2018
     * Time: 14:42
     */

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

class VirtualDeskSiteAmbienteHelper
    {
        const tagchaveModulo = 'ambiente';

    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
    }

    public static function CheckReferencia($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('ref')
            ->from("#__virtualdesk_form_main_rel_ambiente")
            ->where($db->quoteName('ref') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function createRecolhaAnimais4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('ambiente');  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if (empty($data) ) return false;

        //$db->transactionStart();

        try {

            // carregar estrutura de campos do form
            $formId = $data['formId'];
            $userID = $data['userID'];

            $tipoSel = $data['fieldDocId'];
            $tipoSelRep = $data['fieldDocIdRep'];
            $qualidadeRep = $data['fieldQualidadeRep'];
            $outraQualRep = $data['fieldOutraQualRep'];
            $tipoPedido = $data['fieldtipopedido'];
            $tipoEspecie = $data['fieldTipoEspecie'];
            $fieldOutroTipoEspecie = $data['fieldOutroTipoEspecie'];

            $nif = $data['fieldNif'];
            $numId = $data['fieldNumId'];
            $telemovel = $data['fieldTelemovel'];
            $telefone = $data['fieldTelefone'];
            $fax = $data['fieldFax'];
            $nifRep = $data['fieldNifRep'];
            $numIdRep = $data['fieldNumIdRep'];
            $telemovelRep = $data['fieldTelemovelRep'];
            $telefoneRep = $data['fieldTelefoneRep'];
            $faxRep = $data['fieldFaxRep'];

            if($nif == ''){
                $nif = 'Null';
            }

            if($numId == ''){
                $numId = 'Null';
            }

            if($telemovel == ''){
                $telemovel = 'Null';
            }

            if($telefone == ''){
                $telefone = 'Null';
            }

            if($fax == ''){
                $fax = 'Null';
            }

            if($nifRep == ''){
                $nifRep = 'Null';
            }

            if($numIdRep == ''){
                $numIdRep = 'Null';
            }

            if($telemovelRep == ''){
                $telemovelRep = 'Null';
            }

            if($telefoneRep == ''){
                $telefoneRep = 'Null';
            }

            if($faxRep == ''){
                $faxRep = 'Null';
            }

            if($tipoSel == 1){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
            } else if($tipoSel == 2){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
            } else {
                $docIdentificacao = '';
            }

            if($tipoSelRep == 1){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
            } else if($tipoSelRep == 2){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
            } else {
                $docIdentificacaoRep = '';
            }

            if($qualidadeRep == 1){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
            } else if($qualidadeRep == 2){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
            } else if($qualidadeRep == 3){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
            } else if($qualidadeRep == 4){
                $QualidadeRep = $outraQualRep;
            }  else {
                $QualidadeRep = '';
            }

            if($tipoPedido == 1){
                $TipoPedido = JText::_('COM_VIRTUALDESK_AMBIENTE_RECOLHAANIMAIS_RECOLHA');
            } else if($tipoPedido == 2){
                $TipoPedido = JText::_('COM_VIRTUALDESK_AMBIENTE_RECOLHAANIMAIS_CAPTURA');
            } else {
                $TipoPedido = '';
            }

            if($tipoEspecie == 1){
                $TipoEspecie = JText::_('COM_VIRTUALDESK_AMBIENTE_RECOLHAANIMAIS_CAO');
            } else if($tipoEspecie == 2){
                $TipoEspecie = JText::_('COM_VIRTUALDESK_AMBIENTE_RECOLHAANIMAIS_GATO');
            } else if($tipoEspecie == 3){
                $TipoEspecie = $fieldOutroTipoEspecie;
            } else {
                $TipoEspecie = '';
            }

            /*Gerar Referencia UNICA*/
            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia = self::random_code();
                $checkREF   = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            /*END Gerar Referencia UNICA*/

            $data['fieldNif'] = $nif;
            $data['fieldNumId'] = $numId;
            $data['fieldTelemovel'] = $telemovel;
            $data['fieldTelefone'] = $telefone;
            $data['fieldFax'] = $fax;
            $data['fieldNifRep'] = $nifRep;
            $data['fieldNumIdRep'] = $numIdRep;
            $data['fieldTelemovelRep'] = $telemovelRep;
            $data['fieldTelefoneRep'] = $telefoneRep;
            $data['fieldFaxRep'] = $faxRep;
            $data['fieldDocId'] = $docIdentificacao;
            $data['fieldDocIdRep'] = $docIdentificacaoRep;
            $data['fieldQualidadeRep'] = $QualidadeRep;
            $data['fieldtipopedido'] = $TipoPedido;
            $data['fieldTipoEspecie'] = $TipoEspecie;
            $data['referencia'] = $referencia;

            // getFormFieldEstrutura
            $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

            // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
            $data2Create = array();
            foreach($data as $keySt => $valSt){
                $tmp = array();
                $tmp['fieldtag'] = $keySt;
                $tmp['campo_id'] = $dataValues[$keySt]['id'];
                $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                $tmp['val'] = $valSt;
                $data2Create[] = $tmp;
            }

            /* Save Pedido*/
            VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
            /*END Save Pedido*/

        }
        catch (Exception $e){
            //$db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.addnew2viaalvaraconcessao4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewadocaoanimais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewalvaraconcessao4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewcolocacaolapides4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewentregaanimais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewequipamentosresurbanos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewimunacao4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewinsalubridadeambiental4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewobrasjazigos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewocupacaoossarios4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewprorrogacaoobrasjazigos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewrecolhaanimais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewrecolharesiduosurbanos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewrecolharesiduosurbanoscomerciais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewruidofestas4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewruidoobras4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.addnewterrenojazigo4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.edit2viaalvaraconcessao4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editadocaoanimais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editalvaraconcessao4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editcolocacaolapides4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editentregaanimais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editequipamentosresurbanos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editimunacao4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editinsalubridadeambiental4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editobrasjazigos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editocupacaoossarios4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editprorrogacaoobrasjazigos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editrecolhaanimais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editrecolharesiduosurbanos4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editrecolharesiduosurbanoscomerciais4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editruidofestas4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editruidoobras4user.ambiente.data', null);
        $app->setUserState('com_virtualdesk.editterrenojazigo4user.ambiente.data', null);
    }
}
?>