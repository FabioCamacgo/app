<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteTicketsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tickets.php');


/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteTicketsStatsHelper
{
    const tagchaveModulo = 'tickets';

    public $TotalPedidosOnline;
    public $TotalPedidosBalcao;
    public $TotalPedidosPorEstado;
    public $TotalResolvidos;
    public $TotalNaoResolvidos;
    public $TotaisPorFreguesia;
    public $TotaisPorCategoria;
    public $TotaisPorAnoMes;
    public $TempoMedioEstado;
    public $TotalDeProcessos;
    public $DadosImediatos;

    public $TotalPedidosOnline4User;
    public $TotalPedidosBalcao4User;
    public $TotalPedidosPorEstado4User;
    public $TotalResolvidos4User;
    public $TotalNaoResolvidos4User;
    public $TotaisPorCategoria4User;
    public $TotaisPorAnoMes4User;
    public $TotalDeProcessos4User;

    private $UserJoomlaID;
    private $SessionUserNIF;

    private  $host;
    private  $user;
    private  $password;
    private  $database;
    private  $dbprefix;


    public function __construct()
    {   $conf           = JFactory::getConfig();
        $this->host     = $conf->get('host');
        $this->user     = $conf->get('user');
        $this->password = $conf->get('password');
        $this->database = $conf->get('db');
        $this->dbprefix = $conf->get('dbprefix');

        $this->UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $this->SessionUserNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser( $this->UserJoomlaID );
    }


    public function setAllStats($SetAgruparPorModulo=false) {

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário apresenta estatísticas do módulo desligado
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $this->setTotalDeProcessos();
        $this->getPedidosOnlineBalcao();
        $this->getTotaisPorEstado();
        $this->getTotaisResolvidosAndNot();
        $this->getTotaisPorCategoria($SetAgruparPorModulo);
        $this->getTotaisPorAnoMes();
        $this->getTempoMedioPorEstado();
        $this->getDadosImediatos();

    }

    public function setAllStats4User($SetAgruparPorModulo=false) {

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário apresenta estatísticas do módulo desligado
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $this->setTotalDeProcessos4User();
        $this->getPedidosOnlineBalcao4User();
        $this->getTotaisPorEstado4User();
        $this->getTotaisResolvidosAndNot4User();
        $this->getTotaisPorCategoria4User($SetAgruparPorModulo);
        $this->getTotaisPorAnoMes4User();
    }

    public function setTotalDeProcessos ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TotalDeProcessos = 0;

            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.id) as nprocessos ";
            $query .= " FROM #__virtualdesk_tickets as a ";
            $db->setQuery($query);
            $this->TotalDeProcessos = $db->loadResult();

            return(true);

    }


    public function setTotalDeProcessos4User ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TotalDeProcessos4User = 0;

        $db = JFactory::getDBO();

        $query  = " SELECT COUNT(Distinct a.id) as nprocessos ";
        $query .= " FROM #__virtualdesk_tickets as a ";
        $query .= " WHERE a.nif =".$this->SessionUserNIF;
        $db->setQuery($query);
        $this->TotalDeProcessos4User = $db->loadResult();

        return(true);

    }

    public function getTotaisPorEstado ()
    {
        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte
        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;
        $this->TotalPedidosPorEstado = array();

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            $db     = JFactory::getDBO();
            $query  = " SELECT COUNT(Distinct a.id) as npedidos, IFNULL(b.".$EstadoName.", ' ') as estado, IFNULL(a.id_estado, '0') as idestado ";
            $query .= " FROM #__virtualdesk_tickets as a ";
            $query .= " LEFT JOIN #__virtualdesk_tickets_estado AS b  ON b.id = a.id_estado ";
            $query .= " GROUP BY a.id_estado";

            $db->setQuery($query);

            $this->TotalPedidosPorEstado = $db->loadObjectList();
            return(true);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getTotaisPorEstado4User ()
    {
        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte
        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;
        $this->TotalPedidosPorEstado4User = array();

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            $db     = JFactory::getDBO();
            $query  = " SELECT COUNT(Distinct a.id) as npedidos, IFNULL(b.".$EstadoName.", ' ') as estado, IFNULL(a.id_estado, '0') as idestado ";
            $query .= " FROM #__virtualdesk_tickets as a ";
            $query .= " LEFT JOIN #__virtualdesk_tickets_estado AS b  ON b.id = a.id_estado ";
            $query .= " WHERE a.nif =".$this->SessionUserNIF;
            $query .= " GROUP BY a.id_estado";

            $db->setQuery($query);

            $this->TotalPedidosPorEstado4User = $db->loadObjectList();
            return(true);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getTotaisResolvidosAndNot()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        if(empty($this->TotalPedidosPorEstado)) $this->getTotaisPorEstado();
        if(!is_array($this->TotalPedidosPorEstado)) $this->TotalPedidosPorEstado = array();

        $this->TotalResolvidos    = 0;
        $this->TotalNaoResolvidos = 0;

        // Neste caso podem haver vários estados de conclusão
        $arIdConcluido = VirtualDeskSiteTicketsHelper::getEstadoIdConcluido();

        if(!is_array($arIdConcluido)) $arIdConcluido = array();

        foreach ($this->TotalPedidosPorEstado as $row) {
            if(in_array((int)$row->idestado, $arIdConcluido)) {
                $this->TotalResolvidos  += (int)$row->npedidos;
            }
            else {
                $this->TotalNaoResolvidos += (int)$row->npedidos;
            }
        }
        return(true);
    }


    public function getTotaisResolvidosAndNot4User()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        if(empty($this->TotalPedidosPorEstado4User)) $this->getTotaisPorEstado4User();
        if(!is_array($this->TotalPedidosPorEstado4User)) $this->TotalPedidosPorEstado4User = array();

        $this->TotalResolvidos4User    = 0;
        $this->TotalNaoResolvidos4User = 0;

        // Neste caso podem haver vários estados de conclusão
        $arIdConcluido = VirtualDeskSiteTicketsHelper::getEstadoIdConcluido();

        if(!is_array($arIdConcluido)) $arIdConcluido = array();

        foreach ($this->TotalPedidosPorEstado4User as $row) {
            if(in_array((int)$row->idestado, $arIdConcluido)) {
                $this->TotalResolvidos4User  += (int)$row->npedidos;
            }
            else {
                $this->TotalNaoResolvidos4User += (int)$row->npedidos;
            }
        }
        return(true);
    }


    public function getTotaisPorCategoria ($setAgruparPorModulo=false)
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TotaisPorCategoria = array();

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'nome_PT';
            }
            else {
                $CatName = 'nome_EN';
            }

            $CatNemSQL = " IFNULL(b.".$CatName.", ' ') as categoria ";
            if($setAgruparPorModulo===true) $CatNemSQL = " '".JText::_('COM_VIRTUALDESK_TICKETS_HEADING') . "' as categoria ";

            $db = JFactory::getDBO();
            $query  = " SELECT COUNT(Distinct a.id) as npedidos, ".$CatNemSQL.", IFNULL(a.id_departamento, '0') as idcategoria ";
            $query .= " FROM #__virtualdesk_tickets as a ";
            $query .= " LEFT JOIN #__virtualdesk_tickets_departamento AS b ON b.id = a.id_departamento ";
            $query .= " GROUP BY a.id_departamento";

            $db->setQuery($query);

            $this->TotaisPorCategoria = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getTotaisPorCategoria4User ($setAgruparPorModulo=false)
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TotaisPorCategoria4User = array();

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'nome_PT';
            }
            else {
                $CatName = 'nome_EN';
            }

            $CatNemSQL = " IFNULL(b.".$CatName.", ' ') as categoria ";
            if($setAgruparPorModulo===true) $CatNemSQL = " '".JText::_('COM_VIRTUALDESK_TICKETS_HEADING') . "' as categoria ";

            $db = JFactory::getDBO();
            $query  = " SELECT COUNT(Distinct a.id) as npedidos, ".$CatNemSQL.", IFNULL(a.id_departamento, '0') as idcategoria ";
            $query .= " FROM #__virtualdesk_tickets as a ";
            $query .= " LEFT JOIN #__virtualdesk_tickets_departamento AS b ON b.id = a.id_departamento ";
            $query .= " WHERE a.nif =".$this->SessionUserNIF;
            $query .= " GROUP BY a.id_departamento";

            $db->setQuery($query);

            $this->TotaisPorCategoria4User = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getTotaisPorAnoMes ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte dos relatórios

        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TotaisPorAnoMes = array();

        try
        {
            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.id) as npedidos, YEAR(a.data_criacao) as year , MONTH(a.data_criacao) as mes ";
            $query .= " FROM #__virtualdesk_tickets as a ";
            $query .= " where a.data_criacao >= DATE_SUB(now(), INTERVAL 12 MONTH) ";
            $query .= " GROUP BY YEAR(a.data_criacao), MONTH(a.data_criacao)";

            $db->setQuery($query);

            $this->TotaisPorAnoMes = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getTotaisPorAnoMes4User ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte dos relatórios

        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;
        if( (int) $this->SessionUserNIF <=0 )  return false;

        $this->TotaisPorAnoMes4User = array();

        try
        {
            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.id) as npedidos, YEAR(a.data_criacao) as year , MONTH(a.data_criacao) as mes ";
            $query .= " FROM #__virtualdesk_tickets as a ";
            $query .= " where a.data_criacao >= DATE_SUB(now(), INTERVAL 12 MONTH) AND a.nif =".$this->SessionUserNIF;
            $query .= " GROUP BY YEAR(a.data_criacao), MONTH(a.data_criacao)";

            $db->setQuery($query);

            $this->TotaisPorAnoMes4User = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getPedidosOnlineBalcao ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TotalPedidosOnline = 0;
        $this->TotalPedidosBalcao = 0;

        try
        {
            $db = JFactory::getDBO();

            $query1  = " SELECT COUNT(Distinct a.id) as npedidos_online  ";
            $query1 .= " FROM #__virtualdesk_tickets as a WHERE balcao=0";
            $db->setQuery($query1);
            $this->TotalPedidosOnline = (int)$db->loadResult();

            $query2  = " SELECT COUNT(Distinct a.id) as npedidos_balcao  ";
            $query2 .= " FROM #__virtualdesk_tickets as a WHERE balcao=1";
            $db->setQuery($query2);
            $this->TotalPedidosBalcao = (int) $db->loadResult();

           return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getPedidosOnlineBalcao4User ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TotalPedidosOnline4User = 0;
        $this->TotalPedidosBalcao4User = 0;

        try
        {
            $db = JFactory::getDBO();

            $query1  = " SELECT COUNT(Distinct a.id) as npedidos_online  ";
            $query1 .= " FROM #__virtualdesk_tickets as a WHERE balcao=0";
            $query1 .= " AND a.nif =".$this->SessionUserNIF;
            $db->setQuery($query1);
            $this->TotalPedidosOnline4User = (int)$db->loadResult();

            $query2  = " SELECT COUNT(Distinct a.id) as npedidos_balcao  ";
            $query2 .= " FROM #__virtualdesk_tickets as a WHERE balcao=1";
            $query2 .= " AND a.nif =".$this->SessionUserNIF;
            $db->setQuery($query2);
            $this->TotalPedidosBalcao4User = (int) $db->loadResult();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Vai comparar da ata de criação com a data do estado atual na tabela estado_historico */
    public function getTempoMedioPorEstado ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;

        $this->TempoMedioEstado = array();

        try
        {

            // Todo chamar por método....
            $IdEstadoInicial    = 1;
            $vsEstadoConcluido = '(4)';
            $TempoMedio = array();

            $db = JFactory::getDBO();


            // 1ª situação: não tem registo de histórico, então conta estado 1 data criação até data hoje
            $querySemAlteracaoEstado   = " select sum(DATEDIFF(now(), list.b_created)) as Soma, COUNT(DISTINCT b_idevento) as Total
FROM (
SELECT  b.id_evento as b_idevento, b.estado as b_estado, b.data_criacao as b_created, a.id, a.id_ticket, a.id_estado, a.created
  , LAG(created, 1) OVER (PARTITION BY a.id_ticket ORDER BY a.created desc)  next_created_value
  , LAG(created, 1) OVER (PARTITION BY a.id_ticket  ORDER BY a.created asc)  prev_created_value
  , DATEDIFF(LAG(created, 1) OVER (PARTITION BY a.id_ticket     ORDER BY a.created desc), created) as diff
  FROM #__virtualdesk_tickets AS b
  LEFT JOIN #__virtualdesk_tickets_estado_historico AS a ON a.id_ticket = b.id_evento  ORDER BY a.id_ticket, a.created, b.id_evento ) as list
WHERE (list.next_created_value is null and list.prev_created_value is null and b_estado = ".$IdEstadoInicial.' )';
            $db->setQuery($querySemAlteracaoEstado);
            $TempoTotalSemIntervencao = $db->loadObject();
            if((int) $TempoTotalSemIntervencao->Total>0) {
                $TempoMedio[1] += round((int)$TempoTotalSemIntervencao->Soma / (int)$TempoTotalSemIntervencao->Total);
            }


            // 2ª situação , o histórico next está null mas o prev está prrenchido é o estado concluído (mas forçamos que seja o estado = 3)
            $queryConcluidos  = " select   
sum(DATEDIFF(list.a_created, list.prev_created_value)) as Soma
, COUNT(*) as Total
from
(
  select  
    b.estado as b_estado,
    b.id_evento as b_idevento,
    b.data_criacao as b_created,
    a.id,
    a.id_ticket,
    a.id_estado as a_idestado,
    a.created as a_created,
    LAG(created, 1) OVER (PARTITION BY a.id_ticket
    ORDER BY a.created desc)                       next_created_value,
    LAG(created, 1) OVER (PARTITION BY a.id_ticket
    ORDER BY a.created asc)                        prev_created_value,
    DATEDIFF(LAG(created, 1) OVER (PARTITION BY a.id_ticket
             ORDER BY a.created desc), created) as diff
  from
    #__virtualdesk_tickets AS b
    LEFT JOIN #__virtualdesk_tickets_estado_historico AS a ON a.id_ticket = b.id_evento
  ORDER BY a.id_ticket, a.created, b.id_evento
) as list
WHERE (list.next_created_value is null and list.prev_created_value is not null and b_estado in ".$vsEstadoConcluido." ) ";
            $db->setQuery($queryConcluidos);
            $TempoTotalConcluidos = $db->loadObject();
            if((int) $TempoTotalConcluidos->Total>0) {
                $TempoMedio[3] += round((int)$TempoTotalConcluidos->Soma / (int)$TempoTotalConcluidos->Total);
            }


            // 3ª Situação, soma o tempo que levou a fazerem várias alterações no estado (não é o inicial nem o final), mas pode ter passado por um desses
            //  Devemos contar e separar  por estado 1, 2 e 3 , quando o prev e o next não são nulos
            $queryHistorico  = " select
    sum(case when list.a_idestado=1        then DATEDIFF( list.next_created_value , list.a_created) end) as Soma1
  , sum(case when list.a_idestado in (2,3) then DATEDIFF( list.next_created_value , list.a_created) end) as Soma2
  , COUNT(*) as Total
from
  (
  select  
      b.estado as b_estado,
      b.id_evento as b_idevento,
      b.data_criacao as b_created,
      a.id,
      a.id_ticket,
      a.id_estado as a_idestado,
      a.created as a_created,
      LAG(created, 1) OVER (PARTITION BY a.id_ticket
      ORDER BY a.created desc)  next_created_value,
      LAG(created, 1) OVER (PARTITION BY a.id_ticket
      ORDER BY a.created asc)   prev_created_value,
      DATEDIFF(LAG(created, 1) OVER (PARTITION BY a.id_ticket
               ORDER BY a.created desc), created) as diff
    from
      #__virtualdesk_tickets AS b
      LEFT JOIN #__virtualdesk_tickets_estado_historico AS a ON a.id_ticket = b.id_evento

    ORDER BY a.id_ticket, a.created, b.id_evento
  ) as list

where (list.next_created_value is not null and list.prev_created_value is not null )  ";
            $db->setQuery($queryHistorico);
            $TempoTotalHistorico = $db->loadObject();
            if((int) $TempoTotalHistorico->Total>0) {
                $TempoMedio[1] += round((int)$TempoTotalHistorico->Soma1 / (int)$TempoTotalHistorico->Total);
                $TempoMedio[2] += round((int)$TempoTotalHistorico->Soma2 / (int)$TempoTotalHistorico->Total);

            }

            $this->TempoMedioEstado = $TempoMedio;

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Vai comparar da ata de criação com a data do estado atual na tabela estado_historico */
    public function getDadosImediatos ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte
        if( empty($this->UserJoomlaID) )  return false;
        if( (int)$this->UserJoomlaID<=0 )  return false;


        try
        {
            $this->DadosImediatos = new stdClass();

            $db = JFactory::getDBO();

            $queryUltimoMes  = " select  COUNT(id) as Total from gtonq_virtualdesk_tickets where  data_criacao >= DATE(NOW()) - INTERVAL 1 MONTH ";
            $db->setQuery($queryUltimoMes);
            $this->DadosImediatos->UltimoMes= $db->loadResult();

            $queryUltimoSemana = " select  COUNT(id) as Total from gtonq_virtualdesk_tickets where data_criacao >= DATE(NOW()) - INTERVAL 7 DAY ";
            $db->setQuery($queryUltimoSemana);
            $this->DadosImediatos->UltimaSemana= $db->loadResult();

            $queryOntem = " select  COUNT(id) as Total from gtonq_virtualdesk_tickets where data_criacao >= DATE(NOW()- INTERVAL 1 DAY) ";
            $db->setQuery($queryOntem);
            $this->DadosImediatos->Ontem= $db->loadResult();

            $queryHoje = " select  COUNT(id) as Total from gtonq_virtualdesk_tickets where data_criacao = DATE(NOW()) ";
            $db->setQuery($queryHoje);
            $this->DadosImediatos->Hoje= $db->loadResult();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

}
