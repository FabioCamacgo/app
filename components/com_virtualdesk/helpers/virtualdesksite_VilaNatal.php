<?php
/**
 * Created by PhpStorm.
 * User: Camacho
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');

    class VirtualDeskSiteVilaNatalHelper
    {

        /*Verifica se o nif já existe*/
        public static function seeNIFExiste($fiscalid){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nipc')
                ->from("#__virtualdesk_VilaNatal_Empresas")
                ->where($db->quoteName('nipc') . "='" . $db->escape($fiscalid) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Insere o novo alerta na BD*/
        public static function saveNewEmpresa($nome, $fiscalid, $horario, $facebook, $instagramform, $Website)
        {
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('nome','nipc','horario','facebook','instagram','website');
            $values = array($db->quote($nome), $db->quote($fiscalid), $db->quote($horario), $db->quote($facebook), $db->quote($instagramform), $db->quote($Website));
            $query
                ->insert($db->quoteName('#__virtualdesk_VilaNatal_Empresas'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);

        }

        /*Envia Mail ao utilizador*/
        public static function SendEmailOcorrencia($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone){

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Alerta_Email.html');

            require_once(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpdf.php');
            $pdf = new VirtualDeskPDFHelper();
            $pdfAdmin = new VirtualDeskPDFHelper();

            if(empty($lat)){
                $latitude = 'Não indicado';
                $longitude = 'Não indicado';
            } else{
                $latitude = $lat;
                $longitude = $long;
            }

            $pdf->pdfAlertaAdmin($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $latitude, $longitude, $fiscalid, $telefone);

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $email;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
            $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_INTRO',$nome);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO');
            $BODY_SECONDLINE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO2',$referencia);
            $BODY_THIRDLINE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO3');

            $BODY_REFERENCIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_REFERENCIA');
            $BODY_REFERENCIA_VALUE       = JText::sprintf($referencia);
            $BODY_CATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CATEGORIA');
            $BODY_CATEGORIA_VALUE       = JText::sprintf($catName);
            $BODY_SUBCATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SUBCATEGORIA');
            $BODY_SUBCATEGORIA_VALUE       = JText::sprintf($subcatName);
            $BODY_DESCRICAO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE       = JText::sprintf($descricao);
            $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FREGUESIA');
            $BODY_FREGUESIA_VALUE       = JText::sprintf($fregName);
            $BODY_SITIO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SITIO');
            $BODY_SITIO_VALUE       = JText::sprintf($sitio);
            $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_MORADA');
            $BODY_MORADA_VALUE       = JText::sprintf($morada);
            $BODY_PTOSREF_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_PTOSREF');
            $BODY_PTOSREF_VALUE       = JText::sprintf($ptosrefs);
            $BODY_LAT_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LATITUDE');
            $BODY_LAT_VALUE       = JText::sprintf($latitude);
            $BODY_LONG_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LONGITUDE');
            $BODY_LONG_VALUE       = JText::sprintf($longitude);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_TITLE",$BODY_REFERENCIA_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_VALUE",$BODY_REFERENCIA_VALUE, $body);
            $body      = str_replace("%BODY_CATEGORIA_TITLE",$BODY_CATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_CATEGORIA_VALUE",$BODY_CATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_TITLE",$BODY_SUBCATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_VALUE",$BODY_SUBCATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_DESCRICAO_TITLE",$BODY_DESCRICAO_TITLE, $body);
            $body      = str_replace("%BODY_DESCRICAO_VALUE",$BODY_DESCRICAO_VALUE, $body);
            $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
            $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
            $body      = str_replace("%BODY_SITIO_TITLE",$BODY_SITIO_TITLE, $body);
            $body      = str_replace("%BODY_SITIO_VALUE",$BODY_SITIO_VALUE, $body);
            $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
            $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
            $body      = str_replace("%BODY_PTOSREF_TITLE",$BODY_PTOSREF_TITLE, $body);
            $body      = str_replace("%BODY_PTOSREF_VALUE",$BODY_PTOSREF_VALUE, $body);
            $body      = str_replace("%BODY_LAT_TITLE",$BODY_LAT_TITLE, $body);
            $body      = str_replace("%BODY_LAT_VALUE",$BODY_LAT_VALUE, $body);
            $body      = str_replace("%BODY_LONG_TITLE",$BODY_LONG_TITLE, $body);
            $body      = str_replace("%BODY_LONG_VALUE",$BODY_LONG_VALUE, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Alerta_BannerEmail.png', "banner", "Alerta_BannerEmail.png");

            $pdf->SetIdProcesso ($referencia);
            $pdf->SetTagProcesso ('Ocorrências');
            $resSave = $pdf->saveFile();
            if($resSave==true) {
                $resFilepath = $pdf->resFileSaved ['filepath'];
                $resFilename = $pdf->resFileSaved ['filename'];
                $newActivationEmail->addAttachment(JPATH_BASE.'/'.$resFilepath.'/'.$resFilename);
            }

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }
    }
?>