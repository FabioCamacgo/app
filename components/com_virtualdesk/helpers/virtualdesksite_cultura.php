<?php

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteCulturaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura_files.php');
JLoader::register('VirtualDeskTableCulturaEventos', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/cultura_eventos.php');
JLoader::register('VirtualDeskTableCulturaEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/cultura_estado_historico.php');


    class VirtualDeskSiteCulturaHelper
    {
        const tagchaveModulo = 'cultura';

        public static function getConcelhos()
        {

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCulturaFreguesia($idwebsitelist,$onAjaxVD=0)
        {
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('freguesia ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id as id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }


            return ($data);
        }


        public static function revertDataOrder($var){
            $splitData = explode("-", $var);
            $splitData[0];
            $splitData[1];
            $splitData[2];

            $dataFormated = $splitData[2] . '-' . $splitData[1] . '-' . $splitData[0];
            return ($dataFormated);
        }


        public static function getEventsFilter(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_evento, a.nif, a.referencia_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, c.freguesia as freguesia, a.data_inicio, a.mes, a.Ano, a.estado'))
                ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia_evento')
                ->from("#__virtualdesk_Cultura_Eventos as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEventsFiltered($nomePost, $categoriaPost, $freguesiaPost, $mesPost, $anoPost, $dataAtual){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $nome2 = '%' . $nomePost . '%';

            $db = JFactory::getDBO();

            $anoEscolhido = VirtualDeskSiteCulturaHelper::getAnoSelect($anoPost);

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            if(!empty($nomePost)){
                $where .= 'AND' . $db->quoteName('nome_evento') . "LIKE '" . $db->escape($nome2) . "'";
            }

            if(!empty($categoriaPost)){
                $where .= 'AND' . $db->quoteName('a.cat_evento') . "='" . $db->escape($categoriaPost) . "'";
            }

            if(!empty($freguesiaPost)){
                $where .= 'AND' . $db->quoteName('a.freguesia_evento') . "='" . $db->escape($freguesiaPost) . "'";
            }

            if(!empty($anoPost)){
                $where .= 'AND' . $db->quoteName('a.Ano') . "='" . $db->escape($anoEscolhido) . "'";
            }

            if(!empty($mesPost)){
                $where .= 'AND' . $db->quoteName('mes') . "='" . $db->escape($mesPost) . "'";
            }


            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_evento, a.nif, a.referencia_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, c.freguesia as freguesia, a.data_inicio, a.mes, a.Ano, a.estado'))
                ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia_evento')
                ->from("#__virtualdesk_Cultura_Eventos as a")
                ->where($where)
                ->order('data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getFregSelect($freguesia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getCatSelect($categoria2){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $CatName  . ' as categoria')
                ->from("#__virtualdesk_Cultura_Categoria as a")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria2) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCat($categoria2){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as categoria'))
                ->from("#__virtualdesk_Cultura_Categoria as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($categoria2) . "'")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEventos($estado, $dataAtual){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_evento, a.referencia_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, a.freguesia_evento, a.data_inicio, a.data_fim, a.estado'))
                ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                ->from("#__virtualdesk_Cultura_Eventos as a")
                ->where($db->quoteName('estado') . "='" . $db->escape($estado) . "'")
                ->where($db->quoteName('data_fim') . ">='" . $db->escape($dataAtual) . "'")
                ->setLimit('8')
                ->order('data_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventosAnteriores($estado, $dataAtual){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_evento, a.referencia_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, a.freguesia_evento, a.data_inicio, a.data_fim, a.estado'))
                ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                ->from("#__virtualdesk_Cultura_Eventos as a")
                ->where($db->quoteName('estado') . "='" . $db->escape($estado) . "'")
                ->setLimit('8')
                ->order('data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEventosTicker($estado, $dataAtual){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }



            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.data_inicio, a.data_fim, a.estado'))
                ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                ->from("#__virtualdesk_Cultura_Eventos as a")
                ->where($db->quoteName('estado') . "='" . $db->escape($estado) . "'")
                ->where($db->quoteName('data_fim') . ">='" . $db->escape($dataAtual) . "'")
                ->order('data_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEventosTickerAnteriores($estado){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_evento, a.referencia_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, a.freguesia_evento, a.data_inicio, a.data_fim, a.estado'))
                ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                ->from("#__virtualdesk_Cultura_Eventos as a")
                ->where($db->quoteName('estado') . "='" . $db->escape($estado) . "'")
                ->setLimit('8')
                ->order('data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getImgEventos($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_Cultura_Files")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getIdCat($categoria){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Cultura_Categoria")
                ->where($db->quoteName($CatName) . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getEstadoById($idestado){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado';
            }
            else {
                $EstadoName = 'estado';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_estado, '.$EstadoName.' as name ')
                ->from("#__virtualdesk_Cultura_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($idestado) . "'")
            );
            $data = $db->loadObject();
            return ($data);
        }

        public static function getImagemGeral($categoria){
            if($categoria == '1'){
                return('cinema.jpg');
            } else if($categoria == '2'){
                return('conferencia_workshops.jpg');
            } else if($categoria == '3'){
                return('desporto.jpg');
            } else if($categoria == '4'){
                return('exposicoes.jpg');
            } else if($categoria == '5'){
                return('literatura.jpg');
            } else if($categoria == '6'){
                return('musica.jpg');
            } else if($categoria == '7'){
                return('teatro_danca.jpg');
            } else if($categoria == '8'){
                return('tradicoes.jpg');
            } else if($categoria == '9'){
                return('feiras_Certames.jpg');
            } else if($categoria == '10'){
                return('festivais.jpg');
            } else if($categoria == '11'){
                return('lazer.jpg');
            }
        }


        public static function getImagemHorizontal($categoria){
            if($categoria == '1'){
                return('cinema.jpg');
            } else if($categoria == '2'){
                return('conferencia_workshops.jpg');
            } else if($categoria == '3'){
                return('desporto.jpg');
            } else if($categoria == '4'){
                return('exposicoes.jpg');
            } else if($categoria == '5'){
                return('literatura.jpg');
            } else if($categoria == '6'){
                return('musica.jpg');
            } else if($categoria == '7'){
                return('teatro_danca.jpg');
            } else if($categoria == '8'){
                return('tradicoes.jpg');
            } else if($categoria == '9'){
                return('feiras_Certames.jpg');
            } else if($categoria == '10'){
                return('festivais.jpg');
            } else if($categoria == '11'){
                return('lazer.jpg');
            }
        }


        public static function getImagemVertical($categoria){
            if($categoria == '1'){
                return('cinema.jpg');
            } else if($categoria == '2'){
                return('conferencia_workshops.jpg');
            } else if($categoria == '3'){
                return('desporto.jpg');
            } else if($categoria == '4'){
                return('exposicoes.jpg');
            } else if($categoria == '5'){
                return('literatura.jpg');
            } else if($categoria == '6'){
                return('musica.jpg');
            } else if($categoria == '7'){
                return('teatro_danca.jpg');
            } else if($categoria == '8'){
                return('tradicao.jpg');
            } else if($categoria == '9'){
                return('feiras_Certames.jpg');
            } else if($categoria == '10'){
                return('festivais.jpg');
            } else if($categoria == '11'){
                return('lazer.jpg');
            }
        }


        public static function compareDate($dataInicio, $dataAtual){
            $ArrDataAtual = explode("-",$dataAtual);
            $ArrDataInicio = explode("-",$dataInicio);

            if($ArrDataInicio[0] < $ArrDataAtual[0]){
                return (false);
            } else if($ArrDataInicio[0] == $ArrDataAtual[0]){
                if($ArrDataInicio[1] < $ArrDataAtual[1]){
                    return (false);
                } else if($ArrDataInicio[1] == $ArrDataAtual[1]){
                    if($ArrDataInicio[2] < $ArrDataAtual[2]){
                        return (false);
                    } else {
                        return (true);
                    }
                } else {
                    return (true);
                }
            } else{
                return (true);
            }

        }


        public static function getInfoEvento($id)
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_evento, a.coOrganizacao, a.referencia_evento, c.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, b.freguesia as freguesia, a.local_evento, a.data_inicio, a.hora_inicio, a.data_fim, a.hora_fim, a.latitude, a.longitude, a.website, a.facebook, a.instagram, a.observacoes, a.layout, d.nome as promotor'))
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS b ON b.id = a.freguesia_evento')
                ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS c ON c.id = a.cat_evento')
                ->join('LEFT', '#__virtualdesk_Cultura_Users AS d ON d.nif = a.nif')
                ->from("#__virtualdesk_Cultura_Eventos as a")
                ->where($db->quoteName('id_evento') . "='" . $db->escape($id) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNameMonth($month){
            if($month == '01'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_JAN'));
            } else if($month == '02'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_FEV'));
            } else if($month == '03'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_MAR'));
            } else if($month == '04'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_ABR'));
            } else if($month == '05'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_MAI'));
            } else if($month == '06'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_JUN'));
            } else if($month == '07'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_JUL'));
            } else if($month == '08'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_AGO'));
            } else if($month == '09'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_SET'));
            } else if($month == '10'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_OUT'));
            } else if($month == '11'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_NOV'));
            } else if($month == '12'){
                return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_DEZ'));
            }
        }


        public static function getIdMonth($startMonth){
            if($startMonth == '01'){
                return('1');
            } else if($startMonth == '02'){
                return('2');
            } else if($startMonth == '03'){
                return('3');
            } else if($startMonth == '04'){
                return('4');
            } else if($startMonth == '05'){
                return('5');
            } else if($startMonth == '06'){
                return('6');
            } else if($startMonth == '07'){
                return('7');
            } else if($startMonth == '08'){
                return('8');
            } else if($startMonth == '09'){
                return('9');
            } else if($startMonth == '10'){
                return('10');
            } else if($startMonth == '11'){
                return('11');
            } else if($startMonth == '12'){
                return('12');
            }
        }


        public static function getMeses(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Mes = 'mes_PT';
            }
            else {
                $Mes = 'mes_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $Mes . ' as mes'))
                ->from("#__virtualdesk_Cultura_Meses")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getMesSelect($mes){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Mes = 'mes_PT';
            }
            else {
                $Mes = 'mes_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Mes)
                ->from("#__virtualdesk_Cultura_Meses")
                ->where($db->quoteName('id') . "='" . $db->escape($mes) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeMes($mes){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Mes = 'mes_PT';
            }
            else {
                $Mes = 'mes_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $Mes . ' as mes'))
                ->from("#__virtualdesk_Cultura_Meses")
                ->where($db->quoteName('id') . "!='" . $db->escape($mes) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, Ano'))
                ->from("#__virtualdesk_Cultura_Ano")
                ->order('Ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnoSelect($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('Ano')
                ->from("#__virtualdesk_Cultura_Ano")
                ->where($db->quoteName('id') . "='" . $db->escape($ano) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeAno($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, Ano'))
                ->from("#__virtualdesk_Cultura_Ano")
                ->where($db->quoteName('id') . "!='" . $db->escape($ano) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getPromotor(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, nome, estado'))
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludePromotor($promotor){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, nome, estado'))
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($promotor) . "'")
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getPromotorSelect($promotor){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('id') . "='" . $db->escape($promotor) . "'")
                ->order('nome ASC')
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function RefEvent($fiscalid, $startDay, $startMonth, $endDay, $endMonth)
        {

            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);

            $refer = 'N' . $fiscalid . 'SD' . $startDay . 'SM' . $startMonth . $randomString . 'ED' . $endDay . 'EM' . $endMonth;

            return $refer;
        }


        public static function getCategoria(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as categoria'))
                ->from("#__virtualdesk_Cultura_Categoria as a")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatName($categoria)
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($CatName)
                ->from("#__virtualdesk_Cultura_Categoria")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getFreguesia($concelho)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeFreguesia($concelho, $freguesia2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($freguesia2) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getFregName($freguesia)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getConcName($concelho)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getNameFreg($freguesia)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function checkLogin($fiscalid)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('password')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function checkEstado($fiscalid)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('estado')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getNIF($nif)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nif')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($nif) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNIFPromotor($promotor){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nif')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('id') . "='" . $db->escape($promotor) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getEstadoNIF($nif)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('estado')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($nif) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function checkValidarNIF($nif) {

            $nif=trim($nif);
            $ignoreFirst=true;
            $errNif = 0;

            if (!is_numeric($nif) || strlen($nif)!=9) {
                $errNif = 1;
            } else {
                $nifSplit=str_split($nif);
                if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);

                    if($checkDigit>=10) $checkDigit=0;

                    if ($checkDigit==$nifSplit[8]) {

                    } else {
                        $errNif = 1;
                    }
                } else {
                    $errNif = 1;
                }
            }

            return($errNif);

        }


        public static function getUserEmail($fiscalid)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getUserName($fiscalid)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        /*Verifica se o email já existe*/
        public static function seeEmailExiste($email)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('email') . "='" . $db->escape($email) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        /*Retorna Nome da Associação e Pass*/
        public static function NomePass($email)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('nome', 'password'))
                ->from("#__virtualdesk_Cultura_Users")
                ->where($db->quoteName('email') . "='" . $db->escape($email) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategorias(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $CatName . ' as categoria'))
                ->from("#__virtualdesk_Cultura_Categoria")
                ->order(' ordering ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getImagemCategorias($categoria){
            if($categoria == '1'){
                return('cinema.jpg');
            } else if($categoria == '2'){
                return('conferencias_workshops.jpg');
            } else if($categoria == '3'){
                return('desporto.jpg');
            } else if($categoria == '4'){
                return('exposicoes.jpg');
            } else if($categoria == '5'){
                return('literatura.jpg');
            } else if($categoria == '6'){
                return('musica.jpg');
            } else if($categoria == '7'){
                return('teatro_danca.jpg');
            } else if($categoria == '8'){
                return('tradicoes.jpg');
            } else if($categoria == '9'){
                return('feiras_Certames.jpg');
            } else if($categoria == '10'){
                return('festivais.jpg');
            } else if($categoria == '11'){
                return('lazer.jpg');
            }
        }


        public static function SaveNovoRegisto($nome, $fiscalid, $telefone, $email, $morada, $concelho, $freguesia, $codPostal, $websitePromotor, $facebookPromotor, $instagramPromotor)
        {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('nif', 'tipo_User', 'email', 'nome', 'morada', 'concelho', 'freguesia', 'codigo_postal', 'telefone', 'website', 'facebook', 'instagram', 'estado');
            $values = array($db->quote($fiscalid), $db->quote('2'), $db->quote($email), $db->quote($nome), $db->quote($morada), $db->quote($concelho), $db->quote($freguesia), $db->quote($codPostal), $db->quote($telefone), $db->quote($websitePromotor), $db->quote($facebookPromotor), $db->quote($instagramPromotor), $db->quote('1'));
            $query
                ->insert($db->quoteName('#__virtualdesk_Cultura_Users'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);
        }


        public static function SaveNovoRegistoV2($fiscalid, $nomePromotor, $email)
        {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('nif', 'tipo_User', 'email', 'nome', 'estado');
            $values = array($db->quote($fiscalid), $db->quote('2'), $db->quote($email), $db->quote($nomePromotor), $db->quote('2'));
            $query
                ->insert($db->quoteName('#__virtualdesk_Cultura_Users'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);
        }


        public static function SaveUpdateRegistoV2($fiscalid, $nomePromotor, $email)
        {
            $db = JFactory::getDbo();

            $fields = array();

            if(!is_null($nomePromotor)) array_push($fields, 'nome="'.$db->escape($nomePromotor).'"');
            if(!is_null($email)) array_push($fields, 'email="'.$db->escape($email).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...


            $query = $db->getQuery(true);
            $query
                ->update('#__virtualdesk_Cultura_Users')
                ->set($fields)
                ->where(' nif = '.$db->escape($fiscalid));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);

        }


        public static function SendMailAdmin($contactoTelefCopyrightEmail, $dominioMunicipio, $emailCopyrightGeral, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $nome, $fiscalid, $email)
        {

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Cultura_NovoRegistoAdmin_Email.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $email;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOADMIN_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOADMIN_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOADMIN_INTRO', $nome);
            $BODY_MESSAGE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOADMIN_CORPO', $fiscalid);
            $BODY_DOMINIO_VALUE = JText::sprintf($dominioMunicipio);
            $BODY_COPYMAIL_VALUE = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYLINK_VALUE = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME_VALUE = JText::sprintf($copyrightAPP);
            $BODY_COPYTELEF_VALUE = JText::sprintf($contactoTelefCopyrightEmail);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_MESSAGE", $BODY_MESSAGE, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_DOMINIO_VALUE", $BODY_DOMINIO_VALUE, $body);
            $body = str_replace("%BODY_COPYMAIL_VALUE", $BODY_COPYMAIL_VALUE, $body);
            $body = str_replace("%BODY_COPYLINK_VALUE", $BODY_COPYLINK_VALUE, $body);
            $body = str_replace("%BODY_COPYNAME_VALUE", $BODY_COPYNAME_VALUE, $body);
            $body = str_replace("%BODY_COPYTELEF_VALUE", $BODY_COPYTELEF_VALUE, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Cultura_BannerEmail.png', "banner", "Cultura_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailClient($contactoTelefCopyrightEmail, $dominioMunicipio, $emailCopyrightGeral, $LinkCopyright, $nome, $fiscalid, $telefone, $email, $password, $morada, $freguesia, $codPostal, $copyrightAPP, $nomeMunicipio)
        {

            $config = JFactory::getConfig();

            $pass = base64_decode($password);

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Cultura_NovoRegistoClient_Email.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $email;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_TITULO',$nomeMunicipio);
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_TITULO',$nomeMunicipio);
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_INTRO', $nome,$nomeMunicipio);

            $BODY_NIF_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_NIF');
            $BODY_NIF_VALUE = JText::sprintf($fiscalid);
            $BODY_PASSWORD_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_PASSWORD');
            $BODY_PASSWORD_VALUE = JText::sprintf($pass);
            $BODY_TELEFONE_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_TELEFONE');
            $BODY_TELEFONE_VALUE = JText::sprintf($telefone);
            $BODY_EMAIL_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_EMAIL');
            $BODY_EMAIL_VALUE = JText::sprintf($email);
            $BODY_MORADA_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_MORADA');
            $BODY_MORADA_VALUE = JText::sprintf($morada);
            $BODY_FREGUESIA_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_FREGUESIA');
            $BODY_FREGUESIA_VALUE = JText::sprintf($freguesia);
            $BODY_CODPOSTAL_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOREGISTOCLIENT_CODPOSTAL');
            $BODY_CODPOSTAL_VALUE = JText::sprintf($codPostal);
            $BODY_DOMINIO_VALUE = JText::sprintf($dominioMunicipio);
            $BODY_COPYMAIL_VALUE = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYLINK_VALUE = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME_VALUE = JText::sprintf($copyrightAPP);
            $BODY_COPYTELEF_VALUE = JText::sprintf($contactoTelefCopyrightEmail);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_NIF_TITLE", $BODY_NIF_TITLE, $body);
            $body = str_replace("%BODY_NIF_VALUE", $BODY_NIF_VALUE, $body);
            $body = str_replace("%BODY_PASSWORD_TITLE", $BODY_PASSWORD_TITLE, $body);
            $body = str_replace("%BODY_PASSWORD_VALUE", $BODY_PASSWORD_VALUE, $body);
            $body = str_replace("%BODY_TELEFONE_TITLE", $BODY_TELEFONE_TITLE, $body);
            $body = str_replace("%BODY_TELEFONE_VALUE", $BODY_TELEFONE_VALUE, $body);
            $body = str_replace("%BODY_EMAIL_TITLE", $BODY_EMAIL_TITLE, $body);
            $body = str_replace("%BODY_EMAIL_VALUE", $BODY_EMAIL_VALUE, $body);
            $body = str_replace("%BODY_MORADA_TITLE", $BODY_MORADA_TITLE, $body);
            $body = str_replace("%BODY_MORADA_VALUE", $BODY_MORADA_VALUE, $body);
            $body = str_replace("%BODY_FREGUESIA_TITLE", $BODY_FREGUESIA_TITLE, $body);
            $body = str_replace("%BODY_FREGUESIA_VALUE", $BODY_FREGUESIA_VALUE, $body);
            $body = str_replace("%BODY_CODPOSTAL_TITLE", $BODY_CODPOSTAL_TITLE, $body);
            $body = str_replace("%BODY_CODPOSTAL_VALUE", $BODY_CODPOSTAL_VALUE, $body);
            $body = str_replace("%BODY_DOMINIO_VALUE", $BODY_DOMINIO_VALUE, $body);
            $body = str_replace("%BODY_COPYMAIL_VALUE", $BODY_COPYMAIL_VALUE, $body);
            $body = str_replace("%BODY_COPYLINK_VALUE", $BODY_COPYLINK_VALUE, $body);
            $body = str_replace("%BODY_COPYNAME_VALUE", $BODY_COPYNAME_VALUE, $body);
            $body = str_replace("%BODY_COPYTELEF_VALUE", $BODY_COPYTELEF_VALUE, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Cultura_BannerEmail.png', "banner", "Cultura_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SaveNovoEvento($fiscalid, $coOrganizacao, $categoria, $referencia, $nomeEvento, $descricao, $freguesia, $localEvento, $dataInicio, $horaInicio, $dataFim, $horaFim, $idMonth, $idAno, $lat, $long, $websiteEvento, $facebookEvento, $instagramEvento, $observacoes, $layout, $balcao=0)
        {
            $IdEstadoStart = (int)self::getEstadoIdInicio();

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('nif', 'coOrganizacao', 'referencia_evento', 'cat_evento', 'nome_evento', 'desc_evento', 'freguesia_evento', 'local_evento', 'data_inicio', 'hora_inicio', 'data_fim', 'hora_fim', 'mes', 'Ano', 'latitude', 'longitude', 'website', 'facebook', 'instagram', 'observacoes', 'layout', 'estado','balcao');
            $values = array($db->quote($fiscalid), $db->quote($coOrganizacao), $db->quote($referencia), $db->quote($categoria), $db->quote($nomeEvento), $db->quote($descricao), $db->quote($freguesia), $db->quote($localEvento), $db->quote($dataInicio), $db->quote($horaInicio), $db->quote($dataFim), $db->quote($horaFim), $db->quote($idMonth), $db->quote($idAno), $db->quote($lat), $db->quote($long), $db->quote($websiteEvento), $db->quote($facebookEvento), $db->quote($instagramEvento), $db->quote($observacoes), $db->quote($layout), $db->quote($IdEstadoStart),  $db->quote($balcao) );
            $query
                ->insert($db->quoteName('#__virtualdesk_Cultura_Eventos'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);
        }


        /*Insere o Evento editado na BD*/
        public static function saveEditedEvento4Manager($cultura_id, $coOrganizacao, $nif_evento, $cat_evento, $nome_evento, $desc_evento, $freguesia_evento, $local_evento, $dataInicio, $horaInicio, $dataFim, $horaFim, $idMonth, $idAno, $lat, $long, $website, $facebook, $instagram, $observacoes, $layout_evento)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }
            if((int)$cultura_id<=0) return false;
            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($nif_evento)) array_push($fields, 'nif='.$db->escape($nif_evento));
            if(!is_null($coOrganizacao)) array_push($fields, 'coOrganizacao="'.$db->escape($coOrganizacao).'"');
            if(!is_null($nome_evento)) array_push($fields, 'nome_evento="'.$db->escape($nome_evento).'"');
            if(!is_null($desc_evento)) array_push($fields, 'desc_evento="'.$db->escape($desc_evento).'"');
            if(!is_null($cat_evento)) array_push($fields, 'cat_evento='.$db->escape($cat_evento));
            if(!is_null($freguesia_evento))  array_push($fields, 'freguesia_evento='.$db->escape($freguesia_evento));
            if(!is_null($local_evento))  array_push($fields, 'local_evento="'.$db->escape($local_evento).'"');
            if(!is_null($dataInicio))  array_push($fields, 'data_inicio="'.$db->escape($dataInicio).'"');
            if(!is_null($idMonth))  array_push($fields, 'mes='.$db->escape($idMonth));
            if(!is_null($idAno))  array_push($fields, 'Ano='.$db->escape($idAno));
            if(!is_null($horaInicio))  array_push($fields, 'hora_inicio="'.$db->escape($horaInicio).'"');
            if(!is_null($dataFim))  array_push($fields, 'data_fim="'.$db->escape($dataFim).'"');
            if(!is_null($horaFim))  array_push($fields, 'hora_fim="'.$db->escape($horaFim).'"');

            if(!is_null($website))  array_push($fields, 'website="'.$db->escape($website).'"');
            if(!is_null($facebook))  array_push($fields, 'facebook="'.$db->escape($facebook).'"');
            if(!is_null($instagram))  array_push($fields, 'instagram="'.$db->escape($instagram).'"');
            if(!is_null($observacoes))  array_push($fields, 'observacoes="'.$db->escape($observacoes).'"');
            if(!is_null($layout_evento))  array_push($fields, 'layout='.$db->escape($layout_evento));

            if(!is_null($lat))  array_push($fields, 'latitude="'.$db->escape($lat).'"');
            if(!is_null($long))  array_push($fields, 'longitude="'.$db->escape($long).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $dateModified = new DateTime();
            array_push($fields, 'data_alteracao="'.$dateModified->format('Y-m-d H:i:s').'"');

            $query
                ->update('#__virtualdesk_Cultura_Eventos')
                ->set($fields)
                ->where(' id_evento = '.$db->escape($cultura_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        /*Insere o Evento editado na BD*/
        public static function saveEditedEvento4User($cultura_id, $coOrganizacao, $cat_evento, $nome_evento, $desc_evento, $freguesia_evento, $local_evento, $dataInicio, $horaInicio, $dataFim, $horaFim, $idMonth, $idAno, $lat, $long, $website, $facebook, $instagram, $observacoes, $layout_evento)
        {
            /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4users'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }
            if((int)$cultura_id<=0) return false;
            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($coOrganizacao)) array_push($fields, 'coOrganizacao="'.$db->escape($coOrganizacao).'"');
            if(!is_null($nome_evento)) array_push($fields, 'nome_evento="'.$db->escape($nome_evento).'"');
            if(!is_null($desc_evento)) array_push($fields, 'desc_evento="'.$db->escape($desc_evento).'"');
            if(!is_null($cat_evento)) array_push($fields, 'cat_evento='.$db->escape($cat_evento));
            if(!is_null($freguesia_evento))  array_push($fields, 'freguesia_evento='.$db->escape($freguesia_evento));
            if(!is_null($local_evento))  array_push($fields, 'local_evento="'.$db->escape($local_evento).'"');
            if(!is_null($dataInicio))  array_push($fields, 'data_inicio="'.$db->escape($dataInicio).'"');
            if(!is_null($idMonth))  array_push($fields, 'mes='.$db->escape($idMonth));
            if(!is_null($idAno))  array_push($fields, 'Ano='.$db->escape($idAno));
            if(!is_null($horaInicio))  array_push($fields, 'hora_inicio="'.$db->escape($horaInicio).'"');
            if(!is_null($dataFim))  array_push($fields, 'data_fim="'.$db->escape($dataFim).'"');
            if(!is_null($horaFim))  array_push($fields, 'hora_fim="'.$db->escape($horaFim).'"');

            if(!is_null($website))  array_push($fields, 'website="'.$db->escape($website).'"');
            if(!is_null($facebook))  array_push($fields, 'facebook="'.$db->escape($facebook).'"');
            if(!is_null($instagram))  array_push($fields, 'instagram="'.$db->escape($instagram).'"');
            if(!is_null($observacoes))  array_push($fields, 'observacoes="'.$db->escape($observacoes).'"');
            if(!is_null($layout_evento))  array_push($fields, 'layout='.$db->escape($layout_evento));

            if(!is_null($lat))  array_push($fields, 'latitude="'.$db->escape($lat).'"');
            if(!is_null($long))  array_push($fields, 'longitude="'.$db->escape($long).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $dateModified = new DateTime();
            array_push($fields, 'data_alteracao="'.$dateModified->format('Y-m-d H:i:s').'"');

            $query
                ->update('#__virtualdesk_Cultura_Eventos')
                ->set($fields)
                ->where(' id_evento = '.$db->escape($cultura_id)  .' and nif='.$UserSessionNIF);

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function SendMailAdminEvento($nomeMunicipio, $nameUser, $referencia){
            $config = JFactory::getConfig();

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('submeterevento','email2admin');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }
            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailCultura = $obParam->getParamsByTag('logosendmailCultura');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $websiteCopyrightEmail = $obParam->getParamsByTag('websiteCopyrightEmail');
            $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
            $mailAdminCultura = $obParam->getParamsByTag('mailAdminCultura');



            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOADMIN_TITULO',$nomeMunicipio);
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOADMIN_TITULO',$nomeMunicipio);
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOADMIN_CORPO', $nameUser, $referencia);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%TELEF", $contactoTelefCopyrightEmail, $body);
            $body = str_replace("%NAMESITE", $copyrightAPP, $body);
            $body = str_replace("%websiteCopyrightEmail", $websiteCopyrightEmail, $body);
            $body = str_replace("%linkCopyrightEmail", $linkCopyrightEmail, $body);
            $body = str_replace("%emailCopyrightGeral", $emailCopyrightGeral, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->setSender($mailAdminCultura);
            $newActivationEmail->setFrom($data['fromname']);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newActivationEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newActivationEmail->addRecipient($emailAdmin);

            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailCultura, "banner", "Logo");

            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }

        public static function SendMailAdminEventoUpdate($nomeEvento, $nameUser, $email, $referencia='')
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Cultura_EditEventoAdmin.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailCultura = $obParam->getParamsByTag('logosendmailCultura');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $websiteCopyrightEmail = $obParam->getParamsByTag('websiteCopyrightEmail');
            $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
            $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');


            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');


            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_UPDATEEVENTOADMIN_TITULO', $nomeMunicipio);
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_UPDATEEVENTOADMIN_TITULO', $nomeMunicipio);
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_UPDATEEVENTOADMIN_CORPO', $nomeEvento);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%TELEF", $contactoTelefCopyrightEmail, $body);
            $body = str_replace("%NAMESITE", $copyrightAPP, $body);
            $body = str_replace("%websiteCopyrightEmail", $websiteCopyrightEmail, $body);
            $body = str_replace("%linkCopyrightEmail", $linkCopyrightEmail, $body);
            $body = str_replace("%emailCopyrightGeral", $emailCopyrightGeral, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);

            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newActivationEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newActivationEmail->addRecipient($emailAdmin);

            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailCultura, "banner", "Logo");

            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }

        public static function SendMailAdminAltEstado($copyrightAPP, $emailCopyrightGeral, $LinkCopyright, $contactoTelefCopyrightEmail, $dominioMunicipio, $nomeEvento, $nameUser, $email, $referencia='',$Estado)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Cultura_AlterarEstado.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailCultura = $obParam->getParamsByTag('logosendmailCultura');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $websiteCopyrightEmail = $obParam->getParamsByTag('websiteCopyrightEmail');
            $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_ALTESTADOCLIENT_CORPO', $nomeEvento , $referencia);
            $BODY_SPACE = "__";

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_STATE", $Estado, $body);
            $body = str_replace("%BODY_SPACE", $BODY_SPACE, $body);
            $body = str_replace("%TELEF", $contactoTelefCopyrightEmail, $body);
            $body = str_replace("%NAMESITE", $copyrightAPP, $body);
            $body = str_replace("%websiteCopyrightEmail", $websiteCopyrightEmail, $body);
            $body = str_replace("%linkCopyrightEmail", $linkCopyrightEmail, $body);
            $body = str_replace("%emailCopyrightGeral", $emailCopyrightGeral, $body);
            $body = str_replace("%BODY_FILES_LIST_TITLE", '', $body);
            $body = str_replace("%BODY_FILES_LIST", '', $body);

            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom']);
            $newEmail->setSender($data['mailfrom']);
            $newEmail->setFrom($data['fromname']);

            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

            $newEmail->setSubject($data['sitename']);
            $newEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailCultura, "banner", "Logo");

            $return = (boolean)$newEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }

        public static function SendMailClientEvento($nameUser, $nomeEvento, $catName, $emailUser, $freguesiaName, $localEvento, $eventStart, $eventEnd, $descEmail, $web, $fac, $inst, $nomeMunicipio)
        {
            $config = JFactory::getConfig();

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('submeterevento','email2user');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }
            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);


            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailCultura = $obParam->getParamsByTag('logosendmailCultura');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $websiteCopyrightEmail = $obParam->getParamsByTag('websiteCopyrightEmail');
            $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_TITULO',$nomeMunicipio);
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_TITULO',$nomeMunicipio);
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_INTRO', $nameUser);
            $BODY_LINE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_INTRO1');
            $BODY_LINE3 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_INTRO2');
            $BODY_LINE4 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_INTRO3',$nomeMunicipio);
            $BODY_LINE5 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_INTRO4',$emailCopyrightGeral);
            $BODY_LINE6 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_INTRO5');

            $BODY_NOMEEVENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_NOMEEVENTO');
            $BODY_NOMEEVENTO_VALUE = JText::sprintf($nomeEvento);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descEmail);
            $BODY_CAT_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_CATEGORIA');
            $BODY_CAT_VALUE = JText::sprintf($catName);
            $BODY_FREGUESIA_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_FREGUESIA');
            $BODY_FREGUESIA_VALUE = JText::sprintf($freguesiaName);
            $BODY_LOCALEVENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_LOCALEVENTO');
            $BODY_LOCALEVENTO_VALUE = JText::sprintf($localEvento);
            $BODY_DATAINICIO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_DATAINICIO');
            $BODY_DATAINICIO_VALUE = JText::sprintf($eventStart);
            $BODY_DATAFIM_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_DATAFIM');
            $BODY_DATAFIM_VALUE = JText::sprintf($eventEnd);
            $BODY_WEBSITE_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_WEBSITE');
            $BODY_WEBSITE_VALUE = JText::sprintf($web);
            $BODY_FACEBOOK_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_FACEBOOK');
            $BODY_FACEBOOK_VALUE = JText::sprintf($fac);
            $BODY_INSTAGRAM_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_INSTAGRAM');
            $BODY_INSTAGRAM_VALUE = JText::sprintf($inst);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_LINE2", $BODY_LINE2, $body);
            $body = str_replace("%BODY_LINE3", $BODY_LINE3, $body);
            $body = str_replace("%BODY_LINE4", $BODY_LINE4, $body);
            $body = str_replace("%BODY_LINE5", $BODY_LINE5, $body);
            $body = str_replace("%BODY_LINE6", $BODY_LINE6, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_NOMEEVENTO_TITLE", $BODY_NOMEEVENTO_TITLE, $body);
            $body = str_replace("%BODY_NOMEEVENTO_VALUE", $BODY_NOMEEVENTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_LOCALEVENTO_TITLE", $BODY_LOCALEVENTO_TITLE, $body);
            $body = str_replace("%BODY_LOCALEVENTO_VALUE", $BODY_LOCALEVENTO_VALUE, $body);
            $body = str_replace("%BODY_CAT_TITLE", $BODY_CAT_TITLE, $body);
            $body = str_replace("%BODY_CAT_VALUE", $BODY_CAT_VALUE, $body);
            $body = str_replace("%BODY_FREGUESIA_TITLE", $BODY_FREGUESIA_TITLE, $body);
            $body = str_replace("%BODY_FREGUESIA_VALUE", $BODY_FREGUESIA_VALUE, $body);
            $body = str_replace("%BODY_DATAINICIO_TITLE", $BODY_DATAINICIO_TITLE, $body);
            $body = str_replace("%BODY_DATAINICIO_VALUE", $BODY_DATAINICIO_VALUE, $body);
            $body = str_replace("%BODY_DATAFIM_TITLE", $BODY_DATAFIM_TITLE, $body);
            $body = str_replace("%BODY_DATAFIM_VALUE", $BODY_DATAFIM_VALUE, $body);
            $body = str_replace("%BODY_WEBSITE_TITLE", $BODY_WEBSITE_TITLE, $body);
            $body = str_replace("%BODY_WEBSITE_VALUE", $BODY_WEBSITE_VALUE, $body);
            $body = str_replace("%BODY_FACEBOOK_TITLE", $BODY_FACEBOOK_TITLE, $body);
            $body = str_replace("%BODY_FACEBOOK_VALUE", $BODY_FACEBOOK_VALUE, $body);
            $body = str_replace("%BODY_INSTAGRAM_TITLE", $BODY_INSTAGRAM_TITLE, $body);
            $body = str_replace("%BODY_INSTAGRAM_VALUE", $BODY_INSTAGRAM_VALUE, $body);
            $body = str_replace("%TELEF", $contactoTelefCopyrightEmail, $body);
            $body = str_replace("%NAMESITE", $copyrightAPP, $body);
            $body = str_replace("%websiteCopyrightEmail", $websiteCopyrightEmail, $body);
            $body = str_replace("%linkCopyrightEmail", $linkCopyrightEmail, $body);
            $body = str_replace("%emailCopyrightGeral", $emailCopyrightGeral, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailCultura, "banner", "Logo");

            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }

        public static function SendMailClientEventoUpdate($nomeMunicipio, $nameUser, $nomeEvento, $descricao, $emailUser, $freguesiaName, $localEvento, $eventStart, $eventEnd)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Cultura_EditEvento_Email.html');

            $data['fromname'] = $config->get('fromname');
            // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailCultura = $obParam->getParamsByTag('logosendmailCultura');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $websiteCopyrightEmail = $obParam->getParamsByTag('websiteCopyrightEmail');
            $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');

            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_UPDATEEVENTOCLIENT_TITULO', $nomeMunicipio);
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_UPDATEEVENTOCLIENT_TITULO', $nomeMunicipio);
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_UPDATEEVENTOCLIENT_INTRO', $nameUser, $nomeMunicipio);

            $BODY_NOMEEVENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_NOMEEVENTO');
            $BODY_NOMEEVENTO_VALUE = JText::sprintf($nomeEvento);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
            $BODY_FREGUESIA_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_FREGUESIA');
            $BODY_FREGUESIA_VALUE = JText::sprintf($freguesiaName);
            $BODY_LOCALEVENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_LOCALEVENTO');
            $BODY_LOCALEVENTO_VALUE = JText::sprintf($localEvento);
            $BODY_DATAINICIO_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_DATAINICIO');
            $BODY_DATAINICIO_VALUE = JText::sprintf($eventStart);
            $BODY_DATAFIM_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_NOVOEVENTOCLIENT_DATAFIM');
            $BODY_DATAFIM_VALUE = JText::sprintf($eventEnd);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_NOMEEVENTO_TITLE", $BODY_NOMEEVENTO_TITLE, $body);
            $body = str_replace("%BODY_NOMEEVENTO_VALUE", $BODY_NOMEEVENTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_LOCALEVENTO_TITLE", $BODY_LOCALEVENTO_TITLE, $body);
            $body = str_replace("%BODY_LOCALEVENTO_VALUE", $BODY_LOCALEVENTO_VALUE, $body);
            $body = str_replace("%BODY_FREGUESIA_TITLE", $BODY_FREGUESIA_TITLE, $body);
            $body = str_replace("%BODY_FREGUESIA_VALUE", $BODY_FREGUESIA_VALUE, $body);
            $body = str_replace("%BODY_DATAINICIO_TITLE", $BODY_DATAINICIO_TITLE, $body);
            $body = str_replace("%BODY_DATAINICIO_VALUE", $BODY_DATAINICIO_VALUE, $body);
            $body = str_replace("%BODY_DATAFIM_TITLE", $BODY_DATAFIM_TITLE, $body);
            $body = str_replace("%BODY_DATAFIM_VALUE", $BODY_DATAFIM_VALUE, $body);
            $body = str_replace("%TELEF", $contactoTelefCopyrightEmail, $body);
            $body = str_replace("%NAMESITE", $copyrightAPP, $body);
            $body = str_replace("%websiteCopyrightEmail", $websiteCopyrightEmail, $body);
            $body = str_replace("%linkCopyrightEmail", $linkCopyrightEmail, $body);
            $body = str_replace("%emailCopyrightGeral", $emailCopyrightGeral, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailCultura, "banner", "Logo");

            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }

        public static function SendMailClientAltEstado($nameUser, $nomeEvento, $descricao, $emailUser, $freguesiaName, $Estado, $referencia)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Cultura_AlterarEstado.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailCultura = $obParam->getParamsByTag('logosendmailCultura');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $websiteCopyrightEmail = $obParam->getParamsByTag('websiteCopyrightEmail');
            $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');


            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');


            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_ALTESTADOCLIENT_CORPO', $nomeEvento , $referencia);
            $BODY_SPACE = "__";

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_STATE", $Estado, $body);
            $body = str_replace("%BODY_SPACE", $BODY_SPACE, $body);
            $body = str_replace("%TELEF", $contactoTelefCopyrightEmail, $body);
            $body = str_replace("%NAMESITE", $copyrightAPP, $body);
            $body = str_replace("%websiteCopyrightEmail", $websiteCopyrightEmail, $body);
            $body = str_replace("%linkCopyrightEmail", $linkCopyrightEmail, $body);
            $body = str_replace("%emailCopyrightGeral", $emailCopyrightGeral, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailCultura, "banner", "Logo");

            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }

        public static function SendEmailRecuperaPass($LinkCopyright, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $copyrightAPP, $email, $nome, $pass)
        {

            $config = JFactory::getConfig();

            $password = base64_decode($pass);

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Cultura_ForgotPassword_Email.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $email;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_FORGOTPASSWORD_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_FORGOTPASSWORD_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_FORGOTPASSWORD_INTRO', $nome);
            $BODY_MESSAGE = JText::sprintf('COM_VIRTUALDESK_CULTURA_EMAIL_FORGOTPASSWORD_CORPO', $password);
            $BODY_COPYLINK = JText::sprintf($LinkCopyright);
            $BODY_COPYTELE = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM = JText::sprintf($dominioMunicipio);
            $BODY_COPYNAME = JText::sprintf($copyrightAPP);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_MESSAGE", $BODY_MESSAGE, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_COPYLINK", $BODY_COPYLINK, $body);
            $body = str_replace("%BODY_COPYTELE", $BODY_COPYTELE, $body);
            $body = str_replace("%BODY_COPYMAIL", $BODY_COPYMAIL, $body);
            $body = str_replace("%BODY_COPYDOM", $BODY_COPYDOM, $body);
            $body = str_replace("%BODY_COPYNAME", $BODY_COPYNAME, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Cultura_BannerEmail.png', "banner", "Cultura_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        /* Define qual o  email por defeito do manager, se não existir devolve o do Admin */
        public static function getNameAndEmailDoManagerEAdmin(&$emailManager, &$emailAdmin)
        {
            $config                      = JFactory::getConfig();
            $objVDParams                 = new VirtualDeskSiteParamsHelper();
            $MailManagerbyDefault  = $objVDParams->getParamsByTag('Cultura_Mail_Manager_by_Default');
            $MailAdminbyDefault    = $objVDParams->getParamsByTag('mailAdminCultura');
            $adminSiteDefaultEmail       = $config->get('mailfrom');

            // Verifica se tem email do mananger da agenda de cultura do alerta caso contrário fica o admin do site
            $emailManager = $MailManagerbyDefault;
            if(empty($emailManager)) {
                $emailManager = $MailAdminbyDefault;
                if(empty($emailManager)) {
                    $emailManager =  $adminSiteDefaultEmail;
                }
            }

            // Verifica se tem email do admin do alerta caso contrário fica o admin do site
            $emailAdmin = $MailAdminbyDefault;
            if(empty($emailAdmin)) {
                $emailAdmin = $adminSiteDefaultEmail;
            }
            return(true);
        }


        public static function SaveLogsRecuperaPass($nome)
        {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->insert($db->quoteName('#__virtualdesk_Cultura_RecuperaPassword'))
                ->columns('nome')
                ->values($db->quote($nome));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);

        }


        public static function getLayout()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,layout'))
                ->from("#__virtualdesk_Cultura_Layout")
                ->order('layout ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getLayoutSelect($layout){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('layout')
                ->from("#__virtualdesk_Cultura_Layout")
                ->where($db->quoteName('id') . "='" . $db->escape($layout) . "'")
            );
            $data = $db->loadResult();
            return ($data);

        }


        public static function excludeLayout($layout)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,layout'))
                ->from("#__virtualdesk_Cultura_Layout")
                ->where($db->quoteName('id') . "!='" . $db->escape($layout) . "'")
                ->order('layout ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        /* Carrega lista com dados de todos os eventos (acesso ao USER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getCulturaList4User ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('cultura', 'list4users');
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'catName_PT';
                    $EstadoName = 'estado';
                }
                else {
                    $CatName = 'catName_EN';
                    $EstadoName = 'estado';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=view4user&cultura_id=');


                    $table  = " ( SELECT a.id_evento as id, a.id_evento as cultura_id, CONCAT('".$dummyHRef."', CAST(a.id_evento as CHAR(10))) as dummy, a.referencia_evento as codigo, a.nome_evento as nome_evento, a.desc_evento as descricao_evento";
                    $table .= " , IFNULL(b.".$CatName.",' ') as categoria, IFNULL(b.".$CatName.",' ') as category_name ";
                    $table .= " , d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado , a.cat_evento as idcategoria";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_Cultura_Eventos as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia_evento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Estado AS e ON e.id_estado = a.estado ";
                    $table .= " WHERE (a.nif=$UserSessionNIF) ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'created',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 ),
                        array( 'db' => 'id',            'dt' => 6 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'createdFull',   'dt' => 8 ),
                        array( 'db' => 'nome_evento',   'dt' => 9 ),
                        array( 'db' => 'descricao_evento','dt' => 10 ),
                        array( 'db' => 'idcategoria',   'dt' => 11 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();


                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com dados de todos os eventos (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getCulturaList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('cultura', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('cultura','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'catName_PT';
                    $EstadoName = 'estado';
                }
                else {
                    $CatName = 'catName_EN';
                    $EstadoName = 'estado';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=view4manager&cultura_id=');

                    $table  = " ( SELECT a.id_evento as id, a.id_evento as cultura_id, CONCAT('".$dummyHRef."', CAST(a.id_evento as CHAR(10))) as dummy, a.referencia_evento as codigo, a.nome_evento as nome_evento, a.desc_evento as descricao_evento";
                    $table .= " , IFNULL(b.".$CatName.",' ') as categoria, IFNULL(b.".$CatName.",' ') as category_name ";
                    $table .= " , d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado , a.cat_evento as idcategoria ";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_Cultura_Eventos as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia_evento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Estado AS e ON e.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'created',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 ),
                        array( 'db' => 'id',            'dt' => 6 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'createdFull',   'dt' => 8 ),
                        array( 'db' => 'nome_evento',   'dt' => 9 ),
                        array( 'db' => 'descricao_evento','dt' => 10 ),
                        array( 'db' => 'idcategoria',   'dt' => 11 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com os TODOS os estados */
        public static function getCulturaEstadoAllOptions ($lang, $displayPermError=true)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
            // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name_pt','estado as name_en') )
                    ->from("#__virtualdesk_Cultura_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $rowName
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Devolve CSS a ser aplicado de acordo com o ESTADO atual */
        public static function getCulturaEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                    $defCss = 'label-pendente';
                    break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }


        public static function getCategoriaAllOptions(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'catName_PT';
            }
            else {
                $CatName = 'catName_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as name'))
                ->from("#__virtualdesk_Cultura_Categoria as a ")
                ->order($CatName.' ASC')
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->name
                );
            }
            return($response);
        }

        /* Carrega dados visualização do cultura para o USER */
        public static function getCulturaView4UserDetail ($IdCultura)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('cultura');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdCultura) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            // Current Session User...
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'catName_PT';
                    $EstadoName = 'estado';
                }
                else {
                    $CatName = 'catName_EN';
                    $EstadoName = 'estado';
                }

                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id_evento as cultura_id', 'a.coOrganizacao as coOrganizacao', 'a.referencia_evento as codigo', 'a.desc_evento as desc_evento', 'a.nome_evento as nome_evento', 'a.data_criacao', 'a.data_alteracao'
                    , "IFNULL(b.".$CatName.",'') as categoria", "IFNULL(e.".$EstadoName.", ' ') as estado", 'e.id_estado as idestado', 'a.latitude', 'a.longitude'
                    , 'a.data_inicio as data_inicio', 'a.Ano as ano', 'a.mes as mes', 'a.hora_inicio as hora_inicio', 'a.data_fim as data_fim', 'a.hora_fim as hora_fim'
                    , 'a.local_evento as local_evento', 'a.website as website','a.facebook as facebook', 'a.instagram as instagram'
                    , 'g.id as id_layout_evento', 'g.layout as layout_evento', 'a.nif as nif_evento', 'a.observacoes as observacoes'
                    , 'd.freguesia as freguesia', 'a.freguesia_evento as id_freguesia', 'a.cat_evento as id_categoria'
                    ))
                    ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia_evento')
                    ->join('LEFT', '#__virtualdesk_Cultura_Estado AS e ON e.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Cultura_Layout AS g ON g.id = a.layout')
                    ->from("#__virtualdesk_Cultura_Eventos as a")
                    ->where( $db->quoteName('a.id_evento') . '=' . $db->escape($IdCultura) . ' and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();

                $objDadosPromotor = self::getCulturaPromotorByNIF4User($dataReturn->nif_evento);

                $objVDParams    = new VirtualDeskSiteParamsHelper();
                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Cultura');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_VIEW');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$dataReturn->nif_evento.' | '.'email: '.$objDadosPromotor->email.' | '.'tel: '.$objDadosPromotor->telefone.' | '.'nome: '.$objDadosPromotor->nome .' | '.'morada: '.$objDadosPromotor->morada;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPView();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = $IdCultura;
                    $eventdata['ref']           = $dataReturn->codigo;
                    $vdlog->insertRgpdLog($eventdata);
                }

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega dados visualização do cultura para o MANAGER */
        public static function getCulturaView4ManagerDetail ($IdCultura)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('cultura');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('cultura', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('cultura'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('cultura','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdCultura) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            // Current Session User...
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'catName_PT';
                    $EstadoName = 'estado';
                }
                else {
                    $CatName = 'catName_EN';
                    $EstadoName = 'estado';
                }

                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id_evento as cultura_id', 'a.coOrganizacao as coOrganizacao', 'a.referencia_evento as codigo', 'a.desc_evento as desc_evento', 'a.nome_evento as nome_evento', 'a.data_criacao', 'a.data_alteracao'
                    , "IFNULL(b.".$CatName.",'') as categoria", "IFNULL(e.".$EstadoName.", ' ') as estado", 'e.id_estado as idestado', 'a.latitude', 'a.longitude'
                    , 'a.data_inicio as data_inicio', 'a.Ano as ano', 'a.mes as mes', 'a.hora_inicio as hora_inicio', 'a.data_fim as data_fim', 'a.hora_fim as hora_fim'
                    , 'a.local_evento as local_evento', 'a.website as website','a.facebook as facebook', 'a.instagram as instagram'
                    , 'g.id as id_layout_evento', 'g.layout as layout_evento', 'a.nif as nif_evento', 'a.observacoes as observacoes'
                    , 'd.freguesia as freguesia', 'a.freguesia_evento as id_freguesia', 'a.cat_evento as id_categoria'
                    , 'h.nome as promotor_nome', 'h.email as promotor_email'
                    ))
                    ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia_evento')
                    ->join('LEFT', '#__virtualdesk_Cultura_Estado AS e ON e.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Cultura_Layout AS g ON g.id = a.layout')
                    ->join('LEFT', '#__virtualdesk_Cultura_Users AS h ON h.nif = a.nif')
                    ->from("#__virtualdesk_Cultura_Eventos as a")
                    ->where( $db->quoteName('a.id_evento') . '=' . $db->escape($IdCultura)  )
                );
                $dataReturn = $db->loadObject();

                $objDadosPromotor = self::getCulturaPromotorByNIF4Manager($dataReturn->nif_evento);

                $objVDParams    = new VirtualDeskSiteParamsHelper();
                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Cultura');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_VIEW');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$dataReturn->nif_evento.' | '.'email: '.$objDadosPromotor->email.' | '.'tel: '.$objDadosPromotor->telefone.' | '.'nome: '.$objDadosPromotor->nome .' | '.'morada: '.$objDadosPromotor->morada;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPView();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = $IdCultura;
                    $eventdata['ref']           = $dataReturn->codigo;
                    $vdlog->insertRgpdLog($eventdata);
                }

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega dados do Promotor do Evento: pesquisa nos USERS VD e carrega o Nome e o EMail se não existir vai aos USERS da Cultura */
        public static function getCulturaPromotorByNIF4Manager ($nif_evento)
        {
            if((int) $nif_evento<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('cultura');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('cultura', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('cultura'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('cultura','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $dataPromotor = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($nif_evento);

                if( (!empty($dataPromotor)) && ($dataPromotor!=false) )
                {
                    $dataReturn = new stdClass();
                    $dataReturn->nome  = $dataPromotor->name;
                    $dataReturn->nif   = $dataPromotor->nif;
                    $dataReturn->email = $dataPromotor->email;
                    return($dataReturn);
                }
                else {
                    $db = JFactory::getDBO();
                    $db->setQuery($db->getQuery(true)
                        ->select(array('nif, nome, email, morada, telefone, facebook, instagram'))
                        ->from("#__virtualdesk_Cultura_Users")
                        ->where($db->quoteName('nif') . "=" . $db->escape($nif_evento) )
                    );
                    $dataReturn = $db->loadObject();
                    if(empty($dataReturn)) return false;
                    return($dataReturn);
                }
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega dados do Promotor do Evento: pesquisa nos USERS VD e carrega o Nome e o EMail se não existir vai aos USERS da Cultura */
        public static function getCulturaPromotorByNIF4User ($nif_evento)
        {
            if((int) $nif_evento<=0) return false;

            /*
           * Check PERMISSÕES
           */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('cultura');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if( (int)$nif_evento != (int)$UserSessionNIF ) return false;

            try
            {
                $dataPromotor = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($nif_evento);

                if( (!empty($dataPromotor)) && ($dataPromotor!=false) )
                {
                    $dataReturn = new stdClass();
                    $dataReturn->nome  = $dataPromotor->name;
                    $dataReturn->nif   = $dataPromotor->nif;
                    $userfield_login_type = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
                    if( $userfield_login_type == 'login_as_nif' ) $dataReturn->nif = $dataPromotor->login;
                    $dataReturn->email = $dataPromotor->email;
                    return($dataReturn);
                }
                else {
                    $db = JFactory::getDBO();
                    $db->setQuery($db->getQuery(true)
                        ->select(array('nif, nome, email, morada, telefone, facebook, instagram'))
                        ->from("#__virtualdesk_Cultura_Users")
                        ->where($db->quoteName('nif') . "=" . $db->escape($nif_evento) )
                    );
                    $dataReturn = $db->loadObject();
                    if(empty($dataReturn)) return false;
                    return($dataReturn);
                }
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega dados (objecto) do ESTADO atual do processo do cultura em questão */
        public static function getCulturaEstadoAtualObjectByIdCultura ($lang, $id_cultura)
        {
            if((int) $id_cultura<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('cultura');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('b.id_estado as id_estado','b.estado as estado_PT','b.estado as estado_EN') )
                    ->join('LEFT', '#__virtualdesk_Cultura_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Cultura_Eventos as a")
                    ->where(" a.id_evento = ". $db->escape($id_cultura))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                $obj= new stdClass();
                $obj->id_estado =  $dataReturn->id_estado;
                if( empty($lang) or ($lang='pt_PT') ) {
                    $obj->name = $dataReturn->estado_PT;
                }
                else {
                    $obj->name  = $dataReturn->estado_EN;
                }

                $obj->cssClass = self::getCulturaEstadoCSS($dataReturn->id_estado);

                return($obj);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Grava dados para definir um novo ESTADO do processo de Cultura  */
        public static function saveAlterar2NewEstado4ManagerByAjax($getInputCultura_id, $NewEstadoId, $NewEstadoDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('cultura', 'alterarestado4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewEstadoId) ) return false;
            if((int) $getInputCultura_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            $data = array();
            $data['estado'] = $NewEstadoId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();

            $Table = new VirtualDeskTableCulturaEventos($db);
            $Table->load(array('id_evento'=>$getInputCultura_id));

            // Só alterar se o estado for diferente
            if((int)$Table->estado == (int)$NewEstadoId )  return true;

            if(!empty($data)) {
                $dateModified = new DateTime();
                $data['data_alteracao'] = $dateModified->format('Y-m-d H:i:s');
            }

            $db->transactionStart();

            try {
                //$data['id_evento'] = $Table->id_evento;
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTableCulturaEstadoHistorico($db);
                $dataHist = array();
                $dataHist['id_estado']  = $NewEstadoId;
                $dataHist['id_evento']  = $getInputCultura_id;
                $dataHist['descricao']  = $NewEstadoDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Cultura_Log_Geral_Enabled');
                $dominioMunicipio = $objVDParams->getParamsByTag('dominioMunicipio');
                $contactoTelefCopyrightEmail = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $LinkCopyright = $objVDParams->getParamsByTag('LinkCopyright');
                $emailCopyrightGeral = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $copyrightAPP = $objVDParams->getParamsByTag('copyrightAPP');

                /* Send Email - Para Admin, Manager e User */
                $objDadosPromotor = self::getCulturaPromotorByNIF4Manager ($Table->nif);
                $SendPromotorNome    = (string)$objDadosPromotor->nome;
                $SendPromotorEmail   = (string)$objDadosPromotor->email;
                $ObjEstado  = self::getEstadoById($Table->estado);
                $EstadoName = $ObjEstado->name;
                self::SendMailAdminAltEstado($copyrightAPP, $emailCopyrightGeral, $LinkCopyright, $contactoTelefCopyrightEmail, $dominioMunicipio, $Table->nome_evento, $SendPromotorNome, $SendPromotorEmail, $Table->referencia_evento, $EstadoName);

                $FregName   = self::getFregName($Table->freguesia_evento);
                $eventStart = $Table->data_inicio . " " . $Table->hora_inicio;
                $eventEnd   = $Table->data_fim . " " . $Table->hora_fim;

                self::SendMailClientAltEstado( $SendPromotorNome, $Table->nome_evento, $Table->desc_evento, $SendPromotorEmail, $FregName, $EstadoName,$Table->referencia_evento);
                /* END Send Email */

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_STATE_ALTERADO')  . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_cultura=' . $dataHist['id_cultura'];
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $Table->referencia_evento;
                    $vdlog->insertEventLog($eventdata);

                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Retorna a Ref Id apenas se o User atual da sessão tem o mesmo NIF da ocorrência
               Útil para saber se o user tem acesso a este cultura...
            */
        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia_evento as refid' ))
                    ->from("#__virtualdesk_Cultura_Eventos as a")
                    ->where( $db->quoteName('a.referencia_evento') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Cria novo Evento para o ecrã/permissões do USER */
        public static function create4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('cultura');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $obParam      = new VirtualDeskSiteParamsHelper();
            $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;

            $UserNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if ((int) $UserNIF <=0) return false;

            $db->transactionStart();

            try {
                if(!empty($data['desc_evento'])) $data['desc_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['desc_evento']), true);
                if(!empty($data['observacoes'])) $data['observacoes']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                $nif_evento          = $UserNIF;
                $coOrganizacao        = $data['coOrganizacao'];
                $nome_evento         = $data['nome_evento'];
                $desc_evento         = $data['desc_evento'];
                $cat_evento          = $data['cat_evento'];
                $freguesia_evento    = $data['freguesia_evento'];
                $local_evento        = $data['local_evento'];
                $dataInicio          = $data['data_inicio'];
                $horaInicio          = $data['hora_inicio'];
                $dataFim             = $data['data_fim'];
                $horaFim             = $data['hora_fim'];
                $website             = $data['website'];
                $facebook            = $data['facebook'];
                $instagram           = $data['instagram'];
                $observacoes         = $data['observacoes'];
                $layout_evento       = $data['layout_evento'];

                $dataInicioFormated = $dataInicio;
                $dataFimFormated    = $dataFim;

                // TODO Verificar -> colocar assim ?


                /* Gerar REFERÊNCIA UNICA de cultura*/
                $refExiste = 1;

                $splitData = explode("-", $dataInicioFormated);
                $splitData[0];
                $splitData[1];
                $splitData[2];

                $splitData2 = explode("-", $dataFimFormated);
                $splitData2[0];
                $splitData2[1];
                $splitData2[2];

                $referencia = VirtualDeskSiteCulturaHelper::RefEvent($nif_evento, $splitData[2], $splitData[1], $splitData2[2], $splitData2[1]);
                /*END Gerar Referencia UNICA de cultura*/

                $splitCoord = explode(",", $data['coordenadas']);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];

                $idMonth = VirtualDeskSiteCulturaHelper::getIdMonth($splitData[1]);
                $idAno = $splitData[0];

                /* Save Evento : START*/
                /*Valida Nome Promotor*/
                $errNif = self::checkValidarNIF($nif_evento);
                /*Valida Layout*/
                if(empty($layout_evento)) $errLayout = 1;
                /*Valida Categoria*/
                if(empty($cat_evento))  $errCategoria = 1;
                /*Valida Nome Evento*/
                if(empty($nome_evento)) $errNome = 1;
                /*Valida Descricao*/
                if(empty($desc_evento)) $errDescricao = 1;
                /*Valida Freguesia*/
                if(empty($freguesia_evento))  $errFreguesia = 1;
                /*Valida Local Evento*/
                if(empty($local_evento)) $errLocal = 1;


                if( $errNif==0 && $errLayout == 0 && $errCategoria == 0 && $errNome == 0 && $errDescricao == 0 && $errFreguesia == 0 && $errLocal == 0)
                {
                    $resSaveEvento = VirtualDeskSiteCulturaHelper::SaveNovoEvento($nif_evento, $coOrganizacao, $cat_evento, $referencia, $nome_evento, $desc_evento, $freguesia_evento, $local_evento, $dataInicioFormated, $horaInicio, $dataFimFormated, $horaFim, $idMonth, $idAno, $lat, $long, $website, $facebook, $instagram, $observacoes, $layout_evento,0);
                    if ($resSaveEvento!==true) {
                        $db->transactionRollback();
                        return false;
                    }
                }
                else
                {   $db->transactionRollback();
                    return false;
                }
                /*END Save Evento*/

                $nifExiste = VirtualDeskSiteCulturaHelper::getNIF($nif_evento);
                if((int)$nifExiste == 0) {
                    $objDadosPromotor = self::getCulturaPromotorByNIF4User ($nif_evento);
                    $promotor_nome    = (string)$objDadosPromotor->nome;
                    $promotor_email   = (string)$objDadosPromotor->email;
                    if(empty($promotor_nome) || empty($promotor_email)) {
                        $db->transactionRollback();
                        return false;
                    }
                    if(!empty($promotor_nome) && !empty($promotor_email) && (int)$nif_evento>0) {
                        $resSaveRegisto = VirtualDeskSiteCulturaHelper::SaveNovoRegistoV2($nif_evento, $promotor_nome, $promotor_email);
                    }

                } else {
                    $resSaveRegisto = true;
                }
                if (empty($resSaveRegisto)) {
                    $db->transactionRollback();
                    return false;
                }


                /* FILES */
                $objFiles                = new VirtualDeskSiteCulturaFilesHelper();
                $objFiles->tagprocesso   = 'CULTURA_POST';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false ) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();


                $nif_evento = $UserNIF;

                $objDadosPromotor = self::getCulturaPromotorByNIF4User ($nif_evento);
                $SendPromotorNome    = (string)$objDadosPromotor->nome;
                $SendPromotorEmail   = (string)$objDadosPromotor->email;
                self::SendMailAdminEvento($nomeMunicipio, $SendPromotorNome, $referencia);

                $descEmail = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML ($db->escape($data['desc_evento']));
                $catName = self::getCatSelect($data['cat_evento']);

                $FregName   = self::getFregName($data['freguesia_evento']);
                $eventStart = $data['data_inicio'] . " " . $data['hora_inicio'];
                $eventEnd   = $data['data_fim'] . " " . $data['hora_fim'];
                self::SendMailClientEvento($SendPromotorNome, $data['nome_evento'], $catName, $SendPromotorEmail, $FregName, $data['local_evento'], $eventStart, $eventEnd, $descEmail, $data['website'], $data['facebook'], $data['instagram'], $nomeMunicipio);
                /* END Send Email */


                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Cultura_Log_Geral_Enabled');

                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($referencia);
                    $vdlog = new VirtualDeskLogHelper();

                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_CREATED')  . 'ref=' . $referencia;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4User();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Cultura');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_CREATED');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$nif_evento.' | '.'email: '.$objDadosPromotor->email.' | '.'tel: '.$objDadosPromotor->telefone.' | '.'nome: '.$objDadosPromotor->nome .' | '.'morada: '.$objDadosPromotor->morada;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPCreate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = '';
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }

            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        /* Cria novo Evento para o ecrã/permissões do MANAGER */
        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('cultura');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $obParam      = new VirtualDeskSiteParamsHelper();
            $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                if(!empty($data['desc_evento'])) $data['desc_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['desc_evento']), true);
                if(!empty($data['observacoes'])) $data['observacoes']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                $promotor_nome       = $data['promotor_nome'];
                $promotor_email      = $data['promotor_email'];
                $nif_evento          = $data['nif_evento'];
                $coOrganizacao       = $data['coOrganizacao'];

                $nome_evento         = $data['nome_evento'];
                $desc_evento         = $data['desc_evento'];
                $cat_evento          = $data['cat_evento'];
                $freguesia_evento    = $data['freguesia_evento'];
                $local_evento        = $data['local_evento'];
                $dataInicio          = $data['data_inicio'];
                $horaInicio          = $data['hora_inicio'];
                $dataFim             = $data['data_fim'];
                $horaFim             = $data['hora_fim'];
                $website             = $data['website'];
                $facebook            = $data['facebook'];
                $instagram           = $data['instagram'];
                $observacoes         = $data['observacoes'];
                $layout_evento       = $data['layout_evento'];

                $dataInicioFormated = $dataInicio;
                $dataFimFormated    = $dataFim;


                /* Gerar REFERÊNCIA UNICA de cultura*/
                $refExiste = 1;

                $splitData = explode("-", $dataInicioFormated);
                $splitData[0];
                $splitData[1];
                $splitData[2];

                $splitData2 = explode("-", $dataFimFormated);
                $splitData2[0];
                $splitData2[1];
                $splitData2[2];

                $referencia = VirtualDeskSiteCulturaHelper::RefEvent($nif_evento, $splitData[2], $splitData[1], $splitData2[2], $splitData2[1]);
                /*END Gerar Referencia UNICA de cultura*/



                $splitCoord = explode(",", $data['coordenadas']);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];

                $idMonth = VirtualDeskSiteCulturaHelper::getIdMonth($splitData[1]);
                $idAno = $splitData[0];

                /* Save Evento : START*/
                /*Valida Nome Promotor*/
                $errNif = self::checkValidarNIF($nif_evento);



                /*Valida Layout*/
                if(empty($layout_evento)) $errLayout = 1;
                /*Valida Categoria*/
                if(empty($cat_evento))  $errCategoria = 1;
                /*Valida Nome Evento*/
                if(empty($nome_evento)) $errNome = 1;
                /*Valida Descricao*/
                if(empty($desc_evento)) $errDescricao = 1;
                /*Valida Freguesia*/
                if(empty($freguesia_evento))  $errFreguesia = 1;
                /*Valida Local Evento*/
                if(empty($local_evento)) $errLocal = 1;

                if( $errNif==0 && $errLayout == 0 && $errCategoria == 0 && $errNome == 0 && $errDescricao == 0 && $errFreguesia == 0 && $errLocal == 0)
                {
                    $resSaveEvento = VirtualDeskSiteCulturaHelper::SaveNovoEvento($nif_evento, $coOrganizacao, $cat_evento, $referencia, $nome_evento, $desc_evento, $freguesia_evento, $local_evento, $dataInicioFormated, $horaInicio, $dataFimFormated, $horaFim, $idMonth, $idAno, $lat, $long, $website, $facebook, $instagram, $observacoes, $layout_evento,1);
                    if (empty($resSaveEvento)) {
                        $db->transactionRollback();
                        return false;
                    }
                }
                else
                {
                    $db->transactionRollback();
                    return false;
                }
                /*END Save Evento*/

                $nifExiste = VirtualDeskSiteCulturaHelper::getNIF($nif_evento);
                if((int)$nifExiste == 0){
                   if(empty($promotor_nome) || empty($promotor_nome)) {
                       $objDadosPromotor = self::getCulturaPromotorByNIF4Manager ($nif_evento);
                       $promotor_nome    = (string)$objDadosPromotor->nome;
                       $promotor_email   = (string)$objDadosPromotor->email;
                   }
                   if(empty($promotor_nome) || empty($promotor_email)) {
                       $db->transactionRollback();
                       return false;
                   }
                   if(!empty($promotor_nome) && !empty($promotor_email) && (int)$nif_evento>0) {
                       $resSaveRegisto = VirtualDeskSiteCulturaHelper::SaveNovoRegistoV2($nif_evento, $promotor_nome, $promotor_email);
                   }

                } else {
                    $resSaveRegisto = true;
                }
                if (empty($resSaveRegisto)) {
                    $db->transactionRollback();
                    return false;
                }


                /* FILES */
                $objFiles                = new VirtualDeskSiteCulturaFilesHelper();
                $objFiles->tagprocesso   = 'CULTURA_POST';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false ) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();


                /* Send Email - Para Admin, Manager e User */
                $objDadosPromotor = self::getCulturaPromotorByNIF4Manager ($nif_evento);
                $SendPromotorNome    = (string)$objDadosPromotor->nome;
                $SendPromotorEmail   = (string)$objDadosPromotor->email;
                self::SendMailAdminEvento($nomeMunicipio, $SendPromotorNome, $referencia);

                $descEmail = VirtualDeskSiteGeneralHelper::decodeDatabase2HTML ($db->escape($data['desc_evento']));
                $catName = self::getCatSelect($data['cat_evento']);
                $FregName   = self::getFregName($data['freguesia_evento']);
                $eventStart = $data['data_inicio'] . " " . $data['hora_inicio'];
                $eventEnd   = $data['data_fim'] . " " . $data['hora_fim'];
                self::SendMailClientEvento($SendPromotorNome, $data['nome_evento'], $catName, $SendPromotorEmail, $FregName, $data['local_evento'], $eventStart, $eventEnd, $descEmail, $data['website'], $data['facebook'], $data['instagram'], $nomeMunicipio);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Cultura_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_CREATED')  . 'ref=' . $referencia;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Cultura');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_CREATED');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$nif_evento.' | '.'email: '.$objDadosPromotor->email.' | '.'tel: '.$objDadosPromotor->telefone.' | '.'nome: '.$objDadosPromotor->nome .' | '.'morada: '.$objDadosPromotor->morada;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPCreate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = '';
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }

            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        /* Atualiza um evento para o ecrã/permissões do USER */
        public static function update4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4users'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $cultura_id = $data['cultura_id'];
            if((int) $cultura_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableCulturaEventos($db);
            $Table->load(array('id_evento'=>$cultura_id,'nif'=>$UserSessionNIF));


            // Se o estado não for o inicial, o user não pode editar
            $chkIdEstadoInicial = (int) self::getEstadoIdInicio();
            $chkIdEstadoAtual   = (int)$Table->estado;
            if($chkIdEstadoInicial != $chkIdEstadoAtual && $chkIdEstadoAtual>0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db->transactionStart();

            try {

                $referencia          = $Table->referencia_evento;
                $obParam             = new VirtualDeskSiteParamsHelper();

                //$nomePromotor     = null;
                //$email          = null;
                //$nif_evento       = null;
                $coOrganizacao    = null;
                $nome_evento      = null;
                $desc_evento      = null;
                $cat_evento       = null;
                $freguesia_evento = null;
                $local_evento     = null;
                $dataInicio       = null;
                $horaInicio       = null;
                $dataFim          = null;
                $horaFim          = null;
                $freguesia        = null;
                $website          = null;
                $facebook         = null;
                $instagram        = null;
                $observacoes      = null;
                $layout_evento    = null;
                $layout_evento    = null;
                $lat              = null;
                $long             = null;



                if(!empty($data['desc_evento'])) $data['desc_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['desc_evento']),true);
                if(!empty($data['observacoes'])) $data['observacoes']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);


                //if(array_key_exists('nome_promotor',$data))     $nomePromotor = $data['nome_promotor'];
                //if(array_key_exists('email_evento',$data))         $email_evento = $data['email_evento'];
                //if(array_key_exists('nif_evento',$data))        $nif_evento = $data['nif_evento'];
                if(array_key_exists('coOrganizacao',$data))     $coOrganizacao = $data['coOrganizacao'];
                if(array_key_exists('nome_evento',$data))       $nome_evento = $data['nome_evento'];
                if(array_key_exists('desc_evento',$data))       $desc_evento = $data['desc_evento'];
                if(array_key_exists('cat_evento',$data))        $cat_evento = $data['cat_evento'];
                if(array_key_exists('freguesia_evento',$data))  $freguesia_evento = $data['freguesia_evento'];
                if(array_key_exists('local_evento',$data))      $local_evento = $data['local_evento'];

                if(array_key_exists('data_inicio',$data)) {
                    $dataInicio      = $data['data_inicio'];
                    $splitDataInicio = explode("-", $dataInicio);
                    $idMonth         = VirtualDeskSiteCulturaHelper::getIdMonth($splitDataInicio[1]);
                    $idAno           = $splitDataInicio[0];
                }
                if(array_key_exists('hora_inicio',$data))       $horaInicio = $data['hora_inicio'];
                if(array_key_exists('data_fim',$data))          $dataFim = $data['data_fim'];
                if(array_key_exists('hora_fim',$data))          $horaInicio = $data['hora_fim'];

                if(array_key_exists('website',$data))           $website = $data['website'];
                if(array_key_exists('facebook',$data))          $facebook = $data['facebook'];
                if(array_key_exists('instagram',$data))         $instagram = $data['instagram'];
                if(array_key_exists('observacoes',$data))       $observacoes = $data['observacoes'];
                if(array_key_exists('layout_evento',$data))     $layout_evento = $data['layout_evento'];


                if(array_key_exists('coordenadas',$data)) {
                    $splitCoord = explode(",", $data['coordenadas']);
                    $lat = $splitCoord[0];
                    $long = $splitCoord[1];
                }


                /* BEGIN Save Evento */
                $resSaveEvento = VirtualDeskSiteCulturaHelper::saveEditedEvento4User($cultura_id, $coOrganizacao, $cat_evento, $nome_evento, $desc_evento, $freguesia_evento, $local_evento, $dataInicio, $horaInicio, $dataFim, $horaFim, $idMonth, $idAno, $lat, $long, $website, $facebook, $instagram, $observacoes, $layout_evento);
                /* END Save Evento */

                if (empty($resSaveEvento)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objFiles                = new VirtualDeskSiteCulturaFilesHelper();
                $listFile2Eliminar       = $objFiles->setListFile2EliminarFromPOST ($referencia);
                $resFileDelete           = $objFiles->deleteFiles ($referencia , $listFile2Eliminar);
                if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Capa', 'error');
                }

                // Insere os NOVOS ficheiros
                /* FILES */
                $objFiles                = new VirtualDeskSiteCulturaFilesHelper();
                $objFiles->tagprocesso   = 'CULTURA_POST';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');


                if ($resFileSave===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $obParam      = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');


                $db->transactionCommit();

                $nif_evento = $UserSessionNIF;
                /* Send Email - Para Admin, Manager e User */
                $objDadosPromotor = self::getCulturaPromotorByNIF4User($nif_evento);
                $SendPromotorNome    = (string)$objDadosPromotor->nome;
                $SendPromotorEmail   = (string)$objDadosPromotor->email;
                self::SendMailAdminEventoUpdate( $Table->nome_evento, $SendPromotorNome, $SendPromotorEmail, $referencia);

                $FregName   = self::getFregName($Table->freguesia_evento);
                $eventStart = $Table->data_inicio . " " . $Table->hora_inicio;
                $eventEnd   = $Table->data_fim . " " . $Table->hora_fim;
                self::SendMailClientEventoUpdate($nomeMunicipio, $SendPromotorNome, $Table->nome_evento, $Table->desc_evento, $SendPromotorEmail, $FregName, $Table->local_evento, $eventStart, $eventEnd);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Cultura_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_UPDATE');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_UPDATE')  . 'ref=' . $referencia;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog). ' | '.JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_DELETED').implode(",", $listFile2Eliminar);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4User();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Cultura');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_UPDATE');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$nif_evento.' | '.'email: '.$objDadosPromotor->email.' | '.'tel: '.$objDadosPromotor->telefone.' | '.'nome: '.$objDadosPromotor->nome .' | '.'morada: '.$objDadosPromotor->morada;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPUpdate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = $cultura_id;
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }


                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Atualiza um evento para o ecrã/permissões do MANAGER */
        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $cultura_id = $data['cultura_id'];
            if((int) $cultura_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableCulturaEventos($db);
            $Table->load(array('id_evento'=>$cultura_id));

            $db->transactionStart();

            try {

                $referencia          = $Table->referencia_evento;
                $obParam             = new VirtualDeskSiteParamsHelper();

                $promotor_nome    = null;
                $promotor_email   = null;
                $nif_evento       = null;
                $coOrganizacao    = null;
                $nome_evento      = null;
                $desc_evento      = null;
                $cat_evento       = null;
                $freguesia_evento = null;
                $local_evento     = null;
                $dataInicio       = null;
                $horaInicio       = null;
                $dataFim          = null;
                $horaFim          = null;
                $freguesia        = null;
                $website          = null;
                $facebook         = null;
                $instagram        = null;
                $observacoes      = null;
                $layout_evento    = null;
                $layout_evento    = null;
                $lat              = null;
                $long             = null;

                if(!empty($data['desc_evento'])) $data['desc_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['desc_evento']), true);
                if(!empty($data['observacoes'])) $data['observacoes']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                if(array_key_exists('promotor_nome',$data))     $promotor_nome = $data['promotor_nome'];
                if(array_key_exists('promotor_email',$data))    $promotor_email = $data['promotor_email'];
                if(array_key_exists('nif_evento',$data))        $nif_evento = $data['nif_evento'];
                if(array_key_exists('coOrganizacao',$data))     $coOrganizacao = $data['coOrganizacao'];
                if(array_key_exists('nome_evento',$data))       $nome_evento = $data['nome_evento'];
                if(array_key_exists('desc_evento',$data))       $desc_evento = $data['desc_evento'];
                if(array_key_exists('cat_evento',$data))        $cat_evento = $data['cat_evento'];
                if(array_key_exists('freguesia_evento',$data))  $freguesia_evento = $data['freguesia_evento'];
                if(array_key_exists('local_evento',$data))      $local_evento = $data['local_evento'];

                if(array_key_exists('data_inicio',$data)) {
                    $dataInicio      = $data['data_inicio'];
                    $splitDataInicio = explode("-", $dataInicio);
                    $idMonth         = VirtualDeskSiteCulturaHelper::getIdMonth($splitDataInicio[1]);
                    $idAno           = $splitDataInicio[0];
                }
                if(array_key_exists('hora_inicio',$data))       $horaInicio = $data['hora_inicio'];
                if(array_key_exists('data_fim',$data))          $dataFim = $data['data_fim'];
                if(array_key_exists('hora_fim',$data))          $horaInicio = $data['hora_fim'];

                if(array_key_exists('website',$data))           $website = $data['website'];
                if(array_key_exists('facebook',$data))          $facebook = $data['facebook'];
                if(array_key_exists('instagram',$data))         $instagram = $data['instagram'];
                if(array_key_exists('observacoes',$data))       $observacoes = $data['observacoes'];
                if(array_key_exists('layout_evento',$data))     $layout_evento = $data['layout_evento'];


                if(array_key_exists('coordenadas',$data)) {
                    $splitCoord = explode(",", $data['coordenadas']);
                    $lat = $splitCoord[0];
                    $long = $splitCoord[1];
                }

                /* BEGIN Save Evento */
                $resSaveEvento = VirtualDeskSiteCulturaHelper::saveEditedEvento4Manager($cultura_id, $coOrganizacao, $nif_evento, $cat_evento, $nome_evento, $desc_evento, $freguesia_evento, $local_evento, $dataInicio, $horaInicio, $dataFim, $horaFim, $idMonth, $idAno, $lat, $long, $website, $facebook, $instagram, $observacoes, $layout_evento);
                /* END Save Evento */

                if ($resSaveEvento===false) {
                    $db->transactionRollback();
                    return false;
                }

                /* Save Promotor, se não existir */
                /* SAVE New USER da Cultura - Promotor */

                if(is_null($nif_evento)) $nif_evento = $Table->nif;

                $nifExiste = VirtualDeskSiteCulturaHelper::getNIF($nif_evento);
                if((int)$nifExiste == 0){
                    // O NIF não existe. Se não estiver definido o nome e o email, tenta carregar dos users existente
                    if(empty($promotor_nome) || empty($promotor_email)) {
                        $objDadosPromotor = self::getCulturaPromotorByNIF4Manager ($nif_evento);
                        $promotor_nome    = (string)$objDadosPromotor->nome;
                        $promotor_email   = (string)$objDadosPromotor->email;
                    }
                    if(empty($promotor_nome) || empty($promotor_email)) {
                        // Se não conseguir carregar, dá erro e faz rollback...
                        $db->transactionRollback();
                        return false;
                    }
                    if(!empty($promotor_nome) && !empty($promotor_email) && (int)$nif_evento>0) {
                        // Só quando conseguir o nome e o email do promotor é que tenta gravar...
                        $resSaveRegisto = VirtualDeskSiteCulturaHelper::SaveNovoRegistoV2($nif_evento, $promotor_nome, $promotor_email);
                    }

                } else {
                    // O nif já existe inserido nos promotores, então FAZ o UPDATE
                    $resSaveRegisto = self::SaveUpdateRegistoV2($nif_evento, $promotor_nome, $promotor_email);
                }
                if ($resSaveRegisto===false) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objFiles                = new VirtualDeskSiteCulturaFilesHelper();
                $listFile2Eliminar       = $objFiles->setListFile2EliminarFromPOST ($referencia);
                $resFileDelete           = $objFiles->deleteFiles ($referencia , $listFile2Eliminar);
                if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Capa', 'error');
                }

                // Insere os NOVOS ficheiros
                /* FILES */
                $objFiles                = new VirtualDeskSiteCulturaFilesHelper();
                $objFiles->tagprocesso   = 'CULTURA_POST';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');


                if ($resFileSave===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }


                $db->transactionCommit();

                $obParam      = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

                /* Send Email - Para Admin, Manager e User */
                $objDadosPromotor = self::getCulturaPromotorByNIF4Manager ($nif_evento);
                $SendPromotorNome    = (string)$objDadosPromotor->nome;
                $SendPromotorEmail   = (string)$objDadosPromotor->email;
                self::SendMailAdminEventoUpdate( $Table->nome_evento, $SendPromotorNome, $SendPromotorEmail, $referencia);

                $FregName   = self::getFregName($Table->freguesia_evento);
                $eventStart = $Table->data_inicio . " " . $Table->hora_inicio;
                $eventEnd   = $Table->data_fim . " " . $Table->hora_fim;
                self::SendMailClientEventoUpdate($nomeMunicipio, $SendPromotorNome, $Table->nome_evento, $Table->desc_evento, $SendPromotorEmail, $FregName, $Table->local_evento, $eventStart, $eventEnd);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Cultura_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_UPDATE');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_UPDATE')  . 'ref=' . $referencia;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog). ' | '.JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_DELETED').implode(",", $listFile2Eliminar);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Cultura');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_CULTURA_EVENTLOG_UPDATE');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$nif_evento.' | '.'email: '.$objDadosPromotor->email.' | '.'tel: '.$objDadosPromotor->telefone.' | '.'nome: '.$objDadosPromotor->nome .' | '.'morada: '.$objDadosPromotor->morada;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPUpdate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = $cultura_id;
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Retorna Id do Estado Concluido*/
        public static function getEstadoIdConcluido ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado'))
                    ->from("#__virtualdesk_Cultura_Estado as a")
                    ->where(" a.bitEnd=1 ")
                );
                $response = $db->loadObjectList();

                $arReturn = array();
                foreach ($response as $row)
                {
                    $arReturn[] = (int)$row->id_estado;
                }
                return($arReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Retorna Id do Estado Inicial*/
        public static function getEstadoIdInicio ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado'))
                    ->from("#__virtualdesk_Cultura_Estado as a")
                    ->where(" a.bitStart=1 ")
                );
                $response = $db->loadObject();
                return($response->id_estado);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }



        /* Limpa os dados na sessão "users states" do joomla */
        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.cultura.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.cultura.data', null);
            $app->setUserState('com_virtualdesk.edit4user.cultura.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.cultura.data', null);

        }
        
    }
?>