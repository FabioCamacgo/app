<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');


/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteAlertaStatsHelper
{
    const tagchaveModulo = 'alerta';

    public $TotalPedidosOnline;
    public $TotalPedidosBalcao;
    public $TotalPedidosPorEstado;
    public $TotalResolvidos;
    public $TotalNaoResolvidos;
    public $TotaisPorFreguesia;
    public $TotaisPorCategoria;
    public $TotaisPorAnoMes;
    public $TempoMedioEstado;
    public $TotalDeProcessos;
    public $DadosImediatos;

    public $TotalPedidosOnline4User;
    public $TotalPedidosBalcao4User;
    public $TotalPedidosPorEstado4User;
    public $TotalResolvidos4User;
    public $TotalNaoResolvidos4User;
    public $TotaisPorCategoria4User;
    public $TotaisPorAnoMes4User;
    public $TotalDeProcessos4User;

    private $UserJoomlaID;
    private $SessionUserNIF;

    private  $host;
    private  $user;
    private  $password;
    private  $database;
    private  $dbprefix;


    public function __construct()
    {   $conf           = JFactory::getConfig();
        $this->host     = $conf->get('host');
        $this->user     = $conf->get('user');
        $this->password = $conf->get('password');
        $this->database = $conf->get('db');
        $this->dbprefix = $conf->get('dbprefix');

        $this->UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $this->SessionUserNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser( $this->UserJoomlaID );
    }


    public function setAllStats($SetAgruparPorModulo=false) {

        /*
         * Check PERMISSÕES
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário apresenta estatísticas do módulo desligado
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $this->setTotalDeProcessos();
        $this->getAlertaPedidosOnlineBalcao();
        $this->getAlertaTotaisPorEstado();
        $this->getAlertaTotaisResolvidosAndNot();
        $this->getAlertaTotaisPorCategoria($SetAgruparPorModulo);
        $this->getAlertaTotaisPorAnoMes();
        $this->getAlertaTempoMedioPorEstado();
        $this->getAlertaDadosImediatos();

    }


    public function setAllStats4User($SetAgruparPorModulo=false) {

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário apresenta estatísticas do módulo desligado
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $this->setTotalDeProcessos4User();
        $this->getAlertaPedidosOnlineBalcao4User();
        $this->getAlertaTotaisPorEstado4User();
        $this->getAlertaTotaisResolvidosAndNot4User();
        $this->getAlertaTotaisPorCategoria4User($SetAgruparPorModulo);
        $this->getAlertaTotaisPorAnoMes4User();

    }


    public function setTotalDeProcessos ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;

        $this->TotalDeProcessos = 0;

            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.Id_alerta) as nprocessos ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $db->setQuery($query);
            $this->TotalDeProcessos = $db->loadResult();

            return(true);

    }


    public function setTotalDeProcessos4User ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;
        if( (int) $this->SessionUserNIF <=0 )  return false;

        $this->TotalDeProcessos4User = 0;

        $db = JFactory::getDBO();

        $query  = " SELECT COUNT(Distinct a.Id_alerta) as nprocessos ";
        $query .= " FROM #__virtualdesk_alerta as a ";
        $query .= " WHERE a.nif =".$this->SessionUserNIF;
        $db->setQuery($query);
        $this->TotalDeProcessos4User = $db->loadResult();

        return(true);

    }


    public function getAlertaTotaisPorEstado ()
    {
        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte
        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;
        $this->TotalPedidosPorEstado = array();

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            $db     = JFactory::getDBO();
            $query  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos, IFNULL(b.".$EstadoName.", ' ') as estado, IFNULL(a.estado, '0') as idestado ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $query .= " LEFT JOIN #__virtualdesk_alerta_estados AS b  ON b.id_estado = a.estado ";
            $query .= " GROUP BY a.estado";

            $db->setQuery($query);

            $this->TotalPedidosPorEstado = $db->loadObjectList();
            return(true);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaTotaisPorEstado4User ()
    {
        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte
        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;
        $this->TotalPedidosPorEstado4User = array();

        if( (int) $this->SessionUserNIF <=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            $db     = JFactory::getDBO();
            $query  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos, IFNULL(b.".$EstadoName.", ' ') as estado, IFNULL(a.estado, '0') as idestado ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $query .= " LEFT JOIN #__virtualdesk_alerta_estados AS b  ON b.id_estado = a.estado ";
            $query .= " WHERE a.nif =".$this->SessionUserNIF;
            $query .= " GROUP BY a.estado";

            $db->setQuery($query);

            $this->TotalPedidosPorEstado4User = $db->loadObjectList();
            return(true);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaTotaisResolvidosAndNot()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;

        if(empty($this->TotalPedidosPorEstado)) $this->getAlertaTotaisPorEstado( );
        if(!is_array($this->TotalPedidosPorEstado)) $this->TotalPedidosPorEstado = array();

        $this->TotalResolvidos    = 0;
        $this->TotalNaoResolvidos = 0;

        $idConcluido = (int) VirtualDeskSiteAlertaHelper::getEstadoIdConcluido();

        foreach ($this->TotalPedidosPorEstado as $row) {

            if((int)$row->idestado == $idConcluido) {
                $this->TotalResolvidos  += (int)$row->npedidos;
            }
            else {
                $this->TotalNaoResolvidos += (int)$row->npedidos;
            }
        }
        return(true);
    }


    public function getAlertaTotaisResolvidosAndNot4User()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;
        if( (int) $this->SessionUserNIF <=0 )  return false;

        if(empty($this->TotalPedidosPorEstado4User)) $this->getAlertaTotaisPorEstado4User( );
        if(!is_array($this->TotalPedidosPorEstado4User)) $this->TotalPedidosPorEstado4User = array();

        $this->TotalResolvidos4User    = 0;
        $this->TotalNaoResolvidos4User = 0;

        $idConcluido = (int) VirtualDeskSiteAlertaHelper::getEstadoIdConcluido();

        foreach ($this->TotalPedidosPorEstado4User as $row) {
            if((int)$row->idestado == $idConcluido) {
                $this->TotalResolvidos4User  += (int)$row->npedidos;
            }
            else {
                $this->TotalNaoResolvidos4User += (int)$row->npedidos;
            }
        }
        return(true);
    }


    public function getAlertaTotaisPorFreguesia ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;

        try
        {
            /*$lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'estado_PT';
            }
            else {
                $CatName = 'estado_EN';
            }*/

            $CatName = 'freguesia';

            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos, IFNULL(b.".$CatName.", ' ') as freguesia, IFNULL(a.freguesia, '0') as idfreguesia ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $query .= " LEFT JOIN #__virtualdesk_digitalGov_freguesias AS b  ON b.id = a.freguesia ";
            $query .= " GROUP BY a.freguesia";

            $db->setQuery($query);

            $this->TotaisPorFreguesia = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaTotaisPorCategoria ($setAgruparPorModulo=false)
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;

        $this->TotaisPorCategoria = array();

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
            }
            else {
                $CatName = 'name_EN';
            }

            $CatNemSQL = " IFNULL(b.".$CatName.", ' ') as categoria ";
            if($setAgruparPorModulo===true) $CatNemSQL = " '".JText::_('COM_VIRTUALDESK_ALERTA_HEADING') . "' as categoria ";

            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos, ".$CatNemSQL.", IFNULL(a.categoria, '0') as idcategoria ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $query .= " LEFT JOIN #__virtualdesk_alerta_categoria AS b  ON b.ID_categoria = a.categoria ";
            $query .= " GROUP BY a.categoria";

            $db->setQuery($query);

            $this->TotaisPorCategoria = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaTotaisPorCategoria4User ($setAgruparPorModulo=false)
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;
        if( (int) $this->SessionUserNIF <=0 )  return false;

        $this->TotaisPorCategoria4User = array();

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
            }
            else {
                $CatName = 'name_EN';
            }

            $CatNemSQL = " IFNULL(b.".$CatName.", ' ') as categoria ";
            if($setAgruparPorModulo===true) $CatNemSQL = " '".JText::_('COM_VIRTUALDESK_ALERTA_HEADING') . "' as categoria ";

            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos, ".$CatNemSQL.", IFNULL(a.categoria, '0') as idcategoria ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $query .= " LEFT JOIN #__virtualdesk_alerta_categoria AS b  ON b.ID_categoria = a.categoria ";
            $query .= " WHERE a.nif =".$this->SessionUserNIF;
            $query .= " GROUP BY a.categoria";

            $db->setQuery($query);

            $this->TotaisPorCategoria4User = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaTotaisPorAnoMes ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte dos relatórios

        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;

        $this->TotaisPorAnoMes = array();

        try
        {
            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos, YEAR(a.data_criacao) as year , MONTH(a.data_criacao) as mes ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $query .= " where a.data_criacao >= DATE_SUB(now(), INTERVAL 12 MONTH) ";
            $query .= " GROUP BY YEAR(a.data_criacao), MONTH(a.data_criacao)";

            $db->setQuery($query);

            $this->TotaisPorAnoMes = $db->loadObjectList();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaTotaisPorAnoMes4User ()
    {
        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte dos relatórios

        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;
        if( (int) $this->SessionUserNIF <=0 )  return false;

        $this->TotaisPorAnoMes4User = array();

        try
        {
            $db = JFactory::getDBO();

            $query  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos, YEAR(a.data_criacao) as year , MONTH(a.data_criacao) as mes ";
            $query .= " FROM #__virtualdesk_alerta as a ";
            $query .= " where a.data_criacao >= DATE_SUB(now(), INTERVAL 12 MONTH) AND a.nif =".$this->SessionUserNIF;
            $query .= " GROUP BY YEAR(a.data_criacao), MONTH(a.data_criacao)";

            $db->setQuery($query);

            $this->TotaisPorAnoMes4User = $db->loadObjectList();

            return(true);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaPedidosOnlineBalcao ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;

        $this->TotalPedidosOnline = 0;
        $this->TotalPedidosBalcao = 0;

        try
        {
            $db = JFactory::getDBO();

            $query1  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos_online  ";
            $query1 .= " FROM #__virtualdesk_alerta as a WHERE balcao=0";
            $db->setQuery($query1);
            $this->TotalPedidosOnline = (int)$db->loadResult();

            $query2  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos_balcao  ";
            $query2 .= " FROM #__virtualdesk_alerta as a WHERE balcao=1";
            $db->setQuery($query2);
            $this->TotalPedidosBalcao = (int) $db->loadResult();

           return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getAlertaPedidosOnlineBalcao4User ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte

        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;
        if( (int) $this->SessionUserNIF <=0 )  return false;

        $this->TotalPedidosOnline4User = 0;
        $this->TotalPedidosBalcao4User = 0;

        try
        {
            $db = JFactory::getDBO();

            $query1  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos_online  ";
            $query1 .= " FROM #__virtualdesk_alerta as a WHERE balcao=0";
            $query1 .= " AND a.nif =".$this->SessionUserNIF;
            $db->setQuery($query1);
            $this->TotalPedidosOnline4User = (int)$db->loadResult();

            $query2  = " SELECT COUNT(Distinct a.Id_alerta) as npedidos_balcao  ";
            $query2 .= " FROM #__virtualdesk_alerta as a WHERE balcao=1";
            $query2 .= " AND a.nif =".$this->SessionUserNIF;
            $db->setQuery($query2);
            $this->TotalPedidosBalcao4User = (int) $db->loadResult();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Vai comparar da ata de criação com a data do estado atual na tabela estado_historico */
    public function getAlertaTempoMedioPorEstado ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;

        $this->TempoMedioEstado = array();

        try
        {

            // Todo chamar por método....
            $IdEstadoInicial    = 1;
            $IdEstadoConcluíddo = 3;
            $TempoMedio = array();

            $db = JFactory::getDBO();

            // 1ª situação: não tem registo de histórico, então conta estado 1 data criação até data hoje

            // SQL
            /*select
  b_idalerta
  ,b_created
,list.next_created_value
  ,list.prev_created_value
  ,DATEDIFF(now(), list.b_created) as Diff

            sum(DATEDIFF(now(), list.b_created)) as Soma
from
(
    select
    b.codOco,
    b.Id_alerta as b_idalerta,
    b.data_criacao                              as b_created,
    a.id,
    a.id_alerta,
    a.id_estado,
    a.created,
    LAG(created, 1) OVER (PARTITION BY a.id_alerta
    ORDER BY a.created desc)                       next_created_value,
    LAG(created, 1) OVER (PARTITION BY a.id_alerta
    ORDER BY a.created asc)                        prev_created_value,
    DATEDIFF(LAG(created, 1) OVER (PARTITION BY a.id_alerta
             ORDER BY a.created desc), created) as diff
  from
    gtonq_virtualdesk_alerta AS b
    LEFT JOIN gtonq_virtualdesk_alerta_estado_historico AS a ON a.id_alerta = b.Id_alerta

  ORDER BY a.id_alerta, a.created, b.Id_alerta
) as list

            where (list.next_created_value is null and list.prev_created_value is null)
            */

            $querySemAlteracaoEstado   = " select sum(DATEDIFF(now(), list.b_created)) as Soma, COUNT(DISTINCT b_idalerta) as Total ";
            $querySemAlteracaoEstado  .= " FROM ( select b.codOco,      b.Id_alerta as b_idalerta,                                             b.estado as b_estado,";
            $querySemAlteracaoEstado  .= " b.data_criacao as b_created,                                             a.id,                                             a.id_alerta,                                             a.id_estado,                                             a.created,                                             LAG(created, 1) OVER (PARTITION BY a.id_alerta                                             ORDER BY a.created desc)  next_created_value,  LAG(created, 1) OVER (PARTITION BY a.id_alerta  ORDER BY a.created asc)  prev_created_value,";
            $querySemAlteracaoEstado  .= " DATEDIFF(LAG(created, 1) OVER (PARTITION BY a.id_alerta     ORDER BY a.created desc), created) as diff  from   gtonq_virtualdesk_alerta AS b";
            $querySemAlteracaoEstado  .=" LEFT JOIN gtonq_virtualdesk_alerta_estado_historico AS a ON a.id_alerta = b.Id_alerta  ORDER BY a.id_alerta, a.created, b.Id_alerta ) as list";
            $querySemAlteracaoEstado  .= ' where (list.next_created_value is null and list.prev_created_value is null and b_estado ='.$IdEstadoInicial.')';
            $db->setQuery($querySemAlteracaoEstado);
            $TempoTotalSemIntervencao = $db->loadObject();
            if((int) $TempoTotalSemIntervencao->Total>0) {
                $TempoMedio[1] += round((int)$TempoTotalSemIntervencao->Soma / (int)$TempoTotalSemIntervencao->Total);
            }


            // 2ª situação , o histórico next está null mas o prev está prrenchido é o estado concluído (mas forçamos que seja o estado = 3)
            $queryConcluidos  = " select
   /* b_idalerta
  ,b_created
  ,b_estado
  , a_created
  ,list.a_idestado
  ,list.next_created_value
  ,list.prev_created_value
  ,DATEDIFF(list.a_created, list.prev_created_value) as Diff
*/
sum(DATEDIFF(list.a_created, list.prev_created_value)) as Soma
, COUNT(*) as Total

from
(
  select
    b.codOco,
    b.estado as b_estado,
    b.Id_alerta as b_idalerta,
    b.data_criacao as b_created,
    a.id,
    a.id_alerta,
    a.id_estado as a_idestado,
    a.created as a_created,
    LAG(created, 1) OVER (PARTITION BY a.id_alerta
    ORDER BY a.created desc)                       next_created_value,
    LAG(created, 1) OVER (PARTITION BY a.id_alerta
    ORDER BY a.created asc)                        prev_created_value,
    DATEDIFF(LAG(created, 1) OVER (PARTITION BY a.id_alerta
             ORDER BY a.created desc), created) as diff
  from
    gtonq_virtualdesk_alerta AS b
    LEFT JOIN gtonq_virtualdesk_alerta_estado_historico AS a ON a.id_alerta = b.Id_alerta

  ORDER BY a.id_alerta, a.created, b.Id_alerta
) as list

where (list.next_created_value is null and list.prev_created_value is not null and b_estado = ".$IdEstadoConcluíddo." ) ";
            $db->setQuery($queryConcluidos);
            $TempoTotalConcluidos = $db->loadObject();
            if((int) $TempoTotalConcluidos->Total>0) {
                $TempoMedio[3] += round((int)$TempoTotalConcluidos->Soma / (int)$TempoTotalConcluidos->Total);
            }


            // 3ª Situação, soma o tempo que levou a fazerem várias alterações no estado (não é o inicial nem o final), mas pode ter passado por um desses
            //  Devemos contar e separar  por estado 1, 2 e 3 , quando o prev e o next não são nulos
            $queryHistorico  = " select
  /* b_idalerta
 ,b_created
 ,b_estado
 , a_created
 ,list.a_idestado
 ,list.next_created_value
 ,list.prev_created_value */
   sum(case when list.a_idestado=1 then DATEDIFF( list.next_created_value , list.a_created) end) as Soma1
  , sum(case when list.a_idestado=2 then DATEDIFF( list.next_created_value , list.a_created) end) as Soma2
  , sum(case when list.a_idestado=3 then DATEDIFF( list.next_created_value , list.a_created) end) as Soma3
  , COUNT(*) as Total
from
  (
    select
      b.codOco,
      b.estado as b_estado,
      b.Id_alerta as b_idalerta,
      b.data_criacao as b_created,
      a.id,
      a.id_alerta,
      a.id_estado as a_idestado,
      a.created as a_created,
      LAG(created, 1) OVER (PARTITION BY a.id_alerta
      ORDER BY a.created desc)                       next_created_value,
      LAG(created, 1) OVER (PARTITION BY a.id_alerta
      ORDER BY a.created asc)                        prev_created_value,
      DATEDIFF(LAG(created, 1) OVER (PARTITION BY a.id_alerta
               ORDER BY a.created desc), created) as diff
    from
      gtonq_virtualdesk_alerta AS b
      LEFT JOIN gtonq_virtualdesk_alerta_estado_historico AS a ON a.id_alerta = b.Id_alerta

    ORDER BY a.id_alerta, a.created, b.Id_alerta
  ) as list

where (list.next_created_value is not null and list.prev_created_value is not null )  ";
            $db->setQuery($queryHistorico);
            $TempoTotalHistorico = $db->loadObject();
            if((int) $TempoTotalHistorico->Total>0) {
                $TempoMedio[1] += round((int)$TempoTotalHistorico->Soma1 / (int)$TempoTotalHistorico->Total);
                $TempoMedio[2] += round((int)$TempoTotalHistorico->Soma2 / (int)$TempoTotalHistorico->Total);
                $TempoMedio[3] += round((int)$TempoTotalHistorico->Soma3 / (int)$TempoTotalHistorico->Total);
            }

            $this->TempoMedioEstado = $TempoMedio;

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Vai comparar da ata de criação com a data do estado atual na tabela estado_historico */
    public function getAlertaDadosImediatos ()
    {

        // ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta parte


        if( empty( $this->UserJoomlaID ) )  return false;
        if( (int) $this->UserJoomlaID <=0 )  return false;


        try
        {
            $this->DadosImediatos = new stdClass();

            $db = JFactory::getDBO();

            $queryUltimoMes  = " select  COUNT(Id_alerta) as Total from gtonq_virtualdesk_alerta where  data_criacao >= DATE(NOW()) - INTERVAL 1 MONTH ";
            $db->setQuery($queryUltimoMes);
            $this->DadosImediatos->UltimoMes= $db->loadResult();

            $queryUltimoSemana = " select  COUNT(Id_alerta) as Total from gtonq_virtualdesk_alerta where data_criacao >= DATE(NOW()) - INTERVAL 7 DAY ";
            $db->setQuery($queryUltimoSemana);
            $this->DadosImediatos->UltimaSemana= $db->loadResult();

            $queryOntem = " select  COUNT(Id_alerta) as Total from gtonq_virtualdesk_alerta where data_criacao >= DATE(NOW()- INTERVAL 1 DAY) ";
            $db->setQuery($queryOntem);
            $this->DadosImediatos->Ontem= $db->loadResult();

            $queryHoje = " select  COUNT(Id_alerta) as Total from gtonq_virtualdesk_alerta where data_criacao = DATE(NOW()) ";
            $db->setQuery($queryHoje);
            $this->DadosImediatos->Hoje= $db->loadResult();

            return(true);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

}
