<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskTableListaFormulariosFiles', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/formularios_lista.php');
JLoader::register('VirtualDeskTableRelacaoFormsCamposFiles', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/rel_forms_fields_lista.php');
JLoader::register('VirtualDeskTableListaUploadsFiles', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/uploads_lista.php');
JLoader::register('VirtualDeskTableRelacaoFormsUploads', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/rel_forms_uploads_lista.php');

class VirtualDeskSiteFormmainHelper
{
    const tagchaveModulo = 'formmain';

    public static function getEstadoCSS ($idestado)
    {
        $defCss = 'label-default';
        switch ($idestado)
        {   case '1':
            //Em espera
                $defCss = 'label-espera';
                break;
            case '2':
                // Em análise
                $defCss = 'label-analise';
                break;
            case '3':
                // Espera de documentos
                $defCss = 'label-documentos';
                break;
            case '4':
                // Concluído
                $defCss = 'label-concluido';
                break;
        }
        return ($defCss);
    }

    public static function getEstadoList2Change ($lang, $id_estado_atual, $modulo)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($modulo);                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if((int) $id_estado_atual<=0) $id_estado_atual = -1;

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('estAct.id','a.id_estado as id_estado_act','a.id_estado_next as id_estado_next','a.id_estado_prev as id_estado_prev'
                ,'estNext.estado_PT as estNextname_pt' ,'estNext.estado_EN as estNextname_en'
                ,'estPrev.estado_PT as estPrevname_pt','estPrev.estado_EN as estPrevname_en'
                ,'estAct.estado_PT as estActname_pt','estAct.estado_EN as estActname_en') )
                ->join('LEFT', '#__virtualdesk_form_main_estado AS estNext ON estNext.id = a.id_estado_next')
                ->join('LEFT', '#__virtualdesk_form_main_estado AS estPrev ON estPrev.id = a.id_estado_prev')
                ->join('LEFT', '#__virtualdesk_form_main_estado AS estAct  ON estAct.id = a.id_estado')
                ->from("#__virtualdesk_form_main_estado_sequencia as a")
                ->where(" a.id_estado = $id_estado_atual ")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {

                // Acrescenta o ATUAL para surgir na lista
                if((int) $row->id_estado_act>0 ) {
                    if (empty($lang) or ($lang = 'pt_PT')) {
                        $rowNameAct = $row->estActname_pt;
                    } else {
                        $rowNameAct = $row->estActname_en;
                    }
                    $response[$row->id_estado_act] = array(
                        'id'   => $row->id_estado_act,
                        'name' => $rowNameAct
                    );
                }


                // NEXT
                if((int) $row->id_estado_next>0 ) {
                    if (empty($lang) or ($lang = 'pt_PT')) {
                        $rowNameNxt = $row->estNextname_pt;
                    } else {
                        $rowNameNxt = $row->estNextname_en;
                    }
                    $response[$row->id_estado_next] = array(
                        'id'   => $row->id_estado_next,
                        'name' => $rowNameNxt
                    );
                }
                // PREV
                if((int) $row->id_estado_prev>0 ) {
                    if (empty($lang) or ($lang = 'pt_PT')) {
                        $rowNamePrev = $row->estPrevname_pt;
                    } else {
                        $rowNamePrev = $row->estPrevname_en;
                    }
                    $response[$row->id_estado_prev] = array(
                        'id' => $row->id_estado_prev,
                        'name' => $rowNamePrev
                    );
                }
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getModulo(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome'))
            ->from("#__virtualdesk_perm_modulo")
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getModuloSelect($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nome')
            ->from("#__virtualdesk_perm_modulo")
            ->where($db->quoteName('id') . "='" . $db->escape($id) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function excludeModulo($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome'))
            ->from("#__virtualdesk_perm_modulo")
            ->where($db->quoteName('id') . "!='" . $db->escape($id) . "'")
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getModuleId($moduleName){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_perm_modulo")
            ->where($db->quoteName('tagchave') . "='" . $db->escape($moduleName) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getFormId($tag){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('tag') . "='" . $db->escape($tag) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getFormTagById($idform){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('tag')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('id') . "='" . $db->escape($idform) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getFormIdByIdrel($getInputPedido_Id, $modulo){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('form_id')
            ->from("#__virtualdesk_form_main_rel_" . $modulo)
            ->where($db->quoteName('id') . "='" . $db->escape($getInputPedido_Id) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getFormNameByTag($tag){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nome')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('tag') . "='" . $db->escape($tag) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getFormNameById($formId){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nome')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('id') . "='" . $db->escape($formId) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getTipoCampo(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, tipo'))
            ->from("#__virtualdesk_form_fields_type")
            ->order('tipo ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getTipoCampoSelect($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('tipo')
            ->from("#__virtualdesk_form_fields_type")
            ->where($db->quoteName('id') . "='" . $db->escape($id) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function excludeTipoCampo($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, tipo'))
            ->from("#__virtualdesk_form_fields_type")
            ->where($db->quoteName('id') . "!='" . $db->escape($id) . "'")
            ->order('tipo ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getFieldIdByTag($fieldTag){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_form_fields")
            ->where($db->quoteName('tag') . "='" . $db->escape($fieldTag) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
    }

    public static function CheckReferencia($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('tag')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('tag') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function create4Admin($UserJoomlaID, $UserVDId, $data)
    {

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'addnew4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {

            $nomeFormulario = $data['nomeFormulario'];
            $refInterna = $data['refInterna'];
            $descricao = $data['descricao'];
            $modulo = $data['modulo'];

            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia       = self::random_code();
                $checkREF = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }

            $resSaveForm = self::saveNewForm($db, $nomeFormulario, $referencia, $refInterna, $descricao, $modulo);

            if (empty($resSaveForm)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function saveNewForm($db, $nomeFormulario, $referencia, $refInterna, $descricao, $modulo){

        if(!isset($db))   $db = JFactory::getDbo();

        $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);

        $query = $db->getQuery(true);
        $columns = array('tag','reftag','nome'); //,'descricao','modulo','enabled');
        $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($refInterna)), $db->quote($db->escape($nomeFormulario)));  // , $db->quote($db->escape($descricao)), $db->quote($db->escape($modulo)), 1
        $query
            ->insert($db->quoteName('#__virtualdesk_form_main'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));
        $db->setQuery($query);
        $result = (boolean) $db->execute();
        return($result);
    }

    public static function getFormulariosList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('formmain','list4admin');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=editform4admin&form_id=');

                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.setFormEnableByAjax&form_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.getFormEnableValByAjax&form_id=');

                $table  = " ( SELECT a.id as id, a.id as form_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tag as tag, a.nome as nome, a.reftag as reftag";
                $table .= " , a.enabled as enabled, a.modulo as idmodulo, IFNULL(c.nome, ' ') as modulo";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_form_main as a";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_modulo AS c ON c.id = a.modulo";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',            'dt' => 0 ),
                    array( 'db' => 'tag',           'dt' => 1 ),
                    array( 'db' => 'reftag',        'dt' => 2 ),
                    array( 'db' => 'nome',          'dt' => 3 ),
                    array( 'db' => 'modulo',        'dt' => 4 ),
                    array( 'db' => 'enabled',       'dt' => 5 ),
                    array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idmodulo',      'dt' => 7 ),
                    array( 'db' => 'SwitchSetHRef', 'dt' => 8 ),
                    array( 'db' => 'SwitchGetHRef', 'dt' => 9 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function setPluginEnableState($UserJoomlaID, $getInputPlugin_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_plugin_id = $getInputPlugin_id;
        if((int) $configadmin_plugin_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_plugin_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableListaFormulariosFiles($db);
        $Table->load(array('id'=>$configadmin_plugin_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function checkFormEnableVal($getInputForm_id)
    {
        if( empty($getInputForm_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            $db->setQuery("Select enabled From #__virtualdesk_form_main Where id =" . $db->escape($getInputForm_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function createFields4Admin($UserJoomlaID, $UserVDId, $data)
    {

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'addnew4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {

            $tagCampo = $data['tagCampo'];
            $nomeCampo = $data['nomeCampo'];
            $descricao = $data['descricao'];
            $tipoCampo = $data['tipoCampo'];


            $resSaveForm = self::saveNewFields($db, $tagCampo, $nomeCampo, $descricao, $tipoCampo);

            if (empty($resSaveForm)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function saveNewFields($db, $tagCampo, $nomeCampo, $descricao, $tipoCampo){
        if(!isset($db))   $db = JFactory::getDbo();

        $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);

        $query = $db->getQuery(true);
        $columns = array('tag','nome','descricao','tipo');
        $values = array($db->quote($db->escape($tagCampo)), $db->quote($db->escape($nomeCampo)), $db->quote($db->escape($descricao)), $db->quote($db->escape($tipoCampo)));
        $query
            ->insert($db->quoteName('#__virtualdesk_form_fields'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));
        $db->setQuery($query);
        $result = (boolean) $db->execute();
        return($result);
    }

    public static function getCamposList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('formmain','list4admin');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=editfields4admin&field_id=');

                $table  = " ( SELECT a.id as id, a.id as form_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tag as tag, a.nome as nome";
                $table .= ", a.tipo as idtipo, IFNULL(b.tipo, ' ') as tipo";
                $table .= " FROM ".$dbprefix."virtualdesk_form_fields as a";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_form_fields_type AS b ON b.id = a.tipo";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',        'dt' => 0 ),
                    array( 'db' => 'tag',       'dt' => 1 ),
                    array( 'db' => 'nome',      'dt' => 2 ),
                    array( 'db' => 'tipo',      'dt' => 3 ),
                    array( 'db' => 'dummy',     'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idtipo',    'dt' => 5 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getFieldsData($tag_form, $modulo_id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('c.id, c.tag'))
            ->from ("#__virtualdesk_form_main as a")
            ->leftJoin ("#__virtualdesk_form_fields_rel as b on a.id=b.form_id")
            ->leftJoin ("#__virtualdesk_form_fields as c on b.fields_id=c.id")
            ->leftJoin ("#__virtualdesk_perm_modulo as d on d.id=a.modulo")
            ->where($db->quoteName('a.enabled') . "= 1")
            ->where($db->quoteName('b.enabled') . "= 1")
            ->where($db->quoteName('d.enabled') . "= 1")
            ->where($db->quoteName('a.tag') . "='" . $db->escape($tag_form) . "'")
            ->where($db->quoteName('a.modulo') . "=" . $db->escape($modulo_id) . "")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getFieldsDataById($formId){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('c.id, c.tag'))
            ->from ("#__virtualdesk_form_main as a")
            ->leftJoin ("#__virtualdesk_form_fields_rel as b on a.id=b.form_id")
            ->leftJoin ("#__virtualdesk_form_fields as c on b.fields_id=c.id")
            ->leftJoin ("#__virtualdesk_perm_modulo as d on d.id=a.modulo")
            ->where($db->quoteName('a.enabled') . "= 1")
            ->where($db->quoteName('b.enabled') . "= 1")
            ->where($db->quoteName('d.enabled') . "= 1")
            ->where($db->quoteName('a.id') . "='" . $db->escape($formId) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getRelacaoList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('formmain','list4admin');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=editrelformsfields4admin&rel_id=');

                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.setRelacaoEnableByAjax&rel_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.getRelacaoEnableValByAjax&rel_id=');

                $table  = " ( SELECT a.id as id, a.id as rel_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.form_id, a.fields_id";
                $table .= " , a.enabled, IFNULL(b.nome, ' ') as formulario, IFNULL(c.nome, ' ') as campo, a.ordem";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_form_fields_rel as a";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_form_main AS b ON b.id = a.form_id";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_form_fields AS c ON c.id = a.fields_id";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',            'dt' => 0 ),
                    array( 'db' => 'formulario',    'dt' => 1 ),
                    array( 'db' => 'campo',         'dt' => 2 ),
                    array( 'db' => 'ordem',         'dt' => 3 ),
                    array( 'db' => 'enabled',       'dt' => 4 ),
                    array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'form_id',       'dt' => 6 ),
                    array( 'db' => 'fields_id',     'dt' => 7 ),
                    array( 'db' => 'SwitchSetHRef', 'dt' => 8 ),
                    array( 'db' => 'SwitchGetHRef', 'dt' => 9 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function setPluginRelEnableState($UserJoomlaID, $getInputPlugin_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_plugin_id = $getInputPlugin_id;
        if((int) $configadmin_plugin_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_plugin_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableRelacaoFormsCamposFiles($db);
        $Table->load(array('id'=>$configadmin_plugin_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function getFormularios(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome'))
            ->from("#__virtualdesk_form_main")
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getCampos(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome'))
            ->from("#__virtualdesk_form_fields")
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getUploads(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome'))
            ->from("#__virtualdesk_form_main_upload")
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getEstadoFormularios(){
        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $EstadoName = 'estado_PT';
        }
        else {
            $EstadoName = 'estado_EN';
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id,' . $EstadoName . ' as estado'))
            ->from("#__virtualdesk_form_main_estado")
            ->order('id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function checkRelEnableVal($getInputForm_id)
    {
        if( empty($getInputForm_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            $db->setQuery("Select enabled From #__virtualdesk_form_fields_rel Where id =" . $db->escape($getInputForm_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function checkFormEnabled($tag){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('enabled')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('tag') . "='" . $db->escape($tag) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getRefTag($tag){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('reftag')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('tag') . "='" . $db->escape($tag) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getRefTagById($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('reftag')
            ->from("#__virtualdesk_form_main")
            ->where($db->quoteName('id') . "='" . $db->escape($id) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getFormFieldsEstrutura($idform)
    {
        try
        {

            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select(array('f.id as id', 'f.tag as fieldtag', 'fType.tipo'))
                ->from('#__virtualdesk_form_fields_rel as fRel')
                ->join('LEFT','#__virtualdesk_form_fields as f on f.id=fRel.fields_id')
                ->join('LEFT','#__virtualdesk_form_fields_type as fType on fType.id=f.tipo')
                ->where($db->quoteName('fRel.enabled') . ' = 1 and ' . $db->quoteName('fRel.form_id') . ' = ' . $db->escape($idform)
                );

            $db->setQuery($query);

            $dataReturn = $db->loadAssocList();
            $response =  array();
            foreach($dataReturn as $row) {
                $response[$row['fieldtag']] = $row;
            }
            return($response);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getDataVarchar($id_rel, $idField, $tagchavemodulo){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('val_varchar')
            ->from("#__virtualdesk_form_main_data_" . $tagchavemodulo)
            ->where($db->quoteName('id_rel') . "='" . $db->escape($id_rel) . "'")
            ->where($db->quoteName('fields_id') . "='" . $db->escape($idField) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function insertMainrel($db, $idform, $tagchavemodulo, $referencia, $userID){

        $query = $db->getQuery(true);
        $columns = array('form_id', 'ref', 'user_id', 'estado_id');
        $values = array($db->quote($idform), $db->quote($referencia), $db->quote($userID), $db->quote('1'));
        $query
            ->insert($db->quoteName('#__virtualdesk_form_main_rel_' . $tagchavemodulo))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean)$db->execute();
        return ($result);

    }

    public static function insertDatarel($db, $idform, $tagchavemodulo, $data2Create, $id_rel){

        foreach($data2Create as $row){

            $query = $db->getQuery(true);

            $columns = array('id_rel', 'fields_id');
            $values = array($db->quote($id_rel), $db->quote($row['campo_id']));
            switch ($row['campo_t']) {
                case 'Varchar':
                    array_push($columns,'val_varchar'   );
                    array_push($values, $db->quote($row['val'])   );
                    break;
                case 'Número':
                    array_push($columns,'val_number'   );
                    array_push($values, $row['val']  );
                    break;
                case 'Data':
                    array_push($columns,'val_date'   );
                    array_push($values, $db->quote($row['val'])   );
                    break;
                case 'Texto':
                    array_push($columns,'val_text'   );
                    $fieldText = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($row['val']),true);
                    array_push($values, $db->quote($fieldText)   );
                    break;
                case 'Raw':
                    array_push($columns,'val_raw'   );
                    array_push($values, $db->quote($row['val'])   );
                    break;
                default :
                    array_push($columns,'val_varchar'   );
                    array_push($values, $db->quote($row['val'])   );
                break;
            }

            $query
                ->insert($db->quoteName('#__virtualdesk_form_main_data_' . $tagchavemodulo))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);
            $result = (boolean)$db->execute();
            if(!$result) return(false);
            $columns = array();
            $values = array();
        }

        return (true);
    }

    public static function insertNew($idform, $tagchavemodulo, $data2Create, $referencia, $userID){

        $db    = JFactory::getDbo();

        $db->transactionStart();

        $tagForm = self::getRefTagById($idform);

        $resInsertMainRel = self::insertMainrel($db, $idform, $tagchavemodulo, $referencia, $userID);

        if (empty($resInsertMainRel)) {
            $db->transactionRollback();
            return false;
        }

        $id_rel = $db->insertid();

        $resInsertDataRel = self::insertDatarel($db, $idform, $tagchavemodulo, $data2Create, $id_rel);

        if (empty($resInsertDataRel)) {
            $db->transactionRollback();
            return false;
        }

        if($resInsertMainRel == true && $resInsertDataRel == true){

            $modulo_id = self::getModuleId($tagchavemodulo);
            $tag_form = self::getFormTagById($idform);
            $fieldsForm = self::getFieldsData($tag_form, $modulo_id);

            $newFieldsExists = array();
            foreach ($fieldsForm as $keyF) {
                $newFieldsExists[] = $keyF['tag'];
            }

            if (in_array('fieldEmail', $newFieldsExists)) {
                $idField1 = self::getFieldIdByTag('fieldEmail');
                $mailUser = self::getDataVarchar($id_rel, $idField1, $tagchavemodulo);
            } else {
                $mailUser = '';
            }

            if (in_array('fieldEmailRep', $newFieldsExists)) {
                $idField2 = self::getFieldIdByTag('fieldEmailRep');
                $mailReq = self::getDataVarchar($id_rel, $idField2, $tagchavemodulo);
            } else {
                $mailReq = '';
            }

            if (in_array('fieldNomeRep', $newFieldsExists)) {
                $idField3 = self::getFieldIdByTag('fieldNomeRep');
                $nameReq = self::getDataVarchar($id_rel, $idField3, $tagchavemodulo);
            } else {
                $nameReq = 'representante';
            }

            if (in_array('fieldName', $newFieldsExists)) {
                $idField4 = self::getFieldIdByTag('fieldName');
                $nameUser = self::getDataVarchar($id_rel, $idField4, $tagchavemodulo);
            }

            $formName = self::getFormNameByTag($tag_form);

            if(!empty($mailUser) && !empty($mailReq)){

                $mailByRep = 1;
                self::sendMailUser($mailUser, $mailByRep, $formName);
                self::sendMailRepresentante($nameUser, $mailReq, $nameReq, $formName);

            } else {

                $mailByRep = 0;
                self::sendMailUser($mailUser, $mailByRep, $formName);

            }
        }

        $db->transactionCommit();

    }

    public static function sendMailUser($mailUser, $mailByRep, $formName){

        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/balcaoOnline/virtualdesk_NovoPedidoUser_Email.html');

        $obParam             = new VirtualDeskSiteParamsHelper();

        $nomeMunicipio                  = $obParam->getParamsByTag('nomeMunicipio');
        $emailCopyrightGeral            = $obParam->getParamsByTag('emailCopyrightGeral');
        $contactoTelefCopyrightEmail    = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
        $dominioMunicipio               = $obParam->getParamsByTag('dominioMunicipio');
        $LinkCopyright                  = $obParam->getParamsByTag('LinkCopyright');
        $copyrightAPP                   = $obParam->getParamsByTag('copyrightAPP');
        $logoEmailGeral                 = $obParam->getParamsByTag('logoEmailGeral');

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_COPYLINK  = JText::sprintf($LinkCopyright);
        $BODY_COPYNAME  = JText::sprintf($copyrightAPP);
        $BODY_COPYTELE  = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_COPYMAIL  = JText::sprintf($emailCopyrightGeral);
        $BODY_COPYDOM   = JText::sprintf($dominioMunicipio);
        $BODY_TITLE     = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_NOVOPEDIDO_TITLE');
        $BODY_TITULO    = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_NOVOPEDIDO', $formName);
        $BODY_GREETING  = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_GREETING');
        if($mailByRep == 1){
            $BODY_INTRO     = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_RECEBEUPEDIDO_BY_REPRESENTANTE', $formName, $nomeMunicipio);
        } else {
            $BODY_INTRO     = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_RECEBEUPEDIDO', $formName, $nomeMunicipio);
        }
        $BODY_ENDMAIL   = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_ANALISE', $copyrightAPP);

        $body   = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body   = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
        $body   = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
        $body   = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
        $body   = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
        $body   = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
        $body   = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
        $body   = str_replace("%BODY_TITULO",$BODY_TITULO, $body);
        $body   = str_replace("%BODY_GREETING",$BODY_GREETING, $body);
        $body   = str_replace("%BODY_INTRO",$BODY_INTRO, $body);
        $body   = str_replace("%BODY_ENDMAIL",$BODY_ENDMAIL, $body);

        $newEmail = JFactory::getMailer();
        $newEmail->Encoding = 'base64';
        $newEmail->isHtml(true);
        $newEmail->setBody($body);
        $newEmail->addReplyTo($data['mailfrom']);
        $newEmail->setSender($data['mailfrom']);
        $newEmail->setFrom($data['fromname']);
        $newEmail->addRecipient($mailUser);
        $newEmail->setSubject($data['sitename']);
        $newEmail->AddEmbeddedImage(JPATH_ROOT . $logoEmailGeral, "banner", "BannerEmail.png");

        $return = (boolean)$newEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;
    }

    public static function sendMailRepresentante($nameUser, $mailReq, $nameReq, $formName){
        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/balcaoOnline/virtualdesk_NovoPedidoRepresentante_Email.html');

        $obParam             = new VirtualDeskSiteParamsHelper();

        $nomeMunicipio                  = $obParam->getParamsByTag('nomeMunicipio');
        $emailCopyrightGeral            = $obParam->getParamsByTag('emailCopyrightGeral');
        $contactoTelefCopyrightEmail    = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
        $dominioMunicipio               = $obParam->getParamsByTag('dominioMunicipio');
        $LinkCopyright                  = $obParam->getParamsByTag('LinkCopyright');
        $copyrightAPP                   = $obParam->getParamsByTag('copyrightAPP');
        $logoEmailGeral                 = $obParam->getParamsByTag('logoEmailGeral');

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_COPYLINK  = JText::sprintf($LinkCopyright);
        $BODY_COPYNAME  = JText::sprintf($copyrightAPP);
        $BODY_COPYTELE  = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_COPYMAIL  = JText::sprintf($emailCopyrightGeral);
        $BODY_COPYDOM   = JText::sprintf($dominioMunicipio);
        $BODY_TITLE     = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_NOVOPEDIDO_TITLE');
        $BODY_TITULO    = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_NOVOPEDIDO', $formName);
        $BODY_GREETING  = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_GREETING_REPRESENTANTE', $nameReq);
        $BODY_INTRO     = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_RECEBEUPEDIDO_REPRESENTANTE', $formName, $nameUser, $nomeMunicipio);
        $BODY_ENDMAIL   = JText::sprintf('COM_VIRTUALDESK_BALCAOONLINE_ANALISE', $copyrightAPP);

        $body   = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body   = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
        $body   = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
        $body   = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
        $body   = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
        $body   = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
        $body   = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
        $body   = str_replace("%BODY_TITULO",$BODY_TITULO, $body);
        $body   = str_replace("%BODY_GREETING",$BODY_GREETING, $body);
        $body   = str_replace("%BODY_INTRO",$BODY_INTRO, $body);
        $body   = str_replace("%BODY_ENDMAIL",$BODY_ENDMAIL, $body);

        $newEmail = JFactory::getMailer();
        $newEmail->Encoding = 'base64';
        $newEmail->isHtml(true);
        $newEmail->setBody($body);
        $newEmail->addReplyTo($data['mailfrom']);
        $newEmail->setSender($data['mailfrom']);
        $newEmail->setFrom($data['fromname']);
        $newEmail->addRecipient($mailReq);
        $newEmail->setSubject($data['sitename']);
        $newEmail->AddEmbeddedImage(JPATH_ROOT . $logoEmailGeral, "banner", "BannerEmail.png");

        $return = (boolean)$newEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;
    }

    public static function updateDatarel($db, $data2Create, $id_rel, $tagchavemodulo){

        foreach($data2Create as $row){

            $query = $db->getQuery(true);

            $fields = array();

            // Conditions for which records should be updated.
            $conditions = array(
                $db->quoteName('fields_id') . ' = ' . $row['campo_id'],
                $db->quoteName('id_rel') . ' = ' . $id_rel
            );


            switch ($row['campo_t']) {
                case 'Varchar':
                    array_push($fields, 'val_varchar = '.  $db->quote($row['val']));
                    break;
                case 'Número':
                    array_push($fields, 'val_number = '.  $row['val']);
                    break;
                case 'Data':
                    array_push($fields, 'val_date = '.  $db->quote($row['val']));
                    break;
                case 'Texto':
                    $fieldText = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($row['val']),true);
                    array_push($fields, 'val_text = '.  $db->quote($fieldText));
                    break;
                case 'Raw':
                    array_push($fields, 'val_raw = '.  $db->quote($row['val']));
                    break;
                default :
                    array_push($fields, 'val_varchar = '.  $db->quote($row['val']));
                    break;
            }

            $query
                ->update($db->quoteName('#__virtualdesk_form_main_data_' . $tagchavemodulo))
                ->set($fields)
                ->where($conditions);


            $db->setQuery($query);
            $result = (boolean)$db->execute();
            if(!$result) return(false);
            $fields = array();
            $conditions = array();
        }

        return (true);
    }

    public static function saveEditedForm4User($pedido_id, $data2Create, $tagchavemodulo, $UserJoomlaID){

        $db    = JFactory::getDbo();

        $db->transactionStart();

        $id_rel = $pedido_id;

        $resUpdateDataRel = self::updateDatarel($db, $data2Create, $id_rel, $tagchavemodulo);

        if (empty($resUpdateDataRel)) {
            $db->transactionRollback();
            return false;
        }

        $db->transactionCommit();

        return true;
    }

    public static function SendEmail4NewMsgHistFromManager($nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $msg, $TO, $BCC, $IntroName, $refPedido){

        $config = JFactory::getConfig();

        /* Carrega parâmetros*/
        $objVDParams     = new VirtualDeskSiteParamsHelper();
        $layoutPathEmail = $objVDParams->getParamsByTag('formMain_SendMail_NewMsgHistFromManager_LayoutPath');

        if((string)$layoutPathEmail=='') {
            // Se não carregar o caminho do layout sai com erro...
            echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
            exit();
        }
        $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
        $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
        $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_FORMMAIN_EMAIL_INTRO',$IntroName);
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_FORMMAIN_EMAIL_NEWMSG_CORPO',$refPedido);
        $BODY_SECONDLINE    = JText::sprintf('COM_VIRTUALDESK_FORMMAIN_EMAIL_NEWMSG_CORPO3');
        $explodeMSG = explode('<br/>', $msg);

        if (strpos($explodeMSG[0], 'Em espera') !== false) {
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#bd4e2e; color:#fff;"><span style="color:#bd4e2e">_</span>Em espera<span style="color:#bd4e2e">_</span></span>';
        } else if (strpos($explodeMSG[0], 'Pendente') !== false) {
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#bd4e2e; color:#fff;"><span style="color:#bd4e2e">_</span>Em espera<span style="color:#bd4e2e">_</span></span>';
        } else if(strpos($explodeMSG[0], 'Em análise') !== false){
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#d4c744; color:#fff;"><span style="color:#d4c744">_</span>Em análise<span style="color:#d4c744">_</span></span>';
        } else if(strpos($explodeMSG[0], 'Concluído') !== false){
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#4b744f; color:#fff;"><span style="color:#4b744f">_</span>Concluído<span style="color:#4b744f">_</span></span>';
        } else if(strpos($explodeMSG[0], 'Espera de documentos') !== false){
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#009ddc; color:#fff;"><span style="color:#009ddc">_</span>Espera de documentos<span style="color:#009ddc">_</span></span>';
        } else {
            $BODY_THIRDLINE = $explodeMSG[0];
        }


        if(!empty($explodeMSG[1]) && $explodeMSG[1] != ' '){
            $BODY_DESCRITIVO  = '<b>Mensagem:</b> <br>' . $explodeMSG[1];
        } else {
            $BODY_DESCRITIVO = '';
        }


        $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
        $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
        $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
        $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
        $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
        $body      = str_replace("%BODY_DESCRITIVO",$BODY_DESCRITIVO, $body);
        $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
        $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
        $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
        $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
        $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

        // Send the password reset request email.
        $newEmail = JFactory::getMailer();
        $newEmail->Encoding = 'base64';
        $newEmail->isHtml(true);
        $newEmail->setBody($body);
        $newEmail->addReplyTo( $data['mailfrom']);
        $newEmail->setSender( $data['mailfrom']);
        $newEmail->setFrom( $data['fromname']);
        if(!is_array($TO)) $TO = array();
        foreach ($TO as $chvR => $valR) {
            if((string)$valR['email']!='') $newEmail->addRecipient($valR['email'], $valR['name']);
        }
        if(!is_array($BCC)) $BCC = array();
        foreach ($BCC as $chvB => $valB) {
            if((string)$valB['email']!='') $newEmail->addBcc($valB['email'], $valB['name']);
        }
        $newEmail->setSubject($data['sitename']);

        $newEmail->AddEmbeddedImage(JPATH_ROOT.$logoEmailGeral, "banner", "Logo");

        $return = $newEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }

    public static function SendEmailToUser4NewMsgHistFromManager($formName, $nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $msg, $TO, $BCC, $IntroName, $refPedido){
        $config = JFactory::getConfig();

        /* Carrega parâmetros*/
        $objVDParams     = new VirtualDeskSiteParamsHelper();
        $layoutPathEmail = $objVDParams->getParamsByTag('formMain_SendMail_NewMsgHistFromManager_LayoutPath');

        if((string)$layoutPathEmail=='') {
            // Se não carregar o caminho do layout sai com erro...
            echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
            exit();
        }
        $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
        $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
        $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_FORMMAIN_EMAIL_INTRO',$IntroName);
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_FORMMAIN_EMAIL_NEWMSG_CORPO2',$formName, $refPedido);
        $BODY_SECONDLINE    = JText::sprintf('COM_VIRTUALDESK_FORMMAIN_EMAIL_NEWMSG_CORPO3');
        $explodeMSG = explode('<br/>', $msg);

        if (strpos($explodeMSG[0], 'Em espera') !== false) {
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#bd4e2e; color:#fff;"><span style="color:#bd4e2e">_</span>Em espera<span style="color:#bd4e2e">_</span></span>';
        } else if (strpos($explodeMSG[0], 'Pendente') !== false) {
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#bd4e2e; color:#fff;"><span style="color:#bd4e2e">_</span>Em espera<span style="color:#bd4e2e">_</span></span>';
        } else if(strpos($explodeMSG[0], 'Em análise') !== false){
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#d4c744; color:#fff;"><span style="color:#d4c744">_</span>Em análise<span style="color:#d4c744">_</span></span>';
        } else if(strpos($explodeMSG[0], 'Concluído') !== false){
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#4b744f; color:#fff;"><span style="color:#4b744f">_</span>Concluído<span style="color:#4b744f">_</span></span>';
        } else if(strpos($explodeMSG[0], 'Espera de documentos') !== false){
            $BODY_THIRDLINE = '<b>Novo estado do processo:</b> <br> <span style="background:#009ddc; color:#fff;"><span style="color:#009ddc">_</span>Espera de documentos<span style="color:#009ddc">_</span></span>';
        } else {
            $BODY_THIRDLINE = $explodeMSG[0];
        }


        if(!empty($explodeMSG[1]) && $explodeMSG[1] != ' '){
            $BODY_DESCRITIVO  = '<b>Mensagem:</b> <br>' . $explodeMSG[1];
        } else {
            $BODY_DESCRITIVO = '';
        }


        $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
        $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
        $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
        $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
        $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
        $body      = str_replace("%BODY_DESCRITIVO",$BODY_DESCRITIVO, $body);
        $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
        $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
        $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
        $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
        $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

        // Send the password reset request email.
        $newEmail = JFactory::getMailer();
        $newEmail->Encoding = 'base64';
        $newEmail->isHtml(true);
        $newEmail->setBody($body);
        $newEmail->addReplyTo( $data['mailfrom']);
        $newEmail->setSender( $data['mailfrom']);
        $newEmail->setFrom( $data['fromname']);
        if(!is_array($TO)) $TO = array();
        foreach ($TO as $chvR => $valR) {
            if((string)$valR['email']!='') $newEmail->addRecipient($valR['email'], $valR['name']);
        }
        if(!is_array($BCC)) $BCC = array();
        foreach ($BCC as $chvB => $valB) {
            if((string)$valB['email']!='') $newEmail->addBcc($valB['email'], $valB['name']);
        }
        $newEmail->setSubject($data['sitename']);

        $newEmail->AddEmbeddedImage(JPATH_ROOT.$logoEmailGeral, "banner", "Logo");

        $return = $newEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;
    }

    public static function getReqName($getInputPedido_Id, $modulo){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('val_varchar')
            ->from("#__virtualdesk_form_main_data_" . $modulo)
            ->where($db->quoteName('id_rel') . "='" . $db->escape($getInputPedido_Id) . "'")
            ->where($db->quoteName('fields_id') . "='" . $db->escape('1') . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getReqMail($getInputPedido_Id, $modulo){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('val_varchar')
            ->from("#__virtualdesk_form_main_data_" . $modulo)
            ->where($db->quoteName('id_rel') . "='" . $db->escape($getInputPedido_Id) . "'")
            ->where($db->quoteName('fields_id') . "='" . $db->escape('16') . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getRepName($getInputPedido_Id, $modulo){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('val_varchar')
            ->from("#__virtualdesk_form_main_data_" . $modulo)
            ->where($db->quoteName('id_rel') . "='" . $db->escape($getInputPedido_Id) . "'")
            ->where($db->quoteName('fields_id') . "='" . $db->escape('18') . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getRepMail($getInputPedido_Id, $modulo){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('val_varchar')
            ->from("#__virtualdesk_form_main_data_" . $modulo)
            ->where($db->quoteName('id_rel') . "='" . $db->escape($getInputPedido_Id) . "'")
            ->where($db->quoteName('fields_id') . "='" . $db->escape('33') . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.addnewform4admin.formmain.data', null);
        $app->setUserState('com_virtualdesk.addnewfields4admin.formmain.data', null);
    }

    public static function getUploadsList4Manager($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('formmain','list4admin');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=editupload4admin&upload_id=');

                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.setUploadEnableByAjax&upload_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.getUploadEnableValByAjax&upload_id=');

                $table  = " ( SELECT a.id as id, a.id as upload_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.tag as tag, a.nome as nome, a.ref as ref";
                $table .= " , a.enabled as enabled";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_form_main_upload as a";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',            'dt' => 0 ),
                    array( 'db' => 'ref',           'dt' => 1 ),
                    array( 'db' => 'tag',           'dt' => 2 ),
                    array( 'db' => 'nome',          'dt' => 3 ),
                    array( 'db' => 'enabled',       'dt' => 4 ),
                    array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchSetHRef', 'dt' => 6 ),
                    array( 'db' => 'SwitchGetHRef', 'dt' => 7 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function setPluginUploadEnableState($UserJoomlaID, $getInputPlugin_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_plugin_id = $getInputPlugin_id;
        if((int) $configadmin_plugin_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_plugin_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableListaUploadsFiles($db);
        $Table->load(array('id'=>$configadmin_plugin_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function checkUploadEnableVal($getInputUpload_id)
    {
        if( empty($getInputUpload_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            $db->setQuery("Select enabled From #__virtualdesk_form_main_upload Where id =" . $db->escape($getInputUpload_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getRelacaoUploadsList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('formmain','list4admin');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=editrelformsuploads4admin&rel_id=');

                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.setRelacaoUploadEnableByAjax&rel_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=formmain.getRelacaoUploadEnableValByAjax&rel_id=');

                $table  = " ( SELECT a.id as id, a.id as rel_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.form_id, a.upload_id";
                $table .= " , a.enabled, IFNULL(b.nome, ' ') as formulario, IFNULL(c.nome, ' ') as upload, a.ordem";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " FROM ".$dbprefix."virtualdesk_form_main_upload_rel as a";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_form_main AS b ON b.id = a.form_id";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_form_main_upload AS c ON c.id = a.upload_id";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',            'dt' => 0 ),
                    array( 'db' => 'formulario',    'dt' => 1 ),
                    array( 'db' => 'upload',        'dt' => 2 ),
                    array( 'db' => 'ordem',         'dt' => 3 ),
                    array( 'db' => 'enabled',       'dt' => 4 ),
                    array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'form_id',       'dt' => 6 ),
                    array( 'db' => 'upload_id',     'dt' => 7 ),
                    array( 'db' => 'SwitchSetHRef', 'dt' => 8 ),
                    array( 'db' => 'SwitchGetHRef', 'dt' => 9 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function setPluginUploadRelEnableState($UserJoomlaID, $getInputPlugin_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_plugin_id = $getInputPlugin_id;
        if((int) $configadmin_plugin_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_plugin_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableRelacaoFormsUploads($db);
        $Table->load(array('id'=>$configadmin_plugin_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function checkRelUploadEnableVal($getInputForm_id)
    {
        if( empty($getInputForm_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('formmain');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            $db->setQuery("Select enabled From #__virtualdesk_form_main_upload_rel Where id =" . $db->escape($getInputForm_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }
}
?>