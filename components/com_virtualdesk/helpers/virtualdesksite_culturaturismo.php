<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

class VirtualDeskSiteCulturaTurismoHelper
    {
        const tagchaveModulo = 'culturaturismo';

        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnewcedenciaespacos4user.culturaturismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdoacaolivros4user.culturaturismo.data', null);
            $app->setUserState('com_virtualdesk.addnewinscricaofeiras4user.culturaturismo.data', null);
            $app->setUserState('com_virtualdesk.addnewmarcacaovisitas4user.culturaturismo.data', null);
            $app->setUserState('com_virtualdesk.addnewreservabilhetes4user.culturaturismo.data', null);
        }
    }
?>