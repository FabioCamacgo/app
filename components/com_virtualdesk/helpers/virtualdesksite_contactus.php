<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskTableContactUs', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/contactus.php');
JLoader::register('VirtualDeskTableContactUsFile', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/contactus_file.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/**
 * VirtualDesk ContactUs helper class.
 * @since  1.6
 */
class VirtualDeskSiteContactUsHelper
{

    public static function getContactUsListCount ($UserJoomlaID)
    {
        if( empty($UserJoomlaID) )  return false;

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id'))
                ->from("#__virtualdesk_contactus")
                ->where($db->quoteName('iduserjos').'='.$db->escape($UserJoomlaID) )
            );
            $db->execute();
            $num_rows = $db->getNumRows();

            return($num_rows);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
     * $vbForDataTables: se for true, vai carrega o JSON para os datatables
     */
    public static function getContactUsList ($UserJoomlaID, $vbForDataTables=false, $setLimit=-1)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('contactus', 'list');
        $vbHasReadAll = $objCheckPerm->checkReadAllAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_pt';
            }
            else {
                $CatName = 'name_en';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&contactus_id=');

                $table  = " ( SELECT a.id as id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.subject, a.idcategory as category, a.idstatus as status, b.".$CatName." as category_name, ";
                $table .= " DATE_FORMAT(a.created, '%Y-%m-%d') as created, DATE_FORMAT(a.modified, '%Y-%m-%d %H:%i') as modified, DATE_FORMAT(a.created, '%Y-%m-%d %H:%i') as createdFull, ";
                $table .= " IFNULL(c.".$CatName.", ' ') as estado, IFNULL(a.idestado, '0')  as idestado";
                $table .= " FROM ".$dbprefix."virtualdesk_contactus as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_contactus_category AS b ON b.id = a.idcategory ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_contactus_estado AS   c  ON c.id = a.idestado ";
                // Verifica se pode ler todos os registos ou apenas os do utilizador atual
                if($vbHasReadAll!==true) $table .= " WHERE (a.iduserjos=$UserJoomlaID) ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'created',       'dt' => 0 ),
                    array( 'db' => 'subject',       'dt' => 1 ),
                    array( 'db' => 'category_name', 'dt' => 2 ),
                    array( 'db' => 'estado',        'dt' => 3 ),
                    array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',      'dt' => 5 ),
                    array( 'db' => 'id',            'dt' => 6 ),
                    array( 'db' => 'modified',      'dt' => 7 ),
                    array( 'db' => 'createdFull',   'dt' => 8 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();

            // Verifica se pode ler todos os registos ou apenas os do utilizador atual
            $QueryWhere = ' 1=1 '; // por defeito
            if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('a.iduserjos').'='.$db->escape($UserJoomlaID);

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id', 'a.id as contactus_id', 'a.subject', 'a.description','a.description2','a.description3','a.description4','a.obs'
                 ,'a.idtype as type', 'a.idcategory as category', 'a.idsubcategory as subcategory', 'a.idstatus as status','a.areaact'
                 , 'b.'.$CatName. ' as category_name', 'c.'.$CatName. ' as subcategory_name', 'd.'.$CatName. ' as status_name'
                 , 'e.'.$CatName. ' as ctlanguage_name', 'f.'.$CatName. ' as vdwebsitelist_name', 'g.'.$CatName. ' as vdmenumain_name', 'h.'.$CatName. ' as vdmenusec_name', 'i.'.$CatName. ' as vdmenusec2_name'
                 , 'a.created as created','a.modified as modified','a.attachment as attachment'))
                ->join('LEFT', '#__virtualdesk_contactus_subcategory AS c ON c.id = a.idsubcategory')
                ->join('LEFT', '#__virtualdesk_contactus_category AS b ON b.id = a.idcategory')
                ->join('LEFT', '#__virtualdesk_contactus_status AS d ON d.id = a.idstatus')
                ->join('LEFT', '#__virtualdesk_language AS e ON e.id = a.idctlanguage')
                ->join('LEFT', '#__virtualdesk_contactus_websitelist AS f ON f.id = a.vdwebsitelist')
                ->join('LEFT', '#__virtualdesk_contactus_menumain AS g ON g.id = a.vdmenumain')
                ->join('LEFT', '#__virtualdesk_contactus_menusec AS h ON h.id = a.vdmenusec')
                ->join('LEFT', '#__virtualdesk_contactus_menusec2 AS i ON i.id = a.vdmenusec2')
                ->from("#__virtualdesk_contactus as a")
                ->where( $QueryWhere )
                ->order(' modified DESC ' . $setLimitSQL)
            );
            $dataReturn = $db->loadObjectList();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getContactUsListByCat ($UserJoomlaID, $IdCategory)
    {

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('contactus', 'list');
        $vbHasReadAll = $objCheckPerm->checkReadAllAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        if( empty($UserJoomlaID) )  return false;
        if( empty($IdCategory) )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_pt';
            }
            else {
                $CatName = 'name_en';
            }

            $db = JFactory::getDBO();

            // Verifica se pode ler todos os registos ou apenas os do utilizador atual
            $QueryWhere = ' 1=1 '; // por defeito
            if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('a.iduserjos').'='.$db->escape($UserJoomlaID);

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id', 'a.id as contactus_id', 'a.subject', 'a.description','a.description2','a.description3','a.description4','a.obs'
                ,'a.idtype as type', 'a.idcategory as category', 'a.idsubcategory as subcategory', 'a.idstatus as status','a.areaact'
                , 'b.'.$CatName. ' as category_name', 'c.'.$CatName. ' as subcategory_name','d.'.$CatName. ' as status_name'
                , 'e.'.$CatName. ' as ctlanguage_name', 'f.'.$CatName. ' as vdwebsitelist_name', 'g.'.$CatName. ' as vdmenumain_name', 'h.'.$CatName. ' as vdmenusec_name'
                , 'a.created as created','a.modified as modified','a.attachment as attachment'))
                ->join('LEFT', '#__virtualdesk_contactus_subcategory AS c ON c.id = a.idsubcategory')
                ->join('LEFT', '#__virtualdesk_contactus_category AS b ON b.id = a.idcategory')
                ->join('LEFT', '#__virtualdesk_contactus_status AS d ON d.id = a.idstatus')
                ->join('LEFT', '#__virtualdesk_language AS e ON e.id = a.idctlanguage')
                ->join('LEFT', '#__virtualdesk_contactus_websitelist AS f ON f.id = a.vdwebsitelist')
                ->join('LEFT', '#__virtualdesk_contactus_menumain AS g ON g.id = a.vdmenumain')
                ->join('LEFT', '#__virtualdesk_contactus_menusec AS h ON h.id = a.vdmenusec')
                ->from("#__virtualdesk_contactus as a")
                ->where($QueryWhere .' AND '.  $db->quoteName('a.idcategory').'='.$db->escape($IdCategory))
                ->order(' modified DESC ')
            );
            $dataReturn = $db->loadObjectList();


            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getContactUsDetail ($UserJoomlaID, $IdContactUs)
    {

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('contactus');
        $vbHasReadAll = $objCheckPerm->checkReadAllAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        if( empty($UserJoomlaID) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();

            // Verifica se pode ler todos os registos ou apenas os do utilizador atual
            $QueryWhere = ' 1=1 '; // por defeito
            if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('iduserjos') . '=' . $db->escape($UserJoomlaID) ;


            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as contactus_id','iduser','iduserjos','subject','description','description2','description3','description4','idtype as type'
                ,'idcategory as category', 'idsubcategory as subcategory','idstatus as status', 'idctlanguage as ctlanguage' , 'created','modified','attachment','obs','areaact'
                ,'vdwebsitelist', 'vdmenumain', 'vdmenusec', 'vdmenusec2' ))
                ->from("#__virtualdesk_contactus")
                ->where( $QueryWhere . ' AND ' . $db->quoteName('id') . '=' . $db->escape($IdContactUs) )
            );
            $dataReturn = $db->loadObject();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Carrega lista de TODAS as categorias
    */
    public static function getContactUsCategoryAll ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_pt','name_en') )
                ->from("#__virtualdesk_contactus_category")
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                // Add Area Act Group
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }
              $response[] =  array (
                'id' => $row->id,
                'name' => $rowName
                );
            }

            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Carrega lista de TODAS os tipos
    */
    public static function getContactUsTypeAll ($lang)
    {
        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_pt','name_en') )
                ->from("#__virtualdesk_contactus_type")
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {

                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }

                $response[] =  array (
                    'id' => $row->id,
                    'name' => $rowName
                );
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    public static function getContactUsStatusAll ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_pt','name_en') )
                ->from("#__virtualdesk_contactus_status")
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {

                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }

                $response[] =  array (
                    'id' => $row->id,
                    'name' => $rowName
                );
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    public static function getContactUsWebSiteListAll ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_pt','name_en') )
                ->from("#__virtualdesk_contactus_websitelist")
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {

                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }

                $response[] =  array (
                    'id' => $row->id,
                    'name' => $rowName
                );
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    public static function getContactUsLanguageAll ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_pt','name_en') )
                ->from("#__virtualdesk_language")
                ->where('contactus=1')
            );
            // Dump the query
            //echo $db->getQuery()->dump();
            $dataReturn = $db->loadObjectList();
            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }
                $response[] =  array (
                    'id' => $row->id,
                    'name' => $rowName
                );
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }





    /*
    * Carrega lista de TODAS os tipos
    */
    public static function getContactUsTypeById ($data, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idtype','a.idcategory','b.name_pt as name_pt','b.name_en as name_en') )
                ->join('LEFT', '#__virtualdesk_contactus_type AS b ON b.id = a.idtype')
                ->from("#__virtualdesk_contactus as a")
                ->where($db->quoteName('a.id') . '=' . $db->escape($data->contactus_id))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();

            if(empty($dataReturn)) return ('');

            //$rowName   = '';
            if( empty($lang) or ($lang='pt_PT') ) {
                   $rowName = $dataReturn->name_pt;
            }
            else {
                   $rowName = $dataReturn->name_en;
            }
            return($rowName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Carrega lista de TODAS os tipos
    */
    public static function getContactUsCategoryById ($data, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idtype','a.idcategory','b.name_pt as name_pt','b.name_en as name_en') )
                ->join('LEFT', '#__virtualdesk_contactus_category AS b ON b.id = a.idcategory')
                ->from("#__virtualdesk_contactus as a")
                ->where($db->quoteName('a.id') . '=' . $db->escape($data->contactus_id))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return ('');

            //$rowName   = '';
            if( empty($lang) or ($lang='pt_PT') ) {
                $rowName = $dataReturn->name_pt;
            }
            else {
                $rowName = $dataReturn->name_en;
            }
            return($rowName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Carrega lista de TODAS os tipos
    */
    public static function getContactUsCategoryNameById ($CatId, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('name_pt','name_en') )
                ->from("#__virtualdesk_contactus_category")
                ->where($db->quoteName('id') . '=' . $db->escape($CatId))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return ('');
            //$rowName   = '';
            if( empty($lang) or ($lang='pt_PT') ) {
                $rowName = $dataReturn->name_pt;
            }
            else {
                $rowName = $dataReturn->name_en;
            }
            return($rowName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
   *
   */
    public static function getContactUsSubCategoryById ($CatId, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_pt','name_en') )
                ->from("#__virtualdesk_contactus_subcategory")
                ->where($db->quoteName('idcategory') . '=' . $db->escape($CatId))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {

                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }

                $response[] =  array (
                    'id' => $row->id,
                    'name' => $rowName
                );
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getContactUsSubCategoryNameById ($SubCatId, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('name_pt','name_en') )
                ->from("#__virtualdesk_contactus_subcategory")
                ->where($db->quoteName('id') . '=' . $db->escape($SubCatId))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return ('');
            //$rowName   = '';
            if( empty($lang) or ($lang='pt_PT') ) {
                $rowName = $dataReturn->name_pt;
            }
            else {
                $rowName = $dataReturn->name_en;
            }
            return($rowName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getContactUsStatusNameById ($StatusId, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('name_pt','name_en') )
                ->from("#__virtualdesk_contactus_status")
                ->where($db->quoteName('id') . '=' . $db->escape($StatusId))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return ('');
            //$rowName   = '';
            if( empty($lang) or ($lang='pt_PT') ) {
                $rowName = $dataReturn->name_pt;
            }
            else {
                $rowName = $dataReturn->name_en;
            }
            return($rowName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getContactUsLanguageNameById ($LanguageId, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('name_pt','name_en') )
                ->from("#__virtualdesk_language")
                ->where($db->quoteName('id') . '=' . $db->escape($LanguageId) .' AND contactus=1 ')
            );

            // Dump the query
            //echo $db->getQuery()->dump();
            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return ('');
            //$rowName   = '';
            if( empty($lang) or ($lang='pt_PT') ) {
                $rowName = $dataReturn->name_pt;
            }
            else {
                $rowName = $dataReturn->name_en;
            }
            return($rowName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    public static function getContactUsWebSiteListNameById ($WebSiteId, $lang)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('name_pt','name_en') )
                ->from("#__virtualdesk_contactus_websitelist")
                ->where($db->quoteName('id') . '=' . $db->escape($WebSiteId) )
            );

            // Dump the query
            //echo $db->getQuery()->dump();
            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return ('');
            //$rowName   = '';
            if( empty($lang) or ($lang='pt_PT') ) {
                $rowName = $dataReturn->name_pt;
            }
            else {
                $rowName = $dataReturn->name_en;
            }
            return($rowName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Faz a gravação de um pedido de contacto (update)
    */
    public static function updateContactUs($UserJoomlaID, $UserVDId, $data)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailEditAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $IdContactUs = $data['contactus_id'];
        if((int) $IdContactUs <=0 ) return false;
        $data['id'] = $IdContactUs;
        unset($data['contactus_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $TableContactUs = new VirtualDeskTableContactUs($db);
        $TableContactUs->load(array('id'=>$IdContactUs, 'iduser'=>$UserVDId, 'iduserjos'=>$UserJoomlaID));
        if((integer)$TableContactUs->iduserjos !== (integer)$UserJoomlaID ) return false;


        //Poder anexar vários documentos ou imagens a um pedido
        // Se tem um ou mais ficheiros então...
        if ( !empty($data['attachment']) ) {

            $FileNameList = VirtualDeskSiteFileUploadHelper::saveFileListToFolder ( $UserJoomlaID , $data['attachment'] , JComponentHelper::getParams('com_virtualdesk')->get('contactus_filefolder') );
            if($FileNameList === false) {
                $data['attachment'] = '';
                return false;
            }
            else if (!empty($FileNameList)) {
                // Vai colocar os ficheiros associados ao
                $resFiles2Table = self::saveFileListToTable($FileNameList, $data);
                if($resFiles2Table === false) {
                    $data['attachment'] = '';
                    return false;
                }
            }
        }
        unset($data['attachment']);


        // Verifica se é para atualizar ficheiros nome/descrição: atualiza na base de dados
        if ( !empty($data['attachmentUPDATE']) ) {
            // Apaga da BD
            $FileNameList = self::updateFileListFromTable ($UserJoomlaID, $data['attachmentUPDATE'], $data );
            if($FileNameList === false) {
                $data['attachmentUPDATE'] = '';
                return false;
            }
        }
        unset($data['attachmentUPDATE']);


        // Verifica se é para eliminar ficheiros: depois de apagar da base de dados deve apagar do servidor
        if ( !empty($data['attachmentDELETE']) ) {
            // Apaga da BD
            $FileNameList = self::removeFileListFromTable ($UserJoomlaID, $data['attachmentDELETE'], $data );
            if($FileNameList === false) {
                $data['attachmentDELETE'] = '';
                return false;
            }
        }
        unset($data['attachmentDELETE']);


        $dataReturnAreaAct = self::saveContactUsAreaAct ($UserJoomlaID, $IdContactUs, $data);
        if($dataReturnAreaAct===false) return false;
        //if( $dataReturnAreaAct!==true && !empty((string)$dataReturnAreaAct) ) $data['areaact'] = $dataReturnAreaAct;
        if($dataReturnAreaAct !==true ) $data['areaact'] = (string)$dataReturnAreaAct;

        // Excepções de nomes... a corrigir quando tiver tempo
        if(!empty($data['type'])) {
            $data['idtype'] = $data['type'];
            unset($data['type']);
        }
        if(!empty($data['category'])) {
            $data['idcategory'] = $data['category'];
            unset($data['category']);
        }
        if(!empty($data['subcategory'])) {
            $data['idsubcategory'] = $data['subcategory'];
            unset($data['subcategory']);
        }
        if(!empty($data['status'])) {
            $data['idstatus'] = $data['status'];
            unset($data['status']);
        }
        if(!empty($data['ctlanguage'])) {
            $data['idctlanguage'] = $data['ctlanguage'];
            unset($data['ctlanguage']);
        }

        if(!empty($data)) {
           $dateModified = new DateTime();
           $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        // Bind the data.
        if (!$TableContactUs->bind($data)) return false;

        // Store the data.
        if (!$TableContactUs->save($data)) return false;



        // Foi alterado o Pedido do tipo Contact Us
        $vdlog = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['iduser']        = $UserVDId;
        $eventdata['idjos']         = $UserJoomlaID;
        $eventdata['title']         = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_UPDATED_TITLE');
        $eventdata['desc']          = self::getEventLogMailDesc ($TableContactUs, JText::_('COM_VIRTUALDESK_UPDATED'));
        $eventdata['filelist']      = self::getEventLogMailFileList($TableContactUs);  // lista de ficheiro comm links guest, ou seja, qualquer com o link faz downlaod do ficheiro
        $eventdata['category']      = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;
    }


    /*
    * Faz a gravação de uma empresa de um utilizador (update)
    */
    public static function createContactUs($UserJoomlaID, $UserVDId, $data)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailAddNewAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $TableContactUs = new VirtualDeskTableContactUs($db);

        if (empty((integer)$UserJoomlaID) ) return false;
        if (empty((integer)$UserVDId) ) return false;
        if (empty($data) ) return false;

        // Associa ao novo registo os Ids do utilizador
        $data['iduser']    = $UserVDId;
        $data['iduserjos'] = $UserJoomlaID;


        //Poder anexar vários documentos ou imagens a um pedido
        // Se tem um ou mais ficheiros então...
        $TmpFilesAttachTemp = array();
        if ( !empty($data['attachment']) ) {
            $TmpFilesAttachTemp = $data['attachment'];
        }
        unset($data['attachment']);


        // Excepções de nomes... a corrigir quando tiver tempo
        if(!empty($data['type'])) {
            $data['idtype'] = $data['type'];
            unset($data['type']);
        }
        if(!empty($data['category'])) {
            $data['idcategory'] = $data['category'];
            unset($data['category']);
        }
        if(!empty($data['subcategory'])) {
            $data['idsubcategory'] = $data['subcategory'];
            unset($data['subcategory']);
        }
        if(!empty($data['status'])) {
            $data['idstatus'] = $data['status'];
            unset($data['status']);
        }
        if(!empty($data['ctlanguage'])) {
            $data['idctlanguage'] = $data['ctlanguage'];
            unset($data['ctlanguage']);
        }


        // Bind the data.
        if (!$TableContactUs->bind($data)) return false;
        // Store the data.
        if (!$TableContactUs->save($data)) return false;

        $GetCreateContactUsId = $TableContactUs->id;
        if( empty($GetCreateContactUsId) ) return false;



        // Se correu tudo bem ao criar o registo e tem ficheiros, então associa os ficheiros uploaded anteriores ao registo de CONTACTUS acabado de inserir
        if (!empty($TmpFilesAttachTemp)) {
            $FileNameList = VirtualDeskSiteFileUploadHelper::saveFileListToFolder ( $UserJoomlaID , $TmpFilesAttachTemp , JComponentHelper::getParams('com_virtualdesk')->get('contactus_filefolder') );
            if($FileNameList === false) return false;
            $data['id'] = $GetCreateContactUsId;
            if (!empty($FileNameList)) {
                $resFiles2Table = self::saveFileListToTable($FileNameList, $data);
                if ($resFiles2Table === false) return false;
            }
        }


        // Grava novamente apenas a AREAACT...
        $dataReturnAreaAct = self::saveContactUsAreaAct($UserJoomlaID, $GetCreateContactUsId , $data);
        if($dataReturnAreaAct===false) return false;
        unset($data);
        $data = array();
        if($dataReturnAreaAct!==true ) $data['areaact'] = $dataReturnAreaAct;
        // Bind the data.
        if (!$TableContactUs->bind($data)) return false;
        // Store the data.
        if (!$TableContactUs->save($data)) return false;


        // Foi alterado o Pedido do tipo Contact Us
        $vdlog = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['iduser']        = $UserVDId;
        $eventdata['idjos']         = $UserJoomlaID;
        $eventdata['title']         = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_CREATED_TITLE');
        $eventdata['desc']          = self::getEventLogMailDesc ($TableContactUs,  JText::_('COM_VIRTUALDESK_NEW') );
        $eventdata['filelist']      = self::getEventLogMailFileList($TableContactUs);  // lista de ficheiro comm links guest, ou seja, qualquer com o link faz downlaod do ficheiro
        $eventdata['category']      = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;
    }


    /*
    * Elimina um pedido de contacto (delete)
    */
    public static function deleteContactUs($UserJoomlaID, $UserVDId, $data)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailDeleteAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $IdContactUs = $data['contactus_id'];
        if((int) $IdContactUs <=0 ) return false;
        $data['id'] = $IdContactUs;
        unset($data['contactus_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $TableContactUs = new VirtualDeskTableContactUs($db);
        $TableContactUs->load(array('id'=>$IdContactUs, 'iduser'=>$UserVDId, 'iduserjos'=>$UserJoomlaID));
        if((integer)$TableContactUs->iduserjos !== (integer)$UserJoomlaID ) return false;

        // Eliimina ficheiros associados ao pedido
        if(!self::deleteFilesAssociatedInRequest ($IdContactUs )) return (false);

        // elimina area act
        $queryDel   = $db->getQuery(true);
        $conditions = array( $db->quoteName('idcontactus') . ' = ' . $db->escape($IdContactUs)   );
        $queryDel->delete($db->quoteName('#__virtualdesk_contactus_areaact'));
        $queryDel->where($conditions);
        $db->setQuery($queryDel);
        $db->execute();

        if (!$TableContactUs->delete($IdContactUs)) return false;

        // Foi alterado o Pedido do tipo Contact Us
        $vdlog = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['iduser']        = $UserVDId;
        $eventdata['idjos']         = $UserJoomlaID;
        $eventdata['title']         = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_DELETED_TITLE');
        $eventdata['desc']          = self::getEventLogMailDesc ($TableContactUs, $eventdata['title'] );
        $eventdata['category']      = JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;
    }



    /*
   *
   */
    public static function getCheckLayoutToRedirect($layout2Redirect)
    {
        $IdCategoryContactUs = JFactory::getApplication()->input->getInt('contactus_idcategory');
        $ParamIdCategory     = '&contactus_idcategory='. $IdCategoryContactUs;
        $LayoutToDisplay     = JFactory::getApplication()->input->get('layout');
        if( (int) $IdCategoryContactUs > 0 || substr($LayoutToDisplay,-3)=='cat' ) {

            if(empty($layout2Redirect) &&  substr($LayoutToDisplay,-3)=='cat' )  return($LayoutToDisplay . $ParamIdCategory);

            switch($layout2Redirect){
                case 'edit':
                    return('editcat' . $ParamIdCategory);
                    break;
                case 'list':
                    return('listcat' . $ParamIdCategory);
                    break;
                case 'addnew':
                    return('addnewcat' . $ParamIdCategory);
                    break;
                case 'default':
                    return('defaultcat' . $ParamIdCategory);
                    break;
                case '':
                    return('defaultcat' . $ParamIdCategory);
                    break;
            }
        }
        return($layout2Redirect);
    }



    /*
    * Carrega lista ficheiros de um registo
    */
    public static function getContactUsFileListById ($UserJoomlaID, $ContactusId)
    {

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('contactus');
        $vbHasReadAll = $objCheckPerm->checkReadAllAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }



        try
        {   $db = JFactory::getDBO();

            // Verifica se pode ler todos os registos ou apenas os do utilizador atual
            $QueryWhere = ' 1=1 '; // por defeito
            if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID) ;


            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idcontactus','a.desc','a.basename','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->join('LEFT', '#__virtualdesk_contactus AS b ON b.id = a.idcontactus')
                ->from("#__virtualdesk_contactus_file AS a")
                ->where($db->quoteName('a.idcontactus') . '=' . $db->escape($ContactusId) . ' AND ' . $QueryWhere )
            );


            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Carrega ficheiro associado a um registo
    */
    public static function getContactUsFileNameById ($UserJoomlaID, $ContactusId, $basename)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('contactus');
        $vbHasReadAll = $objCheckPerm->checkReadAllAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {   $db = JFactory::getDBO();

            // Verifica se pode ler todos os registos ou apenas os do utilizador atual
            $QueryWhere = ' 1=1 '; // por defeito
            if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID) ;

            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idcontactus','a.basename','a.desc','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->join('LEFT', '#__virtualdesk_contactus AS b ON b.id = a.idcontactus')
                ->from("#__virtualdesk_contactus_file AS a")
                ->where($db->quoteName('a.idcontactus') . '=' . $db->escape($ContactusId)
                    . ' AND ' . $QueryWhere
                    . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
   * Carrega ficheiro associado a um registo SEM ser necessário o joomla user
   */
    public static function getContactUsFileName2GuestLink ($ContactusId, $basename)
    {
        // Aqui não é necessário verificar permissões...

        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idcontactus','a.basename','a.desc','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->join('LEFT', '#__virtualdesk_contactus AS b ON b.id = a.idcontactus')
                ->from("#__virtualdesk_contactus_file AS a")
                ->where($db->quoteName('a.idcontactus') . '=' . $db->escape($ContactusId)
                    . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
            );


            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }




    /**
    * Carrega os ficheiros associados ao registo e apresenta de acordo com o tipo de ficheiro o icon
    * O ideal será escolher entre uma disposição em lista ou em tabela para os ficheiros associados
    * Apresenta o icon ou o preview se for imagem, o nome, o tamanho e a data ( de modificação)
    */
    public static function getContactUsFilesLayout ($data)
    {
        if(empty($data->contactus_id)) return false;
        // carregar a lista de ficheiros, com os dados e os icons respectivos
        $userReqFileList = VirtualDeskSiteContactUsHelper::getContactUsFileListById (VirtualDeskSiteUserHelper::getUserSessionId(), $data->contactus_id);
        $userReqFileList = VirtualDeskSiteFileUploadHelper::setFileListIconsOrPreview ($userReqFileList);
        return($userReqFileList);
    }




    public static function saveFileListToTable ($FileList, $dataSent )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasEditAccess    = $objCheckPerm->checkDetailEditAccess('contactus');
        $vbHasAddNewAccess  = $objCheckPerm->checkDetailAddNewAccess('contactus');
        if($vbHasEditAccess===false && $vbHasAddNewAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        foreach ($FileList as $rowFile)
        {
            $IdContactUs = $dataSent['id'];
            if((int) $IdContactUs <=0 ) return false;
            $data['idcontactus'] = $IdContactUs;
            $data['filename']    = $rowFile['filename'];
            $data['basename']    = $rowFile['basename'];
            $data['ext']         = $rowFile['ext'];
            $data['type']        = $rowFile['type'];
            $data['size']        = $rowFile['size'];
            $data['desc']        = $rowFile['desc'];
            $data['filepath']    = $rowFile['filepath'];

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $TableContactUsFile = new VirtualDeskTableContactUsFile($db);

            // Bind the data.
            if (!$TableContactUsFile->bind($data)) return false;

            // Store the data.
            if (!$TableContactUsFile->save($data)) return false;



        }

        return true;
    }



    public static function removeFileListFromTable ($UserJoomlaID, $FileList, $dataSent )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasEditAccess    = $objCheckPerm->checkDetailEditAccess('contactus');
        $vbHasAddNewAccess  = $objCheckPerm->checkDetailAddNewAccess('contactus');
        $vbHasReadAll       = $objCheckPerm->checkReadAllAccess('contactus');
        if($vbHasEditAccess===false && $vbHasAddNewAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        foreach ($FileList as $basename)
        {
            $IdContactUs = $dataSent['id'];
            if((int) $IdContactUs <=0 ) return false;

            try
            {   $db = JFactory::getDBO();

                // Verifica se pode ler todos os registos ou apenas os do utilizador atual
                $QueryWhere = ' 1=1 '; // por defeito
                if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID) ;

                $db->setQuery($db->getQuery(true)
                    ->select( array('a.id','a.basename','a.filename','a.filepath') )
                    ->join('LEFT', '#__virtualdesk_contactus AS b ON b.id = a.idcontactus')
                    ->from("#__virtualdesk_contactus_file AS a")
                    ->where($db->quoteName('a.idcontactus') . '=' . $db->escape($IdContactUs)
                        . ' AND ' . $QueryWhere
                        . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
                );
                $dataReturn = $db->loadObject();

                if($dataReturn->basename == $basename) {
                    $db = JFactory::getDBO();
                    $TableContactUsFile = new VirtualDeskTableContactUsFile($db);
                    $resDelete = $TableContactUsFile->delete($dataReturn->id);
                    if (!$resDelete ) return($resDelete);
                    $resDelete = VirtualDeskSiteFileUploadHelper::deleteFileFromFolder($dataReturn->filepath, $dataReturn->filename);
                    if (!$resDelete ) return($resDelete);
                }
                else {
                    return false;
                }

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }
        return true;
    }


    public static function updateFileListFromTable ($UserJoomlaID, $FileList, $dataSent )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';


        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasEditAccess    = $objCheckPerm->checkDetailEditAccess('contactus');
        $vbHasAddNewAccess  = $objCheckPerm->checkDetailAddNewAccess('contactus');
        $vbHasReadAll       = $objCheckPerm->checkReadAllAccess('contactus');
        if($vbHasEditAccess===false && $vbHasAddNewAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        foreach ($FileList as $basename => $filenameupdate)
        {
            $IdContactUs = $dataSent['id'];
            if((int) $IdContactUs <=0 ) return false;

            try
            {   $db = JFactory::getDBO();

                // Verifica se pode ler todos os registos ou apenas os do utilizador atual
                $QueryWhere = ' 1=1 '; // por defeito
                if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID) ;

                $db->setQuery($db->getQuery(true)
                    ->select( array('a.id','a.basename','a.filename','a.filepath') )
                    ->join('LEFT', '#__virtualdesk_contactus AS b ON b.id = a.idcontactus')
                    ->from("#__virtualdesk_contactus_file AS a")
                    ->where($db->quoteName('a.idcontactus') . '=' . $db->escape($IdContactUs)
                        . ' AND ' . $QueryWhere
                        . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
                );
                $dataReturn = $db->loadObject();

                // Dump the query
                //echo $db->getQuery()->dump();

                if($dataReturn->basename == $basename) {
                    $data = array();
                    $data['id']   = $dataReturn->id;
                    $data['desc'] = $filenameupdate;

                    // Carrega dados atuais na base de dados
                    $db    = JFactory::getDbo();
                    $TableContactUsFile = new VirtualDeskTableContactUsFile($db);
                    // Bind the data.
                    if (!$TableContactUsFile->bind($data)) return false;
                    // Store the data.
                    if (!$TableContactUsFile->save($data)) return false;
                }
                else {
                    return false;
                }

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }
        return true;
    }


    public static function getFilesAssociatedInRequest ($UserJoomlaID, $IdContactUs )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($UserJoomlaID) )  return -1;
        if ( empty($IdContactUs) )  return -1;

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasReadAll       = $objCheckPerm->checkReadAllAccess('contactus');

        $db = JFactory::getDBO();

        // Verifica se pode ler todos os registos ou apenas os do utilizador atual
        $QueryWhere = ' 1=1 '; // por defeito
        if($vbHasReadAll!==true) $QueryWhere = $db->quoteName('b.iduserjos') . '=' . $db->escape($UserJoomlaID) ;

        $db->setQuery($db->getQuery(true)
            ->select( array('a.id') )
            ->join('LEFT', '#__virtualdesk_contactus AS b ON b.id = a.idcontactus')
            ->from("#__virtualdesk_contactus_file AS a")
            ->where($db->quoteName('a.idcontactus') . '=' . $db->escape($IdContactUs)
                . ' AND ' . $QueryWhere )
        );
        $db->execute();
        return($db->getNumRows());
     }


    public static function CheckFileLimitOfSaveRequest ($UserJoomlaID, $data )
    {

      if ( empty($data) )  return true;
      $IdContactUs = $data['contactus_id'];
      if ( empty($UserJoomlaID) )  return false;
      if ( empty($IdContactUs) )  $IdContactUs = -1;

      // Verifica se foi atingido o limite de ficheiros que podem ser anexados
      if ( !is_array($data['attachment'])) {
        $NewFiles = 0;
      }
      else {
        $NewFiles = sizeof($data['attachment']);
      }

      if ( !is_array($data['attachmentDELETE'])) {
            $FilesToDelete = 0;
      }
      else {
            $FilesToDelete = sizeof($data['attachmentDELETE']);
      }

      $Limit = (int) JComponentHelper::getParams('com_virtualdesk')->get('contactus_filelimitnum');
      $AtualFiles     = self::getFilesAssociatedInRequest ($UserJoomlaID, $IdContactUs);
      $FilesAfterSave = (int)$AtualFiles+(int)$NewFiles-(int)$FilesToDelete;

      if((int)$FilesAfterSave <=(int)$Limit ) return true;
      return false;
    }


    public static function deleteFilesAssociatedInRequest ($IdContactUs )
    {

        // Não coloquei aqui permissões porque já estão a ser invocadas no método anterior...

        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($IdContactUs) )  return -1;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','filepath','filename') )
            ->from("#__virtualdesk_contactus_file")
            ->where($db->quoteName('idcontactus') . '=' . $db->escape($IdContactUs) )
        );
        $FileList = $db->loadObjectList();

        foreach ($FileList as $File)
        {
          VirtualDeskSiteFileUploadHelper::deleteFileFromFolder($File->filepath, $File->filename);
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->delete('#__virtualdesk_contactus_file')
            ->where($db->quoteName('idcontactus') . '=' . $db->escape($IdContactUs))
        );
        return($db->execute());
    }



    /*
    * Carrega lista das áreas de atuação associadas a um utilizador (quando é uma empresa)
    */
    public static function saveContactUsAreaAct ($UserJoomlaID, $IdContactUs, $data)
    {
        if( empty($UserJoomlaID) )  return false;
        if( empty($IdContactUs) )  return false;
        if( !array_key_exists('areaact',$data) )  return true;  // não foram alterados os dados...

        $dataReturn = "";
        try
        {
            $db = JFactory::getDBO();

            if(!empty($data['areaact']))
            {   $queryDel = $db->getQuery(true);
                $conditions = array(
                    $db->quoteName('idcontactus') . ' = ' . $db->escape($IdContactUs)
                );
                $queryDel->delete($db->quoteName('#__virtualdesk_contactus_areaact'));
                $queryDel->where($conditions);
                $db->setQuery($queryDel);
                if (!$db->execute()) return false;

                // Create a new query object.
                $queryIns = $db->getQuery(true);
                // Insert columns.
                $columns = array('idcontactus', 'idareaact');
                // Insert values.
                $queryIns
                    ->insert($db->quoteName('#__virtualdesk_contactus_areaact'))
                    ->columns($db->quoteName($columns));
                foreach($data['areaact'] as $keyDataArea )
                {
                    $values = $IdContactUs.',' . $keyDataArea;
                    $queryIns->values($values);
                }

                // Set the query using our newly populated query object and execute it.
                $db->setQuery($queryIns);

                if (!$db->execute()) return false;
                asort($data['areaact']);
                $dataReturn = implode('|', $data['areaact']);
            }
            else
            {   // não tem nenhuma escolha... se carregou dados das AreasAct então devem ser eliminados
                if(empty($dataReturn)) {
                    $query = $db->getQuery(true);
                    $conditions = array(
                        $db->quoteName('idcontactus') . ' = ' . $db->escape($IdContactUs)
                    );
                    $query->delete($db->quoteName('#__virtualdesk_contactus_areaact'));
                    $query->where($conditions);
                    $db->setQuery($query);

                    if (!$db->execute()) return false;
                }
            }

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }






    public static function getEventLogMailDesc ($tabledata, $EventTitle )
    {
        //$return    = "";
        $emailHTML = file_get_contents(JPATH_SITE . '/components/com_virtualdesk/helpers/html/virtualdesksite_logemail_desc.html');

        $fileLangSufix             = VirtualDeskHelper::getLanguageTag();
        $DESC_CATEGORY_VAL         = VirtualDeskSiteContactUsHelper::getContactUsCategoryNameById($tabledata->idcategory, $fileLangSufix);
        $DESC_STATUS_VAL           = VirtualDeskSiteContactUsHelper::getContactUsStatusNameById($tabledata->idstatus, $fileLangSufix);
        $DESC_CTLANGUAGE_VAL       = VirtualDeskSiteContactUsHelper::getContactUsLanguageNameById($tabledata->idctlanguage, $fileLangSufix);
        $DESC_AREAACT_LISTVAL      = VirtualDeskSiteContactUsFieldsHelper::getAreaActListSelectName($tabledata, $fileLangSufix);
        $DESC_USER_VAL             = JFactory::getUser()->name;
        $DESC_USEREMAIL_VAL        = JFactory::getUser()->email;
        $DESC_SUBJECT_VAL          = $tabledata->subject;
        $DESC_DESCRIPTION_VAL      = $tabledata->description;
        $DESC_DESCRIPTION2_VAL     = $tabledata->description2;
        $DESC_DESCRIPTION3_VAL     = $tabledata->description3;
        $DESC_DESCRIPTION4_VAL     = $tabledata->description4;
        $DESC_OBS_VAL              = $tabledata->obs;

        $DESC_WEBSITELIST_VAL      = VirtualDeskSiteContactUsHelper::getContactUsWebSiteListNameById($tabledata->vdwebsitelist, $fileLangSufix);
        $DESC_MENUMAIN_VAL         = VirtualDeskSiteContactUsFieldsHelper::getMenuMainById($tabledata->vdmenumain);
        $DESC_MENUSEC_VAL          = VirtualDeskSiteContactUsFieldsHelper::getMenuSecById($tabledata->vdmenusec);
        $DESC_MENUSEC2_VAL          = VirtualDeskSiteContactUsFieldsHelper::getMenuSec2ById($tabledata->vdmenusec2);


        if(!is_array($DESC_AREAACT_LISTVAL)) $DESC_AREAACT_LISTVAL = array();
        $DESC_AREAACT_VAL = htmlentities(implode(', ',$DESC_AREAACT_LISTVAL), ENT_QUOTES, 'UTF-8');

        $return = $emailHTML;

        if(empty($DESC_CATEGORY_VAL)) $DESC_CATEGORY_VAL = '-';
            $return      = str_replace("%DESC_CATEGORY_LABEL",JText::_('COM_VIRTUALDESK_CONTACTUS_CATEGORY_LABEL').':', $return );
            $return      = str_replace("%DESC_CATEGORY_VAL",$DESC_CATEGORY_VAL, $return );


        if(empty($DESC_STATUS_VAL))  $DESC_STATUS_VAL = '-';
        $return = str_replace("%DESC_STATUS_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_STATUS_LABEL') . ':', $return);
            $return = str_replace("%DESC_STATUS_VAL", $DESC_STATUS_VAL, $return);

        if(empty($DESC_CTLANGUAGE_VAL))  $DESC_CTLANGUAGE_VAL = '-';
        $return = str_replace("%DESC_CTLANGUAGE_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_CTLANGUAGE_LABEL') . ':', $return);
        $return = str_replace("%DESC_CTLANGUAGE_VAL", $DESC_CTLANGUAGE_VAL, $return);

        if(empty($DESC_AREAACT_VAL))  $DESC_AREAACT_VAL = '-';
        $return = str_replace("%DESC_AREAACT_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_AREAACT_LABEL') . ':', $return);
        $return = str_replace("%DESC_AREAACT_VAL", $DESC_AREAACT_VAL, $return);

        if(empty($DESC_SUBJECT_VAL)) $DESC_SUBJECT_VAL = '-';
            $return = str_replace("%DESC_SUBJECT_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_SUBJECT_LABEL') . ':', $return);
            $return = str_replace("%DESC_SUBJECT_VAL", $DESC_SUBJECT_VAL, $return);

        if(empty($DESC_DESCRIPTION_VAL)) $DESC_DESCRIPTION_VAL = '-';
            $return = str_replace("%DESC_DESCRIPTION_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION_LABEL') . ':', $return);
            $return = str_replace("%DESC_DESCRIPTION_VAL", $DESC_DESCRIPTION_VAL, $return);

        if(empty($DESC_DESCRIPTION2_VAL)) $DESC_DESCRIPTION2_VAL = '-';
            $return = str_replace("%DESC_DESCRIPTION2_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION2_LABEL') . ':', $return);
            $return = str_replace("%DESC_DESCRIPTION2_VAL", $DESC_DESCRIPTION2_VAL, $return);


        if(empty($DESC_DESCRIPTION3_VAL)) $DESC_DESCRIPTION3_VAL = '-';
            $return = str_replace("%DESC_DESCRIPTION3_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION3_LABEL') . ':', $return);
            $return = str_replace("%DESC_DESCRIPTION3_VAL", $DESC_DESCRIPTION3_VAL, $return);


        if(empty($DESC_DESCRIPTION4_VAL)) $DESC_DESCRIPTION4_VAL = '-';
            $return = str_replace("%DESC_DESCRIPTION4_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_LABEL') . ':', $return);
            $return = str_replace("%DESC_DESCRIPTION4_VAL", $DESC_DESCRIPTION4_VAL, $return);


        if(empty($DESC_OBS_VAL)) $DESC_OBS_VAL = '-';
            $return = str_replace("%DESC_OBS_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_OBS_LABEL') . ':', $return);
            $return = str_replace("%DESC_OBS_VAL", $DESC_OBS_VAL, $return);


        if(empty($DESC_USER_VAL)) $DESC_USER_VAL = '-';
            $return = str_replace("%DESC_USER_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_MAIL_DESCUSER'), $return);
            $return = str_replace("%DESC_USER_VAL", $DESC_USER_VAL, $return);


        if(empty($DESC_WEBSITELIST_VAL)) $DESC_WEBSITELIST_VAL = '-';
        $return = str_replace("%DESC_WEBSITELIST_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_VDWEBSITELIST_LABEL'), $return);
        $return = str_replace("%DESC_WEBSITELIST_VAL", $DESC_WEBSITELIST_VAL, $return);

        if(empty($DESC_MENUMAIN_VAL)) $DESC_MENUMAIN_VAL = '-';
        $return = str_replace("%DESC_MENUMAIN_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_VDMENUMAIN_LABEL'), $return);
        $return = str_replace("%DESC_MENUMAIN_VAL", $DESC_MENUMAIN_VAL, $return);

        if(empty($DESC_MENUSEC_VAL)) $DESC_MENUSEC_VAL = '-';
        $return = str_replace("%DESC_MENUSEC_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_VDMENUSEC_LABEL'), $return);
        $return = str_replace("%DESC_MENUSEC_VAL", $DESC_MENUSEC_VAL, $return);

        if(empty($DESC_MENUSEC2_VAL)) $DESC_MENUSEC2_VAL = '-';
        $return = str_replace("%DESC_MENUSEC2_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_VDMENUSEC2_LABEL'), $return);
        $return = str_replace("%DESC_MENUSEC2_VAL", $DESC_MENUSEC2_VAL, $return);

        if(empty($DESC_USER_VAL)) $DESC_USEREMAIL_VAL = '-';
            $return      = str_replace("%DESC_USEREMAIL_VAL",$DESC_USEREMAIL_VAL, $return );

        $return      = str_replace("%EVENT_TITLE",$EventTitle, $return );

        return($return);
    }



    public static function getEventLogMailFileList ($tabledata)
    {
        $return    = "";
        $emailHTML = file_get_contents(JPATH_SITE . '/components/com_virtualdesk/helpers/html/virtualdesksite_logemail_filelist.html');

        $FileList = self::getContactUsFileListById (JFactory::getUser()->id, $tabledata->id);
        if(empty($FileList)) return ($return);

        $DESC_FILELIST = '';
        foreach ($FileList as $fileRow) {

            $linkRoute  = JUri::root().'index.php?options=com_virtualdesk&task=contactus.download&contactus_id=' . $tabledata->id;
            $linkRoute .= '&bname=' . $fileRow->basename;
            $linkRoute .= '&cname=' . VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($fileRow->basename);

            $DESC_FILELIST .= '<a href="'.JRoute::_($linkRoute, false).'">' . $fileRow->desc . '</a>';
            $DESC_FILELIST .= '<br/>';
        }

        $return   = str_replace("%DESC_FILELIST",$DESC_FILELIST, $emailHTML );
        $return   = str_replace("%DESC_LISTLABEL",JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_MAIL_FILELIST_LABEL') .':', $return );

        return($return);
    }



    public static function getContactUsEstadoAllOptions ($lang)
    {
        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_pt','name_en') )
                ->from("#__virtualdesk_contactus_estado")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }
                $response[] = array(
                        'id' => $row->id,
                        'name' => $rowName
                 );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



}