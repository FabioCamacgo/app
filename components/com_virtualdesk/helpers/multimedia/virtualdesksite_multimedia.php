<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteMultimediaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/multimedia/fotoCapa.php');
    JLoader::register('VirtualDeskTableMultimedia', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/multimedia.php');
    JLoader::register('VirtualDeskTableMultimediaEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/multimediaEstadoHistorico.php');
    JLoader::register('VirtualDeskTableMultimediaFotoCapaFiles', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/multimediaFotoCapa.php');

    class VirtualDeskSiteMultimediaHelper
    {

        const tagchaveModulo = 'multimedia';


        public static function getCategoria(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Multimedia_Categoria")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriaSelect($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Multimedia_Categoria")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCategoria($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Multimedia_Categoria")
                ->where($db->quoteName('id') . "!='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstado(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Multimedia_Estado")
                ->order('id_estado ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstadoSelect($estado){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('estado')
                ->from("#__virtualdesk_Multimedia_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($estado) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeEstado($estado){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Multimedia_Estado")
                ->where($db->quoteName('id_estado') . "!='" . $db->escape($estado) . "'")
                ->order('id_estado ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'DISTINCT ano')
                ->from("#__virtualdesk_Multimedia")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeAno($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'DISTINCT ano')
                ->from("#__virtualdesk_Multimedia")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('ano') . "!='" . $db->escape($ano) . "'")
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 15);
        }


        public static function CheckReferencia($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_Multimedia")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('multimedia');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('multimedia', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $refExiste = 1;
                $referencia = '';
                while($refExiste == 1){
                    $random = self::random_code();

                    $referencia = 'multi' . $random;
                    $checkREF = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                $nomeVideo = $data['nomeVideo'];
                $linkVideo = $data['linkVideo'];
                $creditos = $data['creditos'];
                $categoria = $data['categoria'];
                $data_publicacao = $data['data_publicacao'];
                $publish_FP = '0';
                $estado = '2';

                $resSaveVideo = self::saveNewVideo($referencia, $nomeVideo, $linkVideo, $creditos, $categoria, $data_publicacao, $publish_FP, $estado);

                if (empty($resSaveVideo)) {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteMultimediaCapaFilesHelper();
                $objCapaFiles->tagprocesso   = 'MULTIMEDIA_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

                if ($resFileSaveCapa===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Multimedia_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_MULTIMEDIA_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_MULTIMEDIA_EVENTLOG_CREATED') . 'ref=' . $referencia;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_MULTIMEDIA_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewVideo($referencia, $nomeVideo, $linkVideo, $creditos, $categoria, $data_publicacao, $publish_FP, $estado){
            if(empty($data_publicacao)){
                $data_publicacao = date("Y-m-d");
            }
            $splidData = explode('-', $data_publicacao);
            $ano = $splidData[0];
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $columns = array('referencia', 'nome', 'link', 'creditos', 'categoria', 'ano', 'publish_FP', 'estado', 'data_publicacao');
            $values = array($db->quote($referencia), $db->quote($nomeVideo), $db->quote($linkVideo), $db->quote($creditos), $db->quote($categoria), $db->quote($ano), $db->quote($publish_FP), $db->quote($estado), $db->quote($data_publicacao));
            $query
                ->insert($db->quoteName('#__virtualdesk_Multimedia'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);
        }


        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name') )
                    ->from("#__virtualdesk_Multimedia_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-pendente';
                break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }


        public static function getMultimediaList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('multimedia', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('multimedia','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=multimedia&layout=view4manager&multimedia_id=');

                    $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=multimedia.setVideoEnableByAjax&multimedia_id=');
                    $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=multimedia.getVideoEnableValByAjax&multimedia_id=');

                    $table  = " ( SELECT a.id as id, a.id as multimedia_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as referencia, a.categoria as idcategoria, b.categoria as categoria, a.creditos, a.publish_FP, a.estado as idestado, c.estado as estado, a.nome as nome, a.ano as ano, a.data_criacao as data_criacao";
                    $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                    $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                    $table .= " FROM ".$dbprefix."virtualdesk_Multimedia as a";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Multimedia_Categoria AS b ON b.id = a.categoria";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Multimedia_Estado AS c ON c.id_estado = a.estado";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'referencia',    'dt' => 0 ),
                        array( 'db' => 'nome',          'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'ano',           'dt' => 3 ),
                        array( 'db' => 'creditos',      'dt' => 4 ),
                        array( 'db' => 'publish_FP',    'dt' => 5 ),
                        array( 'db' => 'estado',        'dt' => 6 ),
                        array( 'db' => 'dummy',         'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'multimedia_id', 'dt' => 8 ),
                        array( 'db' => 'idcategoria',   'dt' => 9 ),
                        array( 'db' => 'idestado',      'dt' => 10 ),
                        array( 'db' => 'data_criacao',  'dt' => 11 ),
                        array( 'db' => 'SwitchSetHRef', 'dt' => 12 ),
                        array( 'db' => 'SwitchGetHRef', 'dt' => 13 ),
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function setPluginEnableState($UserJoomlaID, $getInputVideo_id, $setId2Enable)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
            if($vbInAdminGroup===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
                return false;
            }
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('multimedia');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('multimedia', 'pluginedit'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($UserVDId === false)  return false;

            $data = array();
            $configadmin_Video_id = $getInputVideo_id;
            if((int) $configadmin_Video_id <=0 ) return false;
            if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

            $data['id']     = $configadmin_Video_id;
            $data['publish_FP'] = $setId2Enable;


            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableMultimedia($db);
            $Table->load(array('id'=>$configadmin_Video_id));

            try {
                // Bind the data.
                if (!$Table->bind($data)) return false;

                // Store the data.
                if (!$Table->save($data)) return false;


                // Foi alterado
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_MULTIMEDIA_UPDATED');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_MULTIMEDIA_UPDATED') . 'id=' . $data['id'];
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_MULTIMEDIA_EVENTLOG');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function checkMultimediaEnableVal($getInputVideo_id)
        {
            if( empty($getInputVideo_id) )  return false;

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
            if($vbInAdminGroup===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
                return false;
            }
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('multimedia');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('multimedia', 'pluginedit'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDbo();
                $db->setQuery("Select publish_FP From #__virtualdesk_Multimedia Where id =" . $db->escape($getInputVideo_id));
                $res =  $db->loadResult();

                return($res);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getMultimediaDetail4Manager($id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('multimedia');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('multimedia', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('multimedia'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('multimedia','edit4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as multimedia_id', 'a.referencia as referencia', 'a.nome as nome', 'a.link as link', 'a.creditos', 'b.estado as estado', 'a.estado as id_estado', 'c.categoria as categoria', 'a.categoria as id_categoria', 'a.ano', 'a.data_criacao', 'a.data_alteracao', 'a.data_publicacao'))
                    ->join('LEFT', '#__virtualdesk_Multimedia_Estado AS b ON b.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Multimedia_Categoria AS c ON c.id = a.categoria')
                    ->from("#__virtualdesk_Multimedia as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('multimedia');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('multimedia', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $multimedia_id = $data['multimedia_id'];
            if((int) $multimedia_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableMultimedia($db);
            $Table->load(array('id'=>$multimedia_id));

            $db->transactionStart();

            try {
                $referencia          = $Table->referencia;
                $obParam    = new VirtualDeskSiteParamsHelper();

                $nome = null;
                $link = null;
                $categoria = null;
                $creditos = null;
                $ano = null;
                $estado = null;
                $data_publicacao = null;

                if(array_key_exists('nomeVideo',$data))         $nomeVideo = $data['nomeVideo'];
                if(array_key_exists('linkVideo',$data))         $linkVideo = $data['linkVideo'];
                if(array_key_exists('creditos',$data))          $creditos = $data['creditos'];
                if(array_key_exists('categoria',$data))         $categoria = $data['categoria'];
                if(array_key_exists('estado',$data))            $estado = $data['estado'];
                if(array_key_exists('data_publicacao',$data)){
                    $data_publicacao = $data['data_publicacao'];
                    $explodePubData = explode('-', $data_publicacao);
                    $ano = $explodePubData[0];
                }


                /* BEGIN Save Multimedia */
                $resSaveMultimedia = self::saveEditedMultimedia4Manager($multimedia_id, $nomeVideo, $linkVideo, $creditos, $categoria, $ano, $estado, $data_publicacao);
                /* END Save Multimedia */

                if (empty($resSaveMultimedia)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objCapaFiles                = new VirtualDeskSiteMultimediaCapaFilesHelper();
                $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_capa');
                $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
                if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o ficheiros de Capa', 'error');
                }

                // Insere os NOVOS ficheiros
                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteMultimediaCapaFilesHelper();
                $objCapaFiles->tagprocesso   = 'MULTIMEDIA_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

                if ($resFileSaveCapa===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Multimedia_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_MULTMEDIA_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_MULTMEDIA_EVENTLOG_CREATED') . 'ref=' . $multimedia_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_MULTMEDIA_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedMultimedia4Manager($multimedia_id, $nome, $link, $creditos, $categoria, $ano, $estado, $data_publicacao)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('multimedia');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('multimedia', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($nome))             array_push($fields, 'nome="'.$db->escape($nome).'"');
            if(!is_null($link))             array_push($fields, 'link="'.$db->escape($link).'"');
            if(!is_null($creditos))         array_push($fields, 'creditos="'.$db->escape($creditos).'"');
            if(!is_null($categoria))        array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($estado))           array_push($fields, 'estado="'.$db->escape($estado).'"');
            if(!is_null($data_publicacao))  {
                array_push($fields, 'data_publicacao="'.$db->escape($data_publicacao).'"');
                array_push($fields, 'ano="'.$db->escape($ano).'"');
            }


            $data_alteracao = date("Y-m-d H:i:s");

            array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Multimedia')
                ->set($fields)
                ->where(' id = '.$db->escape($multimedia_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function getVideosFP($limitVideosFP){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('referencia, link, nome'))
                ->from("#__virtualdesk_Multimedia")
                ->where($db->quoteName('publish_FP') . '=' . $db->escape('1'))
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('nome ASC')
                ->setLimit($limitVideosFP)
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getListVideos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('referencia, link, nome, data_criacao'))
                ->from("#__virtualdesk_Multimedia")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('data_criacao DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getListVideosFiltered($pesquisaLivre, $categoria, $ano){
            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $where .= 'AND' . $db->quoteName('a.nome') . "LIKE '" . $db->escape($nomeEvento) . "'";
            }

            if(!empty($categoria)){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($ano)){
                $where .= 'AND' . $db->quoteName('a.ano') . "='" . $db->escape($ano) . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.link, a.nome, a.data_criacao'))
                ->from("#__virtualdesk_Multimedia as a")
                ->where($where)
                ->order('data_criacao DESC')
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_Multimedia_FotoCapa as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4manager.multimedia.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.multimedia.data', null);
            $app->setUserState('com_virtualdesk.list4manager.multimedia.data', null);
            $app->setUserState('com_virtualdesk.view4manager.multimedia.data', null);
        }
    }
?>