<?php
/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


class VirtualDeskSiteEnterpriseFieldsHelper
{
    var $arConfFieldNames = array();
    var $arEnterpriseFieldsConfig     = array();
    var $arEmptyFieldsError           = array();

    public function VirtualDeskSiteEnterpriseFieldsHelper(){

        $this->arConfFieldNames =  array( 'address'=>'ADDRESS',
            'postalcode'=>'POSTALCODE',
            'phone1'=>'PHONE1',
            'phone2'=>'PHONE2',
            'website'=>'WEBSITE',
            'facebook'=>'FACEBOOK',
            'freguesia'=>'FREGUESIA',
            'localidade'=>'LOCALIDADE',
            'areaact'=>'AREAACT',
            'attachment'=>'ATTACHMENT'
        );

        $this->setFields();
    }

    public function setFields()
    {
        foreach($this->arConfFieldNames as $key => $val)
        {
            // Field : visible
            $this->arEnterpriseFieldsConfig['EnterpriseField_' . $val]  = false;
            if(JComponentHelper::getParams('com_virtualdesk')->get('enterprisefield_' . $key) == 1 ) $this->arEnterpriseFieldsConfig['EnterpriseField_' . $val] = true;

            // Field: obrigatório
            $this->arEnterpriseFieldsConfig['EnterpriseField_' . $val. '_Required']    = "";
            $setEnterprisefield_morada_required = JComponentHelper::getParams('com_virtualdesk')->get('enterprisefield_' .$key. '_required');
            if(JComponentHelper::getParams('com_virtualdesk')->get('enterprisefield_'.$key.'_required') == 1 ) $this->arEnterpriseFieldsConfig['EnterpriseField_' . $val. '_Required'] = 'required';
        }

    }


    public function getFields()
    {
        return $this->arEnterpriseFieldsConfig;
    }


    /*
    * Verifica se os campos recebidos se estão vazios e nesse caso não inicializa o array de dados.
    * Ou se são obrigatórios nesse caso não podem vir vazios.
    *
    */
    public function checkCamposEmpty($arCamposGet, $data)
    {
        foreach($this->arConfFieldNames as $key => $val)
        {
            if ($this->arEnterpriseFieldsConfig['EnterpriseField_' . $val]) {
                if ($this->arEnterpriseFieldsConfig['EnterpriseField_' . $val . '_Required']) {
                    if (!empty($arCamposGet[$key])) {
                        $data[$key] = $arCamposGet[$key];
                    }
                    else {
                        // é obrigatório e está vazio... Marca como estando com erro
                        $this->arEmptyFieldsError[$key] = true;
                    }
                } else { // Se não é obrigatório pode ir vazio
                    $data[$key] = $arCamposGet[$key];
                }
            }
        }





        return($data);
    }


    public function checkRequiredEmptyFields()
    {
        $msg ="";

        foreach($this->arConfFieldNames as $key => $val) {
            if( $this->arEmptyFieldsError[$key] )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_ENTERPRISE_' . $val . '_LABEL');
        }

        return($msg);
    }
    

}
