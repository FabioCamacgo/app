<?php

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteSuporteFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte_files.php');
JLoader::register('VirtualDeskTableSuporte', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/suporte.php');
JLoader::register('VirtualDeskTableSuporteEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/suporte_estado_historico.php');


    class VirtualDeskSiteSuporteHelper
    {
        const tagchaveModulo = 'suporte';



        public static function RefEvent($fiscalid, $startDay, $startMonth, $endDay, $endMonth)
        {

            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);

            $refer = 'N' . $fiscalid . 'SD' . $startDay . 'SM' . $startMonth . $randomString . 'ED' . $endDay . 'EM' . $endMonth;

            return $refer;
        }


        public static function getDepartamento(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            }
            else {
                $Name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $Name  . ' as departamento'))
                ->from("#__virtualdesk_suporte_departamento as a")
                ->order('departamento ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDepartamentoName($idDepart){
            $data = array();
            if((int)$idDepart <=0 ) return ($data);

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            }
            else {
                $Name = 'nome_EN';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $Name  . ' as departamento'))
                ->from("#__virtualdesk_suporte_departamento as a")
                ->where('a.id = '.$db->escape($idDepart))
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAssunto(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            }
            else {
                $Name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $Name  . ' as assunto'))
                ->from("#__virtualdesk_suporte_assunto as a")
                ->order('assunto ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getAssuntoName($idAssunto){
            $data = array();
            if((int)$idAssunto <=0 ) return ($data);

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            }
            else {
                $Name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $Name  . ' as assunto'))
                ->from("#__virtualdesk_suporte_assunto as a")
                ->where('a.id = '.$db->escape($idAssunto))
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function checkValidarNIF($nif) {

            $nif=trim($nif);
            $ignoreFirst=true;
            $errNif = 0;

            if (!is_numeric($nif) || strlen($nif)!=9) {
                $errNif = 1;
            } else {
                $nifSplit=str_split($nif);
                if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);

                    if($checkDigit>=10) $checkDigit=0;

                    if ($checkDigit==$nifSplit[8]) {

                    } else {
                        $errNif = 1;
                    }
                } else {
                    $errNif = 1;
                }
            }

            return($errNif);

        }


        public static function getUserEmail($fiscalid)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_Suporte_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getUserName($fiscalid)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome')
                ->from("#__virtualdesk_Suporte_Users")
                ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function SendMailAdmin($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $Assunto, $nameUser, $email, $referencia='')
        {

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Suporte_NovoAdmin_Email.html');

            $data['fromname'] = $config->get('fromname');
            // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE     = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOADMIN_TITULO');
            $BODY_TITLE2    = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOADMIN_TITULO');
            $BODY_GREETING  = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOADMIN_CORPO', $Assunto, $nameUser . " ( " . $email . " ) " );
            $BODY_FILES_LIST_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_FILES_LIST_TITLE');


            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_FILES_LIST_TITLE",$BODY_FILES_LIST_TITLE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Lista de ficheiros para downlaod
            if(empty($referencia)===false) {
                $objEventFile = new VirtualDeskSiteSuporteFilesHelper();
                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                $FileList21Html = '';
                foreach ($arFileList as $rowFile) {
                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                }
                $body = str_replace("%BODY_FILES_LIST", $FileList21Html, $body);
            }


            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom']);
            $newEmail->setSender($data['mailfrom']);
            $newEmail->setFrom($data['fromname']);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

            $newEmail->setSubject($data['sitename']);
            $newEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Suporte_BannerEmail.png', "banner", "Suporte__BannerEmail.png");


            $return = (boolean)$newEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailAdminUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $Assunto, $nameUser, $email, $referencia='')
        {

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Suporte_NovoAdmin_Email.html');

            $data['fromname'] = $config->get('fromname');
            // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATEADMIN_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATEADMIN_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATEADMIN_CORPO', $Assunto, $nameUser . " ( " . $email . " ) " );
            $BODY_FILES_LIST_TITLE       = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_FILES_LIST_TITLE');


            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_FILES_LIST_TITLE",$BODY_FILES_LIST_TITLE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Lista de ficheiros para downlaod
            if(empty($referencia)===false) {
                $objEventFile = new VirtualDeskSiteSuporteFilesHelper();
                $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
                $FileList21Html = '';
                foreach ($arFileList as $rowFile) {
                    $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
                }
                $body = str_replace("%BODY_FILES_LIST", $FileList21Html, $body);
            }


            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom']);
            $newEmail->setSender($data['mailfrom']);
            $newEmail->setFrom($data['fromname']);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

            $newEmail->setSubject($data['sitename']);
            $newEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Suporte_BannerEmail.png', "banner", "Suporte_BannerEmail.png");


            $return = (boolean)$newEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailAdminAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $Assunto, $nameUser, $email, $referencia='',$Estado)
        {

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Suporte_NovoAdmin_Email.html');

            $data['fromname'] = $config->get('fromname');
            // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_ALTESTADOADMIN_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_ALTESTADOADMIN_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_ALTESTADOADMIN_CORPO', $Estado, $Assunto, $referencia);


            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_FILES_LIST_TITLE", '', $body);
            $body = str_replace("%BODY_FILES_LIST", '', $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom']);
            $newEmail->setSender($data['mailfrom']);
            $newEmail->setFrom($data['fromname']);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

            $newEmail->setSubject($data['sitename']);
            $newEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Suporte_BannerEmail.png', "banner", "Suporte_BannerEmail.png");


            $return = (boolean)$newEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailClient($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Suporte_NovoClient_Email.html');

            $data['fromname'] = $config->get('fromname');
            // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOCLIENT_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOCLIENT_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOCLIENT_INTRO', $nameUser . " ( " . $emailUser . " ) ", $copyrightAPP);

            $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOCLIENT_ASSUNTO');
            $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOCLIENT_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
            $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_NOVOCLIENT_DEPARTAMENTO');
            $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);
            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Suporte_BannerEmail.png', "banner", "Suporte_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailClientUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento)
        {

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Suporte_NovoClient_Email.html');

            $data['fromname'] = $config->get('fromname');
            // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');


            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATECLIENT_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATECLIENT_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATECLIENT_INTRO', $nameUser . " ( " . $emailUser . " ) ", $nomeMunicipio);

            $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATECLIENT_ASSUNTO');
            $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATECLIENT_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
            $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_UPDATECLIENT_DEPArTAMENTO');
            $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);

            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);

            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);



            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Suporte_BannerEmail.png', "banner", "Suporte_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailClientAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento, $Estado, $referencia)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Suporte_NovoClient_Email.html');

            $data['fromname'] = $config->get('fromname');
            // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');


            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_SUPORTE_EMAIL_ALTESTADOCLIENT_CORPO', $Estado, $Assunto, $referencia);

            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);




            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Suporte_BannerEmail.png', "banner", "Suporte_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        /* Define qual o  email por defeito do manager, se não existir devolve o do Admin */
        public static function getNameAndEmailDoManagerEAdmin(&$emailManager, &$emailAdmin)
        {
            $config                      = JFactory::getConfig();
            $objVDParams                 = new VirtualDeskSiteParamsHelper();
            $MailManagerbyDefault  = $objVDParams->getParamsByTag('Suporte_Mail_Manager_by_Default');
            $MailAdminbyDefault    = $objVDParams->getParamsByTag('mailAdminSuporte');
            $adminSiteDefaultEmail       = $config->get('mailfrom');

            // Verifica se tem email do mananger DE suporte caso contrário fica o admin do site
            $emailManager = $MailManagerbyDefault;
            if(empty($emailManager)) {
                $emailManager = $MailAdminbyDefault;
                if(empty($emailManager)) {
                    $emailManager =  $adminSiteDefaultEmail;
                }
            }

            // Verifica se tem email do admin do alerta caso contrário fica o admin do site
            $emailAdmin = $MailAdminbyDefault;
            if(empty($emailAdmin)) {
                $emailAdmin = $adminSiteDefaultEmail;
            }
            return(true);
        }


        /* Carrega lista com dados de todos os eventos (acesso ao USER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getSuporteList4User ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('suporte', 'list4users');
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $DepartName = 'nome_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $DepartName = 'nome_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=view4user&suporte_id=');

                    $table  = " ( SELECT a.id as id, a.id as suporte_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.assunto as assunto, a.descricao as descricao , a.observacoes as observacoes";
                    $table .= " , IFNULL(b.".$DepartName.",' ') as departamento , IFNULL(e.".$EstadoName.", ' ') as estado, a.id_estado as idestado , a.id_departamento as iddepartamento";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_suporte as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_departamento AS b ON b.id = a.id_departamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_estado AS e ON e.id = a.id_estado ";
                    $table .= " WHERE (a.nif=$UserSessionNIF) ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'created',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'assunto',       'dt' => 2 ),
                        array( 'db' => 'departamento',  'dt' => 3 ),
                        array( 'db' => 'estado',        'dt' => 4 ),
                        array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 6 ),
                        array( 'db' => 'id',            'dt' => 7 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 8 ),
                        array( 'db' => 'createdFull',   'dt' => 9 ),
                        array( 'db' => 'descricao',     'dt' => 10 ),
                        array( 'db' => 'iddepartamento','dt' => 11 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();


                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com dados de todos os eventos (acesso ao ADMIN). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getSuporteList4Admin ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('suporte', 'list4admins');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdmin(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('suporte','list4admins');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $DepartName = 'nome_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $DepartName = 'nome_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=view4admin&suporte_id=');

                    $table  = " ( SELECT a.id as id, a.id as suporte_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.assunto as assunto, a.descricao as descricao , a.observacoes as observacoes";
                    $table .= " , IFNULL(b.".$DepartName.",' ') as departamento , IFNULL(e.".$EstadoName.", ' ') as estado, a.id_estado as idestado , a.id_departamento as iddepartamento";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_suporte as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_departamento AS b ON b.id = a.id_departamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_estado AS e ON e.id = a.id_estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'created',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'assunto',       'dt' => 2 ),
                        array( 'db' => 'departamento',  'dt' => 3 ),
                        array( 'db' => 'estado',        'dt' => 4 ),
                        array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 6 ),
                        array( 'db' => 'id',            'dt' => 7 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 8 ),
                        array( 'db' => 'createdFull',   'dt' => 9 ),
                        array( 'db' => 'descricao',     'dt' => 10 ),
                        array( 'db' => 'iddepartamento','dt' => 11 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();


                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com dados de todos os eventos (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getSuporteList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('suporte', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            // $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('suporte','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $DepartName = 'nome_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $DepartName = 'nome_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {



                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=view4manager&suporte_id=');

                    $table  = " ( SELECT a.id as id, a.id as suporte_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.assunto as assunto, a.descricao as descricao , a.observacoes as observacoes";
                    $table .= " , IFNULL(b.".$DepartName.",' ') as departamento , IFNULL(e.".$EstadoName.", ' ') as estado, a.id_estado as idestado , a.id_departamento as iddepartamento";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_suporte as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_departamento AS b ON b.id = a.id_departamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_estado AS e ON e.id = a.id_estado ";

                    $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('suporte','list4managers');
                    if($vbHasReadAll===false) {
                        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
                        $table .= " WHERE (a.nif=$UserSessionNIF) ";
                    }

                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'created',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'assunto',       'dt' => 2 ),
                        array( 'db' => 'departamento',  'dt' => 3 ),
                        array( 'db' => 'estado',        'dt' => 4 ),
                        array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 6 ),
                        array( 'db' => 'id',            'dt' => 7 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 8 ),
                        array( 'db' => 'createdFull',   'dt' => 9 ),
                        array( 'db' => 'descricao',     'dt' => 10 ),
                        array( 'db' => 'iddepartamento','dt' => 11 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();


                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com os TODOS os estados */
        public static function getSuporteEstadoAllOptions ($lang, $displayPermError=true)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
            // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('suporte');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id as id','estado_PT as name_pt','estado_EN as name_en') )
                    ->from("#__virtualdesk_suporte_estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $rowName
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getDepartamentoAllOptions(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            }
            else {
                $Name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $Name  . ' as departamento'))
                ->from("#__virtualdesk_suporte_departamento as a")
                ->order('departamento ASC')
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->departamento;
                }
                else {
                    $rowName = $row->departamento;
                }
                $response[] = array(
                    'id' => $row->id,
                    'name' => $rowName
                );

            }
            return ($response);
        }


        /* Devolve CSS a ser aplicado de acordo com o ESTADO atual */
        public static function getSuporteEstadoCSS ($idestado)
        {

            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                    // pendente
                    $defCss = 'label-pendente ';
                break;
                case '2':
                    // ema nalise
                    $defCss = 'label-emanalise';
                    break;
                case '3':
                    // ema nalise
                    $defCss = 'label-emresolucao';
                    break;
                case '4':
                    // concluído
                    $defCss = 'label-concluido';
                    break;
            }
            return ($defCss);
        }


        /* Carrega dados visualização do suporte para o USER */
        public static function getSuporteView4UserDetail ($IdSuporte)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('suporte');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdSuporte) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            // Current Session User...
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $DepartName = 'nome_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $DepartName = 'nome_EN';
                    $EstadoName = 'estado_EN';
                }

                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as suporte_id', 'a.referencia as codigo', 'a.descricao as descricao', 'a.assunto as assunto', 'a.data_criacao', 'a.data_alteracao'
                    , "IFNULL(b.".$DepartName.",'') as departamento", "IFNULL(e.".$EstadoName.", ' ') as estado", 'a.id_estado as idestado'
                    ,  'a.iduser as iduser' , 'a.nif as nif' , 'a.observacoes as observacoes' , 'a.id_departamento as id_departamento'
                    ))
                    ->join('LEFT', '#__virtualdesk_suporte_departamento AS b ON b.id = a.id_departamento')
                    ->join('LEFT', '#__virtualdesk_suporte_estado AS e ON e.id = a.id_estado')
                    ->from("#__virtualdesk_suporte as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdSuporte) . ' and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega dados visualização do suporte para o ADMIN */
        public static function getSuporteView4AdminDetail ($IdSuporte)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('suporte');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('suporte', 'view4admins'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('suporte'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('suporte','view4admins'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdSuporte) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            // Current Session User...
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $DepartName = 'nome_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $DepartName = 'nome_EN';
                    $EstadoName = 'estado_EN';
                }

                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as suporte_id', 'a.referencia as codigo', 'a.descricao as descricao', 'a.assunto as assunto', 'a.data_criacao', 'a.data_alteracao'
                    , "IFNULL(b.".$DepartName.",'') as departamento", "IFNULL(e.".$EstadoName.", ' ') as estado", 'a.id_estado as idestado'
                    ,  'a.iduser as iduser' , 'a.nif as nif' , 'a.observacoes as observacoes' , 'a.id_departamento as id_departamento'
                    ))
                    ->join('LEFT', '#__virtualdesk_suporte_departamento AS b ON b.id = a.id_departamento')
                    ->join('LEFT', '#__virtualdesk_suporte_estado AS e ON e.id = a.id_estado')
                    ->from("#__virtualdesk_suporte as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdSuporte)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados visualização do suporte para o MANAGER */
        public static function getSuporteView4ManagerDetail ($IdSuporte)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('suporte');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('suporte', 'view4managers'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdSuporte) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            // Current Session User...
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $DepartName = 'nome_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $DepartName = 'nome_EN';
                    $EstadoName = 'estado_EN';
                }

                $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('suporte','view4managers'); // Verifica permissões READALL no layout
                $FilterOnlyUserSession = '';
                if($vbHasReadAll===false) {
                    $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
                    $FilterOnlyUserSession .= " AND (a.nif=$UserSessionNIF) ";
                }

                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as suporte_id', 'a.referencia as codigo', 'a.descricao as descricao', 'a.assunto as assunto', 'a.data_criacao', 'a.data_alteracao'
                    , "IFNULL(b.".$DepartName.",'') as departamento", "IFNULL(e.".$EstadoName.", ' ') as estado", 'a.id_estado as idestado'
                    ,  'a.iduser as iduser' , 'a.nif as nif' , 'a.observacoes as observacoes' , 'a.id_departamento as id_departamento'
                    ))
                    ->join('LEFT', '#__virtualdesk_suporte_departamento AS b ON b.id = a.id_departamento')
                    ->join('LEFT', '#__virtualdesk_suporte_estado AS e ON e.id = a.id_estado')
                    ->from("#__virtualdesk_suporte as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdSuporte) .  $FilterOnlyUserSession  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega dados do Promotor do Evento: pesquisa nos USERS VD e carrega o Nome e o EMail se não existir vai aos USERS da Suporte */
        public static function getSuportePromotorByNIF4Manager ($nif_evento)
        {
            if((int) $nif_evento<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('suporte');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('suporte', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('suporte'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('suporte','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $dataPromotor = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($nif_evento);

                if( (!empty($dataPromotor)) && ($dataPromotor!=false) )
                {
                    $dataReturn = new stdClass();
                    $dataReturn->nome  = $dataPromotor->nome;
                    $dataReturn->nif   = $dataPromotor->nif;
                    $dataReturn->email = $dataPromotor->email;
                    return($dataReturn);
                }
                else {
                    $db = JFactory::getDBO();
                    $db->setQuery($db->getQuery(true)
                        ->select(array('nif, nome, email'))
                        ->from("#__virtualdesk_Suporte_Users")
                        ->where($db->quoteName('nif') . "=" . $db->escape($nif_evento) )
                    );
                    $dataReturn = $db->loadObject();
                    if(empty($dataReturn)) return false;
                    return($dataReturn);
                }
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados do Promotor do Evento: pesquisa nos USERS VD e carrega o Nome e o EMail se não existir vai aos USERS da Suporte */
        public static function getSuportePromotorByNIF4User ($nif_evento)
        {
            if((int) $nif_evento<=0) return false;

            /*
           * Check PERMISSÕES
           */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('suporte');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if( (int)$nif_evento != (int)$UserSessionNIF ) return false;

            try
            {
                $dataPromotor = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($nif_evento);

                if( (!empty($dataPromotor)) && ($dataPromotor!=false) )
                {
                    $dataReturn = new stdClass();
                    $dataReturn->nome  = $dataPromotor->name;
                    $dataReturn->nif   = $dataPromotor->nif;
                    $userfield_login_type = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
                    if( $userfield_login_type == 'login_as_nif' ) $dataReturn->nif = $dataPromotor->login;
                    $dataReturn->email = $dataPromotor->email;
                    return($dataReturn);
                }
                else {
                    $db = JFactory::getDBO();
                    $db->setQuery($db->getQuery(true)
                        ->select(array('nif, nome, email'))
                        ->from("#__virtualdesk_Suporte_Users")
                        ->where($db->quoteName('nif') . "=" . $db->escape($nif_evento) )
                    );
                    $dataReturn = $db->loadObject();
                    if(empty($dataReturn)) return false;
                    return($dataReturn);
                }
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega dados (objecto) do ESTADO atual do processo do suporte em questão */
        public static function getSuporteEstadoAtualObjectByIdSuporte ($lang, $id_suporte)
        {
            if((int) $id_suporte<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('suporte');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('b.id as id_estado','b.estado_PT as estado_PT','b.estado_EN as estado_EN') )
                    ->join('LEFT', '#__virtualdesk_suporte_estado AS b ON b.id = a.id_estado')
                    ->from("#__virtualdesk_suporte as a")
                    ->where(" a.id = ". $db->escape($id_suporte))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                $obj= new stdClass();
                $obj->id_estado =  $dataReturn->id_estado;
                if( empty($lang) or ($lang='pt_PT') ) {
                    $obj->name = $dataReturn->estado_PT;
                }
                else {
                    $obj->name  = $dataReturn->estado_EN;
                }

                $obj->cssClass = self::getSuporteEstadoCSS($dataReturn->id_estado);

                return($obj);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Grava dados para definir um novo ESTADO do processo de Suporte  */
        public static function saveAlterar2NewEstado4AdminByAjax($getInputSuporte_id, $NewEstadoId, $NewEstadoDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('suporte');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'edit4admins'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdmin(); // Verifica se está no grupo Manager ou Admin da APP
            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('suporte', 'alterarestado4admins'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewEstadoId) ) return false;
            if((int) $getInputSuporte_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            $data = array();
            $data['id_estado'] = $NewEstadoId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();

            $Table = new VirtualDeskTableSuporte($db);
            $Table->load(array('id'=>$getInputSuporte_id));

            // Só alterar se o estado for diferente
            if((int)$Table->id_estado == (int)$NewEstadoId )  return true;

            if(!empty($data)) {
                $dateModified = new DateTime();
                $data['data_alteracao'] = $dateModified->format('Y-m-d H:i:s');
            }

            $db->transactionStart();

            try {
                //$data['id_evento'] = $Table->id_evento;
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTableSuporteEstadoHistorico($db);
                $dataHist = array();
                $dataHist['id_estado']  = $NewEstadoId;
                $dataHist['id_ticket']  = $getInputSuporte_id;
                $dataHist['descricao']  = $NewEstadoDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                /* Send Email - Para Admin, Manager e User */
                // Carrega dados atuais na base de dados
                $lang = VirtualDeskHelper::getLanguageTag();
                $ObjEstado  = self::getSuporteEstadoAtualObjectByIdSuporte($lang, $Table->id_estado);
                $EstadoNome = $ObjEstado->name;
                self::SendMailAdminAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $Table->assunto, $objUserVD->name, $objUserVD->email, $Table->referencia, $EstadoNome);
                $DepartName   = self::getDepartamentoName($Table->id_departamento);
                self::SendMailClientAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserVD->name, $Table->assunto,$Table->descricao, $objUserVD->email, $DepartName, $EstadoNome,$Table->referencia);
                /* END Send Email */

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_suporte=' . $dataHist['id_suporte'];
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Admin();
                    $eventdata['ref']         = $Table->referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Grava dados para definir um novo ESTADO do processo de Suporte  */
        public static function saveAlterar2NewEstado4ManagerByAjax($getInputSuporte_id, $NewEstadoId, $NewEstadoDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('suporte');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('suporte', 'alterarestado4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewEstadoId) ) return false;
            if((int) $getInputSuporte_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            $data = array();
            $data['id_estado'] = $NewEstadoId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();

            $Table = new VirtualDeskTableSuporte($db);
            $Table->load(array('id'=>$getInputSuporte_id));

            // Só alterar se o estado for diferente
            if((int)$Table->id_estado == (int)$NewEstadoId )  return true;

            if(!empty($data)) {
                $dateModified = new DateTime();
                $data['data_alteracao'] = $dateModified->format('Y-m-d H:i:s');
            }

            $db->transactionStart();

            try {
                //$data['id_evento'] = $Table->id_evento;
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTableSuporteEstadoHistorico($db);
                $dataHist = array();
                $dataHist['id_estado']  = $NewEstadoId;
                $dataHist['id_ticket']  = $getInputSuporte_id;
                $dataHist['descricao']  = $NewEstadoDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                /* Send Email - Para Admin, Manager e User */
                // Carrega dados atuais na base de dados
                $lang = VirtualDeskHelper::getLanguageTag();
                $ObjEstado  = self::getSuporteEstadoAtualObjectByIdSuporte($lang, $Table->id_estado);
                $EstadoNome = $ObjEstado->name;
                self::SendMailAdminAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $Table->assunto, $objUserVD->name, $objUserVD->email, $Table->referencia, $EstadoNome);
                $DepartName   = self::getDepartamentoName($Table->id_departamento);
                self::SendMailClientAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserVD->name, $Table->assunto,$Table->descricao, $objUserVD->email, $DepartName, $EstadoNome,$Table->referencia);
                /* END Send Email */

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_suporte=' . $dataHist['id_suporte'];
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $Table->referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Retorna a Ref Id apenas se o User atual da sessão tem o mesmo NIF da ocorrência
               Útil para saber se o user tem acesso a este suporte...
            */
        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_suporte as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /*Gera código aleatório para a referência da ocorrencia*/
        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 12);
        }


        public static function gerarReferenciaNova ()
        {
            $referencia = '';
            $refExiste  = 1;
            $year       = substr(date("Y"), -2);
            while($refExiste == 1){
                $refRandom  = self::random_code();
                $referencia = $year . strtoupper($refRandom);
                $checkREF = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            return($referencia);
        }


        /*Verifica de a referencia da ocorrencia existe*/
        public static function CheckReferencia($ref){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_suporte")
                ->where($db->quoteName('referencia') . "='" . $db->escape($ref) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getUsersListAtiveNotBlocked4Admin ()
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('suporte', 'list4admins');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdmin(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('suporte','list4admins');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $conf = JFactory::getConfig();
                $db = JFactory::getDBO();
                $dbprefix = $conf->get('dbprefix');

                $sql = "SELECT distinct s1.iduser, s2.id as id, s2.name as name from (";
                $sql.= "SELECT distinct c.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_usersaction c on c.idpermaction = b.id ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and c.iduser is not null ";
                $sql.= " union ";
                $sql.= " SELECT distinct d.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_groupsaction c on c.idpermaction = b.id ";
                $sql.= " left JOIN ".$dbprefix."virtualdesk_perm_groupsusers d on d.idpermgroup = c.idpermgroup ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and d.iduser is not null ";
                $sql.= " ) as s1 left join ".$dbprefix."virtualdesk_users as s2 on s1.iduser=s2.id ";
                $sql.= " WHERE s2.blocked=0 and s2.activated=1";

                $db->setQuery($sql);

                $dataReturn = $db->loadObjectList();
                return($dataReturn);

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getUsersListAtiveNotBlocked ()
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('suporte', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('suporte','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.name as name', 'a.id as iduser', 'a.idjos as idjoomla', 'a.email as email', 'a.name as nomeuser', 'a.login as login', 'a.blocked as blocked', 'a.activated as activated' ))
                    ->from("#__virtualdesk_users as a")
                    ->where('a.blocked =0 and activated=1'));
                $dataReturn = $db->loadObjectList();
                return($dataReturn);

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Cria novo Evento para o ecrã/permissões do USER */
        public static function create4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('suporte');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;

            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;
            $NIFUserVD = VirtualDeskSiteUserHelper::getFiscalNumberFromVDUser($UserVDId);
            if ((int) $NIFUserVD <=0) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableSuporte($db);

            $db->transactionStart();

            try {

                $EstadoIdInicio = (int)self::getEstadoIdInicio();

                // Prepara dados a inserir...
                $data['iduser'] = $UserVDId;
                $data['nif']    = $NIFUserVD;
                $data['id_departamento'] = $data['departamento'];
                $data['id_estado'] = $EstadoIdInicio;
                $data['balcao'] = 0;
                unset($data['departamento']);

                if(!empty($data['descricao'])) $data['descricao']      = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao']), true);
                if(!empty($data['observacoes'])) $data['observacoes']  = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                /* Gerar REFERÊNCIA UNICA de suporte*/
                $RefNew = self::gerarReferenciaNova();
                $data['referencia'] = $RefNew;

                /* Save : START*/
                if (!$Table->save($data))  {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $objFiles->tagprocesso   = 'SUPORTE_POST';
                $objFiles->idprocesso    = $RefNew;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false ) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams      = new VirtualDeskSiteParamsHelper();

                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                /* Send Email - Para Admin, Manager e User */
                self::SendMailAdmin($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['assunto'], $objUserVD->name, $objUserVD->email, $RefNew);
                $DepartName   = self::getDepartamentoName($data['id_departamento']);
                self::SendMailClient($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserVD->name, $data['assunto'], $data['descricao'], $objUserVD->email, $DepartName);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_CREATED') . 'ref=' . $RefNew;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $RefNew;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        /* Cria novo  para o ecrã/permissões do ADMIN */
        public static function create4Admin($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('suporte');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'addnew4admins'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdmin(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $utilizador = $data['utilizador'];
            if ((int) $utilizador <=0) return false;
            /* Valida Nif de Utilizador a ser associado*/
            $NIFUserVD = VirtualDeskSiteUserHelper::getFiscalNumberFromVDUser($utilizador);
            if ((int) $NIFUserVD <=0) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableSuporte($db);

            $db->transactionStart();

            try {

                $EstadoIdInicio = (int)self::getEstadoIdInicio();

                // Prepara dados a inserir...
                $data['iduser'] = $data['utilizador'];
                $data['id_departamento'] = $data['departamento'];
                $data['id_estado'] = $EstadoIdInicio;
                $data['balcao'] = 1;
                $data['nif']    = $NIFUserVD;
                unset($data['utilizador']);
                unset($data['departamento']);

                if(!empty($data['descricao'])) $data['descricao']      = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao']), true);
                if(!empty($data['observacoes'])) $data['observacoes']  = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                /* Gerar REFERÊNCIA UNICA de suporte*/
                $RefNew = self::gerarReferenciaNova();
                $data['referencia'] = $RefNew;

                /* Save : START*/
                if (!$Table->save($data))  {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $objFiles->tagprocesso   = 'SUPORTE_POST';
                $objFiles->idprocesso    = $RefNew;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false ) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams      = new VirtualDeskSiteParamsHelper();

                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                /* Send Email - Para Admin, Manager e User */
                $objUserTicket=  VirtualDeskSiteUserHelper::getUserObjById($data['iduser']);
                self::SendMailAdmin($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['assunto'], $objUserTicket->name, $objUserTicket->email, $RefNew);
                $DepartName   = self::getDepartamentoName($data['id_departamento']);
                self::SendMailClient($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserTicket->name, $data['assunto'], $data['descricao'], $objUserTicket->email, $DepartName);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($RefNew);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_CREATED') . 'ref=' . $RefNew;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Admin();
                    $eventdata['ref']         = $RefNew;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        /* Cria novo  para o ecrã/permissões do MANAGER */
        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('suporte');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }



            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;
            $NIFUserVD = VirtualDeskSiteUserHelper::getFiscalNumberFromVDUser($UserVDId);
            if ((int) $NIFUserVD <=0) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableSuporte($db);

            $db->transactionStart();

            try {

                $EstadoIdInicio = (int)self::getEstadoIdInicio();

                // Prepara dados a inserir...
                $data['iduser'] = $UserVDId;
                $data['nif']    = $NIFUserVD;
                $data['id_departamento'] = $data['departamento'];
                $data['id_estado'] = $EstadoIdInicio;
                $data['balcao'] = 0;
                unset($data['departamento']);

                if(!empty($data['descricao'])) $data['descricao']      = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao']), true);
                if(!empty($data['observacoes'])) $data['observacoes']  = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                /* Gerar REFERÊNCIA UNICA de suporte*/
                $RefNew = self::gerarReferenciaNova();
                $data['referencia'] = $RefNew;

                /* Save : START*/
                if (!$Table->save($data))  {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $objFiles->tagprocesso   = 'SUPORTE_POST';
                $objFiles->idprocesso    = $RefNew;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false ) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams      = new VirtualDeskSiteParamsHelper();

                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                /* Send Email - Para Admin, Manager e User */
                $objUserTicket=  VirtualDeskSiteUserHelper::getUserObjById($data['iduser']);
                self::SendMailAdmin($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['assunto'], $objUserTicket->name, $objUserTicket->email, $RefNew);
                $DepartName   = self::getDepartamentoName($data['id_departamento']);
                self::SendMailClient($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserTicket->name, $data['assunto'], $data['descricao'], $objUserTicket->email, $DepartName);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($RefNew);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_CREATED') . 'ref=' . $RefNew;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $RefNew;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        /* Atualiza um  para o ecrã/permissões do USER */
        public static function update4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('suporte');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'edit4users'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $suporte_id = $data['suporte_id'];
            if((int) $suporte_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableSuporte($db);
            $Table->load(array('id'=>$suporte_id));

            // Se o estado não for o inicial, o user não pode editar
            $chkIdEstadoInicial = (int) self::getEstadoIdInicio();
            $chkIdEstadoAtual   = (int)$Table->id_estado;
            if($chkIdEstadoInicial != $chkIdEstadoAtual && $chkIdEstadoAtual>0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db->transactionStart();

            try {
                // Prepara dados para update
                unset($data['suporte_id']);

                if((int)$data['departamento']>0) {
                    $data['id_departamento'] = $data['departamento'];
                    unset($data['departamento']);
                }

                if(!empty($data['descricao'])) $data['descricao']      = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao']), true);
                if(!empty($data['observacoes'])) $data['observacoes']  = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                $referencia = $Table->referencia;

                if (!$Table->save($data))  {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $listFile2Eliminar       = $objFiles->setListFile2EliminarFromPOST ($referencia);
                $resFileDelete           = $objFiles->deleteFiles ($referencia , $listFile2Eliminar);
                if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Capa', 'error');
                }

                // Insere os NOVOS ficheiros
                /* FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $objFiles->tagprocesso   = 'SUPORTE_POST';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');


                if ($resFileSave===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }


                $db->transactionCommit();

                $obParam      = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');

                /* Send Email - Para Admin, Manager e User */
                self::SendMailAdminUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $Table->assunto, $objUserVD->name, $objUserVD->email, $referencia);
                $DepartName   = self::getDepartamentoName($Table->id_departamento);
                self::SendMailClientUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $objUserVD->name, $Table->assunto, $Table->descricao, $objUserVD->email, $DepartName);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_UPDATE');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_UPDATE') . 'ref=' . $referencia;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog). ' | '.JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_DELETED').implode(",", $listFile2Eliminar);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Atualiza um evento para o ecrã/permissões do ADMIN */
        public static function update4Admin($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('suporte');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'edit4admins'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdmin(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $suporte_id = $data['suporte_id'];
            if((int) $suporte_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableSuporte($db);
            $Table->load(array('id'=>$suporte_id));

            $db->transactionStart();

            try {
                // Prepara dados para update
                unset($data['suporte_id']);

                if((int)$data['utilizador']>0) {
                    $utilizador = $data['utilizador'];
                    /* Valida Nif de Utilizador a ser associado*/
                    $NIFUserVD = VirtualDeskSiteUserHelper::getFiscalNumberFromVDUser($utilizador);
                    if ((int) $NIFUserVD <=0) return false;
                    $data['iduser'] = $data['utilizador'];
                    $data['nif']    = $NIFUserVD;
                    unset($data['utilizador']);
                }

                if((int)$data['departamento']>0) {
                    $data['id_departamento'] = $data['departamento'];
                    unset($data['departamento']);
                }

                if(!empty($data['descricao'])) $data['descricao']      = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao']), true);
                if(!empty($data['observacoes'])) $data['observacoes']  = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                $referencia = $Table->referencia;

                if (!$Table->save($data))  {
                    $db->transactionRollback();
                    return false;
                }


                /* DELETE - FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $listFile2Eliminar       = $objFiles->setListFile2EliminarFromPOST ($referencia);
                $resFileDelete           = $objFiles->deleteFiles ($referencia , $listFile2Eliminar);
                if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Capa', 'error');
                }

                // Insere os NOVOS ficheiros
                /* FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $objFiles->tagprocesso   = 'SUPORTE_POST';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');


                if ($resFileSave===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $obParam      = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');

                $db->transactionCommit();

                /* Send Email - Para Admin, Manager e User */
                $TableLoaded = new VirtualDeskTableSuporte($db);
                $TableLoaded->load(array('id'=>$suporte_id));
                $objUserTicket=  VirtualDeskSiteUserHelper::getUserObjById($TableLoaded->iduser);
                self::SendMailAdminUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $TableLoaded->assunto, $objUserTicket->name, $objUserTicket->email, $referencia);
                $DepartName   = self::getDepartamentoName($TableLoaded->id_departamento);
                self::SendMailClientUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $objUserTicket->name, $TableLoaded->assunto, $TableLoaded->descricao, $objUserTicket->email, $DepartName);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_UPDATE');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_UPDATE') . 'ref=' . $referencia;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog). ' | '.JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_DELETED').implode(",", $listFile2Eliminar);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Admin();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Atualiza um evento para o ecrã/permissões do MANAGER */
        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('suporte');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $suporte_id = $data['suporte_id'];
            if((int) $suporte_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableSuporte($db);
            $Table->load(array('id'=>$suporte_id));

            // Se o estado não for o inicial, o user não pode editar
            $chkIdEstadoInicial = (int) self::getEstadoIdInicio();
            $chkIdEstadoAtual   = (int)$Table->id_estado;
            if($chkIdEstadoInicial != $chkIdEstadoAtual && $chkIdEstadoAtual>0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db->transactionStart();

            try {
                // Prepara dados para update
                unset($data['suporte_id']);

                if((int)$data['departamento']>0) {
                    $data['id_departamento'] = $data['departamento'];
                    unset($data['departamento']);
                }

                if(!empty($data['descricao'])) $data['descricao']      = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao']), true);
                if(!empty($data['observacoes'])) $data['observacoes']  = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes']), true);

                $referencia = $Table->referencia;

                if (!$Table->save($data))  {
                    $db->transactionRollback();
                    return false;
                }


                /* DELETE - FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $listFile2Eliminar       = $objFiles->setListFile2EliminarFromPOST ($referencia);
                $resFileDelete           = $objFiles->deleteFiles ($referencia , $listFile2Eliminar);
                if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Capa', 'error');
                }

                // Insere os NOVOS ficheiros
                /* FILES */
                $objFiles                = new VirtualDeskSiteSuporteFilesHelper();
                $objFiles->tagprocesso   = 'SUPORTE_POST';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');


                if ($resFileSave===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $obParam      = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');

                $db->transactionCommit();

                /* Send Email - Para Admin, Manager e User */
                $TableLoaded = new VirtualDeskTableSuporte($db);
                $TableLoaded->load(array('id'=>$suporte_id));
                $objUserTicket=  VirtualDeskSiteUserHelper::getUserObjById($TableLoaded->iduser);
                self::SendMailAdminUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $TableLoaded->assunto, $objUserTicket->name, $objUserTicket->email, $referencia);
                $DepartName   = self::getDepartamentoName($TableLoaded->id_departamento);
                self::SendMailClientUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $objUserTicket->name, $TableLoaded->assunto, $TableLoaded->descricao, $objUserTicket->email, $DepartName);
                /* END Send Email */

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Suporte_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $objFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_UPDATE');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_SUPORTE_EVENTLOG_UPDATE') . 'ref=' . $referencia;
                    $eventdata['desc']  .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog). ' | '.JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_DELETED').implode(",", $listFile2Eliminar);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        /* Retorna Id Concluido*/
        public static function getEstadoIdConcluido ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id'))
                    ->from("#__virtualdesk_suporte_estado as a")
                    ->where(" a.bitEnd=1 ")
                );
                $response = $db->loadObjectList();

                $arReturn = array();
                foreach ($response as $row)
                {
                    $arReturn[] = (int)$row->id;
                }
                return($arReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Retorna Id Concluido*/
        public static function getEstadoIdInicio ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id'))
                    ->from("#__virtualdesk_suporte_estado as a")
                    ->where(" a.bitStart=1 ")
                );
                $response = $db->loadObject();
                return($response->id);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }



        /* Limpa os dados na sessão "users states" do joomla */
        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.suporte.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.suporte.data', null);
            $app->setUserState('com_virtualdesk.addnew4admin.suporte.data', null);
            $app->setUserState('com_virtualdesk.edit4user.suporte.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.suporte.data', null);
            $app->setUserState('com_virtualdesk.edit4admin.suporte.data', null);
        }
        
    }
?>