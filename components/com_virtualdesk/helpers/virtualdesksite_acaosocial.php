<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
JLoader::register('VirtualDeskTableFormMainRelAcaoSocial', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/form_main_rel_acaosocial.php');
JLoader::register('VirtualDeskTableFormMainEstadoHistoricoAcaoSocial', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/form_main_estado_historico_acaosocial.php');
JLoader::register('VirtualDeskTableFormMainHistoricoAcaoSocial', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/form_main_historico_acaosocial.php');

class VirtualDeskSiteAcaosocialHelper
{
    const tagchaveModulo = 'acaosocial';

    public static function getPedidosList4User($vbForDataTables=false, $setLimit=-1){
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('acaosocial', 'list4users');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $modulo_id = VirtualDeskSiteFormmainHelper::getModuleId('acaosocial');

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

        if((int)$UserSessionNIF<=0) return false;

        try {

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if ($vbForDataTables === true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef = JRoute::_('index.php?option=com_virtualdesk&view=acaosocial&layout=view4user&pedido_id=');

                $table = " ( SELECT a.id as id, a.id as pedido_id, c.nome as formulario, CONCAT('" . $dummyHRef . "', CAST(a.id as CHAR(10))) as dummy, a.ref";
                $table .= " , IFNULL(g.".$EstadoName.", ' ') as estado, a.estado_id as idestado";
                $table .= " , a.create_date as created, DATE_FORMAT(a.modify_date, '%Y-%m-%d') as modified";
                $table .= " FROM " . $dbprefix . "virtualdesk_form_main_rel_acaosocial as a ";
                $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_form_main as c on c.id=a.form_id";
                $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_form_main_estado as g on g.id=a.estado_id";
                $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_perm_modulo as d on d.id=c.modulo";
                $table .= " WHERE (a.user_id=$UserJoomlaID AND c.enabled=1 AND d.enabled=1 AND c.modulo=$modulo_id)";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array('db' => 'created', 'dt' => 0),
                    array('db' => 'ref', 'dt' => 1),
                    array('db' => 'formulario', 'dt' => 2),
                    array('db' => 'estado', 'dt' => 3),
                    array('db' => 'dummy', 'dt' => 4, 'formatter' => 'URL_ENCRYPT'),
                    array('db' => 'idestado', 'dt' => 5),
                    array('db' => 'id', 'dt' => 6, 'formatter' => 'VALUE_ENCRYPT'),
                    array('db' => 'modified', 'dt' => 7)
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db' => $database,
                    'host' => $host
                );

                $data = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);

                return $data;
            }
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getPedidosList4Manager($vbForDataTables=false, $setLimit=-1){
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('acaosocial', 'list4managers');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $modulo_id = VirtualDeskSiteFormmainHelper::getModuleId('acaosocial');

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

        if((int)$UserSessionNIF<=0) return false;

        try {

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if ($vbForDataTables === true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef = JRoute::_('index.php?option=com_virtualdesk&view=acaosocial&layout=view4manager&pedido_id=');

                $table = " ( SELECT a.id as id, a.id as pedido_id, c.nome as formulario, CONCAT('" . $dummyHRef . "', CAST(a.id as CHAR(10))) as dummy, a.ref";
                $table .= " , IFNULL(g.".$EstadoName.", ' ') as estado, a.estado_id as idestado";
                $table .= " , a.create_date as created, DATE_FORMAT(a.modify_date, '%Y-%m-%d') as modified";
                $table .= " FROM " . $dbprefix . "virtualdesk_form_main_rel_acaosocial as a ";
                $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_form_main as c on c.id=a.form_id";
                $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_form_main_estado as g on g.id=a.estado_id";
                $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_perm_modulo as d on d.id=c.modulo";
                $table .= " WHERE (c.enabled=1 AND d.enabled=1 AND c.modulo=$modulo_id)";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array('db' => 'created', 'dt' => 0),
                    array('db' => 'ref', 'dt' => 1),
                    array('db' => 'formulario', 'dt' => 2),
                    array('db' => 'estado', 'dt' => 3),
                    array('db' => 'dummy', 'dt' => 4, 'formatter' => 'URL_ENCRYPT'),
                    array('db' => 'idestado', 'dt' => 5),
                    array('db' => 'id', 'dt' => 6, 'formatter' => 'VALUE_ENCRYPT'),
                    array('db' => 'modified', 'dt' => 7)
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db' => $database,
                    'host' => $host
                );

                $data = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);

                return $data;
            }
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getDadosFormView4UserDetail($Pedido_Id){

        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('acaosocial');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $modulo_id = VirtualDeskSiteFormmainHelper::getModuleId('acaosocial');

        if( empty($Pedido_Id) )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        // Current Session User...
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select(array('relAS.id', 'dataAS.id', 'dataAS.fields_id', 'fieldRel.form_id', 'dataAS.fields_id as campo',
                'dataAS.val_text', 'dataAS.val_varchar', 'dataAS.val_date', 'dataAS.val_number', 'dataAS.val_raw', 'fieldM.tag as field_tag'))
                ->join('LEFT', '#__virtualdesk_form_main_data_acaosocial as dataAS on dataAS.id_rel=relAS.id')
                ->join('LEFT', '#__virtualdesk_form_fields_rel as fieldRel on (fieldRel.fields_id=dataAS.fields_id and fieldRel.form_id=relAS.form_id)')
                ->join('LEFT', '#__virtualdesk_form_fields as fieldM on (fieldM.id=fieldRel.fields_id )')
                ->from("#__virtualdesk_form_main_rel_acaosocial as relAS")
                ->where( $db->quoteName('relAS.user_id') . '=' . $db->escape($UserJoomlaID) . ' and ' . $db->quoteName('fieldRel.enabled').'= 1 and ' . $db->quoteName('relAS.id') .'=' . $db->escape($Pedido_Id)
            );

            $db->setQuery($query);

            //echo $db->replacePrefix((string) $query);

            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[$row->field_tag] = $row;

            }
            return ($response);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getDadosFormView4ManagerDetail($Pedido_Id){

        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('acaosocial');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $modulo_id = VirtualDeskSiteFormmainHelper::getModuleId('acaosocial');

        if( empty($Pedido_Id) )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        // Current Session User...
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select(array('relAS.id', 'dataAS.id', 'dataAS.fields_id', 'fieldRel.form_id', 'dataAS.fields_id as campo',
                'dataAS.val_text', 'dataAS.val_varchar', 'dataAS.val_date', 'dataAS.val_number', 'dataAS.val_raw', 'fieldM.tag as field_tag'))
                ->join('LEFT', '#__virtualdesk_form_main_data_acaosocial as dataAS on dataAS.id_rel=relAS.id')
                ->join('LEFT', '#__virtualdesk_form_fields_rel as fieldRel on (fieldRel.fields_id=dataAS.fields_id and fieldRel.form_id=relAS.form_id)')
                ->join('LEFT', '#__virtualdesk_form_fields as fieldM on (fieldM.id=fieldRel.fields_id )')
                ->from("#__virtualdesk_form_main_rel_acaosocial as relAS")
                ->where( $db->quoteName('fieldRel.enabled').'= 1 and ' . $db->quoteName('relAS.id') .'=' . $db->escape($Pedido_Id)
                );

            $db->setQuery($query);

            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[$row->field_tag] = $row;
            }
            return ($response);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getDadosFormRel4manager($getInputPedido_Id){
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('acaosocial', 'view4managers');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $EstadoName = 'estado_PT';
        }
        else {
            $EstadoName = 'estado_EN';
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.ref, a.estado_id, ' . $EstadoName . ' as estado'))
            ->leftJoin ("#__virtualdesk_form_main_estado as b on b.id=a.estado_id")
            ->from("#__virtualdesk_form_main_rel_acaosocial as a")
            ->where($db->quoteName('a.id') . "=" . $db->escape($getInputPedido_Id) . "")
        );
        $data = $db->loadAssocList();
        return ($data);

    }

    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
    }

    public static function CheckReferencia($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('ref')
            ->from("#__virtualdesk_form_main_rel_acaosocial")
            ->where($db->quoteName('ref') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function createReqApoioSocial4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('acaosocial');  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if (empty($data) ) return false;

        //$db->transactionStart();

        try {

            // carregar estrutura de campos do form
            $formId = $data['formId'];
            $userID = $data['userID'];


            $tipoSel = $data['fieldDocId'];
            $tipoSelRep = $data['fieldDocIdRep'];
            $qualidadeRep = $data['fieldQualidadeRep'];
            $outraQualRep = $data['fieldOutraQualRep'];
            $tipopedido = $data['fieldtipopedido'];
            $outroTipoPedido = $data['fieldOutroTipoPedido'];

            $nif = $data['fieldNif'];
            $numId = $data['fieldNumId'];
            $telemovel = $data['fieldTelemovel'];
            $telefone = $data['fieldTelefone'];
            $fax = $data['fieldFax'];
            $nifRep = $data['fieldNifRep'];
            $numIdRep = $data['fieldNumIdRep'];
            $telemovelRep = $data['fieldTelemovelRep'];
            $telefoneRep = $data['fieldTelefoneRep'];
            $faxRep = $data['fieldFaxRep'];

            if($nif == ''){
                $nif = 'Null';
            }

            if($numId == ''){
                $numId = 'Null';
            }

            if($telemovel == ''){
                $telemovel = 'Null';
            }

            if($telefone == ''){
                $telefone = 'Null';
            }

            if($fax == ''){
                $fax = 'Null';
            }

            if($nifRep == ''){
                $nifRep = 'Null';
            }

            if($numIdRep == ''){
                $numIdRep = 'Null';
            }

            if($telemovelRep == ''){
                $telemovelRep = 'Null';
            }

            if($telefoneRep == ''){
                $telefoneRep = 'Null';
            }

            if($faxRep == ''){
                $faxRep = 'Null';
            }

            if($tipoSel == 1){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSel == 2){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacao = '';
            }

            if($tipoSelRep == 1){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSelRep == 2){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacaoRep = '';
            }

            if($qualidadeRep == 1){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_REPLEGAL');
            } else if($qualidadeRep == 2){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_GESTNEG');
            } else if($qualidadeRep == 3){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_MANDATARIO');
            } else if($qualidadeRep == 4){
                $QualidadeRep = $outraQualRep;
            }  else {
                $QualidadeRep = '';
            }

            if($tipopedido == 1){
                $pedidoType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_EDUCACAO');
            } else if($tipopedido == 2){
                $pedidoType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_HABITACAO');
            } else if($tipopedido == 3){
                $pedidoType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_SAUDE');
            } else if($tipopedido == 4){
                $pedidoType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_DEFICIENCIA');
            } else if($tipopedido == 5){
                $pedidoType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_SUBSISTENCIA');
            } else if($tipopedido == 6){
                $pedidoType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_AGRICULTURA');
            } else if($tipopedido == 7){
                $pedidoType = $outroTipoPedido;
            } else {
                $pedidoType = '';
            }

            /*Gerar Referencia UNICA*/
            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia = self::random_code();
                $checkREF   = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            /*END Gerar Referencia UNICA*/

            $data['fieldNif'] = $nif;
            $data['fieldNumId'] = $numId;
            $data['fieldTelemovel'] = $telemovel;
            $data['fieldTelefone'] = $telefone;
            $data['fieldFax'] = $fax;
            $data['fieldNifRep'] = $nifRep;
            $data['fieldNumIdRep'] = $numIdRep;
            $data['fieldTelemovelRep'] = $telemovelRep;
            $data['fieldTelefoneRep'] = $telefoneRep;
            $data['fieldFaxRep'] = $faxRep;
            $data['fieldDocId'] = $docIdentificacao;
            $data['fieldDocIdRep'] = $docIdentificacaoRep;
            $data['fieldQualidadeRep'] = $QualidadeRep;
            $data['fieldtipopedido'] = $pedidoType;
            $data['referencia'] = $referencia;

            // getFormFieldEstrutura
            $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);


            // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
            $data2Create = array();
            foreach($data as $keySt => $valSt){
                $tmp = array();
                $tmp['fieldtag'] = $keySt;
                $tmp['campo_id'] = $dataValues[$keySt]['id'];
                $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                $tmp['val'] = $valSt;
                $data2Create[] = $tmp;
            }

            // Enviar os dados anteriores para funçao generica no helper formmain que vai receber idform, user, ref e a estrutura  fieldtag + idcampo + valor do data + tagchavemodulo
            // $resSavePedido = formmain insert new


            /* Save Pedido*/
            VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
            /*END Save Pedido*/



        }
        catch (Exception $e){
            //$db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function createBolsaEstudoEnsinoSuperior4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('acaosocial');  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if (empty($data) ) return false;

        //$db->transactionStart();

        try {

            // carregar estrutura de campos do form
            $formId = $data['formId'];
            $userID = $data['userID'];

            $tipoSel = $data['fieldDocId'];
            $tipoSelRep = $data['fieldDocIdRep'];
            $qualidadeRep = $data['fieldQualidadeRep'];
            $outraQualRep = $data['fieldOutraQualRep'];
            $aproveitamentoEscolar = $data['fieldAproveitamento'];
            $residenciaUniversitaria = $data['fieldResidenciaUniversitaria'];
            $AgregadoInput = $data['AgregadoInput'];

            $nif = $data['fieldNif'];
            $numId = $data['fieldNumId'];
            $telemovel = $data['fieldTelemovel'];
            $telefone = $data['fieldTelefone'];
            $fax = $data['fieldFax'];
            $nifRep = $data['fieldNifRep'];
            $numIdRep = $data['fieldNumIdRep'];
            $telemovelRep = $data['fieldTelemovelRep'];
            $telefoneRep = $data['fieldTelefoneRep'];
            $faxRep = $data['fieldFaxRep'];

            if($nif == ''){
                $nif = 'Null';
            }

            if($numId == ''){
                $numId = 'Null';
            }

            if($telemovel == ''){
                $telemovel = 'Null';
            }

            if($telefone == ''){
                $telefone = 'Null';
            }

            if($fax == ''){
                $fax = 'Null';
            }

            if($nifRep == ''){
                $nifRep = 'Null';
            }

            if($numIdRep == ''){
                $numIdRep = 'Null';
            }

            if($telemovelRep == ''){
                $telemovelRep = 'Null';
            }

            if($telefoneRep == ''){
                $telefoneRep = 'Null';
            }

            if($faxRep == ''){
                $faxRep = 'Null';
            }

            if($tipoSel == 1){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSel == 2){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacao = '';
            }

            if($tipoSelRep == 1){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSelRep == 2){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacaoRep = '';
            }

            if($qualidadeRep == 1){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_REPLEGAL');
            } else if($qualidadeRep == 2){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_GESTNEG');
            } else if($qualidadeRep == 3){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_MANDATARIO');
            } else if($qualidadeRep == 4){
                $QualidadeRep = $outraQualRep;
            }  else {
                $QualidadeRep = '';
            }

            if($aproveitamentoEscolar == 1){
                $aproveitamentoEscolarType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_SIM');
            } else if($aproveitamentoEscolar == 2){
                $aproveitamentoEscolarType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_NAO');
            } else {
                $aproveitamentoEscolarType = '';
            }

            if($residenciaUniversitaria == 1){
                $residenciaUniversitariaType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_SIM');
            } else if($residenciaUniversitaria == 2){
                $residenciaUniversitariaType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_NAO');
            } else {
                $residenciaUniversitariaType = '';
            }

            /*Tratamento repeater*/
            foreach($AgregadoInput as $keyPar => $valAgre ){

                if(empty($agregadoFamiliar)){
                    $agregadoFamiliar = $valAgre['nomeAgregado'] . ';;' . $valAgre['idadeAgregado'] . ';;' . $valAgre['parentescoAgregado'] . ';;' . $valAgre['sitProfissionalAgregado'];
                } else {
                    $agregadoFamiliar .= '||' . $valAgre['nomeAgregado'] . ';;' . $valAgre['idadeAgregado'] . ';;' . $valAgre['parentescoAgregado'] . ';;' . $valAgre['sitProfissionalAgregado'];
                }

            }

            /*Gerar Referencia UNICA*/
            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia = self::random_code();
                $checkREF   = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            /*END Gerar Referencia UNICA*/

            $data['fieldNif'] = $nif;
            $data['fieldNumId'] = $numId;
            $data['fieldTelemovel'] = $telemovel;
            $data['fieldTelefone'] = $telefone;
            $data['fieldFax'] = $fax;
            $data['fieldNifRep'] = $nifRep;
            $data['fieldNumIdRep'] = $numIdRep;
            $data['fieldTelemovelRep'] = $telemovelRep;
            $data['fieldTelefoneRep'] = $telefoneRep;
            $data['fieldFaxRep'] = $faxRep;
            $data['fieldDocId'] = $docIdentificacao;
            $data['fieldDocIdRep'] = $docIdentificacaoRep;
            $data['fieldQualidadeRep'] = $QualidadeRep;
            $data['fieldAproveitamento'] = $aproveitamentoEscolarType;
            $data['fieldResidenciaUniversitaria'] = $residenciaUniversitariaType;
            $data['AgregadoInput'] = $agregadoFamiliar;
            $data['referencia'] = $referencia;

            // getFormFieldEstrutura
            $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

            // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
            $data2Create = array();
            foreach($data as $keySt => $valSt){
                $tmp = array();
                $tmp['fieldtag'] = $keySt;
                $tmp['campo_id'] = $dataValues[$keySt]['id'];
                $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                $tmp['val'] = $valSt;
                $data2Create[] = $tmp;
            }

            // Enviar os dados anteriores para funçao generica no helper formmain que vai receber idform, user, ref e a estrutura  fieldtag + idcampo + valor do data + tagchavemodulo
            // $resSavePedido = formmain insert new


            /* Save Pedido*/
            VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
            /*END Save Pedido*/



        }
        catch (Exception $e){
            //$db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function createCandApoioSocial4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('acaosocial');  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if (empty($data) ) return false;

        //$db->transactionStart();

        try {

            // carregar estrutura de campos do form
            $formId = $data['formId'];
            $userID = $data['userID'];

            $tipoSel = $data['fieldDocId'];
            $tipoSelRep = $data['fieldDocIdRep'];
            $qualidadeRep = $data['fieldQualidadeRep'];
            $outraQualRep = $data['fieldOutraQualRep'];
            $opcaoCandidatura = $data['fieldOpcaoCandidatura'];

            $nif = $data['fieldNif'];
            $numId = $data['fieldNumId'];
            $telemovel = $data['fieldTelemovel'];
            $telefone = $data['fieldTelefone'];
            $fax = $data['fieldFax'];
            $nifRep = $data['fieldNifRep'];
            $numIdRep = $data['fieldNumIdRep'];
            $telemovelRep = $data['fieldTelemovelRep'];
            $telefoneRep = $data['fieldTelefoneRep'];
            $faxRep = $data['fieldFaxRep'];

            if($nif == ''){
                $nif = 'Null';
            }

            if($numId == ''){
                $numId = 'Null';
            }

            if($telemovel == ''){
                $telemovel = 'Null';
            }

            if($telefone == ''){
                $telefone = 'Null';
            }

            if($fax == ''){
                $fax = 'Null';
            }

            if($nifRep == ''){
                $nifRep = 'Null';
            }

            if($numIdRep == ''){
                $numIdRep = 'Null';
            }

            if($telemovelRep == ''){
                $telemovelRep = 'Null';
            }

            if($telefoneRep == ''){
                $telefoneRep = 'Null';
            }

            if($faxRep == ''){
                $faxRep = 'Null';
            }

            if($tipoSel == 1){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSel == 2){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacao = '';
            }

            if($tipoSelRep == 1){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSelRep == 2){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacaoRep = '';
            }

            if($qualidadeRep == 1){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_REPLEGAL');
            } else if($qualidadeRep == 2){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_GESTNEG');
            } else if($qualidadeRep == 3){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_MANDATARIO');
            } else if($qualidadeRep == 4){
                $QualidadeRep = $outraQualRep;
            }  else {
                $QualidadeRep = '';
            }

            if($opcaoCandidatura == 1){
                $opcaoCandidaturaType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_ALTERACAOCANDIDATURA');
            } else if($opcaoCandidatura == 2){
                $opcaoCandidaturaType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_NOTIFICACAOAPERFEICOAMENTO');
            } else if($opcaoCandidatura == 3){
                $opcaoCandidaturaType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_NOTIFICACAOAUDIENCIA');
            } else if($opcaoCandidatura == 4){
                $opcaoCandidaturaType = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_INICIATIVAPROPRIA');
            } else {
                $opcaoCandidaturaType = '';
            }

            /*Gerar Referencia UNICA*/
            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia = self::random_code();
                $checkREF   = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            /*END Gerar Referencia UNICA*/

            $data['fieldNif'] = $nif;
            $data['fieldNumId'] = $numId;
            $data['fieldTelemovel'] = $telemovel;
            $data['fieldTelefone'] = $telefone;
            $data['fieldFax'] = $fax;
            $data['fieldNifRep'] = $nifRep;
            $data['fieldNumIdRep'] = $numIdRep;
            $data['fieldTelemovelRep'] = $telemovelRep;
            $data['fieldTelefoneRep'] = $telefoneRep;
            $data['fieldFaxRep'] = $faxRep;
            $data['fieldDocId'] = $docIdentificacao;
            $data['fieldDocIdRep'] = $docIdentificacaoRep;
            $data['fieldQualidadeRep'] = $QualidadeRep;
            $data['fieldOpcaoCandidatura'] = $opcaoCandidaturaType;
            $data['referencia'] = $referencia;

            // getFormFieldEstrutura
            $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

            // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
            $data2Create = array();
            foreach($data as $keySt => $valSt){
                $tmp = array();
                $tmp['fieldtag'] = $keySt;
                $tmp['campo_id'] = $dataValues[$keySt]['id'];
                $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                $tmp['val'] = $valSt;
                $data2Create[] = $tmp;
            }

            /* Save Pedido*/
            VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
            /*END Save Pedido*/

        }
        catch (Exception $e){
            //$db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function createMarcacaoAtendimentoSocial4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('acaosocial');  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if (empty($data) ) return false;

        //$db->transactionStart();

        try {

            // carregar estrutura de campos do form
            $formId = $data['formId'];
            $userID = $data['userID'];

            $tipoSel = $data['fieldDocId'];
            $tipoSelRep = $data['fieldDocIdRep'];
            $qualidadeRep = $data['fieldQualidadeRep'];
            $outraQualRep = $data['fieldOutraQualRep'];

            $nif = $data['fieldNif'];
            $numId = $data['fieldNumId'];
            $telemovel = $data['fieldTelemovel'];
            $telefone = $data['fieldTelefone'];
            $fax = $data['fieldFax'];
            $nifRep = $data['fieldNifRep'];
            $numIdRep = $data['fieldNumIdRep'];
            $telemovelRep = $data['fieldTelemovelRep'];
            $telefoneRep = $data['fieldTelefoneRep'];
            $faxRep = $data['fieldFaxRep'];

            if($nif == ''){
                $nif = 'Null';
            }

            if($numId == ''){
                $numId = 'Null';
            }

            if($telemovel == ''){
                $telemovel = 'Null';
            }

            if($telefone == ''){
                $telefone = 'Null';
            }

            if($fax == ''){
                $fax = 'Null';
            }

            if($nifRep == ''){
                $nifRep = 'Null';
            }

            if($numIdRep == ''){
                $numIdRep = 'Null';
            }

            if($telemovelRep == ''){
                $telemovelRep = 'Null';
            }

            if($telefoneRep == ''){
                $telefoneRep = 'Null';
            }

            if($faxRep == ''){
                $faxRep = 'Null';
            }

            if($tipoSel == 1){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSel == 2){
                $docIdentificacao = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacao = '';
            }

            if($tipoSelRep == 1){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CC');
            } else if($tipoSelRep == 2){
                $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PASSAPORTE');
            } else {
                $docIdentificacaoRep = '';
            }

            if($qualidadeRep == 1){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_REPLEGAL');
            } else if($qualidadeRep == 2){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_GESTNEG');
            } else if($qualidadeRep == 3){
                $QualidadeRep = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_MANDATARIO');
            } else if($qualidadeRep == 4){
                $QualidadeRep = $outraQualRep;
            }  else {
                $QualidadeRep = '';
            }

            /*Gerar Referencia UNICA*/
            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia = self::random_code();
                $checkREF   = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            /*END Gerar Referencia UNICA*/

            $data['fieldNif'] = $nif;
            $data['fieldNumId'] = $numId;
            $data['fieldTelemovel'] = $telemovel;
            $data['fieldTelefone'] = $telefone;
            $data['fieldFax'] = $fax;
            $data['fieldNifRep'] = $nifRep;
            $data['fieldNumIdRep'] = $numIdRep;
            $data['fieldTelemovelRep'] = $telemovelRep;
            $data['fieldTelefoneRep'] = $telefoneRep;
            $data['fieldFaxRep'] = $faxRep;
            $data['fieldDocId'] = $docIdentificacao;
            $data['fieldDocIdRep'] = $docIdentificacaoRep;
            $data['fieldQualidadeRep'] = $QualidadeRep;
            $data['referencia'] = $referencia;

            // getFormFieldEstrutura
            $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

            // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
            $data2Create = array();
            foreach($data as $keySt => $valSt){
                $tmp = array();
                $tmp['fieldtag'] = $keySt;
                $tmp['campo_id'] = $dataValues[$keySt]['id'];
                $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                $tmp['val'] = $valSt;
                $data2Create[] = $tmp;
            }

            /* Save Pedido*/
            VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
            /*END Save Pedido*/

        }
        catch (Exception $e){
            //$db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function updateReqApoioSocial4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('acaosocial');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('acaosocial', 'edit4users'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $pedido_id = $data['pedido_id'];
        if((int) $pedido_id <=0 ) return false;
        unset($data['pedido_id']);

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        $formId = $data['formId'];
        unset($data['formId']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableFormMainRelAcaoSocial($db);
        $Table->load(array('id'=>$pedido_id,'form_id'=>$formId));

        // Se o estado não for o inicial, o user não pode editar
        /*$chkIdEstadoInicial = (int) self::getEstadoIdInicio();
        $chkIdEstadoAtual   = (int)$Table->estado;
        if($chkIdEstadoInicial != $chkIdEstadoAtual && $chkIdEstadoAtual>0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }*/

        try {

            // getFormFieldEstrutura
            $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

            // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
            $data2Create = array();
            foreach($data as $keySt => $valSt){
                $tmp = array();
                $tmp['fieldtag'] = $keySt;
                $tmp['campo_id'] = $dataValues[$keySt]['id'];
                $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                $tmp['val'] = $valSt;
                $data2Create[] = $tmp;
            }

            /* BEGIN Save Form */
            $resSaveForm = VirtualDeskSiteFormmainHelper::saveEditedForm4User($pedido_id, $data2Create, self::tagchaveModulo, $UserJoomlaID);
            /* END Save Form */

            if (empty($resSaveForm)) return false;

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function getEstadoConfigObjectByIdPedido ($id_estado){
        if((int) $id_estado<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('acaosocial');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array(' * ') )
                ->from("#__virtualdesk_form_main_estado_config_acaosocial")
                ->where(" id_estado = ". $db->escape($id_estado))
            );
            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return false;
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getHistoricoTipoIdByTag ($tagchave)
    {
        if(empty($tagchave)) return false;
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array(' id ') )
            ->from("#__virtualdesk_form_main_historico_tipo_acaosocial")
            ->where(" tagchave='". $db->escape($tagchave)."'")
        );
        $dataReturn = $db->loadObject();
        if(empty($dataReturn)) return false;
        return((int)$dataReturn->id);
    }

    public static function getEstadoNameById ($lang, $id_estado)
    {
        if((int) $id_estado<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('acaosocial');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','estado_PT','estado_EN') )
                ->from("#__virtualdesk_form_main_estado")
                ->where(" id = ". $db->escape($id_estado))
            );
            $dataReturn = $db->loadObject();

            if(empty($dataReturn)) return false;

            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = $dataReturn->estado_PT;
            }
            else {
                $EstadoName = $dataReturn->estado_EN;
            }

            return($EstadoName);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getReferenciaById($pedido_id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('ref')
            ->from("#__virtualdesk_form_main_rel_acaosocial")
            ->where($db->quoteName('id') . "=" . $db->escape($pedido_id))
        );
        $data = $db->loadColumn();

        $vsRet = '';
        if(!empty($data[0])) $vsRet = $data[0];
        return ($vsRet);
    }

    public static function getProcManagerAtualObjectByIdPedido ($lang, $id_pedido)
    {
        if((int) $id_pedido<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('acaosocial');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('b.id as id_user','b.name as name','b.email as email') )
                ->join('LEFT', '#__virtualdesk_users AS b ON b.id = a.user_id')
                ->from("#__virtualdesk_form_main_rel_acaosocial as a")
                ->where(" a.id = ". $db->escape($id_pedido))
            );
            $dataReturn = $db->loadObject();

            if(empty($dataReturn)) return false;

            $obj= new stdClass();
            $obj->name  =  $dataReturn->name;
            $obj->email =  $dataReturn->email;

            return($obj);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function saveEstadoProcesso($db, $pedido_id, $estado){
        $query = $db->getQuery(true);

        $fields = array();

        $conditions = array(
            $db->quoteName('id') . ' = ' . $pedido_id
        );

        array_push($fields, 'estado_id = '.  $db->quote($estado));

        $query
            ->update($db->quoteName('#__virtualdesk_form_main_rel_acaosocial'))
            ->set($fields)
            ->where($conditions);

        $db->setQuery($query);
        $result = (boolean)$db->execute();

        return ($result);
    }

    public static function getNameAndEmailDoManagerEAdmin($pedido_id, &$nomeManager, &$emailManager, &$emailAdmin)
    {
        $config                             = JFactory::getConfig();
        $objVDParams                        = new VirtualDeskSiteParamsHelper();
        $AcaoSocialMailManagerbyDefault     = $objVDParams->getParamsByTag('AcaoSocial_Mail_Manager_by_Default');
        $AcaoSocialMailAdminbyDefault       = $objVDParams->getParamsByTag('mailAdminAcaoSocial');
        $adminSiteDefaultEmail              = $config->get('mailfrom');

        // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin
        $objManagerPedidoTmp = self::getProcManagerAtualObjectByIdPedido ('', $pedido_id);
        $emailManager = (string) $objManagerPedidoTmp->email;
        $nomeManager  = (string) $objManagerPedidoTmp->name;
        if(empty($emailManager)) {
            $emailManager = $AcaoSocialMailManagerbyDefault;
            if(empty($emailManager)) {
                $emailManager = $AcaoSocialMailAdminbyDefault;
                if(empty($emailManager)) {
                    $emailManager =  $adminSiteDefaultEmail;
                }
            }
        }

        // Verifica se tem email do admin do alerta caso contrário fica o admin do site
        $emailAdmin = $AcaoSocialMailAdminbyDefault;
        if(empty($emailAdmin)) {
            $emailAdmin = $adminSiteDefaultEmail;
        }

        return(true);
    }

    public static function saveNewMsgHist4ManagerByAjax($getInputPedido_id, $NewMsg, $setVisible4User, $seTipoMsg, $EnableSendMail=1)
    {
        $config = JFactory::getConfig();

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('acaosocial');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('acaosocial', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewMsg) ) return false;
        if((int) $getInputPedido_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;

        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableFormMainHistoricoAcaoSocial($db);

        $data = array();
        $data['id_rel']         = $getInputPedido_id;
        $data['mensagem']       = $db->escape($NewMsg);
        $data['idtipo']         = $seTipoMsg;
        $data['iduser']         = $UserVDId;
        $data['iduserjos']      = $UserJoomlaID;
        $data['setbymanager']   = 1;
        $data['visible4user']   = $setVisible4User;

        try {
            // Store the data.
            if (!$Table->save($data)) return false;

            /* Carrega parâmetros*/
            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setSendNotificacao             = $objVDParams->getParamsByTag('AcaoSocial_SendNotificacao_NewMsgHistFromManager');
            $setSendMailBCCAdmin            = $objVDParams->getParamsByTag('AcaoSocial_SendMail_AllMsg_BCC_To_Admin');
            $setSendMail                    = $objVDParams->getParamsByTag('AcaoSocial_SendMail_NewMsgHistFromManager');
            $setLogGeralEnabled             = $objVDParams->getParamsByTag('AcaoSocial_Log_Geral_Enabled');
            $NotifMaxSizeTituloMsg          = (int)$objVDParams->getParamsByTag('Notificacao_MaxSizeTituloMsg');
            $nomeMunicipio                  = $objVDParams->getParamsByTag('nomeMunicipio');
            $emailCopyrightGeral            = $objVDParams->getParamsByTag('emailCopyrightGeral');
            $contactoTelefCopyrightEmail    = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
            $dominioMunicipio               = $objVDParams->getParamsByTag('dominioMunicipio');
            $LinkCopyright                  = $objVDParams->getParamsByTag('LinkCopyright');
            $copyrightAPP                   = $objVDParams->getParamsByTag('copyrightAPP');
            $logoEmailGeral                 = $objVDParams->getParamsByTag('logoEmailGeral');


            $refPedido = self::getReferenciaById($getInputPedido_id);

            // Envio de email, se estiver configurad
            if ((int)$setSendMail == 1 && (int)$EnableSendMail==1) {

                // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
                $nomeManager   = '';
                $emailManager  = '';
                $emailAdmin    = '';
                $resNameEMail  = self::getNameAndEmailDoManagerEAdmin($getInputPedido_id, $nomeManager, $emailManager, $emailAdmin);

                $TOManager = array();
                $TOUser    = array();
                $TOAdmin   = array();
                $BCC       = array();

                // Vai enviar mail para o MANAGER...
                if(!empty($emailManager)) {
                    $TOManager[] = array('name' => $nomeManager, 'email' => $emailManager);
                    if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ACAOSOCIAL_PROCMANAGER');
                    VirtualDeskSiteFormmainHelper::SendEmail4NewMsgHistFromManager($nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $data['mensagem'], $TOManager, $BCC, $nomeManager, $refPedido);
                }

                // Vai enviar mail para o USER se...
                if ((int)$setVisible4User == 1) {
                    // Vai tentar verificar qual o email do utilizador. Se não estiver no alerta, tenta carregar a partir do NIF a ver se existe como user
                    // Caso contrário não envia o email para o utilizador
                    $TableAlerta = new VirtualDeskTableAlerta($db);
                    $TableAlerta->load(array('Id_alerta'=>$getInputPedido_id));
                    $emailUser = (string) $TableAlerta->email;
                    $nomeUser  = $TableAlerta->utilizador;
                    if(empty($emailUser)) {
                        if(!empty($TableAlerta->nif)) {
                            $objUserAlertaTmp = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($TableAlerta->nif);
                            if(!empty($objUserAlertaTmp->email)) {
                                $emailUser = $objUserAlertaTmp->email;
                                $nomeUser = $objUserAlertaTmp->name;
                            }
                        }
                    }
                    $TOUser[] = array('name'=>$nomeUser , 'email'=>$emailUser);
                    VirtualDeskSiteFormmainHelper::SendEmail4NewMsgHistFromManager($nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $data['mensagem'], $TOUser, $BCC, $nomeUser, $refPedido);
                }

                // Vai enviar mail para o ADMIN se...
                // Se não tiver definido o bit não envia em BCC para o Admin
                if((int)$setSendMailBCCAdmin == 1) {
                    $TOAdmin[] = array('name'=>'Admin' , 'email'=>$emailAdmin);
                    VirtualDeskSiteFormmainHelper::SendEmail4NewMsgHistFromManager($nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $data['mensagem'], $TOAdmin, $BCC, 'Admin', $refPedido);
                }

            }

            // Envio de NOTIFICAÇÂO para o USER se estiver configurado o envio e se a mensagem é VISIVEL para o USER
            if ( ((int)$setVisible4User == 1) && ((int)$setSendNotificacao == 1)) {
                # Vai carrfega o Id do Utilizador associado ao alerta

                $setNotifUserId  = array();
                $setNotifGroupId = array();

                $TableAcaoSocial = new VirtualDeskTableFormMainRelAcaoSocial($db);
                $TableAcaoSocial->load(array('id'=>$getInputPedido_id));


                if(!empty($setNotifUserId)) {
                    //  Se ultrapassar em algumas situações o ideal é cortar o titulo e passar a mensagem toda para a Descrição da notificação.
                    $setNotifMsgDesc = '';
                    $setNotifMsgTitulo =  $NewMsg;
                    if( strlen($NewMsg) > (int)$NotifMaxSizeTituloMsg ) {
                        $setNotifMsgTitulo = substr($NewMsg,$NotifMaxSizeTituloMsg);
                        $setNotifMsgDesc   =  $NewMsg;
                    }

                    //VirtualDeskSiteNotificacaoHelper::saveNovaNotificacaoByProcesso4ManagerByAjax ($getInputPedido_id, 'acaosocial',$setNotifUserId, $setNotifGroupId, $setNotifMsgDesc, $setNotifMsgTitulo);
                }
            }

            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_EVENTLOG_MSGFROMMANAGER');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_EVENTLOG_MSGFROMMANAGER') . 'msg=' . $db->escape($NewMsg) . ' manager=' . $nomeManager . '-' . $emailManager;
                $eventdata['desc'] .= '<br> '.json_encode($data);
                $eventdata['filelist'] = "";
                $eventdata['category'] = '';
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $eventdata['rgpd']        =  false;
                $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                $eventdata['ref']         = $refPedido;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function saveAlterar2NewEstado4ManagerByAjax($getInputPedido_id, $NewEstadoId, $NewEstadoDesc)
    {
        $config = JFactory::getConfig();
        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('acaosocial');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('acaosocial', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('acaosocial', 'alterarestado4managers'); // Ver se tem acesso ao botão
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewEstadoId) ) return false;
        if((int) $getInputPedido_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;
        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        $data = array();
        $data['estado'] = $NewEstadoId;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();

        $Table = new VirtualDeskTableFormMainRelAcaoSocial($db);
        $Table->load(array('id'=>$getInputPedido_id));

        // Só alterar se o estado for diferente
        if((int)$Table->estado_id == (int)$NewEstadoId )  return true;

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        $db->transactionStart();

        try {

            $saveEstado = self::saveEstadoProcesso($db, $getInputPedido_id, $NewEstadoId);
            if($saveEstado == false){
                $db->transactionRollback();
            }

            // Envia para o histório
            $TableHist  = new VirtualDeskTableFormMainEstadoHistoricoAcaoSocial($db);
            $dataHist = array();
            $dataHist['id_estado']  = $NewEstadoId;
            $dataHist['id_rel']     = $getInputPedido_id;
            $dataHist['descricao']  = $NewEstadoDesc;
            $dataHist['createdby']  = $UserJoomlaID;
            $dataHist['modifiedby'] = $UserJoomlaID;

            // Store the data.
            if (!$TableHist->save($dataHist)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled             = $objVDParams->getParamsByTag('AcaoSocial_Log_Geral_Enabled');
            $nomeMunicipio                  = $objVDParams->getParamsByTag('nomeMunicipio');
            $emailCopyrightGeral            = $objVDParams->getParamsByTag('emailCopyrightGeral');
            $contactoTelefCopyrightEmail    = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
            $dominioMunicipio               = $objVDParams->getParamsByTag('dominioMunicipio');
            $LinkCopyright                  = $objVDParams->getParamsByTag('LinkCopyright');
            $copyrightAPP                   = $objVDParams->getParamsByTag('copyrightAPP');
            $logoEmailGeral                 = $objVDParams->getParamsByTag('logoEmailGeral');

            /* Carrega configuração para o envio de mensagens do estado atual */
            $objEstadoConfig = self::getEstadoConfigObjectByIdPedido($NewEstadoId);

            // A alteração de estado é visivel para o utilizador nos histórico de mensagens do processo ?
            $setVisible4User = 0;
            if(!empty($objEstadoConfig->bitHistoricoVisible4User)) $setVisible4User = (int) $objEstadoConfig->bitHistoricoVisible4User;

            // Define tipo de mensagem a ser colocada no histórico
            $IdTipoMsgAltEstado = (int) self::getHistoricoTipoIdByTag ('statechange');

            // Vai agora colocar mensagem no histórico (com ou sem visibilidade para o user)
            $NomeNovoEstado      = self::getEstadoNameById ($lang, $NewEstadoId);
            $NewMsg              = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_CHANGED_ESTADO_PARA')." ".$NomeNovoEstado;
            $NewMsg             .= " <br/> ". $NewEstadoDesc;

            // Coloca no histórico, mas não envia email nesta função porque neste método existe essa gestão pois depende do estado
            self::saveNewMsgHist4ManagerByAjax($getInputPedido_id, $NewMsg, $setVisible4User, $IdTipoMsgAltEstado, 0);

            $refPedido = self::getReferenciaById($getInputPedido_id);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $nomeManager   = '';
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin($getInputPedido_id, $nomeManager, $emailManager, $emailAdmin);

            // Vai tentar verificar qual o email do utilizador. Se não estiver no alerta, tenta carregar a partir do NIF a ver se existe como user
            // Caso contrário não envia o email para o utilizador

            $formId = VirtualDeskSiteFormmainHelper::getFormIdByIdrel($getInputPedido_id, 'acaosocial');
            $formName = VirtualDeskSiteFormmainHelper::getFormNameById($formId);

            $bitsendmail4user= (int)$objEstadoConfig->bitsendemail4user;
            $reqName = VirtualDeskSiteFormmainHelper::getReqName($getInputPedido_id, 'acaosocial');
            $reqMail = VirtualDeskSiteFormmainHelper::getReqMail($getInputPedido_id, 'acaosocial');

            if ( ((int)$bitsendmail4user === 1) && !empty($reqMail)) {
                $TO   = array();
                $BCC  = array();
                $TO[] = array('name'=>$reqName , 'email'=>$reqMail);

                VirtualDeskSiteFormmainHelper::SendEmailToUser4NewMsgHistFromManager($formName, $nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $NewMsg, $TO, $BCC , $reqName, $refPedido);
            }


            $fieldsForm = VirtualDeskSiteFormmainHelper::getFieldsDataById($formId);

            $newFieldsExists = array();
            foreach ($fieldsForm as $keyF) {
                $newFieldsExists[] = $keyF['tag'];
            }

            if (in_array('fieldNomeRep', $newFieldsExists) && in_array('fieldEmailRep', $newFieldsExists)) :
                $repName = VirtualDeskSiteFormmainHelper::getRepName($getInputPedido_id, 'acaosocial');
                $repMail = VirtualDeskSiteFormmainHelper::getRepMail($getInputPedido_id, 'acaosocial');

                if(!empty($repName) && !empty($repMail)){
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>$repName , 'email'=>$repMail);

                    VirtualDeskSiteFormmainHelper::SendEmailToUser4NewMsgHistFromManager($formName, $nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $NewMsg, $TO, $BCC , $repName, $refPedido);
                }

            endif;

            // Vai enviar email para o Manager se estiver configurado na tabela do EstadoConfig,
            $bitsendmail4manager= (int)$objEstadoConfig->bitsendemail4manager;
            if ( ((int)$bitsendmail4manager === 1) && !empty($emailManager)) {
                $TO   = array();
                $BCC  = array();
                $TO[] = array('name'=>$nomeManager , 'email'=>$emailManager);
                if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ALERTA_PROCMANAGER');
                VirtualDeskSiteFormmainHelper::SendEmail4NewMsgHistFromManager($nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $NewMsg, $TO, $BCC , $nomeManager, $refPedido);
            }

            // Vai enviar email para o Admin se estiver configurado na tabela do EstadoConfig,
            $bitsendmail4admin= (int)$objEstadoConfig->bitsendemail4admin;
            if ( ((int)$bitsendmail4admin === 1) && !empty($emailAdmin)) {
                $TO   = array();
                $BCC  = array();
                $TO[] = array('name'=>'' , 'email'=>$emailAdmin);
                VirtualDeskSiteFormmainHelper::SendEmail4NewMsgHistFromManager($nomeMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $logoEmailGeral, $NewMsg, $TO, $BCC , 'Admin', $refPedido);
            }

            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_EVENTLOG_STATE_ALTERADO');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ACAOSOCIAL_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'pedido_id=' . $dataHist['id_rel'];
                $eventdata['desc'] .= '<br> '.json_encode($data);
                $eventdata['filelist'] = "";
                $eventdata['category'] = '';
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $eventdata['rgpd']        =  false;
                $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                $eventdata['ref']         = $refPedido;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.addnewreqapoiosocial4user.acaosocial.data', null);
        $app->setUserState('com_virtualdesk.addnewbolsaestudoensinosuperior4user.acaosocial.data', null);
        $app->setUserState('com_virtualdesk.addnewcandapoiosocial4user.acaosocial.data', null);
        $app->setUserState('com_virtualdesk.addnewmarcacaoatendimentosocial4user.acaosocial.data', null);
        $app->setUserState('com_virtualdesk.editreqapoiosocial4user.acaosocial.data', null);
        $app->setUserState('com_virtualdesk.editbolsaestudoensinosuperior4user.acaosocial.data', null);
        $app->setUserState('com_virtualdesk.editcandapoiosocial4user.acaosocial.data', null);
        $app->setUserState('com_virtualdesk.editmarcacaoatendimentosocial4user.acaosocial.data', null);
    }
}
?>