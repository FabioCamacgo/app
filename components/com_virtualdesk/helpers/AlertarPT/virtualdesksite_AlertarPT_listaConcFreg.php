<?php
    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');

    class VirtualDeskSiteAlertarPTListaConcFregHelper
    {
        public static function getConcelho(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.concelho as concelho, a.id_distrito as id_distrito, b.distrito as distritoName'))
                ->join('LEFT', '#__virtualdesk_digitalGov_distritos AS b ON b.id = a.id_distrito')
                ->from("#__virtualdesk_digitalGov_concelhos as a")
                ->order('distritoName ASC')
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistrito($distrito){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(' distrito')
                ->from("#__virtualdesk_digitalGov_distritos")
                ->where($db->quoteName('id') . '=' . $db->escape($distrito))
            );
            $data = $db->loadResult();
            return ($data);
        }
    }
?>