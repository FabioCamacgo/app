<?php
/**
 * Created by PhpStorm.
 * User: Camacho
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');

    class VirtualDeskSiteAlertarPTNovaOcorrenciaHelper
    {
        public static function getCategorias()
        {

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_AlertarPT_categorias")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCatSelect($categoria){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_AlertarPT_categorias")
                ->where($db->quoteName('id') . '=' . $db->escape($categoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeCat($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_AlertarPT_categorias")
                ->where($db->quoteName('id') . '!=' . $db->escape($categoria))
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getSubcategoria($idwebsitelist, $onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, subcategoria, categoria_id'))
                    ->from("#__virtualdesk_AlertarPT_subcategorias")
                    ->where($db->quoteName('categoria_id') . '=' . $db->escape($idwebsitelist))
                    ->order('subcategoria ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id as id, subcategoria as name'))
                    ->from("#__virtualdesk_AlertarPT_subcategorias")
                    ->where($db->quoteName('categoria_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }


            return ($data);
        }

        public static function getSubCatName($subcategoria){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('subcategoria')
                ->from("#__virtualdesk_AlertarPT_subcategorias")
                ->where($db->quoteName('id') . '=' . $db->escape($subcategoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludesubCat($categoria, $subcategoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, subcategoria as name'))
                ->from("#__virtualdesk_AlertarPT_subcategorias")
                ->where($db->quoteName('categoria_id') . "='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($subcategoria) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelho(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcSelect($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . '=' . $db->escape($concelho))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getConcName($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . '=' . $db->escape($concelho))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getLatitude($idURL){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('latitude')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . '=' . $db->escape($idURL))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getLongitude($idURL){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('longitude')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . '=' . $db->escape($idURL))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeConc($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . '!=' . $db->escape($concelho))
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getFreguesias($idwebsitelist, $onAjaxVD=0){
            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('freguesia ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id as id, freguesia as name'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }


            return ($data);
        }

        public static function getFregName($freguesia){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . '=' . $db->escape($freguesia))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeFreg($concelho, $freguesia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia as name'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($freguesia) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function validaSelect($item){
            if($item == NULL || empty($item)){
                return 1;
            } else {
                return 0;
            }
        }

        public static function validaText($item){
            if($item == Null){
                return 1;
            } else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$item)) {
                return 2;
            } else {
                return 0;
            }
        }

        public static function validaMorada($item){
            if(empty($item) || $item == NULL){
                return 1;
            } else {
                return 0;
            }
        }

        public static function validaNIF($fiscalid){
            $nif=trim($fiscalid);
            $ignoreFirst=true;
            if (!is_numeric($nif) || strlen($nif)!=9) {
                return 1;
            } else {
                $nifSplit=str_split($nif);
                if (
                    in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9))
                    ||
                    $ignoreFirst
                ) {
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);
                    if($checkDigit>=10) $checkDigit=0;
                    if ($checkDigit==$nifSplit[8]) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else {
                    return 1;
                }
            }
        }

        public static function validaEmail($email, $emailConf){
            if($email == Null){
                return 1;
            } else if($emailConf == Null || $email != $emailConf){
                return 2;
            } else {
                return 0;
            }
        }

        public static function validaTelefone($telefone){
            $TelefoneSplit=str_split($telefone);
            if($telefone == Null){
                $errTelefone = 1;
            } else if(!is_numeric($telefone) || strlen($telefone) != 9){
                $errTelefone = 2;
            } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
                $errTelefone = 2;
            } else if($TelefoneSplit[0] == 9){
                if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
                    $errTelefone = 2;
                }
            }
        }

        public static function getReferencia($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_AlertarPT_categorias")
                ->where($db->quoteName('id') . '=' . $db->escape($categoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
        }

        public static function CheckReferencia($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_AlertarPT_ocorrencias")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function saveNewOcorrencia($referencia, $categoria, $subcategoria, $descricao, $concelho, $freguesia, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, $dataAtual){
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','categoria','subcategoria','descricao','concelho','freguesia','sitio','morada','pontosReferencia','latitude','longitude','nome', 'email','nif','telefone','estado','dataEntrada');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($categoria)), $db->quote($db->escape($subcategoria)), $db->quote($db->escape($descricao)), $db->quote($db->escape($concelho)), $db->quote($db->escape($freguesia)), $db->quote($db->escape($sitio)), $db->quote($db->escape($morada)), $db->quote($db->escape($ptosrefs)), $db->quote($db->escape($lat)), $db->quote($db->escape($long)), $db->quote($db->escape($nome)), $db->quote($db->escape($email)),  $db->quote($db->escape($fiscalid)),  $db->quote($db->escape($telefone)),  $db->quote($db->escape(1)),  $db->quote($db->escape($dataAtual)));
            $query
                ->insert($db->quoteName('#__virtualdesk_AlertarPT_ocorrencias'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();

            return($result);
        }

        /* Envia Mail ao utilizador */
        public static function SendEmailOcorrencia($referencia, $catName, $subcatName, $descricao, $concelho, $nameConcelho, $fregName, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, $dataAtual){

            $config = JFactory::getConfig();

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('novaOcorrenciaAlertarPT','email2user');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }
            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            if(empty($lat)){
                $latitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                $longitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
            } else{
                $latitude = $lat;
                $longitude = $long;
            }

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_INTRO',$nome);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_CORPO');
            $BODY_SECONDLINE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_CORPO2');
            $BODY_THIRDLINE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_CORPO3');

            $BODY_REFERENCIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_REFERENCIA');
            $BODY_REFERENCIA_VALUE       = JText::sprintf($referencia);
            $BODY_CATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CATEGORIA');
            $BODY_CATEGORIA_VALUE       = JText::sprintf($catName);
            $BODY_SUBCATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SUBCATEGORIA');
            $BODY_SUBCATEGORIA_VALUE       = JText::sprintf($subcatName);
            $BODY_DESCRICAO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE       = JText::sprintf($descricao);
            $BODY_CONCELHO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_CONCELHO');
            $BODY_CONCELHO_VALUE       = JText::sprintf($nameConcelho);
            $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FREGUESIA');
            $BODY_FREGUESIA_VALUE       = JText::sprintf($fregName);
            $BODY_SITIO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SITIO');
            $BODY_SITIO_VALUE       = JText::sprintf($sitio);
            $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_MORADA');
            $BODY_MORADA_VALUE       = JText::sprintf($morada);
            $BODY_PTOSREF_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_PTOSREF');
            $BODY_PTOSREF_VALUE       = JText::sprintf($ptosrefs);
            $BODY_LAT_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LATITUDE');
            $BODY_LAT_VALUE       = JText::sprintf($latitude);
            $BODY_LONG_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LONGITUDE');
            $BODY_LONG_VALUE       = JText::sprintf($longitude);
            $BODY_FILES_LIST_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FILES_LIST_TITLE');
            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_TITLE",$BODY_REFERENCIA_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_VALUE",$BODY_REFERENCIA_VALUE, $body);
            $body      = str_replace("%BODY_CATEGORIA_TITLE",$BODY_CATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_CATEGORIA_VALUE",$BODY_CATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_TITLE",$BODY_SUBCATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_VALUE",$BODY_SUBCATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_DESCRICAO_TITLE",$BODY_DESCRICAO_TITLE, $body);
            $body      = str_replace("%BODY_DESCRICAO_VALUE",$BODY_DESCRICAO_VALUE, $body);
            $body      = str_replace("%BODY_CONCELHO_TITLE",$BODY_CONCELHO_TITLE, $body);
            $body      = str_replace("%BODY_CONCELHO_VALUE",$BODY_CONCELHO_VALUE, $body);
            $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
            $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
            $body      = str_replace("%BODY_SITIO_TITLE",$BODY_SITIO_TITLE, $body);
            $body      = str_replace("%BODY_SITIO_VALUE",$BODY_SITIO_VALUE, $body);
            $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
            $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
            $body      = str_replace("%BODY_PTOSREF_TITLE",$BODY_PTOSREF_TITLE, $body);
            $body      = str_replace("%BODY_PTOSREF_VALUE",$BODY_PTOSREF_VALUE, $body);
            $body      = str_replace("%BODY_LAT_TITLE",$BODY_LAT_TITLE, $body);
            $body      = str_replace("%BODY_LAT_VALUE",$BODY_LAT_VALUE, $body);
            $body      = str_replace("%BODY_LONG_TITLE",$BODY_LONG_TITLE, $body);
            $body      = str_replace("%BODY_LONG_VALUE",$BODY_LONG_VALUE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);

            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.'/images/Brasoes/concelhos/c_'. $concelho . '.png', "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }

        /* Envia Mail Admin */
        public static function SendEmailOcorrenciaAdmin($referencia, $catName, $subcatName, $descricao, $concelho, $nameConcelho, $fregName, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, $dataAtual){

            $config = JFactory::getConfig();

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('novaOcorrenciaAlertarPT','email2admin');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }

            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            if(empty($lat)){
                $latitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                $longitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
            } else{
                $latitude = $lat;
                $longitude = $long;
            }

            $obParam      = new VirtualDeskSiteParamsHelper();

            $emailAdmin = $obParam->getParamsByTag('mailAdminAlertarPT');


            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TITULO_ADMIN');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TITULO_ADMIN');

            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAILADMIN_CORPO',$referencia);
            $BODY_MESSAGE2       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAILADMIN_2CORPO2');

            $BODY_REFERENCIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_REFERENCIA');
            $BODY_REFERENCIA_VALUE       = JText::sprintf($referencia);
            $BODY_CATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CATEGORIA');
            $BODY_CATEGORIA_VALUE       = JText::sprintf($catName);
            $BODY_SUBCATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SUBCATEGORIA');
            $BODY_SUBCATEGORIA_VALUE       = JText::sprintf($subcatName);
            $BODY_DESCRICAO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE       = JText::sprintf($descricao);
            $BODY_CONCELHO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_CONCELHO');
            $BODY_CONCELHO_VALUE       = JText::sprintf($nameConcelho);
            $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FREGUESIA');
            $BODY_FREGUESIA_VALUE       = JText::sprintf($fregName);
            $BODY_SITIO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SITIO');
            $BODY_SITIO_VALUE       = JText::sprintf($sitio);
            $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_MORADA');
            $BODY_MORADA_VALUE       = JText::sprintf($morada);
            $BODY_PTOSREF_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_PTOSREF');
            $BODY_PTOSREF_VALUE       = JText::sprintf($ptosrefs);
            $BODY_LAT_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LATITUDE');
            $BODY_LAT_VALUE       = JText::sprintf($latitude);
            $BODY_LONG_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LONGITUDE');
            $BODY_LONG_VALUE       = JText::sprintf($longitude);
            $BODY_NOME_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_NOME');
            $BODY_NOME_VALUE       = JText::sprintf($nome);
            $BODY_NIF_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_NIF');
            $BODY_NIF_VALUE       = JText::sprintf($fiscalid);
            $BODY_EMAIL_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_EMAIL');
            $BODY_EMAIL_VALUE       = JText::sprintf($email);
            $BODY_TELEFONE_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TELEFONE');
            $BODY_TELEFONE_VALUE       = JText::sprintf($telefone);
            $BODY_DADOSGERAIS_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAILADMIN_DADOSGERAIS');
            $BODY_DADOSPESSOAIS_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAILADMIN_DADOSPESSOAIS');

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_2MESSAGE",$BODY_MESSAGE2, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_TITLE",$BODY_REFERENCIA_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_VALUE",$BODY_REFERENCIA_VALUE, $body);
            $body      = str_replace("%BODY_CATEGORIA_TITLE",$BODY_CATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_CATEGORIA_VALUE",$BODY_CATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_TITLE",$BODY_SUBCATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_VALUE",$BODY_SUBCATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_DESCRICAO_TITLE",$BODY_DESCRICAO_TITLE, $body);
            $body      = str_replace("%BODY_DESCRICAO_VALUE",$BODY_DESCRICAO_VALUE, $body);
            $body      = str_replace("%BODY_CONCELHO_TITLE",$BODY_CONCELHO_TITLE, $body);
            $body      = str_replace("%BODY_CONCELHO_VALUE",$BODY_CONCELHO_VALUE, $body);
            $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
            $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
            $body      = str_replace("%BODY_SITIO_TITLE",$BODY_SITIO_TITLE, $body);
            $body      = str_replace("%BODY_SITIO_VALUE",$BODY_SITIO_VALUE, $body);
            $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
            $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
            $body      = str_replace("%BODY_PTOSREF_TITLE",$BODY_PTOSREF_TITLE, $body);
            $body      = str_replace("%BODY_PTOSREF_VALUE",$BODY_PTOSREF_VALUE, $body);
            $body      = str_replace("%BODY_LAT_TITLE",$BODY_LAT_TITLE, $body);
            $body      = str_replace("%BODY_LAT_VALUE",$BODY_LAT_VALUE, $body);
            $body      = str_replace("%BODY_LONG_TITLE",$BODY_LONG_TITLE, $body);
            $body      = str_replace("%BODY_LONG_VALUE",$BODY_LONG_VALUE, $body);
            $body      = str_replace("%BODY_NOME_TITLE",$BODY_NOME_TITLE, $body);
            $body      = str_replace("%BODY_NOME_VALUE",$BODY_NOME_VALUE, $body);
            $body      = str_replace("%BODY_NIF_TITLE",$BODY_NIF_TITLE, $body);
            $body      = str_replace("%BODY_NIF_VALUE",$BODY_NIF_VALUE, $body);
            $body      = str_replace("%BODY_EMAIL_TITLE",$BODY_EMAIL_TITLE, $body);
            $body      = str_replace("%BODY_EMAIL_VALUE",$BODY_EMAIL_VALUE, $body);
            $body      = str_replace("%BODY_TELEFONE_TITLE",$BODY_TELEFONE_TITLE, $body);
            $body      = str_replace("%BODY_TELEFONE_VALUE",$BODY_TELEFONE_VALUE, $body);
            $body      = str_replace("%BODY_DADOSGERAIS_TITLE",$BODY_DADOSGERAIS_TITLE, $body);
            $body      = str_replace("%BODY_DADOSPESSOAIS_TITLE",$BODY_DADOSPESSOAIS_TITLE, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($emailAdmin);
            $newActivationEmail->setSubject($data['sitename']);

            $logosendmailImage = $obParam->getParamsByTag('logoMailAdminAlertarPT');
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }



    }
?>