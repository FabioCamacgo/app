<?php
    /**
     * Created by PhpStorm.
     * User:
     * Date: 04/09/2018
     * Time: 14:42
     */

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    class VirtualDeskSiteProfileHelper
    {
        const tagchaveModulo = 'profile';

        public static function getUserType($UserID){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('group_id'))
                ->from("#__user_usergroup_map")
                ->where($db->quoteName('user_id') . "='" . $db->escape($UserID) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDadosUserDetail(){

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            try
            {

                $db = JFactory::getDBO();
                $query = $db->getQuery(true);

                $query->select(array('a.name', 'a.login', 'a.email', 'a.address', 'a.postalcode',
                    'a.fiscalid', 'a.phone1', 'b.concelho as concelhoName', 'c.freguesia as freguesiaName', 'a.veracidadedados', 'a.politicaprivacidade'))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos as b on b.id = a.concelho')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias as c on c.id = a.freguesia')
                    ->from("#__virtualdesk_users as a")
                    ->where( $db->quoteName('a.idjos') . '=' . $db->escape($UserJoomlaID)
                    );

                $db->setQuery($query);

                $dataReturn = $db->loadObject();
                return($dataReturn);

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

    }
?>