<?php
/**
* @package     VirtualDesk
* load common footer for template: javascript...
* @copyright
* @license
*/

defined('_JEXEC') or die;

if(!is_array($userReqFileList)) $userReqFileList = array();
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
$baseurl       = JUri::base();

$param_contactus_filelimitnum  = (int)JComponentHelper::getParams('com_virtualdesk')->get('contactus_filelimitnum');
?>



<div id="vd-filelist" class="table-scrollable" >
<table class="table table-hover">
<?php foreach ($userReqFileList as $row) : ?>
        <tr class="vd-filelist-item">
            <td class="text-center">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>">
                    <?php echo $row->fileicon; ?>
                </a>
            </td>
            <td>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>"
                   id="<?php echo $row->basename?>" class="linkFileName"  data-type="text" data-original-title="Enter <?php echo $keyConfFieldNames; ?>">
                    <?php echo $row->desc; ?>
                </a>
            </td>

            <td>


                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>"
                   class="btn blue btn-outline btn-circle btn-icon-only popovers" data-placement="top" data-trigger="hover" data-content="<?php echo JText::_( 'COM_VIRTUALDESK_DOWNLOADFILE' ); ?>" >
                    <i class="fa fa-download"></i> </a>

                <a dataFileId="<?php echo $row->basename?>"
                   href="javascript:;" class="editableLinkFileName btn green btn-outline btn-circle btn-icon-only popovers" data-placement="top" data-trigger="hover" data-content="<?php echo JText::_( 'COM_VIRTUALDESK_CHANGEFILENAME' ); ?> ">
                    <i class="fa fa-pencil"></i> </a>

                <a dataFileId="<?php echo $row->basename?>" href="javascript:;"  class="btn red btn-outline btn-circle btn-icon-only vdFileListDel popovers"
                   data-title="<?php echo JText::_( 'COM_VIRTUALDESK_DELETE' ); ?>   '<?php echo $row->desc; if(!empty($row->ext)) echo ' (.'. $row->ext.')'; ?>' ?"
                   data-type="warning"
                   data-allow-outside-click="true"
                   data-show-confirm-button="true"
                   data-show-cancel-button="true"
                   data-cancel-button-class="btn-danger"
                   data-cancel-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_NOCANCEL' ); ?> "
                   data-confirm-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_YESDELETE' ); ?> "
                   data-confirm-button-class="btn-info"
                   data-placement="top"
                   data-trigger="hover"><i class="fa fa-close"></i> </a>

            </td>

        </tr>
<?php endforeach; ?>
</table>
</div>


<div id="vdFilesToDelete" style="display:none;"></div>
<div id="vdFilesToUpdate" style="display:none;"></div>

<div id="vd-filelist-repeater" class="mt-repeater">
    <div data-repeater-list="<?php echo $keyConfFieldNames; ?>">
        <div data-repeater-item class="mt-repeater-item vd-filelist-repeater-item">

            <div class="mt-repeater-input fileinput fileinput-new" data-provides="fileinput">
                <div class="input-group input-xlarge">
                    <div class="form-control uneditable-input input-fixed input-large" data-trigger="fileinput">
                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                        <span class="fileinput-filename"> </span>
                    </div>
                    <span class="input-group-addon btn default btn-file">
                                                                    <span class="fileinput-new"> <?php echo JText::_( 'COM_VIRTUALDESK_SELECTFILE' ); ?> </span>
                                                                    <span class="fileinput-exists"> <?php echo JText::_( 'COM_VIRTUALDESK_CHANGE' ); ?> </span>
                                                                    <input type="file" name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"> </span>
                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?>  </a>
                </div>
            </div>

            <div class="mt-repeater-input">
            </div>

        </div>
    </div>


    <div id="FileListNumReachedAlert" class="alert alert-danger fade in " style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span><?php echo JText::_( 'COM_VIRTUALDESK_FILELIMITREACHED' ); ?></span>
    </div>
    <div id="FileListNumReachedAlertShow">
    </div>


    <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
        <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

</div>



<script>

    var param_contactus_filelimitnum =  Number(<?php echo $param_contactus_filelimitnum; ?>);

    var FormEditable = function () {

        return {
            //main function to initiate the module
            init: function () {
                //editables element samples
                jQuery('a.editableLinkFileName').click(function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    var CurrentFileId = jQuery(this).attr('dataFileId');

                    jQuery(this).closest('tr').find('a.linkFileName').editable({
                        success: function(response, newValue) {
                          jQuery('#vdFilesToUpdate').append('<input type="text" name="<?php echo $keyConfFieldNames; ?>UPDATE[' + CurrentFileId + ']" value="' + newValue + '"/>');
                        }
                    });

                    jQuery(this).closest('tr').find('a.linkFileName').editable('toggle');
                });

            }
        };

    }();


    jQuery(document).ready(function() {

        jQuery('.vdFileListDel').click(function(evt) {
            evt.preventDefault();

            var CurrentFileId = jQuery(this).attr('dataFileId');
            var ClosestRow    =  jQuery(this).closest('tr');

                var sa_title = jQuery(this).data('title');
                var sa_message = jQuery(this).data('message');
                var sa_type = jQuery(this).data('type');
                var sa_allowOutsideClick = jQuery(this).data('allow-outside-click');
                var sa_showConfirmButton = jQuery(this).data('show-confirm-button');
                var sa_showCancelButton = jQuery(this).data('show-cancel-button');
                var sa_closeOnConfirm = jQuery(this).data('close-on-confirm');
                var sa_closeOnCancel = jQuery(this).data('close-on-cancel');
                var sa_confirmButtonText = jQuery(this).data('confirm-button-text');
                var sa_cancelButtonText = jQuery(this).data('cancel-button-text');
                var sa_confirmButtonClass = jQuery(this).data('confirm-button-class');
                var sa_cancelButtonClass = jQuery(this).data('cancel-button-class');

            swal({
                    title: sa_title,
                    text: sa_message,
                    type: sa_type,
                    allowOutsideClick: sa_allowOutsideClick,
                    showConfirmButton: sa_showConfirmButton,
                    showCancelButton: sa_showCancelButton,
                    confirmButtonClass: sa_confirmButtonClass,
                    cancelButtonClass: sa_cancelButtonClass,
                    closeOnConfirm: sa_closeOnConfirm,
                    closeOnCancel: sa_closeOnCancel,
                    confirmButtonText: sa_confirmButtonText,
                    cancelButtonText: sa_cancelButtonText,
                },
                function(isConfirm){
                    if (isConfirm){

                        jQuery('#vdFilesToDelete').append('<input type="text" name="<?php echo $keyConfFieldNames; ?>DELETE[]" value="' + CurrentFileId + '"/>');

                        ClosestRow.fadeTo("slow", 0.01, function(){ //fade
                            jQuery(this).slideUp(function() { jQuery(this).closest('tr').remove(); });

                            // colocar a aparecer o botão +Adicionar se o nº de ficheirs estiver ok...
                            if(checkIfFileLimtiHasReached()===false) {
                                jQuery('a.mt-repeater-add').show();
                                jQuery('#FileListNumReachedAlertShow > div').hide();
                            }

                        });
                    }
                });
        });


        // X-editable
        FormEditable.init();

 });
</script>