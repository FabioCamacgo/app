<?php
/**
* @package     VirtualDesk
* load common footer for template: javascript...
* @copyright
* @license
*/

defined('_JEXEC') or die;

?>
<div class="table-scrollable">
<table class="table table-hover">
<?php foreach ($userReqFileList as $row) : ?>
        <tr>
            <td class="text-center">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.download&enterprise_id=' . $this->data->enterprise_id . '&bname=' . $row->basename); ?>">
                    <?php echo $row->fileicon; ?>
                </a>
            </td>
            <td>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.download&enterprise_id=' . $this->data->enterprise_id . '&bname=' . $row->basename); ?>">
                    <?php echo $row->desc; ?>
                </a>
            </td>
            <td>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.download&enterprise_id=' . $this->data->enterprise_id . '&bname=' . $row->basename); ?>"
                   class="btn blue btn-outline btn-circle btn-icon-only popovers" data-placement="top" data-trigger="hover" data-content="<?php echo JText::_( 'COM_VIRTUALDESK_DOWNLOADFILE' ); ?>" >
                   <i class="fa fa-download"></i> </a>
            </td>

        </tr>
<?php endforeach; ?>
</table>
</div>
