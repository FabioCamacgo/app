<?php
defined('_JEXEC') or die;

?>
<style>
    .timeline-body-head-actions .timeline-icon {width:40px; height:40px; padding-top:0px; background-color:inherit;}
    .timeline-body-head-actions .timeline-icon i {font-size: 25px; }
</style>


<!-- portlet-body -->

<div class="col-md-8">
    <div class="timeline">
        <?php foreach ($data as $row ): ?>
        <div class="timeline-item">
            <div class="timeline-badge">
                <img class="timeline-badge-userpic" src="<?php echo JUri::base(). '/templates/virtualdesk/assets/layouts/layout/img/avatar_vd3.jpg'; ?>">
            </div>

            <div class="timeline-body">
                <div class="timeline-body-arrow"> </div>
                <div class="timeline-body-head">
                    <div class="timeline-body-head-caption">
                        <a href="javascript:;" class="timeline-body-title font-blue-madison"><?php echo $row->username; ?></a>
                        <span class="timeline-body-time font-grey-cascade"><?php echo $row->createdFull; ?></span>
                    </div>
                    <div class="timeline-body-head-actions">
                        <div class="timeline-icon" title="<?php echo $row->tipo .' [ '. $row->id .' ] '; ?>">
                            <?php switch($row->idtipo) {
                            case 1:
                                echo '<i class="icon-speech font-grey-cascade"></i>';
                            break;
                            case 2:
                                echo '<i class="icon-info font-grey-cascade"></i>';
                            break;
                            case 3:
                                echo '<i class="icon-flag font-grey-cascade"></i>';
                            break;
                            }
                            ?>
                        </div>

                    </div>
                </div>
                <div class="timeline-body-content">
                    <span class="font-grey-cascade"><?php echo nl2br($row->mensagem); ?></span>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="col-md-4">
    <span>&nbsp;</span>
</div>








