<?php
/**
* @package     VirtualDesk
* load common footer for template: javascript...
* @copyright
* @license
*/

defined('_JEXEC') or die;

if(!is_array($userReqFileList)) $userReqFileList = array();
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
$baseurl       = JUri::base();

?>
<style>
    .DocumentsBlockByList.table-scrollable {border: none;}
    .DocumentsBlockByList .table>tbody>tr>td {border: none;}

    .DocumentsBlockByGrid .cbp-caption-defaultWrap img.icon2FileWrap {border: 1px solid #ededed;}
    .DocumentsBlockByGrid .cbp-caption-defaultWrap span.icon2FileWrap {
    display: block;
    left: 0;
    right: 0;
    text-align: center;
    top: 50%;
    position: absolute;
    transform: translateY(-50%);
    color: #337ab7;
    font-size:7em;
    }


</style>


<div class="" style="float:right;">
    <span><button type="button" id="ButtonSetDocumentsBlockByList" class="btn btn-default"><i class="fa fa-bars"></i> </button></span>
    <span><button type="button" id="ButtonSetDocumentsBlockByGrid" class="btn btn-default"><i class="fa fa-th-large"></i> </button></span>
</div>


<div id="DocumentsBlockByList" style="opacity:0; position: absolute; top: 60px; width: 100%;">
<div class="DocumentsBlockByList table-scrollable" >
<table class="table table-hover">
<?php foreach ($userReqFileList as $row) : ?>
        <tr>
            <td class="text-center">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>">
                    <?php echo $row->fileicon; ?>
                </a>
            </td>
            <td>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>">
                    <?php echo $row->desc; ?>
                </a>
            </td>
            <td>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>"
                   class="btn blue btn-outline btn-circle btn-icon-only popovers" data-placement="top" data-trigger="hover" data-content="<?php echo JText::_( 'COM_VIRTUALDESK_DOWNLOADFILE' ); ?>" >
                   <i class="fa fa-download"></i> </a>
            </td>

        </tr>
<?php endforeach; ?>
</table>
</div>
</div>


<div id="DocumentsBlockByGrid">
<div class="DocumentsBlockByGrid portfolio-content portfolio-1" style="margin-top:30px;">
    <div id="js-filters-juicy-projects" class="cbp-l-filters-button">
    </div>
    <div id="js-grid-juicy-projects" class="cbp">


        <?php foreach ($userReqFileList as $row) : ?>
        <div class="cbp-item ">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <?php $IsAtualFileImageType =  VirtualDeskSiteFileUploadHelper::checkFileIsImageType($row->ext); ?>
                    <?php if($IsAtualFileImageType) : ?>
                       <img src="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' .  $row->basename . '&thumb=true'); ?>" alt="">
                    <?php else: ?>
                        <img class="icon2FileWrap" src="<?php echo $baseurl.'templates/virtualdesk/images/transp300.png'; ?>" alt="">
                        <span class="icon2FileWrap">
                        <?php echo VirtualDeskSiteFileUploadHelper::getFileTypeFAIcon ($row->ext,''); ?>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">

                            <span>
                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>"
                                   class="btn red popovers" data-placement="top" data-trigger="hover" data-content="<?php echo JText::_( 'COM_VIRTUALDESK_DOWNLOADFILE' ); ?>" >
                                    <i class="fa fa-download"></i>
                                </a>
                            </span>
                            <?php if($IsAtualFileImageType) : ?>
                                <span style="padding:10px;"></span>
                                <span>
                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.download&contactus_id=' . $this->data->contactus_id . '&bname=' . $row->basename); ?>"
                                       class="cbp-lightbox btn red uppercase popovers" data-placement="top" data-trigger="hover" data-content="<?php echo JText::_( 'COM_VIRTUALDESK_VIEWFULLIMAGE' ); ?>"
                                       data-title="<?php //echo $row->desc; ?>" >
                                        <i class="fa fa-search-plus"></i>
                                    </a>
                                </span>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title uppercase text-center"> <?php echo $row->desc; ?></div>
            <div class="cbp-l-grid-projects-desc text-center ">
            </div>
        </div>
        <?php endforeach; ?>

    </div>
</div>
</div>

<script>
jQuery(document).ready(function() {

    jQuery("#ButtonSetDocumentsBlockByList").click(function(){
        jQuery("#DocumentsBlockByList").css('z-index','0');
        jQuery("#DocumentsBlockByGrid").fadeTo( "slow" , 0, function() {
            jQuery("#DocumentsBlockByList").fadeTo( "slow" , 1).css('z-index','1');
        });
    });

    jQuery("#ButtonSetDocumentsBlockByGrid").click(function(){
        jQuery("#DocumentsBlockByList").fadeTo( "fast" , 0, function() {
            jQuery("#DocumentsBlockByGrid").fadeTo( "fast" , 1).css('z-index','1');
        });
    });

});
</script>

