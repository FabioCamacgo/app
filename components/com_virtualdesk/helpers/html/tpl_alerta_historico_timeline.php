<?php
defined('_JEXEC') or die;

?>
<style>
.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda {text-align:left;}
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita {text-align:right;}

.mt-timeline-2>.mt-container>.mt-item>.mt-timeline-content>.mt-content-container {padding:10px; }

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container:before {right: 40px;}
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container:before {left: 40px;}

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container { margin-left: 0; margin-right: 60px; }
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container { margin-left: 60px; margin-right: 0;}

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container .mt-title {float: right; text-align: right; margin-right: 0px;}
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container .mt-title {float: left; text-align: left; margin-left: 0px;}

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-icon>i {right: 0; left: 50%; transform: translateY(-50%) translateX(-50%);}
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-icon>i  {right: 50%; left: -4px; transform: translateY(-50%) translateX(-50%);}

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container .mt-author {float: left; margin-right: 0px; margin-bottom: 5px;}
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container .mt-author {float: right; margin-left: 0px; margin-bottom: 5px;}

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container .mt-avatar { float: left; }
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container .mt-avatar { float: right; }

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container .mt-author-name {text-align: left;}
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container .mt-author-name {text-align: right;}

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container:before {left: inherit; right: 50px; border-right: none; border-left: 10px solid #d3d7e9;  }
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container:before { left: 50px; right: inherit; border-right: 10px solid #d3d7e9; border-left: none;}

.mt-timeline-2>.mt-container>.mt-item.vdTLEsquerda>.mt-timeline-content>.mt-content-container .mt-author-notes {text-align: left;}
.mt-timeline-2>.mt-container>.mt-item.vdTLDireita>.mt-timeline-content>.mt-content-container .mt-author-notes {text-align: right;}


</style>


<!-- portlet-body -->
<div class="">
    <div class="col-md-1">&nbsp;</div>
    <div class="col-md-10">
    <div class="mt-timeline-2">
        <div class="mt-timeline-line border-grey-steel"></div>
        <ul class="mt-container">
        <?php foreach ($data as $row ): ?>

            <?php
            $ClassAlinhamento = 'vdTLEsquerda';
            if ((int)$row->setbymanager==1) $ClassAlinhamento = 'vdTLDireita';
            ?>

            <li class="mt-item <?php echo $ClassAlinhamento; ?>" >

                    <?php switch($row->idtipo) {
                        case 1:
                            echo '<div class="mt-timeline-icon bg-grey font-green-haze border-grey-steel" >';
                            echo '<i class="icon-speech"></i> </div>';
                        break;
                        case 2:
                            echo '<div class="mt-timeline-icon bg-blue bg-font-blue border-grey-steel" >';
                            echo '<i class="icon-info"></i> </div>';
                        break;
                        case 3:
                            echo '<div class="mt-timeline-icon bg-yellow bg-font-yellow border-grey-steel" >';
                            echo '<i class="icon-flag"></i> </div>';
                        break;
                        case 4:
                            echo '<div class="mt-timeline-icon bg-green bg-font-green border-grey-steel" >';
                            echo '<i class="icon-home"></i> </div>';
                        break;
                        default:
                            echo '<div class="mt-timeline-icon bg-green bg-font-green border-grey-steel" >';
                            echo '</div>';
                            break;
                    }
                    ?>


            <?php if ((int)$row->setbymanager==1) : ?>
            <!-- MANAGER  right -->
                <div class="mt-timeline-content">
                    <div class="mt-content-container ">

                        <div class="mt-author">
                            <div class="mt-avatar">
                                <img src="<?php echo JUri::base(). '/templates/virtualdesk/assets/layouts/layout/img/avatar_vd3.jpg'; ?>" />
                            </div>
                            <div class="mt-author-name">
                                <a href="javascript:;" ><?php echo $row->username; ?></a>
                            </div>
                            <div class="mt-author-notes "><?php echo $row->createdFull; ?></div>
                        </div>
                        <div class="mt-title">
                            <h3 class="mt-content-title"><?php echo $row->tipo .' [ '. $row->id .' ] '; ?></h3>
                        </div>
                        <div class="mt-content border-grey-salt">
                            <p><?php echo $row->mensagem; ?></p>
                        </div>
                    </div>
                </div>

            <?php else: ?>
            <!-- USER MANAGER left -->
                <div class="mt-timeline-content">
                    <div class="mt-content-container ">

                        <div class="mt-author">
                            <div class="mt-avatar">
                                <img src="<?php echo JUri::base(). '/templates/virtualdesk/assets/layouts/layout/img/avatar_vd3.jpg'; ?>" />
                            </div>
                            <div class="mt-author-name">
                                <a href="javascript:;" ><?php echo $row->username; ?></a>
                            </div>
                            <div class="mt-author-notes"><?php echo $row->created .' [id '. $row->id .' ] '?></div>
                        </div>
                        <div class="mt-title">
                            <h3 class="mt-content-title"><?php echo $row->tipo ; ?></h3>
                        </div>
                        <div class="mt-content border-grey-salt">
                            <p><?php echo nl2br($row->mensagem); ?></p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>
    </div>
    <div class="col-md-1">&nbsp;</div>
</div>








