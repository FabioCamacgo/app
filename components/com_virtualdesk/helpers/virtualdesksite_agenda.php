<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');
JLoader::register('VirtualDeskSiteVAgendaPrecarioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_precario_files.php');
JLoader::register('VirtualDeskTableAgendaEventos', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/agenda_eventos.php');
JLoader::register('VirtualDeskTableAgendaCategorias', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/agenda_categorias.php');
JLoader::register('VirtualDeskTableAgendaSubCategorias', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/agenda_subcategorias.php');
JLoader::register('VirtualDeskTableAgendaEstouComVontade', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/agenda_estoucomvontade.php');
JLoader::register('VirtualDeskTableAgendaEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/agenda_estado_historico.php');
JLoader::register('VirtualDeskSiteVAgendaEmpresasFotoCapaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_capa_files.php');
JLoader::register('VirtualDeskSiteVAgendaEmpresasLogosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_logos_files.php');
JLoader::register('VirtualDeskSiteVAgendaEmpresasGaleriaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_galeria_files.php');
JLoader::register('VirtualDeskTableVAgendaEmpresasCapa', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/v_agenda_empresas_fotoCapa.php');
JLoader::register('VirtualDeskTableVAgendaEmpresasLogo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/v_agenda_empresas_logo.php');
JLoader::register('VirtualDeskTableVAgendaEmpresasGaleria', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/v_agenda_empresas_galeria.php');
JLoader::register('VirtualDeskTableVAgendaEmpresas', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/v_agenda_empresas.php');
JLoader::register('VirtualDeskTableVAgendaEmpresasEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/v_agenda_empresas_estado_historico.php');
JLoader::register('VirtualDeskSiteVAgendaPatrocinadoresLogosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_patrocinadores_logos.php');
JLoader::register('VirtualDeskTableVAgendaPatrocinadores', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/v_agenda_patrocinadores.php');
JLoader::register('VirtualDeskTableVAgendaDiasInternacionais', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/v_agenda_diasinternacionais.php');

class VirtualDeskSiteAgendaHelper
{
    const tagchaveModulo = 'agenda';


    /* Carrega lista com dados de todos os eventos (acesso ao USER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getAgendaList4User ($vbForDataTables=false, $setLimit=-1)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'list4users');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

        if((int)$UserSessionNIF<=0) return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
                $EstadoName = 'estado';
            }
            else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
                $EstadoName = 'estado';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=view4user&agenda_id=');


                $table  = " ( SELECT a.id as id, a.id as agenda_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.nome_evento as nome_evento, a.descricao_evento as descricao_evento";
                $table .= " , IFNULL(b.".$CatName.",' ') as categoria, IFNULL(c.".$SubCatName.",' ') as subcategoria, IFNULL(b.".$CatName.",' ') as category_name ";
                $table .= " , d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado_evento as idestado , a.categoria as idcategoria ";
                $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_eventos as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_subcategoria AS c ON c.id = a.subcategoria ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_estado AS e ON e.id = a.estado_evento ";
                $table .= " WHERE (a.nif=$UserSessionNIF) ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'created',       'dt' => 0 ),
                    array( 'db' => 'codigo',        'dt' => 1 ),
                    array( 'db' => 'categoria',     'dt' => 2 ),
                    array( 'db' => 'estado',        'dt' => 3 ),
                    array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',      'dt' => 5 ),
                    array( 'db' => 'id',            'dt' => 6 ),
                    array( 'db' => 'modified',      'dt' => 7 ),
                    array( 'db' => 'createdFull',   'dt' => 8 ),
                    array( 'db' => 'nome_evento',   'dt' => 9 ),
                    array( 'db' => 'descricao_evento','dt' => 10 ),
                    array( 'db' => 'idcategoria'     ,'dt' => 11 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();


            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega lista com dados de todos os eventos (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getAgendaList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'list4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','list4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
                $EstadoName = 'estado';
            }
            else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
                $EstadoName = 'estado';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=view4manager&agenda_id=');

                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.setTopEventoEnableByAjax&agenda_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.getTopEventoEnableValByAjax&agenda_id=');
                $SwitchSetHRefPremium =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.setEventoPremiumEnableByAjax&agenda_id=');
                $SwitchGetHRefPremium =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.getEventoPremiumEnableValByAjax&agenda_id=');
                $SwitchSetHRefagendamunicipal =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.setagendamunicipalEnableByAjax&agenda_id=');
                $SwitchGetHRefagendamunicipal =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.getagendamunicipalEnableValByAjax&agenda_id=');
                $SwitchSetHRefmesames =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.setmesamesEnableByAjax&agenda_id=');
                $SwitchGetHRefmesames =  JRoute::_('index.php?option=com_virtualdesk&task=agenda.getmesamesEnableValByAjax&agenda_id=');

                $table  = " ( SELECT a.id as id, a.id as agenda_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.nome_evento as nome_evento, a.descricao_evento as descricao_evento";
                $table .= " , IFNULL(b.".$CatName.",' ') as categoria, IFNULL(c.".$SubCatName.",' ') as subcategoria, IFNULL(b.".$CatName.",' ') as category_name ";
                $table .= " , d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado_evento as idestado , a.categoria as idcategoria, a.data_inicio as data_inicio, IFNULL(f.concelho,' ') as concelho, a.concelho as idconcelho, a.top_Evento as top_Evento, a.evento_premium as evento_premium, a.agenda_Municipal as agenda_Municipal, a.votos as votos, a.mes_a_mes as mes_a_mes";
                $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " , CONCAT('".$SwitchSetHRefPremium."', CAST(a.id as CHAR(10))) as SwitchSetHRefPremium ";
                $table .= " , CONCAT('".$SwitchGetHRefPremium."', CAST(a.id as CHAR(10))) as SwitchGetHRefPremium ";
                $table .= " , CONCAT('".$SwitchSetHRefagendamunicipal."', CAST(a.id as CHAR(10))) as SwitchSetHRefagendamunicipal ";
                $table .= " , CONCAT('".$SwitchGetHRefagendamunicipal."', CAST(a.id as CHAR(10))) as SwitchGetHRefagendamunicipal ";
                $table .= " , CONCAT('".$SwitchSetHRefmesames."', CAST(a.id as CHAR(10))) as SwitchSetHRefmesames ";
                $table .= " , CONCAT('".$SwitchGetHRefmesames."', CAST(a.id as CHAR(10))) as SwitchGetHRefmesames ";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_eventos as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_subcategoria AS c ON c.id = a.subcategoria ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_estado AS e ON e.id = a.estado_evento ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_concelhos AS f ON f.id = a.concelho ";
                $table .= "  ) temp ";

                $primaryKey = 'id';


                $columns = array(
                    array( 'db' => 'data_inicio',                   'dt' => 0 ),
                    array( 'db' => 'concelho',                      'dt' => 1 ),
                    array( 'db' => 'categoria',                     'dt' => 2 ),
                    array( 'db' => 'estado',                        'dt' => 3 ),
                    array( 'db' => 'dummy',                         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',                      'dt' => 5 ),
                    array( 'db' => 'id',                            'dt' => 6 ),
                    array( 'db' => 'modified',                      'dt' => 7 ),
                    array( 'db' => 'createdFull',                   'dt' => 8 ),
                    array( 'db' => 'nome_evento',                   'dt' => 9 ),
                    array( 'db' => 'descricao_evento',              'dt' => 10 ),
                    array( 'db' => 'idcategoria',                   'dt' => 11 ),
                    array( 'db' => 'codigo',                        'dt' => 12 ),
                    array( 'db' => 'idconcelho',                    'dt' => 13 ),
                    array( 'db' => 'top_Evento',                    'dt' => 14 ),
                    array( 'db' => 'SwitchSetHRef',                 'dt' => 15 ),
                    array( 'db' => 'SwitchGetHRef',                 'dt' => 16 ),
                    array( 'db' => 'evento_premium',                'dt' => 17 ),
                    array( 'db' => 'SwitchSetHRefPremium',          'dt' => 18 ),
                    array( 'db' => 'SwitchGetHRefPremium',          'dt' => 19 ),
                    array( 'db' => 'agenda_Municipal',              'dt' => 20 ),
                    array( 'db' => 'SwitchSetHRefagendamunicipal',  'dt' => 21 ),
                    array( 'db' => 'SwitchGetHRefagendamunicipal',  'dt' => 22 ),
                    array( 'db' => 'mes_a_mes',                     'dt' => 23 ),
                    array( 'db' => 'SwitchSetHRefmesames',          'dt' => 24 ),
                    array( 'db' => 'SwitchGetHRefmesames',          'dt' => 25 ),
                    array( 'db' => 'votos',                         'dt' => 26 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function setPluginEnableState($UserJoomlaID, $getInputTopEvento_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_TopEvento_id = $getInputTopEvento_id;
        if((int) $configadmin_TopEvento_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_TopEvento_id;
        $data['top_Evento'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaEventos($db);
        $Table->load(array('id'=>$configadmin_TopEvento_id));

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function checkTopEventoEnableVal($getInputTopEvento_id)
    {
        if( empty($getInputTopEvento_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select enabled From #__virtualdesk_v_agenda_eventos Where id =" . $db->escape($getInputTopEvento_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function setPluginEnableStatePremium($userSessionID, $getInputPremium_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_Premium_id = $getInputPremium_id;
        if((int) $configadmin_Premium_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_Premium_id;
        $data['evento_premium'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaEventos($db);
        $Table->load(array('id'=>$configadmin_Premium_id));

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function checkPremiumEnableVal($getInputPremium_id)
    {
        if( empty($getInputPremium_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select enabled From #__virtualdesk_v_agenda_eventos Where id =" . $db->escape($getInputPremium_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function setPluginEnableStateagendamunicipal($userSessionID, $getInputagendamunicipal_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_agendamunicipal_id = $getInputagendamunicipal_id;
        if((int) $configadmin_agendamunicipal_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_agendamunicipal_id;
        $data['agenda_Municipal'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaEventos($db);
        $Table->load(array('id'=>$configadmin_agendamunicipal_id));

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function checkagendamunicipalEnableVal($getInputagendamunicipal_id)
    {
        if( empty($getInputagendamunicipal_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select enabled From #__virtualdesk_v_agenda_eventos Where id =" . $db->escape($getInputagendamunicipal_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function setPluginEnableStatemesames($userSessionID, $getInputmesames_id, $setId2Enable)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $configadmin_mesames_id = $getInputmesames_id;
        if((int) $configadmin_mesames_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $configadmin_mesames_id;
        $data['mes_a_mes'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaEventos($db);
        $Table->load(array('id'=>$configadmin_mesames_id));

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_EVENTLOG_TOPEVENTO');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function checkmesamesEnableVal($getInputmesames_id)
    {
        if( empty($getInputmesames_id) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'pluginedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select enabled From #__virtualdesk_v_agenda_eventos Where id =" . $db->escape($getInputmesames_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega lista com dados de todas os categorias (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getAgendaListCategorias4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'catlist4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','catlist4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $EstadoName = 'estado';
            }
            else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $EstadoName = 'estado';
            }
            else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $EstadoName = 'estado';
            }
            else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $EstadoName = 'estado';
            }
            else {
                $CatName = 'cat_EN';
                $EstadoName = 'estado';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewcat4manager&agenda_id=');

                $table  = " ( SELECT a.id as id, a.id as agenda_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.cat_PT as cat_PT, a.cat_EN as cat_EN, a.cat_FR as cat_FR";
                $table .= " , a.cat_DE as cat_DE, a.Ref as Ref, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_categoria as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_estado AS e ON e.id = a.estado";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',                'dt' => 0 ),
                    array( 'db' => 'cat_PT',            'dt' => 1 ),
                    array( 'db' => 'cat_EN',            'dt' => 2 ),
                    array( 'db' => 'cat_FR',            'dt' => 3 ),
                    array( 'db' => 'cat_DE',            'dt' => 4 ),
                    array( 'db' => 'Ref',               'dt' => 5 ),
                    array( 'db' => 'estado',            'dt' => 6 ),
                    array( 'db' => 'dummy',             'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',          'dt' => 8 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega lista com dados de todas os categorias (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getAgendaListSubcategorias4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'subcatlist4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','subcatlist4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado';
            }
            else if($lang='en_EN'){
                $EstadoName = 'estado';
            }
            else if($lang='de_DE'){
                $EstadoName = 'estado';
            }
            else if($lang='fr_FR'){
                $EstadoName = 'estado';
            }
            else {
                $EstadoName = 'estado';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewsubcat4manager&agenda_id=');

                $table  = " ( SELECT a.id as id, a.id as agenda_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.subcat_PT as subcat_PT, a.subcat_EN as subcat_EN, a.subcat_FR as subcat_FR";
                $table .= " , a.subcat_DE as subcat_DE, b.cat_PT as cat_PT, a.categoria_id as categoria_id, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_subcategoria as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria_id";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_estado AS e ON e.id = a.estado";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',                'dt' => 0 ),
                    array( 'db' => 'subcat_PT',            'dt' => 1 ),
                    array( 'db' => 'subcat_EN',            'dt' => 2 ),
                    array( 'db' => 'subcat_FR',            'dt' => 3 ),
                    array( 'db' => 'subcat_DE',            'dt' => 4 ),
                    array( 'db' => 'cat_PT',               'dt' => 5 ),
                    array( 'db' => 'estado',            'dt' => 6 ),
                    array( 'db' => 'dummy',             'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',          'dt' => 8 ),
                    array( 'db' => 'categoria_id',      'dt' => 9 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega lista com os TODOS os estados */
    public static function getAgendaEstadoAllOptions ($lang, $displayPermError=true)
    {

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
        // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            if($displayPermError!=false) JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','estado') )
                ->from("#__virtualdesk_v_agenda_estado")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->estado;
                }
                else {
                    $rowName = $row->estado;
                }
                $response[] = array(
                    'id' => $row->id,
                    'name' => $rowName
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega lista com TODAS as categorias */
    public static function getAgendaCatsAllOptions ($lang, $displayPermError=true)
    {

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
        // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            if($displayPermError!=false) JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','cat_PT') )
                ->from("#__virtualdesk_v_agenda_categoria")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->cat_PT
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /* Carrega lista com os TODOS as categorias */
    public static function getAgendaCategoriasAllOptions ()
    {
        try
        {   $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            }
            else if($lang='en_EN') {
                $CatName = 'cat_EN';
            }
            else if($lang='fr_FR') {
                $CatName = 'cat_FR';
            }
            else if($lang='de_DE') {
                $CatName = 'cat_DE';
            }
            else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as name'))
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->order(' ' .$CatName.' ASC')
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->name
                );
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getAgendaConcelhosAllOptions(){
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
        // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            if($displayPermError!=false) JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','concelho') )
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where( $db->quoteName('id_distrito') . '=' . $db->escape('19')  )
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->concelho
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Devolve CSS a ser aplicado de acordo com o ESTADO atual */
    public static function getAgendaEstadoCSS ($idestado)
    {
        $defCss = 'label-default';
        switch ($idestado)
        {   case '1':
                // pendente
                $defCss = 'label-pendente';
                break;
            case '2':
                // publicado
                $defCss = 'label-publicado';
                break;
            case '3':
                // despublicado
                $defCss = 'label-despublicado';
                break;
            case '4':
                // despublicado
                $defCss = 'label-rejeitado';
                break;
        }
        return ($defCss);
    }


    /* Carrega dados visualização do agenda para o USER */
    public static function getAgendaView4UserDetail ($IdAgenda)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdAgenda) )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        // Current Session User...
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
                $EstadoName = 'estado';
            }
            else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
                $EstadoName = 'estado';
            }

            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as agenda_id', 'a.referencia as codigo', 'a.descricao_evento as descricao_evento', 'a.nome_evento as nome_evento'
                , "IFNULL(b.".$CatName.",'') as categoria", "IFNULL(c.".$SubCatName.",' ') as subcategoria", 'd.freguesia as freguesia', 'f.concelho as concelho'
                , "IFNULL(e.".$EstadoName.", ' ') as estado", 'e.id as idestado', 'a.latitude', 'a.longitude', 'a.data_criacao'
                , 'a.data_inicio as data_inicio', 'a.ano_inicio as ano_inicio', 'a.mes_inicio as mes_inicio', 'a.hora_inicio as hora_inicio'
                , 'a.data_fim as data_fim', 'a.ano_fim as ano_fim', 'a.mes_fim as mes_fim', 'a.hora_fim as hora_fim'
                , 'a.local_evento as local_evento', 'a.facebook as facebook', 'a.instagram as instagram', 'a.youtube as youtube', 'a.vimeo as vimeo'
                , 'h.tipo as tipo_evento', 'a.nif as nif_evento', 'a.estado_nif as obsExiste', 'a.entradas_detalhe as preco_evento'
                , 'a.concelho as id_concelho', 'a.freguesia as id_freguesia', 'a.categoria id_categoria','a.subcategoria as id_subcategoria'
                ))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS c ON c.id = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_estado AS e ON e.id = a.estado_evento')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS f ON f.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_v_agenda_tipo_evento AS h ON h.id = a.top_Evento')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdAgenda) . ' and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getNomePromotorEdit($nif){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('a.nome')
            ->from("#__virtualdesk_v_agenda_users as a")
            ->where( $db->quoteName('a.nif') . '=' . $db->escape($nif)  )
        );
        $dataReturn = $db->loadResult();
        return($dataReturn);
    }

    public static function getMailPromotorEdit($nif){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('a.email')
            ->from("#__virtualdesk_v_agenda_users as a")
            ->where( $db->quoteName('a.nif') . '=' . $db->escape($nif)  )
        );
        $dataReturn = $db->loadResult();
        return($dataReturn);
    }


    /* Carrega dados visualização do agenda para o MANAGER */
    public static function getAgendaView4ManagerDetail ($IdAgenda)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'view4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','view4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdAgenda) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
                $tipoName = 'tipo_PT';
            }
            else if( empty($lang) or ($lang='en_EN') ) {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
                $tipoName = 'tipo_EN';
            }
            else if( empty($lang) or ($lang='fr_FR') ) {
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
                $tipoName = 'tipo_FR';
            }
            else if( empty($lang) or ($lang='de_DE') ) {
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
                $tipoName = 'tipo_DE';
            }
            else{
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
                $tipoName = 'tipo_EN';
            }

            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as agenda_id', 'a.referencia as codigo', 'a.nif as nif', "IFNULL(b.".$CatName.",'') as categoria", 'a.categoria as idCat',
                    "IFNULL(c.".$SubCatName.",' ') as subcategoria",'a.subcategoria as idSubCat', 'a.tags', 'a.nome_evento as nome_evento', 'a.descricao_evento as descricao_evento',
                    'a.data_inicio as data_inicio', 'a.ano_inicio as ano_inicio', 'a.mes_inicio as mes_inicio', 'a.data_fim as data_fim', 'a.ano_fim as ano_fim', 'a.mes_fim as mes_fim',
                    'a.hora_inicio as hora_inicio', 'a.hora_fim as hora_fim', 'a.local_evento as local_evento', 'f.concelho as concelho', 'a.concelho as idConcelho',
                    'd.freguesia as freguesia', 'a.freguesia as idFreg', 'a.latitude', 'a.longitude', "IFNULL(g.".$tipoName.",'') as tipo_evento", 'a.tipo_evento as id_tipoEvento',
                    'a.observacoes_precario as observacoes_precario', 'a.facebook as facebook', 'a.instagram as instagram', 'a.youtube as youtube', 'a.vimeo as vimeo', 'a.videosEvento',
                    'a.catVideosEvento', 'a.data_criacao as data_criacao', 'a.data_alteracao as data_alteracao', 'a.estado_evento as idestado', 'e.estado as estado'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS c ON c.id = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_estado AS e ON e.id = a.estado_evento')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS f ON f.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_v_agenda_tipo_evento AS g ON g.id = a.tipo_evento')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdAgenda)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getDadosPromotor($nif){
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'view4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','view4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id', 'a.nome as nomePromotor', 'a.email as emailPromotor'))
                ->from("#__virtualdesk_v_agenda_users as a")
                ->where( $db->quoteName('a.nif') . '=' . $db->escape($nif)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega dados visualização das categorias da agenda para o MANAGER */
    public static function getAgendaViewCat4ManagerDetail ($IdAgenda)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'viewcat4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','viewcat4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdAgenda) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as agenda_id', 'a.cat_PT as cat_PT', 'a.cat_EN as cat_EN', 'a.cat_FR as cat_FR', 'a.cat_DE as cat_DE', 'a.Ref as Ref', 'a.estado as idestado', 'e.estado as estado'))
                ->join('LEFT', '#__virtualdesk_v_agenda_estado AS e ON e.id = a.estado')
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdAgenda)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega dados visualização das categorias da agenda para o MANAGER */
    public static function getAgendaViewSubCat4ManagerDetail ($IdAgenda)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'viewsubcat4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','viewsubcat4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdAgenda) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as agenda_id', 'a.subcat_PT as subcat_PT', 'a.subcat_EN as subcat_EN', 'a.subcat_FR as subcat_FR', 'a.subcat_DE as subcat_DE', 'a.categoria_id as categoria_id', 'b.cat_PT as cat_PT', 'a.estado as idestado', 'e.estado as estado'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria_id')
                ->join('LEFT', '#__virtualdesk_v_agenda_estado AS e ON e.id = a.estado')
                ->from("#__virtualdesk_v_agenda_subcategoria as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdAgenda)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega dados (objecto) do ESTADO atual do processo do agenda em questão */
    public static function getAgendaEstadoAtualObjectByIdAgenda ($lang, $id_agenda)
    {
        if((int) $id_agenda<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('b.id as id_estado','b.estado as estado_PT','b.estado as estado_EN') )
                ->join('LEFT', '#__virtualdesk_v_agenda_estado AS b ON b.id = a.estado_evento')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where(" a.id = ". $db->escape($id_agenda))
            );
            $dataReturn = $db->loadObject();

            if(empty($dataReturn)) return false;

            $obj= new stdClass();
            $obj->id_estado =  $dataReturn->id_estado;
            if( empty($lang) or ($lang='pt_PT') ) {
                $obj->name = $dataReturn->estado_PT;
            }
            else {
                $obj->name  = $dataReturn->estado_EN;
            }

            $obj->cssClass = self::getAgendaEstadoCSS($dataReturn->id_estado);

            return($obj);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega dados (objecto) do ESTADO atual da categoria do agenda em questão */
    public static function getAgendaCategoriaEstadoAtualObject ($lang, $id_agenda)
    {
        if((int) $id_agenda<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id as id_estado','a.estado as estado') )
                ->from("#__virtualdesk_v_agenda_estado as a")
                ->where(" a.id = ". $db->escape($id_agenda))
            );
            $dataReturn = $db->loadObject();

            if(empty($dataReturn)) return false;

            $obj= new stdClass();
            $obj->id_estado =  $dataReturn->id_estado;
            $obj->name = $dataReturn->estado;

            $obj->cssClass = self::getAgendaEstadoCSS($dataReturn->id_estado);

            return($obj);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Grava dados para definir um novo ESTADO do processo de Agenda  */
    public static function saveAlterar2NewEstado4ManagerByAjax($getInputAgenda_id, $NewEstadoId, $NewEstadoDesc)
    {
        $config = JFactory::getConfig();
        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers'); // Ver se tem acesso ao botão
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewEstadoId) ) return false;
        if((int) $getInputAgenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;
        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        $data = array();
        $data['estado_evento'] = $NewEstadoId;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();

        $Table = new VirtualDeskTableAgendaEventos($db);
        $Table->load(array('id'=>$getInputAgenda_id));

        // Só alterar se o estado for diferente
        if((int)$Table->estado_evento == (int)$NewEstadoId )  return true;

        if(!empty($data)) {
            //$dateModified = new DateTime();
            //$data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        $db->transactionStart();

        try {
            // Store the data.
            if (!$Table->save($data)) {
                $db->transactionRollback();
                return false;
            }

            // Envia para o histório
            $TableHist  = new VirtualDeskTableAgendaEstadoHistorico($db);
            $dataHist = array();
            $dataHist['id_estado']  = $NewEstadoId;
            $dataHist['id_agenda']  = $getInputAgenda_id;
            $dataHist['descricao']  = $NewEstadoDesc;
            $dataHist['createdby']  = $UserJoomlaID;
            $dataHist['modifiedby'] = $UserJoomlaID;

            // Store the data.
            if (!$TableHist->save($dataHist)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');
            $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
            $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
            $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
            $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

            /* Carrega configuração para o envio de mensagens do estado atual */
            ##$objEstadoConfig = self::getAlertaEstadoConfigObjectByIdAlerta($NewEstadoId);

            // A alteração de estado é visivel para o utilizador nos histórico de mensagens do processo ?
            ##$setVisible4User = 0;
            ##if(!empty($objEstadoConfig->bitHistoricoVisible4User)) $setVisible4User = (int) $objEstadoConfig->bitHistoricoVisible4User;

            // Define tipo de mensagem a ser colocada no histórico
            ##$IdTipoMsgAltEstado = (int) self::getAlertaHistoricoTipoIdByTag ('statechange');

            // Vai agora colocar mensagem no histórico (com ou sem visibilidade para o user)
            ##$NomeNovoEstado      = self::getAlertaEstadoNameById ($lang, $NewEstadoId);
            ##$NewMsg              = JText::_('COM_VIRTUALDESK_ALERTA_CHANGED_ESTADO_PARA')." ".$NomeNovoEstado;
            ##$NewMsg             .= " <br/> ". $NewEstadoDesc;

            // Coloca no histórico, mas não envia email nesta função porque neste método existe essa gestão pois depende do estado
            ##self::saveNewMsgHist4ManagerByAjax($getInputAlerta_id, $NewMsg, $setVisible4User, $IdTipoMsgAltEstado, 0);

            ##$refAlerta = self::getReferenciaById($getInputAlerta_id);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            ##$nomeManager   = '';
            ##$emailManager  = '';
            ##$emailAdmin    = '';
            ##$resNameEMail  = self::getNameAndEmailDoManagerEAdmin($getInputAlerta_id, $nomeManager, $emailManager, $emailAdmin);

            // Vai tentar verificar qual o email do utilizador. Se não estiver no alerta, tenta carregar a partir do NIF a ver se existe como user
            // Caso contrário não envia o email para o utilizador
            /*$emailUser = (string) $Table->email;
            $nomeUser  = $Table->utilizador;
            if(empty($emailUser)) {
                if(!empty($Table->nif)) {
                    $objUserAlertaTmp = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($Table->nif);
                    if(!empty($objUserAlertaTmp->email)) {
                        $emailUser = $objUserAlertaTmp->email;
                        $nomeUser = $objUserAlertaTmp->name;
                    }
                }
            }*/

            // Vai enviar email para o User, se estiver configurado na tabela do EstadoConfig
            /*$bitsendmail4user= (int)$objEstadoConfig->bitsendemail4user;
            if ( ((int)$bitsendmail4user === 1) && !empty($emailUser)) {
                $TO   = array();
                $BCC  = array();
                $TO[] = array('name'=>$nomeUser , 'email'=>$emailUser);
                self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeUser, $refAlerta);
            }*/

            // Vai enviar email para o Manager se estiver configurado na tabela do EstadoConfig,
            /*$bitsendmail4manager= (int)$objEstadoConfig->bitsendemail4manager;
            if ( ((int)$bitsendmail4manager === 1) && !empty($emailManager)) {
                $TO   = array();
                $BCC  = array();
                $TO[] = array('name'=>$nomeManager , 'email'=>$emailManager);
                if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ALERTA_PROCMANAGER');
                self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeManager, $refAlerta);
            }*/

            // Vai enviar email para o Admin se estiver configurado na tabela do EstadoConfig,
            /*$bitsendmail4admin= (int)$objEstadoConfig->bitsendemail4admin;
            if ( ((int)$bitsendmail4admin === 1) && !empty($emailAdmin)) {
                $TO   = array();
                $BCC  = array();
                $TO[] = array('name'=>'' , 'email'=>$emailAdmin);
                self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , 'Admin', $refAlerta);
            }*/

            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_alerta=' . $dataHist['id_alerta'];
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Grava dados para definir um novo ESTADO da categoria de Agenda  */
    public static function saveAlterarCat2NewEstado4ManagerByAjax($getInputAgenda_id, $NewEstadoId, $NewEstadoDesc)
    {
        $config = JFactory::getConfig();
        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editcat4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers'); // Ver se tem acesso ao botão
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewEstadoId) ) return false;
        if((int) $getInputAgenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;
        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        $data = array();
        $data['estado'] = $NewEstadoId;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();

        $Table = new VirtualDeskTableAgendaCategorias($db);
        $Table->load(array('id'=>$getInputAgenda_id));

        // Só alterar se o estado for diferente
        if((int)$Table->estado == (int)$NewEstadoId )  return true;

        if(!empty($data)) {
        }

        $db->transactionStart();

        try {
            // Store the data.
            if (!$Table->save($data)) {
                $db->transactionRollback();
                return false;
            }

            // Envia para o histório
            $TableHist  = new VirtualDeskTableAgendaEstadoHistorico($db);
            $dataHist = array();
            $dataHist['id_estado']  = $NewEstadoId;
            $dataHist['id_agenda']  = $getInputAgenda_id;
            $dataHist['descricao']  = $NewEstadoDesc;
            $dataHist['createdby']  = $UserJoomlaID;
            $dataHist['modifiedby'] = $UserJoomlaID;

            // Store the data.
            if (!$TableHist->save($dataHist)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');
            $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
            $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
            $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
            $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_alerta=' . $dataHist['id_alerta'];
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Grava dados para definir um novo ESTADO da categoria de Agenda  */
    public static function saveAlterarSubCat2NewEstado4ManagerByAjax($getInputAgenda_id, $NewEstadoId, $NewEstadoDesc)
    {
        $config = JFactory::getConfig();
        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editsubcat4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers'); // Ver se tem acesso ao botão
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewEstadoId) ) return false;
        if((int) $getInputAgenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;
        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        $data = array();
        $data['estado'] = $NewEstadoId;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();

        $Table = new VirtualDeskTableAgendaSubCategorias($db);
        $Table->load(array('id'=>$getInputAgenda_id));

        // Só alterar se o estado for diferente
        if((int)$Table->estado == (int)$NewEstadoId )  return true;

        if(!empty($data)) {
        }

        $db->transactionStart();

        try {
            // Store the data.
            if (!$Table->save($data)) {
                $db->transactionRollback();
                return false;
            }

            // Envia para o histório
            $TableHist  = new VirtualDeskTableAgendaEstadoHistorico($db);
            $dataHist = array();
            $dataHist['id_estado']  = $NewEstadoId;
            $dataHist['id_agenda']  = $getInputAgenda_id;
            $dataHist['descricao']  = $NewEstadoDesc;
            $dataHist['createdby']  = $UserJoomlaID;
            $dataHist['modifiedby'] = $UserJoomlaID;

            // Store the data.
            if (!$TableHist->save($dataHist)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');
            $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
            $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
            $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
            $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_alerta=' . $dataHist['id_alerta'];
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Retorna a Ref Id apenas se o User atual da sessão tem o mesmo NIF da ocorrência
           Útil para saber se o user tem acesso a este alerta...
        */
    public static function checkRefId_4User_By_NIF ($RefId)
    {
        if( empty($RefId) )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia as refid' ))
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn->refid);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Cria novo Evento para o ecrã/permissões do USER */
    public static function create4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {

            if(!empty($data['descricao_evento'])) $data['descricao_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao_evento']),true);

            $categoria     = $data['categoria'];
            $subcategoria  = $data['subcategoria'];
            $dataInicio    = $data['data_inicio'];
            $horaInicio    = $data['hora_inicio'];
            $dataFim       = $data['data_fim'];
            $horaFim       = $data['hora_fim'];
            $nif        = $data['nif_evento'];
            $email      = $data['email_evento'];
            $nomeEvento = $data['nome_evento'];
            $descricao  = $data['descricao_evento'];
            $facebook   = $data['facebook'];
            $instagram  = $data['instagram'];
            $youtube    = $data['youtube'];
            $vimeo      = $data['vimeo'];
            $precoEvento = $data['preco_evento'];
            $dataAtual = date("Y-m-d");
            $horaAtual = date('H:i');
            $layout    = $data['layout_evento'];
            $concelho  = $data['concelho'];
            $freguesia = $data['freguesia'];
            $localEvento = $data['local_evento'];

            ##$dataInicioFormated = VirtualDeskSiteVAgendaHelper::revertDataOrder($dataInicio);
            ##$dataFimFormated    = VirtualDeskSiteVAgendaHelper::revertDataOrder($dataFim);
            $dataInicioFormated = $dataInicio;
            $dataFimFormated    = $dataFim;

            $validacaoNIF = VirtualDeskSiteVAgendaHelper::validaNif((int)$nif);
            $mailExiste = VirtualDeskSiteVAgendaHelper::seeEmailExiste($email, $nif);
            $numEmail = count($mailExiste);
            $validacaoEmail = VirtualDeskSiteVAgendaHelper::validaEmail($email);
            if(!empty($nif) && !empty($email)){
                $seeNifExiste = VirtualDeskSiteVAgendaHelper::seeNifExiste($nif);
                if(count($seeNifExiste) == 1){
                    $getMail = VirtualDeskSiteVAgendaHelper::getEmail($nif);

                    if($getMail != $email){
                        $nifMail = 1;
                    } else {
                        $nifMail = 0;
                    }
                }
            }

            $validacaoCategoria = VirtualDeskSiteVAgendaHelper::validaCategoria($categoria);
            if($categoria != 1) {
                $validacaoSubcategoria = VirtualDeskSiteVAgendaHelper::validaSubcategoria($subcategoria);
            } else {
                $validacaoSubcategoria = 0;
            }
            $validacaoNomeEvento = VirtualDeskSiteVAgendaHelper::validaNomeEvento($nomeEvento);
            $validacaoDataInicio = VirtualDeskSiteVAgendaHelper::validaDataInicio($dataInicioFormated, $dataAtual);
            $validacaoHoraInicio = VirtualDeskSiteVAgendaHelper::validaHoraInicio($horaInicio, $horaAtual, $dataInicioFormated, $dataAtual);
            $validacaoDataFim = VirtualDeskSiteVAgendaHelper::validaDataFim($dataFimFormated, $dataInicioFormated, $dataAtual);
            $validacaoHoraFim = VirtualDeskSiteVAgendaHelper::validaHoraFim($horaFim, $horaInicio, $horaAtual, $dataFimFormated, $dataInicioFormated, $dataAtual);
            $validacaoConcelho = VirtualDeskSiteVAgendaHelper::validaConcelho($concelho);
            $validacaoFreguesia = VirtualDeskSiteVAgendaHelper::validaFreguesia($freguesia);
            $validacaoLocal = VirtualDeskSiteVAgendaHelper::validaLocal($localEvento);
            $validacaoPrecoEvento = VirtualDeskSiteVAgendaHelper::validaPrecoEvento($precoEvento);
            $validacaolayout = VirtualDeskSiteVAgendaHelper::validaFreguesia($layout);

            /*Gerar Referencia UNICA de alerta*/
            $refExiste = 1;

            $refCat          = VirtualDeskSiteVAgendaHelper::getRefCat($categoria);
            $splitDataInicio = explode("-", $dataInicio);
            $splitDataFim    = explode("-", $dataFim);
            $anoInicio       = $splitDataInicio[2];
            $mesInicio       = $splitDataInicio[1];
            $anoFim          = $splitDataFim[2];
            $mesFim          = $splitDataFim[1];

            $obParam      = new VirtualDeskSiteParamsHelper();
            $distrito = $obParam->getParamsByTag('distritoVagenda');


            $referencia = '';
            while($refExiste == 1){
                $refRandom       = VirtualDeskSiteVAgendaHelper::random_code();
                $referencia = $refCat . $refRandom . $nif . $anoInicio . $mesInicio . $anoFim . $mesFim;
                $checkREF = VirtualDeskSiteVAgendaHelper::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }
            /*END Gerar Referencia UNICA de alerta*/

            $seeNifExi = VirtualDeskSiteVAgendaHelper::seeNifExiste($nif);
            if(count($seeNifExi) == 0){
                $obsExiste = 1;
            } else {
                $obsExiste = 2;
            }

            $splitCoord = explode(",", $data['coordenadas']);
            $lat = $splitCoord[0];
            $long = $splitCoord[1];

            $getIdFreg = VirtualDeskSiteVAgendaHelper::getFregSelectID($freguesia);

            /* Save Evento*/

            if($validacaolayout == 0 && $validacaoNIF == 0 && $validacaoEmail == 0 && $numEmail == 0 && $nifMail == 0 && $validacaoCategoria == 0 && $validacaoNomeEvento == 0 && $validacaoDataInicio == 0 && $validacaoHoraInicio == 0 && $validacaoDataFim == 0 && $validacaoHoraFim == 0 && $validacaoConcelho == 0 && $validacaoFreguesia == 0 && $validacaoLocal == 0 && $validacaoPrecoEvento == 0)
            {
                $resSaveEvento = VirtualDeskSiteVAgendaHelper::saveNewEvento($referencia, $nif, $obsExiste, $categoria, $subcategoria, $nomeEvento, $descricao, $dataInicioFormated, $anoInicio, $mesInicio, $horaInicio, $dataFimFormated, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $getIdFreg, $localEvento, $lat, $long, $facebook, $instagram, $youtube, $vimeo, $precoEvento, $layout, $dataAtual, 0);
            }
            else
            {
                $db->transactionRollback();
                return false;
            }



            /*END Save Evento*/

            if (empty($resSaveEvento)) {
                $db->transactionRollback();
                return false;
            }

            /* FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaCapaFilesHelper();
            $objCapaFiles->tagprocesso   = 'AGENDA_POST';
            $objCapaFiles->idprocesso    = $referencia;
            $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

            $objCartazFiles              = new VirtualDeskSiteVAgendaCartazFilesHelper();
            $objCartazFiles->tagprocesso = 'AGENDA_POST';
            $objCartazFiles->idprocesso  = $referencia;
            $resFileSaveCartaz           = $objCartazFiles->saveListFileByPOST('fileupload_cartaz');

            $objGaleriaFiles              = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
            $objGaleriaFiles->tagprocesso = 'AGENDA_POST';
            $objGaleriaFiles->idprocesso  = $referencia;
            $resFileSaveGaleria           = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

            if ($resFileSaveCapa===false || $resFileSaveCartaz==false || $resFileSaveGaleria==false) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                return false;
            }

            $db->transactionCommit();

            /*Send Email*/
            #$catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
            #$subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
            #$fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesias);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            /*END Send Email*/

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    /* Cria novo Evento para o ecrã/permissões do MANAGER */
    public static function create4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
          * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
          */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnew4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {

            if(!empty($data['descricao_evento'])) $data['descricao_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao_evento']), true);
            if(!empty($data['observacoes_precario'])) $data['observacoes_precario']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes_precario']),true);


            $nif_evento         = $data['nif_evento'];
            $nomepromotor        = $data['nomepromotor'];
            $email_evento       = $data['email_evento'];
            $nomeEvento         = $data['nome_evento'];
            $categoria          = $data['categoria'];
            $subcategoria       = $data['subcategoria'];
            $TagsInput          = $data['TagsInput'];
            $descricao_evento   = $data['descricao_evento'];
            $dataInicio         = $data['data_inicio'];
            $horaInicio         = $data['hora_inicio'];
            $dataFim            = $data['data_fim'];
            $horaFim            = $data['hora_fim'];
            $concelho           = $data['concelho'];
            $freguesia_evento   = $data['freguesia_evento'];
            $local_evento        = $data['local_evento'];
            $coordenadas        = $data['coordenadas'];
            $preco              = $data['preco'];
            $observacoes_precario = $data['observacoes_precario'];
            $facebook           = $data['facebook'];
            $instagram          = $data['instagram'];
            $youtube            = $data['youtube'];
            $vimeo              = $data['vimeo'];
            $VideoInput         = $data['VideoInput'];
            $dataAtual = date("Y-m-d");
            $horaAtual = date('H:i');

            /*Gerar Referencia UNICA de alerta*/
            $refExiste = 1;

            $refCat          = VirtualDeskSiteVAgendaHelper::getRefCat($categoria);
            $splitDataInicio = explode("-", $dataInicio);

            if($dataFim == '0000-00-00' || empty($dataFim)){
                $dataFim = $dataInicio;
            }

            $splitDataFim    = explode("-", $dataFim);
            $anoInicio       = $splitDataInicio[0];
            $mesInicio       = $splitDataInicio[1];
            $anoFim          = $splitDataFim[0];
            $mesFim          = $splitDataFim[1];


            foreach($TagsInput as $keyPar => $valParc ){
                if($valParc['tags'] != ''){
                    if(empty($tags)){
                        $tags = $valParc['tags'];
                    } else {
                        $tags .= ';;' . $valParc['tags'];
                    }
                }
            }


            foreach($VideoInput as $keyPar => $valParc ){
                if($valParc['video'] != ''){
                    if(empty($video)){
                        $video = $valParc['video'];
                    } else {
                        $video .= ';;' . $valParc['video'];
                    }

                    if(empty($catVideo)){
                        $catVideo = $valParc['catVideo'];
                    } else {
                        $catVideo .= ';;' . $valParc['catVideo'];
                    }

                }
            }

            $obParam = new VirtualDeskSiteParamsHelper();

            $distrito = $obParam->getParamsByTag('distritoVagenda');

            $referencia = '';
            while($refExiste == 1){
                $refRandom       = VirtualDeskSiteVAgendaHelper::random_code();
                $referencia = $refCat . $refRandom . $nif_evento . $anoInicio . $mesInicio . $anoFim . $mesFim;
                $checkREF = VirtualDeskSiteVAgendaHelper::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }

            $splitCoord = explode(",", $coordenadas);
            $lat = $splitCoord[0];
            $long = $splitCoord[1];

            $getIdFreg = VirtualDeskSiteVAgendaHelper::getFregSelectID($freguesia_evento);

            /* Save Evento*/

            $obParam      = new VirtualDeskSiteParamsHelper();

            $resSaveEvento = VirtualDeskSiteVAgendaHelper::saveNewEvento($referencia, $nif_evento, $nomeEvento, $categoria, $subcategoria, $tags, $descricao_evento, $dataInicio, $anoInicio, $mesInicio, $horaInicio, $dataFim, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $freguesia_evento, $local_evento, $lat, $long, $preco, $observacoes_precario, $facebook, $instagram, $youtube, $vimeo, $video, $catVideo);

            if(empty(VirtualDeskSiteVAgendaHelper::seeNifExiste($nif_evento))){
                $resSaveUser = VirtualDeskSiteVAgendaHelper::saveNewUser($nif_evento, $nomepromotor, $email_evento);
            }

            if(!empty($VideoInput)){
                $videoNumber = 0;
                $today = date("Y-m-d");
                foreach($VideoInput as $keyPar => $valParc ){
                    if($valParc['video'] != ''){
                        if($videoNumber > 0){
                            $nomeEventoVideo = $nomeEvento . ' ' . $videoNumber;
                        } else {
                            $nomeEventoVideo = $nomeEvento;
                        }
                        $video = $valParc['video'];
                        $catVideo = $valParc['catVideo'];
                        VirtualDeskSiteVAgendaHelper::saveVideoMultimedia($referencia, $nomeEventoVideo, $video, $catVideo, $anoInicio, $today);
                        $videoNumber = $videoNumber + 1;
                    }
                }
            }

            /*END Save Evento*/

            if (empty($resSaveEvento)) {
                $db->transactionRollback();
                return false;
            }

            /* FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaCapaFilesHelper();
            $objCapaFiles->tagprocesso   = 'AGENDA_POST';
            $objCapaFiles->idprocesso    = $referencia;
            $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

            $objCartazFiles              = new VirtualDeskSiteVAgendaCartazFilesHelper();
            $objCartazFiles->tagprocesso = 'AGENDA_POST';
            $objCartazFiles->idprocesso  = $referencia;
            $resFileSaveCartaz           = $objCartazFiles->saveListFileByPOST('fileupload_cartaz');

            $objGaleriaFiles              = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
            $objGaleriaFiles->tagprocesso = 'AGENDA_POST';
            $objGaleriaFiles->idprocesso  = $referencia;
            $resFileSaveGaleria           = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

            $objPrecarioFiles              = new VirtualDeskSiteVAgendaPrecarioFilesHelper();
            $objPrecarioFiles->tagprocesso = 'AGENDA_POST';
            $objPrecarioFiles->idprocesso  = $referencia;
            $resFileSavePrecario           = $objPrecarioFiles->saveListFileByPOST('fileupload_precario');

            if ($resFileSaveCapa===false || $resFileSaveCartaz==false || $resFileSaveGaleria==false || $resFileSavePrecario==false) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                return false;
            }

            $db->transactionCommit();

            /*Send Email*/
            #$catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
            #$subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
            #$fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesias);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            /*END Send Email*/

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    /* Atualiza um evento para o ecrã/permissões do USER */
    public static function update4User($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'edit4users'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $agenda_id = $data['agenda_id'];
        if((int) $agenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaEventos($db);
        $Table->load(array('id'=>$agenda_id,'nif'=>$UserSessionNIF));

        // Se o estado não for o inicial, o user não pode editar
        $chkIdEstadoInicial = (int) self::getEstadoIdInicio();
        $chkIdEstadoAtual   = (int)$Table->estado_evento;
        if($chkIdEstadoInicial != $chkIdEstadoAtual && $chkIdEstadoAtual>0) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db->transactionStart();

        try {

            $referencia          = $Table->referencia;
            $obParam             = new VirtualDeskSiteParamsHelper();

            $categoria     = null;
            $subcategoria  = null;
            $nomeEvento    = null;
            $descricao     = null;
            $dataInicio    = null;
            $anoInicio     = null;
            $mesInicio     = null;
            $horaInicio    = null;
            $dataFim       = null;
            $anoFim        = null;
            $mesFim        = null;
            $horaFim       = null;
            $distrito      = null;
            $concelho      = null;
            $freguesia     = null;
            $localEvento   = null;
            $lat           = null;
            $long          = null;
            $facebook      = null;
            $instagram     = null;
            $youtube       = null;
            $vimeo         = null;
            $precoEvento   = null;
            $layout        = null;

            if(!empty($data['descricao_evento'])) $data['descricao_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao_evento']),true);

            if(array_key_exists('categoria',$data))         $categoria = $data['categoria'];
            if(array_key_exists('subcategoria',$data))      $subcategoria = $data['subcategoria'];
            if(array_key_exists('nome_evento',$data))       $nomeEvento = $data['nome_evento'];
            if(array_key_exists('descricao_evento',$data))  $descricao = $data['descricao_evento'];

            if(array_key_exists('data_inicio',$data)) {
                $dataInicio      = $data['data_inicio'];
                $splitDataInicio = explode("-", $dataInicio);
                $anoInicio       = $splitDataInicio[2];
                $mesInicio       = $splitDataInicio[1];
            }
            if(array_key_exists('hora_inicio',$data))        $horaInicio = $data['hora_inicio'];
            if(array_key_exists('data_fim',$data)) {
                $dataFim         = $data['data_fim'];
                $splitDataFim    = explode("-", $dataFim);
                $anoFim          = $splitDataFim[2];
                $mesFim          = $splitDataFim[1];
            }
            if(array_key_exists('hora_fim',$data))        $horaInicio = $data['hora_fim'];
            if(array_key_exists('preco_evento',$data))    $precoEvento = $data['preco_evento'];
            if(array_key_exists('concelho',$data))        $concelho = $data['concelho'];
            if(array_key_exists('freguesia',$data))       $freguesia = $data['freguesia'];
            if(array_key_exists('local_evento',$data))    $localEvento = $data['local_evento'];

            if(array_key_exists('facebook',$data))    $facebook = $data['facebook'];
            if(array_key_exists('instagram',$data))   $instagram = $data['instagram'];
            if(array_key_exists('youtube',$data))     $youtube = $data['youtube'];
            if(array_key_exists('vimeo',$data))       $vimeo = $data['vimeo'];


            if(array_key_exists('coordenadas',$data)) {
                $splitCoord = explode(",", $data['coordenadas']);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];
            }


            /* BEGIN Save Evento */
            $resSaveEvento = VirtualDeskSiteVAgendaHelper::saveEditedEvento4User($agenda_id, $categoria, $subcategoria, $nomeEvento, $descricao, $dataInicio, $anoInicio, $mesInicio, $horaInicio, $dataFim, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $freguesia, $localEvento, $lat, $long, $facebook, $instagram, $youtube, $vimeo, $precoEvento, $layout);
            /* END Save Evento */

            if (empty($resSaveEvento)) {
                $db->transactionRollback();
                return false;
            }

            /* DELETE - FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaCapaFilesHelper();
            $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia);
            $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
            if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Capa', 'error');
            }

            $objCartazFiles              = new VirtualDeskSiteVAgendaCartazFilesHelper();
            $listFileCartaz2Eliminar     = $objCartazFiles->setListFile2EliminarFromPOST ($referencia);
            $resFileCartazDelete         = $objCartazFiles->deleteFiles ($referencia , $listFileCartaz2Eliminar);
            if ($resFileCartazDelete==false && !empty($listFileCartaz2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Cartaz', 'error');
            }

            $objGaleriaFiles             = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
            $listFileGaleria2Eliminar    = $objGaleriaFiles->setListFile2EliminarFromPOST ($referencia);
            $resFileGaleriaDelete        = $objGaleriaFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
            if ($resFileGaleriaDelete==false && !empty($listFileGaleria2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Galeria', 'error');
            }




            // Insere os NOVOS ficheiros
            /* FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaCapaFilesHelper();
            $objCapaFiles->tagprocesso   = 'AGENDA_POST';
            $objCapaFiles->idprocesso    = $referencia;
            $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

            $objCartazFiles              = new VirtualDeskSiteVAgendaCartazFilesHelper();
            $objCartazFiles->tagprocesso = 'AGENDA_POST';
            $objCartazFiles->idprocesso  = $referencia;
            $resFileSaveCartaz           = $objCartazFiles->saveListFileByPOST('fileupload_cartaz');

            $objGaleriaFiles              = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
            $objGaleriaFiles->tagprocesso = 'AGENDA_POST';
            $objGaleriaFiles->idprocesso  = $referencia;
            $resFileSaveGaleria           = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

            if ($resFileSaveCapa===false || $resFileSaveCartaz==false || $resFileSaveGaleria==false) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                return false;
            }


             $db->transactionCommit();

           /*Send Email*/
            #$catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
            #$subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
            #$fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesias);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            /*END Send Email*/

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Atualiza um evento para o ecrã/permissões do MANAGER */
    public static function update4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $agenda_id = $data['agenda_id'];
        if((int) $agenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaEventos($db);
        $Table->load(array('id'=>$agenda_id));

        $db->transactionStart();

        try {

            $referencia          = $Table->referencia;
            $obParam             = new VirtualDeskSiteParamsHelper();

            $nif_evento             = null;
            $nome_evento            = null;
            $categoria              = null;
            $subcategoria           = null;
            $tags                   = null;
            $descricao_evento       = null;
            $dataInicio             = null;
            $horaInicio             = null;
            $dataFim                = null;
            $horaFim                = null;
            $concelho               = null;
            $freguesia_evento       = null;
            $local_evento           = null;
            $coordenadas            = null;
            $latitude               = null;
            $longitude              = null;
            $preco                  = null;
            $observacoes_precario   = null;
            $facebook               = null;
            $instagram              = null;
            $youtube                = null;
            $vimeo                  = null;
            $videosEvento           = null;
            $catVideosEvento        = null;
            $anoInicio              = null;
            $mesInicio              = null;
            $anoFim                 = null;
            $mesFim                 = null;

            if(!empty($data['descricao_evento'])) $data['descricao_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao_evento']),true);
            if(!empty($data['observacoes_precario'])) $data['observacoes_precario']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['observacoes_precario']),true);

            if(array_key_exists('nif_evento',$data))                $nif_evento             = $data['nif_evento'];

            if(array_key_exists('nome_evento',$data))               $nome_evento            = $data['nome_evento'];
            if(array_key_exists('categoria',$data))                 $categoria              = $data['categoria'];
            if(array_key_exists('subcategoria',$data))              $subcategoria           = $data['subcategoria'];
            if(array_key_exists('TagsInput',$data))                 $TagsInput              = $data['TagsInput'];
            if(array_key_exists('descricao_evento',$data))          $descricao_evento       = $data['descricao_evento'];


            if(array_key_exists('data_inicio',$data)) {
                $dataInicio      = $data['data_inicio'];
                if(empty($dataInicio)){
                    $anoInicio = '0';
                    $mesInicio = '0';
                    $dataInicio= '0000-00-00';
                } else {
                    $splitDataInicio = explode("-", $dataInicio);
                    $anoInicio       = $splitDataInicio[0];
                    $mesInicio       = $splitDataInicio[1];
                }
            }
            if(array_key_exists('hora_inicio',$data))               $horaInicio             = $data['hora_inicio'];
            if(array_key_exists('data_fim',$data)) {
                $dataFim         = $data['data_fim'];
                if(empty($dataFim)){
                    $anoFim = '0';
                    $mesFim = '0';
                    $dataFim= '0000-00-00';
                } else {
                    $splitDataFim    = explode("-", $dataFim);
                    $anoFim          = $splitDataFim[0];
                    $mesFim          = $splitDataFim[1];
                }
            }
            if(array_key_exists('hora_fim',$data))                  $horaFim                = $data['hora_fim'];
            if(array_key_exists('preco',$data))                     $preco                  = $data['preco'];
            if(array_key_exists('observacoes_precario',$data))      $observacoes_precario   = $data['observacoes_precario'];
            if(array_key_exists('concelho',$data))                  $concelho               = $data['concelho'];
            if(array_key_exists('freguesia_evento',$data))          $freguesia_evento       = $data['freguesia_evento'];
            if(array_key_exists('local_evento',$data))              $local_evento           = $data['local_evento'];

            if(array_key_exists('facebook',$data))                  $facebook               = $data['facebook'];
            if(array_key_exists('instagram',$data))                 $instagram              = $data['instagram'];
            if(array_key_exists('youtube',$data))                   $youtube                = $data['youtube'];
            if(array_key_exists('vimeo',$data))                     $vimeo                  = $data['vimeo'];
            if(array_key_exists('VideoInput',$data))                $VideoInput             = $data['VideoInput'];


            if(array_key_exists('coordenadas',$data)) {
                $splitCoord = explode(",", $data['coordenadas']);
                $latitude = $splitCoord[0];
                $longitude = $splitCoord[1];
            }

            foreach($TagsInput as $keyPar => $valParc ){
                if($valParc['tags'] != ''){
                    if(empty($tags)){
                        $tags = $valParc['tags'];
                    } else {
                        $tags .= ';;' . $valParc['tags'];
                    }
                }
            }


            foreach($VideoInput as $keyPar => $valParc ){
                if($valParc['video'] != ''){
                    if(empty($video)){
                        $video = $valParc['video'];
                    } else {
                        $video .= ';;' . $valParc['video'];
                    }

                    if(empty($catVideo)){
                        $catVideo = $valParc['catVideo'];
                    } else {
                        $catVideo .= ';;' . $valParc['catVideo'];
                    }

                    if(empty($video)){
                        $catVideo = '';
                    }

                }
            }

            $obParam = new VirtualDeskSiteParamsHelper();

            $distrito = $obParam->getParamsByTag('distritoVagenda');


            $nomepromotor       = null;
            $email_evento       = null;

            /* BEGIN Save Evento */
            $resSaveEvento = VirtualDeskSiteVAgendaHelper::saveEditedEvento4Manager($agenda_id, $nif_evento, $categoria, $subcategoria, $tags, $nome_evento, $descricao_evento, $dataInicio, $anoInicio, $mesInicio, $horaInicio, $dataFim, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $freguesia_evento, $local_evento, $latitude, $longitude, $facebook, $instagram, $youtube, $vimeo, $video, $catVideo, $preco, $observacoes_precario);
            /* END Save Evento */

            if (empty($resSaveEvento)) {
                $db->transactionRollback();
                return false;
            } else {
                $getInfoEvent = self::getInfoEvent($agenda_id);

                foreach($getInfoEvent as $rowStatus) :
                    $ref = $rowStatus['referencia'];
                    $eventName = $rowStatus['nome_evento'];
                endforeach;

                $checkRefMultimedia = self::checkRefMultimedia($ref);
                if(empty($checkRefMultimedia) || $checkRefMultimedia == 0){

                    if(!empty($VideoInput)){
                        $videoNumber = 0;
                        $today = date("Y-m-d");
                        foreach($VideoInput as $keyPar => $valParc ){
                            if($valParc['video'] != ''){
                                if($videoNumber > 0){
                                    $nomeEventoVideo = $eventName . ' ' . $videoNumber;
                                } else {
                                    $nomeEventoVideo = $eventName;
                                }
                                $video = $valParc['video'];
                                $catVideo = $valParc['catVideo'];
                                VirtualDeskSiteVAgendaHelper::saveVideoMultimedia($ref, $nomeEventoVideo, $video, $catVideo, $anoInicio, $today);
                                $videoNumber = $videoNumber + 1;
                            }
                        }
                    }
                } else {
                    self::deleteOldItens($ref);

                    if(!empty($VideoInput)){
                        $videoNumber = 0;
                        $today = date("Y-m-d");
                        foreach($VideoInput as $keyPar => $valParc ){
                            if($valParc['video'] != ''){
                                if($videoNumber > 0){
                                    $nomeEventoVideo = $eventName . ' ' . $videoNumber;
                                } else {
                                    $nomeEventoVideo = $eventName;
                                }
                                $video = $valParc['video'];
                                $catVideo = $valParc['catVideo'];
                                VirtualDeskSiteVAgendaHelper::saveVideoMultimedia($ref, $nomeEventoVideo, $video, $catVideo, $anoInicio, $today);
                                $videoNumber = $videoNumber + 1;
                            }
                        }
                    }
                }
            }

            /* DELETE - FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaCapaFilesHelper();
            $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_capa');
            $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
            if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Capa', 'error');
            }

            $objCartazFiles              = new VirtualDeskSiteVAgendaCartazFilesHelper();
            $listFileCartaz2Eliminar     = $objCartazFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_cartaz');
            $resFileCartazDelete         = $objCartazFiles->deleteFiles ($referencia , $listFileCartaz2Eliminar);
            if ($resFileCartazDelete==false && !empty($listFileCartaz2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Cartaz', 'error');
            }

            $objGaleriaFiles             = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
            $listFileGaleria2Eliminar    = $objGaleriaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_galeria');
            $resFileGaleriaDelete        = $objGaleriaFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
            if ($resFileGaleriaDelete==false && !empty($listFileGaleria2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Galeria', 'error');
            }

            $objPrecarioFiles             = new VirtualDeskSiteVAgendaPrecarioFilesHelper();
            $listFilePrecario2Eliminar    = $objPrecarioFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_precario');
            $resFilePrecarioDelete        = $objPrecarioFiles->deleteFiles ($referencia , $listFilePrecario2Eliminar);
            if ($resFilePrecarioDelete==false && !empty($listFilePrecario2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de Preçário', 'error');
            }




            // Insere os NOVOS ficheiros
            /* FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaCapaFilesHelper();
            $objCapaFiles->tagprocesso   = 'AGENDA_POST';
            $objCapaFiles->idprocesso    = $referencia;
            $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

            $objCartazFiles              = new VirtualDeskSiteVAgendaCartazFilesHelper();
            $objCartazFiles->tagprocesso = 'AGENDA_POST';
            $objCartazFiles->idprocesso  = $referencia;
            $resFileSaveCartaz           = $objCartazFiles->saveListFileByPOST('fileupload_cartaz');

            $objGaleriaFiles              = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
            $objGaleriaFiles->tagprocesso = 'AGENDA_POST';
            $objGaleriaFiles->idprocesso  = $referencia;
            $resFileSaveGaleria           = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

            $objPrecarioFiles              = new VirtualDeskSiteVAgendaPrecarioFilesHelper();
            $objPrecarioFiles->tagprocesso = 'AGENDA_POST';
            $objPrecarioFiles->idprocesso  = $referencia;
            $resFileSavePrecario           = $objPrecarioFiles->saveListFileByPOST('fileupload_precario');

            if ($resFileSaveCapa===false || $resFileSaveCartaz==false || $resFileSaveGaleria==false || $resFileSavePrecario==false) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                return false;
            }


            $db->transactionCommit();

            /*Send Email*/
            #$catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
            #$subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
            #$fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesias);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            #VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
            /*END Send Email*/

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function getInfoEvent($agenda_id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('referencia, nome_evento'))
            ->from("#__virtualdesk_v_agenda_eventos")
            ->where($db->quoteName('id') . "='" . $db->escape($agenda_id) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function checkRefMultimedia($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_Multimedia")
            ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function deleteOldItens($ref){
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $conditions = $db->quoteName('referencia') . ' = ' . $db->quote($ref);

        $query->delete($db->quoteName('#__virtualdesk_Multimedia'));
        $query->where($conditions);

        $db->setQuery($query);

        $result = $db->execute();
    }


    /* Retorna Id Concluido*/
    public static function getEstadoIdConcluido ()
    {
        /*
        * Check PERMISSÕES
        */
        /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }*/

        try
        {
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select( array('id'))
                ->from("#__virtualdesk_v_agenda_estado as a")
                ->where(" a.bitEnd=1 ")
            );
            $response = $db->loadObjectList();

            $arReturn = array();
            foreach ($response as $row)
            {
                $arReturn[] = (int)$row->id;
            }
            return($arReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Retorna Id Concluido*/
    public static function getEstadoIdInicio ()
    {
        /*
        * Check PERMISSÕES
        */
        /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }*/

        try
        {
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select( array('id'))
                ->from("#__virtualdesk_v_agenda_estado as a")
                ->where(" a.bitStart=1 ")
            );
            $response = $db->loadObject();
            return($response->id);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Atualiza uma categoria para o ecrã/permissões do MANAGER */
    public static function updatecat4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editcat4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $agenda_id = $data['agenda_id'];
        if((int) $agenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaCategorias($db);
        $Table->load(array('id'=>$agenda_id));

        $db->transactionStart();

        try {

            $cat_PT         = null;
            $cat_EN         = null;
            $cat_FR         = null;
            $cat_DE         = null;
            $Ref            = null;
            $estado         = null;


            if(array_key_exists('cat_PT',$data))    $cat_PT = $data['cat_PT'];
            if(array_key_exists('cat_EN',$data))    $cat_EN = $data['cat_EN'];
            if(array_key_exists('cat_FR',$data))    $cat_FR = $data['cat_FR'];
            if(array_key_exists('cat_DE',$data))    $cat_DE = $data['cat_DE'];
            if(array_key_exists('Ref',$data))       $Ref = $data['Ref'];
            if(array_key_exists('estado',$data))    $estado = $data['estado'];

            /* BEGIN Save Evento */
            $resSaveCategoria = VirtualDeskSiteVAgendaHelper::saveEditedCat4Manager($agenda_id, $cat_PT, $cat_EN, $cat_FR, $cat_DE, $Ref, $estado);
            /* END Save Evento */

            if (empty($resSaveCategoria)) {
                $db->transactionRollback();
                return false;
            }


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Cria nova categoria para o ecrã/permissões do MANAGER */
    public static function createcat4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
          * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
          */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnewcat4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {
            $cat_PT     = $data['cat_PT'];
            $cat_EN     = $data['cat_EN'];
            $cat_FR     = $data['cat_FR'];
            $cat_DE     = $data['cat_DE'];
            $Ref        = $data['Ref'];
            $estado     = $data['estado'];


            /* Save Categoria*/

            $resSaveCategoria = VirtualDeskSiteVAgendaHelper::saveNewCat($cat_PT, $cat_EN, $cat_FR, $cat_DE, $Ref, $estado);

            /*END Save Categoria*/

            if (empty($resSaveCategoria)) {
                $db->transactionRollback();
                return false;
            }


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    /* Cria nova subcategoria para o ecrã/permissões do MANAGER */
    public static function createsubcat4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
          * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
          */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnewsubcat4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {
            $subcat_PT     = $data['subcat_PT'];
            $subcat_EN     = $data['subcat_EN'];
            $subcat_FR     = $data['subcat_FR'];
            $subcat_DE     = $data['subcat_DE'];
            $categoria     = $data['categoria'];
            $estado        = $data['estado'];


            /* Save Categoria*/

            $resSaveSubcategoria = VirtualDeskSiteVAgendaHelper::saveNewSubCat($subcat_PT, $subcat_EN, $subcat_FR, $subcat_DE, $categoria, $estado);

            /*END Save Categoria*/

            if (empty($resSaveSubcategoria)) {
                $db->transactionRollback();
                return false;
            }


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    /* Atualiza uma subcategoria para o ecrã/permissões do MANAGER */
    public static function updatesubcat4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editsubcat4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $agenda_id = $data['agenda_id'];
        if((int) $agenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaSubCategorias($db);
        $Table->load(array('id'=>$agenda_id));

        $db->transactionStart();

        try {

            $subcat_PT         = null;
            $subcat_EN         = null;
            $subcat_FR         = null;
            $subcat_DE         = null;
            $categoria         = null;
            $estado            = null;


            if(array_key_exists('subcat_PT',$data))    $subcat_PT = $data['subcat_PT'];
            if(array_key_exists('subcat_EN',$data))    $subcat_EN = $data['subcat_EN'];
            if(array_key_exists('subcat_FR',$data))    $subcat_FR = $data['subcat_FR'];
            if(array_key_exists('subcat_DE',$data))    $subcat_DE = $data['subcat_DE'];
            if(array_key_exists('categoria',$data))    $categoria = $data['categoria'];
            if(array_key_exists('estado',$data))       $estado = $data['estado'];

            /* BEGIN Save Evento */
            $resSaveSubCategoria = VirtualDeskSiteVAgendaHelper::saveEditedSubCat4Manager($agenda_id, $subcat_PT, $subcat_EN, $subcat_FR, $subcat_DE, $categoria, $estado);
            /* END Save Evento */

            if (empty($resSaveSubCategoria)) {
                $db->transactionRollback();
                return false;
            }


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Cria novo tema de estou com vontade para o ecrã/permissões do MANAGER */
    public static function createEstouComVontade4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
          * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
          */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnewestoucomvontade4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {
            $nome_PT            = $data['nome_PT'];
            $nome_EN            = $data['nome_EN'];
            $nome_FR            = $data['nome_FR'];
            $nome_DE            = $data['nome_DE'];
            $tipoSel            = $data['tipoSel'];
            $catAssociadas      = $data['catAssociadas'];
            $subcatAssociadas   = $data['subcatAssociadas'];
            $estado             = $data['estado'];

            if($tipoSel == 1) {
                $idscats = implode('|', $catAssociadas);
                $idsSubcats = '';
            } else if($tipoSel == 2) {
                $idsSubcats = implode('|', $subcatAssociadas);
                $idscats = '';
            } else if($tipoSel == 3) {
                $idsSubcats = '';
                $idscats = '';
            }


            /* Save Categoria*/

            $resSaveTema = VirtualDeskSiteVAgendaHelper::saveNewTema($nome_PT, $nome_EN, $nome_FR, $nome_DE, $idscats, $idsSubcats, $estado);

            /*END Save Categoria*/

            if (empty($resSaveTema)) {
                $db->transactionRollback();
                return false;
            }


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED');
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function updateEstouComVontade4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editestoucomvontade4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $agenda_id = $data['agenda_id'];
        if((int) $agenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableAgendaEstouComVontade($db);
        $Table->load(array('id'=>$agenda_id));

        $db->transactionStart();

        try {
            $referencia          = $Table->referencia;
            $obParam    = new VirtualDeskSiteParamsHelper();

            $id = null;
            $nome_PT = null;
            $nome_EN = null;
            $nome_FR = null;
            $nome_DE = null;
            $tipoSel = null;
            $categorias = null;
            $subcategorias = null;
            $estado = null;
            $data_criacao = null;
            $data_alteracao = null;

            if(array_key_exists('nome_PT',$data))           $nome_PT = $data['nome_PT'];
            if(array_key_exists('nome_EN',$data))           $nome_EN = $data['nome_EN'];
            if(array_key_exists('nome_FR',$data))           $nome_FR = $data['nome_FR'];
            if(array_key_exists('nome_DE',$data))           $nome_DE = $data['nome_DE'];
            if(array_key_exists('tipoSel',$data))           $tipoSel = $data['tipoSel'];
            if(array_key_exists('catAssociadas',$data))     $categorias = $data['catAssociadas'];
            if(array_key_exists('subcatAssociadas',$data))  $subcategorias = $data['subcatAssociadas'];
            if(array_key_exists('estado',$data))            $estado = $data['estado'];


            if($tipoSel == 1){
                for($i=0; $i< count($categorias); $i++){
                    if($i==0){
                        $cats = $categorias['0'];
                    } else {
                        $cats .= '|' . $categorias[$i];
                    }
                }

                $subcats = '';

            } else if($tipoSel == 2){
                for($i=0; $i< count($subcategorias); $i++){
                    if($i==0){
                        $subcats = $subcategorias['0'];
                    } else {
                        $subcats .= '|' . $subcategorias[$i];
                    }
                }

                $cats ='';

            } else {
                $subcats='';
                $cats ='';
            }





            /* BEGIN Save Estou com vontade */
            $resSaveEstouVontade = self::saveEditedEstouVontade4Manager($agenda_id, $nome_PT, $nome_EN, $nome_FR, $nome_DE, $cats, $subcats, $estado);
            /* END Save Estou com vontade */

            if (empty($resSaveEstouVontade)) {
                $db->transactionRollback();
                return false;
            }


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('estouComVontade_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_AGENDA_ESTOUCOMVONTADE_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_AGENDA_ESTOUCOMVONTADE_EVENTLOG_CREATED') . 'ref=' . $agenda_id;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_AGENDA_ESTOUCOMVONTADE_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function saveEditedEstouVontade4Manager($agenda_id, $nome_PT, $nome_EN, $nome_FR, $nome_DE, $cats, $subcats, $estado){
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editestoucomvontade4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        $db     = JFactory::getDbo();

        $query  = $db->getQuery(true);
        $fields = array();

        if(!is_null($nome_PT))          array_push($fields, 'nome_PT="'.$db->escape($nome_PT).'"');
        if(!is_null($nome_EN))   array_push($fields, 'nome_EN="'.$db->escape($nome_EN).'"');
        if(!is_null($nome_FR))             array_push($fields, 'nome_FR="'.$db->escape($nome_FR).'"');
        if(!is_null($nome_DE))   array_push($fields, 'nome_DE="'.$db->escape($nome_DE).'"');
        array_push($fields, 'categorias="' . $db->escape($cats) . '"');
        array_push($fields, 'subcategorias="' . $db->escape($subcats) . '"');
        if(!is_null($nome_DE))   array_push($fields, 'estado="'.$db->escape($estado).'"');


        $data_alteracao = date("Y-m-d H:i:s");
        array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

        if(sizeof($fields)<=0) return(true); // Nada para atualizar...

        $query
            ->update('#__virtualdesk_v_agenda_estouVontade')
            ->set($fields)
            ->where(' id = '.$db->escape($agenda_id));

        $db->setQuery($query);
        $result = (boolean) $db->execute();

        return($result);
    }


    public static function getAgendaListEstouComVontade4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'listestoucomvontade4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','listestoucomvontade4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado';
            }
            else if($lang='en_EN'){
                $EstadoName = 'estado';
            }
            else if($lang='de_DE'){
                $EstadoName = 'estado';
            }
            else if($lang='fr_FR'){
                $EstadoName = 'estado';
            }
            else {
                $EstadoName = 'estado';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewestoucomvontade4manager&agenda_id=');

                $table  = " ( SELECT a.id as id, a.id as agenda_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.nome_PT as nome_PT, a.nome_EN as nome_EN, a.nome_FR as nome_FR";
                $table .= " , a.nome_DE as nome_DE, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_estouVontade as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_estado AS e ON e.id = a.estado";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',                'dt' => 0 ),
                    array( 'db' => 'nome_PT',            'dt' => 1 ),
                    array( 'db' => 'nome_EN',            'dt' => 2 ),
                    array( 'db' => 'nome_FR',            'dt' => 3 ),
                    array( 'db' => 'nome_DE',            'dt' => 4 ),
                    array( 'db' => 'estado',            'dt' => 5 ),
                    array( 'db' => 'dummy',             'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',          'dt' => 7 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getAgendaViewEstouComVontade4ManagerDetail ($IdAgenda)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'viewestoucomvontade4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','viewestoucomvontade4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdAgenda) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as agenda_id', 'a.nome_PT as nome_PT', 'a.nome_EN as nome_EN', 'a.nome_FR as nome_FR', 'a.nome_DE as nome_DE', 'a.categorias as categorias', 'a.subcategorias as subcategorias', 'a.estado as idestado', 'e.estado as estado'))
                ->join('LEFT', '#__virtualdesk_v_agenda_estado AS e ON e.id = a.estado')
                ->from("#__virtualdesk_v_agenda_estouVontade as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdAgenda)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega dados (objecto) do ESTADO atual do tema estou com vontade em questão */
    public static function getAgendaEstouComvontadeEstadoAtualObject ($lang, $id_agenda)
    {
        if((int) $id_agenda<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id as id_estado','a.estado as estado') )
                ->from("#__virtualdesk_v_agenda_estado as a")
                ->where(" a.id = ". $db->escape($id_agenda))
            );
            $dataReturn = $db->loadObject();

            if(empty($dataReturn)) return false;

            $obj= new stdClass();
            $obj->id_estado =  $dataReturn->id_estado;
            $obj->name = $dataReturn->estado;

            $obj->cssClass = self::getAgendaEstadoCSS($dataReturn->id_estado);

            return($obj);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Grava dados para definir um novo ESTADO da categoria de Agenda  */
    public static function saveAlterarEstouComvontade2NewEstado4ManagerByAjax($getInputAgenda_id, $NewEstadoId, $NewEstadoDesc)
    {
        $config = JFactory::getConfig();
        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editestoucomvontade4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers'); // Ver se tem acesso ao botão
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewEstadoId) ) return false;
        if((int) $getInputAgenda_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;
        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        $data = array();
        $data['estado'] = $NewEstadoId;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();

        $Table = new VirtualDeskTableAgendaEstouComVontade($db);
        $Table->load(array('id'=>$getInputAgenda_id));

        // Só alterar se o estado for diferente
        if((int)$Table->estado == (int)$NewEstadoId )  return true;

        if(!empty($data)) {
        }

        $db->transactionStart();

        try {
            // Store the data.
            if (!$Table->save($data)) {
                $db->transactionRollback();
                return false;
            }

            // Envia para o histório
            $TableHist  = new VirtualDeskTableAgendaEstadoHistorico($db);
            $dataHist = array();
            $dataHist['id_estado']  = $NewEstadoId;
            $dataHist['id_agenda']  = $getInputAgenda_id;
            $dataHist['descricao']  = $NewEstadoDesc;
            $dataHist['createdby']  = $UserJoomlaID;
            $dataHist['modifiedby'] = $UserJoomlaID;

            // Store the data.
            if (!$TableHist->save($dataHist)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Agenda_Log_Geral_Enabled');
            $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
            $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
            $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
            $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
            $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_alerta=' . $dataHist['id_alerta'];
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function getConcelho($IdDistrito)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','concelho') )
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->where($db->quoteName('id_distrito') . "='" . $db->escape($IdDistrito) . "'")
            ->order('concelho ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getConcelhoName($concelho)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( 'concelho')
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludeConcelho($IdDistrito, $concelho)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','concelho') )
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
            ->where($db->quoteName('id_distrito') . "='" . $db->escape($IdDistrito) . "'")
            ->order('concelho ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getFreguesia($idwebsitelist)
    {
        if (empty($idwebsitelist)) return false;

        if($onAjaxVD=0) {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia as name, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
        }
        else{
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia as name, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
        }

        return ($data);
    }


    public static function excludeFreguesia($concelho, $freguesia2){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia, concelho_id'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
            ->where($db->quoteName('id') . "!='" . $db->escape($freguesia2) . "'")
            ->order('freguesia ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getFregName($freguesia)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('freguesia')
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getCategoriaEmpresa()
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','name_PT as categoria') )
            ->from("#__virtualdesk_v_agenda_Empresas_Categorias")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->order('name_PT ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getCategoriaEmpresaName ($categoria)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( 'name_PT')
            ->from("#__virtualdesk_v_agenda_Empresas_Categorias")
            ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludeCategoriaEmpresa($categoria)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','name_PT as categoria') )
            ->from("#__virtualdesk_v_agenda_Empresas_Categorias")
            ->where($db->quoteName('id') . "!='" . $db->escape($categoria) . "'")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->order('name_PT ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getSubcategoriaEmpresa($idwebsitelist)
    {
        if (empty($idwebsitelist)) return false;

        if($onAjaxVD=0) {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, name_PT as name, cat_id'))
                ->from("#__virtualdesk_v_agenda_Empresas_Subcategorias")
                ->where($db->quoteName('cat_id') . '=' . $db->escape($idwebsitelist))
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
        }
        else{
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, name_PT as name, cat_id'))
                ->from("#__virtualdesk_v_agenda_Empresas_Subcategorias")
                ->where($db->quoteName('cat_id') . '=' . $db->escape($idwebsitelist))
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
        }

        return ($data);
    }


    public static function excludeSubcategoriaEmpresa($categoria, $subcategoria){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_PT as name, cat_id'))
            ->from("#__virtualdesk_v_agenda_Empresas_Subcategorias")
            ->where($db->quoteName('cat_id') . "='" . $db->escape($categoria) . "'")
            ->where($db->quoteName('id') . "!='" . $db->escape($subcategoria) . "'")
            ->order('name_PT ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getSubcategoriaEmpresaName($subcategoria)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('name_PT')
            ->from("#__virtualdesk_v_agenda_Empresas_Subcategorias")
            ->where($db->quoteName('id') . "='" . $db->escape($subcategoria) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getTipoEventos()
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','tag as tipoEvento') )
            ->from("#__virtualdesk_v_agenda_Empresas_tags")
            ->order('id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function checkRefId_4User_By_NIF_Capa ($RefId)
    {
        if( empty($RefId) )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia as refid' ))
                ->from("#__virtualdesk_v_agenda_Empresas_FotoCapa as a")
                ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn->refid);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function checkRefId_4User_By_NIF_Logo ($RefId)
    {
        if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia as refid' ))
                ->from("#__virtualdesk_v_agenda_Empresas_Logo as a")
                ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn->refid);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function checkRefId_4User_By_NIF_Galeria ($RefId)
    {
        if( empty($RefId) )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia as refid' ))
                ->from("#__virtualdesk_v_agenda_Empresas_Galeria as a")
                ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn->refid);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 20);
    }


    public static function CheckReferencia($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('referencia')
            ->from("#__virtualdesk_v_agenda_Empresas")
            ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function createEmpresas4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
       * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
       */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnewEmpresas4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        try {
            $refExiste = 1;

            $nomeEmpresa = $data['nomeEmpresa'];
            $designacaoComercial = $data['designacaoComercial'];
            $nipcEmpresa = $data['nipcEmpresa'];
            $emailContacto = $data['emailContacto'];
            $descricao = $data['descricao'];
            $categoria = $data['categoria'];
            $subcategoria = $data['subcategoria'];
            $checkboxHide1 = $data['checkboxHide1'];
            $checkboxHide2 = $data['checkboxHide2'];
            $checkboxHide3 = $data['checkboxHide3'];
            $checkboxHide4 = $data['checkboxHide4'];
            $checkboxHide5 = $data['checkboxHide5'];
            $checkboxHide6 = $data['checkboxHide6'];
            $checkboxHide7 = $data['checkboxHide7'];
            $checkboxHide8 = $data['checkboxHide8'];
            $checkboxHide9 = $data['checkboxHide9'];
            $checkboxHide10 = $data['checkboxHide10'];
            $outroTipoEvento = $data['outroTipoEvento'];
            $concelho = $data['concelho'];
            $freguesia = $data['freguesia'];
            $morada = $data['morada'];
            $coordenadas = $data['coordenadas'];
            $telef = $data['telef'];
            $website = $data['website'];
            $emailComercial = $data['emailComercial'];
            $emailAdministrativo = $data['emailAdministrativo'];
            $facebook = $data['facebook'];
            $instagram = $data['instagram'];
            $youtube = $data['youtube'];
            $twitter = $data['twitter'];
            $estado = '2';


            for($i=0; $i< count($subcategoria); $i++){
                if($i==0){
                    $subcats = $subcategoria['0'];
                } else {
                    $subcats .= ',' . $subcategoria[$i];
                }
            }


            $vdFileUpChangedCapa = $data['vdFileUpChangedCapa'];
            $vdFileUpChangedLogo = $data['vdFileUpChangedLogo'];
            $vdFileUpChangedGaleria = $data['vdFileUpChangedGaleria'];


            if($checkboxHide1 == 1){
                $tipoEventos = '1';
            }

            if($checkboxHide2 == 2){
                if(!empty($idiomasFalados)){
                    $tipoEventos .= ';2';
                } else {
                    $tipoEventos = '2';
                }
            }

            if($checkboxHide3 == 3){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';3';
                } else {
                    $tipoEventos = '3';
                }
            }

            if($checkboxHide4 == 4){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';4';
                } else {
                    $tipoEventos = '4';
                }
            }

            if($checkboxHide5 == 5){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';5';
                } else {
                    $tipoEventos = '5';
                }
            }

            if($checkboxHide6 == 6){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';6';
                } else {
                    $tipoEventos = '6';
                }
            }

            if($checkboxHide7 == 7){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';7';
                } else {
                    $tipoEventos = '7';
                }
            }

            if($checkboxHide8 == 8){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';8';
                } else {
                    $tipoEventos = '8';
                }
            }

            if($checkboxHide9 == 9){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';9';
                } else {
                    $tipoEventos = '9';
                }
            }

            if($checkboxHide10 == 10){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';10';
                } else {
                    $tipoEventos = '10';
                }
            } else {
                $outroTipoEvento = '';
            }

            $splitCoord = explode(",", $coordenadas);
            $lat = $splitCoord[0];
            $long = $splitCoord[1];


            $referencia = '';
            while($refExiste == 1){
                $random = self::random_code();

                $referencia = $random;
                $checkREF = self::CheckReferencia($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }

            $resSaveEmpresa = self::saveNewEmpresa4Manager($referencia, $nomeEmpresa, $designacaoComercial, $nipcEmpresa, $emailContacto, $descricao, $categoria, $subcats, $tipoEventos, $outroTipoEvento, $concelho, $freguesia, $morada, $lat, $long, $telef, $website, $emailComercial, $emailAdministrativo, $facebook, $instagram, $youtube, $twitter, $estado);

            if (empty($resSaveEmpresa)) return false;

            $dirServicosFiles               =  new VirtualDeskSiteVAgendaEmpresasFotoCapaHelper();
            $dirServicosFiles->tagprocesso  = 'CAPA_POST';
            $dirServicosFiles->idprocesso   = $referencia;
            $resFileSave                    = $dirServicosFiles->saveListFileByPOST('fileuploadCapa');

            $dirServicosLogoFiles               =  new VirtualDeskSiteVAgendaEmpresasLogosHelper();
            $dirServicosLogoFiles->tagprocesso  = 'LOGO_POST';
            $dirServicosLogoFiles->idprocesso   = $referencia;
            $resLogoSave                        = $dirServicosLogoFiles->saveListFileByPOST('fileuploadLogo');

            $dirServicosGaleriaFiles               =  new VirtualDeskSiteVAgendaEmpresasGaleriaHelper();
            $dirServicosGaleriaFiles->tagprocesso  = 'GALERIA_POST';
            $dirServicosGaleriaFiles->idprocesso   = $referencia;
            $resGaleriaSave                        = $dirServicosGaleriaFiles->saveListFileByPOST('fileuploadGaleria');

            if ($resFileSave===false || $resLogoSave===false || $resGaleriaSave===false) {
                JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function saveNewEmpresa4Manager($referencia, $nomeEmpresa, $designacaoComercial, $nipcEmpresa, $emailContacto, $descricao, $categoria, $subcats, $tipoEventos, $outroTipoEvento, $concelho, $freguesia, $morada, $lat, $long, $telef, $website, $emailComercial, $emailAdministrativo, $facebook, $instagram, $youtube, $twitter, $estado){
        $db = JFactory::getDbo();

        $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);

        $query = $db->getQuery(true);
        $columns = array('referencia', 'nome_PT', 'nome_comercial', 'nipc', 'email_contacto', 'descricao', 'categoria', 'subcategoria', 'tipoEventos', 'outroTipoEventos', 'concelho', 'freguesia', 'morada', 'latitude', 'longitude', 'telefone', 'website', 'email_comercial', 'email_administrativo', 'facebook', 'instagram', 'youtube', 'twitter', 'estado');
        $values = array($db->quote($referencia), $db->quote($nomeEmpresa), $db->quote($designacaoComercial), $db->quote($nipcEmpresa), $db->quote($emailContacto), $db->quote($descricao), $db->quote($categoria), $db->quote($subcats), $db->quote($tipoEventos), $db->quote($outroTipoEvento), $db->quote($concelho), $db->quote($freguesia), $db->quote($morada), $db->quote($lat), $db->quote($long), $db->quote($telef), $db->quote($website), $db->quote($emailComercial), $db->quote($emailAdministrativo), $db->quote($facebook), $db->quote($instagram), $db->quote($youtube), $db->quote($twitter), $db->quote($estado));
        $query
            ->insert($db->quoteName('#__virtualdesk_v_agenda_Empresas'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean)$db->execute();

        return ($result);
    }


    public static function getEstadoEmpresa(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id_estado as id','estado') )
            ->from("#__virtualdesk_v_agenda_Empresas_Estado")
            ->order('id_estado ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getEstadoEmpresaCSS ($idestado)
    {
        $defCss = 'label-default';
        switch ($idestado)
        {   case '1':
            $defCss = 'label-pendente';
            break;
            case '2':
                $defCss = 'label-publicado';
                break;
            case '3':
                $defCss = 'label-despublicado';
                break;
            case '4':
                $defCss = 'label-rejeitado';
                break;
        }
        return ($defCss);
    }


    public static function getVAgendaEmpresasList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'listEmpresas4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','listEmpresas4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewempresas4manager&empresa_id=');

                $table  = " ( SELECT a.id as id, a.id as empresa_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.nipc as nipc, a.categoria as idcategoria, b.name_PT as categoria, a.freguesia as idfreguesia, c.freguesia as freguesia, a.estado as idestado, d.estado as estado, e.concelho as concelho, a.concelho as idconcelho, a.nome_comercial as nome, a.data_criacao as data_criacao";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_Empresas as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_Empresas_Categorias AS b ON b.id = a.categoria";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_Empresas_Estado AS d ON d.id_estado = a.estado";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_concelhos AS e ON e.id = a.concelho";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'nipc',                  'dt' => 0 ),
                    array( 'db' => 'nome',                  'dt' => 1 ),
                    array( 'db' => 'categoria',             'dt' => 2 ),
                    array( 'db' => 'concelho',              'dt' => 3 ),
                    array( 'db' => 'freguesia',             'dt' => 4 ),
                    array( 'db' => 'estado',                'dt' => 5 ),
                    array( 'db' => 'dummy',                 'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'empresa_id',            'dt' => 7 ),
                    array( 'db' => 'idcategoria',           'dt' => 8 ),
                    array( 'db' => 'idconcelho',            'dt' => 9 ),
                    array( 'db' => 'idfreguesia',           'dt' => 10 ),
                    array( 'db' => 'idestado',              'dt' => 11 ),
                    array( 'db' => 'data_criacao',          'dt' => 12 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEmpresaDetail4Manager($id){
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'editEmpresas4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','editEmpresas4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($id) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id', 'a.referencia as codigo', 'a.nome_PT as nome_empresa', 'a.nome_comercial', 'a.nipc', 'a.email_contacto', 'a.descricao',
                    'a.categoria as id_categoria', 'c.name_PT as categoria', 'a.subcategoria', 'a.tipoEventos as tipoEventos', 'a.outroTipoEventos as outroTipoEventos',
                    'a.concelho as id_concelho', 'd.concelho as concelho', 'a.freguesia as id_freguesia', 'e.freguesia as freguesia', 'a.morada', 'a.latitude', 'a.longitude', 'a.telefone',
                    'a.website', 'a.email_comercial', 'a.email_administrativo', 'a.facebook', 'a.instagram', 'a.youtube', 'a.twitter', 'a.estado as id_estado', 'g.estado as estado', 'a.data_criacao', 'a.data_alteracao'))
                ->join('LEFT', '#__virtualdesk_v_agenda_Empresas_Categorias AS c ON c.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS d ON d.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS e ON e.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_Empresas_Estado AS g ON g.id_estado = a.estado')
                ->from("#__virtualdesk_v_agenda_Empresas as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getTipoEventoName($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( 'tag')
            ->from("#__virtualdesk_v_agenda_Empresas_tags")
            ->where($db->quoteName('id') . "='" . $db->escape($id) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getEstadoEmpresasAllOptions ($lang, $displayPermError=true)
    {
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
        if($vbHasAccess===false ) {
            if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id_estado as id','estado as name') )
                ->from("#__virtualdesk_v_agenda_Empresas_Estado")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->name
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function saveAlterar2NewEstadoEmpresa4ManagerByAjax($getInputEmpresa_id, $NewEstadoId, $NewEstadoDesc)
    {
        $config = JFactory::getConfig();
        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editEmpresas4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers'); // Ver se tem acesso ao botão
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewEstadoId) ) return false;
        if((int) $getInputEmpresa_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;
        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        $data = array();
        $data['estado'] = $NewEstadoId;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();

        $Table = new VirtualDeskTableVAgendaEmpresas($db);
        $Table->load(array('id'=>$getInputEmpresa_id));

        // Só alterar se o estado for diferente
        if((int)$Table->estado == (int)$NewEstadoId )  return true;

        if(!empty($data)) {

        }

        $db->transactionStart();

        try {
            // Store the data.
            if (!$Table->save($data)) {
                $db->transactionRollback();
                return false;
            }

            // Envia para o histório
            $TableHist  = new VirtualDeskTableVAgendaEmpresasEstadoHistorico($db);
            $dataHist = array();
            $dataHist['id_estado']  = $NewEstadoId;
            $dataHist['id_empresa']  = $getInputEmpresa_id;
            $dataHist['descricao']  = $NewEstadoDesc;
            $dataHist['createdby']  = $UserJoomlaID;
            $dataHist['modifiedby'] = $UserJoomlaID;

            // Store the data.
            if (!$TableHist->save($dataHist)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('V_Agenda_Empresas_Log_Geral_Enabled');


            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EVENTLOG_STATE_ALTERADO');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_empresa=' . $dataHist['id_empresa'];
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EVENTLOG_STATE_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updateEmpresas4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editEmpresas4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $empresa_id = $data['empresa_id'];
        if((int) $empresa_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableVAgendaEmpresas($db);
        $Table->load(array('id'=>$empresa_id));

        $db->transactionStart();

        try {
            $referencia          = $Table->referencia;
            $obParam    = new VirtualDeskSiteParamsHelper();

            $id = null;
            $referencia = null;
            $nome_PT = null;
            $nome_comercial = null;
            $nipc = null;
            $email_contacto = null;
            $descricao = null;
            $categoria = null;
            $subcategoria = null;
            $tipoEventos = null;
            $outroTipoEventos = null;
            $concelho = null;
            $freguesia = null;
            $morada = null;
            $latitude = null;
            $longitude = null;
            $telefone = null;
            $website = null;
            $email_comercial = null;
            $email_administrativo = null;
            $facebook = null;
            $instagram = null;
            $youtube = null;
            $twitter = null;
            $estado = null;
            $data_criacao = null;
            $data_alteracao = null;

            if(array_key_exists('referencia',$data))                $referencia = $data['referencia'];
            if(array_key_exists('nomeEmpresa',$data))               $nome_PT = $data['nomeEmpresa'];
            if(array_key_exists('designacaoComercial',$data))       $nome_comercial = $data['designacaoComercial'];
            if(array_key_exists('nipcEmpresa',$data))               $nipc = $data['nipcEmpresa'];
            if(array_key_exists('emailContacto',$data))             $emailContacto = $data['emailContacto'];
            if(array_key_exists('checkboxHide1',$data))             $checkboxHide1 = $data['checkboxHide1'];
            if(array_key_exists('checkboxHide2',$data))             $checkboxHide2 = $data['checkboxHide2'];
            if(array_key_exists('checkboxHide3',$data))             $checkboxHide3 = $data['checkboxHide3'];
            if(array_key_exists('checkboxHide4',$data))             $checkboxHide4 = $data['checkboxHide4'];
            if(array_key_exists('checkboxHide5',$data))             $checkboxHide5 = $data['checkboxHide5'];
            if(array_key_exists('checkboxHide6',$data))             $checkboxHide6 = $data['checkboxHide6'];
            if(array_key_exists('checkboxHide7',$data))             $checkboxHide7 = $data['checkboxHide7'];
            if(array_key_exists('checkboxHide8',$data))             $checkboxHide8 = $data['checkboxHide8'];
            if(array_key_exists('checkboxHide9',$data))             $checkboxHide9 = $data['checkboxHide9'];
            if(array_key_exists('checkboxHide10',$data))            $checkboxHide10 = $data['checkboxHide10'];
            if(array_key_exists('outroTipoEvento',$data))           $outroTipoEventos = $data['outroTipoEvento'];
            if(array_key_exists('descricao',$data))                 $descricao = $data['descricao'];
            if(array_key_exists('categoria',$data))                 $categoria = $data['categoria'];
            if(array_key_exists('subcategoria',$data))              $subcategoria = $data['subcategoria'];
            if(array_key_exists('concelho',$data))                  $concelho = $data['concelho'];
            if(array_key_exists('freguesia',$data))                 $freguesia = $data['freguesia'];
            if(array_key_exists('morada',$data))                    $morada = $data['morada'];
            if(array_key_exists('coordenadas',$data))               $coordenadas = $data['coordenadas'];
            if(array_key_exists('telef',$data))                     $telefone = $data['telef'];
            if(array_key_exists('website',$data))                   $website = $data['website'];
            if(array_key_exists('emailComercial',$data))            $email_comercial = $data['emailComercial'];
            if(array_key_exists('emailAdministrativo',$data))       $email_administrativo = $data['emailAdministrativo'];
            if(array_key_exists('facebook',$data))                  $facebook = $data['facebook'];
            if(array_key_exists('instagram',$data))                 $instagram = $data['instagram'];
            if(array_key_exists('youtube',$data))                   $youtube = $data['youtube'];
            if(array_key_exists('twitter',$data))                   $twitter = $data['twitter'];


            for($i=0; $i< count($subcategoria); $i++){
                if($i==0){
                    $subcats = $subcategoria['0'];
                } else {
                    $subcats .= ',' . $subcategoria[$i];
                }
            }

            if($checkboxHide1 == 1){
                $tipoEventos = '1';
            }

            if($checkboxHide2 == 2){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';2';
                } else {
                    $tipoEventos = '2';
                }
            }

            if($checkboxHide3 == 3){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';3';
                } else {
                    $tipoEventos = '3';
                }
            }

            if($checkboxHide4 == 4){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';4';
                } else {
                    $tipoEventos = '4';
                }
            }

            if($checkboxHide5 == 5){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';5';
                } else {
                    $tipoEventos = '5';
                }
            }

            if($checkboxHide6 == 6){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';6';
                } else {
                    $tipoEventos = '6';
                }
            }

            if($checkboxHide7 == 7){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';7';
                } else {
                    $tipoEventos = '7';
                }
            }

            if($checkboxHide8 == 8){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';8';
                } else {
                    $tipoEventos = '8';
                }
            }

            if($checkboxHide9 == 9){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';9';
                } else {
                    $tipoEventos = '9';
                }
            }

            if($checkboxHide10 == 10){
                if(!empty($tipoEventos)){
                    $tipoEventos .= ';10';
                } else {
                    $tipoEventos = '10';
                }
            } else {
                $outroTipoEventos = '';
            }

            $splitCoord = explode(",", $coordenadas);
            $lat = $splitCoord[0];
            $long = $splitCoord[1];


            /* BEGIN Save Empresa */
            $resSaveEmpresa = self::saveEditedDiretorioServicos4Manager($empresa_id, $referencia, $nome_PT, $nome_comercial, $nipc, $emailContacto, $descricao, $categoria, $subcats, $tipoEventos,
                $outroTipoEventos, $concelho, $freguesia, $morada, $lat, $long, $telefone, $website, $email_comercial, $email_administrativo, $facebook, $instagram, $youtube, $twitter, $estado);
            /* END Save Empresa */

            if (empty($resSaveEmpresa)) {
                $db->transactionRollback();
                return false;
            }

            /* DELETE - FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaEmpresasFotoCapaHelper();
            $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadCapa');
            $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
            if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar a foto de capa da empresa', 'error');
            }


            $objLogoFiles                   = new VirtualDeskSiteVAgendaEmpresasLogosHelper();
            $listFileGaleria2Eliminar       = $objLogoFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadLogo');
            $resFileLogoDelete              = $objLogoFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
            if ($resFileLogoDelete==false && !empty($listFileLogo2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar o logo da empresa', 'error');
            }


            $objGaleriaFiles                = new VirtualDeskSiteVAgendaEmpresasGaleriaHelper();
            $listFileGaleria2Eliminar       = $objGaleriaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadGaleria');
            $resFileGaleriaDelete           = $objGaleriaFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
            if ($resFileGaleriaDelete==false && !empty($listFileGaleria2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de galeria da empresa', 'error');
            }

            /* FILES */
            $EmpresaFiles                   =  new VirtualDeskSiteVAgendaEmpresasFotoCapaHelper();
            $EmpresaFiles->tagprocesso      = 'CAPA_POST';
            $EmpresaFiles->idprocesso       = $referencia;
            $resFileSave                    = $EmpresaFiles->saveListFileByPOST('fileuploadCapa');

            $EmpresaLogoFiles                   =  new VirtualDeskSiteVAgendaEmpresasLogosHelper();
            $EmpresaLogoFiles->tagprocesso      = 'LOGO_POST';
            $EmpresaLogoFiles->idprocesso       = $referencia;
            $resLogoSave                        = $EmpresaLogoFiles->saveListFileByPOST('fileuploadLogo');

            $EmpresaGaleriaFiles                =  new VirtualDeskSiteVAgendaEmpresasGaleriaHelper();
            $EmpresaGaleriaFiles->tagprocesso   = 'GALERIA_POST';
            $EmpresaGaleriaFiles->idprocesso    = $referencia;
            $resGaleriaSave                     = $EmpresaGaleriaFiles->saveListFileByPOST('fileuploadGaleria');


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('DiretorioServicos_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EVENTLOG_CREATED') . 'ref=' . $empresa_id;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function saveEditedDiretorioServicos4Manager($empresa_id, $referencia, $nome_PT, $nome_comercial, $nipc, $emailContacto, $descricao, $categoria, $subcats, $tipoEventos,
                                                               $outroTipoEventos, $concelho, $freguesia, $morada, $lat, $long, $telefone, $website, $email_comercial, $email_administrativo,
                                                               $facebook, $instagram, $youtube, $twitter, $estado){
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editEmpresas4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        $db     = JFactory::getDbo();

        $query  = $db->getQuery(true);
        $fields = array();

        if(!is_null($nome_PT))          array_push($fields, 'nome_PT="'.$db->escape($nome_PT).'"');
        if(!is_null($nome_comercial))   array_push($fields, 'nome_comercial="'.$db->escape($nome_comercial).'"');
        if(!is_null($nipc))             array_push($fields, 'nipc="'.$db->escape($nipc).'"');
        if(!is_null($emailContacto))   array_push($fields, 'email_contacto="'.$db->escape($emailContacto).'"');
        if(!is_null($descricao)) {
            $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);
            array_push($fields, 'descricao="'.$db->escape($descricao).'"');
        }
        if(!is_null($categoria)){
            array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(is_null($subcats)) {
                $subcats = '';
                array_push($fields, 'subcategoria="' . $db->escape($subcats) . '"');
            }else {
                array_push($fields, 'subcategoria="' . $db->escape($subcats) . '"');
            }
        }

        array_push($fields, 'tipoEventos="' . $db->escape($tipoEventos) . '"');
        array_push($fields, 'outroTipoEventos="' . $db->escape($outroTipoEventos) . '"');

        if(!is_null($concelho)){
            array_push($fields, 'concelho="'.$db->escape($concelho).'"');
            if(is_null($freguesia)){
                array_push($fields, 'freguesia="'.$db->escape($freguesia).'"');
            }
        }
        if(!is_null($freguesia))            array_push($fields, 'freguesia="'.$db->escape($freguesia).'"');
        if(!is_null($morada))               array_push($fields, 'morada="'.$db->escape($morada).'"');
        array_push($fields, 'latitude="'.$db->escape($lat).'"');
        array_push($fields, 'longitude="'.$db->escape($long).'"');
        if(!is_null($telefone))             array_push($fields, 'telefone="'.$db->escape($telefone).'"');
        if(!is_null($website))              array_push($fields, 'website="'.$db->escape($website).'"');
        if(!is_null($email_comercial))      array_push($fields, 'email_comercial="'.$db->escape($email_comercial).'"');
        if(!is_null($email_administrativo)) array_push($fields, 'email_administrativo="'.$db->escape($email_administrativo).'"');

        if(!is_null($facebook)) array_push($fields, 'facebook="'.$db->escape($facebook).'"');
        if(!is_null($instagram)) array_push($fields, 'instagram="'.$db->escape($instagram).'"');
        if(!is_null($youtube)) array_push($fields, 'youtube="'.$db->escape($youtube).'"');
        if(!is_null($twitter)) array_push($fields, 'twitter="'.$db->escape($twitter).'"');

        $data_alteracao = date("Y-m-d H:i:s");
        array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

        if(sizeof($fields)<=0) return(true); // Nada para atualizar...

        $query
            ->update('#__virtualdesk_v_agenda_Empresas')
            ->set($fields)
            ->where(' id = '.$db->escape($empresa_id));

        $db->setQuery($query);
        $result = (boolean) $db->execute();

        return($result);
    }




    public static function getEstadoPatrocinador(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id_estado as id','estado') )
            ->from("#__virtualdesk_v_agenda_Patrocinadores_Estado")
            ->order('id_estado ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getStatePatrocinadorSelect($estado){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('a.estado')
            ->from("#__virtualdesk_v_agenda_Patrocinadores_Estado as a")
            ->where($db->quoteName('id_estado') . "='" . $db->escape($estado) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function excludeStatePatrocinador($estado){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id_estado as id','estado') )
            ->from("#__virtualdesk_v_agenda_Patrocinadores_Estado")
            ->where($db->quoteName('id_estado') . "!='" . $db->escape($estado) . "'")
            ->order('id_estado ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getEstadoPatrocinadorCSS ($idestado)
    {
        $defCss = 'label-default';
        switch ($idestado)
        {   case '1':
            $defCss = 'label-pendente';
            break;
            case '2':
                $defCss = 'label-publicado';
                break;
            case '3':
                $defCss = 'label-despublicado';
                break;
            case '4':
                $defCss = 'label-rejeitado';
                break;
        }
        return ($defCss);
    }

    public static function createPatrocinador4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
          * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
          */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnewpatrocinador4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {

            $nifpatrocinador    = $data['nifpatrocinador'];
            $nomepatrocinador   = $data['nomepatrocinador'];
            $urlpatrocinador    = $data['urlpatrocinador'];
            $dataAtual          = date("Y-m-d");

            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia       = VirtualDeskSiteVAgendaHelper::random_code_patrocinador();
                $checkREF = VirtualDeskSiteVAgendaHelper::CheckReferenciaPatrocinador($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }


            $urlHttpsWebsite = explode("https://", $urlpatrocinador);
            if (count($urlHttpsWebsite) == 1) {
                $hasLinkWebsite = 0;
                $urlHttpWebsite = explode("http://", $urlpatrocinador);
                if (count($urlHttpWebsite) == 1) {
                    $hasLinkWebsite = 0;
                } else {
                    $hasLinkWebsite = 1;
                }
            } else {
                $hasLinkWebsite = 1;
            }

            if (!empty($urlpatrocinador)) {
                if ($hasLinkWebsite == 0) {
                    $urlWebsite = 'https://' . $urlpatrocinador;
                } else {
                    $urlWebsite = $urlpatrocinador;
                }
            } else {
                $urlWebsite = '';
            }


            $resSavePatrocinador = VirtualDeskSiteVAgendaHelper::saveNewPatrocinador($referencia, $nifpatrocinador, $nomepatrocinador, $urlWebsite);

            if (empty($resSavePatrocinador)) {
                $db->transactionRollback();
                return false;
            }

            /* FILES */
            $objCapaFiles                = new VirtualDeskSiteVAgendaPatrocinadoresLogosHelper();
            $objCapaFiles->tagprocesso   = 'V_AGENDA_PATROCINADORES_POST';
            $objCapaFiles->idprocesso    = $referencia;
            $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_logo');


            if ($resFileSaveCapa===false) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                return false;
            }

            $db->transactionCommit();


            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Patrocinador_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_PATROCINADORES_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function getVAgendaPatrocinadoresList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'listpatrocinador4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','listpatrocinador4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewpatrocinadores4manager&patrocinador_id=');

                $table  = " ( SELECT a.id as id, a.id as patrocinador_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.nif, a.referencia, a.nome, a.url, a.estado as idestado, b.estado as estado, a.data_criacao as data_criacao";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_Patrocinadores as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_Patrocinadores_Estado AS b ON b.id_estado = a.estado";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'referencia',        'dt' => 0 ),
                    array( 'db' => 'nif',               'dt' => 1 ),
                    array( 'db' => 'nome',              'dt' => 2 ),
                    array( 'db' => 'url',               'dt' => 3 ),
                    array( 'db' => 'estado',            'dt' => 4 ),
                    array( 'db' => 'dummy',             'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'patrocinador_id',   'dt' => 6 ),
                    array( 'db' => 'idestado',          'dt' => 7 ),
                    array( 'db' => 'data_criacao',      'dt' => 8 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getEstadoPatrocinadorAllOptions ($lang, $displayPermError=true)
    {
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
        if((int)$vbCheckModule==0)  return false;

        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            if($displayPermError!=false) JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','estado') )
                ->from("#__virtualdesk_v_agenda_Patrocinadores_Estado")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->estado;
                }
                else {
                    $rowName = $row->estado;
                }
                $response[] = array(
                    'id' => $row->id,
                    'name' => $rowName
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getPatrocinadorDetail4Manager($id){
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'editpatrocinador4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','editpatrocinador4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($id) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id', 'a.referencia as codigo', 'a.nome', 'a.url', 'a.nif', 'a.estado as id_estado', 'b.estado as estado', 'a.data_criacao', 'a.data_alteracao'))
                ->join('LEFT', '#__virtualdesk_v_agenda_Patrocinadores_Estado AS b ON b.id_estado = a.estado')
                ->from("#__virtualdesk_v_agenda_Patrocinadores as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function updatePatrocinador4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editpatrocinador4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $patrocinador_id = $data['patrocinador_id'];
        if((int) $patrocinador_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableVAgendaPatrocinadores($db);
        $Table->load(array('id'=>$patrocinador_id));

        $db->transactionStart();

        try {
            $referencia = $Table->referencia;
            $obParam    = new VirtualDeskSiteParamsHelper();

            $id = null;
            $referencia = null;
            $nif = null;
            $nome = null;
            $url = null;
            $estado = null;
            $data_criacao = null;
            $data_alteracao = null;

            if(array_key_exists('referencia',$data))        $referencia = $data['referencia'];
            if(array_key_exists('nifpatrocinador',$data))   $nif = $data['nifpatrocinador'];
            if(array_key_exists('nomepatrocinador',$data))  $nome = $data['nomepatrocinador'];
            if(array_key_exists('urlpatrocinador',$data))   $url = $data['urlpatrocinador'];
            if(array_key_exists('estado',$data))            $estado = $data['estado'];


            $resSavePatrocinador = self::saveEditedPatrocinador4Manager($patrocinador_id, $referencia, $nif, $nome, $url, $estado);

            if (empty($resSavePatrocinador)) {
                $db->transactionRollback();
                return false;
            }

            /* DELETE - FILES */
            $objLogoFiles                = new VirtualDeskSiteVAgendaPatrocinadoresLogosHelper();
            $listFileLogo2Eliminar       = $objLogoFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_logo');
            $resFileLogoDelete           = $objLogoFiles->deleteFiles ($referencia , $listFileLogo2Eliminar);
            if ($resFileLogoDelete==false && !empty($listFileLogo2Eliminar) ) {
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar o logo do patrocinador', 'error');
            }


            /* FILES */
            $LogoFiles              =  new VirtualDeskSiteVAgendaPatrocinadoresLogosHelper();
            $LogoFiles->tagprocesso = 'LOGO_POST';
            $LogoFiles->idprocesso  = $referencia;
            $resFileSave            = $LogoFiles->saveListFileByPOST('fileupload_logo');


            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('Patrocinadores_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_EVENTLOG_CREATED') . 'ref=' . $patrocinador_id;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function saveEditedPatrocinador4Manager($patrocinador_id, $referencia, $nif, $nome, $url, $estado){

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editpatrocinador4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        $db     = JFactory::getDbo();

        $query  = $db->getQuery(true);
        $fields = array();

        if(!is_null($nif))          array_push($fields, 'nif="'.$db->escape($nif).'"');
        if(!is_null($nome))   array_push($fields, 'nome="'.$db->escape($nome).'"');
        if(!is_null($url)){
            $urlHttpsWebsite = explode("https://", $url);
            if (count($urlHttpsWebsite) == 1) {
                $hasLinkWebsite = 0;
                $urlHttpWebsite = explode("http://", $url);
                if (count($urlHttpWebsite) == 1) {
                    $hasLinkWebsite = 0;
                } else {
                    $hasLinkWebsite = 1;
                }
            } else {
                $hasLinkWebsite = 1;
            }

            if (!empty($url)) {
                if ($hasLinkWebsite == 0) {
                    $urlWebsite = 'https://' . $url;
                } else {
                    $urlWebsite = $url;
                }
            } else {
                $urlWebsite = '';
            }
            array_push($fields, 'url="'.$db->escape($urlWebsite).'"');
        }

        if(!is_null($estado))             array_push($fields, 'estado="'.$db->escape($estado).'"');
        $data_alteracao = date("Y-m-d H:i:s");
        array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

        if(sizeof($fields)<=0) return(true); // Nada para atualizar...

        $query
            ->update('#__virtualdesk_v_agenda_Patrocinadores')
            ->set($fields)
            ->where(' id = '.$db->escape($patrocinador_id));

        $db->setQuery($query);
        $result = (boolean) $db->execute();

        return($result);
    }


    public static function getEstadoDiasInternacionais(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id_estado as id','estado') )
            ->from("#__virtualdesk_v_agenda_dias_internacionais_estado")
            ->order('id_estado ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getEstadoDiaInternacionalName($estado){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('estado')
            ->from("#__virtualdesk_v_agenda_dias_internacionais_estado")
            ->where( $db->quoteName('id_estado') . '=' . $db->escape($estado))
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function excludeEstadoDiaInternacional($estado){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id_estado as id','estado') )
            ->from("#__virtualdesk_v_agenda_dias_internacionais_estado")
            ->where( $db->quoteName('id_estado') . '!=' . $db->escape($estado))
            ->order('id_estado ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getCategoriasDiasInternacionais(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','categoria') )
            ->from("#__virtualdesk_v_agenda_dias_internacionais_categoria")
            ->where( $db->quoteName('estado') . '=' . $db->escape('2'))
            ->order('categoria ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getCategoriasDiasInternacionaisName($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('categoria')
            ->from("#__virtualdesk_v_agenda_dias_internacionais_categoria")
            ->where( $db->quoteName('id') . '=' . $db->escape($id)  )
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function excludeCategoriasDiasInternacionais($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','categoria') )
            ->from("#__virtualdesk_v_agenda_dias_internacionais_categoria")
            ->where( $db->quoteName('estado') . '=' . $db->escape('2'))
            ->where( $db->quoteName('id') . '!=' . $db->escape($id))
            ->order('categoria ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getEstadoDiasInternacionaisCSS ($idestado)
    {
        $defCss = 'label-default';
        switch ($idestado)
        {   case '1':
            $defCss = 'label-pendente';
            break;
            case '2':
                $defCss = 'label-publicado';
                break;
            case '3':
                $defCss = 'label-despublicado';
                break;
            case '4':
                $defCss = 'label-rejeitado';
                break;
        }
        return ($defCss);
    }

    public static function getPatrocinadores(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','nome') )
            ->from("#__virtualdesk_v_agenda_Patrocinadores")
            ->where( $db->quoteName('estado') . '=' . $db->escape('2'))
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getPatrocinadorName($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nome')
            ->from("#__virtualdesk_v_agenda_Patrocinadores")
            ->where( $db->quoteName('id') . '=' . $db->escape($id)  )
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function excludePatrocinador($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id','nome') )
            ->from("#__virtualdesk_v_agenda_Patrocinadores")
            ->where( $db->quoteName('estado') . '=' . $db->escape('2'))
            ->where( $db->quoteName('id') . '!=' . $db->escape($id))
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getDiasInternacionaisList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'listdiainternacional4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','listdiainternacional4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
                $EstadoName = 'estado';
            }
            else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
                $EstadoName = 'estado';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewdiasinternacionais4manager&diainternacional_id=');

                $table  = " ( SELECT a.id as id, a.id as diainternacional_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as referencia, a.dia as dia";
                $table .= " , IFNULL(b.categoria,' ') as categoria, a.ref_patrocinador as ref_patrocinador, IFNULL(d.nome,' ') as patrocinador";
                $table .= " , c.estado as estado, a.estado as idestado , a.categoria as idcategoria, a.data_inicio as data_inicio";
                $table .= " FROM ".$dbprefix."virtualdesk_v_agenda_dias_internacionais as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_dias_internacionais_categoria AS b ON b.id = a.categoria ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_dias_internacionais_estado AS c ON c.id_estado = a.estado";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_v_agenda_Patrocinadores AS d ON d.referencia = a.ref_patrocinador";
                $table .= "  ) temp ";

                $primaryKey = 'id';


                $columns = array(
                    array( 'db' => 'data_inicio',           'dt' => 0 ),
                    array( 'db' => 'dia',                   'dt' => 1 ),
                    array( 'db' => 'categoria',             'dt' => 2 ),
                    array( 'db' => 'patrocinador',          'dt' => 3 ),
                    array( 'db' => 'estado',                'dt' => 4 ),
                    array( 'db' => 'dummy',                 'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',              'dt' => 6 ),
                    array( 'db' => 'diainternacional_id',   'dt' => 7 ),
                    array( 'db' => 'idcategoria',           'dt' => 8 ),
                    array( 'db' => 'referencia',            'dt' => 9 ),
                    array( 'db' => 'ref_patrocinador',      'dt' => 10 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function createDiasInternacionais4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
          * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
          */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnewdiainternacional4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {

            if(!empty($data['descricao_evento'])) $data['descricao_evento']     = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao_evento']), true);

            $nome_evento        = $data['nome_evento'];
            $descricao_evento   = $data['descricao_evento'];
            $categoria          = $data['categoria'];
            $data_evento        = $data['data_evento'];
            $patrocinador       = $data['patrocinador'];
            $refExiste = 1;

            $referencia = '';
            while($refExiste == 1){
                $referencia       = VirtualDeskSiteVAgendaHelper::random_code_diainternacional();
                $checkREF = VirtualDeskSiteVAgendaHelper::CheckReferenciaDiainternacional($referencia);
                if((int)$checkREF == 0){
                    $refExiste = 0;
                }
            }

            $resSaveEvento = VirtualDeskSiteVAgendaHelper::saveNewDiaInternacional($referencia, $nome_evento, $descricao_evento, $categoria, $data_evento, $patrocinador);

            if (empty($resSaveEvento)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('DiaInternacional_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_DIASINTERNACIONAIS_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DIASINTERNACIONAIS_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_DIASINTERNACIONAIS_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }

    public static function getDiasInternacionaisDetail4Manager($id){
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agenda');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agenda', 'editdiainternacional4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agenda'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agenda','editdiainternacional4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($id) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id', 'a.referencia', 'a.dia', 'a.descritivo', 'a.categoria as idCategoria', 'c.categoria as categoria', 'a.data_inicio', 'a.ref_patrocinador', 'd.nome as patrocinador', 'd.id as idPatrocinador', 'a.estado as id_estado', 'b.estado as estado', 'a.data_criacao', 'a.data_alteracao'))
                ->join('LEFT', '#__virtualdesk_v_agenda_dias_internacionais_estado AS b ON b.id_estado = a.estado')
                ->join('LEFT', '#__virtualdesk_v_agenda_dias_internacionais_categoria AS c ON c.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_v_agenda_Patrocinadores AS d ON d.referencia = a.ref_patrocinador')
                ->from("#__virtualdesk_v_agenda_dias_internacionais as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function updateDiasInternacionais4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editdiainternacional4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $diainternacional_id = $data['diainternacional_id'];
        if((int) $diainternacional_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        if (empty($data)) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableVAgendaDiasInternacionais($db);
        $Table->load(array('id'=>$diainternacional_id));

        $db->transactionStart();

        try {
            $referencia = $Table->referencia;
            $obParam    = new VirtualDeskSiteParamsHelper();

            $id = null;
            $referencia = null;
            $dia = null;
            $descritivo = null;
            $categoria = null;
            $data_inicio = null;
            $mes_inicio = null;
            $dia_inicio = null;
            $ref_patrocinador = null;
            $estado = null;
            $data_criacao = null;
            $data_alteracao = null;

            if(array_key_exists('referencia',$data))        $referencia = $data['referencia'];
            if(array_key_exists('nome_evento',$data))       $dia = $data['nome_evento'];
            if(array_key_exists('descricao_evento',$data))  $descritivo = $data['descricao_evento'];
            if(array_key_exists('categoria',$data))         $categoria = $data['categoria'];
            if(array_key_exists('data_evento',$data))       $data_inicio = $data['data_evento'];
            if(array_key_exists('patrocinador',$data))      $ref_patrocinador = $data['patrocinador'];
            if(array_key_exists('estado',$data))            $estado = $data['estado'];


            $resSaveDiasInternacionais = self::saveEditedDiasInternacionais4Manager($diainternacional_id, $referencia, $dia, $descritivo, $categoria, $data_inicio, $ref_patrocinador, $estado);

            if (empty($resSaveDiasInternacionais)) {
                $db->transactionRollback();
                return false;
            }

            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('DiasInternacionais_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_EVENTLOG_CREATED') . 'ref=' . $diainternacional_id;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }

    public static function getRefPatrocinador($patrocinador){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('referencia')
            ->from("#__virtualdesk_v_agenda_Patrocinadores")
            ->where($db->quoteName('id') . "='" . $db->escape($patrocinador) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function saveEditedDiasInternacionais4Manager($diainternacional_id, $referencia, $dia, $descritivo, $categoria, $data_inicio, $ref_patrocinador, $estado){

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editdiainternacional4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        $db     = JFactory::getDbo();

        $query  = $db->getQuery(true);
        $fields = array();

        if(!is_null($dia))          array_push($fields, 'dia="'.$db->escape($dia).'"');

        if(!is_null($descritivo)){
            $encodedDescritivo = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descritivo), true);
            array_push($fields, 'descritivo="'.$db->escape($encodedDescritivo).'"');
        }

        if(!is_null($categoria)){
            array_push($fields, 'categoria="'.$db->escape($categoria).'"');
        }

        if(!is_null($data_inicio)){
            $explodeData = explode('-',$data_inicio);
            $mes = $explodeData[1];
            $dia = $explodeData[2];
            array_push($fields, 'data_inicio="'.$db->escape($data_inicio).'"');
            array_push($fields, 'mes_inicio="'.$db->escape($mes).'"');
            array_push($fields, 'dia_inicio="'.$db->escape($dia).'"');
        }

        if(!is_null($ref_patrocinador)){
            $patrocinador = self::getRefPatrocinador($ref_patrocinador);
            array_push($fields, 'ref_patrocinador="'.$db->escape($patrocinador).'"');
        }

        if(!is_null($estado))             array_push($fields, 'estado="'.$db->escape($estado).'"');
        $data_alteracao = date("Y-m-d H:i:s");
        array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

        if(sizeof($fields)<=0) return(true); // Nada para atualizar...

        $query
            ->update('#__virtualdesk_v_agenda_dias_internacionais')
            ->set($fields)
            ->where(' id = '.$db->escape($diainternacional_id));

        $db->setQuery($query);
        $result = (boolean) $db->execute();

        return($result);
    }

    /* Limpa os dados na sessão "users states" do joomla */
    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.addnew4user.agenda.data', null);
        $app->setUserState('com_virtualdesk.addnew4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.edit4user.agenda.data', null);
        $app->setUserState('com_virtualdesk.edit4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.catlist4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.viewcat4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.editcat4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.addnewcat4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.subcatlist4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.viewsubcat4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.editsubcat4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.addnewsubcat4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.addnewestoucomvontade4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.editestoucomvontade4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.listestoucomvontade4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.viewestoucomvontade4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.addnewempresas4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.editempresas4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.listempresas4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.viewempresas4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.addnewpatrocinadores4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.editpatrocinadores4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.listpatrocinadores4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.viewpatrocinadores4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.addnewdiasinternacionais4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.editdiasinternacionais4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.listdiasinternacionais4manager.agenda.data', null);
        $app->setUserState('com_virtualdesk.viewdiasinternacionais4manager.agenda.data', null);

    }

}
?>