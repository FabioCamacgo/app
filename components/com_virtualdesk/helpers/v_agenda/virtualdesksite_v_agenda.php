<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');

    class VirtualDeskSiteVAgendaHelper{

        public static function getAgendaEstado(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.estado as estado'))
                ->from("#__virtualdesk_v_agenda_estado as a")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getestadoSelect($estado){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.estado as estado')
                ->from("#__virtualdesk_v_agenda_estado as a")
                ->where($db->quoteName('id') . "='" . $db->escape($estado) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeEstado($estado){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.estado as estado'))
                ->from("#__virtualdesk_v_agenda_estado as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($estado) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPreco(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Tipo = 'tipo_PT';
            } else if ($lang='en_EN'){
                $Tipo = 'tipo_EN';
            } else if ($lang='fr_FR'){
                $Tipo = 'tipo_FR';
            } else if ($lang='de_DE'){
                $Tipo = 'tipo_DE';
            } else {
                $Tipo = 'tipo_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $Tipo  . ' as tipo'))
                ->from("#__virtualdesk_v_agenda_tipo_evento as a")
                ->order('tipo ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPrecoSelect($preco){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Tipo = 'tipo_PT';
            } else if ($lang='en_EN'){
                $Tipo = 'tipo_EN';
            } else if ($lang='fr_FR'){
                $Tipo = 'tipo_FR';
            } else if ($lang='de_DE'){
                $Tipo = 'tipo_DE';
            } else {
                $Tipo = 'tipo_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $Tipo  . ' as tipo')
                ->from("#__virtualdesk_v_agenda_tipo_evento as a")
                ->where($db->quoteName('id') . "='" . $db->escape($preco) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludePreco($preco){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Tipo = 'tipo_PT';
            } else if ($lang='en_EN'){
                $Tipo = 'tipo_EN';
            } else if ($lang='fr_FR'){
                $Tipo = 'tipo_FR';
            } else if ($lang='de_DE'){
                $Tipo = 'tipo_DE';
            } else {
                $Tipo = 'tipo_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $Tipo  . ' as tipo'))
                ->from("#__virtualdesk_v_agenda_tipo_evento as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($preco) . "'")
                ->order('tipo ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCategoria(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'cat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'cat_DE';
            } else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as categoria'))
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCatSelect($categoria){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'cat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'cat_DE';
            } else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $CatName  . ' as categoria')
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeCat($categoria){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'cat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'cat_DE';
            } else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as categoria'))
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getVideoCat(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.categoria'))
                ->from("#__virtualdesk_Multimedia_Categoria as a")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getVideoCatName($cat){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.categoria')
                ->from("#__virtualdesk_Multimedia_Categoria as a")
                ->where($db->quoteName('id') . "='" . $db->escape($cat) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeVideoCat($cat){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.categoria'))
                ->from("#__virtualdesk_Multimedia_Categoria as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($cat) . "'")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getSubCatSelectID($subcategoria){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'subcat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'subcat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'subcat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'subcat_DE';
            } else {
                $CatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_v_agenda_subcategoria")
                ->where($db->quoteName($CatName) . "='" . $db->escape($subcategoria) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getAllSubCat(){

            $lang = VirtualDeskHelper::getLanguageTag();

            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'subcat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'subcat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'subcat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'subcat_DE';
            } else {
                $CatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $CatName . ' as subcategoria'))
                ->from("#__virtualdesk_v_agenda_subcategoria")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('subcategoria')
            );
            $data = $db->loadAssocList();

            return ($data);
        }

        public static function getSubCat($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'subcat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'subcat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'subcat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'subcat_DE';
            } else {
                $CatName = 'subcat_EN';
            }

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, ' . $CatName . ' as name, categoria_id'))
                    ->from("#__virtualdesk_v_agenda_subcategoria")
                    ->where($db->quoteName('categoria_id') . '=' . $db->escape($idwebsitelist))
                    ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                    ->order('subcat_PT ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, ' . $CatName . ' as name, categoria_id'))
                    ->from("#__virtualdesk_v_agenda_subcategoria")
                    ->where($db->quoteName('categoria_id') . '=' . $db->escape($idwebsitelist))
                    ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getSubCatSelect($idSelectSubCat){
            $lang = VirtualDeskHelper::getLanguageTag();

            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'subcat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'subcat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'subcat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'subcat_DE';
            } else {
                $CatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($CatName)
                ->from("#__virtualdesk_v_agenda_subcategoria")
                ->where($db->quoteName('id') . "='" . $db->escape($idSelectSubCat) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getSubCatMissing($categoria, $idSelectSubCat){
            $lang = VirtualDeskHelper::getLanguageTag();

            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'subcat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'subcat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'subcat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'subcat_DE';
            } else {
                $CatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $CatName . ' as name, categoria_id'))
                ->from("#__virtualdesk_v_agenda_subcategoria")
                ->where($db->quoteName('categoria_id') . '=' . $db->escape($categoria))
                ->where($db->quoteName('id') . '!=' . $db->escape($idSelectSubCat))
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('name')
            );
            $data = $db->loadAssocList();

            return ($data);
        }

        public static function getConcelho($distritoAgenda){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distritoAgenda) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcSelect($distritoAgenda, $concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distritoAgenda) . "'")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeConc($distritoAgenda, $concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distritoAgenda) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getFreg($idwebsitelist,$onAjaxVD=0){

            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('subcat_PT ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getFregPlugin($idwebsitelist,$onAjaxVD=0){

            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('subcat_PT ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getFregSelect($idSelectFreg){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($idSelectFreg) . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getFregMissing($concelho, $idSelectFreg){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia as name, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($concelho))
                ->where($db->quoteName('id') . '!=' . $db->escape($idSelectFreg))
                ->order('name')
            );
            $data = $db->loadAssocList();

            return ($data);
        }

        public static function getFregSelectID($freguesia){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('freguesia') . "='" . $db->escape($freguesia) . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function revertDataOrder($var){
            $splitData = explode("-", $var);
            $splitData[0];
            $splitData[1];
            $splitData[2];

            $dataFormated = $splitData[2] . '-' . $splitData[1] . '-' . $splitData[0];
            return ($dataFormated);
        }

        public static function validaNif($nif){
            $nif2=trim($nif);
            $ignoreFirst=true;
            if(empty($nif)){
                return (1);
            } else if (!is_numeric($nif2) || strlen($nif2)!=9) {
                return (2);
            } else {
                $nifSplit=str_split($nif2);
                if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);

                    if($checkDigit>=10) $checkDigit=0;

                    if ($checkDigit==$nifSplit[8]) {

                    } else {
                        return (2);
                    }
                } else {
                    return (2);
                }
            }
        }

        public static function validaCategoria($categoria){
            if($categoria == Null){
                return (1);
            } else {
                return(0);
            }
        }

        public static function validaSubcategoria($subcategoria){
            if($subcategoria == Null){
                return (1);
            } else {
                return(0);
            }
        }

        public static function validaNomeEvento($nomeEvento){
            if($nomeEvento == Null){
                return (1);
            } else {
                return(0);
            }
        }

        public static function validaDataInicio($dataInicioFormated, $dataAtual){
            if($dataInicioFormated == Null || empty($dataInicioFormated)){
                return (1);
            } else {
                if($dataInicioFormated < $dataAtual) {
                    return (2);
                } else {
                    return(0);
                }
            }
        }

        public static function validaHoraInicio($horaInicio, $horaAtual, $dataInicioFormated, $dataAtual){
            if($horaInicio == Null || empty($horaInicio)){
                return (1);
            } else if($dataInicioFormated == $dataAtual) {
                if($horaInicio <= $horaAtual){
                    return (2);
                }
            } else {
                return(0);
            }
        }

        public static function validaDataFim($dataFimFormated, $dataInicioFormated, $dataAtual){
            if($dataFimFormated == Null || empty($dataFimFormated)){
                return (1);
            } else {
                if($dataFimFormated < $dataAtual) {
                    return (2);
                } else if($dataFimFormated < $dataInicioFormated) {
                    return (2);
                } else {
                    return(0);
                }
            }
        }

        public static function validaHoraFim($horaFim, $horaInicio, $horaAtual, $dataFimFormated, $dataInicioFormated, $dataAtual){
            if($horaFim == Null || empty($horaFim)){
                return (1);
            } else if($dataFimFormated == $dataAtual) {
                if($horaFim <= $horaAtual){
                    return (2);
                } else if($horaFim <= $horaInicio){
                    return (2);
                } else {
                    return(0);
                }
            } else if($dataFimFormated == $dataInicioFormated){
                if($horaFim <= $horaInicio){
                    return (2);
                } else {
                    return(0);
                }
            }
        }

        public static function validaConcelho($concelho){
            if($concelho == Null){
                return (1);
            } else {
                return(0);
            }
        }

        public static function validaFreguesia($freguesia){
            if($freguesia == Null){
                return (1);
            } else {
                return(0);
            }
        }

        public static function validaLocal($localEvento){
            if($localEvento == Null || empty($localEvento)){
                return (1);
            } else {
                return(0);
            }
        }

        public static function validaPrecoEvento($precoEvento){
            if(empty($precoEvento)){
                return (1);
            } else {
                return(0);
            }
        }

        public static function validaEmail($email){
            $conta = "/^[a-zA-Z0-9\._-]+@";
            $domino = "[a-zA-Z0-9\._-]+.[.]";
            $extensao = "([a-zA-Z]{2,4})$/";
            $pattern = $conta . $domino . $extensao;
            if($email == Null){
                return (1);
            } else if(!preg_match($pattern, $email)) {
                return (2);
            } else{
                return (0);
            }
        }

        public static function seeEmailExiste($email, $nif){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_users")
                ->where($db->quoteName('email') . "='" . $db->escape($email) . "'")
                ->where($db->quoteName('login') . "!='" . $db->escape($nif) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEmail($nif){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_users")
                ->where($db->quoteName('login') . "='" . $db->escape($nif) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
        }

        public static function random_code_patrocinador(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 20);
        }

        public static function random_code_diainternacional(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 20);
        }

        public static function getRefCat($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('Ref')
                ->from("#__virtualdesk_v_agenda_categoria")
                ->where($db->quoteName('id') . '=' . $db->escape($categoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function CheckReferenciaVotacao($ref){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_v_agenda_eventos_votacao")
                ->where($db->quoteName('ref_votacao') . "='" . $db->escape($ref) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function seeNifExiste($nif){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome')
                ->from("#__virtualdesk_v_agenda_users")
                ->where($db->quoteName('nif') . '=' . $db->escape($nif))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function seeNifExistev2($nif){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nif')
                ->from("#__virtualdesk_v_agenda_users")
                ->where($db->quoteName('nif') . '=' . $db->escape($nif))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getLayout(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, layout'))
                ->from("#__virtualdesk_v_agenda_tipolayout")
                ->order('layout ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getLayoutSelect($layout){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('layout')
                ->from("#__virtualdesk_v_agenda_tipolayout")
                ->where($db->quoteName('id') . "='" . $db->escape($layout) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludelayout($layout){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, layout'))
                ->from("#__virtualdesk_v_agenda_tipolayout")
                ->where($db->quoteName('id') . "!='" . $db->escape($layout) . "'")
                ->order('layout ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function saveNewUser($nif_evento, $nomepromotor, $email_evento){
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('nif','nome','email');
            $values = array($db->quote($db->escape($nif_evento)), $db->quote($db->escape($nomepromotor)), $db->quote($db->escape($email_evento)));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_users'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function saveNewEvento($referencia, $nif_evento, $nomeEvento, $categoria, $subcategoria, $tags, $descricao_evento, $dataInicio, $anoInicio, $mesInicio, $horaInicio, $dataFim, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $freguesia_evento, $localEvento, $lat, $long, $preco, $observacoes_precario, $facebook, $instagram, $youtube, $vimeo, $video, $catVideo, $balcao=0){
            if($preco == 2){
                $observacoes_precario = '';
            }

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','nif','categoria','subcategoria','tags','nome_evento','descricao_evento','data_inicio','ano_inicio','mes_inicio','data_fim', 'ano_fim','mes_fim','hora_inicio','hora_fim','local_evento','distrito','concelho','freguesia','latitude','longitude','tipo_evento','observacoes_precario','facebook','instagram', 'youtube', 'vimeo', 'videosEvento', 'catVideosEvento', 'evento_premium','top_Evento','estado_evento','balcao');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nif_evento)), $db->quote($db->escape($categoria)), $db->quote($db->escape($subcategoria)), $db->quote($db->escape($tags)), $db->quote($db->escape($nomeEvento)), $db->quote($db->escape($descricao_evento)), $db->quote($db->escape($dataInicio)), $db->quote($db->escape($anoInicio)), $db->quote($db->escape($mesInicio)), $db->quote($db->escape($dataFim)), $db->quote($db->escape($anoFim)), $db->quote($db->escape($mesFim)), $db->quote($db->escape($horaInicio)), $db->quote($db->escape($horaFim)), $db->quote($db->escape($localEvento)), $db->quote($db->escape($distrito)), $db->quote($db->escape($concelho)), $db->quote($db->escape($freguesia_evento)), $db->quote($db->escape($lat)), $db->quote($db->escape($long)), $db->quote($db->escape($preco)), $db->quote($db->escape($observacoes_precario)), $db->quote($db->escape($facebook)), $db->quote($db->escape($instagram)), $db->quote($db->escape($youtube)), $db->quote($db->escape($vimeo)), $db->quote($db->escape($video)), $db->quote($db->escape($catVideo)), $db->quote($db->escape('0')), $db->quote('0'), $db->quote($db->escape('2')), $db->quote($balcao));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_eventos'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function saveVideoMultimedia($referencia, $nomeEventoVideo, $video, $catVideo, $anoInicio, $today){
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia', 'nome', 'link', 'categoria', 'ano', 'publish_FP', 'estado', 'data_publicacao');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nomeEventoVideo)), $db->quote($db->escape($video)), $db->quote($db->escape($catVideo)), $db->quote($db->escape($anoInicio)), $db->quote($db->escape('0')), $db->quote($db->escape('2')), $db->quote($db->escape($today)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Multimedia'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        /*Insere o evento editado na BD*/
        public static function saveEditedEvento4User($agenda_id, $categoria, $subcategoria, $nomeEvento, $descricao, $dataInicio, $anoInicio, $mesInicio, $horaInicio, $dataFim, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $freguesia, $localEvento, $lat, $long, $facebook, $instagram, $youtube, $vimeo, $precoEvento, $layout)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'edit4users'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria)) array_push($fields, 'categoria='.$db->escape($categoria));
            if(!is_null($subcategoria)) array_push($fields, 'subcategoria='.$db->escape($subcategoria));
            if(!is_null($nomeEvento))  array_push($fields, 'nome_evento="'.$db->escape($nomeEvento).'"');
            if(!is_null($descricao))  array_push($fields, 'descricao_evento="'.$db->escape($descricao).'"');

            if(!is_null($dataInicio))  array_push($fields, 'data_inicio="'.$db->escape($dataInicio).'"');
            if(!is_null($anoInicio))  array_push($fields, 'ano_inicio='.$db->escape($anoInicio).'');
            if(!is_null($mesInicio))  array_push($fields, 'mes_inicio='.$db->escape($mesInicio).'');
            if(!is_null($horaInicio))  array_push($fields, 'hora_inicio="'.$db->escape($horaInicio).'"');

            if(!is_null($dataFim))  array_push($fields, 'data_fim="'.$db->escape($dataFim).'"');
            if(!is_null($anoFim))  array_push($fields, 'ano_fim='.$db->escape($anoFim).'');
            if(!is_null($mesFim))  array_push($fields, 'mes_fim='.$db->escape($mesFim).'');
            if(!is_null($horaFim))  array_push($fields, 'hora_fim="'.$db->escape($horaFim).'"');

            if(!is_null($distrito))  array_push($fields, 'distrito='.$db->escape($distrito));
            if(!is_null($concelho))  array_push($fields, 'concelho='.$db->escape($concelho));
            if(!is_null($freguesia))  array_push($fields, 'freguesia='.$db->escape($freguesia));
            if(!is_null($localEvento))  array_push($fields, 'local_evento="'.$db->escape($localEvento).'"');
            if(!is_null($lat))  array_push($fields, 'latitude="'.$db->escape($lat).'"');
            if(!is_null($long))  array_push($fields, 'longitude="'.$db->escape($long).'"');

            if(!is_null($facebook))  array_push($fields, 'facebook="'.$db->escape($facebook).'"');
            if(!is_null($instagram))  array_push($fields, 'instagram="'.$db->escape($instagram).'"');
            if(!is_null($vimeo))  array_push($fields, 'vimeo="'.$db->escape($vimeo).'"');
            if(!is_null($youtube))  array_push($fields, 'youtube="'.$db->escape($youtube).'"');

            if(!is_null($precoEvento))  array_push($fields, 'entradas_detalhe='.$db->escape($precoEvento));
            if(!is_null($layout))  array_push($fields, 'layout_evento='.$db->escape($layout).'');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_v_agenda_eventos')
                ->set($fields)
                ->where(' id = '.$db->escape($agenda_id) .' and nif='.$UserSessionNIF);

            $db->setQuery($query);

            $result = (boolean) $db->execute();

            return($result);
        }

        /*Insere o evento editado na BD*/
        public static function saveEditedEvento4Manager($agenda_id, $nif_evento, $categoria, $subcategoria, $tags, $nome_evento, $descricao_evento, $dataInicio, $anoInicio, $mesInicio, $horaInicio, $dataFim, $anoFim, $mesFim, $horaFim, $distrito, $concelho, $freguesia_evento, $local_evento, $lat, $long, $facebook, $instagram, $youtube, $vimeo, $video, $catVideo, $preco, $observacoes_precario)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if($preco == 2){
                $observacoes_precario = '';
            }

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($nif_evento))           array_push($fields, 'nif='.$db->escape($nif_evento));

            if(!is_null($categoria))            array_push($fields, 'categoria='.$db->escape($categoria));
            if(!is_null($subcategoria))         array_push($fields, 'subcategoria='.$db->escape($subcategoria));
            array_push($fields, 'tags="'.$db->escape($tags).'"');
            if(!is_null($nome_evento))          array_push($fields, 'nome_evento="'.$db->escape($nome_evento).'"');
            if(!is_null($descricao_evento))     array_push($fields, 'descricao_evento="'.$db->escape($descricao_evento).'"');

            if(!is_null($dataInicio))           array_push($fields, 'data_inicio="'.$db->escape($dataInicio).'"');
            if(!is_null($anoInicio))            array_push($fields, 'ano_inicio='.$db->escape($anoInicio).'');
            if(!is_null($mesInicio))            array_push($fields, 'mes_inicio='.$db->escape($mesInicio).'');
            if(!is_null($horaInicio))           array_push($fields, 'hora_inicio="'.$db->escape($horaInicio).'"');

            if(!is_null($dataFim))              array_push($fields, 'data_fim="'.$db->escape($dataFim).'"');
            if(!is_null($anoFim))               array_push($fields, 'ano_fim='.$db->escape($anoFim).'');
            if(!is_null($mesFim))               array_push($fields, 'mes_fim='.$db->escape($mesFim).'');
            if(!is_null($horaFim))              array_push($fields, 'hora_fim="'.$db->escape($horaFim).'"');

            if(!is_null($distrito))             array_push($fields, 'distrito='.$db->escape($distrito));
            if(!is_null($concelho))             array_push($fields, 'concelho='.$db->escape($concelho));
            if(!is_null($freguesia_evento))     array_push($fields, 'freguesia='.$db->escape($freguesia_evento));
            if(!is_null($local_evento))         array_push($fields, 'local_evento="'.$db->escape($local_evento).'"');
            if(!is_null($lat))                  array_push($fields, 'latitude="'.$db->escape($lat).'"');
            if(!is_null($long))                 array_push($fields, 'longitude="'.$db->escape($long).'"');

            if(!is_null($facebook))             array_push($fields, 'facebook="'.$db->escape($facebook).'"');
            if(!is_null($instagram))            array_push($fields, 'instagram="'.$db->escape($instagram).'"');
            if(!is_null($vimeo))                array_push($fields, 'vimeo="'.$db->escape($vimeo).'"');
            if(!is_null($youtube))              array_push($fields, 'youtube="'.$db->escape($youtube).'"');
            array_push($fields, 'videosEvento="'.$db->escape($video).'"');
            array_push($fields, 'catVideosEvento="'.$db->escape($catVideo).'"');

            if(!is_null($preco))                array_push($fields, 'tipo_evento='.$db->escape($preco));
            if(!is_null($observacoes_precario)) array_push($fields, 'observacoes_precario="'.$db->escape($observacoes_precario).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_v_agenda_eventos')
                ->set($fields)
                ->where(' id = '.$db->escape($agenda_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function getEventos($estadoEvento, $topEvento, $dataAtual){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.estado_evento') . "='" . $db->escape($estadoEvento) . "'")
                ->where($db->quoteName('a.top_Evento') . "='" . $db->escape($topEvento) . "'")
                //->where($db->quoteName('a.data_fim') . ">='" . $db->escape($dataAtual) . "'")
                //->order('data_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getNameMonth($month){
            if($month == '1' || $month == '01'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JAN'));
            } else if($month == '02' || $month == '02'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_FEV'));
            } else if($month == '03' || $month == '03'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAR'));
            } else if($month == '04' || $month == '04'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_ABR'));
            } else if($month == '05' || $month == '05'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAI'));
            } else if($month == '06' || $month == '06'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUN'));
            } else if($month == '07' || $month == '07'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUL'));
            } else if($month == '08' || $month == '08'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_AGO'));
            } else if($month == '09' || $month == '09'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_SET'));
            } else if($month == '10'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_OUT'));
            } else if($month == '11'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_NOV'));
            } else if($month == '12'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DEZ'));
            }
        }

        public static function getImgCapa($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_v_agenda_foto_capa")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getImgCartaz($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_v_agenda_cartaz")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getImgGaleria($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_v_agenda_foto_galeria")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getProximosEventos($estadoEvento, $estadoPromotor, $dataAtual, $totalItemsProximos){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            }
            else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.estado_evento') . "='" . $db->escape($estadoEvento) . "'")
                ->where($db->quoteName('a.data_fim') . ">='" . $db->escape($dataAtual) . "'")
                ->order('data_inicio ASC')
                ->setLimit($totalItemsProximos)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventosFree($estadoEvento, $estadoPromotor, $catRemove, $dataAtual){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            }
            else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.estado_nif, b.' . $CatName . ' as categoria, a.nome_evento, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim, c.concelho as concelho, d.freguesia as freguesia'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.estado_evento') . "='" . $db->escape($estadoEvento) . "'")
                ->where($db->quoteName('a.estado_nif') . "='" . $db->escape($estadoPromotor) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($catRemove) . "'")
                ->where($db->quoteName('a.data_fim') . ">='" . $db->escape($dataAtual) . "'")
                ->order('data_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        /*Verifica de a referencia da ocorrencia existe*/
        public static function CheckReferencia($cod){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function saveEditedCat4Manager($agenda_id, $cat_PT, $cat_EN, $cat_FR, $cat_DE, $Ref, $estado)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editcat4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($cat_PT)) array_push($fields, 'cat_PT="'.$db->escape($cat_PT).'"');
            if(!is_null($cat_EN)) array_push($fields, 'cat_EN="'.$db->escape($cat_EN).'"');
            if(!is_null($cat_FR)) array_push($fields, 'cat_FR="'.$db->escape($cat_FR).'"');
            if(!is_null($cat_DE)) array_push($fields, 'cat_DE="'.$db->escape($cat_DE).'"');
            if(!is_null($Ref))  array_push($fields, 'Ref="'.$db->escape($Ref).'"');
            if(!is_null($estado))  array_push($fields, 'estado='.$db->escape($estado));

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_v_agenda_categoria')
                ->set($fields)
                ->where(' id = '.$db->escape($agenda_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function saveNewCat($cat_PT, $cat_EN, $cat_FR, $cat_DE, $Ref, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('cat_PT','cat_EN','cat_FR','cat_DE','Ref','estado');
            $values = array($db->quote($db->escape($cat_PT)), $db->quote($db->escape($cat_EN)), $db->quote($db->escape($cat_FR)), $db->quote($db->escape($cat_DE)), $db->quote($db->escape($Ref)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_categoria'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function saveNewSubCat($subcat_PT, $subcat_EN, $subcat_FR, $subcat_DE, $categoria, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('subcat_PT','subcat_EN','subcat_FR','subcat_DE','categoria_id','estado');
            $values = array($db->quote($db->escape($subcat_PT)), $db->quote($db->escape($subcat_EN)), $db->quote($db->escape($subcat_FR)), $db->quote($db->escape($subcat_DE)), $db->quote($db->escape($categoria)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_subcategoria'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function saveEditedSubCat4Manager($agenda_id, $subcat_PT, $subcat_EN, $subcat_FR, $subcat_DE, $categoria, $estado)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editsubcat4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($subcat_PT)) array_push($fields, 'subcat_PT="'.$db->escape($subcat_PT).'"');
            if(!is_null($subcat_EN)) array_push($fields, 'subcat_EN="'.$db->escape($subcat_EN).'"');
            if(!is_null($subcat_FR)) array_push($fields, 'subcat_FR="'.$db->escape($subcat_FR).'"');
            if(!is_null($subcat_DE)) array_push($fields, 'subcat_DE="'.$db->escape($subcat_DE).'"');
            if(!is_null($categoria))  array_push($fields, 'categoria_id="'.$db->escape($categoria).'"');
            if(!is_null($estado))  array_push($fields, 'estado='.$db->escape($estado));

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_v_agenda_subcategoria')
                ->set($fields)
                ->where(' id = '.$db->escape($agenda_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function saveNewTema($nome_PT, $nome_EN, $nome_FR, $nome_DE, $catAssociadas, $subcatAssociadas, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('nome_PT','nome_EN','nome_FR','nome_DE','categorias','subcategorias','estado');
            $values = array($db->quote($db->escape($nome_PT)), $db->quote($db->escape($nome_EN)), $db->quote($db->escape($nome_FR)), $db->quote($db->escape($nome_DE)), $db->quote($db->escape($catAssociadas)), $db->quote($db->escape($subcatAssociadas)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_estouVontade'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function getTemas(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $name = 'nome_PT';
            } else if( ($lang='en_EN') ) {
                $name = 'nome_EN';
            } else if( ($lang='fr_FR') ) {
                $name = 'nome_FR';
            } else if( ($lang='de_DE') ) {
                $name = 'nome_DE';
            } else {
                $name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.' . $name . ' as nome'))
                ->from("#__virtualdesk_v_agenda_estouVontade as a")
                ->where($db->quoteName('a.estado') . "='" . $db->escape('2') . "'")
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCatsTema($idTema){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.categorias')
                ->from("#__virtualdesk_v_agenda_estouVontade as a")
                ->where($db->quoteName('a.id') . "='" . $db->escape($idTema) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getSubCatsTema($idTema){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.subcategorias')
                ->from("#__virtualdesk_v_agenda_estouVontade as a")
                ->where($db->quoteName('a.id') . "='" . $db->escape($idTema) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getListaEventosByCat($cats, $dataAtual, $limitEstouVontade){
            $cat0 = 0;
            $cat1 = 0;
            $cat2 = 0;
            $cat3 = 0;
            $cat4 = 0;
            $cat5 = 0;
            $cat6 = 0;
            $cat7 = 0;
            $cat8 = 0;
            $cat9 = 0;
            $cat10 = 0;
            $cat11 = 0;
            $cat12 = 0;
            $cat13 = 0;
            $cat14 = 0;
            $cat15 = 0;
            $cat16 = 0;
            $cat17 = 0;
            $cat18 = 0;
            $cat19 = 0;
            $cat20 = 0;
            $cat21 = 0;
            $cat22 = 0;
            $cat23 = 0;
            $cat24 = 0;
            $cat25 = 0;
            $cat26 = 0;
            $cat27 = 0;
            $cat28 = 0;
            $cat29 = 0;
            $cat30 = 0;
            $cat31 = 0;
            $cat32 = 0;
            $cat33 = 0;
            $cat34 = 0;
            $cat35 = 0;
            $cat36 = 0;
            $cat37 = 0;
            $cat38 = 0;
            $cat39 = 0;
            $cat40 = 0;
            $cat41 = 0;
            $cat42 = 0;
            $cat43 = 0;
            $cat44 = 0;
            $cat45 = 0;
            $cat46 = 0;
            $cat47 = 0;
            $cat48 = 0;
            $cat49 = 0;

            for($i=0; $i<count($cats); $i++){
                if($i==0){
                    $cat0 = $cats[$i];
                } else if($i==1){
                    $cat1 = $cats[$i];
                } else if($i==2){
                    $cat2 = $cats[$i];
                } else if($i==3){
                    $cat3 = $cats[$i];
                } else if($i==4){
                    $cat4 = $cats[$i];
                } else if($i==5){
                    $cat5 = $cats[$i];
                } else if($i==6){
                    $cat6 = $cats[$i];
                } else if($i==7){
                    $cat7 = $cats[$i];
                } else if($i==8){
                    $cat8 = $cats[$i];
                } else if($i==9){
                    $cat9 = $cats[$i];
                } else if($i==10){
                    $cat10 = $cats[$i];
                } else if($i==11){
                    $cat11 = $cats[$i];
                } else if($i==12){
                    $cat12 = $cats[$i];
                } else if($i==13){
                    $cat13 = $cats[$i];
                } else if($i==14){
                    $cat14 = $cats[$i];
                } else if($i==15){
                    $cat15 = $cats[$i];
                } else if($i==16){
                    $cat16 = $cats[$i];
                } else if($i==17){
                    $cat17 = $cats[$i];
                } else if($i==18){
                    $cat18 = $cats[$i];
                } else if($i==19){
                    $cat19 = $cats[$i];
                } else if($i==20){
                    $cat20 = $cats[$i];
                } else if($i==21){
                    $cat21 = $cats[$i];
                } else if($i==22){
                    $cat22 = $cats[$i];
                } else if($i==23){
                    $cat23 = $cats[$i];
                } else if($i==24){
                    $cat24 = $cats[$i];
                } else if($i==25){
                    $cat25 = $cats[$i];
                } else if($i==26){
                    $cat26 = $cats[$i];
                } else if($i==27){
                    $cat27 = $cats[$i];
                } else if($i==28){
                    $cat28 = $cats[$i];
                } else if($i==29){
                    $cat29 = $cats[$i];
                } else if($i==30){
                    $cat30 = $cats[$i];
                } else if($i==31){
                    $cat31 = $cats[$i];
                } else if($i==32){
                    $cat32 = $cats[$i];
                } else if($i==33){
                    $cat33 = $cats[$i];
                } else if($i==34){
                    $cat34 = $cats[$i];
                } else if($i==35){
                    $cat35 = $cats[$i];
                } else if($i==36){
                    $cat36 = $cats[$i];
                } else if($i==37){
                    $cat37 = $cats[$i];
                } else if($i==38){
                    $cat38 = $cats[$i];
                } else if($i==39){
                    $cat39 = $cats[$i];
                } else if($i==40){
                    $cat40 = $cats[$i];
                } else if($i==41){
                    $cat41 = $cats[$i];
                } else if($i==42){
                    $cat42 = $cats[$i];
                } else if($i==43){
                    $cat43 = $cats[$i];
                } else if($i==44){
                    $cat44 = $cats[$i];
                } else if($i==45){
                    $cat45 = $cats[$i];
                } else if($i==46){
                    $cat46 = $cats[$i];
                } else if($i==47){
                    $cat47 = $cats[$i];
                } else if($i==48){
                    $cat48 = $cats[$i];
                } else if($i==49){
                    $cat49 = $cats[$i];
                }
            }

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_inicio') . ">='" . $db->escape($dataAtual) . "'";
            $where .= 'AND' . $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";

            $whereCat0 = $db->quoteName('a.categoria') . "='" . $db->escape($cat0) . "'";
            $whereCat1 = $db->quoteName('a.categoria') . "='" . $db->escape($cat1) . "'";
            $whereCat2 = $db->quoteName('a.categoria') . "='" . $db->escape($cat2) . "'";
            $whereCat3 = $db->quoteName('a.categoria') . "='" . $db->escape($cat3) . "'";
            $whereCat4 = $db->quoteName('a.categoria') . "='" . $db->escape($cat4) . "'";
            $whereCat5 = $db->quoteName('a.categoria') . "='" . $db->escape($cat5) . "'";
            $whereCat6 = $db->quoteName('a.categoria') . "='" . $db->escape($cat6) . "'";
            $whereCat7 = $db->quoteName('a.categoria') . "='" . $db->escape($cat7) . "'";
            $whereCat8 = $db->quoteName('a.categoria') . "='" . $db->escape($cat8) . "'";
            $whereCat9 = $db->quoteName('a.categoria') . "='" . $db->escape($cat9) . "'";
            $whereCat10 = $db->quoteName('a.categoria') . "='" . $db->escape($cat10) . "'";
            $whereCat11 = $db->quoteName('a.categoria') . "='" . $db->escape($cat11) . "'";
            $whereCat12 = $db->quoteName('a.categoria') . "='" . $db->escape($cat12) . "'";
            $whereCat13 = $db->quoteName('a.categoria') . "='" . $db->escape($cat13) . "'";
            $whereCat14 = $db->quoteName('a.categoria') . "='" . $db->escape($cat14) . "'";
            $whereCat15 = $db->quoteName('a.categoria') . "='" . $db->escape($cat15) . "'";
            $whereCat16 = $db->quoteName('a.categoria') . "='" . $db->escape($cat16) . "'";
            $whereCat17 = $db->quoteName('a.categoria') . "='" . $db->escape($cat17) . "'";
            $whereCat18 = $db->quoteName('a.categoria') . "='" . $db->escape($cat18) . "'";
            $whereCat19 = $db->quoteName('a.categoria') . "='" . $db->escape($cat19) . "'";
            $whereCat20 = $db->quoteName('a.categoria') . "='" . $db->escape($cat20) . "'";
            $whereCat21 = $db->quoteName('a.categoria') . "='" . $db->escape($cat21) . "'";
            $whereCat22 = $db->quoteName('a.categoria') . "='" . $db->escape($cat22) . "'";
            $whereCat23 = $db->quoteName('a.categoria') . "='" . $db->escape($cat23) . "'";
            $whereCat24 = $db->quoteName('a.categoria') . "='" . $db->escape($cat24) . "'";
            $whereCat25 = $db->quoteName('a.categoria') . "='" . $db->escape($cat25) . "'";
            $whereCat26 = $db->quoteName('a.categoria') . "='" . $db->escape($cat26) . "'";
            $whereCat27 = $db->quoteName('a.categoria') . "='" . $db->escape($cat27) . "'";
            $whereCat28 = $db->quoteName('a.categoria') . "='" . $db->escape($cat28) . "'";
            $whereCat29 = $db->quoteName('a.categoria') . "='" . $db->escape($cat29) . "'";
            $whereCat30 = $db->quoteName('a.categoria') . "='" . $db->escape($cat30) . "'";
            $whereCat31 = $db->quoteName('a.categoria') . "='" . $db->escape($cat31) . "'";
            $whereCat32 = $db->quoteName('a.categoria') . "='" . $db->escape($cat32) . "'";
            $whereCat33 = $db->quoteName('a.categoria') . "='" . $db->escape($cat33) . "'";
            $whereCat34 = $db->quoteName('a.categoria') . "='" . $db->escape($cat34) . "'";
            $whereCat35 = $db->quoteName('a.categoria') . "='" . $db->escape($cat35) . "'";
            $whereCat36 = $db->quoteName('a.categoria') . "='" . $db->escape($cat36) . "'";
            $whereCat37 = $db->quoteName('a.categoria') . "='" . $db->escape($cat37) . "'";
            $whereCat38 = $db->quoteName('a.categoria') . "='" . $db->escape($cat38) . "'";
            $whereCat39 = $db->quoteName('a.categoria') . "='" . $db->escape($cat39) . "'";
            $whereCat40 = $db->quoteName('a.categoria') . "='" . $db->escape($cat40) . "'";
            $whereCat41 = $db->quoteName('a.categoria') . "='" . $db->escape($cat41) . "'";
            $whereCat42 = $db->quoteName('a.categoria') . "='" . $db->escape($cat42) . "'";
            $whereCat43 = $db->quoteName('a.categoria') . "='" . $db->escape($cat43) . "'";
            $whereCat44 = $db->quoteName('a.categoria') . "='" . $db->escape($cat44) . "'";
            $whereCat45 = $db->quoteName('a.categoria') . "='" . $db->escape($cat45) . "'";
            $whereCat46 = $db->quoteName('a.categoria') . "='" . $db->escape($cat46) . "'";
            $whereCat47 = $db->quoteName('a.categoria') . "='" . $db->escape($cat47) . "'";
            $whereCat48 = $db->quoteName('a.categoria') . "='" . $db->escape($cat48) . "'";
            $whereCat49 = $db->quoteName('a.categoria') . "='" . $db->escape($cat49) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereCat0)
                ->orWhere($where . 'AND' . $whereCat1)
                ->orWhere($where . 'AND' . $whereCat2)
                ->orWhere($where . 'AND' . $whereCat3)
                ->orWhere($where . 'AND' . $whereCat4)
                ->orWhere($where . 'AND' . $whereCat5)
                ->orWhere($where . 'AND' . $whereCat6)
                ->orWhere($where . 'AND' . $whereCat7)
                ->orWhere($where . 'AND' . $whereCat8)
                ->orWhere($where . 'AND' . $whereCat9)
                ->orWhere($where . 'AND' . $whereCat10)
                ->orWhere($where . 'AND' . $whereCat11)
                ->orWhere($where . 'AND' . $whereCat12)
                ->orWhere($where . 'AND' . $whereCat13)
                ->orWhere($where . 'AND' . $whereCat14)
                ->orWhere($where . 'AND' . $whereCat15)
                ->orWhere($where . 'AND' . $whereCat16)
                ->orWhere($where . 'AND' . $whereCat17)
                ->orWhere($where . 'AND' . $whereCat18)
                ->orWhere($where . 'AND' . $whereCat19)
                ->orWhere($where . 'AND' . $whereCat20)
                ->orWhere($where . 'AND' . $whereCat21)
                ->orWhere($where . 'AND' . $whereCat22)
                ->orWhere($where . 'AND' . $whereCat23)
                ->orWhere($where . 'AND' . $whereCat24)
                ->orWhere($where . 'AND' . $whereCat25)
                ->orWhere($where . 'AND' . $whereCat26)
                ->orWhere($where . 'AND' . $whereCat27)
                ->orWhere($where . 'AND' . $whereCat28)
                ->orWhere($where . 'AND' . $whereCat29)
                ->orWhere($where . 'AND' . $whereCat30)
                ->orWhere($where . 'AND' . $whereCat31)
                ->orWhere($where . 'AND' . $whereCat32)
                ->orWhere($where . 'AND' . $whereCat33)
                ->orWhere($where . 'AND' . $whereCat34)
                ->orWhere($where . 'AND' . $whereCat35)
                ->orWhere($where . 'AND' . $whereCat36)
                ->orWhere($where . 'AND' . $whereCat37)
                ->orWhere($where . 'AND' . $whereCat38)
                ->orWhere($where . 'AND' . $whereCat39)
                ->orWhere($where . 'AND' . $whereCat40)
                ->orWhere($where . 'AND' . $whereCat41)
                ->orWhere($where . 'AND' . $whereCat42)
                ->orWhere($where . 'AND' . $whereCat43)
                ->orWhere($where . 'AND' . $whereCat44)
                ->orWhere($where . 'AND' . $whereCat45)
                ->orWhere($where . 'AND' . $whereCat46)
                ->orWhere($where . 'AND' . $whereCat47)
                ->orWhere($where . 'AND' . $whereCat48)
                ->orWhere($where . 'AND' . $whereCat49)
                ->order('a.data_inicio ASC')
                ->setLimit($limitEstouVontade)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getListaEventosBySubCat($subCats, $dataAtual, $limitEstouVontade){
            $subCat0 = 0;
            $subCat1 = 0;
            $subCat2 = 0;
            $subCat3 = 0;
            $subCat4 = 0;
            $subCat5 = 0;
            $subCat6 = 0;
            $subCat7 = 0;
            $subCat8 = 0;
            $subCat9 = 0;
            $subCat10 = 0;
            $subCat11 = 0;
            $subCat12 = 0;
            $subCat13 = 0;
            $subCat14 = 0;
            $subCat15 = 0;
            $subCat16 = 0;
            $subCat17 = 0;
            $subCat18 = 0;
            $subCat19 = 0;
            $subCat20 = 0;
            $subCat21 = 0;
            $subCat22 = 0;
            $subCat23 = 0;
            $subCat24 = 0;
            $subCat25 = 0;
            $subCat26 = 0;
            $subCat27 = 0;
            $subCat28 = 0;
            $subCat29 = 0;
            $subCat30 = 0;
            $subCat31 = 0;
            $subCat32 = 0;
            $subCat33 = 0;
            $subCat34 = 0;
            $subCat35 = 0;
            $subCat36 = 0;
            $subCat37 = 0;
            $subCat38 = 0;
            $subCat39 = 0;
            $subCat40 = 0;
            $subCat41 = 0;
            $subCat42 = 0;
            $subCat43 = 0;
            $subCat44 = 0;
            $subCat45 = 0;
            $subCat46 = 0;
            $subCat47 = 0;
            $subCat48 = 0;
            $subCat49 = 0;

            for($i=0; $i<count($subCats); $i++){
                if($i==0){
                    $subCat0 = $subCats[$i];
                } else if($i==1){
                    $subCat1 = $subCats[$i];
                } else if($i==2){
                    $subCat2 = $subCats[$i];
                } else if($i==3){
                    $subCat3 = $subCats[$i];
                } else if($i==4){
                    $subCat4 = $subCats[$i];
                } else if($i==5){
                    $subCat5 = $subCats[$i];
                } else if($i==6){
                    $subCat6 = $subCats[$i];
                } else if($i==7){
                    $subCat7 = $subCats[$i];
                } else if($i==8){
                    $subCat8 = $subCats[$i];
                } else if($i==9){
                    $subCat9 = $subCats[$i];
                } else if($i==10){
                    $subCat10 = $subCats[$i];
                } else if($i==11){
                    $subCat11 = $subCats[$i];
                } else if($i==12){
                    $subCat12 = $subCats[$i];
                } else if($i==13){
                    $subCat13 = $subCats[$i];
                } else if($i==14){
                    $subCat14 = $subCats[$i];
                } else if($i==15){
                    $subCat15 = $subCats[$i];
                } else if($i==16){
                    $subCat16 = $subCats[$i];
                } else if($i==17){
                    $subCat17 = $subCats[$i];
                } else if($i==18){
                    $subCat18 = $subCats[$i];
                } else if($i==19){
                    $subCat19 = $subCats[$i];
                } else if($i==20){
                    $subCat20 = $subCats[$i];
                } else if($i==21){
                    $subCat21 = $subCats[$i];
                } else if($i==22){
                    $subCat22 = $subCats[$i];
                } else if($i==23){
                    $subCat23 = $subCats[$i];
                } else if($i==24){
                    $subCat24 = $subCats[$i];
                } else if($i==25){
                    $subCat25 = $subCats[$i];
                } else if($i==26){
                    $subCat26 = $subCats[$i];
                } else if($i==27){
                    $subCat27 = $subCats[$i];
                } else if($i==28){
                    $subCat28 = $subCats[$i];
                } else if($i==29){
                    $subCat29 = $subCats[$i];
                } else if($i==30){
                    $subCat30 = $subCats[$i];
                } else if($i==31){
                    $subCat31 = $subCats[$i];
                } else if($i==32){
                    $subCat32 = $subCats[$i];
                } else if($i==33){
                    $subCat33 = $subCats[$i];
                } else if($i==34){
                    $subCat34 = $subCats[$i];
                } else if($i==35){
                    $subCat35 = $subCats[$i];
                } else if($i==36){
                    $subCat36 = $subCats[$i];
                } else if($i==37){
                    $subCat37 = $subCats[$i];
                } else if($i==38){
                    $subCat38 = $subCats[$i];
                } else if($i==39){
                    $subCat39 = $subCats[$i];
                } else if($i==40){
                    $subCat40 = $subCats[$i];
                } else if($i==41){
                    $subCat41 = $subCats[$i];
                } else if($i==42){
                    $subCat42 = $subCats[$i];
                } else if($i==43){
                    $subCat43 = $subCats[$i];
                } else if($i==44){
                    $subCat44 = $subCats[$i];
                } else if($i==45){
                    $subCat45 = $subCats[$i];
                } else if($i==46){
                    $subCat46 = $subCats[$i];
                } else if($i==47){
                    $subCat47 = $subCats[$i];
                } else if($i==48){
                    $subCat48 = $subCats[$i];
                } else if($i==49){
                    $subCat49 = $subCats[$i];
                }
            }

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_inicio') . ">='" . $db->escape($dataAtual) . "'";
            $where .= 'AND' . $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";

            $whereSubCat0 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat0) . "'";
            $whereSubCat1 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat1) . "'";
            $whereSubCat2 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat2) . "'";
            $whereSubCat3 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat3) . "'";
            $whereSubCat4 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat4) . "'";
            $whereSubCat5 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat5) . "'";
            $whereSubCat6 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat6) . "'";
            $whereSubCat7 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat7) . "'";
            $whereSubCat8 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat8) . "'";
            $whereSubCat9 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat9) . "'";
            $whereSubCat10 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat10) . "'";
            $whereSubCat11 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat11) . "'";
            $whereSubCat12 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat12) . "'";
            $whereSubCat13 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat13) . "'";
            $whereSubCat14 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat14) . "'";
            $whereSubCat15 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat15) . "'";
            $whereSubCat16 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat16) . "'";
            $whereSubCat17 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat17) . "'";
            $whereSubCat18 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat18) . "'";
            $whereSubCat19 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat19) . "'";
            $whereSubCat20 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat20) . "'";
            $whereSubCat21 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat21) . "'";
            $whereSubCat22 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat22) . "'";
            $whereSubCat23 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat23) . "'";
            $whereSubCat24 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat24) . "'";
            $whereSubCat25 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat25) . "'";
            $whereSubCat26 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat26) . "'";
            $whereSubCat27 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat27) . "'";
            $whereSubCat28 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat28) . "'";
            $whereSubCat29 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat29) . "'";
            $whereSubCat30 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat30) . "'";
            $whereSubCat31 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat31) . "'";
            $whereSubCat32 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat32) . "'";
            $whereSubCat33 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat33) . "'";
            $whereSubCat34 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat34) . "'";
            $whereSubCat35 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat35) . "'";
            $whereSubCat36 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat36) . "'";
            $whereSubCat37 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat37) . "'";
            $whereSubCat38 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat38) . "'";
            $whereSubCat39 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat39) . "'";
            $whereSubCat40 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat40) . "'";
            $whereSubCat41 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat41) . "'";
            $whereSubCat42 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat42) . "'";
            $whereSubCat43 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat43) . "'";
            $whereSubCat44 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat44) . "'";
            $whereSubCat45 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat45) . "'";
            $whereSubCat46 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat46) . "'";
            $whereSubCat47 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat47) . "'";
            $whereSubCat48 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat48) . "'";
            $whereSubCat49 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat49) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.ano_inicio, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereSubCat0)
                ->orWhere($where . 'AND' . $whereSubCat1)
                ->orWhere($where . 'AND' . $whereSubCat2)
                ->orWhere($where . 'AND' . $whereSubCat3)
                ->orWhere($where . 'AND' . $whereSubCat4)
                ->orWhere($where . 'AND' . $whereSubCat5)
                ->orWhere($where . 'AND' . $whereSubCat6)
                ->orWhere($where . 'AND' . $whereSubCat7)
                ->orWhere($where . 'AND' . $whereSubCat8)
                ->orWhere($where . 'AND' . $whereSubCat9)
                ->orWhere($where . 'AND' . $whereSubCat10)
                ->orWhere($where . 'AND' . $whereSubCat11)
                ->orWhere($where . 'AND' . $whereSubCat12)
                ->orWhere($where . 'AND' . $whereSubCat13)
                ->orWhere($where . 'AND' . $whereSubCat14)
                ->orWhere($where . 'AND' . $whereSubCat15)
                ->orWhere($where . 'AND' . $whereSubCat16)
                ->orWhere($where . 'AND' . $whereSubCat17)
                ->orWhere($where . 'AND' . $whereSubCat18)
                ->orWhere($where . 'AND' . $whereSubCat19)
                ->orWhere($where . 'AND' . $whereSubCat20)
                ->orWhere($where . 'AND' . $whereSubCat21)
                ->orWhere($where . 'AND' . $whereSubCat22)
                ->orWhere($where . 'AND' . $whereSubCat23)
                ->orWhere($where . 'AND' . $whereSubCat24)
                ->orWhere($where . 'AND' . $whereSubCat25)
                ->orWhere($where . 'AND' . $whereSubCat26)
                ->orWhere($where . 'AND' . $whereSubCat27)
                ->orWhere($where . 'AND' . $whereSubCat28)
                ->orWhere($where . 'AND' . $whereSubCat29)
                ->orWhere($where . 'AND' . $whereSubCat30)
                ->orWhere($where . 'AND' . $whereSubCat31)
                ->orWhere($where . 'AND' . $whereSubCat32)
                ->orWhere($where . 'AND' . $whereSubCat33)
                ->orWhere($where . 'AND' . $whereSubCat34)
                ->orWhere($where . 'AND' . $whereSubCat35)
                ->orWhere($where . 'AND' . $whereSubCat36)
                ->orWhere($where . 'AND' . $whereSubCat37)
                ->orWhere($where . 'AND' . $whereSubCat38)
                ->orWhere($where . 'AND' . $whereSubCat39)
                ->orWhere($where . 'AND' . $whereSubCat40)
                ->orWhere($where . 'AND' . $whereSubCat41)
                ->orWhere($where . 'AND' . $whereSubCat42)
                ->orWhere($where . 'AND' . $whereSubCat43)
                ->orWhere($where . 'AND' . $whereSubCat44)
                ->orWhere($where . 'AND' . $whereSubCat45)
                ->orWhere($where . 'AND' . $whereSubCat46)
                ->orWhere($where . 'AND' . $whereSubCat47)
                ->orWhere($where . 'AND' . $whereSubCat48)
                ->orWhere($where . 'AND' . $whereSubCat49)
                ->order('a.data_inicio ASC')
                ->setLimit($limitEstouVontade)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getListaEventosAll($dataAtual, $limitEstouVontade){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_inicio') . ">='" . $db->escape($dataAtual) . "'";
            $where .= 'AND' . $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";



            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where)
                ->order('a.data_inicio ASC')
                ->setLimit($limitEstouVontade)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function DuplicaEvento($idevento,$onAjaxVD=0){
            if (empty($idevento)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, nif, categoria, subcategoria, nome_evento, descricao_evento, data_inicio, ano_inicio, mes_inicio, data_fim, ano_fim, mes_fim, hora_inicio, hora_fim
                    , local_evento, distrito, concelho, freguesia, latitude, longitude, tipo_evento, observacoes_precario, facebook, instagram, youtube, vimeo, videosEvento, catVideosEvento'))
                    ->from("#__virtualdesk_v_agenda_eventos")
                    ->where($db->quoteName('id') . '=' . $db->escape($idevento))
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, nif, categoria, subcategoria, nome_evento, descricao_evento, data_inicio, ano_inicio, mes_inicio, data_fim, ano_fim, mes_fim, hora_inicio, hora_fim
                    , local_evento, distrito, concelho, freguesia, latitude, longitude, tipo_evento, observacoes_precario, facebook, instagram, youtube, vimeo, videosEvento, catVideosEvento'))
                    ->from("#__virtualdesk_v_agenda_eventos")
                    ->where($db->quoteName('id') . '=' . $db->escape($idevento))
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getDistinctYear($anoAtual){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('DISTINCT ano_inicio')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('ano_inicio') . '!=' . $db->escape('0'))
                ->where($db->quoteName('ano_inicio') . '<=' . $db->escape($anoAtual))
                ->order('ano_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistinctMes(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('DISTINCT mes_inicio')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('mes_inicio') . '!=' . $db->escape('0'))
                ->order('mes_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventosHistorico($anoPesquisa, $mesPesquisa){
            $dataAtual = date("Y-m-d");

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if( $lang='en_EN'){
                $CatName = 'cat_EN';
            } else if( $lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if( $lang='de_DE'){
                $CatName = 'cat_DE';
            } else{
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.data_fim') . "<'" . $db->escape($dataAtual) . "'";

            if(!empty($anoPesquisa)){
                $where .= 'AND' . $db->quoteName('a.ano_inicio') . "='" . $db->escape($anoPesquisa) . "'";
            }

            if(!empty($mesPesquisa)){
                $where .= 'AND' . $db->quoteName('a.mes_inicio') . "='" . $db->escape($mesPesquisa) . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.data_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.mes_inicio, a.mes_fim, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where)
                ->order('data_inicio DESC')
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDetalheEvento($referencia){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubcatName = 'subcat_PT';
            } else if( $lang='en_EN'){
                $CatName = 'cat_EN';
                $SubcatName = 'subcat_EN';
            } else if( $lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubcatName = 'subcat_FR';
            } else if( $lang='de_DE'){
                $CatName = 'cat_DE';
                $SubcatName = 'subcat_DE';
            } else{
                $CatName = 'cat_EN';
                $SubcatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.categoria as id_categoria, b.' . $CatName . ' as categoria, a.subcategoria as idSubcategoria, e.' . $SubcatName . ' as subcategoria, 
                a.nome_evento, a.data_inicio, a.data_fim, c.concelho as concelho, d.freguesia as freguesia, a.mes_inicio, a.mes_fim, a.nif, a.descricao_evento, a.local_evento, 
                a.latitude, a.longitude, a.tipo_evento, a.facebook, a.instagram, a.youtube, a.vimeo, a.evento_premium, a.top_Evento, a.votos, a.visualizacoes, a.videosEvento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.referencia') . "='" . $db->escape($referencia) . "'")
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventName($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nome_evento')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getNifPromotor($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nif')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getInfoPromotor($nif){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.nome, a.email'))
                ->from("#__virtualdesk_v_agenda_users as a")
                ->where($db->quoteName('a.nif') . "='" . $db->escape($nif) . "'")
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function convertLink($link){
            $urlHttps = explode("https://", $link);
            if(count($urlHttps) == 1){
                $hasLink = 0;
                $urlHttp = explode("http://", $link);
                if(count($urlHttp) == 1){
                    $hasLink = 0;
                } else {
                    $hasLink = 1;
                }
            } else {
                $hasLink = 1;
            }

            if($hasLink == 0){
                $url = 'https://' . $link;
            } else {
                $url = $link;
            }
            return ($url);
        }

        public static function saveNewVisualizacoes($id, $newVisualizações){
            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            array_push($fields, 'visualizacoes="'.$db->escape($newVisualizações).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_v_agenda_eventos')
                ->set($fields)
                ->where(' id = '.$db->escape($id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function getEventosPromotor($estado, $nifPromotor, $limite, $referencia){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcatcat_PT';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcatcat_PT';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcatcat_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.estado_evento') . "='" . $db->escape($estado) . "'")
                ->where($db->quoteName('a.nif') . "='" . $db->escape($nifPromotor) . "'")
                ->where($db->quoteName('a.referencia') . "!='" . $db->escape($referencia) . "'")
                ->order('data_inicio Desc')
                ->setLimit($limite)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventosVotacao($anoPesquisa, $cats){
            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.ano_inicio') . "='" . $db->escape($anoPesquisa) . "'";

            $where2 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[0]) . "'";
            $where3 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[1]) . "'";
            $where4 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[2]) . "'";
            $where5 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[3]) . "'";
            $where6 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[4]) . "'";
            $where7 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[5]) . "'";
            $where8 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[6]) . "'";
            $where9 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[7]) . "'";
            $where10 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[8]) . "'";
            $where11 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[9]) . "'";


            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.nome_evento, a.subcategoria'))
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->orWhere($where . 'AND' . $where7)
                ->orWhere($where . 'AND' . $where8)
                ->orWhere($where . 'AND' . $where9)
                ->orWhere($where . 'AND' . $where10)
                ->orWhere($where . 'AND' . $where11)
                ->order('data_inicio DESC')
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventosVotacaoFiltered($pesquisaLivre, $categoria, $concelho, $anoPesquisa, $cats){

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.ano_inicio') . "='" . $db->escape($anoPesquisa) . "'";

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $where .= 'AND' . $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
            }

            if(!empty($categoria)){
                $where2 = $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
                $where3 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where4 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where5 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where6 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where7 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where8 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where9 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where10 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
                $where11 = $db->quoteName('a.categoria') . "='" . $db->escape('') . "'";
            } else {
                $where2 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[0]) . "'";
                $where3 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[1]) . "'";
                $where4 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[2]) . "'";
                $where5 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[3]) . "'";
                $where6 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[4]) . "'";
                $where7 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[5]) . "'";
                $where8 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[6]) . "'";
                $where9 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[7]) . "'";
                $where10 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[8]) . "'";
                $where11 = $db->quoteName('a.categoria') . "='" . $db->escape($cats[9]) . "'";
            }

            if(!empty($concelho)){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.nome_evento, a.subcategoria'))
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->orWhere($where . 'AND' . $where7)
                ->orWhere($where . 'AND' . $where8)
                ->orWhere($where . 'AND' . $where9)
                ->orWhere($where . 'AND' . $where10)
                ->orWhere($where . 'AND' . $where11)
                ->order('data_inicio DESC')
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getInfoEvento($ref){
            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.referencia') . "='" . $db->escape($ref) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.nome_evento, a.subcategoria'))
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where)
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCategoriaVotacao($cats){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'cat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'cat_DE';
            } else {
                $CatName = 'cat_EN';
            }


            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";
            $where2 = $db->quoteName('a.id') . "='" . $db->escape($cats[0]) . "'";
            $where3 = $db->quoteName('a.id') . "='" . $db->escape($cats[1]) . "'";
            $where4 = $db->quoteName('a.id') . "='" . $db->escape($cats[2]) . "'";
            $where5 = $db->quoteName('a.id') . "='" . $db->escape($cats[3]) . "'";
            $where6 = $db->quoteName('a.id') . "='" . $db->escape($cats[4]) . "'";
            $where7 = $db->quoteName('a.id') . "='" . $db->escape($cats[5]) . "'";
            $where8 = $db->quoteName('a.id') . "='" . $db->escape($cats[6]) . "'";
            $where9 = $db->quoteName('a.id') . "='" . $db->escape($cats[7]) . "'";
            $where10 = $db->quoteName('a.id') . "='" . $db->escape($cats[8]) . "'";
            $where11 = $db->quoteName('a.id') . "='" . $db->escape($cats[9]) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as categoria'))
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->orWhere($where . 'AND' . $where7)
                ->orWhere($where . 'AND' . $where8)
                ->orWhere($where . 'AND' . $where9)
                ->orWhere($where . 'AND' . $where10)
                ->orWhere($where . 'AND' . $where11)
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCatSelectVotacao($categoria){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'cat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'cat_DE';
            } else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $CatName  . ' as categoria')
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeCatVotacao($categoria, $cats){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if ($lang='en_EN'){
                $CatName = 'cat_EN';
            } else if ($lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if ($lang='de_DE'){
                $CatName = 'cat_DE';
            } else {
                $CatName = 'cat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('id') . "!='" . $db->escape($categoria) . "'";
            $where .= 'AND' . $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            $where2 = $db->quoteName('a.id') . "='" . $db->escape($cats[0]) . "'";
            $where3 = $db->quoteName('a.id') . "='" . $db->escape($cats[1]) . "'";
            $where4 = $db->quoteName('a.id') . "='" . $db->escape($cats[2]) . "'";
            $where5 = $db->quoteName('a.id') . "='" . $db->escape($cats[3]) . "'";
            $where6 = $db->quoteName('a.id') . "='" . $db->escape($cats[4]) . "'";
            $where7 = $db->quoteName('a.id') . "='" . $db->escape($cats[5]) . "'";
            $where8 = $db->quoteName('a.id') . "='" . $db->escape($cats[6]) . "'";
            $where9 = $db->quoteName('a.id') . "='" . $db->escape($cats[7]) . "'";
            $where10 = $db->quoteName('a.id') . "='" . $db->escape($cats[8]) . "'";
            $where11 = $db->quoteName('a.id') . "='" . $db->escape($cats[9]) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $CatName  . ' as categoria'))
                ->from("#__virtualdesk_v_agenda_categoria as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->orWhere($where . 'AND' . $where7)
                ->orWhere($where . 'AND' . $where8)
                ->orWhere($where . 'AND' . $where9)
                ->orWhere($where . 'AND' . $where10)
                ->orWhere($where . 'AND' . $where11)
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getAgendaEvents($concelho, $limitAgendaMunicipal){
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubcatName = 'subcat_PT';
            } else if( $lang='en_EN'){
                $CatName = 'cat_EN';
                $SubcatName = 'subcat_EN';
            } else if( $lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubcatName = 'subcat_FR';
            } else if( $lang='de_DE'){
                $CatName = 'cat_DE';
                $SubcatName = 'subcat_DE';
            } else{
                $CatName = 'cat_EN';
                $SubcatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.nome_evento, a.categoria, b.' . $CatName . ' as CatName, a.subcategoria, c.' . $SubcatName . ' as subcatName, a.mes_inicio, a.mes_fim, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS c ON c.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('a.agenda_Municipal') . "='" . $db->escape('1') . "'")
                ->order('a.nome_evento ASC')
                ->setLimit($limitAgendaMunicipal)
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function random_code_Votacao(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 20);
        }

        public static function validaEmailVotacao($email, $ref){
            $conta = "/^[a-zA-Z0-9\._-]+@";
            $domino = "[a-zA-Z0-9\._-]+.[.]";
            $extensao = "([a-zA-Z]{2,4})$/";
            $mailExiste = self::getVoteByMail($email, $ref);
            $pattern = $conta . $domino . $extensao;
            if($email == Null){
                return (1);
            } else if(!preg_match($pattern, $email)) {
                return (2);
            } else if (count($mailExiste) > 0) {
                return (3);
            } else {
                return (0);
            }
        }

        public static function getVoteByMail($email, $ref){
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.ref_votacao'))
                ->from("#__virtualdesk_v_agenda_eventos_votacao as a")
                ->where($db->quoteName('a.ref_evento') . "='" . $db->escape($ref) . "'")
                ->where($db->quoteName('a.email') . "='" . $db->escape($email) . "'")
                ->where($db->quoteName('a.estado') . "='" . $db->escape('1') . "'")
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function saveNewVotacao($refVotacao, $ref, $email){
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('ref_votacao','ref_evento','email');
            $values = array($db->quote($db->escape($refVotacao)), $db->quote($db->escape($ref)), $db->quote($email));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_eventos_votacao'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function SendEmailVotacao($refVotacao, $email, $nome_evento, $token){

            $config = JFactory::getConfig();

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('enviaVotacao','email2user');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }
            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoV_Agenda = $obParam->getParamsByTag('logoV_Agenda');
            $mail_V_Agenda = $obParam->getParamsByTag('mail_V_Agenda');
            $dominio_V_Agenda = $obParam->getParamsByTag('dominio_V_Agenda');
            $link_V_Agenda = $obParam->getParamsByTag('link_V_Agenda');
            $websiteName_V_Agenda = $obParam->getParamsByTag('websiteName_V_Agenda');
            $sendVoto = $obParam->getParamsByTag('sendVoto');

            $mode = $config->get('force_ssl', 0) == 2 ? 1 : (-1);
            //$link_root   = JUri::root().'activate/?token=' . $token;
            $link_root   = $sendVoto . '?token=' . $token;

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');
            $data['link_text'] = JRoute::_($link_root, false, $mode);

            $BODY_TITLE                 = JText::sprintf('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL_TITULO');
            $BODY_TITLE2                = JText::sprintf('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL_TITULO');
            $BODY_GREETING              = JText::sprintf('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL_INTRO',$email);
            $BODY_FIRSTLINE             = JText::sprintf('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL_INTRO2',$nome_evento);
            $BODY_SECONDLINE            = JText::sprintf('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL_INTRO3');
            $BODY_THIRDLINE             = JText::sprintf('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL_INTRO4');
            $BODY_MAIL_VAGENDA_VALUE    = JText::sprintf($mail_V_Agenda);
            $BODY_DOMINIO_VAGENDA_VALUE = JText::sprintf($dominio_V_Agenda);
            $BODY_LINK_VAGENDA_VALUE    = JText::sprintf($link_V_Agenda);
            $BODY_COPYRIGHT_VALUE       = JText::sprintf($websiteName_V_Agenda);
            $BODY_ACTIVATE_LABELLINK    = JText::sprintf('COM_VIRTUALDESK_VAGENDA_VOTACAO_EMAIL_VALIDAR_LINK');
            $BODY_ACTIVATE_LINK         = JText::sprintf('COM_VIRTUALDESK_USERACTIVATION_SENDMAIL_BODY_ACTIVATE_LINK',$data['link_text']);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_FIRSTLINE",$BODY_FIRSTLINE, $body );
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_MAIL_VAGENDA_VALUE",$BODY_MAIL_VAGENDA_VALUE, $body);
            $body      = str_replace("%BODY_DOMINIO_VAGENDA_VALUE",$BODY_DOMINIO_VAGENDA_VALUE, $body);
            $body      = str_replace("%BODY_LINK_VAGENDA_VALUE",$BODY_LINK_VAGENDA_VALUE, $body);
            $body      = str_replace("%BODY_COPYRIGHT_VALUE",$BODY_COPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_ACTIVATE_LABELLINK",$BODY_ACTIVATE_LABELLINK, $body);
            $body      = str_replace("%BODY_ACTIVATE_LINK", $BODY_ACTIVATE_LINK, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT . '/' . $logoV_Agenda, "banner", "Logo");


            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }

        public static function validaVoto($refVoto){
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            // Fields to update.
            $fields = array(
                $db->quoteName('estado') . ' = 1'
            );

            // Conditions for which records should be updated.
            $conditions = array(
                $db->quoteName('ref_votacao') . ' = ' . $db->quote($db->escape($refVoto))
            );

            $query->update($db->quoteName('#__virtualdesk_v_agenda_eventos_votacao'))->set($fields)->where($conditions);

            $db->setQuery($query);

            $result = $db->execute();
        }

        public static function getNumvotos($ref){
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.ref_votacao'))
                ->from("#__virtualdesk_v_agenda_eventos_votacao as a")
                ->where($db->quoteName('a.ref_evento') . "='" . $db->escape($ref) . "'")
                ->where($db->quoteName('a.estado') . "='" . $db->escape('1') . "'")
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function updateNumVotos($ref, $numVotos){
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            // Fields to update.
            $fields = array(
                $db->quoteName('votos') . ' = ' . $db->quote($db->escape($numVotos))
            );

            // Conditions for which records should be updated.
            $conditions = array(
                $db->quoteName('referencia') . ' = ' . $db->quote($db->escape($ref))
            );

            $query->update($db->quoteName('#__virtualdesk_v_agenda_eventos'))->set($fields)->where($conditions);

            $db->setQuery($query);

            $result = $db->execute();
        }

        public static function getEventosMesAMes($mes){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
            }

            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.nome_evento, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.subcategoria as idSubcategoria'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.mes_inicio') . "='" . $db->escape($mes) . "'")
                ->where($db->quoteName('a.mes_a_mes') . "='" . $db->escape('1') . "'")
                ->setLimit(3)
            );

            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getAnoList($anoAtual){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('DISTINCT ano_inicio as ano')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('ano_inicio') . '!=' . $db->escape('0'))
                ->where($db->quoteName('ano_inicio') . '<=' . $db->escape($anoAtual))
                ->order('ano_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeAnoList($anoAtual, $ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('DISTINCT ano_inicio as ano')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('ano_inicio') . '!=' . $db->escape('0'))
                ->where($db->quoteName('ano_inicio') . '<=' . $db->escape($anoAtual))
                ->where($db->quoteName('ano_inicio') . '!=' . $db->escape($ano))
                ->order('ano_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getMesList(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('DISTINCT mes_inicio as mes')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('mes_inicio') . '!=' . $db->escape('0'))
                ->order('mes_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeMesList($mes){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('DISTINCT mes_inicio as mes')
                ->from("#__virtualdesk_v_agenda_eventos")
                ->where($db->quoteName('mes_inicio') . '!=' . $db->escape('0'))
                ->where($db->quoteName('mes_inicio') . '!=' . $db->escape($mes))
                ->order('mes_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getMonthNameList($month){
            if($month == '1' || $month == '01'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JAN_NAME'));
            } else if($month == '02' || $month == '02'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_FEV_NAME'));
            } else if($month == '03' || $month == '03'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAR_NAME'));
            } else if($month == '04' || $month == '04'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_ABR_NAME'));
            } else if($month == '05' || $month == '05'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_MAI_NAME'));
            } else if($month == '06' || $month == '06'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUN_NAME'));
            } else if($month == '07' || $month == '07'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_JUL_NAME'));
            } else if($month == '08' || $month == '08'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_AGO_NAME'));
            } else if($month == '09' || $month == '09'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_SET_NAME'));
            } else if($month == '10'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_OUT_NAME'));
            } else if($month == '11'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_NOV_NAME'));
            } else if($month == '12'){
                return(JText::_('COM_VIRTUALDESK_VAGENDA_MONTH_DEZ_NAME'));
            }
        }

        public static function getEstouComVontade(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            } else if ($lang='en_EN'){
                $Name = 'nome_EN';
            } else if ($lang='fr_FR'){
                $Name = 'nome_FR';
            } else if ($lang='de_DE'){
                $Name = 'nome_DE';
            } else {
                $Name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $Name . ' as nome'))
                ->from("#__virtualdesk_v_agenda_estouVontade")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeEstouComVontade($estouComVontade){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            } else if ($lang='en_EN'){
                $Name = 'nome_EN';
            } else if ($lang='fr_FR'){
                $Name = 'nome_FR';
            } else if ($lang='de_DE'){
                $Name = 'nome_DE';
            } else {
                $Name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $Name . ' as nome'))
                ->from("#__virtualdesk_v_agenda_estouVontade")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->where($db->quoteName('id') . '!=' . $db->escape($estouComVontade))
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEstouComVontadeSelect($estouComVontade){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'nome_PT';
            } else if ($lang='en_EN'){
                $Name = 'nome_EN';
            } else if ($lang='fr_FR'){
                $Name = 'nome_FR';
            } else if ($lang='de_DE'){
                $Name = 'nome_DE';
            } else {
                $Name = 'nome_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as nome')
                ->from("#__virtualdesk_v_agenda_estouVontade")
                ->where($db->quoteName('id') . '=' . $db->escape($estouComVontade))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getEventsListByData($data_inicio, $categoria, $concelho, $freguesia, $pesquisaLivre){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $new_data = str_replace(' ', '', $data_inicio);

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";

            if(!empty($new_data) && $new_data != 0){
                $where .= 'AND' . $db->quoteName('a.data_fim') . ">='" . $db->escape($new_data) . "'";
            }

            if(!empty($categoria) && $categoria != 0){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereTag)
                ->order('a.data_inicio ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventsListByConcelho($categoria, $ano, $concelho, $freguesia, $pesquisaLivre){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.data_inicio') . "!='" . $db->escape('') . "'";

            if(!empty($categoria) && $categoria != 0){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($ano) && $ano != 0){
                $where .= 'AND' . $db->quoteName('a.ano_inicio') . "='" . $db->escape($ano) . "'";
            }

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.categoria as idCategoria, a.referencia, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereTag)
                ->order('a.data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventsListByMes($categoria, $concelho, $freguesia, $ano, $mes, $pesquisaLivre){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.data_inicio') . "!='" . $db->escape('') . "'";

            if(!empty($categoria) && $categoria != 0){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($ano) && $ano != 0){
                $where .= 'AND' . $db->quoteName('a.ano_inicio') . "='" . $db->escape($ano) . "'";
            }

            if(!empty($mes) && $mes != 0){
                $where .= 'AND' . $db->quoteName('a.mes_inicio') . "='" . $db->escape($mes) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereTag)
                ->order('a.data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventsListByCategoria($categoria, $concelho, $freguesia, $ano, $pesquisaLivre){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.data_inicio') . "!='" . $db->escape('') . "'";

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($ano) && $ano != 0){
                $where .= 'AND' . $db->quoteName('a.ano_inicio') . "='" . $db->escape($ano) . "'";
            }

            if(!empty($categoria) && $categoria != 0){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereTag)
                ->order('a.data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventsListByPesquisa($categoria, $ano, $concelho, $freguesia, $pesquisaLivre){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('a.ano_inicio') . "!='" . $db->escape('') . "'";

            if(!empty($categoria) && $categoria != 0){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($ano) && $ano != 0){
                $where .= 'AND' . $db->quoteName('a.ano_inicio') . "='" . $db->escape($ano) . "'";
            }

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereTag)
                ->order('a.data_inicio DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventsListVontadeByCat($cats, $dataAtual, $pesquisaLivre, $concelho, $freguesia, $limitEstouVontade){
            $cat0 = 0;
            $cat1 = 0;
            $cat2 = 0;
            $cat3 = 0;
            $cat4 = 0;
            $cat5 = 0;
            $cat6 = 0;
            $cat7 = 0;
            $cat8 = 0;
            $cat9 = 0;
            $cat10 = 0;
            $cat11 = 0;
            $cat12 = 0;
            $cat13 = 0;
            $cat14 = 0;
            $cat15 = 0;
            $cat16 = 0;
            $cat17 = 0;
            $cat18 = 0;
            $cat19 = 0;
            $cat20 = 0;
            $cat21 = 0;
            $cat22 = 0;
            $cat23 = 0;
            $cat24 = 0;
            $cat25 = 0;
            $cat26 = 0;
            $cat27 = 0;
            $cat28 = 0;
            $cat29 = 0;
            $cat30 = 0;
            $cat31 = 0;
            $cat32 = 0;
            $cat33 = 0;
            $cat34 = 0;
            $cat35 = 0;
            $cat36 = 0;
            $cat37 = 0;
            $cat38 = 0;
            $cat39 = 0;
            $cat40 = 0;
            $cat41 = 0;
            $cat42 = 0;
            $cat43 = 0;
            $cat44 = 0;
            $cat45 = 0;
            $cat46 = 0;
            $cat47 = 0;
            $cat48 = 0;
            $cat49 = 0;

            for($i=0; $i<count($cats); $i++){
                if($i==0){
                    $cat0 = $cats[$i];
                } else if($i==1){
                    $cat1 = $cats[$i];
                } else if($i==2){
                    $cat2 = $cats[$i];
                } else if($i==3){
                    $cat3 = $cats[$i];
                } else if($i==4){
                    $cat4 = $cats[$i];
                } else if($i==5){
                    $cat5 = $cats[$i];
                } else if($i==6){
                    $cat6 = $cats[$i];
                } else if($i==7){
                    $cat7 = $cats[$i];
                } else if($i==8){
                    $cat8 = $cats[$i];
                } else if($i==9){
                    $cat9 = $cats[$i];
                } else if($i==10){
                    $cat10 = $cats[$i];
                } else if($i==11){
                    $cat11 = $cats[$i];
                } else if($i==12){
                    $cat12 = $cats[$i];
                } else if($i==13){
                    $cat13 = $cats[$i];
                } else if($i==14){
                    $cat14 = $cats[$i];
                } else if($i==15){
                    $cat15 = $cats[$i];
                } else if($i==16){
                    $cat16 = $cats[$i];
                } else if($i==17){
                    $cat17 = $cats[$i];
                } else if($i==18){
                    $cat18 = $cats[$i];
                } else if($i==19){
                    $cat19 = $cats[$i];
                } else if($i==20){
                    $cat20 = $cats[$i];
                } else if($i==21){
                    $cat21 = $cats[$i];
                } else if($i==22){
                    $cat22 = $cats[$i];
                } else if($i==23){
                    $cat23 = $cats[$i];
                } else if($i==24){
                    $cat24 = $cats[$i];
                } else if($i==25){
                    $cat25 = $cats[$i];
                } else if($i==26){
                    $cat26 = $cats[$i];
                } else if($i==27){
                    $cat27 = $cats[$i];
                } else if($i==28){
                    $cat28 = $cats[$i];
                } else if($i==29){
                    $cat29 = $cats[$i];
                } else if($i==30){
                    $cat30 = $cats[$i];
                } else if($i==31){
                    $cat31 = $cats[$i];
                } else if($i==32){
                    $cat32 = $cats[$i];
                } else if($i==33){
                    $cat33 = $cats[$i];
                } else if($i==34){
                    $cat34 = $cats[$i];
                } else if($i==35){
                    $cat35 = $cats[$i];
                } else if($i==36){
                    $cat36 = $cats[$i];
                } else if($i==37){
                    $cat37 = $cats[$i];
                } else if($i==38){
                    $cat38 = $cats[$i];
                } else if($i==39){
                    $cat39 = $cats[$i];
                } else if($i==40){
                    $cat40 = $cats[$i];
                } else if($i==41){
                    $cat41 = $cats[$i];
                } else if($i==42){
                    $cat42 = $cats[$i];
                } else if($i==43){
                    $cat43 = $cats[$i];
                } else if($i==44){
                    $cat44 = $cats[$i];
                } else if($i==45){
                    $cat45 = $cats[$i];
                } else if($i==46){
                    $cat46 = $cats[$i];
                } else if($i==47){
                    $cat47 = $cats[$i];
                } else if($i==48){
                    $cat48 = $cats[$i];
                } else if($i==49){
                    $cat49 = $cats[$i];
                }
            }

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_inicio') . ">='" . $db->escape($dataAtual) . "'";
            $where .= 'AND' . $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $whereCat0 = $db->quoteName('a.categoria') . "='" . $db->escape($cat0) . "'";
            $whereCat1 = $db->quoteName('a.categoria') . "='" . $db->escape($cat1) . "'";
            $whereCat2 = $db->quoteName('a.categoria') . "='" . $db->escape($cat2) . "'";
            $whereCat3 = $db->quoteName('a.categoria') . "='" . $db->escape($cat3) . "'";
            $whereCat4 = $db->quoteName('a.categoria') . "='" . $db->escape($cat4) . "'";
            $whereCat5 = $db->quoteName('a.categoria') . "='" . $db->escape($cat5) . "'";
            $whereCat6 = $db->quoteName('a.categoria') . "='" . $db->escape($cat6) . "'";
            $whereCat7 = $db->quoteName('a.categoria') . "='" . $db->escape($cat7) . "'";
            $whereCat8 = $db->quoteName('a.categoria') . "='" . $db->escape($cat8) . "'";
            $whereCat9 = $db->quoteName('a.categoria') . "='" . $db->escape($cat9) . "'";
            $whereCat10 = $db->quoteName('a.categoria') . "='" . $db->escape($cat10) . "'";
            $whereCat11 = $db->quoteName('a.categoria') . "='" . $db->escape($cat11) . "'";
            $whereCat12 = $db->quoteName('a.categoria') . "='" . $db->escape($cat12) . "'";
            $whereCat13 = $db->quoteName('a.categoria') . "='" . $db->escape($cat13) . "'";
            $whereCat14 = $db->quoteName('a.categoria') . "='" . $db->escape($cat14) . "'";
            $whereCat15 = $db->quoteName('a.categoria') . "='" . $db->escape($cat15) . "'";
            $whereCat16 = $db->quoteName('a.categoria') . "='" . $db->escape($cat16) . "'";
            $whereCat17 = $db->quoteName('a.categoria') . "='" . $db->escape($cat17) . "'";
            $whereCat18 = $db->quoteName('a.categoria') . "='" . $db->escape($cat18) . "'";
            $whereCat19 = $db->quoteName('a.categoria') . "='" . $db->escape($cat19) . "'";
            $whereCat20 = $db->quoteName('a.categoria') . "='" . $db->escape($cat20) . "'";
            $whereCat21 = $db->quoteName('a.categoria') . "='" . $db->escape($cat21) . "'";
            $whereCat22 = $db->quoteName('a.categoria') . "='" . $db->escape($cat22) . "'";
            $whereCat23 = $db->quoteName('a.categoria') . "='" . $db->escape($cat23) . "'";
            $whereCat24 = $db->quoteName('a.categoria') . "='" . $db->escape($cat24) . "'";
            $whereCat25 = $db->quoteName('a.categoria') . "='" . $db->escape($cat25) . "'";
            $whereCat26 = $db->quoteName('a.categoria') . "='" . $db->escape($cat26) . "'";
            $whereCat27 = $db->quoteName('a.categoria') . "='" . $db->escape($cat27) . "'";
            $whereCat28 = $db->quoteName('a.categoria') . "='" . $db->escape($cat28) . "'";
            $whereCat29 = $db->quoteName('a.categoria') . "='" . $db->escape($cat29) . "'";
            $whereCat30 = $db->quoteName('a.categoria') . "='" . $db->escape($cat30) . "'";
            $whereCat31 = $db->quoteName('a.categoria') . "='" . $db->escape($cat31) . "'";
            $whereCat32 = $db->quoteName('a.categoria') . "='" . $db->escape($cat32) . "'";
            $whereCat33 = $db->quoteName('a.categoria') . "='" . $db->escape($cat33) . "'";
            $whereCat34 = $db->quoteName('a.categoria') . "='" . $db->escape($cat34) . "'";
            $whereCat35 = $db->quoteName('a.categoria') . "='" . $db->escape($cat35) . "'";
            $whereCat36 = $db->quoteName('a.categoria') . "='" . $db->escape($cat36) . "'";
            $whereCat37 = $db->quoteName('a.categoria') . "='" . $db->escape($cat37) . "'";
            $whereCat38 = $db->quoteName('a.categoria') . "='" . $db->escape($cat38) . "'";
            $whereCat39 = $db->quoteName('a.categoria') . "='" . $db->escape($cat39) . "'";
            $whereCat40 = $db->quoteName('a.categoria') . "='" . $db->escape($cat40) . "'";
            $whereCat41 = $db->quoteName('a.categoria') . "='" . $db->escape($cat41) . "'";
            $whereCat42 = $db->quoteName('a.categoria') . "='" . $db->escape($cat42) . "'";
            $whereCat43 = $db->quoteName('a.categoria') . "='" . $db->escape($cat43) . "'";
            $whereCat44 = $db->quoteName('a.categoria') . "='" . $db->escape($cat44) . "'";
            $whereCat45 = $db->quoteName('a.categoria') . "='" . $db->escape($cat45) . "'";
            $whereCat46 = $db->quoteName('a.categoria') . "='" . $db->escape($cat46) . "'";
            $whereCat47 = $db->quoteName('a.categoria') . "='" . $db->escape($cat47) . "'";
            $whereCat48 = $db->quoteName('a.categoria') . "='" . $db->escape($cat48) . "'";
            $whereCat49 = $db->quoteName('a.categoria') . "='" . $db->escape($cat49) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereCat0 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat0 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat1 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat1 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat2 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat2 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat3 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat3 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat4 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat4 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat5 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat5 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat6 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat6 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat7 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat7 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat8 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat8 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat9 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat9 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat10 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat10 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat11 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat11 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat12 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat12 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat13 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat13 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat14 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat14 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat15 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat15 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat16 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat16 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat17 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat17 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat18 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat18 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat19 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat19 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat20 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat20 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat21 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat21 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat22 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat22 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat23 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat23 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat24 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat24 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat25 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat25 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat26 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat26 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat27 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat27 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat28 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat28 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat29 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat29 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat30 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat30 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat31 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat31 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat32 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat32 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat33 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat33 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat34 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat34 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat35 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat35 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat36 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat36 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat37 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat37 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat38 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat38 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat39 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat39 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat40 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat40 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat41 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat41 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat42 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat42 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat43 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat43 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat44 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat44 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat45 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat45 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat46 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat46 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat47 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat47 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat48 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat48 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereCat49 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereCat49 . 'AND' . $whereTag)
                ->order('a.data_inicio ASC')
                ->setLimit($limitEstouVontade)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventsListVontadeBySubCat($subCats, $dataAtual, $pesquisaLivre, $concelho, $freguesia, $limitEstouVontade){
            $subCat0 = 0;
            $subCat1 = 0;
            $subCat2 = 0;
            $subCat3 = 0;
            $subCat4 = 0;
            $subCat5 = 0;
            $subCat6 = 0;
            $subCat7 = 0;
            $subCat8 = 0;
            $subCat9 = 0;
            $subCat10 = 0;
            $subCat11 = 0;
            $subCat12 = 0;
            $subCat13 = 0;
            $subCat14 = 0;
            $subCat15 = 0;
            $subCat16 = 0;
            $subCat17 = 0;
            $subCat18 = 0;
            $subCat19 = 0;
            $subCat20 = 0;
            $subCat21 = 0;
            $subCat22 = 0;
            $subCat23 = 0;
            $subCat24 = 0;
            $subCat25 = 0;
            $subCat26 = 0;
            $subCat27 = 0;
            $subCat28 = 0;
            $subCat29 = 0;
            $subCat30 = 0;
            $subCat31 = 0;
            $subCat32 = 0;
            $subCat33 = 0;
            $subCat34 = 0;
            $subCat35 = 0;
            $subCat36 = 0;
            $subCat37 = 0;
            $subCat38 = 0;
            $subCat39 = 0;
            $subCat40 = 0;
            $subCat41 = 0;
            $subCat42 = 0;
            $subCat43 = 0;
            $subCat44 = 0;
            $subCat45 = 0;
            $subCat46 = 0;
            $subCat47 = 0;
            $subCat48 = 0;
            $subCat49 = 0;

            for($i=0; $i<count($subCats); $i++){
                if($i==0){
                    $subCat0 = $subCats[$i];
                } else if($i==1){
                    $subCat1 = $subCats[$i];
                } else if($i==2){
                    $subCat2 = $subCats[$i];
                } else if($i==3){
                    $subCat3 = $subCats[$i];
                } else if($i==4){
                    $subCat4 = $subCats[$i];
                } else if($i==5){
                    $subCat5 = $subCats[$i];
                } else if($i==6){
                    $subCat6 = $subCats[$i];
                } else if($i==7){
                    $subCat7 = $subCats[$i];
                } else if($i==8){
                    $subCat8 = $subCats[$i];
                } else if($i==9){
                    $subCat9 = $subCats[$i];
                } else if($i==10){
                    $subCat10 = $subCats[$i];
                } else if($i==11){
                    $subCat11 = $subCats[$i];
                } else if($i==12){
                    $subCat12 = $subCats[$i];
                } else if($i==13){
                    $subCat13 = $subCats[$i];
                } else if($i==14){
                    $subCat14 = $subCats[$i];
                } else if($i==15){
                    $subCat15 = $subCats[$i];
                } else if($i==16){
                    $subCat16 = $subCats[$i];
                } else if($i==17){
                    $subCat17 = $subCats[$i];
                } else if($i==18){
                    $subCat18 = $subCats[$i];
                } else if($i==19){
                    $subCat19 = $subCats[$i];
                } else if($i==20){
                    $subCat20 = $subCats[$i];
                } else if($i==21){
                    $subCat21 = $subCats[$i];
                } else if($i==22){
                    $subCat22 = $subCats[$i];
                } else if($i==23){
                    $subCat23 = $subCats[$i];
                } else if($i==24){
                    $subCat24 = $subCats[$i];
                } else if($i==25){
                    $subCat25 = $subCats[$i];
                } else if($i==26){
                    $subCat26 = $subCats[$i];
                } else if($i==27){
                    $subCat27 = $subCats[$i];
                } else if($i==28){
                    $subCat28 = $subCats[$i];
                } else if($i==29){
                    $subCat29 = $subCats[$i];
                } else if($i==30){
                    $subCat30 = $subCats[$i];
                } else if($i==31){
                    $subCat31 = $subCats[$i];
                } else if($i==32){
                    $subCat32 = $subCats[$i];
                } else if($i==33){
                    $subCat33 = $subCats[$i];
                } else if($i==34){
                    $subCat34 = $subCats[$i];
                } else if($i==35){
                    $subCat35 = $subCats[$i];
                } else if($i==36){
                    $subCat36 = $subCats[$i];
                } else if($i==37){
                    $subCat37 = $subCats[$i];
                } else if($i==38){
                    $subCat38 = $subCats[$i];
                } else if($i==39){
                    $subCat39 = $subCats[$i];
                } else if($i==40){
                    $subCat40 = $subCats[$i];
                } else if($i==41){
                    $subCat41 = $subCats[$i];
                } else if($i==42){
                    $subCat42 = $subCats[$i];
                } else if($i==43){
                    $subCat43 = $subCats[$i];
                } else if($i==44){
                    $subCat44 = $subCats[$i];
                } else if($i==45){
                    $subCat45 = $subCats[$i];
                } else if($i==46){
                    $subCat46 = $subCats[$i];
                } else if($i==47){
                    $subCat47 = $subCats[$i];
                } else if($i==48){
                    $subCat48 = $subCats[$i];
                } else if($i==49){
                    $subCat49 = $subCats[$i];
                }
            }

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_inicio') . ">='" . $db->escape($dataAtual) . "'";
            $where .= 'AND' . $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $whereSubCat0 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat0) . "'";
            $whereSubCat1 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat1) . "'";
            $whereSubCat2 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat2) . "'";
            $whereSubCat3 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat3) . "'";
            $whereSubCat4 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat4) . "'";
            $whereSubCat5 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat5) . "'";
            $whereSubCat6 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat6) . "'";
            $whereSubCat7 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat7) . "'";
            $whereSubCat8 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat8) . "'";
            $whereSubCat9 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat9) . "'";
            $whereSubCat10 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat10) . "'";
            $whereSubCat11 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat11) . "'";
            $whereSubCat12 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat12) . "'";
            $whereSubCat13 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat13) . "'";
            $whereSubCat14 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat14) . "'";
            $whereSubCat15 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat15) . "'";
            $whereSubCat16 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat16) . "'";
            $whereSubCat17 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat17) . "'";
            $whereSubCat18 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat18) . "'";
            $whereSubCat19 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat19) . "'";
            $whereSubCat20 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat20) . "'";
            $whereSubCat21 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat21) . "'";
            $whereSubCat22 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat22) . "'";
            $whereSubCat23 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat23) . "'";
            $whereSubCat24 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat24) . "'";
            $whereSubCat25 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat25) . "'";
            $whereSubCat26 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat26) . "'";
            $whereSubCat27 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat27) . "'";
            $whereSubCat28 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat28) . "'";
            $whereSubCat29 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat29) . "'";
            $whereSubCat30 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat30) . "'";
            $whereSubCat31 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat31) . "'";
            $whereSubCat32 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat32) . "'";
            $whereSubCat33 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat33) . "'";
            $whereSubCat34 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat34) . "'";
            $whereSubCat35 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat35) . "'";
            $whereSubCat36 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat36) . "'";
            $whereSubCat37 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat37) . "'";
            $whereSubCat38 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat38) . "'";
            $whereSubCat39 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat39) . "'";
            $whereSubCat40 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat40) . "'";
            $whereSubCat41 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat41) . "'";
            $whereSubCat42 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat42) . "'";
            $whereSubCat43 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat43) . "'";
            $whereSubCat44 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat44) . "'";
            $whereSubCat45 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat45) . "'";
            $whereSubCat46 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat46) . "'";
            $whereSubCat47 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat47) . "'";
            $whereSubCat48 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat48) . "'";
            $whereSubCat49 = $db->quoteName('a.subcategoria') . "='" . $db->escape($subCat49) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.ano_inicio, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereSubCat0 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat0 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat1 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat1 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat2 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat2 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat3 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat3 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat4 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat4 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat5 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat5 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat6 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat6 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat7 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat7 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat8 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat8 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat9 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat9 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat10 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat10 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat11 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat11 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat12 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat12 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat13 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat13 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat14 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat14 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat15 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat15 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat16 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat16 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat17 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat17 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat18 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat18 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat19 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat19 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat20 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat20 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat21 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat21 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat22 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat22 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat23 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat23 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat24 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat24 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat25 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat25 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat26 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat26 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat27 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat27 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat28 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat28 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat29 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat29 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat30 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat30 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat31 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat31 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat32 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat32 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat33 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat33 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat34 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat34 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat35 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat35 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat36 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat36 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat37 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat37 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat38 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat38 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat39 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat39 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat40 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat40 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat41 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat41 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat42 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat42 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat43 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat43 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat44 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat44 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat45 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat45 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat46 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat46 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat47 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat47 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat48 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat48 . 'AND' . $whereTag)
                ->orWhere($where . 'AND' . $whereSubCat49 . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereSubCat49 . 'AND' . $whereTag)
                ->order('a.data_inicio ASC')
                ->setLimit($limitEstouVontade)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEventsListVontadeAll($dataAtual, $pesquisaLivre, $concelho, $freguesia, $limitEstouVontade){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'cat_PT';
                $SubCatName = 'subcat_PT';
            } else if($lang='en_EN'){
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            } else if($lang='fr_FR'){
                $CatName = 'cat_FR';
                $SubCatName = 'subcat_FR';
            } else if($lang='de_DE'){
                $CatName = 'cat_DE';
                $SubCatName = 'subcat_DE';
            } else {
                $CatName = 'cat_EN';
                $SubCatName = 'subcat_EN';
            }

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_inicio') . ">='" . $db->escape($dataAtual) . "'";
            $where .= 'AND' . $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";

            if(!empty($concelho) && $concelho != 0){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if(!empty($freguesia) && $freguesia != 0){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($pesquisaLivre)){
                $nomeEvento = '%' . $pesquisaLivre . '%';
                $whereNomeEvento = $db->quoteName('a.nome_evento') . "LIKE '" . $db->escape($nomeEvento) . "'";
                $whereTag = $db->quoteName('a.tags') . "LIKE '" . $db->escape($nomeEvento) . "'";
            } else {
                $whereNomeEvento = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
                $whereTag = $db->quoteName('a.estado_evento') . "='" . $db->escape('2') . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, b.' . $CatName . ' as categoria, a.subcategoria as subcat_ID, e.' . $SubCatName . ' as subcategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.ano_inicio, a.mes_inicio, a.data_fim, a.mes_fim, a.concelho as idConcelho, c.concelho as concelho, d.freguesia as freguesia, a.freguesia as idFreguesia, a.evento_premium, a.top_Evento'))
                ->join('LEFT', '#__virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS c ON c.id = a.concelho')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_v_agenda_subcategoria AS e ON e.id = a.subcategoria')
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($where . 'AND' . $whereNomeEvento)
                ->orWhere($where . 'AND' . $whereTag)
                ->order('a.data_inicio ASC')
                ->setLimit($limitEstouVontade)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getTickerEventos($estadoEvento, $dataAtual){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.categoria as idCategoria, a.subcategoria as idSubcategoria, a.nome_evento, a.data_inicio, a.mes_inicio, a.data_fim, a.mes_fim'))
                ->from("#__virtualdesk_v_agenda_eventos as a")
                ->where($db->quoteName('a.estado_evento') . "='" . $db->escape($estadoEvento) . "'")
                ->where($db->quoteName('a.data_fim') . ">='" . $db->escape($dataAtual) . "'")
                ->order('data_inicio ASC')
                ->setLimit(15)
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function CheckReferenciaPatrocinador($cod){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_v_agenda_Patrocinadores")
                ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function CheckNifPatrocinador($nif){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nif')
                ->from("#__virtualdesk_v_agenda_Patrocinadores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nif) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function saveNewPatrocinador($referencia, $nifpatrocinador, $nomepatrocinador, $urlpatrocinador, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','nif','nome','url','estado');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nifpatrocinador)), $db->quote($db->escape($nomepatrocinador)), $db->quote($db->escape($urlpatrocinador)), $db->quote($db->escape('2')));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_Patrocinadores'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function CheckReferenciaDiainternacional($cod){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_v_agenda_dias_internacionais")
                ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getRefPatrocinador($patrocinador){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_v_agenda_Patrocinadores")
                ->where($db->quoteName('id') . "='" . $db->escape($patrocinador) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function saveNewDiaInternacional($referencia, $nome_evento, $descricao_evento, $categoria, $data_evento, $patrocinador, $balcao=0){

            $explodeData = explode('-',$data_evento);
            $mes = $explodeData[1];
            $dia = $explodeData[2];

            $refPatrocinador = self::getRefPatrocinador($patrocinador);

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','dia','descritivo','categoria','data_inicio','mes_inicio','dia_inicio','ref_patrocinador','estado');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nome_evento)), $db->quote($db->escape($descricao_evento)), $db->quote($db->escape($categoria)), $db->quote($db->escape($data_evento)), $db->quote($db->escape($mes)), $db->quote($db->escape($dia)), $db->quote($db->escape($refPatrocinador)), $db->quote($db->escape('2')));
            $query
                ->insert($db->quoteName('#__virtualdesk_v_agenda_dias_internacionais'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

    }

?>