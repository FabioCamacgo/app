<?php
    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskTableDocumentos', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/documentos.php');
    JLoader::register('VirtualDeskTableDocumentosEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/documentosEstadoHistorico.php');
    JLoader::register('VirtualDeskTableDocumentosCategoriaNivel1', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/documentosCategoriaNivel1.php');
    JLoader::register('VirtualDeskTableDocumentosCategoriaNivel2', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/documentosCategoriaNivel2.php');
    JLoader::register('VirtualDeskTableDocumentosCategoriaNivel3', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/documentosCategoriaNivel3.php');
    JLoader::register('VirtualDeskTableDocumentosCategoriaNivel4', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/documentosCategoriaNivel4.php');
    JLoader::register('VirtualDeskSiteDocumentosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/files.php');

    class VirtualDeskSiteDocumentosHelper
    {

        /*APP*/
        const tagchaveModulo = 'documentos';


        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name') )
                    ->from("#__virtualdesk_Documentos_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-pendente';
                break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }


        public static function getEstado(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Documentos_Estado")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstadoSelect($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('estado')
                ->from("#__virtualdesk_Documentos_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($id) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeEstado($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Documentos_Estado")
                ->where($db->quoteName('id_estado') . "!='" . $db->escape($id) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel1(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Documentos_categoria_nivel1")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel1Select($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel1")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel1) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCatLevel1($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Documentos_categoria_nivel1")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel1) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel2($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($catLevel1) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel2Select($catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel2) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCatLevel2($catLevel1, $catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel2) . "'")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($catLevel1) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriaNivel2($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel1'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel2")
                    ->where($db->quoteName('id_nivel1') . '=' . $db->escape($idwebsitelist))
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel1'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel2")
                    ->where($db->quoteName('id_nivel1') . '=' . $db->escape($idwebsitelist))
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function getCatLevel3($catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel2'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($catLevel2) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel3Select($catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel3) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCatLevel3($catLevel2, $catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel2'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel3) . "'")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($catLevel2) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriaNivel3($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . '=' . $db->escape($idwebsitelist))
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . '=' . $db->escape($idwebsitelist))
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function getCatLevel4($catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel3'))
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id_nivel3') . "='" . $db->escape($catLevel3) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel4Select($catLevel4){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel4) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCatLevel4($catLevel3, $catLevel4){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel3'))
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel4) . "'")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($catLevel3) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriaNivel4($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel3'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel4")
                    ->where($db->quoteName('id_nivel3') . '=' . $db->escape($idwebsitelist))
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel3'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel4")
                    ->where($db->quoteName('id_nivel3') . '=' . $db->escape($idwebsitelist))
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 18);
        }


        public static function random_codeDoc(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 20);
        }


        public static function CheckReferencia($cod){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_Documentos")
                ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function createCatNivel14Admin($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $categoria = $data['categoria'];
                $estado = $data['estado'];

                $resSaveCategoria = self::saveNewCategoriaNivel1($categoria, $estado);

                if (empty($resSaveCategoria)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewCategoriaNivel1($categoria, $estado, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('categoria','estado');
            $values = array($db->quote($db->escape($categoria)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Documentos_categoria_nivel1'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function createCatNivel24Admin($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $categoria = $data['categoria'];
                $catLevel1 = $data['catLevel1'];
                $estado = $data['estado'];

                $resSaveCategoria = self::saveNewCategoriaNivel2($categoria, $catLevel1, $estado);

                if (empty($resSaveCategoria)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewCategoriaNivel2($categoria, $catLevel1, $estado, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('categoria','id_nivel1','estado');
            $values = array($db->quote($db->escape($categoria)), $db->quote($db->escape($catLevel1)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Documentos_categoria_nivel2'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function createCatNivel34Admin($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $categoria = $data['categoria'];
                $catLevel1 = $data['catLevel1'];
                $catLevel2 = $data['catLevel2'];
                $estado = $data['estado'];

                $resSaveCategoria = self::saveNewCategoriaNivel3($categoria, $catLevel1, $catLevel2, $estado);

                if (empty($resSaveCategoria)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewCategoriaNivel3($categoria, $catLevel1, $catLevel2, $estado, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('categoria','id_nivel1','id_nivel2','estado');
            $values = array($db->quote($db->escape($categoria)), $db->quote($db->escape($catLevel1)), $db->quote($db->escape($catLevel2)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Documentos_categoria_nivel3'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function createCatNivel44Admin($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $categoria = $data['categoria'];
                $catLevel1 = $data['catLevel1'];
                $catLevel2 = $data['catLevel2'];
                $catLevel3 = $data['catLevel3'];
                $estado = $data['estado'];

                $resSaveCategoria = self::saveNewCategoriaNivel4($categoria, $catLevel1, $catLevel2, $catLevel3, $estado);

                if (empty($resSaveCategoria)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewCategoriaNivel4($categoria, $catLevel1, $catLevel2, $catLevel3, $estado, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('categoria','id_nivel1','id_nivel2','id_nivel3','estado');
            $values = array($db->quote($db->escape($categoria)), $db->quote($db->escape($catLevel1)), $db->quote($db->escape($catLevel2)), $db->quote($db->escape($catLevel3)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Documentos_categoria_nivel4'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function getCategoriasNivel1List4Admin ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('documentos', 'listcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('documentos','listcat4admin');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=editnivel1cat4admin&categoria_id=');

                    $table  = " ( SELECT a.id as id, a.id as categoria_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.categoria as categoria, a.estado as idestado, b.estado as estado, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Documentos_categoria_nivel1 as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'categoria_id',  'dt' => 0 ),
                        array( 'db' => 'categoria',     'dt' => 1 ),
                        array( 'db' => 'estado',        'dt' => 2 ),
                        array( 'db' => 'dummy',         'dt' => 3 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 4 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoriasNivel2List4Admin ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('documentos', 'listcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('documentos','listcat4admin');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=editnivel2cat4admin&categoria_id=');

                    $table  = " ( SELECT a.id as id, a.id as categoria_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.categoria as categoria, a.estado as idestado, b.estado as estado, a.id_nivel1 as id_nivel1, c.categoria as nivel1, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Documentos_categoria_nivel2 as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel1 AS c ON c.id = a.id_nivel1";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'categoria_id',  'dt' => 0 ),
                        array( 'db' => 'categoria',     'dt' => 1 ),
                        array( 'db' => 'nivel1',        'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 ),
                        array( 'db' => 'id_nivel1',     'dt' => 6 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoriasNivel3List4Admin ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('documentos', 'listcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('documentos','listcat4admin');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=editnivel3cat4admin&categoria_id=');

                    $table  = " ( SELECT a.id as id, a.id as categoria_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.categoria as categoria, a.estado as idestado, b.estado as estado, a.id_nivel1 as id_nivel1, c.categoria as nivel1, a.id_nivel2 as id_nivel2, d.categoria as nivel2, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Documentos_categoria_nivel3 as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel1 AS c ON c.id = a.id_nivel1";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel2 AS d ON d.id = a.id_nivel2";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'categoria_id',  'dt' => 0 ),
                        array( 'db' => 'categoria',     'dt' => 1 ),
                        array( 'db' => 'nivel1',        'dt' => 2 ),
                        array( 'db' => 'nivel2',        'dt' => 3 ),
                        array( 'db' => 'estado',        'dt' => 4 ),
                        array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 6 ),
                        array( 'db' => 'id_nivel1',     'dt' => 7 ),
                        array( 'db' => 'id_nivel2',     'dt' => 8 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoriasNivel4List4Admin ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('documentos', 'listcat4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('documentos','listcat4admin');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=editnivel4cat4admin&categoria_id=');

                    $table  = " ( SELECT a.id as id, a.id as categoria_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.categoria as categoria, a.estado as idestado, b.estado as estado, a.id_nivel1 as id_nivel1, c.categoria as nivel1, a.id_nivel2 as id_nivel2, d.categoria as nivel2, a.id_nivel3 as id_nivel3, e.categoria as nivel3, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Documentos_categoria_nivel4 as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel1 AS c ON c.id = a.id_nivel1";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel2 AS d ON d.id = a.id_nivel2";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel3 AS e ON e.id = a.id_nivel3";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'categoria_id',  'dt' => 0 ),
                        array( 'db' => 'categoria',     'dt' => 1 ),
                        array( 'db' => 'nivel1',        'dt' => 2 ),
                        array( 'db' => 'nivel2',        'dt' => 3 ),
                        array( 'db' => 'nivel3',        'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 7 ),
                        array( 'db' => 'id_nivel1',     'dt' => 8 ),
                        array( 'db' => 'id_nivel2',     'dt' => 9 ),
                        array( 'db' => 'id_nivel3',     'dt' => 10 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoriaNivel14AdminDetail($getInputCategoria_Id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('documentos');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('documentos'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('documentos','editcat4admin'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($getInputCategoria_Id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as categoria_id', 'a.categoria as categoria', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Documentos_categoria_nivel1 as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($getInputCategoria_Id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoriaNivel24AdminDetail($getInputCategoria_Id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('documentos');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('documentos'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('documentos','editcat4admin'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($getInputCategoria_Id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as categoria_id', 'a.categoria as categoria', 'a.id_nivel1 as catLevel1', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Documentos_categoria_nivel2 as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($getInputCategoria_Id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoriaNivel34AdminDetail($getInputCategoria_Id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('documentos');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('documentos'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('documentos','editcat4admin'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($getInputCategoria_Id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as categoria_id', 'a.categoria as categoria', 'a.id_nivel1 as catLevel1', 'a.id_nivel2 as catLevel2', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Documentos_categoria_nivel3 as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($getInputCategoria_Id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoriaNivel44AdminDetail($getInputCategoria_Id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('documentos');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('documentos'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('documentos','editcat4admin'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($getInputCategoria_Id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as categoria_id', 'a.categoria as categoria', 'a.id_nivel1 as catLevel1', 'a.id_nivel2 as catLevel2', 'a.id_nivel3 as catLevel3', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Documentos_categoria_nivel4 as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($getInputCategoria_Id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCatLevel1AllOptions ($displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id as id','categoria as name') )
                    ->from("#__virtualdesk_Documentos_categoria_nivel1")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function updateCatNivel14Admin($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $categoria_id = $data['categoria_id'];
            if((int) $categoria_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDocumentosCategoriaNivel1($db);
            $Table->load(array('id'=>$categoria_id));

            $db->transactionStart();

            try {

                $obParam             = new VirtualDeskSiteParamsHelper();

                $categoria         = null;
                $estado        = null;

                if(array_key_exists('categoria',$data))     $categoria = $data['categoria'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];

                $today = date("Y-m-d H:i:s");

                $obParam = new VirtualDeskSiteParamsHelper();

                /* BEGIN Save Evento */
                $resSaveCat = self::saveEditedCatNivel14Admin($categoria_id, $categoria, $estado, $today);
                /* END Save Evento */

                if (empty($resSaveCat)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedCatNivel14Admin($categoria_id, $categoria, $estado, $data_alteracao){

            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria))  array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));
            if(!is_null($data_alteracao))   array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Documentos_categoria_nivel1')
                ->set($fields)
                ->where(' id = '.$db->escape($categoria_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function updateCatNivel24Admin($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $categoria_id = $data['categoria_id'];
            if((int) $categoria_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDocumentosCategoriaNivel2($db);
            $Table->load(array('id'=>$categoria_id));

            $db->transactionStart();

            try {

                $obParam             = new VirtualDeskSiteParamsHelper();

                $categoria  = null;
                $catLevel1  = null;
                $estado     = null;

                if(array_key_exists('categoria',$data))     $categoria = $data['categoria'];
                if(array_key_exists('catLevel1',$data))     $catLevel1 = $data['catLevel1'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];

                $today = date("Y-m-d H:i:s");

                $obParam = new VirtualDeskSiteParamsHelper();

                /* BEGIN Save Evento */
                $resSaveCat = self::saveEditedCatNivel24Admin($categoria_id, $categoria, $catLevel1, $estado, $today);
                /* END Save Evento */

                if (empty($resSaveCat)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedCatNivel24Admin($categoria_id, $categoria, $catLevel1, $estado, $data_alteracao){

            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria))  array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($catLevel1))   array_push($fields, 'id_nivel1='.$db->escape($catLevel1));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));
            if(!is_null($data_alteracao))   array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Documentos_categoria_nivel2')
                ->set($fields)
                ->where(' id = '.$db->escape($categoria_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function getIdCatParent($catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_nivel1')
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel2) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getIdCatParent2($catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_nivel2')
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel3) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getIdCatParent3($catLevel4){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_nivel3')
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel4) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function updateCatNivel34Admin($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $categoria_id = $data['categoria_id'];
            if((int) $categoria_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDocumentosCategoriaNivel3($db);
            $Table->load(array('id'=>$categoria_id));

            $db->transactionStart();

            try {

                $obParam             = new VirtualDeskSiteParamsHelper();

                $categoria  = null;
                $catLevel1  = null;
                $catLevel2  = null;
                $estado     = null;

                if(array_key_exists('categoria',$data))     $categoria = $data['categoria'];
                if(array_key_exists('catLevel1',$data))     $catLevel1 = $data['catLevel1'];
                if(array_key_exists('catLevel2',$data))     $catLevel2 = $data['catLevel2'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];

                $idParent = self::getIdCatParent($catLevel2);

                if($idParent != $catLevel1){
                    $catLevel2 = 0;
                }

                $today = date("Y-m-d H:i:s");

                $obParam = new VirtualDeskSiteParamsHelper();

                /* BEGIN Save Evento */
                $resSaveCat = self::saveEditedCatNivel34Admin($categoria_id, $categoria, $catLevel1, $catLevel2, $estado, $today);
                /* END Save Evento */

                if (empty($resSaveCat)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedCatNivel34Admin($categoria_id, $categoria, $catLevel1, $catLevel2, $estado, $data_alteracao){

            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria))  array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($catLevel1))   array_push($fields, 'id_nivel1='.$db->escape($catLevel1));
            if(!is_null($catLevel2))   array_push($fields, 'id_nivel2='.$db->escape($catLevel2));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));
            if(!is_null($data_alteracao))   array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Documentos_categoria_nivel3')
                ->set($fields)
                ->where(' id = '.$db->escape($categoria_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function updateCatNivel44Admin($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $categoria_id = $data['categoria_id'];
            if((int) $categoria_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDocumentosCategoriaNivel4($db);
            $Table->load(array('id'=>$categoria_id));

            $db->transactionStart();

            try {

                $obParam             = new VirtualDeskSiteParamsHelper();

                $categoria  = null;
                $catLevel1  = null;
                $catLevel2  = null;
                $catLevel3  = null;
                $estado     = null;

                if(array_key_exists('categoria',$data))     $categoria = $data['categoria'];
                if(array_key_exists('catLevel1',$data))     $catLevel1 = $data['catLevel1'];
                if(array_key_exists('catLevel2',$data))     $catLevel2 = $data['catLevel2'];
                if(array_key_exists('catLevel3',$data))     $catLevel3 = $data['catLevel3'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];

                $idParent = self::getIdCatParent($catLevel2);
                $idParent2 = self::getIdCatParent2($catLevel3);

                if($idParent != $catLevel1){
                    $catLevel2 = 0;
                    $catLevel3 = 0;
                }

                if($idParent2 != $catLevel2){
                    $catLevel3 = 0;
                }

                $today = date("Y-m-d H:i:s");

                $obParam = new VirtualDeskSiteParamsHelper();

                /* BEGIN Save Evento */
                $resSaveCat = self::saveEditedCatNivel44Admin($categoria_id, $categoria, $catLevel1, $catLevel2, $catLevel3, $estado, $today);
                /* END Save Evento */

                if (empty($resSaveCat)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $categoria_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedCatNivel44Admin($categoria_id, $categoria, $catLevel1, $catLevel2, $catLevel3, $estado, $data_alteracao){

            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria))  array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($catLevel1))   array_push($fields, 'id_nivel1='.$db->escape($catLevel1));
            if(!is_null($catLevel2))   array_push($fields, 'id_nivel2='.$db->escape($catLevel2));
            if(!is_null($catLevel3))   array_push($fields, 'id_nivel3='.$db->escape($catLevel3));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));
            if(!is_null($data_alteracao))   array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Documentos_categoria_nivel4')
                ->set($fields)
                ->where(' id = '.$db->escape($categoria_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_Documentos_files as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function create4Admin($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnew4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $nome       = $data['nome'];
                $catLevel1  = $data['catLevel1'];
                $catLevel2  = $data['catLevel2'];
                $catLevel3  = $data['catLevel3'];
                $catLevel4  = $data['catLevel4'];
                $TagInput   = $data['TagInput'];

                $referencia = '';
                $refExiste = 1;
                while($refExiste == 1){
                    $referencia = VirtualDeskSiteDocumentosHelper::random_codeDoc();
                    $checkREF   = VirtualDeskSiteDocumentosHelper::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                foreach($TagInput as $keyPar => $valParc ){
                    if(empty($tags)){
                        $tags = $valParc['tag'];
                    } else {
                        $tags .= ';;' . $valParc['tag'];
                    }
                }

                $resSaveDocumento = self::saveNewDocumento($referencia, $nome, $catLevel1, $catLevel2, $catLevel3, $catLevel4, $tags, '2');

                if (empty($resSaveDocumento)) {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteDocumentosFilesHelper();
                $objCapaFiles->tagprocesso   = 'Documentos_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_doc');

                if ($resFileSaveCapa===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $nome;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewDocumento($referencia, $nome, $catLevel1, $catLevel2, $catLevel3, $catLevel4, $tags, $estado, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','nome','id_nivel1','id_nivel2','id_nivel3','id_nivel4','estado','tags');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nome)), $db->quote($db->escape($catLevel1)), $db->quote($db->escape($catLevel2)), $db->quote($db->escape($catLevel3)), $db->quote($db->escape($catLevel4)), $db->quote($db->escape($estado)), $db->quote($db->escape($tags)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Documentos'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function getDocumentosList4Admin ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('documentos', 'list4admin');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('documentos','list4admin');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=edit4admin&documento_id=');

                    $table  = " ( SELECT a.id as id, a.id as documento_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.nome as nome, a.id_nivel1 as id_nivel1, c.categoria as nivel1, a.id_nivel2 as id_nivel2, d.categoria as nivel2, a.id_nivel3 as id_nivel3, e.categoria as nivel3, a.id_nivel4 as id_nivel4, f.categoria as nivel4, a.estado as idestado, b.estado as estado, a.visualizacoes as visualizacoes, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Documentos as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel1 AS c ON c.id = a.id_nivel1";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel2 AS d ON d.id = a.id_nivel2";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel3 AS e ON e.id = a.id_nivel3";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel4 AS f ON f.id = a.id_nivel4";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'codigo',        'dt' => 0 ),
                        array( 'db' => 'nome',          'dt' => 1 ),
                        array( 'db' => 'nivel1',        'dt' => 2 ),
                        array( 'db' => 'nivel2',        'dt' => 3 ),
                        array( 'db' => 'nivel3',        'dt' => 4 ),
                        array( 'db' => 'nivel4',        'dt' => 5 ),
                        array( 'db' => 'estado',        'dt' => 6 ),
                        array( 'db' => 'visualizacoes', 'dt' => 7 ),
                        array( 'db' => 'dummy',         'dt' => 8 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'documento_id',  'dt' => 9 ),
                        array( 'db' => 'id_nivel1',     'dt' => 10 ),
                        array( 'db' => 'id_nivel2',     'dt' => 11 ),
                        array( 'db' => 'id_nivel3',     'dt' => 12 ),
                        array( 'db' => 'id_nivel4',     'dt' => 13 ),
                        array( 'db' => 'idestado',      'dt' => 14 ),
                        array( 'db' => 'data_criacao',  'dt' => 15 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getDocumentosList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('documentos', 'list4manager');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('documentos','list4manager');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=view4manager&documento_id=');

                    $table  = " ( SELECT a.id as id, a.id as documento_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.nome as nome, a.id_nivel1 as id_nivel1, c.categoria as nivel1, a.id_nivel2 as id_nivel2, d.categoria as nivel2, a.id_nivel3 as id_nivel3, e.categoria as nivel3, a.id_nivel4 as id_nivel4, f.categoria as nivel4, a.estado as idestado, b.estado as estado, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Documentos as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel1 AS c ON c.id = a.id_nivel1";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel2 AS d ON d.id = a.id_nivel2";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel3 AS e ON e.id = a.id_nivel3";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Documentos_categoria_nivel4 AS f ON f.id = a.id_nivel4";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'nome',          'dt' => 0 ),
                        array( 'db' => 'nivel1',        'dt' => 1 ),
                        array( 'db' => 'nivel2',        'dt' => 2 ),
                        array( 'db' => 'nivel3',        'dt' => 3 ),
                        array( 'db' => 'nivel4',        'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'codigo',        'dt' => 6 ),
                        array( 'db' => 'dummy',         'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'documento_id',  'dt' => 8 ),
                        array( 'db' => 'id_nivel1',     'dt' => 9 ),
                        array( 'db' => 'id_nivel2',     'dt' => 10 ),
                        array( 'db' => 'id_nivel3',     'dt' => 11 ),
                        array( 'db' => 'id_nivel4',     'dt' => 12 ),
                        array( 'db' => 'idestado',      'dt' => 13 ),
                        array( 'db' => 'data_criacao',  'dt' => 14 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getDocumentos4AdminDetail($getInputDocumento_Id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('documentos');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('documentos', 'edit4admin'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('documentos'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('documentos','edit4admin'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($getInputDocumento_Id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id', 'a.referencia as referencia', 'a.nome as nome', 'a.id_nivel1 as catLevel1', 'a.id_nivel2 as catLevel2', 'a.id_nivel3 as catLevel3', 'a.id_nivel4 as catLevel4', 'a.estado as idestado', 'b.estado as estado', 'a.tags'))
                    ->join('LEFT', '#__virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Documentos as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($getInputDocumento_Id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCat4DocLevel1(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Documentos_categoria_nivel1")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel1Select($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel1")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel1) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCat4DocLevel1($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Documentos_categoria_nivel1")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel1) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel2($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($catLevel1) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel2Select($catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel2) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCat4DocLevel2($catLevel1, $catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel2) . "'")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($catLevel1) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoria4DocNivel2($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel1'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel2")
                    ->where($db->quoteName('id_nivel1') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel1'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel2")
                    ->where($db->quoteName('id_nivel1') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function getCat4DocLevel3($catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel2'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($catLevel2) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel3Select($catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel3) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCat4DocLevel3($catLevel2, $catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel2'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel3) . "'")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($catLevel2) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoria4DocNivel3($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function getCat4DocLevel4($catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel3'))
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id_nivel3') . "='" . $db->escape($catLevel3) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel4Select($catLevel4){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel4) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCat4DocLevel4($catLevel3, $catLevel4){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel3'))
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel4) . "'")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($catLevel3) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoria4DocNivel4($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel3'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel4")
                    ->where($db->quoteName('id_nivel3') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel3'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel4")
                    ->where($db->quoteName('id_nivel3') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('id ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function update4Admin($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'edit4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $documento_id = $data['documento_id'];
            if((int) $documento_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDocumentos($db);
            $Table->load(array('id'=>$documento_id));

            $db->transactionStart();

            try {
                $referencia          = $Table->referencia;
                $obParam    = new VirtualDeskSiteParamsHelper();

                $nome  = null;
                $catLevel1  = null;
                $catLevel2  = null;
                $catLevel3  = null;
                $catLevel4  = null;
                $estado     = null;
                $TagInput   = null;

                if(array_key_exists('nome',$data))          $nome = $data['nome'];
                if(array_key_exists('catLevel1',$data))     $catLevel1 = $data['catLevel1'];
                if(array_key_exists('catLevel2',$data))     $catLevel2 = $data['catLevel2'];
                if(array_key_exists('catLevel3',$data))     $catLevel3 = $data['catLevel3'];
                if(array_key_exists('catLevel4',$data))     $catLevel4 = $data['catLevel4'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];
                if(array_key_exists('TagInput',$data))      $TagInput = $data['TagInput'];

                $idParent = self::getIdCatParent($catLevel2);
                $idParent2 = self::getIdCatParent2($catLevel3);
                $idParent3 = self::getIdCatParent3($catLevel4);

                if($idParent != $catLevel1 && !empty($catLevel1) && $catLevel1 != 'Null'){
                    $catLevel2 = 0;
                    $catLevel3 = 0;
                    $catLevel4 = 0;
                }

                if($idParent2 != $catLevel2 && !empty($catLevel2) && $catLevel2 != 'Null'){
                    $catLevel3 = 0;
                    $catLevel4 = 0;
                }

                if($idParent3 != $catLevel3 && !empty($catLevel3) && $catLevel3 != 'Null'){
                    $catLevel4 = 0;
                }

                $today = date("Y-m-d H:i:s");

                $obParam = new VirtualDeskSiteParamsHelper();

                foreach($TagInput as $keyPar => $valParc ){
                    if(empty($tags)){
                        $tags = $valParc['tag'];
                    } else {
                        $tags .= ';;' . $valParc['tag'];
                    }
                }


                $resSaveCat = self::saveEdited4Admin($documento_id, $nome, $catLevel1, $catLevel2, $catLevel3, $catLevel4, $estado, $today, $tags);


                if (empty($resSaveCat)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objDocFiles                = new VirtualDeskSiteDocumentosFilesHelper();
                $listFileDoc2Eliminar       = $objDocFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_doc');
                $resFileDocDelete           = $objDocFiles->deleteFiles ($referencia , $listFileDoc2Eliminar);
                if ($resFileDocDelete==false && !empty($listFileDoc2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o documento', 'error');
                }


                // Insere os NOVOS ficheiros
                /* FILES */
                $objDocInFiles                = new VirtualDeskSiteDocumentosFilesHelper();
                $objDocInFiles->tagprocesso   = 'Documentos_POST';
                $objDocInFiles->idprocesso    = $referencia;
                $resFileSaveDoc             = $objDocInFiles->saveListFileByPOST('fileupload_doc');


                if ($resFileSaveDoc===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $documento_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEdited4Admin($documento_id, $nome, $catLevel1, $catLevel2, $catLevel3, $catLevel4, $estado, $data_alteracao, $tags){

            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'edit4admin'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($nome))  array_push($fields, 'nome="'.$db->escape($nome).'"');
            if(!is_null($catLevel1))   array_push($fields, 'id_nivel1='.$db->escape($catLevel1));
            if(!is_null($catLevel2))   array_push($fields, 'id_nivel2='.$db->escape($catLevel2));
            if(!is_null($catLevel3))   array_push($fields, 'id_nivel3='.$db->escape($catLevel3));
            if(!is_null($catLevel4))   array_push($fields, 'id_nivel4='.$db->escape($catLevel4));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));
            array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');
            if(!is_null($tags))  array_push($fields, 'tags="'.$db->escape($tags).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Documentos')
                ->set($fields)
                ->where(' id = '.$db->escape($documento_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnew4manager');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $nome       = $data['nome'];
                $catLevel1  = $data['catLevel1'];
                $catLevel2  = $data['catLevel2'];
                $catLevel3  = $data['catLevel3'];
                $catLevel4  = $data['catLevel4'];
                $TagInput   = $data['TagInput'];

                $referencia = '';
                $refExiste = 1;
                while($refExiste == 1){
                    $referencia = VirtualDeskSiteDocumentosHelper::random_codeDoc();
                    $checkREF   = VirtualDeskSiteDocumentosHelper::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                foreach($TagInput as $keyPar => $valParc ){
                    if(empty($tags)){
                        $tags = $valParc['tag'];
                    } else {
                        $tags .= ';;' . $valParc['tag'];
                    }
                }

                $resSaveDocumento = self::saveNewDocumento($referencia, $nome, $catLevel1, $catLevel2, $catLevel3, $catLevel4, $tags, '1');

                if (empty($resSaveDocumento)) {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteDocumentosFilesHelper();
                $objCapaFiles->tagprocesso   = 'Documentos_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_doc');

                if ($resFileSaveCapa===false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                self::SendEmailDocumentoAdmin($referencia, $nome);

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Documentos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED') . 'ref=' . $nome;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DOCUMENTOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function SendEmailDocumentoAdmin($referencia, $nome){

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Documentos_Admin_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailImage = $obParam->getParamsByTag('logoEmailGeral');
            $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
            $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $mailAdmin = $obParam->getParamsByTag('mailAdminDocumentos');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $mailAdmin;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_DOCUMENTOS_EMAIL_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_DOCUMENTOS_EMAIL_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_DOCUMENTOS_EMAIL_INTRO', $nome, $referencia);
            $BODY_COPYLINK = JText::sprintf($LinkCopyright);
            $BODY_COPYTELE = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM = JText::sprintf($dominioMunicipio);
            $BODY_COPYNAME = JText::sprintf($copyrightAPP);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_COPYLINK", $BODY_COPYLINK, $body);
            $body = str_replace("%BODY_COPYTELE", $BODY_COPYTELE, $body);
            $body = str_replace("%BODY_COPYMAIL", $BODY_COPYMAIL, $body);
            $body = str_replace("%BODY_COPYDOM", $BODY_COPYDOM, $body);
            $body = str_replace("%BODY_COPYNAME", $BODY_COPYNAME, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($mailAdmin);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT. '/' . $logosendmailImage, "banner", "Logo");

            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function getDocumentosDetail4Manager($id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('documentos');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('documentos', 'view4manager'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('documentos'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('documentos','view4manager'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as documento_id', 'a.referencia as codigo', 'a.nome as nome', 'c.categoria as catLevel1', 'd.categoria as catLevel2', 'e.categoria as catLevel3', 'f.categoria as catLevel4', 'a.estado as idestado', 'b.estado as estado', 'a.tags'))
                    ->join('LEFT', '#__virtualdesk_Documentos_Estado AS b ON b.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Documentos_categoria_nivel1 AS c ON c.id = a.id_nivel1')
                    ->join('LEFT', '#__virtualdesk_Documentos_categoria_nivel2 AS d ON d.id = a.id_nivel2')
                    ->join('LEFT', '#__virtualdesk_Documentos_categoria_nivel3 AS e ON e.id = a.id_nivel3')
                    ->join('LEFT', '#__virtualdesk_Documentos_categoria_nivel4 AS f ON f.id = a.id_nivel4')
                    ->from("#__virtualdesk_Documentos as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4admin.documentos.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.documentos.data', null);
            $app->setUserState('com_virtualdesk.edit4admin.documentos.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.documentos.data', null);

        }


        /*PLUGIN*/

        public static function getCat4DocLevel1Plugin(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Documentos_categoria_nivel1")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel2Plugin($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($catLevel1) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel3Plugin($catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel2'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($catLevel2) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCat4DocLevel4Plugin($catLevel3){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel3'))
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id_nivel3') . "='" . $db->escape($catLevel3) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatsFilteredLevel1($idCatLevel1Link){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel1 as nivel_anterior'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($idCatLevel1Link) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatsFilteredLevel2($idCatLevel2Link){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel2 as nivel_anterior'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($idCatLevel2Link) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatsFilteredLevel3($idCatLevel3Link){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel3 as nivel_anterior'))
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($db->quoteName('id_nivel3') . "='" . $db->escape($idCatLevel3Link) . "'")
                ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getIdCatLevel1($idCatLevel2Link){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_nivel1')
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "='" . $db->escape($idCatLevel2Link) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getIdCatLevel2($idCatLevel3Link){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_nivel2')
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id') . "='" . $db->escape($idCatLevel3Link) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getDocsFiltered($idCatLevel1Link, $idCatLevel2Link, $idCatLevel3Link, $idCatLevel4Link){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            if(!empty($idCatLevel1Link)){
                $where .= 'AND' . $db->quoteName('id_nivel1') . "='" . $db->escape($idCatLevel1Link) . "'";
            } else {
                $where .= 'AND' . $db->quoteName('id_nivel1') . "='" . $db->escape('0') . "'";
            }

            if(!empty($idCatLevel2Link)){
                $where .= 'AND' . $db->quoteName('id_nivel2') . "='" . $db->escape($idCatLevel2Link) . "'";
            } else {
                $where .= 'AND' . $db->quoteName('id_nivel2') . "='" . $db->escape('0') . "'";
            }

            if(!empty($idCatLevel3Link)){
                $where .= 'AND' . $db->quoteName('id_nivel3') . "='" . $db->escape($idCatLevel3Link) . "'";
            } else {
                $where .= 'AND' . $db->quoteName('id_nivel3') . "='" . $db->escape('0') . "'";
            }

            if(!empty($idCatLevel4Link)){
                $where .= 'AND' . $db->quoteName('id_nivel4') . "='" . $db->escape($idCatLevel4Link) . "'";
            } else {
                $where .= 'AND' . $db->quoteName('id_nivel4') . "='" . $db->escape('0') . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('id, referencia, nome, visualizacoes, data_criacao, data_alteracao'))
                ->from("#__virtualdesk_Documentos")
                ->where($where)
                ->order('nome ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDocsbyCat($maincat){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            $where .= 'AND' . $db->quoteName('id_nivel1') . "='" . $db->escape($maincat) . "'";


            $db->setQuery($db->getQuery(true)
                ->select(array('id, referencia, nome, visualizacoes, data_criacao, data_alteracao'))
                ->from("#__virtualdesk_Documentos")
                ->where($where)
                ->order('data_criacao DESC')
                //->setLimit('50')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDocsCatsbyCat($maincat){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            $where .= 'AND' . $db->quoteName('id_nivel1') . "='" . $db->escape($maincat) . "'";


            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($where)
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDocsCatsLevel3byCat($categoria){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            $where .= 'AND' . $db->quoteName('id_nivel2') . "='" . $db->escape($categoria) . "'";


            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel2'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($where)
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDocsCatsLevel4byCat($subcategoria){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            $where .= 'AND' . $db->quoteName('id_nivel3') . "='" . $db->escape($subcategoria) . "'";


            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel3'))
                ->from("#__virtualdesk_Documentos_categoria_nivel4")
                ->where($where)
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDocsbyCatFiltered($documento, $categoria, $subcategoria, $maincat){

            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            $where .= 'AND' . $db->quoteName('id_nivel1') . "='" . $db->escape($maincat) . "'";

            if(!empty($documento)){
                $nomeDoc = '%' . $documento . '%';
                $where .= 'AND' . $db->quoteName('nome') . "LIKE '" . $db->escape($nomeDoc) . "'";
            }

            if(!empty($categoria)){
                $where .= 'AND' . $db->quoteName('id_nivel2') . "='" . $db->escape($categoria) . "'";
            }

            if(!empty($subcategoria)){
                $where .= 'AND' . $db->quoteName('id_nivel3') . "='" . $db->escape($subcategoria) . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('id, referencia, nome, visualizacoes, data_criacao, data_alteracao'))
                ->from("#__virtualdesk_Documentos")
                ->where($where)
                ->order('data_criacao DESC')
                //->setLimit('50')
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getViews($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, visualizacoes'))
                ->from("#__virtualdesk_Documentos")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function updateViews($newVisualizacao, $id){
            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            array_push($fields, 'visualizacoes="'.$db->escape($newVisualizacao).'"');

            $query
                ->update('#__virtualdesk_Documentos')
                ->set($fields)
                ->where(' id = '.$db->escape($id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);

        }


        public static function getCatLevel2ByCat($catLevel1){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($catLevel1) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel2SelectByCat($catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "='" . $db->escape($catLevel2) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCatLevel2ByCat($catLevel1, $catLevel2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel1'))
                ->from("#__virtualdesk_Documentos_categoria_nivel2")
                ->where($db->quoteName('id') . "!='" . $db->escape($catLevel2) . "'")
                ->where($db->quoteName('id_nivel1') . "='" . $db->escape($catLevel1) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel3ByCat($idwebsitelist)
        {
            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . "='" . $db->escape($idwebsitelist) . "'")
                    ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                    ->order('name DESC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . "='" . $db->escape($idwebsitelist) . "'")
                    ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                    ->order('name DESC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function excludeCatLevel3ByCat($categoria, $subcategoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria as name, id_nivel2'))
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id_nivel2') . "='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($subcategoria) . "'")
                ->order('categoria DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCatLevel3ByCatName($subcategoria)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Documentos_categoria_nivel3")
                ->where($db->quoteName('id') . "='" . $db->escape($subcategoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getCategoriaNivel3Plugin($idwebsitelist,$onAjaxVD=0){

            $lang = VirtualDeskHelper::getLanguageTag();

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('categoria ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, categoria as name, id_nivel2'))
                    ->from("#__virtualdesk_Documentos_categoria_nivel3")
                    ->where($db->quoteName('id_nivel2') . '=' . $db->escape($idwebsitelist))
                    ->where( $db->quoteName('estado') . '=' . $db->escape('2')  )
                    ->order('categoria ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

    }

?>