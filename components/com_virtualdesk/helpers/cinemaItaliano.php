<?php

/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


class VirtualDeskSiteCinemaItalianoHelper
{

    public static function getFilmes()
    {
        $today = date("Y-m-d");

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, filme, tipo, data_filme'))
            ->from("#__virtualdesk_CinemaItaliano_Filmes")
            ->where($db->quoteName('data_maximo') . ">='" . $db->escape($today) . "'")
            ->order('id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function excludeFilme($filme){
        $today = date("Y-m-d");
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, filme, tipo, data_filme'))
            ->from("#__virtualdesk_CinemaItaliano_Filmes")
            ->where($db->quoteName('data_maximo') . ">='" . $db->escape($today) . "'")
            ->where($db->quoteName('id') . "!='" . $db->escape($filme) . "'")
            ->order('id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getFilmeName($filme)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, filme, tipo, data_filme'))
            ->from("#__virtualdesk_CinemaItaliano_Filmes")
            ->where($db->quoteName('id') . "='" . $db->escape($filme) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getNameFilme($filme){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('filme')
            ->from("#__virtualdesk_CinemaItaliano_Filmes")
            ->where($db->quoteName('id') . "='" . $db->escape($filme) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function validaNome($nome){
        if(empty($nome)){
            return (1);
        }else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$nome)) {
            return (2);
        } else {
            return (0);
        }
    }


    public static function validaEmail($email, $email2){
        if(empty($email)){
            return (1);
        } else if(empty($email2) || $email != $email2){
            return (2);
        } else {
            return (0);
        }
    }


    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
    }


    public static function CheckReferencia($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('referencia')
            ->from("#__virtualdesk_CinemaItaliano_Inscricoes")
            ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function saveNewForm($referencia, $filme, $numBilhetes, $residente, $nameReq, $emailReq){
        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);
        $columns = array('referencia','filme','numBilhetes','residente','nomeRequerente','emailRequerente');
        $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($filme)), $db->quote($db->escape($numBilhetes)), $db->quote($db->escape($residente)), $db->quote($db->escape($nameReq)), $db->quote($db->escape($emailReq)));
        $query
            ->insert($db->quoteName('#__virtualdesk_CinemaItaliano_Inscricoes'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean) $db->execute();

        return($result);

    }


    public static function SendEmailAdmin($referencia, $filme, $numBilhetes, $residente, $nameReq, $emailReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio){

        $config = JFactory::getConfig();

        /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
        $obPlg      = new VirtualDeskSitePluginsHelper();
        $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('cinemaItaliano','email2admin');

        if((string)$layoutPathEmail=='') {
            echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
            exit();
        }

        $nomeFilme = self::getNameFilme($filme);
        if($residente == 1){
            $residenteStatus = 'Residente no concelho ';
        } else {
            $residenteStatus = 'Não residente';
        }

        if($numBilhetes == 1){
            $numBilhetesStatus = 'Uma reserva';
        } else {
            $numBilhetesStatus = 'Duas reservas';
        }

        $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

        $obParam      = new VirtualDeskSiteParamsHelper();
        $emailAdmin = $obParam->getParamsByTag('mailAdminCinemaItaliano');


        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE                     = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL_TITULO');
        $BODY_TITLE2                    = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL_TITULO');
        $BODY_MESSAGE                   = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAILADMIN_CORPO', $nomeFilme);
        $BODY_MESSAGE2                  = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAILADMIN_2CORPO2');

        $BODY_SECTION1_TITLE            = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_INFORMACAO_FILME');
        $BODY_SECTION2_TITLE            = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_IDENTIFICACAO_REQUERENTE');
        $BODY_NAMEFILME_TITLE              = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_FILME');
        $BODY_NAMEFILME_VALUE              = JText::sprintf($nomeFilme);
        $BODY_NUMBILHETES_TITLE            = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_NUM_BILHETES');
        $BODY_NUMBILHETES_VALUE            = JText::sprintf($numBilhetesStatus);
        $BODY_RESTATUS_TITLE             = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_RELACAOCONCELHO');
        $BODY_RESTATUS_VALUE             = JText::sprintf($residenteStatus);
        $BODY_NAMEREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_NOMEREQUERENTE');
        $BODY_NAMEREQ_VALUE             = JText::sprintf($nameReq);
        $BODY_MAILREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL');
        $BODY_MAILREQ_VALUE             = JText::sprintf($emailReq);
        $BODY_EMAILNAME_VALUE           = JText::sprintf($emailCopyrightGeral);
        $BODY_TELEFNAME_VALUE           = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_DOMINIOMUNICIPIO_VALUE    = JText::sprintf($dominioMunicipio);
        $BODY_LINKMUNICIPIO_VALUE       = JText::sprintf($LinkCopyright);
        $BODY_COPYRIGHT_VALUE           = JText::sprintf($copyrightAPP);


        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_2MESSAGE",$BODY_MESSAGE2, $body);
        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
        $body      = str_replace("%BODY_SECTION1_TITLE",$BODY_SECTION1_TITLE, $body);
        $body      = str_replace("%BODY_SECTION2_TITLE",$BODY_SECTION2_TITLE, $body);
        $body      = str_replace("%BODY_NAMEFILME_TITLE",$BODY_NAMEFILME_TITLE, $body);
        $body      = str_replace("%BODY_NAMEFILME_VALUE",$BODY_NAMEFILME_VALUE, $body);
        $body      = str_replace("%BODY_NUMBILHETES_TITLE",$BODY_NUMBILHETES_TITLE, $body);
        $body      = str_replace("%BODY_NUMBILHETES_VALUE",$BODY_NUMBILHETES_VALUE, $body);
        $body      = str_replace("%BODY_RESTATUS_TITLE",$BODY_RESTATUS_TITLE, $body);
        $body      = str_replace("%BODY_RESTATUS_VALUE",$BODY_RESTATUS_VALUE, $body);
        $body      = str_replace("%BODY_NAMEREQ_TITLE",$BODY_NAMEREQ_TITLE, $body);
        $body      = str_replace("%BODY_NAMEREQ_VALUE",$BODY_NAMEREQ_VALUE, $body);
        $body      = str_replace("%BODY_MAILREQ_TITLE",$BODY_MAILREQ_TITLE, $body);
        $body      = str_replace("%BODY_MAILREQ_VALUE",$BODY_MAILREQ_VALUE, $body);
        $body      = str_replace("%BODY_EMAILNAME_VALUE",$BODY_EMAILNAME_VALUE, $body);
        $body      = str_replace("%BODY_TELEFNAME_VALUE",$BODY_TELEFNAME_VALUE, $body);
        $body      = str_replace("%BODY_DOMINIOMUNICIPIO_VALUE",$BODY_DOMINIOMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_LINKMUNICIPIO_VALUE",$BODY_LINKMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHT_VALUE",$BODY_COPYRIGHT_VALUE, $body);


        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($emailAdmin);
        $newActivationEmail->addRecipient('balcaoonline@cm-pontadosol.pt');
        $newActivationEmail->setSubject($data['sitename']);
        $logosendmailImage = $obParam->getParamsByTag('LogoEmailRegistoAPP');
        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.'/'.$logosendmailImage, "banner", "Logo");

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }


    public static function SendEmailUser($referencia, $filme, $numBilhetes, $residente, $nameReq, $emailReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio){

        $config = JFactory::getConfig();

        /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
        $obPlg      = new VirtualDeskSitePluginsHelper();
        $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('cinemaItaliano','email2user');

        if((string)$layoutPathEmail=='') {
            echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
            exit();
        }

        $nomeFilme = self::getNameFilme($filme);
        if($residente == 1){
            $residenteStatus = 'Residente no concelho ';
        } else {
            $residenteStatus = 'Não residente';
        }

        if($numBilhetes == 1){
            $numBilhetesStatus = 'Uma reserva';
        } else {
            $numBilhetesStatus = 'Duas reservas';
        }

        $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

        $obParam      = new VirtualDeskSiteParamsHelper();

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE                     = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL_TITULO');
        $BODY_TITLE2                    = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL_TITULO');
        $BODY_MESSAGE                   = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAILUSER_CORPO',$nomeFilme);
        $BODY_MESSAGE2                  = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAILUSER_CORPO2');
        $BODY_MESSAGE3                  = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAILADMIN_2CORPO2');

        $BODY_SECTION1_TITLE            = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_INFORMACAO_FILME');
        $BODY_SECTION2_TITLE            = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_IDENTIFICACAO_REQUERENTE');
        $BODY_NAMEFILME_TITLE              = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_FILME');
        $BODY_NAMEFILME_VALUE              = JText::sprintf($nomeFilme);
        $BODY_NUMBILHETES_TITLE            = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_NUM_BILHETES');
        $BODY_NUMBILHETES_VALUE            = JText::sprintf($numBilhetesStatus);
        $BODY_RESTATUS_TITLE             = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_RELACAOCONCELHO');
        $BODY_RESTATUS_VALUE             = JText::sprintf($residenteStatus);
        $BODY_NAMEREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_NOMEREQUERENTE');
        $BODY_NAMEREQ_VALUE             = JText::sprintf($nameReq);
        $BODY_MAILREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_CINEMAITALIANO_EMAIL');
        $BODY_MAILREQ_VALUE             = JText::sprintf($emailReq);
        $BODY_EMAILNAME_VALUE           = JText::sprintf($emailCopyrightGeral);
        $BODY_TELEFNAME_VALUE           = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_DOMINIOMUNICIPIO_VALUE    = JText::sprintf($dominioMunicipio);
        $BODY_LINKMUNICIPIO_VALUE       = JText::sprintf($LinkCopyright);
        $BODY_COPYRIGHT_VALUE           = JText::sprintf($copyrightAPP);


        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_2MESSAGE",$BODY_MESSAGE2, $body);
        $body      = str_replace("%BODY_3MESSAGE",$BODY_MESSAGE3, $body);
        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
        $body      = str_replace("%BODY_SECTION1_TITLE",$BODY_SECTION1_TITLE, $body);
        $body      = str_replace("%BODY_SECTION2_TITLE",$BODY_SECTION2_TITLE, $body);
        $body      = str_replace("%BODY_NAMEFILME_TITLE",$BODY_NAMEFILME_TITLE, $body);
        $body      = str_replace("%BODY_NAMEFILME_VALUE",$BODY_NAMEFILME_VALUE, $body);
        $body      = str_replace("%BODY_NUMBILHETES_TITLE",$BODY_NUMBILHETES_TITLE, $body);
        $body      = str_replace("%BODY_NUMBILHETES_VALUE",$BODY_NUMBILHETES_VALUE, $body);
        $body      = str_replace("%BODY_RESTATUS_TITLE",$BODY_RESTATUS_TITLE, $body);
        $body      = str_replace("%BODY_RESTATUS_VALUE",$BODY_RESTATUS_VALUE, $body);
        $body      = str_replace("%BODY_NAMEREQ_TITLE",$BODY_NAMEREQ_TITLE, $body);
        $body      = str_replace("%BODY_NAMEREQ_VALUE",$BODY_NAMEREQ_VALUE, $body);
        $body      = str_replace("%BODY_MAILREQ_TITLE",$BODY_MAILREQ_TITLE, $body);
        $body      = str_replace("%BODY_MAILREQ_VALUE",$BODY_MAILREQ_VALUE, $body);
        $body      = str_replace("%BODY_EMAILNAME_VALUE",$BODY_EMAILNAME_VALUE, $body);
        $body      = str_replace("%BODY_TELEFNAME_VALUE",$BODY_TELEFNAME_VALUE, $body);
        $body      = str_replace("%BODY_DOMINIOMUNICIPIO_VALUE",$BODY_DOMINIOMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_LINKMUNICIPIO_VALUE",$BODY_LINKMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHT_VALUE",$BODY_COPYRIGHT_VALUE, $body);


        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($emailReq);
        $newActivationEmail->setSubject($data['sitename']);
        $logosendmailImage = $obParam->getParamsByTag('LogoEmailRegistoAPP');
        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.'/'.$logosendmailImage, "banner", "Logo");

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }

}

?>