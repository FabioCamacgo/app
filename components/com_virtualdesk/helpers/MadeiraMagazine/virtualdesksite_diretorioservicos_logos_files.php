<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 *
 * @since  1.6
 */

JLoader::register('VirtualDeskTableDiretorioServicosLogo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/diretorioservicos_logo.php');
JLoader::register('VirtualDeskSiteDiretorioServicosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE. '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');

class VirtualDeskSiteDiretorioServicosLogosFilesHelper
{

    private $filefolder;

    public $idprocesso;
    public $tagprocesso;
    public $filetype;
    private $tmp_filename;

    public $resFileSaved;
    public $resFilesSave2Table;


    public function __construct()
    {
        $this->filefolder = JComponentHelper::getParams('com_virtualdesk')->get('diretorioservicosLogo_filefolder');
    }

    public function SetIdProcesso($idprocesso){
        $this->idprocesso = $idprocesso;
    }

    public function SetUserJoomlaId($iduser){
        $this->iduser = $iduser;
    }

    public function SetTagProcesso($tagprocesso){
        $this->tagprocesso = $tagprocesso;
    }



    public function saveListFileByAjax ($FileUploadTag)
    {
        $dtz     = new DateTimeZone("Europe/London"); //Your timezone
        $date    = new DateTime('NOW', $dtz);

        $FileListAjax = $_FILES[$FileUploadTag];
        if(empty($FileListAjax)) return false;

        $FileList = array();

        $timenow = $date->getTimestamp();
        $data    = $date->format('Y-m-d H:i:s');
        $doc_name  = 'diretorioservicosLogo_' . rand(100,999) . '_' . $timenow ;

        $rowFileAdd = array();
        $rowFileAdd['name']  = $FileListAjax['name'][0];
        $rowFileAdd['size']  = $FileListAjax['size'][0];
        $rowFileAdd['error'] = $FileListAjax['error'][0];
        $rowFileAdd['type']  = $FileListAjax['type'][0];
        $rowFileAdd['tmp_name']  = $FileListAjax['tmp_name'][0];
        $rowFileAdd['forceuniquename'] = $doc_name;
        $FileList[]['attachment'] = $rowFileAdd;

        $this->resFileSaved = VirtualDeskSiteFileUploadHelper::saveFileListToFolderSecured (0, $FileList,   $this->filefolder);

        if ($this->resFileSaved !== false or $this->resFileSaved !== null) {

            $this->resFilesSave2Table = $this->saveFileToTable($this->resFileSaved);
        }

        return(true);
    }

    public function saveListFileByPOST ($FileUploadTag)
    {
        $dtz     = new DateTimeZone("Europe/London"); //Your timezone
        $date    = new DateTime('NOW', $dtz);
        $FileListPOST = $_FILES[$FileUploadTag];
        if(empty($FileListPOST)) return true;
        $FileList = array();

        foreach ($FileListPOST['name'] as $idrow => $valrow) {
            $timenow  = $date->getTimestamp();
            $data     = $date->format('Y-m-d H:i:s');
            $doc_name = 'diretorioservicosLogo_' . rand(100, 999) . '_' . $timenow;
            $basename = $FileListPOST['data'][$idrow]['basename']; // se existir o basename é porque o ficheirio já existe e não é necessário voltar a fazer o uploda

            if((int) $FileListPOST['error'][$idrow]==0 &&  (string)$basename=='') {
                $rowFileAdd = array();
                $rowFileAdd['name'] = $FileListPOST['name'][$idrow];
                $rowFileAdd['size'] = $FileListPOST['size'][$idrow];
                $rowFileAdd['error'] = $FileListPOST['error'][$idrow];
                $rowFileAdd['type'] = $FileListPOST['type'][$idrow];
                $rowFileAdd['tmp_name'] = $FileListPOST['tmp_name'][$idrow];
                $rowFileAdd['forceuniquename'] = $doc_name;
                $FileList[]['attachment'] = $rowFileAdd;
            }
        }

        $this->resFileSaved = VirtualDeskSiteFileUploadHelper::saveFileListToFolderSecured (0, $FileList,  $this->filefolder);

        if ($this->resFileSaved !== false && $this->resFileSaved !== null) {
            $this->resFilesSave2Table = $this->saveFileToTable($this->resFileSaved);
        }
        else {
            return false;
        }

        return(true);
    }

    public function saveFileToFolder ($FileList)
    {
        $saveFileList = VirtualDeskSiteFileUploadHelper::saveFileListToFolderSecured (0, $FileList,   $this->filefolder, '');

        return $saveFileList;
    }


    public function saveFileToTable ($FileSaved )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( !is_array($FileSaved) )  return false;
        if ( empty($FileSaved) )  return false;

        foreach ($FileSaved as $rowFileSaved) {

            $data['idprocesso'] = $this->idprocesso;
            $data['tagprocesso'] = $this->tagprocesso;
            $data['filename'] = $rowFileSaved['filename'];
            $data['basename'] = $rowFileSaved['basename'];
            $data['ext'] = $rowFileSaved['ext'];
            $data['type'] = $rowFileSaved['type'];
            $data['size'] = $rowFileSaved['size'];
            $data['desc'] = $rowFileSaved['desc'];
            $data['filepath'] = $rowFileSaved['filepath'];

            // Carrega dados atuais na base de dados
            $db = JFactory::getDbo();
            $Table = new VirtualDeskTableDiretorioServicosLogo($db);

            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;
        }
        return true;
    }


    /**
     * Método para poder descarregar um ficheiro a partir de um link com o seu basename
     * Eset link vai poder ser invocado de um email sem ser necessário iniciar sessão
     * @return mixed
     */
    public function downloadWithNoSession()
    {
        $jinput = JFactory::getApplication()->input;
        $EventoRefId = $jinput->get('refid');
        $basename    = $jinput->get('bname');
        $checkname   = $jinput->get('cname'); // nome

        // vai comparar se o basename e o cname correspondem a partir da desencriptação
        if(!empty($checkname)) {
            $verificaNome = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($basename);
            $objFile = null;
            if( (string)$verificaNome === (string) $checkname && (string)$verificaNome != '' ) {
                // carrega o ficheiro...
                $objFile = $this->getFileName2GuestLink ($EventoRefId, $basename);
            }
        }

        if (empty($objFile)) {
            return false;
            exit;
        }
        else {

            VirtualDeskSiteFileUploadHelper::setDownloadHeaders ($objFile);
            exit;
        }
    }



    /*
    * Carrega ficheiro associado a um registo SEM ser necessário o joomla user
    */
    public function getFileName2GuestLink ($RefId, $basename)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idprocesso','a.basename','a.desc','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Logo AS a")
                ->where($db->quoteName('a.idprocesso') . '=\'' . $db->escape($RefId). '\' '
                    . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Carrega a lista de ficheiros associados a um evento/cultura pelo RefId
    */
    public function getFileListByRefId ($RefId)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idprocesso','a.basename','a.desc','a.filename','a.filepath','a.ext','a.created','a.modified','a.size','a.type') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Logo AS a")
                ->where($db->quoteName('a.idprocesso') . '=\'' . $db->escape($RefId). '\' ')
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Gera a lista de "guest links" para fazer download de ficheiros sem haver necessidade de iniciar sessão na App
    */
    public function getFileGuestLinkByRefId ($RefId)
    {
        if(empty($RefId)) return(false);

        $objFileLst = $this->getFileListByRefId ($RefId);

        // Percorre cada ficheiro encontrado e gera um link para ser utilizado posteriormente

        if(empty($objFileLst))return(false);

        $arReturn = array();
        foreach ($objFileLst as $rowFile) {
            $objFileTmp = new stdClass();
            $objFileTmp->refid = $rowFile->idprocesso;
            $objFileTmp->bname = $rowFile->basename;
            $objFileTmp->fname = $rowFile->filename;
            $objFileTmp->fpath = $rowFile->filepath;
            $objFileTmp->desc = $rowFile->desc;
            $objFileTmp->cname = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($rowFile->basename);

            $objFileTmp->guestlink = JUri::root().'diretorioservicosLogo/dl/?refid=' . $rowFile->idprocesso . '&bname=' . $rowFile->basename . '&cname=' .  $objFileTmp->cname;

            //$objFileTmp->guestlink = $rowFile->filepath . '/' . $rowFile->filename;

            $arReturn[] = $objFileTmp;
        }

        return($arReturn);
    }


    /*
    * Gera a lisa de basenames de ficheiros de um evento
    */
    public function getFileBaseNameByRefId ($RefId)
    {
        if(empty($RefId)) return(false);
        $objFileLst = $this->getFileListByRefId ($RefId);
        if(empty($objFileLst))return(false);
        $arReturn = array();
        foreach ($objFileLst as $rowFile) {
            $arReturn[] = $rowFile->basename;
        }

        return($arReturn);
    }



    /*
    * Gera a lista ficheiros para serem pré carregados no fileuploader https://innostudio.de/fileuploader
    * Aqui prevemos que será necessário uma sessão para este acesso ?
    */
    public function getFileUploaderJS_ByRefId_4Manager ($RefId)
    {
        if(empty($RefId)) return(false);

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('diretorioservicos');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $objFileLst = $this->getFileListByRefId ($RefId);

        // Percorre cada ficheiro encontrado e gera um link para ser utilizado posteriormente
        if(empty($objFileLst))return(false);

        $vsReturnJSIni = '[';
        $vsReturnJS    = '';
        foreach ($objFileLst as $rowFile) {
            $cname     = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($rowFile->basename);
            $guestlink = JUri::root().'diretorioservicosLogo/dl/?refid=' . $rowFile->idprocesso . '&bname=' . $rowFile->basename . '&cname=' . $cname;

            if(!empty($vsReturnJS)) $vsReturnJS .= ' , ';

            $vsReturnJS .= '{';
            $vsReturnJS .= "name:'".$rowFile->filename."',";
            $vsReturnJS .= "size:".(int)$rowFile->size.",";
            $vsReturnJS .= "type:'".$rowFile->type."',";
            $vsReturnJS .= "file:'".$guestlink."',";
            $vsReturnJS .= "data:{bname:'".$rowFile->basename."'}";
            $vsReturnJS .= '}';
        }
        $vsReturnJSEnd = ']';

        return($vsReturnJSIni . $vsReturnJS . $vsReturnJSEnd);
    }


    /*
   * Gera a lista ficheiros para serem pré carregados no fileuploader https://innostudio.de/fileuploader
   * Aqui prevemos que será necessário uma sessão para este acesso ?
   */
    public function getFileUploaderJS_ByRefId_4User ($RefId)
    {
        if(empty($RefId)) return(false);

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('diretorioservicos');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'edit4users'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $RefIdCheck = VirtualDeskSiteDiretorioServicosHelper::checkRefId_4User_By_NIF_Logo ($RefId);
        if((string)$RefIdCheck != (string)$RefId) return false;

        $objFileLst = $this->getFileListByRefId ($RefId);

        // Percorre cada ficheiro encontrado e gera um link para ser utilizado posteriormente
        if(empty($objFileLst))return(false);

        $vsReturnJSIni = '[';
        $vsReturnJS    = '';
        foreach ($objFileLst as $rowFile) {
            $cname     = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($rowFile->basename);
            $guestlink = JUri::root().'diretorioservicosLogo/dl/?refid=' . $rowFile->idprocesso . '&bname=' . $rowFile->basename . '&cname=' . $cname;

            if(!empty($vsReturnJS)) $vsReturnJS .= ' , ';

            $vsReturnJS .= '{';
            $vsReturnJS .= "name:'".$rowFile->filename."',";
            $vsReturnJS .= "size:".$rowFile->size.",";
            $vsReturnJS .= "type:'".$rowFile->type."',";
            $vsReturnJS .= "file:'".$guestlink."',";
            $vsReturnJS .= "data:{bname:'".$rowFile->basename."'}";
            $vsReturnJS .= '}';
        }
        $vsReturnJSEnd = ']';

        return($vsReturnJSIni . $vsReturnJS . $vsReturnJSEnd);
    }

    /*
   * Gera a lista ficheiros para serem pré carregados no fileuploader https://innostudio.de/fileuploader
   * Aqui prevemos que será necessário uma sessão para este acesso ?
   */
    public function setListFile2EliminarFromPOST ($RefId,$FileUploadTag='')
    {
        // Verifica ficheiros a eliminar, comparando o que existe com o que foi enviado
        $listFicheiroPOST  = array();
        $listBNamePOST     = array();
        $FileUploadTag2Use = 'fileuploader-list-fileupload';
        if(!empty($FileUploadTag)) $FileUploadTag2Use = $FileUploadTag;

        if((string)$_POST[$FileUploadTag2Use]!='') {
            $listFicheiroPOST   = json_decode($_POST[$FileUploadTag2Use]);
        }
        foreach($listFicheiroPOST as $keyFlPost => $valFlPost)
        {
            if(!empty($valFlPost->file)) {
                if((string)$valFlPost->file!='') {
                    $listBNameTMP = array();
                    parse_str($valFlPost->file, $listBNameTMP);
                    if(!empty((string)$listBNameTMP['bname'])) {
                        $listBNamePOST[] = (string)$listBNameTMP['bname'];
                    }
                }
            }
        }

        $listFicheiroAtuais = $this->getFileGuestLinkByRefId($RefId);
        // Verifica ficheiros a eliminar
        $listFile2Eliminar = array();
        foreach($listFicheiroAtuais as $keyFlAct => $valFlAct )
        {
            if(in_array((string)$valFlAct->bname,$listBNamePOST) !== true) {
                $listFile2Eliminar[] = (string)$valFlAct->bname;
            }
        }

        return($listFile2Eliminar);
    }



    public function deleteFiles ($RefId , $listFile2Eliminar)
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( !is_array($listFile2Eliminar) )  return false;
        if ( empty($listFile2Eliminar) || empty($RefId) )  return false;

        // Carrega dados atuais na base de dados
        $db = JFactory::getDbo();
        $db->transactionStart();
        foreach ($listFile2Eliminar as $rowBName) {

            $Table = new VirtualDeskTableDiretorioServicosLogo($db);
            $Table->load(array('idprocesso'=>$RefId, 'basename'=>$rowBName));

            if ((int)$Table->id<=0) {
                $db->transactionRollback();
                return false;
            }

            // Eliminar ficheiro do diretório....
            $Folder    = (string)$Table->filepath;
            $FileName  = (string)$Table->filename;

            // Vai eliminar da bd de dados e depois vai eliminar os ficheiros...
            $Table->delete($Table->id);

            $resDel = VirtualDeskSiteFileUploadHelper::deleteFileFromFolder ($Folder, $FileName);

            if($resDel!=true )  {
                $db->transactionRollback();
                return false;
            }
        }
        $db->transactionCommit();
        return true;
    }

}