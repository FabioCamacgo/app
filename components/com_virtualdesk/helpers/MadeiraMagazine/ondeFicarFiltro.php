<?php

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

class VirtualDeskSiteMMOndeFicarFiltroHelper{

    public static function getCatOndeFicar(){
        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $Name = 'name_PT';
        } else if($lang='en_EN') {
            $Name = 'name_EN';
        } else if($lang='fr_FR') {
            $Name = 'name_FR';
        } else if($lang='de_DE') {
            $Name = 'name_DE';
        } else {
            $Name = 'name_PT';
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, ' . $Name . ' as name'))
            ->from("#__virtualdesk_MadeiraMagazine_OndeFicar_Categorias")
            ->order('name ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function excludeCatOndeFicar($catOndeFicar){
        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $Name = 'name_PT';
        } else if($lang='en_EN') {
            $Name = 'name_EN';
        } else if($lang='fr_FR') {
            $Name = 'name_FR';
        } else if($lang='de_DE') {
            $Name = 'name_DE';
        } else {
            $Name = 'name_PT';
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id,' . $Name . ' as name'))
            ->from("#__virtualdesk_MadeiraMagazine_OndeFicar_Categorias")
            ->where($db->quoteName('id') . "!='" . $db->escape($catOndeFicar) . "'")
            ->order('name ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getCatOndeFicarSelect($catOndeFicar){

        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $Name = 'name_PT';
        } else if($lang='en_EN') {
            $Name = 'name_EN';
        } else if($lang='fr_FR') {
            $Name = 'name_FR';
        } else if($lang='de_DE') {
            $Name = 'name_DE';
        } else {
            $Name = 'name_PT';
        }


        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select($Name . ' as name')
            ->from("#__virtualdesk_MadeiraMagazine_OndeFicar_Categorias")
            ->where($db->quoteName('id') . "='" . $db->escape($catOndeFicar) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getConcelhos(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, concelho'))
            ->from("#__virtualdesk_MadeiraMagazine_Concelhos")
            ->order('concelho ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function excludeConcelhos($concelho){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, concelho'))
            ->from("#__virtualdesk_MadeiraMagazine_Concelhos")
            ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
            ->order('concelho ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getConcelhosSelect($concelho){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('concelho')
            ->from("#__virtualdesk_MadeiraMagazine_Concelhos")
            ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

}

?>