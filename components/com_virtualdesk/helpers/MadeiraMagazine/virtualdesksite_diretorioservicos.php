<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_capa_files.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_logos_files.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_galeria_files.php');
    JLoader::register('VirtualDeskTableDiretorioServicos', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/diretorioservicos.php');
    JLoader::register('VirtualDeskTableDiretorioServicosEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/diretorioservicos_estado_historico.php');


    class VirtualDeskSiteDiretorioServicosHelper
    {

        const tagchaveModulo = 'diretorioservicos';


        public static function getFreguesia($idwebsitelist)
        {
            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function excludeFreguesia($concelho, $freguesia2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($freguesia2) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getFregName($freguesia)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 20);
        }


        public static function CheckReferencia($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Empresas")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-pendente';
                break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }


        public static function getEstado ()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id_estado as id','estado') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Estado")
                ->order('id_estado ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name') )
                    ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstadoName ($estado)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'estado')
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($estado) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeEstado($estado)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id_estado as id','estado') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Estado")
                ->where($db->quoteName('id_estado') . "!='" . $db->escape($estado) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoria()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_PT as categoria') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Categorias")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('name_PT ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriaName ($categoria)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'name_PT')
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Categorias")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCategoria($categoria)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_PT as categoria') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Categorias")
                ->where($db->quoteName('id') . "!='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('name_PT ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getSubcategoria($idwebsitelist)
        {
            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, name_PT as name, cat_id'))
                    ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Subcategorias")
                    ->where($db->quoteName('cat_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, name_PT as name, cat_id'))
                    ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Subcategorias")
                    ->where($db->quoteName('cat_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function excludeSubcategoria($categoria, $subcategoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, name_PT as name, cat_id'))
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Subcategorias")
                ->where($db->quoteName('cat_id') . "='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($subcategoria) . "'")
                ->order('name_PT ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getSubcategoriaName($subcategoria)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('name_PT')
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Subcategorias")
                ->where($db->quoteName('id') . "='" . $db->escape($subcategoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getConcelho($IdDistrito)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','concelho') )
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($IdDistrito) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getConcelhoName($concelho)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeConcelho($IdDistrito, $concelho)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','concelho') )
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($IdDistrito) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTipoFirma()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_PT as tipoFirma') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_TipoFirma")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('name_PT ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getipoFirmaName($tipofirma)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'name_PT')
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_TipoFirma")
                ->where($db->quoteName('id') . "='" . $db->escape($tipofirma) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeTipoFirma($tipofirma)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_PT as tipoFirma') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_TipoFirma")
                ->where($db->quoteName('id') . "!='" . $db->escape($tipofirma) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('name_PT ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getIdiomaFalado()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_PT as idioma') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_IdiomasFalados")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getIdiomaName($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'name_PT as idioma')
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_IdiomasFalados")
                ->where($db->quoteName('id') . "='" . $db->escape($id) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getPremios()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','name_PT as premio') )
                ->from("#__virtualdesk_MadeiraMagazine_DirServicos_SelosPremios")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function checkRefId_4User_By_NIF_Capa ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_MadeiraMagazine_DirServicos_FotoCapa as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function checkRefId_4User_By_NIF_Galeria ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Galeria as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function checkRefId_4User_By_NIF_Logo ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Logo as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('diretorioservicos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            try {
                $refExiste = 1;


                $nomeEmpresa = $data['nomeEmpresa'];
                $designacaoComercial = $data['designacaoComercial'];
                $nipcEmpresa = $data['nipcEmpresa'];
                $emailContacto = $data['emailContacto'];
                $tipofirma = $data['tipofirma'];
                $descricao = $data['descricao'];
                $categoria = $data['categoria'];
                $subcategoria = $data['subcategoria'];
                $concelho = $data['concelho'];
                $freguesia = $data['freguesia'];
                $morada = $data['morada'];
                $coordenadas = $data['coordenadas'];
                $telefone = $data['telefone'];
                $website = $data['website'];
                $emailComercial = $data['emailComercial'];
                $emailAdministrativo = $data['emailAdministrativo'];
                $horaAbertura = $data['horaAbertura'];
                $horaEncerramento = $data['horaEncerramento'];
                $abertoAlmoco = $data['abertoAlmoco'];
                $inicioAlmoco = $data['inicioAlmoco'];
                $fimAlmoco = $data['fimAlmoco'];
                $abertoFimSemana = $data['abertoFimSemana'];
                $horaAberturaSabado = $data['horaAberturaSabado'];
                $horaEncerramentoSabado = $data['horaEncerramentoSabado'];
                $horaAberturaDomingo = $data['horaAberturaDomingo'];
                $horaEncerramentoDomingo = $data['horaEncerramentoDomingo'];
                $facebook = $data['facebook'];
                $instagram = $data['instagram'];
                $youtube = $data['youtube'];
                $twitter = $data['twitter'];
                $checkboxHide1 = $data['checkboxHide1'];
                $checkboxHide2 = $data['checkboxHide2'];
                $checkboxHide3 = $data['checkboxHide3'];
                $checkboxHide4 = $data['checkboxHide4'];
                $checkboxHide5 = $data['checkboxHide5'];
                $checkboxHide6 = $data['checkboxHide6'];
                $checkboxHide7 = $data['checkboxHide7'];
                $checkboxHide8 = $data['checkboxHide8'];
                $checkboxHide9 = $data['checkboxHide9'];
                $checkboxHide10 = $data['checkboxHide10'];
                $checkboxHide11 = $data['checkboxHide11'];
                $checkboxHide12 = $data['checkboxHide12'];
                $checkboxoutro = $data['checkboxoutro'];
                $IdiomaInput = $data['IdiomaInput'];
                $premioHide1 = $data['premioHide1'];
                $premioHide2 = $data['premioHide2'];
                $informacoes = $data['informacoes'];
                $VideoInput = $data['VideoInput'];
                $estado = '2';
                $tipo_Pack = '1';


                for($i=0; $i< count($subcategoria); $i++){
                    if($i==0){
                        $subcats = $subcategoria['0'];
                    } else {
                        $subcats .= ',' . $subcategoria[$i];
                    }
                }

                $vdFileUpChangedCapa = $data['vdFileUpChangedCapa'];
                $vdFileUpChangedLogo = $data['vdFileUpChangedLogo'];
                $vdFileUpChangedGaleria = $data['vdFileUpChangedGaleria'];

                if($abertoAlmoco == 0 || empty($abertoAlmoco)){
                    $abertoAlmoco = 0;
                }

                if($abertoFimSemana == 0 || empty($abertoFimSemana)){
                    $abertoFimSemana = 0;
                }

                if($checkboxHide1 == 1){
                    $idiomasFalados = '1';
                }

                if($checkboxHide2 == 2){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';2';
                    } else {
                        $idiomasFalados = '2';
                    }
                }

                if($checkboxHide3 == 3){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';3';
                    } else {
                        $idiomasFalados = '3';
                    }
                }

                if($checkboxHide4 == 4){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';4';
                    } else {
                        $idiomasFalados = '4';
                    }
                }

                if($checkboxHide5 == 5){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';5';
                    } else {
                        $idiomasFalados = '5';
                    }
                }

                if($checkboxHide6 == 6){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';6';
                    } else {
                        $idiomasFalados = '6';
                    }
                }

                if($checkboxHide7 == 7){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';7';
                    } else {
                        $idiomasFalados = '7';
                    }
                }

                if($checkboxHide8 == 8){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';8';
                    } else {
                        $idiomasFalados = '8';
                    }
                }

                if($checkboxHide9 == 9){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';9';
                    } else {
                        $idiomasFalados = '9';
                    }
                }

                if($checkboxHide10 == 10){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';10';
                    } else {
                        $idiomasFalados = '10';
                    }
                }

                if($checkboxHide11 == 11){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';11';
                    } else {
                        $idiomasFalados = '11';
                    }
                }

                if($checkboxHide12 == 12){
                    if(!empty($idiomasFalados)){
                        $idiomasFalados .= ';12';
                    } else {
                        $idiomasFalados = '12';
                    }
                }

                if($premioHide1 == 1){
                    $cleanSafe = '1';
                } else {
                    $cleanSafe = '2';
                }

                if($premioHide2 == 1){
                    $marcaMadeira = '1';
                } else {
                    $marcaMadeira = '2';
                }


                foreach($VideoInput as $keyPar => $valParc ){
                    if($valParc['video'] != ''){
                        if(empty($video)){
                            $video = $valParc['video'];
                        } else {
                            $video .= ';;' . $valParc['video'];
                        }

                    }
                }

                foreach($IdiomaInput as $keyPar => $valParc ){
                    if($valParc['idioma'] != ''){
                        if(empty($idioma)){
                            $idioma = $valParc['idioma'];
                        } else {
                            $idioma .= ';;' . $valParc['idioma'];
                        }
                    }
                }

                $splitCoord = explode(",", $coordenadas);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];


                $referencia = '';
                while($refExiste == 1){
                    $random = self::random_code();

                    $referencia = $random;
                    $checkREF = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                $resSaveEmpresa = self::saveNewEmpresa4Manager($referencia, $nomeEmpresa, $designacaoComercial, $nipcEmpresa, $emailContacto, $tipofirma, $descricao, $categoria, $subcats, $concelho, $freguesia, $morada, $lat, $long, $telefone, $website, $emailComercial, $emailAdministrativo, $horaAbertura, $horaEncerramento, $abertoAlmoco, $inicioAlmoco, $fimAlmoco, $abertoFimSemana, $horaAberturaSabado, $horaEncerramentoSabado, $horaAberturaDomingo, $horaEncerramentoDomingo, $facebook, $instagram, $youtube, $twitter, $idiomasFalados, $idioma, $cleanSafe, $marcaMadeira, $informacoes, $video, $estado, $tipo_Pack);

                if (empty($resSaveEmpresa)) return false;

                $dirServicosFiles               =  new VirtualDeskSiteDiretorioServicosFilesHelper();
                $dirServicosFiles->tagprocesso  = 'CAPA_POST';
                $dirServicosFiles->idprocesso   = $referencia;
                $resFileSave                    = $dirServicosFiles->saveListFileByPOST('fileuploadCapa');

                $dirServicosLogoFiles               =  new VirtualDeskSiteDiretorioServicosLogosFilesHelper();
                $dirServicosLogoFiles->tagprocesso  = 'LOGO_POST';
                $dirServicosLogoFiles->idprocesso   = $referencia;
                $resLogoSave                        = $dirServicosLogoFiles->saveListFileByPOST('fileuploadLogo');

                $dirServicosGaleriaFiles               =  new VirtualDeskSiteDiretorioServicosGaleriaFilesHelper();
                $dirServicosGaleriaFiles->tagprocesso  = 'GALERIA_POST';
                $dirServicosGaleriaFiles->idprocesso   = $referencia;
                $resGaleriaSave                        = $dirServicosGaleriaFiles->saveListFileByPOST('fileuploadGaleria');

                if ($resFileSave===false || $resLogoSave===false || $resGaleriaSave===false) {
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewEmpresa4Manager($referencia, $nomeEmpresa, $designacaoComercial, $nipcEmpresa, $emailContacto, $tipofirma, $descricao, $categoria, $subcategoria, $concelho, $freguesia, $morada, $lat, $long, $telefone, $website, $emailComercial, $emailAdministrativo, $horaAbertura, $horaEncerramento, $abertoAlmoco, $inicioAlmoco, $fimAlmoco, $abertoFimSemana, $horaAberturaSabado, $horaEncerramentoSabado, $horaAberturaDomingo, $horaEncerramentoDomingo, $facebook, $instagram, $youtube, $twitter, $idiomasFalados, $idioma, $cleanSafe, $marcaMadeira, $informacoes, $video, $estado, $tipo_Pack){
            $db = JFactory::getDbo();

            $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);
            $informacoes = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($informacoes),true);

            $query = $db->getQuery(true);
            $columns = array('referencia', 'nome_PT', 'nome_comercial', 'nipc', 'email_contacto', 'tipo_firma', 'descricao', 'categoria', 'subcategoria', 'concelho', 'freguesia', 'morada', 'latitude', 'longitude', 'telefone', 'website', 'email_comercial', 'email_administrativo', 'hora_abertura', 'hora_fecho', 'aberto_almoco', 'inicio_almoco', 'fim_almoco', 'abertoFimSemana', 'abertura_sabado', 'fecho_sabado', 'abertura_domingo', 'fecho_domingo', 'facebook', 'instagram', 'youtube', 'twitter', 'idiomas', 'outros_idiomas', 'cleanAndSafe', 'produtoMadeira', 'informacoes', 'videos', 'tipo_conta', 'estado');
            $values = array($db->quote($referencia), $db->quote($nomeEmpresa), $db->quote($designacaoComercial), $db->quote($nipcEmpresa), $db->quote($emailContacto), $db->quote($tipofirma), $db->quote($descricao), $db->quote($categoria), $db->quote($subcategoria), $db->quote($concelho), $db->quote($freguesia), $db->quote($morada), $db->quote($lat), $db->quote($long), $db->quote($telefone), $db->quote($website), $db->quote($emailComercial), $db->quote($emailAdministrativo), $db->quote($horaAbertura), $db->quote($horaEncerramento), $db->quote($abertoAlmoco), $db->quote($inicioAlmoco), $db->quote($fimAlmoco), $db->quote($abertoFimSemana), $db->quote($horaAberturaSabado), $db->quote($horaEncerramentoSabado), $db->quote($horaAberturaDomingo), $db->quote($horaEncerramentoDomingo), $db->quote($facebook), $db->quote($instagram), $db->quote($youtube), $db->quote($twitter), $db->quote($idiomasFalados), $db->quote($idioma), $db->quote($cleanSafe), $db->quote($marcaMadeira), $db->quote($informacoes), $db->quote($video), $db->quote($tipo_Pack), $db->quote($estado));
            $query
                ->insert($db->quoteName('#__virtualdesk_MadeiraMagazine_DirServicos_Empresas'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);
        }


        public static function getDiretorioServicosList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('diretorioservicos','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=diretorioservicos&layout=view4manager&diretorioservicos_id=');

                    $table  = " ( SELECT a.id as id, a.id as diretorioservicos_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as referencia, a.categoria as idcategoria, b.name_PT as categoria, a.subcategoria as idsubcategoria, c.name_PT as subcategoria, a.estado as idestado, d.estado as estado, e.concelho as concelho, a.concelho as idconcelho, a.nome_comercial as nome, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_MadeiraMagazine_DirServicos_Empresas as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_MadeiraMagazine_DirServicos_Categorias AS b ON b.id = a.categoria";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_MadeiraMagazine_DirServicos_Subcategorias AS c ON c.id = a.subcategoria";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_MadeiraMagazine_DirServicos_Estado AS d ON d.id_estado = a.estado";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_concelhos AS e ON e.id = a.concelho";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'referencia',            'dt' => 0 ),
                        array( 'db' => 'nome',                  'dt' => 1 ),
                        array( 'db' => 'categoria',             'dt' => 2 ),
                        array( 'db' => 'subcategoria',          'dt' => 3 ),
                        array( 'db' => 'concelho',              'dt' => 4 ),
                        array( 'db' => 'estado',                'dt' => 5 ),
                        array( 'db' => 'dummy',                 'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'diretorioservicos_id',  'dt' => 7 ),
                        array( 'db' => 'idcategoria',           'dt' => 8 ),
                        array( 'db' => 'idsubcategoria',        'dt' => 9 ),
                        array( 'db' => 'idconcelho',            'dt' => 10 ),
                        array( 'db' => 'idestado',              'dt' => 11 ),
                        array( 'db' => 'data_criacao',          'dt' => 12 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getDiretorioServicosDetail4Manager($id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('diretorioservicos');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('diretorioservicos'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('diretorioservicos','edit4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id', 'a.referencia as codigo', 'a.nome_PT as nome_empresa', 'a.nome_comercial', 'a.nipc', 'a.email_contacto', 'a.tipo_firma as id_tipo_firma',
                        'b.name_PT as tipo_firma', 'a.descricao', 'a.categoria as id_categoria', 'c.name_PT as categoria', 'a.subcategoria', 'a.concelho as id_concelho', 'd.concelho as concelho',
                        'a.freguesia as if_freguesia', 'e.freguesia as freguesia', 'a.morada', 'a.latitude', 'a.longitude', 'a.telefone', 'a.website', 'a.email_comercial', 'a.email_administrativo',
                        'a.hora_abertura', 'a.hora_fecho', 'a.aberto_almoco', 'a.inicio_almoco', 'a.fim_almoco', 'a.abertoFimSemana', 'a.abertura_sabado', 'a.fecho_sabado', 'a.abertura_domingo',
                        'a.fecho_domingo', 'a.facebook', 'a.instagram', 'a.youtube', 'a.twitter', 'a.idiomas', 'a.outros_idiomas', 'a.cleanAndSafe', 'a.produtoMadeira',
                        'a.informacoes', 'a.videos', 'a.tipo_conta', 'a.estado as id_estado', 'g.estado as estado', 'a.data_criacao', 'a.data_alteracao'))
                    ->join('LEFT', '#__virtualdesk_MadeiraMagazine_DirServicos_TipoFirma AS b ON b.id = a.tipo_firma')
                    ->join('LEFT', '#__virtualdesk_MadeiraMagazine_DirServicos_Categorias AS c ON c.id = a.categoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS d ON d.id = a.concelho')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS e ON e.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_MadeiraMagazine_DirServicos_Estado AS g ON g.id_estado = a.estado')
                    ->from("#__virtualdesk_MadeiraMagazine_DirServicos_Empresas as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function saveAlterar2NewEstado4ManagerByAjax($getInputDiretorioServicos_id, $NewEstadoId, $NewEstadoDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('diretorioservicos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('diretorioservicos', 'alterarestado4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewEstadoId) ) return false;
            if((int) $getInputDiretorioServicos_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            $data = array();
            $data['estado'] = $NewEstadoId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();

            $Table = new VirtualDeskTableDiretorioServicos($db);
            $Table->load(array('id'=>$getInputDiretorioServicos_id));

            // Só alterar se o estado for diferente
            if((int)$Table->estado == (int)$NewEstadoId )  return true;

            if(!empty($data)) {
                //$dateModified = new DateTime();
                //$data['modified'] = $dateModified->format('Y-m-d H:i:s');
            }

            $db->transactionStart();

            try {
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTableDiretorioServicosEstadoHistorico($db);
                $dataHist = array();
                $dataHist['id_estado']  = $NewEstadoId;
                $dataHist['id_empresa']  = $getInputDiretorioServicos_id;
                $dataHist['descricao']  = $NewEstadoDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('DiretorioServicos_Log_Geral_Enabled');


                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_empresa=' . $dataHist['id_empresa'];
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EVENTLOG_STATE_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('diretorioservicos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $diretorioservicos_id = $data['empresa_id'];
            if((int) $diretorioservicos_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDiretorioServicos($db);
            $Table->load(array('id'=>$diretorioservicos_id));

            $db->transactionStart();

            try {
                $referencia          = $Table->referencia;
                $obParam    = new VirtualDeskSiteParamsHelper();

                $nomeEmpresa = null;
                $designacaoComercial = null;
                $nipc = null;
                $email_contacto = null;
                $tipo_firma = null;
                $descricao = null;
                $categoria = null;
                $subcategoria = null;
                $concelho = null;
                $freguesia = null;
                $morada = null;
                $latitude = null;
                $longitude = null;
                $telefone = null;
                $website = null;
                $email_comercial = null;
                $email_administrativo = null;
                $hora_abertura = null;
                $hora_fecho = null;
                $aberto_almoco = null;
                $inicio_almoco = null;
                $fim_almoco = null;
                $abertoFimSemana = null;
                $abertura_sabado = null;
                $fecho_sabado = null;
                $abertura_domingo = null;
                $fecho_domingo = null;
                $facebook = null;
                $instagram = null;
                $youtube = null;
                $twitter = null;
                $idiomas = null;
                $outros_idiomas = null;
                $cleanAndSafe = null;
                $produtoMadeira = null;
                $informacoes = null;
                $videos = null;
                $tipo_conta = null;
                $estado = null;
                $data_criacao = null;
                $data_alteracao = null;

                if(array_key_exists('nomeEmpresa',$data))               $nomeEmpresa = $data['nomeEmpresa'];
                if(array_key_exists('designacaoComercial',$data))       $designacaoComercial = $data['designacaoComercial'];
                if(array_key_exists('nipcEmpresa',$data))               $nipcEmpresa = $data['nipcEmpresa'];
                if(array_key_exists('emailContacto',$data))             $emailContacto = $data['emailContacto'];
                if(array_key_exists('tipofirma',$data))                 $tipofirma = $data['tipofirma'];
                if(array_key_exists('descricao',$data))                 $descricao = $data['descricao'];
                if(array_key_exists('categoria',$data))                 $categoria = $data['categoria'];
                if(array_key_exists('subcategoria',$data))              $subcategoria = $data['subcategoria'];
                if(array_key_exists('concelho',$data))                  $concelho = $data['concelho'];
                if(array_key_exists('freguesia',$data))                 $freguesia = $data['freguesia'];
                if(array_key_exists('morada',$data))                    $morada = $data['morada'];
                if(array_key_exists('coordenadas',$data))               $coordenadas = $data['coordenadas'];
                if(array_key_exists('telefone',$data))                  $telefone = $data['telefone'];
                if(array_key_exists('website',$data))                   $website = $data['website'];
                if(array_key_exists('emailComercial',$data))            $emailComercial = $data['emailComercial'];
                if(array_key_exists('emailAdministrativo',$data))       $emailAdministrativo = $data['emailAdministrativo'];
                if(array_key_exists('horaAbertura',$data))              $horaAbertura = $data['horaAbertura'];
                if(array_key_exists('horaEncerramento',$data))          $horaEncerramento = $data['horaEncerramento'];
                if(array_key_exists('abertoAlmoco',$data))              $abertoAlmoco = $data['abertoAlmoco'];
                if(array_key_exists('inicioAlmoco',$data))              $inicioAlmoco = $data['inicioAlmoco'];
                if(array_key_exists('fimAlmoco',$data))                 $fimAlmoco = $data['fimAlmoco'];
                if(array_key_exists('abertoFimSemana',$data))           $abertoFimSemana = $data['abertoFimSemana'];
                if(array_key_exists('horaAberturaSabado',$data))        $horaAberturaSabado = $data['horaAberturaSabado'];
                if(array_key_exists('horaEncerramentoSabado',$data))    $horaEncerramentoSabado = $data['horaEncerramentoSabado'];
                if(array_key_exists('horaAberturaDomingo',$data))       $horaAberturaDomingo = $data['horaAberturaDomingo'];
                if(array_key_exists('horaEncerramentoDomingo',$data))   $horaEncerramentoDomingo = $data['horaEncerramentoDomingo'];
                if(array_key_exists('facebook',$data))                  $facebook = $data['facebook'];
                if(array_key_exists('instagram',$data))                 $instagram = $data['instagram'];
                if(array_key_exists('youtube',$data))                   $youtube = $data['youtube'];
                if(array_key_exists('twitter',$data))                   $twitter = $data['twitter'];
                if(array_key_exists('checkboxHide1',$data))             $checkboxHide1 = $data['checkboxHide1'];
                if(array_key_exists('checkboxHide2',$data))             $checkboxHide2 = $data['checkboxHide2'];
                if(array_key_exists('checkboxHide3',$data))             $checkboxHide3 = $data['checkboxHide3'];
                if(array_key_exists('checkboxHide4',$data))             $checkboxHide4 = $data['checkboxHide4'];
                if(array_key_exists('checkboxHide5',$data))             $checkboxHide5 = $data['checkboxHide5'];
                if(array_key_exists('checkboxHide6',$data))             $checkboxHide6 = $data['checkboxHide6'];
                if(array_key_exists('checkboxHide7',$data))             $checkboxHide7 = $data['checkboxHide7'];
                if(array_key_exists('checkboxHide8',$data))             $checkboxHide8 = $data['checkboxHide8'];
                if(array_key_exists('checkboxHide9',$data))             $checkboxHide9 = $data['checkboxHide9'];
                if(array_key_exists('checkboxHide10',$data))            $checkboxHide10 = $data['checkboxHide10'];
                if(array_key_exists('checkboxHide11',$data))            $checkboxHide11 = $data['checkboxHide11'];
                if(array_key_exists('checkboxHide12',$data))            $checkboxHide12 = $data['checkboxHide12'];
                if(array_key_exists('checkboxoutro',$data))             $checkboxoutro = $data['checkboxoutro'];
                if(array_key_exists('IdiomaInput',$data))               $IdiomaInput = $data['IdiomaInput'];
                if(array_key_exists('premioHide1',$data))               $premioHide1 = $data['premioHide1'];
                if(array_key_exists('premioHide2',$data))               $premioHide2 = $data['premioHide2'];
                if(array_key_exists('informacoes',$data))               $informacoes = $data['informacoes'];
                if(array_key_exists('VideoInput',$data))                $VideoInput = $data['VideoInput'];
                if(array_key_exists('tipo_conta',$data))                $tipo_conta = $data['tipo_conta'];
                if(array_key_exists('estado',$data))                    $estado = $data['estado'];


                for($i=0; $i< count($subcategoria); $i++){
                    if($i==0){
                        $subcats = $subcategoria['0'];
                    } else {
                        $subcats .= ',' . $subcategoria[$i];
                    }
                }

                if($checkboxHide1 == 1){
                    $idiomas = '1';
                }

                if($checkboxHide2 == 2){
                    if(!empty($idiomas)){
                        $idiomas .= ';2';
                    } else {
                        $idiomas = '2';
                    }
                }

                if($checkboxHide3 == 3){
                    if(!empty($idiomas)){
                        $idiomas .= ';3';
                    } else {
                        $idiomas = '3';
                    }
                }

                if($checkboxHide4 == 4){
                    if(!empty($idiomas)){
                        $idiomas .= ';4';
                    } else {
                        $idiomas = '4';
                    }
                }

                if($checkboxHide5 == 5){
                    if(!empty($idiomas)){
                        $idiomas .= ';5';
                    } else {
                        $idiomas = '5';
                    }
                }

                if($checkboxHide6 == 6){
                    if(!empty($idiomas)){
                        $idiomas .= ';6';
                    } else {
                        $idiomas = '6';
                    }
                }

                if($checkboxHide7 == 7){
                    if(!empty($idiomas)){
                        $idiomas .= ';7';
                    } else {
                        $idiomas = '7';
                    }
                }

                if($checkboxHide8 == 8){
                    if(!empty($idiomas)){
                        $idiomas .= ';8';
                    } else {
                        $idiomas = '8';
                    }
                }

                if($checkboxHide9 == 9){
                    if(!empty($idiomas)){
                        $idiomas .= ';9';
                    } else {
                        $idiomas = '9';
                    }
                }

                if($checkboxHide10 == 10){
                    if(!empty($idiomas)){
                        $idiomas .= ';10';
                    } else {
                        $idiomas = '10';
                    }
                }

                if($checkboxHide11 == 11){
                    if(!empty($idiomas)){
                        $idiomas .= ';11';
                    } else {
                        $idiomas = '11';
                    }
                }

                if($checkboxHide12 == 12){
                    if(!empty($idiomas)){
                        $idiomas .= ';12';
                    } else {
                        $idiomas = '12';
                    }
                }

                if($premioHide1 == 1){
                    $cleanAndSafe = '1';
                } else {
                    $cleanAndSafe = '2';
                }

                if($premioHide2 == 1){
                    $produtoMadeira = '1';
                } else {
                    $produtoMadeira = '2';
                }

                foreach($VideoInput as $keyPar => $valParc ){
                    if($valParc['video'] != ''){
                        if(empty($video)){
                            $video = $valParc['video'];
                        } else {
                            $video .= ';;' . $valParc['video'];
                        }

                    }
                }

                foreach($IdiomaInput as $keyPar => $valParc ){
                    if($valParc['idioma'] != ''){
                        if(empty($outroIdioma)){
                            $outroIdioma = $valParc['idioma'];
                        } else {
                            $outroIdioma .= ';;' . $valParc['idioma'];
                        }
                    }
                }



                $splitCoord = explode(",", $coordenadas);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];


                /* BEGIN Save Diretorio Servicos */
                $resSaveDiretorioServicos = self::saveEditedDiretorioServicos4Manager($diretorioservicos_id, $nomeEmpresa, $designacaoComercial, $nipcEmpresa, $emailContacto, $tipofirma, $descricao,
                    $categoria, $subcats, $concelho, $freguesia, $morada, $lat, $long, $telefone, $website, $emailComercial, $emailAdministrativo, $horaAbertura, $horaEncerramento,
                    $abertoAlmoco, $inicioAlmoco, $fimAlmoco, $abertoFimSemana, $horaAberturaSabado, $horaEncerramentoSabado, $horaAberturaDomingo, $horaEncerramentoDomingo, $facebook, $instagram,
                    $youtube, $twitter, $idiomas, $outroIdioma, $cleanAndSafe, $produtoMadeira, $informacoes, $video, $tipo_conta, $estado);
                /* END Save Diretorio Servicos */

                if (empty($resSaveDiretorioServicos)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objCapaFiles                = new VirtualDeskSiteDiretorioServicosFilesHelper();
                $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadCapa');
                $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
                if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar a foto de capa da empresa', 'error');
                }


                $objLogoFiles                   = new VirtualDeskSiteDiretorioServicosLogosFilesHelper();
                $listFileGaleria2Eliminar       = $objLogoFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadLogo');
                $resFileLogoDelete              = $objLogoFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
                if ($resFileLogoDelete==false && !empty($listFileLogo2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o logo da empresa', 'error');
                }


                $objGaleriaFiles                = new VirtualDeskSiteDiretorioServicosGaleriaFilesHelper();
                $listFileGaleria2Eliminar       = $objGaleriaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadGaleria');
                $resFileGaleriaDelete           = $objGaleriaFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
                if ($resFileGaleriaDelete==false && !empty($listFileGaleria2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de galeria da empresa', 'error');
                }

                /* FILES */
                $dirServicosFiles               =  new VirtualDeskSiteDiretorioServicosFilesHelper();
                $dirServicosFiles->tagprocesso  = 'CAPA_POST';
                $dirServicosFiles->idprocesso   = $referencia;
                $resFileSave                    = $dirServicosFiles->saveListFileByPOST('fileuploadCapa');

                $dirServicosLogoFiles               =  new VirtualDeskSiteDiretorioServicosLogosFilesHelper();
                $dirServicosLogoFiles->tagprocesso  = 'LOGO_POST';
                $dirServicosLogoFiles->idprocesso   = $referencia;
                $resLogoSave                        = $dirServicosLogoFiles->saveListFileByPOST('fileuploadLogo');

                $dirServicosGaleriaFiles               =  new VirtualDeskSiteDiretorioServicosGaleriaFilesHelper();
                $dirServicosGaleriaFiles->tagprocesso  = 'GALERIA_POST';
                $dirServicosGaleriaFiles->idprocesso   = $referencia;
                $resGaleriaSave                        = $dirServicosGaleriaFiles->saveListFileByPOST('fileuploadGaleria');


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('DiretorioServicos_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EVENTLOG_CREATED') . 'ref=' . $diretorioservicos_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedDiretorioServicos4Manager($diretorioservicos_id, $nome_PT, $nome_comercial, $nipc, $email_contacto, $tipo_firma, $descricao, $categoria, $subcats,
                                                                   $concelho, $freguesia, $morada, $latitude, $longitude, $telefone, $website, $email_comercial, $email_administrativo, $hora_abertura,
                                                                   $hora_fecho, $aberto_almoco, $inicio_almoco, $fim_almoco, $abertoFimSemana, $abertura_sabado, $fecho_sabado, $abertura_domingo,
                                                                   $fecho_domingo, $facebook, $instagram, $youtube, $twitter, $idiomas, $outros_idiomas, $cleanAndSafe, $produtoMadeira, $informacoes,
                                                                   $videos, $tipo_conta, $estado){
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('diretorioservicos');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            //
            //$informacoes = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($informacoes),true);

            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($nome_PT))          array_push($fields, 'nome_PT="'.$db->escape($nome_PT).'"');
            if(!is_null($nome_comercial))   array_push($fields, 'nome_comercial="'.$db->escape($nome_comercial).'"');
            if(!is_null($nipc))             array_push($fields, 'nipc="'.$db->escape($nipc).'"');
            if(!is_null($email_contacto))   array_push($fields, 'email_contacto="'.$db->escape($email_contacto).'"');
            if(!is_null($tipo_firma))       array_push($fields, 'tipo_firma="'.$db->escape($tipo_firma).'"');
            if(!is_null($descricao)) {
                $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);
                array_push($fields, 'descricao="'.$db->escape($descricao).'"');
            }
            if(!is_null($categoria)){
                array_push($fields, 'categoria="'.$db->escape($categoria).'"');
                if(is_null($subcats)) {
                    $subcats = '';
                    array_push($fields, 'subcategoria="' . $db->escape($subcats) . '"');
                } else {
                    array_push($fields, 'subcategoria="' . $db->escape($subcats) . '"');
                }
            }
            if(!is_null($concelho)){
                array_push($fields, 'concelho="'.$db->escape($concelho).'"');
                if(is_null($freguesia)){
                    array_push($fields, 'freguesia="'.$db->escape($freguesia).'"');
                }
            }
            if(!is_null($freguesia))            array_push($fields, 'freguesia="'.$db->escape($freguesia).'"');
            if(!is_null($morada))               array_push($fields, 'morada="'.$db->escape($morada).'"');
            array_push($fields, 'latitude="'.$db->escape($latitude).'"');
            array_push($fields, 'longitude="'.$db->escape($longitude).'"');
            if(!is_null($telefone))             array_push($fields, 'telefone="'.$db->escape($telefone).'"');
            if(!is_null($website))              array_push($fields, 'website="'.$db->escape($website).'"');
            if(!is_null($email_comercial))      array_push($fields, 'email_comercial="'.$db->escape($email_comercial).'"');
            if(!is_null($email_administrativo)) array_push($fields, 'email_administrativo="'.$db->escape($email_administrativo).'"');
            if(!is_null($hora_abertura)) array_push($fields, 'hora_abertura="'.$db->escape($hora_abertura).'"');
            if(!is_null($hora_fecho)) array_push($fields, 'hora_fecho="'.$db->escape($hora_fecho).'"');
            if(!is_null($aberto_almoco)){
                array_push($fields, 'aberto_almoco="'.$db->escape($aberto_almoco).'"');
                if($aberto_almoco == 1){
                    array_push($fields, 'inicio_almoco="'.$db->escape('').'"');
                    array_push($fields, 'fim_almoco="'.$db->escape('').'"');
                }
            }
            if($aberto_almoco == 2){
                if(!is_null($inicio_almoco)) array_push($fields, 'inicio_almoco="'.$db->escape($inicio_almoco).'"');
                if(!is_null($fim_almoco)) array_push($fields, 'fim_almoco="'.$db->escape($fim_almoco).'"');
            }
            if(!is_null($abertoFimSemana)){
                array_push($fields, 'abertoFimSemana="'.$db->escape($abertoFimSemana).'"');
                if($abertoFimSemana == 1){
                    if(!is_null($abertura_sabado)) array_push($fields, 'abertura_sabado="'.$db->escape($abertura_sabado).'"');
                    if(!is_null($fecho_sabado)) array_push($fields, 'fecho_sabado="'.$db->escape($fecho_sabado).'"');
                    if(!is_null($abertura_domingo)) array_push($fields, 'abertura_domingo="'.$db->escape($abertura_domingo).'"');
                    if(!is_null($fecho_domingo)) array_push($fields, 'fecho_domingo="'.$db->escape($fecho_domingo).'"');
                }
            }

            if(!is_null($abertura_sabado)) array_push($fields, 'abertura_sabado="'.$db->escape($abertura_sabado).'"');
            if(!is_null($fecho_sabado)) array_push($fields, 'fecho_sabado="'.$db->escape($fecho_sabado).'"');
            if(!is_null($abertura_domingo)) array_push($fields, 'abertura_domingo="'.$db->escape($abertura_domingo).'"');
            if(!is_null($fecho_domingo)) array_push($fields, 'fecho_domingo="'.$db->escape($fecho_domingo).'"');

            if($abertoFimSemana == 2){
                array_push($fields, 'abertura_sabado="'.$db->escape('').'"');
                array_push($fields, 'fecho_sabado="'.$db->escape('').'"');
                array_push($fields, 'abertura_domingo="'.$db->escape('').'"');
                array_push($fields, 'fecho_domingo="'.$db->escape('').'"');
            }
            if(!is_null($facebook)) array_push($fields, 'facebook="'.$db->escape($facebook).'"');
            if(!is_null($instagram)) array_push($fields, 'instagram="'.$db->escape($instagram).'"');
            if(!is_null($youtube)) array_push($fields, 'youtube="'.$db->escape($youtube).'"');
            if(!is_null($twitter)) array_push($fields, 'twitter="'.$db->escape($twitter).'"');
            if(!is_null($idiomas)) array_push($fields, 'idiomas="'.$db->escape($idiomas).'"');
            if(!is_null($outros_idiomas)) array_push($fields, 'outros_idiomas="'.$db->escape($outros_idiomas).'"');
            if(!is_null($cleanAndSafe)) array_push($fields, 'cleanAndSafe="'.$db->escape($cleanAndSafe).'"');
            if(!is_null($produtoMadeira)) array_push($fields, 'produtoMadeira="'.$db->escape($produtoMadeira).'"');
            if(!is_null($informacoes)) {
                $informacoes = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($informacoes),true);
                array_push($fields, 'informacoes="'.$db->escape($informacoes).'"');
            }
            if(!is_null($videos)) array_push($fields, 'videos="'.$db->escape($videos).'"');

            $data_alteracao = date("Y-m-d H:i:s");

            array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_MadeiraMagazine_DirServicos_Empresas')
                ->set($fields)
                ->where(' id = '.$db->escape($diretorioservicos_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.diretorioservicos.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.diretorioservicos.data', null);
            $app->setUserState('com_virtualdesk.edit4user.diretorioservicos.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.diretorioservicos.data', null);

        }
    }
?>