<?php

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    class VirtualDeskSiteMMFiltroHelper{

        public static function getOpcoesProcurar(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $Name . ' as name'))
                ->from("#__virtualdesk_MadeiraMagazine_Procurar")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


       public static function excludePrograma($procurar){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as name'))
                ->from("#__virtualdesk_MadeiraMagazine_Procurar")
                ->where($db->quoteName('id') . "!='" . $db->escape($procurar) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getProcurarSelect($procurar){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as name')
                ->from("#__virtualdesk_MadeiraMagazine_Procurar")
                ->where($db->quoteName('id') . "='" . $db->escape($procurar) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getConcelhos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_MadeiraMagazine_Concelhos")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeConcelhos($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_MadeiraMagazine_Concelhos")
                ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getConcelhosSelect($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_MadeiraMagazine_Concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getCategorias(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $Name . ' as name'))
                ->from("#__virtualdesk_MadeiraMagazine_Categoria_FP")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeCategorias($categorias){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as name'))
                ->from("#__virtualdesk_MadeiraMagazine_Categoria_FP")
                ->where($db->quoteName('id') . "!='" . $db->escape($categorias) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriasSelect($categorias){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as name')
                ->from("#__virtualdesk_MadeiraMagazine_Categoria_FP")
                ->where($db->quoteName('id') . "='" . $db->escape($categorias) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }




        public static function getViajo(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ' . $Name . ' as name'))
                ->from("#__virtualdesk_MadeiraMagazine_Meses")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeViajo($viajo){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as name'))
                ->from("#__virtualdesk_MadeiraMagazine_Meses")
                ->where($db->quoteName('id') . "!='" . $db->escape($viajo) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getViajoSelect($viajo){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as name')
                ->from("#__virtualdesk_MadeiraMagazine_Meses")
                ->where($db->quoteName('id') . "='" . $db->escape($viajo) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

    }

?>