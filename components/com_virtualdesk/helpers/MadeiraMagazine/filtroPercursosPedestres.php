<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    class MMFiltroPercursosPedestresHelper{

        public static function getDistancia(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distancia_min, distancia_max'))
                ->from("#__virtualdesk_PercursosPedestres_Distancia")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeDistancia($distancia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, distancia_min, distancia_max'))
                ->from("#__virtualdesk_PercursosPedestres_Distancia")
                ->where($db->quoteName('id') . "!='" . $db->escape($distancia) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDistanciaSelect($distancia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('distancia_min, distancia_max'))
                ->from("#__virtualdesk_PercursosPedestres_Distancia")
                ->where($db->quoteName('id') . "='" . $db->escape($distancia) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDuracao(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, duracao_min, duracao_max'))
                ->from("#__virtualdesk_PercursosPedestres_Duracao")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeDuracao($duracao){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, duracao_min, duracao_max'))
                ->from("#__virtualdesk_PercursosPedestres_Duracao")
                ->where($db->quoteName('id') . "!='" . $db->escape($duracao) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDuracaoSelect($duracao){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('duracao_min, duracao_max'))
                ->from("#__virtualdesk_PercursosPedestres_Duracao")
                ->where($db->quoteName('id') . "='" . $db->escape($duracao) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getSentido(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as sentido'))
                ->from("#__virtualdesk_PercursosPedestres_Sentido")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeSentido($sentido){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as sentido'))
                ->from("#__virtualdesk_PercursosPedestres_Sentido")
                ->where($db->quoteName('id') . "!='" . $db->escape($sentido) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getSentidoSelect($sentido){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as sentido')
                ->from("#__virtualdesk_PercursosPedestres_Sentido")
                ->where($db->quoteName('id') . "='" . $db->escape($sentido) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getTerreno(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as terreno'))
                ->from("#__virtualdesk_PercursosPedestres_Terreno")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeTerreno($terreno){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as terreno'))
                ->from("#__virtualdesk_PercursosPedestres_Terreno")
                ->where($db->quoteName('id') . "!='" . $db->escape($terreno) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTerrenoSelect($terreno){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as terreno')
                ->from("#__virtualdesk_PercursosPedestres_Terreno")
                ->where($db->quoteName('id') . "='" . $db->escape($terreno) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getTipologia(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as tipologia'))
                ->from("#__virtualdesk_PercursosPedestres_Tipologia")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeTipologia($tipologia){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as tipologia'))
                ->from("#__virtualdesk_PercursosPedestres_Tipologia")
                ->where($db->quoteName('id') . "!='" . $db->escape($tipologia) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTipologiaSelect($tipologia){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as tipologia')
                ->from("#__virtualdesk_PercursosPedestres_Tipologia")
                ->where($db->quoteName('id') . "='" . $db->escape($tipologia) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getVertigens(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as vertigens'))
                ->from("#__virtualdesk_PercursosPedestres_Vertigens")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeVertigens($vertigens){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as vertigens'))
                ->from("#__virtualdesk_PercursosPedestres_Vertigens")
                ->where($db->quoteName('id') . "!='" . $db->escape($vertigens) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getVertigensSelect($vertigens){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as vertigens')
                ->from("#__virtualdesk_PercursosPedestres_Vertigens")
                ->where($db->quoteName('id') . "='" . $db->escape($vertigens) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getZonamento(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as zonamento'))
                ->from("#__virtualdesk_PercursosPedestres_Zonamento")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeZonamento($zonamento){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as zonamento'))
                ->from("#__virtualdesk_PercursosPedestres_Zonamento")
                ->where($db->quoteName('id') . "!='" . $db->escape($zonamento) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getZonamentoSelect($zonamento){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as zonamento')
                ->from("#__virtualdesk_PercursosPedestres_Zonamento")
                ->where($db->quoteName('id') . "='" . $db->escape($zonamento) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getPaisagem(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as paisagem'))
                ->from("#__virtualdesk_PercursosPedestres_Paisagem")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludePaisagem($paisagem){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id,' . $Name . ' as paisagem'))
                ->from("#__virtualdesk_PercursosPedestres_Paisagem")
                ->where($db->quoteName('id') . "!='" . $db->escape($paisagem) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getPaisagemSelect($paisagem){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($Name . ' as paisagem')
                ->from("#__virtualdesk_PercursosPedestres_Paisagem")
                ->where($db->quoteName('id') . "='" . $db->escape($paisagem) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getConcelho($distrito){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distrito) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeConcelho($distrito, $concelho){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distrito) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getConcelhoSelect($distrito, $concelho){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id_distrito') . "='" . $db->escape($distrito) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getPercursosAll(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.percurso, b.concelho as concelho, a.percurso_laurissilva, a.costa_laurissilva, a.costa_sol'))
                ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho')
                ->from("#__virtualdesk_PercursosPedestres as a")
                ->where($db->quoteName('mmg') . "='" . $db->escape('1') . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('percurso ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getPercursosFiltered($topic, $costSolValue, $costLaurissilvaValue, $percLaurissilvaValue, $percurso, $tipologia, $terreno, $sentido, $duracao, $distancia, $vertigens, $zonamento, $paisagem, $concelho){

            $db = JFactory::getDBO();

            $where = $db->quoteName('mmg') . "='" . $db->escape('1') . "'";
            $where .= 'AND' . $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            if(!empty($percurso)){
                $nomePercursos = '%' . $percurso . '%';
                $where .= 'AND' . $db->quoteName('a.percurso') . "LIKE '" . $db->escape($nomePercursos) . "'";
            }

            if(!empty($percLaurissilvaValue) && $percLaurissilvaValue == 1){
                $where .= 'AND' . $db->quoteName('a.percurso_laurissilva') . "='" . $db->escape($percLaurissilvaValue) . "'";
            }

            if(!empty($costLaurissilvaValue) && $costLaurissilvaValue == 1){
                $where .= 'AND' . $db->quoteName('a.costa_laurissilva') . "='" . $db->escape($costLaurissilvaValue) . "'";
            }

            if(!empty($costSolValue) && $costSolValue == 1){
                $where .= 'AND' . $db->quoteName('a.costa_sol') . "='" . $db->escape($costSolValue) . "'";
            }

            if(!empty($duracao)){
                $where .= 'AND' . $db->quoteName('a.idDuracao') . "='" . $db->escape($duracao) . "'";
            }

            if(!empty($distancia)){
                $where .= 'AND' . $db->quoteName('a.idDistancia') . "='" . $db->escape($distancia) . "'";
            }

            if(!empty($sentido)){
                $where .= 'AND' . $db->quoteName('a.sentido') . "='" . $db->escape($sentido) . "'";
            }

            if(!empty($terreno)){
                $where .= 'AND' . $db->quoteName('a.terreno') . "='" . $db->escape($terreno) . "'";
            }

            if(!empty($tipologia)){
                $where .= 'AND' . $db->quoteName('a.tipologia') . "='" . $db->escape($tipologia) . "'";
            }

            if(!empty($vertigens)){
                $where .= 'AND' . $db->quoteName('a.vertigens') . "='" . $db->escape($vertigens) . "'";
            }

            if(!empty($zonamento)){
                $where .= 'AND' . $db->quoteName('a.zonamento') . "='" . $db->escape($zonamento) . "'";
            }

            if(!empty($paisagem)){
                $where .= 'AND' . $db->quoteName('a.paisagem') . "='" . $db->escape($paisagem) . "'";
            }

            if(!empty($concelho)){
                $where .= 'AND' . $db->quoteName('a.concelho') . "='" . $db->escape($concelho) . "'";
            }

            if($topic == 1){
                $nomeTopic = '0%';
                $nomeTopic2 = '1%';
                $nomeTopic3 = '2%';
                $nomeTopic4 = '3%';
                $nomeTopic5 = '4%';
                $nomeTopic6 = '5%';
                $nomeTopic7 = '6%';
                $nomeTopic8 = '7%';
                $nomeTopic9 = '8%';
                $nomeTopic10 = '9%';
                $where2 = $db->quoteName('a.percurso') . "LIKE' " . $db->escape($nomeTopic) . "'";
                $where3 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic2) . "'";
                $where4 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic3) . "'";
                $where5 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic4) . "'";
                $where6 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic5) . "'";
                $where7 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic6) . "'";
                $where8 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic7) . "'";
                $where9 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic8) . "'";
                $where10 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic9) . "'";
                $where11 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic10) . "'";

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.percurso, b.concelho as concelho, a.percurso_laurissilva, a.costa_laurissilva, a.costa_sol'))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where($where . 'AND' . $where2)
                    ->orWhere($where . 'AND' . $where3)
                    ->orWhere($where . 'AND' . $where4)
                    ->orWhere($where . 'AND' . $where5)
                    ->orWhere($where . 'AND' . $where6)
                    ->orWhere($where . 'AND' . $where7)
                    ->orWhere($where . 'AND' . $where8)
                    ->orWhere($where . 'AND' . $where9)
                    ->orWhere($where . 'AND' . $where10)
                    ->orWhere($where . 'AND' . $where11)
                    ->order('percurso ASC')
                );

            } else if($topic == 2){
                $nomeTopic = 'A%';
                $nomeTopic2 = 'B%';
                $nomeTopic3 = 'C%';
                $nomeTopic4 = 'D%';
                $nomeTopic5 = 'E%';
                $nomeTopic6 = 'F%';
                $where2 = $db->quoteName('a.percurso') . "LIKE' " . $db->escape($nomeTopic) . "'";
                $where3 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic2) . "'";
                $where4 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic3) . "'";
                $where5 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic4) . "'";
                $where6 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic5) . "'";
                $where7 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic6) . "'";

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.percurso, b.concelho as concelho, a.percurso_laurissilva, a.costa_laurissilva, a.costa_sol'))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where($where . 'AND' . $where2)
                    ->orWhere($where . 'AND' . $where3)
                    ->orWhere($where . 'AND' . $where4)
                    ->orWhere($where . 'AND' . $where5)
                    ->orWhere($where . 'AND' . $where6)
                    ->orWhere($where . 'AND' . $where7)
                    ->order('percurso ASC')
                );
            } else if($topic == 3){
                $nomeTopic = 'G%';
                $nomeTopic2 = 'H%';
                $nomeTopic3 = 'I%';
                $nomeTopic4 = 'J%';
                $nomeTopic5 = 'K%';
                $nomeTopic6 = 'L%';
                $where2 = $db->quoteName('a.percurso') . "LIKE' " . $db->escape($nomeTopic) . "'";
                $where3 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic2) . "'";
                $where4 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic3) . "'";
                $where5 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic4) . "'";
                $where6 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic5) . "'";
                $where7 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic6) . "'";

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.percurso, b.concelho as concelho, a.percurso_laurissilva, a.costa_laurissilva, a.costa_sol'))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where($where . 'AND' . $where2)
                    ->orWhere($where . 'AND' . $where3)
                    ->orWhere($where . 'AND' . $where4)
                    ->orWhere($where . 'AND' . $where5)
                    ->orWhere($where . 'AND' . $where6)
                    ->orWhere($where . 'AND' . $where7)
                    ->order('percurso ASC')
                );
            } else if($topic == 4){
                $nomeTopic = 'M%';
                $nomeTopic2 = 'N%';
                $nomeTopic3 = 'O%';
                $nomeTopic4 = 'P%';
                $nomeTopic5 = 'Q%';
                $nomeTopic6 = 'R%';
                $where2 = $db->quoteName('a.percurso') . "LIKE' " . $db->escape($nomeTopic) . "'";
                $where3 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic2) . "'";
                $where4 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic3) . "'";
                $where5 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic4) . "'";
                $where6 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic5) . "'";
                $where7 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic6) . "'";

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.percurso, b.concelho as concelho, a.percurso_laurissilva, a.costa_laurissilva, a.costa_sol'))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where($where . 'AND' . $where2)
                    ->orWhere($where . 'AND' . $where3)
                    ->orWhere($where . 'AND' . $where4)
                    ->orWhere($where . 'AND' . $where5)
                    ->orWhere($where . 'AND' . $where6)
                    ->orWhere($where . 'AND' . $where7)
                    ->order('percurso ASC')
                );
            } else if($topic == 5){
                $nomeTopic = 'S%';
                $nomeTopic2 = 'T%';
                $nomeTopic3 = 'U%';
                $nomeTopic4 = 'V%';
                $nomeTopic5 = 'X%';
                $nomeTopic6 = 'Z%';
                $where2 = $db->quoteName('a.percurso') . "LIKE' " . $db->escape($nomeTopic) . "'";
                $where3 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic2) . "'";
                $where4 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic3) . "'";
                $where5 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic4) . "'";
                $where6 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic5) . "'";
                $where7 = $db->quoteName('a.percurso') . "LIKE'" . $db->escape($nomeTopic6) . "'";

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.percurso, b.concelho as concelho, a.percurso_laurissilva, a.costa_laurissilva, a.costa_sol'))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where($where . 'AND' . $where2)
                    ->orWhere($where . 'AND' . $where3)
                    ->orWhere($where . 'AND' . $where4)
                    ->orWhere($where . 'AND' . $where5)
                    ->orWhere($where . 'AND' . $where6)
                    ->orWhere($where . 'AND' . $where7)
                    ->order('percurso ASC')
                );
            } else {
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.percurso, b.concelho as concelho, a.percurso_laurissilva, a.costa_laurissilva, a.costa_sol'))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where($where)
                    ->order('percurso ASC')
                );
            }

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDetalhePercurso($ref){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $Name = 'name_PT';
            } else if($lang='en_EN') {
                $Name = 'name_EN';
            } else if($lang='fr_FR') {
                $Name = 'name_FR';
            } else if($lang='de_DE') {
                $Name = 'name_DE';
            } else {
                $Name = 'name_PT';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.percurso, a.descricao, a.creditosFotoCapa, a.linkCreditos, b.freguesia as inicio_freguesia, c.freguesia as fim_freguesia, a.distancia, a.duracao, d.' . $Name . ' as terreno, e.' . $Name . ' as sentido, f.' . $Name . ' as vertigens, a.altitude_max, a.altitude_min, g.name as dificuldade'))
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS b ON b.id = a.inicio_freguesia')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.fim_freguesia')
                ->join('LEFT', '#__virtualdesk_PercursosPedestres_Terreno AS d ON d.id = a.terreno')
                ->join('LEFT', '#__virtualdesk_PercursosPedestres_Sentido AS e ON e.id = a.sentido')
                ->join('LEFT', '#__virtualdesk_PercursosPedestres_Vertigens AS f ON f.id = a.vertigens')
                ->join('LEFT', '#__virtualdesk_PercursosPedestres_Dificuldade AS g ON g.id = a.dificuldade')
                ->from("#__virtualdesk_PercursosPedestres as a")
                ->where($db->quoteName('a.referencia') . "='" . $db->escape($ref) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getDetalheParte2Percurso($ref, $fileLangSufix){
            if( empty($fileLangSufix) or ($fileLangSufix='pt_PT') ) {
                $destaque = 'destaques';
            } else if($fileLangSufix='en_EN') {
                $destaque = 'destaquesEN';
            } else if($fileLangSufix='fr_FR') {
                $destaque = 'destaquesFR';
            } else if($fileLangSufix='de_DE') {
                $destaque = 'destaquesDE';
            } else {
                $destaque = 'destaques';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.' . $destaque . ' as destaques, b.freguesia as inicio_freguesia, c.freguesia as fim_freguesia, a.inicio_sitio, a.fim_sitio, a.hastags'))
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS b ON b.id = a.inicio_freguesia')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.fim_freguesia')
                ->from("#__virtualdesk_PercursosPedestres as a")
                ->where($db->quoteName('a.referencia') . "='" . $db->escape($ref) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);

        }


        public static function getDescricaoPercurso($ref, $fileLangSufix){

            if( empty($fileLangSufix) or ($fileLangSufix='pt_PT') ) {
                $Name = 'descricao';
            } else if($fileLangSufix='en_EN') {
                $Name = 'descricaoEN';
            } else if($fileLangSufix='fr_FR') {
                $Name = 'descricaoFR';
            } else if($fileLangSufix='de_DE') {
                $Name = 'descricaoDE';
            } else {
                $Name = 'descricao';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.' . $Name . ' as descricao')
                ->from("#__virtualdesk_PercursosPedestres as a")
                ->where($db->quoteName('a.referencia') . "='" . $db->escape($ref) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getIDPatrocinador($ref){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('patrocinador')
                ->from("#__virtualdesk_PercursosPedestres as a")
                ->where($db->quoteName('a.referencia') . "='" . $db->escape($ref) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getPatrocinadorPercurso($id){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('referencia, nome, url'))
                ->from("#__virtualdesk_Patrocinadores as a")
                ->where($db->quoteName('a.id') . "='" . $db->escape($id) . "'")
                ->where($db->quoteName('a.estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getLogoPatrocinador($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_Patrocinadores_Logo")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getImgCapa($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_PercursosPedestres_FotoCapa")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getGPX($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_PercursosPedestres_GPX")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getKML($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_PercursosPedestres_KML")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 3);
        }

    }

?>