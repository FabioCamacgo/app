<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSitePercursosPedestresCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/fotoCapa_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/galeria_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGPXFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/gpx_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresKMLFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/kml_files.php');
    JLoader::register('VirtualDeskTablePercursosPedestres', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres.php');
    JLoader::register('VirtualDeskTablePercursosPedestresEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestresEstadoHistorico.php');
    JLoader::register('VirtualDeskTableTipologia', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_tipologia.php');
    JLoader::register('VirtualDeskTableTerreno', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_terreno.php');
    JLoader::register('VirtualDeskTableVertigens', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_vertigens.php');
    JLoader::register('VirtualDeskTableZonamento', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_zonamento.php');
    JLoader::register('VirtualDeskTableSentido', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_sentido.php');
    JLoader::register('VirtualDeskTablePaisagem', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_paisagem.php');
    JLoader::register('VirtualDeskTableDificuldade', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_dificuldade.php');
    JLoader::register('VirtualDeskTableDuracao', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_duracao.php');
    JLoader::register('VirtualDeskTableDistancia', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_distancia.php');
    JLoader::register('VirtualDeskTableAltitude', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/percursospedestres_altitude.php');

    class VirtualDeskSitePercursosPedestresHelper
    {
        const tagchaveModulo = 'percursospedestres';

        public static function getImgGaleria($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_PercursosPedestres_FotoGaleria")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getTipologia(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as tipologia'))
                ->from("#__virtualdesk_PercursosPedestres_Tipologia as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getTipologiaSelect($tipologia){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $TipoName  . ' as tipologia')
                ->from("#__virtualdesk_PercursosPedestres_Tipologia as a")
                ->where($db->quoteName('id') . "='" . $db->escape($tipologia) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeTipologia($tipologia){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as tipologia'))
                ->from("#__virtualdesk_PercursosPedestres_Tipologia as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($tipologia) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPaisagem(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as paisagem'))
                ->from("#__virtualdesk_PercursosPedestres_Paisagem as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPaisagemSelect($tipoPaisagem){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $TipoName  . ' as paisagem')
                ->from("#__virtualdesk_PercursosPedestres_Paisagem as a")
                ->where($db->quoteName('id') . "='" . $db->escape($tipoPaisagem) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludePaisagem($tipoPaisagem){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as paisagem'))
                ->from("#__virtualdesk_PercursosPedestres_Paisagem as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($tipoPaisagem) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getSentido(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as sentido'))
                ->from("#__virtualdesk_PercursosPedestres_Sentido as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getSentidoSelect($sentido){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $TipoName  . ' as sentido')
                ->from("#__virtualdesk_PercursosPedestres_Sentido as a")
                ->where($db->quoteName('id') . "='" . $db->escape($sentido) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeSentido($sentido){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as sentido'))
                ->from("#__virtualdesk_PercursosPedestres_Sentido as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($sentido) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getTerreno(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as terreno'))
                ->from("#__virtualdesk_PercursosPedestres_Terreno as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getTerrenoSelect($terreno){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $TipoName  . ' as terreno')
                ->from("#__virtualdesk_PercursosPedestres_Terreno as a")
                ->where($db->quoteName('id') . "='" . $db->escape($terreno) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeTerreno($terreno){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as terreno'))
                ->from("#__virtualdesk_PercursosPedestres_Terreno as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($terreno) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getVertigens(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as vertigens'))
                ->from("#__virtualdesk_PercursosPedestres_Vertigens as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getVertigensSelect($terreno){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $TipoName  . ' as vertigens')
                ->from("#__virtualdesk_PercursosPedestres_Vertigens as a")
                ->where($db->quoteName('id') . "='" . $db->escape($terreno) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeVertigens($terreno){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as vertigens'))
                ->from("#__virtualdesk_PercursosPedestres_Vertigens as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($terreno) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDificuldade(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.name as dificuldade'))
                ->from("#__virtualdesk_PercursosPedestres_Dificuldade as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDificuldadeSelect($dificuldade){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.name as dificuldade')
                ->from("#__virtualdesk_PercursosPedestres_Dificuldade as a")
                ->where($db->quoteName('id') . "='" . $db->escape($dificuldade) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeDificuldade($dificuldade){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.name as dificuldade'))
                ->from("#__virtualdesk_PercursosPedestres_Dificuldade as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($dificuldade) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getZonamento(){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as zonamento'))
                ->from("#__virtualdesk_PercursosPedestres_Zonamento as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getZonamentoSelect($zonamento){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a. ' . $TipoName  . ' as zonamento')
                ->from("#__virtualdesk_PercursosPedestres_Zonamento as a")
                ->where($db->quoteName('id') . "='" . $db->escape($zonamento) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeZonamento($zonamento){
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $TipoName = 'name_PT';
            } else if ($lang='en_EN'){
                $TipoName = 'name_EN';
            } else if ($lang='fr_FR'){
                $TipoName = 'name_FR';
            } else if ($lang='de_DE'){
                $TipoName = 'name_DE';
            } else {
                $TipoName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a. ' . $TipoName  . ' as zonamento'))
                ->from("#__virtualdesk_PercursosPedestres_Zonamento as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($zonamento) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistrito(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.distrito'))
                ->from("#__virtualdesk_digitalGov_distritos as a")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistritoSelect($distrito){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.distrito')
                ->from("#__virtualdesk_digitalGov_distritos as a")
                ->where($db->quoteName('id') . "='" . $db->escape($distrito) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeDistrito($distrito){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.distrito'))
                ->from("#__virtualdesk_digitalGov_distritos as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($distrito) . "'")
                ->order('distrito ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelho($idwebsitelist,$onAjaxVD=0){

            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho as name, id_distrito'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, concelho as name, id_distrito'))
                    ->from("#__virtualdesk_digitalGov_concelhos")
                    ->where($db->quoteName('id_distrito') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getConcelhoSelect($concelho){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeConcelho($distrito, $concelho){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho as name, id_distrito'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id_distrito') . '=' . $db->escape($distrito))
                ->where($db->quoteName('id') . '!=' . $db->escape($concelho))
                ->order('name')
            );
            $data = $db->loadAssocList();

            return ($data);
        }

        public static function getFreguesia($idwebsitelist,$onAjaxVD=0){

            if (empty($idwebsitelist)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getFreguesiaEdited(){

            $db = JFactory::getDBO();

            $where = $db->quoteName('concelho_id') . "='" . $db->escape('57') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('59') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('105') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('133') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('203') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('212') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('213') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('222') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('230') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('235') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('244') . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.freguesia'))
                ->from("#__virtualdesk_digitalGov_freguesias as a")
                ->where($where)
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getFreguesiaSelect($freguesia){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias as a")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeFreguesia($concelho, $freguesia){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia as name, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($concelho))
                ->where($db->quoteName('id') . '!=' . $db->escape($freguesia))
                ->order('name')
            );
            $data = $db->loadAssocList();

            return ($data);
        }

        public static function excludeFreguesiaEdited($freguesia){

            $db = JFactory::getDBO();

            $where = $db->quoteName('concelho_id') . "='" . $db->escape('57') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('59') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('105') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('133') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('203') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('212') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('213') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('222') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('230') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('235') . "'";
            $where .= 'OR' . $db->quoteName('concelho_id') . "='" . $db->escape('244') . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.freguesia'))
                ->from("#__virtualdesk_digitalGov_freguesias as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($freguesia) . "'")
                ->where($where)
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPatrocinador(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.nome as patrocinador'))
                ->from("#__virtualdesk_Patrocinadores as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPatrocinadorSelect($patrocinador){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.nome as patrocinador')
                ->from("#__virtualdesk_Patrocinadores as a")
                ->where($db->quoteName('id') . "='" . $db->escape($patrocinador) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludePatrocinador($patrocinador){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.nome as patrocinador'))
                ->from("#__virtualdesk_Patrocinadores as a")
                ->where($db->quoteName('id') . "!='" . $db->escape($patrocinador) . "'")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_PercursosPedestres_FotoCapa as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
        }

        public static function CheckReferencia($cod){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_PercursosPedestres")
                ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEstado(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_estado, a.estado'))
                ->from("#__virtualdesk_PercursosPedestres_Estado as a")
                ->order('id_estado ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getEstadoSelect($estado){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.estado')
                ->from("#__virtualdesk_PercursosPedestres_Estado as a")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($estado) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeEstado($estado){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id_estado, a.estado'))
                ->from("#__virtualdesk_PercursosPedestres_Estado as a")
                ->where($db->quoteName('id_estado') . "!='" . $db->escape($estado) . "'")
                ->order('id_estado ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getDistanciaMinima(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.distancia_min, a.distancia_max'))
                ->from("#__virtualdesk_PercursosPedestres_Distancia as a")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getIdDistancia($distanciaMinima){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.id')
                ->from("#__virtualdesk_PercursosPedestres_Distancia as a")
                ->where($db->quoteName('distancia_min') . "='" . $db->escape($distanciaMinima) . "'")
                ->order('id ASC')
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getDuracaoMinima(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.duracao_min, a.duracao_max'))
                ->from("#__virtualdesk_PercursosPedestres_Duracao as a")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getIdDuracao($duracaoMinima){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.id')
                ->from("#__virtualdesk_PercursosPedestres_Duracao as a")
                ->where($db->quoteName('duracao_min') . "='" . $db->escape($duracaoMinima) . "'")
                ->order('id ASC')
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function saveNewPercurso($patrocinador, $hastags, $destaques, $destaquesEN, $destaquesFR, $destaquesDE, $nomecredito, $linkcredito, $descricao, $descricaoEN, $descricaoFR, $descricaoDE, $localidades, $referencia, $nomepercurso, $tipologia, $tipoPaisagem, $percLaurissilvaValue, $costLaurissilvaValue, $costSolValue, $mmgValue, $sentido, $terreno, $vertigens, $dificuldade, $distancia, $idDistancia, $duracao, $idDuracao, $altitudemax, $altitudemin, $subidaAcumulado, $descidaAcumulado, $distrito, $concelho, $freguesia, $zonamento, $freguesiaInicio, $freguesiaFim, $sitioInicio, $sitioFim, $balcao=0){
            if($percLaurissilvaValue == 0){
                $percLaurissilva = '2';
            } else {
                $percLaurissilva = $percLaurissilvaValue;
            }

            if($costLaurissilvaValue == 0){
                $costLaurissilva = '2';
            } else {
                $costLaurissilva = $costLaurissilvaValue;
            }

            if($costSolValue == 0){
                $costSol = '2';
            } else {
                $costSol = $costSolValue;
            }

            if($mmgValue == 0){
                $mmg = '2';
            } else {
                $mmg = $mmgValue;
            }

            $db    = JFactory::getDbo();

            $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);
            $descricaoEN = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricaoEN),true);
            $descricaoFR = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricaoFR),true);
            $descricaoDE = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricaoDE),true);
            $localidades = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($localidades),true);

            $query = $db->getQuery(true);
            $columns = array('referencia','percurso','descricao','descricaoEN','descricaoFR','descricaoDE','tipologia','paisagem','percurso_laurissilva','costa_laurissilva','costa_sol','mmg', 'sentido', 'terreno', 'vertigens', 'distancia','idDistancia','duracao','idDuracao','altitude_max','altitude_min','subida_acumulado','descida_acumulado','dificuldade','distrito','concelho','freguesia', 'inicio_freguesia', 'fim_freguesia', 'zonamento','inicio_sitio','fim_sitio','localidades','estado','creditosFotoCapa','linkCreditos','destaques','destaquesEN','destaquesFR','destaquesDE','hastags','patrocinador');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nomepercurso)), $db->quote($db->escape($descricao)), $db->quote($db->escape($descricaoEN)), $db->quote($db->escape($descricaoFR)), $db->quote($db->escape($descricaoDE)), $db->quote($db->escape($tipologia)), $db->quote($db->escape($tipoPaisagem)), $db->quote($db->escape($percLaurissilva)), $db->quote($db->escape($costLaurissilva)), $db->quote($db->escape($costSol)), $db->quote($db->escape($mmg)), $db->quote($db->escape($sentido)), $db->quote($db->escape($terreno)), $db->quote($db->escape($vertigens)), $db->quote($db->escape($distancia)), $db->quote($db->escape($idDistancia)), $db->quote($db->escape($duracao)), $db->quote($db->escape($idDuracao)), $db->quote($db->escape($altitudemax)), $db->quote($db->escape($altitudemin)), $db->quote($db->escape($subidaAcumulado)), $db->quote($db->escape($descidaAcumulado)), $db->quote($db->escape($dificuldade)), $db->quote($db->escape($distrito)), $db->quote($db->escape($concelho)), $db->quote($db->escape($freguesia)), $db->quote($db->escape($freguesiaInicio)), $db->quote($db->escape($freguesiaFim)), $db->quote($db->escape($zonamento)), $db->quote($db->escape($sitioInicio)), $db->quote($db->escape($sitioFim)), $db->quote($db->escape($localidades)), $db->quote($db->escape('2')), $db->quote($db->escape($nomecredito)), $db->quote($db->escape($linkcredito)), $db->quote($db->escape($destaques)), $db->quote($db->escape($destaquesEN)), $db->quote($db->escape($destaquesFR)), $db->quote($db->escape($destaquesDE)), $db->quote($db->escape($hastags)), $db->quote($db->escape($patrocinador)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        /* Cria novo Evento para o ecrã/permissões do MANAGER */
        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $nomepercurso = $data['nomepercurso'];
                $tipologia = $data['tipologia'];
                $tipoPaisagem = $data['tipoPaisagem'];
                $percLaurissilvaValue = $data['percLaurissilvaValue'];
                $costLaurissilvaValue = $data['costLaurissilvaValue'];
                $costSolValue = $data['costSolValue'];
                $mmgValue = $data['mmgValue'];
                $sentido = $data['sentido'];
                $terreno = $data['terreno'];
                $vertigens = $data['vertigens'];
                $dificuldade = $data['dificuldade'];
                $distancia = $data['distancia'];
                $horas = $data['horas'];
                $minutos = $data['minutos'];
                $altitudemax = $data['altitudemax'];
                $altitudemin = $data['altitudemin'];
                $subidaAcumulado = $data['subidaAcumulado'];
                $descidaAcumulado = $data['descidaAcumulado'];
                $distrito = $data['distrito'];
                $concelho = $data['concelho'];
                $freguesia = $data['freguesia'];
                $zonamento = $data['zonamento'];
                $freguesiaInicio = $data['freguesiaInicio'];
                $freguesiaFim = $data['freguesiaFim'];
                $sitioInicio = $data['sitioInicio'];
                $sitioFim = $data['sitioFim'];
                $descricao = $data['descricao'];
                $descricaoEN = $data['descricaoEN'];
                $descricaoFR = $data['descricaoFR'];
                $descricaoDE = $data['descricaoDE'];
                $localidades = $data['localidades'];
                $linkcredito = $data['linkcredito'];
                $nomecredito = $data['nomecredito'];
                $DestaqueInput = $data['DestaqueInput'];
                $HastagsInput = $data['HastagsInput'];
                $patrocinador = $data['patrocinador'];
                $dataAtual = date("Y-m-d");
                $horaAtual = date('H:i');
                if(empty($minutos) || $minutos == 0){
                    $duracao = $horas;
                } else {
                    $duracao = $horas . ',' . $minutos;
                }

                $arrayDistancia = explode(".", $distancia);
                if(!empty($arrayDistancia[1])){
                    $distanciaEditado = $arrayDistancia[0] . ',' . $arrayDistancia[1];
                } else {
                    $distanciaEditado = $arrayDistancia[0];
                }

                $distMin = self::getDistanciaMinima();
                foreach($distMin as $rowWSL) :
                    $distanciaMinima = $rowWSL['distancia_min'];
                    $distanciaMaxima = $rowWSL['distancia_max'];
                    if($arrayDistancia[0] < $distanciaMaxima) {
                        if($arrayDistancia[0] >= $distanciaMinima){
                            $idDistancia = self::getIdDistancia($distanciaMinima);
                            break;
                        }
                    }
                endforeach;

                $arrayDuracao = explode(",", $duracao);

                $distMin = self::getDuracaoMinima();
                foreach($distMin as $rowWSL) :
                    $duracaoMinima = $rowWSL['duracao_min'];
                    $duracaoMaxima = $rowWSL['duracao_max'];
                    if($arrayDuracao[0] < $duracaoMaxima) {
                        if($arrayDuracao[0] >= $duracaoMinima){
                            $idDuracao = self::getIdDuracao($duracaoMinima);
                            break;
                        }
                    }
                endforeach;

                foreach($DestaqueInput as $keyPar => $valParc ){
                    if(empty($destaques)){
                        $destaques = $valParc['destaque'];
                    } else {
                        $destaques .= ';;' . $valParc['destaque'];
                    }

                    if(empty($destaquesEN)){
                        $destaquesEN = $valParc['destaqueEN'];
                    } else {
                        $destaquesEN .= ';;' . $valParc['destaqueEN'];
                    }

                    if(empty($destaquesFR)){
                        $destaquesFR = $valParc['destaqueFR'];
                    } else {
                        $destaquesFR .= ';;' . $valParc['destaqueFR'];
                    }

                    if(empty($destaquesDE)){
                        $destaquesDE = $valParc['destaqueDE'];
                    } else {
                        $destaquesDE .= ';;' . $valParc['destaqueDE'];
                    }
                }

                foreach($HastagsInput as $keyPar => $valParc ){
                    if($valParc['hastags'] != ''){
                        if(empty($hastags)){
                            $hastags = $valParc['hastags'];
                        } else {
                            $hastags .= ';;' . $valParc['hastags'];
                        }

                    }
                }

                /*Gerar Referencia UNICA de alerta*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $refRandom       = self::random_code();
                    $referencia = 'PP' . $refRandom . $distrito . $concelho . $freguesia;
                    $checkREF = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA de alerta*/

                $resSavePercurso = self::saveNewPercurso($patrocinador, $hastags, $destaques, $destaquesEN, $destaquesFR, $destaquesDE, $nomecredito, $linkcredito, $descricao, $descricaoEN, $descricaoFR, $descricaoDE, $localidades, $referencia, $nomepercurso, $tipologia, $tipoPaisagem, $percLaurissilvaValue, $costLaurissilvaValue, $costSolValue, $mmgValue, $sentido, $terreno, $vertigens, $dificuldade, $distanciaEditado, $idDistancia, $duracao, $idDuracao, $altitudemax, $altitudemin, $subidaAcumulado, $descidaAcumulado, $distrito, $concelho, $freguesia, $zonamento, $freguesiaInicio, $freguesiaFim, $sitioInicio, $sitioFim);

                if (empty($resSavePercurso)) {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
                $objCapaFiles->tagprocesso   = 'PERCURSOSPEDESTRES_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

                $objGaleriaFiles              = new VirtualDeskSitePercursosPedestresGaleriaFilesHelper();
                $objGaleriaFiles->tagprocesso = 'PERCURSOSPEDESTRES_POST';
                $objGaleriaFiles->idprocesso  = $referencia;
                $resFileSaveGaleria           = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

                $objGPXFiles                = new VirtualDeskSitePercursosPedestresGPXFilesHelper();
                $objGPXFiles->tagprocesso   = 'PERCURSOSPEDESTRES_POST';
                $objGPXFiles->idprocesso    = $referencia;
                $resFileSaveGPX             = $objGPXFiles->saveListFileByPOST('fileupload_gpx');

                $objKMLFiles                = new VirtualDeskSitePercursosPedestresKMLFilesHelper();
                $objKMLFiles->tagprocesso   = 'PERCURSOSPEDESTRES_POST';
                $objKMLFiles->idprocesso    = $referencia;
                $resFileSaveKML             = $objKMLFiles->saveListFileByPOST('fileupload_kml');

                if ($resFileSaveCapa===false || $resFileSaveGPX==false || $resFileSaveGaleria==false || $resFileSaveKML==false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_CREATED') . 'ref=' . $referencia;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name_pt','estado as name_en') )
                    ->from("#__virtualdesk_PercursosPedestres_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $rowName
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=view4manager&percursospedestres_id=');

                    $table  = " ( SELECT a.id as id, a.id as percursospedestres_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.percurso as percurso";
                    $table .= " , IFNULL(b.concelho,' ') as concelho, IFNULL(c.name_PT,' ') as tipologia, IFNULL(d.name_PT,' ') as zonamento";
                    $table .= " , IFNULL(e.estado, ' ') as estado, IFNULL(f.nome,' ') as patrocinador, a.concelho as idconcelho, a.tipologia as idtipologia, a.zonamento as idzonamento, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Tipologia AS c ON c.id = a.tipologia ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Zonamento AS d ON d.id = a.zonamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS e ON e.id_estado = a.estado ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Patrocinadores AS f ON f.id = a.patrocinador ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'codigo',        'dt' => 0 ),
                        array( 'db' => 'percurso',      'dt' => 1 ),
                        array( 'db' => 'patrocinador',  'dt' => 2 ),
                        array( 'db' => 'tipologia',     'dt' => 3 ),
                        array( 'db' => 'zonamento',     'dt' => 4 ),
                        array( 'db' => 'concelho',      'dt' => 5 ),
                        array( 'db' => 'estado',        'dt' => 6 ),
                        array( 'db' => 'dummy',         'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 8 ),
                        array( 'db' => 'id',            'dt' => 9 ),
                        array( 'db' => 'idtipologia',   'dt' => 10 ),
                        array( 'db' => 'idzonamento',   'dt' => 11 ),
                        array( 'db' => 'idconcelho',    'dt' => 12 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresMMGList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=view4manager&percursospedestres_id=');

                    $table  = " ( SELECT a.id as id, a.id as percursospedestres_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.percurso as percurso";
                    $table .= " , IFNULL(b.concelho,' ') as concelho, IFNULL(c.name_PT,' ') as tipologia, IFNULL(d.name_PT,' ') as zonamento";
                    $table .= " , IFNULL(e.estado, ' ') as estado, IFNULL(f.nome,' ') as patrocinador, a.concelho as idconcelho, a.tipologia as idtipologia, a.zonamento as idzonamento, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_concelhos AS b ON b.id = a.concelho ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Tipologia AS c ON c.id = a.tipologia ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Zonamento AS d ON d.id = a.zonamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS e ON e.id_estado = a.estado ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Patrocinadores AS f ON f.id = a.patrocinador ";
                    $table .= " WHERE a.mmg = 1 ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'codigo',        'dt' => 0 ),
                        array( 'db' => 'percurso',      'dt' => 1 ),
                        array( 'db' => 'patrocinador',  'dt' => 2 ),
                        array( 'db' => 'tipologia',     'dt' => 3 ),
                        array( 'db' => 'zonamento',     'dt' => 4 ),
                        array( 'db' => 'concelho',      'dt' => 5 ),
                        array( 'db' => 'estado',        'dt' => 6 ),
                        array( 'db' => 'dummy',         'dt' => 7 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 8 ),
                        array( 'db' => 'id',            'dt' => 9 ),
                        array( 'db' => 'idtipologia',   'dt' => 10 ),
                        array( 'db' => 'idzonamento',   'dt' => 11 ),
                        array( 'db' => 'idconcelho',    'dt' => 12 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListTipologias4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'tipologia4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','tipologia4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=edittipologia4manager&tipologia_id=');

                    $table  = " ( SELECT a.id as id, a.id as tipologia_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.name_PT as name_PT, a.name_EN as name_EN, a.name_FR as name_FR, a.name_DE as name_DE, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Tipologia as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'name_PT',      'dt' => 1 ),
                        array( 'db' => 'name_EN',     'dt' => 2 ),
                        array( 'db' => 'name_FR',     'dt' => 3 ),
                        array( 'db' => 'name_DE',      'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 7 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListDificuldade4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'dificuldade4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','dificuldade4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editdificuldade4manager&dificuldade_id=');

                    $table  = " ( SELECT a.id as id, a.id as dificuldade_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.name as name, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Dificuldade as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'name',      'dt' => 1 ),
                        array( 'db' => 'estado',        'dt' => 2 ),
                        array( 'db' => 'dummy',         'dt' => 3 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 4 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListTerrenos4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'terreno4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','terreno4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editterreno4manager&terreno_id=');

                    $table  = " ( SELECT a.id as id, a.id as terreno_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.name_PT as name_PT, a.name_EN as name_EN, a.name_FR as name_FR, a.name_DE as name_DE, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Terreno as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'name_PT',      'dt' => 1 ),
                        array( 'db' => 'name_EN',     'dt' => 2 ),
                        array( 'db' => 'name_FR',     'dt' => 3 ),
                        array( 'db' => 'name_DE',      'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 7 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListDuracao4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'duracao4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','duracao4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editduracao4manager&duracao_id=');

                    $table  = " ( SELECT a.id as id, a.id as duracao_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.duracao_min as duracao_min, a.duracao_max as duracao_max, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Duracao as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'duracao_min',      'dt' => 1 ),
                        array( 'db' => 'duracao_max',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListVertigens4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'vertigens4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','vertigens4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editvertigens4manager&vertigens_id=');

                    $table  = " ( SELECT a.id as id, a.id as vertigens_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.name_PT as name_PT, a.name_EN as name_EN, a.name_FR as name_FR, a.name_DE as name_DE, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Vertigens as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'name_PT',      'dt' => 1 ),
                        array( 'db' => 'name_EN',     'dt' => 2 ),
                        array( 'db' => 'name_FR',     'dt' => 3 ),
                        array( 'db' => 'name_DE',      'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 7 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListZonamento4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'zonamento4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','zonamento4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editzonamento4manager&zonamento_id=');

                    $table  = " ( SELECT a.id as id, a.id as zonamento_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.name_PT as name_PT, a.name_EN as name_EN, a.name_FR as name_FR, a.name_DE as name_DE, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Zonamento as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'name_PT',      'dt' => 1 ),
                        array( 'db' => 'name_EN',     'dt' => 2 ),
                        array( 'db' => 'name_FR',     'dt' => 3 ),
                        array( 'db' => 'name_DE',      'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 7 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListSentido4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'sentido4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','sentido4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editsentido4manager&sentido_id=');

                    $table  = " ( SELECT a.id as id, a.id as sentido_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.name_PT as name_PT, a.name_EN as name_EN, a.name_FR as name_FR, a.name_DE as name_DE, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Sentido as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'name_PT',      'dt' => 1 ),
                        array( 'db' => 'name_EN',     'dt' => 2 ),
                        array( 'db' => 'name_FR',     'dt' => 3 ),
                        array( 'db' => 'name_DE',      'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 7 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListPaisagem4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'paisagem4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','paisagem4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editpaisagem4manager&paisagem_id=');

                    $table  = " ( SELECT a.id as id, a.id as paisagem_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.name_PT as name_PT, a.name_EN as name_EN, a.name_FR as name_FR, a.name_DE as name_DE, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Paisagem as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'name_PT',      'dt' => 1 ),
                        array( 'db' => 'name_EN',     'dt' => 2 ),
                        array( 'db' => 'name_FR',     'dt' => 3 ),
                        array( 'db' => 'name_DE',      'dt' => 4 ),
                        array( 'db' => 'estado',        'dt' => 5 ),
                        array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 7 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListDistancia4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'distancia4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','distancia4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editdistancia4manager&distancia_id=');

                    $table  = " ( SELECT a.id as id, a.id as distancia_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.distancia_min as distancia_min, a.distancia_max as distancia_max, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Distancia as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'distancia_min',      'dt' => 1 ),
                        array( 'db' => 'distancia_max',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosPedestresListAltitude4ManagerByAjax ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'altitude4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','altitude4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=editaltitude4manager&altitude_id=');

                    $table  = " ( SELECT a.id as id, a.id as altitude_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy";
                    $table .= " , a.altitude_min as altitude_min, a.altitude_max as altitude_max, b.estado as estado, a.estado as idestado";
                    $table .= " FROM ".$dbprefix."virtualdesk_PercursosPedestres_Altitude as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'id',        'dt' => 0 ),
                        array( 'db' => 'altitude_min',      'dt' => 1 ),
                        array( 'db' => 'altitude_max',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-pendente';
                break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }

        public static function getPercursosPedestresEstadoAtualObjectByIdPercursosPedestres ($lang, $id_percurso)
        {
            if((int) $id_percurso<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('b.id_estado as id_estado','b.estado as estado_PT','b.estado as estado_EN') )
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where(" a.id = ". $db->escape($id_percurso))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                $obj= new stdClass();
                $obj->id_estado =  $dataReturn->id_estado;
                if( empty($lang) or ($lang='pt_PT') ) {
                    $obj->name = $dataReturn->estado_PT;
                }
                else {
                    $obj->name  = $dataReturn->estado_EN;
                }

                $obj->cssClass = self::getEstadoCSS($dataReturn->id_estado);

                return($obj);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosDetail4Manager ($IdPercurso)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdPercurso) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as percursospedestres_id', 'a.referencia as codigo', 'a.percurso as percurso', 'a.descricao as descricao',
                        'a.descricaoEN as descricaoEN', 'a.descricaoFR as descricaoFR', 'a.descricaoDE as descricaoDE', "IFNULL(b.name_PT,'') as tipologia",
                        "IFNULL(c.name_PT,'') as paisagem", 'a.percurso_laurissilva as percurso_laurissilva', 'a.costa_laurissilva as costa_laurissilva', 'a.costa_sol as costa_sol',
                        'a.costa_sol as costa_sol', 'a.mmg as mmg', "IFNULL(d.name_PT,'') as sentido", "IFNULL(e.name_PT,'') as terreno", "IFNULL(f.name_PT,'') as vertigens", 'a.distancia as distancia',
                        'a.duracao as duracao', 'a.altitude_max as altitude_max', 'a.altitude_min as altitude_min', 'a.subida_acumulado as subida_acumulado', 'a.descida_acumulado as descida_acumulado',
                        'a.mmg as mmg', "IFNULL(g.name,'') as dificuldade", "IFNULL(h.distrito,'') as distrito", "IFNULL(i.concelho,'') as concelho", "IFNULL(j.freguesia,'') as freguesia",
                        "IFNULL(k.freguesia,'') as inicio_freguesia", "IFNULL(l.freguesia,'') as fim_freguesia", "IFNULL(m.name_PT,'') as zonamento", 'a.inicio_sitio as inicio_sitio',
                        'a.fim_sitio as fim_sitio', 'a.localidades as localidades', "IFNULL(n.estado,'') as estado", "IFNULL(o.nome,'') as nome_patrocinador", 'a.paisagem as idpaisagem', 'a.sentido as idsentido', 'a.tipologia as idtipologia',
                        'a.terreno as idterreno', 'a.vertigens as idvertigens', 'a.dificuldade as iddificuldade', 'a.distrito as iddistrito', 'a.concelho as idconcelho', 'a.concelho as idconcelho',
                        'a.freguesia as idfreguesia', 'a.inicio_freguesia as idinicio_freguesia', 'a.fim_freguesia as idfim_freguesia', 'a.zonamento as idzonamento', 'a.estado as idestado', 'a.creditosFotoCapa as nomecredito',
                        'a.linkCreditos as linkcredito', 'a.destaques as destaques', 'a.destaquesEN as destaquesEN', 'a.destaquesFR as destaquesFR', 'a.destaquesDE as destaquesDE', 'a.hastags as hastags', 'a.patrocinador as patrocinador', 'a.data_criacao as data_criacao', 'a.data_alteracao as data_alteracao'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Tipologia AS b ON b.id = a.tipologia')
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Paisagem AS c ON c.id = a.paisagem')
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Sentido AS d ON d.id = a.sentido')
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Terreno AS e ON e.id = a.terreno')
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Vertigens AS f ON f.id = a.vertigens')
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Dificuldade AS g ON g.id = a.dificuldade')
                    ->join('LEFT', '#__virtualdesk_digitalGov_distritos AS h ON h.id = a.distrito')
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS i ON i.id = a.concelho')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS j ON j.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS k ON k.id = a.inicio_freguesia')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS l ON l.id = a.fim_freguesia')
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Zonamento AS m ON m.id = a.zonamento')
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS n ON n.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Patrocinadores AS o ON o.id = a.patrocinador')
                    ->from("#__virtualdesk_PercursosPedestres as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdPercurso)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPercursosEstadoAllOptions ($lang, $displayPermError=true)
        {

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
            // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false) JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado','estado') )
                    ->from("#__virtualdesk_PercursosPedestres_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->estado;
                    }
                    else {
                        $rowName = $row->estado;
                    }
                    $response[] = array(
                        'id' => $row->id_estado,
                        'name' => $rowName
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function saveAlterar2NewEstado4ManagerByAjax($getInputPercursosPedestres_id, $NewEstadoId, $NewEstadoDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('percursospedestres', 'alterarestado4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewEstadoId) ) return false;
            if((int) $getInputPercursosPedestres_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            $data = array();
            $data['estado'] = $NewEstadoId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();

            $Table = new VirtualDeskTablePercursosPedestres($db);
            $Table->load(array('id'=>$getInputPercursosPedestres_id));

            // Só alterar se o estado for diferente
            if((int)$Table->estado == (int)$NewEstadoId )  return true;

            if(!empty($data)) {

            }

            $db->transactionStart();

            try {
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTablePercursosPedestresEstadoHistorico($db);
                $dataHist = array();
                $dataHist['id_estado']  = $NewEstadoId;
                $dataHist['id_percurso']  = $getInputPercursosPedestres_id;
                $dataHist['descricao']  = $NewEstadoDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_percurso=' . $dataHist['id_percurso'];
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_STATE');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $percursospedestres_id = $data['percursospedestres_id'];
            if((int) $percursospedestres_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTablePercursosPedestres($db);
            $Table->load(array('id'=>$percursospedestres_id));

            $db->transactionStart();

            try {

                $referencia          = $Table->referencia;
                $obParam             = new VirtualDeskSiteParamsHelper();

                $percurso = null;
                $descricao = null;
                $descricaoEN = null;
                $descricaoFR = null;
                $descricaoDE = null;
                $tipologia = null;
                $paisagem = null;
                $percurso_laurissilva = null;
                $costa_laurissilva = null;
                $costa_sol = null;
                $mmg = null;
                $sentido = null;
                $terreno = null;
                $vertigens = null;
                $distancia = null;
                $duracao = null;
                $altitude_max = null;
                $altitude_min = null;
                $subida_acumulado = null;
                $descida_acumulado = null;
                $dificuldade = null;
                $distrito = null;
                $concelho = null;
                $freguesia = null;
                $inicio_freguesia = null;
                $fim_freguesia = null;
                $zonamento = null;
                $inicio_sitio = null;
                $fim_sitio = null;
                $localidades = null;
                $nomecredito = null;
                $linkcredito = null;
                $DestaqueInput = null;
                $HastagsInput = null;
                $patrocinador = null;

                if(array_key_exists('nomepercurso',$data))          $percurso = $data['nomepercurso'];
                if(array_key_exists('descricao',$data))             $descricao = $data['descricao'];
                if(array_key_exists('descricaoEN',$data))           $descricaoEN = $data['descricaoEN'];
                if(array_key_exists('descricaoFR',$data))           $descricaoFR = $data['descricaoFR'];
                if(array_key_exists('descricaoDE',$data))           $descricaoDE = $data['descricaoDE'];
                if(array_key_exists('tipologia',$data))             $tipologia = $data['tipologia'];
                if(array_key_exists('tipoPaisagem',$data))          $paisagem = $data['tipoPaisagem'];
                if(array_key_exists('percLaurissilvaValue',$data))  $percurso_laurissilva = $data['percLaurissilvaValue'];
                if(array_key_exists('costLaurissilvaValue',$data))  $costa_laurissilva = $data['costLaurissilvaValue'];
                if(array_key_exists('costSolValue',$data))          $costa_sol = $data['costSolValue'];
                if(array_key_exists('mmgValue',$data))              $mmg = $data['mmgValue'];
                if(array_key_exists('sentido',$data))               $sentido = $data['sentido'];
                if(array_key_exists('terreno',$data))               $terreno = $data['terreno'];
                if(array_key_exists('vertigens',$data))             $vertigens = $data['vertigens'];
                if(array_key_exists('distancia',$data))             $distancia = $data['distancia'];
                if(array_key_exists('horas',$data))                 $horas = $data['horas'];
                if(array_key_exists('minutos',$data))               $minutos = $data['minutos'];
                if(array_key_exists('altitudemax',$data))           $altitude_max = $data['altitudemax'];
                if(array_key_exists('altitudemin',$data))           $altitude_min = $data['altitudemin'];
                if(array_key_exists('subidaAcumulado',$data))       $subida_acumulado = $data['subidaAcumulado'];
                if(array_key_exists('descidaAcumulado',$data))      $descida_acumulado = $data['descidaAcumulado'];
                if(array_key_exists('dificuldade',$data))           $dificuldade = $data['dificuldade'];
                if(array_key_exists('distrito',$data))              $distrito = $data['distrito'];
                if(array_key_exists('concelho',$data))              $concelho = $data['concelho'];
                if(array_key_exists('freguesia',$data))             $freguesia = $data['freguesia'];
                if(array_key_exists('freguesiaInicio',$data))       $inicio_freguesia = $data['freguesiaInicio'];
                if(array_key_exists('freguesiaFim',$data))          $fim_freguesia = $data['freguesiaFim'];
                if(array_key_exists('zonamento',$data))             $zonamento = $data['zonamento'];
                if(array_key_exists('sitioInicio',$data))           $inicio_sitio = $data['sitioInicio'];
                if(array_key_exists('sitioFim',$data))              $fim_sitio = $data['sitioFim'];
                if(array_key_exists('localidades',$data))           $localidades = $data['localidades'];
                if(array_key_exists('nomecredito',$data))           $nomecredito = $data['nomecredito'];
                if(array_key_exists('linkcredito',$data))           $linkcredito = $data['linkcredito'];
                if(array_key_exists('DestaqueInput',$data))         $DestaqueInput = $data['DestaqueInput'];
                if(array_key_exists('HastagsInput',$data))          $HastagsInput = $data['HastagsInput'];
                if(array_key_exists('patrocinador',$data))          $patrocinador = $data['patrocinador'];

                $duracao = $horas . ',' . $minutos;


                $arrayDistancia = explode(".", $distancia);
                if(!empty($arrayDistancia[1])){
                    $distanciaEditado = $arrayDistancia[0] . ',' . $arrayDistancia[1];
                } else {
                    $distanciaEditado = $arrayDistancia[0];
                }

                $arrayDistancia2 = explode(",", $distanciaEditado);

                $distMin = self::getDistanciaMinima();
                foreach($distMin as $rowWSL) :
                    $distanciaMinima = $rowWSL['distancia_min'];
                    $distanciaMaxima = $rowWSL['distancia_max'];
                    if($arrayDistancia2[0] < $distanciaMaxima) {
                        if($arrayDistancia2[0] >= $distanciaMinima){
                            $idDistancia = self::getIdDistancia($distanciaMinima);
                            break;
                        }
                    }
                endforeach;

                $arrayDuracao2 = explode(",", $duracao);

                $durMin = self::getDuracaoMinima();
                foreach($durMin as $rowWSL) :
                    $duracaoMinima = $rowWSL['duracao_min'];
                    $duracaoMaxima = $rowWSL['duracao_max'];
                    if($arrayDuracao2[0] < $duracaoMaxima) {
                        if($arrayDuracao2[0] >= $duracaoMinima){
                            $idDuracao = self::getIdDuracao($duracaoMinima);
                            break;
                        }
                    }
                endforeach;

                foreach($DestaqueInput as $keyPar => $valParc ){
                    if(empty($destaques)){
                        $destaques = $valParc['destaque'];
                    } else {
                        $destaques .= ';;' . $valParc['destaque'];
                    }

                    if(empty($destaquesEN)){
                        $destaquesEN = $valParc['destaqueEN'];
                    } else {
                        $destaquesEN .= ';;' . $valParc['destaqueEN'];
                    }

                    if(empty($destaquesFR)){
                        $destaquesFR = $valParc['destaqueFR'];
                    } else {
                        $destaquesFR .= ';;' . $valParc['destaqueFR'];
                    }

                    if(empty($destaquesDE)){
                        $destaquesDE = $valParc['destaqueDE'];
                    } else {
                        $destaquesDE .= ';;' . $valParc['destaqueDE'];
                    }
                }

                foreach($HastagsInput as $keyPar => $valHasTag ){
                    if($valHasTag['hastags'] != ''){
                        if(empty($hastags)){
                            $hastags = $valHasTag['hastags'];
                        } else {
                            $hastags .= ';;' . $valHasTag['hastags'];
                        }

                    }
                }

                /* BEGIN Save Percurso */
                $resSavePercurso = self::saveEditedPercurso4Manager($percursospedestres_id, $patrocinador, $hastags, $destaques, $destaquesEN, $destaquesFR, $destaquesDE, $nomecredito, $linkcredito, $percurso, $descricao, $descricaoEN, $descricaoFR, $descricaoDE, $tipologia, $paisagem, $percurso_laurissilva, $costa_laurissilva, $costa_sol, $mmg, $sentido, $terreno, $vertigens, $distanciaEditado, $idDistancia, $duracao, $idDuracao, $altitude_max, $altitude_min, $subida_acumulado, $descida_acumulado, $dificuldade, $distrito, $concelho, $freguesia, $inicio_freguesia, $fim_freguesia, $zonamento, $inicio_sitio, $fim_sitio, $localidades);
                /* END Save Percurso */

                if (empty($resSavePercurso)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objCapaFiles                = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
                $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_capa');
                $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
                if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o ficheiros de Capa', 'error');
                }

                $objGaleriaFiles             = new VirtualDeskSitePercursosPedestresGaleriaFilesHelper();
                $listFileGaleria2Eliminar    = $objGaleriaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_galeria');
                $resFileGaleriaDelete        = $objGaleriaFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
                if ($resFileGaleriaDelete==false && !empty($listFileGaleria2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros da Galeria', 'error');
                }

                $objKMLFiles              = new VirtualDeskSitePercursosPedestresKMLFilesHelper();
                $listFileKML2Eliminar     = $objKMLFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_kml');
                $resFileKMLDelete         = $objKMLFiles->deleteFiles ($referencia , $listFileKML2Eliminar);
                if ($resFileKMLDelete==false && !empty($listFileKML2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o ficheiro KML', 'error');
                }

                $objGPXFiles             = new VirtualDeskSitePercursosPedestresGPXFilesHelper();
                $listFileGPX2Eliminar    = $objGPXFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_gpx');
                $resFileGPXDelete        = $objGPXFiles->deleteFiles ($referencia , $listFileGPX2Eliminar);
                if ($resFileGPXDelete==false && !empty($listFileGPX2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o ficheiro GPX', 'error');
                }




                // Insere os NOVOS ficheiros
                /* FILES */
                $objCapaFiles                = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
                $objCapaFiles->tagprocesso   = 'PERCURSOSPEDESTRES_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');

                $objGPXFiles              = new VirtualDeskSitePercursosPedestresGPXFilesHelper();
                $objGPXFiles->tagprocesso = 'PERCURSOSPEDESTRES_POST';
                $objGPXFiles->idprocesso  = $referencia;
                $resFileSaveGPX           = $objGPXFiles->saveListFileByPOST('fileupload_gpx');

                $objGaleriaFiles              = new VirtualDeskSitePercursosPedestresGaleriaFilesHelper();
                $objGaleriaFiles->tagprocesso = 'PERCURSOSPEDESTRES_POST';
                $objGaleriaFiles->idprocesso  = $referencia;
                $resFileSaveGaleria           = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

                $objKMLFiles              = new VirtualDeskSitePercursosPedestresKMLFilesHelper();
                $objKMLFiles->tagprocesso = 'PERCURSOSPEDESTRES_POST';
                $objKMLFiles->idprocesso  = $referencia;
                $resFileSaveKML           = $objKMLFiles->saveListFileByPOST('fileupload_kml');

                if ($resFileSaveCapa===false || $resFileSaveGPX==false || $resFileSaveGaleria==false || $resFileSaveKML==false) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_CREATED') . 'ref=' . $referencia;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedPercurso4Manager($percursospedestres_id, $patrocinador, $hastags, $destaques, $destaquesEN, $destaquesFR, $destaquesDE, $nomecredito, $linkcredito, $percurso, $descricao, $descricaoEN, $descricaoFR, $descricaoDE, $tipologia, $paisagem, $percurso_laurissilva, $costa_laurissilva, $costa_sol, $mmg, $sentido, $terreno, $vertigens, $distancia, $idDistancia, $duracao, $idDuracao, $altitude_max, $altitude_min, $subida_acumulado, $descida_acumulado, $dificuldade, $distrito, $concelho, $freguesia, $inicio_freguesia, $fim_freguesia, $zonamento, $inicio_sitio, $fim_sitio, $localidades)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);
            $descricaoEN = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricaoEN),true);
            $descricaoFR = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricaoFR),true);
            $descricaoDE = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricaoDE),true);
            $localidades = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($localidades),true);

            if(!is_null($percurso))  array_push($fields, 'percurso="'.$db->escape($percurso).'"');
            if(!is_null($descricao))  array_push($fields, 'descricao="'.$db->escape($descricao).'"');
            if(!is_null($descricaoEN))  array_push($fields, 'descricaoEN="'.$db->escape($descricaoEN).'"');
            if(!is_null($descricaoFR))  array_push($fields, 'descricaoFR="'.$db->escape($descricaoFR).'"');
            if(!is_null($descricaoDE))  array_push($fields, 'descricaoDE="'.$db->escape($descricaoDE).'"');
            if(!is_null($tipologia)) array_push($fields, 'tipologia='.$db->escape($tipologia));
            if(!is_null($paisagem)) array_push($fields, 'paisagem='.$db->escape($paisagem));
            if(!is_null($percurso_laurissilva)) array_push($fields, 'percurso_laurissilva='.$db->escape($percurso_laurissilva));
            if(!is_null($costa_laurissilva)) array_push($fields, 'costa_laurissilva='.$db->escape($costa_laurissilva));
            if(!is_null($costa_sol)) array_push($fields, 'costa_sol='.$db->escape($costa_sol));
            if(!is_null($mmg)) array_push($fields, 'mmg='.$db->escape($mmg));
            if(!is_null($sentido)) array_push($fields, 'sentido='.$db->escape($sentido));
            if(!is_null($terreno)) array_push($fields, 'terreno='.$db->escape($terreno));
            if(!is_null($vertigens)) array_push($fields, 'vertigens='.$db->escape($vertigens));
            if(!is_null($distancia))  array_push($fields, 'distancia="'.$db->escape($distancia).'"');
            if(!is_null($idDistancia))  array_push($fields, 'idDistancia="'.$db->escape($idDistancia).'"');
            if(!is_null($duracao))  array_push($fields, 'duracao="'.$db->escape($duracao).'"');
            if(!is_null($idDuracao))  array_push($fields, 'idDuracao="'.$db->escape($idDuracao).'"');
            if(!is_null($altitude_max)) array_push($fields, 'altitude_max='.$db->escape($altitude_max));
            if(!is_null($altitude_min)) array_push($fields, 'altitude_min='.$db->escape($altitude_min));
            if(!is_null($subida_acumulado)) array_push($fields, 'subida_acumulado='.$db->escape($subida_acumulado));
            if(!is_null($descida_acumulado)) array_push($fields, 'descida_acumulado='.$db->escape($descida_acumulado));
            if(!is_null($dificuldade)) array_push($fields, 'dificuldade='.$db->escape($dificuldade));
            if(!is_null($distrito)) array_push($fields, 'distrito='.$db->escape($distrito));
            if(!is_null($concelho)) array_push($fields, 'concelho='.$db->escape($concelho));
            if(!is_null($freguesia)) array_push($fields, 'freguesia='.$db->escape($freguesia));
            if(!is_null($inicio_freguesia)) array_push($fields, 'inicio_freguesia='.$db->escape($inicio_freguesia));
            if(!is_null($fim_freguesia)) array_push($fields, 'fim_freguesia='.$db->escape($fim_freguesia));
            if(!is_null($zonamento)) array_push($fields, 'zonamento='.$db->escape($zonamento));
            if(!is_null($inicio_sitio))  array_push($fields, 'inicio_sitio="'.$db->escape($inicio_sitio).'"');
            if(!is_null($fim_sitio))  array_push($fields, 'fim_sitio="'.$db->escape($fim_sitio).'"');
            if(!is_null($localidades))  array_push($fields, 'localidades="'.$db->escape($localidades).'"');
            if(!is_null($nomecredito))  array_push($fields, 'creditosFotoCapa="'.$db->escape($nomecredito).'"');
            if(!is_null($linkcredito))  array_push($fields, 'linkCreditos="'.$db->escape($linkcredito).'"');
            if(!is_null($destaques))  array_push($fields, 'destaques="'.$db->escape($destaques).'"');
            if(!is_null($destaquesEN))  array_push($fields, 'destaquesEN="'.$db->escape($destaquesEN).'"');
            if(!is_null($destaquesFR))  array_push($fields, 'destaquesFR="'.$db->escape($destaquesFR).'"');
            if(!is_null($destaquesDE))  array_push($fields, 'destaquesDE="'.$db->escape($destaquesDE).'"');
            if(!is_null($hastags))  array_push($fields, 'hastags="'.$db->escape($hastags).'"');
            if(!is_null($patrocinador))  array_push($fields, 'patrocinador="'.$db->escape($patrocinador).'"');


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres')
                ->set($fields)
                ->where(' id = '.$db->escape($percursospedestres_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function createTipologia4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewtipologia4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $name_PT = $data['name_PT'];
                $name_EN = $data['name_EN'];
                $name_FR = $data['name_FR'];
                $name_DE = $data['name_DE'];
                $estado = $data['estado'];

                $resSaveTipologia = self::saveNewTipologia($name_PT, $name_EN, $name_FR, $name_DE, $estado);

                if (empty($resSaveTipologia)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewTipologia($name_PT, $name_EN, $name_FR, $name_DE, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('name_PT','name_EN','name_FR','name_DE','estado');
            $values = array($db->quote($db->escape($name_PT)), $db->quote($db->escape($name_EN)), $db->quote($db->escape($name_FR)), $db->quote($db->escape($name_DE)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Tipologia'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createDificuldade4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewdificuldade4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $name = $data['name'];
                $estado = $data['estado'];

                $resSaveDificuldade = self::saveNewDificuldade($name, $estado);

                if (empty($resSaveDificuldade)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewDificuldade($name, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('name','estado');
            $values = array($db->quote($db->escape($name)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Dificuldade'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createTerreno4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewterreno4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $name_PT = $data['name_PT'];
                $name_EN = $data['name_EN'];
                $name_FR = $data['name_FR'];
                $name_DE = $data['name_DE'];
                $estado = $data['estado'];

                $resSaveTerreno = self::saveNewTerreno($name_PT, $name_EN, $name_FR, $name_DE, $estado);

                if (empty($resSaveTerreno)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewTerreno($name_PT, $name_EN, $name_FR, $name_DE, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('name_PT','name_EN','name_FR','name_DE','estado');
            $values = array($db->quote($db->escape($name_PT)), $db->quote($db->escape($name_EN)), $db->quote($db->escape($name_FR)), $db->quote($db->escape($name_DE)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Terreno'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createDuracao4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewduracao4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $duracao_min = $data['duracao_min'];
                $duracao_max = $data['duracao_max'];
                $estado = $data['estado'];

                $resSaveDuracao = self::saveNewDuracao($duracao_min, $duracao_max, $estado);

                if (empty($resSaveDuracao)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewDuracao($duracao_min, $duracao_max, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('duracao_min','duracao_max','estado');
            $values = array($db->quote($db->escape($duracao_min)), $db->quote($db->escape($duracao_max)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Duracao'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createVertigens4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewvertigens4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $name_PT = $data['name_PT'];
                $name_EN = $data['name_EN'];
                $name_FR = $data['name_FR'];
                $name_DE = $data['name_DE'];
                $estado = $data['estado'];

                $resSaveVertigens = self::saveNewVertigens($name_PT, $name_EN, $name_FR, $name_DE, $estado);

                if (empty($resSaveVertigens)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewVertigens($name_PT, $name_EN, $name_FR, $name_DE, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('name_PT','name_EN','name_FR','name_DE','estado');
            $values = array($db->quote($db->escape($name_PT)), $db->quote($db->escape($name_EN)), $db->quote($db->escape($name_FR)), $db->quote($db->escape($name_DE)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Vertigens'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createZonamento4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewzonamento4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $name_PT = $data['name_PT'];
                $name_EN = $data['name_EN'];
                $name_FR = $data['name_FR'];
                $name_DE = $data['name_DE'];
                $estado = $data['estado'];

                $resSaveZonamento = self::saveNewZonamento($name_PT, $name_EN, $name_FR, $name_DE, $estado);

                if (empty($resSaveZonamento)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewZonamento($name_PT, $name_EN, $name_FR, $name_DE, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('name_PT','name_EN','name_FR','name_DE','estado');
            $values = array($db->quote($db->escape($name_PT)), $db->quote($db->escape($name_EN)), $db->quote($db->escape($name_FR)), $db->quote($db->escape($name_DE)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Zonamento'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createSentido4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewsentido4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $name_PT = $data['name_PT'];
                $name_EN = $data['name_EN'];
                $name_FR = $data['name_FR'];
                $name_DE = $data['name_DE'];
                $estado = $data['estado'];

                $resSaveSentido = self::saveNewSentido($name_PT, $name_EN, $name_FR, $name_DE, $estado);

                if (empty($resSaveSentido)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewSentido($name_PT, $name_EN, $name_FR, $name_DE, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('name_PT','name_EN','name_FR','name_DE','estado');
            $values = array($db->quote($db->escape($name_PT)), $db->quote($db->escape($name_EN)), $db->quote($db->escape($name_FR)), $db->quote($db->escape($name_DE)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Sentido'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createPaisagem4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewpaisagem4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $name_PT = $data['name_PT'];
                $name_EN = $data['name_EN'];
                $name_FR = $data['name_FR'];
                $name_DE = $data['name_DE'];
                $estado = $data['estado'];

                $resSavePaisagem = self::saveNewPaisagem($name_PT, $name_EN, $name_FR, $name_DE, $estado);

                if (empty($resSavePaisagem)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewPaisagem($name_PT, $name_EN, $name_FR, $name_DE, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('name_PT','name_EN','name_FR','name_DE','estado');
            $values = array($db->quote($db->escape($name_PT)), $db->quote($db->escape($name_EN)), $db->quote($db->escape($name_FR)), $db->quote($db->escape($name_DE)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Paisagem'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createDistancia4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewduracao4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $distancia_min = $data['distancia_min'];
                $distancia_max = $data['distancia_max'];
                $estado = $data['estado'];

                $resSaveDistancia = self::saveNewDistancia($distancia_min, $distancia_max, $estado);

                if (empty($resSaveDistancia)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewDistancia($distancia_min, $distancia_max, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('distancia_min','distancia_max','estado');
            $values = array($db->quote($db->escape($distancia_min)), $db->quote($db->escape($distancia_max)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Distancia'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function createAltitude4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
              * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
              */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewaltitude4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $altitude_min = $data['altitude_min'];
                $altitude_max = $data['altitude_max'];
                $estado = $data['estado'];

                $resSaveAltitude = self::saveNewAltitude($altitude_min, $altitude_max, $estado);

                if (empty($resSaveAltitude)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursoPedestre_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_EVENTLOG_CREATED');
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_EVENTLOG_CREATED');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function saveNewAltitude($altitude_min, $altitude_max, $estado){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('altitude_min','altitude_max','estado');
            $values = array($db->quote($db->escape($altitude_min)), $db->quote($db->escape($altitude_max)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_PercursosPedestres_Altitude'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }

        public static function getTipologiaDetail4Manager ($IdTipologia)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edittipologia4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','edittipologia4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdTipologia) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as tipologia_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN', 'a.name_FR as name_FR', 'a.name_DE as name_DE', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Tipologia as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdTipologia)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getDificuldadeDetail4Manager ($IdDificuldade)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdificuldade4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editdificuldade4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdDificuldade) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as dificuldade_id', 'a.name as name', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Dificuldade as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdDificuldade)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getTerrenoDetail4Manager ($IdTerreno)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editterreno4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editterreno4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdTerreno) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as terreno_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN', 'a.name_FR as name_FR', 'a.name_DE as name_DE', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Terreno as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdTerreno)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getDuracaoDetail4Manager ($IdDuracao)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editduracao4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editduracao4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdDuracao) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as duracao_id', 'a.duracao_min as duracao_min', 'a.duracao_max as duracao_max', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Duracao as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdDuracao)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getVertigensDetail4Manager ($IdVertigens)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editvertigens4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editvertigens4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdVertigens) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as vertigens_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN', 'a.name_FR as name_FR', 'a.name_DE as name_DE', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Vertigens as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdVertigens)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getZonamentoDetail4Manager ($IdZonamento)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editzonamento4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editzonamento4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdZonamento) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as zonamento_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN', 'a.name_FR as name_FR', 'a.name_DE as name_DE', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Zonamento as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdZonamento)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getSentidoDetail4Manager ($IdSentido)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editsentido4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editsentido4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdSentido) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as sentido_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN', 'a.name_FR as name_FR', 'a.name_DE as name_DE', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Sentido as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdSentido)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getPaisagemDetail4Manager ($IdPaisagem)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editpaisagem4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editpaisagem4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdPaisagem) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as paisagem_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN', 'a.name_FR as name_FR', 'a.name_DE as name_DE', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Paisagem as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdPaisagem)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getDistanciaDetail4Manager ($IdDistancia)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdistancia4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editdistancia4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdDistancia) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as distancia_id', 'a.distancia_min as distancia_min', 'a.distancia_max as distancia_max', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Distancia as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdDistancia)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getAltitudeDetail4Manager ($IdAltitude)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('percursospedestres');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editaltitude4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('percursospedestres'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('percursospedestres','editaltitude4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdAltitude) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as altitude_id', 'a.altitude_min as altitude_min', 'a.altitude_max as altitude_max', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_PercursosPedestres_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_PercursosPedestres_Altitude as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($IdAltitude)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function updateTipologia4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edittipologia4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $tipologia_id = $data['tipologia_id'];
            if((int) $tipologia_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableTipologia($db);
            $Table->load(array('id'=>$tipologia_id));

            $db->transactionStart();

            try {

                $name_PT = null;
                $name_EN = null;
                $name_FR = null;
                $name_DE = null;
                $estado = null;

                if(array_key_exists('name_PT',$data))   $name_PT = $data['name_PT'];
                if(array_key_exists('name_EN',$data))   $name_EN = $data['name_EN'];
                if(array_key_exists('name_FR',$data))   $name_FR = $data['name_FR'];
                if(array_key_exists('name_DE',$data))   $name_DE = $data['name_DE'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];


                /* BEGIN Save Percurso */
                $resSaveTipologia = self::saveEditedTipologia4Manager($tipologia_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado);
                /* END Save Percurso */

                if (empty($resSaveTipologia)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_EVENTLOG_EDITED') . 'ref=' . $tipologia_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedTipologia4Manager($tipologia_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edittipologia4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($name_PT))  array_push($fields, 'name_PT="'.$db->escape($name_PT).'"');
            if(!is_null($name_EN))  array_push($fields, 'name_EN="'.$db->escape($name_EN).'"');
            if(!is_null($name_FR))  array_push($fields, 'name_FR="'.$db->escape($name_FR).'"');
            if(!is_null($name_DE))  array_push($fields, 'name_DE="'.$db->escape($name_DE).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Tipologia')
                ->set($fields)
                ->where(' id = '.$db->escape($tipologia_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateTerreno4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editterreno4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $terreno_id = $data['terreno_id'];
            if((int) $terreno_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableTerreno($db);
            $Table->load(array('id'=>$terreno_id));

            $db->transactionStart();

            try {

                $name_PT = null;
                $name_EN = null;
                $name_FR = null;
                $name_DE = null;
                $estado = null;

                if(array_key_exists('name_PT',$data))   $name_PT = $data['name_PT'];
                if(array_key_exists('name_EN',$data))   $name_EN = $data['name_EN'];
                if(array_key_exists('name_FR',$data))   $name_FR = $data['name_FR'];
                if(array_key_exists('name_DE',$data))   $name_DE = $data['name_DE'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];


                /* BEGIN Save Percurso */
                $resSaveTerreno = self::saveEditedTerreno4Manager($terreno_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado);
                /* END Save Percurso */

                if (empty($resSaveTerreno)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_EVENTLOG_EDITED') . 'ref=' . $terreno_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedTerreno4Manager($terreno_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editterreno4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($name_PT))  array_push($fields, 'name_PT="'.$db->escape($name_PT).'"');
            if(!is_null($name_EN))  array_push($fields, 'name_EN="'.$db->escape($name_EN).'"');
            if(!is_null($name_FR))  array_push($fields, 'name_FR="'.$db->escape($name_FR).'"');
            if(!is_null($name_DE))  array_push($fields, 'name_DE="'.$db->escape($name_DE).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Terreno')
                ->set($fields)
                ->where(' id = '.$db->escape($terreno_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateVertigens4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editvertigens4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $vertigens_id = $data['vertigens_id'];
            if((int) $vertigens_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableVertigens($db);
            $Table->load(array('id'=>$vertigens_id));

            $db->transactionStart();

            try {

                $name_PT = null;
                $name_EN = null;
                $name_FR = null;
                $name_DE = null;
                $estado = null;

                if(array_key_exists('name_PT',$data))   $name_PT = $data['name_PT'];
                if(array_key_exists('name_EN',$data))   $name_EN = $data['name_EN'];
                if(array_key_exists('name_FR',$data))   $name_FR = $data['name_FR'];
                if(array_key_exists('name_DE',$data))   $name_DE = $data['name_DE'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];


                /* BEGIN Save Percurso */
                $resSaveVertigens = self::saveEditedVertigens4Manager($vertigens_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado);
                /* END Save Percurso */

                if (empty($resSaveVertigens)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_EVENTLOG_EDITED') . 'ref=' . $vertigens_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedVertigens4Manager($vertigens_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editvertigens4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($name_PT))  array_push($fields, 'name_PT="'.$db->escape($name_PT).'"');
            if(!is_null($name_EN))  array_push($fields, 'name_EN="'.$db->escape($name_EN).'"');
            if(!is_null($name_FR))  array_push($fields, 'name_FR="'.$db->escape($name_FR).'"');
            if(!is_null($name_DE))  array_push($fields, 'name_DE="'.$db->escape($name_DE).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Vertigens')
                ->set($fields)
                ->where(' id = '.$db->escape($vertigens_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateZonamento4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editzonamento4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $zonamento_id = $data['zonamento_id'];
            if((int) $zonamento_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableZonamento($db);
            $Table->load(array('id'=>$zonamento_id));

            $db->transactionStart();

            try {

                $name_PT = null;
                $name_EN = null;
                $name_FR = null;
                $name_DE = null;
                $estado = null;

                if(array_key_exists('name_PT',$data))   $name_PT = $data['name_PT'];
                if(array_key_exists('name_EN',$data))   $name_EN = $data['name_EN'];
                if(array_key_exists('name_FR',$data))   $name_FR = $data['name_FR'];
                if(array_key_exists('name_DE',$data))   $name_DE = $data['name_DE'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];


                /* BEGIN Save Percurso */
                $resSaveZonamento = self::saveEditedZonamento4Manager($zonamento_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado);
                /* END Save Percurso */

                if (empty($resSaveZonamento)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_EVENTLOG_EDITED') . 'ref=' . $zonamento_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedZonamento4Manager($zonamento_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editzonamento4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($name_PT))  array_push($fields, 'name_PT="'.$db->escape($name_PT).'"');
            if(!is_null($name_EN))  array_push($fields, 'name_EN="'.$db->escape($name_EN).'"');
            if(!is_null($name_FR))  array_push($fields, 'name_FR="'.$db->escape($name_FR).'"');
            if(!is_null($name_DE))  array_push($fields, 'name_DE="'.$db->escape($name_DE).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Zonamento')
                ->set($fields)
                ->where(' id = '.$db->escape($zonamento_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateSentido4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editsentido4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $sentido_id = $data['sentido_id'];
            if((int) $sentido_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableSentido($db);
            $Table->load(array('id'=>$sentido_id));

            $db->transactionStart();

            try {

                $name_PT = null;
                $name_EN = null;
                $name_FR = null;
                $name_DE = null;
                $estado = null;

                if(array_key_exists('name_PT',$data))   $name_PT = $data['name_PT'];
                if(array_key_exists('name_EN',$data))   $name_EN = $data['name_EN'];
                if(array_key_exists('name_FR',$data))   $name_FR = $data['name_FR'];
                if(array_key_exists('name_DE',$data))   $name_DE = $data['name_DE'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];


                /* BEGIN Save Percurso */
                $resSaveSentido = self::saveEditedSentido4Manager($sentido_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado);
                /* END Save Percurso */

                if (empty($resSaveSentido)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_EVENTLOG_EDITED') . 'ref=' . $sentido_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedSentido4Manager($sentido_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editsentido4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($name_PT))  array_push($fields, 'name_PT="'.$db->escape($name_PT).'"');
            if(!is_null($name_EN))  array_push($fields, 'name_EN="'.$db->escape($name_EN).'"');
            if(!is_null($name_FR))  array_push($fields, 'name_FR="'.$db->escape($name_FR).'"');
            if(!is_null($name_DE))  array_push($fields, 'name_DE="'.$db->escape($name_DE).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Sentido')
                ->set($fields)
                ->where(' id = '.$db->escape($sentido_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updatePaisagem4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editpaisagem4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $paisagem_id = $data['paisagem_id'];
            if((int) $paisagem_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTablePaisagem($db);
            $Table->load(array('id'=>$paisagem_id));

            $db->transactionStart();

            try {

                $name_PT = null;
                $name_EN = null;
                $name_FR = null;
                $name_DE = null;
                $estado = null;

                if(array_key_exists('name_PT',$data))   $name_PT = $data['name_PT'];
                if(array_key_exists('name_EN',$data))   $name_EN = $data['name_EN'];
                if(array_key_exists('name_FR',$data))   $name_FR = $data['name_FR'];
                if(array_key_exists('name_DE',$data))   $name_DE = $data['name_DE'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];


                /* BEGIN Save Paisagem */
                $resSavePaisagem = self::saveEditedPaisagem4Manager($paisagem_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado);
                /* END Save Paisagem */

                if (empty($resSavePaisagem)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_EVENTLOG_EDITED') . 'ref=' . $paisagem_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedPaisagem4Manager($paisagem_id, $name_PT, $name_EN, $name_FR, $name_DE, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editpaisagem4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($name_PT))  array_push($fields, 'name_PT="'.$db->escape($name_PT).'"');
            if(!is_null($name_EN))  array_push($fields, 'name_EN="'.$db->escape($name_EN).'"');
            if(!is_null($name_FR))  array_push($fields, 'name_FR="'.$db->escape($name_FR).'"');
            if(!is_null($name_DE))  array_push($fields, 'name_DE="'.$db->escape($name_DE).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Paisagem')
                ->set($fields)
                ->where(' id = '.$db->escape($paisagem_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateDificuldade4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdificuldade4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $dificuldade_id = $data['dificuldade_id'];
            if((int) $dificuldade_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDificuldade($db);
            $Table->load(array('id'=>$dificuldade_id));

            $db->transactionStart();

            try {

                $name = null;
                $estado = null;

                if(array_key_exists('name',$data))      $name = $data['name'];
                if(array_key_exists('estado',$data))    $estado = $data['estado'];


                /* BEGIN Save Dificuldade */
                $resSaveDificuldade = self::saveEditedDificuldade4Manager($dificuldade_id, $name, $estado);
                /* END Save Dificuldade */

                if (empty($resSaveDificuldade)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE_EVENTLOG_EDITED') . 'ref=' . $dificuldade_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedDificuldade4Manager($dificuldade_id, $name, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdificuldade4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($name))  array_push($fields, 'name='.$db->escape($name));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Dificuldade')
                ->set($fields)
                ->where(' id = '.$db->escape($dificuldade_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateDuracao4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editduracao4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $duracao_id = $data['duracao_id'];
            if((int) $duracao_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDuracao($db);
            $Table->load(array('id'=>$duracao_id));

            $db->transactionStart();

            try {

                $name = null;
                $estado = null;

                if(array_key_exists('duracao_min',$data))   $duracao_min = $data['duracao_min'];
                if(array_key_exists('duracao_max',$data))   $duracao_max = $data['duracao_max'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];


                /* BEGIN Save Duracao */
                $resSaveDuracao = self::saveEditedDuracao4Manager($duracao_id, $duracao_min, $duracao_max, $estado);
                /* END Save Duracao */

                if (empty($resSaveDuracao)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EVENTLOG_EDITED') . 'ref=' . $duracao_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedDuracao4Manager($duracao_id, $duracao_min, $duracao_max, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editduracao4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($duracao_min))  array_push($fields, 'duracao_min='.$db->escape($duracao_min));
            if(!is_null($duracao_max))  array_push($fields, 'duracao_max='.$db->escape($duracao_max));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Duracao')
                ->set($fields)
                ->where(' id = '.$db->escape($duracao_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateDistancia4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdistancia4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $distancia_id = $data['distancia_id'];
            if((int) $distancia_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableDistancia($db);
            $Table->load(array('id'=>$distancia_id));

            $db->transactionStart();

            try {

                $name = null;
                $estado = null;

                if(array_key_exists('distancia_min',$data))     $distancia_min = $data['distancia_min'];
                if(array_key_exists('distancia_max',$data))     $distancia_max = $data['distancia_max'];
                if(array_key_exists('estado',$data))            $estado = $data['estado'];


                /* BEGIN Save Distancia */
                $resSaveDistancia = self::saveEditedDistancia4Manager($distancia_id, $distancia_min, $distancia_max, $estado);
                /* END Save Distancia */

                if (empty($resSaveDistancia)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_EVENTLOG_EDITED') . 'ref=' . $distancia_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedDistancia4Manager($distancia_id, $distancia_min, $distancia_max, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdistancia4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($distancia_min))  array_push($fields, 'distancia_min='.$db->escape($distancia_min));
            if(!is_null($distancia_max))  array_push($fields, 'distancia_max='.$db->escape($distancia_max));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Distancia')
                ->set($fields)
                ->where(' id = '.$db->escape($distancia_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function updateAltitude4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editaltitude4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $altitude_id = $data['altitude_id'];
            if((int) $altitude_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAltitude($db);
            $Table->load(array('id'=>$altitude_id));

            $db->transactionStart();

            try {

                $name = null;
                $estado = null;

                if(array_key_exists('altitude_min',$data))  $altitude_min = $data['altitude_min'];
                if(array_key_exists('altitude_max',$data))  $altitude_max = $data['altitude_max'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];


                /* BEGIN Save Altitude */
                $resSaveAltitude = self::saveEditedAltitude4Manager($altitude_id, $altitude_min, $altitude_max, $estado);
                /* END Save Altitude */

                if (empty($resSaveAltitude)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('PercursosPedestres_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_EVENTLOG_EDITED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_EVENTLOG_EDITED') . 'ref=' . $altitude_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDE_EVENTLOG_EDITED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function saveEditedAltitude4Manager($altitude_id, $altitude_min, $altitude_max, $estado)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editaltitude4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($altitude_min))  array_push($fields, 'altitude_min='.$db->escape($altitude_min));
            if(!is_null($altitude_max))  array_push($fields, 'altitude_max='.$db->escape($altitude_max));
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_PercursosPedestres_Altitude')
                ->set($fields)
                ->where(' id = '.$db->escape($altitude_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }

        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.percursospedestres.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.percursospedestres.data', null);
            $app->setUserState('com_virtualdesk.edit4user.percursospedestres.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.percursospedestres.data', null);

        }

    }

?>