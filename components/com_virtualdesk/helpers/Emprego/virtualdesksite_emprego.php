<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');


    class VirtualDeskSiteEmpregoHelper
    {

        public static function getOfertasEmpregoDataTables($dbprefix, $detalhe){

            // Gerar Link para o detalhe...
            $dummyHRef =  JRoute::_($detalhe);

            $table  = " ( SELECT a.id, a.ref_externa as referencia, a.nome_oferta as nome , b.nome as concelho, c.freguesia as freguesia, d.opcao as experiencia, CONCAT('".$dummyHRef."', CAST(a.ref_externa as CHAR(10))) as dummy";
            $table .= " FROM ".$dbprefix."virtualdesk_Emprego_Ofertas as a ";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Emprego_Concelho AS b ON b.id = a.concelho ";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Emprego_Freguesia AS c  ON c.id_freguesia = a.freguesia ";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Emprego_yesNo AS d ON d.id = a.exp_anterior ";
            $table .= " ORDER BY a.ref_externa ASC ) temp ";

            return ($table);

        }


        public static function getDadosOferta($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('nome_oferta, b.freguesia as freguesia, c.habilitacoes as habilitacoes, d.formprofissional as formProfissional, e.opcao as expAnterior, tempo_minimo_exp, f.opcao as cartaConducao, tipo_carta_conducao, g.opcao as transp_proprio, tipo_contrato, h.regHorario as reg_horario, horas_dia, hora_inicio, hora_fim, descanso_semanal, remuneracao, subs_alimentacao, perfil_candidato'))
                ->join('LEFT', '#__virtualdesk_Emprego_Freguesia AS b ON b.id_freguesia = a.freguesia')
                ->join('LEFT', '#__virtualdesk_Emprego_Hab_Academicas AS c ON c.id = a.habAcademicas')
                ->join('LEFT', '#__virtualdesk_Emprego_Form_Profissional AS d ON d.id = a.form_profissional')
                ->join('LEFT', '#__virtualdesk_Emprego_yesNo AS e ON e.id = a.exp_anterior')
                ->join('LEFT', '#__virtualdesk_Emprego_yesNo AS f ON f.id = a.carta_conducao')
                ->join('LEFT', '#__virtualdesk_Emprego_yesNo AS g ON g.id = a.transp_proprio')
                ->join('LEFT', '#__virtualdesk_Emprego_RegHorario AS h ON h.id = a.reg_horario')
                ->from("#__virtualdesk_Emprego_Ofertas as a")
                ->where($db->quoteName('ref_externa') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getIdFreg($freguesia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_freguesia')
                ->from("#__virtualdesk_Emprego_Freguesia")
                ->where($db->quoteName('freguesia') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getIdCarta($cartaConducao){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Emprego_yesNo")
                ->where($db->quoteName('opcao') . "='" . $db->escape($cartaConducao) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getTipoCarta($tipoCarta){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('tipo')
                ->from("#__virtualdesk_Emprego_Tipo_Carta_Conducao")
                ->where($db->quoteName('id') . "='" . $db->escape($tipoCarta) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

    }

?>
