<?php
/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


class VirtualDeskSiteContactUsFieldsHelper
{
    var $arConfFieldNames = array();
    var $arContactUsFieldsConfig     = array();
    var $arEmptyFieldsError           = array();

    public function VirtualDeskSiteContactUsFieldsHelper(){

        $this->arConfFieldNames =  array(
            'type'=>'TYPE',
            'category'=>'CATEGORY',
            'subcategory'=>'SUBCATEGORY',
            'status'=>'STATUS',
            'ctlanguage'=>'CTLANGUAGE',
            'vdwebsitelist'=>'VDWEBSITELIST',
            'vdmenumain'=>'VDMENUMAIN',
            'vdmenusec'=>'VDMENUSEC',
            'vdmenusec2'=>'VDMENUSEC2',
            'areaact'=>'AREAACT',
            'obs'=>'OBS',
            'attachment'=>'ATTACHMENT'
        );

        $this->setFields();
    }


    public function setFields()
    {
        foreach($this->arConfFieldNames as $key => $val)
        {
            // Field : visible
            $this->arContactUsFieldsConfig['ContactUsField_' . $val]  = false;
            if(JComponentHelper::getParams('com_virtualdesk')->get('contactusfield_' . $key) == 1 ) $this->arContactUsFieldsConfig['ContactUsField_' . $val] = true;

            // Field: obrigatório
            $this->arContactUsFieldsConfig['ContactUsField_' . $val. '_Required']    = "";
            if(JComponentHelper::getParams('com_virtualdesk')->get('contactusfield_'.$key.'_required') == 1 ) $this->arContactUsFieldsConfig['ContactUsField_' . $val. '_Required'] = 'required';
        }

    }


    public function getFields()
    {
        return $this->arContactUsFieldsConfig;
    }


    /*
    * Verifica se os campos recebidos se estão vazios e nesse caso não inicializa o array de dados.
    * Ou se são obrigatórios nesse caso não podem vir vazios.
    *
    */
    public function checkCamposEmpty($arCamposGet, $data)
    {
        foreach($this->arConfFieldNames as $key => $val)
        {
            if ($this->arContactUsFieldsConfig['ContactUsField_' . $val]) {
                if ($this->arContactUsFieldsConfig['ContactUsField_' . $val . '_Required']) {
                    if (!empty($arCamposGet[$key])) {
                        // não está vazio
                        $data[$key] = $arCamposGet[$key];
                    }
                    else {
                        // é obrigatório e está vazio... Marca como estando com erro
                        $this->arEmptyFieldsError[$key] = true;
                    }
                } else { // Se não é obrigatório pode ir vazio
                    $data[$key] = $arCamposGet[$key];
                }
            }
        }

        return($data);
    }


    public function checkRequiredEmptyFields()
    {
        $msg ="";

        foreach($this->arConfFieldNames as $key => $val) {
            if( $this->arEmptyFieldsError[$key] )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_CONTACTUS_' . $val . '_LABEL');
        }

        return($msg);
    }


    /*
    * Carrega lista de TODAS áreas de atuação para serem apresentadas
    */
    public static function getAreaActListAllByGroup ($lang)
    {
        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id as id','a.idgroup as idgroup','a.name_pt as name_pt','a.name_en as name_en','b.groupname_pt as groupname_pt','b.groupname_en as groupname_en') )
                ->join('LEFT', '#__virtualdesk_areaact_group AS b ON b.id = a.idgroup')
                ->from("#__virtualdesk_areaact as a")
                ->order('a.idgroup')
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                // Add Area Act Group
                $rowGroupName   = '';
                $rowAreaActName = '';

                if (empty($response[$row->idgroup])) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowGroupName = $row->groupname_pt;
                    }
                    else {
                        $rowGroupName = $row->groupname_en;
                    }
                    $response[$row->idgroup]['group'] = array(
                        'idgroup' => $row->idgroup,
                        'groupname' => $rowGroupName,
                    );
                }

                // Add Area Act
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowAreaActName = $row->name_pt;
                }
                else {
                    $rowAreaActName = $row->name_en;
                }

                $response[$row->idgroup]['rows'][] =  array (
                    'id' => $row->id,
                    'name' => $rowAreaActName
                );
            }

            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Carrega lista das áreas de atuação associadas a um registo
    */
    public static function getAreaActListSelected ($UserJoomlaID, $IdContactUs)
    {
        if( empty($UserJoomlaID) )  return false;
        if( empty($IdContactUs) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','idcontactus','idareaact') )
                ->from("#__virtualdesk_contactus_areaact")
                ->where($db->quoteName('idcontactus') . '=' . $db->escape($IdContactUs))
            );
            $dataReturn = $db->loadAssocList();
            $dataList = array();
            foreach( $dataReturn as $keyList )
            {
                $dataList[] = $keyList['idareaact'];
            }

            return($dataList);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


   /*
   * Carrega lista das áreas de atuação associadas a uma empresa
   */
    public static function getAreaActListSelectName ($data, $lang)
    {
        if( empty($data) )  return false;
        if( empty($data->id) )  return false;

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idcontactus','a.idareaact','b.name_pt as name_pt','b.name_en as name_en') )
                ->join('LEFT', '#__virtualdesk_areaact AS b ON b.id = a.idareaact')
                ->from("#__virtualdesk_contactus_areaact as a")
                ->where($db->quoteName('idcontactus') . '=' . $db->escape($data->id))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                // Add Area Act Group
                $rowAreaActName = '';
                // Add Area Act
                if( empty($lang) or ($lang='pt_PT') ) {
                    $response[] = $row->name_pt;
                }
                else {
                    $response[] = $row->name_en;
                }
            }

            return($response);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getMenuMainList()
    {
        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_contactus_menumain")
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getMenuMainByWebSiteList($idwebsitelist)
    {
        if(empty($idwebsitelist)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_contactus_menumain")
            ->where($db->quoteName('idwebsitelist').'='.$db->escape($idwebsitelist))
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getMenuSecByMenuMainList($idmenumain)
    {
        if(empty($idmenumain)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_contactus_menusec")
            ->where($db->quoteName('idmenumain').'='.$db->escape($idmenumain))
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getMenuSec2ByMenuMainList($idmenusec)
    {
        if(empty($idmenusec)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_contactus_menusec2")
            ->where($db->quoteName('idmenusec').'='.$db->escape($idmenusec))
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getMenuMainById($idmenumain)
    {
        if(empty($idmenumain)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('name_pt as name'))
            ->from("#__virtualdesk_contactus_menumain")
            ->where($db->quoteName('id').'='.$db->escape($idmenumain))
            ->order('name ASC' )
        );
        $data = $db->loadResult();
        return($data);
    }


    public static function getMenuSecById($idmenusec)
    {
        if(empty($idmenusec)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('name_pt as name'))
            ->from("#__virtualdesk_contactus_menusec")
            ->where($db->quoteName('id').'='.$db->escape($idmenusec))
            ->order('name ASC' )
        );
        $data = $db->loadResult();
        return($data);
    }


    public static function getMenuSec2ById($idmenusec2)
    {
        if(empty($idmenusec2)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('name_pt as name'))
            ->from("#__virtualdesk_contactus_menusec2")
            ->where($db->quoteName('id').'='.$db->escape($idmenusec2))
            ->order('name ASC' )
        );
        $data = $db->loadResult();
        return($data);
    }

}