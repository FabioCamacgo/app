<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskTableRgpd', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/rgpd.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');


    class VirtualDeskSiteRgpdHelper
    {
        const tagchaveModulo   = 'rgpd';

        const ModuloTagAlerta  = 'alerta';
        const ModuloTagCultura = 'cultura';
        const ModuloTagProfile = 'profile'; // Dados de Utilizador



        /* Carrega lista de TODAS as rgpds para acesso aos MANAGERS */
        public static function getRgpdListLogs4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('rgpd', 'listlogs4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('rgpd','listlogs4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;


            try
            {
                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host     = $conf->get('host');
                    $user     = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    //$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');
                    //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT(\'".$dummyHRef."\', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";

                    $table  = " ( SELECT a.id as id, a.title, a.created as created, a.ip as ip";
                    $table .= " , a.ref_id as ref_id, a.ref as ref, '' as dummy, a.descricao as descricao ";
                    $table .= " , d.email as uemail, d.login as ulogin, d.name as unome , DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull";
                    $table .= " , e.name as tipo_op, f.nome as modulo, g.nome as plugin, CONCAT( IFNULL(f.nome,'') ,' ', IFNULL(g.nome,'')) as modulo_plugin ";
                    $table .= " , f.tagchave as modulotagchave, g.tagchave as plugintagchave, a.id_tipo_op as id_tipo_op";
                    $table .= " FROM ".$dbprefix."virtualdesk_eventlog_rgpd as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON a.sessionuserid=d.idjos ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_eventlog_tipo_op AS e ON a.id_tipo_op=e.id ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_modulo AS f ON a.idmodulo=f.id ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_config_plugin AS g ON a.idplugin=g.id ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'createdFull',    'dt' => 0 ),
                        array( 'db' => 'ulogin',         'dt' => 1 ),
                        array( 'db' => 'unome',          'dt' => 2 ),
                        array( 'db' => 'tipo_op',        'dt' => 3 ),
                        array( 'db' => 'ref_id',         'dt' => 4 ),
                        array( 'db' => 'ref',            'dt' => 5 ),
                        array( 'db' => 'title',          'dt' => 6 ),
                        array( 'db' => 'descricao',      'dt' => 7 ),
                        array( 'db' => 'modulo_plugin',  'dt' => 8 ),
                        array( 'db' => 'ip',             'dt' => 9 ),
                        array( 'db' => 'dummy',          'dt' => 10 ),

                        array( 'db' => 'id',             'dt' => 11, 'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'plugintagchave', 'dt' => 12 ),
                        array( 'db' => 'modulotagchave', 'dt' => 13),
                        array( 'db' => 'id_tipo_op',     'dt' => 14 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                $dataReturn = array();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista de DADOS PESSOAIS existentes na APP para acesso aos MANAGERS */
        public static function getRgpdListDados4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('rgpd', 'listdados4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('rgpd','listdados4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;


            try
            {
                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host     = $conf->get('host');
                    $user     = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    $vdlog = new VirtualDeskLogHelper();

                    // Tags e Nome de Módulos - implementado assim para facilitar
                    $ModuloTagAlerta    = self::ModuloTagAlerta;
                    $ModuloTagCultura   =  self::ModuloTagCultura;
                    $ModuloTagProfile   =  self::ModuloTagProfile;

                    $ModuloNomeAlerta   = $vdlog->getModuleNameByTag($ModuloTagAlerta);
                    $ModuloNomeCultura  = $vdlog->getModuleNameByTag($ModuloTagCultura);
                    $ModuloNomeProfile  = $vdlog->getModuleNameByTag($ModuloTagProfile);

                    $table  = " (  ";
                    $table .= " SELECT  id, idAll, ref_id, ref, tagmodulo, nomemodulo, dadosrgpd FROM ( ";


                    // Dados RGPD no ALERTA
                    $table .= " SELECT a.Id_alerta as id, CONCAT('ALRT',CONVERT(a.Id_alerta , CHAR(50))) as idAll ,  a.Id_alerta as ref_id, a.codOco as ref ";
                    $table .= " , '".$ModuloTagAlerta."' as tagmodulo "." , '".$ModuloNomeAlerta."' as nomemodulo ";
                    $table .= " , CONCAT('utilizador: ', IFNULL(utilizador,'') , ' | nif: ',  IFNULL(nif,''), ' | email: ', IFNULL(email,''), ' | tel: ', IFNULL(telefone,'')) as dadosrgpd ";
                    $table .= " FROM " . $dbprefix . "virtualdesk_alerta as a ";

                    $table .= " UNION ";

                    // Dados RGPD na CULTURA (agenda municipio)
                    $table .= " SELECT a.id as id, CONCAT('CLTU',CONVERT(a.id , CHAR(50))) as idAll , a.id as ref_id , ''  as ref ";
                    $table .= " , '".$ModuloTagCultura."' as tagmodulo "." , '".$ModuloNomeCultura."' as nomemodulo ";
                    $table .= " , CONCAT('nome: ', IFNULL(nome,''), ' | nif: ', IFNULL(nif,''), ' | email: ', IFNULL(email,'') , ' | tel: ', IFNULL(telefone,''), ' | morada: ', IFNULL(morada,'')) as dadosrgpd ";
                    $table .= " FROM ".$dbprefix."virtualdesk_Cultura_Users as a ";

                    $table .= " UNION ";

                    // Dados RGPD nos Users/Profile
                    $userfield_login_type = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
                    $CampoNIF = 'fiscalid';
                    if($userfield_login_type=='login_as_nif') $CampoNIF = 'login';

                    $table .= " SELECT a.id as id, CONCAT('PRFU',CONVERT(a.id , CHAR(50))) as idAll , a.id as ref_id , a.idjos as ref ";
                    $table .= " , '".$ModuloTagProfile."' as tagmodulo "." , '".$ModuloNomeProfile."' as nomemodulo ";
                    $table .= " , CONCAT('name: ', IFNULL(name,'')    ";
                    $table .= ", ' | nif: '     , IFNULL(".$CampoNIF.",'') ";
                    $table .= ", ' | civilid: ' , IFNULL(civilid,'') ";
                    $table .= ", ' | email: '   , IFNULL(email,'') ";
                    $table .= ", ' | phone1: '  , IFNULL(phone1,'') ";
                    $table .= ", ' | phone2: '  , IFNULL(phone2,'') ";
                    $table .= ", ' | address: ' , IFNULL(address,'') ";
                    $table .= " ) as dadosrgpd ";
                    $table .= " FROM ".$dbprefix."virtualdesk_users as a ";


                    $table .= " ) as allUnion ";

                    $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                    if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                    $table .= $setLimitSQL;
                    $table .= " ) temp ";

                    $primaryKey = 'idAll';

                    $columns = array(
                        array( 'db' => 'idAll',         'dt' => 0 ),
                        array( 'db' => 'ref_id',        'dt' => 1 ),
                        array( 'db' => 'ref',           'dt' => 2 ),
                        array( 'db' => 'nomemodulo',    'dt' => 3 ),
                        array( 'db' => 'tagmodulo',     'dt' => 4 ),
                        array( 'db' => 'dadosrgpd',     'dt' => 5 ),
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                $dataReturn = array();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega detalhe da tarefa   */
        public static function getRgpdLogDetalhe4Manager ($IdRgpd)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('rgpd', 'listlogs4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('rgpd','listlogs4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( (int)$IdRgpd <=0 )  return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if( (int)$UserJoomlaID<=0 ) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id', 'a.title as title', 'a.descricao as descricao', 'a.ref_id as ref_id', 'a.ref as ref', 'a.referer as referer', 'a.request as request'
                        , 'd.email as uemail', 'd.login as ulogin', 'd.name as unome' , 'DATE_FORMAT(a.created, "%Y-%m-%d %H:%i:%s") as createdFull'
                        , 'e.name as tipo_op', 'f.nome as modulo', 'g.nome as plugin' , 'a.ip as ip'  ) )
                    ->join('LEFT', '#__virtualdesk_users AS d ON a.sessionuserid=d.idjos')
                    ->join('LEFT', '#__virtualdesk_eventlog_tipo_op AS e ON a.id_tipo_op=e.id ')
                    ->join('LEFT', '#__virtualdesk_perm_modulo AS f ON a.idmodulo=f.id')
                    ->join('LEFT', '#__virtualdesk_config_plugin AS g ON a.idplugin=g.id')
                    ->from("#__virtualdesk_eventlog_rgpd as a")
                    ->where( 'a.id='.$db->escape($IdRgpd) )

                );
                $dataReturn = $db->loadObject();

                // Se estiver definida a encriptação, vai encriptar os valores do array
                $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
                $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
                $dataReturn->id             = $obVDCrypt->setIdInputValueEncrypt($dataReturn->id, $setencrypt_forminputhidden);

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }





    }
?>