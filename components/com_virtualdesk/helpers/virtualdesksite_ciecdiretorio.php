<?php

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteciecdiretorioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio_files.php');
JLoader::register('VirtualDeskTableCiecDiretorio', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/ciecDiretorio.php');
JLoader::register('VirtualDeskTableCiecDiretorioEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/ciecDiretorio_estado_historico.php');
JLoader::register('VirtualDeskSiteFormularioHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/Formulario.php');
JLoader::register('VirtualDeskSiteFormularioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/Formulario_files.php');


class VirtualDeskSiteciecdiretorioHelper
{
    const tagchaveModulo = 'ciecdiretorio';


    public static function getTipologiaName($tipologia){
        $nameTipo = '';
        $pieces = explode(",", $tipologia);
        $db = JFactory::getDBO();

        for($i=0; $i<count($pieces); $i++){
            if($i==0){
                $db->setQuery($db->getQuery(true)
                    ->select('tipologia')
                    ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                    ->where($db->quoteName('id') . "='" . $db->escape($pieces[$i]) . "'")
                );
                $data = $db->loadResult();
                $nameTipo = $data;
            } else {
                $db->setQuery($db->getQuery(true)
                    ->select('tipologia')
                    ->from("#__virtualdesk_EuropaCriativa_Tipologia")
                    ->where($db->quoteName('id') . "='" . $db->escape($pieces[$i]) . "'")
                );
                $data = $db->loadResult();
                $nameTipo = $nameTipo . ', ' . $data;
            }
        }

        return ($nameTipo);
    }


    public static function RefEvent($fiscalid, $startDay, $startMonth, $endDay, $endMonth)
    {

        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);

        $refer = 'N' . $fiscalid . 'SD' . $startDay . 'SM' . $startMonth . $randomString . 'ED' . $endDay . 'EM' . $endMonth;

        return $refer;
    }


    public static function checkValidarNIF($nif) {

        $nif=trim($nif);
        $ignoreFirst=true;
        $errNif = 0;

        if (!is_numeric($nif) || strlen($nif)!=9) {
            $errNif = 1;
        } else {
            $nifSplit=str_split($nif);
            if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
                $checkDigit=0;
                for($i=0; $i<8; $i++) {
                    $checkDigit+=$nifSplit[$i]*(10-$i-1);
                }
                $checkDigit=11-($checkDigit % 11);

                if($checkDigit>=10) $checkDigit=0;

                if ($checkDigit==$nifSplit[8]) {

                } else {
                    $errNif = 1;
                }
            } else {
                $errNif = 1;
            }
        }

        return($errNif);

    }


    public static function getUserEmail($fiscalid)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('email')
            ->from("#__virtualdesk_ciecdiretorio_Users")
            ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getUserName($fiscalid)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nome')
            ->from("#__virtualdesk_ciecdiretorio_Users")
            ->where($db->quoteName('nif') . "='" . $db->escape($fiscalid) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getListaParceiros($referencia)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.parceiro as parceiro, b.pais as pais'))
            ->join('LEFT', '#__virtualdesk_EuropaCriativa_paises AS b ON b.id = a.pais')
            ->from("#__virtualdesk_EuropaCriativa_Parceiros as a")
            ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function SendMailAdmin($Assunto, $nameUser, $email, $referencia='')
    {

        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ciecdiretorio_NovoAdmin_Email.html');

        $data['fromname'] = $config->get('fromname');
        // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE     = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOADMIN_TITULO');
        $BODY_TITLE2    = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOADMIN_TITULO');
        $BODY_GREETING  = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOADMIN_CORPO', $Assunto, $nameUser . " ( " . $email . " ) " );
        $BODY_FILES_LIST_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_FILES_LIST_TITLE');


        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
        $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
        $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
        $body = str_replace("%BODY_FILES_LIST_TITLE",$BODY_FILES_LIST_TITLE, $body);

        // Lista de ficheiros para downlaod
        if(empty($referencia)===false) {
            $objEventFile = new VirtualDeskSiteciecdiretorioFilesHelper();
            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile) {
                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
            }
            $body = str_replace("%BODY_FILES_LIST", $FileList21Html, $body);
        }


        // Send the password reset request email.
        $newEmail = JFactory::getMailer();
        $newEmail->Encoding = 'base64';
        $newEmail->isHtml(true);
        $newEmail->setBody($body);
        $newEmail->addReplyTo($data['mailfrom']);
        $newEmail->setSender($data['mailfrom']);
        $newEmail->setFrom($data['fromname']);

        // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
        $emailManager  = '';
        $emailAdmin    = '';
        $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
        if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
        if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

        $newEmail->setSubject($data['sitename']);
        $newEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/ciecdiretorio_BannerEmail.png', "banner", "ciecdiretorio__BannerEmail.png");


        $return = (boolean)$newEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;

    }


    public static function SendMailAdminUpdate($Assunto, $nameUser, $email, $referencia='')
    {

        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ciecdiretorio_NovoAdmin_Email.html');

        $data['fromname'] = $config->get('fromname');
        // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATEADMIN_TITULO');
        $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATEADMIN_TITULO');
        $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATEADMIN_CORPO', $Assunto, $nameUser . " ( " . $email . " ) " );
        $BODY_FILES_LIST_TITLE       = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_FILES_LIST_TITLE');


        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
        $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
        $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
        $body = str_replace("%BODY_FILES_LIST_TITLE",$BODY_FILES_LIST_TITLE, $body);

        // Lista de ficheiros para downlaod
        if(empty($referencia)===false) {
            $objEventFile = new VirtualDeskSiteciecdiretorioFilesHelper();
            $arFileList = $objEventFile->getFileGuestLinkByRefId($referencia);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile) {
                $FileList21Html .= "<a href='" . JRoute::_($rowFile->guestlink, false) . "' target='_blank'>" . $rowFile->desc . "</a><br>\n\r";
            }
            $body = str_replace("%BODY_FILES_LIST", $FileList21Html, $body);
        }


        // Send the password reset request email.
        $newEmail = JFactory::getMailer();
        $newEmail->Encoding = 'base64';
        $newEmail->isHtml(true);
        $newEmail->setBody($body);
        $newEmail->addReplyTo($data['mailfrom']);
        $newEmail->setSender($data['mailfrom']);
        $newEmail->setFrom($data['fromname']);

        // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
        $emailManager  = '';
        $emailAdmin    = '';
        $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
        if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
        if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

        $newEmail->setSubject($data['sitename']);
        $newEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/ciecdiretorio_BannerEmail.png', "banner", "ciecdiretorio_BannerEmail.png");


        $return = (boolean)$newEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;

    }


    public static function SendMailAdminAltEstado($Assunto, $nameUser, $email, $referencia='',$Estado)
    {

        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ciecdiretorio_NovoAdmin_Email.html');

        $data['fromname'] = $config->get('fromname');
        // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_ALTESTADOADMIN_TITULO');
        $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_ALTESTADOADMIN_TITULO');
        $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_ALTESTADOADMIN_CORPO', $Estado, $Assunto, $referencia);

        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
        $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
        $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
        $body = str_replace("%BODY_FILES_LIST_TITLE", '', $body);
        $body = str_replace("%BODY_FILES_LIST", '', $body);

        // Send the password reset request email.
        $newEmail = JFactory::getMailer();
        $newEmail->Encoding = 'base64';
        $newEmail->isHtml(true);
        $newEmail->setBody($body);
        $newEmail->addReplyTo($data['mailfrom']);
        $newEmail->setSender($data['mailfrom']);
        $newEmail->setFrom($data['fromname']);

        // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
        $emailManager  = '';
        $emailAdmin    = '';
        $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
        if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
        if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

        $newEmail->setSubject($data['sitename']);
        $newEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/ciecdiretorio_BannerEmail.png', "banner", "ciecdiretorio_BannerEmail.png");


        $return = (boolean)$newEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;

    }


    public static function SendMailClient($nameUser, $Assunto, $descricao, $emailUser, $Departamento)
    {
        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ciecdiretorio_NovoClient_Email.html');

        $data['fromname'] = $config->get('fromname');
        // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOCLIENT_TITULO');
        $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOCLIENT_TITULO');
        $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOCLIENT_INTRO', $nameUser . " ( " . $emailUser . " ) ");

        $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOCLIENT_ASSUNTO');
        $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
        $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOCLIENT_DESCRICAO');
        $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
        $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_NOVOCLIENT_DEPARTAMENTO');
        $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);


        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
        $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
        $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
        $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
        $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
        $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
        $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
        $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
        $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);


        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender($data['mailfrom']);
        $newActivationEmail->setFrom($data['fromname']);
        $newActivationEmail->addRecipient($emailUser);
        $newActivationEmail->setSubject($data['sitename']);
        $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/ciecdiretorio_BannerEmail.png', "banner", "ciecdiretorio_BannerEmail.png");


        $return = (boolean)$newActivationEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;

    }


    public static function SendMailClientUpdate($nameUser, $Assunto, $descricao, $emailUser, $Departamento)
    {
        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ciecdiretorio_NovoClient_Email.html');

        $data['fromname'] = $config->get('fromname');
        // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');


        $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATECLIENT_TITULO');
        $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATECLIENT_TITULO');
        $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATECLIENT_INTRO', $nameUser . " ( " . $emailUser . " ) ");

        $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATECLIENT_ASSUNTO');
        $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
        $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATECLIENT_DESCRICAO');
        $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
        $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_UPDATECLIENT_DEPArTAMENTO');
        $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);


        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
        $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
        $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
        $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
        $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
        $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
        $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
        $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
        $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);



        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender($data['mailfrom']);
        $newActivationEmail->setFrom($data['fromname']);
        $newActivationEmail->addRecipient($emailUser);
        $newActivationEmail->setSubject($data['sitename']);
        $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/ciecdiretorio_BannerEmail.png', "banner", "ciecdiretorio_BannerEmail.png");


        $return = (boolean)$newActivationEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;

    }


    public static function SendMailClientAltEstado($nameUser, $Assunto, $descricao, $emailUser, $Departamento, $Estado, $referencia)
    {
        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ciecdiretorio_NovoClient_Email.html');

        $data['fromname'] = $config->get('fromname');
        // $data['mailfrom'] = $email;  -- Retirei porque o mail from tem de ser sempre do site...
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');


        $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_ALTESTADOCLIENT_TITULO');
        $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_ALTESTADOCLIENT_TITULO');
        $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_ciecdiretorio_EMAIL_ALTESTADOCLIENT_CORPO', $Estado, $Assunto, $referencia);

        $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
        $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
        $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);




        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender($data['mailfrom']);
        $newActivationEmail->setFrom($data['fromname']);
        $newActivationEmail->addRecipient($emailUser);
        $newActivationEmail->setSubject($data['sitename']);
        $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/ciecdiretorio_BannerEmail.png', "banner", "ciecdiretorio_BannerEmail.png");


        $return = (boolean)$newActivationEmail->send();

        // Check for an error.
        if ($return !== true) {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }

        return $return;

    }


    /* Define qual o  email por defeito do manager, se não existir devolve o do Admin */
    public static function getNameAndEmailDoManagerEAdmin(&$emailManager, &$emailAdmin)
    {
        $config                      = JFactory::getConfig();
        $objVDParams                 = new VirtualDeskSiteParamsHelper();
        $MailManagerbyDefault  = $objVDParams->getParamsByTag('ciecdiretorio_Mail_Manager_by_Default');
        $MailAdminbyDefault    = $objVDParams->getParamsByTag('mailAdminciecdiretorio');
        $adminSiteDefaultEmail       = $config->get('mailfrom');

        // Verifica se tem email do mananger DE ciecdiretorio caso contrário fica o admin do site
        $emailManager = $MailManagerbyDefault;
        if(empty($emailManager)) {
            $emailManager = $MailAdminbyDefault;
            if(empty($emailManager)) {
                $emailManager =  $adminSiteDefaultEmail;
            }
        }

        // Verifica se tem email do admin do alerta caso contrário fica o admin do site
        $emailAdmin = $MailAdminbyDefault;
        if(empty($emailAdmin)) {
            $emailAdmin = $adminSiteDefaultEmail;
        }
        return(true);
    }


    /* Carrega lista com dados de todos os eventos (acesso ao USER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getciecdiretorioList4User ($vbForDataTables=false, $setLimit=-1)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'list4users');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

        if((int)$UserSessionNIF<=0) return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio&layout=view4user&ciecdiretorio_id=');

                $table  = " ( SELECT a.id as id, a.id as ciecdiretorio_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo, a.assunto as assunto, a.descricao as descricao , a.observacoes as observacoes";
                $table .= " , IFNULL(e.".$EstadoName.", ' ') as estado, a.id_estado as idestado , a.id_departamento as iddepartamento";
                $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                $table .= " FROM ".$dbprefix."virtualdesk_ciecdiretorio as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_ciecdiretorio_estado AS e ON e.id = a.id_estado ";
                $table .= " WHERE (a.nif=$UserSessionNIF) ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'created',       'dt' => 0 ),
                    array( 'db' => 'codigo',        'dt' => 1 ),
                    array( 'db' => 'assunto',       'dt' => 2 ),
                    array( 'db' => 'departamento',  'dt' => 3 ),
                    array( 'db' => 'estado',        'dt' => 4 ),
                    array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',      'dt' => 6 ),
                    array( 'db' => 'id',            'dt' => 7 ,'formatter'=>'VALUE_ENCRYPT'),
                    array( 'db' => 'modified',      'dt' => 8 ),
                    array( 'db' => 'createdFull',   'dt' => 9 ),
                    array( 'db' => 'descricao',     'dt' => 10 ),
                    array( 'db' => 'iddepartamento','dt' => 11 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();


            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega lista com dados de todos os eventos (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getciecdiretorioList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'list4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('ciecdiretorio','list4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();


            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio&layout=view4manager&ciecdiretorio_id=');

                $table  = " ( SELECT a.id as id, a.id as ciecdiretorio_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, 
                a.referencia as referencia, a.projeto as projeto, e.programa as programa, a.programa as idprog, d.ano as anoApoio, a.estado as idestado, b.estado_PT as estado, 
                c.linha as linhaFinanciamento, a.valorFinanciamento as valorFinanciamento, a.data_Criacao as dataCriacao";
                $table .= " FROM ".$dbprefix."virtualdesk_EuropaCriativa_ProjetosApoiados as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_EuropaCriativa_estado AS b ON b.id = a.estado ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_EuropaCriativa_linhasFinanciamento AS c ON c.id = a.linha_Financiamento ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_EuropaCriativa_anos AS d ON d.id = a.ano_Apoio ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_EuropaCriativa_programa AS e ON e.id = a.programa";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'dataCriacao',       'dt' => 0 ),
                    array( 'db' => 'programa',        'dt' => 1 ),
                    array( 'db' => 'projeto',       'dt' => 2 ),
                    array( 'db' => 'linhaFinanciamento',  'dt' => 3 ),
                    array( 'db' => 'valorFinanciamento',      'dt' => 4 ),
                    array( 'db' => 'estado',        'dt' => 5 ),
                    array( 'db' => 'dummy',         'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',        'dt' => 7 ),
                    array( 'db' => 'idprog',        'dt' => 8 ),
                    array( 'db' => 'ciecdiretorio_id',        'dt' => 9 ,'formatter'=>'VALUE_ENCRYPT'),
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();


            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega lista com os TODOS os estados */
    public static function getciecdiretorioEstadoAllOptions ($lang)
    {
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('ciecdiretorio');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id as id','estado_PT as name_pt','estado_EN as name_en') )
                ->from("#__virtualdesk_EuropaCriativa_estado")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }
                $response[] = array(
                    'id' => $row->id,
                    'name' => $rowName
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getProgramaAllOptions(){


        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.id, a.programa as programa'))
            ->from("#__virtualdesk_EuropaCriativa_programa as a")
            ->order('programa ASC')
        );
        $dataReturn = $db->loadObjectList();
        $response = array();
        foreach ($dataReturn as $row) {
            $rowName = $row->programa;
            $response[] = array(
                'id' => $row->id,
                'name' => $rowName
            );

        }
        return ($response);
    }


    /* Devolve CSS a ser aplicado de acordo com o ESTADO atual */
    public static function getciecdiretorioEstadoCSS ($idestado)
    {

        $defCss = 'label-default';
        switch ($idestado)
        {   case '1':
            // pendente
            $defCss = 'despublicado';
            break;
            case '2':
                // concluído
                $defCss = 'publicado';
                break;
        }
        return ($defCss);
    }


    /* Carrega dados visualização do ciecdiretorio para o MANAGER */
    public static function getciecdiretorioView4ManagerDetail ($Idciecdiretorio)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('ciecdiretorio');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'view4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('ciecdiretorio'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('ciecdiretorio','view4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($Idciecdiretorio) )  return false;

        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int)$UserJoomlaID<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as ciecdiretorio_id', 'a.referencia as referencia', 'a.escala as id_escala', 'h.nome as escala', 'a.projeto', 'a.designacao', 'a.programa as idPrograma', 'c.programa as programa', 'a.entidade', 'b.estado_PT as estadoProjeto', 'a.estado as idestado', 'a.lider' , 'a.pais_lider as idPaisLider', 'd.pais as pais_lider' , 'a.ano_Apoio as idAnoApoio', 'e.ano as ano_Apoio' , 'a.linha_Financiamento as idLinha', 'f.linha as linha_Financiamento', 'a.tipologia', 'g.categoria as categoria', 'a.link', 'a.data_Inicio', 'a.data_Fim', 'a.call', 'a.valorFinanciamento', 'a.layout as idLayout', 'i.layout as layout', 'a.data_Criacao', 'a.data_Alteracao'))
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_estado AS b ON b.id = a.estado')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_programa AS c ON c.id = a.programa')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_paises AS d ON d.id = a.pais_lider')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_anos AS e ON e.id = a.ano_Apoio')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_linhasFinanciamento AS f ON f.id = a.linha_Financiamento')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_categorias AS g ON g.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_Escala AS h ON h.id = a.escala')
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_TipoLayout AS i ON i.id = a.layout')
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($Idciecdiretorio)  )
            );
            $dataReturn = $db->loadObject();


            // CARREGA DADOS DA AREA ACTUAÇÃO - multiple choice
            // comparar dados serializados com explode/implode com dados carregados têm de ser iguais
            $ComparaSerializeTipologia = VirtualDeskSiteciecdiretorioHelper::getTipologiaListSelected ($dataReturn->ciecdiretorio_id);
            asort($ComparaSerializeTipologia);
            $ComparaSerializeTipologia = implode('|',$ComparaSerializeTipologia);
            if($ComparaSerializeTipologia!=  $dataReturn->tipologia )  return false;
            $FieldSerializaTipologia   = explode('|',$dataReturn->tipologia);
            if (!empty($dataReturn->tipologia)) $dataReturn->tipologia = $FieldSerializaTipologia;


            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega dados do Promotor do Evento: pesquisa nos USERS VD e carrega o Nome e o EMail se não existir vai aos USERS da ciecdiretorio */
    public static function getciecdiretorioPromotorByNIF4Manager ($nif_evento)
    {
        if((int) $nif_evento<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('ciecdiretorio');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'view4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('ciecdiretorio'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('ciecdiretorio','view4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $dataPromotor = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($nif_evento);

            if( (!empty($dataPromotor)) && ($dataPromotor!=false) )
            {
                $dataReturn = new stdClass();
                $dataReturn->nome  = $dataPromotor->nome;
                $dataReturn->nif   = $dataPromotor->nif;
                $dataReturn->email = $dataPromotor->email;
                return($dataReturn);
            }
            else {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('nif, nome, email'))
                    ->from("#__virtualdesk_ciecdiretorio_Users")
                    ->where($db->quoteName('nif') . "=" . $db->escape($nif_evento) )
                );
                $dataReturn = $db->loadObject();
                if(empty($dataReturn)) return false;
                return($dataReturn);
            }
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Carrega dados (objecto) do ESTADO atual do processo do ciecdiretorio em questão */
    public static function getciecdiretorioEstadoAtualObjectByIdciecdiretorio ($lang, $id_ciecdiretorio)
    {
        if((int) $id_ciecdiretorio<=0) return false;

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('ciecdiretorio');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('b.id as id_estado','b.estado_PT as estado_PT','b.estado_EN as estado_EN') )
                ->join('LEFT', '#__virtualdesk_EuropaCriativa_estado AS b ON b.id = a.estado')
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados as a")
                ->where(" a.id = ". $db->escape($id_ciecdiretorio))
            );
            $dataReturn = $db->loadObject();

            if(empty($dataReturn)) return false;

            $obj= new stdClass();
            $obj->id_estado =  $dataReturn->id_estado;
            if( empty($lang) or ($lang='pt_PT') ) {
                $obj->name = $dataReturn->estado_PT;
            }
            else {
                $obj->name  = $dataReturn->estado_EN;
            }

            $obj->cssClass = self::getciecdiretorioEstadoCSS($dataReturn->id_estado);

            return($obj);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Grava dados para definir um novo ESTADO do processo de ciecdiretorio  */
    public static function saveAlterar2NewEstado4ManagerByAjax($getInputciecdiretorio_id, $NewEstadoId, $NewEstadoDesc)
    {
        $config = JFactory::getConfig();
        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('ciecdiretorio');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($NewEstadoId) ) return false;
        if((int) $getInputciecdiretorio_id <=0 ) return false;

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
        if((int) $UserJoomlaID <=0 ) return false;
        $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($objUserVD === false) return false;
        $UserVDId = $objUserVD->id;
        if ((int) $UserVDId <=0) return false;

        $data = array();
        $data['estado'] = $NewEstadoId;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();

        $Table = new VirtualDeskTableCiecDiretorio($db);
        $Table->load(array('id'=>$getInputciecdiretorio_id));

        // Só alterar se o estado for diferente
        if((int)$Table->estado == (int)$NewEstadoId )  return true;

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['data_Alteracao'] = $dateModified->format('Y-m-d H:i:s');
        }

        $db->transactionStart();

        try {
            //$data['id_evento'] = $Table->id_evento;
            // Store the data.
            if (!$Table->save($data)) {
                $db->transactionRollback();
                return false;
            }

            // Envia para o histório
            $TableHist  = new VirtualDeskTableCiecDiretorioEstadoHistorico($db);
            $dataHist = array();
            $dataHist['id_estado']  = $NewEstadoId;
            $dataHist['id_ticket']  = $getInputciecdiretorio_id;
            $dataHist['descricao']  = $NewEstadoDesc;
            $dataHist['createdby']  = $UserJoomlaID;
            $dataHist['modifiedby'] = $UserJoomlaID;

            // Store the data.
            if (!$TableHist->save($dataHist)) {
                $db->transactionRollback();
                return false;
            }


            $db->transactionCommit();

            $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('ciecdiretorio_Log_Geral_Enabled');

            /* Send Email - Para Admin, Manager e User */
            // Carrega dados atuais na base de dados
            //$lang = VirtualDeskHelper::getLanguageTag();
            //$ObjEstado  = self::getciecdiretorioEstadoAtualObjectByIdciecdiretorio($lang, $Table->id_estado);
            //$EstadoNome = $ObjEstado->name;
            //self::SendMailAdminAltEstado( $Table->assunto, $objUserVD->name, $objUserVD->email, $Table->referencia, $EstadoNome);
            //$DepartName   = self::getDepartamentoName($Table->id_departamento);
            //self::SendMailClientAltEstado($objUserVD->name, $Table->assunto,$Table->descricao, $objUserVD->email, $DepartName, $EstadoNome,$Table->referencia);
            /* END Send Email */

            // LOG GERAL
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos'] = $UserJoomlaID;
                $eventdata['title'] = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_STATE_ALTERADO');
                $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_ciecdiretorio=' . $dataHist['id_ciecdiretorio'];
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_STATE_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e) {
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Retorna a Ref Id apenas se o User atual da sessão tem o mesmo NIF da ocorrência
           Útil para saber se o user tem acesso a este ciecdiretorio...
        */
    public static function checkRefId_4User_By_NIF ($RefId)
    {
        if( empty($RefId) )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
        if((int)$UserSessionNIF<=0) return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia as refid' ))
                ->from("#__virtualdesk_ciecdiretorio as a")
                ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn->refid);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*Gera código aleatório para a referência da ocorrencia*/
    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 12);
    }


    public static function gerarReferenciaNova ($indicativoPrograma)
    {
        $referencia = '';
        $refExiste  = 1;
        $year       = substr(date("Y"), -2);
        while($refExiste == 1){
            $refRandom  = self::random_code();
            $referencia = $indicativoPrograma . '_' . $refRandom;
            $checkREF = self::CheckReferencia($referencia);
            if((int)$checkREF == 0){
                $refExiste = 0;
            }
        }
        return($referencia);
    }

    /*Verifica de a referencia da ocorrencia existe*/
    public static function CheckReferencia($ref){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('referencia')
            ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados")
            ->where($db->quoteName('referencia') . "='" . $db->escape($ref) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getUsersListAtiveNotBlocked ()
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'list4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('ciecdiretorio','list4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.name as name', 'a.id as iduser', 'a.idjos as idjoomla', 'a.email as email', 'a.name as nomeuser', 'a.login as login', 'a.blocked as blocked', 'a.activated as activated' ))
                ->from("#__virtualdesk_users as a")
                ->where('a.blocked =0 and activated=1'));
            $dataReturn = $db->loadObjectList();
            return($dataReturn);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
  * Carrega lista de TODAS tipoloigias para serem apresentadas
  */
    public static function getTipologiaListAll ()
    {

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id as id','a.tipologia as name_pt','a.tipologia as name_en') )
                ->from("#__virtualdesk_EuropaCriativa_Tipologia as a")
            );

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {

                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }

                $response[] =  array (
                    'id'   => $row->id,
                    'name' => $rowName
                );
            }

            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getTipologiaListSelected ($Idciecdiretorio)
    {

        /*
       * Check PERMISSÕES
       */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('ciecdiretorio');                  // verifica permissão de read
        $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'view4managers'); // verifica permissão acesso ao layout para editar
        $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('ciecdiretorio'); // Verifica permissões READALL no módulo
        $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('ciecdiretorio','view4managers'); // Verifica permissões READALL no layout
        if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','id_tipologia as tipologia') )
                ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados_Tipologia")
                ->where($db->quoteName('id_projecto_apoiado') . '=' . $db->escape($Idciecdiretorio))
            );
            $dataReturn = $db->loadAssocList();
            $dataList = array();
            foreach( $dataReturn as $keyList )
            {
                $dataList[] = $keyList['tipologia'];
            }

            return($dataList);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /* Cria novo  para o ecrã/permissões do MANAGER */
    public static function create4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('ciecdiretorio');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'addnew4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $db    = JFactory::getDbo();

        if (empty($data) ) return false;

        $db->transactionStart();

        try {

            $EstadoIdInicio = (int)self::getEstadoIdInicio();

            // Prepara dados a inserir...
            $nomeProjeto = $data['nomeProjeto'];
            $descricao = $data['descricao'];
            $programa = $data['programa'];
            $linhaFinanciamento = $data['linhaFinanciamento'];
            $escala = $data['escala'];
            $entidade = $data['entidade'];
            $ano = $data['ano'];
            $link = $data['link'];
            $call = $data['call'];
            $dataInicio = $data['dataInicio'];
            $dataFim = $data['dataFim'];
            $valor = $data['valor'];
            $layout = $data['layout'];
            $lider = $data['lider'];
            $pais = $data['pais'];
            $tipologia = $data['tipologia'];
            $ProgramaParceiro = $data['ProgramaParceiro'];
            $distrito='';
            $concelhos='';




            /*REFERENCIA*/
            $indicativoPrograma = VirtualDeskSiteFormularioHelper::getIndicativo($programa);
            $RefNew = self::gerarReferenciaNova($indicativoPrograma);
            $referencia = $RefNew;


            if(!empty($data['descricao'])){
                $descricaoEncoded = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao), true);
            }

            if($layout != 1 && $layout != 2){
                $layout = 2;
            }

            if($programa == '1'){

                if(empty($tipologia['0'])){
                    $idsTipologia = '30';
                }else {
                    $idsTipologia = implode('|',$tipologia);
                }
            } else {
                $lider = 'NULL';
                $pais = 'NULL';
                $idsTipologia = '30';
            }

            if($linhaFinanciamento != 15){
                $escala = '';
            }

            $dataAtual = date("Y-m-d");

            $splitDataInicio = explode("-", $dataInicio);
            $DataInicioFormated = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];


            $splitDataFim = explode("-", $dataFim);
            $DataFimFormated = $splitDataFim[2] . '-' . $splitDataFim[1] . '-' . $splitDataFim[0];


            if($DataFimFormated == '--'){
                $DataFimBD = 'Em curso';
            } else {
                $DataFimBD = $DataFimFormated;
            }

            $idCat = VirtualDeskSiteFormularioHelper::getCat($linhaFinanciamento);


            $saveProj = VirtualDeskSiteFormularioHelper::saveProjeto($referencia, $nomeProjeto, $descricaoEncoded, $programa, $entidade, $lider, $pais, $ano, $linhaFinanciamento, $escala, $distrito, $concelhos, $idsTipologia, $idCat, $link, $DataInicioFormated, $DataFimBD, $call, $valor, $layout, $dataAtual);

            if (empty($saveProj)) {
                $db->transactionRollback();
                return false;
            } else {

                if ($programa == 1) {
                    foreach($ProgramaParceiro as $keyPar => $valParc ){
                        if($valParc['parceiro'] != ''){
                            VirtualDeskSiteFormularioHelper::saveParceiros($referencia, $valParc['parceiro'], $valParc['paisParceiro']);
                        }
                    }
                }

                $idProj = VirtualDeskSiteFormularioHelper::getIdProj($referencia);

                $piecesTipologia = explode("|", $idsTipologia);

                for($i=0; $i<count($piecesTipologia); $i++){
                    VirtualDeskSiteFormularioHelper::saveTipologiasV2($idProj, $piecesTipologia[$i]);
                }



                /* FILES */
                $objFiles                = new VirtualDeskSiteciecdiretorioFilesHelper();
                $objFiles->tagprocesso   = 'ProjetosApoiados';
                $objFiles->idprocesso    = $referencia;
                $resFileSave             = $objFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false ) {
                    $db->transactionRollback();
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                    return false;
                }
            }

            $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('ciecdiretorio_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }
        }
        catch (Exception $e){
            $db->transactionRollback();
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    /* Atualiza um  para o ecrã/permissões do USER */
    public static function update4User($UserJoomlaID, $UserVDId, $data)
    {
        try {
            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /* Atualiza um evento para o ecrã/permissões do MANAGER */
    public static function update4Manager($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('ciecdiretorio');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $ciecdiretorio_id = $data['ciecdiretorio_id'];
        if((int) $ciecdiretorio_id <=0 ) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTableCiecDiretorio($db);
        $Table->load(array('id'=>$ciecdiretorio_id));

        if (empty($data)) return false;

        $db->transactionStart();

        try {

            $referencia = $Table->referencia;


            $projeto = null;
            $designacao = null;
            $programa = null;
            $linha_Financiamento = null;
            $escala = null;
            $entidade = null;
            $ano_Apoio = null;
            $link = null;
            $call = null;
            $data_Inicio = null;
            $data_Fim = null;
            $valorFinanciamento = null;
            $layout = null;
            $lider = null;
            $pais_lider = null;
            //$tipologia = null;
            $distrito = null;
            $concelho = null;

            if(array_key_exists('nomeProjeto',$data))               $projeto = $data['nomeProjeto'];
            if(array_key_exists('descricao',$data))                 $designacao = $data['descricao'];
            if(array_key_exists('programa',$data))                  $programa = $data['programa'];
            if(array_key_exists('linhaFinanciamento',$data))        $linha_Financiamento = $data['linhaFinanciamento'];
            if(array_key_exists('escala',$data))                    $escala = $data['escala'];
            if(array_key_exists('entidade',$data))                  $entidade = $data['entidade'];
            if(array_key_exists('ano',$data))                       $ano_Apoio = $data['ano'];
            if(array_key_exists('link',$data))                      $link = $data['link'];

            if(array_key_exists('dataInicio',$data)) {
                $data_Inicio      = $data['dataInicio'];
                $splitDataInicio = explode("-", $data_Inicio);
                $invertData = $splitDataInicio[2] . '-' . $splitDataInicio[1] . '-' . $splitDataInicio[0];
            }

            if(array_key_exists('dataFim',$data)) {
                $data_Fim      = $data['dataFim'];
                $splitDatFim = explode("-", $data_Fim);
                $invertDataFim = $splitDatFim[2] . '-' . $splitDatFim[1] . '-' . $splitDatFim[0];
            }

            if(array_key_exists('call',$data))                      $call = $data['call'];
            if(array_key_exists('valor',$data))                     $valorFinanciamento = $data['valor'];
            if(array_key_exists('layoutDetalhe',$data))             $layout = $data['layoutDetalhe'];
            if(array_key_exists('lider',$data))                     $lider = $data['lider'];
            if(array_key_exists('pais',$data))                      $pais_lider = $data['pais'];
            //if(array_key_exists('tipologia',$data))                 $tipologia = $data['tipologia'];

            if(!empty($data['descricao'])){
                $data['descricao']      = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($data['descricao']), true);
                $designacaoEncoded = $data['descricao'];
            }


            /* TIPOLOGIA */
            $resTipologiaSave = self::saveTipologia4Manager ($UserJoomlaID, $UserVDId, $data);
            if ($resTipologiaSave===false) {
                JFactory::getApplication()->enqueueMessage('Erro ao gravar a tipologia', 'error');
                $db->transactionRollback();
                return false;
            }
            $data['tipologia'] = $resTipologiaSave;


            /* BEGIN Save Projeto */
                $resSaveProjeto = VirtualDeskSiteFormularioHelper::saveEditedProjeto4Manager($ciecdiretorio_id, $referencia, $projeto, $designacaoEncoded, $programa, $linha_Financiamento, $escala, $entidade, $ano_Apoio, $lider, $pais_lider, $link, $invertData, $invertDataFim, $valorFinanciamento, $layout, $data['tipologia'], $call);
            /* END Save Projeto */

            if (empty($resSaveProjeto)) {
                $db->transactionRollback();
                return false;
            }





            /* DELETE - FILES */
            $objFiles                = new VirtualDeskSiteciecdiretorioFilesHelper();
            $listFile2Eliminar       = $objFiles->setListFile2EliminarFromPOST ($referencia);
            $resFileDelete           = $objFiles->deleteFiles ($referencia , $listFile2Eliminar);

            if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros', 'error');
            }

            // Insere os NOVOS ficheiros
            /* FILES */
            $objFiles                = new VirtualDeskSiteciecdiretorioFilesHelper();
            $objFiles->tagprocesso   = 'ProjetosApoiados';
            $objFiles->idprocesso    = $referencia;
            $resFileSave             = $objFiles->saveListFileByPOST('fileupload');


            if ($resFileSave===false) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                return false;
            }

              $db->transactionCommit();

            $objVDParams = new VirtualDeskSiteParamsHelper();
            $setLogGeralEnabled  = $objVDParams->getParamsByTag('ciecdiretorio_Log_Geral_Enabled');

            // Foi alterado o Pedido do tipo Contact Us
            if((int)$setLogGeralEnabled===1) {
                $vdlog = new VirtualDeskLogHelper();
                $eventdata = array();
                $eventdata['iduser'] = $UserVDId;
                $eventdata['idjos']  = $UserJoomlaID;
                $eventdata['title']  = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_CREATED');
                $eventdata['desc']   = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_CREATED') . 'ref=' . $referencia;
                $eventdata['filelist'] = "";
                $eventdata['category'] = JText::_('COM_VIRTUALDESK_ciecdiretorio_EVENTLOG_CREATED_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);
            }

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    /*
    * Carrega lista das áreas de atuação associadas a um utilizador (quando é uma empresa)
    */
    public static function saveTipologia4Manager ($UserJoomlaID, $UserVDId, $data)
    {
        /*
         * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
         */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('ciecdiretorio');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $ciecdiretorio_id = $data['ciecdiretorio_id'];
        if((int) $ciecdiretorio_id <=0 ) return false;

        if( !array_key_exists('tipologia',$data) )  return true;  // não foram alterados os dados...

        $dataReturn = "";
        try
        {
            $db = JFactory::getDBO();

            if(!empty($data['tipologia']))
            {   $queryDel = $db->getQuery(true);
                $conditions = array(
                    $db->quoteName('id_projecto_apoiado') . ' = ' . $db->escape($ciecdiretorio_id)
                );
                $queryDel->delete($db->quoteName('#__virtualdesk_EuropaCriativa_ProjetosApoiados_Tipologia'));
                $queryDel->where($conditions);
                $db->setQuery($queryDel);
                if (!$db->execute()) return false;

                // Create a new query object.
                $queryIns = $db->getQuery(true);
                // Insert columns.
                $columns = array('id_projecto_apoiado', 'id_tipologia');
                // Insert values.
                $queryIns
                    ->insert($db->quoteName('#__virtualdesk_EuropaCriativa_ProjetosApoiados_Tipologia'))
                    ->columns($db->quoteName($columns));
                foreach($data['tipologia'] as $keyDataArea )
                {
                    $values = $ciecdiretorio_id.',' . $keyDataArea;
                    $queryIns->values($values);
                }

                // Set the query using our newly populated query object and execute it.
                $db->setQuery($queryIns);

                // Dump the query
                //echo $db->getQuery()->dump();

                if (!$db->execute()) return false;

                asort($data['tipologia']);

                $dataReturn = implode('|', $data['tipologia']);
            }
            else
            {   // não tem nenhuma escolha... se carregou dados das Tipologias então devem ser eliminados
                if(empty($dataReturn)) {
                    $query = $db->getQuery(true);
                    $conditions = array(
                        $db->quoteName('id_projecto_apoiado') . ' = ' . $db->escape($ciecdiretorio_id)
                    );
                    $query->delete($db->quoteName('#__virtualdesk_EuropaCriativa_ProjetosApoiados_Tipologia'));
                    $query->where($conditions);
                    $db->setQuery($query);

                    if (!$db->execute()) return false;
                }
            }

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    public static function getListaTipologias($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id_tipologia'))
            ->from("#__virtualdesk_EuropaCriativa_ProjetosApoiados_Tipologia")
            ->where($db->quoteName('id_projecto_apoiado') . "='" . $db->escape($id) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    /* Retorna Id Concluido*/
    public static function getEstadoIdConcluido ()
    {
        /*
        * Check PERMISSÕES
        */
        /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }*/

        try
        {
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select( array('id'))
                ->from("#__virtualdesk_ciecdiretorio_estado as a")
                ->where(" a.bitEnd=1 ")
            );
            $response = $db->loadObjectList();

            $arReturn = array();
            foreach ($response as $row)
            {
                $arReturn[] = (int)$row->id;
            }
            return($arReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Retorna Id Concluido*/
    public static function getEstadoIdInicio ()
    {
        /*
        * Check PERMISSÕES
        */
        /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }*/

        try
        {
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select( array('id'))
                ->from("#__virtualdesk_ciecdiretorio_estado as a")
                ->where(" a.bitStart=1 ")
            );
            $response = $db->loadObject();
            return($response->id);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /* Limpa os dados na sessão "users states" do joomla */
    public static function cleanAllTmpUserState() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.addnew4user.ciecdiretorio.data', null);
        $app->setUserState('com_virtualdesk.addnew4manager.ciecdiretorio.data', null);
        $app->setUserState('com_virtualdesk.edit4user.ciecdiretorio.data', null);
        $app->setUserState('com_virtualdesk.edit4manager.ciecdiretorio.data', null);

    }

}
?>