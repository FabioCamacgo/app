<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteGeneralHelper
{
    /**
     * Removes Joomla system messages from application queue
     *
     * Can remove messages by text or by type.
     *
     * @param   array  $messages2Remove  Messages to be removed by type
     * @param   array  $types2Remove     Types of messages to be removed
     *
     * @return   type  Description
     */
    public static function removeSysAppMessage($messages2Remove = array(), $types2Remove = array() )
    {
        if (empty($messages2Remove) && empty($types2Remove) )
        {
            return;
        }

        $messages2Remove = (array) $messages2Remove;
        $types2Remove = (array) $types2Remove;

        $app = JFactory::getApplication();
        $appReflection = new ReflectionClass(get_class($app));
        $_messageQueue = $appReflection->getProperty('_messageQueue');
        $_messageQueue->setAccessible(true);
        $messages = $_messageQueue->getValue($app);

        foreach ($messages as $key => $message)
        {
            if (in_array($message['message'], $messages2Remove) || in_array($message['type'], $types2Remove))
            {
                unset($messages[$key]);
            }
        }

        $_messageQueue->setValue($app, $messages);
    }



    public static function removeDisplayMsgListMessage($msgList = array(), $messages2Remove = array(), $types2Remove = array() )
    {
        if (empty($messages2Remove) && empty($types2Remove) )
        {
            return;
        }

        $messages2Remove = (array) $messages2Remove;
        $types2Remove = (array) $types2Remove;
        $messages = $msgList;

        foreach ($messages as $key => $message)
        {
            if (in_array($message, $messages2Remove) || in_array($key, $types2Remove))
            {
                unset($messages[$key]);
            }
        }

       return($messages);
    }


    public static function getCurrentLanguageTag () {

        // Idioma
        $jinput = JFactory::getApplication()->input;
        $language_tag = $jinput->get('lang', 'pt-PT', 'string');

        if(empty($language_tag)) return ('pt_PT');

        switch($language_tag)
        { case 'pt-PT':
            $fileLangSufix = 'pt_PT';
            break;
            default:
                $fileLangSufix = substr($language_tag, 0, 2);
                break;
        }
        return($fileLangSufix);
    }


    public static function encodeHTML2Database ($html, $RemoveNR=false)
    {
        if($RemoveNR===true) {
         $html = str_replace('\r\n', '', $html);
         return( trim(htmlspecialchars( $html )));
        }

        return (  trim(htmlspecialchars($html))  );

    }

    public static function decodeDatabase2HTML($campo)
    {
        return( stripslashes(htmlspecialchars_decode($campo)) );
    }


    public static function setHeaderPHPByList($Lista)
    {
        if( !is_array($Lista) ) $Lista = array();
        $urlHost = parse_url ($_SERVER['HTTP_REFERER'],PHP_URL_HOST);
        if( $urlHost=='' ) return(true);
        foreach ($Lista as $header) {
            if (strpos($header, $urlHost) !== false) {
                header($header);
                break;
            }
        }
        return(true);
    }
}
