<?php
/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskTableUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/user.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
//Import filesystem libraries. Perhaps not necessary, but does not hurt
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');


/**
 * VirtualDesk User FileUpload helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteFileUploadHelper extends VirtualDeskUserHelper
{


    private static $executable = array(
        'php', 'js', 'exe', 'phtml', 'java', 'perl', 'py', 'asp','dll', 'go', 'ade', 'adp', 'bat', 'chm', 'cmd', 'com', 'cpl', 'hta', 'ins', 'isp',
        'jse', 'lib', 'mde', 'msc', 'msp', 'mst', 'pif', 'scr', 'sct', 'shb', 'sys', 'vb', 'vbe', 'vbs', 'vxd', 'wsc', 'wsf', 'wsh'
    );

    /*
     * Verifica se foi enviado um ficheiro/foto então cumpre os requisitos
    */
    public static function checkUserAvatar ($UserJoomlaID, $Files)
    {

        $params = JComponentHelper::getParams('com_virtualdesk');

        return true;
    }


    public static function saveGenericAvatarFileToFolder ($UserJoomlaID, $GenericId , $File)
    {
       $UserJoomlaID = $UserJoomlaID . '_' . $GenericId;
       return (self::saveUserAvatarFileToFolder ($UserJoomlaID, $File));
    }


    public static function saveUserAvatarFileToFolder ($UserJoomlaID, $File)
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($File['name']) )  return '';

        $params       = JComponentHelper::getParams('com_virtualdesk');
        $extensions   = $params->get('useravatar_extensions');
        $max_size     = $params->get('useravatar_max_size');
        $avatarFolder = $params->get('useravatar_folder');
        $maxWidth     = $params->get('useravatar_width');
        $maxHeight    = $params->get('useravatar_height');


        $fileTypes = explode('.', $File['name']);

        if (count($fileTypes) < 2)
        {   // There seems to be no extension.
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'), 'error' );
            return false;
        }

        array_shift($fileTypes);
        // Check if the file has an executable extension.
        $executable = array(
            'php', 'js', 'exe', 'phtml', 'java', 'perl', 'py', 'asp','dll', 'go', 'ade', 'adp', 'bat', 'chm', 'cmd', 'com', 'cpl', 'hta', 'ins', 'isp',
            'jse', 'lib', 'mde', 'msc', 'msp', 'mst', 'pif', 'scr', 'sct', 'shb', 'sys', 'vb', 'vbe', 'vbs', 'vxd', 'wsc', 'wsf', 'wsh'
        );

        $check = array_intersect($fileTypes, $executable);
        if (!empty($check))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'), 'error' );
            return false;
        }

        $fileType = array_pop($fileTypes);
        $allowable = array_map('trim', explode(',', $extensions ));
        if ($fileType == '' || $fileType == false || (!in_array($fileType, $allowable)))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'), 'error' );
            return false;
        }

        $uploadMaxSize = $max_size* 1024 * 1024;
        $uploadMaxFileSize = self::toBytes(ini_get('upload_max_filesize'));

        if (($File['error'] == 1)
            || ($uploadMaxSize > 0 && $File['size'] > $uploadMaxSize)
            || ($uploadMaxFileSize > 0 && $File['size'] > $uploadMaxFileSize))
        {
            $max_sizeToMessageInMB = $max_size;
            $uploadMaxFileSizeInMB = (int)$uploadMaxFileSize/(1024*1024);
            if($uploadMaxFileSizeInMB < (int)$max_size) $max_sizeToMessageInMB = $uploadMaxFileSizeInMB;

            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_FILE_TOO_LARGE') . $max_sizeToMessageInMB .' Mb.', 'error' );
            return false;
        }

        // Make the file name unique.
        $md5String = $UserJoomlaID . $File['name'] . JFactory::getDate();
        $avatarFileName = JFile::makeSafe(md5($md5String));
        if (empty($avatarFileName))
        {   // No file name after the name was cleaned by JFile::makeSafe.
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_NO_FILENAME'), 'error' );
            return false;
        }

        $avatarPath = JPath::clean($avatarFolder . '/' . $avatarFileName . '.' . $fileType);

        if (JFile::exists($avatarPath))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_EXISTS'), 'error' );
            return false;
        }

        self::cropImageFile($File['tmp_name'],  JPath::clean($avatarFolder),  JPath::clean($avatarFileName), $fileType, 200, 200);

        return $avatarFileName . '.' . $fileType;

    }



    public static function saveFileToFolder ($UserJoomlaID, $File, $FileFolder = '', $FileSubFolder = '')
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($File['name']) )  return '';

        $params       = JComponentHelper::getParams('com_virtualdesk');
        $max_size     = $params->get('useravatar_max_size');
        $avatarFolder = $params->get('useravatar_folder');
        if(!empty($FileFolder)) $avatarFolder = $FileFolder . $FileSubFolder;


        $fileTypes = explode('.', $File['name']);
        $fileType = array_pop($fileTypes);

        if (count($fileTypes) < 1)
        {   // There seems to be no extension.
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'), 'error' );
            return false;
        }


        array_shift($fileTypes);
        // Check if the file has an executable extension.
        $executable = array(
            'php', 'js', 'exe', 'phtml', 'java', 'perl', 'py', 'asp','dll', 'go', 'ade', 'adp', 'bat', 'chm', 'cmd', 'com', 'cpl', 'hta', 'ins', 'isp',
            'jse', 'lib', 'mde', 'msc', 'msp', 'mst', 'pif', 'scr', 'sct', 'shb', 'sys', 'vb', 'vbe', 'vbs', 'vxd', 'wsc', 'wsf', 'wsh'
        );

        $check = array_intersect($fileTypes, $executable);
        if (!empty($check))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'), 'error' );
            return false;
        }


        $uploadMaxSize = $max_size* 1024 * 1024;
        $uploadMaxFileSize = self::toBytes(ini_get('upload_max_filesize'));

        if (($File['error'] == 1)
            || ($uploadMaxSize > 0 && $File['size'] > $uploadMaxSize)
            || ($uploadMaxFileSize > 0 && $File['size'] > $uploadMaxFileSize))
        {
            $max_sizeToMessageInMB = $max_size;
            $uploadMaxFileSizeInMB = (int)$uploadMaxFileSize/(1024*1024);
            if($uploadMaxFileSizeInMB < (int)$max_size) $max_sizeToMessageInMB = $uploadMaxFileSizeInMB;

            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_FILE_TOO_LARGE') . $max_sizeToMessageInMB .' Mb.', 'error' );
            return false;
        }

        // Make the file name unique.
        $md5String = $UserJoomlaID . $File['name'] . JFactory::getDate();
        $avatarFileName = JFile::makeSafe(md5($md5String));
        if (empty($avatarFileName))
        {   // No file name after the name was cleaned by JFile::makeSafe.
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_NO_FILENAME'), 'error' );
            return false;
        }

        $avatarPath = JPath::clean($avatarFolder . '/' . $avatarFileName . '.' . $fileType);

        // Se a pasta e subpasta onde guardar o ficheiro não existir ainda cria
        if (!JFolder::exists(JPath::clean($avatarFolder)))
        {
            if( !JFolder::create($avatarFolder) )
            { JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error' );
              return false;
            }
        }


        /// Na pasta deve também colocar um ficheiro vazio index.html
        if (!JFile::exists($avatarFolder.'/'.'index.html')) {
            if (!JFile::write($avatarFolder . '/' . 'index.html', '<html></html>')) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                return false;
            }
        }

        // File Path exists ?
        if (JFile::exists($avatarPath))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_EXISTS'), 'error' );
            return false;
        }

        // Verifica se existe a folder year + sub folder month, se não existir crtiar e devolve o subfolder onde deve ser guarda o ficheiro



        if ( !JFile::upload(JPath::clean($File['tmp_name']), $avatarPath) )
        {  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_EXISTS'), 'error' );
           return false;
        }

        // Depois do ficheiro criado, gera thumbnail
        self::createImageFileThumb ($avatarPath, $avatarFolder, $avatarFileName, $fileType, 300, 300);

        return $avatarFileName . '.' . $fileType;
    }



    public static function saveFileListToFolder ($UserJoomlaID, $FileList, $FileFolder)
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';
        $saveFileList = array();

        foreach ($FileList as $rowFile)
        {
            $File          = $rowFile['attachment'];
            $FileSubFolder =  '/' . date("Y") . '/' . date("m");

            $resUploaded   = self::saveFileToFolder ($UserJoomlaID, $File, $FileFolder, $FileSubFolder);
            if($resUploaded !== false) {
               // Se for false não entra aqui porque deu erro no upladop, mas pode vir vazio porque não foi colocadp nenhum ficheiro no campos
               // nesse caso não dá erro, mas também não coloca na lista
               if( !empty($resUploaded) ) {
                   $resUploadedRow = array();
                   $fileTypes = explode('.', $resUploaded);
                   $fileType  = array_pop($fileTypes);
                   $basename  = array_pop($fileTypes);
                   $resUploadedRow['filename'] = $resUploaded;
                   $resUploadedRow['ext']      = $fileType;
                   $resUploadedRow['desc']     = $File['name'];
                   $resUploadedRow['type']     = $File['type'];
                   $resUploadedRow['size']     = $File['size'];
                   $resUploadedRow['basename'] = $basename;
                   $resUploadedRow['filepath'] = $FileFolder . $FileSubFolder;
                   $saveFileList[] = $resUploadedRow;
               }
            }
            else   {
                return(false);
            }
        }
        return($saveFileList);
    }



    public static function saveFileToFolderSecured ($UserJoomlaID, $File, $FileFolder = '')
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($File['name']) ) return '';

        $ForceUniqueFileName = $File['forceuniquename'];

        $params       = JComponentHelper::getParams('com_virtualdesk');
        $max_size     = $params->get('useravatar_max_size');

        $FileSubFolderY =  $FileFolder. DS . date("Y");
        $FileSubFolder  =  $FileSubFolderY . DS  . date("m");
        $Folder2Save    =  $FileSubFolder;
        $Folder2Save = self::pathNormalize(DS, $Folder2Save);

        $fileTypes = explode('.', $File['name']);
        $fileType = array_pop($fileTypes);

        if (count($fileTypes) < 1)
        {   // There seems to be no extension.
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'), 'error' );
            return false;
        }

        array_shift($fileTypes);
        // Check if the file has an executable extension.
        $executable = self::$executable;

        $check = array_intersect($fileTypes, $executable);
        if (!empty($check))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'), 'error' );
            return false;
        }

        $uploadMaxSize = $max_size* 1024 * 1024;
        $uploadMaxFileSize = self::toBytes(ini_get('upload_max_filesize'));

        if (($File['error'] == 1)
            || ($uploadMaxSize > 0 && $File['size'] > $uploadMaxSize)
            || ($uploadMaxFileSize > 0 && $File['size'] > $uploadMaxFileSize))
        {
            $max_sizeToMessageInMB = $max_size;
            $uploadMaxFileSizeInMB = (int)$uploadMaxFileSize/(1024*1024);
            if($uploadMaxFileSizeInMB < (int)$max_size) $max_sizeToMessageInMB = $uploadMaxFileSizeInMB;

            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_FILE_TOO_LARGE') . $max_sizeToMessageInMB .' Mb.', 'error' );
            return false;
        }

        // Make the file name unique.
        $md5String = $UserJoomlaID . $File['name'] . JFactory::getDate();
        if($ForceUniqueFileName != '')  $md5String = $ForceUniqueFileName;
        $FileName2Save = JFile::makeSafe(md5($md5String));

        if (empty($FileName2Save))
        {   // No file name after the name was cleaned by JFile::makeSafe.
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_NO_FILENAME'), 'error' );
            return false;
        }

        $Path2Save = JPath::clean($Folder2Save . DS . $FileName2Save . '.' . $fileType);

        $Path2Save = self::pathNormalize(DS, $Path2Save);

        // Se a pasta e subpasta onde guardar o ficheiro não existir , cria
        if (!JFolder::exists(JPath::clean($Folder2Save)))
        {

            if( !JFolder::create($Folder2Save) )
            { JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error' );
                return false;
            }

            // Na pasta deve também colocar um ficheiro vazio index.html
            if (!JFile::exists($FileSubFolderY. DS .'index.html')) {
                if (!JFile::write($FileSubFolderY . DS  . 'index.html', '<html></html>')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($FileSubFolderY. DS .'.htaccess')) {
                if (!JFile::write($FileSubFolderY . DS  . '.htaccess', "Deny from all\nAuthType Basic\nAuthName 'VirtualDesk'\nAuthUserFile .htpasswd\nRequire valid-user")) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($FileSubFolderY.DS .'.htpasswd')) {
                if (!JFile::write($FileSubFolderY . DS  . '.htpasswd', 'vduser123:$apr1$c5Po9wbO$4lupFVXknqpcfaYv/5CcN/')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }

            // Na pasta deve também colocar um ficheiro vazio index.html
            if (!JFile::exists($Folder2Save.DS .'index.html')) {
                if (!JFile::write($Folder2Save . DS  . 'index.html', '<html></html>')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($Folder2Save.DS .'.htaccess')) {
                if (!JFile::write($Folder2Save . DS  . '.htaccess', "Deny from all\nAuthType Basic\nAuthName 'VirtualDesk'\nAuthUserFile .htpasswd\nRequire valid-user")) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
            if (!JFile::exists($Folder2Save. DS .'.htpasswd')) {
                if (!JFile::write($Folder2Save . DS  . '.htpasswd', 'vduser123:$apr1$c5Po9wbO$4lupFVXknqpcfaYv/5CcN/')) {
                    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_CREATEFOLDER'), 'error');
                    return false;
                }
            }
        }


        // File Path exists ?
        if (JFile::exists($Path2Save))
        {   JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_EXISTS'), 'error' );
            return false;
        }

        // Verifica se existe a folder year + sub folder month, se não existir crtiar e devolve o subfolder onde deve ser guarda o ficheiro
        if ( !JFile::upload(JPath::clean($File['tmp_name']), $Path2Save) )
        {  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_EXISTS'), 'error' );
            return false;
        }


        // ToDo : Se for imagem gera um thumbnail... e retorna nos resultados de modo a poder gravar na tabela de dados.
        if(self::checkFileIsImageType ($fileType)) {
            // Depois do ficheiro criado, gera thumbnail
            //self::createImageFileThumb ($avatarPath, $avatarFolder, $FileName2Save, $fileType, 300, 300);
        }


        return ( array('file'=>$FileName2Save.'.'.$fileType , 'path'=>JPath::clean($Folder2Save), 'thumb'=>'' ) );
    }


    public static function saveFileListToFolderSecured ($UserJoomlaID, $FileList, $FileFolder)
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( empty($FileList) )  return '';
        $saveFileList = array();

        foreach ($FileList as $rowFile)
        {
            $File = $rowFile['attachment'];
            $FileFolder = self::pathNormalize(DS, $FileFolder);

            $resUploaded   = self::saveFileToFolderSecured ($UserJoomlaID, $File, $FileFolder);
            if($resUploaded !== false) {
                // Se for false não entra aqui porque deu erro no upladop, mas pode vir vazio porque não foi colocadp nenhum ficheiro no campos
                // nesse caso não dá erro, mas também não coloca na lista
                if( !empty($resUploaded) ) {
                    $resUploadedRow = array();
                    $fileTypes = explode('.', $resUploaded['file']);
                    $fileType  = array_pop($fileTypes);
                    $basename  = array_pop($fileTypes);
                    $resUploadedRow['filename'] = $resUploaded['file'];
                    $resUploadedRow['ext']      = $fileType;
                    $resUploadedRow['desc']     = $File['name'];
                    $resUploadedRow['type']     = $File['type'];
                    $resUploadedRow['size']     = $File['size'];
                    $resUploadedRow['basename'] = $basename;
                    $resUploadedRow['filepath'] = $resUploaded['path'];
                    $saveFileList[] = $resUploadedRow;
                }
            }
            else   {
                return(false);
            }
        }
        return($saveFileList);
    }



    /**
     * Small helper function that properly converts any
     * configuration options to their byte representation.
     * From libraries/cms/helper/media.php
     *
     * @param   string|integer  $val  The value to be converted to bytes.
     *
     * @return integer The calculated bytes value from the input.
     *
     * @since 1.0.0
     */
    protected static function toBytes($val)
    {
        switch ($val[strlen($val) - 1])
        {
            case 'M':
            case 'm':
                return (int) $val * 1048576;
            case 'K':
            case 'k':
                return (int) $val * 1024;
            case 'G':
            case 'g':
                return (int) $val * 1073741824;
            default:
                return $val;
        }
    }


    public static function deleteUserAvatarFile ($FileName)
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if (empty($FileName)) return '';

        $params = JComponentHelper::getParams('com_virtualdesk');
        $avatarFolder = $params->get('useravatar_folder');

        JFile::delete($avatarFolder . DS . $FileName);
    }


    /**
    * Carrega os ficheiros associados e de acordo com o tipo de ficheiro o icon
    *
    */
    public static function setFileListIconsOrPreview ($FileList)
    {
        foreach ($FileList as $key => $row) {

            $fileTypes = explode('.', $row->filename);
            $fileType = array_pop($fileTypes);

            $FileList[$key]->fileicon = self::getFileTypeFAIcon ($fileType,'fa-2x');

        }
        return($FileList);
    }


    public static function getFileTypeFAIcon ($fileType,$fasize)
    {
        switch ($fileType)
        {
            case 'pdf':
            case 'PDF':
                return('<i class="fa fa-file-pdf-o '.$fasize.' "></i>');
                break;
            case 'jpg':
            case 'JPG':
                return('<i class="fa fa-file-image-o '.$fasize.'"></i>');
                break;
            case 'png':
            case 'PNG':
                return('<i class="fa fa-file-image-o '.$fasize.'"></i>');
                break;
            case 'gif':
            case 'GIF':
                return('<i class="fa fa-file-image-o '.$fasize.'"></i>');
                break;
            case 'doc':
            case 'docx':
                return('<i class="fa fa-file-word-o '.$fasize.'"></i>');
                break;
            case 'xls':
            case 'xlsx':
                return('<i class="fa fa-file-excel-o '.$fasize.'"></i>');
                break;
            case 'ppt':
            case 'pptx':
                return('<i class="fa fa-file-powerpoint-o '.$fasize.'"></i>');
                break;
            default:
                return('<i class="fa fa-file-text-o '.$fasize.'"></i>');
                break;
        }
    }


    public static function setDownloadHeaders ($objFile)
    {

        $firstCharFolder = substr($objFile->filepath, 0,1);
        $lastCharFolder  = substr($objFile->filepath, -1);

        if( ($firstCharFolder !='/' || $firstCharFolder !="\\")) $objFile->filepath = '/' . $objFile->filepath;
        if( ($lastCharFolder !='/' && $lastCharFolder !="\\") )  $objFile->filepath .= '/';

        $filefullpath     = JPATH_ROOT . $objFile->filepath . $objFile->filename;
        //$rawFileName      = urlencode(trim($objFile->desc));
        $rawFileName      = rawurlencode(trim($objFile->desc));
        $rawFileName  = VirtualDeskHelper::removeAccents($objFile->desc);


        //$rawFileName      = utf8_encode(trim($objFile->desc));

        $escapedFileName  = preg_replace('/[^A-Za-z0-9_\-]/', '_', $rawFileName);
        //$escapedFileName  =  $rawFileName;
        $escapedFileName .= '.' . trim($objFile->ext);

        header('Content-Description: File Transfer');
        //header('Content-Type: image/jpeg');
        header('Content-Type: application/octet-stream');
        //header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename=' . $escapedFileName);
        //header('Content-Disposition: attachment; filename=' . trim($objFile->desc) . '.' . trim($objFile->ext));
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filefullpath));
        ob_clean();
        flush();
        readfile($filefullpath);
        exit;

    }



    public static function deleteFileFromFolder ($Folder, $FileName)
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if (empty($Folder)) return '';
        if (empty($FileName)) return '';

        $FileNameThumb = array();

        $fileTypes = explode('.', $FileName);
        if(sizeof($fileTypes)>0) {
            $FileNameThumb[0] = $fileTypes[0].'_thumb_300x300.'.$fileTypes[1];
            //$FileNameThumb[1] = $fileTypes[0].'_thumb_200x200.'.$fileTypes[1];
        }

        $resDelMain  = JFile::delete($Folder . DS . $FileName);

        if(sizeof($FileNameThumb)>0) {
            if ($FileNameThumb[0] != '') JFile::delete($Folder . DS . $FileNameThumb[0]);
        }

        return($resDelMain);
    }



    public static function createImageFileThumb ($UploadedImage, $NewFileFolder, $NewFileName, $UploadedType, $thumb_width, $thumb_height)
    {
        if(!self::checkFileIsImageType ($UploadedType)) return(true);
        $NewFileName = $NewFileName.'_thumb_'.$thumb_width.'x'.$thumb_height;
        self::cropImageFile($UploadedImage, $NewFileFolder, $NewFileName, $UploadedType, $thumb_width, $thumb_height);
    }


    public static function cropImageFile ($UploadedImage, $NewFileFolder, $NewFileName, $UploadedType, $crop_width, $crop_height)
    {
        $upload_image = $UploadedImage;
        $file_ext     = strtolower ($UploadedType);
        $cropfile     = $NewFileFolder .'/'. $NewFileName.'.' . $UploadedType;

        // Get new dimensions
        list($width_orig, $height_orig) = getimagesize($upload_image);

//        $ratio_orig = $width_orig/$height_orig;
//        if ($crop_width/$crop_height > $ratio_orig) {
//            $crop_width = $crop_height*$ratio_orig;
//        } else {
//            $crop_height = $crop_width/$ratio_orig;
//        }

        // calculating the part of the image to use for thumbnail
        if ($width_orig > $height_orig) {
            $y = 0;
            $x = ($width_orig - $height_orig) / 2;
            $smallestSide = $height_orig;
        } else {
            $x = 0;
            $y = ($height_orig - $width_orig) / 2;
            $smallestSide = $width_orig;
        }

        $crop_create = imagecreatetruecolor($crop_width,$crop_height);

        switch($file_ext){
            case 'jpg':
                $source = imagecreatefromjpeg($upload_image);
                break;
            case 'jpeg':
                $source = imagecreatefromjpeg($upload_image);
                break;
            case 'png':
                $source = imagecreatefrompng($upload_image);
                break;
            case 'gif':
                $source = imagecreatefromgif($upload_image);
                break;
            default:
                $source = imagecreatefromjpeg($upload_image);
        }

        //imagecopyresized($crop_create,$source,0,0,0,0,$crop_width,$crop_height,$width_orig,$height_orig);
        imagecopyresized($crop_create,$source,0,0,0,0,$crop_width,$crop_height,$smallestSide,$smallestSide);

        switch($file_ext){
            case 'jpg' || 'jpeg':
                imagejpeg($crop_create,$cropfile,100);
                break;
            case 'png':
                imagepng($crop_create,$cropfile,100);
                break;

            case 'gif':
                imagegif($crop_create,$cropfile);
                break;
            default:
                imagejpeg($crop_create,$cropfile,100);
        }
    }



    public static function getImageFileThumbPath ($FileName, $thumb_width)
    {
        $fileTypes = explode('.', $FileName);
        if(sizeof($fileTypes)>0) {
            return($fileTypes[0].'_thumb_'.$thumb_width.'x'.$thumb_width.'.'.$fileTypes[1]);
        }
        return('');
    }


    public static function checkFileIsImageType ($FileExt)
    {
        $FileExt = strtolower($FileExt);
        $images = array('jpg', 'jpeg', 'gif', 'png');
        if ( in_array($FileExt, $images, true) ) return(true);
        return(false);
    }


    // MD5( File name) + MD5 ( param secretdownloadlink )
    public static function setFileGuestDownloadLink ($filename)
    {
        $return      = '';
        $paramsecret = JComponentHelper::getParams('com_virtualdesk')->get('guestdownloadlinksecret');
        if(!empty($paramsecret))  $return = JFile::makeSafe( hash('sha256',$paramsecret) . hash('sha256',$filename) );

        return($return);
    }


    public static function pathNormalize($DS, $path)
    {
        $resPath = ($DS === '\\') ? str_replace('/', '\\', $path) : str_replace('\\', '/', $path);
        return ($resPath);
    }

}