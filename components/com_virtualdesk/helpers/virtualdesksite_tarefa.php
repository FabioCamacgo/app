<?php
/**
 * 2020-04-11 Acerto por sobreposição de ficheiros no NAS e noutras Apps
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskTableTarefa', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/tarefa.php');
JLoader::register('VirtualDeskTableTarefaHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/tarefa_historico.php');
JLoader::register('VirtualDeskTableTarefaProcesso', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/tarefa_processo.php');
JLoader::register('VirtualDeskTableTarefaUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/tarefa_user.php');
JLoader::register('VirtualDeskTableTarefaGroup', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/tarefa_group.php');;
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    class VirtualDeskSiteTarefaHelper
    {
        const tagchaveModulo = 'tarefa';


        /* Carrega ID do tipo de processo das tarefa a partir da tag enviada */
        public static function getTipoProcessoIdByTag ($tagchave)
        {
            if(empty($tagchave)) return false;
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array(' id ') )
                ->from("#__virtualdesk_tarefa_tipoprocesso")
                ->where(" tagchave='". $db->escape($tagchave)."'")
            );
            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return false;
            return((int)$dataReturn->id);
        }

        /* Grava dados para criar uma Nova Tarefa para outros utilizadores e/ou grupos */
        public static function saveNovaTarefa4ManagerByAjax ($getProcessoId, $getProcessoTipo, $setUserId, $setGroupId, $setDesc, $setNome)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess($getProcessoTipo);                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess($getProcessoTipo, 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            //if( empty($setUserId) and empty($setGroupId))  return false;
            if((int) $getProcessoId <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Set Tabela da BD
            $db    = JFactory::getDbo();

            $db->transactionStart();

            try {

                // Parametros para os emails
                $obParam                    = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio              = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio           = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral        = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP               = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright              = $obParam->getParamsByTag('LinkCopyright');


                // Grava a tarefa e retorna o Id do novo registo
                $idTarefaTipoProcesso  = self::getTipoProcessoIdByTag($getProcessoTipo);

                $TableTarefa           = new VirtualDeskTableTarefa($db);
                $dataTrf               = array();
                $dataTrf['id_tarefa_estado'] = 1; // TODO inicial, por defeito ou vir por parâmetro ??????
                $dataTrf['id_tarefa_tipoprocesso'] = $idTarefaTipoProcesso; // carrega tag do Alerta
                $dataTrf['nome']       = $db->escape($setNome);
                $dataTrf['descricao']  = $db->escape($setDesc);
                $dataTrf['createdby']  = $UserJoomlaID;
                $dataTrf['modifiedby'] = $UserJoomlaID;

                // Tabela Tarefa
                if (!$TableTarefa->save($dataTrf)) {
                    $db->transactionRollback();
                    return false;
                }

                $newTarefaId = (int)$TableTarefa->id;

                if ($newTarefaId<=0) {
                    $db->transactionRollback();
                    return false;
                }

                //Já gravou a tarefa, temos o Id, agora coloca o Id processo associado
                $TableTrfProcesso  = new VirtualDeskTableTarefaProcesso($db);
                $dataTrfProcesso   = array();
                $dataTrfProcesso['id_tarefa']   = $newTarefaId; // TODO inicial, por defeito ou vir por parâmetro ??????
                $dataTrfProcesso['id_tarefa_tipoprocesso'] = $idTarefaTipoProcesso;
                $dataTrfProcesso['id_processo'] = $getProcessoId;

                // Tabela Tarefa
                if (!$TableTrfProcesso->save($dataTrfProcesso)) {
                    $db->transactionRollback();
                    return false;
                }

                //Agora envia para o histórico da tarefa a indicar que foi iniciada
                $TableTrfHistorico  = new VirtualDeskTableTarefaHistorico($db);
                $dataTrfHistorico   = array();
                $dataTrfHistorico['id_tarefa'] = $newTarefaId;
                $dataTrfHistorico['nome']      = $db->escape($setNome);
                $dataTrfHistorico['descricao'] = $db->escape($setDesc);;
                $dataTrfHistorico['iduser']    = $UserVDId;
                $dataTrfHistorico['iduserjos'] = $UserJoomlaID;

                // Tabela Tarefa
                if (!$TableTrfHistorico->save($dataTrfHistorico)) {
                    $db->transactionRollback();
                    return false;
                }

                if(!empty($setUserId) && is_array($setUserId) ) {
                    foreach ($setUserId as $chvU => $valU) {
                        $TableUser  = new VirtualDeskTableTarefaUser($db);
                        $dataUser = array();
                        $dataUser['id_tarefa']     = $newTarefaId;
                        $dataUser['id_user']       = $valU;
                        $dataUser['createdby']     = $UserJoomlaID;
                        $dataUser['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableUser->save($dataUser)) {
                            $db->transactionRollback();
                            return false;
                        }
                        else {
                            # Envia email de tarefa para o user atribuído à tarefa
                            $objUserInTask = VirtualDeskSiteUserHelper::getUserObjById($valU);
                            self::SendMailUserInTask($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserInTask->name, $setNome, $setDesc, $objUserInTask->email, '');
                            $objUserInTask = null;
                        }
                    }
                }

                // Tabela Group
                if(!empty($setGroupId) && is_array($setGroupId) ) {
                    foreach ($setGroupId as $chvG => $valG) {
                        $TableGroup  = new VirtualDeskTableTarefaGroup($db);
                        $dataGroup = array();
                        $dataGroup['id_tarefa'] = $newTarefaId;
                        $dataGroup['id_group']      = $valG;
                        $dataGroup['createdby']     = $UserJoomlaID;
                        $dataGroup['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableGroup->save($dataGroup)) {
                            $db->transactionRollback();
                            return false;
                        } else {
                            # Envia email de tarefa para o grupo atribuído à tarefa. Se o grupo não tiver email envia para todos os utilizadores do grupo
                            $objGroupUserInTask   = $objCheckPerm->getGrupoOrUsersEmail2Obj($valG);
                            $GroupUserInTaskName  = $objGroupUserInTask->grupo->nome;
                            $GroupUserInTaskEmail = '';
                            if(empty($objGroupUserInTask->grupo->email) || (string)$objGroupUserInTask->grupo->email == '') {
                                # envia para todos os utilizadores do grupo
                                if(!is_array($objGroupUserInTask->users)) $objGroupUserInTask->users = array();
                                foreach ($objGroupUserInTask->users as $chvGUsrDetail => $valGUsrDetail) {
                                    $GroupUserInTaskEmail .= $valGUsrDetail->email .',';
                                }
                            } else {
                                $GroupUserInTaskEmail = $objGroupUserInTask->grupo->email;
                            }

                            if(!empty($GroupUserInTaskEmail)) {
                                self::SendMailUserInTask($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $GroupUserInTaskName, $setNome, $setDesc, $GroupUserInTaskEmail, '');
                            }
                            $objGroupUserInTask = null;
                        }
                    }
                }

                $db->transactionCommit();

                #$lang                       = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();
                $objVDParams                = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled         = $objVDParams->getParamsByTag('Tarefa_Log_Geral_Enabled');

                /* Send Email - Para Admin, Manager e User (que criou a tarefa) */
                self::SendMailAdmin($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $setNome, $objUserVD->name, $objUserVD->email, $getProcessoTipo);
                self::SendMailUserWhoIniated($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserVD->name, $setNome, $setDesc, $objUserVD->email, '');
                /* END Send Email */

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_CREATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_CREATED') . ' TarefaId=' . $newTarefaId . ' TipoPrc: '.$getProcessoTipo;
                    $eventdata['desc'] .= '<br> '.json_encode($dataTrf);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = 'TrfId= '. $newTarefaId;
                    $vdlog->insertEventLog($eventdata);
                }



                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Grava a alteração do estado de uma tarefa por processo Id*/
        public static function saveTarefaAlteraEstadoByProcesso4ManagerByAjax ($getProcessoId, $getProcessoTipo, $getTarefaId, $setNewEstadoId, $setObs)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess($getProcessoTipo);                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess($getProcessoTipo, 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if((int)$getProcessoId <=0 ) return false;
            if((int)$getTarefaId <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Set Tabela da BD
            $db    = JFactory::getDbo();

            // Valida os IDs tarefa e id processo
            $TableTrfProcesso = new VirtualDeskTableTarefaProcesso($db);
            $TableTrfProcesso->load(array('id_tarefa'=>$getTarefaId,'id_processo'=>$getProcessoId));
            if( (int)$TableTrfProcesso->id <= 0 || (int)$TableTrfProcesso->id_processo!=(int)$getProcessoId || (int)$TableTrfProcesso->id_tarefa!=(int)$getTarefaId ) return false;

            $TableTarefa = new VirtualDeskTableTarefa($db);
            $TableTarefa->load(array('id'=>$getTarefaId));
            if( (int)$TableTarefa->id <= 0 ) return false;
            $dataTrf = array();
            if((int)$TableTarefa->id_tarefa_estado!=(int)$setNewEstadoId ) {
                $dateModified = new DateTime();
                $dataTrf['modified']   = $dateModified->format('Y-m-d H:i:s');
                $dataTrf['modifiedby'] = $UserJoomlaID;
            }
            else
            {   // não foi alterado o estado
                return true;
            }

            $db->transactionStart();

            try {
                // Grava a alteração de estado da tarefa
                $dataTrf['id_tarefa_estado'] = $setNewEstadoId;
                $dataTrf['modifiedby'] = $UserJoomlaID;
                $dataTrf['modifiedby'] = $UserJoomlaID;

                // Se o estado a colocar for o concluído, coloca data de fim...?
                $getIdEstadoConcluido = self::getEstadoIdConcluido();
                $dataTrf['datafim'] = '';
                if((int)$getIdEstadoConcluido == (int)$setNewEstadoId) $dataTrf['datafim'] = $dateModified->format('Y-m-d H:i:s');

                // Tabela Tarefa
                if (!$TableTarefa->save($dataTrf)) {
                    $db->transactionRollback();
                    return false;
                }

                $Novo2SetEstadoNome = self::getEstadoNomeById ($setNewEstadoId);

                //Agora envia para o histórico da tarefa a indicar que foi iniciada
                $TableTrfHistorico  = new VirtualDeskTableTarefaHistorico($db);
                $dataTrfHistorico   = array();
                $dataTrfHistorico['id_tarefa'] = $getTarefaId;
                $dataTrfHistorico['descalt']   = JText::_('COM_VIRTUALDESK_TAREFA_ALTERAR_ESTADO_MSG').' '.$Novo2SetEstadoNome;
                $dataTrfHistorico['nome']      = '';
                $dataTrfHistorico['descricao'] = '';
                $dataTrfHistorico['obs'] = $db->escape($setObs);
                $dataTrfHistorico['id_tarefa_estado'] = (int) $dataTrf['id_tarefa_estado'];
                $dataTrfHistorico['iduser']    = $UserVDId;
                $dataTrfHistorico['iduserjos'] = $UserJoomlaID;

                // Tabela Tarefa
                if (!$TableTrfHistorico->save($dataTrfHistorico)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams                = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled         = $objVDParams->getParamsByTag('Tarefa_Log_Geral_Enabled');

                $obParam                    = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio              = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio           = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral        = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP               = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright              = $obParam->getParamsByTag('LinkCopyright');

                # Carrega os dados do utilizador que criou a tarefa... para poder ser avisado que foi alterado o estado.
                $objUserCreated = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID($TableTarefa->createdby);

                /* Send Email - Para Admin, Manager e User */
                self::SendMailAdminAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $TableTarefa->nome, $objUserVD->name, $objUserVD->email, '', $Novo2SetEstadoNome);
                if(!empty($objUserCreated->email)) {
                    # Enmia email para quem criou a tarefa de modo a indicaar que foi alterdo o estado
                    self::SendMailUserWhoInitiatedAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserCreated->name, $TableTarefa->nome, $TableTarefa->descricao, $objUserCreated->email, '', $Novo2SetEstadoNome, '');
                }
                /* END Send Email */

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_STATE_ALTERADO') . ' NewEstado='.$Novo2SetEstadoNome . ' TarefaId=' . $getTarefaId . ' TipoPrc: '.$getProcessoTipo . 'id_estado=' . $setNewEstadoId;
                    $eventdata['desc'] .= '<br> '.json_encode($dataTrf);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = 'TrfId= '. $getTarefaId;
                    $vdlog->insertEventLog($eventdata);
                }



                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Grava a alteração do estado de uma tarefa */
        public static function saveTarefaAlteraEstado4ManagerByAjax ($getTarefaId, $setNewEstadoId, $setObs)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('tarefa');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('tarefa', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if((int)$getTarefaId <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Set Tabela da BD
            $db    = JFactory::getDbo();

            $TableTarefa = new VirtualDeskTableTarefa($db);
            $TableTarefa->load(array('id'=>$getTarefaId));
            if( (int)$TableTarefa->id <= 0 ) return false;
            $dataTrf = array();
            if((int)$TableTarefa->id_tarefa_estado!=(int)$setNewEstadoId ) {
                $dateModified = new DateTime();
                $dataTrf['modified']   = $dateModified->format('Y-m-d H:i:s');
                $dataTrf['modifiedby'] = $UserJoomlaID;
            }
            else
            {   // não foi alterado o estado
                return true;
            }

            $db->transactionStart();

            try {
                // Grava a alteração de estado da tarefa
                $dataTrf['id_tarefa_estado'] = $setNewEstadoId;
                $dataTrf['modifiedby'] = $UserJoomlaID;
                $dataTrf['modifiedby'] = $UserJoomlaID;

                // Se o estado a colocar for o concluído, coloca data de fim...?
                $getIdEstadoConcluido = self::getEstadoIdConcluido();
                $dataTrf['datafim'] = '';
                if((int)$getIdEstadoConcluido == (int)$setNewEstadoId) $dataTrf['datafim'] = $dateModified->format('Y-m-d H:i:s');

                // Tabela Tarefa
                if (!$TableTarefa->save($dataTrf)) {
                    $db->transactionRollback();
                    return false;
                }

                $Novo2SetEstadoNome = self::getEstadoNomeById ($setNewEstadoId);

                //Agora envia para o histórico da tarefa a indicar que foi iniciada
                $TableTrfHistorico  = new VirtualDeskTableTarefaHistorico($db);
                $dataTrfHistorico   = array();
                $dataTrfHistorico['id_tarefa'] = $getTarefaId;
                $dataTrfHistorico['descalt']   = JText::_('COM_VIRTUALDESK_TAREFA_ALTERAR_ESTADO_MSG').' '.$Novo2SetEstadoNome;
                $dataTrfHistorico['nome']      = '';
                $dataTrfHistorico['descricao'] = '';
                $dataTrfHistorico['obs'] = $db->escape($setObs);
                $dataTrfHistorico['id_tarefa_estado'] = (int) $dataTrf['id_tarefa_estado'];
                $dataTrfHistorico['iduser']    = $UserVDId;
                $dataTrfHistorico['iduserjos'] = $UserJoomlaID;

                // Tabela Tarefa
                if (!$TableTrfHistorico->save($dataTrfHistorico)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams                = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled         = $objVDParams->getParamsByTag('Tarefa_Log_Geral_Enabled');

                $obParam                    = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio              = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio           = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral        = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP               = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright              = $obParam->getParamsByTag('LinkCopyright');

                # Carrega os dados do utilizador que criou a tarefa... para poder ser avisado que foi alterado o estado.
                $objUserCreated = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID($TableTarefa->createdby);

                /* Send Email - Para Admin, Manager e User */
                self::SendMailAdminAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $TableTarefa->nome, $objUserVD->name, $objUserVD->email, '', $Novo2SetEstadoNome);
                if(!empty($objUserCreated->email)) {
                    self::SendMailUserWhoInitiatedAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserCreated->name, $TableTarefa->nome, $TableTarefa->descricao, $objUserCreated->email, '', $Novo2SetEstadoNome, '');
                }
                /* END Send Email */

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_STATE_ALTERADO') . ' NewEstado='.$Novo2SetEstadoNome . ' TarefaId=' . $getTarefaId  . 'id_estado=' . $setNewEstadoId;
                    $eventdata['desc'] .= '<br> '.json_encode($dataTrf);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = 'TrfId= '. $getTarefaId;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Grava a alteração de uma tarefa, a partir de um processo como por exemplo o alerta */
        public static function saveTarefaEditarByProcesso4ManagerByAjax ($getProcessoId, $getProcessoTipo, $getTarefaId, $setEstadoId, $setNome, $setDesc, $setUserId, $setGroupId)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess($getProcessoTipo);                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess($getProcessoTipo, 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if((int)$getProcessoId <=0 ) return false;
            if((int)$getTarefaId <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Set Tabela da BD
            $db    = JFactory::getDbo();

            // Valida os IDs tarefa e id processo
            $TableTrfProcesso = new VirtualDeskTableTarefaProcesso($db);
            $TableTrfProcesso->load(array('id_tarefa'=>$getTarefaId,'id_processo'=>$getProcessoId));
            if( (int)$TableTrfProcesso->id <= 0 || (int)$TableTrfProcesso->id_processo!=(int)$getProcessoId || (int)$TableTrfProcesso->id_tarefa!=(int)$getTarefaId ) return false;

            $TableTarefa = new VirtualDeskTableTarefa($db);
            $TableTarefa->load(array('id'=>$getTarefaId));
            if( (int)$TableTarefa->id <= 0 ) return false;

            // Verifica se houve alterações nos users e7ou nos grupos
            $getTrfUserLst  = self::getTarefaUserListById ($getTarefaId);
            $getTrfGroupLst = self::getTarefaGroupListById ($getTarefaId);

            // ordena os arrays por id
            $vbCheckAlteraUser = true;
            if(!is_array($getTrfUserLst)) {
                $vbCheckAlteraUser = false;
            }
            else {
                sort($getTrfUserLst);
                sort($setUserId);
                if($setUserId === $getTrfUserLst) $vbCheckAlteraUser = false;
            }

            $vbCheckAlteraGroup = true;
            if(!is_array($getTrfGroupLst)) {
                $vbCheckAlteraGroup = false;
            }
            else {
                sort($getTrfGroupLst);
                sort($setGroupId);
                if($setGroupId === $getTrfGroupLst) $vbCheckAlteraGroup = false;
            }

            $dataTrf = array();
            if( (int)$TableTarefa->id_tarefa_estado!=(int)$setEstadoId || (string)$TableTarefa->nome!=(string)$setNome  || (string)$TableTarefa->descricao!=(string)$setDesc || $vbCheckAlteraUser===true || $vbCheckAlteraGroup===true)  {
                $dateModified = new DateTime();
                $dataTrf['modified']   = $dateModified->format('Y-m-d H:i:s');
                $dataTrf['modifiedby'] = $UserJoomlaID;
            }
            else
            {   // não foi alterado o estado
                return true;
            }

            $db->transactionStart();

            try {
                // Parametros para os emails
                $obParam                    = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio              = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio           = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral        = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP               = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright              = $obParam->getParamsByTag('LinkCopyright');

                // Grava a alteração de estado da tarefa
                if( (int)$TableTarefa->id_tarefa_estado!=(int)$setEstadoId) {
                    $dataTrf['id_tarefa_estado'] = $setEstadoId;
                    // Se o estado a colocar for o concluído, coloca data de fim...?
                    $getIdEstadoConcluido = self::getEstadoIdConcluido();
                    $dataTrf['datafim'] = '';
                    if((int)$getIdEstadoConcluido == (int)$setEstadoId) $dataTrf['datafim'] = $dateModified->format('Y-m-d H:i:s');
                }
                if( (string)$TableTarefa->nome!=(string)$setNome) $dataTrf['nome'] = $setNome;
                if( (string)$TableTarefa->descricao!=(string)$setDesc) $dataTrf['descricao'] = $setDesc;


                // Tabela Tarefa
                if (!$TableTarefa->save($dataTrf)) {
                    $db->transactionRollback();
                    return false;
                }

                // Limpa os Users associados se vai haver alteração
                if($vbCheckAlteraUser===true) {
                    $query = $db->getQuery(true);
                    $query->delete($db->quoteName('#__virtualdesk_tarefa_user'));
                    $query->where($db->quoteName('id_tarefa').'='.$getTarefaId);
                    $db->setQuery($query);
                    $resultUp01 = $db->execute();
                    if (!$resultUp01) {
                        $db->transactionRollback();
                        return false;
                    }
                }

                # Insere novos utilizadores
                $UsersName2Hist = '';
                if(!empty($setUserId) && is_array($setUserId) && $vbCheckAlteraUser===true) {
                    foreach ($setUserId as $chvU => $valU) {
                        $TableUser  = new VirtualDeskTableTarefaUser($db);
                        $dataUser = array();
                        $dataUser['id_tarefa']     = $getTarefaId;
                        $dataUser['id_user']       = $valU;
                        $dataUser['createdby']     = $UserJoomlaID;
                        $dataUser['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableUser->save($dataUser)) {
                            $db->transactionRollback();
                            return false;
                        }
                        else {
                            # Envia email de tarefa para o user atribuído à tarefa
                            $objUserInTask = VirtualDeskSiteUserHelper::getUserObjById($valU);
                            $UsersName2Hist .= ' '. $objUserInTask->name. ' , ';
                            self::SendMailUserInTaskUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserInTask->name, $setNome, $setDesc, $objUserInTask->email, '');
                            $objUserInTask = null;
                        }
                    }
                }


                // Limpa os Users associados se vai haver alteração
                if($vbCheckAlteraGroup===true) {
                    $query = $db->getQuery(true);
                    $query->delete($db->quoteName('#__virtualdesk_tarefa_group'));
                    $query->where($db->quoteName('id_tarefa').'='.$getTarefaId);
                    $db->setQuery($query);
                    $resultUp02 = $db->execute();
                    if (!$resultUp02) {
                        $db->transactionRollback();
                        return false;
                    }
                }

                // Insere novos grupos
                $GroupsName2Hist = '';
                if(!empty($setGroupId) && is_array($setGroupId) && $vbCheckAlteraGroup===true) {
                    foreach ($setGroupId as $chvG => $valG) {
                        $TableGroup  = new VirtualDeskTableTarefaGroup($db);
                        $dataGroup = array();
                        $dataGroup['id_tarefa'] = $getTarefaId;
                        $dataGroup['id_group']      = $valG;
                        $dataGroup['createdby']     = $UserJoomlaID;
                        $dataGroup['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableGroup->save($dataGroup)) {
                            $db->transactionRollback();
                            return false;
                        } else {
                            # Envia email de tarefa para o grupo atribuído à tarefa. Se o grupo não tiver email envia para todos os utilizadores do grupo
                            $objGroupUserInTask   = $objCheckPerm->getGrupoOrUsersEmail2Obj($valG);
                            $GroupUserInTaskName  = $objGroupUserInTask->grupo->nome;
                            $GroupsName2Hist     .= ' '. $GroupUserInTaskName. ' , ';
                            $GroupUserInTaskEmail = '';
                            if(empty($objGroupUserInTask->grupo->email) || (string)$objGroupUserInTask->grupo->email == '') {
                                # envia para todos os utilizadores do grupo
                                if(!is_array($objGroupUserInTask->users)) $objGroupUserInTask->users = array();
                                foreach ($objGroupUserInTask->users as $chvGUsrDetail => $valGUsrDetail) {
                                    $GroupUserInTaskEmail .= $valGUsrDetail->email .',';
                                }
                            } else {
                                $GroupUserInTaskEmail = $objGroupUserInTask->grupo->email;
                            }

                            if(!empty($GroupUserInTaskEmail)) {
                                self::SendMailUserInTaskUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $GroupUserInTaskName, $setNome, $setDesc, $GroupUserInTaskEmail, '');
                            }
                            $objGroupUserInTask = null;
                        }
                    }
                }


                //Agora envia para o histórico da tarefa a indicar que foi iniciada
                $TableTrfHistorico  = new VirtualDeskTableTarefaHistorico($db);
                $dataTrfHistorico   = array();
                $dataTrfHistorico['id_tarefa'] = $getTarefaId;
                $dataTrfHistorico['descalt']   = JText::_('COM_VIRTUALDESK_TAREFA_ALTERAR_MSG');
                $dataTrfHistorico['obs']       = 'Usr: '.$UsersName2Hist . ' -  Grp: '.$GroupsName2Hist; //  colocar os utilizadores e grupos que foram atualizados
                $dataTrfHistorico['nome']      = (string) $dataTrf['nome'];
                $dataTrfHistorico['descricao'] = (string) $dataTrf['descricao'];
                $dataTrfHistorico['id_tarefa_estado'] = (int) $dataTrf['id_tarefa_estado'];
                $dataTrfHistorico['iduser']    = $UserVDId;
                $dataTrfHistorico['iduserjos'] = $UserJoomlaID;


                // Tabela Tarefa
                if (!$TableTrfHistorico->save($dataTrfHistorico)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                #$lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams                = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled         = $objVDParams->getParamsByTag('Tarefa_Log_Geral_Enabled');

                # Carrega os dados do utilizador que criou a tarefa... para poder ser avisado que foi alterado o estado.
                $objUserCreated = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID($TableTarefa->createdby);

                /* Send Email - Para Admin, Manager e User */
                self::SendMailAdminUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $TableTarefa->nome, $objUserVD->name, $objUserVD->email, '');
                if(!empty($objUserCreated->email)) {
                    self::SendMailUserWhoIniatedUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $objUserCreated->name, $TableTarefa->nome, $TableTarefa->descricao, $objUserCreated->email, '', '');
                }
                /* END Send Email */


                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_UPDATE');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_UPDATE') . ' TarefaId=' . $getTarefaId  . ' TipoPrc: '.$getProcessoTipo.' U '.implode(',',$setUserId).' ; G '.implode(',',$setGroupId); //  colocar os utilizadores e grupos que foram atualizados
                    $eventdata['desc']  .= '<br> '.json_encode($dataTrf);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = 'TrfId= '. $getTarefaId;
                    $vdlog->insertEventLog($eventdata);
                }


                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Grava a alteração de uma tarefa */
        public static function saveTarefaEditar4ManagerByAjax ($getTarefaId, $setEstadoId, $setNome, $setDesc, $setUserId, $setGroupId)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('tarefa');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('tarefa', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if((int)$getTarefaId <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Set Tabela da BD
            $db    = JFactory::getDbo();

            $TableTarefa = new VirtualDeskTableTarefa($db);
            $TableTarefa->load(array('id'=>$getTarefaId));
            if( (int)$TableTarefa->id <= 0 ) return false;

            // Verifica se houve alterações nos users e7ou nos grupos
            $getTrfUserLst  = self::getTarefaUserListById ($getTarefaId);
            $getTrfGroupLst = self::getTarefaGroupListById ($getTarefaId);

            // ordena os arrays por id
            $vbCheckAlteraUser = true;
            if(!is_array($getTrfUserLst)) {
                $vbCheckAlteraUser = false;
            }
            else {
                sort($getTrfUserLst);
                sort($setUserId);
                if($setUserId === $getTrfUserLst) $vbCheckAlteraUser = false;
            }

            $vbCheckAlteraGroup = true;
            if(!is_array($getTrfGroupLst)) {
                $vbCheckAlteraGroup = false;
            }
            else {
                sort($getTrfGroupLst);
                sort($setGroupId);
                if($setGroupId === $getTrfGroupLst) $vbCheckAlteraGroup = false;
            }

            $dataTrf = array();
            if( (int)$TableTarefa->id_tarefa_estado!=(int)$setEstadoId || (string)$TableTarefa->nome!=(string)$setNome  || (string)$TableTarefa->descricao!=(string)$setDesc || $vbCheckAlteraUser===true || $vbCheckAlteraGroup===true)  {
                $dateModified = new DateTime();
                $dataTrf['modified']   = $dateModified->format('Y-m-d H:i:s');
                $dataTrf['modifiedby'] = $UserJoomlaID;
            }
            else
            {   // não foi alterado o estado
                return true;
            }

            $db->transactionStart();

            try {
                // Parametros para os emails
                $obParam                    = new VirtualDeskSiteParamsHelper();
                $nomeMunicipio              = $obParam->getParamsByTag('nomeMunicipio');
                $dominioMunicipio           = $obParam->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral        = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP               = $obParam->getParamsByTag('copyrightAPP');
                $LinkCopyright              = $obParam->getParamsByTag('LinkCopyright');

                // Grava a alteração de estado da tarefa
                if( (int)$TableTarefa->id_tarefa_estado!=(int)$setEstadoId) {
                    $dataTrf['id_tarefa_estado'] = $setEstadoId;
                    // Se o estado a colocar for o concluído, coloca data de fim...?
                    $getIdEstadoConcluido = self::getEstadoIdConcluido();
                    $dataTrf['datafim'] = '';
                    if((int)$getIdEstadoConcluido == (int)$setEstadoId) $dataTrf['datafim'] = $dateModified->format('Y-m-d H:i:s');
                }
                if( (string)$TableTarefa->nome!=(string)$setNome) $dataTrf['nome'] = $setNome;
                if( (string)$TableTarefa->descricao!=(string)$setDesc) $dataTrf['descricao'] = $setDesc;


                // Tabela Tarefa
                if (!$TableTarefa->save($dataTrf)) {
                    $db->transactionRollback();
                    return false;
                }

                // Limpa os Users associados se vai haver alteração
                if($vbCheckAlteraUser===true) {
                    $query = $db->getQuery(true);
                    $query->delete($db->quoteName('#__virtualdesk_tarefa_user'));
                    $query->where($db->quoteName('id_tarefa').'='.$getTarefaId);
                    $db->setQuery($query);
                    $resultUp01 = $db->execute();
                    if (!$resultUp01) {
                        $db->transactionRollback();
                        return false;
                    }
                }

                # Altera novos utilizadores, se houve alterações
                $UsersName2Hist = '';
                if(!empty($setUserId) && is_array($setUserId) && $vbCheckAlteraUser===true) {
                    foreach ($setUserId as $chvU => $valU) {
                        $TableUser  = new VirtualDeskTableTarefaUser($db);
                        $dataUser = array();
                        $dataUser['id_tarefa']     = $getTarefaId;
                        $dataUser['id_user']       = $valU;
                        $dataUser['createdby']     = $UserJoomlaID;
                        $dataUser['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableUser->save($dataUser)) {
                            $db->transactionRollback();
                            return false;
                        }
                        else {
                            # Envia email de tarefa para o user atribuído à tarefa
                            $objUserInTask  = VirtualDeskSiteUserHelper::getUserObjById($valU);
                            $UsersName2Hist .= ' '. $objUserInTask->name. ' , ';
                            self::SendMailUserInTaskUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $objUserInTask->name, $setNome, $setDesc, $objUserInTask->email, '');
                            $objUserInTask = null;
                        }
                    }
                }


                // Limpa os Users associados se vai haver alteração
                if($vbCheckAlteraGroup===true) {
                    $query = $db->getQuery(true);
                    $query->delete($db->quoteName('#__virtualdesk_tarefa_group'));
                    $query->where($db->quoteName('id_tarefa').'='.$getTarefaId);
                    $db->setQuery($query);
                    $resultUp02 = $db->execute();
                    if (!$resultUp02) {
                        $db->transactionRollback();
                        return false;
                    }
                }

                // Insere novos grupos
                $GroupsName2Hist = '';
                if(!empty($setGroupId) && is_array($setGroupId) && $vbCheckAlteraGroup===true) {
                    foreach ($setGroupId as $chvG => $valG) {
                        $TableGroup  = new VirtualDeskTableTarefaGroup($db);
                        $dataGroup = array();
                        $dataGroup['id_tarefa'] = $getTarefaId;
                        $dataGroup['id_group']      = $valG;
                        $dataGroup['createdby']     = $UserJoomlaID;
                        $dataGroup['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableGroup->save($dataGroup)) {
                            $db->transactionRollback();
                            return false;
                        } else {
                            # Envia email de tarefa para o grupo atribuído à tarefa. Se o grupo não tiver email envia para todos os utilizadores do grupo
                            $objGroupUserInTask   = $objCheckPerm->getGrupoOrUsersEmail2Obj($valG);
                            $GroupUserInTaskName  = $objGroupUserInTask->grupo->nome;
                            $GroupsName2Hist     .= ' '. $GroupUserInTaskName. ' , ';
                            $GroupUserInTaskEmail = '';
                            if(empty($objGroupUserInTask->grupo->email) || (string)$objGroupUserInTask->grupo->email == '') {
                                # envia para todos os utilizadores do grupo
                                if(!is_array($objGroupUserInTask->users)) $objGroupUserInTask->users = array();
                                foreach ($objGroupUserInTask->users as $chvGUsrDetail => $valGUsrDetail) {
                                    $GroupUserInTaskEmail .= $valGUsrDetail->email .',';
                                }
                            } else {
                                $GroupUserInTaskEmail = $objGroupUserInTask->grupo->email;
                            }

                            if(!empty($GroupUserInTaskEmail)) {
                                self::SendMailUserInTaskUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $GroupUserInTaskName, $setNome, $setDesc, $GroupUserInTaskEmail, '');
                            }
                            $objGroupUserInTask = null;
                        }
                    }
                }


                //Agora envia para o histórico da tarefa a indicar que foi iniciada
                $TableTrfHistorico  = new VirtualDeskTableTarefaHistorico($db);
                $dataTrfHistorico   = array();
                $dataTrfHistorico['id_tarefa'] = $getTarefaId;
                $dataTrfHistorico['descalt']   = JText::_('COM_VIRTUALDESK_TAREFA_ALTERAR_MSG');
                $dataTrfHistorico['obs']       = 'Usr: '.$UsersName2Hist . ' -  Grp: '.$GroupsName2Hist; //  colocar os utilizadores e grupos que foram atualizados
                $dataTrfHistorico['nome']      = (string) $dataTrf['nome'];
                $dataTrfHistorico['descricao'] = (string) $dataTrf['descricao'];
                $dataTrfHistorico['id_tarefa_estado'] = (int) $dataTrf['id_tarefa_estado'];
                $dataTrfHistorico['iduser']    = $UserVDId;
                $dataTrfHistorico['iduserjos'] = $UserJoomlaID;


                // Tabela Tarefa
                if (!$TableTrfHistorico->save($dataTrfHistorico)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams                = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled         = $objVDParams->getParamsByTag('Tarefa_Log_Geral_Enabled');

                # Carrega os dados do utilizador que criou a tarefa... para poder ser avisado que foi alterado o estado.
                $objUserCreated = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID($TableTarefa->createdby);

                /* Send Email - Para Admin, Manager e User */
                self::SendMailAdminUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $TableTarefa->nome, $objUserVD->name, $objUserVD->email, '');
                if(!empty($objUserCreated->email)) {
                    self::SendMailUserWhoIniatedUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $objUserCreated->name, $TableTarefa->nome, $TableTarefa->descricao, $objUserCreated->email, '', '');
                }
                /* END Send Email */



                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_UPDATE');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_TAREFA_EVENTLOG_UPDATE') . ' TarefaId=' . $getTarefaId.' U '.implode(',',$setUserId).' ; G '.implode(',',$setGroupId); //  colocar os utilizadores e grupos que foram atualizados;
                    $eventdata['desc']  .= '<br> '.json_encode($dataTrf);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = 'TrfId= '. $getTarefaId;
                    $vdlog->insertEventLog($eventdata);
                }


                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Carrega lista com tarefas de um processo por exmeplo do alerta para acesso ao MANAGER */
        public static function getTarefasListByProcesso4Manager ($IdProcesso, $TagTipoProcesso, $vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess($TagTipoProcesso);                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess($TagTipoProcesso, 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess($TagTipoProcesso); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess($TagTipoProcesso,'view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdProcesso) || (int)$IdProcesso<=0 )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $db = JFactory::getDBO();

                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName   = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $TipoName   = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // Grava a tarefa e retorna o Id do novo registo
                $idTarefaTipoProcesso  = self::getTipoProcessoIdByTag($TagTipoProcesso);

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    ##$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id=');

                    $table  = "( SELECT b.id as id, b.nome as titulo, b.descricao as descricao, d.name as createdby, e.name as modifiedby, c.".$EstadoName." as estado, b.id_tarefa_estado as id_tarefa_estado";
                    $table .= " , DATE_FORMAT(b.created, '%Y-%m-%d') as created, DATE_FORMAT(b.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(b.modified, '%Y-%m-%d') as modified ";
                    $table .= " FROM ".$dbprefix."virtualdesk_tarefa_processo a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa AS b ON b.id = a.id_tarefa ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_estados AS c ON c.id = b.id_tarefa_estado ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.idjos = b.createdby ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS e ON e.id = b.modifiedby ";

                    $table .= " WHERE (a.id_processo=" . $db->escape($IdProcesso)." and a.id_tarefa_tipoprocesso=" . $db->escape($idTarefaTipoProcesso)." ) ";
                    $table .= " ) temp ";

                    $primaryKey = 'id';

                    # array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    $columns = array(
                        array( 'db' => 'createdFull',   'dt' => 0 ),
                        array( 'db' => 'titulo',        'dt' => 1 ),
                        array( 'db' => 'descricao',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'createdby',     'dt' => 4 ),
                        array( 'db' => 'modifiedby',    'dt' => 5 ),
                        array( 'db' => 'created',       'dt' => 6 ),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'id',            'dt' => 8 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'id_tarefa_estado', 'dt' => 9 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;



                /*
                $db->setQuery($db->getQuery(true)
                  ->select(array(' a.id as id', 'a.idalerta as alerta_id', "IFNULL(b.".$TipoName.",'') as tipo", 'a.mensagem as mensagem', 'a.anulado as anulado', 'a.setbymanager as setbymanager'
                      , "DATE_FORMAT(a.created, '%Y-%m-%d') as created", "DATE_FORMAT(a.modified, '%Y-%m-%d') as modified ", "DATE_FORMAT(a.created, '%Y-%m-%d %H:%i') as createdFull"
                      ,'a.idtipo as idtipo' , " '1' AS dummyuser ", " '1' AS dummymanager ",  'a.iduser', 'a.iduserjos',  'd.name as username'
                  ))
                  ->join('LEFT', '#__virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo')
                  ->join('LEFT', '#__virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta')
                  ->join('LEFT', '#__virtualdesk_users AS d ON d.id = a.iduser')
                  ->from("#__virtualdesk_alerta_historico as a")
                  ->where( $db->quoteName('a.idalerta').'='.$db->escape($IdAlerta) . ' and a.anulado<>1 and visible4user=1 and ' . $db->quoteName('c.nif').'='.$db->escape($UserSessionNIF) )
                  ->order(' a.created DESC ' . $setLimitSQL)
                );*/

                /* $queryFull  = " SELECT a.id as id,a.idalerta as alerta_id,IFNULL(b.name_PT,'') as tipo,a.mensagem as mensagem,a.anulado as anulado,a.setbymanager as setbymanager,DATE_FORMAT(a.created, '%Y-%m-%d') as created,DATE_FORMAT(a.modified, '%Y-%m-%d') as modified ,DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull,a.idtipo as idtipo, '1' AS dummyuser , '1' AS dummymanager ,a.iduser,a.iduserjos,d.name as username";
                $queryFull .= " FROM gtonq_virtualdesk_alerta_historico as a ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_users AS d ON d.id = a.iduser";
                $queryFull .= " WHERE `a`.`idalerta`=".$db->escape($IdAlerta)." and a.anulado<>1 ";
                $queryFull .= " UNION ";
                $queryFull .= " SELECT '' as id,a2.Id_alerta as alerta_id, '".JText::_('COM_VIRTUALDESK_INITIATED')."' as tipo, '".JText::_('COM_VIRTUALDESK_INITIATED')."' as mensagem, '' as anulado, '' as setbymanager,DATE_FORMAT(a2.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a2.data_alteracao, '%Y-%m-%d')  as modified ,DATE_FORMAT(a2.data_criacao, '%Y-%m-%d %H:%i:%s') as createdFull, 4 as idtipo, '1' AS dummyuser , '1' AS dummymanager , '' as iduser, '' as iduserjos, '' as username ";
                $queryFull .= " FROM gtonq_virtualdesk_alerta as a2";
                $queryFull .= " WHERE `a2`.`Id_alerta`=".$db->escape($IdAlerta)." ";
                $queryFull .= " ORDER BY createdFull DESC ";

                $db->setQuery($queryFull);
                $dataReturn = $db->loadObjectList();
                */

                $dataReturn = array();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista de TODAS as tarefas para acesso aos MANAGERS */
        public static function getTarefasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('tarefa'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','list4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $db = JFactory::getDBO();

                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName   = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $TipoName   = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    $obParam    = new VirtualDeskSiteParamsHelper();

                    // Gerar Links para o detalhe...
                    $AlertaMenuId4Manager = $obParam->getParamsByTag('Alerta_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Alerta =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&Itemid='.$AlertaMenuId4Manager.'&alerta_id=');


                    $table  = " (  ";
                    $table .= " SELECT  id, idAll, dummy, titulo, descricao, createdby, modifiedby, estado, id_tarefa_estado, id_processo, processo_tagchave, created, createdFull, modified FROM ( ";

                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleAlerta = $objCheckPerm->loadModuleEnabledByTag('alerta');
                    if((int)$vbChecModuleAlerta==1) {
                        // Alerta
                        $table .= " SELECT a.id as id, CONCAT('ALRT',CONVERT(a.id , CHAR(50))) as idAll, CONCAT('" . $dummyHRef_Alerta . "', CAST(b.id_processo as CHAR(10)) , '#tabAlertaTarefas') as dummy , a.nome as titulo, a.descricao as descricao ";
                        $table .= " , d.name as createdby, e.name as modifiedby, IFNULL(c.".$EstadoName.", ' ') as estado, a.id_tarefa_estado as id_tarefa_estado";
                        $table .= " , b.id_processo as id_processo, f.tagchave as processo_tagchave ";
                        $table .= " , DATE_FORMAT(a.created, '%Y-%m-%d') as created, DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(a.modified, '%Y-%m-%d') as modified ";
                        $table .= " FROM ".$dbprefix."virtualdesk_tarefa a ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_processo AS b ON b.id_tarefa = a.id ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_estados AS c ON c.id = a.id_tarefa_estado ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.idjos = a.createdby ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS e ON e.id = a.modifiedby ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_tipoprocesso AS f ON f.id = a.id_tarefa_tipoprocesso ";
                        $table .= " WHERE f.tagchave = 'alerta' ";
                    }

                    /*
                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('agenda');
                    if((int)$vbChecModuleAgenda==1) {
                        $table .= " UNION ";
                        // Agenda
                    }
                    */

                    $table .= " ) as allUnion ";
                    $table .= " ) temp ";

                    $primaryKey = 'idAll';

                    $columns = array(
                        array( 'db' => 'createdFull',   'dt' => 0 ),
                        array( 'db' => 'titulo',        'dt' => 1 ),
                        array( 'db' => 'descricao',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'createdby',     'dt' => 4 ),
                        array( 'db' => 'modifiedby',    'dt' => 5 ),
                        array( 'db' => 'created',       'dt' => 6 ),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'id',            'dt' => 8 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'id_tarefa_estado',     'dt' => 9 ),
                        array( 'db' => 'id_processo',          'dt' => 10 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'processo_tagchave',    'dt' => 11 ),
                        array( 'db' => 'idAll',                'dt' => 12 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'dummy',                'dt' => 13 ,'formatter'=>'URL_ENCRYPT')
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***

                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                $dataReturn = array();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista das tarefas de 1 utilizador MANAGERS */
        public static function getTarefasMinhasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('tarefa'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','list4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            try
            {
                $db = JFactory::getDBO();

                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName   = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $TipoName   = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    $obParam    = new VirtualDeskSiteParamsHelper();

                    // Gerar Links para o detalhe...
                    $AlertaMenuId4Manager = $obParam->getParamsByTag('Alerta_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Alerta =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&Itemid='.$AlertaMenuId4Manager.'&alerta_id=');


                    $table  = " (  ";
                    $table .= " SELECT  id, idAll, dummy, titulo, descricao, createdby, modifiedby, estado, id_tarefa_estado, id_processo, processo_tagchave, created, createdFull, modified FROM ( ";

                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleAlerta = $objCheckPerm->loadModuleEnabledByTag('alerta');
                    if((int)$vbChecModuleAlerta==1) {
                        // Alerta

                        $table .= " SELECT g.id as id, CONCAT('ALRT',CONVERT(g.id , CHAR(50))) as idAll, CONCAT('" . $dummyHRef_Alerta . "', CAST(b.id_processo as CHAR(10)) , '#tabAlertaTarefas') as dummy ,  g.nome  as titulo,  g.descricao as descricao ";
                        $table .= " , d.name as createdby, e.name as modifiedby, IFNULL(c.".$EstadoName.", ' ') as estado, g.id_tarefa_estado as id_tarefa_estado";
                        $table .= " , b.id_processo as id_processo, f.tagchave as processo_tagchave ";
                        $table .= " , DATE_FORMAT(g.created, '%Y-%m-%d') as created, DATE_FORMAT(g.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(g.modified, '%Y-%m-%d') as modified ";
                        $table .= " FROM ".$dbprefix."virtualdesk_tarefa_user a ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_processo AS b ON b.id_tarefa = a.id_tarefa ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_tipoprocesso AS f ON f.id = b.id_tarefa_tipoprocesso ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa AS g ON g.id = a.id_tarefa ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.idjos = g.createdby ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS e ON e.id = g.modifiedby ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_estados AS c ON c.id = g.id_tarefa_estado ";
                        $table .= " WHERE  (f.tagchave = 'alerta') AND ( a.id_user=" . $db->escape($UserVDId)." ) ";
                    }

                    /*
                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('agenda');
                    if((int)$vbChecModuleAgenda==1) {
                        $table .= " UNION ";
                        // Agenda
                    }
                    */

                    $table .= " ) as allUnion ";
                    $table .= " ) temp ";

                    $primaryKey = 'idAll';

                    $columns = array(
                        array( 'db' => 'createdFull',   'dt' => 0 ),
                        array( 'db' => 'titulo',        'dt' => 1 ),
                        array( 'db' => 'descricao',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'createdby',     'dt' => 4 ),
                        array( 'db' => 'modifiedby',    'dt' => 5 ),
                        array( 'db' => 'created',       'dt' => 6 ),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'id',            'dt' => 8 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'id_tarefa_estado',     'dt' => 9 ),
                        array( 'db' => 'id_processo',          'dt' => 10 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'processo_tagchave',    'dt' => 11 ),
                        array( 'db' => 'idAll',                'dt' => 12 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'dummy',                'dt' => 13 ,'formatter'=>'URL_ENCRYPT')
                    );


                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***

                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                $dataReturn = array();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista das tarefas de 1 utilizador MANAGERS */
        public static function getTarefasMeusGruposList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('tarefa'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','list4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            try
            {
                $db = JFactory::getDBO();

                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName   = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $TipoName   = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $arGroupsFromUser = $objCheckPerm->loadGroupsFromUser($UserJoomlaID);
                    $GroupsFromUser   = implode(",", $arGroupsFromUser);

                    if(empty($GroupsFromUser)) return ('');

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');


                    $obParam    = new VirtualDeskSiteParamsHelper();

                    // Gerar Links para o detalhe...
                    $AlertaMenuId4Manager = $obParam->getParamsByTag('Alerta_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Alerta =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&Itemid='.$AlertaMenuId4Manager.'&alerta_id=');

                    $table  = " (  ";
                    $table .= " SELECT  id, idAll, dummy, titulo, descricao, createdby, modifiedby, estado, id_tarefa_estado, id_processo, processo_tagchave, created, createdFull, modified FROM ( ";

                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleAlerta = $objCheckPerm->loadModuleEnabledByTag('alerta');
                    if((int)$vbChecModuleAlerta==1) {
                        // Alerta

                        $table .= " SELECT g.id as id, CONCAT('ALRT',CONVERT(g.id , CHAR(50))) as idAll, CONCAT('" . $dummyHRef_Alerta . "', CAST(b.id_processo as CHAR(10)) , '#tabAlertaTarefas') as dummy ,  g.nome  as titulo,  g.descricao as descricao ";
                        $table .= " , d.name as createdby, e.name as modifiedby, IFNULL(c.".$EstadoName.", ' ') as estado, g.id_tarefa_estado as id_tarefa_estado";
                        $table .= " , b.id_processo as id_processo, f.tagchave as processo_tagchave ";
                        $table .= " , DATE_FORMAT(g.created, '%Y-%m-%d') as created, DATE_FORMAT(g.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(g.modified, '%Y-%m-%d') as modified ";
                        $table .= " FROM ".$dbprefix."virtualdesk_tarefa_group a ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_processo AS b ON b.id_tarefa = a.id_tarefa ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_tipoprocesso AS f ON f.id = b.id_tarefa_tipoprocesso ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa AS g ON g.id = a.id_tarefa ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.idjos = g.createdby ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS e ON e.id = g.modifiedby ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_estados AS c ON c.id = g.id_tarefa_estado ";
                        $table .= " WHERE (f.tagchave = 'alerta') AND ( a.id_group in (" . $db->escape($GroupsFromUser).") ) ";
                    }

                    /*
                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('agenda');
                    if((int)$vbChecModuleAgenda==1) {
                        $table .= " UNION ";
                        // Agenda
                    }
                    */

                    $table .= " ) as allUnion ";
                    $table .= " ) temp ";

                    $primaryKey = 'idAll';

                    $columns = array(
                        array( 'db' => 'createdFull',   'dt' => 0 ),
                        array( 'db' => 'titulo',        'dt' => 1 ),
                        array( 'db' => 'descricao',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'createdby',     'dt' => 4 ),
                        array( 'db' => 'modifiedby',    'dt' => 5 ),
                        array( 'db' => 'created',       'dt' => 6 ),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'id',            'dt' => 8 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'id_tarefa_estado',     'dt' => 9 ),
                        array( 'db' => 'id_processo',          'dt' => 10 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'processo_tagchave',    'dt' => 11 ),
                        array( 'db' => 'idAll',                'dt' => 12 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'dummy',                'dt' => 13 ,'formatter'=>'URL_ENCRYPT')
                    );


                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***

                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                $dataReturn = array();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com tarefas obter as tarefas que não estão concluídas nem estão anuladas para um ID processo, por exemplo para um alerta, acesso para MANAGER do processo */
        public static function getTarefasListaAbertasByProcesso4Manager ($IdProcesso, $TagTipoProcesso)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess($TagTipoProcesso);                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess($TagTipoProcesso, 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess($TagTipoProcesso); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess($TagTipoProcesso,'view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdProcesso) || (int)$IdProcesso<=0 )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $IdEstadoConcluido = self::getEstadoIdConcluido();
                $IdEstadoAnulado = self::getEstadoIdAnulado();

                $db = JFactory::getDBO();
                $idTarefaTipoProcesso  = self::getTipoProcessoIdByTag($TagTipoProcesso);
                // Retirei Os IDs porque não estão a ser utilizados e temos depois de alguma forma encriptar estes IDS
                // 'a.id as id', , 'c.id_processo as id_processo'
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id_tarefa_estado as id_tarefa_estado'
                                  , 'a.nome as nome', 'a.descricao as descricao','DATE_FORMAT(a.created, "%Y-%m-%d") as created'))
                    ->join('LEFT', '#__virtualdesk_tarefa_estados AS b ON b.id = a.id_tarefa_estado')
                    ->join('LEFT', '#__virtualdesk_tarefa_processo AS c ON c.id_tarefa = a.id')
                    ->from("#__virtualdesk_tarefa as a")
                    ->where( 'a.id_tarefa_estado not in ('.$IdEstadoConcluido.','.$IdEstadoAnulado.') and c.id_processo=' . $db->escape($IdProcesso).' and a.id_tarefa_tipoprocesso=' . $db->escape($idTarefaTipoProcesso) ));
                $dataReturn = $db->loadObjectList();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com TODAS as tarefas que não estão concluídas nem estão anuladas , acesso para MANAGER */
        public static function getTarefasListaAbertas4Manager ()
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('tarefa'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','list4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $IdEstadoConcluido = self::getEstadoIdConcluido();
                $IdEstadoAnulado = self::getEstadoIdAnulado();

                $obParam          = new VirtualDeskSiteParamsHelper();
                $avisosMaxRecords = (int)$obParam->getParamsByTag('avisosBarraSuperiorGeralMaxRecords');

                $db = JFactory::getDBO();
                // Retirei Os IDs porque não estão a ser utilizados e temos depois de alguma forma encriptar estes IDS
                // 'a.id as id', , 'c.id_processo as id_processo'
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id_tarefa_estado as id_tarefa_estado', 'a.nome as nome'
                                 , 'a.descricao as descricao','DATE_FORMAT(a.created, "%Y-%m-%d") as created'))
                    ->join('LEFT', '#__virtualdesk_tarefa_estados AS b ON b.id = a.id_tarefa_estado')
                    ->join('LEFT', '#__virtualdesk_tarefa_processo AS c ON c.id_tarefa = a.id')
                    ->from("#__virtualdesk_tarefa as a")
                    ->where( 'a.id_tarefa_estado not in ('.$IdEstadoConcluido.','.$IdEstadoAnulado.')' )
                );
                //$dummyHRef  =  JRoute::_('index.php?option=com_virtualdesk&view=tarefa&layout=view4manager&tarefa_id=');
                $dataReturn = $db->loadObjectList();
                //$obVDCrypt  = new VirtualDeskSiteCryptHelper();
                //$params     = JComponentHelper::getParams('com_virtualdesk');
                //$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
                $dataReturnCut       = array();
                $dataReturnCut['dt'] = array();
                $dataReturnCut['nr'] = 0;
                $counti = 0;
                foreach ($dataReturn as $keyR => $valR) {
                    $dataReturnCut['dt'][$keyR]        = $valR;
                    //$dataReturnCut['dt'][$keyR]->dummy = $dummyHRef.$valR->dmId;
                    //if($setencrypt_forminputhidden==1) $dataReturnCut['dt'][$keyR]->dummy = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($dummyHRef.$valR->dmId);
                    //unset($dataReturnCut['dt'][$keyR]->dmId);
                    $counti++;
                    if($counti>=(int)$avisosMaxRecords) break;
                }
                $dataReturnCut['nr'] = sizeof($dataReturn);
                return($dataReturnCut);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com as tarefas do USER SESSION que não estão concluídas nem estão anuladas , acesso para MANAGER */
        public static function getTarefasMinhasListaAbertas4Manager ()
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('tarefa'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','list4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            try
            {
                $IdEstadoConcluido = self::getEstadoIdConcluido();
                $IdEstadoAnulado = self::getEstadoIdAnulado();

                $obParam          = new VirtualDeskSiteParamsHelper();
                $avisosMaxRecords = (int)$obParam->getParamsByTag('avisosBarraSuperiorGeralMaxRecords');

                $db = JFactory::getDBO();
                // Retirei Os IDs porque não estão a ser utilizados e temos depois de alguma forma encriptar estes IDS
                // 'a.id_tarefa as id_tarefa',
                $db->setQuery($db->getQuery(true)
                    ->select(array('b.id_tarefa_estado as id_tarefa_estado', 'b.nome as nome', 'b.descricao as descricao'
                    , 'DATE_FORMAT(a.created, "%Y-%m-%d") as created'))
                    ->join('LEFT', '#__virtualdesk_tarefa AS b ON b.id = a.id_tarefa')
                    ->join('LEFT', '#__virtualdesk_tarefa_estados AS c ON c.id = b.id_tarefa_estado')
                    ->from("#__virtualdesk_tarefa_user as a")
                    ->where( ' a.id_user='. $db->escape($UserVDId).' and b.id_tarefa_estado not in ('.$IdEstadoConcluido.','.$IdEstadoAnulado.')')
                );

                //$dummyHRef  =  JRoute::_('index.php?option=com_virtualdesk&view=tarefa&layout=view4manager&tarefa_id=');
                $dataReturn = $db->loadObjectList();
                //$obVDCrypt  = new VirtualDeskSiteCryptHelper();
                //$params     = JComponentHelper::getParams('com_virtualdesk');
                //$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
                $dataReturnCut       = array();
                $dataReturnCut['dt'] = array();
                $dataReturnCut['nr'] = 0;
                $counti = 0;
                foreach ($dataReturn as $keyR => $valR) {
                    $dataReturnCut['dt'][$keyR]        = $valR;
                    //$dataReturnCut['dt'][$keyR]->dummy = $dummyHRef.$valR->dmId;
                    //if($setencrypt_forminputhidden==1) $dataReturnCut['dt'][$keyR]->dummy = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($dummyHRef.$valR->dmId);
                    //unset($dataReturnCut['dt'][$keyR]->dmId);
                    $counti++;
                    if($counti>=(int)$avisosMaxRecords) break;
                }
                $dataReturnCut['nr'] = sizeof($dataReturn);
                return($dataReturnCut);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com as tarefas dos GRUPOS do USER SESSION que não estão concluídas nem estão anuladas , acesso para MANAGER */
        public static function getTarefasMeusGruposListaAbertas4Manager ()
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('tarefa'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','list4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            try
            {
                $arGroupsFromUser = $objCheckPerm->loadGroupsFromUser($UserJoomlaID);
                $GroupsFromUser   = implode(",", $arGroupsFromUser);

                $IdEstadoConcluido = self::getEstadoIdConcluido();
                $IdEstadoAnulado = self::getEstadoIdAnulado();

                $obParam          = new VirtualDeskSiteParamsHelper();
                $avisosMaxRecords = (int)$obParam->getParamsByTag('avisosBarraSuperiorGeralMaxRecords');

                $db = JFactory::getDBO();
                // Retirei Os IDs porque não estão a ser utilizados e temos depois de alguma forma encriptar estes IDS
                // 'a.id_tarefa as id_tarefa',
                $db->setQuery($db->getQuery(true)
                    ->select(array('b.id_tarefa_estado as id_tarefa_estado', 'b.nome as nome', 'b.descricao as descricao'
                    , 'DATE_FORMAT(a.created, "%Y-%m-%d") as created'))
                    ->join('LEFT', '#__virtualdesk_tarefa AS b ON b.id = a.id_tarefa')
                    ->join('LEFT', '#__virtualdesk_tarefa_estados AS c ON c.id = b.id_tarefa_estado')
                    ->from("#__virtualdesk_tarefa_group as a")
                    ->where( ' a.id_group in ('. $db->escape($GroupsFromUser).') and b.id_tarefa_estado not in ('.$IdEstadoConcluido.','.$IdEstadoAnulado.')')
                );

                //$dummyHRef  =  JRoute::_('index.php?option=com_virtualdesk&view=tarefa&layout=view4manager&tarefa_id=');
                $dataReturn = $db->loadObjectList();
                //$obVDCrypt  = new VirtualDeskSiteCryptHelper();
                //$params     = JComponentHelper::getParams('com_virtualdesk');
                //$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
                $dataReturnCut       = array();
                $dataReturnCut['dt'] = array();
                $dataReturnCut['nr'] = 0;
                $counti = 0;
                foreach ($dataReturn as $keyR => $valR) {
                    $dataReturnCut['dt'][$keyR]        = $valR;
                    //$dataReturnCut['dt'][$keyR]->dummy = $dummyHRef.$valR->dmId;
                    //if($setencrypt_forminputhidden==1) $dataReturnCut['dt'][$keyR]->dummy = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($dummyHRef.$valR->dmId);
                    //unset($dataReturnCut['dt'][$keyR]->dmId);
                    $counti++;
                    if($counti>=(int)$avisosMaxRecords) break;
                }
                $dataReturnCut['nr'] = sizeof($dataReturn);
                return($dataReturnCut);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega detalhe da tarefa associados a um id processo, por exemplo um alerta  */
        public static function getTarefaDetailByProcesso4Manager ($IdProcesso, $IdTarefa, $TagTipoProcesso)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess($TagTipoProcesso, 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess($TagTipoProcesso,'list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( (int)$IdTarefa <=0 || (int)$IdProcesso <=0)  return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if( (int)$UserJoomlaID<=0 ) return false;

            try
            {
                $idTarefaTipoProcesso  = self::getTipoProcessoIdByTag($TagTipoProcesso);

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id','a.id_tarefa_estado as id_tarefa_estado', 'a.nome as nome', 'a.descricao as descricao'
                    , 'c.id_processo as id_processo', 'DATE_FORMAT(a.datafim, "%Y-%m-%d %H:%i:%s") as datafim'  ))
                    ->join('LEFT', '#__virtualdesk_tarefa_estados AS b ON b.id = a.id_tarefa_estado')
                    ->join('LEFT', '#__virtualdesk_tarefa_processo AS c ON c.id_tarefa = a.id')
                    ->from("#__virtualdesk_tarefa as a")
                    ->where( 'a.id='.$db->escape($IdTarefa).' and c.id_processo=' . $db->escape($IdProcesso).' and a.id_tarefa_tipoprocesso=' . $db->escape($idTarefaTipoProcesso) )
                );
                $dataReturnTrf = $db->loadObject();
                if((int)$dataReturnTrf->id_processo!=(int)$IdProcesso) return false;

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id','a.id_user as id_user'))
                    ->join('LEFT', '#__virtualdesk_users AS b ON b.id = a.id_user')
                    ->from("#__virtualdesk_tarefa_user as a")
                    ->where( 'a.id_tarefa='.$db->escape($IdTarefa)  )
                );
                $dataReturnUser = $db->loadObjectList();

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id','a.id_group as id_group'))
                    ->join('LEFT', '#__virtualdesk_perm_groups AS b ON b.id = a.id_group')
                    ->from("#__virtualdesk_tarefa_group as a")
                    ->where( ' a.id_tarefa='.$db->escape($IdTarefa)  )
                );
                $dataReturnGroup = $db->loadObjectList();

                // Se estiver definida a encriptação, vai encriptar os valores do array
                $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
                $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');

                $dataReturnTrf->id          = $obVDCrypt->setIdInputValueEncrypt($dataReturnTrf->id, $setencrypt_forminputhidden);
                $dataReturnTrf->id_processo = $obVDCrypt->setIdInputValueEncrypt($dataReturnTrf->id_processo, $setencrypt_forminputhidden);

                foreach($dataReturnUser as $keyU => $valU) {
                    $dataReturnUser[$keyU]->id      = $obVDCrypt->setIdInputValueEncrypt($valU->id, $setencrypt_forminputhidden);
                    $dataReturnUser[$keyU]->id_user = $obVDCrypt->setIdInputValueEncrypt($valU->id_user, $setencrypt_forminputhidden);
                }

                foreach($dataReturnGroup as $keyG => $valG) {
                    $dataReturnGroup[$keyG]->id      = $obVDCrypt->setIdInputValueEncrypt($valG->id, $setencrypt_forminputhidden);
                    $dataReturnGroup[$keyG]->id_group = $obVDCrypt->setIdInputValueEncrypt($valG->id_group, $setencrypt_forminputhidden);
                }


                $dataReturn['trf'] = $dataReturnTrf;
                $dataReturn['usr'] = $dataReturnUser;
                $dataReturn['grp'] = $dataReturnGroup;

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega detalhe da tarefa   */
        public static function getTarefaDetail4Manager ($IdTarefa)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('tarefa','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( (int)$IdTarefa <=0 )  return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if( (int)$UserJoomlaID<=0 ) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id','a.id_tarefa_estado as id_tarefa_estado', 'a.nome as nome', 'a.descricao as descricao'
                    , 'c.id_processo as id_processo', 'DATE_FORMAT(a.datafim, "%Y-%m-%d %H:%i:%s") as datafim'  ))
                    ->join('LEFT', '#__virtualdesk_tarefa_estados AS b ON b.id = a.id_tarefa_estado')
                    ->join('LEFT', '#__virtualdesk_tarefa_processo AS c ON c.id_tarefa = a.id')
                    ->from("#__virtualdesk_tarefa as a")
                    ->where( 'a.id='.$db->escape($IdTarefa) )
                );
                $dataReturnTrf = $db->loadObject();

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id','a.id_user as id_user'))
                    ->join('LEFT', '#__virtualdesk_users AS b ON b.id = a.id_user')
                    ->from("#__virtualdesk_tarefa_user as a")
                    ->where( 'a.id_tarefa='.$db->escape($IdTarefa)  )
                );
                $dataReturnUser = $db->loadObjectList();

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id','a.id_group as id_group'))
                    ->join('LEFT', '#__virtualdesk_perm_groups AS b ON b.id = a.id_group')
                    ->from("#__virtualdesk_tarefa_group as a")
                    ->where( ' a.id_tarefa='.$db->escape($IdTarefa)  )
                );
                $dataReturnGroup = $db->loadObjectList();

                // Se estiver definida a encriptação, vai encriptar os valores do array
                $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
                $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');

                $dataReturnTrf->id          = $obVDCrypt->setIdInputValueEncrypt($dataReturnTrf->id, $setencrypt_forminputhidden);
                $dataReturnTrf->id_processo = $obVDCrypt->setIdInputValueEncrypt($dataReturnTrf->id_processo, $setencrypt_forminputhidden);

                foreach($dataReturnUser as $keyU => $valU) {
                    $dataReturnUser[$keyU]->id      = $obVDCrypt->setIdInputValueEncrypt($valU->id, $setencrypt_forminputhidden);
                    $dataReturnUser[$keyU]->id_user = $obVDCrypt->setIdInputValueEncrypt($valU->id_user, $setencrypt_forminputhidden);
                }

                foreach($dataReturnGroup as $keyG => $valG) {
                    $dataReturnGroup[$keyG]->id      = $obVDCrypt->setIdInputValueEncrypt($valG->id, $setencrypt_forminputhidden);
                    $dataReturnGroup[$keyG]->id_group = $obVDCrypt->setIdInputValueEncrypt($valG->id_group, $setencrypt_forminputhidden);
                }

                $dataReturn['trf'] = $dataReturnTrf;
                $dataReturn['usr'] = $dataReturnUser;
                $dataReturn['grp'] = $dataReturnGroup;

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com dados do histórico de uma alerta para acesso ao MANAGER */
        public static function getTarefasHistoricoListByProcesso4Manager ($IdProcesso, $IdTarefa, $TagTipoProcesso, $vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess($TagTipoProcesso);                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess($TagTipoProcesso, 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess($TagTipoProcesso); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess($TagTipoProcesso,'view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $data = array();

            if( empty($IdProcesso) || (int)$IdProcesso<=0 )  return $data;
            if( empty($IdTarefa) || (int)$IdTarefa<=0 )  return $data;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return $data;
            if( (int)$UserJoomlaID<=0 )  return $data;

            try
            {
                $db = JFactory::getDBO();

                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName   = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $TipoName   = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // Grava a tarefa e retorna o Id do novo registo
                $idTarefaTipoProcesso  = self::getTipoProcessoIdByTag($TagTipoProcesso);

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    ##$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id=');

                    $table  = "( SELECT c.id as id, c.descalt as descalt, c.obs as obs, c.nome as nome, c.descricao as descricao, d.name as createdby ";
                    $table .= " , DATE_FORMAT(c.created, '%Y-%m-%d') as created, DATE_FORMAT(c.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(c.modified, '%Y-%m-%d') as modified ";
                    $table .= " FROM ".$dbprefix."virtualdesk_tarefa_processo a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa AS b ON b.id = a.id_tarefa ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_historico AS c ON c.id_tarefa = b.id ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.idjos = c.iduserjos";
                    $table .= " WHERE (a.id_processo=" . $db->escape($IdProcesso)." and a.id_tarefa_tipoprocesso=" . $db->escape($idTarefaTipoProcesso)." and b.id=" . $db->escape($IdTarefa)." ) ";
                    $table .= " ) temp ";

                    $primaryKey = 'id';

                    # array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    $columns = array(
                        array( 'db' => 'createdFull',   'dt' => 0 ),
                        array( 'db' => 'descalt',       'dt' => 1 ),
                        array( 'db' => 'obs',           'dt' => 2 ),
                        array( 'db' => 'nome',          'dt' => 3 ),
                        array( 'db' => 'descricao',     'dt' => 4 ),
                        array( 'db' => 'createdby',     'dt' => 5 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }



                $dataReturn = array();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com dados do histórico da um tarefa para acesso ao MANAGER */
        public static function getTarefasHistoricoList4Manager ($IdTarefa, $vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('tarefa', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('tarefa'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('tarefa','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $data = array();

            if( empty($IdTarefa) || (int)$IdTarefa<=0 )  return $data;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return $data;
            if( (int)$UserJoomlaID<=0 )  return $data;

            try
            {
                $db = JFactory::getDBO();

                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName   = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $TipoName   = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    ##$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id=');

                    $table  = "( SELECT c.id as id, c.descalt as descalt, c.obs as obs, c.nome as nome, c.descricao as descricao, d.name as createdby ";
                    $table .= " , DATE_FORMAT(c.created, '%Y-%m-%d') as created, DATE_FORMAT(c.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(c.modified, '%Y-%m-%d') as modified ";
                    $table .= " FROM ".$dbprefix."virtualdesk_tarefa_processo a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa AS b ON b.id = a.id_tarefa ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tarefa_historico AS c ON c.id_tarefa = b.id ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.idjos = c.iduserjos";
                    $table .= " WHERE ( b.id=" . $db->escape($IdTarefa)." ) ";
                    $table .= " ) temp ";

                    $primaryKey = 'id';

                    # array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    $columns = array(
                        array( 'db' => 'createdFull',   'dt' => 0 ),
                        array( 'db' => 'descalt',       'dt' => 1 ),
                        array( 'db' => 'obs',           'dt' => 2 ),
                        array( 'db' => 'nome',          'dt' => 3 ),
                        array( 'db' => 'descricao',     'dt' => 4 ),
                        array( 'db' => 'createdby',     'dt' => 5 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }



                $dataReturn = array();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com os estados das tarefas para serem utilizados num processo como o Alerta, etc*/
        public static function getEstadoListAllBy2Process ($TipoProcessoTag)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $EstadoName = 'estado_PT';
                }
                else {
                    $EstadoName = 'estado_EN';
                }

                $db->setQuery($db->getQuery(true)
                    ->select( array('id','a.'.$EstadoName.' as name'))
                    ->from("#__virtualdesk_tarefa_estados as a")
                );
                $response = $db->loadAssocList();
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os estados das tarefas p*/
        public static function getEstadoListAll ()
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('tarefa');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $EstadoName = 'estado_PT';
                }
                else {
                    $EstadoName = 'estado_EN';
                }

                $db->setQuery($db->getQuery(true)
                    ->select( array('id','a.'.$EstadoName.' as name'))
                    ->from("#__virtualdesk_tarefa_estados as a")
                );
                $response = $db->loadAssocList();
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Retorna Id Concluido de uma tarefa*/
        public static function getEstadoIdConcluido ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id'))
                    ->from("#__virtualdesk_tarefa_estados as a")
                    ->where(" a.bitEnd=1 ")
                );
                $response = $db->loadObject();
                return($response->id);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Retorna Id Anuladode uma tarefa*/
        public static function getEstadoIdAnulado ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id'))
                    ->from("#__virtualdesk_tarefa_estados as a")
                    ->where(" a.bitCancel=1 ")
                );
                $response = $db->loadObject();
                return($response->id);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os estados das tarefas para serem utilizados num processo como o Alerta, etc*/
        public static function getEstadoNomeById ($IdTarefaEstado)
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $EstadoName = 'estado_PT';
                }
                else {
                    $EstadoName = 'estado_EN';
                }

                $db->setQuery($db->getQuery(true)
                    ->select( array('id','a.'.$EstadoName.' as name'))
                    ->from("#__virtualdesk_tarefa_estados as a")
                    ->where(" a.id =".$db->escape($IdTarefaEstado))
                );
                $response = $db->loadObject();
                return($response->name);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega array com Îds dos utilizadores associados a uma tarefa */
        public static function getTarefaUserListById ($IdTarefa)
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id_user'))
                    ->from("#__virtualdesk_tarefa_user as a")
                    ->where(" a.id_tarefa =".$db->escape($IdTarefa))
                );
                $response = $db->loadColumn();
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega array com Îds dos grupos associados a uma tarefa */
        public static function getTarefaGroupListById ($IdTarefa)
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id_group'))
                    ->from("#__virtualdesk_tarefa_group as a")
                    ->where(" a.id_tarefa =".$db->escape($IdTarefa))
                );
                $response = $db->loadColumn();
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Devolve CSS a ser aplicado de acordo com o ESTADO atual */
        public static function getTarefaEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                    //Nova
                    $defCss = 'label-nova';
                    break;
                case '2':
                    // Concluído
                    $defCss = 'label-concluido';
                    break;
            }
            return ($defCss);
        }

        /* Limpa os dados na sessão "users states" do joomla */
        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            //$app->setUserState('com_virtualdesk.addnew4user.tarefa.data', null);

        }


        /* Carrega lista com os USERS que podem ser utilizados no REENCAMINHAR (acesso ao MANAGER).  */
        public static function getUserDropDownList4Manager ()
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('tarefa');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('tarefa', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $conf = JFactory::getConfig();
                $db = JFactory::getDBO();
                $dbprefix = $conf->get('dbprefix');

                $sql = "SELECT distinct s1.iduser, s2.id as id, s2.name as name from (";
                $sql.= "SELECT distinct c.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_usersaction c on c.idpermaction = b.id ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and c.iduser is not null ";
                $sql.= " union ";
                $sql.= " SELECT distinct d.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_groupsaction c on c.idpermaction = b.id ";
                $sql.= " left JOIN ".$dbprefix."virtualdesk_perm_groupsusers d on d.idpermgroup = c.idpermgroup ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and d.iduser is not null ";
                $sql.= " ) as s1 left join ".$dbprefix."virtualdesk_users as s2 on s1.iduser=s2.id ";
                $sql.= " WHERE s2.blocked=0 and s2.activated=1";

                $db->setQuery($sql);

                $dataReturn = $db->loadAssocList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os GRUPOS que podem ser utilizados no REENCAMINHAR (acesso ao MANAGER).  */
        public static function getGroupDropDownList4Manager ()
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('tarefa');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('tarefa', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $conf = JFactory::getConfig();
                $db = JFactory::getDBO();
                $dbprefix = $conf->get('dbprefix');

                $sql = " SELECT distinct d.id as id, d.nome as name FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_groupsaction c on c.idpermaction = b.id ";
                $sql.= " left JOIN ".$dbprefix."virtualdesk_perm_groups d on d.id = c.idpermgroup ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and d.id is not null ";

                $db->setQuery($sql);
                $dataReturn = $db->loadAssocList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }



        public static function SendMailAdmin($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $Assunto, $nameUser, $email, $ref)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_NovoAdmin_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');


            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE     = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_NOVOADMIN_TITULO');
            $BODY_TITLE2    = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_NOVOADMIN_TITULO');
            $BODY_GREETING  = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_NOVOADMIN_CORPO', $Assunto);


            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom']);
            $newEmail->setSender($data['mailfrom']);
            $newEmail->setFrom($data['fromname']);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

            $newEmail->setSubject($data['sitename']);
            $newEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa__BannerEmail.png");


            $return = (boolean)$newEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailAdminUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $Assunto, $nameUser, $email, $referencia)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_NovoAdmin_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_UPDATEADMIN_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_UPDATEADMIN_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_UPDATEADMIN_CORPO', $Assunto, $referencia);


            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom']);
            $newEmail->setSender($data['mailfrom']);
            $newEmail->setFrom($data['fromname']);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

            $newEmail->setSubject($data['sitename']);
            $newEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa_BannerEmail.png");


            $return = (boolean)$newEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailAdminAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $Assunto, $nameUser, $email, $referencia,$Estado)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_NovoAdmin_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ALTESTADOADMIN_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ALTESTADOADMIN_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ALTESTADOADMIN_CORPO', $Assunto, $Estado);


            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_FILES_LIST_TITLE", '', $body);
            $body = str_replace("%BODY_FILES_LIST", '', $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom']);
            $newEmail->setSender($data['mailfrom']);
            $newEmail->setFrom($data['fromname']);

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
            $emailManager  = '';
            $emailAdmin    = '';
            $resNameEMail  = self::getNameAndEmailDoManagerEAdmin( $emailManager, $emailAdmin);
            if(!empty($emailManager)) $newEmail->addRecipient($emailManager);
            if(!empty($emailAdmin))   $newEmail->addRecipient($emailAdmin);

            $newEmail->setSubject($data['sitename']);
            $newEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa_BannerEmail.png");


            $return = (boolean)$newEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailUserWhoIniated($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_NovoClient_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');
            $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_NOVA_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_NOVA_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINITIATED_INTRO', $nameUser, $nomeMunicipio);
            $BODY_GREETING2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINITIATED_INTRO2', $Assunto);
            $BODY_GREETING3 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINITIATED_INTRO3', $nomeMunicipio);

            $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ASSUNTO');
            $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
            $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DEPARTAMENTO');
            $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);
            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_2GREETING", $BODY_GREETING2, $body);
            $body = str_replace("%BODY_3GREETING", $BODY_GREETING3, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailUserInTask($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_NovoClient_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');
            $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_NOVA_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_NOVA_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINTASK_INTRO', $nameUser, $nomeMunicipio);
            $BODY_GREETING2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINTASK_INTRO2', $Assunto);
            $BODY_GREETING3 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINTASK_INTRO3', $nomeMunicipio);

            $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ASSUNTO');
            $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
            $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DEPARTAMENTO');
            $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);
            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_2GREETING", $BODY_GREETING2, $body);
            $body = str_replace("%BODY_3GREETING", $BODY_GREETING3, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailUserInTaskUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_NovoClient_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');
            $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_UPDATE_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_UPDATE_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINTASK_INTRO', $nameUser, $nomeMunicipio);
            $BODY_GREETING2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINTASK_INTRO2', $Assunto);
            $BODY_GREETING3 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINTASK_INTRO3', $nomeMunicipio);

            $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ASSUNTO');
            $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
            $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DEPARTAMENTO');
            $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);
            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_2GREETING", $BODY_GREETING2, $body);
            $body = str_replace("%BODY_3GREETING", $BODY_GREETING3, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailUserWhoIniatedUpdate($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nomeMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento, $referencia)
        {

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_Alteracao_Client_Email.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_UPDATE_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_UPDATE_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINITIATEDUPDATE_INTRO', $nameUser, $nomeMunicipio);
            $BODY_GREETING2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINITIATED_INTRO2', $Assunto);
            $BODY_GREETING3 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_USERINITIATED_INTRO3', $nomeMunicipio);

            $BODY_ASSUNTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ASSUNTO');
            $BODY_ASSUNTO_VALUE = JText::sprintf($Assunto);
            $BODY_DESCRICAO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE = JText::sprintf($descricao);
            $BODY_DEPARTAMENTO_TITLE = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_DEPARTAMENTO');
            $BODY_DEPARTAMENTO_VALUE = JText::sprintf($Departamento);
            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_2GREETING", $BODY_GREETING2, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_TITLE", $BODY_ASSUNTO_TITLE, $body);
            $body = str_replace("%BODY_ASSUNTO_VALUE", $BODY_ASSUNTO_VALUE, $body);
            $body = str_replace("%BODY_DESCRICAO_TITLE", $BODY_DESCRICAO_TITLE, $body);
            $body = str_replace("%BODY_DESCRICAO_VALUE", $BODY_DESCRICAO_VALUE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_TITLE", $BODY_DEPARTAMENTO_TITLE, $body);
            $body = str_replace("%BODY_DEPARTAMENTO_VALUE", $BODY_DEPARTAMENTO_VALUE, $body);

            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function SendMailUserWhoInitiatedAltEstado($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $nameUser, $Assunto, $descricao, $emailUser, $Departamento, $Estado, $referencia)
        {
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Tarefa_AlteracaoEstado_Client_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailTarefa = $obParam->getParamsByTag('logosendmailTarefa');
            $nomeMunicipio = $obParam->getParamsByTag('nomeMunicipio');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');


            $BODY_TITLE  = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ALTESTADOCLIENT_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ALTESTADOCLIENT_INTRO', $nameUser, $Estado);
            $BODY_GREETING2 = JText::sprintf('COM_VIRTUALDESK_TAREFA_EMAIL_ALTESTADOCLIENT_INTRO2', $Assunto);

            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_2GREETING", $BODY_GREETING2, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($emailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT . $logosendmailTarefa, "banner", "Tarefa_BannerEmail.png");


            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        /* Define qual o  email por defeito do manager, se não existir devolve o do Admin */
        public static function getNameAndEmailDoManagerEAdmin(&$emailManager, &$emailAdmin)
        {
            $config                      = JFactory::getConfig();
            $objVDParams                 = new VirtualDeskSiteParamsHelper();
            $MailManagerbyDefault  = $objVDParams->getParamsByTag('Tarefa_Mail_Manager_by_Default');
            $MailAdminbyDefault    = $objVDParams->getParamsByTag('mailAdminTarefa');
            $adminSiteDefaultEmail       = $config->get('mailfrom');

            // Verifica se tem email do mananger DE tarefa caso contrário fica o admin do site
            $emailManager = $MailManagerbyDefault;
            if(empty($emailManager)) {
                $emailManager = $MailAdminbyDefault;
                if(empty($emailManager)) {
                    $emailManager =  $adminSiteDefaultEmail;
                }
            }

            // Verifica se tem email do admin do alerta caso contrário fica o admin do site
            $emailAdmin = $MailAdminbyDefault;
            if(empty($emailAdmin)) {
                $emailAdmin = $adminSiteDefaultEmail;
            }
            return(true);
        }


    }
?>