<?php

defined('_JEXEC') or die;


// Import filesystem libraries. Perhaps not necessary, but does not hurt
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

class VirtualDeskSiteParamsHelper {

   ## private $rotas = array();


    public function __construct() {

    }




    public function getParamsByTag($tagparam)
    {
        if( empty($tagparam) )  return false;
        try
        {   $dataReturn = '';
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.tagchave as tagchave', 'a.valor as valor'   ))
                ->from("#__virtualdesk_config_params as a")
                ->where( ' enabled=1 and ' . $db->quoteName('a.tagchave') . '="' . $db->escape($tagparam) .'"'  )
            );

            $dataRes = $db->loadObject();
            if(!empty($dataRes->valor)) $dataReturn = $dataRes->valor;

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getParamsMultiLinhaByTag($tagparam)
    {
        if( empty($tagparam) )  return false;
        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.tagchave as tagchave', 'a.valor as valor'   ))
                ->from("#__virtualdesk_config_params as a")
                ->where( ' enabled=1 and ' . $db->quoteName('a.tagchave') . '="' . $db->escape($tagparam) .'"'  )
            );

            $dataRes = $db->loadObject();
            $dataValor = $dataRes->valor;

            $dataReturn = preg_split("/\r\n|\n|\r/", $dataValor);

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public function getParamsByTagAndTipo($tagparam,$tagtipo)
    {
        if( empty($tagparam) ||  empty($tagtipo) )  return false;
        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.tagchave as tagchave', 'a.valor as valor'   ))
                ->from("#__virtualdesk_config_params as a")
                ->leftJoin("#__virtualdesk_config_params_tipo as b on b.id=a.idtipoparam")
                ->where( ' enabled=1 and ' . $db->quoteName('a.tagchave') . '="' . $db->escape($tagparam) .'" and ' . $db->quoteName('b.tagchave') . '="' . $db->escape($tagtipo).'" '  )
            );

            $dataRes = $db->loadObject();
            $dataReturn = $dataRes->valor;

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


}