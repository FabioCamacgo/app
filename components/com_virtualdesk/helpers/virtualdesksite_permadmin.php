<?php
/**
 * @package     Joomla.Site.VirtualDesk
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskTablePermModulo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_modulo.php');
JLoader::register('VirtualDeskTablePermGrupo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_grupo.php');
JLoader::register('VirtualDeskTablePermTipo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_tipo.php');
JLoader::register('VirtualDeskTablePermTipoModulo', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_tipomodulo.php');
JLoader::register('VirtualDeskTablePermAction', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_action.php');
JLoader::register('VirtualDeskTablePermUsersAction', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_usersaction.php');

/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSitePermAdminHelper
{


    function __construct() {
    }


    /*
    * $vbForDataTables: se for true, vai carrega o JSON para os datatables
    */
    public static function getPermModuleList ($vbForDataTables=false, $setLimit=-1)
    {

        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');

                // Gerar Link para o detalhe...
                $SwitchSetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=permadmin.setModuloEnableByAjax&permadmin_modulo_id=');
                $SwitchGetHRef =  JRoute::_('index.php?option=com_virtualdesk&task=permadmin.getModuloEnableValByAjax&permadmin_modulo_id=');


                $table  = " ( SELECT a.id as id, a.nome as nome ,a.enabled as enabled ";
                $table .= " , CONCAT('".$dummyHRef    ."', CAST(a.id as CHAR(10))) as dummy ";
                $table .= " , CONCAT('".$SwitchSetHRef."', CAST(a.id as CHAR(10))) as SwitchSetHRef ";
                $table .= " , CONCAT('".$SwitchGetHRef."', CAST(a.id as CHAR(10))) as SwitchGetHRef ";
                $table .= " , a.tagchave as tagchave ";
                $table .= " FROM ".$dbprefix."virtualdesk_perm_modulo as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'nome',       'dt' => 0 ),
                    array( 'db' => 'tagchave',   'dt' => 1 ),
                    array( 'db' => 'enabled',    'dt' => 2 ),
                    array( 'db' => 'dummy',      'dt' => 3 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchSetHRef', 'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'SwitchGetHRef', 'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'id',         'dt' => 6 ),

                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
        * $vbForDataTables: se for true, vai carrega o JSON para os datatables
        */
    public static function getPermGroupList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
       * Check Permissões
       */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=grupoview&permadmin_grupo_id=');

                $table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, isManager, email ";
                $table .= " FROM ".$dbprefix."virtualdesk_perm_groups as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'nome',       'dt' => 0 ),
                    array( 'db' => 'email',      'dt' => 1 ),
                    array( 'db' => 'isManager',  'dt' => 2 ),
                    array( 'db' => 'dummy',      'dt' => 3 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'id',         'dt' => 4 )

                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }
            else {
                // Verifica se o username existe noutro utilizador no VirtualDesk users ...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.nome as nome', 'a.id as id' ))
                    ->from("#__virtualdesk_perm_groups as a"));
                $dataReturn = $db->loadObjectList();
                return($dataReturn);
            }
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
       * $vbForDataTables: se for true, vai carrega o JSON para os datatables
       */
    public static function getPermUsersList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&permadmin_user_id=');

                $table  = " ( SELECT a.id as id, a.idjos as idjoomla, a.email as email, a.name as nome, a.login as login, a.blocked as blocked, a.activated as activated, b.group_id as gvdadmin, c.group_id as gvduser, d.group_id as gvdmanager, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy ";
                $table .= " FROM ".$dbprefix."virtualdesk_users as a ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."user_usergroup_map AS b ON b.user_id=a.idjos AND b.group_id=10 ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."user_usergroup_map AS c ON c.user_id=a.idjos AND c.group_id=11 ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."user_usergroup_map AS d ON d.user_id=a.idjos AND d.group_id=12 ";



                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',         'dt' => 0 ),
                    array( 'db' => 'idjoomla',   'dt' => 1 ),
                    array( 'db' => 'nome',       'dt' => 2 ),
                    array( 'db' => 'login',      'dt' => 3 ),
                    array( 'db' => 'email',      'dt' => 4 ),
                    array( 'db' => 'blocked',    'dt' => 5 ),
                    array( 'db' => 'activated',  'dt' => 6 ),
                    array( 'db' => 'gvdadmin',   'dt' => 7 ),
                    array( 'db' => 'gvdmanager', 'dt' => 8 ),
                    array( 'db' => 'gvduser',    'dt' => 9 ),
                    array( 'db' => 'dummy',      'dt' => 10 ,'formatter'=>'URL_ENCRYPT')

                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }
            else {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.name as name', 'a.id as iduser', 'a.idjos as idjoomla', 'a.email as email', 'a.name as nomeuser', 'a.login as login', 'a.blocked as blocked', 'a.activated as activated' ))
                    ->from("#__virtualdesk_users as a"));
                $dataReturn = $db->loadObjectList();
                return($dataReturn);
            }

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPermGroupUsersList ($vbForDataTables=false, $setLimit=-1, $IdPermGrupo, $vbReturnObject=true)
    {
        /*
         * Check Permissões
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupoview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermGrupo) )  return false;

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();
                $db   = JFactory::getDBO();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&permadmin_user_id=');

                $table  = " ( SELECT b.id, nome, a.id as iduser, a.idjos as idjoomla, a.email as email, a.name as nomeuser, a.login as login, a.blocked as blocked, a.activated as activated, d.group_id as gvdadmin, e.group_id as gvduser, f.group_id as gvdmanager, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy ";
                $table .= " FROM ".$dbprefix."virtualdesk_perm_groups as b ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."virtualdesk_perm_groupsusers as c ON c.idpermgroup=b.id";
                $table .= " LEFT OUTER JOIN ".$dbprefix."virtualdesk_users as a ON a.id=c.iduser";
                $table .= " LEFT OUTER JOIN ".$dbprefix."user_usergroup_map AS d ON d.user_id=a.idjos AND d.group_id=10 ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."user_usergroup_map AS e ON e.user_id=a.idjos AND e.group_id=11 ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."user_usergroup_map AS f ON f.user_id=a.idjos AND f.group_id=12 ";
                $table .= " WHERE b.id=".$db->escape($IdPermGrupo).' AND c.iduser>0';
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'iduser',     'dt' => 0 ),
                    array( 'db' => 'idjoomla',   'dt' => 1 ),
                    array( 'db' => 'nomeuser',   'dt' => 2 ),
                    array( 'db' => 'login',      'dt' => 3 ),
                    array( 'db' => 'email',      'dt' => 4 ),
                    array( 'db' => 'blocked',    'dt' => 5 ),
                    array( 'db' => 'activated',  'dt' => 6 ),
                    array( 'db' => 'gvdadmin',   'dt' => 7 ),
                    array( 'db' => 'gvdmanager', 'dt' => 8 ),
                    array( 'db' => 'gvduser',    'dt' => 9 ),
                    array( 'db' => 'dummy',      'dt' => 10 ,'formatter'=>'URL_ENCRYPT')


                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }
            else {
                //
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                        ->select(array('b.id', 'nome', 'a.id as iduser', 'a.idjos as idjoomla', 'a.idjos as iduserjos', 'a.email as email', 'a.name as nomeuser', 'a.login as login', 'a.blocked as blocked', 'a.activated as activated' ))
                        ->from("#__virtualdesk_perm_groups as b")
                        ->join("LEFT OUTER", ' #__virtualdesk_perm_groupsusers as c ON c.idpermgroup=b.id' )
                        ->join("LEFT OUTER", ' #__virtualdesk_users as a ON a.id=c.iduser' )
                        ->where( $db->quoteName('b.id') . '=' . $db->escape($IdPermGrupo).' AND c.iduser>0' )
                );

                if($vbReturnObject==false) {
                  $dataReturn = $db->loadAssocList();
                }
                else {
                  $dataReturn = $db->loadObjectList();
                }

                return($dataReturn);
            }
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getGroupUsersOnlyIDList ($setLimit=-1,$IdPermGrupo)
    {
        /*
         * Check Permissões
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupoview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermGrupo) )  return false;

        try
        {

                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('b.id', 'nome', 'a.id as iduser', 'a.idjos as idjoomla', 'a.email as email', 'a.name as nomeuser', 'a.login as login', 'a.blocked as blocked', 'a.activated as activated' ))
                    ->from("#__virtualdesk_perm_groups as b")
                    ->join("LEFT OUTER", ' #__virtualdesk_perm_groupsusers as c ON c.idpermgroup=b.id' )
                    ->join("LEFT OUTER", ' #__virtualdesk_users as a ON a.id=c.iduser' )
                    ->where( $db->quoteName('b.id') . '=' . $db->escape($IdPermGrupo).' AND c.iduser>0' )
                );

                $dataReturn = $db->loadAssocList();

            $arGrupoUsersIds =  array();
            foreach ($dataReturn as $rowGrupoUser) {
                $arGrupoUsersIds[] =$rowGrupoUser['iduser'];
            }

            return($arGrupoUsersIds);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPermUserGroupsList ($vbForDataTables=false, $setLimit=-1, $IdPermUser, $vbReturnObject=true)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermUser) )  return false;

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();
                $db   = JFactory::getDBO();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=grupoview&permadmin_grupo_id=');

                $table  = " ( SELECT a.id, a.nome as nome, b.id as iduser, b.idjos as idjoomla, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy ";
                $table .= " FROM ".$dbprefix."virtualdesk_users as b ";
                $table .= " LEFT OUTER JOIN ".$dbprefix."virtualdesk_perm_groupsusers as c ON c.iduser=b.id";
                $table .= " LEFT OUTER JOIN ".$dbprefix."virtualdesk_perm_groups as a ON a.id=c.idpermgroup";
                $table .= " WHERE b.id=".$db->escape($IdPermUser).' AND c.idpermgroup>0';
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'id',         'dt' => 0 ),
                    array( 'db' => 'nome',       'dt' => 1 ),
                    array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT')
                );
                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );
                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );
                return $data;
            }
            else {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id', 'a.nome', 'b.id as iduser', 'b.idjos as idjoomla', 'b.idjos as iduserjos' ))
                    ->from("#__virtualdesk_users as b")
                    ->join("LEFT OUTER", ' #__virtualdesk_perm_groupsusers as c ON c.iduser=b.id' )
                    ->join("LEFT OUTER", ' #__virtualdesk_perm_groups as a ON a.id=c.idpermgroup' )
                    ->where( $db->quoteName('b.id') . '=' . $db->escape($IdPermUser).' AND c.idpermgroup>0' )
                );
                if($vbReturnObject==false) {
                    $dataReturn = $db->loadAssocList();
                }
                else {
                    $dataReturn = $db->loadObjectList();
                }
                return($dataReturn);
            }
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getUserGroupsOnlyIDList ($setLimit=-1,$IdPermUser)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermUser) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id', 'a.nome', 'b.id as iduser', 'b.idjos as idjoomla' , 'b.idjos as iduserjos'))
                ->from("#__virtualdesk_users as b")
                ->join("LEFT OUTER", ' #__virtualdesk_perm_groupsusers as c ON c.iduser=b.id' )
                ->join("LEFT OUTER", ' #__virtualdesk_perm_groups as a ON a.id=c.idpermgroup' )
                ->where( $db->quoteName('b.id') . '=' . $db->escape($IdPermUser).' AND c.idpermgroup>0' )
            );

            $dataReturn = $db->loadAssocList();

            $arIds =  array();
            foreach ($dataReturn as $rowUserGrupo) {
                $arIds[] =$rowUserGrupo['id'];
            }
            return($arIds);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
        * $vbForDataTables: se for true, vai carrega o JSON para os datatables
        */
    public static function getPermTipoList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=tipoview&permadmin_tipo_id=');

                $table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy , a.tagchave as tagchave";
                $table .= " FROM ".$dbprefix."virtualdesk_perm_tipo as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'nome',       'dt' => 0 ),
                    array( 'db' => 'tagchave',   'dt' => 1 ),
                    array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'id',         'dt' => 3 )

                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
       * $vbForDataTables: se for true, vai carrega o JSON para os datatables
       */
    public static function getPermActionsList ($vbForDataTables=false, $setLimit=-1)
    {
        /*
       * Check Permissões
       */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=actionview&permadmin_action_id=');

                $table  = " ( SELECT a.id as id, a.nome as nome , CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy , a.tagchave as tagchave, a.descricao as descricao";
                $table .= " , d.nome as modulo, c.nome as tipo";
                $table .= " FROM ".$dbprefix."virtualdesk_perm_action as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_tipomodulo as b ON a.idpermtipomodulo = b.id";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_tipo as c ON b.idpermtipo = c.id";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_modulo as d ON b.idpermmodulo = d.id";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'nome',       'dt' => 0 ),
                    array( 'db' => 'tagchave',   'dt' => 1 ),
                    array( 'db' => 'descricao',  'dt' => 2 ),
                    array( 'db' => 'modulo',     'dt' => 3 ),
                    array( 'db' => 'tipo',       'dt' => 4 ),
                    array( 'db' => 'dummy',      'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'id',         'dt' => 6 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPermModuloDetail ($IdPermModulo)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'moduloview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermModulo) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as permadmin_modulo_id', 'nome', 'tagchave as tagchave' ,'enabled' , 'enabled2dropbox', 'created', 'modified' ))
                ->from("#__virtualdesk_perm_modulo")
                ->where( $db->quoteName('id') . '=' . $db->escape($IdPermModulo) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function getPermGrupoDetail ($IdPermGrupo)
    {
        /*
       * Check Permissões
       */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupoview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermGrupo) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as permadmin_grupo_id', 'nome','descricao','email','isManager', 'created', 'modified' ))
                ->from("#__virtualdesk_perm_groups")
                ->where( $db->quoteName('id') . '=' . $db->escape($IdPermGrupo) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPermGrupoActionsDetail ($IdPermGrupo)
    {
        /*
      * Check Permissões
      */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupoview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermGrupo) )  return false;

        try
        {

            $dataReturn = $objCheckPerm->loadModulesTipoActionsGrupoForEditing($IdPermGrupo);

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPermTipoDetail ($IdPermTipo)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'tipoview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermTipo) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id','id as permadmin_tipo_id', 'nome', 'tagchave as tagchave' , 'created', 'modified' ))
                ->from("#__virtualdesk_perm_tipo")
                ->where( $db->quoteName('id') . '=' . $db->escape($IdPermTipo) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function getPermActionDetail ($IdPermAction)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'actionview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermAction) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id','a.id as permadmin_action_id', 'a.nome', 'a.tagchave as tagchave', 'a.descricao as descricao', 'b.idpermtipo as tipo', 'b.idpermmodulo as modulo', 'd.nome as modulodesc' , 'c.nome as tipodesc', 'a.created', 'a.modified' ))
                ->from("#__virtualdesk_perm_action as a")
                ->leftJoin("#__virtualdesk_perm_tipomodulo as b ON a.idpermtipomodulo = b.id")
                ->leftJoin("#__virtualdesk_perm_tipo as c ON b.idpermtipo = c.id")
                ->leftJoin("#__virtualdesk_perm_modulo as d ON b.idpermmodulo = d.id")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdPermAction) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {

            return false;
        }
    }


    public static function getPermUserDetail ($IdPermUser)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        if( empty($IdPermUser) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id','a.idjos as idjos','a.id as permadmin_user_id', 'a.name as name', 'a.login as login', 'a.email as email', 'a.blocked as blocked', 'a.activated as activated', 'a.created', 'a.modified' ))
                ->from("#__virtualdesk_users as a")
                ->where( $db->quoteName('a.id') . '=' . $db->escape($IdPermUser) )
            );
            $dataReturn = $db->loadObject();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPermUserActionsDetail ($IdPermUser)
    {
        /*
       * Check Permissões
       */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        if( empty($IdPermUser) )  return false;

        try
        {
            //$dataReturn = $objCheckPerm->loadUsersActionsTipoModulesFullObject ($IdPermUser);
            $dataReturn = $objCheckPerm->loadModulesTipoActionsForEditing ($IdPermUser);

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Update de um módulo
    */
    public static function updateModulo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'moduloedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_modulo_id = $data['permadmin_modulo_id'];
        if((int) $permadmin_modulo_id <=0 ) return false;
        $data['id'] = $permadmin_modulo_id;
        unset($data['permadmin_modulo_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTablePermModulo($db);
        $Table->load(array('id'=>$permadmin_modulo_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_MODULO_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_MODULO_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_MODULO_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
       catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function setModuloEnableState($UserJoomlaID, $getInputModulo_id, $setId2Enable)
    {
        /*
         * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
         */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'moduloedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $UserVDId = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
        if ($UserVDId === false)  return false;

        $data = array();
        $permadmin_modulo_id = $getInputModulo_id;
        if((int) $permadmin_modulo_id <=0 ) return false;
        if((int) $setId2Enable <0 || (int) $setId2Enable >1 ) return false;

        $data['id']     = $permadmin_modulo_id;
        $data['enabled'] = $setId2Enable;


        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTablePermModulo($db);
        $Table->load(array('id'=>$permadmin_modulo_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_CONFIGADMIN_EVENTLOG_PLUGIN_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function checkModuloEnableVal($getInputModulo_id)
    {
        if( empty($getInputModulo_id) )  return false;

        /*
         * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
         */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'moduloedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();

            $db->setQuery("Select enabled From #__virtualdesk_perm_modulo Where id =" . $db->escape($getInputModulo_id));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Update de um módulo
    */
    public static function updateGrupo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupoedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_grupo_id = $data['permadmin_grupo_id'];
        if((int) $permadmin_grupo_id <=0 ) return false;
        $data['id'] = $permadmin_grupo_id;
        unset($data['permadmin_grupo_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTablePermGrupo($db);
        $Table->load(array('id'=>$permadmin_grupo_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        if((int)$permadmin_grupo_id<=4) {
            unset($data['isManager']);
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GRUPO_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GRUPO_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GRUPO_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updateTipo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'tipoedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_tipo_id = $data['permadmin_tipo_id'];
        if((int) $permadmin_tipo_id <=0 ) return false;
        $data['id'] = $permadmin_tipo_id;
        unset($data['permadmin_tipo_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTablePermTipo($db);
        $Table->load(array('id'=>$permadmin_tipo_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_TIPO_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_TIPO_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_TIPO_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updateAction($UserJoomlaID, $UserVDId, $data)
    {
        /*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'actionedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_action_id = $data['permadmin_action_id'];
        if((int) $permadmin_action_id <=0 ) return false;
        $data['id'] = $permadmin_action_id;
        unset($data['permadmin_action_id']);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $TableAction = new VirtualDeskTablePermAction($db);
        $TableAction->load(array('id'=>$permadmin_action_id));

        if(!empty($data)) {
            $dateModified = new DateTime();
            $data['modified'] = $dateModified->format('Y-m-d H:i:s');
        }

        try {


            // Temos de pesquisar se a chave idmodulo + idtipo escolhida já existe na tabela tipo módulo
            $TableTipoModulo = new VirtualDeskTablePermTipoModulo($db);
            $TableTipoModulo->load(array('idpermtipo'=>$data['tipo'] , 'idpermmodulo'=>$data['modulo']));

            //Se não existir, criamos essa chave, carregamos o id criado e utilizamos na gravação
            if((int)$TableTipoModulo->id <= 0) {
                $dataNewTipoModulo = new stdClass();

                $dataNewTipoModulo->idpermtipo   = $data['tipo'];
                $dataNewTipoModulo->idpermmodulo = $data['modulo'];
                if( (empty($dataNewTipoModulo->idpermtipo)) || (empty($dataNewTipoModulo->idpermmodulo)) )  return false;

                // Bind the data.
                if (!$TableTipoModulo->bind($dataNewTipoModulo)) return false;
                // Store the data.
                if (!$TableTipoModulo->save($dataNewTipoModulo)) return false;
                $GetCreateId = $TableTipoModulo->id;
                if (empty($GetCreateId)) return false;
            }

            //Carrega o idtipomodulo e associa para a gravação da action
            unset($data['modulo']);
            unset($data['tipo']);
            if (empty($TableTipoModulo->id)) return false;
            $data['idpermtipomodulo'] = $TableTipoModulo->id ;

            // Bind the data.
            if (!$TableAction->bind($data)) return false;
            // Store the data.
            if (!$TableAction->save($data)) return false;


            // Foi alterado
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_ACTION_UPDATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_ACTION_UPDATED') . 'id=' . $data['id'];
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_ACTION_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }
    }


    public static function updateUserAction($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'useractionedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_user_id = $data['permadmin_user_id'];
        if((int) $permadmin_user_id <=0 ) return false;
        $user_id_jos = VirtualDeskSiteUserHelper::getJoomlaIDFromVDUserId ($permadmin_user_id);

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);
        //$TableUsersAction = new VirtualDeskTablePermUsersAction($db);

        if(!empty($data)) {
            $dateModified  = new DateTime();
            $data_modified = $dateModified->format('Y-m-d H:i:s');
        }

        foreach($data['uaid'] as $key => $row) {

            if(sizeof($row)<=0) continue;

            $chaves   = explode('_', $key);
            $moduloid = (int)$chaves[0];
            $tipoid   = (int)$chaves[1];
            $actionid = (int)$chaves[2];
            $uactionsid = (int)$chaves[3];

            if ($moduloid<=0 || $tipoid<=0 || $actionid<=0) return false;

            $TableUserActiob = '#__virtualdesk_perm_usersaction';
            $columns = array();
            $values  = array();
            $fields  = array();

            if((string)$row['c']!='') {
                array_push($columns,'permcreate');
                array_push($values,$row['c']);
                array_push($fields,$db->quoteName('permcreate') . ' = '.$row['c']);
            }
            if((string)$row['r']!='') {
                array_push($columns,'permread');
                array_push($values,$row['r']);
                array_push($fields,$db->quoteName('permread') . ' = '.$row['r']);
            }
            if((string)$row['u']!='') {
                array_push($columns,'permupdate');
                array_push($values,$row['u']);
                array_push($fields,$db->quoteName('permupdate') . ' = '.$row['u']);
            }
            if((string)$row['d']!='') {
                array_push($columns,'permdelete');
                array_push($values,$row['d']);
                array_push($fields,$db->quoteName('permdelete') . ' = '.$row['d']);
            }
            if((string)$row['rall']!='') {
                array_push($columns,'permreadall');
                array_push($values,$row['rall']);
                array_push($fields,$db->quoteName('permreadall') . ' = '.$row['rall']);
            }

            unset($query);
            $query = $db->getQuery(true);

            if ( $uactionsid<=0 ) {
                // é um INSERT ***
                $query->insert($db->quoteName($TableUserActiob));

                array_push($columns,'idpermaction','iduser', 'iduserjos','modified','created');
                array_push($values , $actionid,$permadmin_user_id , $user_id_jos , $db->quote($data_modified) , $db->quote($data_modified));

                $query->columns($db->quoteName($columns))
                      ->values( implode(',', $values));
            }
            else  {
                // é um UPDATE ***

                array_push($fields,$db->quoteName('modified') . ' = '.$db->quote($data_modified));
                // Conditions for which records should be updated.
                $conditions = array(
                    $db->quoteName('id') . ' = '.$uactionsid,
                    $db->quoteName('idpermaction') . ' = '.$actionid
                );

                $query->update($db->quoteName($TableUserActiob))->set($fields)->where($conditions);
            }

            try {
                $db->setQuery($query);
                $result = $db->execute();
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        // Foi alterado
        $vdlog = new VirtualDeskLogHelper();
        $eventdata = array();
        $eventdata['iduser'] = $UserVDId;
        $eventdata['idjos'] = $UserJoomlaID;
        $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_USERSACTION_UPDATED');
        $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_USERSACTION_UPDATED') . 'id=' . $data['id'];
        $eventdata['filelist'] = "";
        $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_USERSACTION_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;

    }


    public static function updateGrupoAction($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupoactionedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_grupo_id = $data['permadmin_grupo_id'];
        if((int) $permadmin_grupo_id <=0 ) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();


        if(!empty($data)) {
            $dateModified  = new DateTime();
            $data_modified = $dateModified->format('Y-m-d H:i:s');
        }

        foreach($data['uaid'] as $key => $row) {

            if(sizeof($row)<=0) continue;

            $chaves   = explode('_', $key);
            $moduloid = (int)$chaves[0];
            $tipoid   = (int)$chaves[1];
            $actionid = (int)$chaves[2];
            $gactionsid = (int)$chaves[3];

            if ($moduloid<=0 || $tipoid<=0 || $actionid<=0) return false;

            $TableGrupoAction = '#__virtualdesk_perm_groupsaction';
            $columns = array();
            $values  = array();
            $fields  = array();

            if((string)$row['c']!='') {
                array_push($columns,'permcreate');
                array_push($values,$row['c']);
                array_push($fields,$db->quoteName('permcreate') . ' = '.$row['c']);
            }
            if((string)$row['r']!='') {
                array_push($columns,'permread');
                array_push($values,$row['r']);
                array_push($fields,$db->quoteName('permread') . ' = '.$row['r']);
            }
            if((string)$row['u']!='') {
                array_push($columns,'permupdate');
                array_push($values,$row['u']);
                array_push($fields,$db->quoteName('permupdate') . ' = '.$row['u']);
            }
            if((string)$row['d']!='') {
                array_push($columns,'permdelete');
                array_push($values,$row['d']);
                array_push($fields,$db->quoteName('permdelete') . ' = '.$row['d']);
            }
            if((string)$row['rall']!='') {
                array_push($columns,'permreadall');
                array_push($values,$row['rall']);
                array_push($fields,$db->quoteName('permreadall') . ' = '.$row['rall']);
            }

            unset($query);
            $query = $db->getQuery(true);

            if ( $gactionsid<=0 ) {
                // é um INSERT ***
                $query->insert($db->quoteName($TableGrupoAction));

                array_push($columns,'idpermaction','idpermgroup' , 'modified','created');
                array_push($values , $actionid,$permadmin_grupo_id , $db->quote($data_modified) , $db->quote($data_modified));

                $query->columns($db->quoteName($columns))
                    ->values( implode(',', $values));
            }
            else  {
                // é um UPDATE ***

                array_push($fields,$db->quoteName('modified') . ' = '.$db->quote($data_modified));
                // Conditions for which records should be updated.
                $conditions = array(
                    $db->quoteName('id') . ' = '.$gactionsid,
                    $db->quoteName('idpermaction') . ' = '.$actionid
                );

                $query->update($db->quoteName($TableGrupoAction))->set($fields)->where($conditions);
            }


            try {
                $db->setQuery($query);
                $result = $db->execute();
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        // Foi alterado
        $vdlog = new VirtualDeskLogHelper();
        $eventdata = array();
        $eventdata['iduser'] = $UserVDId;
        $eventdata['idjos'] = $UserJoomlaID;
        $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_UPDATED');
        $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_UPDATED') . 'id=' . $data['id'];
        $eventdata['filelist'] = "";
        $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;

    }


    public static function updateGrupoUsers($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupousersedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_grupo_id = $data['permadmin_grupo_id'];
        if((int) $permadmin_grupo_id <=0 ) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();


        if(!empty($data)) {
            $dateModified  = new DateTime();
            $data_modified = $dateModified->format('Y-m-d H:i:s');
        }

        if(!is_array($data['gusrid_delete'])) $data['gusrid_delete'] = array();
        if(!is_array($data['gusrid_insert'])) $data['gusrid_insert'] = array();

        $TableGrupoUsers = '#__virtualdesk_perm_groupsusers';

        foreach($data['gusrid_delete'] as $keyD=> $rowD) {
            unset($query);
            $query      = $db->getQuery(true);
            $conditions = array(
                $db->quoteName('iduser') . ' = '.$keyD,
                $db->quoteName('idpermgroup') . ' = '.$permadmin_grupo_id
            );
            $query->delete($db->quoteName($TableGrupoUsers))->where($conditions);
            try {
                $db->setQuery($query);
                $result = $db->execute();
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        foreach($data['gusrid_insert'] as $keyI=> $keyI) {
            unset($query);
            $columns = array();
            $values  = array();

            $query      = $db->getQuery(true);
            // é um INSERT ***
            $query->insert($db->quoteName($TableGrupoUsers));

            $user_id_jos = VirtualDeskSiteUserHelper::getJoomlaIDFromVDUserId ($keyI);

            array_push($columns,'iduser','iduserjos','idpermgroup' , 'modified','created');
            array_push($values , $keyI, (int)$user_id_jos, $permadmin_grupo_id , $db->quote($data_modified) , $db->quote($data_modified));

            $query->columns($db->quoteName($columns))->values( implode(',', $values));
            try {
                $db->setQuery($query);
                $result = $db->execute();
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }








        // Foi alterado
        $vdlog = new VirtualDeskLogHelper();
        $eventdata = array();
        $eventdata['iduser'] = $UserVDId;
        $eventdata['idjos'] = $UserJoomlaID;
        $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_UPDATED');
        $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_UPDATED') . 'id=' . $data['id'];
        $eventdata['filelist'] = "";
        $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;

    }


    public static function updateUserGrupos($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'usergruposedit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $permadmin_user_id = $data['permadmin_user_id'];
        if((int) $permadmin_user_id <=0 ) return false;

        // Carrega dados atuais na base de dados
        $db    = JFactory::getDbo();


        if(!empty($data)) {
            $dateModified  = new DateTime();
            $data_modified = $dateModified->format('Y-m-d H:i:s');
        }

        if(!is_array($data['ugrpsid_delete'])) $data['ugrpsid_delete'] = array();
        if(!is_array($data['ugrpsid_insert'])) $data['ugrpsid_insert'] = array();

        $TableGrupoUsers = '#__virtualdesk_perm_groupsusers';

        foreach($data['ugrpsid_delete'] as $keyD=> $rowD) {
            unset($query);
            $query      = $db->getQuery(true);
            $conditions = array(
                $db->quoteName('iduser') . ' = '.$permadmin_user_id ,
                $db->quoteName('idpermgroup') . ' = '.$keyD
            );
            $query->delete($db->quoteName($TableGrupoUsers))->where($conditions);
            try {
                $db->setQuery($query);
                $result = $db->execute();
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        foreach($data['ugrpsid_insert'] as $keyI=> $keyI) {
            unset($query);
            $columns = array();
            $values  = array();

            $query      = $db->getQuery(true);
            // é um INSERT ***
            $query->insert($db->quoteName($TableGrupoUsers));

            $user_id_jos = VirtualDeskSiteUserHelper::getJoomlaIDFromVDUserId ($permadmin_user_id );

            array_push($columns,'iduser','iduserjos','idpermgroup' , 'modified','created');
            array_push($values , $permadmin_user_id, (int)$user_id_jos, $keyI , $db->quote($data_modified) , $db->quote($data_modified));

            $query->columns($db->quoteName($columns))->values( implode(',', $values));
            try {
                $db->setQuery($query);
                $result = $db->execute();
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }








        // Foi alterado
        $vdlog = new VirtualDeskLogHelper();
        $eventdata = array();
        $eventdata['iduser'] = $UserVDId;
        $eventdata['idjos'] = $UserJoomlaID;
        $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_UPDATED');
        $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_UPDATED') . 'id=' . $data['id'];
        $eventdata['filelist'] = "";
        $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GROUPSACTION_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;

    }


    public static function createModulo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'moduloaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTablePermModulo($db);

        if (empty($data) ) return false;

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_MODULO_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_MODULO_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_MODULO_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function createGrupo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'grupoaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }



        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTablePermGrupo($db);

        if (empty($data) ) return false;

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GRUPO_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GRUPO_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_GRUPO_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function createTipo($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'tipoaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $Table = new VirtualDeskTablePermTipo($db);

        if (empty($data) ) return false;

        try {
            // Bind the data.
            if (!$Table->bind($data)) return false;
            // Store the data.
            if (!$Table->save($data)) return false;

            $GetCreateId = $Table->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_TIPO_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_TIPO_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_TIPO_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function createAction($UserJoomlaID, $UserVDId, $data)
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'actionaddnew'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $db    = JFactory::getDbo();
        $TableAction = new VirtualDeskTablePermAction($db);

        if (empty($data) ) return false;

        try {

            // Temos de pesquisar se a chave idmodulo + idtipo escolhida já existe na tabela tipo módulo
            $TableTipoModulo = new VirtualDeskTablePermTipoModulo($db);
            $TableTipoModulo->load(array('idpermtipo'=>$data['tipo'] , 'idpermmodulo'=>$data['modulo']));

            //Se não existir, criamos essa chave, carregamos o id criado e utilizamos na gravação
            if((int)$TableTipoModulo->id <= 0) {
                $dataNewTipoModulo = new stdClass();

                $dataNewTipoModulo->idpermtipo   = $data['tipo'];
                $dataNewTipoModulo->idpermmodulo = $data['modulo'];
                if( (empty($dataNewTipoModulo->idpermtipo)) || (empty($dataNewTipoModulo->idpermmodulo)) )  return false;

                // Bind the data.
                if (!$TableTipoModulo->bind($dataNewTipoModulo)) return false;
                // Store the data.
                if (!$TableTipoModulo->save($dataNewTipoModulo)) return false;
                $GetCreateId = $TableTipoModulo->id;
                if (empty($GetCreateId)) return false;
            }

            //Carrega o idtipomodulo e associa para a gravação da action
            unset($data['modulo']);
            unset($data['tipo']);
            if (empty($TableTipoModulo->id)) return false;
            $data['idpermtipomodulo'] = $TableTipoModulo->id ;

            // Bind the data.
            if (!$TableAction->bind($data)) return false;
            // Store the data.
            if (!$TableAction->save($data)) return false;

            $GetCreateId = $TableAction->id;
            if (empty($GetCreateId)) return false;


            // Foi alterado o Pedido do tipo Contact Us
            $vdlog = new VirtualDeskLogHelper();
            $eventdata = array();
            $eventdata['iduser'] = $UserVDId;
            $eventdata['idjos'] = $UserJoomlaID;
            $eventdata['title'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_ACTION_CREATED');
            $eventdata['desc'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_ACTION_CREATED') . 'id=' . $GetCreateId;
            $eventdata['filelist'] = "";
            $eventdata['category'] = JText::_('COM_VIRTUALDESK_PERMADMIN_EVENTLOG_ACTION_CAT');
            $eventdata['sessionuserid'] = JFactory::getUser()->id;
            $vdlog->insertEventLog($eventdata);

            return true;
        }
        catch (Exception $e){
            JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
            return false;
        }

    }


    public static function cleanAllTmpUserStare() {
        $app = JFactory::getApplication();
        $app->setUserState('com_virtualdesk.moduloaddnew.permadmin.data', null);
        $app->setUserState('com_virtualdesk.moduloedit.permadmin.data',null);
        $app->setUserState('com_virtualdesk.grupoaddnew.permadmin.data', null);
        $app->setUserState('com_virtualdesk.grupoedit.permadmin.data',null);
        $app->setUserState('com_virtualdesk.tipoaddnew.permadmin.data', null);
        $app->setUserState('com_virtualdesk.tipoedit.permadmin.data',null);
        $app->setUserState('com_virtualdesk.actionaddnew.permadmin.data', null);
        $app->setUserState('com_virtualdesk.actionedit.permadmin.data',null);
        $app->setUserState('com_virtualdesk.useractionedit.permadmin.data', null);
        $app->setUserState('com_virtualdesk.grupoactionedit.permadmin.data', null);
        $app->setUserState('com_virtualdesk.grupousersedit.permadmin.data', null);
        $app->setUserState('com_virtualdesk.usergruposedit.permadmin.data', null);
        $app->setUserState('com_virtualdesk.edit.profile.data', null);



    }


    public static function getModuloAll ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id', ' CONCAT(nome," ",tagchave) as nome') )
                ->from("#__virtualdesk_perm_modulo")
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadAssocList();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getTipoAll ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id', ' CONCAT(nome," ",tagchave) as nome') )
                ->from("#__virtualdesk_perm_tipo")
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadAssocList();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getPermModuleActionsFullList ()
    {
        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'moduloview'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $dataReturn = $objCheckPerm->loadModulesTipoActions();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


}
