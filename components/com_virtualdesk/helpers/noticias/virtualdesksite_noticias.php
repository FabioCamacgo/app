<?php
    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskTableCategorias', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/noticiasCategorias.php');
    JLoader::register('VirtualDeskTableNoticiasEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/noticiasEstadoHistorico.php');
    JLoader::register('VirtualDeskTableNoticias', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/noticias.php');
    JLoader::register('VirtualDeskSiteNoticiasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias_fotoCapa_files.php');
    JLoader::register('VirtualDeskSiteNoticiasGaleriaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias_galeria_files.php');

    class VirtualDeskSiteNoticiasHelper
    {
        /*APP*/
        const tagchaveModulo = 'noticias';


        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name') )
                    ->from("#__virtualdesk_Noticias_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-pendente';
                break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }


        public static function getEstado(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Noticias_Estado")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstadoSelect($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('estado')
                ->from("#__virtualdesk_Noticias_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($id) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeEstado($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id_estado as id, estado'))
                ->from("#__virtualdesk_Noticias_Estado")
                ->where($db->quoteName('id_estado') . "!='" . $db->escape($id) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoria(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Noticias_categorias")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriaSelect($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Noticias_categorias")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCategoria($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Noticias_categorias")
                ->where($db->quoteName('id') . "!='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function createCat4Manager($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('noticias');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'addnewcat4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $categoria = $data['categoria'];
                $estado = $data['estado'];

                $resSaveCategoria = self::saveNewCategoria($categoria, $estado);

                if (empty($resSaveCategoria)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Noticias_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED') . 'ref=' . $categoria;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewCategoria($categoria, $estado, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('categoria','estado');
            $values = array($db->quote($db->escape($categoria)), $db->quote($db->escape($estado)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Noticias_categorias'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function getCategoriasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('noticias', 'listcat4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('noticias','listcat4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=editcat4manager&categoria_id=');

                    $table  = " ( SELECT a.id as id, a.id as categoria_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.categoria as categoria, a.estado as idestado, b.estado as estado";
                    $table .= " FROM ".$dbprefix."virtualdesk_Noticias_categorias as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Noticias_Estado AS b ON b.id_estado = a.estado";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'categoria_id',       'dt' => 0 ),
                        array( 'db' => 'categoria',        'dt' => 1 ),
                        array( 'db' => 'estado',     'dt' => 2 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getCategoria4ManagerDetail($getInputCategoria_Id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('noticias');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('noticias', 'editcat4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('noticias'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('noticias','editcat4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($getInputCategoria_Id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as categoria_id', 'a.categoria as categoria', 'a.estado as idestado', 'b.estado as estado'))
                    ->join('LEFT', '#__virtualdesk_Noticias_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Noticias_categorias as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($getInputCategoria_Id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function updateCat4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'editcat4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $categoria_id = $data['categoria_id'];
            if((int) $categoria_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableCategorias($db);
            $Table->load(array('id'=>$categoria_id));

            $db->transactionStart();

            try {

                $obParam             = new VirtualDeskSiteParamsHelper();

                $categoria         = null;
                $estado        = null;

                if(array_key_exists('categoria',$data))     $categoria = $data['categoria'];
                if(array_key_exists('estado',$data))        $estado = $data['estado'];


                $obParam = new VirtualDeskSiteParamsHelper();

                /* BEGIN Save Evento */
                $resSaveCat = self::saveEditedCat4Manager($categoria_id, $categoria, $estado);
                /* END Save Evento */

                if (empty($resSaveCat)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Noticias_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED') . 'ref=' . $categoria_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedCat4Manager($categoria_id, $categoria, $estado){

            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'editcat4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria))  array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($estado))   array_push($fields, 'estado='.$db->escape($estado));


            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Noticias_categorias')
                ->set($fields)
                ->where(' id = '.$db->escape($categoria_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 18);
        }


        public static function CheckReferencia($cod){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_Noticias")
                ->where($db->quoteName('referencia') . "='" . $db->escape($cod) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnoNoticia($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Noticias_Anos")
                ->where($db->quoteName('ano') . '=' . $db->escape($ano))
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getAno($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('ano')
                ->from("#__virtualdesk_Noticias")
                ->where($db->quoteName('id') . '=' . $db->escape($id))
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getMes($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
            ->select('mes')
            ->from("#__virtualdesk_Noticias")
            ->where($db->quoteName('id') . '=' . $db->escape($id))
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('noticias');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'addnew4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $titulo = $data['titulo'];
                $descricao = $data['descricao'];
                $categoria = $data['categoria'];
                $data_publicacao = $data['data_publicacao'];
                $facebook = $data['facebook'];
                $video = $data['video'];

                if(empty($data_publicacao)){
                    $data_publicacao = date("Y-m-d");
                }

                $explodePubData = explode('-', $data_publicacao);
                $ano = self::getAnoNoticia($explodePubData[0]);
                $mes = $explodePubData[1];

                $refExiste = 1;
                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                $resSaveNoticia = self::saveNewNoticia($referencia, $titulo, $descricao, $categoria, $facebook, $video, $data_publicacao, $ano, $mes);

                if (empty($resSaveNoticia)) {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteNoticiasFilesHelper();
                $objCapaFiles->tagprocesso   = 'NOTICIAS_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');


                $objGaleriaFiles                = new VirtualDeskSiteNoticiasGaleriaHelper();
                $objGaleriaFiles->tagprocesso   = 'GALERIA_POST';
                $objGaleriaFiles->idprocesso    = $referencia;
                $resFileSaveGaleria             = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Noticias_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED') . 'ref=' . $referencia;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewNoticia($referencia, $titulo, $descricao, $categoria, $facebook, $video, $data_publicacao, $ano, $mes, $balcao=0){

            $db    = JFactory::getDbo();

            $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);

            $query = $db->getQuery(true);
            $columns = array('referencia','titulo','noticia','categoria','facebook','video','estado','data_publicacao','ano','mes');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($titulo)), $db->quote($db->escape($descricao)), $db->quote($db->escape($categoria)), $db->quote($db->escape($facebook)), $db->quote($db->escape($video)), $db->quote($db->escape('2')), $db->quote($db->escape($data_publicacao)), $db->quote($db->escape($ano)), $db->quote($db->escape($mes)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Noticias'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function saveNoticiaDuplicada($referencia, $nomeNoticia, $noticia, $categoria, $facebook, $video, $data_publicacao, $balcao=0){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','titulo','noticia','categoria','facebook','video','estado','data_publicacao');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nomeNoticia)), $db->quote($db->escape($noticia)), $db->quote($db->escape($categoria)), $db->quote($db->escape($facebook)), $db->quote($db->escape($video)), $db->quote($db->escape('3')), $db->quote($db->escape($data_publicacao)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Noticias'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_Noticias as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getNoticiasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('noticias', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('noticias','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=view4manager&noticia_id=');

                    $table  = " ( SELECT a.id as id, a.id as noticia_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as referencia, a.categoria as idcategoria, b.categoria as categoria, a.estado as idestado, c.estado as estado, a.titulo as titulo, a.visualizacoes as visualizacoes, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Noticias as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Noticias_categorias AS b ON b.id = a.categoria";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Noticias_Estado AS c ON c.id_estado = a.estado";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'referencia',    'dt' => 0 ),
                        array( 'db' => 'titulo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'visualizacoes', 'dt' => 4 ),
                        array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'noticia_id',    'dt' => 6 ),
                        array( 'db' => 'idcategoria',   'dt' => 7 ),
                        array( 'db' => 'idestado',      'dt' => 8 ),
                        array( 'db' => 'data_criacao',  'dt' => 9 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getNoticiasList4Gestor ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('noticias', 'list4gestores');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('noticias','list4gestores');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=view4gestor&noticia_id=');

                    $table  = " ( SELECT a.id as id, a.id as noticia_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as referencia, a.categoria as idcategoria, b.categoria as categoria, a.estado as idestado, c.estado as estado, a.titulo as titulo, a.visualizacoes as visualizacoes, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Noticias as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Noticias_categorias AS b ON b.id = a.categoria";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Noticias_Estado AS c ON c.id_estado = a.estado";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'referencia',    'dt' => 0 ),
                        array( 'db' => 'titulo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'visualizacoes', 'dt' => 4 ),
                        array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'noticia_id',    'dt' => 6 ),
                        array( 'db' => 'idcategoria',   'dt' => 7 ),
                        array( 'db' => 'idestado',      'dt' => 8 ),
                        array( 'db' => 'data_criacao',  'dt' => 9 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getNoticiasDetail4Manager($id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('noticias');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('noticias', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('noticias'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('noticias','edit4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id', 'a.referencia as codigo', 'a.titulo as titulo', 'a.noticia as descricao', 'b.estado as estado', 'a.estado as id_estado', 'c.categoria as categoria', 'a.categoria as id_categoria', 'a.facebook', 'a.video', 'a.data_criacao', 'a.data_publicacao', 'a.data_alteracao'))
                    ->join('LEFT', '#__virtualdesk_Noticias_Estado AS b ON b.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Noticias_categorias AS c ON c.id = a.categoria')
                    ->from("#__virtualdesk_Noticias as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getNoticiasDetail4Gestor($id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('noticias');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('noticias', 'view4gestores'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('noticias'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('noticias','view4gestores'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id as id', 'a.referencia as codigo', 'a.titulo as titulo', 'a.noticia as descricao', 'b.estado as estado', 'a.estado as id_estado', 'c.categoria as categoria', 'a.categoria as id_categoria', 'a.facebook', 'a.video', 'a.data_criacao', 'a.data_publicacao', 'a.data_alteracao'))
                    ->join('LEFT', '#__virtualdesk_Noticias_Estado AS b ON b.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Noticias_categorias AS c ON c.id = a.categoria')
                    ->from("#__virtualdesk_Noticias as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function DuplicaNoticia($id, $onAjaxVD=0){
            if (empty($id)) return false;

            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('titulo, noticia, categoria, facebook, video, data_publicacao'))
                    ->from("#__virtualdesk_Noticias")
                    ->where($db->quoteName('id') . '=' . $db->escape($id))
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('titulo, noticia, categoria, facebook, video, data_publicacao'))
                    ->from("#__virtualdesk_Noticias")
                    ->where($db->quoteName('id') . '=' . $db->escape($id))
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $noticia_id = $data['noticia_id'];
            if((int) $noticia_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableNoticias($db);
            $Table->load(array('id'=>$noticia_id));

            $db->transactionStart();

            try {
                $referencia          = $Table->referencia;
                $obParam    = new VirtualDeskSiteParamsHelper();

                $titulo = null;
                $descricao = null;
                $categoria = null;
                $facebook = null;
                $video = null;
                $estado = null;
                $data_publicacao = null;
                $ano = self::getAno($noticia_id);
                $mes = self::getMes($noticia_id);

                if(array_key_exists('titulo',$data))            $titulo = $data['titulo'];
                if(array_key_exists('descricao',$data))         $descricao = $data['descricao'];
                if(array_key_exists('categoria',$data))         $categoria = $data['categoria'];
                if(array_key_exists('facebook',$data))          $facebook = $data['facebook'];
                if(array_key_exists('video',$data))             $video = $data['video'];
                if(array_key_exists('estado',$data))            $estado = $data['estado'];
                if(array_key_exists('data_publicacao',$data)){
                    $data_publicacao = $data['data_publicacao'];
                    $explodePubData = explode('-', $data_publicacao);
                    $ano = self::getAnoNoticia($explodePubData[0]);
                    $mes = $explodePubData[1];
                }





                /* BEGIN Save Noticia */
                $resSaveNoticia = self::saveEditedNoticia4Manager($noticia_id, $titulo, $descricao, $categoria, $facebook, $video, $estado, $data_publicacao, $ano, $mes);
                /* END Save Noticia */

                if (empty($resSaveNoticia)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objCapaFiles                = new VirtualDeskSiteNoticiasFilesHelper();
                $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_capa');
                $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
                if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o ficheiro de capa', 'error');
                }


                $objGaleriaFiles                = new VirtualDeskSiteNoticiasGaleriaHelper();
                $listFileGaleria2Eliminar       = $objGaleriaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileupload_galeria');
                $resFileGaleriaDelete           = $objGaleriaFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
                if ($resFileGaleriaDelete==false && !empty($listFileGaleria2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros de galeria', 'error');
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteNoticiasFilesHelper();
                $objCapaFiles->tagprocesso   = 'NOTICIAS_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');


                $objGaleriaFiles                = new VirtualDeskSiteNoticiasGaleriaHelper();
                $objGaleriaFiles->tagprocesso   = 'GALERIA_POST';
                $objGaleriaFiles->idprocesso    = $referencia;
                $resFileSaveGaleria             = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Noticias_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED') . 'ref=' . $noticia_id;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedNoticia4Manager($noticia_id, $titulo, $descricao, $categoria, $facebook, $video, $estado, $data_publicacao, $ano, $mes)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);

            if(!is_null($titulo))  array_push($fields, 'titulo="'.$db->escape($titulo).'"');
            if(!is_null($descricao))  array_push($fields, 'noticia="'.$db->escape($descricao).'"');
            if(!is_null($categoria))  array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($facebook))  array_push($fields, 'facebook="'.$db->escape($facebook).'"');
            if(!is_null($video))  array_push($fields, 'video="'.$db->escape($video).'"');
            if(!is_null($estado))  array_push($fields, 'estado="'.$db->escape($estado).'"');
            if(!is_null($data_publicacao))  array_push($fields, 'data_publicacao="'.$db->escape($data_publicacao).'"');
            array_push($fields, 'ano="'.$db->escape($ano).'"');
            array_push($fields, 'mes="'.$db->escape($mes).'"');

            $data_alteracao = date("Y-m-d H:i:s");

            array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Noticias')
                ->set($fields)
                ->where(' id = '.$db->escape($noticia_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function create4Gestor($UserJoomlaID, $UserVDId, $data)
        {
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('noticias');
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'addnew4gestores');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            $db->transactionStart();

            try {

                $titulo = $data['titulo'];
                $descricao = $data['descricao'];
                $categoria = $data['categoria'];
                $facebook = $data['facebook'];
                $video = $data['video'];
                $data_publicacao = date("Y-m-d");

                $explodePubData = explode('-', $data_publicacao);
                $ano = self::getAnoNoticia($explodePubData[0]);
                $mes = $explodePubData[1];



                $refExiste = 1;
                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                $resSaveNoticia = self::saveNewNoticiaGestor($referencia, $titulo, $descricao, $categoria, $facebook, $video, $data_publicacao, $ano, $mes);

                if (empty($resSaveNoticia)) {
                    $db->transactionRollback();
                    return false;
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteNoticiasFilesHelper();
                $objCapaFiles->tagprocesso   = 'NOTICIAS_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileupload_capa');


                $objGaleriaFiles                = new VirtualDeskSiteNoticiasGaleriaHelper();
                $objGaleriaFiles->tagprocesso   = 'GALERIA_POST';
                $objGaleriaFiles->idprocesso    = $referencia;
                $resFileSaveGaleria             = $objGaleriaFiles->saveListFileByPOST('fileupload_galeria');

                $db->transactionCommit();

                self::SendEmailNoticiaAdmin($referencia, $titulo);

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Noticias_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED') . 'ref=' . $referencia;
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_NOTICIAS_EVENTLOG_CREATED_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }
            }
            catch (Exception $e){
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewNoticiaGestor($referencia, $titulo, $descricao, $categoria, $facebook, $video, $data_publicacao, $ano, $mes, $balcao=0){

            $db       = JFactory::getDbo();

            $descricao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($descricao),true);

            $query = $db->getQuery(true);

            $columns = array('referencia','titulo','noticia','categoria','facebook','video','estado','data_publicacao','ano','mes');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($titulo)), $db->quote($db->escape($descricao)), $db->quote($db->escape($categoria)), $db->quote($db->escape($facebook)), $db->quote($db->escape($video)), $db->quote($db->escape('1')), $db->quote($db->escape($data_publicacao)), $db->quote($db->escape($ano)), $db->quote($db->escape($mes)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Noticias'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
            $db->setQuery($query);
            $result = (boolean) $db->execute();
            return($result);
        }


        public static function SendEmailNoticiaAdmin($referencia, $titulo){

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_Noticia_Admin_Email.html');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logosendmailImage = $obParam->getParamsByTag('logoEmailGeral');
            $LinkCopyright = $obParam->getParamsByTag('LinkCopyright');
            $contactoTelefCopyrightEmail = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
            $emailCopyrightGeral = $obParam->getParamsByTag('emailCopyrightGeral');
            $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $mailAdmin = $obParam->getParamsByTag('mailAdmin');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $mailAdmin;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE = JText::sprintf('COM_VIRTUALDESK_NOTICIAS_EMAIL_TITULO');
            $BODY_TITLE2 = JText::sprintf('COM_VIRTUALDESK_NOTICIAS_EMAIL_TITULO');
            $BODY_GREETING = JText::sprintf('COM_VIRTUALDESK_NOTICIAS_EMAIL_INTRO', $titulo, $referencia);
            $BODY_COPYLINK = JText::sprintf($LinkCopyright);
            $BODY_COPYTELE = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM = JText::sprintf($dominioMunicipio);
            $BODY_COPYNAME = JText::sprintf($copyrightAPP);


            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $emailHTML);
            $body = str_replace("%BODY_TITULO", $BODY_TITLE2, $body);
            $body = str_replace("%BODY_GREETING", $BODY_GREETING, $body);
            $body = str_replace("%BODY_TITLE", $BODY_TITLE, $body);
            $body = str_replace("%BODY_COPYLINK", $BODY_COPYLINK, $body);
            $body = str_replace("%BODY_COPYTELE", $BODY_COPYTELE, $body);
            $body = str_replace("%BODY_COPYMAIL", $BODY_COPYMAIL, $body);
            $body = str_replace("%BODY_COPYDOM", $BODY_COPYDOM, $body);
            $body = str_replace("%BODY_COPYNAME", $BODY_COPYNAME, $body);


            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender($data['mailfrom']);
            $newActivationEmail->setFrom($data['fromname']);
            $newActivationEmail->addRecipient($mailAdmin);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT. '/' . $logosendmailImage, "banner", "Logo");

            $return = (boolean)$newActivationEmail->send();

            // Check for an error.
            if ($return !== true) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }

            return $return;

        }


        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4gestor.noticias.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.noticias.data', null);
            $app->setUserState('com_virtualdesk.edit4gestor.noticias.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.noticias.data', null);

        }

        /*PLUGIN*/
        public static function getNoticias($cat1, $cat2, $cat3, $cat4, $cat5, $limitBlocks){
            $today = date("Y-m-d");

            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.noticia, b.categoria as categoria, a.data_publicacao'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($db->quoteName('a.data_publicacao') . "<='" . $db->escape($today) . "'")
                ->where($db->quoteName('a.estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat1) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat2) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat3) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat4) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat5) . "'")
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limitBlocks)
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasByCat($cat1, $cat2, $cat3, $cat4, $cat5, $limitBlocks){
            $today = date("Y-m-d");

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_publicacao') . "<='" . $db->escape($today) . "'";
            $where .= 'AND' . $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";

            $where2 = $db->quoteName('a.categoria') . "='" . $db->escape($cat1) . "'";
            $where3 = $db->quoteName('a.categoria') . "='" . $db->escape($cat2) . "'";
            $where4 = $db->quoteName('a.categoria') . "='" . $db->escape($cat3) . "'";
            $where5 = $db->quoteName('a.categoria') . "='" . $db->escape($cat4) . "'";
            $where6 = $db->quoteName('a.categoria') . "='" . $db->escape($cat5) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.noticia, b.categoria as categoria, a.categoria as idCat, a.data_publicacao'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limitBlocks)
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getImgNoticia($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_Noticias_files")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getImgGaleria($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_Noticias_galeria")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasDetailPlugin($ref){
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.titulo, a.noticia, a.categoria as idCategoria, b.categoria as categoria, a.facebook, a.video'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($db->quoteName('a.referencia') . "='" . $db->escape($ref) . "'")
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasDetailByCatID($idCategoria){
            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('referencia'))
                ->from("#__virtualdesk_Noticias")
                ->where($db->quoteName('categoria') . "='" . $db->escape($idCategoria) . "'")
                ->order('data_publicacao DESC')
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNewsName($ref){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('titulo')
                ->from("#__virtualdesk_Noticias")
                ->where($db->quoteName('referencia') . "='" . $db->escape($ref) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getViews($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, visualizacoes'))
                ->from("#__virtualdesk_Noticias")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function updateViews($newVisualizacao, $id){
            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            array_push($fields, 'visualizacoes="'.$db->escape($newVisualizacao).'"');

            $query
                ->update('#__virtualdesk_Noticias')
                ->set($fields)
                ->where(' id = '.$db->escape($id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);

        }


        public static function getCategoriaPlugin($exludedcat1, $exludedcat2, $exludedcat3, $exludedcat4, $exludedcat5){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Noticias_categorias")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->where($db->quoteName('id') . "!='" . $db->escape($exludedcat1) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($exludedcat2) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($exludedcat3) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($exludedcat4) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($exludedcat5) . "'")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoriaPluginByCat($cat1, $cat2, $cat3, $cat4, $cat5){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            $where2 = $db->quoteName('id') . "='" . $db->escape($cat1) . "'";
            $where3 = $db->quoteName('id') . "='" . $db->escape($cat2) . "'";
            $where4 = $db->quoteName('id') . "='" . $db->escape($cat3) . "'";
            $where5 = $db->quoteName('id') . "='" . $db->escape($cat4) . "'";
            $where6 = $db->quoteName('id') . "='" . $db->escape($cat5) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Noticias_categorias")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeCategoriaPlugin($exludedcat1, $exludedcat2, $exludedcat3, $exludedcat4, $exludedcat5, $categoria){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            if(!empty($exludedcat1)){
                $where .= 'AND' . $db->quoteName('id') . "!='" . $db->escape($exludedcat1) . "'";
            }

            if(!empty($exludedcat2)){
                $where .= 'AND' . $db->quoteName('id') . "!='" . $db->escape($exludedcat2) . "'";
            }

            if(!empty($exludedcat3)){
                $where .= 'AND' . $db->quoteName('id') . "!='" . $db->escape($exludedcat3) . "'";
            }

            if(!empty($exludedcat4)){
                $where .= 'AND' . $db->quoteName('id') . "!='" . $db->escape($exludedcat4) . "'";
            }

            if(!empty($exludedcat5)){
                $where .= 'AND' . $db->quoteName('id') . "!='" . $db->escape($exludedcat5) . "'";
            }

            if(!empty($categoria)){
                $where .= 'AND' . $db->quoteName('id') . "!='" . $db->escape($categoria) . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Noticias_categorias")
                ->where($where)
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeCategoriaPluginByCat($cat1, $cat2, $cat3, $cat4, $cat5, $categoria){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";
            $where .= 'AND' . $db->quoteName('id') . "!='" . $db->escape($categoria) . "'";

            $where2 = $db->quoteName('id') . "='" . $db->escape($cat1) . "'";
            $where3 = $db->quoteName('id') . "='" . $db->escape($cat2) . "'";
            $where4 = $db->quoteName('id') . "='" . $db->escape($cat3) . "'";
            $where5 = $db->quoteName('id') . "='" . $db->escape($cat4) . "'";
            $where6 = $db->quoteName('id') . "='" . $db->escape($cat5) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Noticias_categorias")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnoList(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_Noticias_Anos")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAnoListSelect($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('ano')
                ->from("#__virtualdesk_Noticias_Anos")
                ->where($db->quoteName('id') . '=' . $db->escape($id))
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeAnoList($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, ano'))
                ->from("#__virtualdesk_Noticias_Anos")
                ->where($db->quoteName('estado') . '=' . $db->escape('2'))
                ->where($db->quoteName('id') . '!=' . $db->escape($ano))
                ->order('ano DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getMesList(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, mes'))
                ->from("#__virtualdesk_Noticias_Meses")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getMesListSelect($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('mes')
                ->from("#__virtualdesk_Noticias_Meses")
                ->where($db->quoteName('id') . '=' . $db->escape($id))
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeMesList($ano){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, mes'))
                ->from("#__virtualdesk_Noticias_Meses")
                ->where($db->quoteName('id') . '!=' . $db->escape($ano))
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasContent($cat1, $cat2, $cat3, $cat4, $cat5, $limit){
            $today = date("Y-m-d");

            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.noticia, b.categoria as categoria, a.data_publicacao'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($db->quoteName('a.data_publicacao') . "<='" . $db->escape($today) . "'")
                ->where($db->quoteName('a.estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat1) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat2) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat3) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat4) . "'")
                ->where($db->quoteName('a.categoria') . "!='" . $db->escape($cat5) . "'")
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limit)
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasContentByCat($cat1, $cat2, $cat3, $cat4, $cat5, $limit){
            $today = date("Y-m-d");

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_publicacao') . "<='" . $db->escape($today) . "'";
            $where .= 'AND' . $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";

            $where2 = $db->quoteName('a.categoria') . "='" . $db->escape($cat1) . "'";
            $where3 = $db->quoteName('a.categoria') . "='" . $db->escape($cat2) . "'";
            $where4 = $db->quoteName('a.categoria') . "='" . $db->escape($cat3) . "'";
            $where5 = $db->quoteName('a.categoria') . "='" . $db->escape($cat4) . "'";
            $where6 = $db->quoteName('a.categoria') . "='" . $db->escape($cat5) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.noticia, b.categoria as categoria, a.data_publicacao'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limit)
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasContentFiltered($noticia, $mes, $ano, $categoria, $cats1, $cats2, $cats3, $cats4, $cats5, $limit){

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";

            if(!empty($noticia)){
                $nomeNoticia = '%' . $noticia . '%';
                $where .= 'AND' . $db->quoteName('a.titulo') . "LIKE '" . $db->escape($nomeNoticia) . "'";
            }

            if(!empty($mes)){
                $where .= 'AND' . $db->quoteName('a.mes') . "='" . $db->escape($mes) . "'";
            }

            if(!empty($ano)){
                $where .= 'AND' . $db->quoteName('a.ano') . "='" . $db->escape($ano) . "'";
            }

            if(!empty($categoria)){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
            } else {
                if(!empty($cats1)){
                    $where .= 'AND' . $db->quoteName('a.categoria') . "!='" . $db->escape($cats1) . "'";
                }

                if(!empty($cats2)){
                    $where .= 'AND' . $db->quoteName('a.categoria') . "!='" . $db->escape($cats2) . "'";
                }

                if(!empty($cats3)){
                    $where .= 'AND' . $db->quoteName('a.categoria') . "!='" . $db->escape($cats3) . "'";
                }

                if(!empty($cats4)){
                    $where .= 'AND' . $db->quoteName('a.categoria') . "!='" . $db->escape($cats4) . "'";
                }

                if(!empty($cats5)){
                    $where .= 'AND' . $db->quoteName('a.categoria') . "!='" . $db->escape($cats5) . "'";
                }
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.noticia, b.categoria as categoria, a.data_publicacao'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($where)
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limit)
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasContentByCatFiltered($noticia, $mes, $ano, $categoria, $cats1, $cats2, $cats3, $cats4, $cats5, $limit){

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";

            if(!empty($noticia)){
                $nomeNoticia = '%' . $noticia . '%';
                $where .= 'AND' . $db->quoteName('a.titulo') . "LIKE '" . $db->escape($nomeNoticia) . "'";
            }

            if(!empty($mes)){
                $where .= 'AND' . $db->quoteName('a.mes') . "='" . $db->escape($mes) . "'";
            }

            if(!empty($ano)){
                $where .= 'AND' . $db->quoteName('a.ano') . "='" . $db->escape($ano) . "'";
            }

            if(!empty($categoria)){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";

                $where2 = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";
                $where3 = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";
                $where4 = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";
                $where5 = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";
                $where6 = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";

            } else {
                $where2 = $db->quoteName('a.categoria') . "='" . $db->escape($cats1) . "'";
                $where3 = $db->quoteName('a.categoria') . "='" . $db->escape($cats2) . "'";
                $where4 = $db->quoteName('a.categoria') . "='" . $db->escape($cats3) . "'";
                $where5 = $db->quoteName('a.categoria') . "='" . $db->escape($cats4) . "'";
                $where6 = $db->quoteName('a.categoria') . "='" . $db->escape($cats5) . "'";
            }

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.noticia, b.categoria as categoria, a.data_publicacao'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limit)
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasTicker($limitBlocks){
            $today = date("Y-m-d");

            $db = JFactory::getDBO();

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.data_publicacao, a.categoria, b.categoria as catName'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($db->quoteName('a.data_publicacao') . "<='" . $db->escape($today) . "'")
                ->where($db->quoteName('a.estado') . "='" . $db->escape('2') . "'")
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limitBlocks)
            );

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getNoticiasTickerByCat($cat1, $cat2, $cat3, $cat4, $cat5, $limitNews){
            $today = date("Y-m-d");

            $db = JFactory::getDBO();

            $where = $db->quoteName('a.data_publicacao') . "<='" . $db->escape($today) . "'";
            $where .= 'AND' . $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";

            $where2 = $db->quoteName('a.categoria') . "='" . $db->escape($cat1) . "'";
            $where3 = $db->quoteName('a.categoria') . "='" . $db->escape($cat2) . "'";
            $where4 = $db->quoteName('a.categoria') . "='" . $db->escape($cat3) . "'";
            $where5 = $db->quoteName('a.categoria') . "='" . $db->escape($cat4) . "'";
            $where6 = $db->quoteName('a.categoria') . "='" . $db->escape($cat5) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.referencia, a.titulo, a.data_publicacao, a.categoria, b.categoria as catName'))
                ->join('LEFT', '#__virtualdesk_Noticias_categorias AS b ON b.id = a.categoria')
                ->from("#__virtualdesk_Noticias as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->orWhere($where . 'AND' . $where5)
                ->orWhere($where . 'AND' . $where6)
                ->order('a.data_publicacao DESC')
                ->order('a.id DESC')
                ->setLimit($limitNews)
            );

            $data = $db->loadAssocList();
            return ($data);

            $data = $db->loadAssocList();
            return ($data);


        }


        public static function getMonthName($month){
            if($month == '1' || $month == '01'){
                $monthName = 'Jan';
            } else if($month == '2' || $month == '02'){
                $monthName = 'Fev';
            } else if($month == '3' || $month == '03'){
                $monthName = 'Mar';
            } else if($month == '4' || $month == '04'){
                $monthName = 'Abr';
            } else if($month == '5' || $month == '05'){
                $monthName = 'Mai';
            } else if($month == '6' || $month == '06'){
                $monthName = 'Jun';
            } else if($month == '7' || $month == '07'){
                $monthName = 'Jul';
            } else if($month == '8' || $month == '08'){
                $monthName = 'Ago';
            } else if($month == '9' || $month == '09'){
                $monthName = 'Set';
            } else if($month == '10'){
                $monthName = 'Out';
            } else if($month == '11'){
                $monthName = 'Nov';
            } else if($month == '12'){
                $monthName = 'Dez';
            }

            return ($monthName);
        }

    }

?>