<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteCryptHelper
{
    public  $PrefixInputHidden = 'VDINPUT__';
    public  $PrefixInputId     = 'VDINPUTID__';
    public  $PrefixInputMulti  = 'VDINPUTMULTI__';

    private $charsDist         = '_|_|_';

    /**
     * Creates the automatic redirection links
     */
    public function setUrlQueryStringEncrypted()
    {
        $body        = JFactory::getApplication()->getBody();
        $links       = $this->getLinks($body);


        if(!empty($links))
        {
            $uriBase = JURI::base();
            foreach($links[1] as $link)
            {
                $linkParams = explode('?', $link);
                // se tiver mais de um ? tá alguma coisa mal...sai!
                $SizeOfLinkParam = sizeof($linkParams);
                if($SizeOfLinkParam == 2 ) {

                    // verifica se o URL é local e não externo, só codifica os internos
                    // só se começar com o $uriBase ou com / ou com ?
                    if( (strpos($linkParams[0], $uriBase) === 0) || (strpos($linkParams[0], '/') === 0) || ((string)$linkParams[0]==='') )
                    {
                        // TODO: o ideal é nos internos ter 1 parâmetro tipo uvdt=(url virtal desk token)
                        // Pode conter uma ancora do tipo #, nesse caso não encripta essa parte
                        $linkQuery = explode('#', $linkParams[1]);

                        $linkParamsHash = $this->getHashLink($linkQuery[0]);

                        $linkHash       = $linkParams[0] . '?' . $linkParamsHash;

                        // Pode conter uma ancora do tipo #, nesse caso não encripta essa parte
                        if(!empty($linkQuery[1]))  $linkHash =  $linkHash .'#'.$linkQuery[1];

                        $body           = str_replace($link, $linkHash, $body);
                    }
                }
            }
        }

        $formActions = $this->getFormAction($body);
        if(!empty($formActions))
        {
            $uriBase = JURI::base();
            foreach($formActions[1] as $actionlink)
            {
                $linkActionParams = explode('?', $actionlink);
                // se tiver mais de um ? tá alguma coisa mal...sai!
                $SizeOfLinkParam = sizeof($linkActionParams);
                if($SizeOfLinkParam == 2 ) {
                    // verifica se o URL é local e não externo, só codifica os internos
                    // só se começar com o $uriBase ou com /
                    if( (strpos($linkActionParams[0], $uriBase) === 0) || (strpos($linkActionParams[0], '/') === 0) )
                    {
                        // TODO: o ideal é nos internos ter 1 parâmetro tipo uvdt=(url virtal desk token)
                        $linkParamsHash = $this->getHashLink($linkActionParams[1]);
                        $linkHash       = $linkActionParams[0] . '?' . $linkParamsHash;
                        $body           = str_replace($actionlink, $linkHash, $body);
                    }
                }
            }
        }

        return $body;
    }


    public function setUrlQueryStringEncryptedForOneLink($link , $btForce=false)
    {
        $linkHash = '';
        if(!empty($link))
        {
            $uriBase = JURI::base();

                $linkParams = explode('?', $link);
                // se tiver mais de um ? tá alguma coisa mal...sai!
                $SizeOfLinkParam = sizeof($linkParams);
                if($SizeOfLinkParam == 2 ) {

                    // verifica se o URL é local e não externo, só codifica os internos
                    // só se começar com o $uriBase ou com /
                    if( (strpos($linkParams[0], $uriBase) === 0) || (strpos($linkParams[0], '/') === 0) || ($btForce==true) )
                    {
                        // TODO: o ideal é nos internos ter 1 parâmetro tipo uvdt=(url virtal desk token)

                        // Pode conter uma ancora do tipo #, nesse caso não encripta essa parte
                        $linkQuery = explode('#', $linkParams[1]);

                        $linkParamsHash = $this->getHashLink($linkQuery[0]);
                        $linkHash       = $linkParams[0] . '?' . $linkParamsHash;

                        // Pode conter uma ancora do tipo #, nesse caso não encripta essa parte
                        if(!empty($linkQuery[1]))  $linkHash =  $linkHash .'#'.$linkQuery[1];
                    }
                }

        }



        return $linkHash;
    }


    /**
     * Decodes url query string encrypted
     *
     * @param $link
     *
     * @return void
     */
    public function checkURLParamEncoded()
    {
        $uri           = JUri::getInstance();
        $uriEncoded    = $uri->getvar('vdqs','');
        $uriEncoded    = $this->my_simple_crypt($uriEncoded,'d');

        $uri->setQuery($uriEncoded);
    }


    public function setURLQueryDecoded2Array()
    {
        $uri           = JUri::getInstance();
        $uriQuery      = $uri->getQuery();
        $output        = array();
        if(!empty($uriQuery)) parse_str($uriQuery, $output);
        return($output);

    }


    /**
     * Gets all links with the help of a regular expression
     *
     * @param $body
     *
     * @return mixed
     */
    private function getLinks($body)
    {
        $links     = array();
        $linksTmp  = array();
        $tempFull0 = array();
        $tempFull1 = array();

        $Patterns = array();
        $Patterns[]='@<a\s[^>]*href=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</a>@Uism';
        $Patterns[]='@<div\s[^>]*data-vd-url-get=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</div>@Uism';
        $Patterns[]='@<div\s[^>]*data-vd-url-send=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</div>@Uism';
        $Patterns[]='@<div\s[^>]*data-vd-url-getcontent=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</div>@Uism';
        $Patterns[]='@<button\s[^>]*data-vd-url-get=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</button>@Uism';
        $Patterns[]='@<button\s[^>]*data-vd-url-send=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</button>@Uism';
        $Patterns[]='@<button\s[^>]*data-vd-url-getcontent=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</button>@Uism';
        $Patterns[]='@<span\s[^>]*data-vd-url-get=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</span>@Uism';
        $Patterns[]='@<span\s[^>]*data-vd-url-send=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</span>@Uism';
        $Patterns[]='@<span\s[^>]*data-vd-url-getcontent=["\']([^"\']*)["\'][^>]*>[<]?.*[>]?</span>@Uism';

        foreach($Patterns as $keyP => $valueP) {
            $temp0 = array();
            $temp1 = array();
            preg_match_all($valueP, $body, $linksTmp, PREG_PATTERN_ORDER);
            $temp0 = $linksTmp[0];
            $temp1 = $linksTmp[1];
            $tempFull0 = array_merge($tempFull0 , $temp0);
            $tempFull1 = array_merge($tempFull1 , $temp1);
        }

        $links[0] = $tempFull0;
        $links[1] = $tempFull1;
        return $links;
    }


    private function getFormAction($body)
    {
        preg_match_all('@<form\s[^>]*action=[\"\']([^\"\']*)[\"\'][^>]*>[<]?.*[>]?>@Uism', $body, $links, PREG_PATTERN_ORDER);

        return $links;
    }


    /**
     * Creates hash links
     *
     * @param $link
     *
     * @return string
     */
    public function getHashLink($link)
    {
        $date = new DateTime();
        $timeri = mt_rand(100,999);
        $timerf = mt_rand(100,999);

        $linkTimeri = 'vdxtmp'.$timeri.'i=1&';
        $linkTimerf = '&vdxtmp'.$timerf.'f=1';

        $link = $linkTimeri.$link.$linkTimerf;

        $link =  'vdts='. $this->my_simple_crypt($date->getTimestamp(),'e') . '&vdqs='.$this->my_simple_crypt($link,'e');

        return $link;
    }


    private function my_simple_crypt( $string, $action = 'e' ) {
        // you may change these values to your own
        $secret_key = '2020n19s32kA';
        $secret_iv  = '2258n04s41vA';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
            $output = str_replace(['='], [''], $output);
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }

        return $output;
    }


   /*
   *  $vbSetTime:
    *     Os nomes não podem ter o time.Senão vão variar e não conseguimos depois comparar o nome nos pedidos AJAX
    *     Mas o valores podem ter o timer. Logo, no nomes fica a false e nos valores fica a true.
   */
    private function FormInputCRCEncrypt ($string, $vbSetTime=false)
    {
        $charsDist = $this->charsDist;
        if($vbSetTime===true) {
            $nameCRC = $string.$charsDist.hash("crc32b", $string).$charsDist.time();
        }
        else
        {
            $nameCRC = $string.$charsDist.hash("crc32b", $string);
        }
        return($nameCRC);
    }

    /*
    *  $vbSetTime:
    *     Os nomes não podem ter o time.Senão vão variar e não conseguimos depois comparar o nome nos pedidos AJAX
    *     Mas o valores podem ter o timer. Logo, no nomes fica a false e nos valores fica a true.
    */
    private function FormInputCRCDecrypt($string,$vbSetTime=false)
    {
        $RetVal       = '';
        $charsDist    = $this->charsDist;
        $numCharsDist = (int)strlen($this->charsDist);

        // Verificar CRC
        $posCRCStart   = strpos($string, $charsDist );

        // Retira o CRC e compara o CRC
        if($posCRCStart!==false ) {

            if($vbSetTime===true) {
                $posTimerStart = strpos($string, $charsDist, $posCRCStart + $numCharsDist);
                $CRCSend = substr($string, $posCRCStart + $numCharsDist, $posTimerStart - ($posCRCStart + $numCharsDist));
            }
            else {
                $CRCSend = substr($string, $posCRCStart + $numCharsDist);
            }
            $RetVal    = substr($string, 0, $posCRCStart);
            // o cRC tem de dar igual...
            $CRCCalc   = hash("crc32b", $RetVal);
            if((string)$CRCCalc!==(string)($CRCSend))
            {
                // Erro no CRC... retorna NULL
                $RetVal = '';
            }
        }

        return($RetVal);
    }


    public function formInputNameEncrypt($name,$setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $name;
        if( empty($name)) return $name;

        $nameCRC = $this->FormInputCRCEncrypt($name) ; // CRC + time stamp
        $newName = $this->PrefixInputHidden . $this->my_simple_crypt( $nameCRC, 'e');
        return($newName);

    }


    public function formInputNameDecrypt($name, $setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $name;
        if( empty($name)) return $name;

        if( strpos($name, $this->PrefixInputHidden ) === 0) {
          $name    = substr($name, strlen($this->PrefixInputHidden));
          $newName = $this->my_simple_crypt($name, 'd');
          $newName = $this->FormInputCRCDecrypt($newName); // CRC
          return($newName);
        }
        return $name;
    }


    public function formInputValueEncrypt($value,$setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $value;
        if( empty($value)) return $value;

        // Mas o valores podem ter o timer, nos Names não...
        $valueCRC  = $this->FormInputCRCEncrypt($value,true) ;
        $newVal    = $this->my_simple_crypt( $valueCRC, 'e');

        return($newVal);
    }


    public function formInputValueDecrypt($value, $setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $value;
        if( empty($value)) return $value;

        $newVal = $this->my_simple_crypt( $value, 'd');
        $newVal = $this->FormInputCRCDecrypt($newVal,true); // CRC
        return($newVal);
    }


    public function decryptAppPostedInput($setencrypt_forminputhidden)
    {
        $jinput   = JFactory::getApplication()->input;
        $postData = $jinput->post->getArray();

        foreach($postData as $keyPosted => $valPosted) {

            if(strpos($keyPosted, $this->PrefixInputHidden) === 0) {
             // Está encriptada.... vamos desincriptar o nome e o valor e fazer set no post
             $Name = $this->formInputNameDecrypt($keyPosted, $setencrypt_forminputhidden);
             $Val  = $this->formInputValueDecrypt($valPosted, $setencrypt_forminputhidden );

             $jinput->set($Name, $Val);

             // ToDO Devemos eliminar a variável encriptada?
            }

            if(strpos($keyPosted, $this->PrefixInputId) === 0) {
                // Está encriptada.... vamos desincriptar o nome e o valor e fazer set no post
                $Name = $this->setIdInputNameDecrypt($keyPosted, $setencrypt_forminputhidden);
                $Val  = $this->setIdInputValueDecrypt($valPosted, $setencrypt_forminputhidden );

                $jinput->set($Name, $Val);

                // ToDO Devemos eliminar a variável encriptada?
            }
        }
        return('');
    }


    public function setIdInputNameEncrypt($name,$setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $name;
        if( empty($name)) return $name;

        $nameCRC = $this->FormInputCRCEncrypt($name) ; // CRC + time stamp
        $newName = $this->PrefixInputId . $this->my_simple_crypt( $nameCRC, 'e');
        return($newName);
    }


    public function setIdInputNameDecrypt($name, $setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $name;
        if( empty($name)) return $name;

        if( strpos($name, $this->PrefixInputId ) === 0) {
            $name    = substr($name, strlen($this->PrefixInputId));
            $newName = $this->my_simple_crypt($name, 'd');
            $newName = $this->FormInputCRCDecrypt($newName); // CRC
            return($newName);
        }
        return $name;
    }


    public function setIdInputValueEncrypt($value,$setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $value;
        if( empty($value)) return $value;

        // Mas o valores podem ter o timer, nos Names não...
        $valueCRC = $this->FormInputCRCEncrypt($value, true) ; // CRC + time stamp
        $newVal   = $this->my_simple_crypt( $valueCRC, 'e');
        return($newVal);
    }


    public function setIdInputValueDecrypt($value, $setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $value;
        if( empty($value)) return $value;

        // Mas o valores podem ter o timer, nos Names não...
        $newVal = $this->my_simple_crypt( $value, 'd');
        $newVal = $this->FormInputCRCDecrypt($newVal,true); // CRC
        return($newVal);
    }


    /*
    * Os input Multi serão array a serem encriptados, cujo valor já está encriptadom por serem utilizados por Ajax
     * ou cujos valores já foram carregados e encriptados antes... assim é preciso tratar o array recebido de forma diferente
     * Não foi colocada na função geral para desincriptar. Fica em cada "controller" a verificação do nome e do valor
     * (que pode ser um array com valores encriptados ou um array (stringfy) completamente encriptado
    */
    public function setIdInputMultiNameEncrypt($name,$setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $name;
        if( empty($name)) return $name;

        $nameCRC = $this->FormInputCRCEncrypt($name) ; // CRC + time stamp
        $newName = $this->PrefixInputMulti . $this->my_simple_crypt( $nameCRC, 'e');
        return($newName);

    }


    public function setIdInputMultiNameDecrypt($name, $setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $name;
        if( empty($name)) return $name;

        if( strpos($name, $this->PrefixInputMulti ) === 0) {
            $name    = substr($name, strlen($this->PrefixInputMulti));
            $newName = $this->my_simple_crypt($name, 'd');
            $newName = $this->FormInputCRCDecrypt($newName); // CRC
            return($newName);
        }

        return $name;
    }


    public function setIdInputMultiValueEncrypt($value,$setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $value;
        if( empty($value)) return $value;

        if(is_array($value)) {
            $newVal = array();
            foreach($value as $key => $val) {
                // os valores podem ter o timer, nos Names não...
                $valCRC   = $this->FormInputCRCEncrypt($val, true) ; // CRC + time stamp
                $newVal[] = $this->my_simple_crypt( $valCRC, 'e');
            }
        }
        else {
            // os valores podem ter o timer, nos Names não...
            $valueCRC = $this->FormInputCRCEncrypt($value, true) ; // CRC + time stamp
            $newVal   = $this->my_simple_crypt( $valueCRC, 'e');
        }

        return($newVal);
    }


    public function setIdInputMultiValueDecrypt($value, $setencrypt_forminputhidden)
    {
        if($setencrypt_forminputhidden !== '1') return $value;
        if( empty($value)) return $value;

        if(is_array($value)) {
            $newVal = array();
            foreach($value as $key => $val) {
                $newValTmp = $this->my_simple_crypt( $val, 'd');
                // Mas o valores podem ter o timer, nos Names não...
                $newVal[]  = $this->FormInputCRCDecrypt($newValTmp,true); // CRC
            }
        }
        else {
            $newVal = $this->my_simple_crypt( $value, 'd');
            // Mas o valores podem ter o timer, nos Names não..
            $newVal = $this->FormInputCRCDecrypt($newVal, true); // CRC
        }

        return($newVal);
    }


}
