<?php
/**
 * Created by PhpStorm.
 * User: Camacho
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskTableAlerta', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta.php');
JLoader::register('VirtualDeskTableAlertaCategoria', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_categoria.php');
JLoader::register('VirtualDeskTableAlertaSubCategoria', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_subcategoria.php');
JLoader::register('VirtualDeskTableAlertaHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_historico.php');
JLoader::register('VirtualDeskTableAlertaEncaminharHist', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_encaminhar_hist.php');
JLoader::register('VirtualDeskTableAlertaEncaminharUserHist', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_encaminhar_user_hist.php');
JLoader::register('VirtualDeskTableAlertaEncaminharGroupHist', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_encaminhar_group_hist.php');
JLoader::register('VirtualDeskTableAlertaEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_estado_historico.php');
JLoader::register('VirtualDeskTableAlertaProcManagerHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta_procmanager_historico.php');
JLoader::register('VirtualDeskSiteAlertsFilesHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_alerts_files.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');
JLoader::register('VirtualDeskSiteTarefaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tarefa.php');
JLoader::register('VirtualDeskSiteNotificacaoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_notificacao.php');

    class VirtualDeskSiteAlertaHelper
    {

        const tagchaveModulo = 'alerta';

        /*Retorna as categorias principais do alerta*/
        public static function getCategorias($lang, $idConcelhoAlerta)
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
            }
            else {
                $CatName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('ID_categoria,' . $CatName))
                ->from("#__virtualdesk_alerta_categoria")
                ->order('name_PT ASC')
                ->where($db->quoteName('id_concelho') . '=' . $db->escape($idConcelhoAlerta))
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getCatSelect($categoria, $idConcelhoAlerta){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('name_PT')
                ->from("#__virtualdesk_alerta_categoria")
                ->where($db->quoteName('ID_categoria') . '=' . $db->escape($categoria))
                ->where($db->quoteName('id_concelho') . '=' . $db->escape($idConcelhoAlerta))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeCat($categoria, $idConcelhoAlerta){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('ID_categoria, name_PT'))
                ->from("#__virtualdesk_alerta_categoria")
                ->where($db->quoteName('ID_categoria') . '!=' . $db->escape($categoria))
                ->where($db->quoteName('id_concelho') . '=' . $db->escape($idConcelhoAlerta))
                ->order('name_PT ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelhoName($concelhoID){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . '=' . $db->escape($concelhoID))
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Retorna o nome da categoria*/
        public static function getCatName($categoria)
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
            }
            else {
                $CatName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($CatName)
                ->from("#__virtualdesk_alerta_categoria")
                ->where($db->quoteName('ID_categoria') . '=' . $db->escape($categoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Retorna as subcategorias do alerta tendo em conta o id da categoria principal*/
        public static function getAlertaSubcategoria($idwebsitelist,$onAjaxVD=0)
        {
            if (empty($idwebsitelist)) return false;


            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
            }
            else {
                $CatName = 'name_EN';
            }

            // Verifica se o email nos JosUsers


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('Id_subcategoria,' . $CatName . ', Id_categoria'))
                    ->from("#__virtualdesk_alerta_subcategoria")
                    ->where($db->quoteName('Id_categoria') . '=' . $db->escape($idwebsitelist))
                    ->order($CatName . ' ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('Id_subcategoria as id,' . $CatName . ' as name'))
                    ->from("#__virtualdesk_alerta_subcategoria")
                    ->where($db->quoteName('Id_categoria') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }


            return ($data);
        }

        /*Retorna o nome da subcategoria*/
        public static function getSubCatName($subcategoria)
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $SubCatName = 'name_PT';
            }
            else {
                $SubCatName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select($SubCatName)
                ->from("#__virtualdesk_alerta_subcategoria")
                ->where($db->quoteName('Id_subcategoria') . '=' . $db->escape($subcategoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Retorna o email associado a uma categoria*/
        public static function getCatEmail($categoria)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('conf_email')
                ->from("#__virtualdesk_alerta_categoria")
                ->where($db->quoteName('ID_categoria') . '=' . $db->escape($categoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Retorna o email associado a uma categoria*/
        public static function getSubCatEmail($subcategoria)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('conf_email')
                ->from("#__virtualdesk_alerta_subcategoria")
                ->where($db->quoteName('Id_subcategoria') . '=' . $db->escape($subcategoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Dropdown Concelho*/
        public static function getAlertaConcelhosAll()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->order('concelho ASC')
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->concelho
                );
            }
            return($response);
        }

        public static function excludesubCat($categoria, $subcategoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('Id_subcategoria, name_PT as name'))
                ->from("#__virtualdesk_alerta_subcategoria")
                ->where($db->quoteName('Id_categoria') . "='" . $db->escape($categoria) . "'")
                ->where($db->quoteName('Id_subcategoria') . "!='" . $db->escape($subcategoria) . "'")
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        /*Dropdown Freguesias*/
        public static function getAlertaFreguesia($concelhoAlerta)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelhoAlerta) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function excludeFreguesia($concelhoAlerta, $freg){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id as id_freguesia, a.freguesia as freguesia'))
                ->from("#__virtualdesk_digitalGov_freguesias as a")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelhoAlerta) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($freg) . "'")
                ->order('a.id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getAlertaFreguesiaID($getInputAlerta_Id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_alerta")
                ->where($db->quoteName('Id_alerta') . "='" . $db->escape($getInputAlerta_Id) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Nome Freguesia*/
        public static function getFregName($freguesia)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Lista as ocorrencias em espera*/
        public static function getOcorrenciasEspera($dbprefix){

            $table  = " ( SELECT a.Id_alerta, b.name_PT as categoria, c.name_PT as subcategoria, descricao, local, d.freguesia as freguesia, DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as dataCriacao, e.estado_PT as estado, a.latitude as latitude, a.longitude as longitude";
            $table .= " FROM ".$dbprefix."virtualdesk_alerta as a ";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_estados AS e ON e.id_estado = a.estado";
            $table .= " ORDER BY dataCriacao DESC ) temp ";

            return ($table);

        }

        /*Lista as ocorrencias em análise*/
        public static function getOcorrenciasAnalise($lang){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
                $StateName = 'estado_PT';
            }
            else {
                $CatName = 'name_EN';
                $StateName = 'estado_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.Id_alerta, b.' . $CatName . ' as categoria, c.' .$CatName . ' as subcategoria, descricao, local, d.freguesia as freguesia, data_criacao, e.' .$StateName . ' as estado, a.latitude as latitude, a.longitude as longitude'))
                ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                ->from("#__virtualdesk_alerta as a")
                ->where($db->quoteName('estado') . '= 2')
                ->order('data_criacao DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        /*Lista as ocorrencias resolvidas*/
        public static function getOcorrenciasResolvidas($lang){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
                $StateName = 'estado_PT';
            }
            else {
                $CatName = 'name_EN';
                $StateName = 'estado_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.Id_alerta, b.' . $CatName . ' as categoria, c.' .$CatName . ' as subcategoria, descricao, local, d.freguesia as freguesia, data_criacao, e.' .$StateName . ' as estado, a.latitude as latitude, a.longitude as longitude'))
                ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                ->from("#__virtualdesk_alerta as a")
                ->where($db->quoteName('estado') . '= 3')
                ->order('data_criacao DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        /*SLIDER ultimas ocorrencias*/
        public static function getUltimasOcorrenciasSlider($lang){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $CatName = 'name_PT';
            }
            else {
                $CatName = 'name_EN';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.Id_alerta, b.' . $CatName . ' as categoria, c.' .$CatName . ' as subcategoria, d.freguesia as freguesia, a.data_criacao, a.estado'))
                ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->from("#__virtualdesk_alerta as a")
                ->order('a.data_criacao DESC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getcolorestado($Estado){

            if($Estado == 'Em espera' || $Estado == 'On hold'){
                $color='espera';
            }

            if($Estado == 'Em análise' || $Estado == 'Under analysis'){
                $color='analise';
            }

            if($Estado == 'Concluído' || $Estado == 'Completed'){
                $color='concluido';
            }
            return ($color);
        }

        /*Gera código aleatório para a referência da ocorrencia*/
        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
        }

        /*Retorna a Referência da Categoria*/
        public static function getReferencia($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('Ref')
                ->from("#__virtualdesk_alerta_categoria")
                ->where($db->quoteName('ID_categoria') . '=' . $db->escape($categoria))
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Verifica de a referencia da ocorrencia existe*/
        public static function CheckReferencia($codi){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('codOco')
                ->from("#__virtualdesk_alerta")
                ->where($db->quoteName('codOco') . "='" . $db->escape($codi) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getReferenciaById($alerta_id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('codOco')
                ->from("#__virtualdesk_alerta")
                ->where($db->quoteName('Id_alerta') . "=" . $db->escape($alerta_id))
            );
            $data = $db->loadColumn();

            $vsRet = '';
            if(!empty($data[0])) $vsRet = $data[0];
            return ($vsRet);
        }

        public static function getIdEstado($estado){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id_estado')
                ->from("#__virtualdesk_alerta_estados ")
                ->where($db->quoteName('estado_PT') . "='" . $db->escape($estado) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Pesquisa Referências*/
         public static function getresultados($code){
             $lang = VirtualDeskHelper::getLanguageTag();
             if( empty($lang) or ($lang='pt_PT') ) {
                 $CatName = 'name_PT';
                 $StateName = 'estado_PT';
             }
             else {
                 $CatName = 'name_EN';
                 $StateName = 'estado_EN';
             }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.Id_alerta, a.codOco, b.' .$CatName .' as categoria, c.' .$CatName .' as subcategoria, a.descricao, d.freguesia as freguesia, a.pontos_referencia, a.local, a.latitude, a.longitude, a.data_criacao, a.data_alteracao, e.' . $StateName .' as estado, a.observacoes, a.nif'))
                ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                ->from("#__virtualdesk_alerta as a")
                ->where($db->quoteName('codOco') . "='" . $db->escape($code) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getNifHistorico($ref){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('a.nif as nif')
                ->from("#__virtualdesk_alerta as a ")
                ->where($db->quoteName('codOco') . "='" . $db->escape($ref) . "'")
                );
            $data = $db->loadResult();
            return ($data);
        }

        /*Pesquisa Histório de Ocorrencias do utilizador*/
        public static function gethistorico($nifUser, $dbprefix){

            $db = JFactory::getDBO();
            $table  = " ( SELECT a.codOco, b.name_PT as categoria, c.name_PT as subcategoria, d.freguesia as freguesia, data_criacao, e.estado_PT as estado";
            $table .= " FROM ".$dbprefix."virtualdesk_alerta as a ";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia";
            $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_estados AS e ON e.id_estado = a.estado";
            $table .= " WHERE a.nif =" . $db->escape($nifUser);
            $table .= " ) temp ";

            return ($table);

        }

        /* Verifica se foi feito submit do formulário de novo alerta */
        public static function checkNewAlertaFormData()
        {
            // Check for request forgeries.
            if(!JSession::checkToken()) return(false);
            $app          = JFactory::getApplication();
            $newAlertSubmit = $app->input->get('novoalerta',null, 'STRING');
            if((string)$newAlertSubmit != 'novoalerta') return(false);
            return(true);

        }

        /* Inicializa o objecto "limpo" de a submissão de um formulário */
        public static function getNewAlertaCleanPostedData()
        {
            $data = array();
            $data['idwebsitelist']     = null;
            $data['vdmenumain']  =  null;
            $data['descricao']  =  null;
            $data['freguesias'] =  null;
            $data['sitio'] =  null;
            $data['morada'] = null;
            $data['refs'] = null;
            $data['nome']     = null;
            $data['fiscalid']  =  null;
            $data['email1']   =  null;
            $data['email2']  =  null;
            $data['telefone'] =  null;

            return($data);
        }

        /*
        * Inicializa o array com dados se não existirem
        * Útil pra o caso de ter sido feito um post e não forma preenchidos alguns campos
        * Pode acontecer no caso de falha do javascript. Assim garantimos que depois o facto do o indice do campos não estar definido não "rebenta" o php
        */
        public static function getNewAlertaSetIfNullPostData($data)
        {
            if( !is_array($data) )$data = array();
            if( empty($data['idwebsitelist']) )          $data['idwebsitelist']     = null;
            if( empty($data['vdmenumain']) )       $data['vdmenumain']  =  null;
            if( empty($data['descricao']) )           $data['descricao']   =  null;
            if( empty($data['freguesias']) )          $data['freguesias']  =  null;
            if( empty($data['sitio']) )         $data['sitio'] =  null;
            if( empty($data['morada']) )         $data['morada'] =  null;
            if( empty($data['refs']) )   $data['refs'] =  null;
            if( empty($data['nome']) )  $data['nome'] = null;
            if( empty($data['fiscalid']) )        $data['fiscalid'] = null;
            if( empty($data['email1']) )     $data['email1'] = null;
            if( empty($data['email2']) )        $data['email2'] =  null;
            if( empty($data['telefone']) )       $data['telefone'] = null;

            return($data);
        }

        /*Retorna o id da subcategoria*/
        public static function GetSubCatID($subcategoria){

            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $subcat = 'name_PT';
            }
            else {
                $subcat = 'name_EN';
            }


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('Id_subcategoria,'.$subcat.' as name'))
                ->from("#__virtualdesk_alerta_subcategoria")
                ->where($db->quoteName($subcat) . "='" . $db->escape($subcategoria) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        /*Verifica se o email já existe*/
        public static function seeEmailExiste($email){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('email')
                ->from("#__virtualdesk_alerta")
                ->where($db->quoteName('email') . "='" . $db->escape($email) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        /*Verifica se o email já existe*/
        public static function seeEmailExisteNIF($email){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nif')
                ->from("#__virtualdesk_alerta")
                ->where($db->quoteName('email') . "='" . $db->escape($email) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        /*Insere o novo alerta na BD*/
        public static function saveNewAlerta($categoria, $referencia, $subcategoria, $descricao, $concelhoAlerta, $freguesia, $sitio, $morada, $pontosReferencia,
                                             $nome, $nif, $email1, $telefone, $lat, $long, $balcao=0)
        {
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('codOco','categoria','subcategoria','descricao','concelho','freguesia','sitio','local','pontos_referencia','estado','publishFP','utilizador','email','nif','telefone','latitude','longitude','balcao');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($categoria)), $db->quote($db->escape($subcategoria)), $db->quote($db->escape($descricao)), $db->quote($db->escape($concelhoAlerta)), $db->quote($db->escape($freguesia)), $db->quote($db->escape($sitio)), $db->quote($db->escape($morada)), $db->quote($db->escape($pontosReferencia)), $db->quote('1'), $db->quote('0'), $db->quote($db->escape($nome)), $db->quote($db->escape($email1)), $db->quote($db->escape($nif)), $db->quote($db->escape($telefone)),  $db->quote($db->escape($lat)),  $db->quote($db->escape($long)), $db->quote($db->escape($balcao)));
            $query
                ->insert($db->quoteName('#__virtualdesk_alerta'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);

        }

        public static function saveNewAlertaBalcao($categoria, $referencia, $subcategoria, $descricao, $concelhoAlerta, $freguesia, $sitio, $morada, $pontosReferencia,
                                             $nome, $nif, $email1, $telefone, $lat, $long, $balcao)
        {
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('codOco','categoria','subcategoria','descricao','concelho','freguesia','sitio','local','pontos_referencia','estado','publishFP','utilizador','email','nif','telefone','latitude','longitude','balcao');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($categoria)), $db->quote($db->escape($subcategoria)), $db->quote($db->escape($descricao)), $db->quote($db->escape($concelhoAlerta)), $db->quote($db->escape($freguesia)), $db->quote($db->escape($sitio)), $db->quote($db->escape($morada)), $db->quote($db->escape($pontosReferencia)), $db->quote('1'), $db->quote('0'), $db->quote($db->escape($nome)), $db->quote($db->escape($email1)), $db->quote($db->escape($nif)), $db->quote($db->escape($telefone)),  $db->quote($db->escape($lat)),  $db->quote($db->escape($long)), $db->quote($db->escape($balcao)));
            $query
                ->insert($db->quoteName('#__virtualdesk_alerta'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);

        }


        /*Insere o alerta editado na BD*/
        public static function saveEditedAlerta4User($alerta_id, $categoria, $subcategoria, $descricao, $concelhoAlerta, $freguesia, $sitio, $morada, $pontosReferencia, $nome, $nif, $email, $telefone, $lat, $long)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4users'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria)) array_push($fields, 'categoria='.$db->escape($categoria));
            if(!is_null($subcategoria)) array_push($fields, 'subcategoria='.$db->escape($subcategoria));
            if(!is_null($descricao))  array_push($fields, 'descricao="'.$db->escape($descricao).'"');
            if(!is_null($concelhoAlerta))  array_push($fields, 'concelho='.$db->escape($concelhoAlerta));
            if(!is_null($freguesia))  array_push($fields, 'freguesia='.$db->escape($freguesia));
            if(!is_null($sitio))  array_push($fields, 'sitio="'.$db->escape($sitio).'"');
            if(!is_null($morada))  array_push($fields, 'local="'.$db->escape($morada).'"');
            if(!is_null($pontosReferencia))  array_push($fields, 'pontos_referencia="'.$db->escape($pontosReferencia).'"');
            if(!is_null($nome))  array_push($fields, 'utilizador="'.$db->escape($nome).'"');
            if(!is_null($email))  array_push($fields, 'email="'.$db->escape($email).'"');
            if(!is_null($nif))  array_push($fields, 'nif='.$db->escape($nif));
            if(!is_null($telefone))  array_push($fields, 'telefone='.$db->escape($telefone));
            if(!is_null($lat))  array_push($fields, 'latitude="'.$db->escape($lat).'"');
            if(!is_null($long))  array_push($fields, 'longitude="'.$db->escape($long).'"');

            $query
                ->update('#__virtualdesk_alerta')
                ->set($fields)
                ->where(' Id_alerta = '.$db->escape($alerta_id) .' and nif='.$UserSessionNIF);

            $db->setQuery($query);

            $result = (boolean) $db->execute();

            return($result);
        }


        /*Insere o alerta editado na BD*/
        public static function saveEditedAlerta4Manager($alerta_id, $categoria, $subcategoria, $descricao, $concelhoAlerta, $freguesia, $sitio, $morada, $pontosReferencia, $nome, $nif, $email, $telefone, $lat, $long)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            if(!is_null($categoria)) array_push($fields, 'categoria='.$db->escape($categoria));
            if(!is_null($subcategoria)) array_push($fields, 'subcategoria='.$db->escape($subcategoria));
            if(!is_null($descricao))  array_push($fields, 'descricao="'.$db->escape($descricao).'"');
            if(!is_null($concelhoAlerta))  array_push($fields, 'concelho='.$db->escape($concelhoAlerta));
            if(!is_null($freguesia))  array_push($fields, 'freguesia='.$db->escape($freguesia));
            if(!is_null($sitio))  array_push($fields, 'sitio="'.$db->escape($sitio).'"');
            if(!is_null($morada))  array_push($fields, 'local="'.$db->escape($morada).'"');
            if(!is_null($pontosReferencia))  array_push($fields, 'pontos_referencia="'.$db->escape($pontosReferencia).'"');
            if(!is_null($nome))  array_push($fields, 'utilizador="'.$db->escape($nome).'"');
            if(!is_null($email))  array_push($fields, 'email="'.$db->escape($email).'"');
            if(!is_null($nif))  array_push($fields, 'nif='.$db->escape($nif));
            if(!is_null($telefone))  array_push($fields, 'telefone='.$db->escape($telefone));
            if(!is_null($lat))  array_push($fields, 'latitude="'.$db->escape($lat).'"');
            if(!is_null($long))  array_push($fields, 'longitude="'.$db->escape($long).'"');

            $query
                ->update('#__virtualdesk_alerta')
                ->set($fields)
                ->where(' Id_alerta = '.$db->escape($alerta_id));

            $db->setQuery($query);

            $result = (boolean) $db->execute();

            return($result);
        }


        /* Envia Mail Admin */
        public static function SendEmailOcorrenciaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone,$vbUpdate = false){

            $config = JFactory::getConfig();

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('novoalerta','email2admin');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }

            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $layoutPathPDF = $obPlg->getPluginLayoutAtivePath('novoalerta','pdf2admin');

            if((string)$layoutPathPDF=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_PDF_LAYOUT_ERROR'));
                exit();
            }

            require_once(JPATH_ROOT.$layoutPathPDF);

            $pdf = new VirtualDeskPDFHelper();

            if(empty($lat)){
                $latitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                $longitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
            } else{
                $latitude = $lat;
                $longitude = $long;
            }

            $obParam      = new VirtualDeskSiteParamsHelper();

            $emailAdmin = $obParam->getParamsByTag('mailAdminAlerta');

            $pdf->pdfAlertaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $latitude, $longitude, $fiscalid, $telefone);

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO',$nomeMunicipio);
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO',$nomeMunicipio);

            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAILADMIN_CORPO',$referencia);
            $BODY_MESSAGE2       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAILADMIN_2CORPO2');

            if($vbUpdate ==true) {
                $BODY_TITLE      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO_UPDATED',$nomeMunicipio);
                $BODY_TITLE2     = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO_UPDATED',$nomeMunicipio);
                $BODY_MESSAGE    = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO_UPDATED',$referencia);
            }

            $BODY_REFERENCIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_REFERENCIA');
            $BODY_REFERENCIA_VALUE       = JText::sprintf($referencia);
            $BODY_CATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CATEGORIA');
            $BODY_CATEGORIA_VALUE       = JText::sprintf($catName);
            $BODY_SUBCATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SUBCATEGORIA');
            $BODY_SUBCATEGORIA_VALUE       = JText::sprintf($subcatName);
            $BODY_DESCRICAO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE       = JText::sprintf($descricao);
            $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FREGUESIA');
            $BODY_FREGUESIA_VALUE       = JText::sprintf($fregName);
            $BODY_SITIO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SITIO');
            $BODY_SITIO_VALUE       = JText::sprintf($sitio);
            $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_MORADA');
            $BODY_MORADA_VALUE       = JText::sprintf($morada);
            $BODY_PTOSREF_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_PTOSREF');
            $BODY_PTOSREF_VALUE       = JText::sprintf($ptosrefs);
            $BODY_EMAILNAME_VALUE       = JText::sprintf($emailCopyrightGeral);
            $BODY_TELEFNAME_VALUE       = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_DOMINIOMUNICIPIO_VALUE       = JText::sprintf($dominioMunicipio);
            $BODY_LINKMUNICIPIO_VALUE       = JText::sprintf($LinkCopyright);
            $BODY_COPYRIGHT_VALUE       = JText::sprintf($copyrightAPP);
            $BODY_FILES_LIST_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FILES_LIST_TITLE');


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_2MESSAGE",$BODY_MESSAGE2, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_TITLE",$BODY_REFERENCIA_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_VALUE",$BODY_REFERENCIA_VALUE, $body);
            $body      = str_replace("%BODY_CATEGORIA_TITLE",$BODY_CATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_CATEGORIA_VALUE",$BODY_CATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_TITLE",$BODY_SUBCATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_VALUE",$BODY_SUBCATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_DESCRICAO_TITLE",$BODY_DESCRICAO_TITLE, $body);
            $body      = str_replace("%BODY_DESCRICAO_VALUE",$BODY_DESCRICAO_VALUE, $body);
            $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
            $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
            $body      = str_replace("%BODY_SITIO_TITLE",$BODY_SITIO_TITLE, $body);
            $body      = str_replace("%BODY_SITIO_VALUE",$BODY_SITIO_VALUE, $body);
            $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
            $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
            $body      = str_replace("%BODY_PTOSREF_TITLE",$BODY_PTOSREF_TITLE, $body);
            $body      = str_replace("%BODY_PTOSREF_VALUE",$BODY_PTOSREF_VALUE, $body);
            $body      = str_replace("%BODY_EMAILNAME_VALUE",$BODY_EMAILNAME_VALUE, $body);
            $body      = str_replace("%BODY_TELEFNAME_VALUE",$BODY_TELEFNAME_VALUE, $body);
            $body      = str_replace("%BODY_DOMINIOMUNICIPIO_VALUE",$BODY_DOMINIOMUNICIPIO_VALUE, $body);
            $body      = str_replace("%BODY_LINKMUNICIPIO_VALUE",$BODY_LINKMUNICIPIO_VALUE, $body);
            $body      = str_replace("%BODY_COPYRIGHT_VALUE",$BODY_COPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_FILES_LIST_TITLE",$BODY_FILES_LIST_TITLE, $body);

            // Lista de ficheiros para downlaod
            $objAlertFile = new VirtualDeskSiteAlertsFilesHelper();
            $arFileList   = $objAlertFile->getFileGuestLinkByRefId($referencia);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile){
                $FileList21Html .= "<a href='". JRoute::_($rowFile->guestlink, false)   ."' target='_blank'>".$rowFile->desc."</a><br>\n\r";
            }
            $body      = str_replace("%BODY_FILES_LIST",$FileList21Html, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($emailAdmin);
            $newActivationEmail->setSubject($data['sitename']);
            $logosendmailImage = $obParam->getParamsByTag('logosendmailalerta');
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $pdf->SetIdProcesso ($referencia);
            $pdf->SetTagProcesso ('Ocorrências');
            $resSave = $pdf->saveFile();
            if($resSave==true) {
                $resFilepath = $pdf->resFileSaved ['filepath'];
                $resFilename = $pdf->resFileSaved ['filename'];
                $newActivationEmail->addAttachment(JPATH_BASE.'/'.$resFilepath.'/'.$resFilename);
            }

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }


        /* Envia Mail ao utilizador */
        public static function SendEmailOcorrencia($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone, $vbUpdate = false){

            $config = JFactory::getConfig();

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('novoalerta','email2user');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }
            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $layoutPathPDF = $obPlg->getPluginLayoutAtivePath('novoalerta','pdf2user');


            if((string)$layoutPathPDF=='') {
                // Se não carregar o caminho do layout sai com erro...
                echo (JText::_('COM_VIRTUALDESK_LOAD_PDF_LAYOUT_ERROR'));
                exit();
            }
            require_once(JPATH_ROOT.$layoutPathPDF);

            $pdf = new VirtualDeskPDFHelper();

            if(empty($lat)){
                $latitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                $longitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
            } else{
                $latitude = $lat;
                $longitude = $long;
            }

            $obParam      = new VirtualDeskSiteParamsHelper();

            $pdf->pdfAlertaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $latitude, $longitude, $fiscalid, $telefone);

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO',$nomeMunicipio);
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO',$nomeMunicipio);

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_INTRO',$nome);
            $BODY_BEFORELINE      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_INTRO2');
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO');
            $BODY_AFTERMESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO1');
            $BODY_SECONDLINE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO2',$nomeMunicipio, $referencia);
            $BODY_THIRDLINE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO3');

            if($vbUpdate ==true) {
                $BODY_TITLE      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO_UPDATED',$nomeMunicipio);
                $BODY_TITLE2     = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO_UPDATED',$nomeMunicipio);
                $BODY_MESSAGE    = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO_UPDATED',$referencia);
            }

            $BODY_REFERENCIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_REFERENCIA');
            $BODY_REFERENCIA_VALUE       = JText::sprintf($referencia);
            $BODY_CATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CATEGORIA');
            $BODY_CATEGORIA_VALUE       = JText::sprintf($catName);
            $BODY_SUBCATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SUBCATEGORIA');
            $BODY_SUBCATEGORIA_VALUE       = JText::sprintf($subcatName);
            $BODY_DESCRICAO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE       = JText::sprintf($descricao);
            $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FREGUESIA');
            $BODY_FREGUESIA_VALUE       = JText::sprintf($fregName);
            $BODY_SITIO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SITIO');
            $BODY_SITIO_VALUE       = JText::sprintf($sitio);
            $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_MORADA');
            $BODY_MORADA_VALUE       = JText::sprintf($morada);
            $BODY_PTOSREF_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_PTOSREF');
            $BODY_PTOSREF_VALUE       = JText::sprintf($ptosrefs);
            $BODY_LAT_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LATITUDE');
            $BODY_LAT_VALUE       = JText::sprintf($latitude);
            $BODY_LONG_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_LONGITUDE');
            $BODY_LONG_VALUE       = JText::sprintf($longitude);
            $BODY_MUNICIPIONAME_VALUE       = JText::sprintf($copyrightAPP);
            $BODY_LINKNAME_VALUE       = JText::sprintf($LinkCopyright);
            $BODY_DOMINIONAME_VALUE       = JText::sprintf($dominioMunicipio);
            $BODY_TELENAME_VALUE       = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_MAILNAME_VALUE       = JText::sprintf($emailCopyrightGeral);
            $BODY_FILES_LIST_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FILES_LIST_TITLE');

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_BEFORELINE",$BODY_BEFORELINE, $body );
            $body      = str_replace("%BODY_AFTERMESSAGE",$BODY_AFTERMESSAGE, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_TITLE",$BODY_REFERENCIA_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_VALUE",$BODY_REFERENCIA_VALUE, $body);
            $body      = str_replace("%BODY_CATEGORIA_TITLE",$BODY_CATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_CATEGORIA_VALUE",$BODY_CATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_TITLE",$BODY_SUBCATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_VALUE",$BODY_SUBCATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_DESCRICAO_TITLE",$BODY_DESCRICAO_TITLE, $body);
            $body      = str_replace("%BODY_DESCRICAO_VALUE",$BODY_DESCRICAO_VALUE, $body);
            $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
            $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
            $body      = str_replace("%BODY_SITIO_TITLE",$BODY_SITIO_TITLE, $body);
            $body      = str_replace("%BODY_SITIO_VALUE",$BODY_SITIO_VALUE, $body);
            $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
            $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
            $body      = str_replace("%BODY_PTOSREF_TITLE",$BODY_PTOSREF_TITLE, $body);
            $body      = str_replace("%BODY_PTOSREF_VALUE",$BODY_PTOSREF_VALUE, $body);
            $body      = str_replace("%BODY_LAT_TITLE",$BODY_LAT_TITLE, $body);
            $body      = str_replace("%BODY_LAT_VALUE",$BODY_LAT_VALUE, $body);
            $body      = str_replace("%BODY_LONG_TITLE",$BODY_LONG_TITLE, $body);
            $body      = str_replace("%BODY_LONG_VALUE",$BODY_LONG_VALUE, $body);
            $body      = str_replace("%BODY_MUNICIPIONAME_VALUE",$BODY_MUNICIPIONAME_VALUE, $body);
            $body      = str_replace("%BODY_LINKNAME_VALUE ",$BODY_LINKNAME_VALUE , $body);
            $body      = str_replace("%BODY_DOMINIONAME_VALUE",$BODY_DOMINIONAME_VALUE, $body);
            $body      = str_replace("%BODY_TELENAME_VALUE",$BODY_TELENAME_VALUE, $body);
            $body      = str_replace("%BODY_MAILNAME_VALUE",$BODY_MAILNAME_VALUE, $body);
            $body      = str_replace("%BODY_FILES_LIST_TITLE",$BODY_FILES_LIST_TITLE, $body);

            // Lista de ficheiros para downlaod
            $objAlertFile = new VirtualDeskSiteAlertsFilesHelper();
            $arFileList   = $objAlertFile->getFileGuestLinkByRefId($referencia);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile){
                $FileList21Html .= "<a href='". JRoute::_($rowFile->guestlink, false)   ."' target='_blank'>".$rowFile->desc."</a><br>\n\r";
            }
            $body      = str_replace("%BODY_FILES_LIST",$FileList21Html, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($data['sitename']);

            $logosendmailImage = $obParam->getParamsByTag('logosendmailalerta');
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");


            $pdf->SetIdProcesso ($referencia);
            $pdf->SetTagProcesso ('Ocorrências');
            $resSave = $pdf->saveFile();
            if($resSave==true) {
                $resFilepath = $pdf->resFileSaved ['filepath'];
                $resFilename = $pdf->resFileSaved ['filename'];
                $newActivationEmail->addAttachment(JPATH_BASE.'/'.$resFilepath.'/'.$resFilename);
            }



            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }


        /* Envia Mail para o endereço associado à categoria e subcategoria - usa template do admin*/
        public static function SendEmailOcorrenciaCategoria($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone, $vbUpdate = false, $IdCategoria, $IdSubCategoria){

            $config = JFactory::getConfig();

            // verifica email da categoria,
            $CategoriaSendMail = VirtualDeskSiteAlertaHelper::getCatEmail($IdCategoria);
            // verifica email da Subcategoria, se não estiver definido sai do método
            $SubCategoriaSendMail = VirtualDeskSiteAlertaHelper::getSubCatEmail($IdSubCategoria);
            // senão tiver nenhum getNameAndEmailDoManagerEAdmin Cat ou Subcat definido sai do método
            if(empty($CategoriaSendMail) && empty($SubCategoriaSendMail))  return true;

            /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
            $obPlg      = new VirtualDeskSitePluginsHelper();
            $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('novoalerta','email2admin');

            if((string)$layoutPathEmail=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }

            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $layoutPathPDF = $obPlg->getPluginLayoutAtivePath('novoalerta','pdf2admin');

            if((string)$layoutPathPDF=='') {
                echo (JText::_('COM_VIRTUALDESK_LOAD_PDF_LAYOUT_ERROR'));
                exit();
            }

            require_once(JPATH_ROOT.$layoutPathPDF);

            $pdf = new VirtualDeskPDFHelper();

            if(empty($lat)){
                $latitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
                $longitude = JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_NAO_INDICADO');
            } else{
                $latitude = $lat;
                $longitude = $long;
            }

            $obParam      = new VirtualDeskSiteParamsHelper();

            $pdf->pdfAlertaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $latitude, $longitude, $fiscalid, $telefone);

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO',$nomeMunicipio);
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO',$nomeMunicipio);

            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAILADMIN_CORPO',$referencia);
            $BODY_MESSAGE2       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAILADMIN_2CORPO2');

            if($vbUpdate ==true) {
                $BODY_TITLE      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO_UPDATED',$nomeMunicipio);
                $BODY_TITLE2     = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO_UPDATED',$nomeMunicipio);
                $BODY_MESSAGE    = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CORPO_UPDATED',$referencia);
            }

            $BODY_REFERENCIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_REFERENCIA');
            $BODY_REFERENCIA_VALUE       = JText::sprintf($referencia);
            $BODY_CATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_CATEGORIA');
            $BODY_CATEGORIA_VALUE       = JText::sprintf($catName);
            $BODY_SUBCATEGORIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SUBCATEGORIA');
            $BODY_SUBCATEGORIA_VALUE       = JText::sprintf($subcatName);
            $BODY_DESCRICAO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_DESCRICAO');
            $BODY_DESCRICAO_VALUE       = JText::sprintf($descricao);
            $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FREGUESIA');
            $BODY_FREGUESIA_VALUE       = JText::sprintf($fregName);
            $BODY_SITIO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_SITIO');
            $BODY_SITIO_VALUE       = JText::sprintf($sitio);
            $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_MORADA');
            $BODY_MORADA_VALUE       = JText::sprintf($morada);
            $BODY_PTOSREF_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_PTOSREF');
            $BODY_PTOSREF_VALUE       = JText::sprintf($ptosrefs);
            $BODY_EMAILNAME_VALUE       = JText::sprintf($emailCopyrightGeral);
            $BODY_TELEFNAME_VALUE       = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_DOMINIOMUNICIPIO_VALUE       = JText::sprintf($dominioMunicipio);
            $BODY_LINKMUNICIPIO_VALUE       = JText::sprintf($LinkCopyright);
            $BODY_COPYRIGHT_VALUE       = JText::sprintf($copyrightAPP);
            $BODY_FILES_LIST_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_FILES_LIST_TITLE');


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_2MESSAGE",$BODY_MESSAGE2, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_TITLE",$BODY_REFERENCIA_TITLE, $body);
            $body      = str_replace("%BODY_REFERENCIA_VALUE",$BODY_REFERENCIA_VALUE, $body);
            $body      = str_replace("%BODY_CATEGORIA_TITLE",$BODY_CATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_CATEGORIA_VALUE",$BODY_CATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_TITLE",$BODY_SUBCATEGORIA_TITLE, $body);
            $body      = str_replace("%BODY_SUBCATEGORIA_VALUE",$BODY_SUBCATEGORIA_VALUE, $body);
            $body      = str_replace("%BODY_DESCRICAO_TITLE",$BODY_DESCRICAO_TITLE, $body);
            $body      = str_replace("%BODY_DESCRICAO_VALUE",$BODY_DESCRICAO_VALUE, $body);
            $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
            $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
            $body      = str_replace("%BODY_SITIO_TITLE",$BODY_SITIO_TITLE, $body);
            $body      = str_replace("%BODY_SITIO_VALUE",$BODY_SITIO_VALUE, $body);
            $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
            $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
            $body      = str_replace("%BODY_PTOSREF_TITLE",$BODY_PTOSREF_TITLE, $body);
            $body      = str_replace("%BODY_PTOSREF_VALUE",$BODY_PTOSREF_VALUE, $body);
            $body      = str_replace("%BODY_EMAILNAME_VALUE",$BODY_EMAILNAME_VALUE, $body);
            $body      = str_replace("%BODY_TELEFNAME_VALUE",$BODY_TELEFNAME_VALUE, $body);
            $body      = str_replace("%BODY_DOMINIOMUNICIPIO_VALUE",$BODY_DOMINIOMUNICIPIO_VALUE, $body);
            $body      = str_replace("%BODY_LINKMUNICIPIO_VALUE",$BODY_LINKMUNICIPIO_VALUE, $body);
            $body      = str_replace("%BODY_COPYRIGHT_VALUE",$BODY_COPYRIGHT_VALUE, $body);
            $body      = str_replace("%BODY_FILES_LIST_TITLE",$BODY_FILES_LIST_TITLE, $body);

            // Lista de ficheiros para downlaod
            $objAlertFile = new VirtualDeskSiteAlertsFilesHelper();
            $arFileList   = $objAlertFile->getFileGuestLinkByRefId($referencia);
            $FileList21Html = '';
            foreach ($arFileList as $rowFile){
                $FileList21Html .= "<a href='". JRoute::_($rowFile->guestlink, false)   ."' target='_blank'>".$rowFile->desc."</a><br>\n\r";
            }
            $body      = str_replace("%BODY_FILES_LIST",$FileList21Html, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);

            if(!empty($CategoriaSendMail) && !empty($SubCategoriaSendMail)) {
                if ($CategoriaSendMail == $SubCategoriaSendMail){
                    $newActivationEmail->addRecipient($CategoriaSendMail);
                } else {
                    if(!empty($CategoriaSendMail)) $newActivationEmail->addRecipient($CategoriaSendMail);
                    if(!empty($SubCategoriaSendMail)) $newActivationEmail->addRecipient($SubCategoriaSendMail);
                }
            }

            $newActivationEmail->setSubject($data['sitename']);
            $logosendmailImage = $obParam->getParamsByTag('logosendmailalerta');
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $pdf->SetIdProcesso ($referencia);
            $pdf->SetTagProcesso ('Ocorrências');
            $resSave = $pdf->saveFile();
            if($resSave==true) {
                $resFilepath = $pdf->resFileSaved ['filepath'];
                $resFilename = $pdf->resFileSaved ['filename'];
                $newActivationEmail->addAttachment(JPATH_BASE.'/'.$resFilepath.'/'.$resFilename);
            }

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }


        /* Carrega lista com os TODOS os estados */
        public static function getAlertaEstadoAllOptions ($lang, $displayPermError=true)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
            // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false) JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado_PT as name_pt','estado_EN as name_en') )
                    ->from("#__virtualdesk_alerta_estados")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $rowName
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com os TODOS as Categorias */
        public static function getAlertaCategoriasAllOptions ($idConcelhoAlerta, $displayPermError=true)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
            // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
            $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('alerta');
            if((int)$vbChecModuleAgenda==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                }
                else {
                    $CatName = 'name_EN';
                }

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('ID_categoria as id,' . $CatName.' as name '))
                    ->from("#__virtualdesk_alerta_categoria")
                    ->order(' '.$CatName)
                    ->where($db->quoteName('id_concelho') . '=' . $db->escape($idConcelhoAlerta))
                );

                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {

                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com os TODOS as Categorias */
        public static function getAlertaCategoriasAllNoFilter ($displayPermError=true)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
            // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
            $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('alerta');
            if((int)$vbChecModuleAgenda==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                }
                else {
                    $CatName = 'name_EN';
                }

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('ID_categoria as id,' . $CatName.' as name '))
                    ->from("#__virtualdesk_alerta_categoria")
                    ->order(' '.$CatName)
                );

                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {

                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com os tipos de mensagens de histórico */
        public static function getAlertaHistTipoSelectByManager ($lang)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id','name_pt','name_en') )
                    ->from("#__virtualdesk_alerta_historico_tipo")
                    ->where("selectbymanager=1")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowName = $row->name_pt;
                    }
                    else {
                        $rowName = $row->name_en;
                    }
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $rowName
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os estados POSSÌVEIS tendo em conta o ID do ESTADO atual */
        public static function getAlertaEstadoList2Change ($lang, $id_estado_atual)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if((int) $id_estado_atual<=0) $id_estado_atual = -1;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id','a.id_estado as id_estado_act','a.id_estado_next as id_estado_next','a.id_estado_prev as id_estado_prev'
                    ,'estNext.estado_PT as estNextname_pt' ,'estNext.estado_EN as estNextname_en'
                    ,'estPrev.estado_PT as estPrevname_pt','estPrev.estado_EN as estPrevname_en'
                    ,'estAct.estado_PT as estActname_pt','estAct.estado_EN as estActname_en') )
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS estNext ON estNext.id_estado = a.id_estado_next')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS estPrev ON estPrev.id_estado = a.id_estado_prev')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS estAct  ON estAct.id_estado = a.id_estado')
                    ->from("#__virtualdesk_alerta_estado_sequencia as a")
                    ->where(" a.id_estado = $id_estado_atual ")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {

                    // Acrescenta o ATUAL para surgir na lista
                    if((int) $row->id_estado_act>0 ) {
                        if (empty($lang) or ($lang = 'pt_PT')) {
                            $rowNameAct = $row->estActname_pt;
                        } else {
                            $rowNameAct = $row->estActname_en;
                        }
                        $response[$row->id_estado_act] = array(
                            'id'   => $row->id_estado_act,
                            'name' => $rowNameAct
                        );
                    }


                    // NEXT
                    if((int) $row->id_estado_next>0 ) {
                        if (empty($lang) or ($lang = 'pt_PT')) {
                            $rowNameNxt = $row->estNextname_pt;
                        } else {
                            $rowNameNxt = $row->estNextname_en;
                        }
                        $response[$row->id_estado_next] = array(
                            'id'   => $row->id_estado_next,
                            'name' => $rowNameNxt
                        );
                    }
                    // PREV
                    if((int) $row->id_estado_prev>0 ) {
                        if (empty($lang) or ($lang = 'pt_PT')) {
                            $rowNamePrev = $row->estPrevname_pt;
                        } else {
                            $rowNamePrev = $row->estPrevname_en;
                        }
                        $response[$row->id_estado_prev] = array(
                            'id' => $row->id_estado_prev,
                            'name' => $rowNamePrev
                        );
                    }
                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega texto com a designação do ESTADO a partir do ID do estado indicado */
        public static function getAlertaEstadoNameById ($lang, $id_estado)
        {
            if((int) $id_estado<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado','estado_PT','estado_EN') )
                    ->from("#__virtualdesk_alerta_estados")
                    ->where(" id_estado = ". $db->escape($id_estado))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                if( empty($lang) or ($lang='pt_PT') ) {
                        $EstadoName = $dataReturn->estado_PT;
                    }
                else {
                    $EstadoName = $dataReturn->estado_EN;
                    }

                return($EstadoName);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega texto com a designação dp ESTADO atual do processo do alerta em questão */
        public static function getAlertaEstadoAtualNameByIdAlerta ($lang, $id_alerta)
        {
            if((int) $id_alerta<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('b.id_estado as id_estado','b.estado_PT as estado_PT  ','b.estado_EN as estado_EN') )
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->where(" a.Id_alerta = ". $db->escape($id_alerta))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                if( empty($lang) or ($lang='pt_PT') ) {
                    $EstadoName = $dataReturn->estado_PT;
                }
                else {
                    $EstadoName = $dataReturn->estado_EN;
                }

                return($EstadoName);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados (objecto) do ESTADO atual do processo do alerta em questão */
        public static function getAlertaEstadoAtualObjectByIdAlerta ($lang, $id_alerta)
        {
            if((int) $id_alerta<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('b.id_estado as id_estado','b.estado_PT as estado_PT  ','b.estado_EN as estado_EN') )
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->where(" a.Id_alerta = ". $db->escape($id_alerta))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                $obj= new stdClass();
                $obj->id_estado =  $dataReturn->id_estado;
                if( empty($lang) or ($lang='pt_PT') ) {
                    $obj->name = $dataReturn->estado_PT;
                }
                else {
                    $obj->name  = $dataReturn->estado_EN;
                }

                $obj->cssClass = self::getAlertaEstadoCSS($dataReturn->id_estado);

                return($obj);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os USERS que podem ser utilizados no ALTERAR RESPONSÀVEL.  */
        public static function getAlertaProcManagerList2Change ()
        {

            /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $conf = JFactory::getConfig();
                $db = JFactory::getDBO();
                $dbprefix = $conf->get('dbprefix');

                $sql = "SELECT distinct s1.iduser, s2.id as id, s2.name as name from (";
                $sql.= "SELECT distinct c.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_usersaction c on c.idpermaction = b.id ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and c.iduser is not null ";
                $sql.= " union ";
                $sql.= " SELECT distinct d.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_groupsaction c on c.idpermaction = b.id ";
                $sql.= " left JOIN ".$dbprefix."virtualdesk_perm_groupsusers d on d.idpermgroup = c.idpermgroup ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and d.iduser is not null ";
                $sql.= " ) as s1 left join ".$dbprefix."virtualdesk_users as s2 on s1.iduser=s2.id ";
                $sql.= " WHERE s2.blocked=0 and s2.activated=1";

                $db->setQuery($sql);
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {

                    $response[$row->id] = array(
                        'id'   => $row->id,
                        'name' => $row->name
                    );
                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados do utilizador que é o responsável atual do processo do alerta em questão */
        public static function getAlertaProcManagerAtualObjectByIdAlerta ($lang, $id_alerta)
        {
            if((int) $id_alerta<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('b.id as id_user','b.name as name','b.email as email') )
                    ->join('LEFT', '#__virtualdesk_users AS b ON b.id = a.id_procmanager')
                    ->from("#__virtualdesk_alerta as a")
                    ->where(" a.Id_alerta = ". $db->escape($id_alerta))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                $obj= new stdClass();
                $obj->name  =  $dataReturn->name;
                $obj->email =  $dataReturn->email;

                return($obj);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Devolve CSS a ser aplicado de acordo com o ESTADO atual */
        public static function getAlertaEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            //if(data==' ') data = JText::_("COM_VIRTUALDESK_ALERTA_ESTADONAOINICIADO");
            switch ($idestado)
            {   case '1':
                    //Em espera
                    $defCss = 'label-espera';
                break;
                case '2':
                    // Em análise
                    $defCss = 'label-analise';
                    break;
                case '3':
                    // Concluído
                    $defCss = 'label-concluido';
                    break;
            }
            return ($defCss);
        }

        /* Carrega lista com os USERS que podem ser utilizados no REENCAMINHAR (acesso ao MANAGER).  */
        public static function getAlertaReencaminharUserList4Manager ($id_alerta)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $conf = JFactory::getConfig();
                $db = JFactory::getDBO();
                $dbprefix = $conf->get('dbprefix');

                $sql = "SELECT distinct s1.iduser, s2.id as id, s2.name as name from (";
                $sql.= "SELECT distinct c.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= "    left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_usersaction c on c.idpermaction = b.id ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and c.iduser is not null ";
                $sql.= " union ";
                $sql.= " SELECT distinct d.iduser as iduser FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_groupsaction c on c.idpermaction = b.id ";
                $sql.= " left JOIN ".$dbprefix."virtualdesk_perm_groupsusers d on d.idpermgroup = c.idpermgroup ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and d.iduser is not null ";
                $sql.= " ) as s1 left join ".$dbprefix."virtualdesk_users as s2 on s1.iduser=s2.id ";
                $sql.= " WHERE s2.blocked=0 and s2.activated=1";

                $db->setQuery($sql);

                $dataReturn = $db->loadAssocList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os GRUPOS que podem ser utilizados no REENCAMINHAR (acesso ao MANAGER).  */
        public static function getAlertaReencaminharGroupList4Manager ($id_alerta)
        {
           /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $conf = JFactory::getConfig();
                $db = JFactory::getDBO();
                $dbprefix = $conf->get('dbprefix');

                $sql = " SELECT distinct d.id as id, d.nome as name FROM ".$dbprefix."virtualdesk_perm_modulo as i ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_tipomodulo as a on a.idpermmodulo=i.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_action as b on b.idpermtipomodulo =  a.id ";
                $sql.= " left join ".$dbprefix."virtualdesk_perm_groupsaction c on c.idpermaction = b.id ";
                $sql.= " left JOIN ".$dbprefix."virtualdesk_perm_groups d on d.id = c.idpermgroup ";
                $sql.= " WHERE i.tagchave='".self::tagchaveModulo."' and b.tagchave in ('edit4managers','addnew4managers') and d.id is not null ";

                $db->setQuery($sql);
                $dataReturn = $db->loadAssocList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com dados de todos os alertas (acesso ao USER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getAlertaList4User ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4users');
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id=');


                    $table  = " ( SELECT a.Id_alerta as id,a.Id_alerta as alerta_id, CONCAT('".$dummyHRef."', CAST(a.Id_alerta as CHAR(10))) as dummy, a.codOco as codigo, ";
                    $table .=" IFNULL(b.".$CatName.",' ') as categoria, IFNULL(c.".$CatName.",' ') as subcategoria, IFNULL(b.".$CatName.",' ') as category_name, ";
                    $table .= " d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado , a.categoria as idcategoria ";
                    $table .= ", DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d %H:%i') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_estados AS e ON e.id_estado = a.estado ";
                    $table .= " WHERE (a.nif=$UserSessionNIF) ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'modified',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 ),
                        array( 'db' => 'id',            'dt' => 6 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'createdFull',   'dt' => 8 ),
                        array( 'db' => 'idcategoria',   'dt' => 9 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.estado_PT as estado, a.data_criacao'))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->where( $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF))
                    ->order(' modified DESC ' . $setLimitSQL)
                );
                $dataReturn = $db->loadObjectList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com dados do histórico de uma alerta para acesso ao USER */
        public static function getAlertaHistoricoList4User ($IdAlerta, $vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdAlerta) || (int)$IdAlerta<=0 )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName = 'name_PT';
                }
                else {
                    $TipoName = 'name_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    ##$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id=');

                    $table  = "(  SELECT a.id as id, a.idalerta as alerta_id, IFNULL(b.".$TipoName.",' ') as tipo, a.mensagem as mensagem, a.anulado as anulado, a.setbymanager as setbymanager ";
                    $table .= " , DATE_FORMAT(a.created, '%Y-%m-%d') as created, DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(a.modified, '%Y-%m-%d %H:%i') as modified ";
                    $table .= " , '1' AS dummyuser, '1' as dummymanager, a.iduser, a.iduserjos, d.name as username, a.idtipo as idtipo ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta_historico as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.id = a.iduser ";
                    $table .= " WHERE (a.idalerta=$IdAlerta) and a.anulado<>1 and visible4user=1 and c.nif=".$UserSessionNIF;
                    $table .= " ) temp ";

                    $primaryKey = 'id';

                    # array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    $columns = array(
                        array( 'db' => 'modified',   'dt' => 0 ),
                        array( 'db' => 'dummyuser',     'dt' => 1 ),
                        array( 'db' => 'tipo',          'dt' => 2 ),
                        array( 'db' => 'mensagem',      'dt' => 3 ),
                        array( 'db' => 'dummymanager',  'dt' => 4 ),
                        array( 'db' => 'created',       'dt' => 5 ),
                        array( 'db' => 'modified',      'dt' => 6 ),
                        array( 'db' => 'setbymanager',  'dt' => 7 ),
                        array( 'db' => 'username',      'dt' => 8 ),
                        array( 'db' => 'createdFull',   'dt' => 9 ),
                        array( 'db' => 'idtipo',        'dt' => 10 )

                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();

                /*
                $db->setQuery($db->getQuery(true)
                  ->select(array(' a.id as id', 'a.idalerta as alerta_id', "IFNULL(b.".$TipoName.",'') as tipo", 'a.mensagem as mensagem', 'a.anulado as anulado', 'a.setbymanager as setbymanager'
                      , "DATE_FORMAT(a.created, '%Y-%m-%d') as created", "DATE_FORMAT(a.modified, '%Y-%m-%d') as modified ", "DATE_FORMAT(a.created, '%Y-%m-%d %H:%i') as createdFull"
                      ,'a.idtipo as idtipo' , " '1' AS dummyuser ", " '1' AS dummymanager ",  'a.iduser', 'a.iduserjos',  'd.name as username'
                  ))
                  ->join('LEFT', '#__virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo')
                  ->join('LEFT', '#__virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta')
                  ->join('LEFT', '#__virtualdesk_users AS d ON d.id = a.iduser')
                  ->from("#__virtualdesk_alerta_historico as a")
                  ->where( $db->quoteName('a.idalerta').'='.$db->escape($IdAlerta) . ' and a.anulado<>1 and visible4user=1 and ' . $db->quoteName('c.nif').'='.$db->escape($UserSessionNIF) )
                  ->order(' a.created DESC ' . $setLimitSQL)
                );*/

                $queryFull  = " SELECT a.id as id,a.idalerta as alerta_id,IFNULL(b.name_PT,'') as tipo,a.mensagem as mensagem,a.anulado as anulado,a.setbymanager as setbymanager,DATE_FORMAT(a.created, '%Y-%m-%d') as created,DATE_FORMAT(a.modified, '%Y-%m-%d') as modified ,DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull,a.idtipo as idtipo, '1' AS dummyuser , '1' AS dummymanager ,a.iduser,a.iduserjos,d.name as username";
                $queryFull .= " FROM gtonq_virtualdesk_alerta_historico as a ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_users AS d ON d.id = a.iduser";
                $queryFull .= " WHERE `a`.`idalerta`=".$db->escape($IdAlerta)." and a.anulado<>1 and visible4user=1 and `c`.`nif`=".$db->escape($UserSessionNIF);
                $queryFull .= " UNION ";
                $queryFull .= " SELECT '' as id,a2.Id_alerta as alerta_id, '".JText::_('COM_VIRTUALDESK_INITIATED')."' as tipo, '".JText::_('COM_VIRTUALDESK_INITIATED')."' as mensagem, '' as anulado, '' as setbymanager,DATE_FORMAT(a2.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a2.data_alteracao, '%Y-%m-%d')  as modified ,DATE_FORMAT(a2.data_criacao, '%Y-%m-%d %H:%i:%s') as createdFull, 4 as idtipo, '1' AS dummyuser , '1' AS dummymanager , '' as iduser, '' as iduserjos, '' as username ";
                $queryFull .= " FROM gtonq_virtualdesk_alerta as a2";
                $queryFull .= " WHERE `a2`.`Id_alerta`=".$db->escape($IdAlerta)." ";
                $queryFull .= " ORDER BY createdFull DESC ";

                $db->setQuery($queryFull);
                $dataReturn = $db->loadObjectList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com dados do histórico de uma alerta para acesso ao MANAGER */
        public static function getAlertaHistoricoList4Manager ($IdAlerta, $vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('alerta', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('alerta'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('alerta','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdAlerta) || (int)$IdAlerta<=0 )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $TipoName = 'name_PT';
                }
                else {
                    $TipoName = 'name_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    ##$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id=');

                    $table  = "(  SELECT a.id as id, a.idalerta as alerta_id, IFNULL(b.".$TipoName.",' ') as tipo, a.mensagem as mensagem, a.anulado as anulado, a.setbymanager as setbymanager ";
                    $table .= " , DATE_FORMAT(a.created, '%Y-%m-%d') as created, DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull, DATE_FORMAT(a.modified, '%Y-%m-%d %H:%i') as modified ";
                    $table .= " , '1' AS dummyuser, '1' as dummymanager, a.iduser, a.iduserjos, d.name as username, a.idtipo as idtipo ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta_historico as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON d.id = a.iduser ";
                    $table .= " WHERE (a.idalerta=$IdAlerta) and a.anulado<>1 ";
                    $table .= " ) temp ";

                    $primaryKey = 'id';

                    # array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    $columns = array(
                        array( 'db' => 'modified',   'dt' => 0 ),
                        array( 'db' => 'dummyuser',     'dt' => 1 ),
                        array( 'db' => 'tipo',          'dt' => 2 ),
                        array( 'db' => 'mensagem',      'dt' => 3 ),
                        array( 'db' => 'dummymanager',  'dt' => 4 ),
                        array( 'db' => 'created',       'dt' => 5 ),
                        array( 'db' => 'modified',      'dt' => 6 ),
                        array( 'db' => 'setbymanager',  'dt' => 7 ),
                        array( 'db' => 'username',      'dt' => 8 ),
                        array( 'db' => 'createdFull',   'dt' => 9 ),
                        array( 'db' => 'idtipo',        'dt' => 10 ),
                        array( 'db' => 'id',            'dt' => 11 )


                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();

                /*
                $db->setQuery($db->getQuery(true)
                  ->select(array(' a.id as id', 'a.idalerta as alerta_id', "IFNULL(b.".$TipoName.",'') as tipo", 'a.mensagem as mensagem', 'a.anulado as anulado', 'a.setbymanager as setbymanager'
                      , "DATE_FORMAT(a.created, '%Y-%m-%d') as created", "DATE_FORMAT(a.modified, '%Y-%m-%d') as modified ", "DATE_FORMAT(a.created, '%Y-%m-%d %H:%i') as createdFull"
                      ,'a.idtipo as idtipo' , " '1' AS dummyuser ", " '1' AS dummymanager ",  'a.iduser', 'a.iduserjos',  'd.name as username'
                  ))
                  ->join('LEFT', '#__virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo')
                  ->join('LEFT', '#__virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta')
                  ->join('LEFT', '#__virtualdesk_users AS d ON d.id = a.iduser')
                  ->from("#__virtualdesk_alerta_historico as a")
                  ->where( $db->quoteName('a.idalerta').'='.$db->escape($IdAlerta) . ' and a.anulado<>1 and visible4user=1 and ' . $db->quoteName('c.nif').'='.$db->escape($UserSessionNIF) )
                  ->order(' a.created DESC ' . $setLimitSQL)
                );*/

                $queryFull  = " SELECT a.id as id,a.idalerta as alerta_id,IFNULL(b.name_PT,'') as tipo,a.mensagem as mensagem,a.anulado as anulado,a.setbymanager as setbymanager,DATE_FORMAT(a.created, '%Y-%m-%d') as created,DATE_FORMAT(a.modified, '%Y-%m-%d') as modified ,DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull,a.idtipo as idtipo, '1' AS dummyuser , '1' AS dummymanager ,a.iduser,a.iduserjos,d.name as username";
                $queryFull .= " FROM gtonq_virtualdesk_alerta_historico as a ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_alerta_historico_tipo AS b ON b.id = a.idtipo ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_alerta AS c ON c.Id_alerta = a.idalerta ";
                $queryFull .= " LEFT JOIN gtonq_virtualdesk_users AS d ON d.id = a.iduser";
                $queryFull .= " WHERE `a`.`idalerta`=".$db->escape($IdAlerta)." and a.anulado<>1 ";
                $queryFull .= " UNION ";
                $queryFull .= " SELECT '' as id,a2.Id_alerta as alerta_id, '".JText::_('COM_VIRTUALDESK_INITIATED')."' as tipo, '".JText::_('COM_VIRTUALDESK_INITIATED')."' as mensagem, '' as anulado, '' as setbymanager,DATE_FORMAT(a2.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a2.data_alteracao, '%Y-%m-%d')  as modified ,DATE_FORMAT(a2.data_criacao, '%Y-%m-%d %H:%i:%s') as createdFull, 4 as idtipo, '1' AS dummyuser , '1' AS dummymanager , '' as iduser, '' as iduserjos, '' as username ";
                $queryFull .= " FROM gtonq_virtualdesk_alerta as a2";
                $queryFull .= " WHERE `a2`.`Id_alerta`=".$db->escape($IdAlerta)." ";
                $queryFull .= " ORDER BY createdFull DESC ";

                $db->setQuery($queryFull);
                $dataReturn = $db->loadObjectList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com dados de todos os alertas (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getAlertaList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&alerta_id=');

                    $table  = " ( SELECT a.Id_alerta as id, a.Id_alerta as alerta_id, CONCAT('".$dummyHRef."', CAST(a.Id_alerta as CHAR(10))) as dummy, a.codOco as codigo, ";
                    $table .=" IFNULL(b.".$CatName.",' ') as categoria, IFNULL(c.".$CatName.",' ') as subcategoria, IFNULL(b.".$CatName.",' ') as category_name, ";
                    $table .= " d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado, a.categoria as idcategoria";
                    $table .= ", DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d %H:%i') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_estados AS e ON e.id_estado = a.estado ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'modified',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 ),
                        array( 'db' => 'id',            'dt' => 6 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'createdFull',   'dt' => 8 ),
                        array( 'db' => 'idcategoria',   'dt' => 9 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.estado_PT as estado, a.data_criacao'))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->order(' modified DESC ' . $setLimitSQL)
                );
                $dataReturn = $db->loadObjectList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os alertas com TAREFAS para o utilizador atual ou para grupos do utilizador atual . $vbForDataTables: se for true, vai carrega o JSON para os datatables  */
        public static function getAlertaEncaminhadasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    // carrega os alerta aIDs associado ao encaminhamentos atuais do USEr e dos GRUPOS associados a este
                    $arAlertaIdsFromUser     = self::getReencaminharIDs_ByUserAndGroups_4Manager();
                    $arAlertaIdsToFilter     = array();
                    foreach ($arAlertaIdsFromUser as $valFilter) {
                        $arAlertaIdsToFilter[] = $valFilter->id_alerta;
                    }
                    if(empty($arAlertaIdsToFilter)) return(false);
                    $stringAlertaIdsToFilter = implode(',',$arAlertaIdsToFilter);
                    if(empty($stringAlertaIdsToFilter)) return(false);

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&alerta_id=');

                    $table  = " ( SELECT a.Id_alerta as id, a.Id_alerta as alerta_id, CONCAT('".$dummyHRef."', CAST(a.Id_alerta as CHAR(10))) as dummy, a.codOco as codigo, ";
                    $table .=" IFNULL(b.".$CatName.",' ') as categoria, IFNULL(c.".$CatName.",' ') as subcategoria, IFNULL(b.".$CatName.",' ') as category_name, ";
                    $table .= " d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado ";
                    $table .= ", DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d %H:%i') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_estados AS e ON e.id_estado = a.estado ";
                    $table .= " WHERE a.Id_alerta in ( ".$stringAlertaIdsToFilter.")";

                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'modified',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 ),
                        array( 'db' => 'id',            'dt' => 6 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'createdFull',   'dt' => 8 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.estado_PT as estado, a.data_criacao'))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->order(' modified DESC ' . $setLimitSQL)
                );
                $dataReturn = $db->loadObjectList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com os alertas ENCAMINHADAS para o utilizador atual ou para grupos do utilizador atual . $vbForDataTables: se for true, vai carrega o JSON para os datatables  */
        public static function getAlertaTarefasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    // carrega os alerta aIDs associado ao encaminhamentos atuais do USEr e dos GRUPOS associados a este
                    $arAlertaIdsFromUser     = self::getTarefasIDs_ByUserAndGroups_4Manager();
                    $arAlertaIdsToFilter     = array();
                    if(empty($arAlertaIdsFromUser)) $arAlertaIdsFromUser = array();
                    foreach ($arAlertaIdsFromUser as $valFilter) {
                        $arAlertaIdsToFilter[] = $valFilter->id_alerta;
                    }
                    if(empty($arAlertaIdsToFilter)) return(false);
                    $stringAlertaIdsToFilter = implode(',',$arAlertaIdsToFilter);
                    if(empty($stringAlertaIdsToFilter)) return(false);

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&alerta_id=');

                    $table  = " ( SELECT a.Id_alerta as id, a.Id_alerta as alerta_id, CONCAT('".$dummyHRef."', CAST(a.Id_alerta as CHAR(10))) as dummy, a.codOco as codigo, ";
                    $table .=" IFNULL(b.".$CatName.",' ') as categoria, IFNULL(c.".$CatName.",' ') as subcategoria, IFNULL(b.".$CatName.",' ') as category_name, ";
                    $table .= " d.freguesia as freguesia, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado ";
                    $table .= ", DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d %H:%i') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_estados AS e ON e.id_estado = a.estado ";
                    $table .= " WHERE a.Id_alerta in ( ".$stringAlertaIdsToFilter." )";

                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'modified',       'dt' => 0 ),
                        array( 'db' => 'codigo',        'dt' => 1 ),
                        array( 'db' => 'categoria',     'dt' => 2 ),
                        array( 'db' => 'estado',        'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'idestado',      'dt' => 5 ),
                        array( 'db' => 'id',            'dt' => 6 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 7 ),
                        array( 'db' => 'createdFull',   'dt' => 8 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.estado_PT as estado, a.data_criacao'))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->order(' modified DESC ' . $setLimitSQL)
                );
                $dataReturn = $db->loadObjectList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com as categorias dos alertas. $vbForDataTables: se for true, vai carrega o JSON para os datatables  */
        public static function getAlertaListConfCategorias4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasAccessCfg = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasAccessCfg ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();
                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=viewconfcategoria4manager&categoria_id=');

                    $table  = " ( SELECT a.ID_categoria as id, a.ID_categoria as categoria_id, CONCAT('".$dummyHRef."', CAST(a.ID_categoria as CHAR(10))) as dummy ";
                    $table .= " , IFNULL(b.concelho,' ') as concelho , IFNULL(a.conf_email,' ') as conf_email , a.name_PT as name_PT, a.name_EN as name_EN, a.Ref as Ref";
                    $table .= " , DATE_FORMAT(a.creation_date, '%Y-%m-%d') as created, DATE_FORMAT(a.modify_date, '%Y-%m-%d %H:%i') as modified, DATE_FORMAT(a.creation_date, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta_categoria as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_concelhos AS b ON b.id = a.id_concelho ";
                    $table .= " ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'name_PT',       'dt' => 0 ),
                        array( 'db' => 'Ref',           'dt' => 1 ),
                        array( 'db' => 'concelho',      'dt' => 2 ),
                        array( 'db' => 'conf_email',    'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'id',            'dt' => 5 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 6 ),
                        array( 'db' => 'createdFull',   'dt' => 7 ),

                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                $dataReturn = array();

               /* $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.estado_PT as estado, a.data_criacao'))

                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta_categorais as a")
                    ->order(' modified DESC ' . $setLimitSQL)
                );
                $dataReturn = $db->loadObjectList();
               */

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com as Sub Categorias dos alertas. $vbForDataTables: se for true, vai carrega o JSON para os datatables  */
        public static function getAlertaListConfSubCategorias4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasAccessCfg = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasAccessCfg ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();
                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=viewconfsubcategoria4manager&subcategoria_id=');

                    $table  = " ( SELECT a.Id_subcategoria as id, a.Id_subcategoria as subcategoria_id, CONCAT('".$dummyHRef."', CAST(a.Id_subcategoria as CHAR(10))) as dummy ";
                    $table .= " , IFNULL(b.concelho,' ') as concelho , IFNULL(a.conf_email,' ') as conf_email , a.name_PT as name_PT, a.name_EN as name_EN";
                    $table .= " , IFNULL(c.name_PT,' ') as categoria, a.Id_categoria as id_categoria,  IFNULL(b.id,'') as id_concelho";
                    $table .= " , DATE_FORMAT(a.creation_date, '%Y-%m-%d') as created, DATE_FORMAT(a.modify_date, '%Y-%m-%d %H:%i') as modified, DATE_FORMAT(a.creation_date, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " FROM ".$dbprefix."virtualdesk_alerta_subcategoria as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_concelhos AS b ON b.id = a.id_concelho ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_alerta_categoria AS c ON c.ID_categoria = a.Id_categoria ";
                    $table .= " ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'name_PT',       'dt' => 0 ),
                        array( 'db' => 'categoria',     'dt' => 1 ),
                        array( 'db' => 'concelho',      'dt' => 2 ),
                        array( 'db' => 'conf_email',    'dt' => 3 ),
                        array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'id',            'dt' => 5 ,'formatter'=>'VALUE_ENCRYPT'),
                        array( 'db' => 'modified',      'dt' => 6 ),
                        array( 'db' => 'createdFull',   'dt' => 7 ),
                        array( 'db' => 'id_categoria',  'dt' => 8 ),
                        array( 'db' => 'id_concelho',   'dt' => 9 ),

                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                $dataReturn = array();

                /* $db = JFactory::getDBO();
                 $db->setQuery($db->getQuery(true)
                     ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.estado_PT as estado, a.data_criacao'))

                     ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                     ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                     ->from("#__virtualdesk_alerta_categorais as a")
                     ->order(' modified DESC ' . $setLimitSQL)
                 );
                 $dataReturn = $db->loadObjectList();
                */

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados visualização do alerta para o USER */
        public static function getAlertaView4UserDetail ($IdAlerta)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdAlerta) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            // Current Session User...
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria', 'd.freguesia as freguesia', 'e.estado_PT as estado', 'e.id_estado as idestado', 'a.latitude', 'a.longitude', 'a.data_criacao', 'a.descricao as descricao'
                      , 'a.local as local','a.sitio as sitio','a.pontos_referencia as pontos_referencia','a.descritivoBO as descritivoBO','a.observacoes as observacoes'
                      , 'a.email as email','a.nif as nif','a.telefone as telefone', 'a.utilizador as utilizador'
                      , 'a.categoria as id_categoria', 'a.subcategoria as id_subcategoria', 'a.freguesia as id_freguesia'
                    ))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->where( $db->quoteName('Id_alerta') . '=' . $db->escape($IdAlerta) . ' and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();

                if(!empty($dataReturn))  {
                    $objVDParams = new VirtualDeskSiteParamsHelper();
                    $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Alerta');
                    if((int)$setLogEnabled===1) {
                        $vdlog = new VirtualDeskLogHelper();
                        $eventdata = array();
                        $eventdata['title']         = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_VIEW');
                        // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                        $eventdata['descricao']     = 'nif: '.$dataReturn->nif.' | '.'email: '.$dataReturn->email.' | '.'tel: '.$dataReturn->telefone.' | '.'utilz: '.$dataReturn->utilizador ;
                        $eventdata['sessionuserid'] = JFactory::getUser()->id;
                        $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                        $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPView();
                        $eventdata['request']       = serialize($_REQUEST);
                        $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                        $eventdata['ref_id']        = $IdAlerta;
                        $eventdata['ref']           = $dataReturn->codigo;
                        $vdlog->insertRgpdLog($eventdata);
                    }
                }

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados visualização do alerta para o MANAGER */
        public static function getAlertaView4ManagerDetail ($IdAlerta)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('alerta', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('alerta'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('alerta','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdAlerta) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_alerta as alerta_id', 'a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria', 'd.freguesia as freguesia', 'e.estado_PT as estado', 'e.id_estado as idestado', 'a.latitude', 'a.longitude', 'a.data_criacao', 'a.descricao as descricao'
                    , 'a.local as local','a.sitio as sitio','a.pontos_referencia as pontos_referencia','a.descritivoBO as descritivoBO','a.observacoes as observacoes'
                    , 'a.email as email','a.nif as nif','a.telefone as telefone', 'a.utilizador as utilizador',  'a.id_procmanager as id_procmanager',  'f.name as procmanager_name'
                    , 'a.categoria as id_categoria', 'a.subcategoria as id_subcategoria', 'a.freguesia as id_freguesia'
                    ))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_users AS f ON f.id = a.id_procmanager')
                    ->from("#__virtualdesk_alerta as a")
                    ->where( $db->quoteName('Id_alerta') . '=' . $db->escape($IdAlerta) )
                );
                $dataReturn = $db->loadObject();

                if(!empty($dataReturn))  {
                    $objVDParams = new VirtualDeskSiteParamsHelper();
                    $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Alerta');
                    if((int)$setLogEnabled===1) {
                        $vdlog = new VirtualDeskLogHelper();
                        $eventdata = array();
                        $eventdata['title']         = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_VIEW');
                        // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                        $eventdata['descricao']     = 'nif: '.$dataReturn->nif.' | '.'email: '.$dataReturn->email.' | '.'tel: '.$dataReturn->telefone.' | '.'utilz: '.$dataReturn->utilizador ;
                        $eventdata['sessionuserid'] = JFactory::getUser()->id;
                        $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                        $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPView();
                        $eventdata['request']       = serialize($_REQUEST);
                        $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                        $eventdata['ref_id']        = $IdAlerta;
                        $eventdata['ref']           = $dataReturn->codigo;
                        $vdlog->insertRgpdLog($eventdata);
                    }
                }

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados visualização do alerta para o MANAGER */
        public static function getAlertaViewConfCategoria4ManagerDetail ($IdCategoria)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            $vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasAccessCfg = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasAccessCfg ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdCategoria) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.ID_categoria as categoria_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN'
                       , 'a.conf_email as conf_email', 'a.Ref as Ref', 'a.id_concelho as id_concelho', 'b.concelho as concelho'
                       , 'a.creation_date as created', 'a.modify_date as modified'
                    ))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.id_concelho')
                    ->from("#__virtualdesk_alerta_categoria as a")
                    ->where( $db->quoteName('ID_categoria') . '=' . $db->escape($IdCategoria) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados visualização do alerta para o MANAGER */
        public static function getAlertaViewConfSubCategoria4ManagerDetail ($IdSubCategoria)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            $vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasAccessCfg = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasAccessCfg ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdSubCategoria) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.Id_subcategoria as subcategoria_id', 'a.name_PT as name_PT', 'a.name_EN as name_EN'
                    , 'a.conf_email as conf_email', 'a.Id_categoria as id_categoria', 'a.id_concelho as id_concelho'
                    , 'b.concelho as concelho', 'c.name_PT as categoria'
                    , 'a.creation_date as created', 'a.modify_date as modified'
                    ))
                    ->join('LEFT', '#__virtualdesk_digitalGov_concelhos AS b ON b.id = a.id_concelho')
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS c ON c.ID_categoria = a.Id_categoria')
                    ->from("#__virtualdesk_alerta_subcategoria as a")
                    ->where( $db->quoteName('Id_subcategoria') . '=' . $db->escape($IdSubCategoria) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Obtém os dados dos users e/ou grupos com o encaminhamento atual. Devolve array com dados que depois são colocados no ecrã em php ou em javascript*/
        public static function getAlertaView4ManagerDetailGetReencaminhar ($IdAlerta)
        {
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('alerta', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('alerta'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('alerta','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($IdAlerta) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('b.name as nome','b.email as email'))
                    ->join('LEFT', '#__virtualdesk_users AS b ON b.id = a.id_user')
                    ->from("#__virtualdesk_alerta_encaminhar_user_hist as a")
                    ->where( ' a.atual=1 AND ' . $db->quoteName('a.id_alerta') . '=' . $db->escape($IdAlerta) )
                );

                $dataReturn = '';
                $dataUsers = $db->loadObjectList();

                $db->setQuery($db->getQuery(true)
                    ->select(array('b.nome as nome','b.email as email'))
                    ->join('LEFT', '#__virtualdesk_perm_groups AS b ON b.id = a.id_group')
                    ->from("#__virtualdesk_alerta_encaminhar_group_hist as a")
                    ->where( ' a.atual=1 AND ' . $db->quoteName('a.id_alerta') . '=' . $db->escape($IdAlerta) )
                );

                $dataGroups = $db->loadObjectList();

                $dataReturn = array_merge($dataUsers, $dataGroups);

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega dados (objecto) da configuração (envio email, etc) do ESTADO tendo em conta o Id do estado enviado */
        public static function getAlertaEstadoConfigObjectByIdAlerta ($id_estado)
        {
            if((int) $id_estado<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array(' * ') )
                    ->from("#__virtualdesk_alerta_estado_config")
                    ->where(" id_estado = ". $db->escape($id_estado))
                );
                $dataReturn = $db->loadObject();
                if(empty($dataReturn)) return false;
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega ID do tipo de histórico a partir da tag enviada */
        public static function getAlertaHistoricoTipoIdByTag ($tagchave)
        {
            if(empty($tagchave)) return false;
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array(' id ') )
                ->from("#__virtualdesk_alerta_historico_tipo")
                ->where(" tagchave='". $db->escape($tagchave)."'")
            );
            $dataReturn = $db->loadObject();
            if(empty($dataReturn)) return false;
            return((int)$dataReturn->id);
        }

        /* Carrega lista com as ocorrências novas/em espera*/
        public static function getOcorrenciasNovasList4Manager ()
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                $obParam              = new VirtualDeskSiteParamsHelper();
                $avisosMaxRecords     = (int)$obParam->getParamsByTag('avisosBarraSuperiorGeralMaxRecords');
                $AlertaMenuId4Manager = $obParam->getParamsByTag('Alerta_Menu_Id_By_Default_4Managers');
                $dummyHRef            =  JRoute::_('index.php?option=com_virtualdesk&Itemid='.$AlertaMenuId4Manager.'&view=alerta&layout=view4manager&alerta_id=');
                $dataReturn           = array();

                $db = JFactory::getDBO();
                // Retirei Os IDs porque não estão a ser utilizados e temos depois de alguma forma encriptar estes IDS
                // 'a.Id_alerta as alerta_id',
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.'.$EstadoName.' as estado, a.data_criacao', ' "" as dummy ',' Id_alerta as dmId' ))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->where(' a.estado=1 ')
                    ->order(' a.data_alteracao DESC ')
                );

                $dataReturn = $db->loadObjectList();
                $obVDCrypt  = new VirtualDeskSiteCryptHelper();
                $params     = JComponentHelper::getParams('com_virtualdesk');
                $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
                $dataReturnCut       = array();
                $dataReturnCut['dt'] = array();
                $dataReturnCut['nr'] = 0;
                $counti = 0;
                foreach ($dataReturn as $keyR => $valR) {
                    $dataReturnCut['dt'][$keyR]        = $valR;
                    $dataReturnCut['dt'][$keyR]->dummy = $dummyHRef.$valR->dmId;
                    if($setencrypt_forminputhidden==1) $dataReturnCut['dt'][$keyR]->dummy = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($dummyHRef.$valR->dmId);
                    unset($dataReturnCut['dt'][$keyR]->dmId);
                    $counti++;
                    if($counti>=(int)$avisosMaxRecords) break;
                }
                $dataReturnCut['nr'] = sizeof($dataReturn);
                return($dataReturnCut);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com as ocorrências novas/em espera encaminhadas para o utilizador ou para os grupos do utilizador */
        public static function getOcorrenciasNovasEncaminhadasList4Manager ()
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';
                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';
                }

                // carrega os alerta aIDs associado ao encaminhamentos atuais do USEr e dos GRUPOS associados a este
                $arAlertaIdsFromUser     = self::getReencaminharIDs_ByUserAndGroups_4Manager();
                $arAlertaIdsToFilter     = array();
                foreach ($arAlertaIdsFromUser as $valFilter) {
                    $arAlertaIdsToFilter[] = $valFilter->id_alerta;
                }
                if(empty($arAlertaIdsToFilter)) return(false);
                $stringAlertaIdsToFilter = implode(',',$arAlertaIdsToFilter);
                if(empty($stringAlertaIdsToFilter)) return(false);

                $obParam          = new VirtualDeskSiteParamsHelper();
                $avisosMaxRecords = (int)$obParam->getParamsByTag('avisosBarraSuperiorGeralMaxRecords');

                $db = JFactory::getDBO();
                // Retirei Os IDs porque não estão a ser utilizados e temos depois de alguma forma encriptar estes IDS
                // 'a.Id_alerta as alerta_id',
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.codOco as codigo', 'b.name_PT as categoria', 'c.name_PT as subcategoria, d.freguesia as freguesia, e.'.$EstadoName.' as estado, a.data_criacao'))
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_subcategoria AS c ON c.Id_subcategoria = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_alerta_estados AS e ON e.id_estado = a.estado')
                    ->from("#__virtualdesk_alerta as a")
                    ->where(' a.estado=1 and a.Id_alerta in ('.$stringAlertaIdsToFilter.') ' )
                    ->order(' a.data_alteracao DESC ')
                );
                $dummyHRef  =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&alerta_id=');
                $dataReturn = $db->loadObjectList();
                $obVDCrypt  = new VirtualDeskSiteCryptHelper();
                $params     = JComponentHelper::getParams('com_virtualdesk');
                $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
                $dataReturnCut       = array();
                $dataReturnCut['dt'] = array();
                $dataReturnCut['nr'] = 0;
                $counti = 0;
                foreach ($dataReturn as $keyR => $valR) {
                    $dataReturnCut['dt'][$keyR]        = $valR;
                    $dataReturnCut['dt'][$keyR]->dummy = $dummyHRef.$valR->dmId;
                    if($setencrypt_forminputhidden==1) $dataReturnCut['dt'][$keyR]->dummy = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($dummyHRef.$valR->dmId);
                    unset($dataReturnCut['dt'][$keyR]->dmId);
                    $counti++;
                    if($counti>=(int)$avisosMaxRecords) break;
                }
                $dataReturnCut['nr'] = sizeof($dataReturn);
                return($dataReturnCut);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Retorna a Ref Id apenas se o User atual da sessão tem o mesmo NIF da ocorrência
           Útil para saber se o user tem acesso a este alerta...
        */
        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.codOco as refid' ))
                    ->from("#__virtualdesk_alerta as a")
                    ->where( $db->quoteName('a.codOco') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Atualiza um alerta para o ecrã/permissões do USER */
        public static function update4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4users'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $alerta_id = $data['alerta_id'];
            if((int) $alerta_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlerta($db);
            $Table->load(array('Id_alerta'=>$alerta_id,'nif'=>$UserSessionNIF));

            // Se o estado não for o inicial, o user não pode editar
            $chkIdEstadoInicial = (int) self::getEstadoIdInicio();
            $chkIdEstadoAtual   = (int)$Table->estado;
            if($chkIdEstadoInicial != $chkIdEstadoAtual && $chkIdEstadoAtual>0) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try {

                $referencia          = $Table->codOco;
                $obParam             = new VirtualDeskSiteParamsHelper();

                $concelhoAlerta      = $obParam->getParamsByTag('alertaConcelho');
                $espera              = $obParam->getParamsByTag('espera');
                $indicativoConcelho  = $obParam->getParamsByTag('AlertaIndConcelho');
                $codPostalMunicipio  = $obParam->getParamsByTag('codPostalMunicipio');
                $moradaMunicipio  = $obParam->getParamsByTag('moradaMunicipio');
                $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
                $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');
                $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
                $nomeMunicipio  = $obParam->getParamsByTag('nomeMunicipio');

                $categoria     = null;
                $subcategoria  = null;
                $freguesias    = null;
                $sitio         = null;
                $morada        = null;
                $ptosrefs      = null;
                $nome          = null;
                $fiscalid      = null;
                $descricao     = null;
                $email         = null;
                $lat           = null;
                $long          = null;

                if(array_key_exists('categoria',$data))    $categoria = $data['categoria'];
                if(array_key_exists('subcategoria',$data)) $subcategoria = $data['subcategoria'];
                if(array_key_exists('freguesias',$data))   $freguesias = $data['freguesias'];
                if(array_key_exists('sitio',$data))        $sitio = $data['sitio'];
                if(array_key_exists('morada',$data))       $morada = $data['morada'];
                if(array_key_exists('ptosrefs',$data))     $ptosrefs = $data['ptosrefs'];
                if(array_key_exists('nome',$data))         $nome = $data['nome'];
                if(array_key_exists('fiscalid',$data))     $fiscalid = $data['fiscalid'];
                if(array_key_exists('descricao',$data))    $descricao = $data['descricao'];
                if(array_key_exists('email',$data))        $email = $data['email'];
                if(array_key_exists('telefone',$data))     $telefone = $data['telefone'];
                if(array_key_exists('coordenadas',$data)) {
                    $splitCoord = explode(",", $data['coordenadas']);
                    $lat = $splitCoord[0];
                    $long = $splitCoord[1];
                }


                /* BEGIN Save Ocorrencia */
                $resSaveOcorrencia = VirtualDeskSiteAlertaHelper::saveEditedAlerta4User($alerta_id, $categoria, $subcategoria, $descricao, $concelhoAlerta, $freguesias, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long);
                /* END Save Ocorrencia */

                if (empty($resSaveOcorrencia)) return false;

                $alertsFiles        =  new VirtualDeskSiteAlertsFilesHelper();

                // Verifica se existem ficheiros a ELIMINAR
                $listFile2Eliminar  = $alertsFiles->setListFile2EliminarFromPOST ($referencia);
                $resFileDelete      = $alertsFiles->deleteFiles ($referencia , $listFile2Eliminar);

                if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros', 'error');
                }

                // Insere os NOVOS ficheiros
                $alertsFiles->tagprocesso = 'ALERTA_POST';
                $alertsFiles->idprocesso  = $referencia;
                $resFileSave              = $alertsFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false) {
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                }

                /*Send Email*/
                $Table->load(array('Id_alerta'=>$alerta_id));
                $categoria = $Table->categoria;
                $subcategoria = $Table->subcategoria;
                $freguesia = $Table->freguesia;
                $catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
                $subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
                $fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesia);

                $descricao = $Table->descricao;
                $sitio     = $Table->sitio;
                $morada    = $Table->local;
                $ptosrefs  = $Table->pontos_referencia;
                $nome      = $Table->utilizador;
                $email     = $Table->email;
                $lat       = $Table->latitude;
                $long      = $Table->longitude;
                $fiscalid  = $Table->nif;
                $telefone  = $Table->telefone;


                VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone, true);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone, true);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaCategoria($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, '', $email, $lat, $long, $fiscalid, $telefone, true,$categoria,$subcategoria);
                /*END Send Email*/

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // LOG
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $alertsFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED') . ' ref=' . $referencia;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog). ' | '.JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_DELETED').implode(",", $listFile2Eliminar);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Alerta');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$Table->nif.' | '.'email: '.$Table->email.' | '.'tel: '.$Table->telefone.' | '.'utilz: '.$Table->utilizador ;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPUpdate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = $alerta_id;
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Atualiza um alerta para o ecrã/permissões do MANAGER */
        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $alerta_id = $data['alerta_id'];
            if((int) $alerta_id <=0 ) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlerta($db);
            $Table->load(array('Id_alerta'=>$alerta_id));

            if (empty($data)) return false;

            try {

                $referencia          = $Table->codOco;
                $obParam             = new VirtualDeskSiteParamsHelper();

                $concelhoAlerta      = $obParam->getParamsByTag('alertaConcelho');
                $espera              = $obParam->getParamsByTag('espera');
                $indicativoConcelho  = $obParam->getParamsByTag('AlertaIndConcelho');

                $codPostalMunicipio  = $obParam->getParamsByTag('codPostalMunicipio');
                $moradaMunicipio  = $obParam->getParamsByTag('moradaMunicipio');
                $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
                $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');
                $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
                $nomeMunicipio  = $obParam->getParamsByTag('nomeMunicipio');

                $categoria     = null;
                $subcategoria  = null;
                $freguesias    = null;
                $sitio         = null;
                $morada        = null;
                $ptosrefs      = null;
                $nome          = null;
                $fiscalid      = null;
                $descricao     = null;
                $email         = null;
                $lat           = null;
                $long          = null;

                if(array_key_exists('categoria',$data))    $categoria = $data['categoria'];
                if(array_key_exists('subcategoria',$data)) $subcategoria = $data['subcategoria'];
                if(array_key_exists('freguesias',$data))   $freguesias = $data['freguesias'];
                if(array_key_exists('sitio',$data))        $sitio = $data['sitio'];
                if(array_key_exists('morada',$data))       $morada = $data['morada'];
                if(array_key_exists('ptosrefs',$data))     $ptosrefs = $data['ptosrefs'];
                if(array_key_exists('nome',$data))         $nome = $data['nome'];
                if(array_key_exists('fiscalid',$data))     $fiscalid = $data['fiscalid'];
                if(array_key_exists('descricao',$data))    $descricao = $data['descricao'];
                if(array_key_exists('email',$data))        $email = $data['email'];
                if(array_key_exists('telefone',$data))     $telefone = $data['telefone'];
                if(array_key_exists('coordenadas',$data)) {
                    $splitCoord = explode(",", $data['coordenadas']);
                    $lat = $splitCoord[0];
                    $long = $splitCoord[1];
                }


                /* BEGIN Save Ocorrencia */
                $resSaveOcorrencia = VirtualDeskSiteAlertaHelper::saveEditedAlerta4Manager($alerta_id, $categoria, $subcategoria, $descricao, $concelhoAlerta, $freguesias, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, 1);
                /* END Save Ocorrencia */

                if (empty($resSaveOcorrencia)) return false;

                $alertsFiles        =  new VirtualDeskSiteAlertsFilesHelper();

                // Verifica se existem ficheiros a ELIMINAR
                $listFile2Eliminar  = $alertsFiles->setListFile2EliminarFromPOST ($referencia);
                $resFileDelete      = $alertsFiles->deleteFiles ($referencia , $listFile2Eliminar);

                if ($resFileDelete==false && !empty($listFile2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar os ficheiros', 'error');
                }

                // Insere os NOVOS ficheiros
                $alertsFiles->tagprocesso = 'ALERTA_POST';
                $alertsFiles->idprocesso  = $referencia;
                $resFileSave              = $alertsFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false) {
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                }

                /*Send Email*/
                $Table->load(array('Id_alerta'=>$alerta_id));
                $categoria = $Table->categoria;
                $subcategoria = $Table->subcategoria;
                $freguesia = $Table->freguesia;
                $catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
                $subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
                $fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesia);

                $descricao = $Table->descricao;
                $sitio     = $Table->sitio;
                $morada    = $Table->local;
                $ptosrefs  = $Table->pontos_referencia;
                $nome      = $Table->utilizador;
                $email     = $Table->email;
                $lat       = $Table->latitude;
                $long      = $Table->longitude;
                $fiscalid  = $Table->nif;
                $telefone  = $Table->telefone;


                VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone, true);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone, true);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaCategoria($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone, true,$categoria,$subcategoria);
                /*END Send Email*/

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // LOG
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $alertsFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED') . ' ref=' . $referencia;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog). ' | '.JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_DELETED').implode(",", $listFile2Eliminar);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Alerta');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$Table->nif.' | '.'email: '.$Table->email.' | '.'tel: '.$Table->telefone.' | '.'utilz: '.$Table->utilizador ;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPUpdate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = $alerta_id;
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Atualiza uma categoria do alerta - só para MAANAGER */
        public static function updateConfCategoria4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasAccessCfg = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $vbHasAccessCfg ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $categoria_id = $data['categoria_id'];
            if((int) $categoria_id <=0 ) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlertaCategoria($db);
            $Table->load(array('ID_categoria'=>$categoria_id));

            if (empty($data)) return false;
            unset($data['categoria_id']);

            if(!empty($data)) {
                $dateModified = new DateTime();
                $data['modify_date'] = $dateModified->format('Y-m-d H:i:s');
            }

            try {

                // Store the data.
                if (!$Table->save($data)) return false;

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // LOG
                if((int)$setLogGeralEnabled===1) {

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED') . ' Categoria_ID=' . $categoria_id;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $categoria_id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        /* Atualiza uma Sub Categoria do alerta - só para MAANAGER */
        public static function updateConfSubCategoria4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $subcategoria_id = $data['subcategoria_id'];
            if((int) $subcategoria_id <=0 ) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlertaSubCategoria($db);
            $Table->load(array('Id_subcategoria'=>$subcategoria_id));

            if (empty($data)) return false;
            unset($data['subcategoria_id']);

            if(!empty($data)) {
                $dateModified = new DateTime();
                $data['modify_date'] = $dateModified->format('Y-m-d H:i:s');
            }

            try {

                // Store the data.
                if (!$Table->save($data)) return false;

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // LOG
                if((int)$setLogGeralEnabled===1) {

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_UPDATED') . ' SubCategoria_ID=' . $subcategoria_id;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $subcategoria_id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        /* Cria novo alerta para o ecrã/permissões do USER */
        public static function create4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $db    = JFactory::getDbo();
            ##$Table = new VirtualDeskTableAlerta($db);

            if (empty($data) ) return false;

            try {
                /*Gerar Referencia UNICA de alerta*/
                $refExiste = 1;

                $refCat = VirtualDeskSiteAlertaHelper::getReferencia($data['categoria']);
                $year = substr(date("Y"), -2);

                $obParam      = new VirtualDeskSiteParamsHelper();

                $concelhoAlerta      = $obParam->getParamsByTag('alertaConcelho');
                $espera              = $obParam->getParamsByTag('espera');
                $indicativoConcelho  = $obParam->getParamsByTag('AlertaIndConcelho');
                $codPostalMunicipio  = $obParam->getParamsByTag('codPostalMunicipio');
                $moradaMunicipio  = $obParam->getParamsByTag('moradaMunicipio');
                $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
                $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');
                $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
                $nomeMunicipio  = $obParam->getParamsByTag('nomeMunicipio');


                $referencia = '';
                while($refExiste == 1){
                    $refRandom = VirtualDeskSiteAlertaHelper::random_code();
                    $referencia = $indicativoConcelho . '-' . $refCat . strtoupper($refRandom) . '-' . $year;
                    $checkREF = VirtualDeskSiteAlertaHelper::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA de alerta*/

                $categoria    = $data['categoria'];
                $subcategoria = $data['subcategoria'];
                $freguesias   = $data['freguesias'];
                $sitio        = $data['sitio'];
                $morada       = $data['morada'];
                $ptosrefs     = $data['ptosrefs'];
                $nome         = $data['nome'];
                $fiscalid     = $data['fiscalid'];
                $descricao    = $data['descricao'];
                $email        = $data['email'];
                $telefone     = $data['telefone'];

                $splitCoord = explode(",", $data['coordenadas']);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];

                /* Save Ocorrenci a*/
                $resSaveOcorrencia = VirtualDeskSiteAlertaHelper::saveNewAlerta($categoria, $referencia, $subcategoria, $descricao, $concelhoAlerta, $freguesias, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, 0);
                /*END Save Ocorrencia*/

                // Store the data.
                //if (!$Table->save($data)) return false;

                //$GetCreateId = $Table->id;
                if (empty($resSaveOcorrencia)) return false;

                $alertsFiles              =  new VirtualDeskSiteAlertsFilesHelper();
                $alertsFiles->tagprocesso = 'ALERTA_POST';
                $alertsFiles->idprocesso  = $referencia;
                $resFileSave              = $alertsFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false) {
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                }


                /*Send Email*/
                $catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
                $subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
                $fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesias);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaCategoria($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, '', $email, $lat, $long, $fiscalid, $telefone, false,$categoria,$subcategoria);
                /*END Send Email*/

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $alertsFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED') . ' ref=' . $referencia;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $nome         = $data['nome'];
                $fiscalid     = $data['fiscalid'];
                $descricao    = $data['descricao'];
                $email        = $data['email'];
                $telefone     = $data['telefone'];

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Alerta');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$fiscalid.' | '.'email: '.$email.' | '.'tel: '.$telefone.' | '.'nome: '.$nome ;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPCreate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = '';
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }


            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        /* Cria novo alerta para o ecrã/permissões do MANAGER */
        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $db    = JFactory::getDbo();
            ##$Table = new VirtualDeskTableAlerta($db);

            if (empty($data) ) return false;

            try {
                /*Gerar Referencia UNICA de alerta*/
                $refExiste = 1;

                $refCat = VirtualDeskSiteAlertaHelper::getReferencia($data['categoria']);
                $year = substr(date("Y"), -2);

                $obParam      = new VirtualDeskSiteParamsHelper();

                $concelhoAlerta      = $obParam->getParamsByTag('alertaConcelho');
                $espera              = $obParam->getParamsByTag('espera');
                $indicativoConcelho  = $obParam->getParamsByTag('AlertaIndConcelho');
                $codPostalMunicipio  = $obParam->getParamsByTag('codPostalMunicipio');
                $moradaMunicipio  = $obParam->getParamsByTag('moradaMunicipio');
                $emailCopyrightGeral  = $obParam->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $obParam->getParamsByTag('contactoTelefCopyrightEmail');
                $dominioMunicipio  = $obParam->getParamsByTag('dominioMunicipio');
                $LinkCopyright  = $obParam->getParamsByTag('LinkCopyright');
                $copyrightAPP  = $obParam->getParamsByTag('copyrightAPP');
                $nomeMunicipio  = $obParam->getParamsByTag('nomeMunicipio');


                $referencia = '';
                while($refExiste == 1){
                    $refRandom = VirtualDeskSiteAlertaHelper::random_code();
                    $referencia = $indicativoConcelho . '-' . $refCat . strtoupper($refRandom) . '-' . $year;
                    $checkREF = VirtualDeskSiteAlertaHelper::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA de alerta*/

                $categoria    = $data['categoria'];
                $subcategoria = $data['subcategoria'];
                $freguesias   = $data['freguesias'];
                $sitio        = $data['sitio'];
                $morada       = $data['morada'];
                $ptosrefs     = $data['ptosrefs'];
                $nome         = $data['nome'];
                $fiscalid     = $data['fiscalid'];
                $descricao    = $data['descricao'];
                $email        = $data['email'];
                $telefone     = $data['telefone'];

                $splitCoord = explode(",", $data['coordenadas']);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];

                /* Save Ocorrenci a*/
                $resSaveOcorrencia = VirtualDeskSiteAlertaHelper::saveNewAlerta($categoria, $referencia, $subcategoria, $descricao, $concelhoAlerta, $freguesias, $sitio, $morada, $ptosrefs, $nome, $fiscalid, $email, $telefone, $lat, $long, 1);
                /*END Save Ocorrencia*/

                // Store the data.
                //if (!$Table->save($data)) return false;

                //$GetCreateId = $Table->id;
                if (empty($resSaveOcorrencia)) return false;

                $alertsFiles              =  new VirtualDeskSiteAlertsFilesHelper();
                $alertsFiles->tagprocesso = 'ALERTA_POST';
                $alertsFiles->idprocesso  = $referencia;
                $resFileSave              = $alertsFiles->saveListFileByPOST('fileupload');

                if ($resFileSave===false) {
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                }

                /*Send Email*/
                $catName    = VirtualDeskSiteAlertaHelper::getCatName($categoria);
                $subcatName = VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);
                $fregName   = VirtualDeskSiteAlertaHelper::getFregName($freguesias);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrencia($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaAdmin($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, $nome, $email, $lat, $long, $fiscalid, $telefone);
                VirtualDeskSiteAlertaHelper::SendEmailOcorrenciaCategoria($codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio, $catName, $referencia, $subcatName, $descricao, $fregName, $sitio, $morada, $ptosrefs, '', $email, $lat, $long, $fiscalid, $telefone, false,$categoria,$subcategoria);
                /*END Send Email*/

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // Foi alterado o Pedido do tipo Contact Us
                if((int)$setLogGeralEnabled===1) {

                    $fileListLog = $alertsFiles->getFileBaseNameByRefId ($referencia);

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED') . ' ref=' . $referencia;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = JText::_('COM_VIRTUALDESK_EVENTLOG_FILELIST_ACTUAL').implode(",", $fileListLog);
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $referencia;
                    $vdlog->insertEventLog($eventdata);
                }

                $setLogEnabled  = $objVDParams->getParamsByTag('RGPD_Log_Enabled_Alerta');
                if((int)$setLogEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['title']         = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED');
                    // Na descricao devemos colocar os dados pessoais acedidos neste registo e nesta data
                    $eventdata['descricao']     = 'nif: '.$fiscalid.' | '.'email: '.$email.' | '.'tel: '.$telefone.' | '.'nome: '.$nome ;
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['idmodulo']      = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']    = $vdlog->getIdTipoOPCreate();
                    $eventdata['request']       = serialize($_REQUEST);
                    $eventdata['referer']       = $_SERVER['HTTP_REFERER'];
                    $eventdata['ref_id']        = '';
                    $eventdata['ref']           = $referencia;
                    $vdlog->insertRgpdLog($eventdata);
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        /* Cria novo Categoria para o ecrã/permissões do MANAGER */
        public static function createConfCategoria4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasAccessCfg = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $vbHasAccessCfg ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlertaCategoria($db);

            if (empty($data)) return false;

            try {

                // Store the data.
                if (!$Table->save($data)) return false;

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // LOG
                if((int)$setLogGeralEnabled===1) {

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED') . ' New Categoria =' . $data['name_PT'];
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = '';
                    $vdlog->insertEventLog($eventdata);
                }
                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        /* Cria nova SubCategoria c/ MANAGER */
        public static function createConfSubCategoria4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlertaSubCategoria($db);

            if (empty($data)) return false;

            try {

                // Store the data.
                if (!$Table->save($data)) return false;

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');

                // LOG
                if((int)$setLogGeralEnabled===1) {

                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_CREATED') . ' New SubCategoria =' . $data['name_PT'];
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = '';
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPCreate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = '';
                    $vdlog->insertEventLog($eventdata);
                }
                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        /* Grava dados nova mensagem no histórico genérico do Alerta do USER para o MANAGER */
        public static function saveNewMsgHist4UserByAjax($getInputAlerta_id, $NewMsg)
        {
            $config = JFactory::getConfig();

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewMsg) ) return false;
            if((int) $getInputAlerta_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;

            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlertaHistorico($db);

            $data = array();
            $data['idalerta']  = $getInputAlerta_id;
            $data['mensagem']  = $db->escape($NewMsg);
            $data['idtipo']    = (int) self::getAlertaHistoricoTipoIdByTag ('message');
            $data['iduser']    = $UserVDId;
            $data['iduserjos'] = $UserJoomlaID;
            $data['setbymanager'] = 0;
            $data['visible4user'] = 1;

            try {

                // Store the data.
                if (!$Table->save($data)) return false;

                // Envio de email, se estiver configurad
                // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
                $nomeManager   = '';
                $emailManager  = '';
                $emailAdmin    = '';
                $resNameEMail  = self::getNameAndEmailDoManagerEAdmin($getInputAlerta_id, $nomeManager, $emailManager, $emailAdmin);

                /* Carrega parâmetros*/
                $objVDParams  = new VirtualDeskSiteParamsHelper();
                $setSendMail  = $objVDParams->getParamsByTag('Alerta_SendMail_NewMsgHistFromUser');
                $setSendMailBCCAdmin = $objVDParams->getParamsByTag('Alerta_SendMail_AllMsg_BCC_To_Admin');
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');
                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                $refAlerta    = self::getReferenciaById($getInputAlerta_id);

                if((int)$setSendMail==1) {
                    $nomeUser     = $objUserVD->name;

                    $TO      = array();
                    $TOAdmin = array();
                    $BCC     = array();

                    if(!empty($emailManager)) {
                        $TO[] = array('name'=>$nomeManager , 'email'=>$emailManager);
                        if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ALERTA_PROCMANAGER');
                        self::SendEmail4NewMsgHistFromUser($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['mensagem'], $TO, $BCC, $nomeUser, $nomeManager, $refAlerta);
                    }

                    if((int)$setSendMailBCCAdmin == 1) {
                        $TOAdmin[] = array('name'=>'Admin' , 'email'=>$emailAdmin);
                        self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['mensagem'], $TOAdmin, $BCC, 'Admin', $refAlerta);
                    }

                }

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_MSGFROMUSER');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_MSGFROMUSER') . ' msg=' . $db->escape($NewMsg) . ' user=' . $objUserVD->name . '-' . $objUserVD->email;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $refAlerta;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Grava dados nova mensagem no histórico genérico do Alerta do MANAGER para USER
           $EnableSendMail : Se estiver a 0 não envia nenhum email -> para o caso em que o email já foi gerado de outra forma e não pelo "Save M;sg no Hist"
        */
        public static function saveNewMsgHist4ManagerByAjax($getInputAlerta_id, $NewMsg, $setVisible4User, $seTipoMsg, $EnableSendMail=1)
        {
            $config = JFactory::getConfig();

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewMsg) ) return false;
            if((int) $getInputAlerta_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;

            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlertaHistorico($db);

            $data = array();
            $data['idalerta']  = $getInputAlerta_id;
            $data['mensagem']  = $db->escape($NewMsg);
            $data['idtipo']    = $seTipoMsg;
            $data['iduser']    = $UserVDId;
            $data['iduserjos'] = $UserJoomlaID;
            $data['setbymanager'] = 1;
            $data['visible4user'] = $setVisible4User;

            try {
                // Store the data.
                if (!$Table->save($data)) return false;

                /* Carrega parâmetros*/
                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setSendMail = $objVDParams->getParamsByTag('Alerta_SendMail_NewMsgHistFromManager');
                $setSendMailBCCAdmin = $objVDParams->getParamsByTag('Alerta_SendMail_AllMsg_BCC_To_Admin');
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');
                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');
                $setSendNotificacao = $objVDParams->getParamsByTag('Alerta_SendNotificacao_NewMsgHistFromManager');
                $NotifMaxSizeTituloMsg = (int)$objVDParams->getParamsByTag('Notificacao_MaxSizeTituloMsg');


                $refAlerta = self::getReferenciaById($getInputAlerta_id);

                // Envio de email, se estiver configurad
                if ((int)$setSendMail == 1 && (int)$EnableSendMail==1) {

                    // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
                    $nomeManager   = '';
                    $emailManager  = '';
                    $emailAdmin    = '';
                    $resNameEMail  = self::getNameAndEmailDoManagerEAdmin($getInputAlerta_id, $nomeManager, $emailManager, $emailAdmin);

                    $TOManager = array();
                    $TOUser    = array();
                    $TOAdmin   = array();
                    $BCC       = array();

                    // Vai enviar mail para o MANAGER...
                    if(!empty($emailManager)) {
                        $TOManager[] = array('name' => $nomeManager, 'email' => $emailManager);
                        if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ALERTA_PROCMANAGER');
                        self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['mensagem'], $TOManager, $BCC, $nomeManager, $refAlerta);
                    }

                    // Vai enviar mail para o USER se...
                    if ((int)$setVisible4User == 1) {
                        // Vai tentar verificar qual o email do utilizador. Se não estiver no alerta, tenta carregar a partir do NIF a ver se existe como user
                        // Caso contrário não envia o email para o utilizador
                        $TableAlerta = new VirtualDeskTableAlerta($db);
                        $TableAlerta->load(array('Id_alerta'=>$getInputAlerta_id));
                        $emailUser = (string) $TableAlerta->email;
                        $nomeUser  = $TableAlerta->utilizador;
                        if(empty($emailUser)) {
                            if(!empty($TableAlerta->nif)) {
                                $objUserAlertaTmp = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($TableAlerta->nif);
                                if(!empty($objUserAlertaTmp->email)) {
                                    $emailUser = $objUserAlertaTmp->email;
                                    $nomeUser = $objUserAlertaTmp->name;
                                }
                            }
                        }
                        $TOUser[] = array('name'=>$nomeUser , 'email'=>$emailUser);
                        self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['mensagem'], $TOUser, $BCC, $nomeUser, $refAlerta);
                    }

                    // Vai enviar mail para o ADMIN se...
                    // Se não tiver definido o bit não envia em BCC para o Admin
                    if((int)$setSendMailBCCAdmin == 1) {
                        $TOAdmin[] = array('name'=>'Admin' , 'email'=>$emailAdmin);
                        self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $data['mensagem'], $TOAdmin, $BCC, 'Admin', $refAlerta);
                    }

                }

                // Envio de NOTIFICAÇÂO para o USER se estiver configurado o envio e se a mensagem é VISIVEL para o USER
                if ( ((int)$setVisible4User == 1) && ((int)$setSendNotificacao == 1)) {
                    # Vai carrfega o Id do Utilizador associado ao alerta

                    $setNotifUserId  = array();
                    $setNotifGroupId = array();

                    $TableAlerta = new VirtualDeskTableAlerta($db);
                    $TableAlerta->load(array('Id_alerta'=>$getInputAlerta_id));
                    if(!empty($TableAlerta->nif)) {
                        $objUserAlertaTmp = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($TableAlerta->nif);
                        if(!empty($objUserAlertaTmp->id)) {
                            $setNotifUserId = array($objUserAlertaTmp->id);
                        }
                    }

                    if(!empty($setNotifUserId)) {
                        //  Se ultrapassar em algumas situações o ideal é cortar o titulo e passar a mensagem toda para a Descrição da notificação.
                        $setNotifMsgDesc = '';
                        $setNotifMsgTitulo =  $NewMsg;
                        if( strlen($NewMsg) > (int)$NotifMaxSizeTituloMsg ) {
                            $setNotifMsgTitulo = substr($NewMsg,$NotifMaxSizeTituloMsg);
                            $setNotifMsgDesc   =  $NewMsg;
                        }

                        VirtualDeskSiteNotificacaoHelper::saveNovaNotificacaoByProcesso4ManagerByAjax ($getInputAlerta_id, 'alerta',$setNotifUserId, $setNotifGroupId, $setNotifMsgDesc, $setNotifMsgTitulo);
                    }
                }

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_MSGFROMMANAGER');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_MSGFROMMANAGER') . 'msg=' . $db->escape($NewMsg) . ' manager=' . $nomeManager . '-' . $emailManager;
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $refAlerta;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Grava dados para definir um novo ESTADO do processo de alerta  */
        public static function saveAlterar2NewEstado4ManagerByAjax($getInputAlerta_id, $NewEstadoId, $NewEstadoDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('alerta', 'alterarestado4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewEstadoId) ) return false;
            if((int) $getInputAlerta_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            $data = array();
            $data['estado'] = $NewEstadoId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();

            $Table = new VirtualDeskTableAlerta($db);
            $Table->load(array('Id_alerta'=>$getInputAlerta_id));

            // Só alterar se o estado for diferente
            if((int)$Table->estado == (int)$NewEstadoId )  return true;

            if(!empty($data)) {
                $dateModified = new DateTime();
                $data['modified'] = $dateModified->format('Y-m-d H:i:s');
            }

            $db->transactionStart();

            try {
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTableAlertaEstadoHistorico($db);
                $dataHist = array();
                $dataHist['id_estado']  = $NewEstadoId;
                $dataHist['id_alerta']  = $getInputAlerta_id;
                $dataHist['descricao']  = $NewEstadoDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled  = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');
                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                /* Carrega configuração para o envio de mensagens do estado atual */
                $objEstadoConfig = self::getAlertaEstadoConfigObjectByIdAlerta($NewEstadoId);

                // A alteração de estado é visivel para o utilizador nos histórico de mensagens do processo ?
                $setVisible4User = 0;
                if(!empty($objEstadoConfig->bitHistoricoVisible4User)) $setVisible4User = (int) $objEstadoConfig->bitHistoricoVisible4User;

                // Define tipo de mensagem a ser colocada no histórico
                $IdTipoMsgAltEstado = (int) self::getAlertaHistoricoTipoIdByTag ('statechange');

                // Vai agora colocar mensagem no histórico (com ou sem visibilidade para o user)
                $NomeNovoEstado      = self::getAlertaEstadoNameById ($lang, $NewEstadoId);
                $NewMsg              = JText::_('COM_VIRTUALDESK_ALERTA_CHANGED_ESTADO_PARA')." ".$NomeNovoEstado;
                $NewMsg             .= " <br/> ". $NewEstadoDesc;

                // Coloca no histórico, mas não envia email nesta função porque neste método existe essa gestão pois depende do estado
                self::saveNewMsgHist4ManagerByAjax($getInputAlerta_id, $NewMsg, $setVisible4User, $IdTipoMsgAltEstado, 0);

                $refAlerta = self::getReferenciaById($getInputAlerta_id);

                // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
                $nomeManager   = '';
                $emailManager  = '';
                $emailAdmin    = '';
                $resNameEMail  = self::getNameAndEmailDoManagerEAdmin($getInputAlerta_id, $nomeManager, $emailManager, $emailAdmin);

                // Vai tentar verificar qual o email do utilizador. Se não estiver no alerta, tenta carregar a partir do NIF a ver se existe como user
                // Caso contrário não envia o email para o utilizador
                $emailUser = (string) $Table->email;
                $nomeUser  = $Table->utilizador;
                if(empty($emailUser)) {
                    if(!empty($Table->nif)) {
                        $objUserAlertaTmp = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($Table->nif);
                        if(!empty($objUserAlertaTmp->email)) {
                            $emailUser = $objUserAlertaTmp->email;
                            $nomeUser = $objUserAlertaTmp->name;
                        }
                    }
                }

                // Vai enviar email para o User, se estiver configurado na tabela do EstadoConfig
                $bitsendmail4user= (int)$objEstadoConfig->bitsendemail4user;
                if ( ((int)$bitsendmail4user === 1) && !empty($emailUser)) {
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>$nomeUser , 'email'=>$emailUser);
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeUser, $refAlerta);
                }

                // Vai enviar email para o Manager se estiver configurado na tabela do EstadoConfig,
                $bitsendmail4manager= (int)$objEstadoConfig->bitsendemail4manager;
                if ( ((int)$bitsendmail4manager === 1) && !empty($emailManager)) {
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>$nomeManager , 'email'=>$emailManager);
                    if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ALERTA_PROCMANAGER');
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeManager, $refAlerta);
                }

                // Vai enviar email para o Admin se estiver configurado na tabela do EstadoConfig,
                $bitsendmail4admin= (int)$objEstadoConfig->bitsendemail4admin;
                if ( ((int)$bitsendmail4admin === 1) && !empty($emailAdmin)) {
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>'' , 'email'=>$emailAdmin);
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , 'Admin', $refAlerta);
                }

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_alerta=' . $dataHist['id_alerta'];
                    $eventdata['desc'] .= '<br> '.json_encode($data);
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $refAlerta;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin */
        public static function getNameAndEmailDoManagerEAdmin($alerta_id, &$nomeManager, &$emailManager, &$emailAdmin)
        {
            $config                      = JFactory::getConfig();
            $objVDParams                 = new VirtualDeskSiteParamsHelper();
            $AlertaMailManagerbyDefault  = $objVDParams->getParamsByTag('Alerta_Mail_Manager_by_Default');
            $AlertaMailAdminbyDefault    = $objVDParams->getParamsByTag('mailAdminAlerta');
            $adminSiteDefaultEmail       = $config->get('mailfrom');

            // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin
            $objManagerAlertaTmp = self::getAlertaProcManagerAtualObjectByIdAlerta ('', $alerta_id);
            $emailManager = (string) $objManagerAlertaTmp->email;
            $nomeManager  = (string) $objManagerAlertaTmp->name;
            if(empty($emailManager)) {
                $emailManager = $AlertaMailManagerbyDefault;
                if(empty($emailManager)) {
                    $emailManager = $AlertaMailAdminbyDefault;
                    if(empty($emailManager)) {
                        $emailManager =  $adminSiteDefaultEmail;
                    }
                }
            }

            // Verifica se tem email do admin do alerta caso contrário fica o admin do site
            $emailAdmin = $AlertaMailAdminbyDefault;
            if(empty($emailAdmin)) {
                $emailAdmin = $adminSiteDefaultEmail;
            }

            return(true);
        }

        /* Grava dados para definir um novo responsável (MANAGER) pelo processo de alerta  */
        public static function saveAlterar2NewProcManager4ManagerByAjax($getInputAlerta_id, $NewProcManagerId, $NewProcManagerDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewProcManagerId) ) return false;
            if((int) $getInputAlerta_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;


            $data = array();
            $data['id_procmanager'] = $NewProcManagerId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableAlerta($db);
            $Table->load(array('Id_alerta'=>$getInputAlerta_id));

            // Só alterar se o id for diferente
            if((int)$Table->id_procmanager == (int)$NewProcManagerId )  return true;

            if(!empty($data)) {
                $dateModified = new DateTime();
                $data['modified'] = $dateModified->format('Y-m-d H:i:s');
            }

            $db->transactionStart();

            try {
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTableAlertaProcManagerHistorico($db);
                $dataHist = array();
                $dataHist['id_procmanager']  = $NewProcManagerId;
                $dataHist['id_alerta']  = $getInputAlerta_id;
                $dataHist['descricao']  = $NewProcManagerDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams                = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled         = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');
                // Msg Histórico fica visivel para o USER ?
                $setVisible4User            = (int)$objVDParams->getParamsByTag('Alerta_ProcManager_MsgHist_setVisible4User');
                $setProcManagerSendMailUser = (int)$objVDParams->getParamsByTag('Alerta_ProcManager_SendMail_User');
                $setProcManagerSendMailManager = (int)$objVDParams->getParamsByTag('Alerta_ProcManager_SendMail_Manager');
                $setProcManagerSendMailAdmin   = (int)$objVDParams->getParamsByTag('Alerta_ProcManager_SendMail_Admin');
                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                // Define tipo de mensagem a ser colocada no histórico
                $IdTipoMsgInfo = (int) self::getAlertaHistoricoTipoIdByTag ('information');

                // Se gravou vai agora colocar mensagem no histórico e envia email
                $NomeNovoProcManagerName = VirtualDeskSiteUserHelper::getUserNameById($NewProcManagerId);
                $NewMsg              = JText::_('COM_VIRTUALDESK_ALERTA_CHANGED_PROCMANAGER_PARA')." ".$NomeNovoProcManagerName;
                $NewMsg             .= " <br/> ". $NewProcManagerDesc;

                // Coloca no histórico, mas não envia email nesta função porque neste método existe essa gestão
                self::saveNewMsgHist4ManagerByAjax($getInputAlerta_id, $NewMsg, $setVisible4User, $IdTipoMsgInfo, 0);

                $refAlerta = self::getReferenciaById($getInputAlerta_id);

                // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
                $nomeManager   = '';
                $emailManager  = '';
                $emailAdmin    = '';
                if($setProcManagerSendMailManager===1 || $setProcManagerSendMailAdmin===1) {
                    $resNameEMail = self::getNameAndEmailDoManagerEAdmin($getInputAlerta_id, $nomeManager, $emailManager, $emailAdmin);
                }

                // Vai enviar email para o User, se estiver configurado
                if ( ((int)$setProcManagerSendMailUser === 1) ) {

                    // Vai tentar verificar qual o email do utilizador. Se não estiver no alerta, tenta carregar a partir do NIF a ver se existe como user
                    // Caso contrário não envia o email para o utilizador
                    $emailUser = (string) $Table->email;
                    $nomeUser  = $Table->utilizador;
                    if(empty($emailUser)) {
                        if(!empty($Table->nif)) {
                            $objUserAlertaTmp = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($Table->nif);
                            if(!empty($objUserAlertaTmp->email)) {
                                $emailUser = $objUserAlertaTmp->email;
                                $nomeUser = $objUserAlertaTmp->name;
                            }
                        }
                    }
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>$nomeUser , 'email'=>$emailUser);
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeUser, $refAlerta);
                }

                // Vai enviar email para o Manager se estiver configurado na tabela do EstadoConfig,
                if ( ((int)$setProcManagerSendMailManager === 1) && !empty($emailManager)) {
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>$nomeManager , 'email'=>$emailManager);
                    if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ALERTA_PROCMANAGER');
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeManager, $refAlerta);
                }

                // Vai enviar email para o Admin se estiver configurado na tabela do EstadoConfig,
                if ( ((int)$setProcManagerSendMailAdmin === 1) && !empty($emailAdmin)) {
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>'' , 'email'=>$emailAdmin);
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , 'Admin', $refAlerta);
                }

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_PROCMANAGER_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_PROCMANAGER_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_alerta=' . $dataHist['id_alerta'];
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $refAlerta;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Grava dados para reencaminhar um pedido de alerta para outros utilizadores e/ou grupos */
        public static function saveAlterar2Reencaminhar4ManagerByAjax($getInputAlerta_id, $setUserId, $setGroupId, $setReencDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            //if( empty($setUserId) and empty($setGroupId))  return false;
            if((int) $getInputAlerta_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            // Set Tabela da BD
            $db    = JFactory::getDbo();

            $db->transactionStart();

            try {

                // Coloca todos os bits Atual a zero, nas 3 tabelas existentes do Encaminhar Hist
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $query = $db->getQuery(true);
                $query->update($db->quoteName('#__virtualdesk_alerta_encaminhar_hist'))->set($db->quoteName('atual') . ' = 0' )->where($db->quoteName('id_alerta').'='.$getInputAlerta_id);
                $db->setQuery($query);
                $resultUp01 = $db->execute();

                $query->update($db->quoteName('#__virtualdesk_alerta_encaminhar_user_hist'))->set($db->quoteName('atual') . ' = 0' )->where($db->quoteName('id_alerta').'='.$getInputAlerta_id);
                $db->setQuery($query);
                $resultUp02 = $db->execute();

                $query->update($db->quoteName('#__virtualdesk_alerta_encaminhar_group_hist'))->set($db->quoteName('atual') . ' = 0' )->where($db->quoteName('id_alerta').'='.$getInputAlerta_id);
                $db->setQuery($query);
                $resultUp03 = $db->execute();

                if( !($resultUp01) || !($resultUp02) || !($resultUp03) ) {
                    $db->transactionRollback();
                    return false;
                }

                $TableHist              = new VirtualDeskTableAlertaEncaminharHist($db);
                $dataHist               = array();
                $dataHist['id_alerta']  = $getInputAlerta_id;
                $dataHist['atual']      = 1;
                $dataHist['descricao']  = $db->escape($setReencDesc);
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Tabela Hist
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }

                $newEncaminharId = $TableHist->id;

                // Tabela User Hist
                if(!empty($setUserId) && is_array($setUserId) ) {
                    foreach ($setUserId as $chvU => $valU) {
                        $TableUserHist  = new VirtualDeskTableAlertaEncaminharUserHist($db);
                        $dataUserHist = array();
                        $dataUserHist['id_encaminhar'] = $newEncaminharId;
                        $dataUserHist['id_alerta']     = $getInputAlerta_id;
                        $dataUserHist['id_user']       = $valU;
                        $dataUserHist['atual']         = 1;
                        $dataUserHist['createdby']     = $UserJoomlaID;
                        $dataUserHist['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableUserHist->save($dataUserHist)) {
                            $db->transactionRollback();
                            return false;
                        }
                    }
                }

                // Tabela Group Hist
                if(!empty($setGroupId) && is_array($setGroupId) ) {
                    foreach ($setGroupId as $chvG => $valG) {
                        $TableGroupHist  = new VirtualDeskTableAlertaEncaminharGroupHist($db);
                        $dataGroupHist = array();
                        $dataGroupHist['id_encaminhar'] = $newEncaminharId;
                        $dataGroupHist['id_alerta']     = $getInputAlerta_id;
                        $dataGroupHist['id_group']      = $valG;
                        $dataGroupHist['atual']         = 1;
                        $dataGroupHist['createdby']     = $UserJoomlaID;
                        $dataGroupHist['modifiedby']    = $UserJoomlaID;
                        // Store the data.
                        if (!$TableGroupHist->save($dataGroupHist)) {
                            $db->transactionRollback();
                            return false;
                        }
                    }
                }

                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                $objVDParams                = new VirtualDeskSiteParamsHelper();
                $setLogGeralEnabled         = $objVDParams->getParamsByTag('Alerta_Log_Geral_Enabled');
                // Msg Histórico fica visivel para o USER ?
                $setVisible4User            = (int)$objVDParams->getParamsByTag('Alerta_Reencaminhar_MsgHist_setVisible4User');
                $setReencaminharSendMailUser    = (int)$objVDParams->getParamsByTag('Alerta_Reencaminhar_SendMail_User');
                $setReencaminharSendMailManager = (int)$objVDParams->getParamsByTag('Alerta_Reencaminhar_SendMail_Manager');
                $setReencaminharSendMailAdmin   = (int)$objVDParams->getParamsByTag('Alerta_Reencaminhar_SendMail_Admin');
                $setReencaminharSendMailUsersAndGroups = (int)$objVDParams->getParamsByTag('Alerta_Reencaminhar_SendMail_UsersAndGroups');
                $dominioMunicipio  = $objVDParams->getParamsByTag('dominioMunicipio');
                $emailCopyrightGeral  = $objVDParams->getParamsByTag('emailCopyrightGeral');
                $contactoTelefCopyrightEmail  = $objVDParams->getParamsByTag('contactoTelefCopyrightEmail');
                $copyrightAPP  = $objVDParams->getParamsByTag('copyrightAPP');
                $LinkCopyright  = $objVDParams->getParamsByTag('LinkCopyright');

                // Define tipo de mensagem a ser colocada no histórico
                $IdTipoMsgInfo = (int) self::getAlertaHistoricoTipoIdByTag ('forward');

                // Carrega dados dos users para onde foi encaminhado o processo...basicamente o encaminhar é um aviso, poderá depois surgir nas TAREFAS dos users, etc
                $objUsersEmailList    = VirtualDeskSiteUserHelper::getUserEmailNameByIdArray($setUserId);
                $TOUsersRe            = array();
                $UsersEmptyEmail      = array();
                $GroupsEmptyEmail     = array();
                if(is_array($objUsersEmailList)) {
                    $contaTmp      = 0;
                    $NewMsgUsersRe = '';
                    foreach ($objUsersEmailList as $chvUe => $valUe ) {
                       if($contaTmp>0) $NewMsgUsersRe .= ',';
                       $NewMsgUsersRe .= $valUe['name'];
                       $contaTmp ++;
                       if((string)$valUe['email']!='') {
                           $TOUsersRe[] = array('name'=>$valUe['name'] , 'email'=>$valUe['email']);
                       }
                       else {
                         // users sem email definido
                           $UsersEmptyEmail[] = array('name'=>$valUe['name'] , 'email'=>$valUe['email']);
                       }
                    }
                }
                // Carrega dados dos grupos encaminhados
                $objPermGroups = new VirtualDeskSitePermissionsHelper();
                $objGroupsEmailList = $objPermGroups->getGroupEmailNameByIdArray($setGroupId);
                $TOGroupsRe         = array();
                if(is_array($objGroupsEmailList)) {
                    $contaTmp      = 0;
                    $NewMsgGroupsRe = '';
                    foreach ($objGroupsEmailList as $chvGe => $valGe ) {
                        if($contaTmp>0) $NewMsgGroupsRe .= ',';
                        $NewMsgGroupsRe .= $valGe['name'];
                        $contaTmp ++;

                        if((string)$valGe['email']!='') {
                          $TOGroupsRe[] = array('name'=>$valGe['name'] , 'email'=>$valGe['email']);
                        }
                        else {
                          // grupos sem email definido
                            $GroupsEmptyEmail[] = array('name'=>$valGe['name'] , 'email'=>$valGe['email']);
                        }
                    }
                }


                // Se gravou vai agora colocar mensagem no histórico e envia email
                //$NomeNovoProcManagerName = VirtualDeskSiteUserHelper::getUserNameById($NewProcManagerId);
                $NewMsg              = JText::_('COM_VIRTUALDESK_ALERTA_ENCAMINHADOPARA')."  U: " . $NewMsgUsersRe .' G ' . $NewMsgGroupsRe;
                $NewMsg             .= " <br/> ". $setReencDesc;

                // Coloca no histórico, mas não envia email nesta função porque neste método existe essa gestão
                self::saveNewMsgHist4ManagerByAjax($getInputAlerta_id, $NewMsg, $setVisible4User, $IdTipoMsgInfo, 0);

                $refAlerta = self::getReferenciaById($getInputAlerta_id);

                // Define se tem o email do manager do processo, caso contrário vai carregar o email por defeito do manager, caso contrário carrega do Admin$nomeManager, $emailManager, $emailAdmin
                $nomeManager   = '';
                $emailManager  = '';
                $emailAdmin    = '';
                if($setReencaminharSendMailManager===1 || $setReencaminharSendMailAdmin===1) {
                    $resNameEMail = self::getNameAndEmailDoManagerEAdmin($getInputAlerta_id, $nomeManager, $emailManager, $emailAdmin);
                }

                // Vai enviar email para o USER dono do processo, se estiver configurado
                if ( ((int)$setReencaminharSendMailUser === 1) ) {
                    $TableAlerta = new VirtualDeskTableAlerta($db);
                    $TableAlerta->load(array('Id_alerta'=>$getInputAlerta_id));

                    // Vai tentar verificar qual o email do utilizador. Se não estiver no alerta, tenta carregar a partir do NIF a ver se existe como user
                    // Caso contrário não envia o email para o utilizador
                    $emailUser = (string) $TableAlerta->email;
                    $nomeUser  = $TableAlerta->utilizador;
                    if(empty($emailUser)) {
                        if(!empty($TableAlerta->nif)) {
                            $objUserAlertaTmp = VirtualDeskSiteUserHelper::getUserObjByFiscalNumber($TableAlerta->nif);
                            if(!empty($objUserAlertaTmp->email)) {
                                $emailUser = $objUserAlertaTmp->email;
                                $nomeUser = $objUserAlertaTmp->name;
                            }
                        }
                    }
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>$nomeUser , 'email'=>$emailUser);
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeUser, $refAlerta);
                }

                // Vai enviar email para o Manager se estiver configurado na tabela do EstadoConfig,
                if ( ((int)$setReencaminharSendMailManager === 1) && !empty($emailManager)) {
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>$nomeManager , 'email'=>$emailManager);
                    if(empty($nomeManager)) $nomeManager =  JText::_('COM_VIRTUALDESK_ALERTA_PROCMANAGER');
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , $nomeManager, $refAlerta);
                }

                // Vai enviar email para o Admin se estiver configurado na tabela do EstadoConfig,
                if ( ((int)$setReencaminharSendMailAdmin === 1) && !empty($emailAdmin)) {
                    $TO   = array();
                    $BCC  = array();
                    $TO[] = array('name'=>'' , 'email'=>$emailAdmin);
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , 'Admin', $refAlerta);
                }

                // Vai enviar email para os Users e ou Grupos para onde foi reencaminhado
                if ( ((int)$setReencaminharSendMailUsersAndGroups === 1) ) {
                    $TO   = array();
                    $BCC  = array();

                    // USERS , carrega o emails de todos os users do reencaminhamentos e envia UMA mensagem com todos os endereços
                    $TO  = array_merge($TO,$TOUsersRe);
                    // GROUPS, carrega o email dos grupos, associados
                    $TO  = array_merge($TO,$TOGroupsRe);
                    self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsg, $TO, $BCC , 'Utilizadores', $refAlerta);

                    // Se não existir email do GRUPO / UTILIZADORES indica ao responsável e/ou manager que o grupo X não tem email definido?
                    $NewMsgEmpty = '';
                    if(!empty($UsersEmptyEmail) || !empty($GroupsEmptyEmail)) {
                        $TO   = array();
                        $TO[] = array('name'=>'' , 'email'=>$emailAdmin);
                        $TO[] = array('name'=>$nomeManager , 'email'=>$emailManager);
                        if(!empty($UsersEmptyEmail)) {
                            $NewMsgEmpty .= JText::_('COM_VIRTUALDESK_ALERTA_UTILIZADORES_SEM_EMAIL');
                            foreach ($UsersEmptyEmail as $chvEptU => $valEptU) {
                                $NewMsgEmpty .= " <br/> - ". $valEptU['name'];
                            }
                            $NewMsgEmpty .= " <br/> ";
                        }
                        if(!empty($GroupsEmptyEmail)) {
                            $NewMsgEmpty .= JText::_('COM_VIRTUALDESK_ALERTA_GRUPOS_SEM_EMAIL');
                            foreach ($GroupsEmptyEmail as $chvEptG => $valEptG) {
                               $NewMsgEmpty .= " <br/> - ". $valEptG['name'];
                            }
                            $NewMsgEmpty .= " <br/> ";
                        }
                        self::SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $NewMsgEmpty, $TO, $BCC , 'Admin', $refAlerta);
                    }
                }

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos']  = $UserJoomlaID;
                    $eventdata['title']  = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_ENCAMINHAR_ALTERADO');
                    $eventdata['desc']   = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_ENCAMINHAR_ALTERADO') . " U: " . implode($setUserId) .' G: ' . implode($setGroupId) . " - " . 'id_alerta=' . $dataHist['id_alerta'];
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = '';
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $eventdata['rgpd']        =  false;
                    $eventdata['idmodulo']    = $vdlog->getModuleIdByTag(self::tagchaveModulo);
                    $eventdata['id_tipo_op']  = $vdlog->getIdTipoOPUpdate();
                    $eventdata['accesslevel'] = $vdlog->getIdAccessLevel4Manager();
                    $eventdata['ref']         = $refAlerta;
                    $vdlog->insertEventLog($eventdata);
                }



                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        /* Send Email do USER depois de nova mensagem ou notificação do Alerta */
        public static function SendEmail4NewMsgHistFromUser($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $msg, $TO, $BCC, $nomeUser, $IntroName, $refAlerta){

            $config = JFactory::getConfig();

            /* Carrega parâmetros*/
            $objVDParams     = new VirtualDeskSiteParamsHelper();
            $layoutPathEmail = $objVDParams->getParamsByTag('Alerta_SendMail_NewMsgHistFromUser_LayoutPath');

            if((string)$layoutPathEmail=='') {
                // Se não carregar o caminho do layout sai com erro...
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }
            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_INTRO',$IntroName);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_NEWMSG_CORPO',$refAlerta);
            $BODY_SECONDLINE    = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_NEWMSG_CORPO2',$nomeUser);
            $BODY_THIRDLINE     = $msg;

            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo($data['mailfrom'] );
            $newEmail->setSender( $data['mailfrom']);
            $newEmail->setFrom( $data['fromname']);
            if(!is_array($TO)) $TO = array();
            foreach ($TO as $chvR => $valR) {
                $newEmail->addRecipient($valR['email'], $valR['name']);
            }
            if(!is_array($BCC)) $BCC = array();
            foreach ($BCC as $chvB => $valB) {
                $newEmail->addBcc($valB['email'], $valB['name']);
            }
            $newEmail->setSubject($data['sitename']);

            $logosendmailImage = $objVDParams->getParamsByTag('logosendmailalerta');
            $newEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $return = $newEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }

        /* Send Email do MANAGER para o USER ou outros depois de nova notificação do Alerta */
        public static function SendEmail4NewMsgHistFromManager($LinkCopyright, $copyrightAPP, $contactoTelefCopyrightEmail, $emailCopyrightGeral, $dominioMunicipio, $msg, $TO, $BCC, $IntroName, $refAlerta){

            $config = JFactory::getConfig();

            /* Carrega parâmetros*/
            $objVDParams     = new VirtualDeskSiteParamsHelper();
            $layoutPathEmail = $objVDParams->getParamsByTag('Alerta_SendMail_NewMsgHistFromManager_LayoutPath');

            if((string)$layoutPathEmail=='') {
                // Se não carregar o caminho do layout sai com erro...
                echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
                exit();
            }
            $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_TITULO');
            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_INTRO',$IntroName);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_NEWMSG_CORPO',$refAlerta);
            $BODY_SECONDLINE    = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_NEWMSG_CORPO3');
            $explodeMSG = explode('<br/>', $msg);

            if (strpos($explodeMSG[0], 'Em espera') !== false) {
                $BODY_THIRDLINE = '<b>Novo estado da ocorrência:</b> <br> <span style="background:#bd4e2e; color:#fff;"><span style="color:#bd4e2e">_</span>Em espera<span style="color:#bd4e2e">_</span></span>';
            } else if(strpos($explodeMSG[0], 'Em análise') !== false){
                $BODY_THIRDLINE = '<b>Novo estado da ocorrência:</b> <br> <span style="background:#d4c744; color:#fff;"><span style="color:#d4c744">_</span>Em análise<span style="color:#d4c744">_</span></span>';
            } else if(strpos($explodeMSG[0], 'Concluído') !== false){
                $BODY_THIRDLINE = '<b>Novo estado da ocorrência:</b> <br> <span style="background:#4b744f; color:#fff;"><span style="color:#4b744f">_</span>Concluído<span style="color:#4b744f">_</span></span>';
            } else {
                $BODY_THIRDLINE = $explodeMSG[0];
            }

            if(!empty($explodeMSG[1]) && $explodeMSG[1] != ' '){
                $BODY_DESCRITIVO  = '<b>Mensagem:</b> <br>' . $explodeMSG[1];
            } else {
                $BODY_DESCRITIVO = '';
            }


            $BODY_COPYLINK    = JText::sprintf($LinkCopyright);
            $BODY_COPYNAME    = JText::sprintf($copyrightAPP);
            $BODY_COPYTELE    = JText::sprintf($contactoTelefCopyrightEmail);
            $BODY_COPYMAIL    = JText::sprintf($emailCopyrightGeral);
            $BODY_COPYDOM    = JText::sprintf($dominioMunicipio);


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_SECONDLINE",$BODY_SECONDLINE, $body);
            $body      = str_replace("%BODY_THIRDLINE",$BODY_THIRDLINE, $body);
            $body      = str_replace("%BODY_DESCRITIVO",$BODY_DESCRITIVO, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYTELE",$BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYMAIL",$BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);

            // Send the password reset request email.
            $newEmail = JFactory::getMailer();
            $newEmail->Encoding = 'base64';
            $newEmail->isHtml(true);
            $newEmail->setBody($body);
            $newEmail->addReplyTo( $data['mailfrom']);
            $newEmail->setSender( $data['mailfrom']);
            $newEmail->setFrom( $data['fromname']);
            if(!is_array($TO)) $TO = array();
            foreach ($TO as $chvR => $valR) {
                if((string)$valR['email']!='') $newEmail->addRecipient($valR['email'], $valR['name']);
            }
            if(!is_array($BCC)) $BCC = array();
            foreach ($BCC as $chvB => $valB) {
                if((string)$valB['email']!='') $newEmail->addBcc($valB['email'], $valB['name']);
            }
            $newEmail->setSubject($data['sitename']);

            $logosendmailImage = $objVDParams->getParamsByTag('logosendmailalerta');
            $newEmail->AddEmbeddedImage(JPATH_ROOT.$logosendmailImage, "banner", "Logo");

            $return = $newEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }


        public static function getAlertaAbertosMaps4Plugin ($setLimit)
        {

            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.Id_alerta as id_alerta', 'a.latitude as latit','a.longitude as longit','a.codOco as ref'
                , 'b.name_EN as cat_en', 'b.name_PT as cat_pt','c.estado_PT as estado_pt','c.estado_EN as estado_en', 'c.id_estado as idestado') )
                ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                ->join('LEFT', '#__virtualdesk_alerta_estados   AS c  ON c.id_estado = a.estado' )
                ->from("#__virtualdesk_alerta as a ")
                ->order(' a.data_alteracao DESC ' . $setLimitSQL)
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {

                // Acrescenta o ATUAL para surgir na lista
                if (empty($lang) or ($lang = 'pt_PT')) {
                    $EstadoName = $row->estado_pt;
                    $CatName = $row->cat_pt;
                } else {
                    $EstadoName = $row->estado_en;
                    $CatName = $row->cat_en;
                }
                $response[] = array(
                    'id'   => $row->id_alerta,
                    'idestado' => $row->idestado,
                    'estado' => $EstadoName,
                    'cat' => $CatName,
                    'lat' => $row->latit,
                    'long' => $row->longit,
                    'ref' => $row->ref,
                );
            }
            return($response);

        }



        /* Carrega Mapa com alertas para serem visualizados pelo MANAGER */
        public static function getAlertaAbertosMaps4Manager ($setLimit)
        {

            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            try
            {
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('a.Id_alerta as id_alerta', 'a.latitude as latit','a.longitude as longit','a.codOco as ref'
                    , 'b.name_EN as cat_en', 'b.name_PT as cat_pt','c.estado_PT as estado_pt','c.estado_EN as estado_en', 'c.id_estado as idestado') )
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_estados   AS c  ON c.id_estado = a.estado' )
                    ->from("#__virtualdesk_alerta as a ")
                    ->order(' a.data_alteracao DESC ' . $setLimitSQL)
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {

                        // Acrescenta o ATUAL para surgir na lista
                        if (empty($lang) or ($lang = 'pt_PT')) {
                            $EstadoName = $row->estado_pt;
                            $CatName = $row->cat_pt;
                        } else {
                            $EstadoName = $row->estado_en;
                            $CatName = $row->cat_en;
                        }
                        $response[] = array(
                            'id'   => $row->id_alerta,
                            'idestado' => $row->idestado,
                            'estado' => $EstadoName,
                            'cat' => $CatName,
                            'lat' => $row->latit,
                            'long' => $row->longit,
                            'ref' => $row->ref,
                        );
                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega Mapa com alertas para serem visualizados pelo USER */
        public static function getAlertaAbertosMaps4User ($setLimit)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4users');

            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('a.Id_alerta as id_alerta', 'a.latitude as latit','a.longitude as longit','a.codOco as ref'
                    , 'b.name_EN as cat_en', 'b.name_PT as cat_pt','c.estado_PT as estado_pt','c.estado_EN as estado_en', 'c.id_estado as idestado') )
                    ->join('LEFT', '#__virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria')
                    ->join('LEFT', '#__virtualdesk_alerta_estados   AS c  ON c.id_estado = a.estado' )
                    ->from("#__virtualdesk_alerta as a  ")
                    ->where(" a.nif=".$UserSessionNIF )
                    ->order(' a.data_alteracao DESC ' . $setLimitSQL)
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {

                    // Acrescenta o ATUAL para surgir na lista
                    if (empty($lang) or ($lang = 'pt_PT')) {
                        $EstadoName = $row->estado_pt;
                        $CatName = $row->cat_pt;
                    } else {
                        $EstadoName = $row->estado_en;
                        $CatName = $row->cat_en;
                    }
                    $response[] = array(
                        'id'   => $row->id_alerta,
                        'idestado' => $row->idestado,
                        'estado' => $EstadoName,
                        'cat' => $CatName,
                        'lat' => $row->latit,
                        'long' => $row->longit,
                        'ref' => $row->ref,
                    );
                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Carrega lista com IDs dos alertas com reencaminhamento para o USER ou para os GRUPO do user atual da sessão*/
        public static function getReencaminharIDs_ByUserAndGroups_4Manager ()
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( (int)$UserJoomlaID<=0 )  return false;

            // User VD ID
            $UserVDId = VirtualDeskSiteUserHelper::getUserVDIdFromJoomlaID ($UserJoomlaID);

            // Grupos Ids do Utilizador atual
            $GruposIds = VirtualDeskSitePermAdminHelper::getUserGroupsOnlyIDList (null,$UserVDId);

            try
            {
                $db = JFactory::getDBO();
                $q1 = $db->getQuery(true);
                $q2 = $db->getQuery(true);

                $q1->select(array('id_alerta'))
                    ->from("#__virtualdesk_alerta_encaminhar_user_hist")
                    ->where(' (id_user='.$db->escape($UserVDId).' and atual=1)');

                if(!empty($GruposIds)) {
                    $q2->select(array('id_alerta'))
                        ->from("#__virtualdesk_alerta_encaminhar_group_hist")
                        ->where(' (id_group in ('.implode(',',$GruposIds).') and atual=1)');
                    $q1->union($q2);

                    $dataReturn = $db->setQuery($q1)->loadObjectList();
                }
                else {
                    $dataReturn = $db->setQuery($q1)->loadObjectList();
                }

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com IDs dos alertas com TAREFAS não terminadas para o USER ou para os GRUPO do user atual da sessão*/
        public static function getTarefasIDs_ByUserAndGroups_4Manager ()
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( (int)$UserJoomlaID<=0 )  return false;

            // User VD ID
            $UserVDId = VirtualDeskSiteUserHelper::getUserVDIdFromJoomlaID ($UserJoomlaID);

            // Grupos Ids do Utilizador atual
            $GruposIds = VirtualDeskSitePermAdminHelper::getUserGroupsOnlyIDList (null,$UserVDId);

            try
            {
                $db = JFactory::getDBO();
                $q1 = $db->getQuery(true);
                $q2 = $db->getQuery(true);

                $q1->select(array('id_tarefa'))
                    ->from("#__virtualdesk_tarefa_user")
                    ->where(' id_user='.$db->escape($UserVDId) );
                if(!empty($GruposIds)) {
                    $q2->select(array('id_tarefa'))
                        ->from("#__virtualdesk_tarefa_group")
                        ->where(' id_group in ('.implode(',',$GruposIds).') ');
                    $q1->union($q2);

                    $dataReturnTrf = $db->setQuery($q1)->loadObjectList();
                }
                else {
                    $dataReturnTrf = $db->setQuery($q1)->loadObjectList();
                }

                if(empty($dataReturnTrf)) return(false);
                $arTarefasIdsToFilter = array();
                foreach ($dataReturnTrf as $valRet) {
                    $arTarefasIdsToFilter[] = $valRet->id_tarefa;
                }
                if(empty($arTarefasIdsToFilter)) return(false);
                $stringTarefasIdsToFilter = implode(',',$arTarefasIdsToFilter);

                // Só retorna agora as tarefas com os Id processos que não estão fechados e são do ALERTA
                $IdEstadoConcluido     = VirtualDeskSiteTarefaHelper::getEstadoIdConcluido();
                $IdEstadoAnulado       = VirtualDeskSiteTarefaHelper::getEstadoIdAnulado();
                $idTarefaTipoProcesso  = VirtualDeskSiteTarefaHelper::getTipoProcessoIdByTag('alerta');
                $db->setQuery($db->getQuery(true)
                    ->select(array('c.id_processo as id_alerta'))
                    ->join('LEFT', '#__virtualdesk_tarefa_estados AS b ON b.id = a.id_tarefa_estado')
                    ->join('LEFT', '#__virtualdesk_tarefa_processo AS c ON c.id_tarefa = a.id')
                    ->from("#__virtualdesk_tarefa as a")
                    ->where( 'a.id in ('.$stringTarefasIdsToFilter.') and a.id_tarefa_estado not in ('.$IdEstadoConcluido.','.$IdEstadoAnulado.') and a.id_tarefa_tipoprocesso=' . $db->escape($idTarefaTipoProcesso) ));
                $dataReturn = $db->loadObjectList();

                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Retorna Id Concluido*/
        public static function getEstadoIdConcluido ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado'))
                    ->from("#__virtualdesk_alerta_estados as a")
                    ->where(" a.bitEnd=1 ")
                );
                $response = $db->loadObject();
                return($response->id_estado);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        /* Retorna Id Inicio*/
        public static function getEstadoIdInicio ()
        {
            /*
            * Check PERMISSÕES
            */
            /*$objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess($TipoProcessoTag);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                $db = JFactory::getDBO();

                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado'))
                    ->from("#__virtualdesk_alerta_estados as a")
                    ->where(" a.bitStart=1 ")
                );
                $response = $db->loadObject();
                return($response->id_estado);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Limpa os dados na sessão "users states" do joomla */
        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.alerta.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.alerta.data', null);
            $app->setUserState('com_virtualdesk.addnewconfcategoria4manager.alerta.data', null);
            $app->setUserState('com_virtualdesk.addnewconfsubcategoria4manager.alerta.data', null);
            $app->setUserState('com_virtualdesk.edit4user.alerta.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.alerta.data', null);
            $app->setUserState('com_virtualdesk.editconfcategoria4manager.alerta.data', null);
            $app->setUserState('com_virtualdesk.editconfsubcategoria4manager.alerta.data', null);
        }






    }
?>