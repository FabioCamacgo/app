<?php

/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


class VirtualDeskSiteAlojamentoHelper
{

    public static function getFreguesia($concelho)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia, concelho_id'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
            ->order('freguesia ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function excludeFreguesia($concelho, $freguesia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia, concelho_id'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
            ->where($db->quoteName('id') . "!='" . $db->escape($freguesia) . "'")
            ->order('freguesia ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getFregName($freguesia)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('freguesia')
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getTipologia()
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, tipologia'))
            ->from("#__virtualdesk_Empresas_Tipo_Alojamento")
            ->order('tipologia ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function excludeTipologia($tipologia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, tipologia'))
            ->from("#__virtualdesk_Empresas_Tipo_Alojamento")
            ->where($db->quoteName('id') . "!='" . $db->escape($tipologia) . "'")
            ->order('tipologia ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getTipName($tipologia)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('tipologia')
            ->from("#__virtualdesk_Empresas_Tipo_Alojamento")
            ->where($db->quoteName('id') . "='" . $db->escape($tipologia) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getAlojamentos($categoria, $subcategoria, $state){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.id, a.referencia, a.designacao, b.freguesia as freguesia, c.tipologia as tipologia, a.nQuartos_Alojamento, a.nUtentes_Alojamento, a.nCamas_Alojamento'))
            ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS b ON b.id = a.freguesia')
            ->join('LEFT', '#__virtualdesk_Empresas_Tipo_Alojamento AS c ON c.id = a.tipologia_Alojamento')
            ->from("#__virtualdesk_Empresas as a")
            ->where($db->quoteName('categoria') . "='" . $db->escape($categoria) . "'")
            ->where($db->quoteName('subcategoria') . "='" . $db->escape($subcategoria) . "'")
            ->where($db->quoteName('estado') . "='" . $db->escape($state) . "'")
            ->order('designacao ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getImgAL($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
            ->from("#__virtualdesk_Empresas_Files")
            ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getAlojamentosFiltered($categoria, $subcategoria, $state, $nomePost, $tipo, $freg){

        $db = JFactory::getDBO();

        $where = $db->quoteName('a.categoria') . "='" . $db->escape($categoria) . "'";
        $where .= 'AND' . $db->quoteName('a.subcategoria') . "='" . $db->escape($subcategoria) . "'";
        $where .= 'AND' . $db->quoteName('a.estado') . "='" . $db->escape($state) . "'";

        if(!empty($tipo)){
            $where .= 'AND' . $db->quoteName('a.tipologia_Alojamento') . "='" . $db->escape($tipo) . "'";
        }

        if(!empty($freg)){
            $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freg) . "'";
        }

        if(!empty($nomePost)){
            $nome2 = '%' . $nomePost . '%';

            $where2 = $db->quoteName('a.designacao') . "LIKE '" . $db->escape($nome2) . "'";
            $where3 = $db->quoteName('a.nome') . "LIKE '" . $db->escape($nome2) . "'";
            $where4 = $db->quoteName('a.nif') . "LIKE '" . $db->escape($nome2) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.designacao, b.freguesia as freguesia, c.tipologia as tipologia, a.nQuartos_Alojamento, a.nUtentes_Alojamento, a.nCamas_Alojamento'))
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS b ON b.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_Empresas_Tipo_Alojamento AS c ON c.id = a.tipologia_Alojamento')
                ->from("#__virtualdesk_Empresas as a")
                ->where($where . 'AND' . $where2)
                ->orWhere($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->order('designacao ASC')
            );
        } else {

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.designacao, b.freguesia as freguesia, c.tipologia as tipologia, a.nQuartos_Alojamento, a.nUtentes_Alojamento, a.nCamas_Alojamento'))
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS b ON b.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_Empresas_Tipo_Alojamento AS c ON c.id = a.tipologia_Alojamento')
                ->from("#__virtualdesk_Empresas as a")
                ->where($where)
                ->order('designacao ASC')
            );
        }

        $data = $db->loadAssocList();
        return ($data);
    }


    public static function validaNif($fiscalid){
        $nifTrim=trim($fiscalid);
        $ignoreFirst=true;
        if (!is_numeric($nifTrim) || strlen($nifTrim)!=9) {
            return (1);
        } else {
            $nifSplit=str_split($nifTrim);
            if (
                in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9))
                ||
                $ignoreFirst
            ) {
                $checkDigit=0;
                for($i=0; $i<8; $i++) {
                    $checkDigit+=$nifSplit[$i]*(10-$i-1);
                }
                $checkDigit=11-($checkDigit % 11);
                if($checkDigit>=10) $checkDigit=0;
                if ($checkDigit==$nifSplit[8]) {

                } else {
                    return (1);
                }
            } else {
                return (1);
            }
        }
    }

    public static function validaTelefone($telefone){
        $TelefoneSplit=str_split($telefone);
        if(empty($telefone)){
            return (1);
        } else if(!is_numeric($telefone) || strlen($telefone) != 9){
            return (2);
        } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
            return (2);
        } else if($TelefoneSplit[0] == 9){
            if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
                return (2);
            } else {
                return (0);
            }
        } else {
            return (0);
        }
    }

    public static function validaNome($nome){
        if(empty($nome)){
            return (1);
        }else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$nome)) {
            return (2);
        } else {
            return (0);
        }
    }

    public static function validaEmail($email, $email2){
        if(empty($email)){
            return (1);
        } else if(empty($email2) || $email != $email2){
            return (2);
        } else {
            return (0);
        }
    }

    public static function validaNumber($number){
        if(empty($number)){
            return (1);
        } else if (!is_numeric($number)) {
            return (2);
        } else {
            return (0);
        }
    }

    /*Gera código aleatório para a referência da ocorrencia*/
    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16);
    }

    /*Verifica de a referencia da ocorrencia existe*/
    public static function CheckReferencia($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('referencia')
            ->from("#__virtualdesk_AlojamentoLocal_cleanSafe")
            ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function saveNewForm($referencia, $name, $number, $personType, $freguesia, $nameReq, $emailReq, $nif, $telefoneReq){
        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);
        $columns = array('referencia','nome_alojamento','numero_registo','tipo_requerente','freguesia','nome_requerente','email','nif','telefone');
        $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($name)), $db->quote($db->escape($number)), $db->quote($db->escape($personType)), $db->quote($db->escape($freguesia)), $db->quote($db->escape($nameReq)), $db->quote($db->escape($emailReq)), $db->quote($db->escape($nif)), $db->quote($db->escape($telefoneReq)));
        $query
            ->insert($db->quoteName('#__virtualdesk_AlojamentoLocal_cleanSafe'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean) $db->execute();

        return($result);

    }

    public static function getReqType($personType){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('tipo_requerente')
            ->from("#__virtualdesk_AlojamentoLocal_cleanSafe_ReqType")
            ->where($db->quoteName('id') . "='" . $db->escape($personType) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function SendEmailAdmin($referencia, $name, $numeroAL, $reqType, $fregName, $nameReq, $emailReq, $nif, $telefoneReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio){

        $config = JFactory::getConfig();

        /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
        $obPlg      = new VirtualDeskSitePluginsHelper();
        $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('cleanSafe','email2admin');

        if((string)$layoutPathEmail=='') {
            echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
            exit();
        }

        $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

        $layoutPathPDF = $obPlg->getPluginLayoutAtivePath('cleanSafe','pdf2admin');

        if((string)$layoutPathPDF=='') {
            echo (JText::_('COM_VIRTUALDESK_LOAD_PDF_LAYOUT_ERROR'));
            exit();
        }

        require_once(JPATH_ROOT.$layoutPathPDF);

        //$pdf = new VirtualDeskPDFHelper();

        $obParam      = new VirtualDeskSiteParamsHelper();

        $emailAdmin = $obParam->getParamsByTag('mailAdminCleanSafe');

        //$pdf->pdfCleanSafeAdmin($referencia, $name, $numeroAL, $reqType, $fregName, $nameReq, $emailReq, $nif, $telefoneReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio);

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE                     = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL_TITULO',$nomeMunicipio);
        $BODY_TITLE2                    = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL_TITULO',$nomeMunicipio);
        $BODY_MESSAGE                   = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAILADMIN_CORPO', $nomeMunicipio, $referencia);
        $BODY_MESSAGE2                  = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAILADMIN_2CORPO2');
        $BODY_SECTION1_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_IDENTIFICACAO_AL');
        $BODY_SECTION2_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_IDENTIFICACAO_REQUERENTE');
        $BODY_NAMEAL_TITLE              = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NAME');
        $BODY_NAMEAL_VALUE              = JText::sprintf($name);
        $BODY_NUMBERAL_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NUMBER');
        $BODY_NUMBERAL_VALUE            = JText::sprintf($numeroAL);
        $BODY_REQTYPE_TITLE             = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_PERSONTYPE');
        $BODY_REQTYPE_VALUE             = JText::sprintf($reqType);
        $BODY_FREGUESIA_TITLE           = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_FREGUESIA');
        $BODY_FREGUESIA_VALUE           = JText::sprintf($fregName);
        $BODY_NAMEREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NOMEREQUERENTE');
        $BODY_NAMEREQ_VALUE             = JText::sprintf($nameReq);
        $BODY_MAILREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL');
        $BODY_MAILREQ_VALUE             = JText::sprintf($emailReq);
        $BODY_NIFREQ_TITLE              = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_FISCALID');
        $BODY_NIFREQ_VALUE              = JText::sprintf($nif);
        $BODY_PHONEREQ_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_TELEFONE');
        $BODY_PHONEREQ_VALUE            = JText::sprintf($telefoneReq);
        $BODY_EMAILNAME_VALUE           = JText::sprintf($emailCopyrightGeral);
        $BODY_TELEFNAME_VALUE           = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_DOMINIOMUNICIPIO_VALUE    = JText::sprintf($dominioMunicipio);
        $BODY_LINKMUNICIPIO_VALUE       = JText::sprintf($LinkCopyright);
        $BODY_COPYRIGHT_VALUE           = JText::sprintf($copyrightAPP);


        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_2MESSAGE",$BODY_MESSAGE2, $body);
        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
        $body      = str_replace("%BODY_SECTION1_TITLE",$BODY_SECTION1_TITLE, $body);
        $body      = str_replace("%BODY_SECTION2_TITLE",$BODY_SECTION2_TITLE, $body);
        $body      = str_replace("%BODY_NAMEAL_TITLE",$BODY_NAMEAL_TITLE, $body);
        $body      = str_replace("%BODY_NAMEAL_VALUE",$BODY_NAMEAL_VALUE, $body);
        $body      = str_replace("%BODY_NUMBERAL_TITLE",$BODY_NUMBERAL_TITLE, $body);
        $body      = str_replace("%BODY_NUMBERAL_VALUE",$BODY_NUMBERAL_VALUE, $body);
        $body      = str_replace("%BODY_REQTYPE_TITLE",$BODY_REQTYPE_TITLE, $body);
        $body      = str_replace("%BODY_REQTYPE_VALUE",$BODY_REQTYPE_VALUE, $body);
        $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
        $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
        $body      = str_replace("%BODY_NAMEREQ_TITLE",$BODY_NAMEREQ_TITLE, $body);
        $body      = str_replace("%BODY_NAMEREQ_VALUE",$BODY_NAMEREQ_VALUE, $body);
        $body      = str_replace("%BODY_MAILREQ_TITLE",$BODY_MAILREQ_TITLE, $body);
        $body      = str_replace("%BODY_MAILREQ_VALUE",$BODY_MAILREQ_VALUE, $body);
        $body      = str_replace("%BODY_NIFREQ_TITLE",$BODY_NIFREQ_TITLE, $body);
        $body      = str_replace("%BODY_NIFREQ_VALUE",$BODY_NIFREQ_VALUE, $body);
        $body      = str_replace("%BODY_PHONEREQ_TITLE",$BODY_PHONEREQ_TITLE, $body);
        $body      = str_replace("%BODY_PHONEREQ_VALUE",$BODY_PHONEREQ_VALUE, $body);
        $body      = str_replace("%BODY_EMAILNAME_VALUE",$BODY_EMAILNAME_VALUE, $body);
        $body      = str_replace("%BODY_TELEFNAME_VALUE",$BODY_TELEFNAME_VALUE, $body);
        $body      = str_replace("%BODY_DOMINIOMUNICIPIO_VALUE",$BODY_DOMINIOMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_LINKMUNICIPIO_VALUE",$BODY_LINKMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHT_VALUE",$BODY_COPYRIGHT_VALUE, $body);


        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($emailAdmin);
        $newActivationEmail->setSubject($data['sitename']);
        $logosendmailImage = $obParam->getParamsByTag('logoMailCleanSafe');
        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.'/'.$logosendmailImage, "banner", "Logo");

        /*$pdf->SetIdProcesso ($referencia);
        $pdf->SetTagProcesso ('Clean&Safe');
        $resSave = $pdf->saveFile();
        if($resSave==true) {
            $resFilepath = $pdf->resFileSaved ['filepath'];
            $resFilename = $pdf->resFileSaved ['filename'];
            $newActivationEmail->addAttachment(JPATH_BASE.'/'.$resFilepath.'/'.$resFilename);
        }*/

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }


    public static function SendEmailUser($referencia, $name, $numeroAL, $reqType, $fregName, $nameReq, $emailReq, $nif, $telefoneReq, $codPostalMunicipio, $moradaMunicipio, $emailCopyrightGeral, $contactoTelefCopyrightEmail, $dominioMunicipio, $LinkCopyright, $copyrightAPP, $nomeMunicipio){

        $config = JFactory::getConfig();

        /* Escolhe os layout ativos conforme o tipo de layouts que queremos carregar */
        $obPlg      = new VirtualDeskSitePluginsHelper();
        $layoutPathEmail = $obPlg->getPluginLayoutAtivePath('cleanSafe','email2user');

        if((string)$layoutPathEmail=='') {
            echo (JText::_('COM_VIRTUALDESK_LOAD_SENDMAIL_LAYOUT_ERROR'));
            exit();
        }

        $emailHTML = file_get_contents(JPATH_ROOT.$layoutPathEmail);

        $obParam      = new VirtualDeskSiteParamsHelper();

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE                     = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL_TITULO',$nomeMunicipio);
        $BODY_TITLE2                    = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL_TITULO',$nomeMunicipio);
        $BODY_MESSAGE                   = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAILUSER_CORPO',$nomeMunicipio);
        $BODY_MESSAGE2                  = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAILUSER_CORPO2');
        $BODY_MESSAGE3                  = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAILADMIN_2CORPO2');
        $BODY_SECTION1_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_IDENTIFICACAO_AL');
        $BODY_SECTION2_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_IDENTIFICACAO_REQUERENTE');
        $BODY_NAMEAL_TITLE              = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NAME');
        $BODY_NAMEAL_VALUE              = JText::sprintf($name);
        $BODY_NUMBERAL_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NUMBER');
        $BODY_NUMBERAL_VALUE            = JText::sprintf($numeroAL);
        $BODY_REQTYPE_TITLE             = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_PERSONTYPE');
        $BODY_REQTYPE_VALUE             = JText::sprintf($reqType);
        $BODY_FREGUESIA_TITLE           = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_FREGUESIA');
        $BODY_FREGUESIA_VALUE           = JText::sprintf($fregName);
        $BODY_NAMEREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_NOMEREQUERENTE');
        $BODY_NAMEREQ_VALUE             = JText::sprintf($nameReq);
        $BODY_MAILREQ_TITLE             = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_EMAIL');
        $BODY_MAILREQ_VALUE             = JText::sprintf($emailReq);
        $BODY_NIFREQ_TITLE              = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_FISCALID');
        $BODY_NIFREQ_VALUE              = JText::sprintf($nif);
        $BODY_PHONEREQ_TITLE            = JText::sprintf('COM_VIRTUALDESK_ALOJAMENTOLOCAL_TELEFONE');
        $BODY_PHONEREQ_VALUE            = JText::sprintf($telefoneReq);
        $BODY_EMAILNAME_VALUE           = JText::sprintf($emailCopyrightGeral);
        $BODY_TELEFNAME_VALUE           = JText::sprintf($contactoTelefCopyrightEmail);
        $BODY_DOMINIOMUNICIPIO_VALUE    = JText::sprintf($dominioMunicipio);
        $BODY_LINKMUNICIPIO_VALUE       = JText::sprintf($LinkCopyright);
        $BODY_COPYRIGHT_VALUE           = JText::sprintf($copyrightAPP);


        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_2MESSAGE",$BODY_MESSAGE2, $body);
        $body      = str_replace("%BODY_3MESSAGE",$BODY_MESSAGE3, $body);
        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
        $body      = str_replace("%BODY_SECTION1_TITLE",$BODY_SECTION1_TITLE, $body);
        $body      = str_replace("%BODY_SECTION2_TITLE",$BODY_SECTION2_TITLE, $body);
        $body      = str_replace("%BODY_NAMEAL_TITLE",$BODY_NAMEAL_TITLE, $body);
        $body      = str_replace("%BODY_NAMEAL_VALUE",$BODY_NAMEAL_VALUE, $body);
        $body      = str_replace("%BODY_NUMBERAL_TITLE",$BODY_NUMBERAL_TITLE, $body);
        $body      = str_replace("%BODY_NUMBERAL_VALUE",$BODY_NUMBERAL_VALUE, $body);
        $body      = str_replace("%BODY_REQTYPE_TITLE",$BODY_REQTYPE_TITLE, $body);
        $body      = str_replace("%BODY_REQTYPE_VALUE",$BODY_REQTYPE_VALUE, $body);
        $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
        $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
        $body      = str_replace("%BODY_NAMEREQ_TITLE",$BODY_NAMEREQ_TITLE, $body);
        $body      = str_replace("%BODY_NAMEREQ_VALUE",$BODY_NAMEREQ_VALUE, $body);
        $body      = str_replace("%BODY_MAILREQ_TITLE",$BODY_MAILREQ_TITLE, $body);
        $body      = str_replace("%BODY_MAILREQ_VALUE",$BODY_MAILREQ_VALUE, $body);
        $body      = str_replace("%BODY_NIFREQ_TITLE",$BODY_NIFREQ_TITLE, $body);
        $body      = str_replace("%BODY_NIFREQ_VALUE",$BODY_NIFREQ_VALUE, $body);
        $body      = str_replace("%BODY_PHONEREQ_TITLE",$BODY_PHONEREQ_TITLE, $body);
        $body      = str_replace("%BODY_PHONEREQ_VALUE",$BODY_PHONEREQ_VALUE, $body);
        $body      = str_replace("%BODY_EMAILNAME_VALUE",$BODY_EMAILNAME_VALUE, $body);
        $body      = str_replace("%BODY_TELEFNAME_VALUE",$BODY_TELEFNAME_VALUE, $body);
        $body      = str_replace("%BODY_DOMINIOMUNICIPIO_VALUE",$BODY_DOMINIOMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_LINKMUNICIPIO_VALUE",$BODY_LINKMUNICIPIO_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHT_VALUE",$BODY_COPYRIGHT_VALUE, $body);


        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($emailReq);
        $newActivationEmail->setSubject($data['sitename']);
        $logosendmailImage = $obParam->getParamsByTag('logoMailCleanSafe');
        $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.'/'.$logosendmailImage, "banner", "Logo");

        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }

}

?>