<?php

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');


class VirtualDeskSiteAssociativismoHelper
{

    public static function getFreguesia($concelho){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id_freguesia, freguesia, concelho_id'))
            ->from("#__virtualdesk_Associativismo_Freguesia")
            ->where($db->quoteName('concelho_id') . '=' . $db->escape($concelho))
            ->order('freguesia')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getFregSelect($freguesia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('freguesia')
            ->from("#__virtualdesk_Associativismo_Freguesia")
            ->where($db->quoteName('id_freguesia') . '=' . $db->escape($freguesia))
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludeFreguesia($concelho, $freguesia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id_freguesia, freguesia, concelho_id'))
            ->from("#__virtualdesk_Associativismo_Freguesia")
            ->where($db->quoteName('concelho_id') . '=' . $db->escape($concelho))
            ->where($db->quoteName('id_freguesia') . '!=' . $db->escape($freguesia))
            ->order('freguesia')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getAreas(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, area'))
            ->from("#__virtualdesk_Associativismo_AreaIntervencao")
            ->order('area')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getAreaAtuacaoName($areaAtuacao){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('area')
            ->from("#__virtualdesk_Associativismo_AreaIntervencao")
            ->where($db->quoteName('id') . '=' . $db->escape($areaAtuacao))
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludeAreaAtuacao($areaAtuacao){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, area'))
            ->from("#__virtualdesk_Associativismo_AreaIntervencao")
            ->where($db->quoteName('id') . '!=' . $db->escape($areaAtuacao))
            ->order('area')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getSubAreasAtuacao($areaAtuacao){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, subarea'))
            ->from("#__virtualdesk_Associativismo_SubAreaIntervencao")
            ->where($db->quoteName('id_area') . '=' . $db->escape($areaAtuacao))
            ->order('subarea')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getSubAreasAtuacaoDepend($idwebsitelist,$onAjaxVD=0)
    {

        if (empty($idwebsitelist)) return false;


        if($onAjaxVD=0) {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, subarea, id_area'))
                ->from("#__virtualdesk_Associativismo_SubAreaIntervencao")
                ->where($db->quoteName('id_area') . '=' . $db->escape($idwebsitelist))
                ->order('subarea ASC')
            );
            $data = $db->loadAssocList();
        }
        else{
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, subarea as name, id_area'))
                ->from("#__virtualdesk_Associativismo_SubAreaIntervencao")
                ->where($db->quoteName('id_area') . '=' . $db->escape($idwebsitelist))
                ->order('name ASC')
            );
            $data = $db->loadAssocList();
        }

        return ($data);
    }


    public static function getSubAreaName($subAreaAtuacao){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('subarea')
            ->from("#__virtualdesk_Associativismo_SubAreaIntervencao")
            ->where($db->quoteName('id') . '=' . $db->escape($subAreaAtuacao))
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludeSubArea($areaAtuacao, $subAreaAtuacao){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, subarea, id_area'))
            ->from("#__virtualdesk_Associativismo_SubAreaIntervencao")
            ->where($db->quoteName('id_area') . "='" . $db->escape($areaAtuacao) . "'")
            ->where($db->quoteName('id') . "!='" . $db->escape($subAreaAtuacao) . "'")
            ->order('subarea ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getAssocCultura($fiscalid){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_Cultura_Users")
            ->where($db->quoteName('nif') . '=' . $db->escape($fiscalid))
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function ConcatAreas($areaCulturaHide, $areaDesportoHide, $areaSocialHide, $areaEducacaoHide){
        if($areaCulturaHide == 1){
            $concaArea = $areaCulturaHide;
            if($areaDesportoHide == 2){$concaArea .= ',' . $areaDesportoHide;}
            if($areaSocialHide == 3){$concaArea .= ',' . $areaSocialHide;}
            if($areaEducacaoHide == 4){$concaArea .= ',' . $areaEducacaoHide;}
        } else if($areaDesportoHide == 2){
            $concaArea = $areaDesportoHide;
            if($areaSocialHide == 3){$concaArea .= ',' . $areaSocialHide;}
            if($areaEducacaoHide == 4){$concaArea .= ',' . $areaEducacaoHide;}
        } else if($areaSocialHide == 3){
            $concaArea = $areaSocialHide;
            if($areaEducacaoHide == 4){$concaArea .= ',' . $areaEducacaoHide;}
        } else if($areaEducacaoHide == 4){
            $concaArea = $areaEducacaoHide;
        }
        return($concaArea);
    }


    public static function ConcatCultura($folcloreHide, $artesHide, $dancaHide, $feirasHide, $literaturaHide, $musicaHide, $teatroHide){
        if($folcloreHide == 1){
            $concaCult = $folcloreHide;
            if($artesHide == 2){$concaCult .= ',' . $artesHide;}
            if($dancaHide == 3){$concaCult .= ',' . $dancaHide;}
            if($feirasHide == 4){$concaCult .= ',' . $feirasHide;}
            if($literaturaHide == 5){$concaCult .= ',' . $literaturaHide;}
            if($musicaHide == 6){$concaCult .= ',' . $musicaHide;}
            if($teatroHide == 7){$concaCult .= ',' . $teatroHide;}
        } else if($artesHide == 2){
            $concaCult = $artesHide;
            if($dancaHide == 3){$concaCult .= ',' . $dancaHide;}
            if($feirasHide == 4){$concaCult .= ',' . $feirasHide;}
            if($literaturaHide == 5){$concaCult .= ',' . $literaturaHide;}
            if($musicaHide == 6){$concaCult .= ',' . $musicaHide;}
            if($teatroHide == 7){$concaCult .= ',' . $teatroHide;}
        } else if($dancaHide == 3){
            $concaCult = $dancaHide;
            if($feirasHide == 4){$concaCult .= ',' . $feirasHide;}
            if($literaturaHide == 5){$concaCult .= ',' . $literaturaHide;}
            if($musicaHide == 6){$concaCult .= ',' . $musicaHide;}
            if($teatroHide == 7){$concaCult .= ',' . $teatroHide;}
        } else if($feirasHide == 4){
            $concaCult = $feirasHide;
            if($literaturaHide == 5){$concaCult .= ',' . $literaturaHide;}
            if($musicaHide == 6){$concaCult .= ',' . $musicaHide;}
            if($teatroHide == 7){$concaCult .= ',' . $teatroHide;}
        } else if($literaturaHide == 5){
            $concaCult = $literaturaHide;
            if($musicaHide == 6){$concaCult .= ',' . $musicaHide;}
            if($teatroHide == 7){$concaCult .= ',' . $teatroHide;}
        } else if($musicaHide == 6){
            $concaCult = $musicaHide;
            if($teatroHide == 7){$concaCult .= ',' . $teatroHide;}
        } else if($teatroHide == 7){
            $concaCult = $teatroHide;
        }

        return ($concaCult);
    }


    public static function ConcatDesporto($andebolHide, $artMarciaisHide, $atletismoHide, $automobilismoHide, $basquetHide, $ciclismoHide, $difRedHide, $futebolHide, $hoqueiHide, $ginasticaHide, $natacaoHide, $patinagemHide, $tenMesaHide, $naturHide){
        if($andebolHide == 9){
            $concaDesp = $andebolHide;
            if($artMarciaisHide == 10){$concaDesp .= ',' . $artMarciaisHide;}
            if($atletismoHide == 11){$concaDesp .= ',' . $atletismoHide;}
            if($automobilismoHide == 12){$concaDesp .= ',' . $automobilismoHide;}
            if($basquetHide == 13){$concaDesp .= ',' . $basquetHide;}
            if($ciclismoHide == 14){$concaDesp .= ',' . $ciclismoHide;}
            if($difRedHide == 15){$concaDesp .= ',' . $difRedHide;}
            if($futebolHide == 16){$concaDesp .= ',' . $futebolHide;}
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($artMarciaisHide == 10){
            $concaDesp = $artMarciaisHide;
            if($atletismoHide == 11){$concaDesp .= ',' . $atletismoHide;}
            if($automobilismoHide == 12){$concaDesp .= ',' . $automobilismoHide;}
            if($basquetHide == 13){$concaDesp .= ',' . $basquetHide;}
            if($ciclismoHide == 14){$concaDesp .= ',' . $ciclismoHide;}
            if($difRedHide == 15){$concaDesp .= ',' . $difRedHide;}
            if($futebolHide == 16){$concaDesp .= ',' . $futebolHide;}
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($atletismoHide == 11){
            $concaDesp = $atletismoHide;
            if($automobilismoHide == 12){$concaDesp .= ',' . $automobilismoHide;}
            if($basquetHide == 13){$concaDesp .= ',' . $basquetHide;}
            if($ciclismoHide == 14){$concaDesp .= ',' . $ciclismoHide;}
            if($difRedHide == 15){$concaDesp .= ',' . $difRedHide;}
            if($futebolHide == 16){$concaDesp .= ',' . $futebolHide;}
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($automobilismoHide == 12){
            $concaDesp = $automobilismoHide;
            if($basquetHide == 13){$concaDesp .= ',' . $basquetHide;}
            if($ciclismoHide == 14){$concaDesp .= ',' . $ciclismoHide;}
            if($difRedHide == 15){$concaDesp .= ',' . $difRedHide;}
            if($futebolHide == 16){$concaDesp .= ',' . $futebolHide;}
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($basquetHide == 13){
            $concaDesp = $basquetHide;
            if($ciclismoHide == 14){$concaDesp .= ',' . $ciclismoHide;}
            if($difRedHide == 15){$concaDesp .= ',' . $difRedHide;}
            if($futebolHide == 16){$concaDesp .= ',' . $futebolHide;}
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($ciclismoHide == 14){
            $concaDesp = $ciclismoHide;
            if($difRedHide == 15){$concaDesp .= ',' . $difRedHide;}
            if($futebolHide == 16){$concaDesp .= ',' . $futebolHide;}
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($difRedHide == 15){
            $concaDesp = $difRedHide;
            if($futebolHide == 16){$concaDesp .= ',' . $futebolHide;}
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($futebolHide == 16){
            $concaDesp = $futebolHide;
            if($hoqueiHide == 17){$concaDesp .= ',' . $hoqueiHide;}
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($hoqueiHide == 17){
            $concaDesp = $hoqueiHide;
            if($ginasticaHide == 18){$concaDesp .= ',' . $ginasticaHide;}
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($ginasticaHide == 18){
            $concaDesp = $ginasticaHide;
            if($natacaoHide == 19){$concaDesp .= ',' . $natacaoHide;}
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($natacaoHide == 19){
            $concaDesp = $natacaoHide;
            if($patinagemHide == 20){$concaDesp .= ',' . $patinagemHide;}
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($patinagemHide == 20){
            $concaDesp = $patinagemHide;
            if($tenMesaHide == 21){$concaDesp .= ',' . $tenMesaHide;}
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($tenMesaHide == 21){
            $concaDesp = $tenMesaHide;
            if($naturHide == 22){$concaDesp .= ',' . $naturHide;}
        } else if($naturHide == 22){
            $concaDesp = $naturHide;
        }

        return ($concaDesp);
    }


    public static function ConcatSocial($tercIdadeHide, $portDefHide, $apJuvHide, $refDomHide, $viagensHide){
        if($tercIdadeHide == 24){
            $concaSoc = $tercIdadeHide;
            if($portDefHide == 25){$concaSoc .= ',' . $portDefHide;}
            if($apJuvHide == 26){$concaSoc .= ',' . $apJuvHide;}
            if($refDomHide == 27){$concaSoc .= ',' . $refDomHide;}
            if($viagensHide == 28){$concaSoc .= ',' . $viagensHide;}
        } else if($portDefHide == 25){
            $concaSoc = $portDefHide;
            if($apJuvHide == 26){$concaSoc .= ',' . $apJuvHide;}
            if($refDomHide == 27){$concaSoc .= ',' . $refDomHide;}
            if($viagensHide == 28){$concaSoc .= ',' . $viagensHide;}
        } else if($apJuvHide == 26){
            $concaSoc = $apJuvHide;
            if($refDomHide == 27){$concaSoc .= ',' . $refDomHide;}
            if($viagensHide == 28){$concaSoc .= ',' . $viagensHide;}
        } else if($refDomHide == 27){
            $concaSoc = $refDomHide;
            if($viagensHide == 28){$concaSoc .= ',' . $viagensHide;}
        } else if($viagensHide == 28){
            $concaSoc = $viagensHide;
        }

        return ($concaSoc);
    }


    public static function ConcatEducacao($assPaisHide, $assEstHide, $bibliotecaHide, $atlHide, $intercambioHide){
        if($assPaisHide == 30){
            $concaEdu = $assPaisHide;
            if($assEstHide == 31){$concaEdu .= ',' . $assEstHide;}
            if($bibliotecaHide == 32){$concaEdu .= ',' . $bibliotecaHide;}
            if($atlHide == 33){$concaEdu .= ',' . $atlHide;}
            if($intercambioHide == 34){$concaEdu .= ',' . $intercambioHide;}
        } else if($assEstHide == 31){
            $concaEdu = $assEstHide;
            if($bibliotecaHide == 32){$concaEdu .= ',' . $bibliotecaHide;}
            if($atlHide == 33){$concaEdu .= ',' . $atlHide;}
            if($intercambioHide == 34){$concaEdu .= ',' . $intercambioHide;}
        } else if($bibliotecaHide == 32){
            $concaEdu = $bibliotecaHide;
            if($atlHide == 33){$concaEdu .= ',' . $atlHide;}
            if($intercambioHide == 34){$concaEdu .= ',' . $intercambioHide;}
        } else if($atlHide == 33){
            $concaEdu = $atlHide;
            if($intercambioHide == 34){$concaEdu .= ',' . $intercambioHide;}
        } else if($intercambioHide == 33){
            $concaEdu = $intercambioHide;
        }

        return ($concaEdu);
    }


    public static function SaveUserCultura($fiscalid, $email, $nome, $morada, $freguesia, $codPostal, $telefone, $website, $facebook, $instagram){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $columns = array('nif', 'tipo_User', 'email', 'nome', 'morada', 'concelho', 'freguesia', 'codigo_postal', 'telefone', 'website', 'facebook', 'instagram', 'estado');
        $values = array($db->quote($fiscalid), $db->quote('1'), $db->quote($email), $db->quote($nome), $db->quote($morada), $db->quote('4'), $db->quote($freguesia), $db->quote($codPostal), $db->quote($telefone), $db->quote($website), $db->quote($facebook), $db->quote($instagram), $db->quote('1'));
        $query
            ->insert($db->quoteName('#__virtualdesk_Cultura_Users'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean)$db->execute();

        return ($result);
    }


    public static function SaveUserAssociativismo($referencia, $nome, $apresentacao, $fiscalid, $niss, $morada, $codPostal, $freguesia, $email, $telefone, $website, $facebook, $instagram, $coordenadas, $choseAreas, $choseCultura, $outraCultura, $descCultura, $mailCultura, $choseDesporto, $outraDesporto, $descDesporto, $mailDesporto, $choseSocial, $outraSocial, $descSocial, $mailSocial, $choseEducacao, $outraEducacao, $descEducacao, $mailEducacao){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $columns = array('referencia', 'nome', 'apresentacao', 'nif', 'niss', 'morada', 'codpostal', 'freguesia', 'email', 'telefone', 'website', 'facebook', 'instagram', 'coordenadas', 'areasAtuacao', 'areasCultura', 'outraCultura', 'descCultura', 'mailCultura', 'areasDesporto', 'outraDesporto', 'descDesporto', 'mailDesporto', 'areasSocial', 'outraSocial', 'descSocial', 'mailSocial', 'areasEducacao', 'outraEducacao', 'descEducacao', 'mailEducacao', 'estado');
        $values = array($db->quote($referencia), $db->quote($nome), $db->quote($apresentacao), $db->quote($fiscalid), $db->quote($niss), $db->quote($morada), $db->quote($codPostal), $db->quote($freguesia), $db->quote($email), $db->quote($telefone), $db->quote($website), $db->quote($facebook), $db->quote($instagram), $db->quote($coordenadas), $db->quote($choseAreas), $db->quote($choseCultura), $db->quote($outraCultura), $db->quote($descCultura), $db->quote($mailCultura), $db->quote($choseDesporto), $db->quote($outraDesporto), $db->quote($descDesporto), $db->quote($mailDesporto), $db->quote($choseSocial), $db->quote($outraSocial), $db->quote($descSocial), $db->quote($mailSocial), $db->quote($choseEducacao), $db->quote($outraEducacao), $db->quote($descEducacao), $db->quote($mailEducacao), $db->quote('1'));
        $query
            ->insert($db->quoteName('#__virtualdesk_Associativismo_Associacoes'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean)$db->execute();

        return ($result);
    }


    public static function getCategorias(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, area'))
            ->from("#__virtualdesk_Associativismo_AreaIntervencao")
            ->order('area ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getAssociacoes(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nif')
            ->from("#__virtualdesk_Associativismo_Associacoes")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getEventos(){
        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $CatName = 'catName_PT';
        }
        else {
            $CatName = 'catName_EN';
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.id_evento, a.referencia_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, a.nif, c.freguesia as freguesia, a.data_inicio, a.data_fim'))
            ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
            ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia_evento')
            ->join('INNER', '#__virtualdesk_Associativismo_Associacoes AS d ON d.nif = a.nif')
            ->from("#__virtualdesk_Cultura_Eventos as a")
            ->where($db->quoteName('a.estado') . "='" . $db->escape('2') . "'")
            ->where($db->quoteName('d.estado') . "='" . $db->escape('2') . "'")
            ->order('a.data_inicio DESC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getImgEventos($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
            ->from("#__virtualdesk_Cultura_Files")
            ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getNameMonth($month){
        if($month == '01'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_JAN'));
        } else if($month == '02'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_FEV'));
        } else if($month == '03'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_MAR'));
        } else if($month == '04'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_ABR'));
        } else if($month == '05'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_MAI'));
        } else if($month == '06'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_JUN'));
        } else if($month == '07'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_JUL'));
        } else if($month == '08'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_AGO'));
        } else if($month == '09'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_SET'));
        } else if($month == '10'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_OUT'));
        } else if($month == '11'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_NOV'));
        } else if($month == '12'){
            return(JText::_('COM_VIRTUALDESK_CULTURA_MONTH_DEZ'));
        }
    }


    public static function getIdCat($categoria){
        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $CatName = 'catName_PT';
        }
        else {
            $CatName = 'catName_EN';
        }

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_Cultura_Categoria")
            ->where($db->quoteName($CatName) . "='" . $db->escape($categoria) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getImagemGeral($categoria){
        if($categoria == '1'){
            return('cinema.jpg');
        } else if($categoria == '2'){
            return('conferencia_workshops.jpg');
        } else if($categoria == '3'){
            return('desporto.jpg');
        } else if($categoria == '4'){
            return('exposicoes.jpg');
        } else if($categoria == '5'){
            return('literatura.jpg');
        } else if($categoria == '6'){
            return('musica.jpg');
        } else if($categoria == '7'){
            return('teatro_danca.jpg');
        } else if($categoria == '8'){
            return('tradicoes.jpg');
        } else if($categoria == '9'){
            return('feiras_Certames.jpg');
        } else if($categoria == '10'){
            return('festivais.jpg');
        } else if($categoria == '11'){
            return('lazer.jpg');
        }
    }


    public static function getListaAssociacoes(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.id, a.referencia, a.nome, a.freguesia, a.areasAtuacao, a.estado'))
            ->from("#__virtualdesk_Associativismo_Associacoes as a")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getImgAssoc($referencia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
            ->from("#__virtualdesk_Associativismo_Files")
            ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getAssociacoesFiltered($nomePost, $freguesia){
        $db = JFactory::getDBO();

        $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

        if(!empty($freguesia)){
            $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
        }

        if(!empty($nomePost)){
            $nome2 = '%' . $nomePost . '%';

            $where3 = $db->quoteName('a.nome') . "LIKE '" . $db->escape($nome2) . "'";
            $where4 = $db->quoteName('a.nif') . "LIKE '" . $db->escape($nome2) . "'";

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.nome, a.freguesia, a.areasAtuacao, a.areasCultura, a.areasDesporto, a.areasSocial, a.areasEducacao, a.estado'))
                ->from("#__virtualdesk_Associativismo_Associacoes as a")
                ->where($where . 'AND' . $where3)
                ->orWhere($where . 'AND' . $where4)
                ->order('nome ASC')
            );
        } else {

            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.nome, a.freguesia, a.areasAtuacao, a.areasCultura, a.areasDesporto, a.areasSocial, a.areasEducacao, a.estado'))
                ->from("#__virtualdesk_Associativismo_Associacoes as a")
                ->where($where)
                ->order('nome ASC')
            );
        }

        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getDetalhes($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.referencia, a.nome, a.apresentacao, a.morada, a.codpostal, b.freguesia as freguesia, a.email, a.telefone, a.website, a.facebook, a.instagram, a.coordenadas, a.areasAtuacao, a.areasCultura, a.outraCultura, a.areasDesporto, a.outraDesporto, a.areasSocial, a.outraSocial, a.areasEducacao, a.outraEducacao'))
            ->join('LEFT', '#__virtualdesk_Associativismo_Freguesia AS b ON b.id_freguesia = a.freguesia')
            ->from("#__virtualdesk_Associativismo_Associacoes as a")
            ->where($db->quoteName('a.estado') . "='" . $db->escape('2') . "'")
            ->where($db->quoteName('a.id') . "='" . $db->escape($id) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getSubArea($id){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('subarea')
            ->from("#__virtualdesk_Associativismo_SubAreaIntervencao")
            ->where($db->quoteName('id') . "='" . $db->escape($id) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getAssociacoesTicker(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, nome, areasAtuacao'))
            ->from("#__virtualdesk_Associativismo_Associacoes")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->order('nome ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getPromotores(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id, nome'))
            ->from("#__virtualdesk_Associativismo_Associacoes")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->order('nome')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getPromSelect($promotor){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nome')
            ->from("#__virtualdesk_Associativismo_Associacoes")
            ->where($db->quoteName('id') . '=' . $db->escape($promotor))
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludePromotor($promotor){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( array('id, nome'))
            ->from("#__virtualdesk_Associativismo_Associacoes")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->where($db->quoteName('id') . "!='" . $db->escape($promotor) . "'")
            ->order('nome')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getAnos(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( 'DISTINCT Ano')
            ->from("#__virtualdesk_Cultura_Eventos")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->order('Ano')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function excludeAno($ano){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select( 'DISTINCT Ano')
            ->from("#__virtualdesk_Cultura_Eventos")
            ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            ->where($db->quoteName('Ano') . "!='" . $db->escape($ano) . "'")
            ->order('Ano')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getNifPromotor($promotor){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('nif')
            ->from("#__virtualdesk_Associativismo_Associacoes")
            ->where($db->quoteName('estado') . '=' . $db->escape('2'))
            ->where($db->quoteName('id') . '=' . $db->escape($promotor))
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getEventosFiltered($nomePost, $nifPromotor, $ano){
        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $CatName = 'catName_PT';
        }
        else {
            $CatName = 'catName_EN';
        }

        $db = JFactory::getDBO();

        $where = $db->quoteName('a.estado') . "='" . $db->escape('2') . "'";


        if(!empty($ano)){
            $where .= 'AND' . $db->quoteName('a.Ano') . "='" . $db->escape($ano) . "'";
        }

        if(!empty($nomePost)) {
            $nome2 = '%' . $nomePost . '%';
            $where .= 'AND' . $db->quoteName('a.nome_evento') . "LIKE'" . $db->escape($nome2) . "'";
        }

        if($nifPromotor != 0) {
            $where .= 'AND' . $db->quoteName('a.nif') . "='" . $db->escape($nifPromotor) . "'";
        }


        $db->setQuery($db->getQuery(true)
            ->select(array('a.id_evento, a.referencia_evento, b.' . $CatName . ' as categoria, a.nome_evento, a.desc_evento, a.nif, c.freguesia as freguesia, a.data_inicio, a.data_fim'))
            ->join('LEFT', '#__virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento')
            ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia_evento')
            ->join('INNER', '#__virtualdesk_Associativismo_Associacoes AS d ON d.nif = a.nif')
            ->from("#__virtualdesk_Cultura_Eventos as a")
            ->where($where)
            ->order('a.data_inicio DESC')
        );

        $data = $db->loadAssocList();
        return ($data);
    }

}
?>