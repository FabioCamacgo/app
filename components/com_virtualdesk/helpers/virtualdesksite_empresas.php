<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteEmpresasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_files.php');
    JLoader::register('VirtualDeskSiteEmpresasLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_logos_files.php');
    JLoader::register('VirtualDeskTableEmpresas', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/empresasGOV.php');


    class VirtualDeskSiteEmpresasHelper
    {

        public static function getFreguesia($concelho)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeFreguesia($concelho, $freguesia2){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($freguesia2) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getFregName($freguesia)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('freguesia')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getCategorias(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Empresas_Categorias")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getSubCategoria($idwebsitelist,$onAjaxVD=0)
        {

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, subcategoria, id_categoria'))
                    ->from("#__virtualdesk_Empresas_Subcategorias")
                    ->where($db->quoteName('id_categoria') . '=' . $db->escape($idwebsitelist))
                    ->order('subcategoria ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, subcategoria as name, id_categoria'))
                    ->from("#__virtualdesk_Empresas_Subcategorias")
                    ->where($db->quoteName('id_categoria') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }


        public static function getSubCatName($subCcat){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('subcategoria')
                ->from("#__virtualdesk_Empresas_Subcategorias")
                ->where($db->quoteName('id') . '=' . $db->escape($subCcat))
            );
            $data = $db->loadResult();

            return ($data);
        }


        public static function excludeSubCategoria($cat, $subCcat){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, subcategoria, id_categoria'))
                ->from("#__virtualdesk_Empresas_Subcategorias")
                ->where($db->quoteName('id_categoria') . "='" . $db->escape($cat) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($subCcat) . "'")
                ->order('subcategoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getempresasFilter(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.id, a.referencia, a.designacao, b.categoria as categoria, d.subcategoria as subcategoria, c.freguesia as freguesia, a.estado'))
                ->join('LEFT', '#__virtualdesk_Empresas_Categorias AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_Empresas_Subcategorias AS d ON d.id = a.subcategoria')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->order('designacao ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEmpresasFiltered($nomePost, $cat, $subCcat, $freguesia){
            $db = JFactory::getDBO();

            $where = $db->quoteName('estado') . "='" . $db->escape('2') . "'";

            if(!empty($cat)){
                $where .= 'AND' . $db->quoteName('a.categoria') . "='" . $db->escape($cat) . "'";
            }

            if(!empty($subCcat)){
                $where .= 'AND' . $db->quoteName('a.subcategoria') . "='" . $db->escape($subCcat) . "'";
            }

            if(!empty($freguesia)){
                $where .= 'AND' . $db->quoteName('a.freguesia') . "='" . $db->escape($freguesia) . "'";
            }

            if(!empty($nomePost)){
                $nome2 = '%' . $nomePost . '%';

                $where2 = $db->quoteName('a.designacao') . "LIKE '" . $db->escape($nome2) . "'";
                $where3 = $db->quoteName('a.nome') . "LIKE '" . $db->escape($nome2) . "'";
                $where4 = $db->quoteName('a.nif') . "LIKE '" . $db->escape($nome2) . "'";

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.designacao, b.categoria as categoria, d.subcategoria as subcategoria, c.freguesia as freguesia, a.estado'))
                    ->join('LEFT', '#__virtualdesk_Empresas_Categorias AS b ON b.id = a.categoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_Empresas_Subcategorias AS d ON d.id = a.subcategoria')
                    ->from("#__virtualdesk_Empresas as a")
                    ->where($where . 'AND' . $where2)
                    ->orWhere($where . 'AND' . $where3)
                    ->orWhere($where . 'AND' . $where4)
                    ->order('designacao ASC')
                );
            } else {

                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id, a.referencia, a.designacao, b.categoria as categoria, d.subcategoria as subcategoria, c.freguesia as freguesia, a.estado'))
                    ->join('LEFT', '#__virtualdesk_Empresas_Categorias AS b ON b.id = a.categoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_Empresas_Subcategorias AS d ON d.id = a.subcategoria')
                    ->from("#__virtualdesk_Empresas as a")
                    ->where($where)
                    ->order('designacao ASC')
                );
            }

            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getImgEmpresa($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, idprocesso, tagprocesso, filepath, basename, ext'))
                ->from("#__virtualdesk_Empresas_Files")
                ->where($db->quoteName('idprocesso') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getIdCat($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas_Categorias")
                ->where($db->quoteName('categoria') . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getIdSubCat($idCat, $subcategoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas_Subcategorias")
                ->where($db->quoteName('id_categoria') . "='" . $db->escape($idCat) . "'")
                ->where($db->quoteName('subcategoria') . "='" . $db->escape($subcategoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getCatName($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('categoria')
                ->from("#__virtualdesk_Empresas_Categorias")
                ->where($db->quoteName('id') . "='" . $db->escape($categoria) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeCategoria($categoria){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Empresas_Categorias")
                ->where($db->quoteName('id') . "!='" . $db->escape($categoria) . "'")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getInfoEmpresa($idEmpresa){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
            ->select(array('a.id, a.referencia, a.designacao, a.apresentacao, b.categoria as categoria, d.subcategoria as subcategoria, a.morada, a.latitude, a.longitude, c.freguesia as freguesia, a.telefone, a.website, a.facebook, a.instagram'))
                ->join('LEFT', '#__virtualdesk_Empresas_Categorias AS b ON b.id = a.categoria')
                ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS c ON c.id = a.freguesia')
                ->join('LEFT', '#__virtualdesk_Empresas_Subcategorias AS d ON d.id = a.subcategoria')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('a.id') . "='" . $db->escape($idEmpresa) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTipologia(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tipologia'))
                ->from("#__virtualdesk_Empresas_Tipo_Alojamento")
                ->order('tipologia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTipologiaName($tipologia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('tipologia')
                ->from("#__virtualdesk_Empresas_Tipo_Alojamento")
                ->where($db->quoteName('id') . "='" . $db->escape($tipologia) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeTipologia($tipologia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tipologia'))
                ->from("#__virtualdesk_Empresas_Tipo_Alojamento")
                ->where($db->quoteName('id') . "!='" . $db->escape($tipologia) . "'")
                ->order('tipologia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
        }


        public static function getTotal(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTotalFicar(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('categoria') . "='" . $db->escape('1') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTotalComer(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('categoria') . "='" . $db->escape('11') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTotalVestir(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('categoria') . "='" . $db->escape('10') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTotalConstrucao(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('categoria') . "='" . $db->escape('4') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getTotalAgricultura(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('estado') . "='" . $db->escape('2') . "'")
                ->where($db->quoteName('categoria') . "='" . $db->escape('16') . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getInfoGeral($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('nRegisto_Alojamento, capacidade_Alojamento, unidades_Alojamento'))
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('id') . "='" . $db->escape($id) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getInfoAL($id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('a.nRegisto_Alojamento, b.tipologia as tipologia, a.nQuartos_Alojamento, a.nUtentes_Alojamento, a.nCamas_Alojamento'))
                ->join('LEFT', '#__virtualdesk_Empresas_Tipo_Alojamento AS b ON b.id = a.tipologia_Alojamento')
                ->from("#__virtualdesk_Empresas as a")
                ->where($db->quoteName('a.id') . "='" . $db->escape($id) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function SaveNovaEmpresa($referencia, $nomeEmpresa, $designacaoComercial, $apresentacao, $categoria, $subcategoria, $morada, $lat, $long, $codPostal, $freguesia, $fiscalid, $telefone, $email, $website, $facebook, $instagram, $nomeResponsavel, $cargoResponsavel, $telefoneResponsavel, $emailResponsavel, $nregisto, $capacidade, $unidades, $tipologia, $nQuartos, $nUtentes, $nCamas){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia', 'nome', 'designacao', 'apresentacao', 'categoria', 'subcategoria', 'morada', 'latitude', 'longitude', 'codigo_postal', 'freguesia', 'nif', 'telefone', 'email', 'website', 'facebook', 'instagram', 'nome_responsavel', 'cargo_responsavel', 'telefone_responsavel', 'mail_responsavel', 'nRegisto_Alojamento', 'capacidade_Alojamento', 'unidades_Alojamento', 'tipologia_Alojamento', 'nQuartos_Alojamento', 'nUtentes_Alojamento', 'nCamas_Alojamento', 'estado', 'layout');
            $values = array($db->quote($referencia), $db->quote($nomeEmpresa), $db->quote($designacaoComercial), $db->quote($apresentacao), $db->quote($categoria), $db->quote($subcategoria), $db->quote($morada), $db->quote($lat), $db->quote($long), $db->quote($codPostal), $db->quote($freguesia), $db->quote($fiscalid), $db->quote($telefone), $db->quote($email), $db->quote($website), $db->quote($facebook), $db->quote($instagram), $db->quote($nomeResponsavel), $db->quote($cargoResponsavel), $db->quote($telefoneResponsavel), $db->quote($emailResponsavel), $db->quote($nregisto), $db->quote($capacidade), $db->quote($unidades), $db->quote($tipologia), $db->quote($nQuartos), $db->quote($nUtentes), $db->quote($nCamas), $db->quote('1'), $db->quote('1'));
            $query
                ->insert($db->quoteName('#__virtualdesk_Empresas'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);
        }


        /*Backoffice*/

        const tagchaveModulo = 'empresas';


        public static function getEmpresasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('empresas', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('empresas','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=view4manager&empresa_id=');

                    $table  = " ( SELECT a.id as id, a.id as empresa_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.categoria as idcategoria, b.categoria as categoria, a.subcategoria as idsubcategoria, c.subcategoria as subcategoria, a.estado as idestado, d.estado as estado, e.freguesia as freguesia, a.freguesia as idfreguesia, a.designacao as nome, a.nif as nif, a.data_criacao as data_criacao";
                    $table .= " FROM ".$dbprefix."virtualdesk_Empresas as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Empresas_Categorias AS b ON b.id = a.categoria";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Empresas_Subcategorias AS c ON c.id = a.subcategoria";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Empresas_Estado AS d ON d.id_estado = a.estado";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_digitalGov_freguesias AS e ON e.id = a.freguesia";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'nif',               'dt' => 0 ),
                        array( 'db' => 'nome',              'dt' => 1 ),
                        array( 'db' => 'categoria',         'dt' => 2 ),
                        array( 'db' => 'subcategoria',      'dt' => 3 ),
                        array( 'db' => 'freguesia',         'dt' => 4 ),
                        array( 'db' => 'estado',            'dt' => 5 ),
                        array( 'db' => 'dummy',             'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'empresa_id',        'dt' => 7 ),
                        array( 'db' => 'idcategoria',       'dt' => 8 ),
                        array( 'db' => 'idsubcategoria',    'dt' => 9 ),
                        array( 'db' => 'idfreguesia',       'dt' => 10 ),
                        array( 'db' => 'idestado',          'dt' => 11 ),
                        array( 'db' => 'data_criacao',      'dt' => 12 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-pendente';
                break;
                case '2':
                    $defCss = 'label-publicado';
                    break;
                case '3':
                    $defCss = 'label-despublicado';
                    break;
                case '4':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }


        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name') )
                    ->from("#__virtualdesk_Empresas_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEstado ()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id_estado as id','estado') )
                ->from("#__virtualdesk_Empresas_Estado")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getEstadoName ($estado)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( 'estado')
                ->from("#__virtualdesk_Empresas_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($estado) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function excludeEstado($estado)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id_estado as id','estado') )
                ->from("#__virtualdesk_Empresas_Estado")
                ->where($db->quoteName('id_estado') . "!='" . $db->escape($estado) . "'")
                ->order('id ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getCategoria(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, categoria'))
                ->from("#__virtualdesk_Empresas_Categorias")
                ->order('categoria ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getFreguesiaAdmin($concelhoEmpresas){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelhoEmpresas) . "'")
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function CheckReferencia($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('referencia')
                ->from("#__virtualdesk_Empresas")
                ->where($db->quoteName('referencia') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function create4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
           * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
           */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('empresas');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('empresas', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $db    = JFactory::getDbo();

            if (empty($data) ) return false;

            try {
                $refExiste = 1;


                $obParam      = new VirtualDeskSiteParamsHelper();
                $refEmpresas      = $obParam->getParamsByTag('refEmpresas');



                $nomeEmpresa = $data['nomeEmpresa'];
                $designacaoComercial = $data['designacaoComercial'];
                $apresentacao = $data['apresentacao'];
                $categoria = $data['categoria'];
                $subcategoria = $data['subcategoria'];
                $nregisto = $data['nregisto'];
                $capacidade = $data['capacidade'];
                $unidades = $data['unidades'];
                $tipologia = $data['tipologia'];
                $nQuartos = $data['nQuartos'];
                $nUtentes = $data['nUtentes'];
                $nCamas = $data['nCamas'];
                $morada = $data['morada'];
                $coordenadas = $data['coordenadas'];
                $codPostal4 = $data['codPostal4'];
                $codPostal3 = $data['codPostal3'];
                $codPostalText = $data['codPostalText'];
                $freguesia = $data['freguesia'];
                $fiscalid = $data['fiscalid'];
                $telefone = $data['telefone'];
                $email = $data['email'];
                $confEmail = $data['confEmail'];
                $website = $data['website'];
                $facebook = $data['facebook'];
                $instagram = $data['instagram'];
                $nomeResponsavel = $data['nomeResponsavel'];
                $cargoResponsavel = $data['cargoResponsavel'];
                $telefoneResponsavel = $data['telefoneResponsavel'];
                $emailResponsavel = $data['emailResponsavel'];
                $confEmailResponsavel = $data['confEmailResponsavel'];
                $estado = '2';
                $codPostal = $codPostal4 . '-' . $codPostal3 . ' ' . $codPostalText;

                $vdFileUpChangedFoto = $data['vdFileUpChangedFoto'];
                $vdFileUpChangedLogo = $data['vdFileUpChangedLogo'];


                $splitCoord = explode(",", $coordenadas);
                $lat = $splitCoord[0];
                $long = $splitCoord[1];


                $referencia = '';
                while($refExiste == 1){
                    $random = self::random_code();

                    $referencia = $refEmpresas . $fiscalid . '-' . $random;
                    $checkREF = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }

                $resSaveEmpresa = self::saveNewEmpresa4Manager($referencia, $nomeEmpresa, $designacaoComercial, $apresentacao, $categoria, $subcategoria, $nregisto, $capacidade, $unidades, $tipologia, $nQuartos, $nUtentes, $nCamas, $morada, $lat, $long, $codPostal, $freguesia, $fiscalid, $telefone, $email, $website, $facebook, $instagram, $nomeResponsavel, $cargoResponsavel, $telefoneResponsavel, $emailResponsavel, $estado);

                if (empty($resSaveEmpresa)) return false;

                $empresasFiles              =  new VirtualDeskSiteEmpresasFilesHelper();
                $empresasFiles->tagprocesso = 'EMPRESAS_POST';
                $empresasFiles->idprocesso  = $referencia;
                $resFileSave                = $empresasFiles->saveListFileByPOST('fileupload');

                $empresasLogoFiles              =  new VirtualDeskSiteEmpresasLogosFilesHelper();
                $empresasLogoFiles->tagprocesso = 'EMPRESAS_LOGO_POST';
                $empresasLogoFiles->idprocesso  = $referencia;
                $resLogoSave                    = $empresasLogoFiles->saveListFileByPOST('fileuploadLogo');

                if ($resFileSave===false || $resLogoSave===false) {
                    JFactory::getApplication()->enqueueMessage('Erro ao gravar os ficheiros', 'error');
                }

                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }


        public static function saveNewEmpresa4Manager($referencia, $nomeEmpresa, $designacaoComercial, $apresentacao, $categoria, $subcategoria, $nregisto, $capacidade, $unidades, $tipologia, $nQuartos, $nUtentes, $nCamas, $morada, $lat, $long, $codPostal, $freguesia, $fiscalid, $telefone, $email, $website, $facebook, $instagram, $nomeResponsavel, $cargoResponsavel, $telefoneResponsavel, $emailResponsavel, $estado){

            $db = JFactory::getDbo();

            $apresentacao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($apresentacao),true);

            $query = $db->getQuery(true);
            $columns = array('referencia', 'nome', 'designacao', 'apresentacao', 'categoria', 'subcategoria', 'morada', 'latitude', 'longitude', 'codigo_postal', 'freguesia', 'nif', 'telefone', 'email', 'website', 'facebook', 'instagram', 'nome_responsavel', 'cargo_responsavel', 'telefone_responsavel', 'mail_responsavel', 'nRegisto_Alojamento', 'capacidade_Alojamento', 'unidades_Alojamento', 'tipologia_Alojamento', 'nQuartos_Alojamento', 'nUtentes_Alojamento', 'nCamas_Alojamento', 'estado', 'layout');
            $values = array($db->quote($referencia), $db->quote($nomeEmpresa), $db->quote($designacaoComercial), $db->quote($apresentacao), $db->quote($categoria), $db->quote($subcategoria), $db->quote($morada), $db->quote($lat), $db->quote($long), $db->quote($codPostal), $db->quote($freguesia), $db->quote($fiscalid), $db->quote($telefone), $db->quote($email), $db->quote($website), $db->quote($facebook), $db->quote($instagram), $db->quote($nomeResponsavel), $db->quote($cargoResponsavel), $db->quote($telefoneResponsavel), $db->quote($emailResponsavel), $db->quote($nregisto), $db->quote($capacidade), $db->quote($unidades), $db->quote($tipologia), $db->quote($nQuartos), $db->quote($nUtentes), $db->quote($nCamas), $db->quote($estado), $db->quote('1'));
            $query
                ->insert($db->quoteName('#__virtualdesk_Empresas'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean)$db->execute();

            return ($result);
        }


        public static function checkRefId_4User_By_NIF ($RefId)
        {
            if( empty($RefId) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.referencia as refid' ))
                    ->from("#__virtualdesk_Empresas_Logos_Files as a")
                    ->where( $db->quoteName('a.referencia') . '="' . $db->escape($RefId) . '" and ' . $db->quoteName('a.nif').'='.$db->escape($UserSessionNIF) )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn->refid);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function getEmpresasDetail4Manager($id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('empresas');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('empresas', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('empresas'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('empresas','edit4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id', 'a.referencia', 'a.nome', 'a.designacao', 'a.apresentacao', 'a.categoria', 'a.subcategoria', 'a.morada', 'a.latitude', 'a.longitude', 'a.codigo_postal', 'a.freguesia', 'a.nif', 'a.telefone', 'a.email', 'a.website', 'a.facebook', 'a.instagram', 'a.nome_responsavel', 'a.cargo_responsavel', 'a.telefone_responsavel', 'a.mail_responsavel', 'a.nRegisto_Alojamento', 'a.capacidade_Alojamento', 'a.unidades_Alojamento', 'a.tipologia_Alojamento', 'a.nQuartos_Alojamento', 'a.nUtentes_Alojamento', 'a.nCamas_Alojamento', 'a.estado', 'a.layout', 'a.data_criacao', 'a.data_alteracao', 'b.categoria as catName', 'c.subcategoria as subCatName', 'd.freguesia as fregName', 'e.estado as estadoName', 'f.tipologia as tipologiaName'))
                    ->join('LEFT', '#__virtualdesk_Empresas_Categorias AS b ON b.id = a.categoria')
                    ->join('LEFT', '#__virtualdesk_Empresas_Subcategorias AS c ON c.id = a.subcategoria')
                    ->join('LEFT', '#__virtualdesk_digitalGov_freguesias AS d ON d.id = a.freguesia')
                    ->join('LEFT', '#__virtualdesk_Empresas_Estado AS e ON e.id_estado = a.estado')
                    ->join('LEFT', '#__virtualdesk_Empresas_Tipo_Alojamento AS f ON f.id = a.tipologia_Alojamento')
                    ->from("#__virtualdesk_Empresas as a")
                    ->where( $db->quoteName('a.id') . '=' . $db->escape($id))
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        public static function update4Manager($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('empresas');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('empresas', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $empresa_id = $data['empresa_id'];
            if((int) $empresa_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            if (empty($data)) return false;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();
            $Table = new VirtualDeskTableEmpresas($db);
            $Table->load(array('id'=>$empresa_id));

            $db->transactionStart();

            try {
                $referencia          = $Table->referencia;
                $obParam    = new VirtualDeskSiteParamsHelper();


                $nome = null;
                $designacao = null;
                $apresentacao = null;
                $categoria = null;
                $subcategoria = null;
                $morada = null;
                $latitude = null;
                $longitude = null;
                $codigo_postal = null;
                $freguesia = null;
                $nif = null;
                $telefone = null;
                $email = null;
                $website = null;
                $facebook = null;
                $instagram = null;
                $nome_responsavel = null;
                $cargo_responsavel = null;
                $telefone_responsavel = null;
                $mail_responsavel = null;
                $nRegisto_Alojamento = null;
                $capacidade_Alojamento = null;
                $unidades_Alojamento = null;
                $tipologia_Alojamento = null;
                $nQuartos_Alojamento = null;
                $nUtentes_Alojamento = null;
                $nCamas_Alojamento = null;
                $estado = null;
                $layout = null;

                if(array_key_exists('nomeEmpresa',$data))                       $nome = $data['nomeEmpresa'];
                if(array_key_exists('designacaoComercial',$data))               $designacao = $data['designacaoComercial'];
                if(array_key_exists('apresentacao',$data))                      $apresentacao = $data['apresentacao'];
                if(array_key_exists('categoria',$data))                         $categoria = $data['categoria'];
                if(array_key_exists('subcategoria',$data))                      $subcategoria = $data['subcategoria'];
                if(array_key_exists('nregisto',$data))                          $nRegisto_Alojamento = $data['nregisto'];
                if(array_key_exists('capacidade',$data))                        $capacidade_Alojamento = $data['capacidade'];
                if(array_key_exists('unidades',$data))                          $unidades_Alojamento = $data['unidades'];
                if(array_key_exists('tipologia',$data))                         $tipologia_Alojamento = $data['tipologia'];
                if(array_key_exists('nQuartos',$data))                          $nQuartos_Alojamento = $data['nQuartos'];
                if(array_key_exists('nUtentes',$data))                          $nUtentes_Alojamento = $data['nUtentes'];
                if(array_key_exists('nCamas',$data))                            $nCamas_Alojamento = $data['nCamas'];
                if(array_key_exists('morada',$data))                            $morada = $data['morada'];
                if(array_key_exists('coordenadas',$data))    {
                    $coord = explode(',', $data['coordenadas']);
                    $latitude = $coord[0];
                    $longitude = $coord[1];
                }
                if(array_key_exists('codPostal4',$data))                        $codPostal4 = $data['codPostal4'];
                if(array_key_exists('codPostal3',$data))                        $codPostal3 = $data['codPostal3'];
                if(array_key_exists('codPostalText',$data))                     $codPostalText = $data['codPostalText'];
                if(array_key_exists('freguesia',$data))                         $freguesia = $data['freguesia'];
                if(array_key_exists('fiscalid',$data))                          $fiscalid = $data['fiscalid'];
                if(array_key_exists('telefone',$data))                          $telefone = $data['telefone'];
                if(array_key_exists('email',$data))                             $email = $data['email'];
                if(array_key_exists('website',$data))                           $website = $data['website'];
                if(array_key_exists('facebook',$data))                          $facebook = $data['facebook'];
                if(array_key_exists('instagram',$data))                         $instagram = $data['instagram'];
                if(array_key_exists('nomeResponsavel',$data))                   $nomeResponsavel = $data['nomeResponsavel'];
                if(array_key_exists('cargoResponsavel',$data))                  $cargoResponsavel = $data['cargoResponsavel'];
                if(array_key_exists('telefoneResponsavel',$data))               $telefoneResponsavel = $data['telefoneResponsavel'];
                if(array_key_exists('emailResponsavel',$data))                  $emailResponsavel = $data['emailResponsavel'];
                if(array_key_exists('estado',$data))                            $estado = $data['estado'];



                /* BEGIN Save Empresa */
                $resSaveEmpresa = self::saveEditedEmpresa4Manager($empresa_id, $nome, $designacao, $apresentacao, $categoria, $subcategoria, $nRegisto_Alojamento, $capacidade_Alojamento, $unidades_Alojamento, $tipologia_Alojamento, $nQuartos_Alojamento, $nUtentes_Alojamento, $nCamas_Alojamento, $morada, $latitude, $longitude, $codPostal4, $codPostal3, $codPostalText, $freguesia, $fiscalid, $telefone, $email, $website, $facebook, $instagram, $nomeResponsavel, $cargoResponsavel, $telefoneResponsavel, $emailResponsavel, $estado);
                /* END Save Empresa */

                if (empty($resSaveEmpresa)) {
                    $db->transactionRollback();
                    return false;
                }

                /* DELETE - FILES */
                $objCapaFiles                = new VirtualDeskSiteEmpresasFilesHelper();
                $listFileCapa2Eliminar       = $objCapaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadCapa');
                $resFileCapaDelete           = $objCapaFiles->deleteFiles ($referencia , $listFileCapa2Eliminar);
                if ($resFileCapaDelete==false && !empty($listFileCapa2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar a foto de capa da empresa', 'error');
                }


                $objGaleriaFiles                = new VirtualDeskSiteEmpresasLogosFilesHelper();
                $listFileGaleria2Eliminar       = $objGaleriaFiles->setListFile2EliminarFromPOST ($referencia,'fileuploader-list-fileuploadLogo');
                $resFileGaleriaDelete           = $objGaleriaFiles->deleteFiles ($referencia , $listFileGaleria2Eliminar);
                if ($resFileGaleriaDelete==false && !empty($listFileGaleria2Eliminar) ) {
                    JFactory::getApplication()->enqueueMessage('Erro ao eliminar o logo da empresa', 'error');
                }

                /* FILES */
                $objCapaFiles                = new VirtualDeskSiteEmpresasFilesHelper();
                $objCapaFiles->tagprocesso   = 'EMPRESAS_POST';
                $objCapaFiles->idprocesso    = $referencia;
                $resFileSaveCapa             = $objCapaFiles->saveListFileByPOST('fileuploadCapa');


                $objLogoFiles                = new VirtualDeskSiteEmpresasLogosFilesHelper();
                $objLogoFiles->tagprocesso   = 'LOGO_POST';
                $objLogoFiles->idprocesso    = $referencia;
                $resFileSaveLogo             = $objLogoFiles->saveListFileByPOST('fileuploadLogo');


                $db->transactionCommit();

                $objVDParams = new VirtualDeskSiteParamsHelper();


                return true;
            }
            catch (Exception $e){
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }


        public static function saveEditedEmpresa4Manager($empresa_id, $nome, $designacao, $apresentacao, $categoria, $subcategoria, $nRegisto_Alojamento, $capacidade_Alojamento, $unidades_Alojamento, $tipologia_Alojamento, $nQuartos_Alojamento, $nUtentes_Alojamento, $nCamas_Alojamento, $morada, $latitude, $longitude, $codPostal4, $codPostal3, $codPostalText, $freguesia, $fiscalid, $telefone, $email, $website, $facebook, $instagram, $nomeResponsavel, $cargoResponsavel, $telefoneResponsavel, $emailResponsavel, $estado){
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('empresas');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('empresas', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            $db     = JFactory::getDbo();
            $query  = $db->getQuery(true);
            $fields = array();

            $apresentacao = VirtualDeskSiteGeneralHelper::encodeHTML2Database ($db->escape($apresentacao),true);

            if(!is_null($nome))  array_push($fields, 'nome="'.$db->escape($nome).'"');
            if(!is_null($designacao))  array_push($fields, 'designacao="'.$db->escape($designacao).'"');
            if(!is_null($apresentacao))  array_push($fields, 'apresentacao="'.$db->escape($apresentacao).'"');
            if(!is_null($categoria))  array_push($fields, 'categoria="'.$db->escape($categoria).'"');
            if(!is_null($subcategoria))  array_push($fields, 'subcategoria="'.$db->escape($subcategoria).'"');
            if(!is_null($nRegisto_Alojamento))  array_push($fields, 'nRegisto_Alojamento="'.$db->escape($nRegisto_Alojamento).'"');
            if(!is_null($capacidade_Alojamento))  array_push($fields, 'capacidade_Alojamento="'.$db->escape($capacidade_Alojamento).'"');
            if(!is_null($unidades_Alojamento))  array_push($fields, 'unidades_Alojamento="'.$db->escape($unidades_Alojamento).'"');
            if(!is_null($tipologia_Alojamento))  array_push($fields, 'tipologia_Alojamento="'.$db->escape($tipologia_Alojamento).'"');
            if(!is_null($nQuartos_Alojamento))  array_push($fields, 'nQuartos_Alojamento="'.$db->escape($nQuartos_Alojamento).'"');
            if(!is_null($nUtentes_Alojamento))  array_push($fields, 'nUtentes_Alojamento="'.$db->escape($nUtentes_Alojamento).'"');
            if(!is_null($nCamas_Alojamento))  array_push($fields, 'nCamas_Alojamento="'.$db->escape($nCamas_Alojamento).'"');
            if(!is_null($morada))  array_push($fields, 'morada="'.$db->escape($morada).'"');
            if(!is_null($latitude))  array_push($fields, 'latitude="'.$db->escape($latitude).'"');
            if(!is_null($longitude))  array_push($fields, 'longitude="'.$db->escape($longitude).'"');
            if(!is_null($codPostal4)){
                $codPostal = $codPostal4 . '-' . $codPostal3 . ' ' . $codPostalText;
                array_push($fields, 'codigo_postal="'.$db->escape($codPostal).'"');
            }
            if(!is_null($freguesia))  array_push($fields, 'freguesia="'.$db->escape($freguesia).'"');
            if(!is_null($fiscalid))  array_push($fields, 'nif="'.$db->escape($fiscalid).'"');
            if(!is_null($telefone))  array_push($fields, 'telefone="'.$db->escape($telefone).'"');
            if(!is_null($email))  array_push($fields, 'email="'.$db->escape($email).'"');
            if(!is_null($website))  array_push($fields, 'website="'.$db->escape($website).'"');
            if(!is_null($facebook))  array_push($fields, 'facebook="'.$db->escape($facebook).'"');
            if(!is_null($instagram))  array_push($fields, 'instagram="'.$db->escape($instagram).'"');
            if(!is_null($nomeResponsavel))  array_push($fields, 'nome_responsavel="'.$db->escape($nomeResponsavel).'"');
            if(!is_null($cargoResponsavel))  array_push($fields, 'cargo_responsavel="'.$db->escape($cargoResponsavel).'"');
            if(!is_null($telefoneResponsavel))  array_push($fields, 'telefone_responsavel="'.$db->escape($telefoneResponsavel).'"');
            if(!is_null($emailResponsavel))  array_push($fields, 'mail_responsavel="'.$db->escape($emailResponsavel).'"');
            if(!is_null($estado))  array_push($fields, 'estado="'.$db->escape($estado).'"');

            $data_alteracao = date("Y-m-d H:i:s");

            array_push($fields, 'data_alteracao="'.$db->escape($data_alteracao).'"');

            if(sizeof($fields)<=0) return(true); // Nada para atualizar...

            $query
                ->update('#__virtualdesk_Empresas')
                ->set($fields)
                ->where(' id = '.$db->escape($empresa_id));

            $db->setQuery($query);
            $result = (boolean) $db->execute();

            return($result);
        }


        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.empresas.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.empresas.data', null);
            $app->setUserState('com_virtualdesk.edit4user.empresas.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.empresas.data', null);

        }
    }
?>