<?php
/**
 * Created by PhpStorm.
 * User: Camacho
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');
JLoader::register('VirtualDeskSiteTarefaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tarefa.php');


    class VirtualDeskSiteMainHelper
    {
        const ModuloTagAlerta  = 'alerta';
        const ModuloTagAgenda  = 'agenda';
        const ModuloTagCultura = 'cultura';
        const ModuloTagTickets = 'tickets';
        const ModuloTagSuporte = 'suporte';

        /* Carrega lista com dados de todos os PEDIDOS para datatables */
        public static function getAllRequestsList4Manager ( $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            //$vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            //$vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            //if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            if($vbInGroupAM ==false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';

                    $CatNameAgenda = 'cat_PT';
                    $SubCatNameAgenda = 'subcat_PT';
                    $EstadoNameAgenda = 'estado';

                    $CatNameCultura    = 'catName_PT';
                    $EstadoNameCultura = 'estado';

                    $EstadoNameTickets ='estado_PT';
                    $DepNameTickets = 'nome_PT';

                    $EstadoNameSuporte ='estado_PT';
                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';

                    $CatNameAgenda    = 'cat_EN';
                    $SubCatNameAgenda = 'subcat_EN';
                    $EstadoNameAgenda = 'estado';

                    $CatNameCultura    = 'catName_EN';
                    $EstadoNameCultura = 'estado';

                    $EstadoNameTickets ='estado_EN';
                    $DepNameTickets = 'nome_EN';

                    $EstadoNameSuporte ='estado_EN';
                }

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    $obParam    = new VirtualDeskSiteParamsHelper();

                    // Gerar Links para o detalhe...
                    $AlertaMenuId4Manager = $obParam->getParamsByTag('Alerta_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Alerta =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager&Itemid='.$AlertaMenuId4Manager.'&alerta_id=');

                    $AgendaMenuId4Manager = $obParam->getParamsByTag('Agenda_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Agenda =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=view4manager&Itemid='.$AgendaMenuId4Manager.'&agenda_id=');

                    $CulturaMenuId4Manager = $obParam->getParamsByTag('Cultura_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Cultura =  JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=view4manager&Itemid='.$CulturaMenuId4Manager.'&cultura_id=');

                    $TicketsMenuId4Manager = $obParam->getParamsByTag('Tickets_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Tickets     =  JRoute::_('index.php?option=com_virtualdesk&view=tickets&layout=view4manager&Itemid='.$TicketsMenuId4Manager.'&tickets_id=');

                    $SuporteMenuId4Manager = $obParam->getParamsByTag('Suporte_Menu_Id_By_Default_4Managers');
                    $dummyHRef_Suporte     =  JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=view4manager&Itemid='.$SuporteMenuId4Manager.'&suporte_id=');

                    // Tipo de Pedido
                    $TipoPedidoAlerta   = JText::_('COM_VIRTUALDESK_ALERTA_HEADING');
                    $TipoPedidoAgenda   = JText::_('COM_VIRTUALDESK_AGENDA_HEADING');
                    $TipoPedidoCultura  = JText::_('COM_VIRTUALDESK_CULTURA_HEADING');
                    $TipoPedidoTickets  = JText::_('COM_VIRTUALDESK_TICKETS_HEADING');
                    $TipoPedidoSuporte  = JText::_('COM_VIRTUALDESK_SUPORTE_HEADING');

                    // Tag Módulos
                    $ModuloTagAlerta  =  self::ModuloTagAlerta;
                    $ModuloTagAgenda  =  self::ModuloTagAgenda;
                    $ModuloTagCultura =  self::ModuloTagCultura;
                    $ModuloTagTickets =  self::ModuloTagTickets;
                    $ModuloTagSuporte =  self::ModuloTagSuporte;

                    $table  = " (  ";
                    $table .= " SELECT  id, idAll, dummy,  codigo,  titulo, categoria, category_name, estado, idestado, tipo_pedido, tipo_pedido_desc, created, modified, createdFull, tagmodulo, idcategoria FROM ( ";

                    $vbCheckFirstUnion = true; // detecta qual o primeiro union
                    // Para o caso de o módulo não estar ativo ou não permissão na lista, nem faz a pesquisa
                    $vbChecModuleAlerta = $objCheckPerm->loadModuleEnabledByTag('alerta');
                    $vbInGroupAlerta    = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
                    if((int)$vbChecModuleAlerta==1 && $vbInGroupAlerta !==false) {
                        // Alerta
                        $table .= " SELECT a.Id_alerta as id, CONCAT('ALRT',CONVERT(a.Id_alerta , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Alerta . "', CAST(a.Id_alerta as CHAR(10))) as dummy , a.codOco as codigo ,  CONCAT(LEFT (a.descricao,50),'...') as titulo";
                        $table .= " , IFNULL(b." . $CatName . ",' ') as categoria , IFNULL(b." . $CatName . ",' ') as category_name , IFNULL(e." . $EstadoName . ",' ') as estado , CONCAT('ALRT',CONVERT(a.estado, CHAR(50))) as idestado , 'ALRT' as tipo_pedido, '".$TipoPedidoAlerta."' as tipo_pedido_desc ";
                        $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                        $table .= " , '".$ModuloTagAlerta."' as tagmodulo , CONCAT('ALRT',CONVERT(a.categoria, CHAR(50))) as idcategoria ";
                        $table .= " FROM " . $dbprefix . "virtualdesk_alerta as a ";
                        $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria ";
                        $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_alerta_estados AS e ON e.id_estado = a.estado ";

                        $vbCheckFirstUnion = false;
                    }

                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('agenda');
                    $vbInGroupAgenda    = $objCheckPerm->checkReadAllLayoutAccess('agenda','list4managers');
                    if((int)$vbChecModuleAgenda==1 && $vbInGroupAgenda !==false) {
                        if($vbCheckFirstUnion===false) $table .= " UNION ";
                        // Agenda
                        $table .= " SELECT a.id as id, CONCAT('AGND',CONVERT(a.id , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Agenda . "', CAST(a.id as CHAR(10))) as dummy , a.referencia  as codigo ,  CONCAT(LEFT (a.nome_evento,50),'...') as titulo ";
                        $table .= " , IFNULL(b." . $CatNameAgenda . ",' ') as categoria , IFNULL(b." . $CatNameAgenda . ",' ') as category_name , IFNULL(e." . $EstadoNameAgenda . ",' ') as estado , CONCAT('AGND',CONVERT(a.estado_evento, CHAR(50))) as idestado , 'AGND' as tipo_pedido, '".$TipoPedidoAgenda."' as tipo_pedido_desc ";
                        $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                        $table .= " , '".$ModuloTagAgenda."' as tagmodulo , CONCAT('AGND',CONVERT(a.categoria, CHAR(50))) as idcategoria ";
                        $table .= " FROM " . $dbprefix . "virtualdesk_v_agenda_eventos as a ";
                        $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria ";
                        $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_v_agenda_estado AS e ON e.id = a.estado_evento ";

                        $vbCheckFirstUnion = false;
                    }

                    // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                    $vbChecModuleCultura = $objCheckPerm->loadModuleEnabledByTag('cultura');
                    $vbInGroupCultura    = $objCheckPerm->checkReadAllLayoutAccess('cultura','list4managers');
                    if((int)$vbChecModuleCultura==1 && $vbInGroupCultura !==false) {
                        if($vbCheckFirstUnion===false) $table .= " UNION ";
                        // Cultura
                        $table .= " SELECT a.id_evento as id, CONCAT('CULT',CONVERT(a.id_evento , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Cultura . "', CAST(a.id_evento as CHAR(10))) as dummy , a.referencia_evento  as codigo ,  CONCAT(LEFT (a.nome_evento,50),'...') as titulo ";
                        $table .= " , IFNULL(b." . $CatNameCultura . ",' ') as categoria , IFNULL(b." . $CatNameCultura . ",' ') as category_name , IFNULL(e." . $EstadoNameCultura . ",' ') as estado , CONCAT('CULT',CONVERT(a.estado, CHAR(50))) as idestado , 'CULT' as tipo_pedido, '".$TipoPedidoCultura."' as tipo_pedido_desc ";
                        $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                        $table .= " , '".$ModuloTagCultura."' as tagmodulo , CONCAT('CULT',CONVERT(a.cat_evento, CHAR(50))) as idcategoria ";
                        $table .= " FROM ".$dbprefix."virtualdesk_Cultura_Eventos as a ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento ";
                        $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Estado AS e ON e.id_estado = a.estado ";

                        $vbCheckFirstUnion = false;

                    }

                // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                $vbChecModuleTickets = $objCheckPerm->loadModuleEnabledByTag('tickets');
                $vbInGroupTickets    = $objCheckPerm->checkReadAllLayoutAccess('tickets','list4managers');
                if((int)$vbChecModuleTickets==1 && $vbInGroupTickets !==false) {
                    if($vbCheckFirstUnion===false) $table .= " UNION ";
                    // Tickets
                    $table .= " SELECT a.id as id, CONCAT('TICK',CONVERT(a.id , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Tickets . "', CAST(a.id as CHAR(10))) as dummy , a.referencia  as codigo ,  CONCAT(LEFT (a.assunto,50),'...') as titulo ";
                    $table .= " , IFNULL(b." . $DepNameTickets . ",' ') as categoria , IFNULL(b." . $DepNameTickets . ",' ') as category_name , IFNULL(e." . $EstadoNameTickets . ",' ') as estado , CONCAT('TICK',CONVERT(a.id_estado, CHAR(50))) as idestado , 'TICK' as tipo_pedido, '".$TipoPedidoTickets."' as tipo_pedido_desc ";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " , '".$ModuloTagTickets."' as tagmodulo , CONCAT('TICK',CONVERT(a.id_departamento, CHAR(50))) as idcategoria";
                    $table .= " FROM ".$dbprefix."virtualdesk_tickets as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tickets_departamento AS b ON b.id = a.id_departamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tickets_estado AS e ON e.id = a.id_estado ";

                    $vbCheckFirstUnion = false;
                }

                // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                $vbChecModuleSuporte = $objCheckPerm->loadModuleEnabledByTag('suporte');
                $vbInGroupSuporte    = $objCheckPerm->checkReadAllLayoutAccess('suporte','list4managers');
                if((int)$vbChecModuleSuporte==1 || $vbInGroupSuporte !==false) {
                    if($vbCheckFirstUnion===false) $table .= " UNION ";
                    // Suporte
                    $table .= " SELECT a.id as id, CONCAT('SUPT',CONVERT(a.id , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Suporte . "', CAST(a.id as CHAR(10))) as dummy , a.referencia  as codigo ,  CONCAT(LEFT (a.assunto,50),'...') as titulo ";
                    $table .= " , '' as categoria , '' as category_name , IFNULL(e." . $EstadoNameSuporte . ",' ') as estado , CONCAT('SUPT',CONVERT(a.id_estado, CHAR(50))) as idestado , 'SUPT' as tipo_pedido, '".$TipoPedidoSuporte."' as tipo_pedido_desc ";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " , '".$ModuloTagSuporte."' as tagmodulo , CONCAT('SUPT',CONVERT(a.id_departamento, CHAR(50))) as idcategoria";
                    $table .= " FROM ".$dbprefix."virtualdesk_suporte as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_departamento AS b ON b.id = a.id_departamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_suporte_estado AS e ON e.id = a.id_estado ";

                    $vbCheckFirstUnion = false;
                }

                    $table .= " ) as allUnion ";

                    $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                    if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                    $table .= $setLimitSQL;
                    $table .= " ) temp ";

                    $primaryKey = 'idAll';

                $columns = array(
                    array( 'db' => 'created',          'dt' => 0 ),
                    array( 'db' => 'tipo_pedido_desc', 'dt' => 1 ),
                    array( 'db' => 'titulo',           'dt' => 2 ),
                    array( 'db' => 'categoria',        'dt' => 3 ),
                    array( 'db' => 'estado',           'dt' => 4 ),
                    array( 'db' => 'codigo',           'dt' => 5 ),
                    array( 'db' => 'dummy',            'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',         'dt' => 7 ),
                    array( 'db' => 'id',               'dt' => 8 ,'formatter'=>'VALUE_ENCRYPT'),
                    array( 'db' => 'modified',         'dt' => 9 ),
                    array( 'db' => 'createdFull',      'dt' => 10 ),
                    array( 'db' => 'idAll',            'dt' => 11 ,'formatter'=>'VALUE_ENCRYPT'),
                    array( 'db' => 'tipo_pedido',      'dt' => 12 ),
                    array( 'db' => 'tagmodulo',        'dt' => 13 ),
                    array( 'db' => 'idcategoria',      'dt' => 14 )
                );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = array('data');
                    if((int)$vbChecModuleAlerta==0 && (int)$vbChecModuleAgenda==0 && (int)$vbChecModuleCultura==0 && (int)$vbChecModuleTickets==0 && (int)$vbChecModuleSuporte==0) return($data);

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;


            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com dados de todos os PEDIDOS para datatables */
        public static function getAllRequestsList4User ( $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            //$vbHasAccess  = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
            //$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            //$vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('alerta','list4managers');
            //if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            //if($vbInGroupAM ==false ) {
            //    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            //    return false;
            //  }


            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if( empty($UserJoomlaID)  )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $CatName = 'name_PT';
                    $EstadoName = 'estado_PT';

                    $CatNameAgenda = 'cat_PT';
                    $SubCatNameAgenda = 'subcat_PT';
                    $EstadoNameAgenda = 'estado';

                    $CatNameCultura    = 'catName_PT';
                    $EstadoNameCultura = 'estado';

                    $EstadoNameTickets ='estado_PT';
                    $DepNameTickets = 'nome_PT';


                }
                else {
                    $CatName = 'name_EN';
                    $EstadoName = 'estado_EN';

                    $CatNameAgenda    = 'cat_EN';
                    $SubCatNameAgenda = 'subcat_EN';
                    $EstadoNameAgenda = 'estado';

                    $CatNameCultura    = 'catName_EN';
                    $EstadoNameCultura = 'estado';

                    $EstadoNameTickets ='estado_EN';
                    $DepNameTickets = 'nome_EN';

                }

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                $obParam    = new VirtualDeskSiteParamsHelper();

                // Gerar Links para o detalhe...
                $AlertaMenuId4User = $obParam->getParamsByTag('Alerta_Menu_Id_By_Default_4Users');
                $dummyHRef_Alerta =  JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&Itemid='.$AlertaMenuId4User.'&alerta_id=');

                $AgendaMenuId4User = $obParam->getParamsByTag('Agenda_Menu_Id_By_Default_4Users');
                $dummyHRef_Agenda =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=view4user&Itemid='.$AgendaMenuId4User.'&agenda_id=');

                $CulturaMenuId4User = $obParam->getParamsByTag('Cultura_Menu_Id_By_Default_4Users');
                $dummyHRef_Cultura =  JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=view4user&Itemid='.$CulturaMenuId4User.'&cultura_id=');

                $TicketsMenuId4User = $obParam->getParamsByTag('Tickets_Menu_Id_By_Default_4Users');
                $dummyHRef_Tickets     =  JRoute::_('index.php?option=com_virtualdesk&view=tickets&layout=view4user&Itemid='.$TicketsMenuId4User.'&tickets_id=');


                // Tipo de Pedido
                $TipoPedidoAlerta   = JText::_('COM_VIRTUALDESK_ALERTA_HEADING');
                $TipoPedidoAgenda   = JText::_('COM_VIRTUALDESK_AGENDA_HEADING');
                $TipoPedidoCultura  = JText::_('COM_VIRTUALDESK_CULTURA_HEADING');
                $TipoPedidoTickets  = JText::_('COM_VIRTUALDESK_TICKETS_HEADING');


                // Tag Módulos
                $ModuloTagAlerta  =  self::ModuloTagAlerta;
                $ModuloTagAgenda  =  self::ModuloTagAgenda;
                $ModuloTagCultura =  self::ModuloTagCultura;
                $ModuloTagTickets =  self::ModuloTagTickets;

                $table  = " (  ";
                $table .= " SELECT  id, idAll, dummy,  codigo,  titulo, categoria, category_name, estado, idestado, tipo_pedido, tipo_pedido_desc, created, modified, createdFull, tagmodulo, idcategoria FROM ( ";

                $vbCheckFirstUnion = true; // detecta qual o primeiro union
                // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                $vbChecModuleAlerta = $objCheckPerm->loadModuleEnabledByTag('alerta');
                if((int)$vbChecModuleAlerta==1) {
                    // Alerta
                    $table .= " SELECT a.Id_alerta as id, CONCAT('ALRT',CONVERT(a.Id_alerta , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Alerta . "', CAST(a.Id_alerta as CHAR(10))) as dummy , a.codOco as codigo ,  CONCAT(LEFT (a.descricao,50),'...') as titulo";
                    $table .= " , IFNULL(b." . $CatName . ",' ') as categoria , IFNULL(b." . $CatName . ",' ') as category_name , IFNULL(e." . $EstadoName . ",' ') as estado , CONCAT('ALRT',CONVERT(a.estado, CHAR(50))) as idestado , 'ALRT' as tipo_pedido, '".$TipoPedidoAlerta."' as tipo_pedido_desc ";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " , '".$ModuloTagAlerta."' as tagmodulo , CONCAT('ALRT',CONVERT(a.categoria, CHAR(50))) as idcategoria";
                    $table .= " FROM " . $dbprefix . "virtualdesk_alerta as a ";
                    $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_alerta_categoria AS b ON b.ID_categoria = a.categoria ";
                    $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_alerta_estados AS e ON e.id_estado = a.estado ";
                    $table .= " WHERE (a.nif=$UserSessionNIF) ";

                    $vbCheckFirstUnion = false;
                }

                // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('agenda');
                if((int)$vbChecModuleAgenda==1) {
                    if($vbCheckFirstUnion===false) $table .= " UNION ";
                    // Agenda
                    $table .= " SELECT a.id as id, CONCAT('AGND',CONVERT(a.id , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Agenda . "', CAST(a.id as CHAR(10))) as dummy , a.referencia  as codigo ,  CONCAT(LEFT (a.nome_evento,50),'...') as titulo ";
                    $table .= " , IFNULL(b." . $CatNameAgenda . ",' ') as categoria , IFNULL(b." . $CatNameAgenda . ",' ') as category_name , IFNULL(e." . $EstadoNameAgenda . ",' ') as estado , CONCAT('AGND',CONVERT(a.estado_evento, CHAR(50))) as idestado , 'AGND' as tipo_pedido, '".$TipoPedidoAgenda."' as tipo_pedido_desc ";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " , '".$ModuloTagAgenda."' as tagmodulo , CONCAT('AGND',CONVERT(a.categoria, CHAR(50))) as idcategoria ";
                    $table .= " FROM " . $dbprefix . "virtualdesk_v_agenda_eventos as a ";
                    $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_v_agenda_categoria AS b ON b.id = a.categoria ";
                    $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_v_agenda_estado AS e ON e.id = a.estado_evento ";
                    $table .= " WHERE (a.nif=$UserSessionNIF) ";

                    $vbCheckFirstUnion = false;
                }

                // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                $vbChecModuleCultura = $objCheckPerm->loadModuleEnabledByTag('cultura');
                if((int)$vbChecModuleCultura==1) {
                    if($vbCheckFirstUnion===false) $table .= " UNION ";
                    // Cultura
                    $table .= " SELECT a.id_evento as id, CONCAT('CULT',CONVERT(a.id_evento , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Cultura . "', CAST(a.id_evento as CHAR(10))) as dummy , a.referencia_evento  as codigo ,  CONCAT(LEFT (a.nome_evento,50),'...') as titulo ";
                    $table .= " , IFNULL(b." . $CatNameCultura . ",' ') as categoria , IFNULL(b." . $CatNameCultura . ",' ') as category_name , IFNULL(e." . $EstadoNameCultura . ",' ') as estado , CONCAT('CULT',CONVERT(a.estado, CHAR(50))) as idestado , 'CULT' as tipo_pedido, '".$TipoPedidoCultura."' as tipo_pedido_desc ";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " , '".$ModuloTagCultura."' as tagmodulo , CONCAT('CULT',CONVERT(a.cat_evento, CHAR(50))) as idcategoria ";
                    $table .= " FROM ".$dbprefix."virtualdesk_Cultura_Eventos as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Categoria AS b ON b.id = a.cat_evento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Cultura_Estado AS e ON e.id_estado = a.estado ";
                    $table .= " WHERE (a.nif=$UserSessionNIF) ";

                    $vbCheckFirstUnion = false;
                }

                // Para o caso de o módulo não estar ativo, nem faz a pesquisa
                $vbChecModuleTickets = $objCheckPerm->loadModuleEnabledByTag('tickets');
                if((int)$vbChecModuleTickets==1) {
                    if($vbCheckFirstUnion===false) $table .= " UNION ";
                    // Cultura
                    $table .= " SELECT a.id as id, CONCAT('TICK',CONVERT(a.id , CHAR(50))) as idAll , CONCAT('" . $dummyHRef_Tickets . "', CAST(a.id as CHAR(10))) as dummy , a.referencia  as codigo ,  CONCAT(LEFT (a.assunto,50),'...') as titulo ";
                    $table .= " , IFNULL(b." . $DepNameTickets . ",' ') as categoria , IFNULL(b." . $DepNameTickets . ",' ') as category_name , IFNULL(e." . $EstadoNameTickets . ",' ') as estado , CONCAT('TICK',CONVERT(a.id_estado, CHAR(50))) as idestado , 'TICK' as tipo_pedido, '".$TipoPedidoTickets."' as tipo_pedido_desc ";
                    $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_alteracao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                    $table .= " , '".$ModuloTagTickets."' as tagmodulo , CONCAT('TICK',CONVERT(a.id_departamento, CHAR(50))) as idcategoria ";
                    $table .= " FROM ".$dbprefix."virtualdesk_tickets as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tickets_departamento AS b ON b.id = a.id_departamento ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_tickets_estado AS e ON e.id = a.id_estado ";
                    $table .= " WHERE (a.nif=$UserSessionNIF) ";

                    $vbCheckFirstUnion = false;
                }

                $table .= " ) as allUnion ";

                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;
                $table .= $setLimitSQL;
                $table .= " ) temp ";

                $primaryKey = 'idAll';

                $columns = array(
                    array( 'db' => 'created',          'dt' => 0 ),
                    array( 'db' => 'tipo_pedido_desc', 'dt' => 1 ),
                    array( 'db' => 'titulo',           'dt' => 2 ),
                    array( 'db' => 'categoria',        'dt' => 3 ),
                    array( 'db' => 'estado',           'dt' => 4 ),
                    array( 'db' => 'codigo',           'dt' => 5 ),
                    array( 'db' => 'dummy',            'dt' => 6 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',         'dt' => 7 ),
                    array( 'db' => 'id',               'dt' => 8 ,'formatter'=>'VALUE_ENCRYPT'),
                    array( 'db' => 'modified',         'dt' => 9 ),
                    array( 'db' => 'createdFull',      'dt' => 10 ),
                    array( 'db' => 'idAll',            'dt' => 11 ,'formatter'=>'VALUE_ENCRYPT'),
                    array( 'db' => 'tipo_pedido',      'dt' => 12 ),
                    array( 'db' => 'tagmodulo',        'dt' => 13 ),
                    array( 'db' => 'idcategoria',      'dt' => 14 )

                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = array('data');
                if((int)$vbChecModuleAlerta==0 && (int)$vbChecModuleAgenda==0) return($data);

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;


            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista com os TODOS os estados */
        public static function getModuloAllOptions ()
        {
            /*
            * Check PERMISSÕES
            */
          /* $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('cultura');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }*/

            try
            {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = 'nome';
                }
                else {
                    $rowName = 'nome';
                }
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id','tagchave',$rowName.' as name') )
                    ->from("#__virtualdesk_perm_modulo")
                    ->where(" enabled2dropbox=1 ")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->tagchave,
                        'name' => $row->name
                    );
                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

    }
?>