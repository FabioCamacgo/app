<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskTableUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/user.php');
JLoader::register('VirtualDeskTablePermGroupsUsers', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/perm_groupsusers.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskUserActivationHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuseractivation.php');
//JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');


/**
 * VirtualDesk User helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteUserHelper extends VirtualDeskUserHelper
{

    /*
    * Retorna tabela com dados do utilizador VD atual da sessão ou erro se não existir
    */
    public static function getUserFromJoomlaID ($UserJoomlaID)
    {
        //$table = JTable::getInstance('User', 'VirtualDeskTable');
        $db = JFactory::getDbo();
        // Optei por este formato conforme vi no breez, ao definirmos atributos ficam assim visiveis nesta parte do código.
        $table = new VirtualDeskTableUser($db);

        try
        {
            $table->load(array('idjos'=>$UserJoomlaID));

            if((integer)$table->idjos !== (integer)$UserJoomlaID  ) return false;

            $dataReturn = new stdClass();
            $dataReturn->id    = $table->id;
            $dataReturn->idjos = $table->idjos;
            $dataReturn->login = $table->login;
            $dataReturn->username  = $table->login;
            $dataReturn->name  = $table->name;
            $dataReturn->email = $table->email;
            $dataReturn->email1 = $table->email;
            $dataReturn->email2 = $table->email;
            $dataReturn->registerDate = $table->created;
            $dataReturn->lastvisitDate = null;
            $dataReturn->address = $table->address;
            $dataReturn->postalcode = $table->postalcode;
            $dataReturn->civilid = $table->civilid;
            $dataReturn->fiscalid = $table->fiscalid;
            $dataReturn->useravatar = $table->useravatar;

            $dataReturn->firma = $table->firma;
            $dataReturn->designacaocomercial = $table->designacaocomercial;
            $dataReturn->sectoratividade = $table->sectoratividade;
            $dataReturn->caeprincipal = $table->caeprincipal;
            $dataReturn->phone1 = $table->phone1;
            $dataReturn->phone2 = $table->phone2;
            $dataReturn->website = $table->website;
            $dataReturn->facebook = $table->facebook;
            $dataReturn->emailsec = $table->emailsec;
            $dataReturn->concelho = $table->concelho;
            $dataReturn->freguesia = $table->freguesia;
            $dataReturn->localidade = $table->localidade;
            $dataReturn->areaact = $table->areaact;
            $dataReturn->managername = $table->managername;
            $dataReturn->managerposition = $table->managerposition;
            $dataReturn->managercontact = $table->managercontact;
            $dataReturn->manageremail = $table->manageremail;
            $dataReturn->maplatlong = $table->maplatlong;

            $dataReturn->cargo  = $table->cargo;
            $dataReturn->funcao = $table->funcao;

            $dataReturn->veracidadedados  = $table->veracidadedados;
            $dataReturn->politicaprivacidade = $table->politicaprivacidade;


            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Retorna o ID do utilizador atual da sessão ou erro se não existir
    */
    public static function getUserVDIdFromJoomlaID ($UserJoomlaID)
    {
        //$table = JTable::getInstance('User', 'VirtualDeskTable');
        $db = JFactory::getDbo();
        // Optei por este formato conforme vi no breez, ao definirmos atributos ficam assim visiveis nesta parte do código.
        $table = new VirtualDeskTableUser($db);

        try
        {
            $table->load(array('idjos'=>$UserJoomlaID));
            if((integer)$table->idjos !== (integer)$UserJoomlaID  ) return false;
            $dataReturn = new stdClass();
            $dataReturn->id    = $table->id;

            return($dataReturn->id);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getUserTableFromJoomlaID ($UserJoomlaID)
    {
        $db = JFactory::getDbo();
        // Optei por este formato conforme vi no breez, ao definirmos atributos ficam assim visiveis nesta parte do código.
        $table = new VirtualDeskTableUser($db);

        try
        {
            $table->load(array('idjos'=>$UserJoomlaID));
            if((integer)$table->idjos !== (integer)$UserJoomlaID  ) return false;
            return($table);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getJoomlaIDFromVDUserId ($VDUserID)
    {
        //$table = JTable::getInstance('User', 'VirtualDeskTable');
        $db = JFactory::getDbo();
        // Optei por este formato conforme vi no breez, ao definirmos atributos ficam assim visiveis nesta parte do código.
        $table = new VirtualDeskTableUser($db);

        try
        {
            $table->load(array('id'=>$VDUserID));
            if((integer)$table->id !== (integer)$VDUserID  ) return false;
            $dataReturn = new stdClass();
            $dataReturn->idjos    = $table->idjos;

            return($dataReturn->idjos);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Obtém o utilizador VirtualDesk a partir do ID do utilizador do joomla
    * @param recebe o user State Id  para comparar
    */
//    public static function getUserSessionId($userStateId)
    public static function getUserSessionId()
    {
        $userSession   = JFactory::getUser(); // Get the user object $app =

        if( ((integer)$userSession->id == 0) || ($userSession->guest) )
        {
           return(-1);
        }
//        elseif((integer)$userSession->id == (integer)$userStateId)
     elseif((integer)$userSession->id >0 )
        {
            return($userSession->id);
        }
        else
        { return(-1); }

    }


    /*
    * Verifica se o utilizador joomla está Enabled ou seja not blocked and active
    * @param $UserJoomlaID
    */
    public static function checkUserJoomlaIsEnabled($UserJoomlaID)
    {
        // get joomla user
        $josUser = new JUser($UserJoomlaID);

        if( ((integer)$josUser->id > 0) && ((integer)$josUser->block == 0) && ((string)$josUser->activation == '') )
        {
          return(true);
        }
        else
        { return(false); }

    }


    /*
   * Verifica se o utilizador VirtualDesk está Enabled ou seja not blocked and active
   * @param $UserJoomlaID
   */
    public static function checkUserVirtualDeskIsEnabled($UserJoomlaID)
    {
        $db = JFactory::getDbo();
        $db->setQuery("Select id From #__virtualdesk_users Where idjos =" . $db->escape($UserJoomlaID). " and blocked=0 and activated=1 Limit 1" );
        $exists =  $db->loadResult();
        if($exists)
        {
            return(true);
        }
        else
        { return(false); }

    }


    /*
      * Check if the username exists for another user (in Joomla and in VD)
      * Por exemplo no caso de queremos alterar o username atual
      */
    public static function checkIfUserNameisDuplicate($UserJoomlaID, $Username)
    {
        if((string)$Username=='') return false;

        // Verifica se o username existe noutro utilizador no joomla...
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__users")
            ->where($db->quoteName('username').'=\''.$db->escape($Username).'\' AND '. $db->quoteName('id').'<>'.$db->escape($UserJoomlaID) .' Limit 1' )
        );
        $isDuplicate = $db->loadResult();

        if((int)$isDuplicate > 0 && (int)$isDuplicate<>(int)$UserJoomlaID) return true;


        // Verifica se o username existe noutro utilizador no VirtualDesk users...
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('idjos'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('login').'=\''.$db->escape($Username).'\' AND '. $db->quoteName('idjos').'<>'.$db->escape($UserJoomlaID) .' Limit 1' )
        );
        $isDuplicate = $db->loadResult();

        if((int)$isDuplicate > 0 && (int)$isDuplicate<>(int)$UserJoomlaID) return true;



        // else
        return false;
    }



    /*
      * Check if the username exists for another user (in Joomla and in VD)
      * Por exemplo no caso de queremos alterar o username atual
      */
    public static function checkIfEmailisDuplicate($UserJoomlaID, $Email)
    {
        if((string)$Email=='') return false;

        // Verifica se o username existe noutro utilizador no joomla...
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' AND '. $db->quoteName('id').'<>'.$db->escape($UserJoomlaID) .' Limit 1' )
        );
        $isDuplicate = $db->loadResult();

        if((int)$isDuplicate > 0 && (int)$isDuplicate<>(int)$UserJoomlaID) return true;


        // Verifica se o username existe noutro utilizador no VirtualDesk users...
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('idjos'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' AND '. $db->quoteName('idjos').'<>'.$db->escape($UserJoomlaID) .' Limit 1' )
        );
        $isDuplicate = $db->loadResult();

        if((int)$isDuplicate > 0 && (int)$isDuplicate<>(int)$UserJoomlaID) return true;

        return false;
    }



    public static function updateUserAlerta($UserJoomlaID, $data)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $fields = array(
            $db->quoteName('descritivoBO') . ' = ' . $db->quote($data['descritivoBO']),
            $db->quoteName('observacoes') . ' = ' . $db->quote($data['observacoes'])
        );


        $conditions = array(
            $db->quoteName('Id_alerta') . ' = ' . $db->quote($data['alerta_id'])
        );

        $query->update($db->quoteName('#__virtualdesk_alerta'))->set($fields)->where($conditions);

        $db->setQuery($query);

        $result = $db->execute();

        return ($result);
    }




    /*
    * Faz a gravação de um "profile" de um utilizador (update) para no joomla users como no VD users
    * valida alguns dados e se falhar alguma coisa reverte os dados anteriores
    */
    public static function updateUserProfile($UserJoomlaID, $data, $bitFromPermAdmin=false)
    {
        if($bitFromPermAdmin==true) {

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
            if($vbInAdminGroup===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
                return false;
            }
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userfulledit'); // verifica permissão acesso ao layout para editar
            if($vbHasAccess===false || $vbHasAccess2===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $getInputPermUser_id = $data['permadmin_user_id'];
            if( (int)$getInputPermUser_id<=0 ) return false;
            $UserJoomlaID = VirtualDeskSiteUserHelper::getJoomlaIDFromVDUserId($getInputPermUser_id);
        }

        // Carrega dados atuais na base de dados para da tabela VDUsers
        $db    = JFactory::getDbo();
        $origemTableVDUser = new VirtualDeskTableUser($db);
        $origemTableVDUser->load(array('idjos'=>$UserJoomlaID));
        if((integer)$origemTableVDUser->idjos !== (integer)$UserJoomlaID  ) return false;
        $updateTableVDUser  = clone  $origemTableVDUser;

        //Carrega dados atuais do user joomla associado
        $origemTableJosUser = JFactory::getUser($UserJoomlaID);
        if( (!isset($origemTableJosUser->id)) || (intval($origemTableJosUser->id)==0) ) return false;
        $updateTableJosUser = clone $origemTableJosUser;

        // antes deve ser verificado (ex profile->model->save) se o utilizador estiver bloqueado ou por ativar não deve ser permitido fazer alterações
        //Preparação do campo de  email: 'email1' -> 'email'
        if( !empty($data['email1']) )  $data['email'] = JStringPunycode::emailToPunycode($data['email1']);

        //UPDATE: Joomla User
        $dataJosUser = array();

        // Verifica qual o campo a utilizar no nome do utilizador
        $nameofuser_fieldname = self::getNameofUserField($data, $origemTableVDUser, $origemTableJosUser);
        if( $nameofuser_fieldname !== true && !empty($data[$nameofuser_fieldname]) )  {
            $dataJosUser['name'] = $data[$nameofuser_fieldname];
        }
        elseif( (string)$nameofuser_fieldname === 'login' ) {
            $dataJosUser['name'] = $updateTableVDUser->login;
        }

        if( !empty($data['username']) )  $dataJosUser['username'] = $data['username'];
        if( !empty($data['email']) )     $dataJosUser['email']    = $data['email'];
        if( !empty($data['password1']) )
        { $dataJosUser['password']  = $data['password1']; // set the password
            $dataJosUser['password2'] = $data['password2']; // confirm the password
        }
        if( !empty($dataJosUser) ) {
            if (!$updateTableJosUser->bind($dataJosUser)) return false;
            if (!$updateTableJosUser->save()) return false;
        }
        // Limpa os dados atualizados no joomla user
        unset($dataJosUser);
        unset($data['password1']);
        unset($data['password2']);

        //UPDATE: VD User
        // Se tem um ficheiro de foto associado então...  Retira o "files" do $data , faz o upload da foto e coloca no bind
        $AvatarPreviousFileName = '';
        $avatarFileName         = VirtualDeskSiteFileUploadHelper::saveUserAvatarFileToFolder($UserJoomlaID, $data['useravatar']);
        if($avatarFileName === false) {
            $data['useravatar'] = '';
            return false;
        }
        else if((string)$avatarFileName != '') {
            $AvatarPreviousFileName = $updateTableVDUser->useravatar;
        }
        unset($data['useravatar']);
        if((string)$avatarFileName != '')  $data['useravatar'] =  $avatarFileName;


        $dataReturnAreaAct = self::saveUserAreaAct ($UserJoomlaID, $origemTableVDUser->id, $data);
        if($dataReturnAreaAct===false) return false;
        if($dataReturnAreaAct !==true ) $data['areaact'] = (string)$dataReturnAreaAct;


        // Bind the data.
        $resVDUserBind = $updateTableVDUser->bind($data);
        if (!$resVDUserBind)
        {   // Reverte gravação anterior do joomla user associado
            $origemTableJosUser->save();
            return false;
        }
        // Store the data.
        $resVDUserSave = $updateTableVDUser->save($data);
        if (!$resVDUserSave)
        {	// Reverte gravação anterior do joomla user associado
            $origemTableJosUser->save();
            return false;
        }


        // se gravou tudo ok e se alterou o email para um novo e o user é um VD USER (não é admin) então volta a ter um processo de ativação (está configurado SIm ou Não)
        if( (string)$data['email']!='' )
        {
            $newActivationParam = JComponentHelper::getParams('com_virtualdesk')->get('activationwhenupdateemail');
            if($newActivationParam == '1')
            {
                // Set the confirmation token.
                $token              = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
                $UserActivation     = new VirtualDeskUserActivationHelper();
                $SaveUserActivation = $UserActivation->SetUserForActivation($updateTableVDUser, $updateTableJosUser, $token);
                if($SaveUserActivation==false)
                { // Repõe os dados iniciais
                    $origemTableJosUser->save();
                    // o save é um bind+check+store, neste caso queremos guardar o que existe já na row carregada
                    $origemTableVDUser->store();
                    return false;
                }
                $resSendActivatiobEmail = $UserActivation->SendActivationEmail($updateTableJosUser, $token);
                if($resSendActivatiobEmail==false)
                { // Repõe os dados iniciais
                    $origemTableJosUser->save();
                    // o save é um bind+check+store, neste caso queremos guardar o que existe já na row carregada
                    $origemTableVDUser->store();
                    return false;
                }


                // Foi alterado o Pedido do tipo Contact Us
                $vdlog = new VirtualDeskLogHelper();
                $eventdata                  = array();
                $eventdata['iduser']        = $origemTableVDUser->id;
                $eventdata['idjos']         = $UserJoomlaID;
                $eventdata['title']         = JText::_('COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_TITLE');
                $eventdata['desc']          = self::geEventLogMailDescForUserActivation ($origemTableVDUser->id, 'COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_TITLE' );
                $eventdata['category']      = JText::_('COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);


                return 'forcelogout';
            }
        }

        // Se correu tudo bem e alterou o ficheiro da foto/avatar pode eliminar o ficheiro anterior
        if( $AvatarPreviousFileName!=='') VirtualDeskSiteFileUploadHelper::deleteUserAvatarFile ($AvatarPreviousFileName);


        // Log
        $vdlog = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['iduser']        = $origemTableVDUser->id;
        $eventdata['idjos']         = $UserJoomlaID;
        $eventdata['title']         = JText::_('COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_TITLE');
        //$eventdata['desc']          = self::geEventLogMailDesc ($origemTableVDUser->id, 'COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_TITLE' );

        $getTableVDUser = new VirtualDeskTableUser($db);
        $getTableVDUser->load(array('idjos'=>$UserJoomlaID));
        $eventdata['desc']          = self::getEventLogMailDesc ($getTableVDUser, 'COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_TITLE' );

        $eventdata['category']      = JText::_('COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;
    }



    /*
    * Verifica se foi feito submit do formulário de registo
    */
    public static function checkNewUserRegistrationFormData()
    {
        // Check for request forgeries.
        if(!JSession::checkToken()) return(false);
        $app          = JFactory::getApplication();
        $newRegSubmit = $app->input->get('newRegSubmit',null, 'STRING');
        if((string)$newRegSubmit != 'newRegSubmit') return(false);
        return(true);

    }


    /*
    * Grava o registo de um novo utilizador
    */
    public static function saveNewUserRegistration($data)
    {
        //Create New: Joomla User
        $newJosUser = new JUser;
        $dataJosUser = array();

        // Verifica qual o campo a utilizar no nome do utilizador
        $nameofuser_fieldname = self::getNameofUserField($data, null, null);

        if( !empty($data[$nameofuser_fieldname]) )  {
            $dataJosUser['name'] = $data[$nameofuser_fieldname];
        }
        elseif( $nameofuser_fieldname === 'login' ) {
            $dataJosUser['name'] = $data['username'];
        }

        if( !empty($data['username']) )  $dataJosUser['username'] = $data['username'];
        if( !empty($data['email']) )     $dataJosUser['email']    = $data['email'];
        if( !empty($data['password1']) )
        { $dataJosUser['password']  = $data['password1']; // set the password
            $dataJosUser['password2'] = $data['password2']; // confirm the password
        }

        // Grupo por defeito: virtual auser
        $virtualDeskGroupUserId = VirtualDeskUserHelper::getVirtualDeskGroupUserId();
        if(!empty($virtualDeskGroupUserId)) $dataJosUser['groups'] = array($virtualDeskGroupUserId);
        // Bloqueio por defeito...
        $dataJosUser['block']  = 1; // block the User

        if( !empty($dataJosUser) ) {
            if (!$newJosUser->bind($dataJosUser)) return false;
            if (!$newJosUser->save()) return false;
        }
        // Limpa os dados atualizados no joomla user
        unset($dataJosUser);
        unset($data['password1']);
        unset($data['password2']);

        // Jos user ID - adiciona o Id criado ao "data para que seja mapeado no novo VD user
         if((int)$newJosUser->id > 0 ) {
             $data['idjos'] = $newJosUser->id;
         }
        else {
             $newJosUser->delete();
            return false;
        }



        //Create new VD User
        $db    = JFactory::getDbo();
        $newVDUser = new VirtualDeskTableUser($db);
        // Bind the data.
        $resVDUserBind = $newVDUser->bind($data);
        if (!$resVDUserBind)
        {   // ELIMINA o joomla user criado antes
            $newJosUser->delete();
            return false;
        }
        // Store the data.
        $resVDUserSave = $newVDUser->save($data);
        if (!$resVDUserSave)
        {	// ELIMINA o joomla user criado antes
            $newJosUser->delete();
            return false;
        }



        // Associa o novo utilizador por defeito no grupo base de permissões da App
        // Anteriormente já foi definido o grupo base do joomla
        $db    = JFactory::getDbo();
        $newTablePermGroup = new VirtualDeskTablePermGroupsUsers($db);
        $dataPermGroup                 = array();
        $dataPermGroup['iduserjos']    = $newJosUser->id;
        $dataPermGroup['iduser']       = $newVDUser->id;
        $setdefaultusergroupinsideperm = JComponentHelper::getParams('com_virtualdesk')->get('setdefaultusergroupinsideperm');
        $dataPermGroup['idpermgroup']  = intval($setdefaultusergroupinsideperm);

        // Bind the data.
        $newTablePermGroup->bind($dataPermGroup);
        // Store the data.
        $newTablePermGroup->save($dataPermGroup);



        // parametro que verifica se deve enviar email sempre que for criado um utilizador...
        $newActivationParam = JComponentHelper::getParams('com_virtualdesk')->get('activationwhennewregistration');
        if($newActivationParam == '1')
            {
                // Set the confirmation token.
                $token              = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
                $UserActivation     = new VirtualDeskUserActivationHelper();
                $SaveUserActivation = $UserActivation->SetUserForActivation($newVDUser, $newJosUser, $token);
                if($SaveUserActivation==false)
                {   // ELIMINA o joomla user criado antes
                    $newJosUser->delete();
                    $newVDUser->delete();
                    return false;
                }
                $resSendActivatiobEmail = $UserActivation->SendActivationEmail($newJosUser, $token);
                if($resSendActivatiobEmail==false)
                { // Repõe os dados iniciais
                    $newJosUser->delete();
                    $newVDUser->delete();
                    return false;
                }
                JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_SUCESS_EMAIL'), 'message');

                // event log
                $vdlog                      = new VirtualDeskLogHelper();
                $eventdata                  = array();
                $eventdata['iduser']        = $newVDUser->id;
                $eventdata['idjos']         = $newJosUser->id;
                $eventdata['title']         = JText::_('COM_VIRTUALDESK_EVENTLOG_NEWREGISTRATION_TITLE');
                $eventdata['desc']          = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_NEWREGISTRATION_DESC',$newVDUser->email);
                $eventdata['category']      = JText::_('COM_VIRTUALDESK_EVENTLOG_NEWREGISTRATION_CAT');
                $eventdata['sessionuserid'] = JFactory::getUser()->id;
                $vdlog->insertEventLog($eventdata);

                return true;
            }
        // Ficou registado sem ser necessário ativação por email
        JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_NEWREGISTRATION_SUCESS_NOEMAIL'), 'message');

        // event log
        $vdlog                      = new VirtualDeskLogHelper();
        $eventdata                  = array();
        $eventdata['iduser']        = $newVDUser->id;
        $eventdata['idjos']         = $newJosUser->id;
        $eventdata['title']         = JText::_('COM_VIRTUALDESK_EVENTLOG_NEWREGISTRATION_TITLE');
        $eventdata['desc']          = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_NEWREGISTRATION_DESC',$newVDUser->email);
        $eventdata['category']      = JText::_('COM_VIRTUALDESK_EVENTLOG_NEWREGISTRATION_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);

        return true;
    }

    /*
        * Inicializa o objecto "limpo" de a submissão de um formulário
        */
    public static function getNewRegistrationCleanPostedData()
    {
        $data = array();
        $data['login']     = null;
        $data['username']  =  null;
        $data['name']   =  null;
        $data['email']  =  null;
        $data['email1'] =  null;
        $data['email2'] =  null;
        $data['registerDate'] =  null;
        $data['lastvisitDate'] = null;
        $data['address'] = null;
        $data['postalcode'] = null;
        $data['civilid'] =  null;
        $data['fiscalid'] = null;

        $data['firma']     = null;
        $data['designacaocomercial']  =  null;
        $data['sectoratividade']   =  null;
        $data['caeprincipal']  =  null;
        $data['phone1'] =  null;
        $data['phone2'] =  null;
        $data['website'] =  null;
        $data['facebook'] = null;
        $data['emailsec'] = null;
        $data['concelho'] = null;
        $data['freguesia'] =  null;
        $data['localidade'] = null;
        $data['areaact'] =  null;
        $data['managername'] =  null;
        $data['managerposition'] = null;
        $data['managercontact'] = null;
        $data['manageremail'] = null;
        $data['maplatlong'] = null;

        $data['cargo']  = null;
        $data['funcao'] = null;

        $data['veracidadedados']  = null;
        $data['politicaprivacidade'] = null;

        return($data);
    }


    /*
    * Inicializa o array com dados se não existirem
    * Útil pra o caso de ter sido feito um post e não forma preenchidos alguns campos
    * Pode acontecer no caso de falha do javascript. Assim garantimos que depois o facto do o indice do campos não estar definido não "rebenta" o php
    */
    public static function getNewRegistrationSetIfNullPostData($data)
    {
        if( !is_array($data) )$data = array();
        if( empty($data['login']) )          $data['login']     = null;
        if( empty($data['username']) )       $data['username']  =  null;
        if( empty($data['name']) )           $data['name']   =  null;
        if( empty($data['email']) )          $data['email']  =  null;
        if( empty($data['email1']) )         $data['email1'] =  null;
        if( empty($data['email2']) )         $data['email2'] =  null;
        if( empty($data['registerDate']) )   $data['registerDate'] =  null;
        if( empty($data['lastvisitDate']) )  $data['lastvisitDate'] = null;
        if( empty($data['address']) )        $data['address'] = null;
        if( empty($data['postalcode']) )     $data['postalcode'] = null;
        if( empty($data['civilid']) )        $data['civilid'] =  null;
        if( empty($data['fiscalid']) )       $data['fiscalid'] = null;

        if( empty($data['firma']) )                $data['firma']     = null;
        if( empty($data['designacaocomercial']) )  $data['designacaocomercial']  =  null;
        if( empty($data['sectoratividade']) )      $data['caeprincipal']   =  null;
        if( empty($data['phone1']) )               $data['phone1']  =  null;
        if( empty($data['phone2']) )               $data['phone2'] =  null;
        if( empty($data['website']) )              $data['website'] =  null;
        if( empty($data['facebook']) )             $data['facebook'] =  null;
        if( empty($data['emailsec']) )             $data['emailsec'] = null;
        if( empty($data['concelho']) )             $data['concelho'] = null;
        if( empty($data['freguesia']) )            $data['freguesia'] = null;
        if( empty($data['localidade']) )           $data['localidade'] =  null;
        if( empty($data['areaact']) )              $data['areaact'] = null;
        if( empty($data['managername']) )          $data['managername'] =  null;
        if( empty($data['managerposition']) )      $data['managerposition'] = null;
        if( empty($data['managercontact']) )       $data['managercontact'] = null;
        if( empty($data['manageremail']) )         $data['manageremail'] = null;
        if( empty($data['maplatlong']) )           $data['maplatlong'] = null;

        if( empty($data['cargo']) )                $data['cargo'] = null;
        if( empty($data['funcao']) )               $data['funcao'] = null;

        if( empty($data['veracidadedados']) )     $data['veracidadedados'] = null;
        if( empty($data['politicaprivacidade']))  $data['politicaprivacidade'] = null;

        return($data);
    }


    /*
  * Verifica se foi feito submit do formulário de registo
  */
    public static function checkForgotPasswordFormData()
    {
        // Check for request forgeries.
        if(!JSession::checkToken()) return(false);
        $app          = JFactory::getApplication();
        $newRegSubmit = $app->input->get('newForgotPassword',null, 'STRING');
        if((string)$newRegSubmit != 'newForgotPassword') return(false);
        return(true);

    }


    /*
    * Inicializa o objecto "limpo" de a submissão de um formulário
    */
    public static function getForgotPasswordCleanPostedData()
    {
        $data = array();
        $data['email1'] =  null;
        return($data);
    }

    /*
        * Inicializa o array com dados se não existirem
        * Útil pra o caso de ter sido feito um post e não forma preenchidos alguns campos
        * Pode acontecer no caso de falha do javascript. Assim garantimos que depois o facto do o indice do campos não estar definido não "rebenta" o php
        */
    public static function getForgotPasswordSetIfNullPostData($data)
    {
        if( !is_array($data) )$data = array();
        if( empty($data['email1']) )         $data['email1'] =  null;
        return($data);
    }


    /*
   * Grava o pedido de nova password
   */
    public static function saveForgotPasswordRequest($data)
    {
        $arrUsersID = VirtualDeskSiteUserHelper::getForgotPasswordEmailUserID($data['email1']);
        if( !is_array($arrUsersID) ) return false;

        $vdlog = new VirtualDeskLogHelper();

        // Carrega dados atuais na base de dados para da tabela VDUsers
        $db        = JFactory::getDbo();
        $updateVDUser = new VirtualDeskTableUser($db);
        $updateVDUser->load(array('id'=>$arrUsersID['id']));

        //Carrega dados atuais do user joomla associado
        $updateTableJosUser = JFactory::getUser($arrUsersID['idjos']);

        // Grava a nova senha no JosUSER
        $dataJosUser = array();
        $newGeneratedPassword     = VirtualDeskUserHelper::randomPassword();
        $dataJosUser['password']  = $newGeneratedPassword;
        $dataJosUser['password2'] = $dataJosUser['password']; // confirm the password

        if( !empty($dataJosUser) ) {
            if (!$updateTableJosUser->bind($dataJosUser)) return false;
            if (!$updateTableJosUser->save()) return false;
        }


        // parametro que verifica se deve enviar emial com link para ativação ao ser requerida nova senha
        $newActivationParam = JComponentHelper::getParams('com_virtualdesk')->get('activationwhenforgotpassword');
        if($newActivationParam == '1')
        {
            // Set the confirmation token.
            $token              = JApplicationHelper::getHash(JUserHelper::genRandomPassword());
            $UserActivation     = new VirtualDeskUserActivationHelper();
            $SaveUserActivation = $UserActivation->SetUserForActivation($updateVDUser, $updateTableJosUser, $token);
            if($SaveUserActivation==false)
            {   // TODO
                return false;
            }
//            $resSendActivatiobEmail = $UserActivation->SendActivationEmail($updateTableJosUser, $token);
            $resSendActivatiobEmail = VirtualDeskUserHelper::SendForgotPasswordEmail($updateTableJosUser,$newGeneratedPassword,$token);
            if($resSendActivatiobEmail==false)
            {  // TODO
                return false;
            }
            JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_SUCESS_EMAIL'), 'message');
            return true;
        }

        // Vai ser enviada a nova senha sem ser necessário ativação por email
        $resSendEmail = VirtualDeskUserHelper::SendForgotPasswordEmail($updateTableJosUser,$newGeneratedPassword,'');
        if($resSendEmail==false) return false;

        // Send Email
        JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_SUCESS_NOEMAIL'), 'message');

        // Conseguimos ativar o utilizar então regista no event log do utilizador
        $eventdata           = array();
        $eventdata['iduser']    = $arrUsersID['id'];
        $eventdata['idjos']     = $arrUsersID['idjos'];
        $eventdata['title']     = JText::_('COM_VIRTUALDESK_EVENTLOG_FORGOTPASSWORD_TITLE');
        //$eventdata['desc']      = JText::_('COM_VIRTUALDESK_EVENTLOG_FORGOTPASSWORD_DESC');
        $eventdata['desc']      = JText::sprintf('COM_VIRTUALDESK_EVENTLOG_FORGOTPASSWORD_BY',$updateVDUser->login,$updateVDUser->id);
        $eventdata['category']  = JText::_('COM_VIRTUALDESK_EVENTLOG_FORGOTPASSWORD_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $eventdata['accesslevel']   =  JText::_('COM_VIRTUALDESK_EVENTLOG_LEVEL_USERACCESS');
        $vdlog->insertEventLog($eventdata);

        return true;
    }



    /*
     * Processo de recuperação de senha
     *
     * Verifica se o email enviado pelo formulário está associado ao user VD e ao respectivo JosUser
     *
     */
    public static function checkForgotPasswordEmailExists($Email)
    {
        if((string)$Email=='') return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' Limit 1' )
        );
        $IdJosUser = $db->loadResult();
        if((int)$IdJosUser <= 0) return false;

        // Verifica o email na VD Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('idjos','id'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' AND ' . $db->quoteName('idjos') . '=' . $db->escape($IdJosUser) . ' Limit 1' )
        );
        $VDUserRow = $db->loadAssoc();
        if(!is_array($VDUserRow)) return false;
        if((int)$VDUserRow['id'] <= 0 || (int)$IdJosUser<>(int)$VDUserRow['idjos']) return false;

        return true;
    }


    /*
    * Processo de recuperação de senha
    *
    * Verifica se o email enviado pelo formulário está associado ao user VD e ao respectivo JosUser
    *      - O user (VD+Jos) não deve estar bloqueado;
    *      - Ou então se estiver bloqueado, tem de ter um processo de ativação a decorrer mas deve ter sido ativado pelo menos 1 vez;
    *
    */
    public static function checkForgotPasswordEmailUserBlocked($Email)
    {
        if((string)$Email=='') return false;

        $params      = JComponentHelper::getParams('com_virtualdesk');

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id','block','activation'))
            ->from("#__users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' Limit 1' )
        );
        $JosUserRow = $db->loadAssoc();
        if(!is_array($JosUserRow)) return false;
        if((int) $JosUserRow['id'] <= 0) return false;

        // Verifica o email na VD Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('idjos','id','blocked','activated'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' Limit 1' )
        );
        $VDUserRow = $db->loadAssoc();
        if(!is_array($VDUserRow)) return false;
        if((int)$VDUserRow['id'] <= 0 || (int)$JosUserRow['id']<>(int)$VDUserRow['idjos']) return false;

        //Depois de verificar o email e que os users coincidem, verificamos se o user (VD+Jos) não deve estar bloqueado;
        if( ((int)$JosUserRow['block']==1) || ((int)$VDUserRow['blocked']==1) )
        {   // se estiver bloqueado, tem de ter um processo de ativação a decorrer E deve ter sido ativado pelo menos 1 vez;
            // Caso contrário será um utilizador bloqueado que nunca confirmou o email a pedir a password. Não deve poder pedir a senjha nestas condições

            // Verifica se tem um processo no JosUser
            if( ((string)$JosUserRow['activation']=='') ) return false;

            // Verifica se tem um processo no VDUsersAtivation
            $limitInDays   = $params->get('limitindays');
            $timeZone      = $params->get('timezone');
            $linkDataLimit = new DateTime();
            $linkDataLimit->setTimezone(new DateTimeZone($timeZone));
            $linkDataLimit->sub(new DateInterval('P'.$limitInDays.'D'));
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select(array('id', 'iduser', 'idjos', 'token', 'email','login'))
                ->from('#__virtualdesk_users_activation')
                ->where($db->quoteName('idjos').' = '.$VDUserRow['idjos'].' and '.$db->quoteName('enabled').'=1 and '.$db->quoteName('created').'>=\''.$linkDataLimit->format('Y-m-d').'\'')
                ->order(' id DESC limit 1');
            $db->setQuery($query);
            $row   = $db->loadAssoc();
            $ActivationRunning = (integer)$row['id'];
            if((int)$ActivationRunning<=0) return false;

            // Verifica ativou pelo menos 1 vez...
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select(array('id', 'iduser', 'idjos', 'token', 'email','login'))
                ->from('#__virtualdesk_users_activation')
                ->where($db->quoteName('idjos').' = '.$VDUserRow['idjos'].' and '.$db->quoteName('iduser').' = '.$VDUserRow['id'].' and '.$db->quoteName('activated').'=1 ');
            $db->setQuery($query);
            $row = $db->loadAssoc();
            $ActivationDone = (integer)$row['id'];

            if((int)$ActivationDone<=0) return false;

        }

        return true;
    }


    /*
    * Verifica se o email forneciudo corresponde a ujmm user (Joomla) associado ao VDUser
    */
    public static function checkForgotPasswordEmailInVDUserGroup($Email)
    {
        if((string)$Email=='') return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' Limit 1' )
        );
        $IdJosUser = $db->loadResult();
        if((int)$IdJosUser <= 0) return false;

        $IdVDUserGroup = VirtualDeskUserHelper::getVirtualDeskGroupUserId();
//        $usergroup = JAccess::getGroupsByUser($IdVDUserGroup);
        if((int)$IdVDUserGroup <= 0) return false;

        # get logged in user
        $userJos = JFactory::getUser($IdJosUser);

        # see if the user is in both
        if( in_array($IdVDUserGroup, $userJos->groups) ){
            return true;
        }else {
            return false;
        }

    }



    /*
    * Processo de recuperação de senha
    *
    */
    public static function getForgotPasswordEmailUserID($Email)
    {
        if((string)$Email=='') return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' Limit 1' )
        );
        $IdJosUser = $db->loadResult();
        if((int)$IdJosUser <= 0) return false;

        // Verifica o email na VD Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id','idjos'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('email').'=\''.$db->escape($Email).'\' AND ' . $db->quoteName('idjos') . '=' . $db->escape($IdJosUser) . ' Limit 1' )
        );

        $resVDUser     = $db->loadAssoc();
        $IdVDUser      = $resVDUser['id'];
        $IdVDUserIdJos = $resVDUser['idjos'];
        if((int)$IdVDUser <= 0 || (int)$IdJosUser<>(int)$IdVDUserIdJos) return false;

        $arrayRes = array();
        $arrayRes['id']    = $IdVDUser;
        $arrayRes['idjos'] = $IdJosUser;

        return $arrayRes;
    }



    /*
    * Verifica qual o nome do campo a utilizar para o nome do utilizador.
    * Se esse campos estiver vazio, verifica se é obrigatório estar preenchido.
    * Se for obrigatório estar preenchido deve dar erro.
    * Se não for obrigatório assume depois na gravação o valor do campo de login
    */
    public static function getNameofUserField($data, $origemTableVDUser, $origemTableJosUser)
    {   $nameofuser_field = JComponentHelper::getParams('com_virtualdesk')->get('userfield_nameofuser_field');

        //Se esse campos estiver vazio, verifica se é obrigatório estar preenchido.
        if(empty($data[$nameofuser_field]))
        {  //Se não tiver valor é porque o valor não foi alterado... foi verificado no
           // Então só  vai assumir o campo de login (como name) se o campo atual estiver vazio e se campo na tabela VDUser estiver definido e for diferente do JoUser

            if( !isset($origemTableVDUser) && !isset($origemTableJosUser) ) return ('login');

            $ValorNameOfUserVD  = $origemTableVDUser->{$nameofuser_field} ;
            $ValorNameOfUserJos = $origemTableJosUser->name;

            if( !empty($ValorNameOfUserVD) && (string)$ValorNameOfUserVD == (string)$ValorNameOfUserJos )
            {
               // Não faz nada...o campo não sofreu alteaçõe
               return(true);
            }
            else
            {  // Deve assumir o valor do login
               return ('login');
            }
        }

        return($nameofuser_field);
    }



    /*
    * Carrega lista das áreas de atuação associadas a um utilizador (quando é uma empresa)
    */
    public static function saveUserAreaAct ($UserJoomlaID, $IdUser, $data)
    {
        if( empty($UserJoomlaID) )  return false;
        if( empty($IdUser) )  return false;
        if( !array_key_exists('areaact',$data) )  return true;  // não foram alterados os dados...

        $dataReturn = "";
        try
        {
            $db = JFactory::getDBO();

            if(!empty($data['areaact']))
            {   $queryDel = $db->getQuery(true);
                $conditions = array(
                    $db->quoteName('iduser') . ' = ' . $db->escape($IdUser)
                );
                $queryDel->delete($db->quoteName('#__virtualdesk_users_areaact'));
                $queryDel->where($conditions);
                $db->setQuery($queryDel);
                if (!$db->execute()) return false;

                // Create a new query object.
                $queryIns = $db->getQuery(true);
                // Insert columns.
                $columns = array('iduser', 'idareaact');
                // Insert values.
                $queryIns
                    ->insert($db->quoteName('#__virtualdesk_users_areaact'))
                    ->columns($db->quoteName($columns));
                foreach($data['areaact'] as $keyDataArea )
                {
                    $values = $IdUser.',' . $keyDataArea;
                    $queryIns->values($values);
                }

                // Set the query using our newly populated query object and execute it.
                $db->setQuery($queryIns);

                // Dump the query
                //echo $db->getQuery()->dump();

                if (!$db->execute()) return false;

                asort($data['areaact']);

                $dataReturn = implode('|', $data['areaact']);
            }
            else
            {   // não tem nenhuma escolha... se carregou dados das AreasAct então devem ser eliminados
                if(empty($dataReturn)) {
                    $query = $db->getQuery(true);
                    $conditions = array(
                        $db->quoteName('iduser') . ' = ' . $db->escape($IdUser)
                    );
                    $query->delete($db->quoteName('#__virtualdesk_users_areaact'));
                    $query->where($conditions);
                    $db->setQuery($query);

                    if (!$db->execute()) return false;
                }
            }

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    public static function geEventLogMailDesc ($VDUserId, $EventTitle )
    {

          // Não fiz template como no contactus porque... preguiça :-)

          $userJos = VirtualDeskUserHelper::getUserJoomlaByVDUserId($VDUserId);

          $return     = '<br/><br/><h4>'.JText::_($EventTitle).'</h4><br/>';
          $return    .= JText::sprintf('COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_DESC', $userJos->name);
          $return    .= '<br/><br/>';

          return($return);
    }


    public static function geEventLogMailDescForUserActivation ($VDUserId, $EventTitle )
    {
        // Aqui além de ter alterados os seus dados, também alterou o emaiol porque é necessário ativar a conta
        $userJos = VirtualDeskUserHelper::getUserJoomlaByVDUserId($VDUserId);

        $return     = '<br/><br/><h4>'.JText::_($EventTitle).'</h4><br/>';
        $return    .= JText::sprintf('COM_VIRTUALDESK_USER_EVENTLOG_UPDATE_DESC',$userJos->name);
        $return    .= JText::sprintf('COM_VIRTUALDESK_USER_EVENTLOG_UPDATEACTIVATION_DESC', VirtualDeskUserHelper::getUserEmail($VDUserId));
        $return    .= '<br/><br/>';

        return($return);
    }


    public static function getEventLogMailDesc ($tabledata, $EventTitle )
    {
        $return    = "";
        $emailHTML = file_get_contents(JPATH_SITE . '/components/com_virtualdesk/helpers/html/virtualdesksite_logemail_desc_profile.html');

        $fileLangSufix                = VirtualDeskHelper::getLanguageTag();
        $DESC_CONCELHO_VAL            = VirtualDeskSiteUserFieldsHelper::getConcelhoById($tabledata->concelho, $fileLangSufix);
        $DESC_FREGUESIA_VAL           = VirtualDeskSiteUserFieldsHelper::getFreguesiaById($tabledata->freguesia, $fileLangSufix);
        //$DESC_CTLANGUAGE_VAL        = VirtualDeskSiteContactUsHelper::getContactUsLanguageNameById($tabledata->idctlanguage, $fileLangSufix);
        $DESC_AREAACT_LISTVAL         = VirtualDeskSiteUserFieldsHelper::getAreaActListSelectName($tabledata, $fileLangSufix);
        $DESC_USER_VAL                = $tabledata->name;
        $DESC_USERLOGIN_VAL           = $tabledata->login;
        $DESC_USEREMAIL_VAL           = $tabledata->email;

        $DESC_FIRMA_VAL               = $tabledata->firma;
        $DESC_DESIGNACAOCOMERCIAL_VAL = $tabledata->designacaocomercial;
        $DESC_CAEPRINCIPAL_VAL        = $tabledata->caeprincipal;
        
        $DESC_MORADA_VAL              = $tabledata->address;
        $DESC_CODIGOPOSTAL_VAL        = $tabledata->postalcode;
        $DESC_TELPRINCIPAL_VAL        = $tabledata->phone1;
        $DESC_TELALTERNATIVO_VAL      = $tabledata->phone2;
        $DESC_WEBSITE_VAL             = $tabledata->website;
        $DESC_FACEBOOK_VAL            = $tabledata->facebook;
        $DESC_EMAILALTERNATIVO_VAL    = $tabledata->emailsec;
        
        $DESC_LOCALIDADE_VAL          = $tabledata->localidade;
        $DESC_MAPLATLONG_VAL          = $tabledata->maplatlong;
        
        $DESC_NOMERESP_VAL            = $tabledata->managername;
        $DESC_CARGORESP_VAL           = $tabledata->managerposition;
        $DESC_CONTACTORESP_VAL        = $tabledata->managercontact;
        $DESC_EMAILRESP_VAL           = $tabledata->manageremail;
        
        $DESC_CARGO_VAL               = $tabledata->cargo;
        $DESC_FUNCAO_VAL              = $tabledata->funcao;

        $DESC_VERACIDADEDADOS_VAL      = $tabledata->veracidadedados;
        $DESC_POLITICAPRIVACIDADE_VAL  = $tabledata->politicaprivacidade;
       


        if(!is_array($DESC_AREAACT_LISTVAL)) $DESC_AREAACT_LISTVAL = array();
        $DESC_AREAACT_VAL = htmlentities(implode(', ',$DESC_AREAACT_LISTVAL), ENT_QUOTES, 'UTF-8');

        $return = $emailHTML;

       
        if(empty($DESC_FIRMA_VAL)) $DESC_FIRMA_VAL = '-';
        $return      = str_replace("%DESC_FIRMA_LABEL",JText::_('COM_VIRTUALDESK_USER_FIRMA_LABEL').':', $return );
        $return      = str_replace("%DESC_FIRMA_VAL",$DESC_FIRMA_VAL, $return );
        
        if(empty($DESC_DESIGNACAOCOMERCIAL_VAL)) $DESC_DESIGNACAOCOMERCIAL_VAL = '-';
        $return      = str_replace("%DESC_DESIGNACAOCOMERCIAL_LABEL",JText::_('COM_VIRTUALDESK_USER_DESIGNACAOCOMERCIAL_LABEL').':', $return );
        $return      = str_replace("%DESC_DESIGNACAOCOMERCIAL_VAL",$DESC_DESIGNACAOCOMERCIAL_VAL, $return );

        if(empty($DESC_CAEPRINCIPAL_VAL)) $DESC_CAEPRINCIPAL_VAL = '-';
        $return      = str_replace("%DESC_CAEPRINCIPAL_LABEL",JText::_('COM_VIRTUALDESK_USER_CAEPRINCIPAL_LABEL').':', $return );
        $return      = str_replace("%DESC_CAEPRINCIPAL_VAL",$DESC_CAEPRINCIPAL_VAL, $return );
        
        if(empty($DESC_AREAACT_VAL))  $DESC_AREAACT_VAL = '-';
        $return = str_replace("%DESC_AREAACT_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_AREAACT_LABEL') . ':', $return);
        $return = str_replace("%DESC_AREAACT_VAL", $DESC_AREAACT_VAL, $return);
        
        if(empty($DESC_MORADA_VAL)) $DESC_MORADA_VAL = '-';
        $return      = str_replace("%DESC_MORADA_LABEL",JText::_('COM_VIRTUALDESK_USER_ADDRESS_LABEL').':', $return );
        $return      = str_replace("%DESC_MORADA_VAL",$DESC_MORADA_VAL, $return );
        
        if(empty($DESC_CODIGOPOSTAL_VAL)) $DESC_CODIGOPOSTAL_VAL = '-';
        $return      = str_replace("%DESC_CODIGOPOSTAL_LABEL",JText::_('COM_VIRTUALDESK_USER_POSTALCODE_LABEL').':', $return );
        $return      = str_replace("%DESC_CODIGOPOSTAL_VAL",$DESC_CODIGOPOSTAL_VAL, $return );
        
        if(empty($DESC_TELPRINCIPAL_VAL)) $DESC_TELPRINCIPAL_VAL = '-';
        $return      = str_replace("%DESC_TELPRINCIPAL_LABEL",JText::_('COM_VIRTUALDESK_USER_PHONE1_LABEL').':', $return );
        $return      = str_replace("%DESC_TELPRINCIPAL_VAL",$DESC_TELPRINCIPAL_VAL, $return );
        
        if(empty($DESC_TELALTERNATIVO_VAL)) $DESC_TELALTERNATIVO_VAL = '-';
        $return      = str_replace("%DESC_TELALTERNATIVO_LABEL",JText::_('COM_VIRTUALDESK_USER_PHONE2_LABEL').':', $return );
        $return      = str_replace("%DESC_TELALTERNATIVO_VAL",$DESC_TELALTERNATIVO_VAL, $return );
        
        if(empty($DESC_TELALTERNATIVO_VAL)) $DESC_TELALTERNATIVO_VAL = '-';
        $return      = str_replace("%DESC_TELALTERNATIVO_LABEL",JText::_('COM_VIRTUALDESK_USER_PHONE2_LABEL').':', $return );
        $return      = str_replace("%DESC_TELALTERNATIVO_VAL",$DESC_TELALTERNATIVO_VAL, $return );
        
        if(empty($DESC_WEBSITE_VAL)) $DESC_WEBSITE_VAL = '-';
        $return      = str_replace("%DESC_WEBSITE_LABEL",JText::_('COM_VIRTUALDESK_USER_WEBSITE_LABEL').':', $return );
        $return      = str_replace("%DESC_WEBSITE_VAL",$DESC_WEBSITE_VAL, $return );
        
        if(empty($DESC_FACEBOOK_VAL)) $DESC_FACEBOOK_VAL = '-';
        $return      = str_replace("%DESC_FACEBOOK_LABEL",JText::_('COM_VIRTUALDESK_USER_FACEBOOK_LABEL').':', $return );
        $return      = str_replace("%DESC_FACEBOOK_VAL",$DESC_FACEBOOK_VAL, $return );
        
        if(empty($DESC_EMAILALTERNATIVO_VAL)) $DESC_EMAILALTERNATIVO_VAL = '-';
        $return      = str_replace("%DESC_EMAILALTERNATIVO_LABEL",JText::_('COM_VIRTUALDESK_USER_EMAILSEC_LABEL').':', $return );
        $return      = str_replace("%DESC_EMAILALTERNATIVO_VAL",$DESC_EMAILALTERNATIVO_VAL, $return );
        
        if(empty($DESC_CONCELHO_VAL)) $DESC_CONCELHO_VAL = '-';
        $return      = str_replace("%DESC_CONCELHO_LABEL",JText::_('COM_VIRTUALDESK_USER_CONCELHO_LABEL').':', $return );
        $return      = str_replace("%DESC_CONCELHO_VAL",$DESC_CONCELHO_VAL, $return );
        
        if(empty($DESC_FREGUESIA_VAL)) $DESC_FREGUESIA_VAL = '-';
        $return      = str_replace("%DESC_FREGUESIA_LABEL",JText::_('COM_VIRTUALDESK_USER_FREGUESIA_LABEL').':', $return );
        $return      = str_replace("%DESC_FREGUESIA_VAL",$DESC_FREGUESIA_VAL, $return );
        
        if(empty($DESC_LOCALIDADE_VAL)) $DESC_LOCALIDADE_VAL = '-';
        $return      = str_replace("%DESC_LOCALIDADE_LABEL",JText::_('COM_VIRTUALDESK_USER_LOCALIDADE_LABEL').':', $return );
        $return      = str_replace("%DESC_LOCALIDADE_VAL",$DESC_LOCALIDADE_VAL, $return );
        
        if(empty($DESC_MAPLATLONG_VAL)) $DESC_MAPLATLONG_VAL = '-';
        $return      = str_replace("%DESC_MAPLATLONG_LABEL",JText::_('COM_VIRTUALDESK_USER_MAPLATLONG_LABEL').':', $return );
        $return      = str_replace("%DESC_MAPLATLONG_VAL",$DESC_MAPLATLONG_VAL, $return );
        
        if(empty($DESC_NOMERESP_VAL)) $DESC_NOMERESP_VAL = '-';
        $return      = str_replace("%DESC_NOMERESP_LABEL",JText::_('COM_VIRTUALDESK_USER_MANAGERNAME_LABEL').':', $return );
        $return      = str_replace("%DESC_NOMERESP_VAL",$DESC_NOMERESP_VAL, $return );
        
        if(empty($DESC_CARGORESP_VAL)) $DESC_CARGORESP_VAL = '-';
        $return      = str_replace("%DESC_CARGORESP_LABEL",JText::_('COM_VIRTUALDESK_USER_MANAGERPOSITION_LABEL').':', $return );
        $return      = str_replace("%DESC_CARGORESP_VAL",$DESC_CARGORESP_VAL, $return );
        
        if(empty($DESC_CONTACTORESP_VAL)) $DESC_CONTACTORESP_VAL = '-';
        $return      = str_replace("%DESC_CONTACTORESP_LABEL",JText::_('COM_VIRTUALDESK_USER_MANAGERCONTACT_LABEL').':', $return );
        $return      = str_replace("%DESC_CONTACTORESP_VAL",$DESC_CONTACTORESP_VAL, $return );
        
        if(empty($DESC_EMAILRESP_VAL)) $DESC_EMAILRESP_VAL = '-';
        $return      = str_replace("%DESC_EMAILRESP_LABEL",JText::_('COM_VIRTUALDESK_USER_MANAGEREMAIL_LABEL').':', $return );
        $return      = str_replace("%DESC_EMAILRESP_VAL",$DESC_EMAILRESP_VAL, $return );
        
        
         if(empty($DESC_CARGO_VAL)) $DESC_CARGO_VAL = '-';
        $return      = str_replace("%DESC_CARGO_LABEL",JText::_('COM_VIRTUALDESK_USER_CARGO_LABEL').':', $return );
        $return      = str_replace("%DESC_CARGO_VAL",$DESC_CARGO_VAL, $return );
        
        if(empty($DESC_FUNCAO_VAL)) $DESC_FUNCAO_VAL = '-';
        $return      = str_replace("%DESC_FUNCAO_LABEL",JText::_('COM_VIRTUALDESK_USER_FUNCAO_LABEL').':', $return );
        $return      = str_replace("%DESC_FUNCAO_VAL",$DESC_FUNCAO_VAL, $return );

        if(empty($DESC_VERACIDADEDADOS_VAL)) $DESC_VERACIDADEDADOS_VAL = '-';
        $return      = str_replace("%DESC_VERACIDADEDADOS_LABEL",JText::_('COM_VIRTUALDESK_USER_VERACIDADEDADOS_LABEL').':', $return );
        $return      = str_replace("%DESC_VERACIDADEDADOS_VAL",$DESC_VERACIDADEDADOS_VAL, $return );

        if(empty($DESC_POLITICAPRIVACIDADE_VAL)) $DESC_POLITICAPRIVACIDADE_VAL = '-';
        $return      = str_replace("%DESC_POLITICAPRIVACIDADE_LABEL",JText::_('COM_VIRTUALDESK_USER_POLITICAPRIVACIDADE_LABEL').':', $return );
        $return      = str_replace("%DESC_POLITICAPRIVACIDADE_VAL",$DESC_POLITICAPRIVACIDADE_VAL, $return );
     
        if(empty($DESC_USER_VAL)) $DESC_USER_VAL = '-';
        $return = str_replace("%DESC_USER_LABEL", JText::_('COM_VIRTUALDESK_CONTACTUS_EVENTLOG_MAIL_DESCUSER'), $return);
        $return = str_replace("%DESC_USER_VAL", $DESC_USER_VAL, $return);

        if(empty($DESC_USER_VAL)) $DESC_USEREMAIL_VAL = '-';
        $return      = str_replace("%DESC_USEREMAIL_VAL",$DESC_USEREMAIL_VAL, $return );
        
        if(empty($DESC_USERLOGIN_VAL)) $DESC_USERLOGIN_VAL = '-';
        $return      = str_replace("%DESC_USERLOGIN_VAL",$DESC_USERLOGIN_VAL, $return );
        
        $EventTitle = JText::_($EventTitle);

        $return      = str_replace("%EVENT_TITLE",$EventTitle, $return );
        
        return($return);
    }


    public static function setUserBlockState($IdUserVD,$enable)
    {
        if( empty($IdUserVD) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'useredit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $dataJosUser = array();
        $dataVDUser = array();
        $dataJosUser['block']  = $enable;
        $dataVDUser['blocked'] = $enable;

        // Carrega dados atuais na base de dados para da tabela VDUsers
        $db = JFactory::getDbo();
        $origemTableVDUser = new VirtualDeskTableUser($db);
        $origemTableVDUser->load(array('id' => $IdUserVD));
        if ((integer)$origemTableVDUser->id !== (integer)$IdUserVD) return false;
        $updateTableVDUser = clone  $origemTableVDUser;

        //Carrega dados atuais do user joomla associado
        $UserJoomlaID = $origemTableVDUser->idjos;
        $origemTableJosUser = JFactory::getUser($UserJoomlaID);
        if ((!isset($origemTableJosUser->id)) || (intval($origemTableJosUser->id) == 0)) return false;
        $updateTableJosUser = clone $origemTableJosUser;

        //UPDATE: JOS User
        if (!empty($dataJosUser)) {
            if (!$updateTableJosUser->bind($dataJosUser)) return false;
            if (!$updateTableJosUser->save()) return false;
        }

        //UPDATE: VD User
        // Bind the data.
        $resVDUserBind = $updateTableVDUser->bind($dataVDUser);
        if (!$resVDUserBind) {   // Reverte gravação anterior do joomla user associado
            $origemTableJosUser->save();
            return false;
        }
        // Store the data.
        $resVDUserSave = $updateTableVDUser->save($dataVDUser);
        if (!$resVDUserSave) {    // Reverte gravação anterior do joomla user associado
            $origemTableJosUser->save();
            return false;
        }

        // Se está a bloquear o utilizador, deve procurar todas as ativações pendentes e desligar ...
        // pois se é para bloquear não faz sentido ter ativações
        if ($enable == 1) {
            $query = $db->getQuery(true);
            // Fields to update.
            $fields = array(
                $db->quoteName('enabled') . ' = 0'
            );
            // Conditions for which records should be updated.
            $conditions = array(
                $db->quoteName('iduser') . ' = ' . $IdUserVD,
                $db->quoteName('idjos') . ' = ' . $UserJoomlaID,
                $db->quoteName('enabled') . ' = 1'
            );
            $query->update($db->quoteName('#__virtualdesk_users_activation'))->set($fields)->where($conditions);
            $db->setQuery($query);
            $db->execute();
        }

        // Registar no evento de LOGS
        $vdlog = new VirtualDeskLogHelper();
        $eventdata = array();
        $eventdata['iduser'] = $IdUserVD;
        $eventdata['idjos'] = $UserJoomlaID;
        $eventdata['title'] = JText::_('COM_VIRTUALDESK_EVENTLOG_USERENABLE_TITLE');
        $eventdata['desc'] = '';
        $eventdata['filelist'] = "";
        $eventdata['category'] = JText::_('COM_VIRTUALDESK_EVENTLOG_USERENABLE_CAT');
        $eventdata['sessionuserid'] = JFactory::getUser()->id;
        $vdlog->insertEventLog($eventdata);



            return true;
    }



    public static function sendUserActivation($IdUserVD)
    {
        if( empty($IdUserVD) )  return false;

        $IdUserJoomla = self::getJoomlaIDFromVDUserId ($IdUserVD);

        if( empty($IdUserJoomla) )  return false;

        $app    = JFactory::getApplication();
        $db = JFactory::getDbo();

        // Get the user object: Joomla User
        $user = JUser::getInstance($IdUserJoomla);

        // Make sure the user isn't a Super Admin.
        if ($user->authorise('core.admin'))
        {	$app->enqueueMessage(JText::_('COM_VIRTUALDESK_REMIND_SUPERADMIN_ERROR'),'error');
            return false;
        }

        $VirtualDeskUser = self::getUserFromJoomlaID ($IdUserJoomla);

        // Verifica que o email do Joomla User == Virtual DeskUser, caso contrário dá erro ...
        if ($user->email != $VirtualDeskUser->email)
        {	$app->enqueueMessage(JText::_('COM_VIRTUALDESK_USER_EMAIL_NOT_MATCH'),'error');
            return false;
        }

        // ToDo: deve verificar as tentativas falhadas para o mesmo IP + copiar o checkresetlimit e ver como funciona
        // Make sure the user has not exceeded the reset limit
        /*if (!$this->checkResetLimit($user))
        {
            $resetLimit = (int) JFactory::getApplication()->getParams()->get('reset_time');
            $this->setError(JText::plural('COM_USERS_REMIND_LIMIT_ERROR_N_HOURS', $resetLimit));

            return false;
        }*/

        // Set the confirmation token.
        $token = JApplicationHelper::getHash(JUserHelper::genRandomPassword());

        $UserActivation = new VirtualDeskUserActivationHelper();

        $TableVDUser = new VirtualDeskTableUser($db);
        $TableVDUser->load(array('id' => $IdUserVD));

        $SaveUserActivation = $UserActivation->SetUserForActivation($TableVDUser, $user,$token);

        if($SaveUserActivation==false)
        {  $app->enqueueMessage(JText::_(''), 'error');
            return false;
        }

        $resSendActivatiobEmail = $UserActivation->SendActivationEmail($user, $token);
        if($resSendActivatiobEmail==false)
        {  $app->enqueueMessage(JText::_(''), 'error');
            return false;
        }

        return true;
    }




    /*
  * Verifica se o utilizador VirtualDesk está Enabled ou seja not blocked and active
  * @param $UserJoomlaID
  */
    public static function checkUserVDActivatedVal($IdUserVD)
    {

        if( empty($IdUserVD) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'useredit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        try
        {
            $db = JFactory::getDbo();
            $db->setQuery("Select activated From #__virtualdesk_users Where id =" . $db->escape($IdUserVD) );
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
* Verifica se o utilizador VirtualDesk está Enabled ou seja not blocked and active
* @param $UserJoomlaID
*/
    public static function checkUserVDBlockedVal($IdUserVD)
    {
        if( empty($IdUserVD) )  return false;

        /*
        * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
        if($vbInAdminGroup===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
            return false;
        }
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'useredit'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDbo();
            //$db->setQuery("Select CAST(blocked as CHAR(1)) as blocked  From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $db->setQuery("Select blocked From #__virtualdesk_users Where id =" . $db->escape($IdUserVD));
            $res =  $db->loadResult();

            return($res);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
  *
  * @param $UserJoomlaID
  */
    public static function getFiscalNumberFromJoomlaUser($UserJoomlaID)
    {
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $userfield_login_type = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
        $NIF = -1;

        $db = JFactory::getDbo();
        if($userfield_login_type=='login_as_nif') {
            $db->setQuery("Select login From #__virtualdesk_users Where idjos =" . $db->escape($UserJoomlaID). " Limit 1" );
        }
        else{
            $db->setQuery("Select fiscalid From #__virtualdesk_users Where idjos =" . $db->escape($UserJoomlaID). " Limit 1" );
        }

        $dataReturn = $db->loadColumn();

        if( !is_array($dataReturn) || (empty($dataReturn)) ) return $NIF;
        if( ((int)$dataReturn[0] > 0 ) ) $NIF = $dataReturn[0];

        return $NIF;
    }


    /*
*
* @param $NIF
*/
    public static function getUserObjByFiscalNumber($NIF=-1)
    {
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $userfield_login_type = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
        //$NIF = -1;

        $db = JFactory::getDbo();
        if($userfield_login_type=='login_as_nif') {
            $db->setQuery("Select *, login as nif From #__virtualdesk_users Where login =" . $db->escape($NIF). " Limit 1" );
        }
        else{
            $db->setQuery("Select *, fiscalid as nif From #__virtualdesk_users Where fiscalid =" . $db->escape($NIF). " Limit 1" );
        }
        $dataReturn = $db->loadObject();
        if( (empty($dataReturn)) ) return false;

        return $dataReturn;
    }


    public static function getUserNameById($iduser)
    {
        if( empty($iduser) )  return false;

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('name')
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('id') . "=" . $db->escape($iduser))
        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function getUserEmailNameByIdArray($ar_iduser)
    {
        if( !is_array($ar_iduser) ) return false;
        if( empty($ar_iduser) ) return false;
        $setUserIds = implode(',',$ar_iduser);

        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('name , email')
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('id') . " in (" . $db->escape($setUserIds).')')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getUserObjById($iduser)
    {
        if( (int)$iduser<=0 ) return false;
        $db = JFactory::getDbo();
        $db->setQuery("Select * From #__virtualdesk_users Where id =" . $db->escape($iduser). " Limit 1" );
        $dataReturn = $db->loadObject();
        if( (empty($dataReturn)) ) return false;

        return $dataReturn;
    }


    /*
 *
 * @param $UserJoomlaID
 */
    public static function getFiscalNumberFromVDUser($iduser)
    {
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $userfield_login_type = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
        $NIF = -1;

        $db = JFactory::getDbo();
        if($userfield_login_type=='login_as_nif') {
            $db->setQuery("Select login From #__virtualdesk_users Where id =" . $db->escape($iduser). " Limit 1" );
        }
        else{
            $db->setQuery("Select fiscalid From #__virtualdesk_users Where id =" . $db->escape($iduser). " Limit 1" );
        }

        $dataReturn = $db->loadColumn();

        if( !is_array($dataReturn) || (empty($dataReturn)) ) return $NIF;
        if( ((int)$dataReturn[0] > 0 ) ) $NIF = $dataReturn[0];

        return $NIF;
    }

}