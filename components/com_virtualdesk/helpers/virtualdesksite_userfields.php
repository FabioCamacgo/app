<?php
/**
 * @package     Joomla.Administrator
 * @subpackage
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


class VirtualDeskSiteUserFieldsHelper
{
    var $arConfFieldNames = array();
    var $arUserFieldsConfig     = array();
    var $arUserFieldLoginConfig = array();
    var $arEmptyFieldsError     = array();

    //public function VirtualDeskSiteUserFieldsHelper(){
        public function __construct() {

        $this->arConfFieldNames =  array(
            'nif'=>'NIF',
            'civilid'=>'CIVILID',
            'name'=>'NAME',
            'firma'=>'FIRMA',
            'designacaocomercial'=>'DESIGNACAOCOMERCIAL',
            'cargo'=>'CARGO',
            'funcao'=>'FUNCAO',
            'caeprincipal'=>'CAEPRINCIPAL',
            'sectoratividade'=>'SECTORATIVIDADE',
            'areaact'=>'AREAACT',
            'address'=>'ADDRESS',
            'postalcode'=>'POSTALCODE',
            'phone1'=>'PHONE1',
            'phone2'=>'PHONE2',
            'website'=>'WEBSITE',
            'facebook'=>'FACEBOOK',
            'emailsec'=>'EMAILSEC',
            'concelho'=>'CONCELHO',
            'freguesia'=>'FREGUESIA',
            'localidade'=>'LOCALIDADE',
            'maplatlong'=>'MAPLATLONG',
            'managername'=>'MANAGERNAME',
            'managerposition'=>'MANAGERPOSITION',
            'managercontact'=>'MANAGERCONTACT',
            'manageremail'=>'MANAGEREMAIL',
            'veracidadedados'=>'VERACIDADEDADOS',
            'politicaprivacidade'=>'POLITICAPRIVACIDADE'
        );


        $this->setFields();
        $this->setUserFieldLogin();
    }


    public function setFields()
    {
        foreach($this->arConfFieldNames as $key => $val)
        {
            // Field : visible no Registration
            $this->arUserFieldsConfig['UserField_' . $val . '_Registration']  = false;
            if(JComponentHelper::getParams('com_virtualdesk')->get('userfield_' . $key .'_registration') == 1 ) $this->arUserFieldsConfig['UserField_' . $val . '_Registration'] = true;

            // Field : visible no Profile
            $this->arUserFieldsConfig['UserField_' . $val. '_Profile']  = false;
            if(JComponentHelper::getParams('com_virtualdesk')->get('userfield_' . $key .'_profile') == 1 ) $this->arUserFieldsConfig['UserField_' . $val . '_Profile'] = true;

            // Field: obrigatório
            $this->arUserFieldsConfig['UserField_' . $val. '_Required']    = "";
            if(JComponentHelper::getParams('com_virtualdesk')->get('userfield_'.$key .'_required') == 1 ) $this->arUserFieldsConfig['UserField_' . $val. '_Required'] = 'required';
        }

    }

    public function getFields()
    {
        return $this->arUserFieldsConfig;
    }

    public function getUserFields()
    {
        return $this->arUserFieldsConfig;
    }

    public function setUserFieldLogin()
    {
        // Verifica se o campo de login é do tipo Text (livre) ou do tipo NIF
        $setUserFieldLoginType = JComponentHelper::getParams('com_virtualdesk')->get('userfield_login_type');
        $this->arUserFieldLoginConfig['setUserFieldLoginType'] = $setUserFieldLoginType;
        if($setUserFieldLoginType=="login_as_nif") {
            $this->arUserFieldLoginConfig['CampoForm_Username_Label'] = JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL');
            $this->arUserFieldLoginConfig['setUserFieldLoginTypeNIF'] = true;
            $this->arUserFieldLoginConfig['CampoForm_Username_MaxLength'] = "9" ;
            $this->arUserFieldLoginConfig['setUserFieldLoginTypeNIF_JS'] = 1;
        }
        else {
            $this->arUserFieldLoginConfig['CampoForm_Username_Label'] = JText::_('COM_VIRTUALDESK_USER_LOGIN_LABEL');
            $this->arUserFieldLoginConfig['setUserFieldLoginTypeNIF'] = false;
            $this->arUserFieldLoginConfig['CampoForm_Username_MaxLength'] = "250" ;
            $this->arUserFieldLoginConfig['setUserFieldLoginTypeNIF_JS'] = -1;
        }
    }

    public function getUserFieldLogin()
    {
        return $this->arUserFieldLoginConfig;
    }


    /*
    * Verifica se os campos recebidos se estão vazios e nesse caso não inicializa o array de dados.
    * Ou se são obrigatórios e nesse caso não podem vir vazios.
    *
    */
    public function checkCamposEmptyProfile($arCamposGet, $data)
    {
        foreach($this->arConfFieldNames as $key => $val)
        {
            // Excepção para o NIF que pode ser utilizado no login
            if( $key!='nif' )
            {
                if ($this->arUserFieldsConfig['UserField_' . $val . '_Profile']) {
                    if ($this->arUserFieldsConfig['UserField_' . $val . '_Required']) {
                        if (!empty($arCamposGet[$key])) {
                            $data[$key] = $arCamposGet[$key];
                        }
                        else {
                            // é obrigatório e está vazio... Marca como estando com erro
                            $this->arEmptyFieldsError[$key] = true;
                        }
                    } else { // Se não é obrigatório pode ir vazio
                        $data[$key] = $arCamposGet[$key];
                    }
                }
            }
        }


        // Verifica qual o campo a ser utilizado no nome e se deve estar preenchido...
        if(!self::checkNameofUserFieldVal($data))
        {   $nameofuser_field = JComponentHelper::getParams('com_virtualdesk')->get('userfield_nameofuser_field');
            // é obrigatório e está vazio... Marca como estando com erro
            $this->arEmptyFieldsError[$nameofuser_field] = true;
        }


        // Só aceita o valor do NIF e o campo está visivel e se o LOGIN não é do tipo NIF
        if ($this->arUserFieldsConfig['UserField_NIF_Profile'] && !$this->arUserFieldLoginConfig['setUserFieldLoginTypeNIF']) {
            if ($this->arUserFieldsConfig['UserField_NIF_Required']) {
                if (!empty($arCamposGet['fiscalid'])) {
                    $data['fiscalid'] = $arCamposGet['fiscalid'];
                } else {
                    // é obrigatório e está vazio... Marca como estando com erro
                    $this->arEmptyFieldsError['fiscalid'] = true;
                }
            } else { // Se não é obrigatório pode ir vazio
                $data['fiscalid'] = $arCamposGet['fiscalid'];
            }
        }


        return($data);
    }



    /*
    * Verifica se os campos recebidos se estão vazios e nesse caso não inicializa o array de dados.
    * Ou se são obrigatórios e nesse caso não podem vir vazios.
    *
    */
    public function checkCamposEmptyRegistration($arCamposGet, $data)
    {

        foreach($this->arConfFieldNames as $key => $val)
        {
            // Excepção para o NIF que pode ser utilizado no login
            if( $key!='nif' )
            {
                if ($this->arUserFieldsConfig['UserField_' . $val . '_Registration']) {
                    if ($this->arUserFieldsConfig['UserField_' . $val . '_Required']) {
                        if (!empty($arCamposGet[$key])) {
                            $data[$key] = $arCamposGet[$key];
                        }
                        else {
                            // é obrigatório e está vazio... Marca como estando com erro
                            $this->arEmptyFieldsError[$key] = true;
                        }
                    } else { // Se não é obrigatório pode ir vazio
                        $data[$key] = $arCamposGet[$key];
                    }
                }
            }
        }


        // Verifica qual o campo a ser utilizado no nome e se deve estar preenchido...
        if(!self::checkNameofUserFieldVal($data))
        {   $nameofuser_field = JComponentHelper::getParams('com_virtualdesk')->get('userfield_nameofuser_field');
            // é obrigatório e está vazio... Marca como estando com erro
            $this->arEmptyFieldsError[$nameofuser_field] = true;
        }

        // Só aceita o valor do NIF e o campo está visivel e se o LOGIN não é do tipo NIF
        if ($this->arUserFieldsConfig['UserField_NIF_Registration'] && !$this->arUserFieldLoginConfig['setUserFieldLoginTypeNIF']) {
            if ($this->arUserFieldsConfig['UserField_NIF_Required']) {
                if (!empty($arCamposGet['fiscalid'])) {
                    $data['fiscalid'] = $arCamposGet['fiscalid'];
                } else {
                    // é obrigatório e está vazio... Marca como estando com erro
                    $this->arEmptyFieldsError['fiscalid'] = true;
                }
            } else { // Se não é obrigatório pode ir vazio
                $data['fiscalid'] = $arCamposGet['fiscalid'];
            }
        }

        return($data);
    }



//    public function checkRequiredEmptyFieldsProfile()
//    {
//        $msg ="";
//        if( $this->arEmptyFieldsError['fiscalid'] ) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL');
//        if( $this->arEmptyFieldsError['civilid'] )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_CIVILID_LABEL');
//        if( $this->arEmptyFieldsError['address'] )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_ADDRESS_LABEL');
//        if( $this->arEmptyFieldsError['postalcode'] )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_POSTALCODE_LABEL');
//
//        return($msg);
//    }

    public function checkRequiredEmptyFieldsProfile()
    {
        $msg ="";
        foreach($this->arConfFieldNames as $key => $val) {
            if( $this->arEmptyFieldsError[$key] )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_' . $val . '_LABEL');
        }
        return($msg);
    }


//    public function checkRequiredEmptyFieldsRegistration()
//    {
//        $msg ="";
//        if( !empty($this->arEmptyFieldsError['fiscalid']) ) $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL');
//        if( !empty($this->arEmptyFieldsError['civilid']) )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_CIVILID_LABEL');
//        if( !empty($this->arEmptyFieldsError['address']) )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_ADDRESS_LABEL');
//        if( !empty($this->arEmptyFieldsError['postalcode']) )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_POSTALCODE_LABEL');
//
//        return($msg);
//    }


    public function checkRequiredEmptyFieldsRegistration()
    {
        $msg ="";
        foreach($this->arConfFieldNames as $key => $val) {
            if( !empty($this->arEmptyFieldsError[$key]) )  $msg .= "\n" . JText::_('COM_VIRTUALDESK_FORGOTPASSWORD_ERROR_FIELDREQUIRED') . ": " . JText::_('COM_VIRTUALDESK_USER_' . $val . '_LABEL');
        }
        return($msg);
    }


    /*
   * Verifica qual o nome do campo a utilizar para o nome do utilizador.
   * Se esse campos estiver vazio, verifica se é obrigatório estar preenchido.
   * Se for obrigatório estar preenchido deve dar erro.
   * Se não for obrigatório assume depois na gravação o valor do campo de login
   */
    public static function checkNameofUserFieldVal($data)
    {
        $nameofuser_field = JComponentHelper::getParams('com_virtualdesk')->get('userfield_nameofuser_field');
        $nameofuser_isrequired = JComponentHelper::getParams('com_virtualdesk')->get('userfield_nameofuser_isrequired');

        //Se esse campos estiver vazio, verifica se é obrigatório estar preenchido.
        if(empty($data[$nameofuser_field]))
        {
            if($nameofuser_isrequired == '1')
            {
                // É obrigatório, não está preenchido então deve dar erro.
                return(false);
            }
        }
        return(true);
    }


    public static function getConcelhosList()
    {
        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, concelho as name'))
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->order('concelho ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getFreguesiaByConcelhoList($idconcelho)
    {
        if(empty($idconcelho)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia as name'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id').'='.$db->escape($idconcelho))
            ->order('freguesia ASC' )
        );
        $data = $db->loadAssocList();
        return($data);
    }


    public static function getConcelhoById($idconcelho)
    {
        if(empty($idconcelho)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('concelho as name'))
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->where($db->quoteName('id').'='.$db->escape($idconcelho))
            ->order('concelho ASC' )
        );
        $data = $db->loadResult();
        return($data);
    }

    public static function getConcelhoByIdEDITED()
    {
        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('concelho as name'))
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->order('concelho ASC' )
        );
        $data = $db->loadResult();
        return($data);
    }


    public static function getFreguesiaById($idfreg)
    {
        if(empty($idfreg)) return false;

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('freguesia as name'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('id').'='.$db->escape($idfreg))
            ->order('freguesia ASC' )
        );
        $data = $db->loadResult();
        return($data);
    }






    /*
   * Carrega lista de TODAS áreas de atuação para serem apresentadas
   */
    public static function getAreaActListAllByGroup ($lang)
    {
        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id as id','a.idgroup as idgroup','a.name_pt as name_pt','a.name_en as name_en','b.groupname_pt as groupname_pt','b.groupname_en as groupname_en') )
                ->join('LEFT', '#__virtualdesk_areaact_group AS b ON b.id = a.idgroup')
                ->from("#__virtualdesk_areaact as a")
                ->order('a.idgroup')
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                // Add Area Act Group
                $rowGroupName   = '';
                $rowAreaActName = '';

                if (empty($response[$row->idgroup])) {
                    if( empty($lang) or ($lang='pt_PT') ) {
                        $rowGroupName = $row->groupname_pt;
                    }
                    else {
                        $rowGroupName = $row->groupname_en;
                    }
                    $response[$row->idgroup]['group'] = array(
                        'idgroup' => $row->idgroup,
                        'groupname' => $rowGroupName,
                    );
                }

                // Add Area Act
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowAreaActName = $row->name_pt;
                }
                else {
                    $rowAreaActName = $row->name_en;
                }

                $response[$row->idgroup]['rows'][] =  array (
                    'id' => $row->id,
                    'name' => $rowAreaActName
                );
            }

            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Carrega lista das áreas de atuação associadas a um utilizador
    */
    public static function getAreaActListSelected ($UserJoomlaID, $IdUserVD)
    {
        if( empty($UserJoomlaID) )  return false;
        if( empty($IdUserVD) )  return false;

        try
        {
            // Verifica se o username existe noutro utilizador no VirtualDesk users...
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','iduser','idareaact') )
                ->from("#__virtualdesk_users_areaact")
                ->where($db->quoteName('iduser') . '=' . $db->escape($IdUserVD))
            );
            $dataReturn = $db->loadAssocList();
            $dataList = array();
            foreach( $dataReturn as $keyList )
            {
                $dataList[] = $keyList['idareaact'];
            }

            return($dataList);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
   * Carrega lista das áreas de atuação associadas a uma empresa
   */
    public static function getAreaActListSelectName ($data, $lang)
    {
        if( empty($data) )  return false;
        if( empty($data->id) )  return false;

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.iduser','a.idareaact','b.name_pt as name_pt','b.name_en as name_en') )
                ->join('LEFT', '#__virtualdesk_areaact AS b ON b.id = a.idareaact')
                ->from("#__virtualdesk_users_areaact as a")
                ->where($db->quoteName('iduser') . '=' . $db->escape($data->id))
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            $response = array();
            // Transforma a lista com subarray subobjectos por grupo
            foreach ($dataReturn as $row) {
                // Add Area Act Group
                $rowAreaActName = '';
                // Add Area Act
                if( empty($lang) or ($lang='pt_PT') ) {
                    $response[] = $row->name_pt;
                }
                else {
                    $response[] = $row->name_en;
                }
            }

            return($response);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    public static function getSetorAtividadeList($lang)
    {
        if( empty($lang) )  return false;

        try
        {
            if( empty($lang) or ($lang='pt_PT') ) {
               $nameCol = 'name_pt as name';
            }
            else {
               $nameCol = 'name_en as name';
            }

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id',$nameCol) )
                ->from("#__virtualdesk_sectoratividade")
                ->order('name ASC' )
            );

            $dataReturn = $db->loadAssocList();

            return($dataReturn);

        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getSetorAtividadeById($idsetor, $lang)
    {
        if( empty($idsetor) )  return false;
        if( empty($lang) )  return false;

        if( empty($lang) or ($lang='pt_PT') ) {
            $nameCol = 'name_pt as name';
        }
        else {
            $nameCol = 'name_en as name';
        }
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array($nameCol))
            ->from("#__virtualdesk_sectoratividade")
            ->where($db->quoteName('id').'='.$db->escape($idsetor))

        );
        $data = $db->loadResult();
        return($data);
    }




    /*
    * Verifica se todos os campos obrigatórios estão preenchidos na base de dados
    */
    public function checkAllRequiredBDField($UserJoomlaID)
    {
        if( empty($UserJoomlaID)) return false;
        if(JComponentHelper::getParams('com_virtualdesk')->get('userfield_checkdbfreq') == 0 ) return true;

        $db    = JFactory::getDbo();
        $origemTableVDUser = new VirtualDeskTableUser($db);
        $origemTableVDUser->load(array('idjos'=>$UserJoomlaID));

        $arUserFieldsConfig = $this->arUserFieldsConfig;

        foreach ($this->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)
        {
            // Se está vísivel
            if(  $this->arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Profile'] === true ) {
                // Se é onrigatório e na base de ddos esrtá vazio... erro é preciso então preencher dados
                switch($keyConfFieldNames) {
                    case 'nif':
                        $valorAtualBD = $origemTableVDUser->fiscalid;
                        break;
                    default:
                        $valorAtualBD = $origemTableVDUser->{$keyConfFieldNames};
                        break;
                }

                // Caso do NIf que pode ser o campo de login, nesse caso não comparo este campo
                if( ! ($keyConfFieldNames=='nif' && $this->arUserFieldLoginConfig['setUserFieldLoginTypeNIF']) ) {
                    if ((string)$this->arUserFieldsConfig['UserField_' . $valConfFieldNames . '_Required'] == 'required' && empty($valorAtualBD)) return false;
                }
            }
        }
        return(true);
    }






}
