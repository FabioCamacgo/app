<?php
/**
 * Created by PhpStorm.
 * User:
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
//JLoader::register('VirtualDeskTableTarefa', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/tarefa.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

class VirtualDeskSiteUrbanismoHelper
    {
        const tagchaveModulo = 'urbanismo';

        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnewalteracaoutilizacao4user.urbanismo.data', null);;
            $app->setUserState('com_virtualdesk.addnewautilizacaorecintos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewutilizacaorealizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewutilizacaonaoprecedida4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewemissaoalvara4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewprorrogacaoprazo4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewparecercompropriedade4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdestaqueparcela4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewprojetoemparcelamento4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewisencaoautorizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpropriedadehorizontal4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewcertidaocomprovativa4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewcertidaocedencia4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewcertidaourbanismo4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewcompatibilidade4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdenuncia4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewexposicao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewreclamacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewinfraestruturasaptas4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewobrasdemolicao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewobrasedificacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewobrasurbanizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewoperacoesloteamento4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewoutrasoperacoes4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewremodelacaoterrenos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewcertidaolocalizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewvistoriainicial4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdefinicaoobras4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewvistoriafinal4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewconsultareproducao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewcondicionamentosinformacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewacessorios4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewacessoriosedificacoes4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewviabilidaderealizar4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpreviaalteracaoutilizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpreviaobrasdemolicao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpreviaobrasedificacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpreviaobrasurbanizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpreviaoperacoesloteamento4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpreviaoutrasoperacoes4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewpreviaremodelacaoterrenos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewarranjoestetico4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewquesitos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewprojetoarquitetura4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewarquiteturaespecialidades4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewespecialidades4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewoperacoesurbanisticas4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewutilizacaoedificios4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewinformacaotermos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminprojetoarquitetura4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminarquiteturaespecialidades4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminprojetoespecialidades4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminobrasdemolicao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminobrasurbanizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminoperacoesloteamento4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminremodelacaoterrenos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminoutrasoperacoes4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminexecucaotrabalhos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminprorrogacaoespecialidades4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminprorrogacaolicenca4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalteracaoedificacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvaraparcial4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvarademolicao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvaraedificacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvaraurbanizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvarasemobras4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvaracomobras4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvaraoutras4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminalvararemodelacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminobrasurbanizacaoalteracao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladminobrasedificacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewladmincomunicacaodurante4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewlicencaespecial4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewobrasisentas4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewoperacoesurbanisticasparecer4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewocupacaoespaco4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewcoordenadorrojetos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdiretorobra4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdiretorfiscalizacao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewsubstituicaorequerente4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewsubstituicaotitular4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdesistencia4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewdevolucaocaucao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewhabitacaodeposito4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewhabitacaoemissao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewiniciotrabalhos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewjuncaoelementos4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewprazosexecucao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewprazosjuncao4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewrececaodefinitiva4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewrececaoprovisoria4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewreducaomontante4user.urbanismo.data', null);
            $app->setUserState('com_virtualdesk.addnewnumeropolicia4user.urbanismo.data', null);

        }

    }
?>