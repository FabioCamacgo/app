<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 *
 * @since  1.6
 */

JLoader::register('VirtualDeskTableQueimadasFiles', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/queimadas_files.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE. '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');

class VirtualDeskSiteQueimadasFilesHelper
{

    private $filefolder;

    public $idprocesso;
    public $tagprocesso;
    public $filetype;
    private $tmp_filename;

    public $resFileSaved;
    public $resFilesSave2Table;


    public function __construct()
    {
        $this->filefolder = JComponentHelper::getParams('com_virtualdesk')->get('queimadas_filefolder');
    }

    public function SetIdProcesso($idprocesso){
        $this->idprocesso = $idprocesso;
    }

    public function SetUserJoomlaId($iduser){
        $this->iduser = $iduser;
    }

    public function SetTagProcesso($tagprocesso){
        $this->tagprocesso = $tagprocesso;
    }



    public function saveListFileByAjax ($FileUploadTag)
    {
        $dtz     = new DateTimeZone("Europe/London"); //Your timezone
        $date    = new DateTime('NOW', $dtz);

        $FileListAjax = $_FILES[$FileUploadTag];
        if(empty($FileListAjax)) return false;

        $FileList = array();

        $timenow = $date->getTimestamp();
        $data    = $date->format('Y-m-d H:i:s');
        $doc_name  = 'queimadasfile_' . rand(100,999) . '_' . $timenow ;

        $rowFileAdd = array();
        $rowFileAdd['name']  = $FileListAjax['name'][0];
        $rowFileAdd['size']  = $FileListAjax['size'][0];
        $rowFileAdd['error'] = $FileListAjax['error'][0];
        $rowFileAdd['type']  = $FileListAjax['type'][0];
        $rowFileAdd['tmp_name']  = $FileListAjax['tmp_name'][0];
        $rowFileAdd['forceuniquename'] = $doc_name;
        $FileList[]['attachment'] = $rowFileAdd;

        $fold= 'media/com_virtualdesk/queimadas/uploads';

        $this->resFileSaved = VirtualDeskSiteFileUploadHelper::saveFileListToFolderSecured (0, $FileList,  $fold);

        if ($this->resFileSaved !== false or $this->resFileSaved !== null) {

            $this->resFilesSave2Table = $this->saveFileToTable($this->resFileSaved);
        }

        return(true);
    }


    public function saveFileToFolder ($FileList)
    {
        $fold= 'media/com_virtualdesk/queimadas/uploads';
        $saveFileList = VirtualDeskSiteFileUploadHelper::saveFileListToFolderSecured (0, $FileList,  $fold, '');

        return $saveFileList;
    }


    public function saveFileToTable ($FileSaved )
    {
        // Se o ficheiro estiver vazio sai... sem dar erro
        if ( !is_array($FileSaved) )  return false;
        if ( empty($FileSaved) )  return false;

        foreach ($FileSaved as $rowFileSaved) {

            $data['idprocesso'] = $this->idprocesso;
            $data['tagprocesso'] = $this->tagprocesso;
            $data['filename'] = $rowFileSaved['filename'];
            $data['basename'] = $rowFileSaved['basename'];
            $data['ext'] = $rowFileSaved['ext'];
            $data['type'] = $rowFileSaved['type'];
            $data['size'] = $rowFileSaved['size'];
            $data['desc'] = $rowFileSaved['desc'];
            $data['filepath'] = $rowFileSaved['filepath'];

            // Carrega dados atuais na base de dados
            $db = JFactory::getDbo();
            $Table = new VirtualDeskTableQueimadasFiles($db);

            // Bind the data.
            if (!$Table->bind($data)) return false;

            // Store the data.
            if (!$Table->save($data)) return false;
        }
        return true;
    }


    /**
     * Método para poder descarregar um ficheiro a partir de um link com o seu basename
     * Eset link vai poder ser invocado de um email sem ser necessário iniciar sessão
     * @return mixed
     */
    public function downloadWithNoSession()
    {
        $jinput = JFactory::getApplication()->input;
        $EventoRefId = $jinput->get('refid');
        $basename    = $jinput->get('bname');
        $checkname   = $jinput->get('cname'); // nome

        // vai comparar se o basename e o cname correspondem a partir da desencriptação
        if(!empty($checkname)) {
            $verificaNome = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($basename);
            $objFile = null;
            if( (string)$verificaNome === (string) $checkname && (string)$verificaNome != '' ) {
                // carrega o ficheiro...
                $objFile = $this->getFileName2GuestLink ($EventoRefId, $basename);
            }
        }

        if (empty($objFile)) {
            return false;
            exit;
        }
        else {

            VirtualDeskSiteFileUploadHelper::setDownloadHeaders ($objFile);
            exit;
        }
    }



    /*
    * Carrega ficheiro associado a um registo SEM ser necessário o joomla user
    */
    public function getFileName2GuestLink ($RefId, $basename)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idprocesso','a.basename','a.desc','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->from("#__virtualdesk_AguaAmbiente_queimadas_files AS a")
                ->where($db->quoteName('a.idprocesso') . '=\'' . $db->escape($RefId). '\' '
                    . ' AND ' . $db->quoteName('a.basename') . '= \'' . $db->escape($basename) . '\' ' )
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObject();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }



    /*
    * Carrega a lista de ficheiros associados a um evento/cultura pelo RefId
    */
    public function getFileListByRefId ($RefId)
    {
        try
        {   $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('a.id','a.idprocesso','a.basename','a.desc','a.filename','a.filepath','a.ext','a.created','a.modified') )
                ->from("#__virtualdesk_AguaAmbiente_queimadas_files AS a")
                ->where($db->quoteName('a.idprocesso') . '=\'' . $db->escape($RefId). '\' ')
            );

            // Dump the query
            //echo $db->getQuery()->dump();

            $dataReturn = $db->loadObjectList();

            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /*
    * Gera a lista de "guest links" para fazer download de ficheiros sem haver necessidade de iniciar sessão na App
    */
    public function getFileGuestLinkByRefId ($RefId)
    {
        if(empty($RefId)) return(false);

        $objFileLst = $this->getFileListByRefId ($RefId);

        // Percorre cada ficheiro encontrado e gera um link para ser utilizado posteriormente

        if(empty($objFileLst))return(false);

        $arReturn = array();
        foreach ($objFileLst as $rowFile) {
            $objFileTmp = new stdClass();
            $objFileTmp->refid = $rowFile->idprocesso;
            $objFileTmp->bname = $rowFile->basename;
            $objFileTmp->fname = $rowFile->filename;
            $objFileTmp->fpath = $rowFile->filepath;
            $objFileTmp->desc = $rowFile->desc;
            $objFileTmp->cname = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($rowFile->basename);

            $objFileTmp->guestlink = JUri::root().'queimadas/dl/?refid=' . $rowFile->idprocesso . '&bname=' . $rowFile->basename . '&cname=' .  $objFileTmp->cname;

            //$objFileTmp->guestlink = $rowFile->filepath . '/' . $rowFile->filename;

            $arReturn[] = $objFileTmp;
        }

        return($arReturn);
    }



}