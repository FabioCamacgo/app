<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');

    class VirtualDeskSiteCemiteriosHelper{

        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
        }

        public static function getConcelhos(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcSelect($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeconcelho($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
                ->order('concelho ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getFregReq($idwebsitelist,$onAjaxVD=0)
        {

            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('freguesia ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, freguesia as name, concelho_id'))
                    ->from("#__virtualdesk_digitalGov_freguesias")
                    ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                    ->order('name')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getFregReq2($concelho,$idSelectFreg)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia as name, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($concelho))
                ->where($db->quoteName('id') . '!=' . $db->escape($idSelectFreg))
                ->order('name')
            );
            $data = $db->loadAssocList();

            return ($data);
        }

        public static function getFregSelectID($freg){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('id')
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('freguesia') . "='" . $db->escape($freg) . "'")

            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getPedido(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, pedido'))
                ->from("#__virtualdesk_AguaAmbiente_cemiterios_tipoPedido")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getPedidoSelect($pedido){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('pedido')
                ->from("#__virtualdesk_AguaAmbiente_cemiterios_tipoPedido")
                ->where($db->quoteName('id') . '=' . $db->escape($pedido))
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludePedido($pedido){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, pedido'))
                ->from("#__virtualdesk_AguaAmbiente_cemiterios_tipoPedido")
                ->where($db->quoteName('id') . '!=' . $db->escape($pedido))
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function validaNome($nome){
            if($nome == Null){
                return (1);
            }else if(is_numeric($nome)){
                return (2);
            } else if(preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $nome)){
                return (2);
            } else{
                return (0);
            }
        }

        public static function validaTelefone($telefone){
            if($telefone == Null){
                return (1);
            } else if($telefone != Null){
                $TelefoneSplit=str_split($telefone);
                if(!is_numeric($telefone) || strlen($telefone) != 9){
                    return (2);
                } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
                    return (2);
                } else if($TelefoneSplit[0] == 9){
                    if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
                        return (2);
                    }
                }
            }
        }

        public static function validaFreguesia($freg){
            if($freg == Null){
                return (1);
            } else{
                return (0);
            }
        }

        public static function validaNif($fiscalid){
            $nif=trim($fiscalid);
            $ignoreFirst=true;
            if (!is_numeric($nif) || strlen($nif)!=9) {
                return (1);
            } else {
                $nifSplit=str_split($nif);
                if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);

                    if($checkDigit>=10) $checkDigit=0;

                    if ($checkDigit==$nifSplit[8]) {

                    } else {
                        return (1);
                    }
                } else {
                    return (1);
                }
            }
        }

        public static function validaEmail($email, $confemail){
            $conta = "/^[a-zA-Z0-9\._-]+@";
            $domino = "[a-zA-Z0-9\._-]+.[.]";
            $extensao = "([a-zA-Z]{2,4})$/";
            $pattern = $conta . $domino . $extensao;
            if($email == Null){
                return (1);
            } else if(!preg_match($pattern, $email)) {
                return (2);
            } else if($email != $confemail){
                return (3);
            } else{
                return (0);
            }
        }

        public static function validaMorada($local){
            if($local == Null){
                return (1);
            } else{
                return (0);
            }
        }

        public static function validaConcelho($concelho){
            if($concelho == Null || $concelho == 0){
                return (1);
            } else{
                return (0);
            }
        }

        public static function validaLocalidade($localidade){
            if($localidade != Null){
                if(is_numeric($localidade)){
                    return (1);
                } else if(preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $localidade)){
                    return (1);
                } else{
                    return (0);
                }
            } else {
                return (0);
            }
        }

        public static function validaCodigoPostal($codPostal4, $codPostal3, $codPostalText){
            if($codPostal4 == Null || $codPostal3 == Null || $codPostalText == Null){
                return (1);
            } else if(!is_numeric($codPostal4) || !is_numeric($codPostal3) || is_numeric($codPostalText)){
                return (1);
            } else if(preg_match('([0-9])', $codPostalText)){
                return (1);
            } else if(strlen($codPostal4) != 4 || strlen($codPostal3) != 3){
                return (1);
            } else{
                return (0);
            }
        }

        public static function validaCC($cc){
            if($cc == Null){
                return (1);
            } else if (!is_numeric($cc)) {
                return (2);
            } else if(strlen($cc) < 7) {
                return (2);
            } else if(strlen($cc) > 8) {
                return (2);
            } else{
                return (0);
            }
        }

        public static function validaValidadeCC($validadeCC, $dataAtual){
            if($validadeCC == Null || empty($validadeCC)){
                return (1);
            } else {
                $splitCoord = explode("-", $validadeCC);
                $data = $splitCoord[2] . '-' . $splitCoord[1] . '-' . $splitCoord[0];
                if($data < $dataAtual) {
                    return (2);
                } else {
                    return(0);
                }
            }
        }

        public static function validaDataImunacao($dataImunacao, $dataAtual){
            if($dataImunacao == Null || empty($dataImunacao)){
                return (1);
            } else {
                $splitCoord = explode("-", $dataImunacao);
                $data = $splitCoord[2] . '-' . $splitCoord[1] . '-' . $splitCoord[0];
                if($data > $dataAtual) {
                    return (2);
                } else if($data == $dataAtual){
                    return (2);
                } else {
                    return(0);
                }
            }
        }

        public static function validaSepultura($numSepultura){
            if($numSepultura == Null){
                return (1);
            } else if (!is_numeric($numSepultura)) {
                return (2);
            } else {
                return (0);
            }
        }

        public static function SaveNovoPedido($referencia, $nome, $morada, $concelho, $idSelectFreg, $localidade, $codPostal, $cc, $validadeCC, $telefone, $fiscalid, $email, $pedido, $qualPedido, $nomeFalecido, $dataImunacao, $cemiterio, $talhao, $numSepultura, $observacoes, $dataAtual) {
            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('referencia','nome','morada','concelho','fregReq','localidade','codPostal','cc','validadeCC','telefone','nif','email','tipoReq','qualReq','nomeFalecido','dataImunacao','cemiterio','talhao','sepultura','observacoes','estado','dataCriacao');
            $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nome)), $db->quote($db->escape($morada)), $db->quote($db->escape($concelho)), $db->quote($db->escape($idSelectFreg)), $db->quote($db->escape($localidade)), $db->quote($db->escape($codPostal)), $db->quote($db->escape($cc)), $db->quote($db->escape($validadeCC)), $db->quote($db->escape($telefone)), $db->quote($db->escape($fiscalid)), $db->quote($db->escape($email)), $db->quote($db->escape($pedido)), $db->quote($db->escape($qualPedido)), $db->quote($db->escape($nomeFalecido)), $db->quote($db->escape($dataImunacao)), $db->quote($db->escape($cemiterio)), $db->quote($db->escape($talhao)), $db->quote($db->escape($numSepultura)), $db->quote($db->escape($observacoes)), $db->quote('1'),  $db->quote($db->escape($dataAtual)));
            $query
                ->insert($db->quoteName('#__virtualdesk_AguaAmbiente_cemiterios'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);
        }

    }
?>