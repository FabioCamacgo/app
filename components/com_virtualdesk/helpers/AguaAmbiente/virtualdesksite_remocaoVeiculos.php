<?php
/**
 * Created by PhpStorm.
 * User: Camacho
 * Date: 04/09/2018
 * Time: 14:42
 */

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

//JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
//JLoader::register('VirtualDeskTableUser', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/alerta.php');
//JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

class VirtualDeskSiteRemocaoVeiculosHelper
{

    /*Dropdown Freguesias*/
    public static function getFreguesia($concelho)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia, concelho_id'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
            ->order('freguesia ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getFregSelect($freg){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('freguesia')
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('id') . "='" . $db->escape($freg) . "'")

        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludeFreguesia($concelho, $freg){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.id, a.freguesia as freguesia'))
            ->from("#__virtualdesk_digitalGov_freguesias as a")
            ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
            ->where($db->quoteName('id') . "!='" . $db->escape($freg) . "'")
            ->order('a.id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getFreguesia2($concelho)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia, concelho_id'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
            ->order('freguesia ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getFregSelect2($freg){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('freguesia')
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('id') . "='" . $db->escape($freg) . "'")

        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getFregSelectID($freg){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('id')
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('freguesia') . "='" . $db->escape($freg) . "'")

        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludeFreguesia2($concelho, $freg){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.id, a.freguesia as freguesia'))
            ->from("#__virtualdesk_digitalGov_freguesias as a")
            ->where($db->quoteName('concelho_id') . "='" . $db->escape($concelho) . "'")
            ->where($db->quoteName('id_') . "!='" . $db->escape($freg) . "'")
            ->order('a.id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    /*Nome Freguesia*/
    public static function getFregName($freguesia)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('freguesia')
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('id') . "='" . $db->escape($freguesia) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function getConcelhos(){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, concelho'))
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->order('concelho ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getConcSelect($concelho){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('concelho')
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    public static function excludeconcelho($concelho){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('Id_concelho, concelho'))
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
            ->order('concelho ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    public static function getFregReq($idwebsitelist,$onAjaxVD=0)
    {

        if (empty($idwebsitelist)) return false;


        if($onAjaxVD=0) {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                ->order('freguesia ASC')
            );
            $data = $db->loadAssocList();
        }
        else{
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, freguesia as name, concelho_id'))
                ->from("#__virtualdesk_digitalGov_freguesias")
                ->where($db->quoteName('concelho_id') . '=' . $db->escape($idwebsitelist))
                ->order('name')
            );
            $data = $db->loadAssocList();
        }

        return ($data);
    }

    public static function getFregReq2($concelho,$idSelectFreg)
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia as name, concelho_id'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id') . '=' . $db->escape($concelho))
            ->where($db->quoteName('id') . '!=' . $db->escape($idSelectFreg))
            ->order('name')
        );
        $data = $db->loadAssocList();

        return ($data);
    }


    public static function validaNome($nome){
        if($nome == Null){
            return (1);
        }else if(is_numeric($nome)){
            return (2);
        } else if(preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $nome)){
            return (2);
        } else{
            return (0);
        }
    }


    public static function validaMorada($morada){
        if($morada == Null){
            return (1);
        } else{
            return (0);
        }
    }


    public static function validaLocalViatura($localViatura){
        if($localViatura == Null){
            return (1);
        } else{
            return (0);
        }
    }


    public static function validaCodigoPostal($codPostal4, $codPostal3, $codPostalText){
        if($codPostal4 == Null || $codPostal3 == Null || $codPostalText == Null){
            return (1);
        } else if(!is_numeric($codPostal4) || !is_numeric($codPostal3) || is_numeric($codPostalText)){
            return (1);
        } else if(preg_match('([0-9])', $codPostalText)){
            return (1);
        } else if(strlen($codPostal4) != 4 || strlen($codPostal3) != 3){
            return (1);
        } else{
            return (0);
        }
    }


    public static function validaEmail($email, $confemail){
        $conta = "/^[a-zA-Z0-9\._-]+@";
        $domino = "[a-zA-Z0-9\._-]+.[.]";
        $extensao = "([a-zA-Z]{2,4})$/";
        $pattern = $conta . $domino . $extensao;
        if($email == Null){
            return (1);
        } else if(!preg_match($pattern, $email)) {
            return (2);
        } else if($email != $confemail){
            return (3);
        } else{
            return (0);
        }
    }


    public static function numberDia($dia){
        if($dia == '1'){
            return ('01');
        } else if($dia == '2'){
            return ('02');
        } else if($dia == '3'){
            return ('03');
        } else if($dia == '4'){
            return ('04');
        } else if($dia == '5'){
            return ('05');
        } else if($dia == '6'){
            return ('06');
        } else if($dia == '7'){
            return ('07');
        } else if($dia == '8'){
            return ('08');
        } else if($dia == '9'){
            return ('09');
        } else{
            return ($dia);
        }
    }


    public static function numberMes($mes){
        if($mes == '1'){
            return ('01');
        } else if($mes == '2'){
            return ('02');
        } else if($mes == '3'){
            return ('03');
        } else if($mes == '4'){
            return ('04');
        } else if($mes == '5'){
            return ('05');
        } else if($mes == '6'){
            return ('06');
        } else if($mes == '7'){
            return ('07');
        } else if($mes == '8'){
            return ('08');
        } else if($mes == '9'){
            return ('09');
        } else{
            return ($mes);
        }
    }


    public static function validaNif($fiscalid){
        $nif=trim($fiscalid);
        $ignoreFirst=true;
        if (!is_numeric($nif) || strlen($nif)!=9) {
            return (1);
        } else {
            $nifSplit=str_split($nif);
            if (in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9)) || $ignoreFirst) {
                $checkDigit=0;
                for($i=0; $i<8; $i++) {
                    $checkDigit+=$nifSplit[$i]*(10-$i-1);
                }
                $checkDigit=11-($checkDigit % 11);

                if($checkDigit>=10) $checkDigit=0;

                if ($checkDigit==$nifSplit[8]) {

                } else {
                    return (1);
                }
            } else {
                return (1);
            }
        }
    }


    public static function validaTelefone($telefone){
        if($telefone == Null){
            return (1);
        } else if($telefone != Null){
            $TelefoneSplit=str_split($telefone);
            if(!is_numeric($telefone) || strlen($telefone) != 9){
                return (2);
            } else if($TelefoneSplit[0] == 1 || $TelefoneSplit[0] == 3 || $TelefoneSplit[0] == 4 || $TelefoneSplit[0] == 5 || $TelefoneSplit[0] == 6 || $TelefoneSplit[0] == 7 || $TelefoneSplit[0] == 8 || $TelefoneSplit[0] == 0){
                return (2);
            } else if($TelefoneSplit[0] == 9){
                if($TelefoneSplit[1] == 4 || $TelefoneSplit[1] == 5 || $TelefoneSplit[1] == 7 || $TelefoneSplit[1] == 8 || $TelefoneSplit[1] == 9 || $TelefoneSplit[1] == 0){
                    return (2);
                }
            }
        }
    }

    public static function validaCC($cc){
        if($cc == Null){
            return (1);
        } else if (!is_numeric($cc)) {
            return (2);
        } else if(strlen($cc) < 7) {
            return (2);
        } else if(strlen($cc) > 8) {
            return (2);
        } else{
            return (0);
        }
    }

    public static function validaValidadeCC($validadeCC, $dataAtual){
        if($validadeCC == Null || empty($validadeCC)){
            return (1);
        } else {
            $splitCoord = explode("-", $validadeCC);
            $data = $splitCoord[2] . '-' . $splitCoord[1] . '-' . $splitCoord[0];
            if($data < $dataAtual) {
                return (2);
            } else {
                return(0);
            }
        }
    }


    public static function validaFreguesia($freguesias){
        if($freguesias == Null){
            return (1);
        } else{
            return (0);
        }
    }


    public static function validaConcelho($concelho){
        if($concelho == Null || $concelho == 0){
            return (1);
        } else{
            return (0);
        }
    }


    public static function validaFreguesiaVeiculo($freguesiasVeiculo){
        if($freguesiasVeiculo == Null){
            return (1);
        } else{
            return (0);
        }
    }


    public static function validaMarca($marca){
        if($marca != Null){
            if(is_numeric($marca)){
                return (1);
            } else if(preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $marca)){
                return (1);
            } else{
                return (0);
            }
        } else {
            return (0);
        }
    }


    public static function validaCor($cor){
        if($cor != Null){
            if(is_numeric($cor)){
                return (1);
            } else if(preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $cor)){
                return (1);
            } else{
                return (0);
            }
        } else {
            return (0);
        }
    }


    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
    }


    /*Dropdown Permanencia*/
    public static function getPermanencia()
    {
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, permanencia'))
            ->from("#__virtualdesk_AguaAmbiente_remocao_veiculos_permanencia")
            ->order('id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    public static function getPermSelect2($perm){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('permanencia')
            ->from("#__virtualdesk_AguaAmbiente_remocao_veiculos_permanencia")
            ->where($db->quoteName('id') . "='" . $db->escape($perm) . "'")

        );
        $data = $db->loadResult();
        return ($data);
    }


    public static function excludePerm($perm){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('a.id, a.permanencia as permanencia'))
            ->from("#__virtualdesk_AguaAmbiente_remocao_veiculos_permanencia as a")
            ->where($db->quoteName('id') . "!='" . $db->escape($perm) . "'")
            ->order('a.id ASC')
        );
        $data = $db->loadAssocList();
        return ($data);
    }





    public static function SaveNovoRegisto($referencia, $nome, $morada, $concelho, $idSelectFreg, $localidade, $codPostal, $cc, $validadeCC, $telefone, $email, $fiscalid, $marca, $modelo, $cor, $matricula, $freguesiasVeiculo, $perm, $localViatura, $lat, $long, $dataAtual){
        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);
        $columns = array('referencia','nome','morada','concelho','fregReq','localidade','codPostal','cc','validadeCC','telefone','email','nif','marca','modelo','cor','matricula','freguesia_viatura','tempo_permanencia','morada_viatura','latitude','longitude','data_entrada','estado');
        $values = array($db->quote($db->escape($referencia)), $db->quote($db->escape($nome)), $db->quote($db->escape($morada)), $db->quote($db->escape($concelho)), $db->quote($db->escape($idSelectFreg)), $db->quote($db->escape($localidade)), $db->quote($db->escape($codPostal)), $db->quote($db->escape($cc)), $db->quote($db->escape($validadeCC)), $db->quote($db->escape($telefone)), $db->quote($db->escape($email)), $db->quote($db->escape($fiscalid)), $db->quote($db->escape($marca)), $db->quote($db->escape($modelo)), $db->quote($db->escape($cor)), $db->quote($db->escape($matricula)), $db->quote($db->escape($freguesiasVeiculo)), $db->quote($db->escape($perm)), $db->quote($db->escape($localViatura)), $db->quote($db->escape($lat)), $db->quote($db->escape($long)),  $db->quote($db->escape($dataAtual)),  $db->quote('1'));
        $query
            ->insert($db->quoteName('#__virtualdesk_AguaAmbiente_remocao_veiculos'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);

        $result = (boolean) $db->execute();


        return($result);

    }

    public static function getNomeMes($mesAtual){
        if($mesAtual == '01'){
            $mes = 'Janeiro';
        } else if($mesAtual == '02'){
            $mes = 'Fevereiro';
        } else if($mesAtual == '03'){
            $mes = 'Mar&ccedil;o';
        } else if($mesAtual == '04'){
            $mes = 'Abril';
        } else if($mesAtual == '05'){
            $mes = 'Maio';
        } else if($mesAtual == '06'){
            $mes = 'Junho';
        } else if($mesAtual == '07'){
            $mes = 'Julho';
        } else if($mesAtual == '08'){
            $mes = 'Agosto';
        } else if($mesAtual == '09'){
            $mes = 'Setembro';
        } else if($mesAtual == '10'){
            $mes = 'Outubro';
        } else if($mesAtual == '11'){
            $mes = 'Novembro';
        } else if($mesAtual == '12'){
            $mes = 'Dezembro';
        }

        return ($mes);
    }


    /* Carrega lista com dados de todos os pedidos (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getRemocaoVeiculosList4Manager ($vbForDataTables=false, $setLimit=-1)
    {
        // Check PERMISSÕES
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('ambiente', 'list4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agenda','list4managers');
        if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                $obParam    = new VirtualDeskSiteParamsHelper();
                // Gerar Links com id para o item de menu por defeito
                $AmbienteMenuId4Manager = $obParam->getParamsByTag('Ambiente_Menu_Id_By_Default_4Managers');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=remocao_veiculos_view4manager&Itemid='.$AmbienteMenuId4Manager.'&remocaoveiculo_id=');

                $table  = " ( SELECT a.id as id, a.id as remocaoveiculo_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo ";
                $table .= " , a.nome as nome_requerente , CONCAT( a.marca ,' / ', a.modelo ,' / ', a.matricula) as desc_viatura ";
                $table .= " , d.freguesia as freguesia_viatura, b.freguesia as freguesia_requerente, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado ";
                $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                $table .= " FROM ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos_freguesia AS b ON b.id_freguesia = a.fregReq ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos_freguesia AS d ON d.id_freguesia = a.freguesia_viatura ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos_estado AS e ON e.id = a.estado ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'created',       'dt' => 0 ),
                    array( 'db' => 'codigo',        'dt' => 1 ),
                    array( 'db' => 'desc_viatura',  'dt' => 2 ),
                    array( 'db' => 'estado',        'dt' => 3 ),
                    array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',      'dt' => 5 ),
                    array( 'db' => 'id',            'dt' => 6 ),
                    array( 'db' => 'modified',      'dt' => 7 ),
                    array( 'db' => 'createdFull',   'dt' => 8 ),
                    array( 'db' => 'nome_requerente',   'dt' => 9 ),
                    array( 'db' => 'freguesia_viatura', 'dt' => 10 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com dados de todos os pedidos (acesso ao User). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
    public static function getRemocaoVeiculosList4User ($vbForDataTables=false, $setLimit=-1)
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agenda', 'list4users');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
        if( empty($UserJoomlaID) )  return false;
        if( (int)$UserJoomlaID<=0 )  return false;

        // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
        // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
        $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

        if((int)$UserSessionNIF<=0) return false;

        try
        {
            $lang = VirtualDeskHelper::getLanguageTag();
            if( empty($lang) or ($lang='pt_PT') ) {
                $EstadoName = 'estado_PT';
            }
            else {
                $EstadoName = 'estado_EN';
            }

            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host = $conf->get('host');
                $user = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                $obParam    = new VirtualDeskSiteParamsHelper();
                // Gerar Links com id para o item de menu por defeito
                $AmbienteMenuId4User = $obParam->getParamsByTag('Ambiente_Menu_Id_By_Default_4Users');

                // Gerar Link para o detalhe...
                $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=remocao_veiculos_view4user&Itemid='.$AmbienteMenuId4User.'&remocaoveiculo_id=');

                $table  = " ( SELECT a.id as id, a.id as remocaoveiculo_id, CONCAT('".$dummyHRef."', CAST(a.id as CHAR(10))) as dummy, a.referencia as codigo ";
                $table .= " , a.nome as nome_requerente , CONCAT( a.marca ,' / ', a.modelo ,' / ', a.matricula) as desc_viatura ";
                $table .= " , d.freguesia as freguesia_viatura, b.freguesia as freguesia_requerente, IFNULL(e.".$EstadoName.", ' ') as estado, a.estado as idestado ";
                $table .= " , DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as created, DATE_FORMAT(a.data_criacao, '%Y-%m-%d') as modified, DATE_FORMAT(a.data_criacao, '%Y-%m-%d %H:%i') as createdFull ";
                $table .= " FROM ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos_freguesia AS b ON b.id_freguesia = a.fregReq ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos_freguesia AS d ON d.id_freguesia = a.freguesia_viatura ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_AguaAmbiente_remocao_veiculos_estado AS e ON e.id = a.estado ";
                $table .= " WHERE (a.nif=$UserSessionNIF) ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'created',       'dt' => 0 ),
                    array( 'db' => 'codigo',        'dt' => 1 ),
                    array( 'db' => 'desc_viatura',  'dt' => 2 ),
                    array( 'db' => 'estado',        'dt' => 3 ),
                    array( 'db' => 'dummy',         'dt' => 4 ,'formatter'=>'URL_ENCRYPT'),
                    array( 'db' => 'idestado',      'dt' => 5 ),
                    array( 'db' => 'id',            'dt' => 6 ),
                    array( 'db' => 'modified',      'dt' => 7 ),
                    array( 'db' => 'createdFull',   'dt' => 8 ),
                    array( 'db' => 'nome_requerente',   'dt' => 9 ),
                    array( 'db' => 'freguesia_viatura', 'dt' => 10 )
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            // *** Joomla QUERY  ***
            $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
            if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

            $db = JFactory::getDBO();



            return('');
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS os estados */
    public static function getRemocaoVeiculosEstadoAllOptions ($lang)
    {

        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // Para o caso de o módulo não estar ativo, saímos logo caso contrário fica a mensagem de erro.
        // Neste contexto não interessa mostrar a mensagem porque este método está a ser utilizado no homepage
        $vbChecModuleAgenda = $objCheckPerm->loadModuleEnabledByTag('ambiente');
        if((int)$vbChecModuleAgenda==0)  return false;

        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('ambiente');                  // verifica permissão de read
        if($vbHasAccess===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select( array('id','estado_PT as name_pt','estado_EN as name_en') )
                ->from("#__virtualdesk_AguaAmbiente_remocao_veiculos_estado")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                if( empty($lang) or ($lang='pt_PT') ) {
                    $rowName = $row->name_pt;
                }
                else {
                    $rowName = $row->name_en;
                }
                $response[] = array(
                    'id' => $row->id,
                    'name' => $rowName
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    /* Devolve CSS a ser aplicado de acordo com o ESTADO atual */
    public static function getRemocaoVeiculosEstadoCSS ($idestado)
    {
        $defCss = 'label-default';
        switch ($idestado)
        {   case '1':
                // pendente
                $defCss = 'label-pendente';
                break;
            case '2':
                // concluido
                $defCss = 'label-concluido';
                break;
        }
        return ($defCss);
    }


   /* public static function SendEmailAdmin($referencia, $mailrecipient1, $nome, $morada, $codPostal, $nomeFreguesia, $telefone, $email, $cc, $EmissaoCC, $fiscalid, $marcaMail, $modeloMail, $corMail, $matriculaMail, $localViatura, $nomeFreguesiaVeiculo, $nomeMes){

        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_RemocaoVeiculos_Email.html');

        require_once(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpdf.php');
        $pdf = new VirtualDeskPDFHelper();



        $pdf->pdfRemocaoVeiculos($referencia, $nome, $morada, $codPostal, $nomeFreguesia, $telefone, $email, $cc, $EmissaoCC, $fiscalid, $marcaMail, $modeloMail, $corMail, $matriculaMail, $localViatura, $nomeFreguesiaVeiculo, $nomeMes);

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_REMOCAOVEICULOS_EMAIL_TITULO');
        $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_REMOCAOVEICULOS_EMAIL_TITULO');
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_REMOCAOVEICULOS_EMAIL_CORPO', $referencia);



        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);


        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($mailrecipient1);
        $newActivationEmail->setSubject($data['sitename']);
        $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Portal_Machico_Ambiente.jpg', "bannertOP", "Portal_Machico_Ambiente.jpg");

        $pdf->SetIdProcesso ($referencia);
        $pdf->SetTagProcesso ('Remocao_Veiculos');
        $resSave = $pdf->saveFile();
        $resFilepath = $pdf->resFileSaved ['filepath'];
        $resFilename = $pdf->resFileSaved ['filename'];
        $newActivationEmail->addAttachment(JPATH_BASE.'/'.$resFilepath.'/'.$resFilename);


        $result = (boolean) $newActivationEmail->send();

        // Check for an error.
        if ($result !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return($result);

    }



    public static function SendEmailUser($referencia, $nome, $morada, $codPostal, $nomeFreguesia, $telefone, $email, $cc, $EmissaoCC, $fiscalid, $marcaMail, $modeloMail, $corMail, $matriculaMail, $localViatura, $nomeFreguesiaVeiculo, $nomeMes){
        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_RemocaoVeiculosUser_Email.html');

        require_once(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpdf.php');
        $pdf = new VirtualDeskPDFHelper();



        $pdf->pdfRemocaoVeiculos($referencia, $nome, $morada, $codPostal, $nomeFreguesia, $telefone, $email, $cc, $EmissaoCC, $fiscalid, $marcaMail, $modeloMail, $corMail, $matriculaMail, $localViatura, $nomeFreguesiaVeiculo, $nomeMes);

        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $config->get('mailfrom');
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_REMOCAOVEICULOS_EMAILUSER_TITULO');
        $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_REMOCAOVEICULOS_EMAILUSER_TITULO');
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_REMOCAOVEICULOS_EMAILUSER_CORPO');
        $BODY_2MESSAGE       = JText::sprintf('COM_VIRTUALDESK_REMOCAOVEICULOS_EMAILUSER_CORPO2');



        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_2MESSAGE",$BODY_2MESSAGE, $body);
        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);


        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($email);
        $newActivationEmail->setSubject($data['sitename']);
        $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Portal_Machico_Ambiente.jpg', "bannertOP", "Portal_Machico_Ambiente.jpg");

        $pdf->SetIdProcesso ($referencia);
        $pdf->SetTagProcesso ('Remocao_Veiculos_USER');
        $resSave = $pdf->saveFile();
        $resFilepath = $pdf->resFileSaved ['filepath'];
        $resFilename = $pdf->resFileSaved ['filename'];
        $newActivationEmail->addAttachment(JPATH_BASE.'/'.$resFilepath.'/'.$resFilename);


        $result = (boolean) $newActivationEmail->send();

        // Check for an error.
        if ($result !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return($result);
    }
    */
}
?>