<?php
/**
 * @package     Joomla.Site.VirtualDesk
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskLogHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesklog.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');


/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteLogAdminHelper
{


    function __construct() {
    }


    public static function getUserActivationLogs ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('logadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                //$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');

                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT(\'".$dummyHRef."\', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";
                $table  = " ( SELECT a.id as id, a.idactivation as idactivation, a.logstatus as logstatus, a.logdesc as logdesc, a.iduser as iduser, a.idjos as idjos ";
                $table .= "         , b.login as login, b.name as nome, a.email as email, a.token as token, a.created as created, a.ip as ip ";
                $table .= "         , DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull ";

                $table .= " FROM ".$dbprefix."virtualdesk_users_activation_received as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS b ON b.id=a.iduser ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'createdFull',    'dt' => 0 ),
                    array( 'db' => 'logstatus',      'dt' => 1 ),
                    array( 'db' => 'logdesc',        'dt' => 2 ),
                    array( 'db' => 'login',          'dt' => 3 ),
                    array( 'db' => 'nome',           'dt' => 4 ),
                    array( 'db' => 'email',          'dt' => 5 ),
                    array( 'db' => 'token',          'dt' => 6 ),
                    array( 'db' => 'ip',             'dt' => 7 ),
                    array( 'db' => 'id',             'dt' => 8 ),
                    array( 'db' => 'iduser',         'dt' => 9 ),
                    array( 'db' => 'idjos',          'dt' => 10 ),
                    array( 'db' => 'idactivation',   'dt' => 11 ),
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getUserActivation ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('logadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                //$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');

                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT(\'".$dummyHRef."\', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";
                $table  = " ( SELECT a.id as id, a.iduser as iduser, a.idjos as idjos ";
                $table .= "  , a.login as login, b.name as nome, a.email as email, a.token as token ";
                $table .= "  , DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull ";
                $table .= "  , DATE_FORMAT(a.modified, '%Y-%m-%d %H:%i:%s') as modifiedFull ";

                $table .= "  ,a.enabled as enabled, a.createdbyuser as createdbyuser, a.createdbyip as createdbyip, a.activated as activated ";
                $table .= "  ,a.activatedbyip as activatedbyip, a.created as created, a.modified as modified ";
                $table .= " FROM ".$dbprefix."virtualdesk_users_activation as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS b ON b.id=a.iduser ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'modifiedFull',   'dt' => 0 ),
                    array( 'db' => 'login',          'dt' => 1 ),
                    array( 'db' => 'nome',           'dt' => 2 ),
                    array( 'db' => 'email',          'dt' => 3 ),
                    array( 'db' => 'token',          'dt' => 4 ),
                    array( 'db' => 'enabled',        'dt' => 5 ),
                    array( 'db' => 'activated',      'dt' => 6 ),
                    array( 'db' => 'createdbyip',    'dt' => 7 ),

                    array( 'db' => 'createdbyuser',  'dt' => 8 ),
                    array( 'db' => 'activatedbyip',  'dt' => 9 ),
                    array( 'db' => 'id',             'dt' => 10 ),
                    array( 'db' => 'iduser',         'dt' => 11 ),
                    array( 'db' => 'idjos',          'dt' => 12 ),
                    array( 'db' => 'createdFull',    'dt' => 13 ),

                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEventLogs ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('logadmin', 'listdefault');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                //$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');
                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT(\'".$dummyHRef."\', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";

                $table  = " ( SELECT a.id as id, a.iduser as iduser, a.accesslevel AS idaccesslevel, a.category AS idcategory, a.title, a.created as created, a.ip as ip, b.name as category";
                $table .= " , c.name as accesslevel, d.email as email, d.login as login, d.name as nome , DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull";
                $table .= " , e.name as tipo_op, f.nome as modulo, g.nome as plugin, CONCAT( IFNULL(f.nome,'') ,' ', IFNULL(g.nome,'')) as modulo_plugin ";
                $table .= " , a.rgpd as rgpd, a.idmodulo as idmodulo, a.idplugin as idplugin, a.id_tipo_op as id_tipo_op , a.ref as ref ";
                $table .= " , CONCAT( IFNULL(a.desc,'') ,' ------ ', IFNULL(a.filelist,''))  as `desc` ";
                $table .= " FROM ".$dbprefix."virtualdesk_eventlog as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_eventlog_category AS b ON a.category=b.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_eventlog_accesslevel AS c ON a.accesslevel=c.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON a.iduser=d.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_eventlog_tipo_op AS e ON a.id_tipo_op=e.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_modulo AS f ON a.idmodulo=f.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_config_plugin AS g ON a.idplugin=g.id ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'createdFull',    'dt' => 0 ),
                    array( 'db' => 'login',          'dt' => 1 ),
                    array( 'db' => 'nome',           'dt' => 2 ),
                    array( 'db' => 'accesslevel',   'dt' => 3 ),
                    array( 'db' => 'category',       'dt' => 4 ),
                    array( 'db' => 'tipo_op',        'dt' => 5 ),
                    array( 'db' => 'title',          'dt' => 6 ),
                    array( 'db' => 'ref',            'dt' => 7 ),
                    array( 'db' => 'modulo_plugin',  'dt' => 8 ),
                    array( 'db' => 'rgpd',           'dt' => 9 ),
                    array( 'db' => 'ip',             'dt' => 10 ),

                    array( 'db' => 'id',             'dt' => 11 ),
                    array( 'db' => 'idcategory',     'dt' => 12 ),
                    array( 'db' => 'idaccesslevel',  'dt' => 13 ),
                    array( 'db' => 'idplugin',       'dt' => 14 ),
                    array( 'db' => 'idmodulo',       'dt' => 15 ),
                    array( 'db' => 'id_tipo_op',     'dt' => 16 ),
                    array( 'db' => 'desc',           'dt' => 17 ,'formatter'=>'HTMLSPECIALCHARS' ),
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEventLogsRGPD ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('logadmin', 'listdefault');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                //$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');
                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT(\'".$dummyHRef."\', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";

                $table  = " ( SELECT a.id as id, a.iduser as iduser, a.accesslevel AS idaccesslevel, a.category AS idcategory, a.title, a.created as created, a.ip as ip, b.name as category";
                $table .= " , c.name as accesslevel, d.email as email, d.login as login, d.name as nome , DATE_FORMAT(a.created, '%Y-%m-%d %H:%i:%s') as createdFull";
                $table .= " , e.name as tipo_op, f.nome as modulo, g.nome as plugin, CONCAT( IFNULL(f.nome,'') ,' ', IFNULL(g.nome,'')) as modulo_plugin ";
                $table .= " , a.rgpd as rgpd, a.idmodulo as idmodulo, a.idplugin as idplugin, a.id_tipo_op as id_tipo_op , a.ref as ref";
                $table .= " , CONCAT( IFNULL(a.desc,'') ,' ------ ', IFNULL(a.filelist,''))  as `desc` ";
                $table .= " FROM ".$dbprefix."virtualdesk_eventlog as a ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_eventlog_category AS b ON a.category=b.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_eventlog_accesslevel AS c ON a.accesslevel=c.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_users AS d ON a.iduser=d.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_eventlog_tipo_op AS e ON a.id_tipo_op=e.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_perm_modulo AS f ON a.idmodulo=f.id ";
                $table .= " LEFT JOIN ".$dbprefix."virtualdesk_config_plugin AS g ON a.idplugin=g.id ";
                $table .= " WHERE a.rgpd=true ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                $columns = array(
                    array( 'db' => 'createdFull',    'dt' => 0 ),
                    array( 'db' => 'login',          'dt' => 1 ),
                    array( 'db' => 'nome',           'dt' => 2 ),
                    array( 'db' => 'accesslevel',    'dt' => 3 ),
                    array( 'db' => 'category',       'dt' => 4 ),
                    array( 'db' => 'tipo_op',        'dt' => 5 ),
                    array( 'db' => 'title',          'dt' => 6 ),
                    array( 'db' => 'ref',            'dt' => 7 ),
                    array( 'db' => 'modulo_plugin',  'dt' => 8 ),
                    array( 'db' => 'rgpd',           'dt' => 9 ),
                    array( 'db' => 'ip',             'dt' => 10 ),

                    array( 'db' => 'id',             'dt' => 11 ),
                    array( 'db' => 'idcategory',     'dt' => 12 ),
                    array( 'db' => 'idaccesslevel',  'dt' => 13 ),
                    array( 'db' => 'idplugin',       'dt' => 14 ),
                    array( 'db' => 'idmodulo',       'dt' => 15 ),
                    array( 'db' => 'id_tipo_op',     'dt' => 16 ),
                    array( 'db' => 'desc',           'dt' => 17 ,'formatter'=>'HTMLSPECIALCHARS' ),
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEventLogsConfigAccessLevel ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('logadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                //$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');
                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT(\'".$dummyHRef."\', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";

                $table  = " ( SELECT a.id,a.name,a.modified";
                $table .= " FROM ".$dbprefix."virtualdesk_eventlog_accesslevel as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'id',             'dt' => 0 ),
                    array( 'db' => 'name',           'dt' => 1 ),
                    array( 'db' => 'modified',       'dt' => 2 ),
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }


    public static function getEventLogsConfigCats ($vbForDataTables=false, $setLimit=-1)
    {
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkLayoutAccess('logadmin', 'listdefault');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        try
        {
            // *** DATATABLES ***
            // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
            if($vbForDataTables===true) {

                $conf = JFactory::getConfig();

                $host     = $conf->get('host');
                $user     = $conf->get('user');
                $password = $conf->get('password');
                $database = $conf->get('db');
                $dbprefix = $conf->get('dbprefix');

                // Gerar Link para o detalhe...
                //$dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloview&permadmin_modulo_id=');
                //$table  = " ( SELECT a.id as id, a.nome as nome , CONCAT(\'".$dummyHRef."\', CAST(a.id as CHAR(10))) as dummy, a.tagchave as tagchave ";

                $table  = " ( SELECT a.id,a.name,a.modified";
                $table .= " FROM ".$dbprefix."virtualdesk_eventlog_category as a ";
                $table .= "  ) temp ";

                $primaryKey = 'id';

                // array( 'db' => 'dummy',      'dt' => 2 ,'formatter'=>'URL_ENCRYPT'),
                $columns = array(
                    array( 'db' => 'id',             'dt' => 0 ),
                    array( 'db' => 'name',           'dt' => 1 ),
                    array( 'db' => 'modified',       'dt' => 2 ),
                );

                $sql_details = array(
                    'user' => $user,
                    'pass' => $password,
                    'db'   => $database,
                    'host' => $host
                );

                $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                return $data;
            }

            $dataReturn = array();
            return($dataReturn);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS as Categorias */
    public static function getCategoryAllOptions ()
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // TODO

        try
        {

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, name'))
                ->from("#__virtualdesk_eventlog_category")
            );

            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->name
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS as Access Levels */
    public static function getAccessLevelAllOptions ()
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // TODO

        try
        {

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, name'))
                ->from("#__virtualdesk_eventlog_accesslevel")
            );

            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->name
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS as Tipos de Operações */
    public static function getTipoOPAllOptions ()
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // TODO

        try
        {

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, name'))
                ->from("#__virtualdesk_eventlog_tipo_op")
            );

            $dataReturn = $db->loadObjectList();
            $response = array();
            foreach ($dataReturn as $row) {
                $response[] = array(
                    'id' => $row->id,
                    'name' => $row->name
                );

            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS os Módulos */
    public static function getModuloAllOptions ($retTagAsId=false)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // TODO

        try
        {


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(' distinct f.id, f.nome, f.tagchave')
                ->from("#__virtualdesk_eventlog as a")
                ->leftJoin("#__virtualdesk_perm_modulo AS f ON a.idmodulo=f.id ")
                ->where(" a.idmodulo is not null AND a.idmodulo>0 ")
            );

            $dataReturn = $db->loadObjectList();
            $response = array();

            // Retorna Id ou TagChave ?
            if($retTagAsId===true) {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->tagchave,
                        'name' => $row->nome
                    );
                }
            } else
            {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->nome
                    );
                }
            }

            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS os Plugins */
    public static function getPluginAllOptions ($retTagAsId=false)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // TODO

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(' distinct f.id, f.nome,f.tagchave')
                ->from("#__virtualdesk_eventlog as a")
                ->leftJoin("#__virtualdesk_config_plugin AS f ON a.idplugin=f.id ")
                ->where(" a.idplugin is not null AND a.idplugin>0 ")
            );


            $dataReturn = $db->loadObjectList();
            $response = array();
            // Retorna Id ou TagChave ?
            if($retTagAsId===true) {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->tagchave,
                        'name' => $row->nome
                    );
                }
            } else
            {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->nome
                    );
                }
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS os Módulos - Logs RGPD*/
    public static function getModuloRGPDAllOptions ($retTagAsId=false)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // TODO

        try
        {


            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(' distinct f.id, f.nome, f.tagchave')
                ->from("#__virtualdesk_eventlog_rgpd as a")
                ->leftJoin("#__virtualdesk_perm_modulo AS f ON a.idmodulo=f.id ")
                ->where(" a.idmodulo is not null AND a.idmodulo>0 ")
            );

            $dataReturn = $db->loadObjectList();
            $response = array();

            // Retorna Id ou TagChave ?
            if($retTagAsId===true) {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->tagchave,
                        'name' => $row->nome
                    );
                }
            } else
            {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->nome
                    );
                }
            }

            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS os Plugins  - Logs RGPD */
    public static function getPluginRGPDAllOptions ($retTagAsId=false)
    {
        /*
        * Check PERMISSÕES
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();

        // TODO

        try
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(' distinct f.id, f.nome,f.tagchave')
                ->from("#__virtualdesk_eventlog_rgpd as a")
                ->leftJoin("#__virtualdesk_config_plugin AS f ON a.idplugin=f.id ")
                ->where(" a.idplugin is not null AND a.idplugin>0 ")
            );


            $dataReturn = $db->loadObjectList();
            $response = array();
            // Retorna Id ou TagChave ?
            if($retTagAsId===true) {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->tagchave,
                        'name' => $row->nome
                    );
                }
            } else
            {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->nome
                    );
                }
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

    /* Carrega lista com os TODOS os Módulos - Logs RGPD*/
    public static function getModuloRGPDByTagsOptions ($retTagAsId, $TagsString)
    {
        /*
        * Check PERMISSÕES
        */
        #$objCheckPerm = new VirtualDeskSitePermissionsHelper();

        try
        {
            if(empty($TagsString)) return(false);

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(' distinct a.id, a.nome, a.tagchave')
                ->from("#__virtualdesk_perm_modulo as a")
                ->where(" a.tagchave in (".$TagsString.") ")
            );
            $dataReturn = $db->loadObjectList();
            $response = array();
            // Retorna Id ou TagChave ?
            if($retTagAsId===true) {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->tagchave,
                        'name' => $row->nome
                    );
                }
            } else
            {
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->nome
                    );
                }
            }
            return($response);
        }
        catch (RuntimeException $e)
        {
            return false;
        }
    }

}
