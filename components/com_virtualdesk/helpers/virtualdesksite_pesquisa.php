<?php
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
//JLoader::register('VirtualDeskTablePesquisa', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/pesquisa.php');


    class VirtualDeskSitePesquisaHelper
    {
        const tagchaveModulo = 'pesquisa';


        /* Carrega lista com dados de todos os eventos (acesso ao USER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getPesquisaList4User ($vbForDataTables=false, $setLimit=-1)
        {
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('pesquisa', 'list4users');
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                /*if( empty($lang) or ($lang='pt_PT') ) {
                    $Name = 'nome_PT';
                }
                else {
                    $Name = 'nome_EN';
                } */

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {
                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    $table  = " ( SELECT a.id as id, a.id as pesquisa_id, a.nome as nome, a.descricao as descricao, a.breadcrumb as breadcrumb, a.chavespesq as chavespesq ";
                    $table .= "  , CONCAT(a.link,'&Itemid=',a.menuid) as link2url ";
                    $table .= " FROM ".$dbprefix."virtualdesk_pesquisa as a ";
                    $table .= " WHERE is4user=1 ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'nome',          'dt' => 0 ),
                        array( 'db' => 'descricao',     'dt' => 1 ),
                        array( 'db' => 'link2url',      'dt' => 2 , 'formatter'=>'ROUTE_URL_ENCRYPT'),
                        array( 'db' => 'chavespesq',    'dt' => 3 ),
                        array( 'db' => 'breadcrumb',    'dt' => 4 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }


        /* Carrega lista  (acesso ao MANAGER). $vbForDataTables: se for true, vai carrega o JSON para os datatables */
        public static function getPesquisaList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('pesquisa', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                /*if( empty($lang) or ($lang='pt_PT') ) {
                    $Name = 'nome_PT';
                }
                else {
                    $Name = 'nome_EN';
                } */

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if($vbForDataTables===true) {
                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    $table  = " ( SELECT a.id as id, a.id as pesquisa_id, a.nome as nome, a.descricao as descricao, a.breadcrumb as breadcrumb, a.chavespesq as chavespesq ";
                    $table .= "  , CONCAT(a.link,'&Itemid=',a.menuid) as link2url ";
                    $table .= " FROM ".$dbprefix."virtualdesk_pesquisa as a ";
                    $table .= " WHERE is4manager=1 ";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array( 'db' => 'nome',          'dt' => 0 ),
                        array( 'db' => 'descricao',     'dt' => 1 ),
                        array( 'db' => 'link2url',      'dt' => 2 , 'formatter'=>'ROUTE_URL_ENCRYPT'),
                        array( 'db' => 'chavespesq',    'dt' => 3 ),
                        array( 'db' => 'breadcrumb',    'dt' => 4 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }



        /* Limpa os dados na sessão "users states" do joomla */
        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();

        }
        
    }
?>