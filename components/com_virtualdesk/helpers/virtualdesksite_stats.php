<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_messages
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * VirtualDesk helper class.
 *
 * @since  1.6
 */
class VirtualDeskSiteStatsHelper
{


    public static function getNumberOfActiveUsersSiteView()
    {
        // Number of Active Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__virtualdesk_users")
            ->where($db->quoteName('blocked') . '=0 AND ' . $db->quoteName('activated') . '=1')
        );

        $db->execute();

        $stats_initnumberactiveusers = (int)JComponentHelper::getParams('com_virtualdesk')->get('stats_initnumberactiveusers');

        return($db->getNumRows() + $stats_initnumberactiveusers);
    }


    public static function getNumberOfContactUsRequestSiteView()
    {
        // Number of Active Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__virtualdesk_contactus")
        );

        $db->execute();

        return($db->getNumRows() );
    }


    public static function getNumberOfUserAccessSiteView()
    {
        // Number of Active Users
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id'))
            ->from("#__virtualdesk_eventlog")
            ->where($db->quoteName('category') . '=9')
        );

        $db->execute();

        return($db->getNumRows() );
    }


}
