<?php
    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePluginsHelper', JPATH_SITE . '/plugins/system/virtualdesk/helpers/virtualdesksite_plugins.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('SSP', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_ssp_class.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskTableLeituraAgua', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/leituraAgua.php');
    JLoader::register('VirtualDeskTableLeituraAguaEstadoHistorico', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/tables/leituraAgua_estado_historico.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

    class VirtualDeskSiteAguaBackofficeHelper
    {
        /*APP*/
        const tagchaveModulo = 'agua';

        public static function getEstadoAllOptions ($lang, $displayPermError=true)
        {
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();

            $vbCheckModule = $objCheckPerm->loadModuleEnabledByTag(self::tagchaveModulo);
            if((int)$vbCheckModule==0)  return false;

            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess(self::tagchaveModulo);                  // verifica permissão de read
            if($vbHasAccess===false ) {
                if($displayPermError!=false)  JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('id_estado as id','estado as name') )
                    ->from("#__virtualdesk_Agua_Leituras_Estado")
                );
                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );

                }
                return($response);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getEstadoCSS ($idestado)
        {
            $defCss = 'label-default';
            switch ($idestado)
            {   case '1':
                $defCss = 'label-analise';
                break;
                case '2':
                    $defCss = 'label-valido';
                    break;
                case '3':
                    $defCss = 'label-rejeitado';
                    break;
            }
            return ($defCss);
        }

        public static function getLeiturasList4Manager ($vbForDataTables=false, $setLimit=-1)
        {
            // Check PERMISSÕES
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agua', 'list4managers');
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $vbHasReadAll = $objCheckPerm->checkReadAllLayoutAccess('agua','list4managers');
            if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasReadAll == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            try
            {
                if($vbForDataTables===true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef =  JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=view4manager&leitura=');

                    $table  = " ( SELECT a.id_leitura as id_leitura, CONCAT('".$dummyHRef."', CAST(a.id_leitura as CHAR(10))) as dummy, a.nif as nif, a.num_contador as num_contador, a.leitura as leitura, a.estado as idestado, b.estado as estado, a.data_leitura as data_leitura";
                    $table .= " FROM ".$dbprefix."virtualdesk_Agua_Leituras as a ";
                    $table .= " LEFT JOIN ".$dbprefix."virtualdesk_Agua_Leituras_Estado AS b ON b.id_estado = a.estado";
                    $table .= "  ) temp ";

                    $primaryKey = 'id_leitura';

                    $columns = array(
                        array( 'db' => 'nif',           'dt' => 0 ),
                        array( 'db' => 'num_contador',  'dt' => 1 ),
                        array( 'db' => 'leitura',       'dt' => 2 ),
                        array( 'db' => 'data_leitura',  'dt' => 3 ),
                        array( 'db' => 'estado',        'dt' => 4 ),
                        array( 'db' => 'dummy',         'dt' => 5 ,'formatter'=>'URL_ENCRYPT'),
                        array( 'db' => 'id_leitura',    'dt' => 6 ),
                        array( 'db' => 'idestado',      'dt' => 7 )
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db'   => $database,
                        'host' => $host
                    );

                    $data = SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns );

                    return $data;
                }

                // *** Joomla QUERY  ***
                $setLimitSQL = ''; // Vai permitir carregar apenas alguns registos para limitar o desempenho quando, por exemplo, temos as DataTables
                if($setLimit>0) $setLimitSQL = ' LIMIT ' . $setLimit;

                $db = JFactory::getDBO();



                return('');
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getLeiturasDetail4Manager($id){
            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess   = $objCheckPerm->checkDetailReadAccess('agua');                  // verifica permissão de read
            $vbHasAccess2  = $objCheckPerm->checkLayoutAccess('agua', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbHasReadAll  = $objCheckPerm->checkReadAllAccess('agua'); // Verifica permissões READALL no módulo
            $vbHasReadAll2 = $objCheckPerm->checkReadAllLayoutAccess('agua','view4managers'); // Verifica permissões READALL no layout
            if($vbHasAccess===false || $vbHasAccess2===false || $vbHasReadAll == false || $vbHasReadAll2 == false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($id) )  return false;

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int)$UserJoomlaID<=0) return false;

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('a.id_leitura as id', 'a.ref_leitura as codigo', 'a.nif as nif', 'a.num_contador as num_contador', 'a.leitura as leitura', 'a.data_leitura as data_leitura', 'b.estado as estado', 'a.estado as id_estado'))
                    ->join('LEFT', '#__virtualdesk_Agua_Leituras_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Agua_Leituras as a")
                    ->where( $db->quoteName('a.id_leitura') . '=' . $db->escape($id)  )
                );
                $dataReturn = $db->loadObject();
                return($dataReturn);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function getLeituraEstadoAtualObjectById ($lang, $leitura)
        {
            if((int) $leitura<=0) return false;

            /*
            * Check PERMISSÕES
            */
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agua');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            try
            {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select( array('b.id_estado as id_estado','b.estado as estado_PT','b.estado as estado_EN') )
                    ->join('LEFT', '#__virtualdesk_Agua_Leituras_Estado AS b ON b.id_estado = a.estado')
                    ->from("#__virtualdesk_Agua_Leituras as a")
                    ->where(" a.id = ". $db->escape($leitura))
                );
                $dataReturn = $db->loadObject();

                if(empty($dataReturn)) return false;

                $obj= new stdClass();
                $obj->id_estado =  $dataReturn->id_estado;
                if( empty($lang) or ($lang='pt_PT') ) {
                    $obj->name = $dataReturn->estado_PT;
                }
                else {
                    $obj->name  = $dataReturn->estado_EN;
                }

                $obj->cssClass = self::getEstadoCSS($dataReturn->id_estado);

                return($obj);
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function saveAlterar2NewEstado4ManagerByAjax($getInputLeitura_id, $NewEstadoId, $NewEstadoDesc)
        {
            $config = JFactory::getConfig();
            // Idioma
            $jinput = JFactory::getApplication()->input;
            $language_tag = $jinput->get('lang', 'pt-PT', 'string');

            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agua');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agua', 'view4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agua', 'alterarestado4managers'); // Ver se tem acesso ao botão
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $checkAlterarEstado4Managers ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if( empty($NewEstadoId) ) return false;
            if((int) $getInputLeitura_id <=0 ) return false;

            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            if((int) $UserJoomlaID <=0 ) return false;
            $objUserVD = VirtualDeskSiteUserHelper::getUserTableFromJoomlaID ($UserJoomlaID);
            if ($objUserVD === false) return false;
            $UserVDId = $objUserVD->id;
            if ((int) $UserVDId <=0) return false;

            $data = array();
            $data['estado'] = $NewEstadoId;

            // Carrega dados atuais na base de dados
            $db    = JFactory::getDbo();

            $Table = new VirtualDeskTableLeituraAgua($db);
            $Table->load(array('id_leitura'=>$getInputLeitura_id));


            // Só alterar se o estado for diferente
            if((int)$Table->estado == (int)$NewEstadoId )  return true;

            if(!empty($data)) {

            }

            $db->transactionStart();

            try {
                // Store the data.
                if (!$Table->save($data)) {
                    $db->transactionRollback();
                    return false;
                }

                // Envia para o histório
                $TableHist  = new VirtualDeskTableLeituraAguaEstadoHistorico($db);
                $dataHist = array();
                $dataHist['id_estado']  = $NewEstadoId;
                $dataHist['id_leitura']  = $getInputLeitura_id;
                $dataHist['descricao']  = $NewEstadoDesc;
                $dataHist['createdby']  = $UserJoomlaID;
                $dataHist['modifiedby'] = $UserJoomlaID;

                // Store the data.
                if (!$TableHist->save($dataHist)) {
                    $db->transactionRollback();
                    return false;
                }


                $db->transactionCommit();

                $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

                // LOG GERAL
                if((int)$setLogGeralEnabled===1) {
                    $vdlog = new VirtualDeskLogHelper();
                    $eventdata = array();
                    $eventdata['iduser'] = $UserVDId;
                    $eventdata['idjos'] = $UserJoomlaID;
                    $eventdata['title'] = JText::_('COM_VIRTUALDESK_LEITURA_AGUA_EVENTLOG_STATE_ALTERADO');
                    $eventdata['desc'] = JText::_('COM_VIRTUALDESK_LEITURA_AGUA_EVENTLOG_STATE_ALTERADO') . 'id_estado=' . $dataHist['id_estado'] . " - " . 'id_leitura=' . $dataHist['id_leitura'];
                    $eventdata['filelist'] = "";
                    $eventdata['category'] = JText::_('COM_VIRTUALDESK_ALERTA_EVENTLOG_STATE_CAT');
                    $eventdata['sessionuserid'] = JFactory::getUser()->id;
                    $vdlog->insertEventLog($eventdata);
                }

                return true;
            }
            catch (Exception $e) {
                $db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }
        }

        public static function getNif($getInputLeitura_id){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('nif')
                ->from("#__virtualdesk_Agua_Leituras")
                ->where($db->quoteName('id_leitura') . "='" . $db->escape($getInputLeitura_id) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function getDataUser($nifUser){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('email, nome'))
                ->from("#__virtualdesk_Agua_Consumidores")
                ->where($db->quoteName('nif') . "='" . $db->escape($nifUser) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getStateName($NewEstadoId){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('estado')
                ->from("#__virtualdesk_Agua_Leituras_Estado")
                ->where($db->quoteName('id_estado') . "='" . $db->escape($NewEstadoId) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function sendMailAlteracaoEstado($getInputLeitura_id, $NewEstadoId){
            $nifUser = self::getNif($getInputLeitura_id);
            $DataUser = self::getDataUser($nifUser);
            $stateName = self::getStateName($NewEstadoId);
            foreach($DataUser as $rowWSL) :
                $nameUser = $rowWSL['nome'];
                $EmailUser = $rowWSL['email'];
            endforeach;

            $obParam      = new VirtualDeskSiteParamsHelper();
            $mailAdmin = $obParam->getParamsByTag('mailAdminLeituraAgua');
            $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');
            $emailCopyrightEmail = $obParam->getParamsByTag('emailCopyrightEmail');
            $contactoCopyrightEmail = $obParam->getParamsByTag('contactoCopyrightEmail');
            $dominioMunicipio = $obParam->getParamsByTag('dominioMunicipio');
            $linkCopyrightEmail = $obParam->getParamsByTag('linkCopyrightEmail');

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_LeituraAgua_NovoEstado_Email.html');


            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $mailAdmin;
            $data['sitename'] = $config->get('sitename');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_TITULO_CHANGESTATE');
            $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_TITULO_CHANGESTATE');
            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_INTRO_CHANGESTATE',$nameUser);
            $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_AGUA_LEITURAS_EMAIL_CORPO_CHANGESTATE', $stateName);
            $BODY_COPYNAME       = JText::sprintf($copyrightAPP);
            $BODY_COPYMAIL       = JText::sprintf($emailCopyrightEmail);
            $BODY_COPYTELE       = JText::sprintf($contactoCopyrightEmail);
            $BODY_COPYDOM       = JText::sprintf($dominioMunicipio);
            $BODY_COPYLINK       = JText::sprintf($linkCopyrightEmail);


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
            $body      = str_replace("%BODY_COPYNAME", $BODY_COPYNAME, $body);
            $body      = str_replace("%BODY_COPYMAIL", $BODY_COPYMAIL, $body);
            $body      = str_replace("%BODY_COPYTELE", $BODY_COPYTELE, $body);
            $body      = str_replace("%BODY_COPYDOM", $BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYLINK", $BODY_COPYLINK, $body);

            // Send the password reset request email.
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo( $data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($EmailUser);
            $newActivationEmail->setSubject($data['sitename']);
            $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/Agua_BannerEmail.png', "banner", "Agua_BannerEmail.png");
            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;
        }

        public static function getPedidosList4User($vbForDataTables=false, $setLimit=-1){
            // Check Permissões
            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkLayoutAccess('agua', 'list4users');
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $modulo_id = VirtualDeskSiteFormmainHelper::getModuleId('agua');

            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            if( empty($UserJoomlaID) )  return false;
            if( (int)$UserJoomlaID<=0 )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

            if((int)$UserSessionNIF<=0) return false;

            try {

                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $EstadoName = 'estado_PT';
                }
                else {
                    $EstadoName = 'estado_EN';
                }

                // *** DATATABLES ***
                // Se for um pedido para as datatables faz um processamento diferente e retornar o formato JSON
                if ($vbForDataTables === true) {

                    $conf = JFactory::getConfig();

                    $host = $conf->get('host');
                    $user = $conf->get('user');
                    $password = $conf->get('password');
                    $database = $conf->get('db');
                    $dbprefix = $conf->get('dbprefix');

                    // Gerar Link para o detalhe...
                    $dummyHRef = JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=view4user&pedido_id=');

                    $table = " ( SELECT a.id as id, a.id as pedido_id, c.nome as formulario, CONCAT('" . $dummyHRef . "', CAST(a.id as CHAR(10))) as dummy, a.ref";
                    $table .= " , IFNULL(g.".$EstadoName.", ' ') as estado, a.estado_id as idestado";
                    $table .= " , a.create_date as created, DATE_FORMAT(a.modify_date, '%Y-%m-%d') as modified";
                    $table .= " FROM " . $dbprefix . "virtualdesk_form_main_rel_agua as a ";
                    $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_form_main as c on c.id=a.form_id";
                    $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_form_main_estado as g on g.id=a.estado_id";
                    $table .= " LEFT JOIN " . $dbprefix . "virtualdesk_perm_modulo as d on d.id=c.modulo";
                    $table .= " WHERE (a.user_id=$UserJoomlaID AND c.enabled=1 AND d.enabled=1 AND c.modulo=$modulo_id)";
                    $table .= "  ) temp ";

                    $primaryKey = 'id';

                    $columns = array(
                        array('db' => 'created', 'dt' => 0),
                        array('db' => 'ref', 'dt' => 1),
                        array('db' => 'formulario', 'dt' => 2),
                        array('db' => 'estado', 'dt' => 3),
                        array('db' => 'dummy', 'dt' => 4, 'formatter' => 'URL_ENCRYPT'),
                        array('db' => 'idestado', 'dt' => 5),
                        array('db' => 'id', 'dt' => 6, 'formatter' => 'VALUE_ENCRYPT'),
                        array('db' => 'modified', 'dt' => 7)
                    );

                    $sql_details = array(
                        'user' => $user,
                        'pass' => $password,
                        'db' => $database,
                        'host' => $host
                    );

                    $data = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);

                    return $data;
                }
            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function random_code(){
            return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
        }

        public static function CheckReferencia($referencia){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('ref')
                ->from("#__virtualdesk_form_main_rel_agua")
                ->where($db->quoteName('ref') . "='" . $db->escape($referencia) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function createVerificacaoContador4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                }  else {
                    $QualidadeRep = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumConsumidor'] = $numConsumidor;
                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createAlteracaoLocalContador4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $motivo = $data['fieldMotivoRadio'];
                $outroMotivo = $data['fieldOutroMotivo'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($motivo == 1){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_OBRASIMOVEL');
                } else if($motivo == 2){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_MELHORARACESSO');
                } else if($motivo == 3){
                    $Motivo = $outroMotivo;
                } else {
                    $Motivo = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumConsumidor'] = $numConsumidor;
                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldMotivoRadio'] = $Motivo;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createSubstituicaoContador4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];
                $motivo = $data['fieldMotivoRadio'];
                $outroMotivo = $data['fieldOutroMotivo'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_SUBSTCONTADOR');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_SUBSTTORNEIRA');
                } else {
                    $TipoPedido = '';
                }

                if($motivo == 1){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_CONTADORDANIFICADO');
                } else if($motivo == 2){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_CALIBREINSUFICIENTE');
                } else if($motivo == 3){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_TORNEIRADANIFICADA');
                }  else if($motivo == 4){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_CONTADORPARADO');
                }  else if($motivo == 5){
                    $Motivo = $outroMotivo;
                } else {
                    $Motivo = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumConsumidor'] = $numConsumidor;
                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['fieldMotivoRadio'] = $Motivo;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createAlteracaoTitularContador4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $motivo = $data['fieldMotivoRadio'];
                $outroMotivo = $data['fieldOutroMotivo'];
                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $nifNovoTitular = $data['fieldNIFNovoTitular'];
                $telefoneNovoTitular = $data['fieldTelefoneNovoTitular'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($nifNovoTitular == ''){
                    $nifNovoTitular = 'Null';
                }

                if($telefoneNovoTitular == ''){
                    $telefoneNovoTitular = 'Null';
                }


                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($motivo == 1){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_ALTTITULAR_MORTE');
                } else if($motivo == 2){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_ALTTITULAR_COMPRAIMOVEL');
                } else if($motivo == 3){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_ALTTITULAR_HERANCA');
                } else if($motivo == 4){
                    $Motivo = JText::_('COM_VIRTUALDESK_AGUA_ALTTITULAR_DIVORCIO');
                } else if($motivo == 5){
                    $Motivo = $outroMotivo;
                } else {
                    $Motivo = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNIFNovoTitular'] = $nifNovoTitular;
                $data['fieldTelefoneNovoTitular'] = $telefoneNovoTitular;
                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldMotivoRadio'] = $Motivo;

                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createCancelamentoContrato4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $localContador = $data['fieldLocalContador'];$nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($localContador == 1){
                    $LocalContador = JText::_('COM_VIRTUALDESK_AGUA_CONTADOR_INTERIOR');
                } else if($localContador == 2){
                    $LocalContador = JText::_('COM_VIRTUALDESK_AGUA_CONTADOR_EXTERIOR');
                } else {
                    $LocalContador = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldLocalContador'] = $LocalContador;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createContratoPrestacaoServicos4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];
                $outroTipoPedido = $data['fieldOutroTipoPedido'];
                $tipoConsumidor = $data['fieldTipoConsumidor'];
                $outroTipoConsumidor = $data['fieldOutroTipoConsumidor'];
                $debitoDireto = $data['fieldDebitoDireto'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $IBAN = $data['fieldIBAN'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($IBAN == ''){
                    $IBAN = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_ABSTAGUA_SANEAMENTO');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_ABSTAGUA');
                } else if($tipoPedido == 3){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_SANEAMENTO');
                } else if($tipoPedido == 4){
                    $TipoPedido = $outroTipoPedido;
                } else {
                    $TipoPedido = '';
                }

                if($tipoConsumidor == 1){
                    $TipoConsumidor = JText::_('COM_VIRTUALDESK_AGUA_DOMESTICO');
                } else if($tipoConsumidor == 2){
                    $TipoConsumidor = JText::_('COM_VIRTUALDESK_AGUA_NAODOMESTICO');
                } else if($tipoConsumidor == 3){
                    $TipoConsumidor = JText::_('COM_VIRTUALDESK_AGUA_RESPERMANENTE');
                } else if($tipoConsumidor == 4){
                    $TipoConsumidor = JText::_('COM_VIRTUALDESK_AGUA_RESEVENTUAL');
                } else if($tipoConsumidor == 5){
                    $TipoConsumidor = JText::_('COM_VIRTUALDESK_AGUA_ATIVIDADEECONOMICA');
                } else if($tipoConsumidor == 6){
                    $TipoConsumidor = JText::_('COM_VIRTUALDESK_AGUA_OBRA');
                } else if($tipoConsumidor == 7){
                    $TipoConsumidor = $outroTipoConsumidor;
                } else {
                    $TipoConsumidor = '';
                }

                if($debitoDireto == 1){
                    $DebitoDireto = JText::_('COM_VIRTUALDESK_AGUA_SIM');
                } else if($debitoDireto == 2){
                    $DebitoDireto = JText::_('COM_VIRTUALDESK_AGUA_NAO');
                } else {
                    $DebitoDireto = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldIBAN'] = $IBAN;
                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['fieldTipoConsumidor'] = $TipoConsumidor;
                $data['fieldDebitoDireto'] = $DebitoDireto;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createRestabelecimentoAgua4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_FORNAGUA');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_CONTADOR');
                } else {
                    $TipoPedido = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumConsumidor'] = $numConsumidor;
                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createFaturaEletronica4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_FATURAELETRONICA_ADESAO');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_FATURAELETRONICA_ALTERACAO');
                } else if($tipoPedido == 3){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_FATURAELETRONICA_CANCELAMENTO');
                } else {
                    $TipoPedido = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumConsumidor'] = $numConsumidor;

                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createDebitoDireto4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $IBAN = $data['fieldIBAN'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($IBAN == ''){
                    $IBAN = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_DEBITODIRETO_ADESAO');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_DEBITODIRETO_ALTERACAO');
                } else if($tipoPedido == 3){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_DEBITODIRETO_CANCELAMENTO');
                } else {
                    $TipoPedido = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldIBAN'] = $IBAN;
                $data['fieldNumConsumidor'] = $numConsumidor;

                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createTarifarioEspecial4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_TARIFARIO_FAMILIAR');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_TARIFARIO_REFORMADO');
                } else {
                    $TipoPedido = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumConsumidor'] = $numConsumidor;

                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createRamalAbastecimentoAgua4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numAlvara = $data['fieldNumAlvara'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numAlvara == ''){
                    $numAlvara = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_RAMALABASTECIMENTO_LIGACAO');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_RAMALABASTECIMENTO_MODIFICACAO');
                } else {
                    $TipoPedido = '';
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumAlvara'] = $numAlvara;

                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function createRamalAguasResiduais4User($UserJoomlaID, $UserVDId, $data)
        {
            /*
            * Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');  // verifica permissão de update
            if($vbHasAccess===false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            if (empty($data) ) return false;

            //$db->transactionStart();

            try {

                // carregar estrutura de campos do form
                $formId = $data['formId'];
                $userID = $data['userID'];

                $tipoSel = $data['fieldDocId'];
                $tipoSelRep = $data['fieldDocIdRep'];
                $qualidadeRep = $data['fieldQualidadeRep'];
                $outraQualRep = $data['fieldOutraQualRep'];
                $tipoPedido = $data['fieldtipopedido'];

                $nif = $data['fieldNif'];
                $numId = $data['fieldNumId'];
                $telemovel = $data['fieldTelemovel'];
                $telefone = $data['fieldTelefone'];
                $fax = $data['fieldFax'];
                $nifRep = $data['fieldNifRep'];
                $numIdRep = $data['fieldNumIdRep'];
                $telemovelRep = $data['fieldTelemovelRep'];
                $telefoneRep = $data['fieldTelefoneRep'];
                $faxRep = $data['fieldFaxRep'];
                $numConsumidor = $data['fieldNumConsumidor'];

                if($nif == ''){
                    $nif = 'Null';
                }

                if($numId == ''){
                    $numId = 'Null';
                }

                if($telemovel == ''){
                    $telemovel = 'Null';
                }

                if($telefone == ''){
                    $telefone = 'Null';
                }

                if($fax == ''){
                    $fax = 'Null';
                }

                if($nifRep == ''){
                    $nifRep = 'Null';
                }

                if($numIdRep == ''){
                    $numIdRep = 'Null';
                }

                if($telemovelRep == ''){
                    $telemovelRep = 'Null';
                }

                if($telefoneRep == ''){
                    $telefoneRep = 'Null';
                }

                if($faxRep == ''){
                    $faxRep = 'Null';
                }

                if($numConsumidor == ''){
                    $numConsumidor = 'Null';
                }

                if($tipoSel == 1){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSel == 2){
                    $docIdentificacao = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacao = '';
                }

                if($tipoSelRep == 1){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_CC');
                } else if($tipoSelRep == 2){
                    $docIdentificacaoRep = JText::_('COM_VIRTUALDESK_AGUA_PASSAPORTE');
                } else {
                    $docIdentificacaoRep = '';
                }

                if($qualidadeRep == 1){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_REPLEGAL');
                } else if($qualidadeRep == 2){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_GESTNEG');
                } else if($qualidadeRep == 3){
                    $QualidadeRep = JText::_('COM_VIRTUALDESK_AGUA_MANDATARIO');
                } else if($qualidadeRep == 4){
                    $QualidadeRep = $outraQualRep;
                } else {
                    $QualidadeRep = '';
                }

                if($tipoPedido == 1){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_RAMALAGUASRESIDUAIS_LIGACAOAGUASRESIDUAIS');
                } else if($tipoPedido == 2){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_RAMALAGUASRESIDUAIS_LIGACAOAGUASPLUVIAIS');
                } else if($tipoPedido == 3){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_RAMALAGUASRESIDUAIS_MODIFICACAOAGUASRESIDUAIS');
                } else if($tipoPedido == 4){
                    $TipoPedido = JText::_('COM_VIRTUALDESK_AGUA_RAMALAGUASRESIDUAIS_MODIFICACAOAGUASPLUVIAIS');
                }

                /*Gerar Referencia UNICA*/
                $refExiste = 1;

                $referencia = '';
                while($refExiste == 1){
                    $referencia = self::random_code();
                    $checkREF   = self::CheckReferencia($referencia);
                    if((int)$checkREF == 0){
                        $refExiste = 0;
                    }
                }
                /*END Gerar Referencia UNICA*/

                $data['fieldNif'] = $nif;
                $data['fieldNumId'] = $numId;
                $data['fieldTelemovel'] = $telemovel;
                $data['fieldTelefone'] = $telefone;
                $data['fieldFax'] = $fax;
                $data['fieldNifRep'] = $nifRep;
                $data['fieldNumIdRep'] = $numIdRep;
                $data['fieldTelemovelRep'] = $telemovelRep;
                $data['fieldTelefoneRep'] = $telefoneRep;
                $data['fieldFaxRep'] = $faxRep;
                $data['fieldNumConsumidor'] = $numConsumidor;

                $data['fieldDocId'] = $docIdentificacao;
                $data['fieldDocIdRep'] = $docIdentificacaoRep;
                $data['fieldQualidadeRep'] = $QualidadeRep;
                $data['fieldtipopedido'] = $TipoPedido;
                $data['referencia'] = $referencia;

                // getFormFieldEstrutura
                $dataValues = VirtualDeskSiteFormmainHelper::getFormFieldsEstrutura($formId);

                // percorrer o $data, comparando com a estrutura e criando um array com fieldtag + idcampo + valor do data
                $data2Create = array();
                foreach($data as $keySt => $valSt){
                    $tmp = array();
                    $tmp['fieldtag'] = $keySt;
                    $tmp['campo_id'] = $dataValues[$keySt]['id'];
                    $tmp['campo_t']  = $dataValues[$keySt]['tipo'];
                    $tmp['val'] = $valSt;
                    $data2Create[] = $tmp;
                }

                /* Save Pedido*/
                VirtualDeskSiteFormmainHelper::insertNew($formId, self::tagchaveModulo, $data2Create, $referencia, $userID);
                /*END Save Pedido*/

            }
            catch (Exception $e){
                //$db->transactionRollback();
                JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
                return false;
            }

        }

        public static function getDadosFormView4UserDetail($Pedido_Id){

            $objCheckPerm = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agua');                  // verifica permissão de read
            if($vbHasAccess===false ) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }

            $modulo_id = VirtualDeskSiteFormmainHelper::getModuleId('agua');

            if( empty($Pedido_Id) )  return false;

            // Temos agora de encontrar o NIF do utilizador, se não tiver NIF não vai devolver resultados
            // Se o Login da APP está por NIF, devolvemos o login do utilizador VD atual, caso contrário pesquisamos no campo fiscalID
            // Current Session User...
            $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
            $UserSessionNIF = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
            if((int)$UserSessionNIF<=0) return false;

            try
            {
                $lang = VirtualDeskHelper::getLanguageTag();
                if( empty($lang) or ($lang='pt_PT') ) {
                    $EstadoName = 'estado_PT';
                }
                else {
                    $EstadoName = 'estado_EN';
                }

                // Verifica se o username existe noutro utilizador no VirtualDesk users...
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);

                $query->select(array('relAS.id', 'dataAS.id', 'dataAS.fields_id', 'fieldRel.form_id', 'dataAS.fields_id as campo',
                    'dataAS.val_text', 'dataAS.val_varchar', 'dataAS.val_date', 'dataAS.val_number', 'fieldM.tag as field_tag'))
                    ->join('LEFT', '#__virtualdesk_form_main_data_agua as dataAS on dataAS.id_rel=relAS.id')
                    ->join('LEFT', '#__virtualdesk_form_fields_rel as fieldRel on (fieldRel.fields_id=dataAS.fields_id and fieldRel.form_id=relAS.form_id)')
                    ->join('LEFT', '#__virtualdesk_form_fields as fieldM on (fieldM.id=fieldRel.fields_id )')
                    ->from("#__virtualdesk_form_main_rel_agua as relAS")
                    ->where( $db->quoteName('relAS.user_id') . '=' . $db->escape($UserJoomlaID) . ' and ' . $db->quoteName('fieldRel.enabled').'= 1 and ' . $db->quoteName('relAS.id') .'=' . $db->escape($Pedido_Id)
                    );

                $db->setQuery($query);

                //echo $db->replacePrefix((string) $query);

                $dataReturn = $db->loadObjectList();
                $response = array();
                foreach ($dataReturn as $row) {
                    $response[$row->field_tag] = $row;

                }
                return ($response);

            }
            catch (RuntimeException $e)
            {
                return false;
            }
        }

        public static function cleanAllTmpUserState() {
            $app = JFactory::getApplication();
            $app->setUserState('com_virtualdesk.addnew4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewaltcontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewalttitularcontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewcanccontrato4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewcontratoprestservicos4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewdebdireto4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewfateletronica4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewramalagua4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewramalaguaresiduais4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewrestabagua4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewsubscontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewtarespecial4user.agua.data', null);
            $app->setUserState('com_virtualdesk.addnewvercontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.edit4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editaltcontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editalttitularcontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editcanccontrato4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editcontratoprestservicos4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editdebdireto4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editfateletronica4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editramalagua4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editramalaguaresiduais4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editrestabagua4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editsubscontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.edittarespecial4user.agua.data', null);
            $app->setUserState('com_virtualdesk.editvercontador4user.agua.data', null);
            $app->setUserState('com_virtualdesk.listaLeituras4manager.agua.data', null);
            $app->setUserState('com_virtualdesk.addnew4manager.agua.data', null);
            $app->setUserState('com_virtualdesk.edit4manager.agua.data', null);
        }

    }

?>