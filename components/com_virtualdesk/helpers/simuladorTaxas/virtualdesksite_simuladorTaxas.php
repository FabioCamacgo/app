<?php
    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');


    class VirtualDeskSiteSimuladorHelper
    {

        public static function getAreas()
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tema'))
                ->from("#__virtualdesk_SimTaxas_Temas")
                ->order('tema ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function excludeArea($area){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tema'))
                ->from("#__virtualdesk_SimTaxas_Temas")
                ->where($db->quoteName('id') . "!='" . $db->escape($area) . "'")
                ->order('tema ASC')
            );
            $data = $db->loadAssocList();
            return ($data);
        }


        public static function getAreaName($area)
        {
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('tema')
                ->from("#__virtualdesk_SimTaxas_Temas")
                ->where($db->quoteName('id') . "='" . $db->escape($area) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


        public static function getPreco($preco){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('preco')
                ->from("#__virtualdesk_SimTaxas_Cemiterios_Precos")
                ->where($db->quoteName('tag') . "='" . $db->escape($preco) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }


    }

?>