<?php

class VirtualDeskSiteOPHelper{

    /*Retorna as Freguesias*/
    function getFreguesias($freguesia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia, concelho_id'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id') . '=' . $db->escape($freguesia))
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    /*Retorna o nome da freguesia*/
    function getNomeFreguesia($freguesia){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('freguesia')
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('id') . '=' . $db->escape($freguesia))
        );
        $data = $db->loadResult();
        return ($data);
    }

    /*Verifica se o nif já existe*/
    public static function seeNifExiste($nif){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('Nif')
            ->from("#__virtualdesk_OP_utilizadores")
            ->where($db->quoteName('Nif') . "='" . $db->escape($nif) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    /*Verifica se o email já existe*/
    public static function seeEmailExiste($email){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('Email')
            ->from("#__virtualdesk_OP_utilizadores")
            ->where($db->quoteName('Email') . "='" . $db->escape($email) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }

    /*Verifica se o email já existe*/
    public static function seeEmailExisteNIF($email){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('Nif')
            ->from("#__virtualdesk_OP_utilizadores")
            ->where($db->quoteName('Email') . "='" . $db->escape($email) . "'")
        );
        $data = $db->loadResult();
        return ($data);
    }

    /*Gera código aleatório para a referência da proposta*/
    public static function random_code(){
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
    }

    /*Verifica de a referencia da ocorrencia existe*/
    public static function CheckReferencia($codi){
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select('Referencia')
            ->from("#__virtualdesk_OP_Propostas")
            ->where($db->quoteName('Referencia') . "='" . $db->escape($codi) . "'")
        );
        $data = $db->loadAssocList();
        return ($data);
    }


    /*guarda dados pessoais*/
    function guardaDadosPessoais($Nome, $Email, $Nif, $Telefone, $Morada, $fregName, $CodPostal){
        $columns = array('Nome', 'Email', 'Nif', 'Telefone', 'Morada', 'Freguesia', 'Codigo_Postal');
        $values = array("'" . $Nome . "'", "'" . $Email . "'", "'" . $Nif . "'", "'" . $Telefone . "'", "'" . $Morada . "'", "'" . $fregName . "'", "'" . $CodPostal . "'");

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->insert($db->quoteName('#__virtualdesk_OP_utilizadores'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));
        $db->setQuery($query);
        $db->execute();
    }


    /*guarda dados proposta*/
    function guardaDadosProposta($nomeProposta, $descricaoProposta, $freguesiaProposta, $Nif, $referencia, $estado){
        $columns = array('Referencia', 'Estado', 'Nome_Proposta', 'Descricao_Proposta', 'Nif', 'Freguesia');
        $values = array("'" . $referencia . "'", "'" . $estado . "'", "'" . $nomeProposta . "'", "'" . $descricaoProposta . "'", "'" . $Nif . "'", "'" . $freguesiaProposta . "'");

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->insert($db->quoteName('#__virtualdesk_OP_Propostas'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));
        $db->setQuery($query);
        $db->execute();
    }


    /*Envia Mail ao utilizador*/
    public static function SendEmailUser($contactoTelefCopyrightEmail, $dominioMunicipio, $moradaMunicipio, $codPostalMunicipio, $LinkCopyright, $emailCopyrightGeral, $Nome, $Email, $Nif, $Telefone, $Morada, $Freguesia, $CodPostal, $nomeProposta, $descricaoProposta, $freguesiaProposta, $nomeMunicipio, $copyrightAPP){

        $config = JFactory::getConfig();

        $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk_OP_Email.html');


        $data['fromname'] = $config->get('fromname');
        $data['mailfrom'] = $Email;
        $data['sitename'] = $config->get('sitename');

        $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_TITULO', $nomeMunicipio);
        $BODY_TITLE2         = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_TITULO', $nomeMunicipio);
        $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_INTRO',$Nome);
        $BODY_MESSAGE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_CORPO');

        $BODY_NIF_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_NIF');
        $BODY_NIF_VALUE       = JText::sprintf($Nif);
        $BODY_TELEFONE_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_TELEFONE');
        $BODY_TELEFONE_VALUE       = JText::sprintf($Telefone);
        $BODY_MORADA_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_MORADA');
        $BODY_MORADA_VALUE       = JText::sprintf($Morada);
        $BODY_FREGUESIA_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_FREGUESIA');
        $BODY_FREGUESIA_VALUE       = JText::sprintf($Freguesia);
        $BODY_CODPOSTAL_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_CODPOSTAL');
        $BODY_CODPOSTAL_VALUE       = JText::sprintf($CodPostal);
        $BODY_NOMEPROPOSTA_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_NOMEPROPOSTA');
        $BODY_NOMEPROPOSTA_VALUE       = JText::sprintf($nomeProposta);
        $BODY_DESCRICAOPROPOSTA_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_DESCRICAOPROPOSTA');
        $BODY_DESCRICAOPROPOSTA_VALUE       = JText::sprintf($descricaoProposta);
        $BODY_FREGUESIAPROPOSTA_TITLE       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_FREGUESIAPROPOSTA');
        $BODY_FREGUESIAPROPOSTA_VALUE       = JText::sprintf($freguesiaProposta);
        $BODY_SECTION_DADOSPESSOAIS       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_SECTION_DADOSPESSOAIS');
        $BODY_SECTION_DADOSPROPOSTA       = JText::sprintf('COM_VIRTUALDESK_OP_EMAIL_SECTION_DADOSPROPOSTA');
        $BODY_DOMINIO_VALUE       = JText::sprintf($dominioMunicipio);
        $BODY_MUNICIPIOMORADA_VALUE       = JText::sprintf($moradaMunicipio);
        $BODY_MUNICIPIOCODPOSTAL_VALUE       = JText::sprintf($codPostalMunicipio);
        $BODY_COPYRIGHTLINK_VALUE       = JText::sprintf($LinkCopyright);
        $BODY_COPYRIGHTEMAIL_VALUE       = JText::sprintf($emailCopyrightGeral);
        $BODY_COPYRIGHTMUNICIPIO_VALUE       = JText::sprintf($nomeMunicipio);
        $BODY_COPYRIGHTAPP_VALUE       = JText::sprintf($copyrightAPP);
        $BODY_COPYRIGHTTELEF_VALUE       = JText::sprintf($contactoTelefCopyrightEmail);


        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
        $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
        $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
        $body      = str_replace("%BODY_MESSAGE",$BODY_MESSAGE, $body);
        $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
        $body      = str_replace("%BODY_NIF_TITLE",$BODY_NIF_TITLE, $body);
        $body      = str_replace("%BODY_NIF_VALUE",$BODY_NIF_VALUE, $body);
        $body      = str_replace("%BODY_TELEFONE_TITLE",$BODY_TELEFONE_TITLE, $body);
        $body      = str_replace("%BODY_TELEFONE_VALUE",$BODY_TELEFONE_VALUE, $body);
        $body      = str_replace("%BODY_CODPOSTAL_TITLE",$BODY_CODPOSTAL_TITLE, $body);
        $body      = str_replace("%BODY_CODPOSTAL_VALUE",$BODY_CODPOSTAL_VALUE, $body);
        $body      = str_replace("%BODY_NOMEPROPOSTA_TITLE",$BODY_NOMEPROPOSTA_TITLE, $body);
        $body      = str_replace("%BODY_NOMEPROPOSTA_VALUE",$BODY_NOMEPROPOSTA_VALUE, $body);
        $body      = str_replace("%BODY_FREGUESIA_TITLE",$BODY_FREGUESIA_TITLE, $body);
        $body      = str_replace("%BODY_FREGUESIA_VALUE",$BODY_FREGUESIA_VALUE, $body);
        $body      = str_replace("%BODY_DESCRICAOPROPOSTA_TITLE",$BODY_DESCRICAOPROPOSTA_TITLE, $body);
        $body      = str_replace("%BODY_DESCRICAOPROPOSTA_VALUE",$BODY_DESCRICAOPROPOSTA_VALUE, $body);
        $body      = str_replace("%BODY_MORADA_TITLE",$BODY_MORADA_TITLE, $body);
        $body      = str_replace("%BODY_MORADA_VALUE",$BODY_MORADA_VALUE, $body);
        $body      = str_replace("%BODY_FREGUESIAPROPOSTA_TITLE",$BODY_FREGUESIAPROPOSTA_TITLE, $body);
        $body      = str_replace("%BODY_FREGUESIAPROPOSTA_VALUE",$BODY_FREGUESIAPROPOSTA_VALUE, $body);
        $body      = str_replace("%BODY_SECTION_DADOSPESSOAIS",$BODY_SECTION_DADOSPESSOAIS, $body);
        $body      = str_replace("%BODY_SECTION_DADOSPROPOSTA",$BODY_SECTION_DADOSPROPOSTA, $body);
        $body      = str_replace("%BODY_DOMINIO_VALUE",$BODY_DOMINIO_VALUE, $body);
        $body      = str_replace("%BODY_MUNICIPIOMORADA_VALUE",$BODY_MUNICIPIOMORADA_VALUE, $body);
        $body      = str_replace("%BODY_MUNICIPIOCODPOSTAL_VALUE",$BODY_MUNICIPIOCODPOSTAL_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHTLINK_VALUE",$BODY_COPYRIGHTLINK_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHTEMAIL_VALUE",$BODY_COPYRIGHTEMAIL_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHTMUNICIPIO_VALUE ",$BODY_COPYRIGHTMUNICIPIO_VALUE , $body);
        $body      = str_replace("%BODY_COPYRIGHTAPP_VALUE",$BODY_COPYRIGHTAPP_VALUE, $body);
        $body      = str_replace("%BODY_COPYRIGHTTELEF_VALUE",$BODY_COPYRIGHTTELEF_VALUE, $body);

        // Send the password reset request email.
        $newActivationEmail = JFactory::getMailer();
        $newActivationEmail->Encoding = 'base64';
        $newActivationEmail->isHtml(true);
        $newActivationEmail->setBody($body);
        $newActivationEmail->addReplyTo($data['mailfrom']);
        $newActivationEmail->setSender( $data['mailfrom']);
        $newActivationEmail->setFrom( $data['fromname']);
        $newActivationEmail->addRecipient($Email);
        $newActivationEmail->setSubject($data['sitename']);
        $newActivationEmail->AddEmbeddedImage(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/includes/OP_BannerEmail.png', "banner", "OP_BannerEmail.png");
        $return = $newActivationEmail->send();

        // Check for an error.
        if ($return !== true)
        {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
            return false;
        }
        return true;

    }
}

?>