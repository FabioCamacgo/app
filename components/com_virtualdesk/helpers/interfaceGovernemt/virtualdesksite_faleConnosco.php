<?php
    /**
     * Created by PhpStorm.
     * User: Camacho
     * Date: 04/09/2018
     * Time: 14:42
     */

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');

    class VirtualDeskSiteGovernmentFaleConnoscoHelper
    {
        public static function getTipoFaleConnosco(){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, tipologia, idValue'))
                ->from("#__virtualdesk_Government_FaleConnosco_tipologia")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getAssunto($idwebsitelist, $onAjaxVD=0){
            if (empty($idwebsitelist)) return false;


            if($onAjaxVD=0) {
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, assunto, ordem, idTipologia'))
                    ->from("#__virtualdesk_Government_FaleConnosco_assunto")
                    ->where($db->quoteName('idTipologia') . '=' . $db->escape($idwebsitelist))
                    ->order('ordem ASC')
                );
                $data = $db->loadAssocList();
            }
            else{
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id as id, assunto as name, ordem as ordem'))
                    ->from("#__virtualdesk_Government_FaleConnosco_assunto")
                    ->where($db->quoteName('idTipologia') . '=' . $db->escape($idwebsitelist))
                    ->order('ordem ASC')
                );
                $data = $db->loadAssocList();
            }

            return ($data);
        }

        public static function getAssuntoName($assunto){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('assunto')
                ->from("#__virtualdesk_Government_FaleConnosco_assunto")
                ->where($db->quoteName('id') . "='" . $db->escape($assunto) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeAssunto($tipologia, $assunto){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id as id, assunto as name, ordem'))
                ->from("#__virtualdesk_Government_FaleConnosco_assunto")
                ->where($db->quoteName('idTipologia') . "='" . $db->escape($tipologia) . "'")
                ->where($db->quoteName('id') . "!='" . $db->escape($assunto) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelho(){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function getConcelhoName($concelho){
            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select('concelho')
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "='" . $db->escape($concelho) . "'")
            );
            $data = $db->loadResult();
            return ($data);
        }

        public static function excludeConcelho($concelho){

            $db = JFactory::getDBO();
            $db->setQuery($db->getQuery(true)
                ->select(array('id, concelho'))
                ->from("#__virtualdesk_digitalGov_concelhos")
                ->where($db->quoteName('id') . "!='" . $db->escape($concelho) . "'")
            );
            $data = $db->loadAssocList();
            return ($data);
        }

        public static function validaRadioInput($inputRadio){
            if(empty($inputRadio)){
                return 1;
            } else {
                return 0;
            }
        }

        public static function validaSelect($item){
            if($item == Null || empty($item) || $item == 'Escolher opção'){
                return 1;
            } else {
                return 0;
            }
        }

        public static function validaText($item){
            if($item == Null){
                return 1;
            } else if (!preg_match('/^[\xc3\x80-\xc3\x96\xc3\x98-\xc3\xb6\xc3\xb8-\xc3\xbfa-zA-Z ]+$/',$item)) {
                return 2;
            } else {
                return 0;
            }
        }

        public static function validaText2($item){
            if($item == Null){
                return 1;
            } else if (strpos($item, '0') !== false) {
                return 2;
            } else if (strpos($item, '1') !== false) {
                return 2;
            } else if (strpos($item, '2') !== false) {
                return 2;
            } else if (strpos($item, '3') !== false) {
                return 2;
            } else if (strpos($item, '4') !== false) {
                return 2;
            } else if (strpos($item, '5') !== false) {
                return 2;
            } else if (strpos($item, '6') !== false) {
                return 2;
            } else if (strpos($item, '7') !== false) {
                return 2;
            } else if (strpos($item, '8') !== false) {
                return 2;
            } else if (strpos($item, '9') !== false) {
                return 2;
            } else {
                return 0;
            }
        }

        public static function validaNotEmpty($item){
            if($item == Null || empty($item)){
                return 1;
            } else {
                return 0;
            }
        }

        public static function validaNumberMandatory($item){
            if($item == Null || empty($item)){
                return 1;
            } else if(!is_numeric($item)){
                return 2;
            } else {
                return 0;
            }
        }

        public static function validaEmail($email, $emailConf){
            if($email == Null){
                return 1;
            } else if($emailConf == Null || $email != $emailConf){
                return 2;
            } else {
                return 0;
            }
        }

        public static function validaDataReuniao($dataReuniao, $invertDataReuniao, $dataAtual){
            if(empty($dataReuniao) || $dataReuniao == NULL){
                return 1;
            } else if($invertDataReuniao < $dataAtual) {
                return 2;
            } else {
                return 0;
            }
        }

        public static function validaNIF($fiscalid){
            $nif=trim($fiscalid);
            $ignoreFirst=true;
            if (!is_numeric($nif) || strlen($nif)!=9) {
                return 1;
            } else {
                $nifSplit=str_split($nif);
                if (
                    in_array($nifSplit[0], array(1, 2, 5, 6, 8, 9))
                    ||
                    $ignoreFirst
                ) {
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);
                    if($checkDigit>=10) $checkDigit=0;
                    if ($checkDigit==$nifSplit[8]) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else {
                    return 1;
                }
            }
        }

        public static function getRandom($n) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';

            for ($i = 0; $i < $n; $i++) {
                $index = rand(0, strlen($characters) - 1);
                $randomString .= $characters[$index];
            }

            return $randomString;
        }

        public static function SendEmailAdmin($nomeAssunto){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Administrador.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $email = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_GREETING      = JText::sprintf('');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $BODY_ASSUNTO = JText::sprintf($nomeAssunto);
            $MailRodape = JText::sprintf($email);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $emailHTML );
            $body      = str_replace("%BODY_ASSUNTO",$BODY_ASSUNTO, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;
        }


        public static function SendEmailElogiosSugestões($propAlertar, $email, $nome){

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Elogios_Sugestoes.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ELOGIOSSUGESTOES_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ELOGIOSSUGESTOES_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ELOGIOSSUGESTOES_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ELOGIOSSUGESTOES_CORPO');
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ELOGIOSSUGESTOES_CORPO2');

            $BODY_ASSUNTO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_ELOGIOSSUGESTOES_ASSUNTO');
            $BODY_ASSUNTO_VALUE       = JText::sprintf($propAlertar);
            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_ASSUNTO_TITLE",$BODY_ASSUNTO_TITLE, $body);
            $body      = str_replace("%BODY_ASSUNTO_VALUE",$BODY_ASSUNTO_VALUE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }

        public static function SendEmailNovoCV($apresentacaoCV, $nome, $email){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_NovoCV.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_NOVOCV_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_NOVOCV_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_NOVOCV_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_NOVOCV_CORPO');
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_NOVOCV_CORPO3');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $BODY_APRESENTACAO = JText::sprintf($apresentacaoCV);
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body);
            $body      = str_replace("%BODY_APRESENTACAO",$BODY_APRESENTACAO, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;
        }

        public static function SendEmailVagaTrabalho($refVagaTrabalho, $apresentacaoVaga, $nome, $email){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_VagaTrabalho.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_CORPO');
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_CORPO2');
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_CORPO3');
            $BODY_FOURTHCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_CORPO4');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $BODY_REF_NAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_REFERENCIA');
            $BODY_REF_VALUE = JText::sprintf($refVagaTrabalho);
            $BODY_APRESENTACAO_NAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_VAGATRABALHO_APRESENTACAO');
            $BODY_APRESENTACAO_VALUE = JText::sprintf($apresentacaoVaga);
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body);
            $body      = str_replace("%BODY_FOURTHCORPO",$BODY_FOURTHCORPO, $body);
            $body      = str_replace("%BODY_REF_NAME",$BODY_REF_NAME, $body);
            $body      = str_replace("%BODY_REF_VALUE",$BODY_REF_VALUE, $body);
            $body      = str_replace("%BODY_APRESENTACAO_NAME",$BODY_APRESENTACAO_NAME, $body);
            $body      = str_replace("%BODY_APRESENTACAO_VALUE",$BODY_APRESENTACAO_VALUE, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }


        public static function SendEmailParceria($parceria, $email, $nome){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Parceria.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARCERIA_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARCERIA_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARCERIA_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARCERIA_CORPO');
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARCERIA_CORPO2');
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARCERIA_CORPO3');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $PARCERIA = JText::sprintf($parceria);
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body);
            $body      = str_replace("%PARCERIA",$PARCERIA, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }

        public static function SendEmailCCP($email){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_CCP.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_CCP_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_CCP_TITULO');


            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);


            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;
        }

        public static function SendEmailCompromisso($email, $nome){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Compromisso.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_COMPROMISSO_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_COMPROMISSO_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_COMPROMISSO_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_COMPROMISSO_CORPO');
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_COMPROMISSO_CORPO2');
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_COMPROMISSO_CORPO3');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;
        }


        public static function SendEmailOrcamento($observacoesOrcamentoField, $email, $nome){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Orcamento.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ORCAMENTO_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ORCAMENTO_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARCERIA_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ORCAMENTO_CORPO');
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ORCAMENTO_CORPO2');
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ORCAMENTO_CORPO3');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $PARCERIA = JText::sprintf($observacoesOrcamentoField);
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body);
            $body      = str_replace("%PARCERIA",$PARCERIA, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }

        public static function SendEmailTicket($assunto, $projeto, $urlProjeto, $espAssunto, $email, $nome){

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Ticket.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            if($assunto == 1){
                $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_TITULO');
                $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_TITULO');
            } else {
                $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ALTERACAOMODULO_TITULO');
                $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ALTERACAOMODULO_TITULO');
            }

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_INTRO',$nome);
            if($assunto == 1) {
                $BODY_CORPO = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_CORPO');
            } else {
                $BODY_CORPO = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_ALTERACAOMODULO_CORPO');
            }
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_CORPO2');
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_CORPO3');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);


            $BODY_NAME_TITLE        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_PROJETO');
            $BODY_NAME_VALUE        = JText::sprintf($projeto);
            $BODY_URL_TITLE        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_URL');
            $BODY_URL_VALUE        = JText::sprintf($urlProjeto);
            $BODY_MENSAGEM_TITLE        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_TICKET_MENSAGEM');
            $BODY_MENSAGEM_VALUE        = JText::sprintf($espAssunto);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body );
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%BODY_NAME_TITLE",$BODY_NAME_TITLE, $body);
            $body      = str_replace("%BODY_NAME_VALUE",$BODY_NAME_VALUE, $body);
            $body      = str_replace("%BODY_URL_TITLE",$BODY_URL_TITLE, $body);
            $body      = str_replace("%BODY_URL_VALUE",$BODY_URL_VALUE, $body);
            $body      = str_replace("%BODY_MENSAGEM_TITLE",$BODY_MENSAGEM_TITLE, $body);
            $body      = str_replace("%BODY_MENSAGEM_VALUE",$BODY_MENSAGEM_VALUE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }


        public static function SendEmailReuniao($assuntoReuniao, $dataReuniao, $horaReuniao, $nomeReuniao, $email, $nome){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Reuniao.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_CORPO');
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_CORPO2');
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_CORPO3');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $BODY_ASSUNTO_NAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_ASSUNTO');
            $BODY_ASSUNTO_VALUE = JText::sprintf($assuntoReuniao);
            $BODY_DATA_NAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_DATA');
            $BODY_DATA_VALUE = JText::sprintf($dataReuniao);
            $BODY_HORA_NAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_HORA');
            $BODY_HORA_VALUE = JText::sprintf($horaReuniao);
            $BODY_NOME_NAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_REUNIAO_NOME');
            $BODY_NOME_VALUE = JText::sprintf($nomeReuniao);
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body);
            $body      = str_replace("%BODY_ASSUNTO_NAME",$BODY_ASSUNTO_NAME, $body);
            $body      = str_replace("%BODY_ASSUNTO_VALUE",$BODY_ASSUNTO_VALUE, $body);
            $body      = str_replace("%BODY_DATA_NAME",$BODY_DATA_NAME, $body);
            $body      = str_replace("%BODY_DATA_VALUE",$BODY_DATA_VALUE, $body);
            $body      = str_replace("%BODY_HORA_NAME",$BODY_HORA_NAME, $body);
            $body      = str_replace("%BODY_HORA_VALUE",$BODY_HORA_VALUE, $body);
            $body      = str_replace("%BODY_NOME_NAME",$BODY_NOME_NAME, $body);
            $body      = str_replace("%BODY_NOME_VALUE",$BODY_NOME_VALUE, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;
        }

        public static function SendEmailOutroAssunto($outroAssunto, $email, $nome){

            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_OutroAssunto.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_OUTROASSUNTO_TITULO');
            $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_OUTROASSUNTO_TITULO');

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_OUTROASSUNTO_INTRO',$nome);
            $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_OUTROASSUNTO_CORPO');
            $BODY_SECONDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_OUTROASSUNTO_CORPO2');

            $BODY_ASSUNTO_TITLE       = JText::sprintf('COM_VIRTUALDESK_ALERTA_EMAIL_OUTROASSUNTO_ASSUNTO');
            $BODY_ASSUNTO_VALUE       = JText::sprintf($outroAssunto);
            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_SECONDCORPO",$BODY_SECONDCORPO, $body );
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%BODY_ASSUNTO_TITLE",$BODY_ASSUNTO_TITLE, $body);
            $body      = str_replace("%BODY_ASSUNTO_VALUE",$BODY_ASSUNTO_VALUE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;

        }

        public static function SendEmailBrainstorming($apresentacao, $email, $nome, $assunto){
            $config = JFactory::getConfig();

            $emailHTML = file_get_contents(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/InterfaceGovernment/Email/virtualdesk_Email_Brainstorming.html');

            $data['fromname'] = $config->get('fromname');
            $data['mailfrom'] = $config->get('mailfrom');
            $data['sitename'] = $config->get('sitename');

            $obParam      = new VirtualDeskSiteParamsHelper();
            $logoMailClienteAlertarPT = $obParam->getParamsByTag('logoMailClienteTIP');
            $emailAdmin = $obParam->getParamsByTag('mailAdminTIP');
            $linkAlertarPT       = $obParam->getParamsByTag('LinkTIP');
            $dominioAlertarPT       = $obParam->getParamsByTag('DomínioTIP');
            $copyrightAlertarPT       = $obParam->getParamsByTag('copyrightTIP');

            if($assunto == 17){
                $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_PROJETO_TITULO');
                $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_PROJETO_TITULO');
                $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_CORPO');
            } else if($assunto == 18){
                $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_BRAINSTORMING_TITULO');
                $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_BRAINSTORMING_TITULO');
                $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_CORPOA');
            } else if($assunto == 19){
                $BODY_TITLE         = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_CONFERENCIAS_TITULO');
                $BODY_TITLE2        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_CONFERENCIAS_TITULO');
                $BODY_CORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_CORPOB');
            }

            $BODY_GREETING      = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_INTRO',$nome);
            $BODY_THIRDCORPO        = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_EMAIL_PARC_CORPO3');

            $WEBNAME = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_TITLE');
            $RESOLVE = JText::sprintf('COM_VIRTUALDESK_ALERTARPT_RESOLVE');
            $BODY_APRESENTACAO = JText::sprintf($apresentacao);
            $MailRodape = JText::sprintf($emailAdmin);
            $BODY_COPYLINK = JText::sprintf($linkAlertarPT);
            $BODY_COPYDOM = JText::sprintf($dominioAlertarPT);
            $BODY_COPYNAME = JText::sprintf($copyrightAlertarPT);

            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $emailHTML );
            $body      = str_replace("%BODY_TITULO",$BODY_TITLE2, $body );
            $body      = str_replace("%BODY_GREETING",$BODY_GREETING, $body );
            $body      = str_replace("%BODY_CORPO",$BODY_CORPO, $body );
            $body      = str_replace("%BODY_THIRDCORPO",$BODY_THIRDCORPO, $body);
            $body      = str_replace("%BODY_APRESENTACAO",$BODY_APRESENTACAO, $body);
            $body      = str_replace("%BODY_TITLE",$BODY_TITLE, $body);
            $body      = str_replace("%WEBNAME",$WEBNAME, $body);
            $body      = str_replace("%RESOLVE",$RESOLVE, $body);
            $body      = str_replace("%RODAPEMAIL",$MailRodape, $body);
            $body      = str_replace("%BODY_COPYLINK",$BODY_COPYLINK, $body);
            $body      = str_replace("%BODY_COPYDOM",$BODY_COPYDOM, $body);
            $body      = str_replace("%BODY_COPYNAME",$BODY_COPYNAME, $body);

            // Send the password reset request email.*/
            $newActivationEmail = JFactory::getMailer();
            $newActivationEmail->Encoding = 'base64';
            $newActivationEmail->isHtml(true);
            $newActivationEmail->setBody($body);
            $newActivationEmail->addReplyTo($data['mailfrom']);
            $newActivationEmail->setSender( $data['mailfrom']);
            $newActivationEmail->setFrom( $data['fromname']);
            $newActivationEmail->addRecipient($email);
            $newActivationEmail->setSubject($copyrightAlertarPT);
            $newActivationEmail->AddEmbeddedImage(JPATH_ROOT.$logoMailClienteAlertarPT, "banner", "Logo");

            $return = $newActivationEmail->send();

            // Check for an error.
            if ($return !== true)
            {   JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_MAIL_FAILED'), 'error');
                return false;
            }
            return true;
        }

        public static function saveBD($idExterno, $website, $tipoContacto, $assunto, $projeto, $urlProjeto, $espAssunto, $observacoesOrcamentoField, $parceria, $assuntoReuniao, $invertDataReuniao, $horaReuniao, $nomeReuniao, $refVagaTrabalho, $nomeCand, $emailCand, $apresentacaoVaga, $nomeCV, $emailCV, $apresentacaoCV, $apresentacao, $outroAssunto, $propAlertar, $concelho, $email, $nome, $dataAtual){

            $db    = JFactory::getDbo();
            $query = $db->getQuery(true);
            $columns = array('id_externo','website','tipo_form','assunto_form','nome_projeto','url_projeto','especificacao_assunto','obs_Orcamento','esp_parceria','assunto_reuniao','data_reuniao','hora_reuniao','nome_reuniao','ref_vaga_trabalho','nomeCandidato', 'emailCandidato', 'apresentacao_vaga_trabalho','nomeCV','emailCV','apresentacao_cv','esp_bloco_parceria','outro_assunto','elogios_sugestoes','concelho','nif','email','nome','cargo','estado','dataEntrada');
            $values = array($db->quote($db->escape($idExterno)), $db->quote($db->escape($website)), $db->quote($db->escape($tipoContacto)), $db->quote($db->escape($assunto)), $db->quote($db->escape($projeto)), $db->quote($db->escape($urlProjeto)), $db->quote($db->escape($espAssunto)), $db->quote($db->escape($observacoesOrcamentoField)), $db->quote($db->escape($parceria)), $db->quote($db->escape($assuntoReuniao)), $db->quote($db->escape($invertDataReuniao)), $db->quote($db->escape($horaReuniao)), $db->quote($db->escape($nomeReuniao)), $db->quote($db->escape($refVagaTrabalho)), $db->quote($db->escape($nomeCand)), $db->quote($db->escape($emailCand)), $db->quote($db->escape($apresentacaoVaga)), $db->quote($db->escape($nomeCV)), $db->quote($db->escape($emailCV)), $db->quote($db->escape($apresentacaoCV)), $db->quote($db->escape($apresentacao)), $db->quote($db->escape($outroAssunto)), $db->quote($db->escape($propAlertar)), $db->quote($db->escape($concelho)), $db->quote($db->escape('')), $db->quote($db->escape($email)), $db->quote($db->escape($nome)), $db->quote($db->escape('')), $db->quote($db->escape('1')), $db->quote($db->escape($dataAtual)));
            $query
                ->insert($db->quoteName('#__virtualdesk_Government_FaleConnosco'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);

            $result = (boolean) $db->execute();


            return($result);
        }


    }

?>