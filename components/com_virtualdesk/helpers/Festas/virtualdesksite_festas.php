<?php

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');


    class VirtualDeskSiteFestasHelper
    {

            public static function getCartazTicker($dataAtual){
                $db = JFactory::getDBO();
                $db->setQuery($db->getQuery(true)
                    ->select(array('id, data, hora, evento'))
                    ->from("#__virtualdesk_FestasPontaSol_Cartaz")
                    ->where($db->quoteName('data') . ">='" . $db->escape($dataAtual) . "'")
                    ->order('data ASC')
                );
                $data = $db->loadAssocList();
                return ($data);
            }


            public static function getAbvMes($getMes){
                if($getMes == '01'){
                    $nomeMes = 'Jan';
                } else if($getMes == '02'){
                    $nomeMes = 'Fev';
                } else if($getMes == '03'){
                    $nomeMes = 'Mar';
                } else if($getMes == '04'){
                    $nomeMes = 'Abr';
                } else if($getMes == '05'){
                    $nomeMes = 'Mai';
                } else if($getMes == '06'){
                    $nomeMes = 'Jun';
                } else if($getMes == '07'){
                    $nomeMes = 'Jul';
                } else if($getMes == '08'){
                    $nomeMes = 'Ago';
                } else if($getMes == '09'){
                    $nomeMes = 'Set';
                } else if($getMes == '10'){
                    $nomeMes = 'Out';
                } else if($getMes == '11'){
                    $nomeMes = 'Nov';
                } else if($getMes == '12'){
                    $nomeMes = 'Dez';
                }

                return ($nomeMes);
            }
    }

?>