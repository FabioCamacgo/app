<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import joomla controller library
jimport('joomla.application.component.controller');


/**
 * Verifica se o pedido está codificado no url query params
 */
$decodedQueryVars = array();
$uri = JUri::getInstance();
if (JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_urlqueryparams') === "1") {
  JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
  $obVDCrypt = new VirtualDeskSiteCryptHelper();
  $decodedQueryVars = $obVDCrypt->setURLQueryDecoded2Array();
}

// Obtém o objeto  input do joomla
$input = JFactory::getApplication()->input;

// Se tiverem query params previamente descodificados pelo objecto $uri do joomla, então colocamos no input
// Assim podem ser aproveitados pelo controller. Caso contrário os redirects nunca são feitos quando o url está encriptado
if(!empty($decodedQueryVars) ) {
    foreach($decodedQueryVars as $key => $var) {
        $input->set($key, $var);
    }
}

// Get an instance of the controller prefixed by VirtualDesk
$controller = JControllerLegacy::getInstance('VirtualDesk');

// Perform the Request task
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();