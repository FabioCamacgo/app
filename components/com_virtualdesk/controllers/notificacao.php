<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// //JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
//JLoader::register('VirtualDeskBackOfficeAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdeskBackOffice_alerta.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteNotificacaoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_notificacao.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerNotificacao extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 * @return  void
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        return(true);
    }


    /* Nova notificação sem processo associado (ex alerta, agenda, etc), fica por defeito no processo GERAL de notificação  */
    public function sendNovaNotificacao4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $setTrfDesc         = JFactory::getApplication()->input->get('setDesc','','STRING');
        $setTrfNome         = JFactory::getApplication()->input->get('setNome','','STRING');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId          = json_decode(stripslashes($_POST[$NameSetUserId]));  // ARRAY
        $setGroupId         = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if( empty($setUserId) and empty($setGroupId) ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteNotificacaoHelper::saveNovaNotificacao4ManagerByAjax( $setUserId, $setGroupId, $setTrfDesc, $setTrfNome);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    /* Adicionar */
    public function sendNovaNotificacaoByProcesso4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipo    = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        $setTrfDesc         = JFactory::getApplication()->input->get('setDesc','','STRING');
        $setTrfNome         = JFactory::getApplication()->input->get('setNome','','STRING');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId          = json_decode(stripslashes($_POST[$NameSetUserId]));  // ARRAY
        $setGroupId         = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if ((int)$getProcessoId <= 0) exit();
        if( empty($setUserId) and empty($setGroupId) ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteNotificacaoHelper::saveNovaNotificacaoNyProcesso4ManagerByAjax($getProcessoId, $getProcessoTipo, $setUserId, $setGroupId, $setTrfDesc, $setTrfNome);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendNotificacaoAlterarEstadoByProcesso4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipo    = JFactory::getApplication()->input->get('tipoprocesso','','STRING');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        //$getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId = 'notificacao_id';
        //$NameNotificacaoId = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId  = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);

        $setNewEstadoId     = JFactory::getApplication()->input->getInt('setNewEstadoId');
        $setObs             = JFactory::getApplication()->input->get('setObs','','STRING');


        if( (int)$getProcessoId<=0 ||  (int)$getNotificacaoId<=0 || (int)$setNewEstadoId<=0 ) exit();

        $res = VirtualDeskSiteNotificacaoHelper::saveNotificacaoAlteraEstadoByProcesso4ManagerByAjax($getProcessoId, $getProcessoTipo, $getNotificacaoId, $setNewEstadoId, $setObs);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendNotificacaoAlterarEstado4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        $setNewEstadoId     = JFactory::getApplication()->input->getInt('setNewEstadoId');
        $setObs             = JFactory::getApplication()->input->get('setObs','','STRING');

        if( (int)$getNotificacaoId<=0 ) exit();

        $res = VirtualDeskSiteNotificacaoHelper::saveNotificacaoAlteraEstado4ManagerByAjax($getNotificacaoId, $setNewEstadoId, $setObs);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendNotificacaoAlterarEstado4UserByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        $setNewEstadoId     = JFactory::getApplication()->input->getInt('setNewEstadoId');
        $setObs             = JFactory::getApplication()->input->get('setObs','','STRING');

        if( (int)$getNotificacaoId<=0 ) exit();

        $res = VirtualDeskSiteNotificacaoHelper::saveNotificacaoAlteraEstado4UserByAjax($getNotificacaoId, $setNewEstadoId, $setObs);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendNotificacaoEditarByProcesso4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId   = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipo = JFactory::getApplication()->input->get('tipoprocesso','','STRING');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        //$getNotificacaoId     = JFactory::getApplication()->input->getInt('notificacao_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId = 'notificacao_id';
        //$NameNotificacaoId = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId  = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);

        $setEstadoId     = JFactory::getApplication()->input->getInt('setEstadoId');
        $setNome         = JFactory::getApplication()->input->get('setNome','','STRING');
        $setDesc         = JFactory::getApplication()->input->get('setDesc','','STRING');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId       = json_decode(stripslashes($_POST[$NameSetUserId ]));   // ARRAY
        $setGroupId      = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if( (int)$getProcessoId<=0 ||  (int)$getNotificacaoId<=0 || (int)$setEstadoId<=0 ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteNotificacaoHelper::saveNotificacaoEditarByProcesso4ManagerByAjax($getProcessoId, $getProcessoTipo, $getNotificacaoId, $setEstadoId, $setNome,$setDesc, $setUserId, $setGroupId);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendNotificacaoEditar4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getNotificacaoId     = JFactory::getApplication()->input->getInt('notificacao_id');
        $setEstadoId     = JFactory::getApplication()->input->getInt('setEstadoId');
        $setNome         = JFactory::getApplication()->input->get('setNome','','STRING');
        $setDesc         = JFactory::getApplication()->input->get('setDesc','','STRING');

        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId       = json_decode(stripslashes($_POST[$NameSetUserId ]));   // ARRAY
        $setGroupId      = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if( (int)$getNotificacaoId<=0 || (int)$setEstadoId<=0 ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteNotificacaoHelper::saveNotificacaoEditar4ManagerByAjax($getNotificacaoId, $setEstadoId, $setNome,$setDesc, $setUserId, $setGroupId);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Notificacao_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista de notificacaos de um processo id, para serem utiliadas por MANAGERS */
    public function getNotificacoesListByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

               // Vai obter o valor do "processo_id", já tendo em conta que pode estar encriptado
        //$getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameProcessoId = 'processo_id';
        $NameProcessoId = $obVDCrypt->setIdInputNameEncrypt($NameProcessoId,$setencrypt_forminputhidden);
        $getProcessoId  = JFactory::getApplication()->input->get($NameProcessoId);
        $getProcessoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getProcessoId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('processo_tipotag','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesListByProcesso4Manager($getProcessoId,$getProcessoTipoTag,true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista de TODAS notificacaos para serem utiliadas por MANAGERS */
    public function getNotificacoesList4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista da notificacaos de 1 utilizador MANAGER */
    public function getNotificacoesListMinhas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesMinhasList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    public function getNotificacoesListMinhas4UserByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesMinhasList4User(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista da notificacaos dos Grupos de 1 Utilizador MANAGER */
    public function getNotificacoesListMeusGrupos4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesMeusGruposList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter as notificacaos que não estão concluídas nem estão anuladas para um ID processo, por exemplo para um alerta...*/
    public function getNotificacoesListaAbertasByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesListaAbertasByProcesso4Manager($getProcessoId,$getProcessoTipoTag);

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter TODAS as notificacaos que não estão concluídas */
    public function getNotificacoesListaAbertas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesListaAbertas4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter  as notificacaos do User Session que não estão concluídas */
    public function getNotificacoesMinhasListaAbertas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesMinhasListaAbertas4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter  as notificacaos dos Grupos do User Session que não estão concluídas */
    public function getNotificacoesMeusGruposListaAbertas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesMeusGruposListaAbertas4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    /* obter  as notificacaos do User Session que não estão concluídas */
    public function getNotificacoesMinhasListaAbertas4UserByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesMinhasListaAbertas4User();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoDetalheByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        //$getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId = 'notificacao_id';
        //$NameNotificacaoId = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId  = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacaoDetailByProcesso4Manager ($getProcessoId, $getNotificacaoId, $getProcessoTipoTag);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoDetalhe4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        if((int) $getNotificacaoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacaoDetail4Manager ($getNotificacaoId);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoHistoricoByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado mas vem por POST e por AJAX...
        //$getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId = 'notificacao_id';
        // $NameNotificacaoId  = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId  = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();
        if((int) $getNotificacaoId<=0 ) {
            $data = array('data'=>array());
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();

        }

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacaoDetailByProcesso4Manager ($getProcessoId, $getNotificacaoId, $getProcessoTipoTag);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoHistorico4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');

        if((int) $getNotificacaoId<=0 ) {
            $data = array('data'=>array());
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();

        }

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacaoDetail4Manager ( $getNotificacaoId);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoHistorico4UserByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');

        if((int) $getNotificacaoId<=0 ) {
            $data = array('data'=>array());
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();

        }

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacaoDetail4User ( $getNotificacaoId);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoHistoricoListByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Vai obter o valor do "processo_id", já tendo em conta que pode estar encriptado
        //$getProcessoId  = JFactory::getApplication()->input->getInt('processo_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameProcessoId = 'processo_id';
        $NameProcessoId = $obVDCrypt->setIdInputNameEncrypt($NameProcessoId,$setencrypt_forminputhidden);
        $getProcessoId  = JFactory::getApplication()->input->get($NameProcessoId);
        $getProcessoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getProcessoId,$setencrypt_forminputhidden);


        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        // $getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId  = 'notificacao_id';
        $NameNotificacaoId  = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId   = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId   = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);


        $getProcessoTipoTag = JFactory::getApplication()->input->get('processo_tipotag','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesHistoricoListByProcesso4Manager($getProcessoId,$getNotificacaoId,$getProcessoTipoTag,true);

        if(empty($data)) $data = array("data"=>[]);

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoHistoricoList4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId = 'notificacao_id';
        $NameNotificacaoId = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId  = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);

        if((int) $getNotificacaoId>0 ) {
            // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
            $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesHistoricoList4Manager($getNotificacaoId, true);
        }

        if(empty($data)) $data = array("data"=>[]);

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoHistoricoList4UserByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId = 'notificacao_id';
        $NameNotificacaoId = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId  = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);

        if((int) $getNotificacaoId>0 ) {
            // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
            $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesHistoricoList4User($getNotificacaoId, true);
        }

        if(empty($data)) $data = array("data"=>[]);

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoAlterarEstadoByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');

        // Vai obter o valor do "notificacao_id", já tendo em conta que pode estar encriptado
        //$getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameNotificacaoId = 'notificacao_id';
        //$NameNotificacaoId = $obVDCrypt->setIdInputNameEncrypt($NameNotificacaoId,$setencrypt_forminputhidden);
        $getNotificacaoId  = JFactory::getApplication()->input->get($NameNotificacaoId);
        $getNotificacaoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getNotificacaoId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacaoDetailByProcesso4Manager ($getProcessoId, $getNotificacaoId, $getProcessoTipoTag);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNotificacaoAlterarEstado4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();
        $getNotificacaoId        = JFactory::getApplication()->input->getInt('notificacao_id');
        if((int) $getNotificacaoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacaoDetail4Manager ($getNotificacaoId);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter as notificacaos que não estão concluídas nem estão anuladas para o utilizador ATUAL da SESSÃO*/
   /* public function getNotificacoesListaAbertasByUserSession4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Notificacao', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteNotificacaoHelper::getNotificacoesListaAbertasByUserSession4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }*/

}