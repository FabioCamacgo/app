<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerCultura extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 * @return  void
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        return(true);
    }


    public function getCulturaList4UserByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteCulturaHelper::getCulturaList4User(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getCulturaList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteCulturaHelper::getCulturaList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* Verificar se o NIF indicado existe nos USERS do VD ou se existe nos USERS da CULTURA */
    public function checkPromotorNif4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $data = null;

        $getInputNIF_evento   = JFactory::getApplication()->input->getInt('nif_evento');
        if((int) $getInputNIF_evento>0 ) {
            $data = VirtualDeskSiteCulturaHelper:: getCulturaPromotorByNIF4Manager ($getInputNIF_evento);
        }

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNewEstadoName4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputCultura_id   = JFactory::getApplication()->input->getInt('cultura_id');

        if((int) $getInputCultura_id<=0 ) exit();

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

        $resObj = VirtualDeskSiteCulturaHelper::getCulturaEstadoAtualObjectByIdCultura ($lang, $getInputCultura_id);

        if($resObj===false) exit();

        $data=$resObj;

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendAlterar2NewEstado4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputCultura_id   = JFactory::getApplication()->input->getInt('cultura_id');
        $NewEstadoId         = JFactory::getApplication()->input->get('setNewEstadoId');
        $NewEstadoDesc       = JFactory::getApplication()->input->get('setNewEstadoDesc','','STRING');
        $descEstado         = JFactory::getApplication()->input->get('Alterar2NewEstadoDesc');

        if((int) $getInputCultura_id<=0 ) exit();
        if((int) $NewEstadoId<=0 ) exit();

        $res = VirtualDeskSiteCulturaHelper::saveAlterar2NewEstado4ManagerByAjax($getInputCultura_id, $NewEstadoId, $NewEstadoDesc, $descEstado);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Estado_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function create4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));


        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('cultura');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=cultura&layout=addnew4user';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=cultura&layout=addnew4user';
        $sessDataState         = 'com_virtualdesk.addnew4user.cultura.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
    }


    public function create4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('cultura');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'addnew4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=cultura&layout=addnew4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=cultura';
        $sessDataState         = 'com_virtualdesk.addnew4manager.cultura.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
    }


    public function update4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
         * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
         */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4users'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=cultura&layout=edit4user';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=cultura&layout=view4user&vdcleanstate=1&cultura_id='.$data['cultura_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['cultura_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edit4user.cultura.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados para depois poder comparar
        if(!$model->setUserIdAndData4User($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4user.cultura.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4User($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4user.cultura.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData4User($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit4user.cultura.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->save4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4user.cultura.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
    }


    public function update4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('cultura');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Cultura', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=cultura&layout=edit4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=cultura&layout=view4manager&vdcleanstate=1&cultura_id='.$data['cultura_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['cultura_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edit4manager.cultura.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.cultura.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.cultura.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit4manager.cultura.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->save4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.cultura.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
    }


    public function delete4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
    }


    public function delete4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Flush the data from the session.
        VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
    }
}