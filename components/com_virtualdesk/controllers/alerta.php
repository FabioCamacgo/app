<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// //JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskBackOfficeAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdeskBackOffice_alerta.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
//class VirtualDeskController extends JControllerForm
class VirtualDeskControllerAlerta extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }


	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 * @return  void
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        /*
        $AlertaId = $this->input->getInt('alerta_id');
        $basename    = $this->input->get('bname');
        $checkname   = $this->input->get('cname'); // nome
        $thumb       = $this->input->get('thumb');

        // Se vem com cname ttenta carregar directamente o ficheiro se o cname estiver ok

        if(!empty($checkname)) {
            $verificaNome = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($basename);
            $objFile = null;
            if( (string)$verificaNome === (string) $checkname && (string)$verificaNome != '' ) {
               // carrega o ficheiro...
               $objFile = VirtualDeskSiteAlertaHelper::getContactUsFileName2GuestLink ($AlertaId, $basename);
            }
        }
        else{
            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            // verifica se o ficheiro e o Id correspondem ao utilizador da sessão atual
            // a partir do ID do alerta e do filename pesquisa o path do ficheiro
            $objFile = VirtualDeskSiteContactUsHelper::getContactUsFileNameById ($UserJoomlaID, $AlertaId, $basename);
        }


        if (empty($objFile)) {
           // Redirect back to the addnew screen.
           $this->setMessage(JText::_('COM_VIRTUALDESK_ALERTA_DOWNLOAD_FAILED'), 'error');
           $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect(''), false));
           return false;
        }
        else {
           if( $thumb =='true') $objFile->filename = VirtualDeskSiteFileUploadHelper::getImageFileThumbPath($objFile->filename,'300');
           VirtualDeskSiteFileUploadHelper::setDownloadHeaders ($objFile);
           exit;
        }
        */

    }


    public function getmenumainajax()
    {
        $app            = JFactory::getApplication();
        $vdwebsitelist  = $app->input->get('vdwebsitelist',null, 'STRING');

        if(empty($vdwebsitelist)) {
            echo "";
            exit();
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_alerta_menumain")
            ->where($db->quoteName('idwebsitelist').'='.$db->escape($vdwebsitelist).'' )
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getmenusecajax()
    {
        $app            = JFactory::getApplication();
        $vdmenumain       = $app->input->get('vdmenumain',null, 'STRING');

        if(empty($vdmenumain)) {
            echo "";
            exit();
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_alerta_menusec")
            ->where($db->quoteName('idmenumain').'='.$db->escape($vdmenumain).'' )
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getmenusec2ajax()
    {
        $app            = JFactory::getApplication();
        $vdmenusec       = $app->input->get('vdmenusec',null, 'STRING');

        if(empty($vdmenusec)) {
            echo "";
            exit();
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_alerta_menusec2")
            ->where($db->quoteName('idmenusec').'='.$db->escape($vdmenusec).'' )
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getAlertaList4UserByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaList4User(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getAlertaHistoricoList4UserByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id     = JFactory::getApplication()->input->getInt('alerta_id');
        if((int) $getInputAlerta_id<=0 ) exit();


        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaHistoricoList4User($getInputAlerta_id,true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getAlertaHistoricoList4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        //$getInputAlerta_id     = JFactory::getApplication()->input->getInt('alerta_id');
        // Vai obter o valor do "alerta_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameAlertaId = 'alerta_id';
        $NameAlertaId = $obVDCrypt->setIdInputNameEncrypt($NameAlertaId,$setencrypt_forminputhidden);
        $getAlertaId  = JFactory::getApplication()->input->get($NameAlertaId);
        $getAlertaId  = (int)$obVDCrypt->setIdInputValueDecrypt($getAlertaId,$setencrypt_forminputhidden);
        $getInputAlerta_id = $getAlertaId;

        if((int) $getInputAlerta_id<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaHistoricoList4Manager($getInputAlerta_id,true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getAlertaHistTimeLine4UserByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id     = JFactory::getApplication()->input->getInt('alerta_id');
        if((int) $getInputAlerta_id<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaHistoricoList4User($getInputAlerta_id,false);

        if(empty($data)) exit();

        ob_start();
        include_once(JPATH_SITE . '/components/com_virtualdesk/helpers/html/tpl_alerta_historico_timeline.php');
        $tplContent = ob_get_contents();
        ob_end_clean ();

        //$app->setHeader('Content-Type', 'application/json; charset=utf-8');
        //$dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $tplContent;
        exit();
    }


    public function getAlertaHistTimeLine4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id     = JFactory::getApplication()->input->getInt('alerta_id');
        if((int) $getInputAlerta_id<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaHistoricoList4Manager($getInputAlerta_id,false);

        if(empty($data)) exit();

        ob_start();
        include_once(JPATH_SITE . '/components/com_virtualdesk/helpers/html/tpl_alerta_historico_timeline.php');
        $tplContent = ob_get_contents();
        ob_end_clean ();

        //$app->setHeader('Content-Type', 'application/json; charset=utf-8');
        //$dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $tplContent;
        exit();
    }


    public function getAlertaHistTimeLineSimple4UserByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id     = JFactory::getApplication()->input->getInt('alerta_id');
        if((int) $getInputAlerta_id<=0 ) exit();


        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaHistoricoList4User($getInputAlerta_id,false);

        if(empty($data)) exit();

        ob_start();
        include_once(JPATH_SITE . '/components/com_virtualdesk/helpers/html/tpl_alerta_historico_timeline_simple.php');
        $tplContent = ob_get_contents();
        ob_end_clean ();

        //$app->setHeader('Content-Type', 'application/json; charset=utf-8');
        //$dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $tplContent;
        exit();
    }


    public function getAlertaList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obtém a lista das ocorrências com o último encaminhamento para o utilizador ou para um grupo do qual o utilizador faz parte */
    public function getAlertaEncaminhadasList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaEncaminhadasList4Manager(true);

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obtém a lista das ocorrências com tarefas por concluir para o utilizador ou para um grupo do qual o utilizador faz parte */
    public function getAlertaTarefasList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaTarefasList4Manager(true);

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obtém a lista de categorias do alerta */
    public function getAlertaListConfCategorias4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaListConfCategorias4Manager(true);

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    /* obtém a lista de Sub Categorias do alerta */
    public function getAlertaListConfSubCategorias4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getAlertaListConfSubCategorias4Manager(true);

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendNewMsgHist4UserByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id     = JFactory::getApplication()->input->getInt('alerta_id');
        $NewMsg                = JFactory::getApplication()->input->get('sendnewmsgtext','','STRING');
        if((int) $getInputAlerta_id<=0 ) exit();

        $res = VirtualDeskSiteAlertaHelper::saveNewMsgHist4UserByAjax($getInputAlerta_id, $NewMsg);

        if($res===false) exit();

        $data='New_Msg_OK';

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendNewMsgHist4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id     = JFactory::getApplication()->input->getInt('alerta_id');
        $NewMsg                = JFactory::getApplication()->input->get('sendnewmsgtext','','STRING');
        $setVisible4User       = JFactory::getApplication()->input->get('setvisible4user');
        $seTipoMsg             = JFactory::getApplication()->input->getInt('setipomsg');
        if((int) $getInputAlerta_id<=0 ) exit();

        if((string)$setVisible4User=='true') {
            $setVisible4User = 1;
        }
        else {
            $setVisible4User = 0;
        }

        $res = VirtualDeskSiteAlertaHelper::saveNewMsgHist4ManagerByAjax($getInputAlerta_id, $NewMsg, $setVisible4User, $seTipoMsg);

        if($res===false) exit();

        $data='New_Msg_OK';

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendAlterar2NewEstado4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id   = JFactory::getApplication()->input->getInt('alerta_id');
        $NewEstadoId         = JFactory::getApplication()->input->get('setNewEstadoId');
        $NewEstadoDesc       = JFactory::getApplication()->input->get('setNewEstadoDesc','','STRING');

        if((int) $getInputAlerta_id<=0 ) exit();
        if((int) $NewEstadoId<=0 ) exit();

        $res = VirtualDeskSiteAlertaHelper::saveAlterar2NewEstado4ManagerByAjax($getInputAlerta_id, $NewEstadoId, $NewEstadoDesc);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Estado_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendAlterar2Reencaminhar4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id  = JFactory::getApplication()->input->getInt('alerta_id');
        $setReencDesc       = JFactory::getApplication()->input->get('setReencaminharDesc','','STRING');
        //$setUserId          = json_decode(stripslashes($_POST['setUserId']));   // ARRAY
        //$setGroupId         = json_decode(stripslashes($_POST['setGroupId'])); // ARRAY

        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId       = json_decode(stripslashes($_POST[$NameSetUserId ]));   // ARRAY
        $setGroupId      = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if((int) $getInputAlerta_id<=0 ) exit();
        if( empty($setUserId) and empty($setGroupId) ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteAlertaHelper::saveAlterar2Reencaminhar4ManagerByAjax($getInputAlerta_id, $setUserId, $setGroupId, $setReencDesc);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendAlterar2NewProcManager4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id   = JFactory::getApplication()->input->getInt('alerta_id');
        $NewProcManagerId         = JFactory::getApplication()->input->get('setNewProcManagerId');
        $NewProcManagerDesc       = JFactory::getApplication()->input->get('setNewProcManagerDesc','','STRING');

        if((int) $getInputAlerta_id<=0 ) exit();
        if((int) $NewProcManagerId<=0 ) exit();

        $res = VirtualDeskSiteAlertaHelper::saveAlterar2NewProcManager4ManagerByAjax($getInputAlerta_id, $NewProcManagerId, $NewProcManagerDesc);

        //$res = false;

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data = 'New_ProcManager_OK';
        }

        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNewEstadoName4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id   = JFactory::getApplication()->input->getInt('alerta_id');

        if((int) $getInputAlerta_id<=0 ) exit();

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

        $resObj = VirtualDeskSiteAlertaHelper::getAlertaEstadoAtualObjectByIdAlerta ($lang, $getInputAlerta_id);

        if($resObj===false) exit();

        $data=$resObj;

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNewProcManagerName4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id   = JFactory::getApplication()->input->getInt('alerta_id');

        if((int) $getInputAlerta_id<=0 ) exit();

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

        $resObj = VirtualDeskSiteAlertaHelper::getAlertaProcManagerAtualObjectByIdAlerta ($lang, $getInputAlerta_id);

        if($resObj===false) exit();

        $data=$resObj;

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNewReencaminharUserGroup4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputAlerta_id   = JFactory::getApplication()->input->getInt('alerta_id');

        if((int) $getInputAlerta_id<=0 ) exit();

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

        $resObj  = VirtualDeskSiteAlertaHelper::getAlertaView4ManagerDetailGetReencaminhar ($getInputAlerta_id);

        if($resObj===false) exit();

        $data = $resObj;

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter as ocorrências que estão por analisar/são novas*/
    public function getAlertaListaNovas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getOcorrenciasNovasList4Manager(false);

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* Carrega lista com as ocorrências novas/em espera encaminhadas para o utilizador ou para os grupos do utilizador */
    public function getAlertaListaNovasEncaminhadas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAlertaHelper::getOcorrenciasNovasEncaminhadasList4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function create4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));


        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=alerta&layout=addnew4user';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta';
        $sessDataState         = 'com_virtualdesk.addnew4user.alerta.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function create4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'addnew4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=alerta&layout=addnew4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta';
        $sessDataState         = 'com_virtualdesk.addnew4manager.alerta.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function createConfCategoria4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'addnew4managers'); // verifica permissão acesso ao layout
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormDataConfCategoria4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=alerta&layout=addnewconfcategoria4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta&layout=listconf4manager#tab_Alerta_Conf_Categorias';
        $sessDataState         = 'com_virtualdesk.addnewconfcategoria4manager.alerta.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSaveConfCategoria4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createConfCategoria4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function createConfSubCategoria4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');                  // verifica permissão de
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'addnew4managers'); // verifica permissão acesso ao layout
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormDataConfSubCategoria4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=alerta&layout=addnewconfsubcategoria4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta&layout=listconf4manager#tab_Alerta_Conf_SubCategorias';

        $sessDataState         = 'com_virtualdesk.addnewconfsubcategoria4manager.alerta.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSaveConfSubCategoria4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createConfSubCategoria4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function update4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
         * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
         */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4users'); // verifica permissão acesso ao layout para editar
        if($vbHasAccess===false || $vbHasAccess2===false ) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=alerta&layout=edit4user';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta&layout=view4user&vdcleanstate=1&alerta_id='.$data['alerta_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['alerta_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edit4user.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados para depois poder comparar
        if(!$model->setUserIdAndData4User($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4user.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4User($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4user.alerta.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData4User($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit4user.alerta.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->save4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4user.alerta.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function update4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=alerta&layout=edit4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta&layout=view4manager&vdcleanstate=1&alerta_id='.$data['alerta_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['alerta_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edit4manager.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.alerta.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit4manager.alerta.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->save4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.alerta.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function updateConfCategoria4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormDataConfCategoria4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=alerta&layout=editconfcategoria4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta&layout=viewconfcategoria4manager&vdcleanstate=1&categoria_id='.$data['categoria_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['categoria_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editconfcategoria4manager.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndDataConfCategoria4Manager($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editconfcategoria4manager.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSaveConfCategoria4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editconfcategoria4manager.alerta.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormDataConfCategoria4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editconfcategoria4manager.alerta.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveConfCategoria4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editconfcategoria4manager.alerta.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function updateConfSubCategoria4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('Alerta', 'VirtualDeskModel');
        $data   = $model->getPostedFormDataConfSubCategoria4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=alerta&layout=editconfsubcategoria4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alerta&layout=viewconfsubcategoria4manager&vdcleanstate=1&subcategoria_id='.$data['subcategoria_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['subcategoria_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editconfsubcategoria4manager.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndDataConfSubCategoria4Manager($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editconfsubcategoria4manager.alerta.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSaveConfSubCategoria4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editconfsubcategoria4manager.alerta.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormDataConfSubCategoria4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editconfsubcategoria4manager.alerta.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveConfSubCategoria4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editconfsubcategoria4manager.alerta.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function delete4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));




        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


    public function delete4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));




        // Flush the data from the session.
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }


}