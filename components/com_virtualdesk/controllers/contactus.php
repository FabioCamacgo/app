<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// //JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');
JLoader::register('VirtualDeskSiteContactUsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
//class VirtualDeskController extends JControllerForm
class VirtualDeskControllerContactUs extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }



    public function cancel()
    {
        $app = JFactory::getApplication();
        // Flush the data from the session.
        $app->setUserState('com_virtualdesk.addnew.contactus.data', null);
        $app->setUserState('com_virtualdesk.edit.contactus.data', null);

        $itemmenuid_lista= $params = JComponentHelper::getParams('com_virtualdesk')->get('contactus_menuitemid_list');

        //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect(''), false));
        $this->setRedirect(JRoute::_('index.php?Itemid='.$itemmenuid_lista.'&option=com_virtualdesk&view=contactus&layout=list' , true));
        return true;
    }


    public function getsubcategoriasAlerta()
    {
        $app            = JFactory::getApplication();
        $vdwebsitelist       = $app->input->get('vdwebsitelist',null, 'STRING');

        if(empty($vdwebsitelist)) {
            echo "";
            exit();
        }

        $lang = VirtualDeskHelper::getLanguageTag();
        if( empty($lang) or ($lang='pt_PT') ) {
            $CatName = 'name_PT';
        }
        else {
            $CatName = 'name_EN';
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('Id_subcategoria, '. $CatName .' as name'))
            ->from("#__virtualdesk_alerta_subcategoria")
            ->where($db->quoteName('Id_categoria').'='.$db->escape($vdwebsitelist).'' )
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function addnew()
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailAddNewAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        JFactory::getApplication()->input->set('layout','addnew');
        parent::display();
        return true;
    }

    public function addnewcat()
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailAddNewAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        JFactory::getApplication()->input->set('layout','addnewcat');
        parent::display();
        return true;
    }

	/**
	 * Method to check out a user for editing and redirect to the edit form.
	 * @return  boolean
	 * @since   1.6
	 */
	public function edit()
	{
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailEditAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $IdContactUs = $this->input->getInt('contactus_id');

        if((int)$IdContactUs > 0)
        {
            // Redirect to the edit screen.
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit&contactus_id=' . $IdContactUs, false));
        }
        else
        { $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus', false)); }
		return true;
	}

    public function editcat()
    {
        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailEditAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $IdContactUs = $this->input->getInt('contactus_id');

        if((int)$IdContactUs > 0)
        {
            // Redirect to the edit screen.
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=editcat&contactus_id=' . $IdContactUs, false));
        }
        else
        { $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=listcat', false)); }
        return true;
    }


    /**
     * Method to save a contactus data.
     * @return  mixed
     * @since   1.6
     */
    public function create()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailAddNewAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('ContactUs', 'VirtualDeskModel');

        $data = $model->getPostedFormData();

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->contactusavatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.addnew.contactus.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED', $model->getError()), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('addnew'), false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=addnew' , false));
            return false;
        }



        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.addnew.contactus.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED', $model->getError()), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('addnew'), false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=addnew' , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.addnew.contactus.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED', $model->getError()), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('addnew'), false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=addnew' , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        //$redirect = 'index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('default');

        $itemmenuid_lista= $params = JComponentHelper::getParams('com_virtualdesk')->get('contactus_menuitemid_list');
        $redirect = 'index.php?Itemid='.$itemmenuid_lista.'&option=com_virtualdesk&view=contactus&layout=list';
        $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirect, false));

        // Flush the data from the session.
        $app->setUserState('com_virtualdesk.addnew.contactus.data', null);
    }



	/**
	 * Method to UPDATE a contactus data.
	 * @return  mixed
	 * @since   1.6
	 */
	public function update()
	{
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));


        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailEditAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('ContactUs', 'VirtualDeskModel');
        $data   = $model->getPostedFormData();


        // Verifica se foi enviado o id de contactus
        if ((int) $data['contactus_id'] <=0 ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.contactus.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit') , false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit' , false));
            return false;
        }


        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();
        if ($returnUserSession === false ) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.contactus.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED', $model->getError()), 'error');

            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit'), false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit' , false));
            return false;
        }

        // Carrega os dados da "contactus" para depois poder comparar
        if(!$model->setUserIdAndContactUsData($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.contactus.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED'), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect(''), false));
            $itemmenuid_lista= $params = JComponentHelper::getParams('com_virtualdesk')->get('contactus_menuitemid_list');
            $this->setRedirect(JRoute::_('index.php?Itemid='.$itemmenuid_lista.'&option=com_virtualdesk&view=contactus&layout=list' , false));
            return false;
        }


        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit.contactus.data', null);
            // Redirect to the list screen.
            //$redirect = 'index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('') ;
            $itemmenuid_lista= $params = JComponentHelper::getParams('com_virtualdesk')->get('contactus_menuitemid_list');
            $redirect = 'index.php?Itemid='.$itemmenuid_lista.'&option=com_virtualdesk&view=contactus&layout=list';
            $this->setMessage(JText::_('COM_VIRTUALDESK_PROFILE_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirect, false));
            return true;
        }


        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.contactus.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED', $model->getError()), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit'), false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit', false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->save($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.contactus.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED', $model->getError()), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit') , false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit' , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        //$redirect = 'index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('default') ;
        $itemmenuid_lista= $params = JComponentHelper::getParams('com_virtualdesk')->get('contactus_menuitemid_list');
        $redirect = 'index.php?Itemid='.$itemmenuid_lista.'&option=com_virtualdesk&view=contactus&layout=list';
        $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirect, false));


        // Flush the data from the session.
        $app->setUserState('com_virtualdesk.edit.contactus.data', null);
	}


	/**
	 * Function that allows child controller access to model data after the data has been saved.	    
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.	    
	 * @return  void	    
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        $ContactusId = $this->input->getInt('contactus_id');
        $basename    = $this->input->get('bname');
        $checkname   = $this->input->get('cname'); // nome
        $thumb       = $this->input->get('thumb');

        // Se vem com cname ttenta carregar directamente o ficheiro se o cname estiver ok

        if(!empty($checkname)) {
            $verificaNome = VirtualDeskSiteFileUploadHelper::setFileGuestDownloadLink($basename);
            $objFile = null;
            if( (string)$verificaNome === (string) $checkname && (string)$verificaNome != '' ) {
               // carrega o ficheiro...
               $objFile = VirtualDeskSiteContactUsHelper::getContactUsFileName2GuestLink ($ContactusId, $basename);
            }
        }
        else{
            $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();
            // verifica se o ficheiro e o Id correspondem ao utilizador da sessão atual
            // a partir do ID do contactus e do filename pesquisa o path do ficheiro
            $objFile = VirtualDeskSiteContactUsHelper::getContactUsFileNameById ($UserJoomlaID, $ContactusId, $basename);
        }


        if (empty($objFile)) {
           // Redirect back to the addnew screen.
           $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_DOWNLOAD_FAILED'), 'error');
           $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect(''), false));
           return false;
        }
        else {
           if( $thumb =='true') $objFile->filename = VirtualDeskSiteFileUploadHelper::getImageFileThumbPath($objFile->filename,'300');
           VirtualDeskSiteFileUploadHelper::setDownloadHeaders ($objFile);
           exit;
        }

    }


    /**
     * Method to DELETE a contactus data.
     * @return  mixed
     * @since   1.6
     */
    public function delete()
    {
        // Check for request forgeries.
        //JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Check Permissões
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkDetailDeleteAccess('contactus');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('ContactUs', 'VirtualDeskModel');
        $data   = array();

        // Campos BASE  (existem sempre)
        $data['contactus_id']  = $app->input->getInt('contactus_id');

        // Verifica se foi enviado o id de contactus
        if ((int) $data['contactus_id'] <=0 ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_DELETE_FAILED'), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit') , false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit',false));
            return false;
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();
        if ($returnUserSession === false ) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_DELETE_FAILED', $model->getError()), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit'), false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit', false));
            return false;
        }

        // Carrega os dados da "contactus" para depois poder comparar
        if(!$model->setUserIdAndContactUsData($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_DELETE_FAILED'), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect(''), false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit', false));
            return false;
        }

//        // Valida os dados antes da gravação
//        $returnValid = $model->validateBeforeSave($data);
//        // Check for errors.
//        if ($returnValid === false) {
//            // Save the data in the session.
//            $data->attachment = null;  // para não ficar com array de file na sessão
//            $app->setUserState('com_virtualdesk.edit.contactus.data', $data);
//            // Redirect back to the addnew screen.
//            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_SAVE_FAILED', $model->getError()), 'error');
//            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit'), false));
//            return false;
//        }

        // Attempt to save the data.
        $return = $model->delete($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            //$app->setUserState('com_virtualdesk.edit.contactus.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_CONTACTUS_DELETE_FAILED', $model->getError()), 'error');
            //$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('edit') , false));
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=edit' , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        //$redirect = 'index.php?option=com_virtualdesk&view=contactus&layout=' . VirtualDeskSiteContactUsHelper::getCheckLayoutToRedirect('default') ;
        $itemmenuid_lista= $params = JComponentHelper::getParams('com_virtualdesk')->get('contactus_menuitemid_list');
        $redirect = 'index.php?Itemid='.$itemmenuid_lista.'&option=com_virtualdesk&view=contactus&layout=list' ;
        $this->setMessage(JText::_('COM_VIRTUALDESK_CONTACTUS_DELETE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirect, false));


        // Flush the data from the session.
        $app->setUserState('com_virtualdesk.edit.contactus.data', null);
    }


    public function getmenumainajax()
    {
        $app            = JFactory::getApplication();
        $vdwebsitelist  = $app->input->get('vdwebsitelist',null, 'STRING');

        if(empty($vdwebsitelist)) {
            echo "";
            exit();
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_contactus_menumain")
            ->where($db->quoteName('idwebsitelist').'='.$db->escape($vdwebsitelist).'' )
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getmenusecajax()
    {
        $app            = JFactory::getApplication();
        $vdmenumain       = $app->input->get('vdmenumain',null, 'STRING');

        if(empty($vdmenumain)) {
            echo "";
            exit();
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_contactus_menusec")
            ->where($db->quoteName('idmenumain').'='.$db->escape($vdmenumain).'' )
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }



    public function getmenusec2ajax()
    {
        $app            = JFactory::getApplication();
        $vdmenusec       = $app->input->get('vdmenusec',null, 'STRING');

        if(empty($vdmenusec)) {
            echo "";
            exit();
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, name_pt as name'))
            ->from("#__virtualdesk_contactus_menusec2")
            ->where($db->quoteName('idmenusec').'='.$db->escape($vdmenusec).'' )
            ->order('name ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    /*
    * Acesso Principal para a lista com DataTables por Ajax
    */
    public function getDataMainByAjax()
    {
        // JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('ContactUs', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteContactUsHelper::getContactUsList($userSessionID, true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    /*
   * Acesso Principal para os reports com DataTables por Ajax
   */
    public function getDataForReportsByAjax()
    {
        // JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
        JLoader::register('VirtualDeskSiteContactUsReportsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus_reports.php');

        // check session +  joomla user
        $app               = JFactory::getApplication();
        $model             = $this->getModel('ContactUs', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID     = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);
        $jinput            = JFactory::getApplication()->input;
        $datatag         = $jinput->get('datatag', '', 'string');

        if((int) $userSessionID<=0 ) exit();

        //  Grupos e Permissões : As permissão são verificas dentro de cada método de carregamento pois está sempre associado a um report

        $objReport = new VirtualDeskSiteContactUsReportsHelper();

        switch($datatag)
        {   case 'listResumoEstado':
                $data = $objReport->getContactUsReportResume($userSessionID);
                break;

            case 'listResumoCategoria':
                $data = $objReport->getContactUsPedidosPorCategoria($userSessionID);
                break;

            case 'dashboard01':
                break;

            default:
                exit();
                break;
        }

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


}