<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSitealertarptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertarpt.php');
JLoader::register('VirtualDeskSitealertarptFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertarpt_files.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControlleralertarpt extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Function that allows child controller access to model data after the data has been saved.
     * @param   JModelLegacy  $model      The data model object.
     * @param   array         $validData  The validated data.
     * @return  void
     * @since   3.1
     */
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {

    }


    public function download()
    {
        return(true);
    }


    public function getalertarptList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('alertarpt', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitealertarptHelper::getalertarptList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getalertarptFaleConnoscoList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('alertarpt', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitealertarptHelper::getalertarptFaleConnoscoList4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNewEstadoName4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('alertarpt', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputalertarpt_id   = JFactory::getApplication()->input->getInt('alertarpt_id');

        if((int) $getInputalertarpt_id<=0 ) exit();

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

        $resObj = VirtualDeskSitealertarptHelper::getalertarptEstadoAtualObjectByIdalertarpt ($lang, $getInputalertarpt_id);

        if($resObj===false) exit();

        $data=$resObj;

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendAlterar2NewEstado4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('alertarpt', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputalertarpt_id   = JFactory::getApplication()->input->getInt('alertarpt_id');
        $NewEstadoId         = JFactory::getApplication()->input->get('setNewEstadoId');
        $NewEstadoDesc       = JFactory::getApplication()->input->get('setNewEstadoDesc','','STRING');

        if((int) $getInputalertarpt_id<=0 ) exit();
        if((int) $NewEstadoId<=0 ) exit();

        $res = VirtualDeskSitealertarptHelper::saveAlterar2NewEstado4ManagerByAjax($getInputalertarpt_id, $NewEstadoId, $NewEstadoDesc);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Estado_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }



    public function create4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alertarpt');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alertarpt', 'addnew4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('alertarpt', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=alertarpt&layout=addnew4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alertarpt';
        $sessDataState         = 'com_virtualdesk.addnew4manager.alertarpt.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitealertarptHelper::cleanAllTmpUserState();
    }


    public function update4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alertarpt');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alertarpt', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('alertarpt', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=alertarpt&layout=edit4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=alertarpt&layout=view4manager&vdcleanstate=1&alertarpt_id='.$data['alertarpt_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['alertarpt_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edit4manager.alertarpt.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.alertarpt.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.alertarpt.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit4manager.alertarpt.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->save4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.alertarpt.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitealertarptHelper::cleanAllTmpUserState();
    }


    public function delete4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Flush the data from the session.
        VirtualDeskSitealertarptHelper::cleanAllTmpUserState();
    }

    public function download4Manager()
    {

        /*
         * Check PERMISSÕES
         */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alertarpt');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alertarpt', 'view4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $objFile = new VirtualDeskSitealertarptFilesHelper();
        $objFile->download4Manager();

        return(true);
    }


}