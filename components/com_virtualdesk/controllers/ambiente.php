<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteAmbienteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ambiente.php');
JLoader::register('VirtualDeskSiteRemocaoVeiculosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_remocaoVeiculos.php');


jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerAmbiente extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 * @return  void
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{
        return(true);
	}


    public function download()
    {
        return(true);
    }

    public function getRemocaoVeiculosList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Ambiente', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteRemocaoVeiculosHelper::getRemocaoVeiculosList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    public function getRemocaoVeiculosList4UserByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Ambiente', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteRemocaoVeiculosHelper::getRemocaoVeiculosList4User(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    public function createrecolhaanimais4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('ambiente');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('ambiente', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelanimais';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelanimais';
        $sessDataState         = 'com_virtualdesk.addnew4user.ambiente.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateRecolhaAnimaisBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createRecolhaAnimais4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAmbienteHelper::cleanAllTmpUserState();
    }

}