<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// //JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
//JLoader::register('VirtualDeskBackOfficeAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdeskBackOffice_alerta.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteTarefaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tarefa.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerTarefa extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 * @return  void
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        return(true);
    }


    public function sendNovaTarefa4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipo    = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        $setTrfDesc         = JFactory::getApplication()->input->get('setDesc','','STRING');
        $setTrfNome         = JFactory::getApplication()->input->get('setNome','','STRING');

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId          = json_decode(stripslashes($_POST[$NameSetUserId]));  // ARRAY
        $setGroupId         = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if((int) $getProcessoId<=0 ) exit();
        if( empty($setUserId) and empty($setGroupId) ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteTarefaHelper::saveNovaTarefa4ManagerByAjax($getProcessoId, $getProcessoTipo, $setUserId, $setGroupId, $setTrfDesc, $setTrfNome);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendTarefaAlterarEstadoByProcesso4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipo    = JFactory::getApplication()->input->get('tipoprocesso','','STRING');

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        //$getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameTarefaId = 'tarefa_id';
        //$NameTarefaId = $obVDCrypt->setIdInputNameEncrypt($NameTarefaId,$setencrypt_forminputhidden);
        $getTarefaId  = JFactory::getApplication()->input->get($NameTarefaId);
        $getTarefaId  = (int)$obVDCrypt->setIdInputValueDecrypt($getTarefaId,$setencrypt_forminputhidden);

        $setNewEstadoId     = JFactory::getApplication()->input->getInt('setNewEstadoId');
        $setObs             = JFactory::getApplication()->input->get('setObs','','STRING');


        if( (int)$getProcessoId<=0 ||  (int)$getTarefaId<=0 || (int)$setNewEstadoId<=0 ) exit();

        $res = VirtualDeskSiteTarefaHelper::saveTarefaAlteraEstadoByProcesso4ManagerByAjax($getProcessoId, $getProcessoTipo, $getTarefaId, $setNewEstadoId, $setObs);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendTarefaAlterarEstado4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        $setNewEstadoId     = JFactory::getApplication()->input->getInt('setNewEstadoId');
        $setObs             = JFactory::getApplication()->input->get('setObs','','STRING');

        if( (int)$getTarefaId<=0 ) exit();

        $res = VirtualDeskSiteTarefaHelper::saveTarefaAlteraEstado4ManagerByAjax($getTarefaId, $setNewEstadoId, $setObs);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendTarefaEditarByProcesso4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId   = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipo = JFactory::getApplication()->input->get('tipoprocesso','','STRING');

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        //$getTarefaId     = JFactory::getApplication()->input->getInt('tarefa_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameTarefaId = 'tarefa_id';
        //$NameTarefaId = $obVDCrypt->setIdInputNameEncrypt($NameTarefaId,$setencrypt_forminputhidden);
        $getTarefaId  = JFactory::getApplication()->input->get($NameTarefaId);
        $getTarefaId  = (int)$obVDCrypt->setIdInputValueDecrypt($getTarefaId,$setencrypt_forminputhidden);

        $setEstadoId     = JFactory::getApplication()->input->getInt('setEstadoId');
        $setNome         = JFactory::getApplication()->input->get('setNome','','STRING');
        $setDesc         = JFactory::getApplication()->input->get('setDesc','','STRING');

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId       = json_decode(stripslashes($_POST[$NameSetUserId ]));   // ARRAY
        $setGroupId      = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if( (int)$getProcessoId<=0 ||  (int)$getTarefaId<=0 || (int)$setEstadoId<=0 ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteTarefaHelper::saveTarefaEditarByProcesso4ManagerByAjax($getProcessoId, $getProcessoTipo, $getTarefaId, $setEstadoId, $setNome,$setDesc, $setUserId, $setGroupId);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Reencaminhar_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendTarefaEditar4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getTarefaId     = JFactory::getApplication()->input->getInt('tarefa_id');
        $setEstadoId     = JFactory::getApplication()->input->getInt('setEstadoId');
        $setNome         = JFactory::getApplication()->input->get('setNome','','STRING');
        $setDesc         = JFactory::getApplication()->input->get('setDesc','','STRING');

        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameSetUserId  = 'setUserId';
        $NameSetGroupId = 'setGroupId';
        $NameSetUserId   = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetUserId,$setencrypt_forminputhidden);
        $NameSetGroupId  = $obVDCrypt->setIdInputMultiNameEncrypt($NameSetGroupId,$setencrypt_forminputhidden);
        $setUserId       = json_decode(stripslashes($_POST[$NameSetUserId ]));   // ARRAY
        $setGroupId      = json_decode(stripslashes($_POST[$NameSetGroupId])); // ARRAY

        if( (int)$getTarefaId<=0 || (int)$setEstadoId<=0 ) exit();

        $setUserId  = $obVDCrypt->setIdInputMultiValueDecrypt($setUserId, $setencrypt_forminputhidden);
        $setGroupId = $obVDCrypt->setIdInputMultiValueDecrypt($setGroupId, $setencrypt_forminputhidden);

        $res = VirtualDeskSiteTarefaHelper::saveTarefaEditar4ManagerByAjax($getTarefaId, $setEstadoId, $setNome,$setDesc, $setUserId, $setGroupId);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Tarefa_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista de tarefas de um processo id, para serem utiliadas por MANAGERS */
    public function getTarefasListByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

               // Vai obter o valor do "processo_id", já tendo em conta que pode estar encriptado
        //$getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameProcessoId = 'processo_id';
        $NameProcessoId = $obVDCrypt->setIdInputNameEncrypt($NameProcessoId,$setencrypt_forminputhidden);
        $getProcessoId  = JFactory::getApplication()->input->get($NameProcessoId);
        $getProcessoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getProcessoId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('processo_tipotag','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasListByProcesso4Manager($getProcessoId,$getProcessoTipoTag,true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista de TODAS tarefas para serem utiliadas por MANAGERS */
    public function getTarefasList4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista da tarefas de 1 utilizador MANAGER */
    public function getTarefasListMinhas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasMinhasList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* carrega a lista da tarefas dos Grupos de 1 Utilizador MANAGER */
    public function getTarefasListMeusGrupos4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasMeusGruposList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter as tarefas que não estão concluídas nem estão anuladas para um ID processo, por exemplo para um alerta...*/
    public function getTarefasListaAbertasByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');
        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasListaAbertasByProcesso4Manager($getProcessoId,$getProcessoTipoTag);

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter TODAS as tarefas que não estão concluídas */
    public function getTarefasListaAbertas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasListaAbertas4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter  as tarefas do User Session que não estão concluídas */
    public function getTarefasMinhasListaAbertas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasMinhasListaAbertas4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter  as tarefas dos Grupos do User Session que não estão concluídas */
    public function getTarefasMeusGruposListaAbertas4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasMeusGruposListaAbertas4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaDetalheByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        //$getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameTarefaId = 'tarefa_id';
        //$NameTarefaId = $obVDCrypt->setIdInputNameEncrypt($NameTarefaId,$setencrypt_forminputhidden);
        $getTarefaId  = JFactory::getApplication()->input->get($NameTarefaId);
        $getTarefaId  = (int)$obVDCrypt->setIdInputValueDecrypt($getTarefaId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefaDetailByProcesso4Manager ($getProcessoId, $getTarefaId, $getProcessoTipoTag);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaDetalhe4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        if((int) $getTarefaId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefaDetail4Manager ($getTarefaId);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaHistoricoByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado mas vem por POST e por AJAX...
        //$getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameTarefaId = 'tarefa_id';
        // $NameTarefaId  = $obVDCrypt->setIdInputNameEncrypt($NameTarefaId,$setencrypt_forminputhidden);
        $getTarefaId  = JFactory::getApplication()->input->get($NameTarefaId);
        $getTarefaId  = (int)$obVDCrypt->setIdInputValueDecrypt($getTarefaId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();
        if((int) $getTarefaId<=0 ) {
            $data = array('data'=>array());
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();

        }

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefaDetailByProcesso4Manager ($getProcessoId, $getTarefaId, $getProcessoTipoTag);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaHistorico4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');

        if((int) $getTarefaId<=0 ) {
            $data = array('data'=>array());
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();

        }

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefaDetail4Manager ( $getTarefaId);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaHistoricoListByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Vai obter o valor do "processo_id", já tendo em conta que pode estar encriptado
        //$getProcessoId  = JFactory::getApplication()->input->getInt('processo_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameProcessoId = 'processo_id';
        $NameProcessoId = $obVDCrypt->setIdInputNameEncrypt($NameProcessoId,$setencrypt_forminputhidden);
        $getProcessoId  = JFactory::getApplication()->input->get($NameProcessoId);
        $getProcessoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getProcessoId,$setencrypt_forminputhidden);


        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        // $getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameTarefaId  = 'tarefa_id';
        $NameTarefaId  = $obVDCrypt->setIdInputNameEncrypt($NameTarefaId,$setencrypt_forminputhidden);
        $getTarefaId   = JFactory::getApplication()->input->get($NameTarefaId);
        $getTarefaId   = (int)$obVDCrypt->setIdInputValueDecrypt($getTarefaId,$setencrypt_forminputhidden);


        $getProcessoTipoTag = JFactory::getApplication()->input->get('processo_tipotag','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasHistoricoListByProcesso4Manager($getProcessoId,$getTarefaId,$getProcessoTipoTag,true);

        if(empty($data)) $data = array("data"=>[]);

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaHistoricoList4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameTarefaId = 'tarefa_id';
        $NameTarefaId = $obVDCrypt->setIdInputNameEncrypt($NameTarefaId,$setencrypt_forminputhidden);
        $getTarefaId  = JFactory::getApplication()->input->get($NameTarefaId);
        $getTarefaId  = (int)$obVDCrypt->setIdInputValueDecrypt($getTarefaId,$setencrypt_forminputhidden);

        if((int) $getTarefaId>0 ) {
            // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
            $data = VirtualDeskSiteTarefaHelper::getTarefasHistoricoList4Manager($getTarefaId, true);
        }

        if(empty($data)) $data = array("data"=>[]);

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaAlterarEstadoByProcesso4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getProcessoId      = JFactory::getApplication()->input->getInt('processo_id');

        // Vai obter o valor do "tarefa_id", já tendo em conta que pode estar encriptado
        //$getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        $obVDCrypt                  = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        $NameTarefaId = 'tarefa_id';
        //$NameTarefaId = $obVDCrypt->setIdInputNameEncrypt($NameTarefaId,$setencrypt_forminputhidden);
        $getTarefaId  = JFactory::getApplication()->input->get($NameTarefaId);
        $getTarefaId  = (int)$obVDCrypt->setIdInputValueDecrypt($getTarefaId,$setencrypt_forminputhidden);

        $getProcessoTipoTag = JFactory::getApplication()->input->get('tipoprocesso','','STRING');
        if((int) $getProcessoId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefaDetailByProcesso4Manager ($getProcessoId, $getTarefaId, $getProcessoTipoTag);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getTarefaAlterarEstado4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();
        $getTarefaId        = JFactory::getApplication()->input->getInt('tarefa_id');
        if((int) $getTarefaId<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefaDetail4Manager ($getTarefaId);

        if(empty($data)) exit();

        $data = $data['trf'];

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    /* obter as tarefas que não estão concluídas nem estão anuladas para o utilizador ATUAL da SESSÃO*/
   /* public function getTarefasListaAbertasByUserSession4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Tarefa', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteTarefaHelper::getTarefasListaAbertasByUserSession4Manager();

        if(empty($data))  $data = array('data'=>array());

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }*/

}