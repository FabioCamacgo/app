<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
    JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
    jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerDocumentos extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Function that allows child controller access to model data after the data has been saved.
     * @param   JModelLegacy  $model      The data model object.
     * @param   array         $validData  The validated data.
     * @return  void
     * @since   3.1
     */
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {

    }


    public function download()
    {
        return(true);
    }


    public function createNivel1Cat4admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel1FormData4Admin();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel1cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin';
        $sessDataState         = 'com_virtualdesk.addnewnivel1cat4admin.documentos.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel1BeforeSave4Admin($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createCatNivel14Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function createNivel2Cat4admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel2FormData4Admin();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel2cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin';
        $sessDataState         = 'com_virtualdesk.addnewnivel1cat4admin.documentos.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel2BeforeSave4Admin($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createCatNivel24Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function createNivel3Cat4admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel3FormData4Admin();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel3cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin';
        $sessDataState         = 'com_virtualdesk.addnewnivel1cat4admin.documentos.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel3BeforeSave4Admin($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createCatNivel34Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function createNivel4Cat4admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnewcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel4FormData4Admin();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel3cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin';
        $sessDataState         = 'com_virtualdesk.addnewnivel1cat4admin.documentos.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel4BeforeSave4Admin($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createCatNivel44Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function getCategoriasNivel1List4AdminByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteDocumentosHelper::getCategoriasNivel1List4Admin(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getCategoriasNivel2List4AdminByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteDocumentosHelper::getCategoriasNivel2List4Admin(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getCategoriasNivel3List4AdminByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteDocumentosHelper::getCategoriasNivel3List4Admin(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getCategoriasNivel4List4AdminByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteDocumentosHelper::getCategoriasNivel4List4Admin(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function updateCatLevel14admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel1FormData4Admin();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=documentos&layout=editnivel1cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['categoria_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editnivel1cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndDataCatNivel14Admin($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel1cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel1BeforeSave4Admin($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel1cat4admin.documentos.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedCatNivel1FormData4Admin($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editnivel1cat4admin.documentos.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveCatNivel14Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel1cat4admin.documentos.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function updateCatLevel24admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel2FormData4Admin();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=documentos&layout=editnivel2cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['categoria_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editnivel2cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndDataCatNivel24Admin($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel2cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel2BeforeSave4Admin($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel2cat4admin.documentos.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedCatNivel2FormData4Admin($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editnivel2cat4admin.documentos.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveCatNivel24Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel2cat4admin.documentos.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function updateCatLevel34admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel3FormData4Admin();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=documentos&layout=editnivel3cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['categoria_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editnivel3cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndDataCatNivel34Admin($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel3cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel3BeforeSave4Admin($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel3cat4admin.documentos.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedCatNivel3FormData4Admin($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editnivel3cat4admin.documentos.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveCatNivel34Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel3cat4admin.documentos.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function updateCatLevel44admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'editcat4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedCatNivel4FormData4Admin();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=documentos&layout=editnivel4cat4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=listcat4admin&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['categoria_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editnivel4cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndDataCatNivel44Admin($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel4cat4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCatNivel4BeforeSave4Admin($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel4cat4admin.documentos.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedCatNivel4FormData4Admin($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editnivel4cat4admin.documentos.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveCatNivel44Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.editnivel4cat4admin.documentos.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function create4admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnew4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Admin();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=documentos&layout=addnew4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=list4admin';
        $sessDataState         = 'com_virtualdesk.addnew4admin.documentos.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Admin($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function create4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnew4manager'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=documentos&layout=addnew4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=list4manager';
        $sessDataState         = 'com_virtualdesk.addnew4admin.documentos.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


    public function getDocumentosList4AdminByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteDocumentosHelper::getDocumentosList4Admin(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getDocumentosList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteDocumentosHelper::getDocumentosList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function update4admin()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('documentos');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'edit4admin'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('documentos', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Admin();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=documentos&layout=edit4admin';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=documentos&layout=list4admin&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['documento_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edit4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Admin($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4admin.documentos.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Admin($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4admin.documentos.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData4Admin($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit4admin.documentos.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->save4Admin($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4admin.documentos.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }


}