<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerPercursosPedestres extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Function that allows child controller access to model data after the data has been saved.
     * @param   JModelLegacy  $model      The data model object.
     * @param   array         $validData  The validated data.
     * @return  void
     * @since   3.1
     */
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {

    }


    public function download()
    {
        return(true);
    }


    public function getPercursosPedestresList4UserByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresList4User(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresMMGList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresMMGList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListTipologias4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListTipologias4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListDificuldade4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListDificuldade4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListTerrenos4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListTerrenos4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListDuracao4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListDuracao4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListVertigens4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListVertigens4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListZonamento4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListZonamento4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListSentido4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListSentido4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListPaisagem4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListPaisagem4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListDistancia4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListDistancia4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getPercursosPedestresListAltitude4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresListAltitude4ManagerByAjax(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getNewEstadoName4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputPercursosPedestres_id   = JFactory::getApplication()->input->getInt('percursospedestres_id');

        if((int) $getInputPercursosPedestres_id<=0 ) exit();

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

        $resObj = VirtualDeskSitePercursosPedestresHelper::getPercursosPedestresEstadoAtualObjectByIdPercursosPedestres ($lang, $getInputPercursosPedestres_id);

        if($resObj===false) exit();

        $data=$resObj;

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function sendAlterar2NewEstado4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputPercursosPedestres_id   = JFactory::getApplication()->input->getInt('percursospedestres_id');
        $NewEstadoId         = JFactory::getApplication()->input->get('setNewEstadoId');
        $NewEstadoDesc       = JFactory::getApplication()->input->get('setNewEstadoDesc','','STRING');

        if((int) $getInputPercursosPedestres_id<=0 ) exit();
        if((int) $NewEstadoId<=0 ) exit();

        $res = VirtualDeskSitePercursosPedestresHelper::saveAlterar2NewEstado4ManagerByAjax($getInputPercursosPedestres_id, $NewEstadoId, $NewEstadoDesc);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Estado_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function create4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnew4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnew4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres';
        $sessDataState         = 'com_virtualdesk.addnew4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function update4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edit4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=edit4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=view4manager&vdcleanstate=1&percursospedestres_id='.$data['percursospedestres_id'];

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['percursospedestres_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edit4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->save4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function delete4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createTipologia4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewtipologia4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedTipologiaFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewtipologia4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=tipologia4manager';
        $sessDataState         = 'com_virtualdesk.addnewtipologia4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateTipologiaBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createTipologia4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createDificuldade4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewdificuldade4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedDificuldadeFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewdificuldade4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=dificuldade4manager';
        $sessDataState         = 'com_virtualdesk.addnewdificuldade4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateDificuldadeBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createDificuldade4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createTerreno4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewterreno4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedTerrenoFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewterreno4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=terreno4manager';
        $sessDataState         = 'com_virtualdesk.addnewterreno4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateTerrenoBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createTerreno4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createDuracao4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewduracao4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedDuracaoFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewduracao4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=duracao4manager';
        $sessDataState         = 'com_virtualdesk.addnewduracao4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateDuracaoBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createDuracao4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createVertigens4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewvertigens4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedVertigensFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewvertigens4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=vertigens4manager';
        $sessDataState         = 'com_virtualdesk.addnewvertigens4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateVertigensBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createVertigens4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createZonamento4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewzonamento4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedZonamentoFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewzonamento4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=zonamento4manager';
        $sessDataState         = 'com_virtualdesk.addnewzonamento4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateZonamentoBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createZonamento4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createSentido4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewsentido4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedSentidoFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewsentido4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=sentido4manager';
        $sessDataState         = 'com_virtualdesk.addnewsentido4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateSentidoBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createSentido4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createPaisagem4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewpaisagem4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedPaisagemFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewpaisagem4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=paisagem4manager';
        $sessDataState         = 'com_virtualdesk.addnewpaisagem4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validatePaisagemBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createPaisagem4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createDistancia4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewdistancia4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedDistanciaFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewdistancia4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=distancia4manager';
        $sessDataState         = 'com_virtualdesk.addnewdistancia4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateDistanciaBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createDistancia4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function createAltitude4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');                  // verifica permissão de update
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnewaltitude4managers'); // verifica permissão acesso ao layout para editar
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }


        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedAltitudeFormData4Manager();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=addnewaltitude4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=altitude4manager';
        $sessDataState         = 'com_virtualdesk.addnewaltitude4manager.percursospedestres.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateAltitudeBeforeSave4Manager($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createAltitude4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatetipologia4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edittipologia4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedTipologiaFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=edittipologia4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=tipologia4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['tipologia_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.edittipologia4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edittipologia4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateTipologiaBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->attachment = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edittipologia4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedTipologiaFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edittipologia4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveTipologia4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.edittipologia4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updateterreno4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editterreno4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedTerrenoFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editterreno4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=terreno4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['terreno_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editterreno4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editterreno4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateTerrenoBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editterreno4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedTerrenoFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editterreno4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveTerreno4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editterreno4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatevertigens4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editvertigens4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedVertigensFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editvertigens4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=vertigens4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['vertigens_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editvertigens4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editvertigens4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateVertigensBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editvertigens4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedVertigensFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editvertigens4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveVertigens4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editvertigens4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatezonamento4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editzonamento4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedZonamentoFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editzonamento4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=zonamento4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['zonamento_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editzonamento4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editzonamento4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateZonamentoBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editzonamento4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedZonamentoFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editzonamento4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveZonamento4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editzonamento4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatesentido4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editsentido4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedSentidoFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editsentido4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=sentido4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['sentido_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editsentido4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editsentido4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateSentidoBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editsentido4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedSentidoFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editsentido4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveSentido4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editsentido4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatepaisagem4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editpaisagem4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedPaisagemFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editpaisagem4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=paisagem4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['paisagem_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editpaisagem4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editpaisagem4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validatePaisagemBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editpaisagem4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedPaisagemFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editpaisagem4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->savePaisagem4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editpaisagem4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatedificuldade4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdificuldade4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedDificuldadeFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editdificuldade4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=dificuldade4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['dificuldade_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editdificuldade4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editdificuldade4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateDificuldadeBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editdificuldade4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedDificuldadeFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editdificuldade4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveDificuldade4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editdificuldade4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updateduracao4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editduracao4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedDuracaoFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editduracao4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=duracao4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['duracao_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editduracao4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editduracao4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateDuracaoBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editduracao4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedDuracaoFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editduracao4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveDuracao4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editduracao4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatedistancia4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editdistancia4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedDistanciaFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editdistancia4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=distancia4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['distancia_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editdistancia4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editdistancia4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateDistanciaBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editdistancia4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedDistanciaFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editdistancia4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveDistancia4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editdistancia4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


    public function updatealtitude4manager()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
        $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editaltitude4managers');
        $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager();
        if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('PercursosPedestres', 'VirtualDeskModel');
        $data   = $model->getPostedAltitudeFormData4Manager();
        $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=editaltitude4manager';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=percursospedestres&layout=altitude4manager&vdcleanstate=1';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Verifica se foi enviado o id
        if ((int) $data['altitude_id'] <=0 ) {
            $app->setUserState('com_virtualdesk.editaltitude4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }


        // Carrega os dados  para depois poder comparar
        if(!$model->setUserIdAndData4Manager($data) ) {
            $app->setUserState('com_virtualdesk.editaltitude4manager.percursospedestres.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateAltitudeBeforeSave4Manager($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $app->setUserState('com_virtualdesk.editaltitude4manager.percursospedestres.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedAltitudeFormData4Manager($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.editaltitude4manager.percursospedestres.data', null);
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
            return true;
        }

        // Attempt to save the data.
        $return = $model->saveAltitude4Manager($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $app->setUserState('com_virtualdesk.editaltitude4manager.percursospedestres.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


        // Flush the data from the session.
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }


}