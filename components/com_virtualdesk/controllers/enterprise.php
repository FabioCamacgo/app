<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// //JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');
JLoader::register('VirtualDeskSiteEnterpriseHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_enterprise.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for Enterprises.
 * @since  1.6
 */
//class VirtualDeskControllerEnterprise extends JControllerForm
class VirtualDeskControllerEnterprise extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }



    public function cancel()
    {
        $app    = JFactory::getApplication();
        // Flush the data from the session.
        $app->setUserState('com_virtualdesk.addnew.enterprise.data', null);
        $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
        $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise', false));
        return true;
    }


    public function addnew()
    {
        JFactory::getApplication()->input->set('layout','addnew');
        parent::display();
        return true;
    }

	/**
	 * Method to check out a user for editing and redirect to the edit form.
	 * @return  boolean
	 * @since   1.6
	 */
	public function edit()
	{
        $IdEnterprise = $this->input->getInt('enterprise_id');

        if((int)$IdEnterprise > 0)
        {
            // Redirect to the edit screen.
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=edit&enterprise_id=' . $IdEnterprise, false));
        }
        else
        { $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise', false)); }
		return true;
	}


    /**
     * Method to save a enterprise data.
     * @return  mixed
     * @since   1.6
     */
    public function create()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $app    = JFactory::getApplication();
        $model  = $this->getModel('Enterprise', 'VirtualDeskModel');

        $data = $model->getPostedFormData();

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->enterpriseavatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.addnew.enterprise.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=addnew', false));
            return false;
        }



        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.addnew.enterprise.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=addnew', false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->create($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.addnew.enterprise.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=addnew', false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $redirect = 'index.php?option=com_virtualdesk&view=enterprise&layout=default' ;
        $this->setMessage(JText::_('COM_VIRTUALDESK_ENTERPRISE_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirect, false));


        // Flush the data from the session.
        $app->setUserState('com_virtualdesk.addnew.enterprise.data', null);
    }



	/**
	 * Method to UPDATE a enterprise data.
	 * @return  mixed
	 * @since   1.6
	 */
	public function update()
	{
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $app    = JFactory::getApplication();
        $model  = $this->getModel('Enterprise', 'VirtualDeskModel');
        $data   = $model->getPostedFormData();

        // Verifica se foi enviado o id de enterprise
        if ((int) $data['enterprise_id'] <=0 ) {
            $data->enterpriseavatar = null;  // para não ficar com array de file na sessão
            $data->attachment       = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise', false));
            return false;
        }


        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();
        if ($returnUserSession === false ) {
            // Save the data in the session.
            $data->enterpriseavatar = null;  // para não ficar com array de file na sessão
            $data->attachment       = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.enterprise.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=edit', false));
            return false;
        }

        // Carrega os dados da "enterprise" para depois poder comparar
        if(!$model->setUserIdAndEnterpriseData($data) ) {
            $data->enterpriseavatar = null;  // para não ficar com array de file na sessão
            $data->attachment       = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
            $this->setMessage(JText::_('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED'), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise', false));
            return false;
        }


        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
            // Redirect to the list screen.
            $redirect = 'index.php?option=com_virtualdesk&view=enterprise' ;
            $this->setMessage(JText::_('COM_VIRTUALDESK_PROFILE_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirect, false));
            return true;
        }


        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->enterpriseavatar = null;  // para não ficar com array de file na sessão
            $data->attachment       = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.enterprise.data', $data);
            // Redirect back to the addnew screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=edit', false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->save($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->enterpriseavatar = null;  // para não ficar com array de file na sessão
            $data->attachment       = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.enterprise.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_ENTERPRISE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=edit', false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        $redirect = 'index.php?option=com_virtualdesk&view=enterprise&layout=default' ;
        $this->setMessage(JText::_('COM_VIRTUALDESK_ENTERPRISE_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirect, false));


        // Flush the data from the session.
        $app->setUserState('com_virtualdesk.edit.enterprise.data', null);
	}


	/**
	 * Function that allows child controller access to model data after the data has been saved.	    
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.	    
	 * @return  void	    
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        $EnterpriseId = $this->input->getInt('enterprise_id');
        $basename     = $this->input->get('bname');

        $UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();

        // verifica se o ficheiro e o Id correspondem ao utilizador da sessão atual
        // a partir do ID do contactus e do filename pesquisa o path do ficheiro
        $objFile = VirtualDeskSiteEnterpriseHelper::getEnterpriseFileNameById ($UserJoomlaID, $EnterpriseId, $basename);

        if (empty($objFile)) {
            // Redirect back to the addnew screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_ENTERPRISE_DOWNLOAD_FAILED'), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=addnew', false));
            return false;
        }
        else {

            VirtualDeskSiteFileUploadHelper::setDownloadHeaders ($objFile);
            exit;
        }

    }


}
