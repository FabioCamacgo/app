<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');

/**
 * Profile controller class for Users.
 *
 * @since  1.6
 */
class VirtualDeskControllerProfile extends JControllerForm
{

	/**
	 * Method to check out a user for editing and redirect to the edit form.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	public function edit()
	{
		$app         = JFactory::getApplication();
		$user        = JFactory::getUser();
		$loginUserId = (int) $user->get('id');
		$userId      = $this->input->getInt('user_id', null, 'array');

		// Check if the user is trying to edit another users profile.
		if ($userId != $loginUserId)
        {
            $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
			$app->setHeader('status', 403, true);
			return false;
		}

		$cookieLogin = $user->get('cookieLogin');

		// Check if the user logged in with a cookie
		if (!empty($cookieLogin))
		{   // If so, the user must login to edit the password and other data.
			$app->enqueueMessage(JText::_('JGLOBAL_REMEMBER_MUST_LOGIN'), 'message');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
			return false;
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit', false));

		return true;
	}


	/**
	 * Method to save a user's profile data.
	 *
	 * @return  mixed
	 *
	 * @since   1.6
	 */
	public function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$app    = JFactory::getApplication();
		$model  = $this->getModel('Profile', 'VirtualDeskModel');

        $data = $model->getPostedFormData();

        // Carrega e inicializa o user session Id e os dados da BD
        $returnUserSession = $model->setUserIdAndProfileData();
        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.profile.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit', false));
            return false;
        }

        // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
        // Se não for para alterar nada sai...
        $data = $model->getOnlyChangedPostedFormData($data);
        if( !is_array($data) || sizeof($data)<=0) {
            // Flush the data from the session.
            $app->setUserState('com_virtualdesk.edit.profile.data', null);
            // Redirect to the list screen.
            $redirect = 'index.php?option=com_virtualdesk&view=profile&layout=default' ;
            $this->setMessage(JText::_('COM_VIRTUALDESK_PROFILE_NO_CHANGES'));
            $this->setRedirect(JRoute::_($redirect, false));
            return true;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateBeforeSave($data);
        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState('com_virtualdesk.edit.profile.data', $data);
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit', false));
            return false;
        }

		// Attempt to save the data.
		$return = $model->save($data);

		// Check for errors.
		if ($return === false)
		{   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
			$app->setUserState('com_virtualdesk.edit.profile.data', $data);
			// Redirect back to the edit screen.
			$this->setMessage(JText::sprintf('COM_VIRTUALDESK_PROFILE_SAVE_FAILED', $model->getError()), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit', false));
			return false;
		}

        // Verifica se ocorreu alteração do email e nesse caso pode ter ocorrido uma nova validação do user. A conta foi bloqueada e por isso o user tem de sair
        if($model->forcedLogout===true)
        {   // Perform the log out.
            $app->logout();
            $redirect = JUri::root().'updateduserprofile/';
            $app->redirect(JRoute::_($redirect, false));
            return false;
            exit();
        }
        else
        {   // Correu tudo Ok ...
            // Redirect to the list screen.
            $redirect = 'index.php?option=com_virtualdesk&view=profile&layout=default' ;
            $this->setMessage(JText::_('COM_VIRTUALDESK_PROFILE_SAVE_SUCCESS'),'success');
            $this->setRedirect(JRoute::_($redirect, false));
        }

		// Flush the data from the session.
		$app->setUserState('com_virtualdesk.edit.profile.data', null);
	}


	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{


	}

    public function getconcelhoajax()
    {

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, concelho as name'))
            ->from("#__virtualdesk_digitalGov_concelhos")
            ->order('concelho ASC' )
        );
        $data = $db->loadRowList();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');

        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit();
    }



    public function getfreguesiaajax()
    {
        $app         = JFactory::getApplication();
        $idconcelho       = $app->input->get('idconcelho',null, 'STRING');

        if(empty($idconcelho)) {
            echo "";
            exit();
        }

        // Verifica se o email nos JosUsers
        $db = JFactory::getDBO();
        $db->setQuery($db->getQuery(true)
            ->select(array('id, freguesia as name'))
            ->from("#__virtualdesk_digitalGov_freguesias")
            ->where($db->quoteName('concelho_id').'='.$db->escape($idconcelho).'' )
            ->order('freguesia ASC' )
        );
        $data = $db->loadAssocList();

        if(empty($data)) exit();

        $app = JFactory::getApplication();
        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


}
