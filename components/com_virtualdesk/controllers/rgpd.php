<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// //JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
//JLoader::register('VirtualDeskBackOfficeAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdeskBackOffice_alerta.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteRgpdHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_rgpd.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerRgpd extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 * @return  void
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        return(true);
    }


    /* carrega a lista de LOGS do RGPD para serem utiliadas por MANAGERS */
    public function getRgpdListLogs4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Rgpd', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        ##$getProcessoId  = JFactory::getApplication()->input->get($NameProcessoId);
        ##$getProcessoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getProcessoId,$setencrypt_forminputhidden);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteRgpdHelper::getRgpdListLogs4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    /* carrega a lista de DADOS PESSOAIS existentes nos vários módulo da APP p-> acesso a MANAGERS */
    public function getRgpdListDados4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Rgpd', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        ##$getProcessoId  = JFactory::getApplication()->input->get($NameProcessoId);
        ##$getProcessoId  = (int)$obVDCrypt->setIdInputValueDecrypt($getProcessoId,$setencrypt_forminputhidden);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteRgpdHelper::getRgpdListDados4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }


    public function getRgpdLogDetalhe4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Rgpd', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getRgpdId        = JFactory::getApplication()->input->getInt('rgpd_id');

        if((int) $getRgpdId<=0 ) {
            $data = array('data'=>array());
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();

        }

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteRgpdHelper::getRgpdLogDetalhe4Manager ( $getRgpdId);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

}