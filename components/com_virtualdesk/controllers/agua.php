<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
JLoader::register('VirtualDeskSiteAguaBackofficeHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/Agua/virtualdesksite_agua.php');
jimport('joomla.application.component.controller');

/**
 * Profile controller class for s.
 * @since  1.6
 */
class VirtualDeskControllerAgua extends JControllerLegacy
{

    function __construct()
    {
        parent::__construct();
    }

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 * @return  void
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{

	}


    public function download()
    {
        return(true);
    }


    public function getLeiturasList4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAguaBackofficeHelper::getLeiturasList4Manager(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    public function getNewEstadoName4ManagerByAjax()
    {

        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getLeitura_id   = JFactory::getApplication()->input->getInt('leitura');

        if((int) $getLeitura_id<=0 ) exit();

        $lang = VirtualDeskSiteGeneralHelper::getCurrentLanguageTag();

        $resObj = VirtualDeskSiteAguaBackofficeHelper::getLeituraEstadoAtualObjectById ($lang, $getLeitura_id);

        if($resObj===false) exit();

        $data=$resObj;

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    public function sendAlterar2NewEstado4ManagerByAjax()
    {
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('Agenda', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        $getInputLeitura_id   = JFactory::getApplication()->input->getInt('leitura');
        $NewEstadoId         = JFactory::getApplication()->input->get('setNewEstadoId');
        $NewEstadoDesc       = JFactory::getApplication()->input->get('setNewEstadoDesc','','STRING');

        if((int) $getInputLeitura_id<=0 ) exit();
        if((int) $NewEstadoId<=0 ) exit();

        $res = VirtualDeskSiteAguaBackofficeHelper::saveAlterar2NewEstado4ManagerByAjax($getInputLeitura_id, $NewEstadoId, $NewEstadoDesc);

        if($res===false) {
            header("HTTP/1.0 500");
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data['success'] = False;
            $data['message'] = JText::_( 'COM_VIRTUALDESK_ALERTA_OCORREUERRO' ); ;
        }
        else {
            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $data='New_Estado_OK';
        }

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    public function getPedidosList4UserByAjax(){
        // check session +  joomla user
        $app    = JFactory::getApplication();
        $model  = $this->getModel('acaosocial', 'VirtualDeskModel');
        $returnUserSession = $model->setUserIdData();
        $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

        if((int) $userSessionID<=0 ) exit();

        // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
        $data = VirtualDeskSiteAguaBackofficeHelper::getPedidosList4User(true);

        if(empty($data)) exit();

        $app->setHeader('Content-Type', 'application/json; charset=utf-8');
        $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $dataFinal;
        exit();
    }

    public function createvercontador4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontador';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontador';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateVerificacaoContadorBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createVerificacaoContador4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createalteracaolocalcontador4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontador';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontador';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateAlteracaoLocalContadorBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createAlteracaoLocalContador4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createsubstituicaocontador4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontador';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontador';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateSubstituicaoContadorBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createSubstituicaoContador4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createalteracaotitularcontador4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateAlteracaoTitularContadorBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createAlteracaoTitularContador4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Correu tudo Ok ...
        // Redirect to the list screen.
        // Redirect to the list screen.
        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createcancelamentocontrato4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateCancelamentoContratoBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createCancelamentoContrato4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createcontratoprestacaoservico4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateContratoPrestacaoServicosBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createContratoPrestacaoServicos4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createrestabelecimentoagua4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateRestabelecimentoAguaBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createRestabelecimentoAgua4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createfaturaeletronica4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelfaturacao';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelfaturacao';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateFaturaEletronicaBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createFaturaEletronica4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createdebitodireto4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelfaturacao';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelfaturacao';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateDebitoDiretoBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createDebitoDireto4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createtarifarioespecial4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelfaturacao';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelfaturacao';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateTarifarioEspecialBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createTarifarioEspecial4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createramalabastecimentoagua4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelramal';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelramal';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateRamalAbastecimentoAguaBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createRamalAbastecimentoAgua4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }

    public function createramalaguasresiduais4user()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /*
        * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
        */
        $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agua');                  // verifica permissão de update
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $app    = JFactory::getApplication();
        $model  = $this->getModel('agua', 'VirtualDeskModel');
        $data   = $model->getPostedFormData4User();
        $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelramal';
        $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=agua&layout=menu3nivelramal';
        $sessDataState         = 'com_virtualdesk.addnew4user.agua.data';

        $obVDCrypt = new VirtualDeskSiteCryptHelper();
        $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
        if($setencrypt_forminputhidden==1) {
            $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
            $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
        }

        // Carrega o user session Id -> coloca no atributo do objecto atual
        $returnUserSession = $model->setUserIdData();

        if ($returnUserSession === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Valida os dados antes da gravação
        $returnValid = $model->validateRamalAguasResiduaisBeforeSave4User($data);

        // Check for errors.
        if ($returnValid === false) {
            // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        // Attempt to save the data.
        $return = $model->createRamalAguasResiduais4User($data);

        // Check for errors.
        if ($return === false)
        {   // Save the data in the session.
            $data->useravatar = null;  // para não ficar com array de file na sessão
            $app->setUserState($sessDataState, $data);
            $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
            return false;
        }

        $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
        $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

        // Flush the data from the session.
        VirtualDeskSiteAguaBackofficeHelper::cleanAllTmpUserState();
    }


}