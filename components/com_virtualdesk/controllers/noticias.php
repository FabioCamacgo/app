<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_virtualdesk
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteFileUploadHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_fileupload.php');
    JLoader::register('VirtualDeskSiteNoticiasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias.php');
    jimport('joomla.application.component.controller');

    /**
     * Profile controller class for s.
     * @since  1.6
     */
    class VirtualDeskControllerNoticias extends JControllerLegacy
    {

        function __construct()
        {
            parent::__construct();
        }

        /**
         * Function that allows child controller access to model data after the data has been saved.
         * @param   JModelLegacy  $model      The data model object.
         * @param   array         $validData  The validated data.
         * @return  void
         * @since   3.1
         */
        protected function postSaveHook(JModelLegacy $model, $validData = array())
        {

        }


        public function download()
        {
            return(true);
        }


        public function createCat4Manager()
        {
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            /*
            * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'addnewcat4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $data   = $model->getPostedCatFormData4Manager();
            $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=noticias&layout=addnewcat4managers';
            $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=noticias&layout=listcat4managers';
            $sessDataState         = 'com_virtualdesk.addnewcat4manager.noticias.data';

            $obVDCrypt = new VirtualDeskSiteCryptHelper();
            $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
            if($setencrypt_forminputhidden==1) {
                $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
                $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
            }

            // Carrega o user session Id -> coloca no atributo do objecto atual
            $returnUserSession = $model->setUserIdData();

            if ($returnUserSession === false) {
                // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Valida os dados antes da gravação
            $returnValid = $model->validateCatBeforeSave4Manager($data);

            // Check for errors.
            if ($returnValid === false) {
                // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Attempt to save the data.
            $return = $model->createCat4Manager($data);

            // Check for errors.
            if ($return === false)
            {   // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Correu tudo Ok ...
            // Redirect to the list screen.
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

            // Flush the data from the session.
            VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        }


        public function getCategoriasList4ManagerByAjax()
        {

            // check session +  joomla user
            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $returnUserSession = $model->setUserIdData();
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

            if((int) $userSessionID<=0 ) exit();

            // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
            $data = VirtualDeskSiteNoticiasHelper::getCategoriasList4Manager(true);

            if(empty($data)) exit();

            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();
        }


        public function updateCat4manager()
        {
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            /*
            * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'editcat4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $data   = $model->getPostedCatFormData4Manager();
            $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=noticias&layout=editcat4manager';
            $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=noticias&layout=editcat4manager&vdcleanstate=1&categoria_id='.$data['categoria_id'];

            $obVDCrypt = new VirtualDeskSiteCryptHelper();
            $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
            if($setencrypt_forminputhidden==1) {
                $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
                $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
            }

            // Verifica se foi enviado o id
            if ((int) $data['categoria_id'] <=0 ) {
                $app->setUserState('com_virtualdesk.editcat4manager.noticias.data', null);
                $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
                return false;
            }


            // Carrega os dados  para depois poder comparar
            if(!$model->setUserIdAndDataCat4Manager($data) ) {
                $data->attachment = null;  // para não ficar com array de file na sessão
                $app->setUserState('com_virtualdesk.editcat4manager.noticias.data', null);
                $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
                return false;
            }

            // Valida os dados antes da gravação
            $returnValid = $model->validateCatBeforeSave4Manager($data);
            // Check for errors.
            if ($returnValid === false) {
                // Save the data in the session.
                $data->attachment = null;  // para não ficar com array de file na sessão
                $app->setUserState('com_virtualdesk.editcat4manager.noticias.data', $data);
                // Redirect back to the addnew screen.
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
                return false;
            }

            // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
            // Se não for para alterar nada sai...
            $data = $model->getOnlyChangedPostedCatFormData4Manager($data);
            if( !is_array($data) || sizeof($data)<=0) {
                // Flush the data from the session.
                $app->setUserState('com_virtualdesk.editcat4manager.noticias.data', null);
                // Redirect to the list screen.
                $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
                $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
                return true;
            }

            // Attempt to save the data.
            $return = $model->saveCat4Manager($data);

            // Check for errors.
            if ($return === false)
            {   // Save the data in the session.
                $data->attachment = null;  // para não ficar com array de file na sessão
                $app->setUserState('com_virtualdesk.editcat4manager.noticias.data', $data);
                // Redirect back to the edit screen.
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
                return false;
            }

            // Correu tudo Ok ...
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


            // Flush the data from the session.
            VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        }


        public function create4Manager()
        {
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            /*
            * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'addnew4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $data   = $model->getPostedFormData4Manager();
            $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=noticias&layout=list4manager&vdcleanstate=1';
            $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=noticias&layout=list4manager&vdcleanstate=1';
            $sessDataState         = 'com_virtualdesk.addnew4manager.noticias.data';

            $obVDCrypt = new VirtualDeskSiteCryptHelper();
            $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
            if($setencrypt_forminputhidden==1) {
                $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
                $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
            }

            // Carrega o user session Id -> coloca no atributo do objecto atual
            $returnUserSession = $model->setUserIdData();

            if ($returnUserSession === false) {
                // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Valida os dados antes da gravação
            $returnValid = $model->validateBeforeSave4Manager($data);

            // Check for errors.
            if ($returnValid === false) {
                // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Attempt to save the data.
            $return = $model->create4Manager($data);

            // Check for errors.
            if ($return === false)
            {   // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Correu tudo Ok ...
            // Redirect to the list screen.
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

            // Flush the data from the session.
            VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        }


        public function getNoticiasList4ManagerByAjax()
        {

            // check session +  joomla user
            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $returnUserSession = $model->setUserIdData();
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

            if((int) $userSessionID<=0 ) exit();

            // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
            $data = VirtualDeskSiteNoticiasHelper::getNoticiasList4Manager(true);

            if(empty($data)) exit();

            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();
        }


        public function getNoticiasList4GestorByAjax()
        {

            // check session +  joomla user
            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $returnUserSession = $model->setUserIdData();
            $userSessionID = VirtualDeskSiteUserHelper::getUserSessionId($returnUserSession);

            if((int) $userSessionID<=0 ) exit();

            // Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a esta lista no método de carregamento
            $data = VirtualDeskSiteNoticiasHelper::getNoticiasList4Gestor(true);

            if(empty($data)) exit();

            $app->setHeader('Content-Type', 'application/json; charset=utf-8');
            $dataFinal = json_encode($data, JSON_UNESCAPED_UNICODE);
            echo $dataFinal;
            exit();
        }


        public function update4manager()
        {
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            /*
            * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'edit4managers'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $data   = $model->getPostedFormData4Manager();
            $redirectLayoutEdit = 'index.php?option=com_virtualdesk&view=noticias&layout=view4manager';
            $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=noticias&layout=view4manager&vdcleanstate=1&noticia_id='.$data['noticia_id'];

            $obVDCrypt = new VirtualDeskSiteCryptHelper();
            $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
            if($setencrypt_forminputhidden==1) {
                $redirectLayoutEdit = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutEdit, true);
                $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
            }

            // Verifica se foi enviado o id
            if ((int) $data['noticia_id'] <=0 ) {
                $app->setUserState('com_virtualdesk.edit4manager.noticias.data', null);
                $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
                return false;
            }


            // Carrega os dados  para depois poder comparar
            if(!$model->setUserIdAndData4Manager($data) ) {
                $data->attachment = null;  // para não ficar com array de file na sessão
                $app->setUserState('com_virtualdesk.edit4manager.noticias.data', null);
                $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED'), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutDefault , false));
                return false;
            }

            // Valida os dados antes da gravação
            $returnValid = $model->validateBeforeSave4Manager($data);
            // Check for errors.
            if ($returnValid === false) {
                // Save the data in the session.
                $data->attachment = null;  // para não ficar com array de file na sessão
                $app->setUserState('com_virtualdesk.edit4manager.noticias.data', $data);
                // Redirect back to the addnew screen.
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutEdit, false));
                return false;
            }

            // Verifica os dados existente na base de dados com o "posted" no $data para gravar o que for diferente do existente
            // Se não for para alterar nada sai...
            $data = $model->getOnlyChangedPostedFormData4Manager($data);
            if( !is_array($data) || sizeof($data)<=0) {
                // Flush the data from the session.
                $app->setUserState('com_virtualdesk.edit4manager.noticias.data', null);
                // Redirect to the list screen.
                $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_NO_CHANGES'));
                $this->setRedirect(JRoute::_($redirectLayoutDefault, false));
                return true;
            }

            // Attempt to save the data.
            $return = $model->save4Manager($data);

            // Check for errors.
            if ($return === false)
            {   // Save the data in the session.
                $data->attachment = null;  // para não ficar com array de file na sessão
                $app->setUserState('com_virtualdesk.edit4manager.noticias.data', $data);
                // Redirect back to the edit screen.
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutEdit , false));
                return false;
            }

            // Correu tudo Ok ...
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));


            // Flush the data from the session.
            VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        }


        public function create4Gestor()
        {
            // Check for request forgeries.
            JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

            /*
            * Check Permissões - no caso do ConfigAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
            */
            $objCheckPerm   = new VirtualDeskSitePermissionsHelper();
            $objCheckPerm->loadPermission();
            $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('noticias');                  // verifica permissão de update
            $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'addnew4gestores'); // verifica permissão acesso ao layout para editar
            $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
            if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
                JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
                return false;
            }


            $app    = JFactory::getApplication();
            $model  = $this->getModel('noticias', 'VirtualDeskModel');
            $data   = $model->getPostedFormData4Gestor();
            $redirectLayoutAddNew  = 'index.php?option=com_virtualdesk&view=noticias&layout=list4gestor&vdcleanstate=1';
            $redirectLayoutDefault = 'index.php?option=com_virtualdesk&view=noticias&layout=list4gestor&vdcleanstate=1';
            $sessDataState         = 'com_virtualdesk.addnew4gestor.noticias.data';

            $obVDCrypt = new VirtualDeskSiteCryptHelper();
            $setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');
            if($setencrypt_forminputhidden==1) {
                $redirectLayoutAddNew = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutAddNew, true);
                $redirectLayoutDefault = $obVDCrypt->setUrlQueryStringEncryptedForOneLink($redirectLayoutDefault, true);
            }

            // Carrega o user session Id -> coloca no atributo do objecto atual
            $returnUserSession = $model->setUserIdData();

            if ($returnUserSession === false) {
                // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Valida os dados antes da gravação
            $returnValid = $model->validateBeforeSave4Gestor($data);

            // Check for errors.
            if ($returnValid === false) {
                // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Attempt to save the data.
            $return = $model->create4Gestor($data);

            // Check for errors.
            if ($return === false)
            {   // Save the data in the session.
                $data->useravatar = null;  // para não ficar com array de file na sessão
                $app->setUserState($sessDataState, $data);
                $this->setMessage(JText::sprintf('COM_VIRTUALDESK_PERMADMIN_SAVE_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_($redirectLayoutAddNew , false));
                return false;
            }

            // Correu tudo Ok ...
            // Redirect to the list screen.
            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_VIRTUALDESK_PERMADMIN_SAVE_SUCCESS'),'success');
            $this->setRedirect(JRoute::_($redirectLayoutDefault, false));

            // Flush the data from the session.
            VirtualDeskSiteNoticiasHelper::cleanAllTmpUserState();
        }

    }