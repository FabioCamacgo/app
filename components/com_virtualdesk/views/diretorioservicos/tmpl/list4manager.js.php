<?php

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();
    $IdDistrito = $obParam->getParamsByTag('IdDistritoDiretorioServicos');

    $objEstadoOptions  = VirtualDeskSiteDiretorioServicosHelper::getEstado();
    $objCategoriasOptions  = VirtualDeskSiteDiretorioServicosHelper::getCategoria();
    $objConcelhoOptions  = VirtualDeskSiteDiretorioServicosHelper::getConcelho($IdDistrito);

    $EstadoOptionsHTML = '';
    foreach($objEstadoOptions as $row) {
        $EstadoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['estado'] . '</option>';
    }

    $CategoriasOptionsHTML = '';
    foreach($objCategoriasOptions as $row) {
        $CategoriasOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['categoria'] . '</option>';
    }

    $ConcelhoOptionsHTML = '';
    foreach($objConcelhoOptions as $row) {
        $ConcelhoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['concelho'] . '</option>';
    }
?>

var TableDatatablesManaged = function () {

    var initTableEmpresas = function () {

        var table = jQuery('#tabela_lista_empresas');
        var tableTools = jQuery('#tabela_lista_empresas_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"lf <"EstadoFilterDropBox"><"CategoriasFilterDropBox"><"ConcelhoFilterDropBox"> rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [50, 100, 200, -1],
                [50, 100, 200, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 50,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        if(row[1] == null){
                            return(data);
                        } else {
                            return ('<a href="' + row[6] + '">' + data + '</a>');
                        }
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        if(row[2] == null){
                            return(data);
                        } else {
                            return ('<a href="' + row[6] + '">' + data + '</a>');
                        }
                    }
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        if(row[3] == null){
                            return(data);
                        } else {
                            return ('<a href="' + row[6] + '">' + data + '</a>');
                        }
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        if(row[4] == null){
                            return(data);
                        } else {
                            return ('<a href="' + row[6] + '">' + data + '</a>');
                        }
                    }
                },
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_DIRETORIOSERVICOS_ESTADONAOINICIADO"); ?>';

                        switch (row[11])
                        {   case '1':
                            defCss = '<?php echo VirtualDeskSiteDiretorioServicosHelper::getEstadoCSS(1);?>';
                            break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSiteDiretorioServicosHelper::getEstadoCSS(2);?>';
                                break;
                            case '3':
                                defCss = '<?php echo VirtualDeskSiteDiretorioServicosHelper::getEstadoCSS(3);?>';
                                break;
                            case '4':
                                defCss = '<?php echo VirtualDeskSiteDiretorioServicosHelper::getEstadoCSS(4);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 6,
                    "visible":false,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + row[7] + returnLinkf);
                    }
                },
                {
                    "targets": 7,
                    "visible":false
                },
                {
                    "targets": 8,
                    "visible":false
                },
                {
                    "targets": 9,
                    "visible":false
                },
                {
                    "targets": 10,
                    "visible":false
                },
                {
                    "targets": 11,
                    "visible":false
                },
                {
                    "targets": 12,
                    "visible":false
                }
            ],
            "order": [
                [7, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=diretorioservicos.getDiretorioServicosList4ManagerByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // Filtrar ESTADO
                this.api().column(11).every(function(){
                    var column = this;

                    var SelectText2  = '<label> <?php echo JText::_("COM_VIRTUALDESK_DIRETORIOSERVICOS_FILTRO_ESTADO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText2 += '<?php echo ($EstadoOptionsHTML); ?>';
                    SelectText2 += '</select></label>';

                    var select = jQuery(SelectText2).appendTo(jQuery('.EstadoFilterDropBox').empty() )
                    jQuery('.EstadoFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Categorias
                this.api().column(8).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_DIRETORIOSERVICOS_FILTRO_CATEGORIAS"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($CategoriasOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.CategoriasFilterDropBox').empty() )
                    jQuery('.CategoriasFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });


                // Filtrar Concelho
                this.api().column(10).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_DIRETORIOSERVICOS_FILTRO_CONCELHO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($ConcelhoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.ConcelhoFilterDropBox').empty() )
                    jQuery('.ConcelhoFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_empresas_wrapper');

    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableEmpresas();

        }

    };

}();

jQuery(document).ready(function() {

    TableDatatablesManaged.init();
});