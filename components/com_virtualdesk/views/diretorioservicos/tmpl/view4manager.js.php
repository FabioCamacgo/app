<?php
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();
    $latitudeDefault = $obParam->getParamsByTag('latitudeDefaultDiretorioServicos');
    $LongitudeDefault = $obParam->getParamsByTag('LongitudeDefaultDiretorioServicos');
    $pathDelimitacaoMapa = $obParam->getParamsByTag('pathDelimitacaoMapa');
    $pinMapa = $obParam->getParamsByTag('pinMapaDirServ');

    if(empty($this->data->latitude) || empty($this->data->longitude)) {
        $latitude = $latitudeDefault;
        $longitude = $LongitudeDefault;
    } else {
        $latitude = $this->data->latitude;
        $longitude = $this->data->longitude;
    }
?>

var iconPath = '<?php echo JUri::base() . $pinMapa; ?>';
var LatToSet = '<?php echo $latitude; ?>';
var LongToSet = '<?php echo $longitude; ?>';

var PortofolioHandle = function () {

    var initDiretorioServicosFileGridCapa = function (evt) {
        // init cubeportfolio
        jQuery('#vdDiretorioServicosFileGridCapa').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyDiretorioServicosFileGridCapa = function (evt) {
        // init cubeportfolio
        jQuery('#vdDiretorioServicosFileGridCapa').cubeportfolio('destroy');
    };


    var initDiretorioServicosFileGridGaleria = function (evt) {
        // init cubeportfolio
        jQuery('#vdDiretorioServicosFileGridGaleria').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyDiretorioServicosFileGridGaleria = function (evt) {
        // init cubeportfolio
        jQuery('#vdDiretorioServicosFileGridGaleria').cubeportfolio('destroy');
    };


    var initDiretorioServicosFileGridLogo = function (evt) {
        // init cubeportfolio
        jQuery('#vdDiretorioServicosFileGridLogo').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyDiretorioServicosFileGridLogo = function (evt) {
        // init cubeportfolio
        jQuery('#vdDiretorioServicosFileGridLogo').cubeportfolio('destroy');
    };

    return {
        //main function to initiate the module
        init: function () {
            initDiretorioServicosFileGridCapa();
            initDiretorioServicosFileGridGaleria();
            initDiretorioServicosFileGridLogo();
        },
        initDiretorioServicosFileGridCapa    :  initDiretorioServicosFileGridCapa,
        destroyDiretorioServicosFileGridCapa  : destroyDiretorioServicosFileGridCapa,
        initDiretorioServicosFileGridGaleria  : initDiretorioServicosFileGridGaleria,
        destroyDiretorioServicosFileGridGaleria  : destroyDiretorioServicosFileGridGaleria,
        initDiretorioServicosFileGridLogo  : initDiretorioServicosFileGridLogo,
        destroyDiretorioServicosFileGridLogo  : destroyDiretorioServicosFileGridLogo

    };
}();

var MapsGoogle = function () {
    var mapMarker = function () {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });
        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });
        map.setZoom(11);
        GMaps.on('click', map.map, function(event) {
            map.removeMarkers();
            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            jQuery('#coordenadas').val(lat + ',' + lng);
            map.addMarker({
                lat: lat,
                lng: lng,
                title: 'Marker #' + index,
                icon: iconPath,
                infoWindow: {
                    content : ''
                }
            });
        });

    }
    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };
}();

var ButtonHandle = function () {


    var handleButtonAlterarEstadoModal = function (evt) {

        jQuery(".btAlterarEstadoModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarNewEstadoModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2NewEstado');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2newestadoSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewProcManagerId   = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let elNewProcManagerDesc = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
            elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonAlterarEstado = function (evt) {

        jQuery(".alterar2newestadoSend").on('click',function(evt,data){
            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2NewEstado');

            let elNewEstadoDesc  = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            let setNewEstadoDesc = elNewEstadoDesc.val();

            let elNewEstadoId  = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let setNewEstadoId = elNewEstadoId.val();

            vdAjaxCall.sendAlterar2NewEstado(jQuery(this), vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId );

        });
    };


    return {
        //main function to initiate the module
        init: function () {

            handleButtonAlterarEstadoModal();
            handleButtonAlterarEstado();
        }

    };

}();

var vdAjaxCall = function () {

// Gravar novo estado
    var sendAlterar2NewEstado = function (el, vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elNewEstadoDesc.attr('disabled','disabled').addClass('disabled');
        elNewEstadoId.attr('disabled','disabled').addClass('disabled');
        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setNewEstadoDesc =  encodeURIComponent(setNewEstadoDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setNewEstadoDesc=' + setNewEstadoDesc + '&setNewEstadoId=' + setNewEstadoId  ,
            indexValue: {el:el, vd_url_send:vd_url_send, elNewEstadoId:elNewEstadoId, elNewEstadoDesc:elNewEstadoDesc, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError },
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);

                setTimeout(
                    function()
                    {
                        jQuery("#AlterarNewEstadoModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        vdAjaxCall.setNewEstadoStaticVal();

                        el.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.val('')
                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

// obter o valor estático do novo estado
    var setNewEstadoStaticVal = function () {
        let elEstadoStatic = jQuery('.ValorEstadoAtualStatic');
        let vd_url_get = elEstadoStatic .data('vd-url-get');

        jQuery.ajax({
            url: vd_url_get,
            type: "POST",
            indexValue: {elEstadoStatic:elEstadoStatic, vd_url_get:vd_url_get},
            success: function(data){
                var d = JSON.parse(data);
                elEstadoStatic.find('span').fadeOut().html(d.name).removeClass().addClass('label ' + d.cssClass).fadeIn();

            },
            error: function(error){

            }
        });
    };

    return {
        sendAlterar2NewEstado   : sendAlterar2NewEstado,
        setNewEstadoStaticVal   : setNewEstadoStaticVal
    };

}();

jQuery(document).ready(function() {

    PortofolioHandle.init();

    MapsGoogle.init();

    ButtonHandle.init();

});
