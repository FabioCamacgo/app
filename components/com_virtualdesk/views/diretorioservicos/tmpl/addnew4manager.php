<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_capa_files.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_logos_files.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_galeria_files.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('diretorioservicos');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('diretorioservicos', 'addnew4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/diretorioservicos/tmpl/diretorioservicos.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');
    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteDiretorioServicosHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.diretorioservicos.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email2   = $UserProfileData->email;
    $telefone = $UserProfileData->phone1;

    $obParam      = new VirtualDeskSiteParamsHelper();
    $IdDistrito = $obParam->getParamsByTag('IdDistritoDiretorioServicos');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_DIRETORIOSERVICOS_NOVO' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=diretorioservicos&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <div class="tabbable-line nav-justified ">
                <form id="new-empresa" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=diretorioservicos.create4Manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                    <div class="form-body">


                        <div class="bloco">
                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_NOVO_IDENTIFICACAO'); ?></h3></legend>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_NOMEEMPRESA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required class="form-control" autocomplete="off" name="nomeEmpresa" id="nomeEmpresa" maxlength="250" value="<?php echo $nomeEmpresa;?>"/>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_DESIGNACAOCOMERCIAL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="designacaoComercial" id="designacaoComercial" maxlength="250" value="<?php echo $designacaoComercial; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_NIPC'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="nipcEmpresa" id="nipcEmpresa" maxlength="9" value="<?php echo $nipcEmpresa; ?>" />
                                </div>
                            </div>


                            <div class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EMAILCONTACTO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="emailContacto" id="emailContacto" maxlength="250" value="<?php echo htmlentities($emailContacto, ENT_QUOTES, 'UTF-8'); ?>" />
                                </div>
                            </div>



                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_TIPOFIRMA'); ?><span class="required">*</span></label>
                                <?php $TipoFirmas = VirtualDeskSiteDiretorioServicosHelper::getTipoFirma()?>
                                <div class="value col-md-12">
                                    <select name="tipofirma" value="<?php echo $tipofirma; ?>" id="tipofirma" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($tipofirma)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($TipoFirmas as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipoFirma']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $tipofirma; ?>"><?php echo VirtualDeskSiteDiretorioServicosHelper::getipoFirmaName($tipofirma) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeTipoFirma = VirtualDeskSiteDiretorioServicosHelper::excludeTipoFirma($tipofirma)?>
                                            <?php foreach($ExcludeTipoFirma as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipoFirma']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>



                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_DESCRICAO'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control" rows="15" name="descricao" id="descricao" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao); ?></textarea>
                                </div>
                            </div>

                        </div>


                        <div class="bloco">
                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_AREAATUACAO'); ?></h3></legend>

                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_CATEGORIA'); ?><span class="required">*</span></label>
                                <?php $Categorias = VirtualDeskSiteDiretorioServicosHelper::getCategoria()?>
                                <div class="value col-md-12">
                                    <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($categoria)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($Categorias as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['categoria']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteDiretorioServicosHelper::getCategoriaName($categoria) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeCategoria = VirtualDeskSiteDiretorioServicosHelper::excludeCategoria($categoria)?>
                                            <?php foreach($ExcludeCategoria as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['categoria']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <?php
                            $ListaSubcategorias = array();
                            if(!empty($categoria)) {
                                if( (int) $categoria > 0) $ListaSubcategorias = VirtualDeskSiteDiretorioServicosHelper::getSubcategoria($categoria);
                            }
                            ?>
                            <div id="blocoSubcategoria" class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_SUBCATEGORIA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="subcategoria[]" id="subcategoria" value="<?php echo $subcategoria;?>"
                                        <?php
                                        if(empty($categoria)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true" multiple="multiple">
                                        <?php
                                        if(empty($subcategoria)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($ListaSubcategorias as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $subcategoria; ?>"><?php echo VirtualDeskSiteDiretorioServicosHelper::getSubcategoriaName($subcategoria);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeSubcategoria = VirtualDeskSiteDiretorioServicosHelper::excludeSubcategoria($categoria, $subcategoria);?>
                                            <?php foreach($ExcludeSubcategoria as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>

                        </div>


                        <div class="bloco">
                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_LOCALIZACAO'); ?></h3></legend>

                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_CONCELHO'); ?><span class="required">*</span></label>
                                <?php $Concelhos = VirtualDeskSiteDiretorioServicosHelper::getConcelho($IdDistrito)?>
                                <div class="value col-md-12">
                                    <select name="concelho" value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($concelho)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($Concelhos as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['concelho']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteDiretorioServicosHelper::getConcelhoName($concelho) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeConcelho = VirtualDeskSiteDiretorioServicosHelper::excludeConcelho($IdDistrito, $concelho)?>
                                            <?php foreach($ExcludeConcelho as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['concelho']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>



                            <?php
                            $ListaFreguesias = array();
                            if(!empty($concelho)) {
                                if( (int) $concelho > 0) $ListaFreguesias = VirtualDeskSiteDiretorioServicosHelper::getFreguesia($concelho);
                            }
                            ?>
                            <div id="blocoFreguesia" class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_FREGUESIA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="freguesia" id="freguesia" value="<?php echo $freguesia;?>"
                                        <?php
                                        if(empty($concelho)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($freguesia)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($ListaFreguesias as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSiteDiretorioServicosHelper::getFregName($freguesia);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeFreguesia = VirtualDeskSiteDiretorioServicosHelper::excludeFreguesia($concelho, $freguesia);?>
                                            <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_MORADA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="morada" id="morada" maxlength="500" value="<?php echo $morada; ?>"/>
                                </div>
                            </div>

                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_MAPA'); ?></label>
                                <div class="value col-md-12">
                                    <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                    <input type="hidden" class="form-control" name="coordenadas" id="coordenadas" value="<?php echo htmlentities($this->data->coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
                                </div>
                            </div>

                        </div>


                        <div class="bloco">
                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_CONTACTOS'); ?></h3></legend>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_TELEFONE'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                                </div>
                            </div>



                            <div class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_WEBSITE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="website" id="website" maxlength="300" value="<?php echo $website; ?>"/>
                                </div>
                            </div>



                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EMAILCOMERCIAL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="emailComercial" id="emailComercial" maxlength="250" value="<?php echo htmlentities($emailComercial, ENT_QUOTES, 'UTF-8'); ?>" />
                                </div>
                            </div>



                            <div class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EMAILADMINISTRATIVO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="emailAdministrativo" id="emailAdministrativo" maxlength="250" value="<?php echo htmlentities($emailAdministrativo, ENT_QUOTES, 'UTF-8'); ?>" />
                                </div>
                            </div>

                        </div>


                        <div class="bloco">
                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_HORARIOS'); ?></h3></legend>

                            <h4><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_DIASSEMANA'); ?></h4>

                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_HORAABERTURA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="time" class="form-control" name="horaAbertura" id="horaAbertura"  value="<?php echo $horaAbertura; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_HORAENCERRAMENTO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="time" class="form-control" name="horaEncerramento" id="horaEncerramento"  value="<?php echo $horaEncerramento; ?>"/>
                                </div>
                            </div>



                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ALMOCO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="radio" name="radioval" id="sim" value="sim" <?php if (isset($_POST["submitForm"]) && $_POST['abertoAlmoco'] == 1) {echo 'checked="checked"'; } ?>> <?php echo 'Sim'; ?>
                                    <input type="radio" name="radioval" id="nao" value="nao" <?php if (isset($_POST["submitForm"]) && $_POST['abertoAlmoco'] == 2) { echo 'checked="checked"'; } ?>> <?php echo 'Não'; ?>
                                </div>

                                <input type="hidden" id="abertoAlmoco" name="abertoAlmoco" value="<?php echo $abertoAlmoco; ?>">
                            </div>

                            <?php
                                if($abertoAlmoco == 1){
                                    ?>
                                    <script>
                                        document.getElementById('sim').onclick = function() {
                                            document.getElementById("abertoAlmoco").value = 1;
                                            document.getElementById("pausaAlmoco").style.display = "none";
                                        };

                                        document.getElementById('nao').onclick = function() {
                                            document.getElementById("abertoAlmoco").value = 2;
                                            document.getElementById("pausaAlmoco").style.display = "block";
                                        };

                                    </script>
                                <?php
                                } else{
                                ?>
                                    <script>
                                        document.getElementById('sim').onclick = function() {
                                            document.getElementById("abertoAlmoco").value = 1;
                                            document.getElementById("pausaAlmoco").style.display = "none";
                                        };

                                        document.getElementById('nao').onclick = function() {
                                            document.getElementById("abertoAlmoco").value = 2;
                                            document.getElementById("pausaAlmoco").style.display = "block";
                                        };

                                    </script>

                                    <?php
                                }
                            ?>


                            <div id="pausaAlmoco" style="display:none">

                                <div class="form-group half">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_INICIOALMOCO'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="time" class="form-control" name="inicioAlmoco" id="inicioAlmoco"  value="<?php echo $inicioAlmoco; ?>"/>
                                    </div>
                                </div>


                                <div class="form-group half right">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_FIMALMOCO'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="time" class="form-control" name="fimAlmoco" id="fimAlmoco"  value="<?php echo $fimAlmoco; ?>"/>
                                    </div>
                                </div>
                            </div>


                            <h4><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_FIMSEMANA'); ?></h4>

                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_ABERTOFIMSEMANA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="radio" name="radioval2" id="aberto" value="aberto" <?php if (isset($_POST["submitForm"]) && $_POST['abertoFimSemana'] == 1) {echo 'checked="checked"'; } ?>> <?php echo 'Sim'; ?>
                                    <input type="radio" name="radioval2" id="naoAberto" value="naoAberto" <?php if (isset($_POST["submitForm"]) && $_POST['abertoFimSemana'] == 2) { echo 'checked="checked"'; } ?>> <?php echo 'Não'; ?>
                                </div>

                                <input type="hidden" id="abertoFimSemana" name="abertoFimSemana" value="<?php echo $abertoFimSemana; ?>">
                            </div>

                            <?php
                            if($abertoFimSemana == 1){
                                ?>
                                <script>
                                    document.getElementById('aberto').onclick = function() {
                                        document.getElementById("abertoFimSemana").value = 1;
                                        document.getElementById("horarioFimSemana").style.display = "block";
                                    };

                                    document.getElementById('naoAberto').onclick = function() {
                                        document.getElementById("abertoFimSemana").value = 2;
                                        document.getElementById("horarioFimSemana").style.display = "none";
                                    };

                                </script>
                            <?php
                            } else{
                            ?>
                                <script>
                                    document.getElementById('aberto').onclick = function() {
                                        document.getElementById("abertoFimSemana").value = 1;
                                        document.getElementById("horarioFimSemana").style.display = "block";
                                    };

                                    document.getElementById('naoAberto').onclick = function() {
                                        document.getElementById("abertoFimSemana").value = 2;
                                        document.getElementById("horarioFimSemana").style.display = "none";
                                    };

                                </script>

                                <?php
                            }
                            ?>


                            <div id="horarioFimSemana" style="display:none">

                                <div class="form-group half">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_HORAABERTURA_SABADO'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="time" class="form-control" name="horaAberturaSabado" id="horaAberturaSabado" value="<?php echo $horaAberturaSabado; ?>"/>
                                    </div>
                                </div>


                                <div class="form-group half right">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_HORAENCERRAMENTO_SABADO'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="time" class="form-control" name="horaEncerramentoSabado" id="horaEncerramentoSabado"  value="<?php echo $horaEncerramentoSabado; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group half">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_HORAABERTURA_DOMINGO'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="time" class="form-control" name="horaAberturaDomingo" id="horaAberturaDomingo" value="<?php echo $horaAberturaDomingo; ?>"/>
                                    </div>
                                </div>


                                <div class="form-group half right">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_HORAENCERRAMENTO_DOMINGO'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="time" class="form-control" name="horaEncerramentoDomingo" id="horaEncerramentoDomingo"  value="<?php echo $horaEncerramentoDomingo; ?>"/>
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="bloco">
                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_REDESSOCIAIS'); ?></h3></legend>

                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_FACEBOOK'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="facebook" id="facebook" maxlength="300" value="<?php echo $facebook; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_INSTAGRAM'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="instagram" id="instagram" maxlength="300" value="<?php echo $instagram; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_YOUTUBE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="youtube" id="youtube" maxlength="300" value="<?php echo $youtube; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_TWITTER'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="twitter" id="twitter" maxlength="300" value="<?php echo $twitter; ?>"/>
                                </div>
                            </div>
                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_INFORMACOESUTEIS'); ?></h3></legend>


                            <div class="form-group all idiomas">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_IDIOMAS'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">

                                    <?php
                                        $idiomas = VirtualDeskSiteDiretorioServicosHelper::getIdiomaFalado();

                                        foreach($idiomas as $rowWSL) :
                                            $idIdioma = $rowWSL['id'];
                                            $nameIdioma = $rowWSL['idioma'];
                                            ?>
                                            <span><input type="checkbox" name="checkbox<?php echo $idIdioma ?>" id="checkbox<?php echo $idIdioma ?>" value="<?php echo $idIdioma ?>"/><?php echo $nameIdioma;?></span>
                                            <?php
                                        endforeach;

                                        ?>
                                        <span><input type="checkbox" name="checkboxoutro" id="checkboxoutro" value="<?php echo $checkboxoutro ?>"/><?php echo 'Outro';?></span>
                                        <?php
                                        foreach($idiomas as $rowWSL) :
                                            $idIdiomaHide = $rowWSL['id'];
                                            ?>
                                                <span><input type="hidden" id="checkboxHide<?php echo $idIdiomaHide ?>" name="checkboxHide<?php echo $idIdiomaHide ?>" value="checkboxHide<?php echo $idIdiomaHide ?>"></span>
                                            <?php
                                        endforeach;
                                    ?>

                                </div>
                            </div>


                            <div id="outroIdioma" style="display:none">
                                <div class="mt-repeater idioma">
                                    <div data-repeater-list="IdiomaInput">
                                        <div data-repeater-item class="mt-repeater-item ">

                                            <div class="mt-repeater-input IdiomaInput IdiomaInput-new" data-provides="IdiomaInput">

                                                <div class="form-group all" data-trigger="IdiomaInput">
                                                    <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_NOVOIDIOMA'); ?></div>
                                                    <div class="value col-md-12">
                                                        <input type="text" class="form-control" autocomplete="off" placeholder="" name="idioma" maxlength="100" value="<?php echo $idioma; ?>"/>
                                                    </div>
                                                </div>

                                                <a href="javascript:;" class="btn btn-danger mt-repeater-delete IdiomaInput-exists" data-repeater-delete data-dismiss="IdiomaInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                            </div>

                                            <div class="mt-repeater-input">
                                            </div>

                                        </div>

                                    </div>

                                    <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                        <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                                </div>
                            </div>



                            <div class="form-group all premios">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_SELOSPREMIOS'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">

                                    <?php
                                    $premios = VirtualDeskSiteDiretorioServicosHelper::getPremios();

                                    foreach($premios as $rowWSL) :
                                        $idPremios = $rowWSL['id'];
                                        $namePremios = $rowWSL['premio'];
                                        ?>
                                        <span><input type="checkbox" name="premio<?php echo $idPremios ?>" id="premio<?php echo $idPremios ?>" value="premioHide<?php echo $idPremios ?>"/><?php echo $namePremios;?></span>
                                    <?php
                                    endforeach;

                                    foreach($premios as $rowWSL) :
                                        $idPremiosHide = $rowWSL['id'];
                                        ?>
                                        <span><input type="hidden" id="premioHide<?php echo $idPremiosHide ?>" name="premioHide<?php echo $idPremiosHide ?>" value="premioHide<?php echo $idPremiosHide ?>"></span>
                                    <?php
                                    endforeach;
                                    ?>

                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_INFORMACOES'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control" rows="15" name="informacoes" id="informacoes" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($informacoes); ?></textarea>
                                </div>
                            </div>

                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_MULTIMEDIA'); ?></h3></legend>


                            <div class="mt-repeater video ">
                                <div data-repeater-list="VideoInput">
                                    <div data-repeater-item class="mt-repeater-item ">

                                        <div class="mt-repeater-input VideoInput VideoInput-new" data-provides="VideoInput">

                                            <div class="form-group all" data-trigger="VideoInput">
                                                <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_VIDEOS'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="video" maxlength="500" value="<?php echo $video; ?>"/>
                                                </div>
                                            </div>

                                            <a href="javascript:;" class="btn btn-danger mt-repeater-delete VideoInput-exists" data-repeater-delete data-dismiss="VideoInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                        </div>

                                        <div class="mt-repeater-input">
                                        </div>

                                    </div>

                                </div>

                                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                    <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                            </div>



                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_UPLOAD'); ?></label>
                                <div class="value col-md-12">

                                    <div class="file-loading">
                                        <input type="file" name="fileuploadCapa[]" id="fileuploadCapa" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                    <input type="hidden" id="VDAjaxReqProcRefId">

                                </div>
                            </div>


                            <div class="form-group half right">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_UPLOAD_LOGO'); ?></label>
                                <div class="value col-md-12">

                                    <div class="file-loading">
                                        <input type="file" name="fileuploadLogo[]" id="fileuploadLogo" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                    <input type="hidden" id="VDAjaxReqProcRefId">

                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_UPLOAD_GALERIA'); ?></label>
                                <div class="value col-md-12">

                                    <div class="file-loading">
                                        <input type="file" name="fileuploadGaleria[]" id="fileuploadGaleria" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                    <input type="hidden" id="VDAjaxReqProcRefId">

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                </button>
                                <a class="btn default"
                                   href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=diretorioservicos&layout=list4manager&vdcleanstate=1'); ?>"
                                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('diretorioservicos.create4Manager',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                    <?php echo JHtml::_('form.token'); ?>

                </form>
            </div>

        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/diretorioservicos/tmpl/addnew4manager.js.php');
    echo ('</script>');
?>