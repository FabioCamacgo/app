<?php
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_capa_files.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_logos_files.php');
    JLoader::register('VirtualDeskSiteDiretorioServicosGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/MadeiraMagazine/virtualdesksite_diretorioservicos_galeria_files.php');

    $obParam      = new VirtualDeskSiteParamsHelper();
    $pinMapa = $obParam->getParamsByTag('pinMapaDirServ');
    $latitudeDefault = $obParam->getParamsByTag('latitudeDefaultDiretorioServicos');
    $LongitudeDefault = $obParam->getParamsByTag('LongitudeDefaultDiretorioServicos');
    $limitVideoFree = $obParam->getParamsByTag('limitVideoFree');
    $limitIdioma = $obParam->getParamsByTag('limitIdioma');
    $limitGaleriaFree = $obParam->getParamsByTag('limiteGaleriaFree');
    $limitGaleriaPremium = $obParam->getParamsByTag('limiteGaleriaPremium');

    $videosSaved = explode(';;', $this->data->videos);
    $outrosIdiomasSaved = explode(';;', $this->data->outros_idiomas);


    if(count($videosSaved) == 0){
        $videoCount = 1;
    } else {
        $videoCount = count($videosSaved);
    }

    if(count($outrosIdiomasSaved) == 0){
        $outrosIdiomasCount = 1;
    } else {
        $outrosIdiomasCount = count($outrosIdiomasSaved);
    }

    if(!empty($this->data->outros_idiomas)){
        $outroIdioma = 1;
    } else {
        $outroIdioma = '';
    }

    if(empty($this->data->latitude) || empty($this->data->longitude)){
        $latitude = $latitudeDefault;
        $longitude = $LongitudeDefault;
    } else {
        $latitude = $this->data->latitude;
        $longitude = $this->data->longitude;
    }

    $objCapaFiles                = new VirtualDeskSiteDiretorioServicosFilesHelper();
    $vdPreFileUploderCapaLista   = $objCapaFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

    $objGaleriaFiles              = new VirtualDeskSiteDiretorioServicosGaleriaFilesHelper();
    $vdPreFileUploderGaleriaLista = $objGaleriaFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

    $objLogoFiles              = new VirtualDeskSiteDiretorioServicosLogosFilesHelper();
    $vdPreFileUploderLogoLista = $objLogoFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

?>

var iconPath = '<?php echo JUri::base() . $pinMapa; ?>';
var LatToSet = '<?php echo $latitude; ?>';
var LongToSet = '<?php echo $longitude; ?>';
var beginVideo = '<?php echo $videoCount; ?>';
var maxvideo = '<?php echo $limitVideoFree; ?>';
var beginIdioma = '<?php echo $outrosIdiomasCount; ?>';
var maxIdioma = '<?php echo $limitIdioma; ?>';
var abertoAlmoco = '<?php echo $this->data->aberto_almoco; ?>';
var abertoFDS = '<?php echo $this->data->abertoFimSemana; ?>';
var outroIdioma = '<?php echo $outroIdioma; ?>';
var cleanSafe = '<?php echo $this->data->cleanAndSafe; ?>';
var marcaMadeira = '<?php echo $this->data->produtoMadeira; ?>';
var idioma1 = '<?php echo $idioma1; ?>';
var idioma2 = '<?php echo $idioma2; ?>';
var idioma3 = '<?php echo $idioma3; ?>';
var idioma4 = '<?php echo $idioma4; ?>';
var idioma5 = '<?php echo $idioma5; ?>';
var idioma6 = '<?php echo $idioma6; ?>';
var idioma7 = '<?php echo $idioma7; ?>';
var idioma8 = '<?php echo $idioma8; ?>';
var idioma9 = '<?php echo $idioma9; ?>';
var idioma10 = '<?php echo $idioma10; ?>';
var idioma11 = '<?php echo $idioma11; ?>';
var idioma12 = '<?php echo $idioma12; ?>';

var empresasEdit = function() {

    var handleEmpresasEdit = function() {

        jQuery('#edit-empresa').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#edit-empresa').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#edit-empresa').validate().form()) {
                    jQuery('#edit-empresa').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleEmpresasEdit();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        jQuery("#fileuploadCapa").fileuploader({
            changeInput: ' ',
            limit: 1,
            fileMaxSize: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            enableApi: true,
            addMore: true,
            theme: 'thumbnails',
            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploderCapaLista)) echo(', files:  '.$vdPreFileUploderCapaLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#fileuploadCapa').find('input[name="vdFileUpChangedCapa"]').val('1');
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#fileuploadCapa').find('input[name="vdFileUpChangedCapa"]').val('1');
                return true;
            }

        });

        jQuery("#fileuploadLogo").fileuploader({
            changeInput: ' ',
            limit: 1,
            fileMaxSize: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            enableApi: true,
            addMore: true,
            theme: 'thumbnails',
            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }


            <?php if(!empty($vdPreFileUploderLogoLista)) echo(', files:  '.$vdPreFileUploderLogoLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#fileuploadLogo').find('input[name="vdFileUpChangedLogo"]').val('1');
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#fileuploadLogo').find('input[name="vdFileUpChangedLogo"]').val('1');
                return true;
            }
        });

        jQuery("#fileuploadGaleria").fileuploader({
            changeInput: ' ',
            limit: <?php echo $limitGaleriaFree;?>,
            fileMaxSize: 1,
            extensions: ['jpg', 'jpeg', 'png'],
            enableApi: true,
            addMore: true,
            theme: 'thumbnails',
            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }


            <?php if(!empty($vdPreFileUploderGaleriaLista)) echo(', files:  '.$vdPreFileUploderGaleriaLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#fileuploadGaleria').find('input[name="vdFileUpChangedGaleria"]').val('1');
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#fileuploadGaleria').find('input[name="vdFileUpChangedGaleria"]').val('1');
                return true;
            }
        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

var ComponentsEditors = function () {

    var handleWysihtml5 = function () {
        let  ckconfig_removeButtons = 'Subscript,Superscript,Anchor,SpecialChar,Image,About,Scayt';
        let  ckconfig_toolbarGroups =   [
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'others', groups: [ 'others' ] },
            { name: 'colors', groups: [ 'colors' ] }
        ];

        CKEDITOR.replace( 'informacoes', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'descricao', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();
        }
    };

}();

var MapsGoogle = function () {
    var mapMarker = function () {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });
        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });
        map.setZoom(11);
        GMaps.on('click', map.map, function(event) {
            map.removeMarkers();
            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            jQuery('#coordenadas').val(lat + ',' + lng);
            map.addMarker({
                lat: lat,
                lng: lng,
                title: 'Marker #' + index,
                icon: iconPath,
                infoWindow: {
                    content : ''
                }
            });
        });

    }
    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };
}();

var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater.video').each(function(){

                jQuery(this).repeater({
                    show: function () {
                        jQuery(this).slideDown();
                        beginVideo++;
                        if(beginVideo >= maxvideo){
                            jQuery('.video .btn.btn-success.mt-repeater-add').css('display','none');
                        } else {
                            jQuery('.video .btn.btn-success.mt-repeater-add').css('display','inline-block');
                        }

                        if(beginVideo == 1){
                            jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','none');
                        } else {
                            jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                        }
                    },

                    hide: function (deleteElement) {

                        if(confirm('Deseja apagar este elemento?')) {
                            jQuery(this).slideUp(deleteElement);
                            beginVideo--;
                            if(beginVideo >= maxvideo){
                                jQuery('.video .btn.btn-success.mt-repeater-add').css('display','none');
                            } else {
                                jQuery('.video .btn.btn-success.mt-repeater-add').css('display','inline-block');
                            }

                            if(beginVideo == 1){
                                jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','none');
                            } else {
                                jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                            }
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });


            jQuery('.mt-repeater.idioma').each(function(){

                jQuery(this).repeater({
                    show: function () {
                        jQuery(this).slideDown();
                        beginIdioma++;
                        if(beginIdioma >= maxIdioma){
                            jQuery('.idioma .btn.btn-success.mt-repeater-add').css('display','none');
                        } else {
                            jQuery('.idioma .btn.btn-success.mt-repeater-add').css('display','inline-block');
                        }

                        if(beginIdioma == 1){
                            jQuery('.idioma .btn.btn-danger.mt-repeater-delete').css('display','none');
                        } else {
                            jQuery('.idioma .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                        }
                    },

                    hide: function (deleteElement) {

                        if(confirm('Deseja apagar este elemento?')) {
                            jQuery(this).slideUp(deleteElement);
                            beginIdioma--;
                            if(beginIdioma >= maxIdioma){
                                jQuery('.idioma .btn.btn-success.mt-repeater-add').css('display','none');
                            } else {
                                jQuery('.idioma .btn.btn-success.mt-repeater-add').css('display','inline-block');
                            }

                            if(beginIdioma == 1){
                                jQuery('.idioma .btn.btn-danger.mt-repeater-delete').css('display','none');
                            } else {
                                jQuery('.idioma .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                            }
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {

    empresasEdit.init();

    ComponentsSelect2.init();

    ComponentFileUploader.init();

    ComponentsEditors.init();

    MapsGoogle.init();

    FormRepeater.init();

    /*________HORARIOS ALMOCO___________*/
    if(abertoAlmoco == 1){

        document.getElementById("abertoAlmoco").value = 1;
        document.getElementById("pausaAlmoco").style.display = "none";

        document.getElementById('sim').onclick = function() {
            document.getElementById("abertoAlmoco").value = 1;
            document.getElementById("pausaAlmoco").style.display = "none";
        };

        document.getElementById('nao').onclick = function() {
            document.getElementById("abertoAlmoco").value = 2;
            document.getElementById("pausaAlmoco").style.display = "block";
        };

    } else if(abertoAlmoco == 2){

        document.getElementById("abertoAlmoco").value = 2;
        document.getElementById("pausaAlmoco").style.display = "block";

        document.getElementById('sim').onclick = function() {
            document.getElementById("abertoAlmoco").value = 1;
            document.getElementById("pausaAlmoco").style.display = "none";
        };

        document.getElementById('nao').onclick = function() {
            document.getElementById("abertoAlmoco").value = 2;
            document.getElementById("pausaAlmoco").style.display = "block";
        };

    } else {

        document.getElementById('sim').onclick = function() {
            document.getElementById("abertoAlmoco").value = 1;
            document.getElementById("pausaAlmoco").style.display = "none";
        };

        document.getElementById('nao').onclick = function() {
            document.getElementById("abertoAlmoco").value = 2;
            document.getElementById("pausaAlmoco").style.display = "block";
        };
    }


    /*________HORARIOS FIM DE SEMANA___________*/
    if(abertoFDS == 1){

        document.getElementById("abertoFimSemana").value = 1;
        document.getElementById("horarioFimSemana").style.display = "block";

        document.getElementById('aberto').onclick = function() {
            document.getElementById("abertoFimSemana").value = 1;
            document.getElementById("horarioFimSemana").style.display = "block";
        };

        document.getElementById('naoAberto').onclick = function() {
            document.getElementById("abertoFimSemana").value = 2;
            document.getElementById("horarioFimSemana").style.display = "none";
        };

    } else if(abertoFDS == 2){

        document.getElementById("abertoFimSemana").value = 2;
        document.getElementById("horarioFimSemana").style.display = "none";

        document.getElementById('aberto').onclick = function() {
            document.getElementById("abertoFimSemana").value = 1;
            document.getElementById("horarioFimSemana").style.display = "block";
        };

        document.getElementById('naoAberto').onclick = function() {
            document.getElementById("abertoFimSemana").value = 2;
            document.getElementById("horarioFimSemana").style.display = "none";
        };

    } else {
        document.getElementById('aberto').onclick = function() {
            document.getElementById("abertoFimSemana").value = 1;
            document.getElementById("horarioFimSemana").style.display = "block";
        };

        document.getElementById('naoAberto').onclick = function() {
            document.getElementById("abertoFimSemana").value = 2;
            document.getElementById("horarioFimSemana").style.display = "none";
        };
    }

    /*________INICIALIZACAO REPEATER VIDEOS E IDIOMA___________*/
    if(beginVideo == 1){
        jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','none');
    } else {
        jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
    }

    if(beginIdioma == 1){
        jQuery('.idioma .btn.btn-danger.mt-repeater-delete').css('display','none');
    } else {
        jQuery('.idioma .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
    }


    /*________INICILIZACAO SELECT2 CATS E CONCELHOS___________*/
    jQuery("#concelho").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var concelho = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(concelho == null || concelho == '') {

            var select = jQuery('#freguesia');
            select.find('option').remove();
            select.prop('disabled', true);

        } else {

            var dataString = "concelho="+concelho; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoFreguesia',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=diretorioservicos_getfreguesia&concelho=' + concelho ,

                success: function(output) {

                    var select = jQuery('#freguesia');
                    select.find('option').remove();

                    if (output == null || output == '') {
                        select.prop('disabled', true);
                        console.log(output);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoFreguesia');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoFreguesia');
                }
            });
        }
    });


    jQuery("#categoria").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var categoria = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(categoria == null || categoria == '') {

            var select = jQuery('#subcategoria');
            select.find('option').remove();
            select.prop('disabled', true);

        } else {

            var dataString = "categoria="+categoria; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoSubcategoria',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=diretorioservicos_getsubcategoria&categoria=' + categoria ,

                success: function(output) {

                    var select = jQuery('#subcategoria');
                    select.find('option').remove();

                    if (output == null || output == '') {
                        select.prop('disabled', true);
                        console.log(output);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoSubcategoria');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoSubcategoria');
                }
            });
        }
    });


    /*________PREMIOS___________*/
    if(cleanSafe == 1){
        document.getElementById("premioHide1").value = 1;

        document.getElementById('premio1').onclick = function() {
            if ( this.checked ) {
                document.getElementById("premioHide1").value = 1;
            } else {
                document.getElementById("premioHide1").value = 0;
            }
        };

    } else {
        document.getElementById("premioHide1").value = 0;

        document.getElementById('premio1').onclick = function() {
            if ( this.checked ) {
                document.getElementById("premioHide1").value = 1;
            } else {
                document.getElementById("premioHide1").value = 0;
            }
        };

    }


    if(marcaMadeira == 1){
        document.getElementById("premioHide2").value = 1;

        document.getElementById('premio2').onclick = function() {
            if ( this.checked ) {
                document.getElementById("premioHide2").value = 1;
            } else {
                document.getElementById("premioHide2").value = 0;
            }
        };
    } else {
        document.getElementById("premioHide2").value = 0;

        document.getElementById('premio2').onclick = function() {
            if ( this.checked ) {
                document.getElementById("premioHide2").value = 1;
            } else {
                document.getElementById("premioHide2").value = 0;
            }
        };
    }


    /*________IDIOMAS___________*/
    if(idioma1 == 1){

        document.getElementById("checkboxHide1").value = 1;

        document.getElementById('checkbox1').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide1").value = 1;
            } else {
                document.getElementById("checkboxHide1").value = 0;
            }
        };
    } else {

        document.getElementById("checkboxHide1").value = 0;

        document.getElementById('checkbox1').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide1").value = 1;
            } else {
                document.getElementById("checkboxHide1").value = 0;
            }
        };
    }


    if(idioma2 == 1){

        document.getElementById("checkboxHide2").value = 2;

        document.getElementById('checkbox2').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide2").value = 2;
            } else {
                document.getElementById("checkboxHide2").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide2").value = 0;

        document.getElementById('checkbox2').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide2").value = 2;
            } else {
                document.getElementById("checkboxHide2").value = 0;
            }
        };
    }


    if(idioma3 == 1){

        document.getElementById("checkboxHide3").value = 3;

        document.getElementById('checkbox3').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide3").value = 3;
            } else {
                document.getElementById("checkboxHide3").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide3").value = 0;

        document.getElementById('checkbox3').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide3").value = 3;
            } else {
                document.getElementById("checkboxHide3").value = 0;
            }
        };
    }


    if(idioma4 == 1){

        document.getElementById("checkboxHide4").value = 4;

        document.getElementById('checkbox4').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide4").value = 4;
            } else {
                document.getElementById("checkboxHide4").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide4").value = 0;

        document.getElementById('checkbox4').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide4").value = 4;
            } else {
                document.getElementById("checkboxHide4").value = 0;
            }
        };
    }


    if(idioma5 == 1){

        document.getElementById("checkboxHide5").value = 5;

        document.getElementById('checkbox5').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide5").value = 5;
            } else {
                document.getElementById("checkboxHide5").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide5").value = 0;

        document.getElementById('checkbox5').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide5").value = 5;
            } else {
                document.getElementById("checkboxHide5").value = 0;
            }
        };
    }


    if(idioma6 == 1){

        document.getElementById("checkboxHide6").value = 6;

        document.getElementById('checkbox6').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide6").value = 6;
            } else {
                document.getElementById("checkboxHide6").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide6").value = 0;

        document.getElementById('checkbox6').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide6").value = 6;
            } else {
                document.getElementById("checkboxHide6").value = 0;
            }
        };
    }


    if(idioma7 == 1){

        document.getElementById("checkboxHide7").value = 7;

        document.getElementById('checkbox7').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide7").value = 7;
            } else {
                document.getElementById("checkboxHide7").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide7").value = 0;

        document.getElementById('checkbox7').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide7").value = 7;
            } else {
                document.getElementById("checkboxHide7").value = 0;
            }
        };
    }


    if(idioma8 == 1){

        document.getElementById("checkboxHide8").value = 8;

        document.getElementById('checkbox8').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide8").value = 8;
            } else {
                document.getElementById("checkboxHide8").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide8").value = 0;

        document.getElementById('checkbox8').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide8").value = 8;
            } else {
                document.getElementById("checkboxHide8").value = 0;
            }
        };
    }


    if(idioma9 == 1){

        document.getElementById("checkboxHide9").value = 9;

        document.getElementById('checkbox9').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide9").value = 9;
            } else {
                document.getElementById("checkboxHide9").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide9").value = 0;

        document.getElementById('checkbox9').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide9").value = 9;
            } else {
                document.getElementById("checkboxHide9").value = 0;
            }
        };
    }


    if(idioma10 == 1){

        document.getElementById("checkboxHide10").value = 10;

        document.getElementById('checkbox10').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide10").value = 10;
            } else {
                document.getElementById("checkboxHide10").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide10").value = 0;

        document.getElementById('checkbox10').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide10").value = 10;
            } else {
                document.getElementById("checkboxHide10").value = 0;
            }
        };
    }


    if(idioma11 == 1){

        document.getElementById("checkboxHide11").value = 11;

        document.getElementById('checkbox11').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide11").value = 11;
            } else {
                document.getElementById("checkboxHide11").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide11").value = 0;

        document.getElementById('checkbox11').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide11").value = 11;
            } else {
                document.getElementById("checkboxHide11").value = 0;
            }
        };
    }


    if(idioma12 == 1){

        document.getElementById("checkboxHide12").value = 12;

        document.getElementById('checkbox12').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide12").value = 12;
            } else {
                document.getElementById("checkboxHide12").value = 0;
            }
        };

    } else {

        document.getElementById("checkboxHide12").value = 0;

        document.getElementById('checkbox12').onclick = function() {
            if ( this.checked ) {
                document.getElementById("checkboxHide12").value = 12;
            } else {
                document.getElementById("checkboxHide12").value = 0;
            }
        };
    }


    if(outroIdioma == 1){
        jQuery('#outroIdioma').css('display','block');

        document.getElementById('checkboxoutro').onclick = function() {
            if ( this.checked ) {
                jQuery('#outroIdioma').css('display','block');
            } else {
                jQuery('#outroIdioma').css('display','none');
            }
        };
    } else {
        document.getElementById('checkboxoutro').onclick = function() {
            if ( this.checked ) {
                jQuery('#outroIdioma').css('display','block');
            } else {
                jQuery('#outroIdioma').css('display','none');
            }
        };
    }

});
