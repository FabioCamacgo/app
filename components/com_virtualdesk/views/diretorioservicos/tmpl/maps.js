var MapsGoogle = function () {

    var mapBasic = function () {
        new GMaps({
            div: '#gmap_marker',
            lat: 32.72362313575218,
            lng: -17.089385242074854
        });
    }

    return {
        //main function to initiate map samples
        init: function () {
            mapBasic();
        }

    };

}();

jQuery(document).ready(function() {
    MapsGoogle.init();
});