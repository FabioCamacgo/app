<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

 
/**
 * HTML View class for the VirtualDesk Component
 *
 * @since  0.0.1
 */
class VirtualDeskViewVirtualDesk extends JViewLegacy
{
	/**
	 * Display the VirtualDesk view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Assign data to the view
		$this->msg = $this->get('Msg');
 
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JLog::add(implode('<br />', $errors), JLog::WARNING, 'jerror');

            $app = JFactory::getApplication();
            $app->logout();
            //$redirect = JUri::root();
            $redirect = 'index.php?option=com_users&task=user.logout';
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_SESSIONENDED'), 'alert');
            $app->redirect(JRoute::_($redirect, false));
            return false;

		}

        $ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
        $userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();

        $obParam      = new VirtualDeskSiteParamsHelper();
        $copyrightAPP = $obParam->getParamsByTag('copyrightAPP');

        if( !empty($userSessionID) && $userSessionID !=-1 )
        {

            if ( $ObjUserFields->checkAllRequiredBDField($userSessionID) == false && JComponentHelper::getParams('com_virtualdesk')->get('userfield_checkdbfreq_redirect') == 1 )
            {
                $app = JFactory::getApplication();
                $app->enqueueMessage('<br/>' . JText::sprintf('COM_VIRTUALDESK_USER_CHECKBDREQ_ALERT_LABEL', $copyrightAPP) . '<br/><br/>' . JText::_('COM_VIRTUALDESK_USER_CHECKBDREQ_ALERT_DESC'), 'info');
                $app->redirect(JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit', false));
                return false;

            }

        }

		// Display the view
		parent::display($tpl);
	}
}