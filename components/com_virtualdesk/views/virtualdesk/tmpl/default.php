<?php
defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


/*
* Check PERMISSÕES -> Verifica se está no grupo Manager ou Admin da APP
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbInGroupAM === true ) {
    // MANAGER ou ADMIN
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/virtualdesk/tmpl/home-manager.php');
}
else {
    // SITE
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/virtualdesk/tmpl/home-user.php');
}
?>