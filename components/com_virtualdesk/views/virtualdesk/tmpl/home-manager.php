<?php
defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_stats.php');
//JLoader::register('VirtualDeskSiteContactUsReportsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus_reports.php');

JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
JLoader::register('VirtualDeskSiteAlertaStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta_stats.php');

JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
JLoader::register('VirtualDeskSiteAgendaStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda_stats.php');

JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');
JLoader::register('VirtualDeskSiteCulturaStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura_stats.php');

JLoader::register('VirtualDeskSiteTicketsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tickets.php');
JLoader::register('VirtualDeskSiteTicketsStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tickets_stats.php');

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteMainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_main.php');

JLoader::register('VirtualDeskSiteSuporteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte.php');
JLoader::register('VirtualDeskSiteSuporteStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte_stats.php');

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

/*
* Check PERMISSÕES -> Verifica se está no grupo Manager ou Admin da APP
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');


// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';


$localScripts  = '';
#$localScripts  = $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/echarts/echarts.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.waypoints.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.counterup.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/amcharts.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/serial.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/pie.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/light.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/dark.js' . $addscript_end;
#$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/patterns.js' . $addscript_end;
#$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/chalk.js' . $addscript_end;
##$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/ammap/ammap.js' . $addscript_end;
##$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/ammap/maps/js/portugalRegionsHigh.js' . $addscript_end;
##$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/ammap/maps/portugalRegionsLow.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');


// Alerta - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/alerta/tmpl/alerta-comum.css');
// Agenda - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/agenda-comum.css');
// Cultura - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/cultura/tmpl/cultura-comum.css');
// Tickets - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/tickets/tmpl/tickets-comum.css');
// Suporte - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/suporte/tmpl/suporte-comum.css');

$ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
$userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();

$imagback = $baseurl . '/templates/' . $templateName . '/images/image_back.jpg';


/*  ESTATISTICAS - CULTURA */
$objCulturaStats = new VirtualDeskSiteCulturaStatsHelper();
$objCulturaStats->setAllStats(true);

$culturaNumPedidosOnline       = $objCulturaStats->TotalPedidosOnline;
$culturaNumPedidosBalcao       = $objCulturaStats->TotalPedidosBalcao;
$culturaNumResolvidos          = $objCulturaStats->TotalResolvidos;
$culturaNumNaoResolvidos       = $objCulturaStats->TotalNaoResolvidos;
$dataCulturaTotaisPorCategoria = $objCulturaStats->TotaisPorCategoria;
$dataCulturaTotPorAnoMes       = $objCulturaStats->TotaisPorAnoMes;
$dataCulturaTotaisPorEstado    = $objCulturaStats->TotalPedidosPorEstado;
$dataCulturaTempoMedioEstado   = $objCulturaStats->TempoMedioEstado;
$dataCulturaDadosImediatos     = $objCulturaStats->DadosImediatos;

/*  ESTATISTICAS - ALERTA */
$objAlertaStats = new VirtualDeskSiteAlertaStatsHelper();
$objAlertaStats->setAllStats(true);

$alertaNumPedidosOnline       = $objAlertaStats->TotalPedidosOnline;
$alertaNumPedidosBalcao       = $objAlertaStats->TotalPedidosBalcao;
$alertaNumResolvidos          = $objAlertaStats->TotalResolvidos;
$alertaNumNaoResolvidos       = $objAlertaStats->TotalNaoResolvidos;
$dataAlertaTotaisPorCategoria = $objAlertaStats->TotaisPorCategoria;
$dataAlertaTotPorAnoMes       = $objAlertaStats->TotaisPorAnoMes;
$dataAlertaTotaisPorEstado    = $objAlertaStats->TotalPedidosPorEstado;
$dataAlertaTempoMedioEstado   = $objAlertaStats->TempoMedioEstado;
$dataAlertaDadosImediatos     = $objAlertaStats->DadosImediatos;

/*  ESTATISTICAS - AGENDA */
$objStats = new VirtualDeskSiteAgendaStatsHelper();
$objStats->setAllStats(true);

$agendaNumPedidosOnline       = $objStats->TotalPedidosOnline;
$agendaNumPedidosBalcao       = $objStats->TotalPedidosBalcao;
$agendaNumResolvidos          = $objStats->TotalResolvidos;
$agendaNumNaoResolvidos       = $objStats->TotalNaoResolvidos;
$dataAgendaTotaisPorCategoria =  $objStats->TotaisPorCategoria;
$dataAgendaTotPorAnoMes       = $objStats->TotaisPorAnoMes;
$dataAgendaTotaisPorEstado    = $objStats->TotalPedidosPorEstado;
$dataAgendaTempoMedioEstado   = $objStats->TempoMedioEstado;
$dataAgendaDadosImediatos     = $objStats->DadosImediatos;

/*  ESTATISTICAS - TICKETS*/
$objStats = new VirtualDeskSiteTicketsStatsHelper();
$objStats->setAllStats(true);

$ticketsNumPedidosOnline       = $objStats->TotalPedidosOnline;
$ticketsNumPedidosBalcao       = $objStats->TotalPedidosBalcao;
$ticketsNumResolvidos          = $objStats->TotalResolvidos;
$ticketsNumNaoResolvidos       = $objStats->TotalNaoResolvidos;
$dataTicketsTotaisPorCategoria = $objStats->TotaisPorCategoria;
$dataTicketsTotPorAnoMes       = $objStats->TotaisPorAnoMes;
$dataTicketsTotaisPorEstado    = $objStats->TotalPedidosPorEstado;
$dataTicketsTempoMedioEstado   = $objStats->TempoMedioEstado;
$dataTicketsDadosImediatos     = $objStats->DadosImediatos;

/*  ESTATISTICAS - SUPORTE*/
$objStats = new VirtualDeskSiteSuporteStatsHelper();
$objStats->setAllStats(true);

$suporteNumPedidosOnline       = $objStats->TotalPedidosOnline;
$suporteNumPedidosBalcao       = $objStats->TotalPedidosBalcao;
$suporteNumResolvidos          = $objStats->TotalResolvidos;
$suporteNumNaoResolvidos       = $objStats->TotalNaoResolvidos;
$dataSuporteTotaisPorCategoria = $objStats->TotaisPorCategoria;
$dataSuporteTotPorAnoMes       = $objStats->TotaisPorAnoMes;
$dataSuporteTotaisPorEstado    = $objStats->TotalPedidosPorEstado;
$dataSuporteTempoMedioEstado   = $objStats->TempoMedioEstado;
$dataSuporteDadosImediatos     = $objStats->DadosImediatos;


/* ESTATISTICAS - HOME  */
$homeNumPedidosOnline    = 0;
$homeNumPedidosOnline   += $culturaNumPedidosOnline;
$homeNumPedidosOnline   += $alertaNumPedidosOnline;
$homeNumPedidosOnline   += $agendaNumPedidosOnline;
$homeNumPedidosOnline   += $ticketsNumPedidosOnline;
$homeNumPedidosOnline   += $suporteNumPedidosOnline;

$homeNumPedidosBalcao    = 0;
$homeNumPedidosBalcao   += $culturaNumPedidosBalcao;
$homeNumPedidosBalcao   += $alertaNumPedidosBalcao;
$homeNumPedidosBalcao   += $agendaNumPedidosBalcao;
$homeNumPedidosBalcao   += $ticketsNumPedidosBalcao;
$homeNumPedidosBalcao   += $suporteNumPedidosBalcao;

$dataDadosImediatos_Hoje  = 0;
$dataDadosImediatos_Hoje += $dataCulturaDadosImediatos->Hoje;
$dataDadosImediatos_Hoje += $dataAlertaDadosImediatos->Hoje;
$dataDadosImediatos_Hoje += $dataAgendaDadosImediatos->Hoje;
$dataDadosImediatos_Hoje += $dataTicketsDadosImediatos->Hoje;
$dataDadosImediatos_Hoje += $dataSuporteDadosImediatos->Hoje;

$dataDadosImediatos_Ontem  = 0;
$dataDadosImediatos_Ontem += $dataCulturaDadosImediatos->Ontem;
$dataDadosImediatos_Ontem += $dataAlertaDadosImediatos->Ontem;
$dataDadosImediatos_Ontem += $dataAgendaDadosImediatos->Ontem;
$dataDadosImediatos_Ontem += $dataTicketsDadosImediatos->Ontem;
$dataDadosImediatos_Ontem += $dataSuporteDadosImediatos->Ontem;

$dataDadosImediatos_UltimaSemana  = 0;
$dataDadosImediatos_UltimaSemana += $dataCulturaDadosImediatos->UltimaSemana;
$dataDadosImediatos_UltimaSemana += $dataAlertaDadosImediatos->UltimaSemana;
$dataDadosImediatos_UltimaSemana += $dataAgendaDadosImediatos->UltimaSemana;
$dataDadosImediatos_UltimaSemana += $dataTicketsDadosImediatos->UltimaSemana;
$dataDadosImediatos_UltimaSemana += $dataSuporteDadosImediatos->UltimaSemana;

$dataDadosImediatos_UltimoMes  = 0;
$dataDadosImediatos_UltimoMes += $dataCulturaDadosImediatos->UltimoMes;
$dataDadosImediatos_UltimoMes += $dataAlertaDadosImediatos->UltimoMes;
$dataDadosImediatos_UltimoMes += $dataAgendaDadosImediatos->UltimoMes;
$dataDadosImediatos_UltimoMes += $dataTicketsDadosImediatos->UltimoMes;
$dataDadosImediatos_UltimoMes += $dataSuporteDadosImediatos->UltimoMes;

$homeNumResolvidos  = 0;
$homeNumResolvidos += $culturaNumResolvidos;
$homeNumResolvidos += $alertaNumResolvidos;
$homeNumResolvidos += $agendaNumResolvidos;
$homeNumResolvidos += $ticketsNumResolvidos;
$homeNumResolvidos += $suporteNumResolvidos;

$homeNumNaoResolvidos  = 0;
$homeNumNaoResolvidos += $culturaNumNaoResolvidos;
$homeNumNaoResolvidos += $alertaNumNaoResolvidos;
$homeNumNaoResolvidos += $agendaNumNaoResolvidos;
$homeNumNaoResolvidos += $ticketsNumNaoResolvidos;
$homeNumNaoResolvidos += $suporteNumNaoResolvidos;

$dataTempoMedioEstado = array();
if(!empty($dataCulturaTempoMedioEstado[1])) $dataTempoMedioEstado[1] += $dataCulturaTempoMedioEstado[1];
if(!empty($dataAlertaTempoMedioEstado[1]))  $dataTempoMedioEstado[1] += $dataAlertaTempoMedioEstado[1];
if(!empty($dataAlertaTempoMedioEstado[1]))  $dataTempoMedioEstado[1] += $dataAlertaTempoMedioEstado[1];
if(!empty($dataTicketsTempoMedioEstado[1])) $dataTempoMedioEstado[1] += $dataTicketsTempoMedioEstado[1];
if(!empty($dataSuporteTempoMedioEstado[1])) $dataTempoMedioEstado[1] += $dataSuporteTempoMedioEstado[1];

if(!empty($dataCulturaTempoMedioEstado[2])) $dataTempoMedioEstado[2] += $dataCulturaTempoMedioEstado[2];
if(!empty($dataAlertaTempoMedioEstado[2]))  $dataTempoMedioEstado[2] += $dataAlertaTempoMedioEstado[2];
if(!empty($dataAlertaTempoMedioEstado[2]))  $dataTempoMedioEstado[2] += $dataAlertaTempoMedioEstado[2];
if(!empty($dataTicketsTempoMedioEstado[2])) $dataTempoMedioEstado[2] += $dataTicketsTempoMedioEstado[2];
if(!empty($dataSuporteTempoMedioEstado[2])) $dataTempoMedioEstado[2] += $dataSuporteTempoMedioEstado[2];

if(!empty($dataCulturaTempoMedioEstado[3])) $dataTempoMedioEstado[3] += $dataCulturaTempoMedioEstado[3];
if(!empty($dataAlertaTempoMedioEstado[3]))  $dataTempoMedioEstado[3] += $dataAlertaTempoMedioEstado[3];
if(!empty($dataAlertaTempoMedioEstado[3]))  $dataTempoMedioEstado[3] += $dataAlertaTempoMedioEstado[3];
if(!empty($dataTicketsTempoMedioEstado[3])) $dataTempoMedioEstado[3] += $dataTicketsTempoMedioEstado[3];
if(!empty($dataSuporteTempoMedioEstado[3])) $dataTempoMedioEstado[3] += $dataSuporteTempoMedioEstado[3];

// Gráfico por Ano / Mês
$dataHomeTotPorAnoMes = array();

foreach ($dataCulturaTotPorAnoMes as $row){
    if(empty($dataHomeTotPorAnoMes [$row->year.'/'.$row->mes])) $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] = 0;
    $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] += $row->npedidos;
}
foreach ($dataAlertaTotPorAnoMes as $row){
    if(empty($dataHomeTotPorAnoMes [$row->year.'/'.$row->mes])) $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] = 0;
    $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] += $row->npedidos;
}
foreach ($dataAgendaTotPorAnoMes as $row){
    if(empty($dataHomeTotPorAnoMes [$row->year.'/'.$row->mes])) $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] = 0;
    $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] += $row->npedidos;
}
foreach ($dataTicketsTotPorAnoMes as $row){
    if(empty($dataHomeTotPorAnoMes [$row->year.'/'.$row->mes])) $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] = 0;
    $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] += $row->npedidos;
}
foreach ($dataSuporteTotPorAnoMes as $row){
    if(empty($dataHomeTotPorAnoMes [$row->year.'/'.$row->mes])) $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] = 0;
    $dataHomeTotPorAnoMes [$row->year.'/'.$row->mes] += $row->npedidos;
}

ksort($dataHomeTotPorAnoMes);

// Gráfico por ESTADO
$dataHomeTotPorEstado = array();

foreach ($dataCulturaTotaisPorEstado as $row){
    if(empty($dataHomeTotPorEstado [$row->estado])) $dataHomeTotPorEstado [$row->estado] = 0;
    $dataHomeTotPorEstado [$row->estado] += $row->npedidos;
}
foreach ($dataAlertaTotaisPorEstado as $row){
    if(empty($dataHomeTotPorEstado [$row->estado])) $dataHomeTotPorEstado [$row->estado] = 0;
    $dataHomeTotPorEstado [$row->estado] += $row->npedidos;
}
foreach ($dataAgendaTotaisPorEstado as $row){
    if(empty($dataHomeTotPorEstado [$row->estado])) $dataHomeTotPorEstado [$row->estado] = 0;
    $dataHomeTotPorEstado [$row->estado] += $row->npedidos;
}
foreach ($dataTicketsTotaisPorEstado as $row){
    if(empty($dataHomeTotPorEstado [$row->estado])) $dataHomeTotPorEstado [$row->estado] = 0;
    $dataHomeTotPorEstado [$row->estado] += $row->npedidos;
}
foreach ($dataSuporteTotaisPorEstado as $row){
    if(empty($dataHomeTotPorEstado [$row->estado])) $dataHomeTotPorEstado [$row->estado] = 0;
    $dataHomeTotPorEstado [$row->estado] += $row->npedidos;
}


// Gráfico por CATEGORIA
$dataHomeTotPorCategoria = array();

foreach ($dataCulturaTotaisPorCategoria as $row){
    if(empty($dataHomeTotPorCategoria [$row->categoria])) $dataHomeTotPorCategoria [$row->categoria] = 0;
    $dataHomeTotPorCategoria [$row->categoria] += $row->npedidos;
}
foreach ($dataAlertaTotaisPorCategoria as $row){
    if(empty($dataHomeTotPorCategoria [$row->categoria])) $dataHomeTotPorCategoria [$row->categoria] = 0;
    $dataHomeTotPorCategoria [$row->categoria] += $row->npedidos;
}
foreach ($dataAgendaTotaisPorCategoria as $row){
    if(empty($dataHomeTotPorCategoria [$row->categoria])) $dataHomeTotPorCategoria [$row->categoria] = 0;
    $dataHomeTotPorCategoria [$row->categoria] += $row->npedidos;
}
foreach ($dataTicketsTotaisPorCategoria as $row){
    if(empty($dataHomeTotPorCategoria [$row->categoria])) $dataHomeTotPorCategoria [$row->categoria] = 0;
    $dataHomeTotPorCategoria [$row->categoria] += $row->npedidos;
}
foreach ($dataSuporteTotaisPorCategoria as $row){
    if(empty($dataHomeTotPorCategoria [$row->categoria])) $dataHomeTotPorCategoria [$row->categoria] = 0;
    $dataHomeTotPorCategoria [$row->categoria] += $row->npedidos;
}

$AtiveUsersForSiteView     = VirtualDeskSiteStatsHelper::getNumberOfActiveUsersSiteView();
$ContactUsRequestSiteView  = VirtualDeskSiteStatsHelper::getNumberOfContactUsRequestSiteView();
$UserAccessSiteView        = VirtualDeskSiteStatsHelper::getNumberOfUserAccessSiteView();

$obParam      = new VirtualDeskSiteParamsHelper();
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');

?>
<style>

    .imagemback {height: 500px; background-image: url(<?php echo $imagback;?>); background-position: center;    background-repeat: no-repeat;   margin: 0 -20px;}

    .tabbable-line>.tab-content {  padding: 0;  }
    .iconVDModified {padding-left: 15px; }

    .EstadoFilterDropBoxHome , .CatFilterDropBoxHome, .ModulosFilterDropBoxHome {float: right; margin-left: 15px}
    @media (min-width:1364px) {
        .EstadoFilterDropBoxHome{padding-right: 350px;}
    }

    div.vdBlocoContador {float:left; padding-left: 15px; }
    div.vdBlocoContadorTexto {margin-top: -26px; margin-left: 52px;}

</style>


<div class="portlet light bordered form-fit">

    <div class="portlet-body">


        <?php if ( $ObjUserFields->checkAllRequiredBDField($userSessionID) == false ) : ?>
            <div id="MainMessageAlertBlock" class="alert alert-info fade in" >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                <p><?php echo JText::sprintf('COM_VIRTUALDESK_USER_CHECKBDREQ_ALERT_LABEL', $copyrightAPP); ?></p>
                <p><?php echo JText::_('COM_VIRTUALDESK_USER_CHECKBDREQ_ALERT_DESC'); ?></p>
                <p><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit', false); ?>" ><?php echo JText::_('COM_VIRTUALDESK_USER_CHECKBDREQ_ALERTCLICK_LABEL'); ?></a></p>
                <p></p>
                <p></p>
            </div>

        <?php endif; ?>


    <div class="tabbable-line nav-justified ">
        <ul class="nav nav-tabs  ">
            <li class="active">

                <a href="#tab_PaginaHome_Estatistica" data-toggle="tab">
                    <h4>
                        <i class="fa fa-bar-chart"></i>
                        <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                    </h4>
                </a>

            </li>
            <li class="">
                <a href="#tab_PaginaHome_PedidosSubmetidos" data-toggle="tab">
                    <h4>
                        <i class="fa fa-tasks"></i>
                        <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_PEDIDOSSUBMETIDOS'); ?>
                    </h4>
                </a>
            </li>

        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="tab_PaginaHome_Estatistica">

        <div class="row widget-row">

            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMACCESSES'); ?> </h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-green icon-pie-chart"></i>
                        <div class="widget-thumb-body vdBlocoContador">
                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $UserAccessSiteView; ?>"><?php echo $UserAccessSiteView; ?></span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>

            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMPEDIDOS_EFETUADOS'); ?></h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-blue icon-users"></i>

                        <div class="widget-thumb-body vdBlocoContador">
                            <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_ONLINE'); ?></span>
                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $homeNumPedidosOnline; ?>"><?php echo $homeNumPedidosOnline; ?></span>
                        </div>
                        <div class="widget-thumb-body vdBlocoContador">
                            <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_BALCAO'); ?></span>
                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $homeNumPedidosBalcao; ?>"><?php echo $homeNumPedidosBalcao; ?></span>
                        </div>

                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>

            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_RESOLVIDOS'); ?> </h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-blue icon-trophy"></i>
                        <div class="widget-thumb-body ">
                            <span class="widget-thumb-subtitle"></span>
                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $homeNumResolvidos; ?>"><?php echo $homeNumResolvidos; ?></span>
                        </div>
                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>

            <div class="col-md-3">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_PENDENTES'); ?></h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-red icon-paper-clip "></i>
                        <div class="widget-thumb-body vdBlocoContador">
                            <span class="widget-thumb-subtitle"></span>
                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $homeNumNaoResolvidos; ?>"><?php echo $homeNumNaoResolvidos; ?></span>
                        </div>

                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>



        </div>

        <div class="row widget-row">

            <div class="col-md-6">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Dados Imediatos</h4>
                    <div class="widget-thumb-wrap">
                        <div class="col-md-2">
                            <i class="widget-thumb-icon bg-purple icon-speedometer"></i>
                        </div>

                        <div class="col-md-2">
                            <div class="widget-thumb-body vdBlocoContador">
                                <span class="widget-thumb-subtitle">Hoje</span>
                                <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo  $dataDadosImediatos_Hoje; ?>"><?php echo $dataDadosImediatos_Hoje; ?></span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="widget-thumb-body vdBlocoContador">
                                <span class="widget-thumb-subtitle">Ontem</span>
                                <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $dataDadosImediatos_Ontem; ?>"><?php echo $dataDadosImediatos_Ontem; ?></span>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget-thumb-body vdBlocoContador">
                                <span class="widget-thumb-subtitle">Semana</span>
                                <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $dataDadosImediatos_UltimaSemana; ?>"><?php echo $dataDadosImediatos_UltimaSemana; ?></span>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget-thumb-body vdBlocoContador">
                                <span class="widget-thumb-subtitle"> Último Mês</span>
                                <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $dataDadosImediatos_UltimoMes; ?>"><?php echo $dataDadosImediatos_UltimoMes; ?></span>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>


            <div class="col-md-6">
                <!-- BEGIN WIDGET THUMB -->
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Tempo Médio</h4>
                    <div class="widget-thumb-wrap">
                        <div class="col-md-3">
                            <i class="widget-thumb-icon bg-green icon-clock"></i>
                        </div>

                        <div class="col-md-3">
                            <div class="widget-thumb-body vdBlocoContador">
                                <span class="widget-thumb-subtitle">Em Espera</span>
                                <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php if(!empty($dataTempoMedioEstado[1])) echo $dataTempoMedioEstado[1]; ?>"><?php if(!empty($dataTempoMedioEstado[1])) echo $dataTempoMedioEstado[1]; ?></span>
                                <div class=" vdBlocoContadorTexto">dias</div>
                            </div>

                        </div>

                        <div class="col-md-3">
                            <div class="widget-thumb-body vdBlocoContador">
                                <span class="widget-thumb-subtitle">Em Análise</span>
                                <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php if(!empty($dataTempoMedioEstado[2])) echo $dataTempoMedioEstado[2]; ?>"><?php if(!empty($dataTempoMedioEstado[2])) echo $dataTempoMedioEstado[2]; ?></span>
                                <div class=" vdBlocoContadorTexto">dias</div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="widget-thumb-body vdBlocoContador">
                                <span class="widget-thumb-subtitle">Concluídos</span>
                                <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php if(!empty($dataTempoMedioEstado[3])) echo $dataTempoMedioEstado[3]; ?>"><?php if(!empty($dataTempoMedioEstado[3])) echo $dataTempoMedioEstado[3]; ?></span>
                                <div class=" vdBlocoContadorTexto">dias</div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END WIDGET THUMB -->
            </div>


        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-blue"></i>
                            <span class="caption-subject bold uppercase font-blue"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORMES'); ?></span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartHomeTotPorAnoMes" class="chart" style="height: 250px;"></div>
                    </div>
                </div>

            </div>

            <div class="col-md-12">

                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-pie-chart font-green-haze"></i>
                            <span class="caption-subject bold uppercase font-green-haze"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORESTADO'); ?></span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartHomeTotaisPorEstado" class="chart" style="height: 500px;"></div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-pie-chart "></i>
                            <span class="caption-subject bold uppercase "><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORCATEGORIA'); ?></span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartHomeTotaisPorCategoria" class="chart" style="height: 500px;"></div>
                    </div>
                </div>
            </div>

        </div>



            </div>


            <div class="tab-pane " id="tab_PaginaHome_PedidosSubmetidos">

                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                        </div>

                        <div class="actions">


                            <!-- Botões DataTables -->
                            <div class="btn-group">
                                <a class="btn green btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right" id="tabela_pedidossubmetidos_tools">
                                    <li>
                                        <a href="javascript:;" data-action="0" class="tool-action">
                                            <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-action="1" class="tool-action">
                                            <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-action="2" class="tool-action">
                                            <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-action="3" class="tool-action">
                                            <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?></a>
                                    </li>


                                </ul>
                            </div>

                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

                        </div>

                    </div>

                    <div class="portlet-body ">
                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_pedidossubmetidos">
                            <thead>
                            <tr>
                                <th style="min-width: 80px;"><?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_LISTA_DATA'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_LISTA_MODULO'); ?></th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_LISTA_TITULO'); ?></th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_LISTA_CATEGORIA'); ?></th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_LISTA_ESTADO'); ?></th>
                                <th ><?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_LISTA_REFERENCIA'); ?> </th>
                                <th> </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>



    </div>

</div>



<?php

echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/virtualdesk/tmpl/home-manager.js.php');
echo ('</script>');
?>