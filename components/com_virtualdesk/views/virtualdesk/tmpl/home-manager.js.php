<?php
defined('_JEXEC') or die;

$objEstadoOptionsAlerta  = VirtualDeskSiteAlertaHelper::getAlertaEstadoAllOptions($language_tag,false);
$objEstadoOptionsAgenda  = VirtualDeskSiteAgendaHelper::getAgendaEstadoAllOptions($language_tag,false);
$objEstadoOptionsCultura = VirtualDeskSiteCulturaHelper::getCulturaEstadoAllOptions($language_tag,false);
$objEstadoOptionsTickets = VirtualDeskSiteTicketsHelper::getTicketsEstadoAllOptions($language_tag,false);
$objEstadoOptionsSuporte = VirtualDeskSiteSuporteHelper::getSuporteEstadoAllOptions($language_tag,false);

$obParam           = new VirtualDeskSiteParamsHelper();
$concelhoAlerta    = $obParam->getParamsByTag('alertaConcelho');
$objCatOptionsAlerta  = VirtualDeskSiteAlertaHelper::getAlertaCategoriasAllOptions($concelhoAlerta);
$objCatOptionsAgenda  = VirtualDeskSiteAgendaHelper::getAgendaCategoriasAllOptions();
$objCatOptionsCultura = VirtualDeskSiteCulturaHelper::getCategoriaAllOptions();
$objCatOptionsTickets = VirtualDeskSiteTicketsHelper::getDepartamentoAllOptions();
$objCatOptionsSuporte = array(); // não tem departamentos
// $objCatOptionsSuporte = VirtualDeskSiteSuporteHelper::getDepartamentoAllOptions();

$objModulosOptions       = VirtualDeskSiteMainHelper::getModuloAllOptions();

$TipoPedidoAlerta   = JText::_('COM_VIRTUALDESK_ALERTA_HEADING');
$TipoPedidoAgenda   = JText::_('COM_VIRTUALDESK_AGENDA_HEADING');
$TipoPedidoCultura  = JText::_('COM_VIRTUALDESK_CULTURA_HEADING');
$TipoPedidoTickets  = JText::_('COM_VIRTUALDESK_TICKETS_HEADING');
$TipoPedidoSuporte  = JText::_('COM_VIRTUALDESK_SUPORTE_HEADING');

/* ESTADO */
$EstadoOptionsHTML = '';
if(!is_array($objEstadoOptionsAlerta)) $objEstadoOptionsAlerta = array();
foreach($objEstadoOptionsAlerta as $row) {
    $EstadoOptionsHTML .= '<option value="ALRT' . $row['id'] . '">' .$TipoPedidoAlerta.' - '. $row['name'] . '</option>';
}

if(!is_array($objEstadoOptionsAgenda)) $objEstadoOptionsAgenda = array();
foreach($objEstadoOptionsAgenda as $row) {
    $EstadoOptionsHTML .= '<option value="AGND' . $row['id'] . '">' .$TipoPedidoAgenda.' - '. $row['name'] . '</option>';
}

if(!is_array($objEstadoOptionsCultura)) $objEstadoOptionsCultura = array();
foreach($objEstadoOptionsCultura as $row) {
    $EstadoOptionsHTML .= '<option value="CULT' . $row['id'] . '">' .$TipoPedidoCultura.' - '. $row['name'] . '</option>';
}

if(!is_array($objEstadoOptionsTickets)) $objEstadoOptionsTickets = array();
foreach($objEstadoOptionsTickets as $row) {
    $EstadoOptionsHTML .= '<option value="TICK' . $row['id'] . '">' .$TipoPedidoTickets.' - '. $row['name'] . '</option>';
}

if(!is_array($objEstadoOptionsSuporte)) $objEstadoOptionsSuporte = array();
foreach($objEstadoOptionsSuporte as $row) {
    $EstadoOptionsHTML .= '<option value="TICK' . $row['id'] . '">' .$TipoPedidoSuporte.' - '. $row['name'] . '</option>';
}


/* CATEGORIA */
$CatOptionsHTML = '';
if(!is_array($objCatOptionsAlerta)) $objCatOptionsAlerta = array();
foreach($objCatOptionsAlerta as $row) {
    $CatOptionsHTML .= '<option value="ALRT' . $row['id'] . '">' .$TipoPedidoAlerta.' - '. $row['name'] . '</option>';
}

if(!is_array($objCatOptionsAgenda)) $objCatOptionsAgenda = array();
foreach($objCatOptionsAgenda as $row) {
    $CatOptionsHTML .= '<option value="AGND' . $row['id'] . '">' .$TipoPedidoAgenda.' - '. $row['name'] . '</option>';
}

if(!is_array($objCatOptionsCultura)) $objCatOptionsCultura = array();
foreach($objCatOptionsCultura as $row) {
    $CatOptionsHTML .= '<option value="CULT' . $row['id'] . '">' .$TipoPedidoCultura.' - '. $row['name'] . '</option>';
}

if(!is_array($objCatOptionsTickets)) $objCatOptionsTickets = array();
foreach($objCatOptionsTickets as $row) {
    $CatOptionsHTML .= '<option value="TICK' . $row['id'] . '">' .$TipoPedidoTickets.' - '. $row['name'] . '</option>';
}

if(!is_array($objCatOptionsSuporte)) $objCatOptionsSuporte = array();
foreach($objCatOptionsSuporte as $row) {
    $CatOptionsHTML .= '<option value="TICK' . $row['id'] . '">' .$TipoPedidoSuporte.' - '. $row['name'] . '</option>';
}

/* MODULOS */
$ModulosOptionsHTML = '';
foreach($objModulosOptions as $rowM) {
    $ModulosOptionsHTML .= '<option value="' . $rowM['id'] . '">' . $rowM['name'] . '</option>';
}
?>
var ChartsAmcharts = function() {

    var initChartHomeTotPorAnoMes = function() {
        var chart = AmCharts.makeChart("chartHomeTotPorAnoMes", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',
            "color":    '#888',

            "dataProvider": [
                <?php
                if(!is_array($dataHomeTotPorAnoMes)) $dataHomeTotPorAnoMes = array();
                foreach ($dataHomeTotPorAnoMes as $rowAnoMes => $valAnoMes) : ?>
                {
                    "year":  '<?php echo $rowAnoMes ; ?>',
                    "npedidos": <?php echo $valAnoMes ; ?>
                },
                <?php endforeach; ?>


            ],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "startEffect": "easeOutSine", // easeOutSine, easeInSine, elastic, bounce
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] em [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Nº de Pedidos",
                "type": "column",
                "valueField": "npedidos"
            }],
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });

        jQuery('#chartHomeTotPorAnoMes').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    var initChartHomeTotaisPorEstado = function() {
        var chart = AmCharts.makeChart("chartHomeTotaisPorEstado", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [
                <?php
                if(!is_array($dataHomeTotPorEstado)) $dataHomeTotPorEstado = array();
                foreach ($dataHomeTotPorEstado as $row => $val) : ?>
                {
                    "estado":  '<?php echo $row; ?>',
                    "npedidos": <?php echo $val ; ?>
                },
                <?php endforeach; ?>
            ],
            "startEffect": "easeOutSine", // easeOutSine, easeInSine, elastic, bounce
            "startRadius":"90%",
            "valueField": "npedidos",
            "titleField": "estado",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        jQuery('#chartHomeTotaisPorEstado').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    var initChartHomeTotaisPorCategoria = function() {
        var chart = AmCharts.makeChart("chartHomeTotaisPorCategoria", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [

                <?php
        if(!is_array($dataHomeTotPorCategoria)) $dataHomeTotPorCategoria = array();
        foreach ($dataHomeTotPorCategoria as $row => $val) : ?>
                {
                    "categoria":  '<?php echo $row; ?>',
                    "npedidos": <?php echo $val ; ?>
                },
                <?php endforeach; ?>
            ],
            "startEffect": "easeOutSine", // easeOutSine, easeInSine, elastic, bounce
            "startRadius":"90%",
            "valueField": "npedidos",
            "titleField": "categoria",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        jQuery('#chartHomeTotaisPorCategoria').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    return {
        //main function to initiate the module

        init: function() {

            initChartHomeTotaisPorEstado();

            initChartHomeTotaisPorCategoria();

            initChartHomeTotPorAnoMes();

        }
    };
}();

var TableDatatablesManaged = function () {

    var initTablePedidosSubmetidos = function () {

        var table = jQuery('#tabela_pedidossubmetidos');
        var tableTools = jQuery('#tabela_pedidossubmetidos_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            //lfrtip
            "dom": '<"wrapper"lf <"EstadoFilterDropBoxHome">  <"CatFilterDropBoxHome"> <"ModulosFilterDropBoxHome"> rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                    //"targets": [2]
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        var retVal = '<a href="' + row[6] + '">' + row[0] + '</a>';
                        if(row[9] != row[9]) {
                            retVal += '<span class="iconVDModified"></span>';
                            retVal += '<a href="javascript:;" class="btn btn-sm grey-salsa btn-outline popovers" data-toggle="popover" data-placement="top" data-trigger="hover" ';
                            retVal += ' data-content="<?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATACRIACAO');?> ' + row[9] + '\n <?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATAALTERACAO');?> ' + row[10]+'">';
                            retVal += '<i class="fa fa-info"></i></a>';
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_ALERTA_ESTADONAOINICIADO"); ?>';

                        switch (row[12]) {
                            case 'ALRT':
                                switch (row[7]) {
                                    case 'ALRT1':
                                        defCss = '<?php echo VirtualDeskSiteAlertaHelper::getAlertaEstadoCSS(1);?>';
                                        break;
                                    case 'ALRT2':
                                        defCss = '<?php echo VirtualDeskSiteAlertaHelper::getAlertaEstadoCSS(2);?>';
                                        break;
                                    case 'ALRT3':
                                        defCss = '<?php echo VirtualDeskSiteAlertaHelper::getAlertaEstadoCSS(3);?>';
                                        break;
                                }
                                break;
                            case 'AGND':
                                switch (row[7]) {
                                    case 'AGND1':
                                        defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(1);?>';
                                        break;
                                    case 'AGND2':
                                        defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(2);?>';
                                        break;
                                    case 'AGND3':
                                        defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(3);?>';
                                        break;
                                    case 'AGND4':
                                        defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(4);?>';
                                        break;
                                }
                                break;
                            case 'CULT':
                                switch (row[7]) {
                                    case 'CULT1':
                                        defCss = '<?php echo VirtualDeskSiteCulturaHelper::getCulturaEstadoCSS(1);?>';
                                        break;
                                    case 'CULT2':
                                        defCss = '<?php echo VirtualDeskSiteCulturaHelper::getCulturaEstadoCSS(2);?>';
                                        break;
                                    case 'CULT3':
                                        defCss = '<?php echo VirtualDeskSiteCulturaHelper::getCulturaEstadoCSS(3);?>';
                                        break;

                                }
                                break;
                            case 'TICK':
                                switch (row[7]) {
                                    case 'TICK1':
                                        defCss = '<?php echo VirtualDeskSiteTicketsHelper::getTicketsEstadoCSS(1);?>';
                                        break;
                                    case 'TICK2':
                                        defCss = '<?php echo VirtualDeskSiteTicketsHelper::getTicketsEstadoCSS(2);?>';
                                        break;
                                    case 'TICK3':
                                        defCss = '<?php echo VirtualDeskSiteTicketsHelper::getTicketsEstadoCSS(3);?>';
                                        break;
                                    case 'TICK4':
                                        defCss = '<?php echo VirtualDeskSiteTicketsHelper::getTicketsEstadoCSS(4);?>';
                                        break;
                                }
                                break;
                            case 'SUPT':
                                switch (row[7]) {
                                    case 'SUPT1':
                                        defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(1);?>';
                                        break;
                                    case 'SUPT2':
                                        defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(2);?>';
                                        break;
                                    case 'SUPT3':
                                        defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(3);?>';
                                        break;
                                    case 'SUPT4':
                                        defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(4);?>';
                                        break;
                                }
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>' ;
                        return (retVal);
                    }
                },
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 6,
                    "data": 6,
                    "orderable": false,
                    "render": function ( data, type, row, meta) {

                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';

                        return (returnLinki + data + returnLinkf);
                    }
                },
                {
                    "targets": 7,
                    "visible":false
                }
                ,
                {
                    "targets": 8,
                    "visible":false
                }
                ,
                {
                    "targets": 9,
                    "visible":false
                }
                ,
                {
                    "targets": 10,
                    "visible":false
                }
                ,
                {
                    "targets": 11,
                    "visible":false
                }
                ,
                {
                    "targets": 12,
                    "visible":false
                }
                ,
                {
                    "targets": 13,
                    "visible":false
                }
                ,
                {
                    "targets": 14,
                    "visible":false
                }



            ],
            "order": [
                [9, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=virtualdesk.getAllRequestList4ManagerByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

                // Filtrar ESTADO
                this.api().column(7).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_PAGINAHOME_FILTERBY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBoxHome').empty() )
                    jQuery('.EstadoFilterDropBoxHome select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar CATEGORIA
                this.api().column(14).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_PAGINAHOME_FILTERBY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($CatOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.CatFilterDropBoxHome').empty() )
                    jQuery('.CatFilterDropBoxHome select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });


                // Filtrar MODULOS
                this.api().column(13).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_PAGINAHOME_FILTERBY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($ModulosOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.ModulosFilterDropBoxHome').empty() )
                    jQuery('.ModulosFilterDropBoxHome select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_pedidossubmetidos_wrapper');

    }


    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTablePedidosSubmetidos();
        }

    };

}();


jQuery(document).ready(function() {
    ChartsAmcharts.init();

    TableDatatablesManaged.init();


});