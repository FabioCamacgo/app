<?php
defined('_JEXEC') or die;



?>
var ChartsAmcharts = function() {

    var initChartAlertaTotPorAnoMes = function() {
        var chart = AmCharts.makeChart("chartAlertaTotPorAnoMes", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',
            "color":    '#888',

            "dataProvider": [
            <?php foreach ($dataAlertaTotPorAnoMes as $row) : ?>
        {
            "year":  '<?php echo $row->year .' / '. $row->mes ; ?>',
            "npedidos": <?php echo $row->npedidos ; ?>
        },
    <?php endforeach; ?>


    ],
        "valueAxes": [{
            "axisAlpha": 0,
            "position": "left"
        }],
            "startDuration": 1,
            "graphs": [{
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] em [[category]]:<b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Nº de Pedidos",
            "type": "column",
            "valueField": "npedidos"
        }],
            "categoryField": "year",
            "categoryAxis": {
            "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
        }
    });

        jQuery('#chartAlertaTotPorAnoMes').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSample6 = function() {
        var chart = AmCharts.makeChart("chart_6", {
            "type": "pie",
            "theme": "light",

            "fontFamily": 'Open Sans',

            "color":    '#888',

            "dataProvider": [
            <?php foreach ($dataTotaisPorCat as $row) : ?>
        {
            "categoria":  '<?php echo $row->categoria; ?>',
            "npedidos": <?php echo $row->npedidos ; ?>
        },
    <?php endforeach; ?>
    ],
        "valueField": "npedidos",
            "titleField": "categoria",
            "exportConfig": {
            menuItems: [{
                icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                format: 'png'
            }]
        }
    });

        jQuery('#chart_6').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSample6_2 = function() {
        var chart = AmCharts.makeChart("chart_6_2", {
            "type": "pie",
            "theme": "light",

            "fontFamily": 'Open Sans',

            "color":    '#888',

            "dataProvider": [
            <?php foreach ($dataTotaisPorEstado as $row) : ?>
        {
            "estado":  '<?php echo $row->estado; ?>',
            "npedidos": <?php echo $row->npedidos ; ?>
        },
    <?php endforeach; ?>
    ],
        "valueField": "npedidos",
            "titleField": "estado",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
            menuItems: [{
                icon: '/lib/3/images/export.png',
                format: 'png'
            }]
        }
    });

        jQuery('#chart_6_2').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartAlertaTotaisPorEstado = function() {
        var chart = AmCharts.makeChart("chartAlertaTotaisPorEstado", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [
                <?php foreach ($dataAlertaTotaisPorEstado as $row) : ?>
                {
                    "estado":  '<?php echo $row->estado; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>
            ],
            "valueField": "npedidos",
            "titleField": "estado",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        jQuery('#chartAlertaTotaisPorEstado').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartAlertaTotaisPorFreguesia = function() {
        var chart = AmCharts.makeChart("chartAlertaTotaisPorFreguesia", {
            "type": "pie",
            "theme": "dark",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [
                <?php foreach ($dataAlertaTotaisPorFreguesia as $row) : ?>
                {
                    "freguesia":  '<?php echo $row->freguesia; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>
            ],
            "valueField": "npedidos",
            "titleField": "freguesia",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        jQuery('#chartAlertaTotaisPorFreguesia').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }

    var initChartAlertaTotaisPorCategoria = function() {
        var chart = AmCharts.makeChart("chartAlertaTotaisPorCategoria", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [
                <?php foreach ($dataAlertaTotaisPorCategoria as $row) : ?>
                {
                    "categoria":  '<?php echo $row->categoria; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>
            ],
            "valueField": "npedidos",
            "titleField": "categoria",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        jQuery('#chartAlertaTotaisPorCategoria').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    return {
        //main function to initiate the module

        init: function() {

           // initChartSample1();

           // initChartSample6();

            // initChartSample6_2();

            initChartAlertaTotaisPorEstado();

            initChartAlertaTotaisPorFreguesia();

            initChartAlertaTotaisPorCategoria();

            initChartAlertaTotPorAnoMes();

        }

    };

}();

jQuery(document).ready(function() {
    ChartsAmcharts.init();
});