<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Profile view class for Users.
 *
 * @since  1.6
 */
class VirtualDeskViewEnterprise extends JViewLegacy
{
	protected $data;
	protected $form;
	protected $params;
	protected $state;

	/**
	 * An instance of JDatabaseDriver.
	 *
	 * @var    JDatabaseDriver
	 * @since  3.6.3
	 */
	protected $db;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
        $LayoutWasOverrided = false;
        $LayoutAtual        = JFactory::getApplication()->input->get('layout');

        if( JFactory::getApplication()->input->get('task') == 'addnew' || $LayoutAtual=='addnew' ) {
            // Get the view data. --> VAZIO
            $this->data	            = $this->get('DataNew');
            $LayoutWasOverrided     = false;
        }
        else {
            // Get the view data -> TEM de TER DADOS... caso contrário tem de dar ERRO
            $this->data	            = $this->get('Data');

            // Se não tem dados e está a tentar chegar à lista deve ir para o defautl de forma a apresentar o ecrã de adicionar novo pedido
            if (empty($this->data) && $LayoutAtual=='list') {
                $this->setLayout('default');
                JFactory::getApplication()->input->get->set('layout','default');
                $LayoutWasOverrided = true;
            } else {
                // Verifica se tem Id ...
                $IdEnterprise = JFactory::getApplication()->input->getInt('enterprise_id');

                if( empty($LayoutAtual) || $LayoutAtual=='default' || $LayoutAtual == 'list')
                {   // Decide agora se não id é porque é para ver a lista de empresas, mas só se tiver  +1 registo é que vai para o layout "list"
                    if( empty($IdEnterprise) && is_array($this->data) ) {
                        if ( sizeof($this->data > 1 )) {
                            // Apresenta a lista por defeito por tem
                            $this->setLayout('list');
                            JFactory::getApplication()->input->get->set('layout','list');
                            $LayoutWasOverrided = true;
                        }
                        else // Só tem 1 registo então vai logo para o default
                        {   $this->setLayout('default');
                            JFactory::getApplication()->input->get->set('layout', 'default');
                            $LayoutWasOverrided = true;
                        }
                    }
                    else {
                        $this->setLayout('default');
                        JFactory::getApplication()->input->get->set('layout', 'default');
                        $LayoutWasOverrided = true;
                    }
                }
                elseif (JFactory::getApplication()->input->get('layout') == 'edit' && (int)$IdEnterprise > 0 ) {
                    $this->setLayout('edit');
                    JFactory::getApplication()->input->get->set('layout', 'edit');
                    $LayoutWasOverrided = true;
                }
                elseif( JFactory::getApplication()->input->get('layout') == 'edit' && (int) $IdEnterprise <=0 ) {
                    // Se está a tentar chegar ao layout de edit terá de ter definido um id de enterprise
                    JFactory::getApplication()->enqueueMessage(JText::_('COM_VIRTUALDESK_ENTERPRISE_NOTFOUND'), 'error');
                    return false;
                }
            }


        }

		$this->state            = $this->get('State');
		$this->params           = $this->state->get('params');
		$this->db               = JFactory::getDbo();

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
//			JError::raiseError(500, implode('<br />', $errors));
//			return false;

            $app = JFactory::getApplication();
            $app->logout();
            $redirect = JUri::root();
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_SESSIONENDED'), 'alert');
            $app->redirect(JRoute::_($redirect, false));
            return false;
            exit();

		}

		// Check for layout override
		$active = JFactory::getApplication()->getMenu()->getActive();

		if (isset($active->query['layout']) && $LayoutWasOverrided == false)
		{
			$this->setLayout($active->query['layout']);
		}

		// Escape strings for HTML output
		//$this->pageclass_sfx = htmlspecialchars($this->params->get('pageclass_sfx'));

		$this->prepareDocument();

		return parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$user  = JFactory::getUser();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();


		    //switch ( JFactory::getApplication()->input->get('layout') )
            switch ( $this->getLayout() )
            {
               case "addnew":
                   $this->params->def('page_heading', JText::_('COM_VIRTUALDESK_ENTERPRISE_NEW_TITLE'));
                    break;
                case "edit":
                    $this->params->def('page_heading', JText::_('COM_VIRTUALDESK_ENTERPRISE_EDIT_TITLE'));
                    break;
                case "list":
                    $this->params->def('page_heading', JText::_('COM_VIRTUALDESK_ENTERPRISE_LIST_TITLE'));
                    break;
                default:
                    if ($menu) {
                        $this->params->def('page_heading', $this->params->get('page_title', $user->name));
                    }
                    else {
                        $this->params->def('page_heading', JText::_('COM_VIRTUALDESK_ENTERPRISE_TITLE'));
                    }
                    break;
            }


		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}
}
