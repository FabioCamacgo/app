<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteEnterpriseFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_enterprisefields.php');

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/enterprise/tmpl/edit.js'  . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/enterprise/tmpl/maps.js'  . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/components-select2.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;



$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');


//Parâmetros
    $params = JComponentHelper::getParams('com_virtualdesk');
    $useravatar_extensions = $params->get('useravatar_extensions');
    $useravatar_max_size_Mb = (int)$params->get('useravatar_max_size');
    $useravatar_max_size = (int)$params->get('useravatar_max_size') * 1024 * 1024;
    $useravatar_width = $params->get('useravatar_width');
    $useravatar_height = $params->get('useravatar_height');
    $enterpriseavatar_defaultimg = $params->get('enterpriseavatar_defaultimg');

//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->enterpriseavatar)) if (is_array($this->data->enterpriseavatar)) $this->data->enterpriseavatar = '';

//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


// Definição dos campos de utilizador
$ObjEnterpriseFields = new VirtualDeskSiteEnterpriseFieldsHelper();
$arEnterpriseFieldsConfig = $ObjEnterpriseFields->getFields();

// Multichoice / select2 : Áreas de Atuação
$EnterpriseAreaActListAllNyGroup = VirtualDeskSiteEnterpriseHelper::getEnterpriseAreaActListAllByGroup($fileLangSufix);
?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput{display:block;}
    .fileinput-preview {display:block; max-height: 100%; }
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.cancel'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
            <a href="#" class="btn btn-circle btn-outline red">
                <i class="fa fa-trash-o"></i>  <?php echo JText::_('COM_VIRTUALDESK_DELETE'); ?> </a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>

    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <!-- Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js) -->
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <script>
        // mensagem para a validação de um campo tipo nif
        var virtualDeskNIFCheckOptions = {
            message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'); ?>"
        };
    </script>


    <div class="portlet-body form">

        <form id="form-enterprise"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.update'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="form-body">

                <div class="row">
                    <div class="col-md-6">


                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NIPC_LABEL'); ?></label>
                            <div class="col-md-9">
                                <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NIPC_LABEL'); ?>" name="nipc"
                                       id="nipc" maxlength="150"
                                       value="<?php  if(!empty($this->data->nipc)) echo htmlentities($this->data->nipc, ENT_QUOTES, 'UTF-8'); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_FIRMNAME_LABEL'); ?></label>
                            <div class="col-md-9">
                                <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_FIRMNAME_LABEL'); ?>" name="firmname"
                                       id="firmname" maxlength="150"
                                       value="<?php  if(!empty($this->data->firmname)) echo htmlentities($this->data->firmname, ENT_QUOTES, 'UTF-8'); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NAME_LABEL'); ?></label>
                            <div class="col-md-9">
                                <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NAME_LABEL'); ?>" name="name"
                                       id="name" maxlength="250"
                                       value="<?php  if(!empty($this->data->name)) echo htmlentities($this->data->name, ENT_QUOTES, 'UTF-8'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_EMAIL_LABEL'); ?></label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <div class="input-icon">
                                        <i class="fa fa-envelope fa-fw"></i>
                                        <input type="email" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_EMAIL_LABEL'); ?>"
                                               name="email" id="email" maxlength="250"
                                               value="<?php if(!empty($this->data->email)) echo htmlentities($this->data->email, ENT_QUOTES, 'UTF-8'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>





                    </div>

                    <div class="col-md-6">

                            <div class="profile-userpic">

                                <?php if (empty($this->data->enterpriseavatar)) : ?>
                                      <img id="loadedAvatarImg" class="img-responsive" src="<?php echo $enterpriseavatar_defaultimg ?>"/>
                                <?php else : ?>
                                      <img id="loadedAvatarImg" class="img-responsive" src="<?php  if(!empty($this->data)) echo $this->params->get('useravatar_folder') . DS . $this->data->enterpriseavatar ?>"/>
                                <?php endif; ?>

                                <div class="fileinput fileinput-new" data-provides="fileinput">

                                    <div class="fileinput-preview fileinput-exists " style="<?php echo $useravatar_width; ?>px; max-height: <?php echo $useravatar_height; ?>px; line-height: 10px;"></div>
                                    <div style="text-align: center;">
                                <span class="btn default btn-file">
                                     <span class="fileinput-new"> <?php echo JText::_('COM_VIRTUALDESK_PROFILE_AVATAR_SELECTIMAGE'); ?> </span>
                                     <span class="fileinput-exists"> <?php echo JText::_('COM_VIRTUALDESK_PROFILE_AVATAR_ALTERAR'); ?> </span>
                                     <input type="hidden" value=""  name="...">
                                     <input type="file"  name="enterpriseavatar" id="enterpriseavatar">
                                </span>
                                        <a href="javascript:;" class="btn default fileinput-exists"   data-dismiss="fileinput"><?php echo JText::_('COM_VIRTUALDESK_PROFILE_AVATAR_REMOVE'); ?></a>
                                    </div>
                                </div>

                            </div>


                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">


                        <?php foreach ($ObjEnterpriseFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                            <?php if($arEnterpriseFieldsConfig['EnterpriseField_' . $valConfFieldNames ]===true) : ?>
                                <?php if($valConfFieldNames == 'ADDRESS') : ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL'); ?></label>
                                        <div class="col-md-9">
                                           <textarea <?php echo $arEnterpriseFieldsConfig['EnterpriseField_' . $valConfFieldNames . '_Required']; ?>
                                                   class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL'); ?>"
                                                   name="<?php echo $keyConfFieldNames; ?>"  id="<?php echo $keyConfFieldNames; ?>" maxlength="250"><?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?></textarea>
                                        </div>
                                    </div>

                                    <?php elseif ($valConfFieldNames == 'AREAACT') : ?>
                                    <div class="form-group">
                                        <div class="form-group">

                                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL'); ?></label>
                                            <div class="col-md-9">

                                                <div class="input-group select2-bootstrap-prepend">
                                                <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-select2-open="<?php echo $keyConfFieldNames; ?>">
                                                            <span class="glyphicon glyphicon-search"></span>
                                                        </button>
                                                    </span>

                                                <select name="<?php echo $keyConfFieldNames; ?>[]"  id="<?php echo $keyConfFieldNames; ?>" class="form-control input-lg select2-multiple select2-hidden-accessible" multiple tabindex="-1" aria-hidden="true">
                                                    <option></option>
                                                    <?php foreach($EnterpriseAreaActListAllNyGroup as $rowGroupAreaAct) : ?>
                                                    <optgroup label="<?php echo $rowGroupAreaAct['group']['groupname']; ?>">
                                                        <?php foreach($rowGroupAreaAct['rows'] as  $rowAreaAct) : ?>
                                                        <option value="<?php echo $rowAreaAct['id']; ?>"
                                                        <?php if(is_array($this->data->areaact)) {
                                                             if( in_array($rowAreaAct['id'],$this->data->areaact) ) echo 'selected';
                                                           }
                                                        ?>
                                                        ><?php echo $rowAreaAct['name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                                </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php elseif ($valConfFieldNames == 'ATTACHMENT') : ?>
                                    <?php $userReqFileList = VirtualDeskSiteEnterpriseHelper::getEnterpriseFilesLayout ($this->data); ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <div class="col-md-9">
                                            <?php require_once( JPATH_SITE . '/components/com_virtualdesk/helpers/html/tpl_enterprise_file_list_edit.php'); ?>
                                        </div>
                                    </div>

                                <?php else : ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL'); ?></label>
                                        <div class="col-md-9">
                                            <input type="text" <?php echo $arEnterpriseFieldsConfig['EnterpriseField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                                   placeholder="<?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL'); ?>"
                                                   name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>" maxlength="8"
                                                   value="<?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?>"/>
                                        </div>
                                    </div>
                            <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>


                    </div>

                    <div class="col-md-6">
                        <p></p>
                    </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">

                                    <div class="col-md-12">
                                        <div id="gmap_basic" class="gmaps" style="position: relative; overflow: hidden;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-6"></div>

                </div>


                <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SAVECHANGES'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.cancel'); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="enterprise_id" value="<?php echo $this->escape($this->data->enterprise_id); ?>"/>
                <input type="hidden" name="option" value="com_virtualdesk"/>
                <input type="hidden" name="task" value="enterprise.update"/>
                <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>


<?php echo $localScripts; ?>


<script>
    jQuery(document).ready(function() {

        var uploadFile;
        // edit this line to the id of the file element
        uploadFile = 'enterpriseavatar';
        uploadFile = jQuery('#'+uploadFile);
        uploadFile.on('change', function() {
            var files, reader;
            files = !!this.files ? this.files : [];
            if ( !files.length || !window.FileReader ) {
                jQuery('img#loadedAvatarImg').show();
                return; // no file selected, or no FileReader support
            }
            var fsize = files[0].size;
            if( fsize><?php echo $useravatar_max_size; ?>) //do something if file size more than 1 mb (1048576)
            {
                swal("<?php echo JText::_('COM_VIRTUALDESK_USERAVATAR_FILE_TOO_LARGE') . $useravatar_max_size_Mb . "Mb"; ?>",'',"warning");
                uploadFile.val('');
                jQuery('img#loadedAvatarImg').show();
                return(false);
            }

            //get the file size and file type from file input field
            fsize = files[0].size;
            var ftype = files[0].type;
            fname = files[0].name;

            switch(ftype)
            {
                case 'image/png':
                case 'image/gif':
                case 'image/jpeg':
                    break;
                default:
                    swal("<?php echo JText::_('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'); ?>",'',"warning");
                    uploadFile.val('');
                    jQuery('img#loadedAvatarImg').show();
                    return(false);
            }

            if ( /^image/.test(files[0].type) ) { // only image file
                // retira a imagem anterior
                jQuery('img#loadedAvatarImg').hide();
            }

        });

    });
</script>