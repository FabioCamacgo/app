<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');

?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: <?php echo $useravatar_height; ?>px;}
    .fileinput{display:block;}
    .fileinput-preview {display:block; max-height: 100%; }
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.addnew'); ?>" class="btn btn-circle blue-steel btn-outline">
                <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
        </div>

    </div>


    <div class="portlet-body ">


        <div class="table-scrollable">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th> <?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_FIRMNAME_LABEL'); ?> </th>
                    <th> <?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NAME_LABEL'); ?> </th>
                    <th> <?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NIPC_LABEL'); ?> </th>
                    <th> <?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_EMAIL_LABEL'); ?> </th>
                </tr>
                </thead>

                <tbody>

<?php foreach($this->data as $enterprise) :    ?>

                <tr>
                    <td><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&enterprise_id=' . $enterprise->enterprise_id); ?>" > <?php echo $enterprise->firmname; ?> </a> </td>
                    <td><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&enterprise_id=' . $enterprise->enterprise_id); ?>" > <?php echo $enterprise->name; ?> </a> </td>
                    <td> <?php echo htmlentities( $enterprise->nipc, ENT_QUOTES, 'UTF-8');?> </td>
                    <td> <?php echo htmlentities( $enterprise->email, ENT_QUOTES, 'UTF-8');?> </td>
                    <td>
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&enterprise_id=' . $enterprise->enterprise_id); ?>" class=" btn btn-sm grey-salsa btn-outline sbold uppercase">
                            <i class="fa fa-share"></i> View </a>
                    </td>
                </tr>

    <?php endforeach;    ?>

                </tbody>
            </table>
        </div>




    </div>

</div>

<?php

echo $localScripts;

?>