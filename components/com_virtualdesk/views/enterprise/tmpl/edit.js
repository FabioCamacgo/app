/*
* Define estrutura da validação.
* A validação é iniciada depois no final deste ficheiro.
* Foi acrescentado antes da inicialização um método ( .addMethod("vdpasscheck"... ) que permite fazer a validação específica da password.
*/
var EnterpriseEdit = function() {

    var handleEnterpriseEdit = function() {

        jQuery('#form-enterprise').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   

                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = MessageAlert.getRequiredMissed(errors);
                        var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                        MainMessageBlock.find('span').html(message);
                        MainMessageBlock.show();
                        App.scrollTo(MainMessageBlock, -200);
                    }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

              if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                 form.submit();
            }
        });

        jQuery('#form-enterprise').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#form-enterprise').validate().form()) {
                    jQuery('#form-enterprise').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleEnterpriseEdit();
        }
    };

}();


/*
 * Inicialização do Select 2
 */
var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
           // placeholder: placeholder,
            width: null
        });

        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();



var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater').each(function(){
                //console.log('a')
                jQuery(this).repeater({

                    show: function () {
                        $(this).slideDown();
                    },

                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });
        }

    };

}();



/*
* Inicialização da validação
*/

jQuery(document).ready(function() {

    jQuery.validator.addMethod("vdnifcheck", function(value, element, params) {
        var NIFElem = null;
        if(params[1]!='') NIFElem =  jQuery('#'+params[1]);
        // Se o campo não estiver preenchido não deve dar erro de NIF
        if(NIFElem.val()=='' || NIFElem.val()==undefined) return true;

        var resNIFCheckJS = jQuery().virtualDeskNIFCheck(NIFElem.val(),virtualDeskNIFCheckOptions);
        params[0] = resNIFCheckJS.message;
        return resNIFCheckJS.return ;
    }, jQuery.validator.format(" {0} "));

   // Validações
   EnterpriseEdit.init();

   // Select 2
   ComponentsSelect2.init();

   // Repeater
   FormRepeater.init();

});