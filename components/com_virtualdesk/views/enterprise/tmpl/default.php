<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteEnterpriseFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_enterprisefields.php');

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/enterprise/tmpl/maps.js'  . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');



//Parâmetros
    $params = JComponentHelper::getParams('com_virtualdesk');
    $useravatar_width = $params->get('useravatar_width');
    $useravatar_height = $params->get('useravatar_height');
    $enterpriseavatar_defaultimg = $params->get('enterpriseavatar_defaultimg');
    $labelseparator = ' : ';

if(!empty($this->data)) {

    // se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->enterpriseavatar)) {
    if(is_array($this->data->enterpriseavatar)) $this->data->enterpriseavatar = '';
    }
}

// Definição dos campos de utilizador
$ObjEnterpriseFields = new VirtualDeskSiteEnterpriseFieldsHelper();
$arEnterpriseFieldsConfig = $ObjEnterpriseFields->getFields();

// Multichoice / select2 : Áreas de Atuação
$EnterpriseAreaActListByName = VirtualDeskSiteEnterpriseHelper::getEnterpriseAreaActListSelectName ($this->data, $fileLangSufix);
?>

<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.cancel'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=edit&enterprise_id=' . $this->escape($this->data->enterprise_id)); ?>" class="btn btn-circle btn-outline green">
                <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.addnew'); ?>" class="btn btn-circle blue-steel btn-outline">
                <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?>  </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>

    </div>

    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">
            <div class="form-body">



               <?php
               // Data not empty ?
               if(!empty($this->data)) :
               ?>

                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_NIPC_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->nipc, ENT_QUOTES, 'UTF-8')?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_FIRMNAME_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->firmname, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_NAME_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->name, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_EMAIL_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->email, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>



                    </div>

                    <div class="col-md-6">
                        <?php if( empty($this->data->enterpriseavatar) ) : ?>
                            <div class="profile-userpic" style="font-size: 100px;">
                                <img class="img-responsive" src="<?php echo $enterpriseavatar_defaultimg; ?>"/>
                            </div>
                        <?php else : ?>
                            <div class="profile-userpic">
                                <img class="img-responsive"  src="<?php  echo $this->params->get('useravatar_folder'). DS . $this->data->enterpriseavatar ?>" />
                            </div>
                        <?php endif; ?>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">



                <?php foreach ($ObjEnterpriseFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                    <?php if($arEnterpriseFieldsConfig['EnterpriseField_' . $valConfFieldNames ]===true) : ?>
                    <?php if ($valConfFieldNames == 'AREAACT') : ?>

                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php  if(!is_array($EnterpriseAreaActListByName)) $EnterpriseAreaActListByName = array();
                                            echo htmlentities(implode(', ',$EnterpriseAreaActListByName), ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>

                    <?php elseif ( $valConfFieldNames == 'ATTACHMENT' ) : ?>

                            <?php $userReqFileList = VirtualDeskSiteEnterpriseHelper::getEnterpriseFilesLayout ($this->data); ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                <div class="col-md-8">
                                    <?php require_once( JPATH_SITE . '/components/com_virtualdesk/helpers/html/tpl_enterprise_file_list.php'); ?>
                                </div>
                            </div>

                    <?php else : ?>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_ENTERPRISE_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php echo htmlentities( $this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>

                    <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div id="gmap_basic" class="gmaps" style="position: relative; overflow: hidden;"></div>
                            </div>
                        </div>
                    </div>



                </div>



            </div>



            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=enterprise&layout=edit&enterprise_id=' . $this->escape($this->data->enterprise_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=enterprise'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="enterprise_id" value="<?php echo $this->escape($this->data->enterprise_id); ?>"/>

            <?php
            // Data empty ?
            else :
            ?>
                <div class="form-actions right">
                    <div class="row">




                        <div  class="col-md-12 text-left">
                            <div class="alert alert-info fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                                <?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_EMPTYLIST'); ?>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.addnew'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NEW'); ?>"><?php echo JText::_('COM_VIRTUALDESK_ENTERPRISE_NEW'); ?></a>
                            <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=enterprise.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                        </div>
                    </div>
                </div>
            <?php
            endif;
            ?>


        </form>
    </div>
</div>


<?php echo $localScripts; ?>