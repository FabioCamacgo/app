<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/**
 *  view class for Users.
 *
 * @since  1.6
 */
class VirtualDeskViewUrbanismo extends JViewLegacy
{
	protected $data;
	protected $form;
	protected $params;
	protected $state;

	/**
	 * An instance of JDatabaseDriver.
	 *
	 * @var    JDatabaseDriver
	 * @since  3.6.3
	 */
	protected $db;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkViewAccess('urbanismo');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $LayoutWasOverrided = false;
        $LayoutAtual        = JFactory::getApplication()->input->get('layout');
        $TaskAtual          = JFactory::getApplication()->input->get('task');

        // Verifica qwual o layout a apresentar baseado no parametro layout, task e nos dados existentes...
        $resCheckTL = $this->checkTasksAndLayouts ($LayoutWasOverrided, $LayoutAtual, $TaskAtual );
        if($resCheckTL===false) return(false);


		$this->state            = $this->get('State');
		$this->params           = $this->state->get('params');
		$this->db               = JFactory::getDbo();

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
            $app = JFactory::getApplication();
            $app->logout();
            $redirect = JUri::root();
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_SESSIONENDED'), 'alert');
            $app->redirect(JRoute::_($redirect, false));
            return false;
            exit();

		}

		// Check for layout override
		$active = JFactory::getApplication()->getMenu()->getActive();
		if (isset($active->query['layout']) && $LayoutWasOverrided == false)
		{
			$this->setLayout($active->query['layout']);
		}


		// Escape strings for HTML output
		//$this->pageclass_sfx = htmlspecialchars($this->params->get('pageclass_sfx'));

		$this->prepareDocument();

		return parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$user  = JFactory::getUser();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();


		    //switch ( JFactory::getApplication()->input->get('layout') )
            switch ( $this->getLayout() )
            {

                default:
                    $this->params->def('page_heading', JText::_('COM_VIRTUALDESK_URBANISMO_HEADING'));

                    break;
            }


		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}


    /**
     * Prepares the document
     *
     * @return  boolean
     *
     * @since   1.6
     */
    protected function checkTasksAndLayouts (&$LayoutWasOverrided, $LayoutAtual='', $TaskAtual='' ) {

        switch ( $LayoutAtual )
        {
            case "view4user":
            case "view4manager":
            case "edit4user":
            case "edit4manager":
            case "addnew4user":
            case "addnew4manager":
            case "menu3autorizacaoutilizacao4user":
            case "menu3certidoespareceres4user":
            case "menu3comunicacaoprevia4user":
            case "menu3conservacaoimoveis4user":
            case "menu3direitoinformacao4user":
            case "menu3estacoesradiocomunicacoes4user":
            case "menu3informacaoprevia4user":
            case "menu3inspecoesauditorias4user":
            case "menu3legalizacao4user":
            case "menu3licencaadministrativa4user":
            case "menu3licencaespecial4user":
            case "menu3obrasisentascontroloprevio4user":
            case "menu3ocupacaoviapublica4user":
            case "menu3outrosrequerimentos4user":
            case "menu3toponimia4user":
            case "addnewalteracaoutilizacao4user":
            case "addnewautilizacaorecintos4user":
            case "addnewutilizacaorealizacao4user":
            case "addnewutilizacaonaoprecedida4user":
            case "addnewemissaoalvara4user":
            case "addnewprorrogacaoprazo4user":
            case "addnewparecercompropriedade4user":
            case "addnewdestaqueparcela4user":
            case "addnewprojetoemparcelamento4user":
            case "addnewisencaoautorizacao4user":
            case "addnewpropriedadehorizontal4user":
            case "addnewcertidaocomprovativa4user":
            case "addnewcertidaocedencia4user":
            case "addnewcertidaourbanismo4user":
            case "addnewcompatibilidade4user":
            case "addnewdenuncia4user":
            case "addnewexposicao4user":
            case "addnewreclamacao4user":
            case "addnewinfraestruturasaptas4user":
            case "addnewobrasdemolicao4user":
            case "addnewobrasedificacao4user":
            case "addnewobrasurbanizacao4user":
            case "addnewoperacoesloteamento4user":
            case "addnewoutrasoperacoes4user":
            case "addnewremodelacaoterrenos4user":
            case "addnewcertidaolocalizacao4user":
            case "addnewvistoriainicial4user":
            case "addnewdefinicaoobras4user":
            case "addnewvistoriafinal4user":
            case "addnewconsultareproducao4user":
            case "addnewcondicionamentosinformacao4user":
            case "addnewacessorios4user":
            case "addnewacessoriosedificacoes4user":
            case "addnewdeclaracaomanutencao4user":
            case "addnewviabilidaderealizar4user":
            case "addnewpreviaalteracaoutilizacao4user":
            case "addnewpreviaobrasdemolicao4user":
            case "addnewpreviaobrasedificacao4user":
            case "addnewpreviaobrasurbanizacao4user":
            case "addnewpreviaoperacoesloteamento4user":
            case "addnewpreviaoutrasoperacoes4user":
            case "addnewpreviaremodelacaoterrenos4user":
            case "addnewarranjoestetico4user":
            case "addnewquesitos4user":
            case "addnewprojetoarquitetura4user":
            case "addnewarquiteturaespecialidades4user":
            case "addnewespecialidades4user":
            case "addnewoperacoesurbanisticas4user":
            case "addnewutilizacaoedificios4user":
            case "addnewinformacaotermos4user":
            case "addnewladminprojetoarquitetura4user":
            case "addnewladminarquiteturaespecialidades4user":
            case "addnewladminprojetoespecialidades4user":
            case "addnewladminobrasdemolicao4user":
            case "addnewladminobrasurbanizacao4user":
            case "addnewladminoperacoesloteamento4user":
            case "addnewladminremodelacaoterrenos4user":
            case "addnewladminoutrasoperacoes4user":
            case "addnewladminexecucaotrabalhos4user":
            case "addnewladminprorrogacaoespecialidades4user":
            case "addnewladminprorrogacaolicenca4user":
            case "addnewladminalvaraparcial4user":
            case "addnewladminalvarademolicao4user":
            case "addnewladminalvaraedificacao4user":
            case "addnewladminalvaraurbanizacao4user":
            case "addnewladminalvarasemobras4user":
            case "addnewladminalvaracomobras4user":
            case "addnewladminalvaraoutras4user":
            case "addnewladminalvararemodelacao4user":
            case "addnewladminalteracaoedificacao4user":
            case "addnewladminobrasurbanizacaoalteracao4user":
            case "addnewladminobrasedificacao4user":
            case "addnewladmincomunicacaodurante4user":
            case "addnewlicencaespecial4user":
            case "addnewobrasisentas4user":
            case"addnewoperacoesurbanisticasparecer4user":
            case "addnewocupacaoespaco4user":
            case "addnewcoordenadorrojetos4user":
            case "addnewdiretorobra4user":
            case "addnewdiretorfiscalizacao4user":
            case "addnewsubstituicaorequerente4user":
            case "addnewsubstituicaotitular4user":
            case "addnewdesistencia4user":
            case "addnewdevolucaocaucao4user":
            case "addnewhabitacaodeposito4user":
            case "addnewhabitacaoemissao4user":
            case "addnewiniciotrabalhos4user":
            case "addnewjuncaoelementos4user":
            case "addnewprazosexecucao4user":
            case "addnewprazosjuncao4user":
            case "addnewrececaodefinitiva4user":
            case "addnewrececaoprovisoria4user":
            case "addnewreducaomontante4user":
            case "addnewnumeropolicia4user":
                $this->data = array();
                $LayoutWasOverrided = true;
            break;

            default:
            break;
        }

        return(true);
    }


}
