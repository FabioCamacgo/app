
<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUrbanismoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_urbanismo.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('urbanismo', 'list4users');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>

<div class="portlet light bordered">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TAB_COMUNICACAOPREVIA'); ?></span>
        </div>

        <div class="actions">
            <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"
               title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>">
                <i class="fa fa-arrow-left"></i>
                <?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
        </div>

    </div>

    <div class="portlet-body ">

        <div class="tabbable-line nav-justified ">

            <div class="tab-content">
                <div class="row">

                    <?php
                        $Enabled_infraestruturasAptas = VirtualDeskSiteFormmainHelper::checkFormEnabled('1rvx9vax');
                        $Enabled_obrasDemolicao = VirtualDeskSiteFormmainHelper::checkFormEnabled('jw2z93q5');
                        $Enabled_obrasEdificacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('jado4qsu');
                        $Enabled_obrasUrbanizacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('7o63np06');
                        $Enabled_operacoesLoteamento = VirtualDeskSiteFormmainHelper::checkFormEnabled('l4e9r39p');
                        $Enabled_outrasOperacoes = VirtualDeskSiteFormmainHelper::checkFormEnabled('5sg5rl0i');
                        $Enabled_remodelacaoTerrenos = VirtualDeskSiteFormmainHelper::checkFormEnabled('ds0z391h');
                    ?>



                    <?php if($Enabled_infraestruturasAptas == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('1rvx9vax'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('1rvx9vax'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewinfraestruturasaptas4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_obrasDemolicao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('jw2z93q5'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('jw2z93q5'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewobrasdemolicao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_obrasEdificacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('jado4qsu'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('jado4qsu'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewobrasedificacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_obrasUrbanizacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('7o63np06'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('7o63np06'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewobrasurbanizacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_operacoesLoteamento == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('l4e9r39p'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('l4e9r39p'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewoperacoesloteamento4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_outrasOperacoes == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('5sg5rl0i'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('5sg5rl0i'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewoutrasoperacoes4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_remodelacaoTerrenos == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('ds0z391h'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('ds0z391h'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewremodelacaoterrenos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>
</div>

<?php
echo $localScripts;
?>
