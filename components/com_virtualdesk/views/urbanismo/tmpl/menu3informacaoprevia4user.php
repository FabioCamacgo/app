<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUrbanismoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_urbanismo.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('urbanismo', 'list4users');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>

<div class="portlet light bordered">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TAB_INFORMACAOPREVIA'); ?></span>
        </div>

        <div class="actions">
            <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"
               title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>">
                <i class="fa fa-arrow-left"></i>
                <?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
        </div>

    </div>

    <div class="portlet-body ">

        <div class="tabbable-line nav-justified ">

            <div class="tab-content">
                <div class="row">

                    <?php
                    $Enabled_declaracaoManutencao = VirtualDeskSiteFormmainHelper::checkFormEnabled('ng7twmrc');
                    $Enabled_viabilidadeRealizar = VirtualDeskSiteFormmainHelper::checkFormEnabled('h31g90i0');
                    $Enabled_previaAlteracaoUtilizacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('r95gypja');
                    $Enabled_previaObrasDemolicao = VirtualDeskSiteFormmainHelper::checkFormEnabled('48paz7cp');
                    $Enabled_previaObrasEdificacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('4hq8b8f7');
                    $Enabled_previaObrasUrbanizacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('s3u56xlp');
                    $Enabled_previaOperacoesLoteamento = VirtualDeskSiteFormmainHelper::checkFormEnabled('mpq2kqex');
                    $Enabled_previaOutrasOperacoes = VirtualDeskSiteFormmainHelper::checkFormEnabled('ptrtbk6h');
                    $Enabled_previaRemodelacaoTerrenos = VirtualDeskSiteFormmainHelper::checkFormEnabled('cg6x2bh6');

                    ?>


                    <?php if($Enabled_declaracaoManutencao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('ng7twmrc'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('ng7twmrc'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewdeclaracaomanutencao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_viabilidadeRealizar == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('h31g90i0'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('h31g90i0'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewviabilidaderealizar4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_previaAlteracaoUtilizacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('r95gypja'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('r95gypja'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewpreviaalteracaoutilizacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_previaObrasDemolicao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('48paz7cp'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('48paz7cp'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewpreviaobrasdemolicao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_previaObrasEdificacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('4hq8b8f7'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('4hq8b8f7'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewpreviaobrasedificacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_previaObrasUrbanizacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('s3u56xlp'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('s3u56xlp'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewpreviaobrasurbanizacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_previaOperacoesLoteamento == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('mpq2kqex'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('mpq2kqex'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewpreviaoperacoesloteamento4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_previaOutrasOperacoes == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('ptrtbk6h'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('ptrtbk6h'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewpreviaoutrasoperacoes4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_previaRemodelacaoTerrenos == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('cg6x2bh6'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('cg6x2bh6'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewpreviaremodelacaoterrenos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>
</div>

<?php
echo $localScripts;
?>


