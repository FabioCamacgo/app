<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUrbanismoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_urbanismo.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('urbanismo', 'list4users');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>

<div class="portlet light bordered">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TAB_LICENCAADMINISTRATIVA'); ?></span>
        </div>

        <div class="actions">
            <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"
               title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>">
                <i class="fa fa-arrow-left"></i>
                <?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
        </div>

    </div>

    <div class="portlet-body ">

        <div class="tabbable-line nav-justified ">

            <div class="tab-content">
                <div class="row">

                    <?php
                        $Enabled_ladminprojetoArquitetura = VirtualDeskSiteFormmainHelper::checkFormEnabled('3lvxrwaq');
                        $Enabled_ladminarquiteturaEspecialidades = VirtualDeskSiteFormmainHelper::checkFormEnabled('844cv53j');
                        $Enabled_ladminprojetoEspecialidades = VirtualDeskSiteFormmainHelper::checkFormEnabled('pfpgzzgc');
                        $Enabled_ladminobrasDemolicao = VirtualDeskSiteFormmainHelper::checkFormEnabled('okwwsq5m');
                        $Enabled_ladminobrasUrbanizacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('l0cpsqvd');
                        $Enabled_ladminoperacoesLoteamento = VirtualDeskSiteFormmainHelper::checkFormEnabled('rlp1o824');
                        $Enabled_ladminremodelacaoTerrenos = VirtualDeskSiteFormmainHelper::checkFormEnabled('loq8a5iv');
                        $Enabled_ladminoutrasOperacoes = VirtualDeskSiteFormmainHelper::checkFormEnabled('k5z8mu21');
                        $Enabled_ladminexecucaoTrabalhos = VirtualDeskSiteFormmainHelper::checkFormEnabled('cx391kk4');
                        $Enabled_ladminprorrogacaoEspecialidades = VirtualDeskSiteFormmainHelper::checkFormEnabled('lyiw2fc4');
                        $Enabled_ladminprorrogacaoLicenca = VirtualDeskSiteFormmainHelper::checkFormEnabled('2fxjxr3g');
                        $Enabled_ladminalvaraParcial = VirtualDeskSiteFormmainHelper::checkFormEnabled('5v56e34f');
                        $Enabled_ladminalvaraDemolicao = VirtualDeskSiteFormmainHelper::checkFormEnabled('gbmpjjwg');
                        $Enabled_ladminalvaraEdificacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('8jaoa9fe');
                        $Enabled_ladminalvaraUrbanizacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('70weql3s');
                        $Enabled_ladminalvaraSemObras = VirtualDeskSiteFormmainHelper::checkFormEnabled('23nefgt6');
                        $Enabled_ladminalvaraComObras = VirtualDeskSiteFormmainHelper::checkFormEnabled('et5ntiv7');
                        $Enabled_ladminalvaraOutras = VirtualDeskSiteFormmainHelper::checkFormEnabled('d6k9w6a3');
                        $Enabled_ladminalvaraRemodelacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('j5hrm1k3');
                        $Enabled_ladminalteracaoEdificacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('lcx345r4');
                        $Enabled_ladminobrasUrbanizacaoAlteracao = VirtualDeskSiteFormmainHelper::checkFormEnabled('aa3hyqnk');
                        $Enabled_ladminobrasEdificacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('sttnq56u');
                        $Enabled_ladmincomunicacaoDurante = VirtualDeskSiteFormmainHelper::checkFormEnabled('jakarg1j');
                    ?>



                    <?php if($Enabled_ladminprojetoArquitetura == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('3lvxrwaq'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('3lvxrwaq'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminprojetoarquitetura4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminarquiteturaEspecialidades == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('844cv53j'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('844cv53j'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminarquiteturaespecialidades4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminprojetoEspecialidades == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('pfpgzzgc'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('pfpgzzgc'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminprojetoespecialidades4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminobrasDemolicao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('okwwsq5m'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('okwwsq5m'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminobrasdemolicao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminobrasUrbanizacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('l0cpsqvd'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('l0cpsqvd'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminobrasurbanizacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminoperacoesLoteamento == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('rlp1o824'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('rlp1o824'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminoperacoesloteamento4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminremodelacaoTerrenos == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('loq8a5iv'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('loq8a5iv'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminremodelacaoterrenos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminoutrasOperacoes == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('k5z8mu21'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('k5z8mu21'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminoutrasoperacoes4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminexecucaoTrabalhos == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('cx391kk4'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('cx391kk4'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminexecucaotrabalhos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminprorrogacaoEspecialidades == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('lyiw2fc4'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('lyiw2fc4'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminprorrogacaoespecialidades4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminprorrogacaoLicenca == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('2fxjxr3g'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('2fxjxr3g'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminprorrogacaolicenca4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraParcial == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('5v56e34f'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('5v56e34f'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvaraparcial4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraDemolicao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('gbmpjjwg'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('gbmpjjwg'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvarademolicao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraEdificacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('8jaoa9fe'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('8jaoa9fe'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvaraedificacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraUrbanizacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('70weql3s'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('70weql3s'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvaraurbanizacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraSemObras == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('23nefgt6'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('23nefgt6'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvarasemobras4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraComObras == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('et5ntiv7'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('et5ntiv7'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvaracomobras4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraOutras == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('d6k9w6a3'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('d6k9w6a3'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvaraoutras4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalvaraRemodelacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('j5hrm1k3'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('j5hrm1k3'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalvararemodelacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminalteracaoEdificacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('lcx345r4'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('lcx345r4'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminalteracaoedificacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminobrasUrbanizacaoAlteracao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('aa3hyqnk'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('aa3hyqnk'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminobrasurbanizacaoalteracao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladminobrasEdificacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('sttnq56u'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('sttnq56u'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladminobrasedificacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_ladmincomunicacaoDurante == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('jakarg1j'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('jakarg1j'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewladmincomunicacaodurante4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>



                </div>
            </div>
        </div>

    </div>
</div>

<?php
echo $localScripts;
?>






