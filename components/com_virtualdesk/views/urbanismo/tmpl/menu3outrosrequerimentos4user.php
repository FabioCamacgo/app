<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUrbanismoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_urbanismo.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('urbanismo', 'list4users');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>

<div class="portlet light bordered">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TAB_OUTROSREQUERIMENTOS'); ?></span>
        </div>

        <div class="actions">
            <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"
               title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>">
                <i class="fa fa-arrow-left"></i>
                <?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
        </div>

    </div>

    <div class="portlet-body ">

        <div class="tabbable-line nav-justified ">

            <div class="tab-content">
                <div class="row">

                    <?php
                        $Enabled_coordenadorProjetos = VirtualDeskSiteFormmainHelper::checkFormEnabled('r01yxge8');
                        $Enabled_diretorObra = VirtualDeskSiteFormmainHelper::checkFormEnabled('qd25xihn');
                        $Enabled_diretorFiscalizacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('r83465pl');
                        $Enabled_substituicaoRequerente = VirtualDeskSiteFormmainHelper::checkFormEnabled('c8ph7vg1');
                        $Enabled_substituicaoTitular = VirtualDeskSiteFormmainHelper::checkFormEnabled('26qweb4e');
                        $Enabled_desistencia = VirtualDeskSiteFormmainHelper::checkFormEnabled('21s2950e');
                        $Enabled_devolucaoCaucao = VirtualDeskSiteFormmainHelper::checkFormEnabled('i0026kf8');
                        $Enabled_habitacaoDeposito = VirtualDeskSiteFormmainHelper::checkFormEnabled('jdg6x7oe');
                        $Enabled_habitacaoEmissao = VirtualDeskSiteFormmainHelper::checkFormEnabled('rlt0cqgc');
                        $Enabled_inicioTrabalhos = VirtualDeskSiteFormmainHelper::checkFormEnabled('5gwvp64z');
                        $Enabled_juncaoElementos = VirtualDeskSiteFormmainHelper::checkFormEnabled('qfv5okzs');
                        $Enabled_prazosExecucao = VirtualDeskSiteFormmainHelper::checkFormEnabled('iighlfeu');
                        $Enabled_prazosJuncao = VirtualDeskSiteFormmainHelper::checkFormEnabled('mx7go640');
                        $Enabled_rececaoDefinitiva = VirtualDeskSiteFormmainHelper::checkFormEnabled('pe5g9v2o');
                        $Enabled_rececaoProvisoria = VirtualDeskSiteFormmainHelper::checkFormEnabled('ue37abzp');
                        $Enabled_reducaoMontante = VirtualDeskSiteFormmainHelper::checkFormEnabled('rzyldbb8');
                    ?>


                    <?php if($Enabled_coordenadorProjetos == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('r01yxge8'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('r01yxge8'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewcoordenadorrojetos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_diretorObra == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('qd25xihn'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('qd25xihn'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewdiretorobra4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_diretorFiscalizacao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('r83465pl'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('r83465pl'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewdiretorfiscalizacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_substituicaoRequerente == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('c8ph7vg1'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('c8ph7vg1'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewsubstituicaorequerente4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_substituicaoTitular == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('26qweb4e'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('26qweb4e'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewsubstituicaotitular4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_desistencia == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('21s2950e'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('21s2950e'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewdesistencia4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_devolucaoCaucao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('i0026kf8'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('i0026kf8'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewdevolucaocaucao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_habitacaoDeposito == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('jdg6x7oe'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('jdg6x7oe'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewhabitacaodeposito4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_habitacaoEmissao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('rlt0cqgc'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('rlt0cqgc'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewhabitacaoemissao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_inicioTrabalhos == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('5gwvp64z'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('5gwvp64z'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewiniciotrabalhos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_juncaoElementos == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('qfv5okzs'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('qfv5okzs'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewjuncaoelementos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_prazosExecucao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('iighlfeu'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('iighlfeu'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewprazosexecucao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_prazosJuncao == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('mx7go640'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('mx7go640'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewprazosjuncao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_rececaoDefinitiva == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('pe5g9v2o'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('pe5g9v2o'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewrececaodefinitiva4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_rececaoProvisoria == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('ue37abzp'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('ue37abzp'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewrececaoprovisoria4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($Enabled_reducaoMontante == 1) :?>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('rzyldbb8'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo VirtualDeskSiteFormmainHelper::getRefTag('rzyldbb8'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=addnewreducaomontante4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>




                </div>
            </div>
        </div>

    </div>
</div>

<?php
echo $localScripts;
?>








