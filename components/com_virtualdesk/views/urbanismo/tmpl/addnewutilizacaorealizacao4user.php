
<?php
defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteUrbanismoHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_urbanismo.php');
JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('urbanismo');
if($vbHasAccess===false ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

$tag_form='lifpun5s';
$formId = VirtualDeskSiteFormmainHelper::getFormId($tag_form);
$modulo_id=VirtualDeskSiteFormmainHelper::getModuleId('urbanismo');

$fieldsForm = VirtualDeskSiteFormmainHelper::getFieldsData($tag_form, $modulo_id);

$newFieldsExists = array();
foreach ($fieldsForm as $keyF) {
    $newFieldsExists[] = $keyF['tag'];
}

// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

//Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');

$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteUrbanismoHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewutilizacaorealizacao4user.urbanismo.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

$UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
$UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
$nome =  $UserProfileData->name;
$email    = $UserProfileData->email;
$email2   = $UserProfileData->email;
$fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
$telefone = $UserProfileData->phone1;
$morada = $UserProfileData->address;
$codPostal = $UserProfileData->postalcode;

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=menu3autorizacaoutilizacao4user'); ?>"><span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TAB_AUTORIZACAOUTILIZACAO'); ?></span></a>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag($tag_form); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
            <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=menu3autorizacaoutilizacao4user'); ?>"
               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                <i class="fa fa-ban"></i>
                <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body form">


        <div class="tabbable-line nav-justified ">

            <div class="tab-content">

                <form id="new-utilizacaorealizacao" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=urbanismo.createutilizacaorealizacao4user'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_REQUERENTE');?></h3></legend>
                        <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_REQUIRED');?></p>

                        <?php if(in_array('fieldName',$newFieldsExists)) :?>
                            <div class="form-group fieldName">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NOME'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="fieldName" id="fieldName" maxlength="500" value="<?php echo $nome; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldMorada',$newFieldsExists)) :?>
                            <div class="form-group fieldMorada">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_MORADA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="fieldMorada" id="fieldMorada" maxlength="500" value="<?php echo $morada; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldPorta',$newFieldsExists)) :?>
                            <div class="form-group fieldPorta">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NUM_PORTA'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="fieldPorta" id="fieldPorta" maxlength="5" value="<?php echo $numPorta; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldLote',$newFieldsExists)) :?>
                            <div class="form-group fieldLote">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_LOTE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="fieldLote" id="fieldLote" maxlength="5" value="<?php echo $lote; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldLocalidade',$newFieldsExists)) :?>
                            <div class="form-group fieldLocalidade">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_LOCALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="fieldLocalidade" id="fieldLocalidade" maxlength="100" value="<?php echo $localidade; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldCodPostal',$newFieldsExists)) :?>
                            <div class="form-group fieldCodPostal">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_CODPOSTAL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="fieldCodPostal" id="fieldCodPostal" maxlength="30" value="<?php echo $codPostal; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldfreguesia',$newFieldsExists)) :?>
                            <div class="form-group fieldfreguesia">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_FREGUESIA'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="fieldfreguesia" id="fieldfreguesia" maxlength="100" value="<?php echo $freguesia; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldNif',$newFieldsExists)) :?>
                            <div class="form-group fieldNif">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NIF'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="number" required readonly class="form-control" name="fieldNif" id="fieldNif" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldDocId',$newFieldsExists)) :?>
                            <div class="form-group fieldDocId">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TIPODOCIDENTIFICACAO'); ?></label>
                                <div class="col-md-12">
                                    <input type="radio" name="radioval" id="cc" value="cc" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_CC'); ?>
                                    <input type="radio" name="radioval" id="passaporte" value="passaporte" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_PASSAPORTE'); ?>
                                </div>

                                <input type="hidden" id="fieldDocId" name="fieldDocId" value="<?php echo $fieldDocId; ?>">
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldNumId',$newFieldsExists)) :?>
                            <div class="form-group fieldNumId">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NUMERO'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="fieldNumId" id="fieldNumId" maxlength="30" value="<?php echo $numID; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldValId',$newFieldsExists)) :?>
                            <div class="form-group fieldValId">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_VALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="date" class="form-control" name="fieldValId" id="fieldValId" value="<?php echo $validade; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldCCP',$newFieldsExists)) :?>
                            <div class="form-group" id="fieldCCP">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_CCP'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="ccp" id="ccp" maxlength="100" value="<?php echo $ccp; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldTelemovel',$newFieldsExists)) :?>
                            <div class="form-group fieldTelemovel">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TELEMOVEL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="number" required class="form-control" name="fieldTelemovel" id="fieldTelemovel" maxlength="9" value="<?php echo $telemovel; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldTelefone',$newFieldsExists)) :?>
                            <div class="form-group fieldTelefone">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TELEFONE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="fieldTelefone" id="fieldTelefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldFax',$newFieldsExists)) :?>
                            <div class="form-group fieldFax">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_FAX'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="fieldFax" id="fieldFax" maxlength="9" value="<?php echo $fax; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldEmail',$newFieldsExists)) :?>
                            <div class="form-group fieldEmail">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_EMAIL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="fieldEmail" id="fieldEmail" maxlength="200" value="<?php echo $email; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldQualidadeUrbanismo',$newFieldsExists)) :?>
                            <div class="form-group fieldQualidadeUrbanismo">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_QUALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="radio" name="radiovalQuaUrb" id="arrendatario" value="arrendatario" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeUrbanismo'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_ARRENDATARIO'); ?>
                                    <input type="radio" name="radiovalQuaUrb" id="comodatario" value="comodatario" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeUrbanismo'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_COMODATARIO'); ?>
                                    <input type="radio" name="radiovalQuaUrb" id="proprietario" value="proprietario" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeUrbanismo'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_PROPRIETARIO'); ?>
                                    <input type="radio" name="radiovalQuaUrb" id="superficiario" value="superficiario" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeUrbanismo'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_SUPERFICIARIO'); ?>
                                    <input type="radio" name="radiovalQuaUrb" id="usufrutuario" value="usufrutuario" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeUrbanismo'] == 5) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_USUFRUTUARIO'); ?>
                                    <input type="radio" name="radiovalQuaUrb" id="outraQuaUrbanismo" value="outraQuaUrbanismo" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeUrbanismo'] == 6) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_OUTRA'); ?>
                                </div>

                                <input type="hidden" id="fieldQualidadeUrbanismo" name="fieldQualidadeUrbanismo" value="<?php echo $fieldQualidadeUrbanismo; ?>">
                            </div>
                        <?php endif; ?>


                        <?php if(in_array('fieldOutraQualUrbanismo',$newFieldsExists)) :?>
                            <div class="form-group fieldOutraQualUrbanismo" id="hideFieldOutraQualUrbanismo" style="display:none">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_OUTRAQUALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="fieldOutraQualUrbanismo" id="fieldOutraQualUrbanismo" maxlength="200" value="<?php echo $fieldOutraQualUrbanismo; ?>"/>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>

                    <?php if(in_array('fieldNomeRep',$newFieldsExists) || in_array('fieldMoradaRep',$newFieldsExists) || in_array('fieldPortaRep',$newFieldsExists) || in_array('fieldLoteRep',$newFieldsExists) || in_array('fieldLocalRep',$newFieldsExists) || in_array('fieldCodPostRep',$newFieldsExists) || in_array('fieldfreguesiaRep',$newFieldsExists) || in_array('fieldNifRep',$newFieldsExists) || in_array('fieldDocIdRep',$newFieldsExists) || in_array('fieldNumIdRep',$newFieldsExists) || in_array('fieldValIdRep',$newFieldsExists) || in_array('fieldCCPRep',$newFieldsExists) || in_array('fieldTelemovelRep',$newFieldsExists) || in_array('fieldTelefoneRep',$newFieldsExists) || in_array('fieldFaxRep',$newFieldsExists) || in_array('fieldEmailRep',$newFieldsExists) || in_array('fieldQualidadeRep',$newFieldsExists) || in_array('fieldOutraQualRep',$newFieldsExists)) :?>

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_REPRESENTANTE');?></h3></legend>

                            <?php if(in_array('fieldNomeRep',$newFieldsExists)) :?>
                                <div class="form-group fieldNomeRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NOME'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldNomeRep" id="fieldNomeRep" maxlength="500" value="<?php echo $nomeRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldMoradaRep',$newFieldsExists)) :?>
                                <div class="form-group fieldMoradaRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_MORADA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldMoradaRep" id="fieldMoradaRep" maxlength="500" value="<?php echo $moradaRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldPortaRep',$newFieldsExists)) :?>
                                <div class="form-group fieldPortaRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NUM_PORTA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldPortaRep" id="fieldPortaRep" maxlength="5" value="<?php echo $numPortaRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLoteRep',$newFieldsExists)) :?>
                                <div class="form-group fieldLoteRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_LOTE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLoteRep" id="fieldLoteRep" maxlength="5" value="<?php echo $loteRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLocalRep',$newFieldsExists)) :?>
                                <div class="form-group fieldLocalRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_LOCALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLocalRep" id="fieldLocalRep" maxlength="100" value="<?php echo $localidadeRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCodPostRep',$newFieldsExists)) :?>
                                <div class="form-group fieldCodPostRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_CODPOSTAL'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldCodPostRep" id="fieldCodPostRep" maxlength="30" value="<?php echo $codPostalRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldfreguesiaRep',$newFieldsExists)) :?>
                                <div class="form-group fieldfreguesiaRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_FREGUESIA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldfreguesiaRep" id="fieldfreguesiaRep" maxlength="100" value="<?php echo $fieldfreguesiaRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNifRep',$newFieldsExists)) :?>
                                <div class="form-group fieldNifRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NIF'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldNifRep" id="fieldNifRep" maxlength="9" value="<?php echo $fiscalidRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldDocIdRep',$newFieldsExists)) :?>
                                <div class="form-group fieldDocIdRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TIPODOCIDENTIFICACAO'); ?></label>
                                    <div class="col-md-12">
                                        <input type="radio" name="radiovalRep" id="ccRep" value="ccRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_CC'); ?>
                                        <input type="radio" name="radiovalRep" id="passaporteRep" value="passaporteRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_PASSAPORTE'); ?>
                                    </div>

                                    <input type="hidden" id="fieldDocIdRep" name="fieldDocIdRep" value="<?php echo $fieldDocIdRep; ?>">
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNumIdRep',$newFieldsExists)) :?>
                                <div class="form-group fieldNumIdRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_NUMERO'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldNumIdRep" id="fieldNumIdRep" maxlength="30" value="<?php echo $numIDRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldValIdRep',$newFieldsExists)) :?>
                                <div class="form-group fieldValIdRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_VALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="date" class="form-control" name="fieldValIdRep" id="fieldValIdRep" value="<?php echo $validadeRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCCPRep',$newFieldsExists)) :?>
                                <div class="form-group fieldCCPRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_CCP'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldCCPRep" id="fieldCCPRep" maxlength="100" value="<?php echo $ccpRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelemovelRep',$newFieldsExists)) :?>
                                <div class="form-group fieldTelemovelRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TELEMOVEL'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldTelemovelRep" id="fieldTelemovelRep" maxlength="9" value="<?php echo $telemovelRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelefoneRep',$newFieldsExists)) :?>
                                <div class="form-group fieldTelefoneRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_TELEFONE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldTelefoneRep" id="fieldTelefoneRep" maxlength="9" value="<?php echo $telefoneRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldFaxRep',$newFieldsExists)) :?>
                                <div class="form-group fieldFaxRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_FAX'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldFaxRep" id="fieldFaxRep" maxlength="9" value="<?php echo $faxRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldEmailRep',$newFieldsExists)) :?>
                                <div class="form-group fieldEmailRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_EMAIL'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldEmailRep" id="fieldEmailRep" maxlength="200" value="<?php echo $emailRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldQualidadeRep',$newFieldsExists)) :?>
                                <div class="form-group fieldQualidadeRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_QUALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="radio" name="radiovalQuaRep" id="repLegalRep" value="repLegalRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_REPLEGAL'); ?>
                                        <input type="radio" name="radiovalQuaRep" id="gestNegRep" value="gestNegRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_GESTNEG'); ?>
                                        <input type="radio" name="radiovalQuaRep" id="mandatRep" value="mandatRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_MANDATARIO'); ?>
                                        <input type="radio" name="radiovalQuaRep" id="outraRep" value="outraRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_URBANISMO_OUTRA'); ?>
                                    </div>

                                    <input type="hidden" id="fieldQualidadeRep" name="fieldQualidadeRep" value="<?php echo $qualidadeRep; ?>">
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldOutraQualRep',$newFieldsExists)) :?>
                                <div class="form-group fieldOutraQualRep" id="hideFieldOutraQualRep" style="display:none">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_URBANISMO_OUTRAQUALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldOutraQualRep" id="fieldOutraQualRep" maxlength="200" value="<?php echo $outraQualRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>

                    <?php endif; ?>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                </button>
                                <a class="btn default"
                                   href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=urbanismo&layout=menu3autorizacaoutilizacao4user'); ?>"
                                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"     value="<?php echo $obVDCrypt->formInputValueEncrypt('urbanismo.createutilizacaorealizacao4user',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('addnewutilizacaorealizacao4user',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('formId',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($formId,$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('userID',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($UserJoomlaID,$setencrypt_forminputhidden); ?>"/>

                    <?php echo JHtml::_('form.token'); ?>

                </form>

            </div>
        </div>
    </div>
</div>


<?php
echo $localScripts;
echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/urbanismo/tmpl/addnewutilizacaorealizacao4user.js.php');
echo ('</script>');
?>