<?php
defined('_JEXEC') or die;

$objEstadoOptions  = VirtualDeskSitePercursosPedestresHelper::getEstadoAllOptions($language_tag);
$objTipoOptions  = VirtualDeskSitePercursosPedestresHelper::getTipologia($language_tag);
$objZonaOptions  = VirtualDeskSitePercursosPedestresHelper::getZonamento($language_tag);

$EstadoOptionsHTML = '';
foreach($objEstadoOptions as $row) {
    $EstadoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}

$TipoOptionsHTML = '';
foreach($objTipoOptions as $row) {
    $TipoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['tipologia'] . '</option>';
}

$ZonaOptionsHTML = '';
foreach($objZonaOptions as $row) {
    $ZonaOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['zonamento'] . '</option>';
}

?>

var TableDatatablesManaged = function () {

    var initTableAllPercursos = function () {

        var table = jQuery('#tabela_lista_percursos');
        var tableTools = jQuery('#tabela_lista_percursos_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"lf <"EstadoFilterDropBox"><"TipoFilterDropBox"><"ZonaFilterDropBox"> rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_CULTURA_ESTADONAOINICIADO"); ?>';

                        switch (row[8])
                        {   case '1':
                            defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(1);?>';
                            break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(2);?>';
                                break;
                            case '3':
                                defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(3);?>';
                                break;
                            case '4':
                                defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(4);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 7,
                    "data": 7,
                    "orderable": false,
                    "visible":false,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + row[7] + returnLinkf);
                    }
                },
                {
                    "targets": 8,
                    "visible":false
                },
                {
                    "targets": 9,
                    "visible":false
                }
                ,
                {
                    "targets": 10,
                    "visible":false
                }
                ,
                {
                    "targets": 11,
                    "visible":false
                }
                ,
                {
                    "targets": 12,
                    "visible":false
                }
            ],
            "order": [
                [9, "asc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=percursospedestres.getPercursosPedestresList4ManagerByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // Filtrar ESTADO
                this.api().column(8).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESTADO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBox').empty() )
                    jQuery('.EstadoFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Tipo
                this.api().column(10).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($TipoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.TipoFilterDropBox').empty() )
                    jQuery('.TipoFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                //Zonamento Tipo
                this.api().column(11).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($ZonaOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.ZonaFilterDropBox').empty() )
                    jQuery('.ZonaFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });
            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_percursos_wrapper');

    }

    var initTableMMGPercursos = function () {

        var table = jQuery('#tabela_lista_percursos_mmg');
        var tableTools = jQuery('#tabela_lista_percursos_mmg_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"lf <"EstadoFilterDropBox"><"TipoFilterDropBox"><"ZonaFilterDropBox"> rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_CULTURA_ESTADONAOINICIADO"); ?>';

                        switch (row[8])
                        {   case '1':
                            defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(1);?>';
                            break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(2);?>';
                                break;
                            case '3':
                                defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(3);?>';
                                break;
                            case '4':
                                defCss = '<?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS(4);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 7,
                    "data": 7,
                    "orderable": false,
                    "visible":false,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + row[7] + returnLinkf);
                    }
                },
                {
                    "targets": 8,
                    "visible":false
                },
                {
                    "targets": 9,
                    "visible":false
                }
                ,
                {
                    "targets": 10,
                    "visible":false
                }
                ,
                {
                    "targets": 11,
                    "visible":false
                }
                ,
                {
                    "targets": 12,
                    "visible":false
                }
            ],
            "order": [
                [9, "asc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=percursospedestres.getPercursosPedestresMMGList4ManagerByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // Filtrar ESTADO
                this.api().column(8).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESTADO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBox').empty() )
                    jQuery('.EstadoFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Tipo
                this.api().column(10).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($TipoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.TipoFilterDropBox').empty() )
                    jQuery('.TipoFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                //Filtrar Zonamento
                this.api().column(11).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONA"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($ZonaOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.ZonaFilterDropBox').empty() )
                    jQuery('.ZonaFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });
            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_percursos_mmg_wrapper');

    }


    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableAllPercursos();

            initTableMMGPercursos();

        }

    };

}();

jQuery(document).ready(function() {

    TableDatatablesManaged.init();
});