<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');
    JLoader::register('VirtualDeskSitePercursosPedestresCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/fotoCapa_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/galeria_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGPXFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/gpx_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresKMLFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/kml_files.php');


    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'edit4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/percursospedestres/tmpl/percursospedestres.css');

    // Parâmetros
    $labelseparator    = ' : ';
    $params            = JComponentHelper::getParams('com_virtualdesk');
    $getInputPercursos_Id = JFactory::getApplication()->input->getInt('percursospedestres_id');


    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSitePercursosPedestresHelper::getPercursosDetail4Manager($getInputPercursos_Id);
    if( empty($this->data) ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
        return false;
    }

    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit4manager.percursospedestres.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }


    //se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;

    $obParam    = new VirtualDeskSiteParamsHelper();


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_EDIT' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-percursospedestres"
                  action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=percursospedestres.update4manager'); ?>" method="post"
                  class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                  role="form">

                <div class="form-body">

                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DADOSPERCURSO'); ?></h3></legend>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOMEPERCURSO'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="nomepercurso" id="nomepercurso" maxlength="500" value="<?php echo $this->data->percurso; ?>"/>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO'); ?></label>
                            <div class="value col-md-12">
                                <textarea  class="form-control wysihtml5" rows="15" name="descricao" id="descricao" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao); ?></textarea>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_EN'); ?></label>
                            <div class="value col-md-12">
                                <textarea  class="form-control wysihtml5" rows="15" name="descricaoEN" id="descricaoEN" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricaoEN); ?></textarea>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_FR'); ?></label>
                            <div class="value col-md-12">
                                <textarea  class="form-control wysihtml5" rows="15" name="descricaoFR" id="descricaoFR" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricaoFR); ?></textarea>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_DE'); ?></label>
                            <div class="value col-md-12">
                                <textarea  class="form-control wysihtml5" rows="15" name="descricaoDE" id="descricaoDE" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricaoDE); ?></textarea>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA'); ?><span class="required">*</span></label>
                            <?php $tipologias = VirtualDeskSitePercursosPedestresHelper::getTipologia()?>
                            <div class="value col-md-12">
                                <select name="tipologia" value="<?php echo $this->data->idtipologia; ?>" id="tipologia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idtipologia)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($tipologias as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['tipologia']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idtipologia; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getTipologiaSelect($this->data->idtipologia) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeTipo = VirtualDeskSitePercursosPedestresHelper::excludeTipologia($this->data->idtipologia)?>
                                        <?php foreach($ExcludeTipo as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['tipologia']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM'); ?></label>
                            <?php $paisagens = VirtualDeskSitePercursosPedestresHelper::getPaisagem()?>
                            <div class="value col-md-12">
                                <select name="tipoPaisagem" value="<?php echo $this->data->idpaisagem; ?>" id="tipoPaisagem" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idpaisagem)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($paisagens as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['paisagem']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idpaisagem; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getPaisagemSelect($this->data->idpaisagem) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludePaisagem = VirtualDeskSitePercursosPedestresHelper::excludePaisagem($this->data->idpaisagem)?>
                                        <?php foreach($ExcludePaisagem as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['paisagem']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group third left">
                            <div class="name col-md-12">
                                <input type="checkbox" name="percLaurissilva" id="percLaurissilva" value="<?php echo $this->data->percurso_laurissilva; ?>" <?php if ($this->data->percurso_laurissilva == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PERCLAURISSILVA'); ?>
                            </div>

                            <input type="hidden" required id="percLaurissilvaValue" name="percLaurissilvaValue" value="<?php echo $this->data->percurso_laurissilva; ?>">
                        </div>


                        <div class="form-group third center">
                            <div class="name col-md-12">
                                <input type="checkbox" name="costLaurissilva" id="costLaurissilva" value="<?php echo $this->data->costa_laurissilva; ?>" <?php if ($this->data->costa_laurissilva == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTALAURISSILVA'); ?>
                            </div>

                            <input type="hidden" required id="costLaurissilvaValue" name="costLaurissilvaValue" value="<?php echo $this->data->costa_laurissilva; ?>">
                        </div>


                        <div class="form-group third right">
                            <div class="name col-md-12">
                                <input type="checkbox" name="costSol" id="costSol" value="<?php echo $this->data->costa_sol; ?>" <?php if ($this->data->costa_sol == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTASOL'); ?>
                            </div>

                            <input type="hidden" required id="costSolValue" name="costSolValue" value="<?php echo $this->data->costa_sol; ?>">
                        </div>


                        <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PLATAFORMAS'); ?></h4>


                        <div class="form-group third left">
                            <div class="name col-md-12">
                                <input type="checkbox" name="mmg" id="mmg" value="<?php echo $this->data->mmg; ?>" <?php if ($this->data->mmg == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_MMG'); ?>
                            </div>

                            <input type="hidden" required id="mmgValue" name="mmgValue" value="<?php echo $this->data->mmg; ?>">
                        </div>


                    </div>


                    <div class="bloco">

                        <legend>
                            <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PATROCINADORES'); ?></h3>
                        </legend>

                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PATROCINADOR'); ?></label>
                            <?php $patrocinadores = VirtualDeskSitePercursosPedestresHelper::getPatrocinador()?>
                            <div class="value col-md-12">
                                <select name="patrocinador" value="<?php echo $this->data->patrocinador; ?>" id="patrocinador" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->patrocinador)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($patrocinadores as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['patrocinador']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->patrocinador; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getPatrocinadorSelect($this->data->patrocinador) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludePatrocinador = VirtualDeskSitePercursosPedestresHelper::excludePatrocinador($this->data->patrocinador)?>
                                        <?php foreach($ExcludePatrocinador as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['patrocinador']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA'); ?></h3></legend>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO'); ?><span class="required">*</span></label>
                            <?php $sentidos = VirtualDeskSitePercursosPedestresHelper::getSentido()?>
                            <div class="value col-md-12">
                                <select name="sentido" value="<?php echo $this->data->idsentido; ?>" id="sentido" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idsentido)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($sentidos as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['sentido']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idsentido; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getSentidoSelect($this->data->idsentido) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeSentido = VirtualDeskSitePercursosPedestresHelper::excludeSentido($this->data->idsentido)?>
                                        <?php foreach($ExcludeSentido as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['sentido']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO'); ?><span class="required">*</span></label>
                            <?php $terrenos = VirtualDeskSitePercursosPedestresHelper::getTerreno()?>
                            <div class="value col-md-12">
                                <select name="terreno" value="<?php echo $this->data->idterreno; ?>" id="terreno" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idterreno)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($terrenos as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['terreno']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idterreno; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getTerrenoSelect($this->data->idterreno) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeTerreno = VirtualDeskSitePercursosPedestresHelper::excludeTerreno($this->data->idterreno)?>
                                        <?php foreach($ExcludeTerreno as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['terreno']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS'); ?><span class="required">*</span></label>
                            <?php $vertigensAll = VirtualDeskSitePercursosPedestresHelper::getVertigens()?>
                            <div class="value col-md-12">
                                <select name="vertigens" value="<?php echo $this->data->idvertigens; ?>" id="vertigens" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idvertigens)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($vertigensAll as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['vertigens']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idvertigens; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getVertigensSelect($this->data->idvertigens) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeVertigens = VirtualDeskSitePercursosPedestresHelper::excludeVertigens($this->data->idvertigens)?>
                                        <?php foreach($ExcludeVertigens as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['vertigens']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE'); ?><span class="required">*</span></label>
                            <?php $dificuldadeAll = VirtualDeskSitePercursosPedestresHelper::getDificuldade()?>
                            <div class="value col-md-12">
                                <select name="dificuldade" value="<?php echo $this->data->iddificuldade; ?>" id="dificuldade" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->iddificuldade)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($dificuldadeAll as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['dificuldade']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->iddificuldade; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getDificuldadeSelect($this->data->iddificuldade) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeDificuldade = VirtualDeskSitePercursosPedestresHelper::excludeDificuldade($this->data->iddificuldade)?>
                                        <?php foreach($ExcludeDificuldade as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['dificuldade']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="distancia" id="distancia" maxlength="11" value="<?php echo $this->data->distancia; ?>"/>
                            </div>
                        </div>

                        <?php
                            $ExplodeDuracao = explode(",", $this->data->duracao);
                            $horas = $ExplodeDuracao[0];
                            if(strlen($ExplodeDuracao[1]) == 1){
                                $minutos = $ExplodeDuracao[1] . '0';
                            } else {
                                $minutos = $ExplodeDuracao[1];
                            }
                        ?>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO'); ?><span class="required">*</span></label>
                            <div class="value col-md-5">
                                <input type="text" placeholder="hh" class="form-control" name="horas" id="horas" maxlength="2" value="<?php echo $horas; ?>"/>
                            </div>
                            <div class="col-md-2">
                                <span><?php echo ':';?></span>
                            </div>
                            <div class="value col-md-5">
                                <input type="text" placeholder="mm" class="form-control" name="minutos" id="minutos" maxlength="2" value="<?php echo $minutos; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDEMAX'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="altitudemax" id="altitudemax" maxlength="11" value="<?php echo $this->data->altitude_max; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDEMIN'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="altitudemin" id="altitudemin" maxlength="11" value="<?php echo $this->data->altitude_min; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SUBIDAACUMULADO'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="subidaAcumulado" id="subidaAcumulado" maxlength="11" value="<?php echo $this->data->subida_acumulado; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCIDAACUMULADO'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="descidaAcumulado" id="descidaAcumulado" maxlength="11" value="<?php echo $this->data->descida_acumulado; ?>"/>
                            </div>
                        </div>

                    </div>


                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LOCALIZACAO'); ?></h3></legend>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTRITO'); ?><span class="required">*</span></label>
                            <?php $distritos = VirtualDeskSitePercursosPedestresHelper::getDistrito()?>
                            <div class="value col-md-12">
                                <select name="distrito" value="<?php echo $this->data->iddistrito; ?>" id="distrito" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->iddistrito)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($distritos as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['distrito']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->iddistrito; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getDistritoSelect($this->data->iddistrito) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeDistrito = VirtualDeskSitePercursosPedestresHelper::excludeDistrito($this->data->iddistrito)?>
                                        <?php foreach($ExcludeDistrito as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['distrito']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <?php
                        $ListaDeConcelho = array();
                        if(!empty($this->data->iddistrito)) {
                            if( (int) $this->data->iddistrito > 0) $ListaDeConcelho = VirtualDeskSitePercursosPedestresHelper::getConcelho($this->data->iddistrito);
                        }
                        ?>
                        <div id="blocoConcelho" class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_CONCELHO' ); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <select name="concelho" id="concelho" value="<?php echo $this->data->idconcelho;?>"
                                    <?php
                                    if(empty($this->data->iddistrito)) {
                                        echo 'disabled';
                                    }
                                    ?>
                                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idconcelho)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($ListaDeConcelho as $rowMM) : ?>
                                            <option value="<?php echo $rowMM['id']; ?>"
                                            ><?php echo $rowMM['name']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idconcelho; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getConcelhoSelect($this->data->idconcelho);?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeConcelho = VirtualDeskSitePercursosPedestresHelper::excludeConcelho($this->data->iddistrito, $this->data->idconcelho);?>
                                        <?php foreach($ExcludeConcelho as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['name']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>


                        <?php
                        $ListaDeFreguesias = array();
                        if(!empty($this->data->idconcelho)) {
                            if( (int) $this->data->idconcelho > 0) $ListaDeFreguesias = VirtualDeskSitePercursosPedestresHelper::getFreguesia($this->data->idconcelho);
                        }
                        ?>
                        <div id="blocoFreguesia" class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGUESIA' ); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <select name="freguesia" id="freguesia" value="<?php echo $this->data->idfreguesia;?>"
                                    <?php
                                    if(empty($this->data->idconcelho)) {
                                        echo 'disabled';
                                    }
                                    ?>
                                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idfreguesia)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($ListaDeFreguesias as $rowMM) : ?>
                                            <option value="<?php echo $rowMM['id']; ?>"
                                            ><?php echo $rowMM['name']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idfreguesia; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getFreguesiaSelect($this->data->idfreguesia);?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeFreguesia = VirtualDeskSitePercursosPedestresHelper::excludeFreguesia($this->data->idconcelho, $this->data->idfreguesia);?>
                                        <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['name']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO'); ?><span class="required">*</span></label>
                            <?php $Zonamentos = VirtualDeskSitePercursosPedestresHelper::getZonamento()?>
                            <div class="value col-md-12">
                                <select name="zonamento" value="<?php echo $this->data->idzonamento; ?>" id="zonamento" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idzonamento)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Zonamentos as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['zonamento']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idzonamento; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getZonamentoSelect($this->data->idzonamento) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeZonamento = VirtualDeskSitePercursosPedestresHelper::excludeZonamento($this->data->idzonamento)?>
                                        <?php foreach($ExcludeZonamento as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['zonamento']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGINICIO'); ?><span class="required">*</span></label>
                            <?php $Freguesias = VirtualDeskSitePercursosPedestresHelper::getFreguesiaEdited()?>
                            <div class="value col-md-12">
                                <select name="freguesiaInicio" value="<?php echo $this->data->idinicio_freguesia; ?>" id="freguesiaInicio" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idinicio_freguesia)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Freguesias as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['freguesia']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idinicio_freguesia; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getFreguesiaSelect($this->data->idinicio_freguesia) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeFreguesia = VirtualDeskSitePercursosPedestresHelper::excludeFreguesiaEdited($this->data->idinicio_freguesia)?>
                                        <?php foreach($ExcludeFreguesia as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['freguesia']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGFIM'); ?><span class="required">*</span></label>
                            <?php $Freguesias = VirtualDeskSitePercursosPedestresHelper::getFreguesiaEdited()?>
                            <div class="value col-md-12">
                                <select name="freguesiaFim" value="<?php echo $this->data->idfim_freguesia; ?>" id="freguesiaFim" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idfim_freguesia)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Freguesias as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['freguesia']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idfim_freguesia; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getFreguesiaSelect($this->data->idfim_freguesia) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeFreguesia = VirtualDeskSitePercursosPedestresHelper::excludeFreguesiaEdited($this->data->idfim_freguesia)?>
                                        <?php foreach($ExcludeFreguesia as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['freguesia']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIOINICIO'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="sitioInicio" id="sitioInicio" maxlength="250" value="<?php echo $this->data->inicio_sitio; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIOFIM'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="sitioFim" id="sitioFim" maxlength="250" value="<?php echo $this->data->fim_sitio; ?>"/>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LOCALIDADES'); ?></label>
                            <div class="value col-md-12">
                                <textarea  class="form-control wysihtml5" rows="10" name="localidades" id="localidades" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->localidades); ?></textarea>
                            </div>
                        </div>

                    </div>


                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUES'); ?></h3></legend>

                        <div class="mt-repeater">
                            <div data-repeater-list="DestaqueInput">
                                <?php
                                    $explodeDestaques = explode(';;', $this->data->destaques);
                                    $explodeDestaquesEN = explode(';;', $this->data->destaquesEN);
                                    $explodeDestaquesFR = explode(';;', $this->data->destaquesFR);
                                    $explodeDestaquesDE = explode(';;', $this->data->destaquesDE);

                                    for($i=0; $i<count($explodeDestaques); $i++){
                                        ?>
                                            <div data-repeater-item class="mt-repeater-item ">

                                                <div class="mt-repeater-input DestaqueInput DestaqueInput-new" data-provides="DestaqueInput">

                                                    <div class="form-group all" data-trigger="DestaqueInput">

                                                        <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUE'); ?></div>
                                                        <div class="value col-md-12">
                                                            <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaque" maxlength="500" value="<?php echo $explodeDestaques[$i]; ?>"/>
                                                        </div>


                                                        <div class="name col-md-12 second"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEEN'); ?></div>
                                                        <div class="value col-md-12">
                                                            <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaqueEN" maxlength="500" value="<?php echo $explodeDestaquesEN[$i]; ?>"/>
                                                        </div>


                                                        <div class="name col-md-12 second"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEFR'); ?></div>
                                                        <div class="value col-md-12">
                                                            <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaqueFR" maxlength="500" value="<?php echo $explodeDestaquesFR[$i]; ?>"/>
                                                        </div>


                                                        <div class="name col-md-12 second"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEDE'); ?></div>
                                                        <div class="value col-md-12">
                                                            <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaqueDE" maxlength="500" value="<?php echo $explodeDestaquesDE[$i]; ?>"/>
                                                        </div>

                                                    </div>

                                                    <a href="javascript:;" class="btn btn-danger mt-repeater-delete DestaqueInput-exists" data-repeater-delete data-dismiss="DestaqueInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                                </div>

                                                <div class="mt-repeater-input">
                                                </div>

                                            </div>
                                        <?php
                                    }
                                ?>
                            </div>

                            <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                        </div>

                    </div>


                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_HASTAGS'); ?></h3></legend>

                        <div class="mt-repeater Hastags">
                            <div data-repeater-list="HastagsInput">
                                <?php
                                $explodeHastags = explode(';;', $this->data->hastags);

                                for($i=0; $i<count($explodeHastags); $i++){
                                    ?>
                                    <div data-repeater-item class="mt-repeater-item ">

                                        <div class="mt-repeater-input HastagsInput HastagsInput-new" data-provides="HastagsInput">

                                            <div class="form-group all" data-trigger="HastagsInput">
                                                <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_HASTAG'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="hastags" maxlength="500" value="<?php echo $explodeHastags[$i]; ?>"/>
                                                </div>
                                            </div>

                                            <a href="javascript:;" class="btn btn-danger mt-repeater-delete HastagsInput-exists" data-repeater-delete data-dismiss="HastagsInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                        </div>

                                        <div class="mt-repeater-input">
                                        </div>

                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                        </div>

                    </div>


                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_MULTIMEDIA'); ?></h3></legend>


                        <div class="form-group third left" id="uploadFieldCapa">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FOTOCAPA'); ?></label>
                            <div class="value col-md-12">
                                <div class="file-loading">
                                    <input type="file" name="fileupload_capa[]" id="fileupload_capa" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                        </div>


                        <div class="form-group third center" id="uploadFieldGPX">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GPX'); ?></label>
                            <div class="value col-md-12">
                                <div class="file-loading">
                                    <input type="file" name="fileupload_gpx[]" id="fileupload_gpx" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                        </div>


                        <div class="form-group third right" id="uploadFieldKML">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_KML'); ?></label>
                            <div class="value col-md-12">
                                <div class="file-loading">
                                    <input type="file" name="fileupload_kml[]" id="fileupload_kml" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOMECREDITO'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="nomecredito" id="nomecredito" maxlength="500" value="<?php echo $this->data->nomecredito; ?>"/>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LINKCREDITO'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="linkcredito" id="linkcredito" maxlength="500" value="<?php echo $this->data->linkcredito; ?>"/>
                            </div>
                        </div>


                        <div class="form-group all" id="uploadFieldGaleria">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GALERIA'); ?></label>
                            <div class="value col-md-12">
                                <div class="file-loading">
                                    <input type="file" name="fileupload_galeria[]" id="fileupload_galeria" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=list4manager&vdcleanstate=1'); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('percursospedestres_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->percursospedestres_id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('percursospedestres.update4manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>

                <?php echo JHtml::_('form.token'); ?>

            </form>

        </div>


    </div>

<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/percursospedestres/tmpl/edit4manager.js.php');
    echo ('</script>');
?>