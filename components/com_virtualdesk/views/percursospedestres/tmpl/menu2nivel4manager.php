<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('percursospedestres', 'list4managers');
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/percursospedestres/tmpl/percursospedestres.css');

?>

<div class="portlet light bordered">


    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>

    </div>

    <div class="portlet-body ">

        <div class="tab-content">

            <div class="tab-pane" style="display:block;">
                <div class="row">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=addnew4manager&vdcleanstate=1'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_NOVOPERCURSO'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOVOPERCURSO'); ?></h3>
                            </div>
                        </div>
                    </a>

                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=list4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_LISTA'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LISTAPERCURSOS'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=tipologia4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_TIPOLOGIA'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRTIPOLOGIA'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=dificuldade4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_DIFICULDADE'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRDIFICULDADE'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=terreno4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_TERRENO'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRTERRENO'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=duracao4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_DURACAO'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRDURACAO'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=vertigens4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_VERTIGENS'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRVERTIGENS'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=zonamento4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_ZONAMENTO'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRZONAMENTO'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=sentido4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_SENTIDO'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRSENTIDO'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=distancia4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_DISTANCIA'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRDISTANCIA'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=altitude4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_ALTITUDE'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRALTITUDE'); ?></h3>
                            </div>
                        </div>
                    </a>


                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=paisagem4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TITLE_PAISAGEM'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GERIRPAISAGEM'); ?></h3>
                            </div>
                        </div>
                    </a>
                </div>

            </div>

        </div>

    </div>
</div>



<?php

echo $localScripts;

?>


