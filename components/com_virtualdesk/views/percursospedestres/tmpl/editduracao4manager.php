<?php

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');


    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('percursospedestres');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'editduracao4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/percursospedestres/tmpl/percursospedestres.css');

    // Parâmetros
    $labelseparator    = ' : ';
    $params            = JComponentHelper::getParams('com_virtualdesk');
    $getInputDuracao_Id = JFactory::getApplication()->input->getInt('duracao_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSitePercursosPedestresHelper::getDuracaoDetail4Manager($getInputDuracao_Id);
    if( empty($this->data) ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
        return false;
    }

    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.editduracao4manager.percursospedestres.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;

    $obParam    = new VirtualDeskSiteParamsHelper();


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EDIT' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=duracao4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-duracao"
                  action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=percursospedestres.updateduracao4manager'); ?>" method="post"
                  class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                  role="form">

                <div class="form-body">

                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_EDIT'); ?></h3></legend>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_MIN'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" required class="form-control" name="duracao_min" id="duracao_min" maxlength="5" value="<?php echo $this->data->duracao_min; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_MAX'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" required class="form-control" name="duracao_max" id="duracao_max" maxlength="5" value="<?php echo $this->data->duracao_max; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO_ESTADO'); ?><span class="required">*</span></label>
                            <?php $estados = VirtualDeskSitePercursosPedestresHelper::getEstado()?>
                            <div class="value col-md-12">
                                <select name="estado" required value="<?php echo $this->data->idestado; ?>" id="estado" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idestado)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($estados as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id_estado']; ?>"
                                            ><?php echo $rowStatus['estado']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idestado; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoSelect($this->data->idestado) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeEstado = VirtualDeskSitePercursosPedestresHelper::excludeEstado($this->data->idestado)?>
                                        <?php foreach($ExcludeEstado as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id_estado']; ?>"
                                            ><?php echo $rowStatus['estado']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=duracao4manager&vdcleanstate=1'); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('duracao_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->duracao_id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('percursospedestres.updateduracao4manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('duracao4manager',$setencrypt_forminputhidden); ?>"/>

                <?php echo JHtml::_('form.token'); ?>

            </form>

        </div>


    </div>

<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/percursospedestres/tmpl/editduracao4manager.js.php');
    echo ('</script>');
?>