<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');
    JLoader::register('VirtualDeskSitePercursosPedestresCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/fotoCapa_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/galeria_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGPXFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/gpx_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresKMLFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/kml_files.php');

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('percursospedestres');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'addnew4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/percursospedestres/tmpl/percursospedestres.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');
    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSitePercursosPedestresHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.percursospedestres.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email2   = $UserProfileData->email;
    $telefone = $UserProfileData->phone1;

    $obParam      = new VirtualDeskSiteParamsHelper();


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOVOPERCURSO' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <form id="new-percurso" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=percursospedestres.create4manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                    <div class="form-body">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DADOSPERCURSO'); ?></h3></legend>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOMEPERCURSO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="nomepercurso" id="nomepercurso" maxlength="400" value="<?php echo $nomepercurso; ?>"/>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="15" name="descricao" id="descricao" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao); ?></textarea>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_EN'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="15" name="descricaoEN" id="descricaoEN" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricaoEN); ?></textarea>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_FR'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="15" name="descricaoFR" id="descricaoFR" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricaoFR); ?></textarea>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_DE'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="15" name="descricaoDE" id="descricaoDE" maxlength="10000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricaoDE); ?></textarea>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA'); ?><span class="required">*</span></label>
                                <?php $tipologias = VirtualDeskSitePercursosPedestresHelper::getTipologia()?>
                                <div class="value col-md-12">
                                    <select name="tipologia" value="<?php echo $tipologia; ?>" id="tipologia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($tipologia)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($tipologias as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipologia']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $tipologia; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getTipologiaSelect($tipologia) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeTipo = VirtualDeskSitePercursosPedestresHelper::excludeTipologia($tipologia)?>
                                            <?php foreach($ExcludeTipo as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipologia']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM'); ?></label>
                                <?php $paisagens = VirtualDeskSitePercursosPedestresHelper::getPaisagem()?>
                                <div class="value col-md-12">
                                    <select name="tipoPaisagem" value="<?php echo $tipoPaisagem; ?>" id="tipoPaisagem" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($tipoPaisagem)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($paisagens as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['paisagem']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $tipoPaisagem; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getPaisagemSelect($tipoPaisagem) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludePaisagem = VirtualDeskSitePercursosPedestresHelper::excludePaisagem($tipoPaisagem)?>
                                            <?php foreach($ExcludePaisagem as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['paisagem']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group third left">
                                <div class="name col-md-12">
                                    <input type="checkbox" name="percLaurissilva" id="percLaurissilva" value="<?php echo $percLaurissilvaValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['percLaurissilvaValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PERCLAURISSILVA'); ?>
                                </div>

                                <input type="hidden" required id="percLaurissilvaValue" name="percLaurissilvaValue" value="<?php echo $percLaurissilvaValue; ?>">
                            </div>


                            <div class="form-group third center">
                                <div class="name col-md-12">
                                    <input type="checkbox" name="costLaurissilva" id="costLaurissilva" value="<?php echo $costLaurissilvaValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['costLaurissilvaValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTALAURISSILVA'); ?>
                                </div>

                                <input type="hidden" id="costLaurissilvaValue" name="costLaurissilvaValue" value="<?php echo $costLaurissilvaValue; ?>">
                            </div>


                            <div class="form-group third right">
                                <div class="name col-md-12">
                                    <input type="checkbox" name="costSol" id="costSol" value="<?php echo $costSolValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['costSolValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTASOL'); ?>
                                </div>

                                <input type="hidden" id="costSolValue" name="costSolValue" value="<?php echo $costSolValue; ?>">
                            </div>


                            <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PLATAFORMAS'); ?></h4>


                            <div class="form-group third left">
                                <div class="name col-md-12">
                                    <input type="checkbox" name="mmg" id="mmg" value="<?php echo $mmgValue; ?>" <?php if (isset($_POST["submitForm"]) && $_POST['mmgValue'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_MMG'); ?>
                                </div>

                                <input type="hidden" id="mmgValue" name="mmgValue" value="<?php echo $mmgValue; ?>">
                            </div>

                        </div>


                        <div class="bloco">

                            <legend>
                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PATROCINADORES'); ?></h3>
                            </legend>

                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PATROCINADOR'); ?></label>
                                <?php $patrocinadores = VirtualDeskSitePercursosPedestresHelper::getPatrocinador()?>
                                <div class="value col-md-12">
                                    <select name="patrocinador" value="<?php echo $patrocinador; ?>" id="patrocinador" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($patrocinador)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($patrocinadores as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['patrocinador']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $patrocinador; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getPatrocinadorSelect($patrocinador) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludePatrocinador = VirtualDeskSitePercursosPedestresHelper::excludePatrocinador($patrocinador)?>
                                            <?php foreach($ExcludePatrocinador as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['patrocinador']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA'); ?></h3></legend>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO'); ?><span class="required">*</span></label>
                                <?php $sentidos = VirtualDeskSitePercursosPedestresHelper::getSentido()?>
                                <div class="value col-md-12">
                                    <select name="sentido" value="<?php echo $sentido; ?>" id="sentido" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($sentido)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($sentidos as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['sentido']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $sentido; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getSentidoSelect($sentido) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeSentido = VirtualDeskSitePercursosPedestresHelper::excludeSentido($sentido)?>
                                            <?php foreach($ExcludeSentido as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['sentido']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO'); ?><span class="required">*</span></label>
                                <?php $terrenos = VirtualDeskSitePercursosPedestresHelper::getTerreno()?>
                                <div class="value col-md-12">
                                    <select name="terreno" value="<?php echo $terreno; ?>" id="terreno" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($terreno)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($terrenos as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['terreno']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $terreno; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getTerrenoSelect($terreno) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeTerreno = VirtualDeskSitePercursosPedestresHelper::excludeTerreno($terreno)?>
                                            <?php foreach($ExcludeTerreno as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['terreno']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS'); ?><span class="required">*</span></label>
                                <?php $vertigensAll = VirtualDeskSitePercursosPedestresHelper::getVertigens()?>
                                <div class="value col-md-12">
                                    <select name="vertigens" value="<?php echo $vertigens; ?>" id="vertigens" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($vertigens)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($vertigensAll as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['vertigens']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $vertigens; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getVertigensSelect($vertigens) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeVertigens = VirtualDeskSitePercursosPedestresHelper::excludeVertigens($vertigens)?>
                                            <?php foreach($ExcludeVertigens as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['vertigens']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE'); ?><span class="required">*</span></label>
                                <?php $dificuldadeAll = VirtualDeskSitePercursosPedestresHelper::getDificuldade()?>
                                <div class="value col-md-12">
                                    <select name="dificuldade" value="<?php echo $dificuldade; ?>" id="dificuldade" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($dificuldade)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($dificuldadeAll as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['dificuldade']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $dificuldade; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getDificuldadeSelect($dificuldade) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeDificuldade = VirtualDeskSitePercursosPedestresHelper::excludeDificuldade($dificuldade)?>
                                            <?php foreach($ExcludeDificuldade as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['dificuldade']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="distancia" id="distancia" maxlength="11" value="<?php echo $distancia; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO'); ?><span class="required">*</span></label>
                                <div class="value col-md-5">
                                    <input type="text" placeholder="hh" class="form-control" name="horas" id="horas" maxlength="2" value="<?php echo $horas; ?>"/>
                                </div>
                                <div class="col-md-2">
                                    <span><?php echo ':';?></span>
                                </div>
                                <div class="value col-md-5">
                                    <input type="text" placeholder="mm" class="form-control" name="minutos" id="minutos" maxlength="2" value="<?php echo $minutos; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDEMAX'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="altitudemax" id="altitudemax" maxlength="11" value="<?php echo $altitudemax; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDEMIN'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="altitudemin" id="altitudemin" maxlength="11" value="<?php echo $altitudemin; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SUBIDAACUMULADO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="subidaAcumulado" id="subidaAcumulado" maxlength="11" value="<?php echo $subidaAcumulado; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCIDAACUMULADO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="descidaAcumulado" id="descidaAcumulado" maxlength="11" value="<?php echo $descidaAcumulado; ?>"/>
                                </div>
                            </div>

                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LOCALIZACAO'); ?></h3></legend>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTRITO'); ?><span class="required">*</span></label>
                                <?php $distritos = VirtualDeskSitePercursosPedestresHelper::getDistrito()?>
                                <div class="value col-md-12">
                                    <select name="distrito" value="<?php echo $distrito; ?>" id="distrito" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($distrito)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($distritos as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['distrito']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $distrito; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getDistritoSelect($distrito) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeDistrito = VirtualDeskSitePercursosPedestresHelper::excludeDistrito($distrito)?>
                                            <?php foreach($ExcludeDistrito as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['distrito']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <?php
                            $ListaDeConcelho = array();
                            if(!empty($distrito)) {
                                if( (int) $distrito > 0) $ListaDeConcelho = VirtualDeskSitePercursosPedestresHelper::getConcelho($distrito);
                            }
                            ?>
                            <div id="blocoConcelho" class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_CONCELHO' ); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="concelho" id="concelho" value="<?php echo $concelho;?>"
                                        <?php
                                        if(empty($distrito)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($concelho)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($ListaDeConcelho as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getConcelhoSelect($concelho);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeConcelho = VirtualDeskSitePercursosPedestresHelper::excludeConcelho($distrito, $concelho);?>
                                            <?php foreach($ExcludeConcelho as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>


                            <?php
                            $ListaDeFreguesias = array();
                            if(!empty($concelho)) {
                                if( (int) $concelho > 0) $ListaDeFreguesias = VirtualDeskSitePercursosPedestresHelper::getFreguesia($concelho);
                            }
                            ?>
                            <div id="blocoFreguesia" class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGUESIA' ); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="freguesia" id="freguesia" value="<?php echo $freguesia;?>"
                                        <?php
                                        if(empty($concelho)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($freguesia)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($ListaDeFreguesias as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $freguesia; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getFreguesiaSelect($freguesia);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeFreguesia = VirtualDeskSitePercursosPedestresHelper::excludeFreguesia($concelho, $freguesia);?>
                                            <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO'); ?><span class="required">*</span></label>
                                <?php $Zonamentos = VirtualDeskSitePercursosPedestresHelper::getZonamento()?>
                                <div class="value col-md-12">
                                    <select name="zonamento" value="<?php echo $zonamento; ?>" id="zonamento" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($zonamento)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($Zonamentos as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['zonamento']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $zonamento; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getZonamentoSelect($zonamento) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeZonamento = VirtualDeskSitePercursosPedestresHelper::excludeZonamento($zonamento)?>
                                            <?php foreach($ExcludeZonamento as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['zonamento']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGINICIO'); ?><span class="required">*</span></label>
                                <?php $Freguesias = VirtualDeskSitePercursosPedestresHelper::getFreguesiaEdited()?>
                                <div class="value col-md-12">
                                    <select name="freguesiaInicio" value="<?php echo $freguesiaInicio; ?>" id="freguesiaInicio" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($freguesiaInicio)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($Freguesias as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['freguesia']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $freguesiaInicio; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getFreguesiaSelect($freguesiaInicio) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeFreguesia = VirtualDeskSitePercursosPedestresHelper::excludeFreguesiaEdited($freguesiaInicio)?>
                                            <?php foreach($ExcludeFreguesia as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['freguesia']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGFIM'); ?><span class="required">*</span></label>
                                <?php $Freguesias = VirtualDeskSitePercursosPedestresHelper::getFreguesiaEdited()?>
                                <div class="value col-md-12">
                                    <select name="freguesiaFim" value="<?php echo $freguesiaFim; ?>" id="freguesiaFim" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($freguesiaFim)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($Freguesias as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['freguesia']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $freguesiaFim; ?>"><?php echo VirtualDeskSitePercursosPedestresHelper::getFreguesiaSelect($freguesiaFim) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeFreguesia = VirtualDeskSitePercursosPedestresHelper::excludeFreguesiaEdited($freguesiaFim)?>
                                            <?php foreach($ExcludeFreguesia as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['freguesia']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIOINICIO'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="sitioInicio" id="sitioInicio" maxlength="250" value="<?php echo $sitioInicio; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIOFIM'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="sitioFim" id="sitioFim" maxlength="250" value="<?php echo $sitioFim; ?>"/>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LOCALIDADES'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="10" name="localidades" id="localidades" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($localidades); ?></textarea>
                                </div>
                            </div>

                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUES'); ?></h3></legend>

                            <div class="mt-repeater">
                                <div data-repeater-list="DestaqueInput">
                                    <div data-repeater-item class="mt-repeater-item ">

                                        <div class="mt-repeater-input DestaqueInput DestaqueInput-new" data-provides="DestaqueInput">

                                            <div class="form-group all" data-trigger="DestaqueInput">
                                                <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUE'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaque" maxlength="500" value="<?php echo $destaque; ?>"/>
                                                </div>


                                                <div class="name col-md-12 second"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEEN'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaqueEN" maxlength="500" value="<?php echo $destaqueEN; ?>"/>
                                                </div>


                                                <div class="name col-md-12 second"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEFR'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaqueFR" maxlength="500" value="<?php echo $destaqueFR; ?>"/>
                                                </div>


                                                <div class="name col-md-12 second"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEDE'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="destaqueDE" maxlength="500" value="<?php echo $destaqueDE; ?>"/>
                                                </div>
                                            </div>

                                            <a href="javascript:;" class="btn btn-danger mt-repeater-delete DestaqueInput-exists" data-repeater-delete data-dismiss="DestaqueInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                        </div>

                                        <div class="mt-repeater-input">
                                        </div>

                                    </div>
                                </div>

                                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                    <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                            </div>

                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_HASTAGS'); ?></h3></legend>

                            <div class="mt-repeater Hastags">
                                <div data-repeater-list="HastagsInput">
                                    <div data-repeater-item class="mt-repeater-item ">

                                        <div class="mt-repeater-input HastagsInput HastagsInput-new" data-provides="HastagsInput">

                                            <div class="form-group all" data-trigger="HastagsInput">
                                                <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_HASTAG'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="hastags" maxlength="500" value="<?php echo $hastags; ?>"/>
                                                </div>
                                            </div>

                                            <a href="javascript:;" class="btn btn-danger mt-repeater-delete HastagsInput-exists" data-repeater-delete data-dismiss="HastagsInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                        </div>

                                        <div class="mt-repeater-input">
                                        </div>

                                    </div>
                                </div>

                                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                    <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                            </div>

                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_MULTIMEDIA'); ?></h3></legend>


                            <div class="form-group third left" id="uploadFieldCapa">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FOTOCAPA'); ?></label>
                                <div class="value col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_capa[]" id="fileupload_capa" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>


                            <div class="form-group third center" id="uploadFieldGPX">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GPX'); ?></label>
                                <div class="value col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_gpx[]" id="fileupload_gpx" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>


                            <div class="form-group third right" id="uploadFieldKML">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_KML'); ?></label>
                                <div class="value col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_kml[]" id="fileupload_kml" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOMECREDITO'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="nomecredito" id="nomecredito" maxlength="500" value="<?php echo $nomecredito; ?>"/>
                                </div>
                            </div>


                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LINKCREDITO'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="linkcredito" id="linkcredito" maxlength="500" value="<?php echo $linkcredito; ?>"/>
                                </div>
                            </div>


                            <div class="form-group all" id="uploadFieldGaleria">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GALERIA'); ?></label>
                                <div class="value col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_galeria[]" id="fileupload_galeria" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                </button>
                                <a class="btn default"
                                   href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres'); ?>"
                                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('percursospedestres.create4manager',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                    <?php echo JHtml::_('form.token'); ?>

                </form>
            </div>

        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/percursospedestres/tmpl/addnew4manager.js.php');
echo ('</script>');
?>