<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSitePercursosPedestresHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/virtualdesksite_percursospedestres.php');
    JLoader::register('VirtualDeskSitePercursosPedestresCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/fotoCapa_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/galeria_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresGPXFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/gpx_files.php');
    JLoader::register('VirtualDeskSitePercursosPedestresKMLFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/percursospedestres/kml_files.php');

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('percursospedestres');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('percursospedestres', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/percursospedestres/tmpl/percursospedestres.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputPercursosPedestres_Id = JFactory::getApplication()->input->getInt('percursospedestres_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSitePercursosPedestresHelper::getPercursosDetail4Manager($getInputPercursosPedestres_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
        $ListFilesAlert = array();

        $objFotoCapaFiles = new VirtualDeskSitePercursosPedestresCapaFilesHelper();
        $ListFilesFotoCapa = $objFotoCapaFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objGaleriaFiles = new VirtualDeskSitePercursosPedestresGaleriaFilesHelper();
        $ListFilesGaleria = $objGaleriaFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objGPXFiles = new VirtualDeskSitePercursosPedestresGPXFilesHelper();
        $ListFilesGPX = $objGPXFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objKMLFiles = new VirtualDeskSitePercursosPedestresKMLFilesHelper();
        $ListFilesKML = $objKMLFiles->getFileGuestLinkByRefId ($this->data->codigo);
    }

    $imgGaleria = VirtualDeskSitePercursosPedestresHelper::getImgGaleria($this->data->codigo);


    $PercursosPedestresEstadoList2Change = VirtualDeskSitePercursosPedestresHelper::getPercursosEstadoAllOptions($language_tag);

// Todo Ter parametro com o Id de Menu Principal de cada módulo
//$itemmenuid_lista = $params->get('agenda_menuitemid_list');

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

// Dados vazios...
if(empty($this->data)) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
    return false;
}

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_VER_DETALHE' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=edit4manager&percursospedestres_id=' . $this->escape($this->data->percursospedestres_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOVOPERCURSO' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">

                <div class="form-body">

                    <div class="portlet light">

                        <div class="row">


                            <div class="col-md-8">

                                <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                    <div class="portlet-body">

                                        <div class="bloco">

                                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_ESTADOPERCURSO'); ?></h3></legend>

                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_LISTA_REFERENCIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_LISTA_ESTADO' ); ?></div>
                                                    <div class="col-md-6 value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=percursospedestres.getNewEstadoName4ManagerByAjax&percursospedestres_id='. $this->escape($this->data->percursospedestres_id)); ?>" >
                                                        <span class="label <?php echo VirtualDeskSitePercursosPedestresHelper::getEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>

                                                    <?php
                                                    $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('percursospedestres', 'alterarestado4managers');
                                                    if($checkAlterarEstado4Managers===true ) : ?>
                                                        <div class="col-md-6">
                                                            <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                                        </div>

                                                        <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                                    </div>
                                                                    <div class="modal-body blocoAlterar2NewEstado">


                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-md-12 ">
                                                                                    <div class="form-group" style="padding:0">
                                                                                        <label> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                                        <div style="padding:0;">
                                                                                            <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                                <?php foreach($PercursosPedestresEstadoList2Change as $rowEstado) : ?>
                                                                                                    <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                                <?php endforeach; ?>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12 ">
                                                                                    <div class="form-group">
                                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                                        <textarea  rows="5" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" ></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group blocoIconsMsgAviso">
                                                                            <div class="row">
                                                                                <div class="col-md-12 text-center">
                                                                                    <span> <i class="fa"></i> </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12 text-center">
                                                                                    <span class="blocoIconsMsgAviso_Texto"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                        <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=percursospedestres.sendAlterar2NewEstado4ManagerByAjax&percursospedestres_id=' . $this->escape($this->data->percursospedestres_id)); ?>" >
                                                                        <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                                        </button>
                                                                        <span> <i class="fa"></i> </span>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>

                                                    <?php endif; ?>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="bloco">

                                            <legend>
                                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DADOSPERCURSO'); ?></h3>
                                            </legend>


                                            <div class="form-group all">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOMEPERCURSO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->percurso, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group all multiline">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO' ); ?></div>
                                                    <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao); ?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group all multiline">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_EN' ); ?></div>
                                                    <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricaoEN); ?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group all multiline">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_FR' ); ?></div>
                                                    <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricaoFR); ?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group all multiline">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCRICAO_DE' ); ?></div>
                                                    <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricaoDE); ?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_TIPOLOGIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->tipologia, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_PAISAGEM' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->paisagem, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <?php
                                                if($this->data->percurso_laurissilva == 1){
                                                    ?>
                                                        <div class="form-group third">
                                                            <div class="row static-info ">
                                                                <div class="name"></div>
                                                                <input type="checkbox" disabled="disabled" name="percLaurissilva" <?php echo 'checked="checked"'; ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PERCLAURISSILVA'); ?>
                                                            </div>
                                                        </div>
                                                    <?php
                                                }

                                                if($this->data->costa_laurissilva == 1){
                                                    ?>
                                                        <div class="form-group third">
                                                            <div class="row static-info ">
                                                                <div class="name"></div>
                                                                <input type="checkbox" disabled="disabled" name="costLaurissilva" <?php echo 'checked="checked"'; ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTALAURISSILVA'); ?>
                                                            </div>
                                                        </div>
                                                    <?php
                                                }

                                                if($this->data->costa_sol == 1){
                                                    ?>
                                                        <div class="form-group third">
                                                            <div class="row static-info ">
                                                                <div class="name"></div>
                                                                <input type="checkbox" disabled="disabled" name="costa_sol" <?php echo 'checked="checked"'; ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_COSTASOL'); ?>
                                                            </div>
                                                        </div>
                                                    <?php
                                                }

                                                if($this->data->mmg == 1){
                                                    ?>
                                                        <h4><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PLATAFORMASPUBLICADAS'); ?></h4>

                                                        <div class="form-group third">
                                                            <div class="row static-info ">
                                                                <div class="name"></div>
                                                                <input type="checkbox" disabled="disabled" name="mmg" <?php echo 'checked="checked"'; ?>> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_MMG'); ?>
                                                            </div>
                                                        </div>
                                                    <?php
                                                }
                                            ?>


                                        </div>

                                        <?php
                                            if($this->data->patrocinador != 0 && !empty($this->data->patrocinador)){
                                                ?>
                                                    <div class="bloco">

                                                        <legend>
                                                            <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_PATROCINADORES'); ?></h3>
                                                        </legend>

                                                        <div class="form-group all">
                                                            <div class="row static-info ">
                                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_PATROCINADOR' ); ?></div>
                                                                <div class="value"> <?php echo htmlentities( $this->data->nome_patrocinador, ENT_QUOTES, 'UTF-8');?> </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                <?php
                                            }
                                        ?>


                                        <div class="bloco">
                                            <legend>
                                                <h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FICHATECNICA'); ?></h3>
                                            </legend>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_SENTIDO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->sentido, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_TERRENO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->terreno, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_VERTIGENS' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->vertigens, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DIFICULDADE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->dificuldade, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTANCIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->distancia, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <?php
                                                if(!empty($this->data->duracao)){
                                                    $pieces = explode(",", $this->data->duracao);
                                                    $horas = $pieces[0];
                                                    if(strlen($pieces[1]) == 1){
                                                        $minutos = $pieces[1] . '0';
                                                    } else {
                                                        $minutos = $pieces[1];
                                                    }
                                                    $duracao = $horas . 'h' . $minutos . 'min';
                                                } else {
                                                    $duracao = '';
                                                }

                                            ?>

                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DURACAO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $duracao, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDEMAX' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->altitude_max, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_ALTITUDEMIN' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->altitude_min, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_SUBIDAACUMULADO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->subida_acumulado, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESCIDAACUMULADO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->descida_acumulado, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="bloco">

                                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_LOCALIZACAO'); ?></h3></legend>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DISTRITO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->distrito, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_CONCELHO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->concelho, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGUESIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_ZONAMENTO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->zonamento, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGINICIO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->inicio_freguesia, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_FREGFIM' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->fim_freguesia, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIOINICIO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->inicio_sitio, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group half">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_SITIOFIM' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->fim_sitio, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="form-group all multiline">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_LOCALIDADES' ); ?></div>
                                                    <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->localidades); ?> </div>
                                                </div>
                                            </div>

                                        </div>

                                        <?php
                                            if(!empty($this->data->destaques) || !empty($this->data->destaquesEN) || !empty($this->data->destaquesFR) || !empty($this->data->destaquesDE)){

                                                $explodeDestaques = explode(';;', $this->data->destaques);
                                                $explodeDestaquesEN = explode(';;', $this->data->destaquesEN);
                                                $explodeDestaquesFR = explode(';;', $this->data->destaquesFR);
                                                $explodeDestaquesDE = explode(';;', $this->data->destaquesDE);

                                                ?>
                                                    <div class="bloco">
                                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUES'); ?></h3></legend>
                                                        <?php
                                                            for($i=0; $i<count($explodeDestaques); $i++){
                                                                ?>
                                                                <div class="sectionTitle"> <?php echo JText::sprintf('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VIEW_DESTAQUE',$i + 1);?> </div>

                                                                <div class="box">
                                                                    <div class="form-group all">
                                                                        <div class="row static-info ">
                                                                            <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUE'); ?></div>
                                                                            <div class="value"> <?php echo htmlentities( $explodeDestaques[$i], ENT_QUOTES, 'UTF-8');?> </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group all">
                                                                        <div class="row static-info ">
                                                                            <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEEN'); ?></div>
                                                                            <div class="value"> <?php echo htmlentities( $explodeDestaquesEN[$i], ENT_QUOTES, 'UTF-8');?> </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group all">
                                                                        <div class="row static-info ">
                                                                            <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEFR'); ?></div>
                                                                            <div class="value"> <?php echo htmlentities( $explodeDestaquesFR[$i], ENT_QUOTES, 'UTF-8');?> </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group all">
                                                                        <div class="row static-info ">
                                                                            <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_DESTAQUEDE'); ?></div>
                                                                            <div class="value"> <?php echo htmlentities( $explodeDestaquesDE[$i], ENT_QUOTES, 'UTF-8');?> </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                        ?>
                                                    </div>
                                                <?php
                                            }


                                            if(!empty($this->data->hastags)){
                                                ?>
                                                <div class="bloco">
                                                    <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_HASTAGS'); ?></h3></legend>

                                                    <?php
                                                    $explodeHastags = explode(';;', $this->data->hastags);

                                                    for($i=0; $i<count($explodeHastags); $i++){
                                                        ?>
                                                        <div class="form-group all">
                                                            <div class="row static-info ">
                                                                <div class="name"> <?php echo JText::sprintf('COM_VIRTUALDESK_PERCURSOSPEDESTRES_VIEW_HASTAG',$i + 1); ?></div>
                                                                <div class="value"> <?php echo htmlentities( $explodeHastags[$i], ENT_QUOTES, 'UTF-8');?> </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <?php
                                            }

                                            if((int)$imgGaleria != 0){
                                                ?>
                                                    <div class="bloco" style="margin-bottom:0;">

                                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GALERIA'); ?></h3></legend>

                                                        <div id="vdPercursosPedestresFileGridGaleria" class="cbp">
                                                            <?php
                                                            if(!is_array($ListFilesGaleria)) $ListFilesGaleria = array();
                                                            foreach ($ListFilesGaleria as $rowFile) : ?>
                                                                <div class="cbp-item identity logos">
                                                                    <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                                        <div class="cbp-caption-defaultWrap">
                                                                            <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                                        <div class="cbp-caption-activeWrap">
                                                                            <div class="cbp-l-caption-alignLeft">
                                                                                <div class="cbp-l-caption-body">
                                                                                    <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                                    <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            <?php endforeach;?>
                                                        </div>

                                                    </div>
                                                <?php
                                            }
                                        ?>

                                        <div class="row static-info">
                                            <div class="col-md-12 ">
                                                <h6>
                                                    <?php
                                                    echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DATACRIACAO' ).$labelseparator;
                                                    echo htmlentities( $this->data->data_criacao, ENT_QUOTES, 'UTF-8');
                                                    if($this->data->data_criacao != $this->data->data_alteracao) echo ' , ' . JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_DATAALTERACAO' ) . ' ' . htmlentities( $this->data->data_alteracao, ENT_QUOTES, 'UTF-8');
                                                    ?>
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-4">

                                <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                    <div class="portlet-body">

                                        <div class="bloco">

                                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FOTOCAPA'); ?></h3></legend>

                                            <div id="vdPercursosPedestresFileGridCapa" class="cbp">
                                                <?php
                                                if(!is_array($ListFilesFotoCapa)) $ListFilesFotoCapa = array();
                                                foreach ($ListFilesFotoCapa as $rowFile) : ?>
                                                    <div class="cbp-item identity logos">
                                                        <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                            <div class="cbp-caption-activeWrap">
                                                                <div class="cbp-l-caption-alignLeft">
                                                                    <div class="cbp-l-caption-body">
                                                                        <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                        <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>

                                            <div class="form-group all">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_NOMECREDITO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->nomecredito, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="form-group all">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_PERCURSOSPEDESTRES_LINKCREDITO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->linkcredito, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="portlet light bg-inverse bordered ">
                                    <div class="portlet-body">

                                        <div class="bloco">

                                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_GPX'); ?></h3></legend>

                                            <div id="vdPercursosPedestresFileGridGPX" class="cbp">
                                                <?php
                                                if(!is_array($ListFilesGPX)) $ListFilesGPX = array();
                                                foreach ($ListFilesGPX as $rowFile) : ?>
                                                    <div class="cbp-item identity link">
                                                        <a href="<?php echo$rowFile->guestlink; ?>"><?php echo $rowFile->desc; ?></a>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="portlet light bg-inverse bordered ">
                                    <div class="portlet-body">

                                        <div class="bloco">

                                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_KML'); ?></h3></legend>

                                            <div id="vdPercursosPedestresFileGridKML" class="cbp">
                                                <?php
                                                if(!is_array($ListFilesKML)) $ListFilesKML = array();
                                                foreach ($ListFilesKML as $rowFile) : ?>
                                                    <div class="cbp-item identity link">
                                                        <a href="<?php echo$rowFile->guestlink; ?>"><?php echo $rowFile->desc; ?></a>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>


                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=edit4manager&percursospedestres_id=' . $this->escape($this->data->percursospedestres_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=percursospedestres&layout=list4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('percursospedestres_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->percursospedestres_id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/percursospedestres/tmpl/view4manager.js.php');
    echo ('</script>');
?>