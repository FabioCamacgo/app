<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAguaBackofficeHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/Agua/virtualdesksite_agua.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agua');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agua', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agua/tmpl/agua.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getRefLeitura = JFactory::getApplication()->input->getInt('leitura');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAguaBackofficeHelper::getLeiturasDetail4Manager($getRefLeitura);


    $LeiturasEstadoList2Change = VirtualDeskSiteAguaBackofficeHelper::getEstadoAllOptions($language_tag);

    // Todo Ter parametro com o Id de Menu Principal de cada módulo
    //$itemmenuid_lista = $params->get('agenda_menuitemid_list');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
        return false;
    }

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-drop font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_('COM_VIRTUALDESK_LEITURA_AGUA_LISTA_DETALHE'); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">

                <div class="form-body">

                    <div class="portlet light">

                        <div class="col-md-12">

                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend>
                                            <h3><?php echo JText::_('COM_VIRTUALDESK_LEITURA_AGUA_DADOSLEITURA'); ?></h3>
                                        </legend>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_LEITURA_AGUA_NIF' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nif, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_LEITURA_AGUA_CONTADOR' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->num_contador, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_LEITURA_AGUA_LEITURA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->leitura, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_LEITURA_AGUA_DATA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->data_leitura, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="form-group half state">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_LEITURA_AGUA_ESTADO' ); ?></div>
                                                <div class="value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agua.getNewEstadoName4ManagerByAjax&leitura='. $this->escape($this->data->id)); ?>" >
                                                    <span class="label <?php echo VirtualDeskSiteAguaBackofficeHelper::getEstadoCSS($this->data->id_estado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group half stateButton">
                                            <?php
                                            $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agua', 'alterarestado4managers');
                                            if($checkAlterarEstado4Managers===true ) : ?>
                                                <div class="botaoAlterarEstado text-left">
                                                    <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_LEITURA_AGUA_CHANGE_ESTADO' ); ?></button>
                                                </div>

                                                <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                            </div>
                                                            <div class="modal-body blocoAlterar2NewEstado">


                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-12 ">
                                                                            <div class="form-group" style="padding:0">
                                                                                <label> <?php echo JText::_( 'COM_VIRTUALDESK_LEITURA_AGUA_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                                <div style="padding:0;">
                                                                                    <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                        <?php foreach($LeiturasEstadoList2Change as $rowEstado) : ?>
                                                                                            <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                        <?php endforeach; ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group blocoIconsMsgAviso">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <span> <i class="fa"></i> </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <span class="blocoIconsMsgAviso_Texto"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                                <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agua.sendAlterar2NewEstado4ManagerByAjax&leitura=' . $this->escape($this->data->id)); ?>" >
                                                                <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                                </button>
                                                                <span> <i class="fa"></i> </span>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>

                                            <?php endif; ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=list4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('leitura',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id_leitura) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/view4manager.js.php');
    echo ('</script>');
?>