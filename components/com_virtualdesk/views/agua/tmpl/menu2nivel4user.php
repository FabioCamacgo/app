<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteAguaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agua.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('agua', 'list4users');
    if($vbHasAccess===false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');


?>


    <div class="portlet light bordered">


        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            </div>

            <div class="actions">

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

            </div>

        </div>

        <div class="portlet-body ">


            <div class="tabbable-line nav-justified ">
                <ul class="nav nav-tabs">
                    <li class="active">

                        <a href="#tab_AguaLeituras" data-toggle="tab">
                            <h4><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_LEITURAS'); ?>
                                <i class="fa fa-tachometer"></i>
                            </h4>
                        </a>

                    </li>
                    <li class="">
                        <a href="#tab_pedidos" data-toggle="tab">
                            <h4><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_NOVOSPEDIDOS'); ?>
                                <i class="fa fa-wrench"></i>
                            </h4>
                        </a>
                    </li>
                </ul>





                <div class="tab-content">
                    <div class="tab-pane active" id="tab_AguaLeituras">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_LISTALEITURAS'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_LISTALEITURAS'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_COMUNICARLEITURA'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_COMUNICARLEITURA'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_pedidos">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_CONTADOR'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_CONTADOR'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontador'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_CONTRATO'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_CONTRATO'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=menu3nivelcontrato'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_FATURACAO'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_FATURACAO'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=menu3nivelfaturacao'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_RAMAL'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_RAMAL'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=menu3nivelramal'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AGUA_PEDIDOS'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo JText::_('COM_VIRTUALDESK_AGUA_PEDIDOS'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=list4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php
    echo $localScripts;
?>