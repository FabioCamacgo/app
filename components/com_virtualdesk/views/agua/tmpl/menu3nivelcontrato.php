<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteAguaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agua.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('agua', 'list4users');
    if($vbHasAccess===false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>


    <div class="portlet light bordered">

        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua#tab_pedidos'); ?>"><span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_CONTRATO'); ?></span></a>
            </div>

            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua#tab_pedidos'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>">
                    <i class="fa fa-arrow-left"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
            </div>

        </div>

        <div class="portlet-body ">

            <div class="tabbable-line nav-justified ">

                <div class="tab-content">
                    <div class="row">

                        <?php
                            $Enabled_AltTitularContador = VirtualDeskSiteFormmainHelper::checkFormEnabled('K5tst0Q1');
                            $Enabled_CancContrato = VirtualDeskSiteFormmainHelper::checkFormEnabled('TIt3jWQt');
                            $Enabled_ContratoPrestServicos = VirtualDeskSiteFormmainHelper::checkFormEnabled('aNy9OthF');
                            $Enabled_RestAbAgua = VirtualDeskSiteFormmainHelper::checkFormEnabled('o8VoanJp');
                        ?>

                        <?php if($Enabled_AltTitularContador == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('K5tst0Q1'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('K5tst0Q1'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=addnewalttitularcontador4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if($Enabled_CancContrato == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('TIt3jWQt'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('TIt3jWQt'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=addnewcanccontrato4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if($Enabled_ContratoPrestServicos == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('aNy9OthF'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('aNy9OthF'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=addnewcontratoprestservicos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if($Enabled_RestAbAgua == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('o8VoanJp'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('o8VoanJp'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=addnewrestabagua4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>



<?php
echo $localScripts;
?>