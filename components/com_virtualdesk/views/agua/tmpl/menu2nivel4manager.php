<?php
    /**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteAguaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agua.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('agua', 'list4managers');
    $vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}


</style>



<div class="portlet light bordered">


    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>

    </div>

    <div class="portlet-body ">


        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">
                <li class="active">

                    <a href="#tab_AguaLeituras" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_LEITURAS'); ?>
                            <i class="fa fa-tachometer"></i>
                        </h4>
                    </a>

                </li>
                <li class="">
                    <a href="#tab_AguaServicos" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_AGUA_MENU_SERVICOS'); ?>
                            <i class="fa fa-wrench"></i>
                        </h4>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_AguaLeituras">


                    <div class="row">
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title">Lista das Leituras</h3>
                                            <p class="kt-callout__desc">
                                                Lista das Leituras
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=list4manager'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Aceder</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title">Comunicar Leitura</h3>
                                            <p class="kt-callout__desc">
                                                Comunicar leitura.
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">

                        </div>
                    </div>



                </div>

                <div class="tab-pane " id="tab_AguaServicos">

                    <div class="pricing-content-2">
                        <div class="pricing-table-container">
                            <div class="row padding-fix">



                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Aderir à Fatura Eletrónica</h3>
                                                        <p class="kt-callout__desc">
                                                            Aderir à Fatura Eletrónica
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Aderir ao Débito Direto</h3>
                                                        <p class="kt-callout__desc">
                                                            Aderir ao Débito Direto
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Celebrar Contrato</h3>
                                                        <p class="kt-callout__desc">
                                                            Celebrar Contrato
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Cancelar Contrato</h3>
                                                        <p class="kt-callout__desc">
                                                            Cancelar Contrato
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Alteração de Morada</h3>
                                                        <p class="kt-callout__desc">
                                                            Alteração de Morada
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Mudança de Contador</h3>
                                                        <p class="kt-callout__desc">
                                                            Mudança de Contador
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Ramais de Ligação</h3>
                                                        <p class="kt-callout__desc">
                                                            Ramais de Ligação
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Execução de Canalizações</h3>
                                                        <p class="kt-callout__desc">
                                                            Execução de Canalizações
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--info kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Pagamento em Prestações</h3>
                                                        <p class="kt-callout__desc">
                                                            Pagamento em Prestações
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--danger kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Tarifário de Família Numerosa</h3>
                                                        <p class="kt-callout__desc">
                                                            Tarifário de Família Numerosa
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout t-callout--info kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Tarifário Social</h3>
                                                        <p class="kt-callout__desc">
                                                            Tarifário Social
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Morada para Envio da Fatura</h3>
                                                        <p class="kt-callout__desc">
                                                            Morada para Envio da Fatura
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success ">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Inquérito de Satisfação</h3>
                                                        <p class="kt-callout__desc">
                                                            Inquérito de Satisfação
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>



<?php

echo $localScripts;

?>


