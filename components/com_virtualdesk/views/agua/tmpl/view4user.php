<?php

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteAguaBackofficeHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/Agua/virtualdesksite_agua.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkDetailReadAccess('agua');
    if( $vbHasAccess===false ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts = '';
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputPedido_Id = JFactory::getApplication()->input->getInt('pedido_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAguaBackofficeHelper::getDadosFormView4UserDetail($getInputPedido_Id);

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_AGUA_EMPTYLIST'), 'error' );
        return false;
    }

    $formId = VirtualDeskSiteFormmainHelper::getFormIdByIdrel($getInputPedido_Id, 'agua');

    if($formId == 21){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewvercontador4user.php');
    } else if($formId == 22){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewaltcontador4user.php');
    } else if($formId == 23){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewsubscontador4user.php');
    } else if($formId == 24){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewalttitularcontador4user.php');
    } else if($formId == 25){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewcanccontrato4user.php');
    } else if($formId == 26){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewcontratoprestservicos4user.php');
    } else if($formId == 27){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewrestabagua4user.php');
    } else if($formId == 28){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewfateletronica4user.php');
    } else if($formId == 29){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewdebdireto4user.php');
    } else if($formId == 30){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewtarespecial4user.php');
    } else if($formId == 31){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewramalagua4user.php');
    } else if($formId == 32){
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewramalaguaresiduais4user.php');
    }

    echo $localScripts;

?>