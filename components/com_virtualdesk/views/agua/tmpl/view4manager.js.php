<?php
defined('_JEXEC') or die;
?>


var ButtonHandle = function () {

    var handleButton = function (evt) {

        jQuery(".newmsghistSend").on('click',function(evt,data){

            let vd_url_send    = jQuery(this).data('vd-url-send');
            let elText         = jQuery(this).closest('div.rowSendNewMsgText').find('.sendnewmsgtext');
            let sendnewmsgtext = elText.val();
            let elBlocoOptions = jQuery(this).closest('div.rowSendNewMsgText').next('div.newmsghistSendOptionsBlock');

            let elVisible4User = elBlocoOptions.find('input.newmsghistSendSetVisible4User');
            let setvisible4user = elVisible4User.bootstrapSwitch('state');

            let elTipo  = elBlocoOptions.find('select.newmsghistSendSetTipo');
            let setipomsg = elTipo.val();

            vdAjaxCall.sendNewMessage2Hist(jQuery(this), vd_url_send, elText, sendnewmsgtext, setvisible4user, setipomsg );

        });
    };


    var handleButtonAlterarEstadoModal = function (evt) {

        jQuery(".btAlterarEstadoModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarNewEstadoModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2NewEstado');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2newestadoSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewProcManagerId   = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let elNewProcManagerDesc = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
            elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonAlterarEstado = function (evt) {

        jQuery(".alterar2newestadoSend").on('click',function(evt,data){
            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2NewEstado');

            let elNewEstadoDesc  = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            let setNewEstadoDesc = elNewEstadoDesc.val();

            let elNewEstadoId  = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let setNewEstadoId = elNewEstadoId.val();

            vdAjaxCall.sendAlterar2NewEstado(jQuery(this), vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId );

        });
    };


    var handleButtonAlterarReencaminharModal = function (evt) {

        jQuery(".btAlterarReencaminharModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarReencaminharModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2Reencaminhar');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2reencaminharSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewUserId           = elBlocoOptions.find('select.Alterar2ReencaminharUserId');
            let elNewGroupId          = elBlocoOptions.find('select.Alterar2ReencaminharGroupId');
            let elNewReencaminharDesc = elBlocoOptions.find('input.Alterar2ReencaminharDesc');
            elNewUserId.removeAttr('disabled').removeClass('disabled');
            elNewGroupId.removeAttr('disabled').removeClass('disabled');
            elNewReencaminharDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonReencaminhar = function (evt) {

        jQuery(".alterar2reencaminharSend").on('click',function(evt,data){

            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2Reencaminhar');

            let elDesc  = elBlocoOptions.find('input.Alterar2ReencaminharDesc');
            let setDesc = elDesc.val();

            let elUserId     = elBlocoOptions.find('select.Alterar2ReencaminharUserId');
            let elUserIdData = elUserId.select2('data');
            let setUserId    = [];
            var uId;
            for (uId in elUserIdData) {
                setUserId.push(elUserIdData[uId].id);
            }

            let elGroupId     = elBlocoOptions.find('select.Alterar2ReencaminharGroupId');
            let elGroupIdData = elGroupId.select2('data');
            let setGroupId    = [];
            var gId;
            for (gId in elGroupIdData) {
                setGroupId.push(elGroupIdData[gId].id);
            }

            vdAjaxCall.sendAlterarReencaminhar(jQuery(this), vd_url_send, elUserId, setUserId, elGroupId,setGroupId, elDesc, setDesc  );
        });
    };


    var handleButtonAlterarProcManagerModal = function (evt) {

        jQuery(".btAlterarProcManagerModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarProcManagerModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2NewProcManager');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2newprocmanagerSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewProcManagerId   = elBlocoOptions.find('select.Alterar2NewProcManagerId');
            let elNewProcManagerDesc = elBlocoOptions.find('input.Alterar2NewProcManagerDesc');
            elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
            elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonAlterarProcManager = function (evt) {

        jQuery(".alterar2newprocmanagerSend").on('click',function(evt,data){
            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2NewProcManager');

            let elNewProcManagerDesc  = elBlocoOptions.find('input.Alterar2NewProcManagerDesc');
            let setNewProcManagerDesc = elNewProcManagerDesc.val();

            let elNewProcManagerId  = elBlocoOptions.find('select.Alterar2NewProcManagerId');
            let setNewProcManagerId = elNewProcManagerId.val();

            vdAjaxCall.sendAlterar2NewProcManager(jQuery(this), vd_url_send, elNewProcManagerDesc, elNewProcManagerId, setNewProcManagerDesc, setNewProcManagerId );
        });
    };


    var handleButtonNovaTarefaModal = function (evt) {

        jQuery(".btNovaTarefaModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#NovaTarefaModal");
            let elBlocoOptions = elModal.find('div.blocoNovaTarefa');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".novatarefaSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewUserId           = elBlocoOptions.find('select.NovaTarefaUserId');
            let elNewGroupId          = elBlocoOptions.find('select.NovaTarefaGroupId');
            let elNewNovaTarefaDesc = elBlocoOptions.find('input.NovaTarefaDesc');
            elNewUserId.removeAttr('disabled').removeClass('disabled');
            elNewGroupId.removeAttr('disabled').removeClass('disabled');
            elNewNovaTarefaDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonNovaTarefa = function (evt) {

        jQuery(".novatarefaSend").on('click',function(evt,data){

            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoNovaTarefa');

            let elNome  = elBlocoOptions.find('input.NovaTarefaNome');
            let setNome = elNome.val();

            let elDesc  = elBlocoOptions.find('textarea.NovaTarefaDesc');
            let setDesc = elDesc.val();

            let elUserId     = elBlocoOptions.find('select.NovaTarefaUserId');
            let elUserIdData = elUserId.select2('data');
            let setUserId    = [];
            var uId;
            for (uId in elUserIdData) {
                setUserId.push(elUserIdData[uId].id);
            }

            let elGroupId     = elBlocoOptions.find('select.NovaTarefaGroupId');
            let elGroupIdData = elGroupId.select2('data');
            let setGroupId    = [];
            var gId;
            for (gId in elGroupIdData) {
                setGroupId.push(elGroupIdData[gId].id);
            }

            vdAjaxCall.sendNovaTarefa(jQuery(this), vd_url_send, elUserId, setUserId, elGroupId,setGroupId, elDesc, setDesc, elNome, setNome);
        });
    };


    var handleButtonTarefaAlterarEstadoModal = function (evt) {

        jQuery(".btTarefaAlterarEstadoModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let tarefaID    = jQuery(this).data('vd-tarefa-id')
            let newEstadoID = jQuery(this).data('vd-newestado-id');

            let elModal        = jQuery("#TarefaAlterarEstadoModal");
            let elBlocoOptions = elModal.find('div.blocoTarefaAlterarEstado');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".tarefaalterarestadoSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');
            elBtAlterar.attr('data-vd-tarefa-id',0);
            elBtAlterar.attr('data-vd-tarefa-id',tarefaID);

            let elNewEstadoSel = elBlocoOptions.find('select.TarefaAlterarNewEstadoId');
            elNewEstadoSel.val(newEstadoID);
            elNewEstadoSel.selectpicker('refresh');
            let elObs = elBlocoOptions.find('textarea.TarefaAlterarObs');
            elNewEstadoSel.removeAttr('disabled').removeClass('disabled');
            elObs.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            // Carrega dados e coloca dos valores nos campos
            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');
            vdAjaxCall.getTarefaAlterarEstadoData(jQuery(this), urlGetContent, tarefaID, elBlocoOptions, elModalContent);

            elModal.modal('show');
        });
    };


    var handleButtonTarefaAlterarEstado = function (evt) {

        jQuery(".tarefaalterarestadoSend").on('click',function(evt,data){

            let vd_url_send   = jQuery(this).data('vd-url-send');
            let vd_tarefa_id  = jQuery(this).attr('data-vd-tarefa-id'); // bug com o data(), só funcionou com o attr

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoTarefaAlterarEstado');

            let elObs  = elBlocoOptions.find('textarea.TarefaAlterarObs');
            let setObs = elObs.val();

            let elNewEstadoId  = elBlocoOptions.find('select.TarefaAlterarNewEstadoId');
            let setNewEstadoId = elNewEstadoId.val();

            vdAjaxCall.sendTarefaAlterarEstado(jQuery(this), vd_url_send, vd_tarefa_id, elNewEstadoId, setNewEstadoId, elObs, setObs);
        });
    };


    var handleButtonTarefaEditarModal = function (evt) {

        jQuery(".btTarefaEditarModalOpen").on('click',function(evt,data){
            let tarefaID    = jQuery(this).data('vd-tarefa-id')

            let elModal        = jQuery("#TarefaEditarModal");
            let elBlocoOptions = elModal.find('div.blocoTarefaEditar');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');

            let elBtAlterar = elModalContent.find(".tarefaeditarSend");
            elBtAlterar.attr('data-vd-tarefa-id',0);
            elBtAlterar.attr('data-vd-tarefa-id',tarefaID);

            // Carrega dados e coloca dos valores nos campos
            vdAjaxCall.getTarefaEditarDetailData(jQuery(this), urlGetContent, tarefaID, elBlocoOptions, elModalContent);

            elModal.modal('show');
        });
    };


    var handleButtonTarefaEditar = function (evt) {

        jQuery(".tarefaeditarSend").on('click',function(evt,data){

            let vd_url_send   = jQuery(this).data('vd-url-send');
            let vd_tarefa_id  = jQuery(this).attr('data-vd-tarefa-id'); // bug com o data(), só funcionou com o attr

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoTarefaEditar');

            let elNome  = elBlocoOptions.find('input.TarefaEditarNome');
            let setNome = elNome.val();
            let elDesc  = elBlocoOptions.find('textarea.TarefaEditarDesc');
            let setDesc = elDesc.val();

            let elEstadoId  = elBlocoOptions.find('select.TarefaEditarEstadoId');
            let setEstadoId = elEstadoId.val();

            let elUserId     = elBlocoOptions.find('select.TarefaEditarUserId');
            let elUserIdData = elUserId.select2('data');
            let setUserId    = [];
            var uId;
            for (uId in elUserIdData) {
                setUserId.push(elUserIdData[uId].id);
            }

            let elGroupId     = elBlocoOptions.find('select.TarefaEditarGroupId');
            let elGroupIdData = elGroupId.select2('data');
            let setGroupId    = [];
            var gId;
            for (gId in elGroupIdData) {
                setGroupId.push(elGroupIdData[gId].id);
            }

            vdAjaxCall.sendTarefaEditar(jQuery(this), vd_url_send, vd_tarefa_id, elEstadoId, setEstadoId, elNome, setNome, elDesc, setDesc, elUserId, setUserId, elGroupId,setGroupId);
        });
    };


    var handleButtonTarefaHistoricoModal = function (evt) {

        jQuery(".btTarefaHistoricoModalOpen").on('click',function(evt,data){
            let tarefaID    = jQuery(this).data('vd-tarefa-id')

            let elModal        = jQuery("#TarefaHistoricoModal");
            let elBlocoOptions = elModal.find('div.blocoTarefaHistorico');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');

            // Carrega dados e coloca dos valores nos campos
            vdAjaxCall.getTarefaHistoricoData(jQuery(this), urlGetContent, tarefaID, elBlocoOptions, elModalContent);

            TableDatatablesManaged.ReloadTableTarefasHistoricoModal(tarefaID);

            elModal.modal('show');
        });
    };


    return {
        //main function to initiate the module
        init: function () {
            handleButton();
            handleButtonAlterarEstadoModal();
            handleButtonAlterarEstado();
            handleButtonAlterarReencaminharModal();
            handleButtonReencaminhar();
            handleButtonAlterarProcManagerModal();
            handleButtonAlterarProcManager();
            handleButtonNovaTarefaModal();
            handleButtonNovaTarefa();
            handleButtonTarefaAlterarEstado();
            handleButtonTarefaEditar();

        },
        handleButtonTarefaAlterarEstadoModal : handleButtonTarefaAlterarEstadoModal,
        handleButtonTarefaEditarModal : handleButtonTarefaEditarModal,
        handleButtonTarefaHistoricoModal : handleButtonTarefaHistoricoModal

    };

}();


var ModalHandle = function () {

    var handleModalAlterarNewEstado = function (evt) {
        jQuery("#AlterarNewEstadoModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalAlterarReencaminhar = function (evt) {
        jQuery("#AlterarReencaminharModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalAlterarProcManager = function (evt) {
        jQuery("#AlterarProcManagerModal").modal({
            show: false,
            keyboard: true
        })
    };


    var handleModalNovaTarefa = function (evt) {
        jQuery("#NovaTarefaModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalTarefaAlterarEstado = function (evt) {
        jQuery("#TarefaAlterarEstadoModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalTarefaEditar = function (evt) {
        jQuery("#TarefaEditarModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalTarefaHistorico = function (evt) {
        jQuery("#TarefaHistoricoModal").modal({
            show: false,
            keyboard: true
        })
    };

    return {
        //main function to initiate the module
        init: function () {
            handleModalAlterarNewEstado();
            handleModalAlterarProcManager();
            handleModalAlterarReencaminhar();
            handleModalNovaTarefa();
            handleModalTarefaAlterarEstado();
            handleModalTarefaEditar();
            handleModalTarefaHistorico();
        }
    };
}();


var vdAjaxCall = function () {

    // Gravar novo estado
    var sendAlterar2NewEstado = function (el, vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elNewEstadoDesc.attr('disabled','disabled').addClass('disabled');
        elNewEstadoId.attr('disabled','disabled').addClass('disabled');
        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setNewEstadoDesc =  encodeURIComponent(setNewEstadoDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setNewEstadoDesc=' + setNewEstadoDesc + '&setNewEstadoId=' + setNewEstadoId  ,
            indexValue: {el:el, vd_url_send:vd_url_send, elNewEstadoId:elNewEstadoId, elNewEstadoDesc:elNewEstadoDesc, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError },
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);

                window.location.href = '<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=list4manager'); ?>';

                setTimeout(
                    function()
                    {
                        jQuery("#AlterarNewEstadoModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        vdAjaxCall.setNewEstadoStaticVal();

                        el.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.val('')
                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

    return {
        sendAlterar2NewEstado   : sendAlterar2NewEstado,
    };

}();



var ComponentsSelect2 = function() {
    var handleDemo = function() {
        jQuery.fn.select2.defaults.set("theme", "bootstrap");
        var placeholder = "";
        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,
            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


jQuery(document).ready(function() {

    ButtonHandle.init();

    ModalHandle.init();

    ComponentsSelect2.init();

});