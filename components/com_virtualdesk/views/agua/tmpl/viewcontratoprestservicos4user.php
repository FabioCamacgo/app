<?php

    $tag_form='aNy9OthF';
    $modulo_id=VirtualDeskSiteFormmainHelper::getModuleId('agua');

    $fieldsForm = VirtualDeskSiteFormmainHelper::getFieldsData($tag_form, $modulo_id);

    $newFieldsExists = array();
    foreach ($fieldsForm as $keyF) {
        $newFieldsExists[] = $keyF['tag'];
    }

?>

    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_MEUSPEDIDOS' ); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_VER_DETALHE' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=list4user'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=editcontratoprestservicos4user&vdcleanstate=1&pedido_id=' . $this->escape($getInputPedido_Id)); ?>" class="btn btn-circle btn-outline green vdBotaoEditar">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=menu2nivel4user#tab_pedidos'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NOVOPEDIDO' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">
                <div class="form-body">
                    <div class="portlet light">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                    <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGUA_DADOSREQUERENTE'); ?></div></div>
                                    <div class="portlet-body">

                                        <?php if(in_array('fieldName',$newFieldsExists)) :?>
                                            <div class="field fieldName">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NOME' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldName']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldMorada',$newFieldsExists)) :?>
                                            <div class="field fieldMorada">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_MORADA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldMorada']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldPorta',$newFieldsExists)) :?>
                                            <div class="field fieldPorta">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NUM_PORTA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldPorta']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldLote',$newFieldsExists)) :?>
                                            <div class="field fieldLote">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOTE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldLote']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldLocalidade',$newFieldsExists)) :?>
                                            <div class="field fieldLocalidade">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOCALIDADE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldLocalidade']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldCodPostal',$newFieldsExists)) :?>
                                            <div class="field fieldCodPostal">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_CODPOSTAL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldCodPostal']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldfreguesia',$newFieldsExists)) :?>
                                            <div class="field fieldfreguesia">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_FREGUESIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldfreguesia']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldNif',$newFieldsExists)) :?>
                                            <div class="field fieldNif">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NIF' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldNif']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldDocId',$newFieldsExists)) :?>
                                            <div class="field fieldDocId">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_TIPODOCIDENTIFICACAO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldDocId']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldNumId',$newFieldsExists)) :?>
                                            <div class="field fieldNumId">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NUMERO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldNumId']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldValId',$newFieldsExists)) :?>
                                            <?php
                                            if($this->data['fieldValId']->val_date == '' || $this->data['fieldValId']->val_date == 0 || $this->data['fieldValId']->val_date == Null){
                                                $dataValidadeInverted = '';
                                            } else {
                                                $dataValidade = explode("-", $this->data['fieldValId']->val_date);
                                                $dataValidadeInverted = $dataValidade[2] . '-' . $dataValidade[1] . '-' . $dataValidade[0];
                                            }

                                            ?>

                                            <div class="field fieldValId">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_VALIDADE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $dataValidadeInverted, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldCCP',$newFieldsExists)) :?>
                                            <div class="field fieldCCP">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_CCP' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldCCP']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldTelemovel',$newFieldsExists)) :?>
                                            <div class="field fieldTelemovel">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_TELEMOVEL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldTelemovel']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldTelefone',$newFieldsExists)) :?>
                                            <div class="field fieldTelefone">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_TELEFONE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldTelefone']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldFax',$newFieldsExists)) :?>
                                            <div class="field fieldFax">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_FAX' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldFax']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldEmail',$newFieldsExists)) :?>
                                            <div class="field fieldEmail">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_EMAIL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldEmail']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if(in_array('fieldQualidade',$newFieldsExists)) :?>
                                            <div class="field fieldQualidade">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_QUALIDADE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data['fieldQualidade']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>

                                    <?php if(in_array('fieldNomeRep',$newFieldsExists) || in_array('fieldMoradaRep',$newFieldsExists) || in_array('fieldPortaRep',$newFieldsExists) || in_array('fieldLoteRep',$newFieldsExists) || in_array('fieldLocalRep',$newFieldsExists) || in_array('fieldCodPostRep',$newFieldsExists) || in_array('fieldfreguesiaRep',$newFieldsExists) || in_array('fieldNifRep',$newFieldsExists) || in_array('fieldDocIdRep',$newFieldsExists) || in_array('fieldNumIdRep',$newFieldsExists) || in_array('fieldValIdRep',$newFieldsExists) || in_array('fieldCCPRep',$newFieldsExists) || in_array('fieldTelemovelRep',$newFieldsExists) || in_array('fieldTelefoneRep',$newFieldsExists) || in_array('fieldFaxRep',$newFieldsExists) || in_array('fieldEmailRep',$newFieldsExists) || in_array('fieldQualidadeRep',$newFieldsExists) || in_array('fieldOutraQualRep',$newFieldsExists)) :?>

                                        <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGUA_DADOSREPRESENTANTE'); ?></div></div>
                                        <div class="portlet-body">
                                            <?php if(in_array('fieldNomeRep',$newFieldsExists)) :?>
                                                <div class="field fieldNomeRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NOME' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldNomeRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldMoradaRep',$newFieldsExists)) :?>
                                                <div class="field fieldMoradaRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_MORADA' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldMoradaRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldPortaRep',$newFieldsExists)) :?>
                                                <div class="field fieldPortaRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NUM_PORTA' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldPortaRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldLoteRep',$newFieldsExists)) :?>
                                                <div class="field fieldLoteRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOTE' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldLoteRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldLocalRep',$newFieldsExists)) :?>
                                                <div class="field fieldLocalRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOCALIDADE' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldLocalRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldCodPostRep',$newFieldsExists)) :?>
                                                <div class="field fieldCodPostRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_CODPOSTAL' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldCodPostRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldfreguesiaRep',$newFieldsExists)) :?>
                                                <div class="field fieldfreguesiaRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_FREGUESIA' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldfreguesiaRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldNifRep',$newFieldsExists)) :?>
                                                <div class="field fieldNifRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NIF' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldNifRep']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldDocIdRep',$newFieldsExists)) :?>
                                                <div class="field fieldDocIdRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_TIPODOCIDENTIFICACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldDocIdRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldNumIdRep',$newFieldsExists)) :?>
                                                <div class="field fieldNumIdRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NUMERO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldNumIdRep']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldValIdRep',$newFieldsExists)) :?>
                                                <?php
                                                if($this->data['fieldValIdRep']->val_date == '' || $this->data['fieldValIdRep']->val_date == 0 || $this->data['fieldValIdRep']->val_date == Null){
                                                    $dataValidadeReQInverted = '';
                                                } else {
                                                    $dataValidadeReQ = explode("-", $this->data['fieldValIdRep']->val_date);
                                                    $dataValidadeReQInverted = $dataValidadeReQ[2] . '-' . $dataValidadeReQ[1] . '-' . $dataValidadeReQ[0];
                                                }
                                                ?>

                                                <div class="field fieldValIdRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_VALIDADE' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $dataValidadeReQInverted, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldCCPRep',$newFieldsExists)) :?>
                                                <div class="field fieldCCPRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_CCP' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldCCPRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldTelemovelRep',$newFieldsExists)) :?>
                                                <div class="field fieldTelemovelRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_TELEMOVEL' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldTelemovelRep']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldTelefoneRep',$newFieldsExists)) :?>
                                                <div class="field fieldTelefoneRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_TELEFONE' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldTelefoneRep']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldFaxRep',$newFieldsExists)) :?>
                                                <div class="field fieldFaxRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_FAX' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldFaxRep']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldEmailRep',$newFieldsExists)) :?>
                                                <div class="field fieldEmailRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_EMAIL' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldEmailRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldQualidadeRep',$newFieldsExists)) :?>
                                                <div class="field fieldQualidadeRep">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_QUALIDADE' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldQualidadeRep']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                        </div>

                                    <?php endif; ?>
                                    
                                    <?php if(in_array('fieldtipopedido',$newFieldsExists) || in_array('fieldOutroTipoPedido',$newFieldsExists) || in_array('fieldTipoConsumidor',$newFieldsExists) || in_array('fieldOutroTipoConsumidor',$newFieldsExists) || in_array('fieldMoradaPedido',$newFieldsExists) || in_array('fieldNumPedido',$newFieldsExists) || in_array('fieldLotePedido',$newFieldsExists) || in_array('fieldLocalidadePedido',$newFieldsExists) || in_array('fieldCodPostalPedido',$newFieldsExists) || in_array('fieldMoradaFaturacao',$newFieldsExists) || in_array('fieldNumFaturacao',$newFieldsExists) || in_array('fieldLoteFaturacao',$newFieldsExists) || in_array('fieldLocalidadeFaturacao',$newFieldsExists) || in_array('fieldCodPostalFaturacao',$newFieldsExists) || in_array('fieldDebitoDireto',$newFieldsExists) || in_array('fieldIBAN',$newFieldsExists)) : ?>

                                        <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGUA_DADOSPEDIDO'); ?></div></div>
                                        <div class="portlet-body">

                                            <?php if(in_array('fieldtipopedido',$newFieldsExists)) :?>
                                                <div class="field fieldtipopedido">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_PEDIDO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldtipopedido']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldTipoConsumidor',$newFieldsExists)) :?>
                                                <div class="field fieldTipoConsumidor">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_TIPOCONSUMIDOR' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldTipoConsumidor']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>


                                            <?php if(in_array('fieldMoradaPedido',$newFieldsExists) || in_array('fieldNumPedido',$newFieldsExists) || in_array('fieldLotePedido',$newFieldsExists) || in_array('fieldLocalidadePedido',$newFieldsExists) || in_array('fieldCodPostalPedido',$newFieldsExists)) :?>
                                                <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_AGUA_LOCALINSTALACAO');?></p>
                                            <?php endif; ?>


                                            <?php if(in_array('fieldMoradaPedido',$newFieldsExists)) :?>
                                                <div class="field fieldMoradaPedido">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_MORADAINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldMoradaPedido']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldNumPedido',$newFieldsExists)) :?>
                                                <div class="field fieldNumPedido">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NUMINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldNumPedido']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldLotePedido',$newFieldsExists)) :?>
                                                <div class="field fieldLotePedido">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOTEINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldLotePedido']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldLocalidadePedido',$newFieldsExists)) :?>
                                                <div class="field fieldLocalidadePedido">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOCALIDADEINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldLocalidadePedido']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldCodPostalPedido',$newFieldsExists)) :?>
                                                <div class="field fieldCodPostalPedido">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_CODPOSTALINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldCodPostalPedido']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>


                                            <?php if(in_array('fieldMoradaFaturacao',$newFieldsExists) || in_array('fieldNumFaturacao',$newFieldsExists) || in_array('fieldLoteFaturacao',$newFieldsExists) || in_array('fieldLocalidadeFaturacao',$newFieldsExists) || in_array('fieldCodPostalFaturacao',$newFieldsExists)) :?>
                                                <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_AGUA_MORADAFATURACAO');?></p>
                                            <?php endif; ?>


                                            <?php if(in_array('fieldMoradaFaturacao',$newFieldsExists)) :?>
                                                <div class="field fieldMoradaFaturacao">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_MORADAINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldMoradaFaturacao']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldNumFaturacao',$newFieldsExists)) :?>
                                                <div class="field fieldNumFaturacao">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_NUMINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldNumFaturacao']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldLoteFaturacao',$newFieldsExists)) :?>
                                                <div class="field fieldLoteFaturacao">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOTEINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldLoteFaturacao']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldLocalidadeFaturacao',$newFieldsExists)) :?>
                                                <div class="field fieldLocalidadeFaturacao">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_LOCALIDADEINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldLocalidadeFaturacao']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldCodPostalFaturacao',$newFieldsExists)) :?>
                                                <div class="field fieldCodPostalFaturacao">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_CODPOSTALINSTALACAO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldCodPostalFaturacao']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldDebitoDireto',$newFieldsExists)) :?>
                                                <div class="field fieldDebitoDireto">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_ADERIRDEBITODIRETO' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldDebitoDireto']->val_varchar, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if(in_array('fieldIBAN',$newFieldsExists) && $this->data['fieldDebitoDireto']->val_varchar == 'Sim') :?>
                                                <div class="field fieldIBAN">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGUA_IBAN' ); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data['fieldIBAN']->val_number, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                        </div>

                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                    <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGUA_DOCUMENTOSSUBMETIDOS'); ?></div></div>
                                    <div class="portlet-body">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green vdBotaoEditarUser" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=editcontratoprestservicos4user&pedido_id=' . $this->escape($getInputPedido_Id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agua&layout=list4user'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('pedido_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($getInputPedido_Id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('pedido_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('pedido_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>


<?php
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agua/tmpl/viewcontratoprestservicos4user.js.php');
    echo ('</script>');
?>