<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSitealertarptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertarpt.php');
JLoader::register('VirtualDeskSitealertarptFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertarpt_files.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alertarpt');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alertarpt', 'view4managers'); // verifica permissão acesso ao layout para editar
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-gallery.css');


// alertarpt - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/alertarpt/tmpl/alertarpt-comum.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/alertarpt/tmpl/view4manager.css');

// Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');

$getInputalertarpt_Id = JFactory::getApplication()->input->getInt('alertarpt_id');

// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSitealertarptHelper::getalertarptView4ManagerDetail($getInputalertarpt_Id);

// Carrega de ficheiros associados
if(!empty($this->data->referencia)) {
    $objAlertFiles = new VirtualDeskSitealertarptFilesHelper();
    $ListFilesAlert = $objAlertFiles->getFileGuestLinkByRefId ($this->data->referencia);
}

$alertarptEstadoList2Change = VirtualDeskSitealertarptHelper::getalertarptEstadoFaleConnoscoAllOptions($language_tag);

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

// Dados vazios...
if(empty($this->data)) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
    return false;
}

?>

<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: 50% !important;  -moz-border-radius: 50% !important;  border-radius: 50% !important;   width: 150px; height: auto;}
    .portfolio-content.portfolio-1 .cbp-caption-activeWrap {background-color: rgba(50, 197, 210, 0.5); }
    .portlet.light.bordered>.portlet-title { border-bottom: 1px solid #dcdcdc; }
    .mt-timeline-2>.mt-container>.mt-item>.mt-timeline-content>.mt-content-container { border: 1px solid #d3d7e9; }
    .text-wrap {white-space:normal;}
    .static-info{margin-bottom: 20px;}
    .static-info .name {line-height: 2;}

    .static-info .value {background-color: #f9f9f9; padding: 5px; margin:5px;font-weight: normal; border: 1px solid #e7ecf1;}

    .fileuploader-items .fileuploader-item .fileuploader-item-image img { left: 0;  top: 0; -webkit-transform: none; transform: none; position: relative;
        max-width: 100%;
        max-height: 100%;
    }


</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">

        <div class="caption">
            <i class="icon-calendar  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_FALECONNOSCO' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alertarpt'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->
    </div>

    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">

            <div class="form-body">

                <div class="portlet light">
                    <div class="portlet-body">
                        <div class="col-md-8 main">
                            <div class="portlet light bg-inverse bordered" style="min-height: 395px;">
                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTARPT_DADOSDETALHE'); ?></div></div>
                                <h3 class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_LISTA_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->referencia, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="row static-info">
                                                <div class=" name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_ESTADO_LABEL' ); ?></div>
                                                <div class=" value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alertarpt.getNewEstadoName4ManagerByAjax&alertarpt_id='. $this->escape($this->data->alertarpt_id)); ?>" >
                                                    <span class="label-<?php echo VirtualDeskSitealertarptHelper::getalertarptEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estadoName, ENT_QUOTES, 'UTF-8');?> </span>
                                                </div>

                                            </div>
                                        </div>


                                        <div class="col-md-3" style="margin-top: -5px;">
                                            <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                        </div>

                                        <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                    </div>
                                                    <div class="modal-body blocoAlterar2NewEstado">

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group" style="padding:0">
                                                                        <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                        <div style="padding:0;">
                                                                            <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                <?php foreach($alertarptEstadoList2Change as $rowEstado) : ?>
                                                                                    <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group">
                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                        <textarea rows="5" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" ></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group blocoIconsMsgAviso">
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span> <i class="fa"></i> </span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span class="blocoIconsMsgAviso_Texto"></span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                        <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alertarpt.sendAlterar2NewEstado4ManagerByAjax&alertarpt_id=' . $this->escape($this->data->alertarpt_id)); ?>" >
                                                        <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                        </button>
                                                        <span> <i class="fa"></i> </span>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    </div>


                                    <?php
                                    /*Ticket e alteraçao de modulos*/
                                    if(($this->data->id_tipologia == 1 && $this->data->id_assunto == 1) || ($this->data->id_tipologia == 1 && $this->data->id_assunto == 2)) {
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_NOME_PROJETO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->nome_projeto, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_URL_PROJETO' ); ?></div>
                                                    <div class="value"> <a href="<?php echo $this->data->url_projeto;?>" target="_blank"><?php echo htmlentities( $this->data->url_projeto, ENT_QUOTES, 'UTF-8');?></a></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_ESPECIFICACAO_PROJETO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->especificacao_assunto, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    /*Orçamento*/
                                    if(($this->data->id_tipologia == 1 && $this->data->id_assunto == 4) || ($this->data->id_tipologia == 2 && $this->data->id_assunto == 8)) {
                                        if(empty($this->data->categorias)){
                                            $categorias= $this->data->outras_categorias;
                                        } else {
                                            $pieces = explode(",", $this->data->categorias);
                                            for($i=0; $i<count($pieces); $i++){
                                                if($i==0){
                                                    $categoriasAux = VirtualDeskSitealertarptHelper::getCatName($pieces[$i]);
                                                } else {
                                                    $categoriasAux .= ', ' . VirtualDeskSitealertarptHelper::getCatName($pieces[$i]);
                                                }
                                            }

                                            if(!empty($this->data->outras_categorias)){
                                                $outrasCat = str_replace("n*", "", $this->data->outras_categorias);
                                                $outrasCat2 = str_replace("*", "", $outrasCat);
                                                $categorias = $categoriasAux . $outrasCat2;
                                            } else {
                                                $categorias = $categoriasAux;
                                            }
                                        }


                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_CATEGORIAS' ); ?></div>
                                                    <div class="value"><?php echo htmlentities( $categorias, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_HABITANTES' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->populacao, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_NUMTECNICOS' ); ?></div>
                                                    <?php $numTecnicos = VirtualDeskSitealertarptHelper::getTecnicosName($this->data->num_tecnicos); ?>
                                                    <div class="value"> <?php echo htmlentities( $numTecnicos, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_DESIGNACAO' ); ?></div>
                                                    <?php $designacao = VirtualDeskSitealertarptHelper::getDesignacaoName($this->data->designacao); ?>
                                                    <div class="value"><?php echo htmlentities( $designacao, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <?php
                                            if($this->data->designacao != 1) {
                                                ?>
                                                <div class="col-md-6">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_MARCA_MUNICIPAL' ); ?></div>
                                                        <?php
                                                        if($this->data->marca_municipal == 99) {
                                                            ?>
                                                            <div class="value"><?php echo htmlentities( $this->data->outra_marca_municipal, ENT_QUOTES, 'UTF-8');?></div>
                                                            <?php
                                                        } else {
                                                            $marca = VirtualDeskSitealertarptHelper::getMarcaName($this->data->marca_municipal);
                                                            ?>
                                                            <div class="value"><?php echo htmlentities( $marca, ENT_QUOTES, 'UTF-8');?></div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_OBSGERAIS' ); ?></div>
                                                    <div class="value"><?php echo htmlentities( $this->data->obs_Gerais, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    }

                                    /*Parceria*/
                                    if(($this->data->id_tipologia == 1 && $this->data->id_assunto == 5) || ($this->data->id_tipologia == 2 && $this->data->id_assunto == 10) || ($this->data->id_tipologia == 3 && $this->data->id_assunto == 16)){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_PARCERIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->esp_parceria, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    /*Reunião*/
                                    if(($this->data->id_tipologia == 1 && $this->data->id_assunto == 6) || ($this->data->id_tipologia == 2 && $this->data->id_assunto == 12)){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_ASSUNTOREUNIAO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($this->data->assunto_reuniao, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_NOMEREUNIAO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->nome_reuniao, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_DATAREUNIAO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($this->data->data_reuniao, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_HORAREUNIAO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->hora_reuniao, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    /*Outro Assunto*/
                                    if(($this->data->id_tipologia == 1 && $this->data->id_assunto == 7) || ($this->data->id_tipologia == 2 && $this->data->id_assunto == 13)){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_ASSUNTO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->outro_assunto, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    /*Vaga Trabalho*/
                                    if($this->data->id_tipologia == 3 && $this->data->id_assunto == 15){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_CANDIDATO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($this->data->nomeCandidato, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_REFERENCIAOFERTA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($this->data->ref_vaga_trabalho, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_MAILCANDIDATO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->emailCandidato, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_APRESENTACAOCANDIDATO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($this->data->apresentacao_vaga_trabalho, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    }

                                    /*Enviar CV*/
                                    if($this->data->id_tipologia == 3 && $this->data->id_assunto == 14){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_CANDIDATO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($this->data->nomeCV, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_MAILCANDIDATO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->emailCV, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_APRESENTACAOCANDIDATO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($this->data->apresentacao_cv, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    /*Brainstorming / Conferências / Workshops*/
                                    if(($this->data->id_tipologia == 4 && $this->data->id_assunto == 17) || ($this->data->id_tipologia == 4 && $this->data->id_assunto == 18) || ($this->data->id_tipologia  == 4 && $this->data->id_assunto == 19)){
                                        $assName = VirtualDeskSitealertarptHelper::getAssName($this->data->id_assunto);
                                        ?>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_ASSUNTO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($assName, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_BLOCKPARCERIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->esp_bloco_parceria, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }


                                    /*Elogios e Sugestões*/
                                    if($this->data->id_tipologia == 5){
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_ELOGIOS' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->elogios_sugestoes, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    if($this->data->id_tipologia == 1 || $this->data->id_tipologia == 2 || $this->data->id_tipologia == 4 || $this->data->id_tipologia == 5  || ($this->data->id_tipologia == 3 && $this->data->id_assunto == 16)) {
                                        $concName = VirtualDeskSitealertarptHelper::getConcelhoName($this->data->concelho);
                                        ?>

                                        <h3><?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_DADOS_PESSOAIS' ); ?></h3>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_CONCELHO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities($concName, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_NIF' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->nif, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_NOME' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->nome, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_EMAIL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->email, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTARPT_CARGO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->cargo, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }

                                    ?>



                            </div>
                        </div>

                        <div class="col-md-4 main">

                            <div class="portlet light bg-inverse bordered attach" style="min-height: 395px;">
                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_IMGDOCS'); ?></div></div>
                                <div class="portlet-body">

                                    <div class="form-group" id="uploadField">

                                        <div class="col-md-12">

                                            <div class="file-loading">
                                                <input type="file" name="fileupload" id="fileupload" >
                                            </div>
                                            <div id="errorBlock" class="help-block"></div>
                                        </div>
                                        <input type="hidden" name="vdFileUpChanged" value="0">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=alertarpt'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>
        </form>
    </div>

<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/alertarpt/tmpl/view4manager.js.php');
    echo ('</script>');
?>