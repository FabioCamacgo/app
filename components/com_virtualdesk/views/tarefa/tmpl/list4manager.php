<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteTarefaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tarefa.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('tarefa', 'list4managers');
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

$obVDCrypt = new VirtualDeskSiteCryptHelper();
$setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');

// TODO , fazer função de carregamento dos users e groups apenas para a tarefa ???
$NovaTarefaUserList    = VirtualDeskSiteTarefaHelper::getUserDropDownList4Manager ();
$NovaTarefaGrupoList   = VirtualDeskSiteTarefaHelper::getGroupDropDownList4Manager();
$TarefaEditarUserList  = $NovaTarefaUserList;
$TarefaEditarGrupoList = $NovaTarefaGrupoList;
$TarefasEstadoList     = VirtualDeskSiteTarefaHelper::getEstadoListAll ();




// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

// Tarefa - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/tarefa/tmpl/tarefa-comum.css');


?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput{display:block;}
    .fileinput-preview {display:block; max-height: 100%; }
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}
    .iconVDModified {padding-left: 15px; }

    .EstadoFilterDropBox {float: right; padding-right: 30px; }
    .EstadoFilterDropBoxFull {float: right; padding-right: 30px; }
    .EstadoFilterDropBoxMinhas {float: right; padding-right: 30px; }
    .EstadoFilterDropBoxGrupos {float: right; padding-right: 30px; }

</style>

<div class="portlet light bordered tarefa">


    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">

            <!-- Botões DataTables -->
            <div class="btn-group">
                <a class="btn green btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                    <i class="fa fa-share"></i>
                    <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right" id="tabela_lista_tarefa_tools">
                    <li>
                        <a href="javascript:;" data-action="0" class="tool-action">
                            <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="1" class="tool-action">
                            <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="2" class="tool-action">
                            <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="3" class="tool-action">
                            <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?></a>
                    </li>


                </ul>
            </div>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>

    </div>

    <div class="portlet-body ">

        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_ListaTarefaAll" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_TAREFA_TODASASTAREFAS'); ?></h4>
                        <span id="vdTabNavTarefaAllNumAbertas" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_TAREFA_PORRESOLVER'); ?>"
                              data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefasListaAbertas4ManagerByAjax');?>" >
                            <span class="badge badge-danger vdValorNot"></span>
                        </span>
                    </a>

                </li>
                <li class="">
                    <a href="#tab_ListaTarefaMinhas" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_TAREFA_ASMINHASTAREFAS'); ?></h4>
                        <span id="vdTabNavTarefaMinhasNumAbertas" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_TAREFA_PORRESOLVER'); ?>"
                              data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefasMinhasListaAbertas4ManagerByAjax');?>" >
                            <span class="badge badge-danger vdValorNot"></span>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#tab_ListaTarefaMeusGrupos" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_TAREFA_TAREFASDOSMEUSGRUPOS'); ?></h4>
                        <span id="vdTabNavTarefaMeusGruposNumAbertas" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_TAREFA_PORRESOLVER'); ?>"
                              data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefasMeusGruposListaAbertas4ManagerByAjax');?>" >
                        <span class="badge badge-danger vdValorNot"></span>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_ListaTarefaAll">
                    <table class="table table-striped table-bordered table-hover order-column" id="tabela_lista_tarefa">
                        <thead>
                        <tr>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAREFA_TITULO'); ?></th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER'); ?></th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO'); ?></th>
                            <th ></th>
                            <th ></th>
                        </tr>
                        </thead>
                    </table>
                </div>

                <div class="tab-pane" id="tab_ListaTarefaMinhas">
                    <table class="table table-striped table-bordered table-hover order-column" id="tabela_lista_tarefa_minhas">
                        <thead>
                        <tr>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAREFA_TITULO'); ?></th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER'); ?></th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO'); ?></th>
                            <th ></th>
                            <th ></th>
                        </tr>
                        </thead>
                    </table>
                </div>

                <div class="tab-pane" id="tab_ListaTarefaMeusGrupos">
                    <table class="table table-striped table-bordered table-hover order-column" id="tabela_lista_tarefa_meusgrupos">
                        <thead>
                        <tr>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAREFA_TITULO'); ?></th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER'); ?></th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO'); ?></th>
                            <th ></th>
                            <th ></th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>

        </div>

    </div>



    <div class="modal fade" id="TarefaAlterarEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_ALTERARESTADO_TITLE' ); ?></h4>
                </div>
                <div class="modal-body blocoTarefaAlterarEstado"
                     data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefaAlterarEstado4ManagerByAjax'); ?>" >

                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-12 static-info">
                                <div class="form-group">
                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_TAREFA_TITULO' ).$labelseparator; ?></label>
                                    <span class="value ValorEstadoAtualStatic TarefaAlterarEstadoNome"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 static-info">
                                <div class="form-group">
                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_DESCRICAO' ).$labelseparator; ?></label>
                                    <span class="value ValorEstadoAtualStatic TarefaAlterarEstadoDesc"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group" style="padding:0">
                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                    <div style="padding:0;">
                                        <select name="TarefaAlterarNewEstadoId" class="bs-select form-control TarefaAlterarNewEstadoId" data-show-subtext="true">
                                            <?php foreach($TarefasEstadoList as $rowTrfEstado) : ?>
                                                <option value="<?php echo $rowTrfEstado['id']; ?>" ><?php echo $rowTrfEstado['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_OBS_OPCIONAL' ); ?></label>
                                    <textarea type="text" class="form-control TarefaAlterarObs" name="TarefaAlterarObs"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group blocoIconsMsgAviso">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span> <i class="fa"></i> </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="blocoIconsMsgAviso_Texto"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                    <button class="btn green tarefaalterarestadoSend" type="button" name"Alterar"
                    data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.sendTarefaAlterarEstado4ManagerByAjax'); ?>" >
                    <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                    </button>
                    <span> <i class="fa"></i> </span>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="TarefaEditarModal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_EDITAR_TITLE' ); ?></h4>
                </div>
                <div class="modal-body blocoTarefaEditar"
                     data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefaDetalhe4ManagerByAjax'); ?>" >

                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_TAREFA_TITULO' ); ?></label>
                                    <input type="text" class="form-control TarefaEditarNome" name="TarefaEditarNome" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                    <textarea class="form-control TarefaEditarDesc" name="TarefaEditarDesc"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group" style="padding:0">
                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_ESTADO' ).$labelseparator; ?></label>
                                    <div style="padding:0;">
                                        <select name="TarefaEditarEstadoId" class="bs-select form-control TarefaEditarEstadoId" data-show-subtext="true">
                                            <?php foreach($TarefasEstadoList as $rowTrfEstado) : ?>
                                                <option value="<?php echo $rowTrfEstado['id']; ?>" ><?php echo $rowTrfEstado['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group" style="">
                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA_4USER' ).$labelseparator; ?></label>
                                    <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="TarefaEditarUserId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>
                                        <select name="TarefaEditarUserId[]"  id="TarefaEditarUserId" class="form-control input-lg select2-multiple TarefaEditarUserId" multiple>
                                            <?php foreach($TarefaEditarUserList as $rowTrfUser) : ?>
                                                <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowTrfUser['id'],$setencrypt_forminputhidden); ?>"
                                                ><?php echo $rowTrfUser['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group" style="padding:0">
                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA_4GROUP' ).$labelseparator; ?></label>
                                    <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="TarefaEditarGroupId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>
                                        <select name="TarefaEditarGroupId[]"  id="TarefaEditarGroupId" class="form-control input-lg select2-multiple TarefaEditarGroupId" multiple>
                                            <?php foreach($TarefaEditarGrupoList as $rowTrfGrupo) : ?>
                                                <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowTrfGrupo['id'],$setencrypt_forminputhidden); ?>"
                                                ><?php echo $rowTrfGrupo['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="form-group blocoIconsMsgAviso">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span> <i class="fa"></i> </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="blocoIconsMsgAviso_Texto"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button class="btn green tarefaeditarSend" type="button" name"Alterar"
                    data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.sendTarefaEditar4ManagerByAjax'); ?>" >
                    <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                    </button>
                    <span> <i class="fa"></i> </span>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade bs-modal-lg" id="TarefaHistoricoModal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_HIST_TITLE' ); ?></h4>
                </div>
                <div class="modal-body blocoTarefaHistorico"
                     data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefaHistorico4ManagerByAjax'); ?>" >

                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-12 static-info">
                                <div class="form-group">
                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_TAREFA_TITULO' ).$labelseparator; ?></label>
                                    <span class="value ValorEstadoAtualStatic TarefaHistoricoNome"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 static-info">
                                <div class="form-group">
                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_DESCRICAO' ).$labelseparator; ?></label>
                                    <span class="value ValorEstadoAtualStatic TarefaHistoricoDesc"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <table class="table table-striped table-bordered table-hover order-column" id="tabela_tarefa_historico_modal" style="width:100%" >
                                <thead>
                                <tr>
                                    <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>


                    </div>

                    <div class="form-group blocoIconsMsgAviso">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span> <i class="fa"></i> </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="blocoIconsMsgAviso_Texto"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                    <span> <i class="fa"></i> </span>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



</div>





<?php

echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/tarefa/tmpl/list4manager.js.php');
echo ('</script>');

?>


