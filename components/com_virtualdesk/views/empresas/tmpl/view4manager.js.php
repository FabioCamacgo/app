<?php
    defined('_JEXEC') or die;

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    $obParam      = new VirtualDeskSiteParamsHelper();
    $pinMapa = $obParam->getParamsByTag('pinMapa');
    $pathDelimitacaoMapa = $obParam->getParamsByTag('pathDelimitacaoMapa');
?>

var iconPath = '<?php echo JUri::base() . $pinMapa; ?>';
var LatToSet = <?php echo $this->data->latitude;?>;
var LongToSet = <?php echo $this->data->longitude;?>;

var PortofolioHandle = function () {

    var initEmpresasFileGridFoto = function (evt) {
        // init cubeportfolio
        jQuery('#vdEmpresasFileGridFoto').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyEmpresasFileGridFoto = function (evt) {
        // init cubeportfolio
        jQuery('#vdEmpresasFileGridFoto').cubeportfolio('destroy');
    };

    var initEmpresasFileGridLogo = function (evt) {
        // init cubeportfolio
        jQuery('#vdEmpresasFileGridLogo').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyEmpresasFileGridLogo = function (evt) {
        // init cubeportfolio
        jQuery('#vdEmpresasFileGridLogo').cubeportfolio('destroy');
    };



    return {
        //main function to initiate the module
        init: function () {
            initEmpresasFileGridFoto();
            initEmpresasFileGridLogo();
        },
        initEmpresasFileGridFoto    :  initEmpresasFileGridFoto,
        destroyEmpresasFileGridFoto  : destroyEmpresasFileGridFoto,
        initEmpresasFileGridLogo    :  initEmpresasFileGridLogo,
        destroyEmpresasFileGridLogo  : destroyEmpresasFileGridLogo,

    };
}();

var MapsGoogle = function () {
    var mapMarker = function () {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });
        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });
        map.setZoom(13);


        <?php require_once (JPATH_SITE . $pathDelimitacaoMapa); ?>

    }
    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };
}();



jQuery(document).ready(function() {

    PortofolioHandle.init();

    MapsGoogle.init();


});