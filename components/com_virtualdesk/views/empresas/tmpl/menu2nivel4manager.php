<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteEmpresasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('empresas', 'list4managers');
    $vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>




<div class="portlet light bordered">


    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>

    </div>

    <div class="portlet-body ">

        <div class="row">
            <div class="col-lg-4">
                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                    <div class="kt-portlet__body">
                        <div class="kt-callout__body">
                            <div class="kt-callout__content">
                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_REGISTAR');?></h3>
                                <p class="kt-callout__desc">
                                    <?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_REGISTAR');?>
                                </p>
                            </div>
                            <div class="kt-callout__action">
                                <a href="index.php?option=com_virtualdesk&view=empresas&layout=addnew4manager&vdcleanstate=1" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-info"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_ACEDER');?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                    <div class="kt-portlet__body">
                        <div class="kt-callout__body">
                            <div class="kt-callout__content">
                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_LISTA');?></h3>
                                <p class="kt-callout__desc">
                                    <?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_LISTA');?>
                                </p>
                            </div>
                            <div class="kt-callout__action">
                                <a href="index.php?option=com_virtualdesk&view=empresas&layout=list4manager&vdcleanstate=1" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-info"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_ACEDER');?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                    <div class="kt-portlet__body">
                        <div class="kt-callout__body">
                            <div class="kt-callout__content">
                                <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_INFO');?></h3>
                                <p class="kt-callout__desc">
                                    <?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_INFO');?>
                                </p>
                            </div>
                            <div class="kt-callout__action">
                                <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-info"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_ACEDER');?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>



<?php

echo $localScripts;

?>


