<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteEmpresasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas.php');
    JLoader::register('VirtualDeskSiteEmpresasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_files.php');
    JLoader::register('VirtualDeskSiteEmpresasLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_logos_files.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('empresas');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('empresas', 'edit4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/empresas/tmpl/empresas.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator    = ' : ';
    $params            = JComponentHelper::getParams('com_virtualdesk');
    $getInputEmpresas_Id = JFactory::getApplication()->input->getInt('empresa_id');


    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteEmpresasHelper::getEmpresasDetail4Manager($getInputEmpresas_Id);
    if( empty($this->data) ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
        return false;
    }

    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteEmpresasHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit4manager.empresas.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }


    //se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;

    $obParam    = new VirtualDeskSiteParamsHelper();
    $concelhoEmpresas = $obParam->getParamsByTag('concelhoEmpresasGOV');
    $catAlojamento = $obParam->getParamsByTag('catAlojamento');
    $catAlojamentoLocal = $obParam->getParamsByTag('catAlojamentoLocal');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_EMPRESASGOV_EDIT' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-empresas" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=empresas.update4manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                <div class="form-body">

                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NOVO_INFO'); ?></h3></legend>


                        <div class="form-group half">
                            <div class="row static-info ">
                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_REFERENCIA'); ?></div>
                                <div class="value"> <?php echo htmlentities( $this->data->referencia, ENT_QUOTES, 'UTF-8');?> </div>
                            </div>
                        </div>


                        <div class="form-group half right estado">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_ESTADO'); ?><span class="required">*</span></label>
                            <?php $Estados = VirtualDeskSiteEmpresasHelper::getEstado()?>
                            <div class="value col-md-12">
                                <select name="estado" value="<?php echo $this->data->estado; ?>" id="estado" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->freguesia)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Estados as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['estado']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->estado; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getEstadoName($this->data->estado) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeEstado = VirtualDeskSiteEmpresasHelper::excludeEstado($this->data->estado)?>
                                        <?php foreach($ExcludeEstado as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['estado']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NOMEEMPRESA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" required class="form-control" autocomplete="off" name="nomeEmpresa" id="nomeEmpresa" maxlength="150" value="<?php echo $this->data->nome;?>"/>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_DESIGNACAOCOMERCIAL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" required name="designacaoComercial" id="designacaoComercial" maxlength="150" value="<?php echo $this->data->designacao; ?>"/>
                            </div>
                        </div>



                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_APRESENTACAO'); ?></label>
                            <div class="value col-md-12">
                                <textarea  class="form-control" rows="15" name="apresentacao" id="apresentacao" maxlength="2000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->apresentacao); ?></textarea>
                            </div>
                        </div>



                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_CATEGORIA'); ?><span class="required">*</span></label>
                            <?php $Categorias = VirtualDeskSiteEmpresasHelper::getCategorias()?>
                            <div class="value col-md-12">
                                <select name="categoria" value="<?php echo $this->data->categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->categoria)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Categorias as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->categoria; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getCatName($this->data->categoria) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeCategoria = VirtualDeskSiteEmpresasHelper::excludeCategoria($this->data->categoria)?>
                                        <?php foreach($ExcludeCategoria as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <!--   SubCategoria   -->
                        <?php
                        // Carrega menusec se o id de menumain estiver definido.
                        $Subcategorias = array();
                        if(!empty($this->data->categoria)) {
                            if( (int) $this->data->categoria > 0) $Subcategorias = VirtualDeskSiteEmpresasHelper::getSubCategoria($this->data->categoria);
                        }

                        ?>
                        <div id="blocoSubcategoria" class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_EMPRESASGOV_SUBCATEGORIA' ); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <select name="subcategoria" id="subcategoria" value="<?php echo $this->data->subcategoria;?>"
                                    <?php
                                    if(empty($this->data->categoria)) {
                                        echo 'disabled';
                                    }
                                    ?>
                                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->subcategoria)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Subcategorias as $rowMM) : ?>
                                            <option value="<?php echo $rowMM['id']; ?>"
                                            ><?php echo $rowMM['name']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->subcategoria; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getSubCatName($this->data->subcategoria);?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeSubCat = VirtualDeskSiteEmpresasHelper::excludeSubCategoria($this->data->categoria, $this->data->subcategoria);?>
                                        <?php foreach($ExcludeSubCat as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['subcategoria']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>

                                </select>

                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_MORADA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="morada" id="morada" maxlength="200" value="<?php echo $this->data->morada; ?>"/>
                            </div>
                        </div>



                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_MAPA'); ?></label>
                            <div class="value col-md-12">
                                <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                                       value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                            </div>
                        </div>

                        <?php
                            $codPost = explode('-', $this->data->codigo_postal);
                            $codPost2 = explode(' ', $codPost[1]);
                            $codPostal4 = $codPost[0];
                            $codPostal3 = $codPost2[0];
                            $codPostalText = $codPost2[1];
                        ?>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_CODIGOPOSTAL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="codPostal4" id="codPostal4" maxlength="4" value="<?php echo $codPostal4; ?>"/>
                                <?php echo '-'; ?>
                                <input type="text" class="form-control" autocomplete="off" name="codPostal3" id="codPostal3" maxlength="3" value="<?php echo $codPostal3; ?>"/>
                                <input type="text" class="form-control" autocomplete="off" name="codPostalText" id="codPostalText" maxlength="90" value="<?php echo $codPostalText; ?>"/>
                            </div>
                        </div>



                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_FREGUESIA'); ?><span class="required">*</span></label>
                            <?php $Freguesias = VirtualDeskSiteEmpresasHelper::getFreguesia($concelhoEmpresas)?>
                            <div class="value col-md-12">
                                <select name="freguesia" value="<?php echo $this->data->freguesia; ?>" id="freguesia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->freguesia)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Freguesias as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['freguesia']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->freguesia; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getFregName($this->data->freguesia) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeFreguesia = VirtualDeskSiteEmpresasHelper::excludeFreguesia($concelhoEmpresas, $this->data->freguesia)?>
                                        <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['freguesia']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_FISCALID'); ?><span class="required">*</span></label>

                            <?php
                                if($this->data->nif == 0){
                                    $nif = '';
                                } else {
                                    $nif = $this->data->nif;
                                }
                            ?>

                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $nif; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_TELEFONE'); ?><span class="required">*</span></label>

                            <?php
                                if($this->data->telefone == 0){
                                    $telefone = '';
                                } else {
                                    $telefone = $this->data->telefone;
                                }
                            ?>

                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_EMAIL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="email" id="email" maxlength="150" value="<?php echo htmlentities($this->data->email, ENT_QUOTES, 'UTF-8'); ?>" />
                            </div>
                        </div>



                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_WEBSITE'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="website" id="website" maxlength="200" value="<?php echo $this->data->website; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_FACEBOOK'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="facebook" id="facebook" maxlength="200" value="<?php echo $this->data->facebook; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_INSTAGRAM'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="instagram" id="instagram" maxlength="200" value="<?php echo $this->data->instagram; ?>"/>
                            </div>
                        </div>


                        <div id="Alojamento" style="display:none;">

                            <legend><h4><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_DADOSALOJAMENTO'); ?></h4></legend>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NREGISTO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="nregisto" id="nregisto" maxlength="20" value="<?php echo $this->data->nRegisto_Alojamento; ?>"/>
                                </div>
                            </div>



                            <div class="form-group half right" style="display:none;" id="capacidadeAlojamento">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_CAPACIDADE'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="capacidade" id="capacidade" maxlength="4" value="<?php echo $this->data->capacidade_Alojamento; ?>"/>
                                </div>
                            </div>



                            <div class="form-group half" style="display:none;" id="unidadesAlojamento">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_UNIDADES'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="unidades" id="unidades" maxlength="4" value="<?php echo $this->data->unidades_Alojamento; ?>"/>
                                </div>
                            </div>



                            <div class="form-group half right" style="display:none;" id="tipologiaAlojamento">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_TIPOLOGIA'); ?><span class="required">*</span></label>
                                <?php $Tipologia = VirtualDeskSiteEmpresasHelper::getTipologia()?>
                                <div class="value col-md-12">
                                    <select name="tipologia" value="<?php echo $this->data->tipologia_Alojamento; ?>" id="tipologia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($this->data->tipologia_Alojamento)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($Tipologia as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['tipologia']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $this->data->tipologia_Alojamento; ?>"><?php echo VirtualDeskSiteEmpresasHelper::getTipologiaName($this->data->tipologia_Alojamento) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeTipologia = VirtualDeskSiteEmpresasHelper::excludeTipologia($this->data->tipologia_Alojamento)?>
                                            <?php foreach($ExcludeTipologia as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['tipologia']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group half" style="display:none;" id="numeroQuartos">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NQUARTOS'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="nQuartos" id="nQuartos" maxlength="5" value="<?php echo $this->data->nQuartos_Alojamento; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half right" style="display:none;" id="numeroUtentes">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NUTENTES'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="nUtentes" id="nUtentes" maxlength="5" value="<?php echo $this->data->nUtentes_Alojamento; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half" style="display:none;" id="numeroCamas">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NCAMAS'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" autocomplete="off" name="nCamas" id="nCamas" maxlength="5" value="<?php echo $this->data->nCamas_Alojamento; ?>"/>
                                </div>
                            </div>

                        </div>

                        <legend><h4><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_MULTIMEDIA'); ?></h4></legend>

                        <div class="form-group half" id="uploadFieldCapa">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_UPLOAD'); ?></label>
                            <div class="value col-md-12">

                                <div class="file-loading">
                                    <input type="file" name="fileuploadCapa[]" id="fileuploadCapa" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>

                            </div>
                        </div>


                        <div class="form-group half right" id="uploadFieldLogo">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_UPLOAD_LOGO'); ?></label>
                            <div class="value col-md-12">

                                <div class="file-loading">
                                    <input type="file" name="fileuploadLogo[]" id="fileuploadLogo" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                        </div>


                    </div>

                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESAGOV_DADOSRESPONSAVEL'); ?></h3></legend>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAGOV_NOMERESPONSAVEL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="nomeResponsavel" id="nomeResponsavel" maxlength="150" value="<?php echo $this->data->nome_responsavel; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESAGOV_CARGORESPONSAVEL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="cargoResponsavel" id="cargoResponsavel" maxlength="150" value="<?php echo $this->data->cargo_responsavel; ?>"/>
                            </div>
                        </div>

                        <?php
                            if($this->data->telefone_responsavel == 0){
                                $telefone_responsavel = '';
                            } else {
                                $telefone_responsavel = $this->data->telefone_responsavel;
                            }
                        ?>

                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_TELEFONERESPONSAVEL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="telefoneResponsavel" id="telefoneResponsavel" maxlength="9" value="<?php echo $telefone_responsavel; ?>"/>
                            </div>
                        </div>



                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_EMAILRESPONSAVEL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="emailResponsavel" id="emailResponsavel" maxlength="150" value="<?php echo htmlentities($this->data->mail_responsavel, ENT_QUOTES, 'UTF-8'); ?>" />
                            </div>
                        </div>


                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=list4manager&vdcleanstate=1'); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('empresa_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('empresas.update4manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>

                <?php echo JHtml::_('form.token'); ?>

            </form>

        </div>


    </div>

<?php
echo $localScripts;
echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/empresas/tmpl/edit4manager.js.php');
echo ('</script>');
?>