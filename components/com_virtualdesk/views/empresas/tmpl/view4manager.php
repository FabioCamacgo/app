<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteEmpresasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas.php');
    JLoader::register('VirtualDeskSiteEmpresasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_files.php');
    JLoader::register('VirtualDeskSiteEmpresasLogosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_empresas_logos_files.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('empresas');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('empresas', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
}


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/empresas/tmpl/empresas.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputEmpresa_Id = JFactory::getApplication()->input->getInt('empresa_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteEmpresasHelper::getEmpresasDetail4Manager($getInputEmpresa_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->referencia)) {
        $ListFilesAlert = array();

        $objFotoFiles = new VirtualDeskSiteEmpresasFilesHelper();
        $ListFilesFoto = $objFotoFiles->getFileGuestLinkByRefId ($this->data->referencia);

        $objLogoFiles = new VirtualDeskSiteEmpresasLogosFilesHelper();
        $ListFilesLogo = $objLogoFiles->getFileGuestLinkByRefId ($this->data->referencia);

    }

    $EmpresasEstadoList2Change = VirtualDeskSiteEmpresasHelper::getEstadoAllOptions($language_tag);

    // Todo Ter parametro com o Id de Menu Principal de cada módulo
    //$itemmenuid_lista = $params->get('agenda_menuitemid_list');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
        return false;
    }

    $obParam      = new VirtualDeskSiteParamsHelper();
    $catAlojamento = $obParam->getParamsByTag('catAlojamento');
    $catAlojamentoLocal = $obParam->getParamsByTag('catAlojamentoLocal');
    $latitudeDefaultEmpresas = $obParam->getParamsByTag('latitudeDefaultEmpresas');
    $LongitudeDefaultEmpresas = $obParam->getParamsByTag('LongitudeDefaultEmpresas');


    if(empty($this->data->latitude)){
        $this->data->latitude = $latitudeDefaultEmpresas;
        $this->data->longitude = $LongitudeDefaultEmpresas;
    }

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_EMPRESASGOV_VIEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=edit4manager&empresa_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_EMPRESASGOV_NOVO' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">

                <div class="form-body">

                    <div class="portlet light">

                        <div class="col-md-8">
                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESAS_NOVO_INFO'); ?></h3></legend>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_REFERENCIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->referencia, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half state">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_ESTADO'); ?></div>
                                                <div class="value">
                                                    <span class="label <?php echo VirtualDeskSiteEmpresasHelper::getEstadoCSS($this->data->estado); ?>"><?php echo htmlentities( $this->data->estadoName, ENT_QUOTES, 'UTF-8');?> </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NOMEEMPRESA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nome, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_DESIGNACAOCOMERCIAL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->designacao, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_APRESENTACAO'); ?></div>
                                                <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->apresentacao);?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_CATEGORIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->catName, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_SUBCATEGORIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->subCatName, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_MORADA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->morada, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_CODIGOPOSTAL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->codigo_postal, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_FREGUESIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->fregName, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_FISCALID'); ?></div>
                                                <?php
                                                    if($this->data->nif == 0){
                                                        $nif = '';
                                                    } else {
                                                        $nif = $this->data->nif;
                                                    }
                                                ?>
                                                <div class="value"> <?php echo htmlentities($nif, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_TELEFONE'); ?></div>
                                                <?php
                                                    if($this->data->telefone == 0){
                                                        $telefone = '';
                                                    } else {
                                                        $telefone = $this->data->telefone;
                                                    }
                                                ?>
                                                <div class="value"> <?php echo htmlentities($telefone, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_EMAIL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->email, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_WEBSITE'); ?></div>
                                                <div class="value">
                                                    <?php
                                                        if(!empty($this->data->website)){
                                                            if (strpos($this->data->website, 'http://') !== false) {
                                                                echo '<a href="' . $this->data->website .'" target="_blank">' . htmlentities($this->data->website, ENT_QUOTES, 'UTF-8') . '</a>';
                                                            } else {
                                                                if (strpos($this->data->website, 'https://') !== false){
                                                                    echo '<a href="' . $this->data->website .'" target="_blank">' . htmlentities($this->data->website, ENT_QUOTES, 'UTF-8') . '</a>';
                                                                } else {
                                                                    echo '<a href="https://' . $this->data->website .'" target="_blank">' . htmlentities($this->data->website, ENT_QUOTES, 'UTF-8') . '</a>';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_FACEBOOK'); ?></div>
                                                <div class="value">
                                                    <?php
                                                        if(!empty($this->data->facebook)){
                                                            if (strpos($this->data->facebook, 'http://') !== false) {
                                                                echo '<a href="' . $this->data->facebook .'" target="_blank">' . htmlentities($this->data->facebook, ENT_QUOTES, 'UTF-8') . '</a>';
                                                            } else {
                                                                if (strpos($this->data->facebook, 'https://') !== false){
                                                                    echo '<a href="' . $this->data->facebook .'" target="_blank">' . htmlentities($this->data->facebook, ENT_QUOTES, 'UTF-8') . '</a>';
                                                                } else {
                                                                    echo '<a href="https://' . $this->data->facebook .'" target="_blank">' . htmlentities($this->data->facebook, ENT_QUOTES, 'UTF-8') . '</a>';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_INSTAGRAM'); ?></div>
                                                <div class="value">
                                                    <?php
                                                        if(!empty($this->data->instagram)){
                                                            if (strpos($this->data->instagram, 'http://') !== false) {
                                                                echo '<a href="' . $this->data->instagram .'" target="_blank">' . htmlentities($this->data->instagram, ENT_QUOTES, 'UTF-8') . '</a>';
                                                            } else {
                                                                if (strpos($this->data->instagram, 'https://') !== false){
                                                                    echo '<a href="' . $this->data->instagram .'" target="_blank">' . htmlentities($this->data->instagram, ENT_QUOTES, 'UTF-8') . '</a>';
                                                                } else {
                                                                    echo '<a href="https://' . $this->data->instagram .'" target="_blank">' . htmlentities($this->data->instagram, ENT_QUOTES, 'UTF-8') . '</a>';
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>


                                        <?php
                                            if($this->data->categoria == $catAlojamento){
                                                ?>

                                                <legend><h4><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_DADOSALOJAMENTO'); ?></h4></legend>

                                                <div class="form-group half">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NREGISTO'); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data->nRegisto_Alojamento, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>

                                                <?php
                                            }


                                            if($this->data->subcategoria == $catAlojamentoLocal && $this->data->categoria == $catAlojamento){
                                                ?>
                                                <div class="form-group half right">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_TIPOLOGIA'); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data->tipologiaName, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>

                                                <div class="form-group half">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NQUARTOS'); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data->nQuartos_Alojamento, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>

                                                <div class="form-group half right">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NUTENTES'); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data->nUtentes_Alojamento, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>

                                                <div class="form-group half">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_NCAMAS'); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data->nCamas_Alojamento, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }


                                            if($this->data->subcategoria != $catAlojamentoLocal && $this->data->categoria == $catAlojamento){
                                                ?>
                                                <div class="form-group half right">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_CAPACIDADE'); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data->capacidade_Alojamento, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>

                                                <div class="form-group half">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_UNIDADES'); ?></div>
                                                        <div class="value"> <?php echo htmlentities( $this->data->unidades_Alojamento, ENT_QUOTES, 'UTF-8');?> </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }

                                        ?>


                                    </div>


                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESAGOV_DADOSRESPONSAVEL'); ?></h3></legend>

                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESAGOV_NOMERESPONSAVEL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nome_responsavel, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESAGOV_CARGORESPONSAVEL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->cargo_responsavel, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_TELEFONERESPONSAVEL'); ?></div>
                                                <?php
                                                    if($this->data->telefone_responsavel == 0){
                                                        $telefoneResp = '';
                                                    } else {
                                                        $telefoneResp = $this->data->telefone_responsavel;
                                                    }
                                                ?>
                                                <div class="value"> <?php echo htmlentities($telefoneResp, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_EMAILRESPONSAVEL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->mail_responsavel, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">

                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_UPLOAD'); ?></h3></legend>

                                        <div id="vdEmpresasFileGridFoto" class="cbp">
                                            <?php
                                            if(!is_array($ListFilesFoto)) $ListFilesFoto = array();
                                            foreach ($ListFilesFoto as $rowFile) : ?>
                                                <div class="cbp-item identity logos">
                                                    <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                        <div class="cbp-caption-defaultWrap">
                                                            <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                        <div class="cbp-caption-activeWrap">
                                                            <div class="cbp-l-caption-alignLeft">
                                                                <div class="cbp-l-caption-body">
                                                                    <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                    <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_UPLOAD_LOGO'); ?></h3></legend>

                                        <div id="vdEmpresasFileGridLogo" class="cbp">
                                            <?php
                                            if(!is_array($ListFilesLogo)) $ListFilesLogo = array();
                                            foreach ($ListFilesLogo as $rowFile) : ?>
                                                <div class="cbp-item identity logos">
                                                    <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                        <div class="cbp-caption-defaultWrap">
                                                            <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                        <div class="cbp-caption-activeWrap">
                                                            <div class="cbp-l-caption-alignLeft">
                                                                <div class="cbp-l-caption-body">
                                                                    <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                    <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-body">
                                    <div class="bloco">
                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_EMPRESASGOV_MAPA'); ?></h3></legend>
                                        <div>
                                            <div id="gmap_marker" class="gmaps"></div>

                                            <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                                                   value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=edit4manager&empresa_id=' . $this->escape($this->data->id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=empresas&layout=list4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('empresa_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/empresas/tmpl/view4manager.js.php');
    echo ('</script>');
?>