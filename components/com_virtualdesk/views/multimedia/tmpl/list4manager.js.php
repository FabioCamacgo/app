<?php
defined('_JEXEC') or die;

$objEstadoOptions  = VirtualDeskSiteMultimediaHelper::getEstadoAllOptions($language_tag);
$objCategoriasOptions  = VirtualDeskSiteMultimediaHelper::getCategoria();

$EstadoOptionsHTML = '';
foreach($objEstadoOptions as $row) {
    $EstadoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}

$CategoriasOptionsHTML = '';
foreach($objCategoriasOptions as $row) {
    $CategoriasOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['categoria'] . '</option>';
}
?>

var TableDatatablesManaged = function () {

    var initTableMultimedia = function () {

        var table = jQuery('#tabela_lista_multimedia');
        var tableTools = jQuery('#tabela_lista_multimedia_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"lf <"EstadoFilterDropBox"><"CategoriasFilterDropBox"> rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [50, 100, 200, -1],
                [50, 100, 200, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 50,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[7] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        let currState = '';
                        if(data=='1') {
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledPlugin_' + meta.row + '"' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-pluginenable="' + row[12] + '" ';
                        ret += ' data-vd-url-checkpluginenable="' + row[13] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        return (ret);
                    }
                },
                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_MULTIMEDIA_ESTADONAOINICIADO"); ?>';

                        switch (row[10])
                        {   case '1':
                            defCss = '<?php echo VirtualDeskSiteMultimediaHelper::getEstadoCSS(1);?>';
                            break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSiteMultimediaHelper::getEstadoCSS(2);?>';
                                break;
                            case '3':
                                defCss = '<?php echo VirtualDeskSiteMultimediaHelper::getEstadoCSS(3);?>';
                                break;
                            case '4':
                                defCss = '<?php echo VirtualDeskSiteMultimediaHelper::getEstadoCSS(4);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 7,
                    "visible":false,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + row[8] + returnLinkf);
                    }
                },
                {
                    "targets": 8,
                    "visible":false
                },
                {
                    "targets": 9,
                    "visible":false
                },
                {
                    "targets": 10,
                    "visible":false
                },
                {
                    "targets": 11,
                    "visible":false
                }
            ],
            "order": [
                [11, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=multimedia.getMultimediaList4ManagerByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();

                UISwitch.initSwitchPlugin();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // Filtrar ESTADO
                this.api().column(10).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_MULTIMEDIA_FILTRO_ESTADO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBox').empty() )
                    jQuery('.EstadoFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Categorias
                this.api().column(9).every(function(){
                    var column = this;

                    var SelectText  = '<label> <?php echo JText::_("COM_VIRTUALDESK_MULTIMEDIA_FILTRO_CATEGORIAS"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($CategoriasOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.CategoriasFilterDropBox').empty() )
                    jQuery('.CategoriasFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });
            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_multimedia_wrapper');

    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableMultimedia();

        }

    };

}();


var UISwitch = function () {

    var initSwitchPlugin =  function (evt) {
        jQuery("input[name^='enabledPlugin_']").bootstrapSwitch();
    };


    var handleSwitchPlugin = function (evt) {

        jQuery("input[name^='enabledPlugin_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);
            let vd_url_pluginenable = jQuery(this).data('vd-url-pluginenable');
            let vd_url_checkpluginenable = jQuery(this).data('vd-url-checkpluginenable');

            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setPluginEnable(jQuery(this), vd_url_pluginenable, vd_url_checkpluginenable , enableVal);
        });
    };


    return {
        //main function to initiate the module
        init: function () {
        },
        initSwitchPlugin: function () {
            initSwitchPlugin();
            handleSwitchPlugin();
        }
    };

}();


var vdAjaxCall = function () {

    var setPluginEnable = function (el, vd_url_pluginenable, vd_url_checkpluginenable, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_pluginenable = vd_url_pluginenable + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_pluginenable,
            type: "POST",
            indexValue: {el:el, vd_url_checkpluginenable:vd_url_checkpluginenable},
            success: function(data){

                vdAjaxCall.getPluginEnable(this.indexValue.el, this.indexValue.vd_url_checkpluginenable);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var getPluginEnable = function (el, vd_user_checkpluginenable) {

        jQuery.ajax({
            url: vd_user_checkpluginenable,
            type: "POST",
            indexValue: {el:el},
            success: function(data){

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    //vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };


    return {
        setPluginEnable   : setPluginEnable,
        getPluginEnable   : getPluginEnable
    };

}();


jQuery(document).ready(function() {

    TableDatatablesManaged.init();
});