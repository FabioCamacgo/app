<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteMultimediaHelper',JPATH_SITE . '/components/com_virtualdesk/helpers/multimedia/virtualdesksite_multimedia.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteMultimediaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/multimedia/fotoCapa.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('multimedia');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('multimedia', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/multimedia/tmpl/multimedia.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');
    $obParam      = new VirtualDeskSiteParamsHelper();

    $getInputMultimedia_Id = JFactory::getApplication()->input->getInt('multimedia_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteMultimediaHelper::getMultimediaDetail4Manager($getInputMultimedia_Id);

    if(!empty($this->data->referencia)) {
        $ListFilesAlert = array();

        $objFotoCapaFiles = new VirtualDeskSiteMultimediaCapaFilesHelper();
        $ListFilesFotoCapa = $objFotoCapaFiles->getFileGuestLinkByRefId ($this->data->referencia);
    }

    $MultimediaEstadoList2Change = VirtualDeskSiteMultimediaHelper::getEstadoAllOptions($language_tag);

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_MULTIMEDIA_EMPTYLIST'), 'error' );
        return false;
    }

?>


    <div class="portlet light bordered">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_VIEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=multimedia&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=multimedia&layout=edit4manager&multimedia_id=' . $this->escape($this->data->multimedia_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=multimedia&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_NOVO' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <div class="tabbable-line nav-justified ">
                <form class="form-horizontal form-bordered">

                <div class="form-body">

                    <div class="portlet light">

                        <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                            <div class="portlet-body">

                                <div class="bloco">

                                    <legend>
                                        <h3><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_DADOSVIDEO'); ?></h3>
                                    </legend>

                                    <div class="form-group half">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_REFERENCIA' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->referencia, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>


                                    <div class="form-group half state">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_ESTADO' ); ?></div>
                                            <div class="value">
                                                <span class="label <?php echo VirtualDeskSiteMultimediaHelper::getEstadoCSS($this->data->id_estado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group all">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_NOME' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->nome, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>


                                    <div class="form-group all">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_CREDITOS' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->creditos, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>


                                    <div class="form-group half">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_CATEGORIA' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>

                                    <?php
                                    $explodeData = explode('-', $this->data->data_publicacao);
                                    $dataPub = $explodeData[2] . '-' . $explodeData[1] . '-' . $explodeData[0];
                                    ?>


                                    <div class="form-group half right">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_DATAPUBLICACAO'); ?></div>
                                            <div class="value"><?php echo htmlentities($dataPub, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>


                                    <?php
                                        $urlHttps = explode("https://", $this->data->link);
                                        if(count($urlHttps) == 1){
                                            $hasLink = 0;
                                            $urlHttp = explode("http://", $this->data->link);
                                            if(count($urlHttp) == 1){
                                                $hasLink = 0;
                                            } else {
                                                $hasLink = 1;
                                            }
                                        } else {
                                            $hasLink = 1;
                                        }

                                        if(!empty($this->data->link)){
                                            if($hasLink == 0){
                                                $urlVideo = 'https://' . $this->data->link;
                                            } else {
                                                $urlVideo = $this->data->link;
                                            }
                                        } else {
                                            $urlVideo = '';
                                        }
                                    ?>


                                    <div class="form-group all">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_LINK' ); ?></div>
                                            <div class="value"> <a href="<?php echo $urlVideo;?>" target="_blank"><?php echo $urlVideo;?></a> </div>
                                        </div>

                                        <?php
                                            if(!empty($urlVideo)){
                                                if (strpos($urlVideo, 'youtube') !== false) {

                                                    $youtubeCode = $obParam->getParamsByTag('youtubeCode');

                                                    if (strpos($urlVideo, 'watch?v=') !== false) {
                                                        $explodeLink = explode("watch?v=", $urlVideo);
                                                        $idVideo = explode('&', $explodeLink[1]);
                                                        ?>
                                                        <div class="form-group all">
                                                            <div class="row static-info ">
                                                                <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }

                                                    if (strpos($urlVideo, 'embed') !== false) {
                                                        $explodeLink = explode("embed/", $urlVideo);
                                                        $idVideo = explode('?', $explodeLink[1]);
                                                        ?>
                                                        <div class="form-group all">
                                                            <div class="row static-info ">
                                                                <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }

                                                if (strpos($urlVideo, 'youtu.be') !== false) {
                                                    $explodeLink = explode("youtu.be/", $urlVideo);
                                                    $idVideo = explode('?', $explodeLink[1]);

                                                    $youtubeCode = $obParam->getParamsByTag('youtubeCode');
                                                    ?>
                                                    <div class="form-group all">
                                                        <div class="row static-info ">
                                                            <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }

                                                if (strpos($urlVideo, 'facebook') !== false){

                                                    $explodeLink = explode("facebook.com/", $urlVideo);
                                                    $idVideo = $explodeLink[1];
                                                    $facebookCode = $obParam->getParamsByTag('facebookCode');

                                                    ?>
                                                    <div class="video">
                                                        <iframe src="<?php echo $facebookCode . urlencode($urlVideo);?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <?php
                                                }

                                                if (strpos($urlVideo, 'vimeo') !== false){
                                                    $vimeoCode = $obParam->getParamsByTag('vimeoCode');

                                                    if (strpos($urlVideo, 'player.vimeo.com/video/') !== false) {
                                                        $explodeLink = explode("player.vimeo.com/video/", $urlVideo);

                                                        ?>
                                                        <div class="form-group all">
                                                            <div class="row static-info ">
                                                                <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }

                                                    if (strpos($urlVideo, '/vimeo.com/') !== false) {
                                                        $explodeLink = explode("/vimeo.com/", $urlVideo);
                                                        ?>
                                                        <div class="form-group all">
                                                            <div class="row static-info ">
                                                                <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                        ?>
                                    </div>


                                    <div class="form-group half">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_FOTOCAPA' ); ?></div>
                                            <div class="value">
                                                <div id="vdPercursosPedestresFileGridCapa" class="cbp">
                                                    <?php
                                                    if(!is_array($ListFilesFotoCapa)) $ListFilesFotoCapa = array();
                                                    foreach ($ListFilesFotoCapa as $rowFile) : ?>
                                                        <div class="cbp-item identity logos">
                                                            <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                                <div class="cbp-caption-defaultWrap">
                                                                    <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                                <div class="cbp-caption-activeWrap">
                                                                    <div class="cbp-l-caption-alignLeft">
                                                                        <div class="cbp-l-caption-body">
                                                                            <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                            <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    <?php endforeach;?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=multimedia&layout=edit4manager&multimedia_id=' . $this->escape($this->data->multimedia_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=multimedia&layout=list4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('multimedia_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->multimedia_id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
            </div>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/multimedia/tmpl/view4manager.js.php');
    echo ('</script>');
?>