<?php
    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteMultimediaHelper',JPATH_SITE . '/components/com_virtualdesk/helpers/multimedia/virtualdesksite_multimedia.php');
    JLoader::register('VirtualDeskSiteMultimediaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/multimedia/fotoCapa.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('multimedia');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('multimedia', 'addnew4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/multimedia/tmpl/multimedia.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteMultimediaHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.multimedia.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }


    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email2   = $UserProfileData->email;
    $telefone = $UserProfileData->phone1;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-calendar font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_MULTIMEDIA_NOVO' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=multimedia&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body form">

        <div class="tabbable-line nav-justified ">

            <form id="new-multimedia" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=multimedia.create4Manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                <div class="form-body">

                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_NOVO'); ?></h3></legend>

                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_NOME'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" required class="form-control" autocomplete="off" name="nomeVideo" id="nomeVideo" maxlength="500" value="<?php echo $nomeVideo;?>"/>
                            </div>
                        </div>



                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_LINK'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" required class="form-control" autocomplete="off" name="linkVideo" id="linkVideo" maxlength="500" value="<?php echo $linkVideo;?>"/>
                            </div>
                        </div>



                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_CREDITOS'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="creditos" id="creditos" maxlength="250" value="<?php echo $creditos;?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_CATEGORIA'); ?><span class="required">*</span></label>
                            <?php $Categorias = VirtualDeskSiteMultimediaHelper::getCategoria()?>
                            <div class="value col-md-12">
                                <select name="categoria" required value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($categoria)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Categorias as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteMultimediaHelper::getCategoriaSelect($categoria) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeCategoria = VirtualDeskSiteMultimediaHelper::excludeCategoria($categoria)?>
                                        <?php foreach($ExcludeCategoria as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_DATAPUBLICACAO'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="date" class="form-control" name="data_publicacao" id="data_publicacao" value="<?php echo $data_publicacao; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half" id="uploadFieldCapa">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_MULTIMEDIA_FOTOCAPA'); ?></label>
                            <div class="value col-md-12">
                                <div class="file-loading">
                                    <input type="file" name="fileupload_capa[]" id="fileupload_capa" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                        </div>


                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=multimedia'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>


                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('multimedia.create4Manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>

                <?php echo JHtml::_('form.token'); ?>

            </form>
        </div>
    </div>
</div>

<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/multimedia/tmpl/addnew4manager.js.php');
    echo ('</script>');
?>
