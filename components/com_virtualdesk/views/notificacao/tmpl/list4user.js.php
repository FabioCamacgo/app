<?php
defined('_JEXEC') or die;

$EstadoOptionsHTML = '';
foreach($NotificacoesEstadoList as $row) {
    $EstadoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}
?>
var TableDatatablesManaged = function () {

    var initTableNotificacoesMinhas = function () {

        var table = jQuery('#tabela_lista_notificacao_minhas');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": '<"wrapper"lf<"EstadoFilterDropBoxMinhas">rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left"
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        switch (row[9])
                        {   case '1':
                            defCss = '<?php echo VirtualDeskSiteNotificacaoHelper::getNotificacaoEstadoCSS (1);?>';
                            break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSiteNotificacaoHelper::getNotificacaoEstadoCSS (2);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';

                        retVal += '<button class="btn btn-circle btn-outline green btNotificacaoMarcarComoLidaSWAL" title="" data-vd-notificacao-id="'+row[8]+'" type="button">';
                        retVal += '   <i class="fa fa-check"></i>   </button>';

                        retVal += '<button class="btn btn-circle btn-outline red btNotificacaoMarcarPorLerSWAL" title="" data-vd-notificacao-id="'+row[8]+'" type="button">';
                        retVal += '   <i class="fa fa-undo"></i>   </button>';

                        retVal += '<button class="btn btn-circle btn-outline  btAbrirModal btNotificacaoHistoricoModalOpen" title="<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_VERHIST' ); ?>" data-vd-notificacao-id="'+row[8]+'" type="button">';
                        retVal += '   <i class="fa fa-list-alt"></i>   </button>';

                        return (retVal);
                    }
                },
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                        if(row[10]=='' || row[10]=='undefined') return (retVal);
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> <?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_VERPROCASSOCIADO' ); ?> </a>';
                        return (returnLinki + row[13] + returnLinkf);
                    }
                },
                {
                    "targets": 6,
                    "visible":false
                }
                ,
                {
                    "targets": 7,
                    "visible":false
                }
                ,
                {
                    "targets": 8,
                    "visible":false
                }
                ,
                {
                    "targets": 9,
                    "visible":false
                }
                ,
                {
                    "targets": 10,
                    "visible":false
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=notificacao.getNotificacoesListMinhas4UserByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

                ButtonHandle.handleButtonNotificacaoHistoricoModal();
                SweetAlertHandle.handleMarcarComoLida();
                SweetAlertHandle.handleMarcarPorLer();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // username column
                this.api().column(9).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_ALERTA_FILTERBY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBoxMinhas').empty() )
                    jQuery('.EstadoFilterDropBoxMinhas select').on( 'change', function () {
                        column.search( jQuery(this).val() ).draw();
                    } );
                });
            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false

        });

    }

    var initTableNotificacaoHistoricoModal = function () {

        var table = jQuery('#tabela_notificacao_historico_modal');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": 'lfrtip',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left"
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=notificacao.getNotificacaoHistoricoList4UserByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

            },
            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }
            ]
        });
    }

    var ReloadTableNotificacoesMinhas = function () {
        var table = jQuery('#tabela_lista_notificacao_minhas');
        table.DataTable().ajax.reload();
    }

    var ReloadTableNotificacoesHistoricoModal = function (notificacaoID) {
        var table = jQuery('#tabela_notificacao_historico_modal');
        var newURL = "?option=com_virtualdesk&task=notificacao.getNotificacaoHistoricoList4UserByAjax" + "&<?php echo $obVDCrypt->setIdInputNameEncrypt('notificacao_id',$setencrypt_forminputhidden); ?>=" + notificacaoID;
        table.DataTable().ajax.url( newURL);
        table.DataTable().ajax.reload();
    }

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTableNotificacoesMinhas();
            initTableNotificacaoHistoricoModal();
        },

        ReloadTableNotificacoesHistoricoModal:function (notificacaoID) { ReloadTableNotificacoesHistoricoModal(notificacaoID); },
        ReloadTableNotificacoesMinhas:function () { ReloadTableNotificacoesMinhas(); },
    };

}();



var ButtonHandle = function () {

    var handleButtonNotificacaoHistoricoModal = function (evt) {

        jQuery(".btNotificacaoHistoricoModalOpen").on('click',function(evt,data){
            let notificacaoID       = jQuery(this).data('vd-notificacao-id');
            let elModal        = jQuery("#NotificacaoHistoricoModal");
            let elBlocoOptions = elModal.find('div.blocoNotificacaoHistorico');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');

            // Carrega dados e coloca dos valores nos campos
            vdAjaxCall.getNotificacaoHistoricoData(jQuery(this), urlGetContent, notificacaoID, elBlocoOptions, elModalContent);

            TableDatatablesManaged.ReloadTableNotificacoesHistoricoModal(notificacaoID);

            elModal.modal('show');
        });
    };

    return {
        //main function to initiate the module
        init: function () {

        },
        handleButtonNotificacaoHistoricoModal : handleButtonNotificacaoHistoricoModal
    };

}();


var ModalHandle = function () {

    var handleModalNotificacaoHistorico = function (evt) {
        jQuery("#NotificacaoHistoricoModal").modal({
            show: false,
            keyboard: true
        })
    };

    return {
        //main function to initiate the module
        init: function () {
            handleModalNotificacaoHistorico();
        }
    };
}();


var vdAjaxCall = function () {

    // Carrega os campos por ajax do histórico de uma e coloca na janela modal
    var getNotificacaoHistoricoData = function (el, vd_url_getcontent, vd_notificacao_id, elBlocoOptions, elModalContent) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassError   = 'fa-times-circle fa-4x text-danger';

        let elNome      = elBlocoOptions.find('span.NotificacaoHistoricoNome');
        let elDesc      = elBlocoOptions.find('span.NotificacaoHistoricoDesc');
        elNome.html('');
        elDesc.html('');

        let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            data: '&<?php echo $obVDCrypt->setIdInputNameEncrypt('notificacao_id',$setencrypt_forminputhidden); ?>=' + vd_notificacao_id,
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent, vd_notificacao_id:vd_notificacao_id, elDesc:elDesc, elNome:elNome},
            success: function(data){
                let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
                vdClosestI.removeClass(setClassSpinner).removeClass(setClassError);
                setTimeout(
                    function()
                    {
                        var response = JSON.parse(data);
                        elNome.html(response.nome);
                        elDesc.html(response.descricao);

                    }, 250);

            },
            error: function(error){
                let vdClosestI = elModalContent.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };

    // Carrega os campos da notificacao por ajax para o alterar do estado e coloca na janela modal
    var getNotificacaoAlterarEstadoData = function (el, vd_url_getcontent, vd_notificacao_id, elBlocoOptions, elModalContent) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassError   = 'fa-times-circle fa-4x text-danger';

        let elNome      = elBlocoOptions.find('span.NotificacaoAlterarEstadoNome');
        let elDesc      = elBlocoOptions.find('span.NotificacaoAlterarEstadoDesc');
        elNome.html('');
        elDesc.html('');

        let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            data: '&<?php echo $obVDCrypt->setIdInputNameEncrypt('notificacao_id',$setencrypt_forminputhidden); ?>=' + vd_notificacao_id,
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent, vd_notificacao_id:vd_notificacao_id, elDesc:elDesc, elNome:elNome},
            success: function(data){
                let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
                vdClosestI.removeClass(setClassSpinner).removeClass(setClassError);
                setTimeout(
                    function()
                    {
                        var response = JSON.parse(data);
                        elNome.html(response.nome);
                        elDesc.html(response.descricao);

                    }, 250);

            },
            error: function(error){
                let vdClosestI = elModalContent.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };

    // Carrega quantas notificacoes estão ABERTAS (não concluída e não anuladas) e coloca um icon com esse
    var getNotificacaoMinhasNumAbertasData = function (el) {
        let urlGetContent = el.data('vd-url-getcontent');
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {   el.find('.vdValorNot').html('');
                        if(typeof response !== 'undefined' && response.length>0) {
                            el.find('.vdValorNot').html(response.length);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };


    return {
        getNotificacaoHistoricoData      : getNotificacaoHistoricoData,
        getNotificacaoAlterarEstadoData  : getNotificacaoAlterarEstadoData,
        getNotificacaoMinhasNumAbertasData  : getNotificacaoMinhasNumAbertasData,
    };

}();


var ComponentsBootstrapSelect = function () {
    var handleBootstrapSelect = function() {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleBootstrapSelect();
        }
    };
}();


var ComponentsSelect2 = function() {
    var handleDemo = function() {
        jQuery.fn.select2.defaults.set("theme", "bootstrap");
        var placeholder = "";
        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,
            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


var NotificationsHandle = function () {

    var getNotificacaoMinhasNumAbertasData = function (evt) {
        let el = jQuery('#vdTabNavNotificacaoMinhasNumAbertas');
        vdAjaxCall.getNotificacaoMinhasNumAbertasData(el);
    };

    return {
        //main function to initiate the module
        init: function () {
            getNotificacaoMinhasNumAbertasData();
        },
        getNotificacaoAbertasReload: function () {
            getNotificacaoMinhasNumAbertasData();
        },
        getNotificacaoMinhasNumAbertasData: getNotificacaoMinhasNumAbertasData
    };
}();


var SweetAlertHandle = function () {

    var handleMarcarComoLida = function (evt) {

        jQuery(".btNotificacaoMarcarComoLidaSWAL").on('click',function(evt,data){

            let vd_url_send   = jQuery('button.notificacaoalterarestadoSend').data('vd-url-send');
            let vd_notificacao_id  = jQuery(this).attr('data-vd-notificacao-id'); // bug com o data(), só funcionou com o attr

            swal({
                    title: "<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_MARCARLIDA' ); ?>",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    cancelButtonClass: "btn-danger",
                    confirmButtonText: "<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_MARCARLIDA_SIM' ); ?>",
                    cancelButtonText: "<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_MARCARLIDA_NAO' ); ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true
                },
                function(){
                    let setNewEstadoId = 2;

                    jQuery.ajax({
                        url: vd_url_send,
                        type: "POST",
                        data:'setObs=&setNewEstadoId=' + setNewEstadoId + '&<?php echo $obVDCrypt->setIdInputNameEncrypt('notificacao_id',$setencrypt_forminputhidden); ?>=' + vd_notificacao_id,
                        indexValue: {vd_url_send:vd_url_send, vd_notificacao_id:vd_notificacao_id, setNewEstadoId:setNewEstadoId},
                        success: function(data){
                            setTimeout(
                                function()
                                {

                                    TableDatatablesManaged.ReloadTableNotificacoesMinhas();
                                    NotificationsHandle.getNotificacaoAbertasReload();
                                    AlarmsNotificacao_HandleMain4User.reload(); //MAIN

                                    swal("<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_ALTERADACOMSUCESSO' ); ?>", '', "success");

                                }, 500);
                        },
                        error: function(error){
                            swal("<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_ALTERADACOMERRO' ); ?>", '', "error");
                        }
                    });

                });
        });
    };

    var handleMarcarPorLer = function (evt) {

        jQuery(".btNotificacaoMarcarPorLerSWAL").on('click',function(evt,data){

            let vd_url_send   = jQuery('button.notificacaoalterarestadoSend').data('vd-url-send');
            let vd_notificacao_id  = jQuery(this).attr('data-vd-notificacao-id'); // bug com o data(), só funcionou com o attr

            swal({
                    title: "<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_MARCARPORLER' ); ?>",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    cancelButtonClass: "btn-danger",
                    confirmButtonText: "<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_MARCARPORLER_SIM' ); ?>",
                    cancelButtonText: "<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_MARCARPORLER_NAO' ); ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true
                },
                function(){
                    let setNewEstadoId = 1;

                    jQuery.ajax({
                        url: vd_url_send,
                        type: "POST",
                        data:'setObs=&setNewEstadoId=' + setNewEstadoId + '&<?php echo $obVDCrypt->setIdInputNameEncrypt('notificacao_id',$setencrypt_forminputhidden); ?>=' + vd_notificacao_id,
                        indexValue: {vd_url_send:vd_url_send, vd_notificacao_id:vd_notificacao_id, setNewEstadoId:setNewEstadoId},
                        success: function(data){
                            setTimeout(
                                function()
                                {
                                    TableDatatablesManaged.ReloadTableNotificacoesMinhas();
                                    NotificationsHandle.getNotificacaoAbertasReload();
                                    AlarmsNotificacao_HandleMain4User.reload(); //MAIN

                                    swal("<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_ALTERADACOMSUCESSO' ); ?>", '', "success");

                                }, 500);
                        },
                        error: function(error){
                            swal("<?php echo JText::_( 'COM_VIRTUALDESK_NOTIFICACAO_ALTERADACOMERRO' ); ?>", '', "error");
                        }
                    });

                });
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleMarcarComoLida();
            handleMarcarPorLer();
        },
        handleMarcarComoLida : handleMarcarComoLida,
        handleMarcarPorLer : handleMarcarPorLer
    };

}();


jQuery(document).ready(function() {

    TableDatatablesManaged.init();

    ButtonHandle.init();

    ModalHandle.init();

    ComponentsBootstrapSelect.init();

    ComponentsSelect2.init();

    NotificationsHandle.init();

    // Prevent default submit by Enter Key
    jQuery("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

});