<?php
defined('_JEXEC') or die;
?>

var CartaoEstacionamentoNew = function() {

    var handleCartaoEstacionamentoNew = function() {

        jQuery('#new-cartaoestacionamento').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-cartaoestacionamento').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-cartaoestacionamento').validate().form()) {
                    jQuery('#new-cartaoestacionamento').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleCartaoEstacionamentoNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {

    CartaoEstacionamentoNew.init();

    ComponentsSelect2.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('morador').onclick = function() {
        document.getElementById("fieldtipopedido").value = 1;
        document.getElementById("hintMorador").style.display = "block";
        document.getElementById("hintComerciante").style.display = "none";
        document.getElementById("hintFuncionario").style.display = "none";
        document.getElementById("hideFieldNomeEstabelecimento").style.display = "none";
        document.getElementById("hideFieldLocalTrabalho").style.display = "none";
        document.getElementById("hideFieldRuaPedido").style.display = "block";
        document.getElementById("hideFieldNumPedido").style.display = "block";
        document.getElementById("hideFieldLoteFuncionario").style.display = "block";
        document.getElementById("hideFieldCodPostalFuncionario").style.display = "block";
        document.getElementById("hideFieldTelefonePedido").style.display = "block";
        document.getElementById("hideFieldTemGaragem").style.display = "block";
        document.getElementById("hideFieldTemCartaoMorador").style.display = "block";
        document.getElementById("hideFieldCentroComercialComerciante").style.display = "none";
        document.getElementById("hideFieldCartaoComerciante").style.display = "none";
        document.getElementById("hideFieldCentroComercialFuncionario").style.display = "none";
        document.getElementById("hideFieldCartaoFuncionario").style.display = "none";
    };

    document.getElementById('comerciante').onclick = function() {
        document.getElementById("fieldtipopedido").value = 2;
        document.getElementById("hintMorador").style.display = "none";
        document.getElementById("hintComerciante").style.display = "block";
        document.getElementById("hintFuncionario").style.display = "none";
        document.getElementById("hideFieldNomeEstabelecimento").style.display = "block";
        document.getElementById("hideFieldLocalTrabalho").style.display = "none";
        document.getElementById("hideFieldRuaPedido").style.display = "block";
        document.getElementById("hideFieldNumPedido").style.display = "block";
        document.getElementById("hideFieldLoteFuncionario").style.display = "block";
        document.getElementById("hideFieldCodPostalFuncionario").style.display = "block";
        document.getElementById("hideFieldTelefonePedido").style.display = "block";
        document.getElementById("hideFieldTemGaragem").style.display = "none";
        document.getElementById("hideFieldTemCartaoMorador").style.display = "none";
        document.getElementById("hideFieldCentroComercialComerciante").style.display = "block";
        document.getElementById("hideFieldCartaoComerciante").style.display = "block";
        document.getElementById("hideFieldCentroComercialFuncionario").style.display = "none";
        document.getElementById("hideFieldCartaoFuncionario").style.display = "none";
    };

    document.getElementById('funcionario').onclick = function() {
        document.getElementById("fieldtipopedido").value = 3;
        document.getElementById("hintMorador").style.display = "none";
        document.getElementById("hintComerciante").style.display = "none";
        document.getElementById("hintFuncionario").style.display = "block";
        document.getElementById("hideFieldNomeEstabelecimento").style.display = "none";
        document.getElementById("hideFieldLocalTrabalho").style.display = "block";
        document.getElementById("hideFieldRuaPedido").style.display = "block";
        document.getElementById("hideFieldNumPedido").style.display = "block";
        document.getElementById("hideFieldLoteFuncionario").style.display = "block";
        document.getElementById("hideFieldCodPostalFuncionario").style.display = "block";
        document.getElementById("hideFieldTelefonePedido").style.display = "block";
        document.getElementById("hideFieldTemGaragem").style.display = "none";
        document.getElementById("hideFieldTemCartaoMorador").style.display = "none";
        document.getElementById("hideFieldCentroComercialComerciante").style.display = "none";
        document.getElementById("hideFieldCartaoComerciante").style.display = "none";
        document.getElementById("hideFieldCentroComercialFuncionario").style.display = "block";
        document.getElementById("hideFieldCartaoFuncionario").style.display = "block";
    };

    document.getElementById('simGaragem').onclick = function() {
        document.getElementById("fieldTemGaragem").value = 1;
    };

    document.getElementById('naoGaragem').onclick = function() {
        document.getElementById("fieldTemGaragem").value = 2;
    };

    document.getElementById('simCartaoMorador').onclick = function() {
        document.getElementById("fieldTemCartaoMorador").value = 1;
    };

    document.getElementById('naoCartaoMorador').onclick = function() {
        document.getElementById("fieldTemCartaoMorador").value = 2;
    };

    document.getElementById('simCentroComercialComerciante').onclick = function() {
        document.getElementById("fieldCentroComercialComerciante").value = 1;
    };

    document.getElementById('naoCentroComercialComerciante').onclick = function() {
        document.getElementById("fieldCentroComercialComerciante").value = 2;
    };

    document.getElementById('simCartaoComerciante').onclick = function() {
        document.getElementById("fieldCartaoComerciante").value = 1;
    };

    document.getElementById('naoCartaoComerciante').onclick = function() {
        document.getElementById("fieldCartaoComerciante").value = 2;
    };

    document.getElementById('simCentroComercialFuncionario').onclick = function() {
        document.getElementById("fieldCentroComercialFuncionario").value = 1;
    };

    document.getElementById('naoCentroComercialFuncionario').onclick = function() {
        document.getElementById("fieldCentroComercialFuncionario").value = 2;
    };

    document.getElementById('simCartaoFuncionario').onclick = function() {
        document.getElementById("fieldCartaoFuncionario").value = 1;
    };

    document.getElementById('naoCartaoFuncionario').onclick = function() {
        document.getElementById("fieldCartaoFuncionario").value = 2;
    };

});
