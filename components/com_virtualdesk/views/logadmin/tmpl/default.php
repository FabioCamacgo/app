<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteLogAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_logadmin.php');


/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('logadmin', 'listdefault');
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false  || $vbInGroupAM ==false ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}


// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';


//$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts  = '';
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');



//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');
$labelseparator = ' : ';

// Flush dos dados editados temporários na sessão
//VirtualDeskSiteLogAdminHelper::cleanAllTmpUserStare();

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-xs-10 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: 50% !important;  -moz-border-radius: 50% !important;  border-radius: 50% !important;   width: 150px; height: auto;}
    .portfolio-content.portfolio-1 .cbp-caption-activeWrap {background-color: rgba(50, 197, 210, 0.5); }
</style>


    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-eyeglasses font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            </div>
            <div class="actions">
               <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
            </div>
        </div>

        <div class="portlet-body ">

            <div class="tabbable-line nav-justified ">
                <ul class="nav nav-tabs">

                    <li class="active">
                        <a href="#tabUserActivationLogs">
                            <h4>
                                <i class="fa fa-list"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_TAB_USERACTIVATIONLOGS'); ?>
                            </h4>
                        </a>
                    </li>

                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=logadmin&layout=list_eventlog#tabEventLogs'); ?>" >
                            <h4>
                                <i class="fa fa-list-alt"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_TAB_EVENTLOGS'); ?>
                            </h4>
                        </a>
                    </li>

                    <li>
                        <a href="<?php
                            //echo JRoute::_('index.php?option=com_virtualdesk&view=logadmin&layout=list_eventlog_rgpd#tabEventLogsRGPD');
                            echo JRoute::_('index.php?option=com_virtualdesk&view=rgpd&layout=listlogs4manager');
                        ?>" >
                            <h4>
                                <i class="fa fa-th-list"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_TAB_EVENTLOG_RGPD'); ?>
                            </h4>
                        </a>
                    </li>
                </ul>


                <div class="tab-content">
                    <div class="tab-pane active" id="tabUserActivationLogs">

                        <div class="portlet light">

                            <div class="tabbable-line">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tabUserActivationLogsAll" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_TAB_USERACTIVATIONLOGS'); ?>
                                        </a>

                                    </li>
                                    <li class="">
                                        <a href="#tabUserActivation" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_TAB_USERACTIVATION'); ?>
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tabUserActivationLogsAll">
                                        <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_useractivationlogs">
                                        <thead>
                                        <tr>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_CREATED'); ?> </th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_LOGSTATUS'); ?></th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_LOGDESC'); ?></th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_LOGIN'); ?> </th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?> </th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_EMAIL'); ?> </th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_TOKEN'); ?> </th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_IP'); ?> </th>
                                        </tr>
                                        </thead>
                                        </table>
                                    </div>

                                    <div class="tab-pane" id="tabUserActivation">
                                        <div class="portlet light">
                                            <div class="portlet-body ">
                                                <div class="actions">

                                                </div>
                                                <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_useractivation">
                                                    <thead>
                                                    <tr>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_MODIFIED'); ?> </th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_LOGIN'); ?> </th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?> </th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_EMAIL'); ?> </th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_TOKEN'); ?> </th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_ENABLED'); ?> </th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ACTIVATED'); ?> </th>


                                                    </tr>
                                                    </thead>
                                                </table>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>

                    </div>



                    <div class="tab-pane" id="tabEventLogs">
                    </div>

                    <div class="tab-pane" id="tabEventLogsRGPD">
                    </div>

                </div>
            </div>


        </div>
    </div>



<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/logadmin/tmpl/default.js.php');
echo ('</script>');
?>