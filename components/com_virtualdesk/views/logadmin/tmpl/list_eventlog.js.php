<?php
defined('_JEXEC') or die;

$objAccessLevelOptions  = VirtualDeskSiteLogAdminHelper::getAccessLevelAllOptions();
$AccessLevelOptionsHTML = '';
foreach($objAccessLevelOptions as $rowC) {
    $AccessLevelOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

$objCategoryOptions  = VirtualDeskSiteLogAdminHelper::getCategoryAllOptions();
$CategoryOptionsHTML = '';
foreach($objCategoryOptions as $rowC) {
    $CategoryOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

$objTipoOPOptions  = VirtualDeskSiteLogAdminHelper::getTipoOPAllOptions();
$TipoOPOptionsHTML = '';
foreach($objTipoOPOptions as $rowC) {
    $TipoOPOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

$objModuloOptions  = VirtualDeskSiteLogAdminHelper::getModuloAllOptions();
$ModuloOptionsHTML = '';
foreach($objModuloOptions as $rowC) {
    $ModuloOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

$objPluginOptions  = VirtualDeskSiteLogAdminHelper::getPluginAllOptions();
$PluginOptionsHTML = '';
foreach($objPluginOptions as $rowC) {
    $PluginOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

?>
var TableDatatablesManaged = function () {

    var initTableEventLogs = function () {

        var table = jQuery('#tabela_lista_eventlogs');
        var tableTools = jQuery('#tabela_lista_eventlogs_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"lf <"AccessLevelFilterDropBox">  <"CategoryFilterDropBox">  <"TipoOPFilterDropBox"> <"ModuloFilterDropBox">  <"PluginFilterDropBox">  rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 50,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                            retVal = data + '  ';
                            retVal += '<a href="javascript:;" style="float:right" class="btn btn-sm grey-salsa btn-outline popovers" data-toggle="popover" data-placement="top" data-trigger="hover" ';
                            retVal += ' data-content="<?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ID');?>: ' + row[11] + '">';
                            retVal += '<i class="fa fa-info"></i></a>';
                        return (retVal);
                    }
                },

                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                        if(data !=='' && data !== 'undefined') {
                            retVal = row[6] + '  ';
                            retVal += '<a href="javascript:;" style="float:right" class="btn btn-sm grey-salsa btn-outline popovers" data-toggle="popover" data-placement="top" data-trigger="hover" ';
                            retVal += ' data-content="<?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_TITLE');?> ' + row[17] + '">';
                            retVal += '<i class="fa fa-info"></i></a>';
                        }
                        return (retVal);
                    }
                },

                {
                    "targets": 9,
                    "data": 9,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                        if(data=='1')  retVal += '<i class="fa fa-check"></i>';
                        return (retVal);
                    }
                },
                {
                    "targets": 11,
                    "visible":false
                },
                {
                    "targets": 12,
                    "visible":false
                },
                {
                    "targets": 13,
                    "visible":false
                },
                {
                    "targets": 14,
                    "visible":false
                },
                {
                    "targets": 15,
                    "visible":false
                }
                ,
                {
                    "targets": 16,
                    "visible":false
                }
                ,
                {
                    "targets": 17,
                    "visible":false
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=logadmin.getEventLogsByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

                // Filtrar ACCESSLEVEL
                this.api().column(13).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_ACCESSLEVEL"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($AccessLevelOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.AccessLevelFilterDropBox').empty() )
                    jQuery('.AccessLevelFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Category
                this.api().column(12).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_CATEGORY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($CategoryOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.CategoryFilterDropBox').empty() )
                    jQuery('.CategoryFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar TipoOP
                this.api().column(16).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_TIPOOP"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($TipoOPOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.TipoOPFilterDropBox').empty() )
                    jQuery('.TipoOPFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Plugin
                this.api().column(14).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_PLUGIN"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($PluginOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.PluginFilterDropBox').empty() )
                    jQuery('.PluginFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Modlo
                this.api().column(15).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_MODULO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($ModuloOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.ModuloFilterDropBox').empty() )
                    jQuery('.ModuloFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });


            }

        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        //var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');

    }

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableEventLogs();

        }
    };
}();


jQuery(document).ready(function() {

   TableDatatablesManaged.init();

});