<?php
defined('_JEXEC') or die;
?>

var EquipamentosresUrbanosNew = function() {

    var handleEquipamentosresUrbanosNew = function() {

        jQuery('#new-equipamentosresurbanos').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-equipamentosresurbanos').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-equipamentosresurbanos').validate().form()) {
                    jQuery('#new-equipamentosresurbanos').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleEquipamentosresUrbanosNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


jQuery(document).ready(function() {

    EquipamentosresUrbanosNew.init();

    ComponentsSelect2.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('instalacao').onclick = function() {
        document.getElementById("fieldTipoPedido").value = 1;
    };

    document.getElementById('manutencao').onclick = function() {
        document.getElementById("fieldTipoPedido").value = 2;
    };

    document.getElementById('substituicao').onclick = function() {
        document.getElementById("fieldTipoPedido").value = 3;
    };

    document.getElementById('seletiva').onclick = function() {
        document.getElementById("fieldTipoEquipamento").value = 1;
        document.getElementById("hideFieldOutroTipoEquipamento").style.display = "none";
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "block";
    };

    document.getElementById('indiferenciado').onclick = function() {
        document.getElementById("fieldTipoEquipamento").value = 2;
        document.getElementById("hideFieldOutroTipoEquipamento").style.display = "none";
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "none";
    };

    document.getElementById('outroEquipamento').onclick = function() {
        document.getElementById("fieldTipoEquipamento").value = 3;
        document.getElementById("hideFieldOutroTipoEquipamento").style.display = "block";
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "none";
    };

    document.getElementById('azul').onclick = function() {
        document.getElementById("fieldTipoRecolhaSeletiva").value = 1;
    };

    document.getElementById('verde').onclick = function() {
        document.getElementById("fieldTipoRecolhaSeletiva").value = 2;
    };

    document.getElementById('amarelo').onclick = function() {
        document.getElementById("fieldTipoRecolhaSeletiva").value = 3;
    };

    document.getElementById('120L').onclick = function() {
        document.getElementById("fieldCapacidadeEquipamento").value = 1;
        document.getElementById("fieldOutraCapacidade").style.display = "none";
    };

    document.getElementById('800L').onclick = function() {
        document.getElementById("fieldCapacidadeEquipamento").value = 2;
        document.getElementById("hideFieldOutraCapacidade").style.display = "none";
    };

    document.getElementById('1100L').onclick = function() {
        document.getElementById("fieldCapacidadeEquipamento").value = 3;
        document.getElementById("hideFieldOutraCapacidade").style.display = "none";
    };

    document.getElementById('outraCapacidade').onclick = function() {
        document.getElementById("capacidade").value = 4;
        document.getElementById("hideFieldOutraCapacidade").style.display = "block";
    };

});
