<?php
    defined('_JEXEC') or die;
?>

var RuidoFestasNew = function() {

    var handleRuidoFestasNew = function() {

        jQuery('#new-ruidofestas').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-ruidofestas').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-ruidofestas').validate().form()) {
                    jQuery('#new-ruidofestas').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleRuidoFestasNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var ComponentsEditors = function () {

    var handleWysihtml5 = function () {
        let  ckconfig_removeButtons = 'Subscript,Superscript,Anchor,SpecialChar,Image,About,Scayt';
        let  ckconfig_toolbarGroups =   [
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'others', groups: [ 'others' ] },
            { name: 'colors', groups: [ 'colors' ] }
        ];

        CKEDITOR.replace( 'fieldDesignacao', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'fieldJustificacao', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'fieldInicioAtividade', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'fieldFimAtividade', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'fieldMedidasPrevencao', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();

        }
    };

}();

jQuery(document).ready(function() {

    RuidoFestasNew.init();

    ComponentsSelect2.init();

    ComponentsEditors.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('espetaculos').onclick = function() {
        document.getElementById("fieldtipopedido").value = 1;
        document.getElementById("hideFieldOutraAtividade").style.display = "none";
    };

    document.getElementById('eventos').onclick = function() {
        document.getElementById("fieldtipopedido").value = 2;
        document.getElementById("hideFieldOutraAtividade").style.display = "none";
    };

    document.getElementById('feiras').onclick = function() {
        document.getElementById("fieldtipopedido").value = 3;
        document.getElementById("hideFieldOutraAtividade").style.display = "none";
    };

    document.getElementById('mercados').onclick = function() {
        document.getElementById("fieldtipopedido").value = 4;
        document.getElementById("hideFieldOutraAtividade").style.display = "none";
    };

    document.getElementById('provasDesportivas').onclick = function() {
        document.getElementById("fieldtipopedido").value = 5;
        document.getElementById("hideFieldOutraAtividade").style.display = "none";
    };

    document.getElementById('manifestacao').onclick = function() {
        document.getElementById("fieldtipopedido").value = 6;
        document.getElementById("hideFieldOutraAtividade").style.display = "none";
    };

    document.getElementById('festas').onclick = function() {
        document.getElementById("fieldtipopedido").value = 7;
        document.getElementById("hideFieldOutraAtividade").style.display = "none";
    };

    document.getElementById('outra').onclick = function() {
        document.getElementById("fieldtipopedido").value = 8;
        document.getElementById("hideFieldOutraAtividade").style.display = "block";
    };

    document.getElementById('aberto').onclick = function() {
        document.getElementById("fieldEspacoAtividades").value = 1;
    };

    document.getElementById('fechado').onclick = function() {
        document.getElementById("fieldEspacoAtividades").value = 2;
    };

    document.getElementById('simFogo').onclick = function() {
        document.getElementById("fieldFogoArtificio").value = 1;
    };

    document.getElementById('naoFogo').onclick = function() {
        document.getElementById("fieldFogoArtificio").value = 2;
    };

    document.getElementById('simPalco').onclick = function() {
        document.getElementById("fieldPalco").value = 1;
    };

    document.getElementById('naoPalco').onclick = function() {
        document.getElementById("fieldPalco").value = 2;
    };

    document.getElementById('simAmplificacaoSom').onclick = function() {
        document.getElementById("fieldAmplificacaoSom").value = 1;
        document.getElementById("hideAmplificacao").style.display = "block";
    };

    document.getElementById('naoAmplificacaoSom').onclick = function() {
        document.getElementById("fieldAmplificacaoSom").value = 2;
        document.getElementById("hideAmplificacao").style.display = "none";
    };

    document.getElementById('simEmissaoSom').onclick = function() {
        document.getElementById("fieldEmissaoSom").value = 1;
        document.getElementById("hideEmissao").style.display = "block";
    };

    document.getElementById('naoEmissaoSom').onclick = function() {
        document.getElementById("fieldEmissaoSom").value = 2;
        document.getElementById("hideEmissao").style.display = "none";
    };

});
