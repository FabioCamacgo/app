<?php
    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAmbienteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ambiente.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('ambiente');
    if($vbHasAccess===false ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    $tag_form='qiAeWZ8P';
    $formId = VirtualDeskSiteFormmainHelper::getFormId($tag_form);
    $modulo_id=VirtualDeskSiteFormmainHelper::getModuleId('ambiente');

    $fieldsForm = VirtualDeskSiteFormmainHelper::getFieldsData($tag_form, $modulo_id);

    $newFieldsExists = array();
    foreach ($fieldsForm as $keyF) {
        $newFieldsExists[] = $keyF['tag'];
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAmbienteHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewimunacao4user.ambiente.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email    = $UserProfileData->email;
    $email2   = $UserProfileData->email;
    $fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone = $UserProfileData->phone1;
    $morada = $UserProfileData->address;
    $codPostal = $UserProfileData->postalcode;


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelcemiterios'); ?>"><span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_CEMITERIOS'); ?></span></a>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag($tag_form); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelcemiterios'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <div class="tabbable-line nav-justified ">

                <div class="tab-content">

                    <form id="new-imunacao" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=ambiente.createimunacao4user'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REQUERENTE');?></h3></legend>
                            <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REQUIRED');?></p>

                            <?php if(in_array('fieldName',$newFieldsExists)) :?>
                                <div class="form-group fieldName">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NOME'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldName" id="fieldName" maxlength="500" value="<?php echo $nome; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldMorada',$newFieldsExists)) :?>
                                <div class="form-group fieldMorada">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MORADA'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldMorada" id="fieldMorada" maxlength="500" value="<?php echo $morada; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldPorta',$newFieldsExists)) :?>
                                <div class="form-group fieldPorta">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUM_PORTA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldPorta" id="fieldPorta" maxlength="5" value="<?php echo $numPorta; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLote',$newFieldsExists)) :?>
                                <div class="form-group fieldLote">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOTE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLote" id="fieldLote" maxlength="5" value="<?php echo $lote; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldLocalidade',$newFieldsExists)) :?>
                                <div class="form-group fieldLocalidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOCALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldLocalidade" id="fieldLocalidade" maxlength="100" value="<?php echo $localidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCodPostal',$newFieldsExists)) :?>
                                <div class="form-group fieldCodPostal">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CODPOSTAL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldCodPostal" id="fieldCodPostal" maxlength="30" value="<?php echo $codPostal; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldfreguesia',$newFieldsExists)) :?>
                                <div class="form-group fieldfreguesia">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_FREGUESIA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldfreguesia" id="fieldfreguesia" maxlength="100" value="<?php echo $freguesia; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNif',$newFieldsExists)) :?>
                                <div class="form-group fieldNif">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NIF'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required readonly class="form-control" name="fieldNif" id="fieldNif" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldDocId',$newFieldsExists)) :?>
                                <div class="form-group fieldDocId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TIPODOCIDENTIFICACAO'); ?></label>
                                    <div class="col-md-12">
                                        <input type="radio" name="radioval" id="cc" value="cc" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CC'); ?>
                                        <input type="radio" name="radioval" id="passaporte" value="passaporte" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocId'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_PASSAPORTE'); ?>
                                    </div>

                                    <input type="hidden" id="fieldDocId" name="fieldDocId" value="<?php echo $fieldDocId; ?>">
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldNumId',$newFieldsExists)) :?>
                                <div class="form-group fieldNumId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUMERO'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldNumId" id="fieldNumId" maxlength="30" value="<?php echo $numID; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldValId',$newFieldsExists)) :?>
                                <div class="form-group fieldValId">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_VALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="date" class="form-control" name="fieldValId" id="fieldValId" value="<?php echo $validade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldCCP',$newFieldsExists)) :?>
                                <div class="form-group fieldCCP">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CCP'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldCCP" id="fieldCCP" maxlength="100" value="<?php echo $ccp; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelemovel',$newFieldsExists)) :?>
                                <div class="form-group fieldTelemovel">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEMOVEL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="number" required class="form-control" name="fieldTelemovel" id="fieldTelemovel" maxlength="9" value="<?php echo $telemovel; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldTelefone',$newFieldsExists)) :?>
                                <div class="form-group fieldTelefone">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEFONE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldTelefone" id="fieldTelefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldFax',$newFieldsExists)) :?>
                                <div class="form-group fieldFax">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_FAX'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="number" class="form-control" name="fieldFax" id="fieldFax" maxlength="9" value="<?php echo $fax; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldEmail',$newFieldsExists)) :?>
                                <div class="form-group fieldEmail">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_EMAIL'); ?><span class="required">*</span></label>
                                    <div class="value col-md-12">
                                        <input type="text" required readonly class="form-control" name="fieldEmail" id="fieldEmail" maxlength="200" value="<?php echo $email; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if(in_array('fieldQualidade',$newFieldsExists)) :?>
                                <div class="form-group fieldQualidade">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_QUALIDADE'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldQualidade" id="fieldQualidade" maxlength="200" value="<?php echo $qualidade; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>

                        <?php if(in_array('fieldNomeRep',$newFieldsExists) || in_array('fieldMoradaRep',$newFieldsExists) || in_array('fieldPortaRep',$newFieldsExists) || in_array('fieldLoteRep',$newFieldsExists) || in_array('fieldLocalRep',$newFieldsExists) || in_array('fieldCodPostRep',$newFieldsExists) || in_array('fieldfreguesiaRep',$newFieldsExists) || in_array('fieldNifRep',$newFieldsExists) || in_array('fieldDocIdRep',$newFieldsExists) || in_array('fieldNumIdRep',$newFieldsExists) || in_array('fieldValIdRep',$newFieldsExists) || in_array('fieldCCPRep',$newFieldsExists) || in_array('fieldTelemovelRep',$newFieldsExists) || in_array('fieldTelefoneRep',$newFieldsExists) || in_array('fieldFaxRep',$newFieldsExists) || in_array('fieldEmailRep',$newFieldsExists) || in_array('fieldQualidadeRep',$newFieldsExists) || in_array('fieldOutraQualRep',$newFieldsExists)) :?>

                            <div class="bloco">

                                    <legend><h3><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REPRESENTANTE');?></h3></legend>

                                    <?php if(in_array('fieldNomeRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldNomeRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NOME'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldNomeRep" id="fieldNomeRep" maxlength="500" value="<?php echo $nomeRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldMoradaRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldMoradaRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MORADA'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldMoradaRep" id="fieldMoradaRep" maxlength="500" value="<?php echo $moradaRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldPortaRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldPortaRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUM_PORTA'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldPortaRep" id="fieldPortaRep" maxlength="5" value="<?php echo $numPortaRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldLoteRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldLoteRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOTE'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldLoteRep" id="fieldLoteRep" maxlength="5" value="<?php echo $loteRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldLocalRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldLocalRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOCALIDADE'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldLocalRep" id="fieldLocalRep" maxlength="100" value="<?php echo $localidadeRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldCodPostRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldCodPostRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CODPOSTAL'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldCodPostRep" id="fieldCodPostRep" maxlength="30" value="<?php echo $codPostalRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldfreguesiaRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldfreguesiaRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGUA_FREGUESIA'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldfreguesiaRep" id="fieldfreguesiaRep" maxlength="100" value="<?php echo $fieldfreguesiaRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldNifRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldNifRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NIF'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="number" class="form-control" name="fieldNifRep" id="fieldNifRep" maxlength="9" value="<?php echo $fiscalidRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldDocIdRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldDocIdRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TIPODOCIDENTIFICACAO'); ?></label>
                                            <div class="col-md-12">
                                                <input type="radio" name="radiovalRep" id="ccRep" value="ccRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CC'); ?>
                                                <input type="radio" name="radiovalRep" id="passaporteRep" value="passaporteRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldDocIdRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_PASSAPORTE'); ?>
                                            </div>

                                            <input type="hidden" id="fieldDocIdRep" name="fieldDocIdRep" value="<?php echo $fieldDocIdRep; ?>">
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldNumIdRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldNumIdRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUMERO'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="number" class="form-control" name="fieldNumIdRep" id="fieldNumIdRep" maxlength="30" value="<?php echo $numIDRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldValIdRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldValIdRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_VALIDADE'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="date" class="form-control" name="fieldValIdRep" id="fieldValIdRep" value="<?php echo $validadeRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldCCPRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldCCPRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CCP'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldCCPRep" id="fieldCCPRep" maxlength="100" value="<?php echo $ccpRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldTelemovelRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldTelemovelRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEMOVEL'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="number" class="form-control" name="fieldTelemovelRep" id="fieldTelemovelRep" maxlength="9" value="<?php echo $telemovelRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldTelefoneRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldTelefoneRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEFONE'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="number" class="form-control" name="fieldTelefoneRep" id="fieldTelefoneRep" maxlength="9" value="<?php echo $telefoneRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldFaxRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldFaxRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_FAX'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="number" class="form-control" name="fieldFaxRep" id="fieldFaxRep" maxlength="9" value="<?php echo $faxRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldEmailRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldEmailRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_EMAIL'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldEmailRep" id="fieldEmailRep" maxlength="200" value="<?php echo $emailRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldQualidadeRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldQualidadeRep">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_QUALIDADE'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="radio" name="radiovalQuaRep" id="repLegalRep" value="repLegalRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REPLEGAL'); ?>
                                                <input type="radio" name="radiovalQuaRep" id="gestNegRep" value="gestNegRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_GESTNEG'); ?>
                                                <input type="radio" name="radiovalQuaRep" id="mandatRep" value="mandatRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MANDATARIO'); ?>
                                                <input type="radio" name="radiovalQuaRep" id="outraRep" value="outraRep" <?php if (isset($_POST["submitForm"]) && $_POST['fieldQualidadeRep'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_OUTRA'); ?>
                                            </div>

                                            <input type="hidden" id="fieldQualidadeRep" name="fieldQualidadeRep" value="<?php echo $fieldQualidadeRep; ?>">
                                        </div>
                                    <?php endif; ?>


                                    <?php if(in_array('fieldOutraQualRep',$newFieldsExists)) :?>
                                        <div class="form-group fieldOutraQualRep" id="hideFieldOutraQualRep" style="display:none">
                                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_OUTRAQUALIDADE'); ?></label>
                                            <div class="value col-md-12">
                                                <input type="text" class="form-control" name="fieldOutraQualRep" id="fieldOutraQualRep" maxlength="200" value="<?php echo $outraQualRep; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                </div>

                        <?php endif; ?>


                        <?php if(in_array('fieldNomeFalecido',$newFieldsExists) || in_array('fieldEstadoCivilFalecido',$newFieldsExists) || in_array('fieldResidenciaFalecido',$newFieldsExists) || in_array('fieldLocalFalecimento',$newFieldsExists) || in_array('fieldFreguesiaFalecimento',$newFieldsExists) || in_array('fieldDataFalecimento',$newFieldsExists) || in_array('fieldNomeAgencia',$newFieldsExists) || in_array('fieldNIFAgencia',$newFieldsExists) || in_array('fieldTelefoneAgencia',$newFieldsExists) || in_array('fieldEmailAgencia',$newFieldsExists) || in_array('fieldNumRegistoAgencia',$newFieldsExists) || in_array('fieldtipopedido',$newFieldsExists) || in_array('fieldHoraImunacao',$newFieldsExists) || in_array('fieldDataImunacao',$newFieldsExists) || in_array('fieldTipoTransladacao',$newFieldsExists) || in_array('fieldSepulturaExumacao',$newFieldsExists) || in_array('fieldCemiterioDestino',$newFieldsExists) || in_array('fieldColocacaoTransladacao',$newFieldsExists) || in_array('fieldTalhaoDestino',$newFieldsExists) || in_array('fieldLetraDestino',$newFieldsExists) || in_array('fieldNumDestino',$newFieldsExists)) :?>

                            <div class="bloco">

                                <legend><h3><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_PEDIDO');?></h3></legend>
                                <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_INTRO_IMUNACAO');?></p>


                                <?php if(in_array('fieldNomeFalecido',$newFieldsExists) || in_array('fieldEstadoCivilFalecido',$newFieldsExists) || in_array('fieldResidenciaFalecido',$newFieldsExists) || in_array('fieldLocalFalecimento',$newFieldsExists) || in_array('fieldFreguesiaFalecimento',$newFieldsExists) || in_array('fieldDataFalecimento',$newFieldsExists)) :?>
                                    <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_INTRO_IDENTIFICACAOFALECIDO');?></p>
                                <?php endif; ?>


                                <?php if(in_array('fieldNomeFalecido',$newFieldsExists)) :?>
                                    <div class="form-group fieldNomeFalecido">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_NOMEFALECIDO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldNomeFalecido" id="fieldNomeFalecido" maxlength="200" value="<?php echo $nomeFalecido; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldEstadoCivilFalecido',$newFieldsExists)) :?>
                                    <div class="form-group fieldEstadoCivilFalecido">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_ESTADOCIVILFALECIDO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldEstadoCivilFalecido" id="fieldEstadoCivilFalecido" maxlength="50" value="<?php echo $estadoCivilFalecido; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldResidenciaFalecido',$newFieldsExists)) :?>
                                    <div class="form-group fieldResidenciaFalecido">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_RESIDENCIAFALECIDO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldResidenciaFalecido" id="fieldResidenciaFalecido" maxlength="500" value="<?php echo $moradaFalecido; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLocalFalecimento',$newFieldsExists)) :?>
                                    <div class="form-group fieldLocalFalecimento">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_LOCALFALECIMENTO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldLocalFalecimento" id="fieldLocalFalecimento" maxlength="200" value="<?php echo $localFalecimento; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldFreguesiaFalecimento',$newFieldsExists)) :?>
                                    <div class="form-group fieldFreguesiaFalecimento">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_FREGUESIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldFreguesiaFalecimento" id="fieldFreguesiaFalecimento" maxlength="200" value="<?php echo $freguesiaFalecido; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDataFalecimento',$newFieldsExists)) :?>
                                    <div class="form-group fieldDataFalecimento">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_DATAFALECIMENTO'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="date" class="form-control" name="fieldDataFalecimento" id="fieldDataFalecimento" value="<?php echo $dataFalecimento; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNomeAgencia',$newFieldsExists) || in_array('fieldNIFAgencia',$newFieldsExists) || in_array('fieldTelefoneAgencia',$newFieldsExists) || in_array('fieldEmailAgencia',$newFieldsExists) || in_array('fieldNumRegistoAgencia',$newFieldsExists)) :?>
                                    <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_INTRO_IDENTIFICACAOAGENCIA');?></p>
                                <?php endif; ?>


                                <?php if(in_array('fieldNomeAgencia',$newFieldsExists)) :?>
                                    <div class="form-group fieldNomeAgencia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_NOMEAGENCIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldNomeAgencia" id="fieldNomeAgencia" maxlength="200" value="<?php echo $nomeAgencia; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNIFAgencia',$newFieldsExists)) :?>
                                    <div class="form-group fieldNIFAgencia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_NIFAGENCIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNIFAgencia" id="fieldNIFAgencia" maxlength="9" value="<?php echo $nifAgencia; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTelefoneAgencia',$newFieldsExists)) :?>
                                    <div class="form-group fieldTelefoneAgencia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_TELEFAGENCIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldTelefoneAgencia" id="fieldTelefoneAgencia" maxlength="9" value="<?php echo $telefoneAgencia; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldEmailAgencia',$newFieldsExists)) :?>
                                    <div class="form-group fieldEmailAgencia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_EMAILAGENCIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="text" class="form-control" name="fieldEmailAgencia" id="fieldEmailAgencia" maxlength="200" value="<?php echo $fieldEmailAgencia; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNumRegistoAgencia',$newFieldsExists)) :?>
                                    <div class="form-group fieldNumRegistoAgencia">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_REGISTOATIVIDADEAGENCIA'); ?></label>
                                        <div class="value col-md-12">
                                            <input type="number" class="form-control" name="fieldNumRegistoAgencia" id="fieldNumRegistoAgencia" maxlength="50" value="<?php echo $registoAtividadeAgencia; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldtipopedido',$newFieldsExists)) :?>
                                    <div class="form-group fieldtipopedido">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_ACAOPRETENDIDA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalAcaoPretendida" id="imunacao" value="imunacao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldtipopedido'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_IMUNACAO'); ?>
                                            <input type="radio" name="radiovalAcaoPretendida" id="transladacao" value="transladacao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldtipopedido'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_TRANSLADACAO'); ?>
                                            <input type="radio" name="radiovalAcaoPretendida" id="exumacao" value="exumacao" <?php if (isset($_POST["submitForm"]) && $_POST['fieldtipopedido'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_EXUMACAO'); ?>
                                        </div>

                                        <input type="hidden" required id="fieldtipopedido" name="fieldtipopedido" value="<?php echo $fieldtipopedido; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldHoraImunacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldHoraImunacao" id="hideFieldHoraImunacao" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_AS'); ?></label>
                                        <div class="col-md-12">
                                            <input type="time" required class="form-control" name="fieldHoraImunacao" id="fieldHoraImunacao"  value="<?php echo $hora; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldDataImunacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldDataImunacao" id="hideFieldDataImunacao" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_DATAIMUNACAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="date" required class="form-control" name="fieldDataImunacao" id="fieldDataImunacao" value="<?php echo $dataImunacao; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTipoTransladacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldTipoTransladacao" id="hideFieldTipoTransladacao" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOMUNICIPAL_TIPOTRANSLADACAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalTipoTransladacao" id="cadaver" value="cadaver" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTipoTransladacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_CADAVER'); ?>
                                            <input type="radio" name="radiovalTipoTransladacao" id="ossadas" value="ossadas" <?php if (isset($_POST["submitForm"]) && $_POST['fieldTipoTransladacao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_OSSADAS'); ?>
                                         </div>

                                        <input type="hidden" id="fieldTipoTransladacao" name="fieldTipoTransladacao" value="<?php echo $tipoTransladacao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldSepulturaExumacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldSepulturaExumacao" id="hideFieldSepulturaExumacao" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOMUNICIPAL_TIPOSEPULTURA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalTipoSepulturaExu" id="sepulturaParticularExu" value="sepulturaParticularExu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaExumacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURAPARTICULAR'); ?>
                                            <input type="radio" name="radiovalTipoSepulturaExu" id="sepulturaTemporariaExu" value="sepulturaTemporariaExu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaExumacao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURATEMPORARIA'); ?>
                                        </div>

                                        <input type="hidden" id="fieldSepulturaExumacao" name="fieldSepulturaExumacao" value="<?php echo $fieldSepulturaExumacao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCemiterio',$newFieldsExists)) :?>
                                    <div class="form-group fieldCemiterio" id="hideFieldCemiterio" style="display:none">
                                        <label class="name col-md-12" id="nameCemImunacao" style="display:none"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOMUNICIPAL_IMUNACAO'); ?></label>
                                        <label class="name col-md-12" id="nameCemTransladacao" style="display:none"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOMUNICIPAL_TRANSLADACAO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalCemiterio" id="canhas" value="canhas" <?php if (isset($_POST["submitForm"]) && $_POST['fieldCemiterio'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_CANHAS'); ?>
                                            <input type="radio" name="radiovalCemiterio" id="terca" value="terca" <?php if (isset($_POST["submitForm"]) && $_POST['fieldCemiterio'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_TERCAS'); ?>
                                            <input type="radio" name="radiovalCemiterio" id="lombada" value="lombada" <?php if (isset($_POST["submitForm"]) && $_POST['fieldCemiterio'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_LOMBADA'); ?>
                                            <input type="radio" name="radiovalCemiterio" id="madalena" value="madalena" <?php if (isset($_POST["submitForm"]) && $_POST['fieldCemiterio'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_MADALENAMAR'); ?>
                                        </div>

                                        <input type="hidden" id="fieldCemiterio" name="fieldCemiterio" value="<?php echo $fieldCemiterio; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldSepulturaImunacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldSepulturaImunacao" id="hideFieldSepulturaImunacao" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOMUNICIPAL_TIPOSEPULTURA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalTipoSepultura" id="jazigoMunicipalImu" value="jazigoMunicipalImu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaImunacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_JAZIGOMUNICIPAL'); ?>
                                            <input type="radio" name="radiovalTipoSepultura" id="jazigoParticularImu" value="jazigoParticularImu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaImunacao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_JAZIGOPARTICULAR'); ?>
                                            <input type="radio" name="radiovalTipoSepultura" id="jazigoCapelaImu" value="jazigoCapelaImu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaImunacao'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_JAZIGOCAPELA'); ?>
                                            <input type="radio" name="radiovalTipoSepultura" id="ossarioMunicipalImu" value="ossarioMunicipalImu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaImunacao'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_OSSARIOMUNICIPAL'); ?>
                                            <input type="radio" name="radiovalTipoSepultura" id="sepulturaMunicipalImu" value="sepulturaMunicipalImu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaImunacao'] == 5) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURAPARTICULAR'); ?>
                                            <input type="radio" name="radiovalTipoSepultura" id="sepulturaTemporariaImu" value="sepulturaTemporariaImu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaImunacao'] == 6) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURATEMPORARIA'); ?>
                                            <input type="radio" name="radiovalTipoSepultura" id="ossarioParticularImu" value="ossarioParticularImu" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaImunacao'] == 7) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_OSSARIOPARTICULAR'); ?>
                                        </div>

                                        <input type="hidden" id="fieldSepulturaImunacao" name="fieldSepulturaImunacao" value="<?php echo $fieldSepulturaImunacao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldSepulturaTransladacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldSepulturaTransladacao" id="hideFieldSepulturaTransladacao" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOMUNICIPAL_TIPOSEPULTURA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalTipoSepulturaTrans" id="jazigoMunicipalTrans" value="jazigoMunicipalTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaTransladacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_JAZIGOMUNICIPAL'); ?>
                                            <input type="radio" name="radiovalTipoSepulturaTrans" id="sepulturaPerpetuaTrans" value="sepulturaPerpetuaTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaTransladacao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURAPERPETUA'); ?>
                                            <input type="radio" name="radiovalTipoSepulturaTrans" id="sepulturaTemporariaTrans" value="sepulturaTemporariaTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaTransladacao'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURAPARTICULAR'); ?>
                                            <input type="radio" name="radiovalTipoSepulturaTrans" id="ossarioTrans" value="ossarioTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldSepulturaTransladacao'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_OSSARIO'); ?>
                                        </div>

                                        <input type="hidden" id="fieldSepulturaTransladacao" name="fieldSepulturaTransladacao" value="<?php echo $fieldSepulturaTransladacao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTalhao',$newFieldsExists)) :?>
                                    <div class="form-group fieldTalhao" id="hideFieldTalhao" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_TALHAO_NUM'); ?></label>
                                        <div class="col-md-12">
                                            <input type="number" required class="form-control" maxlength="10" name="fieldTalhao" id="fieldTalhao" value="<?php echo $fieldTalhao; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLetra',$newFieldsExists)) :?>
                                    <div class="form-group fieldLetra" id="hideFieldLetra" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_LETRA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="text" required class="form-control" maxlength="10" name="fieldLetra" id="fieldLetra" value="<?php echo $fieldLetra; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNum',$newFieldsExists)) :?>
                                    <div class="form-group fieldNum" id="hideFieldNum" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_NUM'); ?></label>
                                        <div class="col-md-12">
                                            <input type="number" required class="form-control" maxlength="10" name="fieldNum" id="fieldNum" value="<?php echo $fieldNum; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldCemiterioDestino',$newFieldsExists)) :?>
                                    <div class="form-group fieldCemiterioDestino" id="hideFieldCemiterioDestino" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_DESTINO'); ?></label>
                                        <div class="col-md-12">
                                            <input type="text" required class="form-control" maxlength="200" name="fieldCemiterioDestino" id="fieldCemiterioDestino" value="<?php echo $fieldCemiterioDestino; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldColocacaoTransladacao',$newFieldsExists)) :?>
                                    <div class="form-group fieldColocacaoTransladacao" id="hideFieldColocacaoTransladacao" style="display:none">
                                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOMUNICIPAL_TIPOSEPULTURA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="radio" name="radiovalTipoColocacaoTrans" id="sepulturaParticularTrans" value="sepulturaParticularTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldColocacaoTransladacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURAPARTICULAR'); ?>
                                            <input type="radio" name="radiovalTipoColocacaoTrans" id="sepulturaTemporariaTrans" value="sepulturaTemporariaTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldColocacaoTransladacao'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_SEPULTURATEMPORARIA'); ?>
                                            <input type="radio" name="radiovalTipoColocacaoTrans" id="ossarioMunicipalTrans" value="ossarioMunicipalTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldColocacaoTransladacao'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_OSSARIOMUNICIPAL'); ?>
                                            <input type="radio" name="radiovalTipoColocacaoTrans" id="ossarioParticularTrans" value="ossarioParticularTrans" <?php if (isset($_POST["submitForm"]) && $_POST['fieldColocacaoTransladacao'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIOS_OSSARIOPARTICULAR'); ?>
                                        </div>

                                        <input type="hidden" id="fieldColocacaoTransladacao" name="fieldColocacaoTransladacao" value="<?php echo $fieldColocacaoTransladacao; ?>">
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldTalhaoDestino',$newFieldsExists)) :?>
                                    <div class="form-group fieldTalhaoDestino" id="hideFieldTalhaoDestino" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_TALHAO_NUM'); ?></label>
                                        <div class="col-md-12">
                                            <input type="number" required class="form-control" maxlength="10" name="fieldTalhaoDestino" id="fieldTalhaoDestino" value="<?php echo $fieldTalhaoDestino; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldLetraDestino',$newFieldsExists)) :?>
                                    <div class="form-group fieldLetraDestino" id="hideFieldLetraDestino" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_LETRA'); ?></label>
                                        <div class="col-md-12">
                                            <input type="text" required class="form-control" maxlength="10" name="fieldLetraDestino" id="fieldLetraDestino" value="<?php echo $fieldLetraDestino; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?php if(in_array('fieldNumDestino',$newFieldsExists)) :?>
                                    <div class="form-group fieldNumDestino" id="hideFieldNumDestino" style="display:none">
                                        <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CEMITERIO_NUM'); ?></label>
                                        <div class="col-md-12">
                                            <input type="number" required class="form-control" maxlength="10" name="fieldNumDestino" id="fieldNumDestino" value="<?php echo $fieldNumDestino; ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>

                        <?php endif; ?>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                    </button>
                                    <a class="btn default"
                                       href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelcemiterios'); ?>"
                                       title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"     value="<?php echo $obVDCrypt->formInputValueEncrypt('ambiente.createimunacao4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt('addnewimunacao4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('formId',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($formId,$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('userID',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($UserJoomlaID,$setencrypt_forminputhidden); ?>"/>

                        <?php echo JHtml::_('form.token'); ?>

                    </form>

                </div>
            </div>
        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/ambiente/tmpl/addnewimunacao4user.js.php');
    echo ('</script>');
?>