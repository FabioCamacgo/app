<?php
    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAmbienteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ambiente.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('ambiente');
    if($vbHasAccess===false ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAmbienteHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewentregaanimais4user.ambiente.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email    = $UserProfileData->email;
    $email2   = $UserProfileData->email;
    $fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone = $UserProfileData->phone1;
    $morada = $UserProfileData->address;
    $codPostal = $UserProfileData->postalcode;


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag($tag_form); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente#tab_AmbienteAnimais'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">

                <div class="tab-content">

                    <form id="new-entregaanimais" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=ambiente.createentregaanimais4user'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REQUERENTE');?></h3></legend>
                            <p class="hint"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REQUIRED');?></p>

                            <div class="form-group" id="fieldName">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NOME'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="nome" id="nome" maxlength="500" value="<?php echo $nome; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldMorada">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MORADA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="morada" id="morada" maxlength="500" value="<?php echo $morada; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldPorta">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUM_PORTA'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="numPorta" id="numPorta" maxlength="5" value="<?php echo $numPorta; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldLote">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOTE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="lote" id="lote" maxlength="5" value="<?php echo $lote; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldLocalidade">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOCALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="localidade" id="localidade" maxlength="100" value="<?php echo $localidade; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldCodPostal">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CODPOSTAL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="codPostal" id="codPostal" maxlength="30" value="<?php echo $codPostal; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldfreguesia">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_FREGUESIA'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="freguesia" id="freguesia" maxlength="100" value="<?php echo $freguesia; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldNif">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NIF'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="number" required readonly class="form-control" name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldDocId">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TIPODOCIDENTIFICACAO'); ?></label>
                                <div class="col-md-12">
                                    <input type="radio" name="radioval" id="cc" value="cc" <?php if (isset($_POST["submitForm"]) && $_POST['tipoSel'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CC'); ?>
                                    <input type="radio" name="radioval" id="passaporte" value="passaporte" <?php if (isset($_POST["submitForm"]) && $_POST['tipoSel'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_PASSAPORTE'); ?>
                                </div>

                                <input type="hidden" id="tipoSel" name="tipoSel" value="<?php echo $tipoSel; ?>">
                            </div>

                            <div class="form-group" id="fieldNumId">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUMERO'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="numID" id="numID" maxlength="30" value="<?php echo $numID; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldValId">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_VALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="date" class="form-control" name="validade" id="validade" value="<?php echo $validade; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldCCP">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CCP'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="ccp" id="ccp" maxlength="100" value="<?php echo $ccp; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldTelemovel">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEMOVEL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="number" required class="form-control" name="telemovel" id="telemovel" maxlength="9" value="<?php echo $telemovel; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldTelefone">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEFONE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldFax">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_FAX'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="fax" id="fax" maxlength="9" value="<?php echo $fax; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldEmail">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_EMAIL'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required readonly class="form-control" name="email" id="email" maxlength="200" value="<?php echo $email; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldQualidade">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_QUALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="qualidade" id="qualidade" maxlength="200" value="<?php echo $qualidade; ?>"/>
                                </div>
                            </div>

                        </div>

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REPRESENTANTE');?></h3></legend>

                            <div class="form-group" id="fieldNomeRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NOME'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="nomeRep" id="nomeRep" maxlength="500" value="<?php echo $nomeRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldMoradaRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MORADA'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="moradaRep" id="moradaRep" maxlength="500" value="<?php echo $moradaRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldPortaRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUM_PORTA'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="numPortaRep" id="numPortaRep" maxlength="5" value="<?php echo $numPortaRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldLoteRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOTE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="loteRep" id="loteRep" maxlength="5" value="<?php echo $loteRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldLocalRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_LOCALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="localidadeRep" id="localidadeRep" maxlength="100" value="<?php echo $localidadeRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldCodPostRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CODPOSTAL'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="codPostalRep" id="codPostalRep" maxlength="30" value="<?php echo $codPostalRep; ?>"/>
                                </div>
                            </div>

                            <?php if(in_array('fieldfreguesiaRep',$newFieldsExists)) :?>
                                <div class="form-group fieldfreguesiaRep">
                                    <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGUA_FREGUESIA'); ?></label>
                                    <div class="value col-md-12">
                                        <input type="text" class="form-control" name="fieldfreguesiaRep" id="fieldfreguesiaRep" maxlength="100" value="<?php echo $fieldfreguesiaRep; ?>"/>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="form-group" id="fieldNifRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NIF'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="fiscalidRep" id="fiscalidRep" maxlength="9" value="<?php echo $fiscalidRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldDocIdRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TIPODOCIDENTIFICACAO'); ?></label>
                                <div class="col-md-12">
                                    <input type="radio" name="radiovalRep" id="ccRep" value="ccRep" <?php if (isset($_POST["submitForm"]) && $_POST['tipoSelRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CC'); ?>
                                    <input type="radio" name="radiovalRep" id="passaporteRep" value="passaporteRep" <?php if (isset($_POST["submitForm"]) && $_POST['tipoSelRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_PASSAPORTE'); ?>
                                </div>

                                <input type="hidden" id="tipoSelRep" name="tipoSelRep" value="<?php echo $tipoSelRep; ?>">
                            </div>

                            <div class="form-group" id="fieldNumIdRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_NUMERO'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="numIDRep" id="numIDRep" maxlength="30" value="<?php echo $numIDRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldValIdRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_VALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="date" class="form-control" name="validadeRep" id="validadeRep" value="<?php echo $validadeRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldCCPRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_CCP'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="ccpRep" id="ccpRep" maxlength="100" value="<?php echo $ccpRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldTelemovelRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEMOVEL'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="telemovelRep" id="telemovelRep" maxlength="9" value="<?php echo $telemovelRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldTelefoneRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_TELEFONE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="telefoneRep" id="telefoneRep" maxlength="9" value="<?php echo $telefoneRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldFaxRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_FAX'); ?></label>
                                <div class="value col-md-12">
                                    <input type="number" class="form-control" name="faxRep" id="faxRep" maxlength="9" value="<?php echo $faxRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldEmailRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_EMAIL'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="emailRep" id="emailRep" maxlength="200" value="<?php echo $emailRep; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldQualidadeRep">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_QUALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="radio" name="radiovalQuaRep" id="repLegalRep" value="repLegalRep" <?php if (isset($_POST["submitForm"]) && $_POST['qualidadeRep'] == 1) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_REPLEGAL'); ?>
                                    <input type="radio" name="radiovalQuaRep" id="gestNegRep" value="gestNegRep" <?php if (isset($_POST["submitForm"]) && $_POST['qualidadeRep'] == 2) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_GESTNEG'); ?>
                                    <input type="radio" name="radiovalQuaRep" id="mandatRep" value="mandatRep" <?php if (isset($_POST["submitForm"]) && $_POST['qualidadeRep'] == 3) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MANDATARIO'); ?>
                                    <input type="radio" name="radiovalQuaRep" id="outraRep" value="outraRep" <?php if (isset($_POST["submitForm"]) && $_POST['qualidadeRep'] == 4) {echo 'checked="checked"'; } ?>> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_OUTRA'); ?>
                                </div>

                                <input type="hidden" id="qualidadeRep" name="qualidadeRep" value="<?php echo $qualidadeRep; ?>">
                            </div>

                            <div class="form-group" id="fieldOutraQualRep" style="display:none">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_OUTRAQUALIDADE'); ?></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="outraQualRep" id="outraQualRep" maxlength="200" value="<?php echo $outraQualRep; ?>"/>
                                </div>
                            </div>

                        </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                    </button>
                                    <a class="btn default"
                                       href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente#tab_AmbienteAnimais'); ?>"
                                       title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                </div>
                            </div>
                        </div>


                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('ambiente.createentregaanimais4user',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('addnewentregaanimais4user',$setencrypt_forminputhidden); ?>"/>


                        <?php echo JHtml::_('form.token'); ?>

                    </form>

                </div>
            </div>
        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/ambiente/tmpl/addnewentregaanimais4user.js.php');
    echo ('</script>');
?>