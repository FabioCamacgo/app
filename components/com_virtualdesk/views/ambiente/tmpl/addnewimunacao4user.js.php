<?php
defined('_JEXEC') or die;
?>

var ImunacaoNew = function() {

    var handleImunacaoNew = function() {

        jQuery('#new-imunacao').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-imunacao').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-imunacao').validate().form()) {
                    jQuery('#new-imunacao').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleImunacaoNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


jQuery(document).ready(function() {

    ImunacaoNew.init();

    ComponentsSelect2.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('imunacao').onclick = function() {
        document.getElementById("fieldtipopedido").value = 1;
        document.getElementById("hideFieldHoraImunacao").style.display = "block";
        document.getElementById("hideFieldDataImunacao").style.display = "block";
        document.getElementById("hideFieldTipoTransladacao").style.display = "none";
        document.getElementById("hideFieldSepulturaExumacao").style.display = "none";
        document.getElementById("hideFieldCemiterio").style.display = "block";
        document.getElementById("nameCemImunacao").style.display = "block";
        document.getElementById("nameCemTransladacao").style.display = "none";
        document.getElementById("hideFieldSepulturaImunacao").style.display = "block";
        document.getElementById("hideFieldSepulturaTransladacao").style.display = "none";
        document.getElementById("hideFieldTalhao").style.display = "block";
        document.getElementById("hideFieldLetra").style.display = "block";
        document.getElementById("hideFieldNum").style.display = "block";
        document.getElementById("hideFieldCemiterioDestino").style.display = "none";
        document.getElementById("hideFieldColocacaoTransladacao").style.display = "none";
        document.getElementById("hideFieldTalhaoDestino").style.display = "none";
        document.getElementById("hideFieldLetraDestino").style.display = "none";
        document.getElementById("hideFieldNumDestino").style.display = "none";
    };

    document.getElementById('transladacao').onclick = function() {
        document.getElementById("fieldtipopedido").value = 2;
        document.getElementById("hideFieldHoraImunacao").style.display = "none";
        document.getElementById("hideFieldDataImunacao").style.display = "none";
        document.getElementById("hideFieldTipoTransladacao").style.display = "block";
        document.getElementById("hideFieldSepulturaExumacao").style.display = "none";
        document.getElementById("hideFieldCemiterio").style.display = "block";
        document.getElementById("nameCemImunacao").style.display = "none";
        document.getElementById("nameCemTransladacao").style.display = "block";
        document.getElementById("hideFieldSepulturaImunacao").style.display = "none";
        document.getElementById("hideFieldSepulturaTransladacao").style.display = "block";
        document.getElementById("hideFieldTalhao").style.display = "block";
        document.getElementById("hideFieldLetra").style.display = "block";
        document.getElementById("hideFieldNum").style.display = "block";
        document.getElementById("hideFieldCemiterioDestino").style.display = "block";
        document.getElementById("hideFieldColocacaoTransladacao").style.display = "block";
        document.getElementById("hideFieldTalhaoDestino").style.display = "block";
        document.getElementById("hideFieldLetraDestino").style.display = "block";
        document.getElementById("hideFieldNumDestino").style.display = "block";
    };

    document.getElementById('exumacao').onclick = function() {
        document.getElementById("fieldtipopedido").value = 3;
        document.getElementById("hideFieldHoraImunacao").style.display = "none";
        document.getElementById("hideFieldDataImunacao").style.display = "none";
        document.getElementById("hideFieldTipoTransladacao").style.display = "none";
        document.getElementById("hideFieldSepulturaExumacao").style.display = "block";
        document.getElementById("hideFieldCemiterio").style.display = "block";
        document.getElementById("nameCemImunacao").style.display = "none";
        document.getElementById("nameCemTransladacao").style.display = "block";
        document.getElementById("hideFieldSepulturaImunacao").style.display = "none";
        document.getElementById("hideFieldSepulturaTransladacao").style.display = "none";
        document.getElementById("hideFieldTalhao").style.display = "block";
        document.getElementById("hideFieldLetra").style.display = "block";
        document.getElementById("hideFieldNum").style.display = "block";
        document.getElementById("hideFieldCemiterioDestino").style.display = "none";
        document.getElementById("hideFieldColocacaoTransladacao").style.display = "none";
        document.getElementById("hideFieldTalhaoDestino").style.display = "none";
        document.getElementById("hideFieldLetraDestino").style.display = "none";
        document.getElementById("hideFieldNumDestino").style.display = "none";
    };

    document.getElementById('canhas').onclick = function() {
        document.getElementById("fieldCemiterio").value = 1;
    };

    document.getElementById('terca').onclick = function() {
        document.getElementById("fieldCemiterio").value = 2;
    };

    document.getElementById('lombada').onclick = function() {
        document.getElementById("fieldCemiterio").value = 3;
    };

    document.getElementById('madalena').onclick = function() {
        document.getElementById("fieldCemiterio").value = 4;
    };

    document.getElementById('jazigoMunicipalImu').onclick = function() {
        document.getElementById("fieldSepulturaImunacao").value = 1;
    };

    document.getElementById('jazigoParticularImu').onclick = function() {
        document.getElementById("fieldSepulturaImunacao").value = 2;
    };

    document.getElementById('jazigoCapelaImu').onclick = function() {
        document.getElementById("fieldSepulturaImunacao").value = 3;
    };

    document.getElementById('ossarioMunicipalImu').onclick = function() {
        document.getElementById("fieldSepulturaImunacao").value = 4;
    };

    document.getElementById('sepulturaMunicipalImu').onclick = function() {
        document.getElementById("fieldSepulturaImunacao").value = 5;
    };

    document.getElementById('sepulturaTemporariaImu').onclick = function() {
        document.getElementById("fieldSepulturaImunacao").value = 6;
    };

    document.getElementById('ossarioParticularImu').onclick = function() {
        document.getElementById("fieldSepulturaImunacao").value = 7;
    };

    document.getElementById('cadaver').onclick = function() {
        document.getElementById("fieldTipoTransladacao").value = 1;
    };

    document.getElementById('ossadas').onclick = function() {
        document.getElementById("fieldTipoTransladacao").value = 2;
    };

    document.getElementById('jazigoMunicipalTrans').onclick = function() {
        document.getElementById("fieldSepulturaTransladacao").value = 1;
    };

    document.getElementById('sepulturaPerpetuaTrans').onclick = function() {
        document.getElementById("fieldSepulturaTransladacao").value = 2;
    };

    document.getElementById('sepulturaTemporariaTrans').onclick = function() {
        document.getElementById("fieldSepulturaTransladacao").value = 3;
    };

    document.getElementById('ossarioTrans').onclick = function() {
        document.getElementById("fieldSepulturaTransladacao").value = 4;
    };

    document.getElementById('sepulturaParticularTrans').onclick = function() {
        document.getElementById("fieldColocacaoTransladacao").value = 1;
    };

    document.getElementById('sepulturaTemporariaTrans').onclick = function() {
        document.getElementById("fieldColocacaoTransladacao").value = 2;
    };

    document.getElementById('ossarioMunicipalTrans').onclick = function() {
        document.getElementById("fieldColocacaoTransladacao").value = 3;
    };

    document.getElementById('ossarioParticularTrans').onclick = function() {
        document.getElementById("fieldColocacaoTransladacao").value = 4;
    };

    document.getElementById('sepulturaParticularExu').onclick = function() {
        document.getElementById("fieldSepulturaExumacao").value = 1;
    };

    document.getElementById('sepulturaTemporariaExu').onclick = function() {
        document.getElementById("fieldSepulturaExumacao").value = 2;
    };

});
