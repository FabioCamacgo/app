<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteAmbienteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ambiente.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('ambiente', 'list4users');
    if($vbHasAccess===false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

    $obParam    = new VirtualDeskSiteParamsHelper();
    // Gerar Links com id para o item de menu por defeito
    $AmbienteMenuId4User = $obParam->getParamsByTag('Ambiente_Menu_Id_By_Default_4Users');

?>

    <div class="portlet light bordered">


        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_CEMITERIOS'); ?></span>
            </div>

            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>">
                    <i class="fa fa-arrow-left"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
            </div>

        </div>

        <div class="portlet-body ">

            <div class="tabbable-line nav-justified ">

                <div class="tab-content">
                    <div class="row">

                        <?php
                            $Enabled_AlvaraConcessao = VirtualDeskSiteFormmainHelper::checkFormEnabled('EE6mVhye');
                            $Enabled_TerrenoJazigo = VirtualDeskSiteFormmainHelper::checkFormEnabled('bo6p7FAp');
                            $Enabled_2ViaalvaraConcessao = VirtualDeskSiteFormmainHelper::checkFormEnabled('3NLv4Xz6');
                            $Enabled_Imunacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('qiAeWZ8P');
                            $Enabled_OcupacaoOssarios = VirtualDeskSiteFormmainHelper::checkFormEnabled('Ap3YeRtd');
                            $Enabled_ObrasJazigos = VirtualDeskSiteFormmainHelper::checkFormEnabled('63MpJblX');
                            $Enabled_ProrrogacaoObras = VirtualDeskSiteFormmainHelper::checkFormEnabled('clUD7ab5');
                            $Enabled_ColocacaoLapides = VirtualDeskSiteFormmainHelper::checkFormEnabled('wvj3sg6R');
                        ?>


                        <?php if($Enabled_AlvaraConcessao == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('EE6mVhye'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('EE6mVhye'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnewalvaraconcessao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_TerrenoJazigo == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('bo6p7FAp'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('bo6p7FAp'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnewterrenojazigo4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_2ViaalvaraConcessao == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('3NLv4Xz6'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('3NLv4Xz6'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnew2viaalvaraconcessao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_Imunacao == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('qiAeWZ8P'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('qiAeWZ8P'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnewimunacao4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_OcupacaoOssarios == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('Ap3YeRtd'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('Ap3YeRtd'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnewocupacaoossarios4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_ObrasJazigos == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--info kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('63MpJblX'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('63MpJblX'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnewobrasjazigos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_ProrrogacaoObras == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('clUD7ab5'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('clUD7ab5'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnewprorrogacaoobrasjazigos4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_ColocacaoLapides == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('wvj3sg6R'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('wvj3sg6R'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=addnewcolocacaolapides4user&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>



<?php
echo $localScripts;
?>