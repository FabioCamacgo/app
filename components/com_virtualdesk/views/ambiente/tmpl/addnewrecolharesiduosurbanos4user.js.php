<?php
defined('_JEXEC') or die;
?>

var RecolhaResiduosUrbanosNew = function() {

    var handleRecolhaResiduosUrbanosNew = function() {

        jQuery('#new-recolharesiduosurbanos').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-recolharesiduosurbanos').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-recolharesiduosurbanos').validate().form()) {
                    jQuery('#new-recolharesiduosurbanos').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleRecolhaResiduosUrbanosNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


jQuery(document).ready(function() {

    RecolhaResiduosUrbanosNew.init();

    ComponentsSelect2.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('seletivo').onclick = function() {
        document.getElementById("fieldTipoResiduo").value = 1;
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "block";
        document.getElementById("monosContent").style.display = "none";
        document.getElementById("verdesContent").style.display = "none";
        document.getElementById("reeesContent").style.display = "none";
        document.getElementById("hideFieldOutroResiduo").style.display = "none";
    };

    document.getElementById('indiferenciado').onclick = function() {
        document.getElementById("fieldTipoResiduo").value = 2;
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "none";
        document.getElementById("monosContent").style.display = "none";
        document.getElementById("verdesContent").style.display = "none";
        document.getElementById("reeesContent").style.display = "none";
        document.getElementById("hideFieldOutroResiduo").style.display = "none";
    };

    document.getElementById('monos').onclick = function() {
        document.getElementById("fieldTipoResiduo").value = 3;
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "none";
        document.getElementById("monosContent").style.display = "block";
        document.getElementById("verdesContent").style.display = "none";
        document.getElementById("reeesContent").style.display = "none";
        document.getElementById("hideFieldOutroResiduo").style.display = "none";
    };

    document.getElementById('verdes').onclick = function() {
        document.getElementById("fieldTipoResiduo").value = 4;
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "none";
        document.getElementById("monosContent").style.display = "none";
        document.getElementById("verdesContent").style.display = "block";
        document.getElementById("reeesContent").style.display = "none";
        document.getElementById("hideFieldOutroResiduo").style.display = "none";
    };

    document.getElementById('reees').onclick = function() {
        document.getElementById("fieldTipoResiduo").value = 5;
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "none";
        document.getElementById("monosContent").style.display = "none";
        document.getElementById("verdesContent").style.display = "none";
        document.getElementById("reeesContent").style.display = "block";
        document.getElementById("hideFieldOutroResiduo").style.display = "none";
    };

    document.getElementById('outroResiduo').onclick = function() {
        document.getElementById("fieldTipoResiduo").value = 6;
        document.getElementById("hideFieldTipoRecolhaSeletiva").style.display = "none";
        document.getElementById("monosContent").style.display = "none";
        document.getElementById("verdesContent").style.display = "none";
        document.getElementById("reeesContent").style.display = "none";
        document.getElementById("hideFieldOutroResiduo").style.display = "block";
    };

    document.getElementById('azul').onclick = function() {
        document.getElementById("fieldTipoRecolhaSeletiva").value = 1;
    };

    document.getElementById('verde').onclick = function() {
        document.getElementById("fieldTipoRecolhaSeletiva").value = 2;
    };

    document.getElementById('amarelo').onclick = function() {
        document.getElementById("fieldTipoRecolhaSeletiva").value = 3;
    };

});
