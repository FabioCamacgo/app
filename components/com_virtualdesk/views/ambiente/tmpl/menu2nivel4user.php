<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteAmbienteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ambiente.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('ambiente', 'list4users');
    if($vbHasAccess===false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

    $obParam    = new VirtualDeskSiteParamsHelper();
    // Gerar Links com id para o item de menu por defeito
    $AmbienteMenuId4User = $obParam->getParamsByTag('Ambiente_Menu_Id_By_Default_4Users');


?>


    <div class="portlet light bordered">


        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            </div>

            <div class="actions">

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

            </div>

        </div>

        <div class="portlet-body ">

            <div class="tabbable-line nav-justified ">

                <div class="tab-content">

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ANIMAIS'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ANIMAIS'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelanimais'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ATIVIDADESRUIDOSAS'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ATIVIDADESRUIDOSAS'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelatividadesruidosas'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_CEMITERIOS'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_CEMITERIOS'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelcemiterios'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_LIMPEZAURBANA'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_LIMPEZAURBANA'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivellimpezaurbana'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_SAUDEPUBLICA'); ?></h3>
                                            <p class="kt-callout__desc">
                                                <?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_SAUDEPUBLICA'); ?>
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=menu3nivelsaudepublica'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success"><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_ACEDER'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



<?php
    echo $localScripts;
?>