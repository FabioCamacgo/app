<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteAmbienteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ambiente.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('ambiente', 'list4managers');
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}





// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

$obParam    = new VirtualDeskSiteParamsHelper();
// Gerar Links com id para o item de menu por defeito
$AmbienteMenuId4Manager = $obParam->getParamsByTag('Ambiente_Menu_Id_By_Default_4Managers');

?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}


</style>



<div class="portlet light bordered">


    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>

    </div>

    <div class="portlet-body ">


        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">
                <li class="active">

                    <a href="#tab_AmbienteSaneamento" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_SANEAMENTO'); ?>
                            <i class="fa fa-road"></i>
                        </h4>
                    </a>

                </li>
                <li class="">
                    <a href="#tab_AmbienteResiduos" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_RESIDUOS'); ?>
                            <i class="fa fa-trash-o"></i>
                        </h4>
                    </a>
                </li>
                <li class="">
                    <a href="#tab_AmbienteLimpeza" data-toggle="tab">
                        <h4><?php echo JText::_('COM_VIRTUALDESK_AMBIENTE_MENU_LIMPEZA'); ?>
                            <i class="fa fa-paint-brush"></i>
                        </h4>
                    </a>
                </li>
            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab_AmbienteSaneamento">


                    <div class="row">
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--success kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title">Descargas de águas residuais</h3>
                                            <p class="kt-callout__desc">
                                                Descargas de águas residuais
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Aceder</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title">Recolha em fossas séticas</h3>
                                            <p class="kt-callout__desc">
                                                Recolha em fossas séticas
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--success kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title">Contratos de saneamento</h3>
                                            <p class="kt-callout__desc">
                                                Contratos de saneamento
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="kt-portlet kt-callout  kt-callout--success kt-callout--diagonal-bg">
                                <div class="kt-portlet__body">
                                    <div class="kt-callout__body">
                                        <div class="kt-callout__content">
                                            <h3 class="kt-callout__title">Redes Prediais</h3>
                                            <p class="kt-callout__desc">
                                                Redes Prediais
                                            </p>
                                        </div>
                                        <div class="kt-callout__action">
                                            <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">

                        </div>
                        <div class="col-lg-4">

                        </div>
                    </div>

                </div>

                <div class="tab-pane " id="tab_AmbienteResiduos">

                    <div class="pricing-content-2">
                        <div class="pricing-table-container">
                            <div class="row padding-fix">



                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout  kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Recolha de objetos volumosos</h3>
                                                        <p class="kt-callout__desc">
                                                            Recolha de objetos volumosos
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Recolha de resíduos verdes</h3>
                                                        <p class="kt-callout__desc">
                                                            Recolha de resíduos verdes
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Ecopontos públicos</h3>
                                                        <p class="kt-callout__desc">
                                                            Ecopontos públicos
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Adesão ao contrato</h3>
                                                        <p class="kt-callout__desc">
                                                            Adesão ao contrato
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-4">

                                    </div>
                                </div>






                            </div>
                        </div>
                    </div>

                </div>

                <div class="tab-pane " id="tab_AmbienteLimpeza">

                    <div class="pricing-content-2">
                        <div class="pricing-table-container">
                            <div class="row padding-fix">


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Queimadas </h3>
                                                        <p class="kt-callout__desc">
                                                            Queimadas
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Limpeza de terrenos</h3>
                                                        <p class="kt-callout__desc">
                                                            Limpeza de terrenos
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Corte de árvores</h3>
                                                        <p class="kt-callout__desc">
                                                            Corte de árvores
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Limpeza urbana</h3>
                                                        <p class="kt-callout__desc">
                                                            Limpeza urbana
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Remoção de veículos</h3>
                                                        <p class="kt-callout__desc">
                                                            Remoção de veículos
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=remocao_veiculos_list4manager&Itemid='.$AmbienteMenuId4Manager.'&vdcleanstate=1'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Consultar</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="kt-portlet kt-callout kt-callout--success kt-callout--diagonal-bg">
                                            <div class="kt-portlet__body">
                                                <div class="kt-callout__body">
                                                    <div class="kt-callout__content">
                                                        <h3 class="kt-callout__title">Cemitérios </h3>
                                                        <p class="kt-callout__desc">
                                                            Cemitérios
                                                        </p>
                                                    </div>
                                                    <div class="kt-callout__action">
                                                        <a href="#" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Aceder</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>


    </div>
</div>



<?php

echo $localScripts;

?>


