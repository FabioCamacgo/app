<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteRemocaoVeiculosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/AguaAmbiente/virtualdesksite_remocaoVeiculos.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('ambiente', 'list4managers');
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

/*$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.waypoints.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.counterup.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/amcharts.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/serial.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/pie.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/light.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/dark.js' . $addscript_end;
*/

$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');

// Agenda - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/ambiente/tmpl/remocao-veiculos-comum.css');


$ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
$userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();

$imagback = $baseurl . '/templates/' . $templateName . '/images/image_back.jpg';

$obParam    = new VirtualDeskSiteParamsHelper();
// Gerar Links com id para o item de menu por defeito
$AmbienteMenuId4Manager = $obParam->getParamsByTag('Ambiente_Menu_Id_By_Default_4Managers');


// STATS

/*$AtiveUsersForSiteView        = VirtualDeskSiteStatsHelper::getNumberOfActiveUsersSiteView();
$ContactUsRequestSiteView     = VirtualDeskSiteStatsHelper::getNumberOfContactUsRequestSiteView();
$UserAccessSiteView           = VirtualDeskSiteStatsHelper::getNumberOfUserAccessSiteView();
$dataAlertaTotaisPorEstado    = VirtualDeskSiteStatsHelper::getAlertaTotaisPorEstado($userSessionID);
$dataAlertaTotaisPorCategoria = VirtualDeskSiteStatsHelper::getAlertaTotaisPorCategoria($userSessionID);
$dataAlertaTotPorAnoMes       = VirtualDeskSiteStatsHelper::getAlertaTotaisPorAnoMes($userSessionID);

$objReport                = new VirtualDeskSiteContactUsReportsHelper();
$dataTotAnoMes            = $objReport->getContactUsDashboard ($userSessionID);
$dataTotaisPorCat         = $objReport->getContactUsTotaisPorCategoria($userSessionID);
$dataTotaisPorEstado      = $objReport->getContactUsTotaisPorEstado($userSessionID);
*/
?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput{display:block;}
    .fileinput-preview {display:block; max-height: 100%; }
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}

    .tabbable-line>.tab-content {  padding: 0;  }
    .iconVDModified {padding-left: 15px; }
    .EstadoFilterDropBoxAgenda {float: right; padding-right: 30px; }

    div.vdBlocoContador {float:left; padding-left: 15px; }
    div.vdBlocoContadorTexto {margin-top: -26px; margin-left: 52px;}

</style>

<div class="portlet light bordered">
    <div class="portlet-title">
    <div class="caption">
        <i class="icon-calendar font-dark"></i>
        <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_('COM_VIRTUALDESK_REMOCAOVEICULOS_HEADING'); ?></span>
    </div>
    </div>

    <div class="portlet-body">

        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">
                <li class="active">
                    <a href="#tab_RemocaoVeiculos_Submetidos" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_REMOCAOVEICULOS_TAB_SUBMETIDOS'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ambiente&layout=remocao_veiculos_addnew4manager&Itemid='.$AmbienteMenuId4Manager.'&vdcleanstate=1'); ?>">
                        <h4>
                            <i class="fa fa-plus"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_REMOCAOVEICULOS_ADDNEW'); ?>
                        </h4>
                    </a>
                </li>

            </ul>

            <div class="tab-content">


                <div class="tab-pane active" id="tab_RemocaoVeiculos_Submetidos">
                    <div class="portlet">
                        <div class="portlet-title">


                            <div class="actions">

                                <!-- Botões DataTables -->
                                <div class="btn-group">
                                    <a class="btn green btn-outline btn-circle" href="javascript:;"
                                       data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="tabela_lista_remocaoveiculos_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="1" class="tool-action">
                                                <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                                            </a>
                                        </li>


                                    </ul>
                                </div>


                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"
                                   data-original-title="" title=""></a>

                            </div>

                        </div>

                        <div class="portlet-body ">

                            <table class="table table-striped table-bordered table-hover order-column"
                                   id="tabela_lista_remocaoveiculos">
                                <thead>
                                <tr>
                                    <th style="min-width: 100px;"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTA_DATA'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_REMOCAOVEICULOS_LISTA_REFERENCIA'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_REMOCAOVEICULOS_LISTA_DESCVIATURA'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_REMOCAOVEICULOS_LISTA_FREGUESIA'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTA_ESTADO'); ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>


                        </div>
                    </div>
                </div>

                <div class="tab-pane " id="tab_RemocaoVeiculos_Novo">
                </div>

            </div>
        </div>

    </div>
</div>





<?php

echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/ambiente/tmpl/remocao_veiculos_list4manager.js.php');
echo ('</script>');

?>



