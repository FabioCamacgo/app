<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_virtualdesk
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/**
 *  view class for Users.
 *
 * @since  1.6
 */
class VirtualDeskViewAmbiente extends JViewLegacy
{
	protected $data;
	protected $form;
	protected $params;
	protected $state;

	/**
	 * An instance of JDatabaseDriver.
	 *
	 * @var    JDatabaseDriver
	 * @since  3.6.3
	 */
	protected $db;

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed   A string if successful, otherwise an Error object.
	 *
	 * @since   1.6
	 */
	public function display($tpl = null)
	{
        /*
        * Check Permissões
        */
        $objCheckPerm = new VirtualDeskSitePermissionsHelper();
        $objCheckPerm->loadPermission();
        $vbHasAccess = $objCheckPerm->checkViewAccess('ambiente');
        if($vbHasAccess===false) {
            JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
            return false;
        }

        $LayoutWasOverrided = false;
        $LayoutAtual        = JFactory::getApplication()->input->get('layout');
        $TaskAtual          = JFactory::getApplication()->input->get('task');

        // Verifica qwual o layout a apresentar baseado no parametro layout, task e nos dados existentes...
        $resCheckTL = $this->checkTasksAndLayouts ($LayoutWasOverrided, $LayoutAtual, $TaskAtual );
        if($resCheckTL===false) return(false);


		$this->state            = $this->get('State');
		$this->params           = $this->state->get('params');
		$this->db               = JFactory::getDbo();

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
            $app = JFactory::getApplication();
            $app->logout();
            $redirect = JUri::root();
            $app->enqueueMessage(JText::_('COM_VIRTUALDESK_SESSIONENDED'), 'alert');
            $app->redirect(JRoute::_($redirect, false));
            return false;
            exit();

		}

		// Check for layout override
		$active = JFactory::getApplication()->getMenu()->getActive();
		if (isset($active->query['layout']) && $LayoutWasOverrided == false)
		{
			$this->setLayout($active->query['layout']);
		}


		// Escape strings for HTML output
		//$this->pageclass_sfx = htmlspecialchars($this->params->get('pageclass_sfx'));

		$this->prepareDocument();

		return parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();
		$user  = JFactory::getUser();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();


		    //switch ( JFactory::getApplication()->input->get('layout') )
            switch ( $this->getLayout() )
            {

                default:
                    $this->params->def('page_heading', JText::_('COM_VIRTUALDESK_AMBIENTE_HEADING'));

                    break;
            }


		$title = $this->params->get('page_title', '');

		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
	}


    /**
     * Prepares the document
     *
     * @return  boolean
     *
     * @since   1.6
     */
    protected function checkTasksAndLayouts (&$LayoutWasOverrided, $LayoutAtual='', $TaskAtual='' ) {

        switch ( $LayoutAtual )
        {
            case "view4user":
            case "view2viaalvaraconcessao4user":
            case "viewadocaoanimais4user":
            case "viewalvaraconcessao4user":
            case "viewcolocacaolapides4user":
            case "viewentregaanimais4user":
            case "viewequipamentosresurbanos4user":
            case "viewimunacao4user":
            case "viewinsalubridadeambiental4user":
            case "viewobrasjazigos4user":
            case "viewocupacaoossarios4user":
            case "viewprorrogacaoobrasjazigos4user":
            case "viewrecolhaanimais4user":
            case "viewrecolharesiduosurbanos4user":
            case "viewrecolharesiduosurbanoscomerciais4user":
            case "viewruidofestas4user":
            case "viewruidoobras4user":
            case "viewterrenojazigo4user":
            case "view4manager":
            case "edit4user":
            case "edit2viaalvaraconcessao4user":
            case "editadocaoanimais4user":
            case "editalvaraconcessao4user":
            case "editcolocacaolapides4user":
            case "editentregaanimais4user":
            case "editequipamentosresurbanos4user":
            case "editimunacao4user":
            case "editinsalubridadeambiental4user":
            case "editobrasjazigos4user":
            case "editocupacaoossarios4user":
            case "editprorrogacaoobrasjazigos4user":
            case "editrecolhaanimais4user":
            case "editrecolharesiduosurbanos4user":
            case "editrecolharesiduosurbanoscomerciais4user":
            case "editruidofestas4user":
            case "editruidoobras4user":
            case "editterrenojazigo4user":
            case "edit4manager":
            case "addnew4user":
            case "addnew2viaalvaraconcessao4user":
            case "addnewadocaoanimais4user":
            case "addnewalvaraconcessao4user":
            case "addnewcolocacaolapides4user":
            case "addnewentregaanimais4user":
            case "addnewequipamentosresurbanos4user":
            case "addnewimunacao4user":
            case "addnewinsalubridadeambiental4user":
            case "addnewobrasjazigos4user":
            case "addnewocupacaoossarios4user":
            case "addnewprorrogacaoobrasjazigos4user":
            case "addnewrecolhaanimais4user":
            case "addnewrecolharesiduosurbanos4user":
            case "addnewrecolharesiduosurbanoscomerciais4user":
            case "addnewruidofestas4user":
            case "addnewruidoobras4user":
            case "addnewterrenojazigo4user":
            case "menu3nivelanimais":
            case "menu3nivelatividadesruidosas":
            case "menu3nivelcemiterios":
            case "menu3nivellimpezaurbana":
            case "menu3nivelsaudepublica":
            case "addnew4manager":
            case "list4user":
            case "list4manager":
            case "remocao_veiculos_list4user":
            case "remocao_veiculos_list4manager":
                $this->data = array();
                $LayoutWasOverrided = true;
            break;

            default:
            break;
        }

        return(true);
    }


}
