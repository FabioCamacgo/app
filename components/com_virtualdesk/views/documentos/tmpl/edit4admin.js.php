<?php

    JLoader::register('VirtualDeskSiteDocumentosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/files.php');

    $objLogoFiles                = new VirtualDeskSiteDocumentosFilesHelper();
    $vdPreFileUploderDocLista   = $objLogoFiles->getFileUploaderJS_ByRefId_4Admin ($this->data->referencia);

?>

var DocumentoNew = function() {

    var handleDocumentoEdit = function() {

        jQuery('#edit-documento').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#edit-documento').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#edit-documento').validate().form()) {
                    jQuery('#edit-documento').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleDocumentoEdit();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        jQuery("#fileupload_doc").fileuploader({
            changeInput: ' ',
            limit: 1,
            fileMaxSize: 200,
            extensions: ['pdf', 'docx', 'doc', 'xlsx', 'xls', 'pptx', 'ppt'],
            enableApi: true,
            addMore: true,
            theme: 'thumbnails',
            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploderDocLista)) echo(', files:  '.$vdPreFileUploderDocLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#uploadFieldLogo').find('input[name="vdFileUpChangedLogo"]').val('1');
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#uploadFieldLogo').find('input[name="vdFileUpChangedLogo"]').val('1');
                return true;
            }
        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater').each(function(){
                jQuery(this).repeater({
                    show: function () {

                        jQuery(this).slideDown();

                    },

                    hide: function (deleteElement) {

                        if(confirm('Deseja apagar este elemento?')) {
                            jQuery(this).slideUp(deleteElement);
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });
        }

    };

}();

jQuery(document).ready(function() {

    DocumentoNew.init();

    ComponentsSelect2.init();

    ComponentFileUploader.init();

    FormRepeater.init();

    jQuery("#catLevel1").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var catLevel1 = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(catLevel1 == null || catLevel1 == '') {

            var select = jQuery('#catLevel2');
            select.find('option').remove();
            select.prop('disabled', true);

            var select2 = jQuery('#catLevel3');
            select2.find('option').remove();
            select2.prop('disabled', true);
            jQuery('#catLevel3').val() == '0';

            var select3 = jQuery('#catLevel4');
            select3.find('option').remove();
            select3.prop('disabled', true);
            jQuery('#catLevel4').val() == '0';

        } else {

            var select2 = jQuery('#catLevel3');
            select2.find('option').remove();
            select2.prop('disabled', true);
            jQuery('#catLevel3').val() == '0';

            var select3 = jQuery('#catLevel4');
            select3.find('option').remove();
            select3.prop('disabled', true);
            jQuery('#catLevel4').val() == '0';

            var dataString = "catLevel1="+catLevel1; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoCategoriaNivel2',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=documentos_categoriaLevel1&catLevel1=' + catLevel1 ,

                success: function(output) {

                    var select = jQuery('#catLevel2');
                    select.find('option').remove();

                    if (output == null || output == '' || output == '[]') {
                        select.prop('disabled', true);
                        select2.prop('disabled', true);
                        select3.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoCategoriaNivel2');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoCategoriaNivel2');
                }
            });
        }


    });

    jQuery("#catLevel2").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var catLevel2 = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(catLevel2 == null || catLevel2 == '') {

            var select = jQuery('#catLevel3');
            select.find('option').remove();
            select.prop('disabled', true);

            var select4 = jQuery('#catLevel4');
            select4.find('option').remove();
            select4.prop('disabled', true);
            jQuery('#catLevel4').val() == '0';

        } else {

            var select4 = jQuery('#catLevel4');
            select4.find('option').remove();
            select4.prop('disabled', true);
            jQuery('#catLevel4').val() == '0';

            var dataString = "catLevel2="+catLevel2; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoCategoriaNivel3',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=documentos_categoriaLevel2&catLevel2=' + catLevel2 ,

                success: function(output) {

                    var select = jQuery('#catLevel3');
                    select.find('option').remove();

                    if (output == null || output == '' || output == '[]') {
                        select.prop('disabled', true);
                        select4.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoCategoriaNivel3');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoCategoriaNivel3');
                }
            });
        }


    });

    jQuery("#catLevel3").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var catLevel3 = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(catLevel3 == null || catLevel3 == '') {

            var select = jQuery('#catLevel4');
            select.find('option').remove();
            select.prop('disabled', true);

        } else {

            var dataString = "catLevel3="+catLevel3; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoCategoriaNivel4',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=documentos_categoriaLevel3&catLevel3=' + catLevel3 ,

                success: function(output) {

                    var select = jQuery('#catLevel4');
                    select.find('option').remove();

                    if (output == null || output == '' || output == '[]') {
                        select.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoCategoriaNivel4');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoCategoriaNivel4');
                }
            });
        }


    });

});
