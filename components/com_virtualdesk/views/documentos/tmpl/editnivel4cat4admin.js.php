<?php
defined('_JEXEC') or die;
?>

var CategoriaEdit = function() {

    var handleCategoriaEdit = function() {

        jQuery('#edit-categoria-level4').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#edit-categoria-level4').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#edit-categoria-level4').validate().form()) {
                    jQuery('#edit-categoria-level4').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleCategoriaEdit();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {

    CategoriaEdit.init();

    ComponentsSelect2.init();

    jQuery("#catLevel1").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var catLevel1 = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(catLevel1 == null || catLevel1 == '') {

            var select = jQuery('#catLevel2');
            select.find('option').remove();
            select.prop('disabled', true);
            jQuery('#catLevel2').val() == '0';

            var select2 = jQuery('#catLevel3');
            select2.find('option').remove();
            select2.prop('disabled', true);
            jQuery('#catLevel3').val() == '0';

        } else {

            var dataString = "catLevel1="+catLevel1; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoCategoriaNivel2',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=documentos_categoriaLevel1&catLevel1=' + catLevel1 ,

                success: function(output) {

                    var select = jQuery('#catLevel2');
                    select.find('option').remove();

                    var select2 = jQuery('#catLevel3');
                    select2.find('option').remove();

                    if (output == null || output == '' || output == '[]') {
                        select.prop('disabled', true);
                        select2.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));

                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoCategoriaNivel2');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoCategoriaNivel2');
                }
            });
        }


    });

    jQuery("#catLevel2").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var catLevel2 = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(catLevel2 == null || catLevel2 == '') {

            var select = jQuery('#catLevel3');
            select.find('option').remove();
            select.prop('disabled', true);
            jQuery('#catLevel3').val() == '0';

        } else {

            var dataString = "catLevel2="+catLevel2; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoCategoriaNivel3',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=documentos_categoriaLevel2&catLevel2=' + catLevel2 ,

                success: function(output) {

                    var select = jQuery('#catLevel3');
                    select.find('option').remove();

                    if (output == null || output == '' || output == '[]') {
                        select.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoCategoriaNivel3');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoCategoriaNivel3');
                }
            });
        }


    });

});
