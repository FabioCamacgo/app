<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('documentos', 'listcat4admin');
    $vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/documentos/tmpl/documentos.css');

    $ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
    $userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();
?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIAS'); ?></span>
        </div>

    </div>


    <div class="portlet-body">

        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">

                <li class="active">
                    <a href="#tab_Documentos_Categoria_Nivel1" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIAS_NIVEL1'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Documentos_Categoria_Nivel2" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIAS_NIVEL2'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Documentos_Categoria_Nivel3" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIAS_NIVEL3'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Documentos_Categoria_Nivel4" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIAS_NIVEL4'); ?>
                        </h4>
                    </a>
                </li>

            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_Documentos_Categoria_Nivel1">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-calendar font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL1_TITLE'); ?></span>
                            </div>

                            <div class="actions">
                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos'); ?>" class="btn btn-circle btn-default">
                                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel1cat4admin&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL1_NOVO' ); ?></a>

                                <div class="btn-group">
                                    <a class="btn green btn-outline btn-circle" href="javascript:;"
                                       data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="tabela_categorias_documentos_nivel1_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="1" class="tool-action">
                                                <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                                            </a>
                                        </li>


                                    </ul>
                                </div>

                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                            </div>

                        </div>

                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_categorias_documentos_nivel1">
                            <thead>
                            <tr>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ID'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NOME'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ESTADO'); ?></th>
                                <th> </th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>

                <div class="tab-pane" id="tab_Documentos_Categoria_Nivel2">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-calendar font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL2_TITLE'); ?></span>
                            </div>

                            <div class="actions">
                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos'); ?>" class="btn btn-circle btn-default">
                                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel2cat4admin&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL2_NOVO' ); ?></a>

                                <div class="btn-group">
                                    <a class="btn green btn-outline btn-circle" href="javascript:;"
                                       data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="tabela_categorias_documentos_nivel2_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="1" class="tool-action">
                                                <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                                            </a>
                                        </li>

                                    </ul>
                                </div>

                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                            </div>

                        </div>

                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_categorias_documentos_nivel2">
                            <thead>
                            <tr>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ID'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NOME'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_TABELA_NIVEL1'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ESTADO'); ?></th>
                                <th> </th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>

                <div class="tab-pane" id="tab_Documentos_Categoria_Nivel3">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-calendar font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL3_TITLE'); ?></span>
                            </div>

                            <div class="actions">
                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos'); ?>" class="btn btn-circle btn-default">
                                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel3cat4admin&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL3_NOVO' ); ?></a>

                                <div class="btn-group">
                                    <a class="btn green btn-outline btn-circle" href="javascript:;"
                                       data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="tabela_categorias_documentos_nivel3_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="1" class="tool-action">
                                                <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                                            </a>
                                        </li>


                                    </ul>
                                </div>

                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                            </div>

                        </div>

                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_categorias_documentos_nivel3">
                            <thead>
                            <tr>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ID'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NOME'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_TABELA_NIVEL1'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_TABELA_NIVEL2'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ESTADO'); ?></th>
                                <th> </th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>

                <div class="tab-pane" id="tab_Documentos_Categoria_Nivel4">

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-calendar font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL4_TITLE'); ?></span>
                            </div>

                            <div class="actions">
                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos'); ?>" class="btn btn-circle btn-default">
                                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=addnewnivel4cat4admin&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL4_NOVO' ); ?></a>

                                <div class="btn-group">
                                    <a class="btn green btn-outline btn-circle" href="javascript:;"
                                       data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="tabela_categorias_documentos_nivel4_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="1" class="tool-action">
                                                <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                                            </a>
                                        </li>


                                    </ul>
                                </div>

                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                            </div>

                        </div>

                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_categorias_documentos_nivel4">
                            <thead>
                            <tr>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ID'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NOME'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_TABELA_NIVEL1'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_TABELA_NIVEL2'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_TABELA_NIVEL3'); ?> </th>
                                <th><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_ESTADO'); ?></th>
                                <th> </th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/documentos/tmpl/listcat4admin.js.php');
    echo ('</script>');
?>
