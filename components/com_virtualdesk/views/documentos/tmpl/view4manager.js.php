<?php
defined('_JEXEC') or die;
?>

var PortofolioHandle = function () {

    var initDocumentosFile = function (evt) {
        // init cubeportfolio
        jQuery('#vdDocumentosFile').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        });

    };

    var destroyDocumentosFile = function (evt) {
        // init cubeportfolio
        jQuery('#vdDocumentosFile').cubeportfolio('destroy');
    };


    return {
        //main function to initiate the module
        init: function () {
            initDocumentosFile();
        },
        initDocumentosFile    :  initDocumentosFile,
        destroyDocumentosFile  : destroyDocumentosFile

    };
}();

jQuery(document).ready(function() {

    PortofolioHandle.init();

});
