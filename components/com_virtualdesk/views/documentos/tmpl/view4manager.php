<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');
    JLoader::register('VirtualDeskSiteDocumentosFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/files.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('documentos');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'view4manager'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/documentos/tmpl/documentos.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputDocumento_Id = JFactory::getApplication()->input->getInt('documento_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteDocumentosHelper::getDocumentosDetail4Manager($getInputDocumento_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
        $ListFilesAlert = array();

        $objDocumentoFiles = new VirtualDeskSiteDocumentosFilesHelper();
        $ListFilesDoc = $objDocumentoFiles->getFileGuestLinkByRefId ($this->data->codigo);

    }

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
        return false;
    }

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_VIEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_NOVO' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">

                <div class="form-body">

                    <div class="portlet light">

                        <div class="col-md-8">

                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend>
                                            <h3><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_DADOS'); ?></h3>
                                        </legend>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half state">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_ESTADO' ); ?></div>
                                                <div class="value">
                                                    <span class="label <?php echo VirtualDeskSiteDocumentosHelper::getEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_TITLE' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nome, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL1' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->catLevel1, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL2' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->catLevel2, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL3' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->catLevel3, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL4' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->catLevel4, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php
                                        if(!empty($this->data->tags)){
                                            $explodeTags = explode(';;', $this->data->tags);
                                            for($i=0; $i<count($explodeTags); $i++){
                                                if($i==0){
                                                    $tags = '<span>' . $explodeTags[$i] . '</span>';
                                                } else {
                                                    $tags .= '<span>' . $explodeTags[$i] . '</span>';
                                                }
                                            }
                                            ?>
                                                <div class="form-group all tags">
                                                    <div class="row static-info ">
                                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_TAGS' ); ?></div>
                                                        <div class="value"> <?php echo $tags;?> </div>
                                                    </div>
                                                </div>
                                            <?php
                                        }
                                        ?>

                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_FILE'); ?></h3></legend>

                                        <div id="vdDocumentosFile" class="cbp">
                                            <?php
                                            if(!is_array($ListFilesDoc)) $ListFilesDoc = array();
                                            foreach ($ListFilesDoc as $rowFile) : ?>
                                                <div class="cbp-item identity link">
                                                    <?php echo $rowFile->desc; ?>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('documento_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/documentos/tmpl/view4manager.js.php');
    echo ('</script>');
?>