<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteDocumentosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/documentos/virtualdesksite_documentos.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('documentos');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('documentos', 'addnew4manager'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/documentos/tmpl/documentos.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');
    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteDocumentosHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.documentos.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;

    $obParam      = new VirtualDeskSiteParamsHelper();


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_NOVO' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=list4manager'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <form id="new-documento" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=documentos.create4manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                    <div class="form-body">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_NOVO'); ?></h3></legend>

                            <div class="form-group all">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_NOME'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required class="form-control" name="nome" id="nome" maxlength="400" value="<?php echo $nome; ?>"/>
                                </div>
                            </div>


                            <div class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL1'); ?><span class="required">*</span></label>
                                <?php $CatLevel1 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel1()?>
                                <div class="value col-md-12">
                                    <select name="catLevel1" value="<?php echo $catLevel1; ?>" required id="catLevel1" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($catLevel1)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($CatLevel1 as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['categoria']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $catLevel1; ?>"><?php echo VirtualDeskSiteDocumentosHelper::getCat4DocLevel1Select($catLevel1) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeCatLevel1 = VirtualDeskSiteDocumentosHelper::excludeCat4DocLevel1($catLevel1)?>
                                            <?php foreach($ExcludeCatLevel1 as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['categoria']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <?php

                            $ListaDeMenuMain = array();
                            if(!empty($catLevel1)) {
                                if( (int) $catLevel1 > 0) $CatLevel2 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel2($catLevel1);
                            }
                            ?>
                            <div id="blocoCategoriaNivel2" class="form-group half" style="float:right;">
                                <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL2' ); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="catLevel2" id="catLevel2" required value="<?php echo $catLevel2;?>"
                                        <?php
                                        if(empty($catLevel1)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($catLevel2)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($CatLevel2 as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $catLevel2; ?>"><?php echo VirtualDeskSiteDocumentosHelper::getCat4DocLevel2Select($catLevel2);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeCatLevel2 = VirtualDeskSiteDocumentosHelper::excludeCat4DocLevel2($catLevel1, $catLevel2);?>
                                            <?php foreach($ExcludeCatLevel2 as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>



                            <?php

                            $ListaDeMenuMain2 = array();
                            if(!empty($catLevel2)) {
                                if( (int) $catLevel2 > 0) $CatLevel3 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel3($catLevel2);
                            }
                            ?>
                            <div id="blocoCategoriaNivel3" class="form-group half">
                                <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL3' ); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="catLevel3" id="catLevel3" required value="<?php echo $catLevel3;?>"
                                        <?php
                                        if(empty($catLevel3)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($catLevel3)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($CatLevel3 as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $catLevel3; ?>"><?php echo VirtualDeskSiteDocumentosHelper::getCat4DocLevel3Select($catLevel3);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeCatLevel3 = VirtualDeskSiteDocumentosHelper::excludeCat4DocLevel3($catLevel2, $catLevel3);?>
                                            <?php foreach($ExcludeCatLevel3 as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>



                            <?php

                            $ListaDeMenuMain3 = array();
                            if(!empty($catLevel3)) {
                                if( (int) $catLevel3 > 0) $CatLevel4 = VirtualDeskSiteDocumentosHelper::getCat4DocLevel4($catLevel3);
                            }
                            ?>
                            <div id="blocoCategoriaNivel4" class="form-group half" style="float:right;">
                                <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_DOCUMENTOS_CATEGORIA_NIVEL4' ); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="catLevel4" id="catLevel4" required value="<?php echo $catLevel4;?>"
                                        <?php
                                        if(empty($catLevel4)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($catLevel4)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($CatLevel4 as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $catLevel4; ?>"><?php echo VirtualDeskSiteDocumentosHelper::getCat4DocLevel4Select($catLevel4);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeCatLevel4 = VirtualDeskSiteDocumentosHelper::excludeCat4DocLevel4($catLevel3, $catLevel4);?>
                                            <?php foreach($ExcludeCatLevel4 as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>


                            <div class="form-group half" id="uploadFieldDoc">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_DOC'); ?></label>
                                <div class="value col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_doc[]" id="fileupload_doc" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>


                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_TAGS'); ?></h3></legend>

                            <div class="form-group" id="documentTag">
                                <div class="mt-repeater">
                                    <div data-repeater-list="TagInput">
                                        <div data-repeater-item class="mt-repeater-item ">

                                            <div class="mt-repeater-input TagInput TagInput-new" data-provides="TagInput">

                                                <div class="form-group all" data-trigger="TagInput">
                                                    <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_TAG'); ?></div>
                                                    <div class="value col-md-12">
                                                        <input type="text" class="form-control" autocomplete="off" placeholder="<?php echo JText::_('COM_VIRTUALDESK_DOCUMENTOS_TAG_PLACEHOLDER'); ?>" name="tag" maxlength="500" value="<?php echo $tag; ?>"/>
                                                    </div>
                                                </div>

                                                <a href="javascript:;" class="btn btn-danger mt-repeater-delete TagInput-exists" data-repeater-delete data-dismiss="TagInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                            </div>

                                            <div class="mt-repeater-input">
                                            </div>

                                        </div>
                                    </div>

                                    <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                        <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                </button>
                                <a class="btn default"
                                   href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=documentos&layout=list4manager'); ?>"
                                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('documentos.create4manager',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                    <?php echo JHtml::_('form.token'); ?>

                </form>
            </div>

        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/documentos/tmpl/addnew4manager.js.php');
    echo ('</script>');
?>