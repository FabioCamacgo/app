<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteRgpdHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_rgpd.php');
JLoader::register('VirtualDeskSiteLogAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_logadmin.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('rgpd', 'listlogs4managers');
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

$obVDCrypt = new VirtualDeskSiteCryptHelper();
$setencrypt_forminputhidden = JComponentHelper::getParams('com_virtualdesk')->get('setencrypt_forminputhidden');



// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$labelseparator=': ';

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

// Rgpd - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/rgpd/tmpl/rgpd-comum.css');


?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput{display:block;}
    .fileinput-preview {display:block; max-height: 100%; }
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}
    .iconVDModified {padding-left: 15px; }

    .TipoOPFilterDropBox {float: right; padding-right: 50px; }
    .ModuloFilterDropBox {float: right; padding-right: 15px; }
    .PluginFilterDropBox {float: right; padding-right: 15px; }

    .popover {max-width: 350px;}

    div.blocoRgpdLogDetalhe {word-break: break-all;}

</style>

<div class="portlet light bordered rgpd">


    <div class="portlet-title">
        <div class="caption">
            <i class="icon-eyeglasses font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_RGPD_LISTALOGS' ); ?></span>
        </div>

        <div class="actions">

            <!-- Botões DataTables -->
            <div class="btn-group">
                <a class="btn green btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                    <i class="fa fa-share"></i>
                    <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right" id="tabela_rgpd_listalogs_tools">
                    <li>
                        <a href="javascript:;" data-action="0" class="tool-action">
                            <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="1" class="tool-action">
                            <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="2" class="tool-action">
                            <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="3" class="tool-action">
                            <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?></a>
                    </li>


                </ul>
            </div>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>

    </div>

    <div class="portlet-body ">

        <div class="tabbable-line">
            <ul class="nav nav-tabs ">

                <li class="active">
                    <a href="#tab_Rgpd_ListaLogs" data-toggle="tab">
                        <h4>
                            <i class="fa fa-search"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_RGPD_LISTALOGS'); ?>

                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=rgpd&layout=listdados4manager'); ?>" >
                        <h4>
                            <i class="fa fa-search-plus"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_RGPD_LISTADADOS'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=logadmin&layout=list_eventlog#tabEventLogs'); ?>" >
                        <h4>
                            <i class="fa fa-th-list"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_TAB_EVENTLOGS'); ?>
                        </h4>
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_Rgpd_ListaLogs">
                    <table class="table table-striped table-bordered table-hover order-column" id="tabela_rgpd_listalogs">
                        <thead>
                        <tr>
                            <th style="max-width: 100px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_CREATED'); ?></th>
                            <th style="max-width: 80px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_LOGIN'); ?></th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?></th>
                            <th style="max-width: 50px;"><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_TIPOOP'); ?> </th>
                            <th style="max-width: 40px;"><?php echo JText::_('COM_VIRTUALDESK_RGPD_CMP_REFID'); ?> </th>
                            <th style="max-width: 150px;"><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_REF'); ?> </th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_TITLE'); ?> </th>
                            <th ><?php echo JText::_('COM_VIRTUALDESK_RGPD_DESCRICAO'); ?> </th>
                            <th style="max-width: 80px;"><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_MODULOPLUGIN'); ?> </th>
                            <th style="max-width: 50px;"><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_IP'); ?> </th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>

        </div>

    </div>


    <div class="modal fade bs-modal-lg" id="RgpdLogDetalheModal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_DETAIL_TITLE' ); ?></h4>
                </div>
                <div class="modal-body blocoRgpdLogDetalhe"
                     data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=rgpd.getRgpdLogDetalhe4ManagerByAjax'); ?>" >
                <div class="form-group">


                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_ID' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheID"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_CREATED' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheCreatedFull"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_LOGIN' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheULogin"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_NOME' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheUNome"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_CMP_REFID' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheRefId"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_LOGADMIN_CMP_REF' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheRef"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_TITULO' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheTitle"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_LOGADMIN_CMP_TIPOOP' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheTipoOP"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_DESCRICAO' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheDesc"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_CMP_MODULO' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheModulo"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_CMP_PLUGIN' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalhePlugin"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_LOGADMIN_CMP_IP' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheIP"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_CMP_REFERER' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheReferer"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 static-info">
                            <div class="form-group">
                                <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_RGPD_CMP_REQUEST' ).$labelseparator; ?></label>
                                <span class="value ValorEstadoAtualStatic RgpdLogDetalheRequest"></span>
                            </div>
                        </div>
                    </div>



                </div>

                    <div class="form-group blocoIconsMsgAviso">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span> <i class="fa"></i> </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="blocoIconsMsgAviso_Texto"></span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                    <span> <i class="fa"></i> </span>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>




</div>





<?php

echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/rgpd/tmpl/listlogs4manager.js.php');
echo ('</script>');

?>


