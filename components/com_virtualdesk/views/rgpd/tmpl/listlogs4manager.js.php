<?php
defined('_JEXEC') or die;

$objTipoOPOptions  = VirtualDeskSiteLogAdminHelper::getTipoOPAllOptions();
$TipoOPOptionsHTML = '';
foreach($objTipoOPOptions as $rowC) {
    $TipoOPOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

$objModuloOptions  = VirtualDeskSiteLogAdminHelper::getModuloRGPDAllOptions(true);
$ModuloOptionsHTML = '';
foreach($objModuloOptions as $rowC) {
    $ModuloOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

$objPluginOptions  = VirtualDeskSiteLogAdminHelper::getPluginRGPDAllOptions(true);
$PluginOptionsHTML = '';
foreach($objPluginOptions as $rowC) {
    $PluginOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

?>
var TableDatatablesManaged = function () {

    var initTableRgpdListaLogs = function () {

        var table = jQuery('#tabela_rgpd_listalogs');
        var tableTools = jQuery('#tabela_rgpd_listalogs_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"lf <"PluginFilterDropBox"> <"ModuloFilterDropBox"> <"TipoOPFilterDropBox"> rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 50,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                        retVal = data + '  ';
                        retVal += '<a href="javascript:;" style="float:right" class="btn btn-sm grey-salsa btn-outline popovers" data-toggle="popover" data-placement="top" data-trigger="hover" ';
                        retVal += ' data-content="<?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ID');?>: ' + row[11] + '">';
                        retVal += '<i class="fa fa-info"></i></a>';
                        return (retVal);
                    }
                },

                {
                    "targets": 10,
                    "data": 10,
                    "render": function ( data, type, row, meta) {
                        var retVal = '<button class="btn btn-circle btn-outline btAbrirModal btRgpdLogDetalheModalOpen" title="<?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_VERHIST' ); ?>" data-vd-rgpd-id="'+row[11]+'" type="button">';
                        retVal += '   <i class="fa fa-list-alt"></i>   </button>';

                        return (retVal);
                    }
                },
                {
                    "targets": 11,
                    "visible":false
                },
                {
                    "targets": 12,
                    "visible":false
                },
                {
                    "targets": 13,
                    "visible":false
                },
                {
                    "targets": 14,
                    "visible":false
                },

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=rgpd.getRgpdListLogs4ManagerByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
                ButtonHandle.handleButtonRgpdLogDetalheModal();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3,5,6,7,8,9] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3,5,6,7,8,9] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3,5,6,7,8,9] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3,5,6,7,8,9] } }

            ],

            initComplete: function () {

                // Filtrar Plugin
                this.api().column(12).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_PLUGIN"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($PluginOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.PluginFilterDropBox').empty() )
                    jQuery('.PluginFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar Módulo
                this.api().column(13).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_MODULO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($ModuloOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.ModuloFilterDropBox').empty() )
                    jQuery('.ModuloFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar TipoOP
                this.api().column(14).every(function(){
                    var column = this;
                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_LOGADMIN_FILTRARPOR").JText::_("COM_VIRTUALDESK_LOGADMIN_CMP_TIPOOP"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($TipoOPOptionsHTML); ?>';
                    SelectText += '</select></label>';
                    var select = jQuery(SelectText).appendTo(jQuery('.TipoOPFilterDropBox').empty() )
                    jQuery('.TipoOPFilterDropBox select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });


            }

        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });

        //var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');

    }

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableRgpdListaLogs();

        }
    };
}();



var ButtonHandle = function () {



    var handleButtonRgpdLogDetalheModal = function (evt) {

        jQuery(".btRgpdLogDetalheModalOpen").on('click',function(evt,data){
            let rgpdID       = jQuery(this).data('vd-rgpd-id');
            let elModal        = jQuery("#RgpdLogDetalheModal");
            let elBlocoOptions = elModal.find('div.blocoRgpdLogDetalhe');
            let elModalContent = elBlocoOptions.closest('div.modal-content');
            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');

            // Carrega dados e coloca dos valores nos campos
            vdAjaxCall.getRgpdLogDetalheData(jQuery(this), urlGetContent, rgpdID, elBlocoOptions, elModalContent);

            elModal.modal('show');
        });
    };


    return {
        //main function to initiate the module
        init: function () {

        },
        handleButtonRgpdLogDetalheModal : handleButtonRgpdLogDetalheModal,
    };

}();



var ModalHandle = function () {

    var handleModalRgpdLogDetalhe = function (evt) {
        jQuery("#RgpdLogDetalheModal").modal({
            show: false,
            keyboard: true
        })
    };

    return {
        //main function to initiate the module
        init: function () {
            handleModalRgpdLogDetalhe();

        }
    };
}();


var vdAjaxCall = function () {


// Carrega os campos por ajax de uma tarefa de detalhe e coloca na janela modal
    var getRgpdLogDetalheData = function (el, vd_url_getcontent, vd_rgpd_id, elBlocoOptions, elModalContent) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassError   = 'fa-times-circle fa-4x text-danger';

        elBlocoOptions.find('span.RgpdLogDetalheCreatedFull').html('');
        elBlocoOptions.find('span.RgpdLogDetalheULogin').html('');
        elBlocoOptions.find('span.RgpdLogDetalheUNome').html('');
        elBlocoOptions.find('span.RgpdLogDetalheTitle').html('');
        elBlocoOptions.find('span.RgpdLogDetalheTipoOP').html('');
        elBlocoOptions.find('span.RgpdLogDetalheDesc').html('');
        elBlocoOptions.find('span.RgpdLogDetalheModulo').html('');
        elBlocoOptions.find('span.RgpdLogDetalhePlugin').html('');
        elBlocoOptions.find('span.RgpdLogDetalheIP').html('');
        elBlocoOptions.find('span.RgpdLogDetalheReferer').html('');
        elBlocoOptions.find('span.RgpdLogDetalheRequest').html('');

        let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            data: '&<?php echo $obVDCrypt->setIdInputNameEncrypt('rgpd_id',$setencrypt_forminputhidden); ?>=' + vd_rgpd_id,
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent, vd_rgpd_id:vd_rgpd_id, elBlocoOptions:elBlocoOptions},
            success: function(data){
                let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
                vdClosestI.removeClass(setClassSpinner).removeClass(setClassError);

                setTimeout(
                    function()
                    {
                        //let elNome      = elBlocoOptions.find('span.RgpdLogDetalheTitle');
                        //let elDesc      = elBlocoOptions.find('span.RgpdLogDetalheDesc');
                        var response = JSON.parse(data);
                        //elNome.html(response.nome);
                        //elDesc.html(response.descricao);

                        elBlocoOptions.find('span.RgpdLogDetalheID').html(response.id);
                        elBlocoOptions.find('span.RgpdLogDetalheCreatedFull').html(response.createdFull);
                        elBlocoOptions.find('span.RgpdLogDetalheULogin').html(response.ulogin);
                        elBlocoOptions.find('span.RgpdLogDetalheUNome').html(response.unome);
                        elBlocoOptions.find('span.RgpdLogDetalheRefId').html(response.ref_id);
                        elBlocoOptions.find('span.RgpdLogDetalheRef').html(response.ref);
                        elBlocoOptions.find('span.RgpdLogDetalheTitle').html(response.title);
                        elBlocoOptions.find('span.RgpdLogDetalheTipoOP').html(response.tipo_op);
                        elBlocoOptions.find('span.RgpdLogDetalheDesc').html(response.descricao);
                        elBlocoOptions.find('span.RgpdLogDetalheModulo').html(response.modulo);
                        elBlocoOptions.find('span.RgpdLogDetalhePlugin').html(response.plugin);
                        elBlocoOptions.find('span.RgpdLogDetalheIP').html(response.ip);
                        elBlocoOptions.find('span.RgpdLogDetalheReferer').html(response.referer);
                        elBlocoOptions.find('span.RgpdLogDetalheRequest').html(response.request);

                    }, 250);

            },
            error: function(error){
                let vdClosestI = elModalContent.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };


    return {
        getRgpdLogDetalheData   :  getRgpdLogDetalheData,

    };

}();




jQuery(document).ready(function() {

    TableDatatablesManaged.init();

});