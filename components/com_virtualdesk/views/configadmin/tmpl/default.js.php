<?php
defined('_JEXEC') or die;

?>

var TableDatatablesManaged = function () {

    var initTablePluginList = function () {

        var table = jQuery('#tabela_lista_pluginlist');
        var tableTools = jQuery('#tabela_lista_pluginlist_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                // Enabled
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        //ret = '<i class="fa fa-close font-red"></i>';
                        let currState = '';
                        if(data=='1') {
                            //ret = '<i class="fa fa-check font-green-jungle"></i>';
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledPlugin_' + meta.row + '"' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-pluginenable="' + row[7] + '" ';
                        ret += ' data-vd-url-checkpluginenable="' + row[8] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        //console.log(data);
                        return (ret);
                    }
                },
                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=configadmin.getPluginListByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

                //console.log('drawCallback Plugin...');
                //console.log('tableCount='+this.api().data().rows().count());

                UISwitch.initSwitchPlugin();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }

        });
        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
        var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');
    }

    var initTableParamsList = function () {

        var table = jQuery('#tabela_lista_paramslist');
        var tableTools = jQuery('#tabela_lista_paramslist_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);
                        return ('<a href="' + row[8] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[8] + '">' + data + '</a>');
                    }
                },
                // Enabled
                {
                    "targets": 7,
                    "data": 7,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        //ret = '<i class="fa fa-close font-red"></i>';
                        let currState = '';
                        if(data=='1') {
                            //ret = '<i class="fa fa-check font-green-jungle"></i>';
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledParam_' + meta.row + '" ' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-paramenable="' + row[9] + '" ';
                        ret += ' data-vd-url-checkparamenable="' + row[10] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        //console.log(data);
                        return (ret);
                    }
                },
                {
                    "targets": 8,
                    "data": 8,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=configadmin.getParamsListByAjax",
            "drawCallback": function( settings ) {
                //tabvle.find('[data-toggle="popover"]').popover();
                //console.log('drawCallback Params...');
                //console.log('tableCount='+this.api().data().rows().count());

                UISwitch.initSwitchParam();

            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }

        });
        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = jQuery(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
        //var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');
    }

    var initTableParamsTipoList = function () {

        var table = jQuery('#tabela_lista_paramstipolist');
        var tableTools = jQuery('#tabela_lista_paramstipolist_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);
                        return ('<a href="' + row[3] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[3] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=configadmin.getParamsTipoListByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }

        });
        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
        //var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');
    }

    var initTableParamsGrupoList = function () {

        var table = jQuery('#tabela_lista_paramsgrupolist');
        var tableTools = jQuery('#tabela_lista_paramsgrupolist_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);
                        return ('<a href="' + row[3] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[3] + '">' + data + '</a>');
                    }
                },


                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=configadmin.getParamsGrupoListByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();


            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }

        });
        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
        //var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');
    }

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTablePluginList();

            initTableParamsList();

            initTableParamsTipoList();

            initTableParamsGrupoList();

        }

    };
}();


var UISwitch = function () {

    var initSwitchPlugin =  function (evt) {
        jQuery("input[name^='enabledPlugin_']").bootstrapSwitch();
    };


    var initSwitchParam =  function (evt) {
        jQuery("input[name^='enabledParam_']").bootstrapSwitch();
    };


    var handleSwitchPlugin = function (evt) {

        jQuery("input[name^='enabledPlugin_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);
            //console.log('clicked ' + jQuery(this).attr('name') );
            let vd_url_pluginenable = jQuery(this).data('vd-url-pluginenable');
            let vd_url_checkpluginenable = jQuery(this).data('vd-url-checkpluginenable');

            let enableVal = 0;
            if(data===true) enableVal= 1;

           vdAjaxCall.setPluginEnable(jQuery(this), vd_url_pluginenable, vd_url_checkpluginenable , enableVal);
        });
    };


    var handleSwitchParam = function (evt) {

        jQuery("input[name^='enabledParam_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);
            //console.log('clicked ' + jQuery(this).attr('name') );
            let vd_url_paramenable = jQuery(this).data('vd-url-paramenable');
            let vd_url_checkparamenable = jQuery(this).data('vd-url-checkparamenable');

            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setParamEnable(jQuery(this), vd_url_paramenable, vd_url_checkparamenable , enableVal);
        });

    };


    return {
        //main function to initiate the module
        init: function () {
        },
        initSwitchPlugin: function () {
            initSwitchPlugin();
            handleSwitchPlugin();
        },
        initSwitchParam: function () {
            initSwitchParam();
            handleSwitchParam();
        }

    };

}();


var vdAjaxCall = function () {

    var setPluginEnable = function (el, vd_url_pluginenable, vd_url_checkpluginenable, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_pluginenable = vd_url_pluginenable + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_pluginenable,
            type: "POST",
            indexValue: {el:el, vd_url_checkpluginenable:vd_url_checkpluginenable},
            success: function(data){
                //console.log('SUCESSO vd_url_pluginenable...');
                //console.log('data='+data);
                //console.log('this.indexValue.el='+ this.indexValue.el);
                //console.log('this.indexValue.vd_url_checkpluginenable='+ this.indexValue.vd_url_checkpluginenable);

                vdAjaxCall.getPluginEnable(this.indexValue.el, this.indexValue.vd_url_checkpluginenable);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var setParamEnable = function (el, vd_url_paramenable, vd_url_checkparamenable, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_paramenable = vd_url_paramenable + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_paramenable,
            type: "POST",
            indexValue: {el:el, vd_url_checkparamenable:vd_url_checkparamenable},
            success: function(data){
                //console.log('SUCESSO vd_url_paramenable...');
                //console.log('data='+data);
                //console.log('this.indexValue.el='+ this.indexValue.el);
                //console.log('this.indexValue.vd_url_checkparamenable='+ this.indexValue.vd_url_checkparamenable);

                vdAjaxCall.getParamEnable(this.indexValue.el, this.indexValue.vd_url_checkparamenable);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };



    var getPluginEnable = function (el, vd_user_checkpluginenable) {

        jQuery.ajax({
            url: vd_user_checkpluginenable,
            type: "POST",
            indexValue: {el:el},
            success: function(data){
                ////console.log('SUCESSO vd_user_checkpluginenable...');
                //console.log('this.indexValue.el='+ this.indexValue.el);

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                //console.log('data='+data);
                //console.log('currState='+currState);

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };


    var getParamEnable = function (el, vd_user_checkparamenable) {

        jQuery.ajax({
            url: vd_user_checkparamenable,
            type: "POST",
            indexValue: {el:el},
            success: function(data){
                //console.log('SUCESSO vd_user_checkparamenable...');
                //console.log('this.indexValue.el='+ this.indexValue.el);

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                //console.log('data='+data);
                //console.log('currState='+currState);

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    return {

        setPluginEnable   : setPluginEnable,
        getPluginEnable   : getPluginEnable,
        setParamEnable    : setParamEnable,
        getParamEnable    : getParamEnable
    };

}();


jQuery(document).ready(function() {

   TableDatatablesManaged.init();

  // UISwitch.init();
});