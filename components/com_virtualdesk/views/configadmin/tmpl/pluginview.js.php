<?php
defined('_JEXEC') or die;
?>

var TableDatatablesManaged = function () {

    var initTablePluginLayoutList = function () {

        var table = jQuery('#tabela_lista_pluginlayoutlist');
        var tableTools = jQuery('#tabela_lista_pluginlayoutlist_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[6] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=configadmin.getPluginLayoutListByAjax&configadmin_plugin_id=<?php echo($getInputPlugin_id); ?>",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }

        });
        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
        //var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');
    }

    var initTablePluginLayoutAtivosList = function () {

        var table = jQuery('#tabela_lista_pluginlayoutativoslist');
        var tableTools = jQuery('#tabela_lista_pluginlayoutativoslist_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var select_i = '<select class="form-control select2 select2-nosearch select2-hidden-accessible js-data-vd-ajax Select2TipoLayout " data-vd-idtipolayout="' + row[0] + '" id="stl_' + row[0] +'">';
                        if(data == null || data === '') {
                            select_i += '<option > </option>';
                        }
                        else {
                            select_i += '<option value="' + data +'">' + data + '</option>';
                        }
                        var select_f = '</select> <span class="vd-sel2-spinner"><i class="fa"></i></span>';
                        return (select_i + select_f);
                    }
                }
                ,
                // Enabled
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        let currState = '';
                        if(data=='1') {
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledLayoutAtivo_' + meta.row + '"' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-layoutativoenable="' + row[7] + '" ';
                        ret += ' data-vd-url-checklayoutativoenable="' + row[8] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        //console.log(data);
                        return (ret);
                    }
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=configadmin.getPluginLayoutAtivosListByAjax&configadmin_plugin_id=<?php echo($getInputPlugin_id); ?>",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

                ComponentsSelect2.init();

                UISwitch.initSwitchLayoutAtivo();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }

        });
        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
        //var tableWrapper = jQuery('#tabela_lista_useractivationlogs_wrapper');
    }

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTablePluginLayoutList();

            initTablePluginLayoutAtivosList();
        }
    };
}();


/*
* Inicialização do Select 2
*/
var ComponentsSelect2 = function() {

    var SelLayoutAtivo = function() {

        var Select2TipoLayout = jQuery(".Select2TipoLayout");

        var idtipolayout =

        Select2TipoLayout.select2({
            width: null,
            allowClear: false,
            async: false,
            ajax: {
                url: "index.php?option=com_virtualdesk&task=configadmin.getPluginLayoutAllListByAjax&configadmin_plugin_id=<?php echo($getInputPlugin_id); ?>",
                dataType: 'json',
                context: this,
                data: function () {
                    var vdidtipolayout =jQuery(this).data("vd-idtipolayout");
                    return {
                        configadmin_tipolayout_id : vdidtipolayout
                    };
                },
                delay: 250,
                processResults: function(data) {

                    console.log(jQuery(this));

                    var el = jQuery(this)[0].$element;
                    el.data('select2').$dropdown.css('visibility','visible');
                    let vdClosestI = el.parent().find('span.vd-sel2-spinner > i.fa');
                    vdClosestI.removeClass("fa-spin fa-spinner fa-2x text-muted");

                    return {
                        results: data.results
                    };
                },
                error: function (jqXHR, status, error) {
                    alert('Error: ' + error);
                    return { results: [] }; // Return dataset to load after error
                },
                cache: false
            },
            minimumResultsForSearch: -1
            //templateResult: setSel2Result,
            //templateSelection: setSel2Result
        });

        Select2TipoLayout.on('select2:opening', function (e) {
            jQuery(e.currentTarget).data('select2').$dropdown.css('visibility','hidden');
            let vdClosestI = jQuery(this).parent().find('span.vd-sel2-spinner > i.fa');
            vdClosestI.removeClass('fa-check fa-times-circle text-success text-danger').addClass("fa-spin fa-spinner fa-2x text-muted");;

        });

        Select2TipoLayout.on('select2:select', function (e) {
            var data = e.params.data;
            var idtipolayout = jQuery(this).data("vd-idtipolayout")
            var vd_url_pluginlayoutupdate  = "?option=com_virtualdesk&task=configadmin.setPluginLayoutAtivoByAjax&configadmin_plugin_id=<?php echo($getInputPlugin_id); ?>&configadmin_layout_id=" + data.id + "&configadmin_tipolayout_id=" + idtipolayout;
            var vd_url_pluginlayoutpathval = "?option=com_virtualdesk&task=configadmin.getPluginLayoutPathValByAjax&configadmin_plugin_id=<?php echo($getInputPlugin_id); ?>&configadmin_layout_id=" + data.id + "&configadmin_tipolayout_id=" + idtipolayout;
            vdAjaxCall.setPluginLayoutAtivo(jQuery(this), vd_url_pluginlayoutupdate, vd_url_pluginlayoutpathval)
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            SelLayoutAtivo();
        }
    };

}();


var UISwitch = function () {

    var initSwitchLayoutAtivo =  function (evt) {
        jQuery("input[name^='enabledLayoutAtivo_']").bootstrapSwitch();
    };

    var handleSwitchLayoutAtivo = function (evt) {

        jQuery("input[name^='enabledLayoutAtivo_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);
            //console.log('clicked ' + jQuery(this).attr('name') );
            let vd_url_layoutativoenable = jQuery(this).data('vd-url-layoutativoenable');
            let vd_url_checklayoutativoenable = jQuery(this).data('vd-url-checklayoutativoenable');

            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setLayoutAtivoEnable(jQuery(this), vd_url_layoutativoenable, vd_url_checklayoutativoenable , enableVal);
        });
    };


    return {
        //main function to initiate the module
        init: function () {
        },
        initSwitchLayoutAtivo: function () {
            initSwitchLayoutAtivo();
            handleSwitchLayoutAtivo();
        }

    };

}();


var vdAjaxCall = function () {

    var setPluginLayoutAtivo = function (el, vd_url_pluginlayoutupdate, vd_url_pluginlayoutpathval) {
        let vdClosestI = el.parent().find('span.vd-sel2-spinner > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-2x text-muted");

        jQuery.ajax({
            url: vd_url_pluginlayoutupdate,
            type: "POST",
            indexValue: {el:el, vd_url_pluginlayoutpathval:vd_url_pluginlayoutpathval},
            success: function(data){
                let vdClosestI = el.parent().find('span.vd-sel2-spinner > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-2x text-muted').addClass("fa-check fa-2x text-success");

                vdAjaxCall.setPluginLayoutPathVal(el, vd_url_pluginlayoutpathval);

            },
            error: function(error){
                let vdClosestI = el.parent().find('span.vd-sel2-spinner > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-2x text-muted').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };

    var setPluginLayoutPathVal = function (el, vd_url_pluginlayoutpathval) {
        jQuery.ajax({
            url: vd_url_pluginlayoutpathval,
            type: "GET",
            dataType: "json",
            indexValue: {el:el},
            success: function(data){
                console.log(data);
                let vdClosestI =  el.closest('td').next();
                vdClosestI.fadeOut().text(data).fadeIn();

            },
            error: function(error){

            }
        });
    };

    var setLayoutAtivoEnable = function (el, vd_url_layoutativoenable, vd_url_checklayoutativoenable, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_layoutativoenable = vd_url_layoutativoenable + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_layoutativoenable,
            type: "POST",
            indexValue: {el:el, vd_url_checklayoutativoenable:vd_url_checklayoutativoenable},
            success: function(data){
                //console.log('SUCESSO vd_url_pluginenable...');
                //console.log('data='+data);
                //console.log('this.indexValue.el='+ this.indexValue.el);
                //console.log('this.indexValue.vd_url_checkpluginenable='+ this.indexValue.vd_url_checkpluginenable);

                vdAjaxCall.getLayoutAtivoEnable(this.indexValue.el, this.indexValue.vd_url_checklayoutativoenable);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var getLayoutAtivoEnable = function (el, vd_url_checklayoutativoenable) {

        jQuery.ajax({
            url: vd_url_checklayoutativoenable,
            type: "POST",
            indexValue: {el:el},
            success: function(data){
                ////console.log('SUCESSO vd_user_checkpluginenable...');
                //console.log('this.indexValue.el='+ this.indexValue.el);

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                //console.log('data='+data);
                //console.log('currState='+currState);

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    return {
        setPluginLayoutAtivo   : setPluginLayoutAtivo,
        setPluginLayoutPathVal : setPluginLayoutPathVal,
        setLayoutAtivoEnable   : setLayoutAtivoEnable,
        getLayoutAtivoEnable   : getLayoutAtivoEnable

    };

}();


jQuery(document).ready(function() {

   TableDatatablesManaged.init();

});