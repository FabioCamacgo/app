<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteTicketsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tickets.php');
JLoader::register('VirtualDeskSiteTicketsFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tickets_files.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkDetailReadAccess('tickets');
if( $vbHasAccess===false ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = '';
//$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

// Tickets - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/tickets/tmpl/tickets-comum.css');

// Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');

$getInputTickets_Id = JFactory::getApplication()->input->getInt('tickets_id');

// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSiteTicketsHelper::getTicketsView4UserDetail($getInputTickets_Id);

// Carrega de ficheiros associados
if(!empty($this->data->codigo)) {
    $objTicketsFiles  = new VirtualDeskSiteTicketsFilesHelper();
    $ListFilesTickets = $objTicketsFiles->getFileGuestLinkByRefId ($this->data->codigo);
}

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

// Dados vazios...
if(empty($this->data)) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
    return false;
}


?>

<div class="portlet light bordered form-fit">
   <div class="portlet-title">

        <div class="caption">
            <i class="icon-calendar  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=tickets'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=tickets&layout=edit4user&vdcleanstate=1&tickets_id=' . $this->escape($this->data->tickets_id)); ?>" class="btn btn-circle btn-outline green vdBotaoEditar">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=tickets&layout=addnew4user&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_ADDNEW' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->

    </div>


    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">
            <div class="form-body">

                <div class="portlet light">

                    <div class="row">

                        <div class="col-md-8">

                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_TICKETS_DADOSDETALHE'); ?></div></div>
                                <div class="portlet-body">

                                    <div class="col-md-6">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_LISTA_REFERENCIA' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_LISTA_ESTADO' ); ?></div>
                                            <div class="value"> <span class="label <?php echo VirtualDeskSiteTicketsHelper::getTicketsEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_ASSUNTO_LABEL' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->assunto, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row static-info">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_DESCRICAO_LABEL' ); ?></div>
                                            <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao); ?></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row static-info">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_DEPARTAMENTO_LABEL' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->departamento, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="row static-info">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_OBSERVACOES' ); ?></div>
                                            <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->observacoes); ?></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="row static-info">

                                        </div>
                                    </div>


                                    <div class="row static-info">
                                        <div class="col-md-12 ">
                                            <h6>
                                                <?php
                                                echo JText::_( 'COM_VIRTUALDESK_TICKETS_LISTA_DATACRIACAO' ).$labelseparator;
                                                echo htmlentities( $this->data->data_criacao, ENT_QUOTES, 'UTF-8');
                                                if($this->data->data_criacao != $this->data->data_alteracao) echo ' , ' . JText::_( 'COM_VIRTUALDESK_TICKETS_LISTA_DATAALTERACAO' ) . ' ' . htmlentities( $this->data->data_alteracao, ENT_QUOTES, 'UTF-8');
                                                ?>
                                            </h6>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_TICKETS_DOCS'); ?></div></div>
                                <div class="portlet-body">
                                    <div class="form-group" id="uploadField">

                                        <div class="col-md-12">

                                            <div class="file-loading">
                                                <input type="file" name="fileupload" id="fileupload" >
                                            </div>
                                            <div id="errorBlock" class="help-block"></div>
                                        </div>
                                        <input type="hidden" name="vdFileUpChanged" value="0">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn green vdBotaoEditar" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=tickets&layout=edit4user&tickets_id=' . $this->escape($this->data->tickets_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=tickets'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('tickets_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->tickets_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('tickets_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('tickets_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

        </form>
    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/tickets/tmpl/view4user.js.php');
echo ('</script>');
?>