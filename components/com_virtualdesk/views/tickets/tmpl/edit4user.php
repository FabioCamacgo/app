<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteTicketsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tickets.php');
JLoader::register('VirtualDeskSiteTicketsFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tickets_files.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('tickets');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('tickets', 'edit4users'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.min.css');


//Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');
$getInputTickets_Id = JFactory::getApplication()->input->getInt('tickets_id');

// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSiteTicketsHelper::getTicketsView4UserDetail($getInputTickets_Id);
if( empty($this->data) ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
    return false;
}


// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteTicketsHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4user.tickets.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


$obParam      = new VirtualDeskSiteParamsHelper();

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_LISTA' ); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_TICKETS_EDIT' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=tickets'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            <?php
            // Objecto inicializado com as mensagens de tickets já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
            ?>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-tickets"
                  action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tickets.update4user'); ?>" method="post"
                  class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                  role="form">

                <div class="form-body">

                    <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_TICKETS_INFO'); ?></h3></legend>

                    <!--   ASSUNTO   -->
                    <div class="form-group assunto">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_TICKETS_ASSUNTO_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" required class="form-control" name="assunto" id="assunto" maxlength="250" value="<?php echo $this->data->assunto; ?>"/>
                        </div>
                    </div>

                    <!--   DESCRIÇAO   -->
                    <div class="form-group" id="desc">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_TICKETS_DESCRICAO_LABEL'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <textarea  class="form-control wysihtml5" rows="6" required name="descricao" id="descricao" maxlength="1000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao); ?></textarea>
                        </div>
                    </div>

                    <!--   DEPARTAMENTO   -->
                    <div class="form-group" id="departamento">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_TICKETS_DEPARTAMENTO_LABEL'); ?><span class="required">*</span></label>
                        <?php $departamentos = VirtualDeskSiteTicketsHelper::getDepartamento()?>
                        <div class="col-md-9">
                            <select name="departamento" required id="departamento" class="form-control select select2 select2-search" tabindex="-1" >
                                <option value=""></option>
                                <?php foreach($departamentos as $rowDep) : ?>
                                    <option value="<?php echo $rowDep['id']; ?>"
                                        <?php if((int)$this->data->id_departamento == (int)$rowDep['id']) echo('selected'); ?>
                                    ><?php echo $rowDep['departamento']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_LABEL'); ?></label>
                        <div class="col-md-9">
                            <textarea  class="form-control wysihtml5" rows="6" name="observacoes"  id="observacoes" maxlength="1000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->observacoes); ?></textarea>
                        </div>
                    </div>


                    <!--   Upload Imagens  -->
                    <div class="form-group" id="uploadField">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_TICKETS_DOCS'); ?></label>
                        <div class="col-md-9">
                            <div class="file-loading">
                                <input type="file" name="fileupload[]" id="fileupload" multiple>
                            </div>
                            <div id="errorBlock" class="help-block"></div>
                        </div>
                    </div>
                    <input type="hidden" name="vdFileUpChanged" value="0">

                </div>


                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=tickets'); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('tickets_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->tickets_id) ,$setencrypt_forminputhidden); ?>"/>


                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('tickets.update4user',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4user',$setencrypt_forminputhidden); ?>"/>


                <?php echo JHtml::_('form.token'); ?>

            </form>









        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/tickets/tmpl/edit4user.js.php');
echo ('</script>');
?>