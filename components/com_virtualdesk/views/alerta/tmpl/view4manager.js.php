<?php
defined('_JEXEC') or die;
?>
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = jQuery('#tabela_alerta_historico_resumo');
        //var tableTools = jQuery('#tabela_alerta_historico_resumo');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": 'rt',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                    //"targets": [2]
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {

                        var retVal =  row[11] + ' ';
                        if(row[7] != 1 )  {

                            retVal += '  <i class="fa fa-user font-blue-madison"></i>';
                            retVal += '  <i class="fa fa-angle-right font-blue-madison"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        else {

                            retVal += '  <i class="fa fa-users font-green-steel"></i>';
                            retVal += '  <i class="fa fa-angle-right font-green-steel"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                        switch(row[10]) {
                            case '1':
                                retVal += ' <i class="icon-speech font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '2':
                                retVal += ' <i class="icon-info font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '3':
                                retVal += ' <i class="icon-flag font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            default:
                                retVal += data;
                                break;
                        }
                        return (retVal);
                    }
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=alerta.getAlertaHistoricoList4ManagerByAjax&<?php echo $obVDCrypt->setIdInputNameEncrypt('alerta_id',$setencrypt_forminputhidden); ?>=<?php echo $obVDCrypt->setIdInputValueEncrypt($getInputAlerta_Id,$setencrypt_forminputhidden); ?>",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }
            ]

        });

    }

    var ReloadTable1 = function () {
        var table = jQuery('#tabela_alerta_historico_resumo');
        table.DataTable().ajax.reload();
    }

    var ReloadTableFull = function () {
        var table = jQuery('#tabela_alerta_historico_full');
        table.DataTable().ajax.reload();
    }

    var initTableHistFull = function () {

        var table = jQuery('#tabela_alerta_historico_full');
        //var tableTools = jQuery('#tabela_alerta_historico_resumo');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": 'lfrtip',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                    //"targets": [2]
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {

                        var retVal =  row[11] + ' ';
                        if(row[7] != 1 ) {
                            retVal += '<i class="fa fa-user font-blue-madison"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';

                        switch(row[10]) {
                            case '1':
                                retVal += ' <i class="icon-speech font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '2':
                                retVal += ' <i class="icon-info font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '3':
                                retVal += ' <i class="icon-flag font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            default:
                                retVal += data;
                                break;
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {

                        var retVal = '';
                        if(row[7] == 1 ) {
                            retVal += '<i class="fa fa-users font-blue-madison"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        return (retVal);
                    }
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=alerta.getAlertaHistoricoList4ManagerByAjax&<?php echo $obVDCrypt->setIdInputNameEncrypt('alerta_id',$setencrypt_forminputhidden); ?>=<?php echo $obVDCrypt->setIdInputValueEncrypt($getInputAlerta_Id,$setencrypt_forminputhidden); ?>",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ]

        });

        // handle datatable custom tools
        /*tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });*/

        // var tableWrapper = jQuery('#tabela_lista_alerta_wrapper');
    }

    var initTableTarefasFull = function () {

        var table = jQuery('#tabela_alerta_tarefas_full');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": 'lfrtip',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left"
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        switch (row[9])
                        {   case '1':
                                defCss = '<?php echo VirtualDeskSiteTarefaHelper::getTarefaEstadoCSS (1);?>';
                                break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSiteTarefaHelper::getTarefaEstadoCSS (2);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        var retVal = '<button class="btn btn-circle btn-outline green  btAbrirModal btTarefaAlterarEstadoModalOpen" title="<?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_CONCLUIR' ); ?>" data-vd-newestado-id="2" data-vd-tarefa-id="'+row[8]+'" type="button">';
                        retVal += '   <i class="fa fa-check"></i>   </button>';

                        retVal += '<button class="btn btn-circle btn-outline red  btAbrirModal btTarefaAlterarEstadoModalOpen" title="<?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_ANULAR' ); ?>" data-vd-newestado-id="3" data-vd-tarefa-id="'+row[8]+'" type="button">';
                        retVal += '   <i class="fa fa-times"></i>   </button>';

                        retVal += '<button class="btn btn-circle btn-outline blue  btAbrirModal btTarefaEditarModalOpen" title="<?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_EDITAR' ); ?>" data-vd-tarefa-id="'+row[8]+'" type="button">';
                        retVal += '   <i class="fa fa-pencil"></i>   </button>';

                        retVal += '<button class="btn btn-circle btn-outline  btAbrirModal btTarefaHistoricoModalOpen" title="<?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_VERHIST' ); ?>" data-vd-tarefa-id="'+row[8]+'" type="button">';
                        retVal += '   <i class="fa fa-list-alt"></i>   </button>';
                        return (retVal);
                    }
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=tarefa.getTarefasListByProcesso4ManagerByAjax&processo_tipotag=alerta&<?php echo $obVDCrypt->setIdInputNameEncrypt('processo_id',$setencrypt_forminputhidden); ?>=<?php echo $obVDCrypt->setIdInputValueEncrypt($getInputAlerta_Id,$setencrypt_forminputhidden); ?>",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

                ButtonHandle.handleButtonTarefaAlterarEstadoModal();
                ButtonHandle.handleButtonTarefaEditarModal();
                ButtonHandle.handleButtonTarefaHistoricoModal();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ]

        });

    }

    var initTableTarefaHistoricoModal = function () {

        var table = jQuery('#tabela_alerta_tarefa_historico_modal');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": 'lfrtip',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left"
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=tarefa.getTarefaHistoricoListByProcesso4ManagerByAjax&processo_tipotag=alerta&<?php echo $obVDCrypt->setIdInputNameEncrypt('processo_id',$setencrypt_forminputhidden); ?>=<?php echo $obVDCrypt->setIdInputValueEncrypt($getInputAlerta_Id,$setencrypt_forminputhidden); ?>" ,
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

            },
            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }
            ]
        });
    }

    var ReloadTableTarefasFull = function () {
        var table = jQuery('#tabela_alerta_tarefas_full');
        table.DataTable().ajax.reload();
    }

    var ReloadTableTarefasHistoricoModal = function (tarefaID) {
        var table = jQuery('#tabela_alerta_tarefa_historico_modal');
        var newURL = "?option=com_virtualdesk&task=tarefa.getTarefaHistoricoListByProcesso4ManagerByAjax&processo_tipotag=alerta&<?php echo $obVDCrypt->setIdInputNameEncrypt('processo_id',$setencrypt_forminputhidden); ?>=<?php echo $obVDCrypt->setIdInputValueEncrypt($getInputAlerta_Id,$setencrypt_forminputhidden); ?>" + "&<?php echo $obVDCrypt->setIdInputNameEncrypt('tarefa_id',$setencrypt_forminputhidden); ?>=" + tarefaID;
        table.DataTable().ajax.url( newURL);
        table.DataTable().ajax.reload();
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();

            initTableHistFull();

            initTableTarefasFull();

            initTableTarefaHistoricoModal();
        },
        ReloadTable1:function () { ReloadTable1(); },
        ReloadTableFull:function () { ReloadTableFull(); },
        ReloadTableTarefasFull:function () { ReloadTableTarefasFull(); },
        ReloadTableTarefasHistoricoModal:function (tarefaID) { ReloadTableTarefasHistoricoModal(tarefaID); }
    };
}();


var ButtonHandle = function () {

    var handleButton = function (evt) {

        jQuery(".newmsghistSend").on('click',function(evt,data){

            let vd_url_send    = jQuery(this).data('vd-url-send');
            let elText         = jQuery(this).closest('div.rowSendNewMsgText').find('.sendnewmsgtext');
            let sendnewmsgtext = elText.val();
            let elBlocoOptions = jQuery(this).closest('div.rowSendNewMsgText').next('div.newmsghistSendOptionsBlock');

            let elVisible4User = elBlocoOptions.find('input.newmsghistSendSetVisible4User');
            let setvisible4user = elVisible4User.bootstrapSwitch('state');

            let elTipo  = elBlocoOptions.find('select.newmsghistSendSetTipo');
            let setipomsg = elTipo.val();

            vdAjaxCall.sendNewMessage2Hist(jQuery(this), vd_url_send, elText, sendnewmsgtext, setvisible4user, setipomsg );

        });
    };


    var handleButtonAlterarEstadoModal = function (evt) {

        jQuery(".btAlterarEstadoModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarNewEstadoModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2NewEstado');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2newestadoSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewProcManagerId   = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let elNewProcManagerDesc = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
            elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonAlterarEstado = function (evt) {

        jQuery(".alterar2newestadoSend").on('click',function(evt,data){
            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2NewEstado');

            let elNewEstadoDesc  = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            let setNewEstadoDesc = elNewEstadoDesc.val();

            let elNewEstadoId  = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let setNewEstadoId = elNewEstadoId.val();

            vdAjaxCall.sendAlterar2NewEstado(jQuery(this), vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId );

        });
    };


    var handleButtonAlterarReencaminharModal = function (evt) {

        jQuery(".btAlterarReencaminharModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarReencaminharModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2Reencaminhar');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2reencaminharSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewUserId           = elBlocoOptions.find('select.Alterar2ReencaminharUserId');
            let elNewGroupId          = elBlocoOptions.find('select.Alterar2ReencaminharGroupId');
            let elNewReencaminharDesc = elBlocoOptions.find('input.Alterar2ReencaminharDesc');
            elNewUserId.removeAttr('disabled').removeClass('disabled');
            elNewGroupId.removeAttr('disabled').removeClass('disabled');
            elNewReencaminharDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonReencaminhar = function (evt) {

        jQuery(".alterar2reencaminharSend").on('click',function(evt,data){

            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2Reencaminhar');

            let elDesc  = elBlocoOptions.find('input.Alterar2ReencaminharDesc');
            let setDesc = elDesc.val();

            let elUserId     = elBlocoOptions.find('select.Alterar2ReencaminharUserId');
            let elUserIdData = elUserId.select2('data');
            let setUserId    = [];
            var uId;
            for (uId in elUserIdData) {
                setUserId.push(elUserIdData[uId].id);
            }

            let elGroupId     = elBlocoOptions.find('select.Alterar2ReencaminharGroupId');
            let elGroupIdData = elGroupId.select2('data');
            let setGroupId    = [];
            var gId;
            for (gId in elGroupIdData) {
                setGroupId.push(elGroupIdData[gId].id);
            }

            vdAjaxCall.sendAlterarReencaminhar(jQuery(this), vd_url_send, elUserId, setUserId, elGroupId,setGroupId, elDesc, setDesc  );
        });
    };


    var handleButtonAlterarProcManagerModal = function (evt) {

        jQuery(".btAlterarProcManagerModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarProcManagerModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2NewProcManager');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2newprocmanagerSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewProcManagerId   = elBlocoOptions.find('select.Alterar2NewProcManagerId');
            let elNewProcManagerDesc = elBlocoOptions.find('input.Alterar2NewProcManagerDesc');
            elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
            elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonAlterarProcManager = function (evt) {

        jQuery(".alterar2newprocmanagerSend").on('click',function(evt,data){
            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2NewProcManager');

            let elNewProcManagerDesc  = elBlocoOptions.find('input.Alterar2NewProcManagerDesc');
            let setNewProcManagerDesc = elNewProcManagerDesc.val();

            let elNewProcManagerId  = elBlocoOptions.find('select.Alterar2NewProcManagerId');
            let setNewProcManagerId = elNewProcManagerId.val();

            vdAjaxCall.sendAlterar2NewProcManager(jQuery(this), vd_url_send, elNewProcManagerDesc, elNewProcManagerId, setNewProcManagerDesc, setNewProcManagerId );
        });
    };


    var handleButtonNovaTarefaModal = function (evt) {

        jQuery(".btNovaTarefaModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#NovaTarefaModal");
            let elBlocoOptions = elModal.find('div.blocoNovaTarefa');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".novatarefaSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewUserId           = elBlocoOptions.find('select.NovaTarefaUserId');
            let elNewGroupId          = elBlocoOptions.find('select.NovaTarefaGroupId');
            let elNewNovaTarefaDesc = elBlocoOptions.find('input.NovaTarefaDesc');
            elNewUserId.removeAttr('disabled').removeClass('disabled');
            elNewGroupId.removeAttr('disabled').removeClass('disabled');
            elNewNovaTarefaDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonNovaTarefa = function (evt) {

        jQuery(".novatarefaSend").on('click',function(evt,data){

            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoNovaTarefa');

            let elNome  = elBlocoOptions.find('input.NovaTarefaNome');
            let setNome = elNome.val();

            let elDesc  = elBlocoOptions.find('textarea.NovaTarefaDesc');
            let setDesc = elDesc.val();

            let elUserId     = elBlocoOptions.find('select.NovaTarefaUserId');
            let elUserIdData = elUserId.select2('data');
            let setUserId    = [];
            var uId;
            for (uId in elUserIdData) {
                setUserId.push(elUserIdData[uId].id);
            }

            let elGroupId     = elBlocoOptions.find('select.NovaTarefaGroupId');
            let elGroupIdData = elGroupId.select2('data');
            let setGroupId    = [];
            var gId;
            for (gId in elGroupIdData) {
                setGroupId.push(elGroupIdData[gId].id);
            }

            vdAjaxCall.sendNovaTarefa(jQuery(this), vd_url_send, elUserId, setUserId, elGroupId,setGroupId, elDesc, setDesc, elNome, setNome);
        });
    };


    var handleButtonTarefaAlterarEstadoModal = function (evt) {

        jQuery(".btTarefaAlterarEstadoModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let tarefaID    = jQuery(this).data('vd-tarefa-id')
            let newEstadoID = jQuery(this).data('vd-newestado-id');

            let elModal        = jQuery("#TarefaAlterarEstadoModal");
            let elBlocoOptions = elModal.find('div.blocoTarefaAlterarEstado');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".tarefaalterarestadoSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');
            elBtAlterar.attr('data-vd-tarefa-id',0);
            elBtAlterar.attr('data-vd-tarefa-id',tarefaID);

            let elNewEstadoSel = elBlocoOptions.find('select.TarefaAlterarNewEstadoId');
            elNewEstadoSel.val(newEstadoID);
            elNewEstadoSel.selectpicker('refresh');
            let elObs = elBlocoOptions.find('textarea.TarefaAlterarObs');
            elNewEstadoSel.removeAttr('disabled').removeClass('disabled');
            elObs.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            // Carrega dados e coloca dos valores nos campos
            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');
            vdAjaxCall.getTarefaAlterarEstadoData(jQuery(this), urlGetContent, tarefaID, elBlocoOptions, elModalContent);

            elModal.modal('show');
        });
    };


    var handleButtonTarefaAlterarEstado = function (evt) {

        jQuery(".tarefaalterarestadoSend").on('click',function(evt,data){

            let vd_url_send   = jQuery(this).data('vd-url-send');
            let vd_tarefa_id  = jQuery(this).attr('data-vd-tarefa-id'); // bug com o data(), só funcionou com o attr

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoTarefaAlterarEstado');

            let elObs  = elBlocoOptions.find('textarea.TarefaAlterarObs');
            let setObs = elObs.val();

            let elNewEstadoId  = elBlocoOptions.find('select.TarefaAlterarNewEstadoId');
            let setNewEstadoId = elNewEstadoId.val();

            vdAjaxCall.sendTarefaAlterarEstado(jQuery(this), vd_url_send, vd_tarefa_id, elNewEstadoId, setNewEstadoId, elObs, setObs);
        });
    };


    var handleButtonTarefaEditarModal = function (evt) {

        jQuery(".btTarefaEditarModalOpen").on('click',function(evt,data){
            let tarefaID    = jQuery(this).data('vd-tarefa-id')

            let elModal        = jQuery("#TarefaEditarModal");
            let elBlocoOptions = elModal.find('div.blocoTarefaEditar');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');

            let elBtAlterar = elModalContent.find(".tarefaeditarSend");
            elBtAlterar.attr('data-vd-tarefa-id',0);
            elBtAlterar.attr('data-vd-tarefa-id',tarefaID);

            // Carrega dados e coloca dos valores nos campos
            vdAjaxCall.getTarefaEditarDetailData(jQuery(this), urlGetContent, tarefaID, elBlocoOptions, elModalContent);

            elModal.modal('show');
        });
    };


    var handleButtonTarefaEditar = function (evt) {

        jQuery(".tarefaeditarSend").on('click',function(evt,data){

            let vd_url_send   = jQuery(this).data('vd-url-send');
            let vd_tarefa_id  = jQuery(this).attr('data-vd-tarefa-id'); // bug com o data(), só funcionou com o attr

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoTarefaEditar');

            let elNome  = elBlocoOptions.find('input.TarefaEditarNome');
            let setNome = elNome.val();
            let elDesc  = elBlocoOptions.find('textarea.TarefaEditarDesc');
            let setDesc = elDesc.val();

            let elEstadoId  = elBlocoOptions.find('select.TarefaEditarEstadoId');
            let setEstadoId = elEstadoId.val();

            let elUserId     = elBlocoOptions.find('select.TarefaEditarUserId');
            let elUserIdData = elUserId.select2('data');
            let setUserId    = [];
            var uId;
            for (uId in elUserIdData) {
                setUserId.push(elUserIdData[uId].id);
            }

            let elGroupId     = elBlocoOptions.find('select.TarefaEditarGroupId');
            let elGroupIdData = elGroupId.select2('data');
            let setGroupId    = [];
            var gId;
            for (gId in elGroupIdData) {
                setGroupId.push(elGroupIdData[gId].id);
            }

            vdAjaxCall.sendTarefaEditar(jQuery(this), vd_url_send, vd_tarefa_id, elEstadoId, setEstadoId, elNome, setNome, elDesc, setDesc, elUserId, setUserId, elGroupId,setGroupId);
        });
    };


    var handleButtonTarefaHistoricoModal = function (evt) {

        jQuery(".btTarefaHistoricoModalOpen").on('click',function(evt,data){
            let tarefaID    = jQuery(this).data('vd-tarefa-id')

            let elModal        = jQuery("#TarefaHistoricoModal");
            let elBlocoOptions = elModal.find('div.blocoTarefaHistorico');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let urlGetContent = elBlocoOptions.data('vd-url-getcontent');

            // Carrega dados e coloca dos valores nos campos
            vdAjaxCall.getTarefaHistoricoData(jQuery(this), urlGetContent, tarefaID, elBlocoOptions, elModalContent);

            TableDatatablesManaged.ReloadTableTarefasHistoricoModal(tarefaID);

            elModal.modal('show');
        });
    };


    return {
        //main function to initiate the module
        init: function () {
          handleButton();
          handleButtonAlterarEstadoModal();
          handleButtonAlterarEstado();
          handleButtonAlterarReencaminharModal();
          handleButtonReencaminhar();
          handleButtonAlterarProcManagerModal();
          handleButtonAlterarProcManager();
          handleButtonNovaTarefaModal();
          handleButtonNovaTarefa();
          handleButtonTarefaAlterarEstado();
          handleButtonTarefaEditar();

        },
        handleButtonTarefaAlterarEstadoModal : handleButtonTarefaAlterarEstadoModal,
        handleButtonTarefaEditarModal : handleButtonTarefaEditarModal,
        handleButtonTarefaHistoricoModal : handleButtonTarefaHistoricoModal

    };

}();


var ModalHandle = function () {

    var handleModalAlterarNewEstado = function (evt) {
        jQuery("#AlterarNewEstadoModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalAlterarReencaminhar = function (evt) {
        jQuery("#AlterarReencaminharModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalAlterarProcManager = function (evt) {
        jQuery("#AlterarProcManagerModal").modal({
            show: false,
            keyboard: true
        })
    };


    var handleModalNovaTarefa = function (evt) {
        jQuery("#NovaTarefaModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalTarefaAlterarEstado = function (evt) {
        jQuery("#TarefaAlterarEstadoModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalTarefaEditar = function (evt) {
        jQuery("#TarefaEditarModal").modal({
            show: false,
            keyboard: true
        })
    };

    var handleModalTarefaHistorico = function (evt) {
        jQuery("#TarefaHistoricoModal").modal({
            show: false,
            keyboard: true
        })
    };

    return {
        //main function to initiate the module
        init: function () {
            handleModalAlterarNewEstado();
            handleModalAlterarProcManager();
            handleModalAlterarReencaminhar();
            handleModalNovaTarefa();
            handleModalTarefaAlterarEstado();
            handleModalTarefaEditar();
            handleModalTarefaHistorico();
        }
    };
}();


var vdAjaxCall = function () {

    var sendNewMessage2Hist = function (el, vd_url_send, elText, sendnewmsgtext, setvisible4user, setipomsg) {

        el.attr('disabled','disabled').addClass('disabled');
        elText.attr('disabled','disabled').addClass('disabled');
        let vdClosestI = el.parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        sendnewmsgtext =  encodeURIComponent(sendnewmsgtext);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'sendnewmsgtext=' + sendnewmsgtext + '&setvisible4user=' + setvisible4user + '&setipomsg=' +setipomsg,
            indexValue: {el:el, vd_url_send:vd_url_send, elText:elText, sendnewmsgtext:sendnewmsgtext, setvisible4user:setvisible4user, setipomsg:setipomsg},
            success: function(data){
                let vdClosestI = el.parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg').addClass("fa-check fa-2x text-success");

                TableDatatablesManaged.ReloadTable1();
                TableDatatablesManaged.ReloadTableFull();
                TimeLineHandle.init();

                el.removeAttr('disabled').removeClass('disabled');
                elText.removeAttr('disabled').removeClass('disabled');
                elText.val('')

            },
            error: function(error){
                let vdClosestI = el.parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };


    var getTimeLine2Hist = function (el, vd_url_getcontent) {

        let vdClosestI = el.find('div > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-4x text-default");
        el.find('div.TimeLineContent').html('');

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent},
            success: function(data){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default'); //.addClass("fa-check fa-2x text-success");

                el.find('div.TimeLineContent').html(data);
            },
            error: function(error){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };


    var getTimeLineSimple2Hist = function (el, vd_url_getcontent) {

        let vdClosestI = el.find('div > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-4x text-default");
        el.find('div.TimeLineContent').html('');

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent},
            success: function(data){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default'); //.addClass("fa-check fa-2x text-success");

                el.find('div.TimeLineContent').html(data);
            },
            error: function(error){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };

// Gravar novo estado
    var sendAlterar2NewEstado = function (el, vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elNewEstadoDesc.attr('disabled','disabled').addClass('disabled');
        elNewEstadoId.attr('disabled','disabled').addClass('disabled');
        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setNewEstadoDesc =  encodeURIComponent(setNewEstadoDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setNewEstadoDesc=' + setNewEstadoDesc + '&setNewEstadoId=' + setNewEstadoId  ,
            indexValue: {el:el, vd_url_send:vd_url_send, elNewEstadoId:elNewEstadoId, elNewEstadoDesc:elNewEstadoDesc, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError },
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);

                TableDatatablesManaged.ReloadTable1();
                TableDatatablesManaged.ReloadTableFull();
                TimeLineHandle.init();

                setTimeout(
                    function()
                    {
                        jQuery("#AlterarNewEstadoModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        vdAjaxCall.setNewEstadoStaticVal();

                        el.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.val('')
                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

// obter o valor estático do novo estado
    var setNewEstadoStaticVal = function () {
        let elEstadoStatic = jQuery('.ValorEstadoAtualStatic');
        let vd_url_get = elEstadoStatic .data('vd-url-get');

        jQuery.ajax({
            url: vd_url_get,
            type: "POST",
            indexValue: {elEstadoStatic:elEstadoStatic, vd_url_get:vd_url_get},
            success: function(data){
                var d = JSON.parse(data);
                elEstadoStatic.find('span').fadeOut().html(d.name).removeClass().addClass('label ' + d.cssClass).fadeIn();

            },
            error: function(error){

            }
        });
    };


// Gravar Reencaminhar
    var sendAlterarReencaminhar = function (el, vd_url_send, elUserId, setUserId, elGroupId,setGroupId, elDesc, setDesc ) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elDesc.attr('disabled','disabled').addClass('disabled');
        elUserId.attr('disabled','disabled').addClass('disabled');
        elGroupId.attr('disabled','disabled').addClass('disabled');

        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setDesc =  encodeURIComponent(setDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setDesc=' + setDesc + '&<?php echo $obVDCrypt->setIdInputMultiNameEncrypt('setUserId',$setencrypt_forminputhidden); ?>=' + JSON.stringify(setUserId) + '&<?php echo $obVDCrypt->setIdInputMultiNameEncrypt('setGroupId',$setencrypt_forminputhidden); ?>=' + JSON.stringify(setGroupId),
            indexValue: {el:el, vd_url_send:vd_url_send, setUserId:setUserId, setGroupId:setGroupId, setDesc:setDesc, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError},
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);

                //vdAjaxCall.setNewEstadoStaticVal();

                setTimeout(
                    function()
                    {
                        jQuery("#AlterarReencaminharModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        vdAjaxCall.setNewReencaminharStaticVal();

                        el.removeAttr('disabled').removeClass('disabled');
                        elUserId.removeAttr('disabled','disabled').removeClass('disabled');
                        elGroupId.removeAttr('disabled','disabled').removeClass('disabled');
                        elDesc.removeAttr('disabled','disabled').removeClass('disabled');
                        elDesc.val('');

                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elUserId.removeAttr('disabled','disabled').removeClass('disabled');
                elGroupId.removeAttr('disabled','disabled').removeClass('disabled');
                elDesc.removeAttr('disabled','disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

// obter o valor estático dos Users e Groups do Reencaminhar
    var setNewReencaminharStaticVal = function () {
        let elReencaminharStatic = jQuery('.ValorReencaminharAtualStatic');
        let vd_url_get = elReencaminharStatic.data('vd-url-get');

        jQuery.ajax({
            url: vd_url_get,
            type: "POST",
            indexValue: {elProcManagerStatic:elReencaminharStatic, vd_url_get:vd_url_get},
            success: function(data){
                var d = JSON.parse(data);
                var dname = '';
                for (const s of d) {
                    if(dname !='') dname += ' ; '
                    dname += s.nome;
                }
                elReencaminharStatic.find('span').fadeOut().html(dname).fadeIn();
            },
            error: function(error){
            }
        });
    };


// Gravar novo Responsavel - ProcManager
    var sendAlterar2NewProcManager = function (el, vd_url_send, elNewProcManagerDesc, elNewProcManagerId, setNewProcManagerDesc, setNewProcManagerId) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elNewProcManagerDesc.attr('disabled','disabled').addClass('disabled');
        elNewProcManagerId.attr('disabled','disabled').addClass('disabled');
        //let vdClosestI = el.parent().find('span > i.fa');
        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setNewProcManagerDesc =  encodeURIComponent(setNewProcManagerDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setNewProcManagerDesc=' + setNewProcManagerDesc + '&<?php echo $obVDCrypt->setIdInputNameEncrypt('setNewProcManagerId',$setencrypt_forminputhidden); ?>=' + setNewProcManagerId  ,
            indexValue: {el:el, vd_url_send:vd_url_send, elNewProcManagerId:elNewProcManagerId, elNewProcManagerDesc:elNewProcManagerDesc, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError },
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);

                TableDatatablesManaged.ReloadTable1();
                TableDatatablesManaged.ReloadTableFull();
                TimeLineHandle.init();

                setTimeout(
                    function()
                    {
                        jQuery("#AlterarProcManagerModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        vdAjaxCall.setNewProcManagerStaticVal();

                        el.removeAttr('disabled').removeClass('disabled');
                        elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
                        elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');
                        elNewProcManagerDesc.val('')
                    }, 800);
            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
                elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                   let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                   blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

// obter o valor estático do novo responsavel - ProcManager
    var setNewProcManagerStaticVal = function () {
        let elProcManagerStatic = jQuery('.ValorProcManagerAtualStatic');
        let vd_url_get = elProcManagerStatic.data('vd-url-get');

        jQuery.ajax({
            url: vd_url_get,
            type: "POST",
            indexValue: {elProcManagerStatic:elProcManagerStatic, vd_url_get:vd_url_get},
            success: function(data){
                var d = JSON.parse(data);
                elProcManagerStatic.find('span').fadeOut().html(d.name).fadeIn();

            },
            error: function(error){

            }
        });
    };

// Gravar Nova Tarefa
    var sendNovaTarefa = function (el, vd_url_send, elUserId, setUserId, elGroupId,setGroupId, elDesc, setDesc, elNome, setNome ) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elDesc.attr('disabled','disabled').addClass('disabled');
        elUserId.attr('disabled','disabled').addClass('disabled');
        elGroupId.attr('disabled','disabled').addClass('disabled');

        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setDesc =  encodeURIComponent(setDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setDesc=' + setDesc + '&setNome=' + setNome + '&<?php echo $obVDCrypt->setIdInputMultiNameEncrypt('setUserId',$setencrypt_forminputhidden); ?>=' + JSON.stringify(setUserId) + '&<?php echo $obVDCrypt->setIdInputMultiNameEncrypt('setGroupId',$setencrypt_forminputhidden); ?>=' + JSON.stringify(setGroupId),
            indexValue: {el:el, vd_url_send:vd_url_send, setUserId:setUserId, setGroupId:setGroupId, setDesc:setDesc,  setNome:setNome, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError},
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);
                setTimeout(
                    function()
                    {
                        jQuery("#NovaTarefaModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        TableDatatablesManaged.ReloadTableTarefasFull();
                        NotificationsHandle.getTarefaNumAbertasData();
                        NotificationsHandleMain4Manager.getTarefaAbertasReload(); //MAIN

                        el.removeAttr('disabled').removeClass('disabled');
                        elUserId.removeAttr('disabled','disabled').removeClass('disabled');
                        elGroupId.removeAttr('disabled','disabled').removeClass('disabled');
                        elDesc.removeAttr('disabled','disabled').removeClass('disabled');
                        elDesc.val('');

                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elUserId.removeAttr('disabled','disabled').removeClass('disabled');
                elGroupId.removeAttr('disabled','disabled').removeClass('disabled');
                elDesc.removeAttr('disabled','disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

// Alterar Estado de uma Tarefa: concluir, anular, etc
    var sendTarefaAlterarEstado = function (el, vd_url_send, vd_tarefa_id, elNewEstadoId, setNewEstadoId, elObs, setObs ) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elObs.attr('disabled','disabled').addClass('disabled');
        elNewEstadoId.attr('disabled','disabled').addClass('disabled');

        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setObs =  encodeURIComponent(setObs);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setObs=' + setObs + '&setNewEstadoId=' + setNewEstadoId + '&tarefa_id=' + vd_tarefa_id,
            indexValue: {el:el, vd_url_send:vd_url_send, vd_tarefa_id:vd_tarefa_id, setNewEstadoId:setNewEstadoId, setObs:setObs, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError},
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);
                setTimeout(
                    function()
                    {
                        jQuery("#TarefaAlterarEstadoModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        TableDatatablesManaged.ReloadTableTarefasFull();
                        NotificationsHandle.getTarefaNumAbertasData();
                        NotificationsHandleMain4Manager.getTarefaAbertasReload(); //MAIN

                        el.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoId.removeAttr('disabled','disabled').removeClass('disabled');
                        elObs.removeAttr('disabled','disabled').removeClass('disabled');
                        elObs.val('');

                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elNewEstadoId.removeAttr('disabled','disabled').removeClass('disabled');
                elObs.removeAttr('disabled','disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

// Carrega os campos por ajax de uma tarefa de detalhe e coloca na janela modal
    var getTarefaEditarDetailData = function (el, vd_url_getcontent, vd_tarefa_id, elBlocoOptions, elModalContent) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        //let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';

        let elEstadoSel = elBlocoOptions.find('select.TarefaEditarEstadoId');
        let elNome      = elBlocoOptions.find('input.TarefaEditarNome');
        let elDesc      = elBlocoOptions.find('textarea.TarefaEditarDesc');
        let elUserId    = elBlocoOptions.find('select.TarefaEditarUserId');
        let elGroupId   = elBlocoOptions.find('select.TarefaEditarGroupId');
        let elBtAlterar = elModalContent.find(".tarefaeditarSend");

        elNome.val('');
        elDesc.val('');
        elEstadoSel.val('');

        elNome.attr('disabled','disabled').addClass('disabled');
        elDesc.attr('disabled','disabled').addClass('disabled');
        elEstadoSel.attr('disabled','disabled').addClass('disabled');
        elEstadoSel.selectpicker('refresh');
        elBtAlterar.attr('disabled','disabled').addClass('disabled');

        elUserId.attr('disabled','disabled').addClass('disabled');
        elGroupId.attr('disabled','disabled').addClass('disabled');

        let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            data: '&tarefa_id=' + vd_tarefa_id,
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent, vd_tarefa_id:vd_tarefa_id, elEstadoSel:elEstadoSel, elDesc:elDesc, elNome:elNome, elBtAlterar:elBtAlterar, elUserId:elUserId, elGroupId:elGroupId},
            success: function(data){
                let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
                vdClosestI.removeClass(setClassSpinner).removeClass(setClassError);
                setTimeout(
                    function()
                    {
                        var response = JSON.parse(data);

                        let valEstadoID = response['trf'].id_tarefa_estado;

                        elEstadoSel.val(valEstadoID);
                        elEstadoSel.removeAttr('disabled').removeClass('disabled');
                        elEstadoSel.selectpicker('refresh');

                        elNome.val(response['trf'].nome);
                        elDesc.val(response['trf'].descricao);

                        let elUserIdData =  response['usr'];
                        let setUserId    = [];
                        var uId;
                        for (uId in elUserIdData) {
                            setUserId.push(elUserIdData[uId].id_user);
                        }
                        elUserId.val(setUserId);
                        elUserId.select2().trigger('change');

                        let elGroupIdData =  response['grp'];
                        let setGroupId    = [];
                        var gId;
                        for (gId in elGroupIdData) {
                            setGroupId.push(elGroupIdData[gId].id_group);
                        }
                        elGroupId.val(setGroupId);
                        elGroupId.select2().trigger('change');

                        elUserId.removeAttr('disabled').removeClass('disabled');
                        elGroupId.removeAttr('disabled').removeClass('disabled');

                        elBtAlterar.removeAttr('disabled').removeClass('disabled');
                        elNome.removeAttr('disabled').removeClass('disabled');
                        elDesc.removeAttr('disabled').removeClass('disabled');

                    }, 250);

            },
            error: function(error){
                let vdClosestI = elModalContent.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");

                elBtAlterar.removeAttr('disabled').removeClass('disabled');
                elEstadoSel.removeAttr('disabled').removeClass('disabled');
                elDesc.removeAttr('disabled').removeClass('disabled');
                elUserId.removeAttr('disabled').removeClass('disabled');
                elGroupId.removeAttr('disabled').removeClass('disabled');
            }
        });
    };

    // Alterar dados de uma Tarefa no ecrã editar
    var sendTarefaEditar = function (el, vd_url_send, vd_tarefa_id, elEstadoId, setEstadoId, elNome, setNome, elDesc, setDesc, elUserId, setUserId, elGroupId,setGroupId ) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';

        el.attr('disabled','disabled').addClass('disabled');
        elNome.attr('disabled','disabled').addClass('disabled');
        elDesc.attr('disabled','disabled').addClass('disabled');
        elEstadoId.attr('disabled','disabled').addClass('disabled');
        elUserId.attr('disabled','disabled').addClass('disabled');
        elGroupId.attr('disabled','disabled').addClass('disabled');

        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setNome =  encodeURIComponent(setNome);
        setDesc =  encodeURIComponent(setDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setNome=' + setNome + '&setDesc=' + setDesc + '&setEstadoId=' + setEstadoId + '&tarefa_id=' + vd_tarefa_id + '&<?php echo $obVDCrypt->setIdInputMultiNameEncrypt('setUserId',$setencrypt_forminputhidden); ?>='+ JSON.stringify(setUserId) + '&<?php echo $obVDCrypt->setIdInputMultiNameEncrypt('setGroupId',$setencrypt_forminputhidden); ?>=' + JSON.stringify(setGroupId),
            indexValue: {el:el, vd_url_send:vd_url_send, vd_tarefa_id:vd_tarefa_id, setEstadoId:setEstadoId, setNome:setNome, setDesc:setDesc, setUserId:setUserId, setGroupId:setGroupId, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError},
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);
                setTimeout(
                    function()
                    {
                        jQuery("#TarefaEditarModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        TableDatatablesManaged.ReloadTableTarefasFull();
                        NotificationsHandle.getTarefaNumAbertasData();
                        NotificationsHandleMain4Manager.getTarefaAbertasReload(); //MAIN

                        el.removeAttr('disabled').removeClass('disabled');
                        elEstadoId.removeAttr('disabled','disabled').removeClass('disabled');
                        elNome.removeAttr('disabled','disabled').removeClass('disabled');
                        elDesc.removeAttr('disabled','disabled').removeClass('disabled');

                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elEstadoId.removeAttr('disabled','disabled').removeClass('disabled');
                elNome.removeAttr('disabled','disabled').removeClass('disabled');
                elDesc.removeAttr('disabled','disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

    // Carrega os campos por ajax do histórico de uma e coloca na janela modal
    var getTarefaHistoricoData = function (el, vd_url_getcontent, vd_tarefa_id, elBlocoOptions, elModalContent) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassError   = 'fa-times-circle fa-4x text-danger';

        let elNome      = elBlocoOptions.find('span.TarefaHistoricoNome');
        let elDesc      = elBlocoOptions.find('span.TarefaHistoricoDesc');
        elNome.html('');
        elDesc.html('');

        let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            data: '&tarefa_id=' + vd_tarefa_id,
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent, vd_tarefa_id:vd_tarefa_id, elDesc:elDesc, elNome:elNome},
            success: function(data){
                let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
                vdClosestI.removeClass(setClassSpinner).removeClass(setClassError);
                setTimeout(
                    function()
                    {
                        var response = JSON.parse(data);
                        elNome.html(response.nome);
                        elDesc.html(response.descricao);

                    }, 250);

            },
            error: function(error){
                let vdClosestI = elModalContent.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };

    // Carrega os campos da tarefa por ajax para o alterar do estado e coloca na janela modal
    var getTarefaAlterarEstadoData = function (el, vd_url_getcontent, vd_tarefa_id, elBlocoOptions, elModalContent) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassError   = 'fa-times-circle fa-4x text-danger';

        let elNome      = elBlocoOptions.find('span.TarefaAlterarEstadoNome');
        let elDesc      = elBlocoOptions.find('span.TarefaAlterarEstadoDesc');
        elNome.html('');
        elDesc.html('');

        let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            data: '&tarefa_id=' + vd_tarefa_id,
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent, vd_tarefa_id:vd_tarefa_id, elDesc:elDesc, elNome:elNome},
            success: function(data){
                let vdClosestI = elModalContent.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
                vdClosestI.removeClass(setClassSpinner).removeClass(setClassError);
                setTimeout(
                    function()
                    {
                        var response = JSON.parse(data);
                        elNome.html(response.nome);
                        elDesc.html(response.descricao);

                    }, 250);

            },
            error: function(error){
                let vdClosestI = elModalContent.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };

    // Carrega quantas tarefas estão ABERTAS (não concluída e não anuladas) e coloca um icon com esse
    var getTarefaNumAbertasData = function (el) {
        let urlGetContent = el.data('vd-url-getcontent');
        jQuery.ajax({
            url: urlGetContent,
            type: "POST",
            data: '',
            indexValue: {el:el, vd_url_getcontent:urlGetContent},
            success: function(data){
                var response = JSON.parse(data);
                setTimeout(
                    function()
                    {
                        if(typeof response !== 'undefined' && response.length>0) {
                            el.find('.vdValorNot').html(response.length);
                            el.show();
                        }
                    }, 100);
            },
            error: function(error){
            }
        });
    };

    return {
        sendNewMessage2Hist     : sendNewMessage2Hist,
        getTimeLine2Hist        : getTimeLine2Hist,
        getTimeLineSimple2Hist  : getTimeLineSimple2Hist,
        sendAlterar2NewEstado   : sendAlterar2NewEstado,
        setNewEstadoStaticVal   : setNewEstadoStaticVal,
        sendAlterarReencaminhar : sendAlterarReencaminhar,
        setNewReencaminharStaticVal : setNewReencaminharStaticVal,
        sendAlterar2NewProcManager  : sendAlterar2NewProcManager,
        setNewProcManagerStaticVal  : setNewProcManagerStaticVal,
        sendNovaTarefa              : sendNovaTarefa,
        sendTarefaAlterarEstado     : sendTarefaAlterarEstado,
        getTarefaEditarDetailData   : getTarefaEditarDetailData,
        sendTarefaEditar            : sendTarefaEditar,
        getTarefaHistoricoData      : getTarefaHistoricoData,
        getTarefaAlterarEstadoData  : getTarefaAlterarEstadoData,
        getTarefaNumAbertasData     : getTarefaNumAbertasData
    };

}();


var TimeLineHandle = function () {

    var handleTimeLine = function (evt) {
        var el = jQuery("#HistoricoTimeLine");
        let vd_url_getcontent    = el.data('vd-url-getcontent');
        vdAjaxCall.getTimeLine2Hist(el, vd_url_getcontent );
    };

    var handleTimeLineSimple = function (evt) {
        var el = jQuery("#HistoricoTimeLineSimple");
        let vd_url_getcontent    = el.data('vd-url-getcontent');
        vdAjaxCall.getTimeLineSimple2Hist(el, vd_url_getcontent );
    };

    return {
        //main function to initiate the module
        init: function () {
            handleTimeLine();
            handleTimeLineSimple();
        }
    };
}();


var ComponentsBootstrapSelect = function () {
    var handleBootstrapSelect = function() {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleBootstrapSelect();
        }
    };
}();


var ComponentsSelect2 = function() {
    var handleDemo = function() {
        jQuery.fn.select2.defaults.set("theme", "bootstrap");
        var placeholder = "";
        jQuery(".select2, .select2-multiple").select2({
            // placeholder: placeholder,
            width: null
        });

        jQuery(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        jQuery(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        jQuery("button[data-select2-open]").click(function() {
            jQuery("#" + jQuery(this).data("select2-open")).select2("open");
        });

        jQuery(":checkbox").on("click", function() {
            jQuery(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        jQuery(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if (jQuery(this).parents("[class*='has-']").length) {
                var classNames = jQuery(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        jQuery("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        jQuery(".js-btn-set-scaling-classes").on("click", function() {
            jQuery("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            jQuery("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            jQuery(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


var NotificationsHandle = function () {

    var getTarefaNumAbertasData = function (evt) {

        let el = jQuery('#vdTabNavTarefasNumAbertas');
        vdAjaxCall.getTarefaNumAbertasData(el);
    };



    return {
        //main function to initiate the module
        init: function () {
            getTarefaNumAbertasData();
        },
        getTarefaNumAbertasData: getTarefaNumAbertasData

    };
}();


var PortofolioHandle = function () {

    var initAlertaFileGridResumo = function (evt) {

        // init cubeportfolio
        jQuery('#vdAlertaFileGridResumo').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var initAlertaFileGridDetalhe = function (evt) {

        // init cubeportfolio
        jQuery('#vdAlertaFileGridDetalhe').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        });

    };

    var destroyAlertaFileGridDetalhe = function (evt) {
        // init cubeportfolio
        jQuery('#vdAlertaFileGridDetalhe').cubeportfolio('destroy');
    };

    return {
        //main function to initiate the module
        init: function () {
            initAlertaFileGridResumo();
            initAlertaFileGridDetalhe();
        },
        initAlertaFileGridDetalhe     :  initAlertaFileGridDetalhe,
        destroyAlertaFileGridDetalhe  : destroyAlertaFileGridDetalhe

    };
}();


jQuery(document).ready(function() {

    TableDatatablesManaged.init();

    ButtonHandle.init();

    ModalHandle.init();

    TimeLineHandle.init();

    ComponentsBootstrapSelect.init();

    ComponentsSelect2.init();

    NotificationsHandle.init();

    PortofolioHandle.init();


    jQuery('a.vdtabAlertaDetalhe').on('shown.bs.tab', function(e){
        if( jQuery('#gmap_marker2').length ) {
            MapsGoogle.mapMarker2();
        }
    })

    jQuery('.nav-tabs.vdMainNavTab').find('a.vdtabAlertaDetalhe').on('shown.bs.tab', function () {
        // ao clicar no detalhe inicializa a grid
        PortofolioHandle.destroyAlertaFileGridDetalhe();
        PortofolioHandle.initAlertaFileGridDetalhe();

    });

    // Prevent default submit by Enter Key
    jQuery("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

    // Set default submit by Enter Key in Specific field
    jQuery(".sendnewmsgtext").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            var Button = jQuery(this).closest('div.rowSendNewMsgText').find('button.newmsghistSend');
            Button.click();
            return false;
        }
    });


    jQuery(".sendnewmsgtext").on("focus", function (e) {
        jQuery(this).closest('div.rowSendNewMsgText').next('div.newmsghistSendOptionsBlock').fadeIn();
    });





});