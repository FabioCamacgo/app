<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteAlertaFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertafields.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteTarefaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_tarefa.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('alerta');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
            $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
        break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

    // Tarefa - CSS Comum
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/tarefa/tmpl/tarefa-comum.css');

    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputAlerta_Id = JFactory::getApplication()->input->getInt('alerta_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAlertaHelper::getAlertaView4ManagerDetail($getInputAlerta_Id);
    if(empty($this->data))  return false;

    if(empty($this->data->latitude)){
        $this->data->latitude = 32.71669086705518;
        $this->data->longitude = -17.099513263315096;
    }

    $dataDetailGetReencaminhar = VirtualDeskSiteAlertaHelper::getAlertaView4ManagerDetailGetReencaminhar($getInputAlerta_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
        $objAlertFiles = new VirtualDeskSiteAlertsFilesHelper();
        $ListFilesAlert = $objAlertFiles->getFileGuestLinkByRefId ($this->data->codigo);
    }

    // Definição dos campos de utilizador
    $ObjalertaFields = new VirtualDeskSiteAlertaFieldsHelper();
    $aralertaFieldsConfig = $ObjalertaFields->getFields();

    $itemmenuid_lista = $params->get('alerta_menuitemid_list');


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Multichoice / select2 : Áreas de Atuação
    //$AreaActListByName = VirtualDeskSiteAlertaFieldsHelper::getAreaActListSelectName ($this->data, $fileLangSufix);

    // select2 : Tipo de Mensagem de Histórico
    $TipoSelectbyManager = VirtualDeskSiteAlertaHelper::getAlertaHistTipoSelectByManager($fileLangSufix);


    // select2 : Tipo de Mensagem de Histórico
    $AlertaEstadoList2Change     = VirtualDeskSiteAlertaHelper::getAlertaEstadoList2Change ($fileLangSufix, $this->data->idestado);
    $TarefasEstadoList           = VirtualDeskSiteTarefaHelper::getEstadoListAllBy2Process ('alerta');

    $AlertaReencaminharUserList  = VirtualDeskSiteAlertaHelper::getAlertaReencaminharUserList4Manager ($getInputAlerta_Id);

    $AlertaReencaminharGrupoList = VirtualDeskSiteAlertaHelper::getAlertaReencaminharGroupList4Manager ($getInputAlerta_Id);

    $AlertaNovaTarefaUserList    = $AlertaReencaminharUserList;
    $AlertaNovaTarefaGrupoList   = $AlertaReencaminharGrupoList;
    $AlertaTarefaEditarUserList  = $AlertaReencaminharUserList;
    $AlertaTarefaEditarGrupoList = $AlertaReencaminharGrupoList;

    $AlertaProcManagerList2Change    = VirtualDeskSiteAlertaHelper::getAlertaProcManagerList2Change();

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA' ); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_VER_DETALHE' ); ?></span>
            </div>


            <?php
            //Se tem DADOS VAZIOS -> Apresenta mensagem e sai...
            if(empty($this->data)) : ?>
                <div class="actions">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>" class="btn btn-circle btn-default">
                        <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
                <div class="portlet-body form">
                    <div class="form-actions right">
                        <div class="row">

                            <div  class="col-md-12 text-left">
                                <div class="alert alert-info fade in">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                                    <?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMPTYLIST'); ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php exit(); ?>
            <?php endif;  ?>


            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=edit4manager&vdcleanstate=1&alerta_id=' . $this->escape($this->data->alerta_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>

                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_ADDNEW' ); ?></a>


                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>


        <div class="portlet-body form">
            <form class="form-horizontal">
                <div class="form-body">
                    <div class="tabbable-line nav-justified ">
                        <ul class="nav nav-tabs vdMainNavTab">
                            <li class="active">
                                <a href="#tabAlertaResumo" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_RESUMO'); ?></a>
                            </li>
                            <li >
                                <a class="vdtabAlertaDetalhe" href="#tabAlertaDetalhe" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_DETALHE'); ?></a>
                            </li>
                            <li >
                                <a href="#tabAlertaHistorico" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_HISTORICO'); ?></a>
                            </li>
                            <li >
                                <a href="#tabAlertaTarefas" data-toggle="tab" ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_TAREFAS'); ?>
                                  <span id="vdTabNavTarefasNumAbertas" style="display:none;" title="<?php echo JText::_('COM_VIRTUALDESK_TAREFA_PORRESOLVER'); ?>"
                                        data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefasListaAbertasByProcesso4ManagerByAjax&tipoprocesso=alerta&processo_id=' . $this->escape($this->data->alerta_id)); ?>" >
                                        <!--<i class="icon-bell"></i>-->
                                        <span class="badge badge-danger vdValorNot"></span>
                                  </span>
                                  </span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content">

                        <div class="tab-pane active" id="tabAlertaResumo">
                            <div class="portlet light">
                                <div class="portlet-body ">
                                    <div class="actions">
                                    </div>

                                    <div class="row">
                                        <!-- COL1 -->
                                        <div class="col-md-8">
                                            <div class="portlet light bg-inverse bordered">
                                                <div class="portlet-title "><div class="caption">Dados do Alerta</div></div>
                                                <div class="portlet-body">
                                                    <div class="blocoResumoDadosTagDiv">
                                                        <div class="row static-info ">
                                                            <div class="col-md-6">
                                                                <div class="name"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA' ); ?></div>
                                                                <div class="value"><?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?></div>
                                                            </div>

                                                            <div class="col-md-6 estado">
                                                                <div class="col-md-6">
                                                                    <div class="name"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_ESTADO' ); ?></div>
                                                                    <div class="value estado ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.getNewEstadoName4ManagerByAjax&alerta_id='. $this->escape($this->data->alerta_id)); ?>" >
                                                                        <span class="label <?php echo VirtualDeskSiteAlertaHelper::getAlertaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <?php
                                                                    $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('alerta', 'alterarestado4managers');
                                                                    if($checkAlterarEstado4Managers===true ) : ?>
                                                                        <div class="botaoAlterarEstado text-left">
                                                                            <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                                                        </div>

                                                                        <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                                                            <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                        <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                                                    </div>
                                                                                    <div class="modal-body blocoAlterar2NewEstado">


                                                                                        <div class="form-group">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 ">
                                                                                                    <div class="form-group" style="padding:0">
                                                                                                        <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                                                        <div style="padding:0;">
                                                                                                            <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                                                <?php foreach($AlertaEstadoList2Change as $rowEstado) : ?>
                                                                                                                    <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                                                <?php endforeach; ?>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 ">
                                                                                                    <div class="form-group">
                                                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                                                        <textarea class="form-control Alterar2NewEstadoDesc" rows="5" name="Alterar2NewEstadoDesc"></textarea>
                                                                                                        <!--<input type="text" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" />-->
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                        <!--<div class="form-group blocoIconsMsgAviso">
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 text-center">
                                                                                                    <span> <i class="fa"></i> </span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-md-12 text-center">
                                                                                                    <span class="blocoIconsMsgAviso_Texto"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>-->


                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                                                        <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.sendAlterar2NewEstado4ManagerByAjax&alerta_id=' . $this->escape($this->data->alerta_id)); ?>" >
                                                                                        <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                                                        </button>
                                                                                        <span> <i class="fa"></i> </span>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- /.modal-content -->
                                                                            </div>
                                                                            <!-- /.modal-dialog -->
                                                                        </div>

                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <div class="row static-info ">
                                                            <div class="col-md-6">
                                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_PROCMANAGER' ); ?></div>
                                                                <div class="value ValorProcManagerAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.getNewProcManagerName4ManagerByAjax&alerta_id='. $this->escape($this->data->alerta_id)); ?>" >
                                                                    <?php
                                                                        if(empty($this->data->procmanager_name)){
                                                                            ?>
                                                                                <span style="color:#b5b5b5" class=""><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_PROCMANAGER_VAZIO' ); ?></span>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                                <span style="" class=""> <?php echo htmlentities( $this->data->procmanager_name, ENT_QUOTES, 'UTF-8');?> </span>
                                                                            <?php
                                                                        }
                                                                    ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 text-left">
                                                                <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarProcManagerModalOpen"  type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_PROCMANAGER' ); ?></button>

                                                                <div class="modal fade" id="AlterarProcManagerModal" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_PROCMANAGER_ATUAL' ); ?></h4>
                                                                            </div>
                                                                            <div class="modal-body blocoAlterar2NewProcManager">


                                                                                <div class="form-group">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 ">
                                                                                            <div class="form-group" style="padding:0">
                                                                                                <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_PROCMANAGER_ATUAL' ).$labelseparator; ?></label>
                                                                                                <div style="padding:0;">
                                                                                                    <select name="Alterar2NewProcManagerId" class="bs-select form-control Alterar2NewProcManagerId" data-show-subtext="true">
                                                                                                        <?php foreach($AlertaProcManagerList2Change as $rowProcManager) : ?>
                                                                                                            <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowProcManager['id'],$setencrypt_forminputhidden); ?>" <?php if((int)$rowProcManager['id']==$this->data->id_procmanager) echo 'selected';?> ><?php echo $rowProcManager['name']; ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 ">
                                                                                            <div class="form-group">
                                                                                                <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                                                <input type="text" class="form-control Alterar2NewProcManagerDesc" name="Alterar2NewProcManagerDesc" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group blocoIconsMsgAviso">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 text-center">
                                                                                            <span> <i class="fa"></i> </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 text-center">
                                                                                            <span class="blocoIconsMsgAviso_Texto"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal"><?php echo JText::_('COM_VIRTUALDESK_CLOSE');?></button>
                                                                                <button class="btn green alterar2newprocmanagerSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.sendAlterar2NewProcManager4ManagerByAjax&alerta_id=' . $this->escape($this->data->alerta_id)); ?>">
                                                                                <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                                                </button>
                                                                                <span> <i class="fa"></i> </span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </div>


                                                        </div>


                                                        <div class="row static-info ">
                                                            <div class="col-md-6">
                                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_ENCAMINHADOPARA' ); ?></div>
                                                                <div class="value ValorReencaminharAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.getNewReencaminharUserGroup4ManagerByAjax&alerta_id='. $this->escape($this->data->alerta_id)); ?>" >
                                                                <span style="" class="" >
                                                                    <?php
                                                                    $dataGetReencCounter = 0;
                                                                    if(count($dataDetailGetReencaminhar) == 0){
                                                                        ?>
                                                                            <span style="color:#b5b5b5" class=""><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_ENCAMINHADOPARA_VAZIO' ); ?></span>
                                                                        <?php
                                                                    }

                                                                    foreach ($dataDetailGetReencaminhar as $row) {
                                                                        if($dataGetReencCounter>0) echo ' ; ';
                                                                        echo htmlentities( $row->nome, ENT_QUOTES, 'UTF-8');
                                                                        $dataGetReencCounter++;
                                                                    }
                                                                    ?>
                                                                </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 text-left">
                                                                <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarReencaminharModalOpen" type="button"><i class="fa fa-user"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_ENCAMINHAR' ); ?></button>

                                                                <div class="modal fade" id="AlterarReencaminharModal" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_REENCAMINHAR_TITLE' ); ?></h4>
                                                                            </div>
                                                                            <div class="modal-body blocoAlterar2Reencaminhar">

                                                                                <div class="form-group">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 ">
                                                                                            <div class="form-group" style="padding:0">
                                                                                                <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_REENCAMINHAR_4GROUP' ).$labelseparator; ?></label>
                                                                                                <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="Alterar2ReencaminharGroupId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>
                                                                                                    <select name="Alterar2ReencaminharGroupId[]"  id="Alterar2ReencaminharGroupId" class="form-control input-lg select2-multiple Alterar2ReencaminharGroupId" multiple>
                                                                                                        <?php foreach($AlertaReencaminharGrupoList as $rowReencGrupo) : ?>
                                                                                                            <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowReencGrupo['id'],$setencrypt_forminputhidden); ?>"
                                                                                                                <?php
                                                                                                                /*if(!empty($this->data->groupid)) {
                                                                                                                    if(is_array($this->data->groupid)) {
                                                                                                                        if(in_array($rowReencGrupo['id'],$this->data->groupid) ) echo 'selected';
                                                                                                                    }
                                                                                                                }*/
                                                                                                                ?>
                                                                                                            ><?php echo $rowReencGrupo['name']; ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 ">
                                                                                            <div class="form-group" style="">
                                                                                                <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_REENCAMINHAR_4USER' ).$labelseparator; ?></label>
                                                                                                <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="Alterar2ReencaminharUserId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>

                                                                                                    <select name="Alterar2ReencaminharUserId[]"  id="Alterar2ReencaminharUserId" class="form-control input-lg select2-multiple Alterar2ReencaminharUserId" multiple>
                                                                                                        <?php foreach($AlertaReencaminharUserList as $rowReencUser) : ?>
                                                                                                            <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowReencUser['id'],$setencrypt_forminputhidden); ?>"
                                                                                                                <?php
                                                                                                                /*if(!empty($this->data->userid)) {
                                                                                                                    if(is_array($this->data->userid)) {
                                                                                                                        if(in_array($rowReencUser['id'],$this->data->userid) ) echo 'selected';
                                                                                                                    }
                                                                                                                }*/
                                                                                                                ?>
                                                                                                            ><?php echo $rowReencUser['name']; ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-md-12 ">
                                                                                            <div class="form-group">
                                                                                                <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                                                <input type="text" class="form-control Alterar2ReencaminharDesc" name="Alterar2ReencaminharDesc" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group blocoIconsMsgAviso">
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 text-center">
                                                                                            <span> <i class="fa"></i> </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12 text-center">
                                                                                            <span class="blocoIconsMsgAviso_Texto"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal"><?php echo JText::_('COM_VIRTUALDESK_CLOSE');?></button>
                                                                                <button class="btn green alterar2reencaminharSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.sendAlterar2Reencaminhar4ManagerByAjax&alerta_id=' . $this->escape($this->data->alerta_id)); ?>" >
                                                                                <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                                                </button>
                                                                                <span> <i class="fa"></i> </span>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <div class="row static-info">
                                                            <div class="col-md-6">
                                                                <div class="name"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL' ); ?></div>
                                                                <div class="value"><?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?></div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="name"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_FREGUESIA' ); ?></div>
                                                                <div class="value"><?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?></div>
                                                            </div>
                                                        </div>

                                                        <div class="row static-info">
                                                            <div class="col-md-12">
                                                                <div class="name"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER' ); ?></div>
                                                                <div class="value descricao"><?php echo htmlentities( $this->data->descricao, ENT_QUOTES, 'UTF-8');?></div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>


                                            <div class="portlet light bg-inverse bordered">
                                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LASTMESSAGES'); ?></div></div>
                                                <div class="portlet-body">

                                                    <div class="row rowSendNewMsgText">
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <label><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_MSGAVSOBS'); ?></label>
                                                                <textarea class="form-control sendnewmsgtext" name="sendnewmsgtext" rows="2"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 " style="padding:0;">
                                                            <button class="btn green newmsghistSend" type="button" name"Enviar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.sendNewMsgHist4ManagerByAjax&alerta_id=' . $this->escape($this->data->alerta_id)); ?>"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SENDMESSAGES'); ?></button>
                                                            <span> <i class="fa"></i> </span>
                                                        </div>
                                                    </div>


                                                    <div class="row newmsghistSendOptionsBlock" style="font-size:13px; display:none;">
                                                        <div class="form-group">
                                                            <div class="help-block col-md-3 "  style="padding:0 0px 25px;">
                                                                <span><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TIPO').$labelseparator; ?></span>
                                                                <div style="float: right;">
                                                                <select name="newmsghistSendSetTipo" class="bs-select form-control input-sm newmsghistSendSetTipo" data-show-subtext="true">
                                                                    <?php foreach($TipoSelectbyManager as $rowTipo) : ?>
                                                                        <option value="<?php echo $rowTipo['id']; ?>" <?php if((int)$rowTipo['id']==1) echo 'selected';?>
                                                                            <?php switch((int)$rowTipo['id']) {
                                                                                case 1:
                                                                                    echo ' data-icon="fa-commenting-o" ';
                                                                                    break;
                                                                                case 2:
                                                                                    echo ' data-icon="fa-info-circle" ';
                                                                                    break;
                                                                            }?>
                                                                        ><?php echo $rowTipo['name']; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                                </div>
                                                            </div>

                                                            <div class="help-block col-md-4 "  style="padding:0 5px 25px; text-align:right;">
                                                                <span><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SETVISIBLE4USER_LABEL'); ?> </span>

                                                            </div>
                                                            <div class="help-block col-md-2 "  style="padding:0 0px 25px;">
                                                                <input type="checkbox" class="make-switch newmsghistSendSetVisible4User" name="newmsghistSendSetVisible4User" data-on-text="Sim" data-off-text="Não" data-size="small" data-on-color="info" data-off-color="danger" />
                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <table class="table table-striped table-bordered table-hover order-column wrap " id="tabela_alerta_historico_resumo" >
                                                                <thead>
                                                                <tr>
                                                                    <th ></th>
                                                                    <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_UTILIZADOR'); ?></th>
                                                                    <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TIPO'); ?></th>
                                                                    <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONTEUDO'); ?></th>

                                                                </tr>
                                                                </thead>
                                                            </table>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- COL2 -->
                                        <div class="col-md-4">

                                            <div class="portlet light bg-inverse bordered map">
                                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_MAPA'); ?></div></div>
                                                <div class="portlet-body">

                                                    <div>
                                                        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                                        <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                                                               value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                                    </div>

                                                </div>
                                            </div>


                                            <div class="portlet light bg-inverse bordered attach">
                                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_IMGDOCS'); ?></div></div>
                                                <div class="portlet-body">
                                                    <div id="vdAlertaFileGridResumo" class="cbp">
                                                    <?php
                                                    if(!is_array($ListFilesAlert)) $ListFilesAlert = array();
                                                    foreach ($ListFilesAlert as $rowFile) : ?>
                                                        <div class="cbp-item identity logos">
                                                            <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                                <div class="cbp-caption-defaultWrap">
                                                                    <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                                <div class="cbp-caption-activeWrap">
                                                                    <div class="cbp-l-caption-alignLeft">
                                                                        <div class="cbp-l-caption-body">
                                                                            <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                            <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    <?php endforeach;?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane " id="tabAlertaDetalhe">
                            <div class="portlet light">
                                <div class="portlet-body ">
                                    <div class="actions">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">

                                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_DADOSDETALHE'); ?></div></div>
                                                <div class="portlet-body">


                                                    <div class="col-md-6 referencia">
                                                        <div class="row static-info ">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 estado">
                                                        <div class="row static-info ">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_ESTADO' ); ?></div>
                                                            <div class="value estado"> <span class="label <?php echo VirtualDeskSiteAlertaHelper::getAlertaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 categoria">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 subcategoria">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_SUBCATEGORY_LABEL' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->subcategoria, ENT_QUOTES, 'UTF-8');?> </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12 descricao">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER' ); ?></div>
                                                            <div class="value descricao"> <?php echo htmlentities( $this->data->descricao, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 freguesia">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_FREGUESIA' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 sitio">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_SITIO' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->sitio, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12 local">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_LOCAL' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->local, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 latitude">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_LATITUDE' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->latitude, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 longitude">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_LONGITUDE' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->longitude, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12 ptosRef">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_PTOSREF' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->pontos_referencia, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12 observacoes">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_OBSERVACOES' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->observacoes, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>

                                                </div>



                                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DADOS_USER'); ?></div></div>

                                                <div class="portlet-body">


                                                    <div class="col-md-12 utilizador">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_UTILIZADOR' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->utilizador, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 email">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_EMAIL' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->email, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 nif">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_NIF' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->nif, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 telefone">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_TELEFONE' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->telefone, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>


                                        <div class="col-md-4">

                                            <div class="portlet light bg-inverse bordered">
                                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_MAPA'); ?></div></div>
                                                <div class="portlet-body">

                                                    <div>
                                                        <div id="gmap_marker2" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                                        <input type="hidden" required class="form-control" name="coordenadas2" id="coordenadas2"
                                                               value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="portlet light bg-inverse bordered attach" style="min-height: 395px;">
                                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_IMGDOCS'); ?></div></div>
                                                <div class="portlet-body">

                                                    <div id="vdAlertaFileGridDetalhe" class="cbp">

                                                        <?php
                                                        if(!is_array($ListFilesAlert)) $ListFilesAlert = array();
                                                        foreach ($ListFilesAlert as $rowFile) : ?>

                                                            <div class="cbp-item identity logos">
                                                                <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                                    <div class="cbp-caption-defaultWrap">
                                                                        <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                                    <div class="cbp-caption-activeWrap">
                                                                        <div class="cbp-l-caption-alignLeft">
                                                                            <div class="cbp-l-caption-body">
                                                                                <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                                <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>

                                                        <?php endforeach;?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane " id="tabAlertaHistorico">
                            <div class="portlet light">
                                <div class="portlet-body ">

                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">

                                            <li class="active">
                                                <a href="#tab_HistoricoTimeLine" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_MSGAVS'); ?></a>
                                            </li>
                                            <li>
                                                <a href="#tab_tabela_alerta_historico_full" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_PESQUISARLISTA'); ?></a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">

                                            <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTA_NEWMESSAGE');?></h3>

                                            <div class="col-md-12">
                                                <div class="rowSendNewMsgText">
                                                    <div class="col-md-10 ">
                                                        <textarea class="form-control sendnewmsgtext" name="sendnewmsgtext" rows="5"></textarea>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button class="btn green newmsghistSend" type="button" name"Enviar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.sendNewMsgHist4ManagerByAjax&alerta_id=' . $this->escape($this->data->alerta_id)); ?>"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SENDMESSAGES'); ?></button>
                                                        <span> <i class="fa"></i> </span>
                                                    </div>
                                                </div>

                                                <div class="newmsghistSendOptionsBlock" style="font-size:13px; display:none;">
                                                    <!--<div class="help-block col-md-12">
                                                        <span style="float: left; margin-right:10px; margin-top:7px;"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TIPO').$labelseparator; ?></span>
                                                        <div style="float: left;">
                                                            <select name="newmsghistSendSetTipo" class="bs-select form-control input-sm newmsghistSendSetTipo" data-show-subtext="true">
                                                                <?php foreach($TipoSelectbyManager as $rowTipo) : ?>
                                                                    <option value="<?php echo $rowTipo['id']; ?>" <?php if((int)$rowTipo['id']==1) echo 'selected';?>
                                                                        <?php switch((int)$rowTipo['id']) {
                                                                            case 1:
                                                                                echo ' data-icon="fa-commenting-o" ';
                                                                                break;
                                                                            case 2:
                                                                                echo ' data-icon="fa-info-circle" ';
                                                                                break;
                                                                        }?>
                                                                    ><?php echo $rowTipo['name']; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>-->

                                                    <div class="help-block col-md-12">
                                                        <span><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SETVISIBLE4USER_LABEL'); ?></span>
                                                        <input type="checkbox" class="make-switch newmsghistSendSetVisible4User" name="newmsghistSendSetVisible4User" data-on-text="Sim" data-off-text="Não" data-size="small" data-on-color="info" data-off-color="danger" />
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="row">&nbsp;</div>


                                            <div class="tab-pane active" id="tab_HistoricoTimeLine">
                                                <div id="HistoricoTimeLine" data-vd-url-getcontent="?option=com_virtualdesk&task=alerta.getAlertaHistTimeLine4ManagerByAjax&alerta_id=<?php echo $getInputAlerta_Id; ?>" >
                                                    <div class="TimeLineContent">
                                                    </div>
                                                    <div style="text-align:center;margin-top:50px;"> <i class="fa"></i> </div>
                                                </div>
                                            </div>


                                            <div class="tab-pane" id="tab_tabela_alerta_historico_full">
                                                <table class="table table-striped table-bordered table-hover order-column" id="tabela_alerta_historico_full" style="width:100%" >
                                                    <thead>
                                                    <tr>
                                                        <th ><?php echo JText::_('ID'); ?> </th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_UTILIZADOR'); ?></th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TIPO'); ?></th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONTEUDO'); ?></th>
                                                        <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MANAGER'); ?></th>
                                                    </tr>
                                                    </thead>
                                                </table>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="tab-pane " id="tabAlertaTarefas">
                            <div class="portlet light">
                                <div class="portlet-body ">
                                    <div class="actions">
                                        <div class="text-right" >
                                            <button class="btn btn-circle btn-outline blue btn-sm btAbrirModal btNovaTarefaModalOpen" type="button"><i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA' ); ?></button>
                                        </div>

                                        <div class="modal fade" id="NovaTarefaModal" tabindex="-1" role="basic" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA_TITLE' ); ?></h4>
                                                    </div>
                                                    <div class="modal-body blocoNovaTarefa">

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group" style="">
                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA_4USER' ).$labelseparator; ?></label>
                                                                        <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="NovaTarefaUserId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>

                                                                            <select name="NovaTarefaUserId[]"  id="NovaTarefaUserId" class="form-control input-lg select2-multiple NovaTarefaUserId" multiple>
                                                                                <?php foreach($AlertaNovaTarefaUserList as $rowTrfUser) : ?>
                                                                                    <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowTrfUser['id'],$setencrypt_forminputhidden); ?>"
                                                                                        <?php
                                                                                        /*if(!empty($this->data->userid)) {
                                                                                            if(is_array($this->data->userid)) {
                                                                                                if(in_array($rowTrfUser['id'],$this->data->userid) ) echo 'selected';
                                                                                            }
                                                                                        }*/
                                                                                        ?>
                                                                                    ><?php echo $rowTrfUser['name']; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group" style="padding:0">
                                                                        <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA_4GROUP' ).$labelseparator; ?></label>
                                                                        <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="NovaTarefaGroupId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>
                                                                            <select name="NovaTarefaGroupId[]"  id="NovaTarefaGroupId" class="form-control input-lg select2-multiple NovaTarefaGroupId" multiple>
                                                                                <?php foreach($AlertaNovaTarefaGrupoList as $rowTrfGrupo) : ?>
                                                                                    <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowTrfGrupo['id'],$setencrypt_forminputhidden); ?>"
                                                                                        <?php
                                                                                        /* if(!empty($this->data->groupid)) {
                                                                                             if(is_array($this->data->groupid)) {
                                                                                                 if(in_array($rowReencGrupo['id'],$this->data->groupid) ) echo 'selected';
                                                                                             }
                                                                                         }*/
                                                                                        ?>
                                                                                    ><?php echo $rowTrfGrupo['name']; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group">
                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_TAREFA_TITULO' ); ?></label>
                                                                        <input type="text" class="form-control NovaTarefaNome" name="NovaTarefaNome" />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group">
                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                        <textarea type="text" class="form-control NovaTarefaDesc" name="NovaTarefaDesc"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group blocoIconsMsgAviso">
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span> <i class="fa"></i> </span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span class="blocoIconsMsgAviso_Texto"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal"><?php echo JText::_('COM_VIRTUALDESK_CLOSE');?></button>
                                                        <button class="btn green novatarefaSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.sendNovaTarefa4ManagerByAjax&tipoprocesso=alerta&processo_id=' . $this->escape($this->data->alerta_id)); ?>" >
                                                        <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                        </button>
                                                        <span> <i class="fa"></i> </span>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>


                                    </div>

                                    <table class="table table-striped table-bordered table-hover order-column" id="tabela_alerta_tarefas_full"  style="width: 100%;">
                                        <thead>
                                        <tr>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAREFA_TITULO'); ?></th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER'); ?></th>
                                            <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO'); ?></th>
                                            <th ></th>

                                        </tr>
                                        </thead>
                                    </table>


                                    <div class="modal fade" id="TarefaAlterarEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_ALTERARESTADO_TITLE' ); ?></h4>
                                                </div>
                                                <div class="modal-body blocoTarefaAlterarEstado"
                                                     data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefaAlterarEstadoByProcesso4ManagerByAjax&tipoprocesso=alerta&processo_id=' . $this->escape($this->data->alerta_id)); ?>" >

                                                    <div class="form-group">

                                                        <div class="row">
                                                            <div class="col-md-12 static-info">
                                                                <div class="form-group">
                                                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_TAREFA_TITULO' ).$labelseparator; ?></label>
                                                                    <span class="value ValorEstadoAtualStatic TarefaAlterarEstadoNome"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 static-info">
                                                                <div class="form-group">
                                                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_DESCRICAO' ).$labelseparator; ?></label>
                                                                    <span class="value ValorEstadoAtualStatic TarefaAlterarEstadoDesc"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group" style="padding:0">
                                                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                    <div style="padding:0;">
                                                                        <select name="TarefaAlterarNewEstadoId" class="bs-select form-control TarefaAlterarNewEstadoId" data-show-subtext="true">
                                                                            <?php foreach($TarefasEstadoList as $rowTrfEstado) : ?>
                                                                                <option value="<?php echo $rowTrfEstado['id']; ?>" ><?php echo $rowTrfEstado['name']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group">
                                                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_OBS_OPCIONAL' ); ?></label>
                                                                    <textarea type="text" class="form-control TarefaAlterarObs" name="TarefaAlterarObs"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-group blocoIconsMsgAviso">
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <span> <i class="fa"></i> </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <span class="blocoIconsMsgAviso_Texto"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                    <button class="btn green tarefaalterarestadoSend" type="button" name"Alterar"
                                                    data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.sendTarefaAlterarEstadoByProcesso4ManagerByAjax&tipoprocesso=alerta&processo_id=' . $this->escape($this->data->alerta_id)); ?>" >
                                                    <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                    </button>
                                                    <span> <i class="fa"></i> </span>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>



                                    <div class="modal fade" id="TarefaEditarModal" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_EDITAR_TITLE' ); ?></h4>
                                                </div>
                                                <div class="modal-body blocoTarefaEditar"
                                                     data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefaDetalheByProcesso4ManagerByAjax&tipoprocesso=alerta&processo_id=' . $this->escape($this->data->alerta_id)); ?>" >

                                                    <div class="form-group">

                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group">
                                                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_TAREFA_TITULO' ); ?></label>
                                                                    <input type="text" class="form-control TarefaEditarNome" name="TarefaEditarNome" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group">
                                                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                    <textarea class="form-control TarefaEditarDesc" name="TarefaEditarDesc"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group" style="padding:0">
                                                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_ESTADO' ).$labelseparator; ?></label>
                                                                    <div style="padding:0;">
                                                                        <select name="TarefaEditarEstadoId" class="bs-select form-control TarefaEditarEstadoId" data-show-subtext="true">
                                                                            <?php foreach($TarefasEstadoList as $rowTrfEstado) : ?>
                                                                                <option value="<?php echo $rowTrfEstado['id']; ?>" ><?php echo $rowTrfEstado['name']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group" style="">
                                                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA_4USER' ).$labelseparator; ?></label>
                                                                    <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="TarefaEditarUserId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>
                                                                        <select name="TarefaEditarUserId[]"  id="TarefaEditarUserId" class="form-control input-lg select2-multiple TarefaEditarUserId" multiple>
                                                                            <?php foreach($AlertaTarefaEditarUserList as $rowTrfUser) : ?>
                                                                                <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowTrfUser['id'],$setencrypt_forminputhidden); ?>"
                                                                                ><?php echo $rowTrfUser['name']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="form-group" style="padding:0">
                                                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_NOVATAREFA_4GROUP' ).$labelseparator; ?></label>
                                                                    <div class="input-group select2-bootstrap-prepend">
                                                                                            <span class="input-group-btn">
                                                                                                <button class="btn btn-default" type="button" data-select2-open="TarefaEditarGroupId">
                                                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                                                </button>
                                                                                            </span>
                                                                        <select name="TarefaEditarGroupId[]"  id="TarefaEditarGroupId" class="form-control input-lg select2-multiple TarefaEditarGroupId" multiple>
                                                                            <?php foreach($AlertaTarefaEditarGrupoList as $rowTrfGrupo) : ?>
                                                                                <option value="<?php echo $obVDCrypt->setIdInputValueEncrypt($rowTrfGrupo['id'],$setencrypt_forminputhidden); ?>"
                                                                                ><?php echo $rowTrfGrupo['name']; ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="form-group blocoIconsMsgAviso">
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <span> <i class="fa"></i> </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <span class="blocoIconsMsgAviso_Texto"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal"><?php echo JText::_('COM_VIRTUALDESK_CLOSE');?></button>
                                                    <button class="btn green tarefaeditarSend" type="button" name"Alterar"
                                                        data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.sendTarefaEditarByProcesso4ManagerByAjax&tipoprocesso=alerta&processo_id=' . $this->escape($this->data->alerta_id)); ?>" >
                                                    <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                    </button>
                                                    <span> <i class="fa"></i> </span>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>



                                    <div class="modal fade bs-modal-lg" id="TarefaHistoricoModal" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_HIST_TITLE' ); ?></h4>
                                                </div>
                                                <div class="modal-body blocoTarefaHistorico"
                                                     data-vd-url-getcontent="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=tarefa.getTarefaHistoricoByProcesso4ManagerByAjax&tipoprocesso=alerta&processo_id=' . $this->escape($this->data->alerta_id)); ?>" >

                                                    <div class="form-group">

                                                        <div class="row">
                                                            <div class="col-md-12 static-info">
                                                                <div class="form-group">
                                                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_TAREFA_TITULO' ).$labelseparator; ?></label>
                                                                    <span class="value ValorEstadoAtualStatic TarefaHistoricoNome"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 static-info">
                                                                <div class="form-group">
                                                                    <label class="name text-right"><?php echo JText::_( 'COM_VIRTUALDESK_TAREFA_DESCRICAO' ).$labelseparator; ?></label>
                                                                    <span class="value ValorEstadoAtualStatic TarefaHistoricoDesc"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <table class="table table-striped table-bordered table-hover order-column" id="tabela_alerta_tarefa_historico_modal" style="width:100%" >
                                                                <thead>
                                                                <tr>
                                                                    <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                                </thead>
                                                            </table>
                                                        </div>


                                                    </div>

                                                    <div class="form-group blocoIconsMsgAviso">
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <span> <i class="fa"></i> </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <span class="blocoIconsMsgAviso_Texto"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <span> <i class="fa"></i> </span>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="form-actions">
                    <div class="col-md-12">
                        <a class="btn submit" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=edit4manager&alerta_id=' . $this->escape($this->data->alerta_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('alerta_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->alerta_id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('alerta_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('alerta_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/maps.js.php');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/view4manager.js.php');
echo ('</script>');
?>