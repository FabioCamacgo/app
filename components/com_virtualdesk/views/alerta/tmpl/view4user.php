<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
    JLoader::register('VirtualDeskSiteAlertaFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertafields.php');
    JLoader::register('VirtualDeskSiteAlertsFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertsfiles.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkDetailReadAccess('alerta');
    if( $vbHasAccess===false ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');

    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputAlerta_Id = JFactory::getApplication()->input->getInt('alerta_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAlertaHelper::getAlertaView4UserDetail($getInputAlerta_Id);


    if(empty($this->data->latitude)){
        $this->data->latitude = 32.71669086705518;
        $this->data->longitude = -17.099513263315096;
    }

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
        $objAlertFiles = new VirtualDeskSiteAlertsFilesHelper();
        $ListFilesAlert = $objAlertFiles->getFileGuestLinkByRefId ($this->data->codigo);
    }

    // Definição dos campos de utilizador
    $ObjalertaFields = new VirtualDeskSiteAlertaFieldsHelper();
    $aralertaFieldsConfig = $ObjalertaFields->getFields();

    $itemmenuid_lista = $params->get('alerta_menuitemid_list');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>


<div class="portlet light bordered form-fit">
    <div class="portlet-title">

        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_VER_DETALHE' ); ?></span>
        </div>


        <?php
        //Se tem DADOS VAZIOS -> Apresenta mensagem e sai...
        if(empty($this->data)) : ?>
        <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <div class="portlet-body form">
            <div class="form-actions right">
                <div class="row">

                    <div  class="col-md-12 text-left">
                        <div class="alert alert-info fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                            <?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMPTYLIST'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php exit(); ?>
        <?php endif;  ?>


        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=edit4user&vdcleanstate=1&alerta_id=' . $this->escape($this->data->alerta_id)); ?>" class="btn btn-circle btn-outline green vdBotaoEditar">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=addnew4user&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_ADDNEW' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->

    </div>


    <div class="portlet-body form">
        <form class="form-horizontal">
            <div class="form-body">
                <div class="tabbable-line nav-justified ">
                    <ul class="nav nav-tabs vdMainNavTab">
                        <li class="active">
                            <a href="#tabAlertaResumo" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_RESUMO'); ?></a>
                        </li>
                        <li >
                            <a class="vdtabAlertaDetalhe" href="#tabAlertaDetalhe" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_DETALHE'); ?></a>
                        </li>
                        <li >
                            <a href="#tabAlertaHistorico" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_HISTORICO'); ?></a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">

                    <div class="tab-pane active" id="tabAlertaResumo">
                        <div class="portlet light">
                            <div class="portlet-body ">
                                <div class="actions">
                                </div>

                                <div class="row">
                                    <!-- COL1 -->
                                    <div class="col-md-8">
                                        <div class="portlet light bg-inverse bordered">
                                            <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_DADOSDETALHE'); ?></div></div>
                                            <div class="portlet-body">

                                                <div class="blocoResumoDadosTagDiv">
                                                    <div class="row static-info ">
                                                        <div class="col-md-6">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                                        </div>


                                                        <div class="col-md-6 estado">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_ESTADO' ); ?></div>
                                                            <div class="value estado"> <span class="label <?php echo VirtualDeskSiteAlertaHelper::getAlertaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-6">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_FREGUESIA' ); ?></div>
                                                            <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>

                                                    <div class="row static-info">
                                                        <div class="col-md-12">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER' ); ?></div>
                                                            <div class="value descricao"> <?php echo htmlentities( $this->data->descricao, ENT_QUOTES, 'UTF-8');?></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="portlet light bg-inverse bordered">
                                        <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LASTMESSAGES'); ?></div></div>
                                        <div class="portlet-body">

                                        <div class="row rowSendNewMsgText">
                                           <div class="col-md-9 ">
                                                <input class="form-control sendnewmsgtext" type="text" name="sendnewmsgtext" />
                                           </div>
                                           <div class="col-md-3 " style="padding:0;">
                                               <button class="btn green newmsghistSend" type="button" name"Enviar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.sendNewMsgHist4UserByAjax&alerta_id=' . $this->escape($this->data->alerta_id)); ?>"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SENDMESSAGES'); ?></button>
                                               <span> <i class="fa"></i> </span>
                                           </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                        <table class="table table-striped table-bordered table-hover order-column wrap " id="tabela_alerta_historico_resumo" >
                                            <thead>
                                            <tr>
                                                <th ></th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_UTILIZADOR'); ?></th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TIPO'); ?></th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONTEUDO'); ?></th>
                                            </tr>
                                            </thead>
                                        </table>
                                            </div>


                                        </div>
                                        </div>
                                        </div>

                                    </div>

                                    <!-- COL2 -->
                                    <div class="col-md-4">

                                        <div class="portlet light bg-inverse bordered">
                                        <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_MAPA'); ?></div></div>
                                        <div class="portlet-body">

                                        <div>
                                            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                            <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                                                   value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                        </div>

                                        </div>
                                        </div>


                                        <div class="portlet light bg-inverse bordered">
                                        <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_IMGDOCS'); ?></div></div>
                                        <div class="portlet-body">
                                            <div id="vdAlertaFileGridResumo" class="cbp">
                                                <?php
                                                if(!is_array($ListFilesAlert)) $ListFilesAlert = array();
                                                foreach ($ListFilesAlert as $rowFile) : ?>
                                                    <div class="cbp-item identity logos">
                                                        <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                            <div class="cbp-caption-activeWrap">
                                                                <div class="cbp-l-caption-alignLeft">
                                                                    <div class="cbp-l-caption-body">
                                                                        <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                        <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane " id="tabAlertaDetalhe">
                        <div class="portlet light">
                            <div class="portlet-body ">
                                <div class="actions">
                                </div>

                                <div class="row">
                                    <!-- COL1 -->
                                    <div class="col-md-8">
                                        <div class="portlet light bg-inverse bordered " style="min-height: 240px;">
                                        <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_DADOSDETALHE'); ?></div></div>
                                        <div class="portlet-body">

                                            <div class="col-md-6 referencia">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>


                                            <div class="col-md-6 estado">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_ESTADO' ); ?></div>
                                                    <div class="value estado"> <span class="label <?php echo VirtualDeskSiteAlertaHelper::getAlertaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>
                                                </div>
                                            </div>


                                            <div class="col-md-6 categoria">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 subcategoria">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_SUBCATEGORY_LABEL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->subcategoria, ENT_QUOTES, 'UTF-8');?> </div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 descricao">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_DESCRICAOUSER' ); ?></div>
                                                    <div class="value descricao"> <?php echo htmlentities( $this->data->descricao, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 freguesia">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_FREGUESIA' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 sitio">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_SITIO' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->sitio, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 local">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_LOCAL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->local, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 latitude">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_LATITUDE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->latitude, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 longitude">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_LONGITUDE' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->longitude, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 ptosRef">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_PTOSREF' ); ?></div>
                                                    <div class="value descricao"> <?php echo htmlentities( $this->data->pontos_referencia, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 observacoes">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA_OBSERVACOES' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->observacoes, ENT_QUOTES, 'UTF-8');?></div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <!-- COL2 -->
                                    <div class="col-md-4">


                                        <div class="portlet light bg-inverse bordered">
                                            <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_MAPA'); ?></div></div>
                                            <div class="portlet-body">

                                                <div>
                                                    <div id="gmap_marker2" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                                    <input type="hidden" required class="form-control" name="coordenadas2" id="coordenadas2"
                                                           value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="portlet light bg-inverse bordered" style="min-height: 240px;">
                                        <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_IMGDOCS'); ?></div></div>
                                        <div class="portlet-body">


                                            <div id="vdAlertaFileGridDetalhe" class="cbp">

                                                <?php
                                                if(!is_array($ListFilesAlert)) $ListFilesAlert = array();
                                                foreach ($ListFilesAlert as $rowFile) : ?>

                                                    <div class="cbp-item identity logos">
                                                        <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                            <div class="cbp-caption-defaultWrap">
                                                                <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                            <div class="cbp-caption-activeWrap">
                                                                <div class="cbp-l-caption-alignLeft">
                                                                    <div class="cbp-l-caption-body">
                                                                        <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                        <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>

                                                <?php endforeach;?>
                                            </div>


                                        </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane " id="tabAlertaHistorico">
                        <div class="portlet light">
                            <div class="portlet-body ">

                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <a href="#tab_HistoricoTimeLine" data-toggle="tab">Mensagens e Avisos</a>
                                        </li>
                                        <li>
                                            <a href="#tab_tabela_alerta_historico_full" data-toggle="tab">Pesquisar na Lista</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">

                                        <h3><?php echo JText::_('COM_VIRTUALDESK_ALERTA_NEWMESSAGE');?></h3>

                                        <div class="col-md-12 ">
                                            <div class="rowSendNewMsgText">
                                                <div class="col-md-10 ">
                                                    <textarea class="form-control sendnewmsgtext" name="sendnewmsgtext" rows="5"></textarea>
                                                </div>
                                                <div class="col-md-2">
                                                    <button class="btn newmsghistSend" type="button" name"Enviar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.sendNewMsgHist4UserByAjax&alerta_id=' . $this->escape($this->data->alerta_id)); ?>"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SENDMESSAGES'); ?></button>
                                                    <span> <i class="fa"></i> </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">&nbsp;</div>

                                        <div class="tab-pane active" id="tab_HistoricoTimeLine">
                                            <div id="HistoricoTimeLine" data-vd-url-getcontent="?option=com_virtualdesk&task=alerta.getAlertaHistTimeLine4UserByAjax&alerta_id=<?php echo $getInputAlerta_Id; ?>" >
                                                <div class="TimeLineContent">
                                                </div>
                                                <div style="text-align:center;margin-top:50px;"> <i class="fa"></i> </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane" id="tab_tabela_alerta_historico_full">
                                            <table class="table table-striped table-bordered table-hover order-column" id="tabela_alerta_historico_full" style="width:100%" >
                                                <thead>
                                                <tr>
                                                    <th ><?php echo JText::_('ID'); ?> </th>
                                                    <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_UTILIZADOR'); ?></th>
                                                    <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TIPO'); ?></th>
                                                    <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONTEUDO'); ?></th>
                                                    <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MANAGER'); ?></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-actions">
                <div class="col-md-12">
                    <a class="btn submit vdBotaoEditar" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=edit4user&alerta_id=' . $this->escape($this->data->alerta_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                    <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('alerta_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->alerta_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('alerta_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('alerta_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

        </form>
    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/maps.js.php');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/view4user.js.php');
echo ('</script>');
?>