<?php

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
    JLoader::register('VirtualDeskSiteAlertaFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertafields.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteAlertsFilesHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_alerts_files.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
        break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    $localScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator    = ' : ';
    $params            = JComponentHelper::getParams('com_virtualdesk');
    $getInputAlerta_Id = JFactory::getApplication()->input->getInt('alerta_id');


    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAlertaHelper::getAlertaView4ManagerDetail($getInputAlerta_Id);
    if( empty($this->data) ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
        return false;
    }

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit4manager.alerta.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome       =  $UserProfileData->name;
    $email      = $UserProfileData->email;
    $email2     = $UserProfileData->email;
    $fiscalid   = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone   = $UserProfileData->phone1;
    $obParam    = new VirtualDeskSiteParamsHelper();

    $concelhoAlerta      = $obParam->getParamsByTag('alertaConcelho');
    $espera              = $obParam->getParamsByTag('espera');
    $indicativoConcelho  = $obParam->getParamsByTag('AlertaIndConcelho');

    $linkPolitica = $obParam->getParamsByTag('linkPoliticaAlerta');


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>


<div class="portlet light bordered">
    <div class="portlet-title">

        <div class="caption">
            <i class="icon-pointer  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_EDIT' ); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>

    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <?php
        // Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
        ?>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body form">

        <form id="form-new_alerta"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.update4manager'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="form-body">

                <div class="headline">
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA'); ?>
                    <span><?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8'); ?></span>
                </div>

                <div class="bloco">

                    <!--   CATEGORIA   -->
                    <div class="form-group field categoria">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL'); ?><span class="required">*</span></label>
                        <?php $categorias = VirtualDeskSiteAlertaHelper::getCategorias($fileLangSufix, $concelhoAlerta)?>
                        <div class="value col-md-12">
                            <select name="categoria" required value="" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <?php foreach($categorias as $rowStatus) : ?>
                                <option value="<?php echo $rowStatus['ID_categoria']; ?>"
                                    <?php if((int)$this->data->id_categoria == (int)$rowStatus['ID_categoria']) echo('selected'); ?>
                                    ><?php echo $rowStatus['name_PT']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>


                    <!--   SUBCATEGORIA   -->
                    <?php
                    $ListaDeMenuMain = array();
                    if(!empty($this->data->id_categoria)) {
                        if((int)$this->data->id_categoria > 0) $ListaDeMenuMain = VirtualDeskSiteAlertaHelper::getAlertaSubcategoria($this->data->id_categoria);
                    }
                    ?>
                    <div id="blocoMenuMain" class="form-group field subcategoria">
                        <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_SUBCATEGORY_LABEL' ); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <select name="vdmenumain" id="vdmenumain" value="" required
                                <?php if(empty($this->data->id_categoria)) { echo 'disabled'; }  ?>
                                    class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                <?php foreach($ListaDeMenuMain as $rowWSL) : ?>
                                    <option value="<?php echo $rowWSL['id']; ?>"
                                        <?php if((int)$this->data->id_subcategoria == (int)$rowWSL['id']) echo('selected'); ?>
                                    ><?php echo $rowWSL['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>

                        </div>
                    </div>


                    <!--   DESCRIÇAO   -->
                    <div class="form-group field descricao">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DESCRIPTION'); ?></label>
                        <div class="value col-md-12">
                            <textarea  class="form-control" rows="5" name="descricao" id="descricao" maxlength="2000"><?php echo htmlentities( $this->data->descricao, ENT_QUOTES, 'UTF-8'); ?></textarea>
                        </div>
                    </div>


                    <!--   FREGUESIAS   -->
                    <div class="form-group field freguesia">
                        <label class="name col-md-12" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_FREGUESIAS_LABEL') ?><span class="required">*</span></label>
                        <?php $AlertaFreguesias = VirtualDeskSiteAlertaHelper::getAlertaFreguesia($concelhoAlerta)?>
                        <?php $AlertaFreguesiasID = VirtualDeskSiteAlertaHelper::getAlertaFreguesiaID($getInputAlerta_Id);?>
                        <div class="value col-md-12">
                            <select name="freguesias" value="" id="freguesias" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <?php
                                if(empty($this->data->id_freguesia)){
                                    ?>
                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                    <?php foreach($AlertaFreguesias as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id']; ?>"
                                        ><?php echo $rowWSL['freguesia']; ?></option>
                                    <?php endforeach;
                                } else {
                                    ?>
                                    <option value="<?php echo $this->data->id_freguesia; ?>"><?php echo VirtualDeskSiteAlertaHelper::getFregName($this->data->id_freguesia) ?></option>
                                    <?php $ExcludeFreg = VirtualDeskSiteAlertaHelper::excludeFreguesia($concelhoAlerta, $this->data->id_freguesia)?>
                                    <?php foreach($ExcludeFreg as $rowWSL) : ?>
                                        <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                            <?php if((int)$this->data->id_freguesia == (int)$rowWSL['id_freguesia']) echo('selected'); ?>
                                        ><?php echo $rowWSL['freguesia']; ?></option>
                                    <?php endforeach;
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <!--   SÍTIO   -->
                    <div class="form-group field sitio">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SITIO'); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <input type="text" required class="form-control" name="sitio" id="sitio" maxlength="250" value="<?php echo htmlentities( $this->data->sitio, ENT_QUOTES, 'UTF-8'); ?>"/>
                        </div>
                    </div>


                    <!--   MORADA   -->
                    <div class="form-group field morada">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MORADA'); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <input type="text" required class="form-control" name="morada" id="morada" maxlength="250" value="<?php echo htmlentities( $this->data->local, ENT_QUOTES, 'UTF-8'); ?>"/>
                        </div>
                    </div>


                    <!--   PONTOS DE REFERÊNCIA   -->
                    <div class="form-group field ptosRef">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_REFS'); ?><span class="required">*</span></label>
                        <div class="value col-md-12"><textarea required class="form-control" rows="5" name="ptosrefs" id="ptosrefs" maxlength="2000"><?php echo htmlentities( $this->data->pontos_referencia, ENT_QUOTES, 'UTF-8'); ?></textarea>
                        </div>
                    </div>


                    <!--   MAPA   -->
                    <div class="form-group field mapa">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MAPA'); ?></label>
                        <div class="value col-md-12">
                            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;height:500px;"></div>

                            <input type="hidden" required class="form-control"
                                   name="coordenadas" id="coordenadas"
                                   value="<?php echo htmlentities( $this->data->latitude, ENT_QUOTES, 'UTF-8'); ?>,<?php echo htmlentities( $this->data->longitude, ENT_QUOTES, 'UTF-8'); ?>"/>
                        </div>
                    </div>


                    <!--   Upload Imagens   -->
                    <div class="form-group" id="uploadField">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_UPLOAD'); ?></label>
                        <div class="value col-md-12">
                            <div class="file-loading">
                                <input type="file" name="fileupload[]" id="fileupload" multiple>
                            </div>
                            <div id="errorBlock" class="help-block"></div>
                        </div>
                        <input type="hidden" name="vdFileUpChanged" value="0">
                    </div>
                </div>

                <div class="bloco">

                    <legend><h3 class="dadosPessoais"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DADOSPESSOAIS'); ?></h3></legend>


                    <!--   NOME COMPLETO   -->
                    <div class="form-group field name">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_NOME'); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <input type="text" required class="form-control" name="nome" id="nome" readonly value="<?php echo htmlentities($this->data->utilizador, ENT_QUOTES, 'UTF-8'); ?>" maxlength="40"/>
                        </div>
                    </div>

                    <!--   EMAIL   -->

                    <div class="form-group field mail">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_LABEL'); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <input type="text" class="form-control" required name="email" id="email" readonly value="<?php echo $this->data->email; ?>"/>
                        </div>
                    </div>

                    <!--   VERIFICAÇÃO DE EMAIL   -->
                    <div class="form-group field mail2">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_EMAIL_REPEAT_LABEL'); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <input type="text" class="form-control" required name="emailConf" id="emailConf" readonly value="<?php echo $this->data->email; ?>"/>
                        </div>
                    </div>

                    <!--   NIF   -->
                    <div class="form-group field nif">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_FISCALID_LABEL'); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <input type="text" class="form-control" autocomplete="off" required name="fiscalid" id="fiscalid" readonly maxlength="9" value="<?php echo $this->data->nif; ?>"/>
                        </div>
                    </div>

                    <!--   TELEFONE  -->
                    <div class="form-group field telef">
                        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TELEFONE_LABEL'); ?><span class="required">*</span></label>
                        <div class="value col-md-12">
                            <input type="text" class="form-control" autocomplete="off" required name="telefone" id="telefone" readonly maxlength="9" value="<?php echo $this->data->telefone; ?>"/>
                        </div>
                    </div>

                </div>

            </div>


            <div class="form-actions">
                <div class="col-md-12">
                    <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                    </button>
                    <a class="btn default"
                       href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>"
                       title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('alerta_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->alerta_id) ,$setencrypt_forminputhidden); ?>"/>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('alerta.update4manager',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


            <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/edit4manager.js.php');
echo ('</script>');

?>