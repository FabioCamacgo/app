<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

$app    = JFactory::getApplication();
$redirectLayout = 'index.php?option=com_virtualdesk&view=alerta&layout=addnew4user';

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by VirtualDesk
$controller = JControllerLegacy::getInstance('VirtualDesk');

// Redirect
$controller->setRedirect(JRoute::_($redirectLayout));

// Redirect if set by the controller
$controller->redirect();
?>
ND