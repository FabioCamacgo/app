<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
var ConfCategoriaEdit = function() {

    var handleConfCategoriaEdit = function() {

        jQuery('#form-confcategoria').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   

                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = MessageAlert.getRequiredMissed(errors);
                        var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                        MainMessageBlock.find('span').html(message);
                        MainMessageBlock.show();
                        App.scrollTo(MainMessageBlock, -200);
                    }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

              if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                 form.submit();
            }
        });

        jQuery('#form-confcategoria').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#form-confcategoria').validate().form()) {
                    jQuery('#form-confcategoria').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleConfCategoriaEdit();
        }
    };

}();


/*
* Inicialização da validação
*/
jQuery(document).ready(function() {

   // Validações
    ConfCategoriaEdit.init();
});