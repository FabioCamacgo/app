<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_stats.php');
JLoader::register('VirtualDeskSiteContactUsReportsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus_reports.php');
JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
JLoader::register('VirtualDeskSiteAlertaStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta_stats.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
$vbHasAccessCfg  = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
if($vbHasAccess===false || $vbInGroupAM ==false || $vbHasAccessCfg==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;

$headCSS = $addcss_ini . $baseurl . 'components/com_virtualdesk/views/alerta/tmpl/list4manager.css' . $addcss_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');

// Alerta - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/alerta/tmpl/alerta-comum.css');


$ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
$userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();


echo $headCSS;

?>
<style>

 .CategoriasFilterDropBox{
     padding-top:20px;
     padding-bottom:10px;
     float: right;
     margin-right: 50px
 }
 .ConcelhoFilterDropBox{
     padding-top:20px;
     padding-bottom:10px;
     float: right;
     margin-right: 120px
 }
</style>

<div class="portlet light bordered alerta">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CONFLIST' ); ?></span>
        </div>
        <div class="actions">

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>
    </div>

    <div class="portlet-body">


        <div class="tabbable-line nav-justified ">

            <ul class="nav nav-tabs  ">

                <li class="active">
                    <a href="#tab_Alerta_Conf_Categorias" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_TAB_CATEGORIAS'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Alerta_Conf_SubCategorias" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_TAB_SUBCATEGORIAS'); ?>
                        </h4>
                    </a>
                </li>

            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_Alerta_Conf_Categorias">
                    <div class="portlet light">
                        <div class="portlet-body ">
                            <div class="actions">

                                <div class="col-md-8" >
                                </div>
                                <div class="col-md-2" >

                                </div>
                                <div class="col-md-2 text-right" style="padding-bottom: 10px;padding-top: -5px;">
                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=addnewconfcategoria4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
                                </div>

                            </div>
                            <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_categorias">
                                <thead>
                                <tr>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOME'); ?> </th>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA'); ?> </th>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_CONCELHO'); ?> </th>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_EMAIL'); ?> </th>
                                    <th> </th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>


                <div class="tab-pane" id="tab_Alerta_Conf_SubCategorias">
                    <div class="portlet light">
                        <div class="portlet-body ">
                            <div class="actions">

                                <div class="col-md-8" >
                                </div>
                                <div class="col-md-2" >

                                </div>
                                <div class="col-md-2 text-right" style="padding-bottom: 10px;padding-top: -5px;">
                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=addnewconfsubcategoria4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
                                </div>

                            </div>
                            <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_subcategorias">
                                <thead>
                                <tr>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOME'); ?> </th>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_CATEGORIA'); ?> </th>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_CONCELHO'); ?> </th>
                                    <th> <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_EMAIL'); ?> </th>
                                    <th> </th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>


            </div>


        </div>

    </div>
</div>





<?php

echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/listconf4manager.js.php');
echo ('</script>');

?>



