<?php

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
    JLoader::register('VirtualDeskSiteAlertaFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alertafields.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('alerta');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'addnew4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
        break;
    }

    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';
    $addcss_ini    = '<link href="';
    $addcss_end    = '" rel="stylesheet" />';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.alerta.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
}

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email    = $UserProfileData->email;
    $email2   = $UserProfileData->email;
    $fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone = $UserProfileData->phone1;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $concelhoAlerta     = $obParam->getParamsByTag('alertaConcelho');
    $espera             = $obParam->getParamsByTag('espera');
    $indicativoConcelho = $obParam->getParamsByTag('AlertaIndConcelho');
    $linkPolitica       = $obParam->getParamsByTag('linkPoliticaAlerta');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    echo $headCSS;

?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>
    </div>

    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body form">

        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=list4manager#tab_Alerta_PedidosSubmetidos'); ?>" >
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_PEDIDOSSUBMETIDOS'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=list4manager#tab_Alerta_PedidosSubmetidosMapa'); ?>" >
                        <h4>
                            <i class="fa fa-map-marker"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_PEDIDOSSUBMETIDOSMAPA'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=list4manager#tab_Alertar_Estatistica'); ?>" >
                        <h4>
                            <i class="fa fa-bar-chart"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                        </h4>
                    </a>
                </li>

                <li class="active">
                    <a href="#tab_Alerta_NovaOcorrencia" >
                        <h4>
                            <i class="fa fa-plus"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_NOVAOCORRENCIA'); ?>
                        </h4>
                    </a>
                </li>

            </ul>

            <div class="tab-content">

                <div class="tab-pane " id="tab_Alerta_PedidosSubmetidos">
                </div>

                <div class="tab-pane " id="tab_Alerta_Estatistica">
                </div>

                <div class="tab-pane active" id="tab_Alerta_NovaOcorrencia">

                    <form id="form-new_alerta"
                          action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.create4manager'); ?>" method="post"
                          class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                          role="form">

                        <div class="bloco">

                            <!--   CATEGORIA   -->
                            <div class="form-group field categoria">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL'); ?><span class="required">*</span></label>
                                <?php $categorias = VirtualDeskSiteAlertaHelper::getCategorias($fileLangSufix,$concelhoAlerta)?>
                                <div class="value col-md-12">
                                    <select name="categoria" required value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($categoria)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                            <?php foreach($categorias as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['ID_categoria']; ?>"
                                                ><?php echo $rowStatus['name_PT']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteAlertaHelper::getCatSelect($categoria,$concelhoAlerta) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeCat = VirtualDeskSiteAlertaHelper::excludeCat($categoria,$concelhoAlerta)?>
                                            <?php foreach($ExcludeCat as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['ID_categoria']; ?>"
                                                ><?php echo $rowStatus['name_PT']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <!--   SUBCATEGORIA   -->
                            <?php
                            $ListaDeMenuMain = array();
                            if(!empty($categoria)) {
                                if( (int) $categoria > 0) $ListaDeMenuMain = VirtualDeskSiteAlertaHelper::getAlertaSubcategoria($categoria);
                            }
                            ?>
                            <div id="blocoMenuMain" class="form-group field subcategoria">
                                <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_SUBCATEGORY_LABEL' ); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <select name="vdmenumain" id="vdmenumain" value="<?php echo $subcategoria;?>" required
                                        <?php
                                        if(empty($categoria)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($subcategoria)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                            <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $subcategoria; ?>"><?php echo VirtualDeskSiteAlertaHelper::getSubCatName($subcategoria);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeSubCat = VirtualDeskSiteAlertaHelper::excludesubCat($categoria, $subcategoria);?>
                                            <?php foreach($ExcludeSubCat as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>


                            <!--   DESCRIÇAO   -->
                            <div class="form-group field descricao">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_DESCRIPTION'); ?></label>
                                <div class="value col-md-12">
                                    <textarea class="form-control" rows="5" name="descricao" id="descricao" maxlength="2000"><?php echo $descricao; ?></textarea>
                                </div>
                            </div>


                            <!--   FREGUESIAS   -->
                            <div class="form-group field freguesia">
                                <label class="name col-md-12" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_FREGUESIAS_LABEL') ?><span class="required">*</span></label>
                                <?php $AlertaFreguesias = VirtualDeskSiteAlertaHelper::getAlertaFreguesia($concelhoAlerta)?>
                                <div class="value col-md-12">
                                    <select name="freguesias" value="" id="freguesias"  value="<?php echo $freguesias; ?>" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($freguesias)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                            <?php foreach($AlertaFreguesias as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['freguesia']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $freguesias; ?>"><?php echo VirtualDeskSiteAlertaHelper::getFregName($freguesias) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeFreg = VirtualDeskSiteAlertaHelper::excludeFreguesia($concelhoAlerta, $freguesias)?>
                                            <?php foreach($ExcludeFreg as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                                ><?php echo $rowWSL['freguesia']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>



                            <!--   SÍTIO   -->
                            <div class="form-group field sitio">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_SITIO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required class="form-control" name="sitio" id="sitio" maxlength="250" value="<?php echo $sitio; ?>"/>
                                </div>
                            </div>


                            <!--   MORADA   -->
                            <div class="form-group field morada">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MORADA'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required class="form-control" name="morada" id="morada" maxlength="250" value="<?php echo $morada; ?>"/>
                                </div>
                            </div>


                            <!--   PONTOS DE REFERÊNCIA   -->
                            <div class="form-group field ptosRef">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_REFS'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <textarea required class="form-control" rows="5" name="ptosrefs" id="ptosrefs" maxlength="2000"><?php echo $ptosrefs; ?></textarea>
                                </div>
                            </div>


                            <!--   MAPA   -->
                            <div class="form-group field mapa">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_MAPA'); ?></label>
                                <div class="value col-md-12">
                                    <div id="gmap_marker" class="gmaps"></div>

                                    <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas" value=""/>
                                </div>
                            </div>


                            <!--   Upload Imagens   -->
                            <div class="form-group" id="uploadField">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_UPLOAD'); ?></label>
                                <div class="value col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload[]" id="fileupload" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>


                            <input type="hidden" class="form-control" name="nome" id="nome" value="<?php echo htmlentities($nome, ENT_QUOTES, 'UTF-8'); ?>"/>

                            <input type="hidden" class="form-control" name="email" id="email" value="<?php echo $email; ?>"/>

                            <input type="hidden" class="form-control" name="emailConf" id="emailConf" value="<?php echo $email2; ?>"/>

                            <input type="hidden" class="form-control" name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo $fiscalid; ?>"/>

                            <input type="hidden" class="form-control" name="telefone" id="telefone" maxlength="9" value="<?php echo $telefone; ?>"/>


                        </div>

                            <div class="form-actions">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                    </button>
                                    <a class="btn default"
                                       href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta'); ?>"
                                       title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                </div>
                            </div>


                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('alerta.create4manager',$setencrypt_forminputhidden); ?>"/>
                        <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                        <?php echo JHtml::_('form.token'); ?>

                    </form>

                </div>

            </div>
        </div>

    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/addnew4manager.js.php');
echo ('</script>');
?>
