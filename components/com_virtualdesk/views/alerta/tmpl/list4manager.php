<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_stats.php');
JLoader::register('VirtualDeskSiteContactUsReportsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus_reports.php');
JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');
JLoader::register('VirtualDeskSiteAlertaStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta_stats.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('alerta', 'list4managers');
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';
$addcss_ini    = '<link href="';
$addcss_end    = '" rel="stylesheet" />';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.waypoints.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.counterup.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/amcharts.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/serial.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/pie.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/light.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/dark.js' . $addscript_end;


$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;

$headCSS = $addcss_ini . $baseurl . 'components/com_virtualdesk/views/alerta/tmpl/list4manager.css' . $addcss_end;


$doc  = JFactory::getDocument();
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');

// Alerta - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/alerta/tmpl/alerta-comum.css');


$ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
$userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();


/*  ESTATISTICAS */
$objAlertaStats = new VirtualDeskSiteAlertaStatsHelper();
$objAlertaStats->setAllStats();

$alertaNumPedidosOnline       = $objAlertaStats->TotalPedidosOnline;
$alertaNumPedidosBalcao       = $objAlertaStats->TotalPedidosBalcao;
$alertaNumResolvidos          = $objAlertaStats->TotalResolvidos;
$alertaNumNaoResolvidos       = $objAlertaStats->TotalNaoResolvidos;
$dataAlertaTotaisPorCategoria = $objAlertaStats->TotaisPorCategoria;
$dataAlertaTotPorAnoMes       = $objAlertaStats->TotaisPorAnoMes;
$dataAlertaTotaisPorEstado    = $objAlertaStats->TotalPedidosPorEstado;
$dataTempoMedioEstado         = $objAlertaStats->TempoMedioEstado;
$dataDadosImediatos           = $objAlertaStats->DadosImediatos;

echo $headCSS;

?>

<div class="portlet light bordered alerta">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>
        <div class="actions">

            <?php
            $checkAcessoConfig4Managers = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers');
            if($checkAcessoConfig4Managers===true ) : ?>
                <div class="btn-group">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=listconf4manager&vdcleanstate=1'); ?>" >
                        <button class="btn btn-circle btn-outline blue btn-sm" type="button"><i class="fa fa-gear"></i> <?php echo 'Config';  ?></button>
                    </a>
                </div>
            <?php endif; ?>

            <div class="btn-group">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=addnew4manager&vdcleanstate=1'); ?>" >
                    <button class="btn btn-circle btn-outline blue btn-sm" type="button"><i class="fa fa-plus"></i> <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_NOVAOCORRENCIA'); ?></button>
                </a>
            </div>

            <!-- Botões DataTables -->
            <div class="btn-group">
                <a class="btn green btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                    <i class="fa fa-share"></i>
                    <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right" id="tabela_lista_alerta_tools">
                    <li>
                        <a href="javascript:;" data-action="0" class="tool-action">
                            <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="1" class="tool-action">
                            <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="2" class="tool-action">
                            <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="3" class="tool-action">
                            <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?></a>
                    </li>


                </ul>
            </div>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>
    </div>

    <div class="portlet-body">


        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">

                <li class="active">
                    <a href="#tab_Alerta_PedidosSubmetidos" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_PEDIDOSSUBMETIDOS'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Alerta_PedidosSubmetidosMapa" data-toggle="tab">
                        <h4>
                            <i class="fa fa-map-marker"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_PEDIDOSSUBMETIDOSMAPA'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Alerta_Estatistica" data-toggle="tab">
                        <h4>
                            <i class="fa fa-bar-chart"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=addnew4manager&vdcleanstate=1'); ?>" >
                        <h4>
                            <i class="fa fa-plus"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_NOVAOCORRENCIA'); ?>
                        </h4>
                    </a>
                </li>


            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="tab_Alerta_PedidosSubmetidos">
                    <div class="portlet">

                        <div class="portlet-body ">

                            <div class="tabbable-line">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tab_ListaAlertasAll" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTAALL'); ?>
                                        </a>

                                    </li>
                                    <li class="">
                                        <a href="#tab_ListaAlertasEncaminhadas" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTAENCAMINHADASMIM'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_ListaAlertasTarefas" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTTAREFASMIM'); ?>
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_ListaAlertasAll">
                                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_lista_alerta">
                                            <thead>
                                            <tr>
                                                <th style="min-width: 100px;"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                                                <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA'); ?> </th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_CATEGORIA'); ?></th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO'); ?></th>
                                                <th> </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="tab-pane" id="tab_ListaAlertasEncaminhadas">
                                        <p><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTAENCAMINHADASMIM_DESC'); ?> </p>
                                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_lista_alerta_encaminhadas">
                                            <thead>
                                            <tr>
                                                <th style="min-width: 100px;"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                                                <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA'); ?> </th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_CATEGORIA'); ?></th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO'); ?></th>
                                                <th> </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>

                                    <div class="tab-pane" id="tab_ListaAlertasTarefas">
                                        <p><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTTAREFASMIM_DESC'); ?> </p>
                                        <table class="table table-striped table-bordered table-hover order-column" id="tabela_lista_alerta_tarefas">
                                            <thead>
                                            <tr>
                                                <th style="min-width: 100px;"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_DATA'); ?> </th>
                                                <th ><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA'); ?> </th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_CATEGORIA'); ?></th>
                                                <th><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO'); ?></th>
                                                <th> </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="tab_Alerta_PedidosSubmetidosMapa">
                    <div class="portlet-body ">
                        <div style="padding-bottom: 25px;">
                            <?php require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/reports/alerta_abertos_maps4manager.php'); ?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane " id="tab_Alerta_Estatistica">

                    <div class="row widget-row">

                        <div class="col-md-4">
                            <!-- BEGIN NUM PEDIDOS EFETUADOS -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMPEDIDOS_EFETUADOS'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-blue icon-users"></i>

                                    <div class="widget-thumb-body vdBlocoContador">
                                        <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_ONLINE'); ?></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $alertaNumPedidosOnline; ?>"><?php echo $alertaNumPedidosOnline; ?></span>
                                    </div>
                                    <div class="widget-thumb-body vdBlocoContador">
                                        <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_BALCAO'); ?></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $alertaNumPedidosBalcao; ?>"><?php echo $alertaNumPedidosBalcao; ?></span>
                                    </div>

                                </div>
                            </div>
                            <!-- END NUM PEDIDOS EFETUADOS -->
                        </div>

                        <div class="col-md-4">
                            <!-- BEGIN RESOLVIDOS -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_RESOLVIDOS'); ?> </h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-blue icon-trophy"></i>
                                    <div class="widget-thumb-body ">
                                        <span class="widget-thumb-subtitle"></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $alertaNumResolvidos; ?>"><?php echo $alertaNumResolvidos; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END RESOLVIDOS -->
                        </div>

                        <div class="col-md-4">
                            <!-- BEGIN PENDENTE -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_PENDENTES'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-red icon-paper-clip "></i>
                                    <div class="widget-thumb-body vdBlocoContador">
                                        <span class="widget-thumb-subtitle"></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $alertaNumNaoResolvidos; ?>"><?php echo $alertaNumNaoResolvidos; ?></span>
                                    </div>

                                </div>
                            </div>
                            <!-- END PENDENTE -->
                        </div>

                    </div>

                    <div class="row widget-row">

                        <div class="col-md-6">
                            <!-- BEGIN Dados Imediatos -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading">Dados Imediatos</h4>
                                <div class="widget-thumb-wrap">
                                    <div class="col-md-2">
                                        <i class="widget-thumb-icon bg-purple icon-speedometer"></i>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="widget-thumb-body vdBlocoContador">
                                            <span class="widget-thumb-subtitle">Hoje</span>
                                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $dataDadosImediatos->Hoje ; ?>"><?php echo $dataDadosImediatos->Hoje; ?></span>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="widget-thumb-body vdBlocoContador">
                                            <span class="widget-thumb-subtitle">Ontem</span>
                                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $dataDadosImediatos->Ontem; ?>"><?php echo $dataDadosImediatos->Ontem; ?></span>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="widget-thumb-body vdBlocoContador">
                                            <span class="widget-thumb-subtitle">Semana</span>
                                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $dataDadosImediatos->UltimaSemana; ?>"><?php echo $dataDadosImediatos->UltimaSemana; ?></span>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="widget-thumb-body vdBlocoContador">
                                            <span class="widget-thumb-subtitle">Último Mês</span>
                                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $dataDadosImediatos->UltimoMes; ?>"><?php echo $dataDadosImediatos->UltimoMes; ?></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- END Dados Imediatos -->
                        </div>

                        <div class="col-md-6">
                            <!-- BEGIN Tempo Médio -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading">Tempo Médio</h4>
                                <div class="widget-thumb-wrap">
                                    <div class="col-md-3">
                                        <i class="widget-thumb-icon bg-green icon-clock"></i>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="widget-thumb-body vdBlocoContador">
                                            <span class="widget-thumb-subtitle">Em Espera</span>
                                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php if(!empty($dataTempoMedioEstado[1])) echo $dataTempoMedioEstado[1];  ; ?>"><?php if(!empty($dataTempoMedioEstado[1])) echo $dataTempoMedioEstado[1]; ?></span>
                                            <div class=" vdBlocoContadorTexto">dias</div>
                                        </div>

                                    </div>

                                    <div class="col-md-3">
                                        <div class="widget-thumb-body vdBlocoContador">
                                            <span class="widget-thumb-subtitle">Em Análise</span>
                                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php if(!empty($dataTempoMedioEstado[2])) echo $dataTempoMedioEstado[2]; ?>"><?php if(!empty($dataTempoMedioEstado[2])) echo $dataTempoMedioEstado[2]; ?></span>
                                            <div class=" vdBlocoContadorTexto">dias</div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="widget-thumb-body vdBlocoContador">
                                            <span class="widget-thumb-subtitle">Concluídos</span>
                                            <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php if(!empty($dataTempoMedioEstado[3])) echo $dataTempoMedioEstado[3]; ?>"><?php if(!empty($dataTempoMedioEstado[3])) echo $dataTempoMedioEstado[3]; ?></span>
                                            <div class=" vdBlocoContadorTexto">dias</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <!-- NUM REQUESTS POR MES -->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-blue"></i>
                                        <span class="caption-subject bold uppercase font-blue"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORMES'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartAlertaTotPorAnoMes" class="chart" style="height: 250px;"></div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <!-- NUM REQUESTS POR ESTAD -->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pie-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase font-green-haze"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORESTADO'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartAlertaTotaisPorEstado" class="chart" style="height: 250px;"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pie-chart "></i>
                                        <span class="caption-subject bold uppercase "><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORCATEGORIA'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartAlertaTotaisPorCategoria" class="chart" style="height: 500px;"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="tab-pane " id="tab_Alerta_NovaOcorrencia">
                </div>

            </div>
        </div>

    </div>
</div>





<?php

echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/list4manager.js.php');
echo ('</script>');

?>



