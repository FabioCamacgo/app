<?php
defined('_JEXEC') or die;

JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

$obParam      = new VirtualDeskSiteParamsHelper();
$concluida       = $obParam->getParamsByTag('concluida');
$analise       = $obParam->getParamsByTag('analise');
$espera       = $obParam->getParamsByTag('espera');

?>

var iconPathEspera = '<?php echo JUri::base() . $espera; ?>';
var iconPathAnalise = '<?php echo JUri::base() . $analise; ?>';
var iconPathConcluido = '<?php echo JUri::base() . $concluida; ?>';

var MapsGoogle4User = function () {

    // Default
    <?php require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps_default.js.php'); ?>

    var mapMarker = function () {

        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet,
            scrollwheel: false
        });

        <?php foreach($AlertaAbertosMaps4User as $rowMarker) : ?>
        <?php if(!empty($rowMarker['lat']) && !empty($rowMarker['long'])) : ?>

        map.addMarker({
            lat: <?php echo $rowMarker['lat']; ?>,
            lng: <?php echo $rowMarker['long']; ?>,
            title:'<?php echo $rowMarker['ref'] . ' | ' . $rowMarker['cat'] ." | ". $rowMarker['estado']  ; ?>',
            icon:
            <?php switch((int)$rowMarker['idestado']) {
            case 1:
                echo 'iconPathEspera';
            break;
            case 2:
                echo 'iconPathAnalise';
            break;
            case 3:
                 echo 'iconPathConcluido';
            break;}
            ?> ,
            infoWindow: {
                content : '<?php
                    echo ('<div style="min-height: 125px; min-width: 200px; line-height: 15px;">');
                    echo ('<a href="'. JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id='.$rowMarker['id']).'" >');
                    echo ('<b>'.JText::_('COM_VIRTUALDESK_ALERTA_LISTA_REFERENCIA').'</b>: '. $rowMarker['ref'] .' <br><br/>');
                    echo ('<b>'.JText::_('COM_VIRTUALDESK_ALERTA_CATEGORY_LABEL').'</b>: ' .$rowMarker['cat'] ."<br><br/>");
                    echo ('<b>'.JText::_('COM_VIRTUALDESK_ALERTA_LISTA_ESTADO').'</b>: '. $rowMarker['estado']."<br>");
                    echo ('</a>');
                    echo ('<br/>');
                    echo ('<div style="text-align:right;">');
                    echo ('<a  href="'. JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=view4user&alerta_id='.$rowMarker['id']).'" class=" btn btn-sm grey-salsa btn-outline sbold uppercase" >');
                    echo ('<i class="fa fa-share"></i> Ver Mais </a>');
                    echo ('</a>');
                    echo ('</div>');
                    echo ('</div>');
                    ?>'
            }
        });
        <?php endif; ?>
        <?php endforeach; ?>

        map.setZoom(13);

        <?php require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps.js.php'); ?>

        return(map);
    };

    return {
        //main function to initiate map samples
        init: function () {
            var map = mapMarker();
            return(map);
        }
    };

}();



/*
* Inicialização
*/
jQuery(document).ready(function() {


});