<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteAlertaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_alerta.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('alerta');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('alerta', 'edit4managers'); // verifica permissão acesso ao layout para editar
$vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
$vbHasAccessCfg = $objCheckPerm->checkFunctionAccess('alerta', 'acessoconfig4managers'); // Ver se tem acesso ao botão
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false || $vbHasAccessCfg ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/alerta/tmpl/view4manager.css');



//Parâmetros
$labelseparator    = ' : ';
$params = JComponentHelper::getParams('com_virtualdesk');
$getInputSubCategoria_id = JFactory::getApplication()->input->getInt('subcategoria_id');


// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSiteAlertaHelper::getAlertaViewConfSubCategoria4ManagerDetail($getInputSubCategoria_id);
if( empty($this->data) ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
    return false;
}

// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteAlertaHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.editconfsubcategoria4manager.alerta.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

$UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
        .select2-bootstrap-prepend .input-group-btn {vertical-align: bottom};
</style>

<div class="portlet light bordered">
    <div class="portlet-title">

        <div class="caption">
            <i class="icon-lock  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CONFLIST' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CONF_TAB_SUBCATEGORIAS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_EDITAR' ); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=listconf4manager#tab_Alerta_Conf_Sub_Categorias'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>

    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <?php
        // Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
        ?>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body form">

        <form id="form-confsubcategoria"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=alerta.updateConfSubCategoria4manager'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="form-body">

                <div class="headline">
                    <?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_ID'); ?>
                    <span><?php echo htmlentities( $this->data->subcategoria_id, ENT_QUOTES, 'UTF-8'); ?></span>
                </div>

                    <!--   NOME PT  -->
                    <div class="form-group" id="name_PT">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOMEPT'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" required name="name_PT" id="name_PT"  value="<?php echo $this->data->name_PT; ?>"/>
                        </div>
                    </div>

                    <!--   NOME EN  -->
                    <div class="form-group" id="name_EN">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_NOMEEN'); ?><span class="required">*</span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" required name="name_EN" id="name_EN"  value="<?php echo $this->data->name_EN; ?>"/>
                        </div>
                    </div>

                <!--   CATEGORIA -->
                <div class="form-group" id="categoria">
                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_LISTA_CATEGORIA'); ?></label>
                    <?php $CategorisLst = VirtualDeskSiteAlertaHelper::getAlertaCategoriasAllNoFilter();?>
                    <div class="col-md-9">
                        <select name="categoria" value="" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                            <option value="">-</option>
                            <?php foreach($CategorisLst as $rowStatus) : ?>
                                <option value="<?php echo $rowStatus['id']; ?>"
                                    <?php if((int)$this->data->id_categoria == (int)$rowStatus['id']) echo('selected'); ?>
                                ><?php echo $rowStatus['name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                    <!--   EMAIL    -->
                    <div class="form-group" id="mail">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_EMAIL'); ?></label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" name="conf_email" id="conf_email"  value="<?php echo $this->data->conf_email; ?>"/>
                        </div>
                    </div>


                    <!--   CONCELHO  -->
                    <div class="form-group" id="concelho">
                        <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_CONF_CMP_CONCELHO'); ?></label>
                        <?php $ConcelhosLst = VirtualDeskSiteAlertaHelper::getAlertaConcelhosAll();?>
                        <div class="col-md-9">
                            <select name="concelho" value="" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                <option value="">-</option>
                                <?php foreach($ConcelhosLst as $rowStatus) : ?>
                                    <option value="<?php echo $rowStatus['id']; ?>"
                                        <?php if((int)$this->data->id_concelho == (int)$rowStatus['id']) echo('selected'); ?>
                                    ><?php echo $rowStatus['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

            </div>


            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                        </button>
                        <a class="btn default"
                           href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=alerta&layout=listconf4manager#tab_Alerta_Conf_Sub_Categorias'); ?>"
                           title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('subcategoria_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->subcategoria_id) ,$setencrypt_forminputhidden); ?>"/>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('alerta.updateConfSubCategoria4manager',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('listconfsubcategoria4manager',$setencrypt_forminputhidden); ?>"/>


            <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/alerta/tmpl/editconfsubcategoria4manager.js.php');
echo ('</script>');
?>