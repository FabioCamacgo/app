<?php
defined('_JEXEC') or die;

// Se o estado não for o inicial disable o botão editar
$jsIdEstadoInicial = (int) VirtualDeskSiteAlertaHelper::getEstadoIdInicio();
$jsIdEstadoAtual   = (int)$this->data->idestado;

$jsDesligarBotaoEditar = true;
if($jsIdEstadoInicial == $jsIdEstadoAtual && $jsIdEstadoAtual>0) $jsDesligarBotaoEditar = false;

?>
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = jQuery('#tabela_alerta_historico_resumo');
        //var tableTools = jQuery('#tabela_alerta_historico_resumo');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": 'rt',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                    //"targets": [2]
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {

                        var retVal = '';
                        if(row[7] != 1 ) {

                            retVal += '  <i class="fa fa-user font-blue-madison"></i>';
                            retVal += '  <i class="fa fa-angle-right font-blue-madison"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        else {

                            retVal += '  <i class="fa fa-users font-green-steel"></i>';
                            retVal += '  <i class="fa fa-angle-right font-green-steel"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                        //console.log(data);
                        switch(row[10]) {
                            case '1':
                                retVal += ' <i class="icon-speech font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '2':
                                retVal += ' <i class="icon-info font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '3':
                                retVal += ' <i class="icon-flag font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            default:
                                retVal += data;
                                break;
                        }
                        return (retVal);
                    }
                }
                /*,
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var retVal = "<div class='text-wrap'>" + data + "</div>";
                        return (retVal);
                    }
                }*/


            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=alerta.getAlertaHistoricoList4UserByAjax&alerta_id=<?php echo $getInputAlerta_Id; ?>",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }
            ]

        });

        // handle datatable custom tools
        /*tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });*/

       // var tableWrapper = jQuery('#tabela_lista_alerta_wrapper');
    }

    var ReloadTable1 = function () {
        var table = jQuery('#tabela_alerta_historico_resumo');
        //console.log('ReloadTable1');
        table.DataTable().ajax.reload();
    }

    var ReloadTableFull = function () {
        var table = jQuery('#tabela_alerta_historico_full');
        //console.log('ReloadTableFull');
        table.DataTable().ajax.reload();
    }

    var initTableHistFull = function () {

        var table = jQuery('#tabela_alerta_historico_full');
        //var tableTools = jQuery('#tabela_alerta_historico_resumo');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip  <"wrapper"lf<"EstadoFilterDropBox">rtip>
            "dom": 'lfrtip',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                    //"targets": [2]
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {

                        var retVal = '';
                        if(row[7] != 1 ) {
                            retVal += '<i class="fa fa-user font-blue-madison"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var retVal = '';
                        //console.log(data);
                        switch(row[10]) {
                            case '1':
                                retVal += ' <i class="icon-speech font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '2':
                                retVal += ' <i class="icon-info font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            case '3':
                                retVal += ' <i class="icon-flag font-grey-cascade" title="'+ data +'"></i>';
                                break;
                            default:
                                retVal += data;
                                break;
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {

                        var retVal = '';
                        if(row[7] == 1 ) {
                            retVal += '<i class="fa fa-users font-blue-madison"></i>';
                            retVal += '  <span>' + row[8] + '</span>';
                        }
                        return (retVal);
                    }
                }

            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=alerta.getAlertaHistoricoList4UserByAjax&alerta_id=<?php echo $getInputAlerta_Id; ?>",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ]

        });

        // handle datatable custom tools
        /*tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });*/

        // var tableWrapper = jQuery('#tabela_lista_alerta_wrapper');
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();

            initTableHistFull();
        },
        ReloadTable1:function () { ReloadTable1(); },
        ReloadTableFull:function () { ReloadTableFull(); }

    };
}();


var ButtonHandle = function () {

    var handleButton = function (evt) {

        jQuery(".newmsghistSend").on('click',function(evt,data){

            //console.log('newmsghistSend > clicked...')
            let vd_url_send    = jQuery(this).data('vd-url-send');
            let elText         = jQuery(this).closest('div.rowSendNewMsgText').find('.sendnewmsgtext');
            let sendnewmsgtext = elText.val();
            //console.log('vd_url_send =' + vd_url_send);
            vdAjaxCall.sendNewMessage2Hist(jQuery(this), vd_url_send, elText, sendnewmsgtext );
        });
    };


    return {
        //main function to initiate the module
        init: function () {
          handleButton();
        }

    };

}();


var vdAjaxCall = function () {

    var sendNewMessage2Hist = function (el, vd_url_send, elText, sendnewmsgtext) {

        el.attr('disabled','disabled').addClass('disabled');
        elText.attr('disabled','disabled').addClass('disabled');
        let vdClosestI = el.parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        sendnewmsgtext =  encodeURIComponent(sendnewmsgtext);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'sendnewmsgtext=' + sendnewmsgtext,
            indexValue: {el:el, vd_url_send:vd_url_send, elText:elText, sendnewmsgtext:sendnewmsgtext},
            success: function(data){
                let vdClosestI = el.parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg').addClass("fa-check fa-2x text-success");

                TableDatatablesManaged.ReloadTable1();
                TableDatatablesManaged.ReloadTableFull();
                TimeLineHandle.init();

                el.removeAttr('disabled').removeClass('disabled');
                elText.removeAttr('disabled').removeClass('disabled');
                elText.val('')

            },
            error: function(error){
                let vdClosestI = el.parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };


    var getTimeLine2Hist = function (el, vd_url_getcontent) {

        let vdClosestI = el.find('div > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-4x text-default");
        el.find('div.TimeLineContent').html('');

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent},
            success: function(data){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default'); //.addClass("fa-check fa-2x text-success");

                el.find('div.TimeLineContent').html(data);
            },
            error: function(error){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };


    var getTimeLineSimple2Hist = function (el, vd_url_getcontent) {

        let vdClosestI = el.find('div > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-4x text-default");
        el.find('div.TimeLineContent').html('');

        jQuery.ajax({
            url: vd_url_getcontent,
            type: "POST",
            indexValue: {el:el, vd_url_getcontent:vd_url_getcontent},
            success: function(data){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default'); //.addClass("fa-check fa-2x text-success");

                el.find('div.TimeLineContent').html(data);
            },
            error: function(error){
                let vdClosestI = el.find('div > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-4x text-default').addClass("fa-times-circle fa-2x text-danger");
            }
        });
    };


    return {
        sendNewMessage2Hist    : sendNewMessage2Hist,
        getTimeLine2Hist       : getTimeLine2Hist,
        getTimeLineSimple2Hist : getTimeLineSimple2Hist
    };

}();


var TimeLineHandle = function () {

    var handleTimeLine = function (evt) {

        var el = jQuery("#HistoricoTimeLine");
        let vd_url_getcontent    = el.data('vd-url-getcontent');
        //console.log('vd_url_getcontent='+ vd_url_getcontent);
        vdAjaxCall.getTimeLine2Hist(el, vd_url_getcontent );

    };


    var handleTimeLineSimple = function (evt) {

        var el = jQuery("#HistoricoTimeLineSimple");
        let vd_url_getcontent    = el.data('vd-url-getcontent');
        //console.log('vd_url_getcontent='+ vd_url_getcontent);
        vdAjaxCall.getTimeLineSimple2Hist(el, vd_url_getcontent );

    };


    return {
        //main function to initiate the module
        init: function () {
            handleTimeLine();
            handleTimeLineSimple();
        }

    };

}();


var PortofolioHandle = function () {

    var initAlertaFileGridResumo = function (evt) {

        // init cubeportfolio
        jQuery('#vdAlertaFileGridResumo').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var initAlertaFileGridDetalhe = function (evt) {

        // init cubeportfolio
        jQuery('#vdAlertaFileGridDetalhe').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        });

    };

    var destroyAlertaFileGridDetalhe = function (evt) {
        // init cubeportfolio
        jQuery('#vdAlertaFileGridDetalhe').cubeportfolio('destroy');
    };

    return {
        //main function to initiate the module
        init: function () {
            initAlertaFileGridResumo();
            initAlertaFileGridDetalhe();
        },
        initAlertaFileGridDetalhe     :  initAlertaFileGridDetalhe,
        destroyAlertaFileGridDetalhe  : destroyAlertaFileGridDetalhe

    };
}();


jQuery(document).ready(function() {

    TableDatatablesManaged.init();

    ButtonHandle.init();

    TimeLineHandle.init();

    jQuery('a.vdtabAlertaDetalhe').on('shown.bs.tab', function(e){
        if( jQuery('#gmap_marker2').length ) {
            MapsGoogle.mapMarker2();
        }
    })

    PortofolioHandle.init();

    jQuery('.nav-tabs.vdMainNavTab').find('a.vdtabAlertaDetalhe').on('shown.bs.tab', function () {
        // ao clicar no detalhe inicializa a grid
        PortofolioHandle.destroyAlertaFileGridDetalhe();
        PortofolioHandle.initAlertaFileGridDetalhe();

    });


    // Prevent default submit by Enter Key
    jQuery("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });


    // Set default submit by Enter Key in Specific field
    jQuery(".sendnewmsgtext").bind("keypress", function (e) {
        //console.log('keypress sendnewmsgtext in...')
        if (e.keyCode == 13) {
            //console.log('keypress sendnewmsgtext out...')
            var Button = jQuery(this).closest('div.rowSendNewMsgText').find('button.newmsghistSend');
            //console.log(Button);
            Button.click();
            return false;
        }
    });

    <?php if($jsDesligarBotaoEditar===true) : ?>
       jQuery(".vdBotaoEditar").attr('disabled','disabled');
       jQuery("a.vdBotaoEditar").attr('href','').attr('onclick','return false;');
    <?php endif; ?>

});