/*
* Define estrutura da validação.
* A validação é iniciada depois no final deste ficheiro.
* Foi acrescentado antes da inicialização um método ( .addMethod("vdpasscheck"... ) que permite fazer a validação específica da password.
*/
var ProfileEdit = function() {

    var handleProfileEdit = function() {

        jQuery('#edit-profile').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                password1: {
                     vdpasscheck: ['', 'password1',''],
                     required: true
                     },

                password2: {
                    vdpasscheck: ['', 'password2','password1'],
                    required: true
                },

                email2: {
                    equalTo: "#email1"
                },

                fiscalid: {
                    vdnifcheck: ['', 'fiscalid']
                },

                // o campo de login pode ser NIF, tem outra varíavle que permite retorna a validação true se não for fo tipo NIF
                login: {
                    vdnifcheck: ['', 'login'],
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   

                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = MessageAlert.getRequiredMissed(errors);
                        var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                        MainMessageBlock.find('span').html(message);
                        MainMessageBlock.show();
                        App.scrollTo(MainMessageBlock, -200);
                    }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
                    error.insertAfter(jQuery('#register_tnc_error'));
                } else if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                 form.submit();
            }
        });

        jQuery('#edit-profile').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#edit-profile').validate().form()) {
                    jQuery('#edit-profile').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleProfileEdit();
        }
    };

}();



/*
 * Inicialização do Select 2
 */
var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,
            width: null
        });

        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


var FormInputMask = function () {

    var handleInputMasks = function () {
        $("#postalcode").inputmask({
            "mask": "9999-999",
            placeholder: "" // remove underscores from the input mask
        });
    }


    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
        }
    };

}();











/*
* Inicialização da validação
*/

jQuery(document).ready(function() {

    jQuery.validator.addMethod("vdpasscheck", function(value, element, params) {
        var password1      = null;
        var password2      = null;
        var resPassCheckJS = null;

        if(params[1]!='') password1 =  jQuery('#'+params[1]);
        if(params[2]!='') password2 =  jQuery('#'+params[2]);

        if( password2 === null )
        {
            resPassCheckJS = jQuery().virtualDeskPasswordCheck(password1.val(),'',virtualDeskPassCheckOptions);
        }
        else
        {
            resPassCheckJS = jQuery().virtualDeskPasswordCheck(password1.val(),password2.val(),virtualDeskPassCheckOptions);
        }

        params[0] = resPassCheckJS.message;
        return resPassCheckJS.return ;
    }, jQuery.validator.format(" {0} "));


    jQuery.validator.addMethod("vdnifcheck", function(value, element, params) {
        // Verifica se o campo atual é o username/login e não for do tipo NIF então sai
        // valida para as restantes situações: o campo é do tipo nif e não é de´login/username ou é login e é do tipo NIF
        if(setUserFieldLoginTypeNIF_JS<1 && params[1]=='login') return true;

        var NIFElem = null;
        if(params[1]!='') NIFElem =  jQuery('#'+params[1]);
        // Se o campo não estiver preenchido não deve dar erro de NIF
        if(NIFElem.val()=='' || NIFElem.val()==undefined) return true;

        var resNIFCheckJS = jQuery().virtualDeskNIFCheck(NIFElem.val(),virtualDeskNIFCheckOptions);
        params[0] = resNIFCheckJS.message;
        return resNIFCheckJS.return ;
    }, jQuery.validator.format(" {0} "));


    ProfileEdit.init();

    ComponentsSelect2.init();

    FormInputMask.init();

    jQuery("#concelho").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */
        var concelho = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */
        var dataString = "concelho="+concelho; /* STORE THAT TO A DATA STRING */

        // show loader
        App.blockUI({ target:'#blocoFreguesia',  animate: true});

        jQuery.ajax({
            url: 'index.php',
            data:'option=com_virtualdesk&task=profile.getfreguesiaajax&idconcelho=' + concelho,
            success: function(output) {

                var select = jQuery('#freguesia');
                select.find('option').remove();

                if (output == null || output=='') {
                    select.prop('disabled', true);
                }
                else {
                    select.prop('disabled', false);
                    jQuery.each(JSON.parse(output), function (i, obj) {
                        //console.log('i=' + i);
                        select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                    });
                }

                // hide loader
                App.unblockUI('#blocoFreguesia');

            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status +  ' ' + thrownError);
                // hide loader
                select.find('option').remove();
                select.prop('disabled', true);
                App.unblockUI('#blocoFreguesia');
            }
        });

    });



});