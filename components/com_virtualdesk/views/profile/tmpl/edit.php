<?php

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteProfileHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_profile.php');

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
        break;
    }

    // Parametros da Password Chyeck de modo a podermos utilizar na validação javascript
    $passwordcheck_length      = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_length', 8);
    $passwordcheck_no_name     = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_name');
    $passwordcheck_no_email    = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_email');
    $passwordcheck_types_azmin = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmin');
    $passwordcheck_types_azmai = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmai');
    $passwordcheck_types_num   = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_num');
    $passwordcheck_types_special = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_special');

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js'. $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js' . $addscript_end;

    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/profile/tmpl/edit.js'  . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/profile/tmpl/edit_maps.js'  . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

    //Parâmetros
    $params       = JComponentHelper::getParams('com_virtualdesk');
    $useravatar_extensions = $params->get('useravatar_extensions');
    $useravatar_max_size_Mb = (int) $params->get('useravatar_max_size');
    $useravatar_max_size   = (int) $params->get('useravatar_max_size')*1024*1024;
    $useravatar_width      = $params->get('useravatar_width');
    $useravatar_height     = $params->get('useravatar_height');
    $useravatar_defaultimg = $params->get('useravatar_defaultimg');
    $userfield_login_allowedit = $params->get('userfield_login_allowedit');

    // se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
    if(is_array($this->data->useravatar)) $this->data->useravatar = '';

    // Definição dos campos de utilizador
    // require_once(JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    $ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
    $arUserFieldsConfig = $ObjUserFields->getUserFields();
    $arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();
    $ListaDeConcelhos = VirtualDeskSiteUserFieldsHelper::getConcelhosList();
    $AreaActListAllNyGroup = VirtualDeskSiteUserFieldsHelper::getAreaActListAllByGroup($fileLangSufix);
    $ListaDeSetorAtividade = VirtualDeskSiteUserFieldsHelper::getSetorAtividadeList($fileLangSufix);
    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();

    $userType = VirtualDeskSiteProfileHelper::getUserType($UserJoomlaID);

    foreach ($userType as $row):
        if($row['group_id'] == 11) {
            $type = 1;  // é utilizador normal
        } else {
            $type = 2;  // é utilizador manager ou admin
        }
    endforeach;


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-pointer font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">
            <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=default'); ?>"
               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                <i class="fa fa-ban"></i>
                <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null) {
                    return ('');
                }
                if (nErrors == 1) {
                    return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>';
                }
                else if (nErrors > 1) {
                    var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s", nErrors) );
                }
            };
        }
    </script>


    <script>
        <?php
        /*
         * Parametros e Mensagens para a verificação da password em javascript / jquery validation
         */
        ?>
        var virtualDeskPassCheckOptions = {
            // check min length
            m_minLength       :<?php echo $passwordcheck_length ?>
            ,message_MinLength : "<?php echo str_replace('%s','{0}',JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_LENGTH')); ?>"
            // check login in pass
            ,p_Username        :<?php echo $passwordcheck_no_name ?>
            ,p_UsernameElem    : 'login'
            ,message_Username  : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_NAME'); ?>"
            // check email in pass
            ,p_Email           :<?php echo $passwordcheck_no_name ?>
            ,p_EmailElem       : 'email1'
            ,message_Email     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_EMAIL'); ?>"
            //check cchar
            ,message_chartype  : "<?php echo str_replace('%s','{0}',JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE')); ?>"
            // check letras minusculas
            ,p_AZMin           :<?php echo $passwordcheck_types_azmin ?>
            ,message_AZMin     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_LOWERCASELETTER'); ?>"
            // check letras minusculas
            ,p_AZMai           :<?php echo $passwordcheck_types_azmai ?>
            ,message_AZMai     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_UPPERCASELETTER'); ?>"
            // check números
            ,p_Num           :<?php echo $passwordcheck_types_num ?>
            ,message_Num     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_NUMBER'); ?>"
            // check números
            ,p_SpecialChar           :<?php echo $passwordcheck_types_special ?>
            ,message_SpecialChar     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_SPECIALCHARACTER'); ?>"
            // check espaços
            ,message_Spaces     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_STARTSENDS'); ?>"
            // check pass1 e pass2
            ,message_pass1pass2     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_PASS1PASS2'); ?>"

        };

        // js para identificar se o login é do tipo nif
        var setUserFieldLoginTypeNIF_JS = <?php echo $arUserFieldLoginConfig['setUserFieldLoginTypeNIF_JS']; ?>;

        // mensagem para a validação de um campo tipo nif
        var virtualDeskNIFCheckOptions = {
            message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'); ?>"
        };
    </script>


    <div class="portlet-body form">

        <form id="edit-profile"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=profile.save'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="form-body">

                <?php
                    if($type == 1){
                        require_once (JPATH_SITE . '/components/com_virtualdesk/views/profile/tmpl/editfieldsuser.php');
                    } else {
                        require_once (JPATH_SITE . '/components/com_virtualdesk/views/profile/tmpl/editfieldscorporate.php');
                    }
                ?>


                <div class="form-actions">
                    <div class="col-md-12">
                        <button type="submit" class="btn submit"><span><?php echo JText::_('COM_VIRTUALDESK_SAVECHANGES'); ?></span>
                        </button>
                        <a class="btn default"
                           href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=default'); ?>"
                           title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                    </div>
                </div>


                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('profile.save',$setencrypt_forminputhidden); ?>"/>
                <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>


<?php echo $localScripts; ?>


<script>
    jQuery(document).ready(function() {

        jQuery('#vdChangeProfileLogin').click(function(){
            jQuery('input#login').removeAttr('disabled');
        });

        jQuery('#vdChangeProfilePassword').click(function(){
            jQuery('input#password1').removeAttr('disabled');
            jQuery('input#password2').removeAttr('disabled');
        });

        jQuery('#vdChangeProfileEmail').click(function(){
            jQuery('input#email1').removeAttr('disabled');
            jQuery('input#email2').removeAttr('disabled');
        });



        var uploadFile;
        // edit this line to the id of the file element
        uploadFile = 'useravatar';
        uploadFile = jQuery('#'+uploadFile);
        uploadFile.on('change', function() {
            var files, reader;
            files = !!this.files ? this.files : [];
            if ( !files.length || !window.FileReader ) {
                jQuery('img#loadedAvatarImg').show();
                return; // no file selected, or no FileReader support
            }

            var fsize = files[0].size;
            if( fsize><?php echo $useravatar_max_size; ?>) //do something if file size more than 1 mb (1048576)
            {
                swal("<?php echo JText::_('COM_VIRTUALDESK_USERAVATAR_FILE_TOO_LARGE') . $useravatar_max_size_Mb . "Mb"; ?>",'',"warning");
                uploadFile.val('');
                jQuery('img#loadedAvatarImg').show();
                return(false);
            }

            //get the file size and file type from file input field
            var fsize = files[0].size;
            var ftype = files[0].type;
            var fname = files[0].name;

            switch(ftype)
            {
                case 'image/png':
                case 'image/gif':
                case 'image/jpeg':
                    break;
                default:
                    swal("<?php echo JText::_('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'); ?>",'',"warning");
                    uploadFile.val('');
                    jQuery('img#loadedAvatarImg').show();
                    return(false);
            }

            if ( /^image/.test(files[0].type) ) { // only image file
                // retira a imagem anterior
                jQuery('img#loadedAvatarImg').hide();
            }

        });

    });
</script>