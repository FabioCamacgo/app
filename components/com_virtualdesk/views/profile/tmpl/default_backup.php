<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/profile/tmpl/default_maps.js'  . $addscript_end;

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

//Parâmetros
$params       = JComponentHelper::getParams('com_virtualdesk');
$useravatar_width      = $params->get('useravatar_width');
$useravatar_height     = $params->get('useravatar_height');
$useravatar_defaultimg = $params->get('useravatar_defaultimg');

$labelseparator=' : ';

// se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(is_array($this->data->useravatar)) $this->data->useravatar = '';

$ObjUserFields          =  new VirtualDeskSiteUserFieldsHelper();
$arUserFieldsConfig     = $ObjUserFields->getUserFields();
$arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();
// Multichoice / select2 : Áreas de Atuação
$AreaActListByName = VirtualDeskSiteUserFieldsHelper::getAreaActListSelectName ($this->data, $fileLangSufix);
?>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-user font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>
    </div>

    <div class="portlet-body form">
        <form class="form-horizontal well">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo $arUserFieldLoginConfig['CampoForm_Username_Label'].$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->login, ENT_QUOTES, 'UTF-8')?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_EMAIL_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"> <?php echo htmlentities( $this->data->email1, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>

                <?php
                // Apresenta o campo apenas se o login não for do tipo NIF E se o campo NIF estiver ativo na configuração para esta view
                if($arUserFieldLoginConfig['setUserFieldLoginTypeNIF']==false && $arUserFieldsConfig['UserField_NIF_Profile']===true) : ?>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_FISCALID_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->fiscalid, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>

                <?php endif; ?>


                        <?php foreach ($ObjUserFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                            <?php if($arUserFieldsConfig['UserField_' . $valConfFieldNames . '_Profile' ]===true) : ?>
                                <?php if ($valConfFieldNames == 'NIF') : ?>


                                <?php elseif ($valConfFieldNames == 'CONCELHO') : ?>

                                    <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_LOCALIZ_TITLE'); ?></h4>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                            <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getConcelhoById ($this->data->{$keyConfFieldNames}), ENT_QUOTES, 'UTF-8'); ?>
                                            <?php //echo htmlentities( VirtualDeskSiteUserFieldsHelper::getConcelhoByIdEDITED (), ENT_QUOTES, 'UTF-8');?>
                                            </p>
                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'FREGUESIA') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                                <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getFreguesiaById ($this->data->{$keyConfFieldNames}), ENT_QUOTES, 'UTF-8');?>
                                            </p>
                                        </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'MAPLATLONG') : ?>

                                    <div class="form-group">

                                            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                            <input type="hidden" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                                   name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                                   value="<?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?>"/>

                                    </div>


                                <?php elseif ($valConfFieldNames == 'AREAACT') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                             <p class="form-control-static"> <?php  if(!is_array($AreaActListByName)) $AreaActListByName = array();
                                                echo htmlentities(implode(', ',$AreaActListByName), ENT_QUOTES, 'UTF-8');?></p>
                                            </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'SECTORATIVIDADE') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                                <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getSetorAtividadeById($this->data->{$keyConfFieldNames}, $fileLangSufix), ENT_QUOTES, 'UTF-8');?>
                                            </p>
                                        </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'VERACIDADEDADOS') : ?>

                                <?php elseif ($valConfFieldNames == 'POLITICAPRIVACIDADE') : ?>

                                <?php else : ?>


                                    <?php if ($valConfFieldNames == 'FIRMA') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_ENTERPRISE_TITLE'); ?></h4>
                                    <?php endif; ?>

                                    <?php if ($valConfFieldNames == 'ADDRESS') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_CONTACTS_TITLE'); ?></h4>
                                    <?php endif; ?>

                                    <?php if ($valConfFieldNames == 'MANAGERNAME') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_MANAGER_TITLE'); ?></h4>
                                    <?php endif; ?>




                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"> <?php echo htmlentities( $this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8');?></p>
                                        </div>
                                    </div>

                                 <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>

                </div>


                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($ObjUserFields->arConfFieldNames ['veracidadedados'])) : ?>
                            <div class="form-group">
                                <div class="col-md-2 " style="text-align: right;padding: 0;">

                                    <input type="checkbox" disabled name="veracidadedados" id="veracidadedados" class="make-switch form-control" value="1"
                                           data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                                           data-on-color="success" data-off-color="danger"
                                        <?php if((int)$this->data->veracidadedados>0)  echo 'checked="checked"' ?>
                                    />

                                </div>
                                <label class="col-md-10 " ><?php echo JText::_( 'COM_VIRTUALDESK_USER_VERACIDADEDADOS_LABEL' ); ?></label>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($ObjUserFields->arConfFieldNames ['politicaprivacidade'])) : ?>
                            <div class="form-group">
                                <div class="col-md-2 " style="text-align: right;padding: 0;">

                                    <input type="checkbox" disabled name="politicaprivacidade" id="politicaprivacidade" class="make-switch form-control" value="1"
                                           data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                                           data-on-color="success" data-off-color="danger"
                                        <?php if((int)$this->data->politicaprivacidade>0)  echo 'checked="checked"' ?>
                                    />

                                </div>
                                <label class="col-md-10 "  ><?php echo JText::sprintf( 'COM_VIRTUALDESK_USER_POLITICAPRIVACIDADE_LABEL', JUri::root() ); ?></label>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>


            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php echo JUri::base(); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>


<?php echo $localScripts; ?>