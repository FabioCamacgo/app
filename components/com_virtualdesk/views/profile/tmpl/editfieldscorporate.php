<div class="bloco">
    <legend><h3><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_DADOSPESSOAIS'); ?></h3></legend>

    <div class="form-group field userLogin">
        <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_USER_SECTION_LOGIN' ); ?></label>
        <div class="col-md-12">
            <?php // verifica se o campo de login é editável ou não de acord com parametro de config
            if ( $userfield_login_allowedit == 0 ) : ?>
                <input type="text" required class="form-control" disabled value="<?php echo htmlentities($this->dataUser->login, ENT_QUOTES, 'UTF-8'); ?>">
            <?php else  : ?>
                <i class="fa fa-user"></i>
                <input type="text" required class="form-control" placeholder="Enter login"
                       disabled name="login" id="login" maxlength="<?php echo $arUserFieldLoginConfig['CampoForm_Username_MaxLength']; ?>"
                       value="<?php echo htmlentities($this->data->login, ENT_QUOTES, 'UTF-8'); ?>">
                <span class="input-group-btn">
                    <button id="vdChangeProfileLogin" class="btn btn-success" type="button">
                       <i class="icon-pencil"></i>
                    </button>
                </span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group field userName">
        <label class="name col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_USER_SECTION_USERNAME' ); ?></label>
        <div class="col-md-12">
            <input type="text" class="form-control" name="name" id="name" value="<?php echo htmlentities($this->data->name, ENT_QUOTES, 'UTF-8'); ?>"/>
        </div>
    </div>

    <div class="form-group field password1">
        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_LABEL'); ?></label>
        <div class="col-md-12">
            <div class="input-group">
                <input type="password" class="form-control" disabled name="password1" id="password1" maxlength="25"/>
                <span class="input-group-btn">
                        <button id="vdChangeProfilePassword" class="btn btn-success" type="button">
                            <i class="icon-pencil"></i>
                        </button>
                    </span>

            </div>
        </div>
    </div>

    <div class="form-group field password2">
        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_REPEAT_LABEL'); ?></label>
        <div class="col-md-12">
            <input type="password" class="form-control" disabled name="password2" id="password2" maxlength="25"/>
        </div>
    </div>

    <div class="form-group field email1">
        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_LABEL'); ?></label>
        <div class="col-md-12">
            <div class="input-group">
                <input type="email" required class="form-control" disabled name="email1" id="email1" maxlength="250" value="<?php echo htmlentities($this->data->email1, ENT_QUOTES, 'UTF-8'); ?>">
                <span class="input-group-btn">
                    <button id="vdChangeProfileEmail" class="btn btn-success" type="button">
                       <i class="icon-pencil"></i>
                    </button>
                </span>

            </div>
        </div>
    </div>

    <div class="form-group field email2">
        <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_REPEAT_LABEL'); ?></label>
        <div class="col-md-12">
            <input type="email" required class="form-control" disabled name="email2" id="email2" maxlength="250" value="<?php echo htmlentities($this->data->email2, ENT_QUOTES, 'UTF-8'); ?>">
        </div>
    </div>

</div>

<div class="bloco">
    <legend><h3><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_TERMOSCONDICOES'); ?></h3></legend>


    <div class="form-group">
        <div class="col-md-2" style="padding: 0;">
            <input type="checkbox" name="veracidadedados" id="veracidadedados" class="make-switch form-control" value="1"
                   data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                   data-on-color="success" data-off-color="danger"
                <?php if((int)$this->data->veracidadedados>0)  echo 'checked="checked"' ?>
            />
        </div>
        <label class="col-md-10"><?php echo JText::_( 'COM_VIRTUALDESK_USER_VERACIDADEDADOS_LABEL' ); ?></label>
    </div>


    <div class="form-group">
        <div class="col-md-2" style="padding: 0;">
            <input type="checkbox" name="politicaprivacidade" id="politicaprivacidade" class="make-switch form-control" value="1"
                   data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                   data-on-color="success" data-off-color="danger"
                <?php if((int)$this->data->politicaprivacidade>0)  echo 'checked="checked"' ?>
            />
        </div>
        <label class="col-md-10"><?php  echo JText::sprintf( 'COM_VIRTUALDESK_USER_POLITICAPRIVACIDADE_LABEL', JUri::root() ); ?></label>
    </div>

</div>
