<?php
    $this->data = array();
    $this->data = VirtualDeskSiteProfileHelper::getDadosUserDetail();
?>


<div class="bloco">
    <legend><h3><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_DADOSPESSOAIS'); ?></h3></legend>


    <div class="field userLogin">
        <div class="static-info ">
            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_USER_SECTION_LOGIN' ); ?></div>
            <div class="value"> <?php echo htmlentities( $this->data->login, ENT_QUOTES, 'UTF-8');?> </div>
        </div>
    </div>

    <div class="field userName">
        <div class="static-info ">
            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_USER_SECTION_USERNAME' ); ?></div>
            <div class="value"> <?php echo htmlentities( $this->data->name, ENT_QUOTES, 'UTF-8');?> </div>
        </div>
    </div>

    <div class="field userMail">
        <div class="static-info ">
            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_USER_SECTION_USERMAIL' ); ?></div>
            <div class="value"> <?php echo htmlentities( $this->data->email, ENT_QUOTES, 'UTF-8');?> </div>
        </div>
    </div>

</div>

<div class="bloco">
    <legend><h3><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_TERMOSCONDICOES'); ?></h3></legend>

    <div class="form-group">
        <div class="col-md-2" style="padding: 0;">

            <input type="checkbox" disabled name="veracidadedados" id="veracidadedados" class="make-switch form-control" value="1"
                   data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                   data-on-color="success" data-off-color="danger"
                <?php if((int)$this->data->veracidadedados > 0)  echo 'checked="checked"' ?>
            />

        </div>
        <label class="col-md-10 " ><?php echo JText::_( 'COM_VIRTUALDESK_USER_VERACIDADEDADOS_LABEL' ); ?></label>
    </div>

    <div class="form-group">
        <div class="col-md-2" style="padding: 0;">

            <input type="checkbox" disabled name="politicaprivacidade" id="politicaprivacidade" class="make-switch form-control" value="1"
                   data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                   data-on-color="success" data-off-color="danger"
                <?php if((int)$this->data->politicaprivacidade>0)  echo 'checked="checked"' ?>
            />

        </div>
        <label class="col-md-10 "  ><?php echo JText::sprintf( 'COM_VIRTUALDESK_USER_POLITICAPRIVACIDADE_LABEL', JUri::root() ); ?></label>
    </div>

</div>



