var MapsGoogle = function () {

    var mapMarker = function () {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });
        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: ''
        });
        map.setZoom(12);

        GMaps.on('click', map.map, function(event) {

            map.removeMarkers();

            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            jQuery('#maplatlong').val(lat + ',' + lng);

            map.addMarker({
                lat: lat,
                lng: lng,
                title: 'Marker #' + index,
                infoWindow: {
                    content : ''
                }
            });
        });

    }

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };

}();


var LatToSet = 32.7320051;
var LongToSet = -16.8315204;

/*
* Inicialização
*/

jQuery(document).ready(function() {

    var InputLatLong = jQuery('#maplatlong').val();
    if( InputLatLong!=undefined && InputLatLong!="") {
        var ArrayLatLong = InputLatLong.split(",");
        if(ArrayLatLong.length == 2) {
            LatToSet  =  ArrayLatLong[0];
            LongToSet =  ArrayLatLong[1];

            //console.log('LatToSet' + LatToSet);
            //console.log('LongToSet' + LongToSet);
        }
    }

    if( jQuery('#gmap_marker').length ) {
         MapsGoogle.init()
    }

});