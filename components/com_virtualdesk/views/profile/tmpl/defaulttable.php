<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


/*
 *
 *
 * VERSÃO Em TABELA do FORM
 *
 *
 */



defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

//Parâmetros
$params       = JComponentHelper::getParams('com_virtualdesk');
$useravatar_width      = $params->get('useravatar_width');
$useravatar_height     = $params->get('useravatar_height');
$useravatar_defaultimg = $params->get('useravatar_defaultimg');

$labelseparator=' : ';

// se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(is_array($this->data->useravatar)) $this->data->useravatar = '';

$ObjUserFields          =  new VirtualDeskSiteUserFieldsHelper();
$arUserFieldsConfig     = $ObjUserFields->getUserFields();
$arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();
// Multichoice / select2 : Áreas de Atuação
$AreaActListByName = VirtualDeskSiteUserFieldsHelper::getAreaActListSelectName ($this->data, $fileLangSufix);
?>

<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-user font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>
    </div>

    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">
            <div class="form-body">


                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo $arUserFieldLoginConfig['CampoForm_Username_Label'].$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->login, ENT_QUOTES, 'UTF-8')?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_EMAIL_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"> <?php echo htmlentities( $this->data->email1, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-6">
                        <?php if( empty($this->data->useravatar) ) : ?>
                            <div class="profile-userpic" style="font-size: 100px;">
                                <img class="img-responsive" src="<?php echo $useravatar_defaultimg; ?>"/>
                            </div>
                        <?php else : ?>
                            <div class="profile-userpic">
                                <img class="img-responsive"  src="<?php  echo $this->params->get('useravatar_folder'). DS . $this->data->useravatar ?>" />
                            </div>
                        <?php endif; ?>
                    </div>

                </div>


                <?php
                // Apresenta o campo apenas se o login não for do tipo NIF E se o campo NIF estiver ativo na configuração para esta view
                if($arUserFieldLoginConfig['setUserFieldLoginTypeNIF']==false && $arUserFieldsConfig['UserField_NIF_Profile']===true) : ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_FISCALID_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->fiscalid, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>




                <div class="row">
                    <div class="col-md-6">
                        <?php foreach ($ObjUserFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                            <?php if($arUserFieldsConfig['UserField_' . $valConfFieldNames . '_Profile' ]===true) : ?>
                                <?php if ($valConfFieldNames == 'NIF') : ?>


                                <?php elseif ($valConfFieldNames == 'CONCELHO') : ?>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                            <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getConcelhoById ($this->data->{$keyConfFieldNames}), ENT_QUOTES, 'UTF-8');?>
                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'FREGUESIA') : ?>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                                <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getFreguesiaById ($this->data->{$keyConfFieldNames}), ENT_QUOTES, 'UTF-8');?>
                                        </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'AREAACT') : ?>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                             <p class="form-control-static"> <?php  if(!is_array($AreaActListByName)) $AreaActListByName = array();
                                                echo htmlentities(implode(', ',$AreaActListByName), ENT_QUOTES, 'UTF-8');?></p>
                                            </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'SECTORATIVIDADE') : ?>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                                <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getSetorAtividadeById($this->data->{$keyConfFieldNames}, $fileLangSufix), ENT_QUOTES, 'UTF-8');?>
                                        </div>
                                    </div>

                                <?php else : ?>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"> <?php echo htmlentities( $this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8');?></p>
                                        </div>
                                    </div>

                                 <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>



            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php echo JUri::base(); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
