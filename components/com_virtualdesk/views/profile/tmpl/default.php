<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteProfileHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_profile.php');

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/profile/tmpl/default_maps.js'  . $addscript_end;

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    //Parâmetros
    $params       = JComponentHelper::getParams('com_virtualdesk');
    $useravatar_width      = $params->get('useravatar_width');
    $useravatar_height     = $params->get('useravatar_height');
    $useravatar_defaultimg = $params->get('useravatar_defaultimg');

    $labelseparator=' : ';

    // se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
    if(is_array($this->data->useravatar)) $this->data->useravatar = '';

    $ObjUserFields          =  new VirtualDeskSiteUserFieldsHelper();
    $arUserFieldsConfig     = $ObjUserFields->getUserFields();
    $arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();
    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    $AreaActListByName = VirtualDeskSiteUserFieldsHelper::getAreaActListSelectName ($this->data, $fileLangSufix);

    $userType = VirtualDeskSiteProfileHelper::getUserType($UserJoomlaID);

    foreach ($userType as $row):
        if($row['group_id'] == 11) {
            $type = 1;  // é utilizador normal
        } else {
            $type = 2;  // é utilizador manager ou admin
        }
    endforeach;

?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            </div>
        </div>

        <div class="portlet-body form">
            <form class="form-horizontal well">
                <div class="form-body">

                    <?php
                        if($type == 1){
                            require_once (JPATH_SITE . '/components/com_virtualdesk/views/profile/tmpl/viewfieldsuser.php');
                        } else {
                            require_once (JPATH_SITE . '/components/com_virtualdesk/views/profile/tmpl/viewfieldscorporate.php');
                        }
                    ?>

                    <div class="form-actions">
                        <div class="col-md-12">
                            <a class="btn submit" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=profile&layout=edit'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php echo JUri::base(); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                        </div>
                    </div>

            </form>
        </div>
    </div>


<?php echo $localScripts; ?>