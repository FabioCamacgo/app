var MapsGoogle = function () {

    var mapMarker = function () {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });
        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: ''
        });
        map.setZoom(12);
    }

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };
}();


/*
* Inicialização
*/


var LatToSet = 32.7320051;
var LongToSet = -16.8315204;


jQuery(document).ready(function() {
    if(jQuery('#gmap_marker').length>0) {

        var InputLatLong = jQuery('#maplatlong').val();
        if( InputLatLong!=undefined && InputLatLong!="") {
            var ArrayLatLong = InputLatLong.split(",");
            if(ArrayLatLong.length == 2) {
                LatToSet  =  ArrayLatLong[0];
                LongToSet =  ArrayLatLong[1];

                //console.log('LatToSet' + LatToSet);
                //console.log('LongToSet' + LongToSet);
            }
        }
        MapsGoogle.init()
    }
});