<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
    JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('formmain', 'list4admin');
    $vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');

    $ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
    $userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();
?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-calendar font-dark"></i>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=formmain'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_FORMMAIN_MENU_UPLOADLIST' ); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=formmain'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=addnewupload4admin&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_FORMMAIN_NOVOUPLOAD' ); ?></a>

            <div class="btn-group">
                <a class="btn green btn-outline btn-circle" href="javascript:;"
                   data-toggle="dropdown">
                    <i class="fa fa-share"></i>
                    <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right" id="tabela_listaUploads_tools">
                    <li>
                        <a href="javascript:;" data-action="0" class="tool-action">
                            <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="1" class="tool-action">
                            <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="2" class="tool-action">
                            <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-action="3" class="tool-action">
                            <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                        </a>
                    </li>


                </ul>
            </div>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        <div class="portlet">

            <div class="portlet-body ">

                <table class="table table-striped table-bordered table-hover order-column" id="tabela_listaUploads">
                    <thead>
                    <tr>
                        <th><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_ID'); ?> </th>
                        <th><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_REFERENCIA'); ?></th>
                        <th><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_TAG'); ?> </th>
                        <th><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_NOME'); ?></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>

            </div>
        </div>

    </div>
</div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/formmain/tmpl/uploadlist4admin.js.php');
    echo ('</script>');
?>
