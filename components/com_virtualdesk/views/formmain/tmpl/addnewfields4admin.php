<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('formmain');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('formmain', 'addnew4admin'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');
    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteFormmainHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewfields4admin.formmain.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    $email2   = $UserProfileData->email;
    $telefone = $UserProfileData->phone1;

    $obParam      = new VirtualDeskSiteParamsHelper();


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_FORMMAIN_NOVOCAMPO' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=fieldslist4admin'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <form id="new-campos" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=formmain.createfields4admin'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                    <div class="form-body">

                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_NOVOCAMPO'); ?></h3></legend>

                            <div class="form-group" id="fieldTagCampo">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_TAG'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required class="form-control" name="tagCampo" id="tagCampo" maxlength="50" value="<?php echo $tagCampo; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldNomeCampo">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_NOMECAMPO'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" required class="form-control" name="nomeCampo" id="nomeCampo" maxlength="200" value="<?php echo $nomeCampo; ?>"/>
                                </div>
                            </div>

                            <div class="form-group" id="fieldDescricao">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_DESCRICAO'); ?></label>
                                <div class="value col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="15" name="descricao" id="descricao" maxlength="2000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao); ?></textarea>
                                </div>
                            </div>


                            <div class="form-group" id="fieldTipoCampo">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_TIPOCAMPO'); ?></label>
                                <?php $TipoCampos = VirtualDeskSiteFormmainHelper::getTipoCampo()?>
                                <div class="value col-md-12">
                                    <select name="tipoCampo" value="<?php echo $tipoCampo; ?>" id="tipoCampo" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($tipoCampo)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_FORMMAIN_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($TipoCampos as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipo']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $tipoCampo; ?>"><?php echo VirtualDeskSiteFormmainHelper::getTipoCampoSelect($tipoCampo) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeTipoCampo = VirtualDeskSiteFormmainHelper::excludeTipoCampo($tipoCampo)?>
                                            <?php foreach($ExcludeTipoCampo as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipo']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                        </div>

                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                </button>
                                <a class="btn default"
                                   href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=formmain&layout=fieldslist4admin'); ?>"
                                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('formmain.createfields4admin',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('fieldslist4admin',$setencrypt_forminputhidden); ?>"/>


                    <?php echo JHtml::_('form.token'); ?>

                </form>
            </div>

        </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/formmain/tmpl/addnewfields4admin.js.php');
    echo ('</script>');
?>