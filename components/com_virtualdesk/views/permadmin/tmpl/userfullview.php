<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');


/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userfullview'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

//Parâmetros
$params       = JComponentHelper::getParams('com_virtualdesk');
$useravatar_width      = $params->get('useravatar_width');
$useravatar_height     = $params->get('useravatar_height');
$useravatar_defaultimg = $params->get('useravatar_defaultimg');

$labelseparator=' : ';

$getInputPermUser_id = JFactory::getApplication()->input->getInt('permadmin_user_id');

$this->data = array();

##$this->data = VirtualDeskSitePermAdminHelper::getPermUserDetail($getInputPermUser_id);
$LoadedUserJoomlaID = VirtualDeskSiteUserHelper::getJoomlaIDFromVDUserId($getInputPermUser_id);
$this->data   = VirtualDeskSiteUserHelper::getUserFromJoomlaID($LoadedUserJoomlaID);


// se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(is_array($this->data->useravatar)) $this->data->useravatar = '';

$ObjUserFields          =  new VirtualDeskSiteUserFieldsHelper();
$arUserFieldsConfig     = $ObjUserFields->getUserFields();
$arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();
// Multichoice / select2 : Áreas de Atuação
$AreaActListByName = VirtualDeskSiteUserFieldsHelper::getAreaActListSelectName ($this->data, $fileLangSufix);
?>

<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #f1f4f7 !important;}
    .profile-userpic img {-webkit-border-radius: 50% !important;  -moz-border-radius: 50% !important;  border-radius: 50% !important;   width: 150px; height: auto; margin: 0;}
    .form .control-label { font-weight: bold;}
</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-lock  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_TAB_USERS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_VER_DETALHE' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_VER_USERFULL' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <?php
        // Data not empty ?
        if(!empty($this->data)) :
            ?>
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userfulledit&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <!-- <a href="<?php // echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=useraddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
                 -->

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        <?php else :
        ?>
        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabUsers'); ?>" class="btn btn-circle btn-default">
            <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
        <a href="<?php // echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=useraddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
            <i class="fa fa-plus"></i>  <?php  echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
    </div>
    <?php endif;?>
    <!-- END TITLE ACTIONS -->
</div>

    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">
            <div class="form-body">


                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo $arUserFieldLoginConfig['CampoForm_Username_Label'].$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->login, ENT_QUOTES, 'UTF-8')?></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_EMAIL_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"> <?php echo htmlentities( $this->data->email1, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_PHOTO_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                            <?php if( empty($this->data->useravatar) ) : ?>
                                <div class="profile-userpic"  style="border-radius:50% !important; width:150px !important; font-size: 100px;">
                                    <img class="img-responsive" style="border-radius:50% !important; width:150px !important;" src="<?php echo $useravatar_defaultimg; ?>"/>
                                </div>
                            <?php else : ?>
                                <div class="profile-userpic">
                                    <img class="img-responsive" style="border-radius:50% !important; width:150px !important;"  src="<?php  echo $this->params->get('useravatar_folder'). DS . $this->data->useravatar ?>" />
                                </div>
                            <?php endif; ?>
                            </div>
                        </div>





                <?php
                // Apresenta o campo apenas se o login não for do tipo NIF E se o campo NIF estiver ativo na configuração para esta view
                if($arUserFieldLoginConfig['setUserFieldLoginTypeNIF']==false && $arUserFieldsConfig['UserField_NIF_Profile']===true) : ?>

                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_FISCALID_LABEL' ).$labelseparator; ?></label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo htmlentities( $this->data->fiscalid, ENT_QUOTES, 'UTF-8');?></p>
                            </div>
                        </div>

                <?php endif; ?>


                        <?php foreach ($ObjUserFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                            <?php if($arUserFieldsConfig['UserField_' . $valConfFieldNames . '_Profile' ]===true) : ?>
                                <?php if ($valConfFieldNames == 'NIF') : ?>


                                <?php elseif ($valConfFieldNames == 'CONCELHO') : ?>

                                    <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_LOCALIZ_TITLE'); ?></h4>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                            <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getConcelhoById ($this->data->{$keyConfFieldNames}), ENT_QUOTES, 'UTF-8');?>
                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'FREGUESIA') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                                <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getFreguesiaById ($this->data->{$keyConfFieldNames}), ENT_QUOTES, 'UTF-8');?>
                                        </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'MAPLATLONG') : ?>

                                    <div class="form-group">

                                            <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                            <input type="hidden" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                                   name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                                   value="<?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?>"/>

                                    </div>


                                <?php elseif ($valConfFieldNames == 'AREAACT') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                             <p class="form-control-static"> <?php  if(!is_array($AreaActListByName)) $AreaActListByName = array();
                                                echo htmlentities(implode(', ',$AreaActListByName), ENT_QUOTES, 'UTF-8');?></p>
                                            </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'SECTORATIVIDADE') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">
                                                <?php echo htmlentities( VirtualDeskSiteUserFieldsHelper::getSetorAtividadeById($this->data->{$keyConfFieldNames}, $fileLangSufix), ENT_QUOTES, 'UTF-8');?>
                                        </div>
                                    </div>

                                <?php else : ?>


                                    <?php if ($valConfFieldNames == 'FIRMA') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_ENTERPRISE_TITLE'); ?></h4>
                                    <?php endif; ?>

                                    <?php if ($valConfFieldNames == 'ADDRESS') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_CONTACTS_TITLE'); ?></h4>
                                    <?php endif; ?>

                                    <?php if ($valConfFieldNames == 'MANAGERNAME') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_MANAGER_TITLE'); ?></h4>
                                    <?php endif; ?>


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"> <?php echo htmlentities( $this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8');?></p>
                                        </div>
                                    </div>

                                 <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>

            </div>


                </div>



            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userfulledit&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->id)); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>


<?php echo $localScripts; ?>
<script>
    <?php
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/permadmin/tmpl/userfullview.js.php');
    ?>
</script>
