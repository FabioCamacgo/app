<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
?>
<!--  -->
<div class="panel-group accordion" id="accordion1">
            <?php
            $currModulo = '';
            $prevModulo = '';
            $iconChecked    = '<i class="fa fa-check font-green-jungle"></i>';
            $iconNotChecked = '<i class="fa fa-minus font-grey"></i>';
            $iconNotDefinedChecked = '<i class="fa fa-minus font-grey"></i>';
            foreach($this->dataGrupoActions as $rowGrupoActions) :
            $currModulo = $rowGrupoActions->modulo_tagchave;
            //inicia um novo acordion
            if($prevModulo!=$currModulo) :  ?>
                <?php if( ($prevModulo!=$currModulo) && ($prevModulo!='')) :  ?>
                </tbody>
                </table>
                </div>
                </div>
                </div>
                </div>
                <?php endif;?>

                <div class="panel panel-default">
                    <div class="panel-heading blue" >
                        <h4 class="panel-title">

                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion1" href="#<?php echo htmlentities( $rowGrupoActions->modulo_tagchave, ENT_QUOTES, 'UTF-8');?>" >

                                <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_MODULO'); ?> : <span class="font-dark sbold uppercase"><?php echo htmlentities( $rowGrupoActions->modulo_nome, ENT_QUOTES, 'UTF-8');?></span>
                            </a>

                        </h4>
                    </div>
                    <div id="<?php echo htmlentities( $rowGrupoActions->modulo_tagchave, ENT_QUOTES, 'UTF-8');?>" class="panel-collapse collapse"  style="">
                        <div class="panel-body">
                            <div class="form-actions right">
                                <div class="row">
                                    <div  class="col-md-12 text-right">
                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=grupoactionedit&vdcleanstate=1&permadmin_grupo_id=' . $this->escape($this->data->permadmin_grupo_id)) . '#' . htmlentities( $rowGrupoActions->modulo_tagchave,ENT_QUOTES,'UTF-8'); ?>" class="btn btn-circle btn-outline green">
                                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_EDITARPERMISSOES'); ?>
                                </a>
                                    </div>
                                </div>
                            </div>

                            <div class="">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th width="15%" ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TIPO'); ?></th>
                                        <th width="35%" ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ACTION'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITCREATE'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITREAD'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITUPDATE'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITDELETE'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITREADALL'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
            <?php endif;?>
                                    <tr>

                                        <td> <?php echo htmlentities( $rowGrupoActions->tipo_nome, ENT_QUOTES, 'UTF-8');?> </td>
                                        <td> <?php if ((int)$rowGrupoActions->action_id > 0) {echo htmlentities( $rowGrupoActions->action_nome, ENT_QUOTES, 'UTF-8'); } else { echo $iconNotDefinedChecked; } ?>
                                            <h6 class="text-muted"><?php  echo htmlentities( $rowGrupoActions->modulo_nome . '|'. $rowGrupoActions->tipo_nome . '|' . $rowGrupoActions->action_descricao, ENT_QUOTES, 'UTF-8');  ?></h6>
                                        </td>
                                        <?php if ((int)$rowGrupoActions->action_id <=0) : ?>
                                            <td colspan="5">
                                                <h6 class="text-muted"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_ACTIONNOTDEFINED'); ?></h6>
                                            </td>
                                        <?php else : ?>
                                            <td><?php if((string)$rowGrupoActions->pcreate=='1') { echo $iconChecked; } else {  echo $iconNotChecked; } ?></td>
                                            <td><?php if((string)$rowGrupoActions->pread=='1') { echo $iconChecked; } else {  echo $iconNotChecked; } ?></td>
                                            <td><?php if((string)$rowGrupoActions->pupdate=='1') { echo $iconChecked; } else {  echo $iconNotChecked; } ?></td>
                                            <td><?php if((string)$rowGrupoActions->pdelete=='1') { echo $iconChecked; } else {  echo $iconNotChecked; } ?></td>
                                            <td><?php if((string)$rowGrupoActions->preadall=='1') { echo $iconChecked; } else {  echo $iconNotChecked; } ?></td>
                                        <?php endif; ?>
                                    </tr>

            <?php $prevModulo = $rowGrupoActions->modulo_tagchave; ?>
            <?php endforeach; ?>

            <?php if ($prevModulo!='') :  ?>
            </tbody>
            </table>
            </div>
            </div>
            </div>
            </div>
            <?php endif;?>
</div>