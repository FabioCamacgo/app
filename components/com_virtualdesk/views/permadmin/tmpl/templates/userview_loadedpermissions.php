<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);


$iconChecked    = '<i class="fa fa-check font-green-jungle"></i>';
$iconNotChecked = '<i class="fa fa-minus font-grey"></i>';
$iconNotDefinedChecked = '<i class="fa fa-minus font-grey"></i>';

?>
<div class="">
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="15%" ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_MODULO'); ?></th>
            <th width="15%" ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TIPO'); ?></th>
            <th width="35%" ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ACTION'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITCREATE'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITREAD'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITUPDATE'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITDELETE'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITREADALL'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITGRUPO'); ?></th>
            <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITUSER'); ?></th>
        </tr>
        </thead>
        <tbody>
<?php
    foreach($userPermArray as $keyModulo => $rowUserLoadedPermModulo) :

    foreach($rowUserLoadedPermModulo as $keyTipo => $rowUserLoadedPermTipo) :

    foreach($rowUserLoadedPermTipo as $keyAction => $rowUserLoadedPermAction) :

   ?>

                    <tr>
                        <td> <?php echo htmlentities( $keyModulo, ENT_QUOTES, 'UTF-8');?> </td>
                        <td> <?php echo htmlentities( $keyTipo, ENT_QUOTES, 'UTF-8');?> </td>
                        <td> <?php echo htmlentities( $keyAction, ENT_QUOTES, 'UTF-8');?> </td>

                        <td><?php
                            if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermCreate])) {
                                if((string) $rowUserLoadedPermAction[$objCheckPerm->bitPermCreate]=='1')  { echo $iconChecked; } else {  echo $iconNotChecked; }
                            }
                            ?>
                            <div class="text-grey"><?php
                                if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermCreateSrc])) {
                                    echo substr($rowUserLoadedPermAction[$objCheckPerm->bitPermCreateSrc], -1);
                                }
                                ?></div>
                        </td>
                        <td><?php
                            if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermRead])) {
                                if ((string)$rowUserLoadedPermAction[$objCheckPerm->bitPermRead] == '1') {
                                    echo $iconChecked;
                                } else {
                                    echo $iconNotChecked;
                                }
                            }
                            ?>

                            <div class="text-grey"><?php
                                if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermReadSrc])) {
                                    echo substr($rowUserLoadedPermAction[$objCheckPerm->bitPermReadSrc], -1);
                                }
                                ?></div>
                        </td>
                        <td><?php
                            if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermUpdate] )) {
                                if ((string)$rowUserLoadedPermAction[$objCheckPerm->bitPermUpdate] == '1') {
                                    echo $iconChecked;
                                } else {
                                    echo $iconNotChecked;
                                }
                            }
                            ?>
                            <div class="text-grey"><?php
                                if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermUpdateSrc])) {
                                    echo substr($rowUserLoadedPermAction[$objCheckPerm->bitPermUpdateSrc], -1);
                                }
                                ?></div>
                        </td>
                        <td><?php
                            if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermDelete] )) {
                                if ((string)$rowUserLoadedPermAction[$objCheckPerm->bitPermDelete] == '1') {
                                    echo $iconChecked;
                                } else {
                                    echo $iconNotChecked;
                                }
                            }
                            ?>
                            <div class="text-grey"><?php
                                if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermDeleteSrc])) {
                                    echo substr($rowUserLoadedPermAction[$objCheckPerm->bitPermDeleteSrc], -1);
                                }
                                ?></div>
                        </td>
                        <td><?php
                            if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermReadAll] )) {
                                if ((string)$rowUserLoadedPermAction[$objCheckPerm->bitPermReadAll] == '1') {
                                    echo $iconChecked;
                                } else {
                                    echo $iconNotChecked;
                                }
                            }
                            ?>
                            <div class="text-grey"><?php
                                if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermReadAllSrc])) {
                                    echo substr($rowUserLoadedPermAction[$objCheckPerm->bitPermReadAllSrc], -1);
                                }
                                ?></div>
                        </td>
                        <td><?php
                            if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermSourceG] )) {
                                if ((string)$rowUserLoadedPermAction[$objCheckPerm->bitPermSourceG] == '1') {
                                    echo $iconChecked;
                                } else {
                                    echo $iconNotChecked;
                                }
                            }
                            ?>
                            <div class="text-grey"><?php
                                if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermSourceGName])) {
                                    echo $rowUserLoadedPermAction[$objCheckPerm->bitPermSourceGName];
                                }
                                ?></div>
                        </td>
                        <td><?php
                            if(!empty($rowUserLoadedPermAction[$objCheckPerm->bitPermSourceU])) {
                                if ((string)$rowUserLoadedPermAction[$objCheckPerm->bitPermSourceU] == '1') {
                                    echo $iconChecked;
                                } else {
                                    echo $iconNotChecked;
                                }
                            }
                            ?>

                        </td>

                    </tr>


    <?php endforeach; ?>
    <?php endforeach; ?>
    <?php endforeach; ?>

        </tbody>
    </table>
</div>


