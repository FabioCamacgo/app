<?php
defined('_JEXEC') or die;


?>
var TableDatatablesManaged = function () {

    var initTable2 = function () {

        var table = jQuery('#tabela_lista_perm_modulo');
        var tableTools = jQuery('#tabela_lista_perm_modulo_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);
                        return ('<a href="' + row[3] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[3] + '">' + data + '</a>');
                    }
                },

                // Enabled
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        //ret = '<i class="fa fa-close font-red"></i>';
                        let currState = '';
                        if(data=='1') {
                            //ret = '<i class="fa fa-check font-green-jungle"></i>';
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabled" ' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-moduloenable="' + row[4] + '" ';
                        ret += ' data-vd-url-checkmoduloenable="' + row[5] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        //console.log(data);
                        return (ret);
                    }
                },

                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                },



            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=permadmin.getDataMainByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();

                UISwitch.init();

            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_perm_modulo_wrapper');

    }


    var initTable3 = function () {

        var table = jQuery('#tabela_lista_perm_groups');
        var tableTools = jQuery('#tabela_lista_perm_groups_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },


            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                    //"targets": [2]
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[3] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {

                        return ( data );
                    }
                },

                // isManager
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var ret = '<i class="fa fa-minus font-grey"></i> ';
                        if(data=='1') {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }


            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=permadmin.getDataGroupListMainByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_perm_groups_wrapper');

    }


    var initTable4 = function () {

        var table = jQuery('#tabela_lista_perm_tipo');
        var tableTools = jQuery('#tabela_lista_perm_tipo_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },


            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left"
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[2] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[2] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                },

                {
                    "targets": 3,
                    "visible":false
                }


            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=permadmin.getDataTipoListMainByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_perm_tipo_wrapper');

    }


    var initTableActions = function () {

        var table = jQuery('#tabela_lista_perm_action');
        var tableTools = jQuery('#tabela_lista_perm_action_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },


            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left"
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[5] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[5] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[5] + '">' + data + '</a>');
                    }
                },

                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }




            ],
            "order": [
                [3, "asc"],
                [0, "asc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=permadmin.getDataActionsListMainByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_perm_action_wrapper');

    }


    var initTableUsers = function () {

        var table = jQuery('#tabela_lista_perm_users');
        var tableTools = jQuery('#tabela_lista_perm_users_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[10] + '">' + data + '</a>');
                    }
                },

                // Blocked
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        var ret = '<i class="fa fa-minus font-grey"></i>';
                        if(data=='1') {
                            ret = '<i class="fa fa-close font-red"></i>';
                        }
                        return (ret);
                    }
                },

                // Activated
                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var ret = '<i class="fa fa-close font-red"></i>';
                        if(data=='1') {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },


                // VDADMIN
                {
                    "targets": 7,
                    "data": 7,
                    "render": function ( data, type, row, meta) {
                        var ret ='';
                        if(parseInt(data)>0) {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                // VDUSER
                {
                    "targets": 8,
                    "data": 8,
                    "render": function ( data, type, row, meta) {
                        var ret ='';
                        if(parseInt(data)>0) {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                // VDMANAGER
                {
                    "targets": 9,
                    "data": 9,
                    "render": function ( data, type, row, meta) {
                        var ret ='';
                        if(parseInt(data)>0) {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                {
                    "targets": 10,
                    "data": 10,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                },


            ],
            "order": [
                [0, "asc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=permadmin.getDataUsersListByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_perm_modulo_wrapper');

    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            // initTable1();

            initTable2();

            initTable3();

            initTable4();

            initTableActions();

            initTableUsers();
        }

    };

}();



var UISwitch = function () {

    var initSwitch =  function (evt) {
        jQuery("[name='enabled']").bootstrapSwitch();
    };

    var handleSwitch = function (evt) {
        jQuery("[name='enabled']").on('switchChange.bootstrapSwitch',function(evt,data){

            jQuery(this).bootstrapSwitch('disabled',true);

            let vd_url_moduloenable = jQuery(this).data('vd-url-moduloenable');
            let vd_url_checkmoduloenable = jQuery(this).data('vd-url-checkmoduloenable');

            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setModuloEnable(jQuery(this), vd_url_moduloenable, vd_url_checkmoduloenable , enableVal);
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            initSwitch();
            handleSwitch();
        }

    };

}();


var vdAjaxCall = function () {

    var setModuloEnable = function (el, vd_url_moduloenable, vd_url_checkmoduloenable, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_moduloenable = vd_url_moduloenable + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_moduloenable,
            type: "POST",
            indexValue: {el:el, vd_url_checkmoduloenable:vd_url_checkmoduloenable},
            success: function(data){
                //console.log('SUCESSO vd_url_moduloenable...');
                //console.log('data='+data);
                //console.log('this.indexValue.el='+ this.indexValue.el);
                //console.log('this.indexValue.vd_url_checkmoduloenable='+ this.indexValue.vd_url_checkmoduloenable);

                vdAjaxCall.getModuloEnable(this.indexValue.el, this.indexValue.vd_url_checkmoduloenable);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var getModuloEnable = function (el, vd_user_checkmoduloenable) {

        jQuery.ajax({
            url: vd_user_checkmoduloenable,
            type: "POST",
            indexValue: {el:el},
            success: function(data){
                ////console.log('SUCESSO vd_user_checkmoduloenable...');
                //console.log('this.indexValue.el='+ this.indexValue.el);

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                //console.log('data='+data);
                //console.log('currState='+currState);

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    return {

        setModuloEnable    : setModuloEnable,
        getModuloEnable    : getModuloEnable
    };

}();



jQuery(document).ready(function() {
        TableDatatablesManaged.init();


        // Verifica se recebeu tag #vdtab_


});