<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');


/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'moduloview'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$app    = JFactory::getApplication();
$jinput = $app->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');



//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');

$getInputPermModulo_id = JFactory::getApplication()->input->getInt('permadmin_modulo_id');

$this->data = array();
$this->data = VirtualDeskSitePermAdminHelper::getPermModuloDetail($getInputPermModulo_id);

if(!empty($this->data)) {
    // se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
}

//$itemmenuid_lista = $params->get('permadmin_menuitemid_list');

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-xs-10 { background-color: #fff !important;}
    .static-info { margin-bottom: 15px;}
    .static-info .value { border: 1px solid #ddd; background-color: #f6f6f6;;  font-weight: normal;  padding: 5px 10px;  border-radius: 10px;}
    .static-info .name  { font-weight: 600;  padding: 5px 10px;}
    .portlet { margin-bottom: 0px;}
    .portlet > .portlet-title > .caption {font-size: 14px;}
</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-lock  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_TAB_MODULOS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <?php
        // Data not empty ?
        if(!empty($this->data)) :
            ?>
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabModulos'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloedit&vdcleanstate=1&permadmin_modulo_id=' . $this->escape($this->data->permadmin_modulo_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloaddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        <?php else :
            ?>
        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabModulos'); ?>" class="btn btn-circle btn-default">
            <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloaddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
            <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
    </div>
        <?php endif;?>
        <!-- END TITLE ACTIONS -->
    </div>



    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">
            <div class="form-body">

                <?php
                // Data not empty ?
                if(!empty($this->data)) :
                ?>


                <div class="well">
                    <div class="row static-info ">
                        <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_ID' ).$labelseparator; ?></div>
                        <div class="col-md-4 value"> <?php echo htmlentities( $this->data->id, ENT_QUOTES, 'UTF-8');?> </div>
                    </div>
                    <div class="row static-info">
                        <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_NOME' ).$labelseparator; ?></div>
                        <div class="col-md-4 value"> <?php echo htmlentities( $this->data->nome, ENT_QUOTES, 'UTF-8');?> </div>
                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_TAG' ).$labelseparator; ?></div>
                        <div class="col-md-4 value"> <?php echo htmlentities( $this->data->tagchave, ENT_QUOTES, 'UTF-8');?> </div>

                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_LOGADMIN_CMP_ENABLED' ).$labelseparator; ?></div>
                        <div class="col-md-4 value"> <?php echo htmlentities( $this->data->enabled, ENT_QUOTES, 'UTF-8');?>

                            <input type="checkbox" disabled class="make-switch" value="1" <?php  if((string)($this->data->enabled=='1')) echo 'checked'; ?> data-on-color="info" data-off-color="success">
                        </div>

                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_LOGADMIN_CMP_ENABLED2DROPBOX' ).$labelseparator; ?></div>
                        <div class="col-md-4 value"> <?php echo htmlentities( $this->data->enabled2dropbox, ENT_QUOTES, 'UTF-8');?>

                            <input type="checkbox" disabled class="make-switch" value="1" <?php  if((string)($this->data->enabled2dropbox=='1')) echo 'checked'; ?> data-on-color="info" data-off-color="success">
                        </div>

                    </div>

                    <div class="row static-info">
                        <div class="col-md-2 name text-right"></div>
                        <div class="col-md-4 ">
                            <h6>
                                <?php
                                echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_CREATED' ).$labelseparator;
                                echo htmlentities( $this->data->created, ENT_QUOTES, 'UTF-8');
                                if($this->data->created != $this->data->modified) echo ' , ' . JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_MODIFIED' ) . ' ' . htmlentities( $this->data->modified, ENT_QUOTES, 'UTF-8');
                                ?>
                            </h6>
                        </div>
                    </div>
                </div>



            </div>


            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloedit&vdcleanstate=1&permadmin_modulo_id=' . $this->escape($this->data->permadmin_modulo_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabModulos'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('permadmin_modulo_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->permadmin_modulo_id) ,$setencrypt_forminputhidden); ?>"/>

            <?php
            // Data empty ?
            else :
                ?>
                <div class="form-actions right">
                    <div class="row">

                        <div  class="col-md-12 text-left">
                            <div class="alert alert-info fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                                <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_EMPTYLIST'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endif;
            ?>


        </form>
    </div>
</div>


<!-- Export Action URls to be encrypted...-->
<div style="display:none;">
    <a id="ExportActions_Print" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_modulo_id=' . $this->escape($this->data->permadmin_modulo_id).'&tmpl=tplprint' ); ?>"></a>
    <a id="ExportActions_PrintLandscape" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_modulo_id=' . $this->escape($this->data->permadmin_modulo_id).'&tmpl=tplprint&tmplorientation=landscape' ); ?>"></a>
    <a id="ExportActions_PrintPDF" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_modulo_id=' . $this->escape($this->data->permadmin_modulo_id).'&tmpl=tplprint&tmplformat=pdf' ); ?>"></a>
    <a id="ExportActions_PrintPDFLandscape" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_modulo_id=' . $this->escape($this->data->permadmin_modulo_id).'&tmpl=tplprint&tmplformat=pdf&tmplorientation=landscape' ); ?>"></a>
</div>

<?php echo $localScripts; ?>


<script>
jQuery(document).ready(function() {
    // handle action tools
    jQuery('#default_permadmin_tools li > a.tool-action').on('click', function() {

        var action = jQuery(this).attr('data-action');
        var actionURLPrint = '';
        //console.log('action= '+action);

        if(action=='0'){
            actionURLPrint = jQuery('#ExportActions_Print').attr('href');
            if( jQuery("#DocumentsBlockByGrid").length > 0 )
            {
                if(jQuery("#DocumentsBlockByGrid").find(".cbp-item").length > 0) actionURLPrint = jQuery('#ExportActions_PrintLandscape').attr('href');
            }
            //console.log('actionURLPrint= '+actionURLPrint);
            window.open(actionURLPrint)
        }

        if(action=='2'){
            //Verifica se tem ficheiros, nesse caso gera em landscape
           actionURLPrint = jQuery('#ExportActions_PrintPDF').attr('href');
           if( jQuery("#DocumentsBlockByGrid").length > 0 )
           {
               if(jQuery("#DocumentsBlockByGrid").find(".cbp-item").length > 0) actionURLPrint = jQuery('#ExportActions_PrintPDFLandscape').attr('href');
           }
           //console.log('actionURLPrint= '+actionURLPrint);
           window.open(actionURLPrint)
        }


    });
});


</script>