<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');



/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
if($vbInAdminGroup===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
    return false;
}
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'useredit'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}
?>