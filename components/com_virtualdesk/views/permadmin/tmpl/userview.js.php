<?php
defined('_JEXEC') or die;

?>
var TableDatatablesManaged = function () {

    var initTableGrupos= function () {

        var table = jQuery('#tabela_lista_perm_grupos');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                },

            ],
            "order": [
                [0, "asc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=permadmin.getDataUserGroupsListByAjax&permadmin_user_id=<?php echo($getInputPermUser_id); ?>",
            "drawCallback": function( settings ) {
                console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }


        });

    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableGrupos();
        }

    };

}();


var UIButtons = function () {

    var handleButtons = function (evt) {
        jQuery('.vd-button-blockuser2').click(function () {
            var btn = jQuery(this);
            console.log('blockuser2...');

            var sa_title = jQuery(this).data('title');
            var sa_message = jQuery(this).data('message');
            var sa_type = jQuery(this).data('type');
            var sa_allowOutsideClick = jQuery(this).data('allow-outside-click');
            var sa_showConfirmButton = jQuery(this).data('show-confirm-button');
            var sa_showCancelButton = jQuery(this).data('show-cancel-button');
            var sa_closeOnConfirm = jQuery(this).data('close-on-confirm');
            var sa_closeOnCancel = jQuery(this).data('close-on-cancel');
            var sa_confirmButtonText = jQuery(this).data('confirm-button-text');
            var sa_cancelButtonText = jQuery(this).data('cancel-button-text');
            var sa_confirmButtonClass = jQuery(this).data('confirm-button-class');
            var sa_cancelButtonClass = jQuery(this).data('cancel-button-class');

            var vd_url_blockuser = jQuery(this).data('vd-url-blockuser');
            var vd_url_checkuserblocked = jQuery(this).data('vd-url-checkuserblocked');

            swal({  title: sa_title,
                    text: sa_message,
                    type: sa_type,
                    allowOutsideClick: sa_allowOutsideClick,
                    showConfirmButton: sa_showConfirmButton,
                    showCancelButton: sa_showCancelButton,
                    confirmButtonClass: sa_confirmButtonClass,
                    cancelButtonClass: sa_cancelButtonClass,
                    closeOnConfirm: sa_closeOnConfirm,
                    closeOnCancel: sa_closeOnCancel,
                    confirmButtonText: sa_confirmButtonText,
                    cancelButtonText: sa_cancelButtonText
                },
                function(isConfirm){
                    if (isConfirm){

                        vdAjaxCall.setUserBlock('.vd-button-blockuser2', vd_url_blockuser, vd_url_checkuserblocked);

                    }

                });
        });

    }


    var handleButtons3 = function (evt) {
        jQuery('.vd-button-sendactivationuser').click(function () {
            var btn = jQuery(this);
            console.log('sendactivationuser...');

            var sa_title = jQuery(this).data('title');
            var sa_message = jQuery(this).data('message');
            var sa_type = jQuery(this).data('type');
            var sa_allowOutsideClick = jQuery(this).data('allow-outside-click');
            var sa_showConfirmButton = jQuery(this).data('show-confirm-button');
            var sa_showCancelButton = jQuery(this).data('show-cancel-button');
            var sa_closeOnConfirm = jQuery(this).data('close-on-confirm');
            var sa_closeOnCancel = jQuery(this).data('close-on-cancel');
            var sa_confirmButtonText = jQuery(this).data('confirm-button-text');
            var sa_cancelButtonText = jQuery(this).data('cancel-button-text');
            var sa_confirmButtonClass = jQuery(this).data('confirm-button-class');
            var sa_cancelButtonClass = jQuery(this).data('cancel-button-class');

            var vd_url_sendactivationuser = jQuery(this).data('vd-url-sendactivationuser');
            var vd_url_checkuseractivated = jQuery(this).data('vd-url-checkuseractivated');

            swal({  title: sa_title,
                    text: sa_message,
                    type: sa_type,
                    allowOutsideClick: sa_allowOutsideClick,
                    showConfirmButton: sa_showConfirmButton,
                    showCancelButton: sa_showCancelButton,
                    confirmButtonClass: sa_confirmButtonClass,
                    cancelButtonClass: sa_cancelButtonClass,
                    closeOnConfirm: sa_closeOnConfirm,
                    closeOnCancel: sa_closeOnCancel,
                    confirmButtonText: sa_confirmButtonText,
                    cancelButtonText: sa_cancelButtonText
                },
                function(isConfirm){
                    if (isConfirm){

                        vdAjaxCall.sendUserActivation('.vd-button-sendactivationuser', vd_url_sendactivationuser, vd_url_checkuseractivated);

                    }

                });
        });

    }


    return {
        //main function to initiate the module
        init: function () {
            handleButtons();
            handleButtons3();
        }

    };

}();


var vdAjaxCall = function () {

    var setUserBlock = function (el, vd_url_blockuser, vd_url_checkuserblocked) {
        var vdButtonBlockUser = jQuery(el);
        var vdClosestI = vdButtonBlockUser.parent().find('span.vd_span_button_blockuser > i.fa');
        vdClosestI.removeClass('fa-minus font-grey fa-close font-red').addClass("fa-spin fa-spinner fa-lg");
        var vdLadda = Ladda.create(vdButtonBlockUser[0]);
        vdLadda.start();

        jQuery.ajax({
            url: vd_url_blockuser,
            type: "POST",
            indexValue: {el:el, vd_url_checkuserblocked:vd_url_checkuserblocked},
            success: function(data){
                console.log('SUCESSO vd_url_blockuser...');
                console.log('data='+data);
                console.log('this.indexValue.el='+ this.indexValue.el);
                console.log('this.indexValue.vd_url_checkuserblocked='+ this.indexValue.vd_url_checkuserblocked);

                vdAjaxCall.getUserBlock(this.indexValue.el, this.indexValue.vd_url_checkuserblocked);

            },
            error: function(error){
                var elem       = jQuery(el);
                var vdClosestI = elem.parent().find('span.vd_span_button_blockuser > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
            }
        }).always(
            function() {
                vdLadda.stop();
            }
        );
    }

    var getUserBlock = function (el, vd_user_checkuserblocked) {

        jQuery.ajax({
            url: vd_user_checkuserblocked,
            type: "POST",
            indexValue: {el:el},
            success: function(data){
                console.log('SUCESSO vd_user_checkuserblocked...');
                console.log('data='+data);
                console.log('this.indexValue.el='+ this.indexValue.el);
                var elem = jQuery(el);
                console.log('jQuery(el)='+elem.length);
                var vdClosestI = elem.parent().find('span.vd_span_button_blockuser > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                console.log('vdClosestI='+vdClosestI.length);
                if(data=='1') {
                    vdClosestI.removeClass('fa-minus font-grey').addClass("fa-close font-red");
                }
                else if(data=='0'){
                    vdClosestI.removeClass('fa-close font-red').addClass("fa-minus font-grey");
                }
                else {
                    vdClosestI.html('Erro!');
                }
            }
        });
    }

    var sendUserActivation = function (el, vd_url_sendactivationuser, vd_url_checkuseractivated) {
        var vdButtonSendActivationUser = jQuery(el);
        var vdClosestI = vdButtonSendActivationUser.parent().find('span.vd_span_button_sendactivationuser > i.fa');
        vdClosestI.removeClass('fa-minus font-grey fa-close font-red').addClass("fa-spin fa-spinner fa-lg");

        var vdLadda = Ladda.create(vdButtonSendActivationUser[0]);
        vdLadda.start();

        jQuery.ajax({
            url: vd_url_sendactivationuser,
            type: "POST",
            indexValue: {el:el, vd_url_checkuseractivated:vd_url_checkuseractivated},
            success: function(data){
                console.log('SUCESSO vd_user_checkuserblocked...');
                console.log('data='+data);
                console.log('this.indexValue.el='+ this.indexValue.el);
                console.log('this.indexValue.vd_url_checkuseractivated='+ this.indexValue.vd_url_checkuseractivated);
                var elem = jQuery(el);
                console.log('jQuery(el)='+elem.length);

                vdAjaxCall.getUserActivated(this.indexValue.el, this.indexValue.vd_url_checkuseractivated);

            },
            error: function(error){
                var elem = jQuery(el);
                var vdClosestI = elem.parent().find('span.vd_span_button_sendactivationuser > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
            }
        }).always(
            function() {
                vdLadda.stop();
            }
        );
    }

    var getUserActivated = function (el, vd_url_checkuseractivated) {
        jQuery.ajax({
            url: vd_url_checkuseractivated,
            type: "POST",
            indexValue: {el:el},
            success: function(data){
                console.log('SUCESSO vd_url_checkuseractivated...');
                console.log('data='+data);
                console.log('this.indexValue.el='+ this.indexValue.el);
                var elem = jQuery(el);
                console.log('jQuery(el)='+elem.length);
                var vdClosestI = elem.parent().find('span.vd_span_button_sendactivationuser > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                if(data=='0') {
                    vdClosestI.removeClass('fa-check font-green-jungle').addClass("fa-close font-red");
                }
                else if(data=='1') {
                    vdClosestI.removeClass('fa-close font-red ').addClass("fa-check font-green-jungle");
                }
                else {
                    vdClosestI.html('Erro!');
                }

            },
            error: function(error){
                var elem = jQuery(el);
                var vdClosestI        = elem.parent().find('span.vd_span_button_sendactivationuser > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
            }
        });
    }

    return {

        setUserBlock     : setUserBlock,
        getUserBlock     : getUserBlock,
        sendUserActivation : sendUserActivation,
        getUserActivated : getUserActivated
    };

}();


jQuery(document).ready(function() {
    TableDatatablesManaged.init();

    UIButtons.init();

});