<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');


/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
if($vbInAdminGroup===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
    return false;
}
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'useractionedit'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$app    = JFactory::getApplication();
$jinput = $app->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$labelseparator = ' : ';

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/permadmin/tmpl/useractionedit.js'  . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');


//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');

$getInputPermUser_id = JFactory::getApplication()->input->getInt('permadmin_user_id');

$this->data = array();
$this->data = VirtualDeskSitePermAdminHelper::getPermUserDetail($getInputPermUser_id);
$this->dataUserActions = array();
$this->dataUserActions = VirtualDeskSitePermAdminHelper::getPermUserActionsDetail($getInputPermUser_id);


// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.useractionedit.permadmin.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}


// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
   .form-group { margin-bottom: 0;}
   .mt-checkbox.mt-checkbox-outline.vdcheckbox {margin-bottom: 0;}
   .form-horizontal .form-group .mt-checkbox-inline {padding-top: 0;}
   .mt-checkbox-inline {padding: 0;}
   #portletUser.portlet, #portletListPerm.portlet {padding-bottom: 0px;}
   .mt-checkbox > span::after {border-color: #26C281;}

   .static-info { margin-bottom: 15px;}
   .static-info .value { border: 1px solid #ddd; background-color: #f6f6f6;;  font-weight: normal;  padding: 5px 10px;  border-radius: 10px;}
   .static-info .name  { font-weight: 600;  padding: 5px 10px;}
   .portlet { margin-bottom: 0px;}
   .portlet > .portlet-title > .caption {font-size: 14px;}
   .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled.collapsed { background-position: left 12px; margin-left:15px; padding-left: 25px;}
   .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled { background-position: left -19px; margin-left:15px; padding-left: 25px;}

</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-lock font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_TAB_USERS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_EDITARPERMISSOES' ); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id)); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>



    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <?php
        // Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
        ?>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body ">

        <div class="portlet light " id="portletUser">
            <div class="portlet-title">
                <div class="caption font-blue  ">
                    <i class="fa fa-user font-blue"></i>
                    <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_USERINFO'); ?>
                </div>
            </div>

            <div class="portlet-body ">
                <div class="well">
                    <div class="row static-info ">
                        <div class="col-md-1 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_ID' ).$labelseparator; ?></div>
                        <div class="col-md-1 value"> <?php echo htmlentities( $this->data->id, ENT_QUOTES, 'UTF-8');?> </div>
                        <div class="col-md-1 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_LOGIN' ).$labelseparator; ?></div>
                        <div class="col-md-1 value"> <?php echo htmlentities( $this->data->login, ENT_QUOTES, 'UTF-8');?> </div>
                        <div class="col-md-1 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_NOME' ).$labelseparator; ?></div>
                        <div class="col-md-2 value"> <?php echo htmlentities( $this->data->name, ENT_QUOTES, 'UTF-8');?> </div>
                        <div class="col-md-1 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_EMAIL' ).$labelseparator; ?></div>
                        <div class="col-md-2 value"> <?php echo htmlentities( $this->data->email, ENT_QUOTES, 'UTF-8');?> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="portlet-body ">

        <div class="portlet light " id="portletListPerm">
            <div class="portlet-title">
                <div class="caption font-blue ">
                    <i class="fa fa-lock font-blue"></i>
                    <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_LISTAPERMISSOES'); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="portlet-body form">

        <form id="form-permadminuser"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.useractionupdate'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="row" id="">
                <div class="col-xs-12">

                    <div class="panel-group accordion" id="accordion1">
                        <?php
                        $currModulo = '';
                        $prevModulo = '';
                        $iconNotDefinedChecked = '<i class="fa fa-minus font-grey"></i>';
                        $vbShowCheckValsdbg = false;
                        foreach($this->dataUserActions as $rowUserActions) :
                        $currModulo = $rowUserActions->modulo_tagchave;
                        //inicia um novo acordion
                        if($prevModulo!=$currModulo) :  ?>

                        <?php if( ($prevModulo!=$currModulo) && ($prevModulo!='')) :  ?>
                        </tbody>
                        </table>
                        </div>

                        </div>
                        </div>
                        </div>
                        <?php endif;?>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion1" href="#<?php echo htmlentities( $rowUserActions->modulo_tagchave, ENT_QUOTES, 'UTF-8');?>" >

                                        <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_MODULO'); ?> : <span class="font-dark sbold uppercase"><?php echo htmlentities( $rowUserActions->modulo_nome, ENT_QUOTES, 'UTF-8');?></span>

                                    </a>
                                </h4>
                            </div>
                            <div id="<?php echo htmlentities( $rowUserActions->modulo_tagchave, ENT_QUOTES, 'UTF-8');?>" class="panel-collapse collapse"  style="">
                                <div class="panel-body">

                                <div class="">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr >
                                            <th width="15%" ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TIPO'); ?></th>
                                            <th width="35%" ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ACTION'); ?></th>
                                            <th class="text-center"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITCREATE'); ?></th>
                                            <th class="text-center"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITREAD'); ?></th>
                                            <th class="text-center"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITUPDATE'); ?></th>
                                            <th class="text-center"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITDELETE'); ?></th>
                                            <th class="text-center"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_BITREADALL'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php endif;?>
                                                    <tr >
                                                        <td> <?php echo htmlentities( $rowUserActions->tipo_nome, ENT_QUOTES, 'UTF-8');?> </td>
                                                        <td>  <?php if ((int)$rowUserActions->action_id > 0) {echo htmlentities( $rowUserActions->action_nome, ENT_QUOTES, 'UTF-8'); } else { echo $iconNotDefinedChecked; } ?>
                                                            <h6 class="text-muted"><?php  echo htmlentities( $rowUserActions->modulo_nome . '|'. $rowUserActions->tipo_nome . '|' . $rowUserActions->action_descricao, ENT_QUOTES, 'UTF-8');  ?></h6>
                                                        </td>
                                                        <?php if ((int)$rowUserActions->action_id <=0) : ?>
                                                            <td colspan="5"><h6 class="text-muted"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_ACTIONNOTDEFINED'); ?></h6> </td>
                                                        <?php else :
                                                            $chaveCurrCheckBox ="uaid[". $rowUserActions->modulo_id."_".$rowUserActions->tipo_id."_".$rowUserActions->action_id."_".$rowUserActions->uactions_id."]";
                                                        ?>
                                                        <td class="text-center">
                                                            <div class="form-group">
                                                            <div class="mt-checkbox-inline">
                                                                <?php if((int)$rowUserActions->tipo_id==1) : ?>
                                                                <label class="mt-checkbox mt-checkbox-outline vdcheckbox">
                                                                   <input type="checkbox" value="1" name="<?php echo $chaveCurrCheckBox."[c]"; ?>"  <?php if($rowUserActions->pcreate=="1") echo('checked'); ?> />
                                                                   <h6><?php if($vbShowCheckValsdbg==true ) echo (int)$rowUserActions->pcreate;?></h6>
                                                                   <span></span>
                                                                </label>
                                                                <?php else: echo $iconNotDefinedChecked; ?>
                                                                <?php endif; ?>
                                                            </div>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="form-group">
                                                                <div class="mt-checkbox-inline">
                                                                    <label class="mt-checkbox mt-checkbox-outline vdcheckbox">
                                                                        <input type="checkbox" value="1" name="<?php echo $chaveCurrCheckBox."[r]"; ?>"  <?php if($rowUserActions->pread=="1") echo('checked'); ?> />
                                                                        <h6><?php if($vbShowCheckValsdbg==true ) echo (int)$rowUserActions->pread;?></h6>
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="form-group">
                                                                <div class="mt-checkbox-inline">
                                                                    <?php if((int)$rowUserActions->tipo_id==1) : ?>
                                                                    <label class="mt-checkbox mt-checkbox-outline vdcheckbox">
                                                                        <input type="checkbox" value="1" name="<?php echo $chaveCurrCheckBox."[u]"; ?>"  <?php if($rowUserActions->pupdate=="1") echo('checked'); ?> />
                                                                        <h6><?php if($vbShowCheckValsdbg==true ) echo (int)$rowUserActions->pupdate;?></h6>
                                                                        <span></span>
                                                                    </label>
                                                                    <?php else: echo $iconNotDefinedChecked; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="form-group">
                                                                <div class="mt-checkbox-inline">
                                                                    <?php if((int)$rowUserActions->tipo_id==1) : ?>
                                                                    <label class="mt-checkbox mt-checkbox-outline vdcheckbox">
                                                                        <input type="checkbox" value="1" name="<?php echo $chaveCurrCheckBox."[d]"; ?>"  <?php if($rowUserActions->pdelete=="1") echo('checked'); ?> />
                                                                        <h6><?php if($vbShowCheckValsdbg==true ) echo (int)$rowUserActions->pdelete;?></h6>
                                                                        <span></span>
                                                                    </label>
                                                                    <?php else: echo $iconNotDefinedChecked; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="form-group">
                                                                <div class="mt-checkbox-inline">
                                                                    <?php if((int)$rowUserActions->tipo_id==1 || (int)$rowUserActions->tipo_id==2) : ?>
                                                                    <label class="mt-checkbox mt-checkbox-outline vdcheckbox">
                                                                        <input type="checkbox" value="1" name="<?php echo $chaveCurrCheckBox."[rall]"; ?>"  <?php if($rowUserActions->preadall=="1") echo('checked'); ?> />
                                                                        <h6><?php if($vbShowCheckValsdbg==true ) echo (int)$rowUserActions->preadall;?></h6>
                                                                        <span></span>
                                                                    </label>
                                                                    <?php else: echo $iconNotDefinedChecked; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <?php endif; ?>
                                                    </tr>

                            <?php $prevModulo = $rowUserActions->modulo_tagchave; ?>
                        <?php endforeach; ?>

                        <?php if ($prevModulo!='') :  ?>
                            </tbody>
                            </table>
                            </div>

                            </div>
                            </div>
                            </div>
                        <?php endif;?>

                     </div>

                </div>
            </div>



            <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id)); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
            </div>


               <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('permadmin_user_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->permadmin_user_id) ,$setencrypt_forminputhidden); ?>"/>

               <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"            value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
               <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"              value="<?php echo $obVDCrypt->formInputValueEncrypt('permadmin.useractionupdate',$setencrypt_forminputhidden); ?>"/>

               <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>


<?php echo $localScripts; ?>
