<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteUserFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');


/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
if($vbInAdminGroup===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
    return false;
}
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userfulledit'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Parametros da Password Chyeck de modo a podermos utilizar na validação javascript
$passwordcheck_length      = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_length', 8);
$passwordcheck_no_name     = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_name');
$passwordcheck_no_email    = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_no_email');
$passwordcheck_types_azmin = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmin');
$passwordcheck_types_azmai = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_azmai');
$passwordcheck_types_num   = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_num');
$passwordcheck_types_special = (int) JComponentHelper::getParams('com_virtualdesk')->get('passwordcheck_types_special');


// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';


$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdeskpasswordcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js' . $addscript_end;




$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');


//Parâmetros
$params       = JComponentHelper::getParams('com_virtualdesk');
$useravatar_extensions = $params->get('useravatar_extensions');
$useravatar_max_size_Mb = (int) $params->get('useravatar_max_size');
$useravatar_max_size   = (int) $params->get('useravatar_max_size')*1024*1024;
$useravatar_width      = $params->get('useravatar_width');
$useravatar_height     = $params->get('useravatar_height');
$useravatar_defaultimg = $params->get('useravatar_defaultimg');
$userfield_login_allowedit = $params->get('userfield_login_allowedit');


$getInputPermUser_id = JFactory::getApplication()->input->getInt('permadmin_user_id');
$this->data = array();
##$this->data = VirtualDeskSitePermAdminHelper::getPermUserDetail($getInputPermUser_id);
$LoadedUserJoomlaID = VirtualDeskSiteUserHelper::getJoomlaIDFromVDUserId($getInputPermUser_id);
$this->data   = VirtualDeskSiteUserHelper::getUserFromJoomlaID($LoadedUserJoomlaID);



// se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(is_array($this->data->useravatar)) $this->data->useravatar = '';

// Definição dos campos de utilizador
// require_once(JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_userfields.php');
$ObjUserFields      =  new VirtualDeskSiteUserFieldsHelper();
$arUserFieldsConfig = $ObjUserFields->getUserFields();
$arUserFieldLoginConfig = $ObjUserFields->getUserFieldLogin();
$ListaDeConcelhos = VirtualDeskSiteUserFieldsHelper::getConcelhosList();
// Multichoice / select2 : Áreas de Atuação
$AreaActListAllNyGroup = VirtualDeskSiteUserFieldsHelper::getAreaActListAllByGroup($fileLangSufix);
$ListaDeSetorAtividade = VirtualDeskSiteUserFieldsHelper::getSetorAtividadeList($fileLangSufix);

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto; margin:0;}
    .profile-userpic .botaoInserirImg {margin-top: 10px; margin-left: 60px;}
    .fileinput{display:inline-block;}
    .fileinput-preview {display:inline-block; max-height: 100%; }
    .fileinput-preview img {display:inline-block; float: none; margin: 0 auto;}
    .select2-bootstrap-prepend .input-group-btn {vertical-align: bottom};
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-user font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>
    </div>




    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <?php
        /* Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js) */
        ?>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null) {
                    return ('');
                }
                if (nErrors == 1) {
                    return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>';
                }
                else if (nErrors > 1) {
                    var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s", nErrors) );
                }
            };
        }
    </script>


    <script>
        <?php
        /*
         * Parametros e Mensagens para a verificação da password em javascript / jquery validation
         */
        ?>
        var virtualDeskPassCheckOptions = {
            // check min length
            m_minLength       :<?php echo $passwordcheck_length ?>
            ,message_MinLength : "<?php echo str_replace('%s','{0}',JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_LENGTH')); ?>"
            // check login in pass
            ,p_Username        :<?php echo $passwordcheck_no_name ?>
            ,p_UsernameElem    : 'login'
            ,message_Username  : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_NAME'); ?>"
            // check email in pass
            ,p_Email           :<?php echo $passwordcheck_no_name ?>
            ,p_EmailElem       : 'email1'
            ,message_Email     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_EMAIL'); ?>"
            //check cchar
            ,message_chartype  : "<?php echo str_replace('%s','{0}',JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE')); ?>"
            // check letras minusculas
            ,p_AZMin           :<?php echo $passwordcheck_types_azmin ?>
            ,message_AZMin     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_LOWERCASELETTER'); ?>"
            // check letras minusculas
            ,p_AZMai           :<?php echo $passwordcheck_types_azmai ?>
            ,message_AZMai     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_UPPERCASELETTER'); ?>"
            // check números
            ,p_Num           :<?php echo $passwordcheck_types_num ?>
            ,message_Num     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_NUMBER'); ?>"
            // check números
            ,p_SpecialChar           :<?php echo $passwordcheck_types_special ?>
            ,message_SpecialChar     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_TYPE_SPECIALCHARACTER'); ?>"
            // check espaços
            ,message_Spaces     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_STARTSENDS'); ?>"
            // check pass1 e pass2
            ,message_pass1pass2     : "<?php echo JText::_('COM_VIRTUALDESK_FORCEPASSWORDCOMPLEXITY_ERROR_PASS1PASS2'); ?>"

        };

        // js para identificar se o login é do tipo nif
        var setUserFieldLoginTypeNIF_JS = <?php echo $arUserFieldLoginConfig['setUserFieldLoginTypeNIF_JS']; ?>;

        // mensagem para a validação de um campo tipo nif
        var virtualDeskNIFCheckOptions = {
            message_NIFInvalid : "<?php echo JText::_('COM_VIRTUALDESK_USER_INVALID_NIF'); ?>"
        };
    </script>


    <div class="portlet-body form">

        <form id="member-profile"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.userfullupdate'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="form-body">

                <div class="row">
                    <div class="col-md-10">

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo $arUserFieldLoginConfig['CampoForm_Username_Label']; ?></label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <?php // verifica se o campo de login é editável ou não de acord com parametro de config
                                    if ( $userfield_login_allowedit == 0 ) : ?>
                                        <div class="input-icon">
                                            <i class="fa fa-user"></i>
                                            <input type="text" required class="form-control" disabled value="<?php echo htmlentities($this->data->login, ENT_QUOTES, 'UTF-8'); ?>">
                                        </div>
                                    <?php else  : ?>
                                        <div class="input-icon">
                                            <i class="fa fa-user"></i>
                                            <input type="text" required class="form-control" placeholder="Enter login"
                                                   disabled name="login" id="login" maxlength="<?php echo $arUserFieldLoginConfig['CampoForm_Username_MaxLength']; ?>"
                                                   value="<?php echo htmlentities($this->data->login, ENT_QUOTES, 'UTF-8'); ?>">
                                        </div>
                                        <span class="input-group-btn">
                                       <button id="vdChangeProfileLogin" class="btn btn-success" type="button">
                                           <i class="icon-pencil"></i>
                                        </button>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_LABEL'); ?></label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <div class="input-icon">
                                        <i class="fa fa-key fa-fw"></i>
                                        <input type="password" class="form-control" placeholder="Enter Password"
                                               disabled name="password1" id="password1" maxlength="25"/>
                                    </div>
                                    <span class="input-group-btn">
                               <button id="vdChangeProfilePassword" class="btn btn-success" type="button">
                                    <i class="icon-pencil"></i>
                                </button>
                            </span>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_PASSWORD_REPEAT_LABEL'); ?></label>
                            <div class="col-md-10">

                                <div class="input-icon">
                                    <i class="fa fa-lock fa-fw"></i>
                                    <input type="password" class="form-control" placeholder="Confirm Password" disabled
                                           name="password2" id="password2" maxlength="25"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_LABEL'); ?></label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <div class="input-icon">
                                        <i class="fa fa-envelope fa-fw"></i>
                                        <input type="email" required class="form-control" placeholder="Email Address"
                                               disabled name="email1" id="email1" maxlength="250"
                                               value="<?php echo htmlentities($this->data->email1, ENT_QUOTES, 'UTF-8'); ?>">
                                    </div>
                                    <span class="input-group-btn">
                               <button id="vdChangeProfileEmail" class="btn btn-success" type="button">
                                   <i class="icon-pencil"></i>
                                </button>
                            </span>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_EMAIL_REPEAT_LABEL'); ?></label>
                            <div class="col-md-10">
                                <div class="input-icon">
                                    <i class="fa fa-envelope fa-fw"></i>
                                    <input type="email" required class="form-control" placeholder="Email Address"
                                           disabled name="email2" id="email2" maxlength="250"
                                           value="<?php echo htmlentities($this->data->email2, ENT_QUOTES, 'UTF-8'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_PHOTO_LABEL' ); ?></label>
                            <div class="col-md-10">
                                <div class="profile-userpic">

                                    <?php if (empty($this->data->useravatar)) : ?>
                                        <img id="loadedAvatarImg" class="img-responsive" src="<?php echo $useravatar_defaultimg ?>"/>
                                    <?php else : ?>
                                        <img id="loadedAvatarImg" class="img-responsive" src="<?php echo $this->params->get('useravatar_folder') . DS . $this->data->useravatar ?>"/>
                                    <?php endif; ?>

                                    <div class="fileinput fileinput-new" data-provides="fileinput">

                                        <div class="fileinput-preview fileinput-exists " style="<?php echo $useravatar_width; ?>px; max-height: <?php echo $useravatar_height; ?>px; line-height: 10px;"></div>
                                        <div class="botaoInserirImg" >
                                    <span class="btn default btn-file">
                                         <span class="fileinput-new"> <?php echo JText::_('COM_VIRTUALDESK_PROFILE_AVATAR_SELECTIMAGE'); ?> </span>
                                         <span class="fileinput-exists"> <?php echo JText::_('COM_VIRTUALDESK_PROFILE_AVATAR_ALTERAR'); ?> </span>
                                         <input type="hidden" value=""  name="...">
                                         <input type="file"  name="useravatar" id="useravatar">
                                    </span>
                                            <a href="javascript:;" class="btn default fileinput-exists"   data-dismiss="fileinput"><?php echo JText::_('COM_VIRTUALDESK_PROFILE_AVATAR_REMOVE'); ?></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?php foreach ($ObjUserFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                            <?php if($arUserFieldsConfig['UserField_' . $valConfFieldNames . '_Profile' ]===true) : ?>
                                <?php if ($valConfFieldNames == 'NIF') : ?>

                                    <?php
                                    // Apresenta o campo apenas se o login não for do tipo NIF E se o campo NIF estiver ativo na configuração para esta view
                                    if($arUserFieldLoginConfig['setUserFieldLoginTypeNIF']==false && $arUserFieldsConfig['UserField_NIF_Registration']===true) : ?>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL'); ?></label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" autocomplete="off"  <?php echo $arUserFieldsConfig['UserField_NIF_Required']; ?>
                                                       placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_FISCALID_LABEL'); ?>"
                                                       name="fiscalid" id="fiscalid" maxlength="9" value="<?php echo htmlentities($this->data->fiscalid, ENT_QUOTES, 'UTF-8'); ?>">
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                <?php elseif ($valConfFieldNames == 'CAEPRINCIPAL') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <div class="col-md-10">
                                            <input type="text" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                                   placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL'); ?>"
                                                   name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                                   value="<?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?>"/>
                                            <span class="help-block"> <?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_HELPBLOCK' ); ?></span>
                                        </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'CIVILID') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <div class="col-md-10">
                                            <input type="text" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                                   placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL'); ?>"
                                                   name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                                   maxlength="10"
                                                   value="<?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?>"/>
                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'CONCELHO') : ?>

                                    <?php if(empty($this->data->concelho)) $this->data->concelho = (int) JComponentHelper::getParams('com_virtualdesk')->get('userfield_concelho_defaultselect'); ?>

                                    <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_LOCALIZ_TITLE'); ?></h4>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label "><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <span class="ladda-label"></span>
                                        <div class="col-md-10">

                                            <select name="<?php echo $keyConfFieldNames; ?>"  id="<?php echo $keyConfFieldNames; ?>"
                                                <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control "  tabindex="-1">
                                                <option></option>
                                                <?php foreach($ListaDeConcelhos as $rowConcelho) : ?>
                                                    <option value="<?php echo $rowConcelho['id']; ?>" <?php if($this->data->concelho == $rowConcelho['id']) echo 'selected'; ?>><?php echo $rowConcelho['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        </div>
                                    </div>

                                <?php elseif ($valConfFieldNames == 'FREGUESIA') : ?>

                                    <?php
                                    // Carrega freguesias se o id de concelho estiver definido.
                                    $ListaDeFreguesias = array();
                                    if( (int) $this->data->concelho > 0) $ListaDeFreguesias = VirtualDeskSiteUserFieldsHelper::getFreguesiaByConcelhoList($this->data->concelho);
                                    ?>

                                    <div id="blocoFreguesia" class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <div class="col-md-10">

                                            <select name="<?php echo $keyConfFieldNames; ?>"  id="<?php echo $keyConfFieldNames; ?>"
                                                <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?>
                                                <?php if( (int)$this->data->concelho <= 0 || empty($ListaDeFreguesias) ) echo 'disabled'; ?>
                                                    class="form-control " tabindex="-1">
                                                <option></option>
                                                <?php foreach($ListaDeFreguesias as $rowFreguesia) : ?>
                                                    <option value="<?php echo $rowFreguesia['id']; ?>" <?php if($this->data->freguesia == $rowFreguesia['id']) echo 'selected'; ?>><?php echo $rowFreguesia['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'MAPLATLONG') : ?>

                                    <div class="form-group">


                                        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                        <input type="hidden" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                               name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                               value="<?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?>"/>

                                    </div>

                                <?php elseif ($valConfFieldNames == 'ADDRESS') : ?>

                                    <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_CONTACTS_TITLE'); ?></h4>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <div class="col-md-10">
                                            <textarea <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control" rows="3"
                                                                                                                                        placeholder="<?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?>"
                                                                                                                                        name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                                                                                                                        maxlength="250"><?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?></textarea>
                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'SECTORATIVIDADE') : ?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label "><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <span class="ladda-label"></span>
                                        <div class="col-md-10">

                                            <select name="<?php echo $keyConfFieldNames; ?>"  id="<?php echo $keyConfFieldNames; ?>"
                                                <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control "  tabindex="-1">
                                                <option></option>
                                                <?php foreach($ListaDeSetorAtividade as $rowSetor) : ?>
                                                    <option value="<?php echo $rowSetor['id']; ?>" <?php if($this->data->sectoratividade == $rowSetor['id']) echo 'selected'; ?>><?php echo $rowSetor['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'AREAACT') : ?>
                                    <div class="form-group">

                                        <label class="col-md-2 control-label"><?php echo JText::_('COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL'); ?></label>
                                        <div class="col-md-10">

                                            <div class="input-group select2-bootstrap-prepend">
                                                <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-select2-open="<?php echo $keyConfFieldNames; ?>">
                                                            <span class="glyphicon glyphicon-search"></span>
                                                        </button>
                                                </span>
                                                <div>
                                                    <select name="<?php echo $keyConfFieldNames; ?>[]"  id="<?php echo $keyConfFieldNames; ?>"
                                                        <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?>
                                                            class="form-control input-lg select2-multiple select2-hidden-accessible" multiple tabindex="-1" aria-hidden="true">
                                                        <option></option>
                                                        <?php foreach($AreaActListAllNyGroup as $rowGroupAreaAct) : ?>
                                                            <optgroup label="<?php echo $rowGroupAreaAct['group']['groupname']; ?>">
                                                                <?php foreach($rowGroupAreaAct['rows'] as  $rowAreaAct) : ?>
                                                                    <option value="<?php echo $rowAreaAct['id']; ?>"
                                                                        <?php if(is_array($this->data->areaact)) {
                                                                            if( in_array($rowAreaAct['id'],$this->data->areaact) )
                                                                            {
                                                                                echo 'selected';
                                                                            }
                                                                        }
                                                                        ?>
                                                                    ><?php echo $rowAreaAct['name']; ?></option>
                                                                <?php endforeach; ?>
                                                            </optgroup>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                <?php elseif ($valConfFieldNames == 'VERACIDADEDADOS') : ?>

                                <?php elseif ($valConfFieldNames == 'POLITICAPRIVACIDADE') : ?>


                                <?php else: ?>

                                    <?php if ($valConfFieldNames == 'FIRMA') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_ENTERPRISE_TITLE'); ?></h4>
                                    <?php endif; ?>

                                    <?php if ($valConfFieldNames == 'MANAGERNAME') : ?>
                                        <h4 class="form-section"><?php echo JText::_('COM_VIRTUALDESK_USER_SECTION_MANAGER_TITLE'); ?></h4>
                                    <?php endif; ?>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <div class="col-md-10">
                                            <input type="text" <?php echo $arUserFieldsConfig['UserField_' .$valConfFieldNames. '_Required']; ?> class="form-control"
                                                   placeholder="<?php echo JText::_('COM_VIRTUALDESK_USER_' .$valConfFieldNames. '_LABEL'); ?>"
                                                   name="<?php echo $keyConfFieldNames; ?>" id="<?php echo $keyConfFieldNames; ?>"
                                                   value="<?php echo htmlentities($this->data->{$keyConfFieldNames}, ENT_QUOTES, 'UTF-8'); ?>"/>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>

                        <?php endforeach; ?>
                    </div>
                </div>


                <h4 class="form-section"></h4>
                <div class="row">
                    <div class="col-md-12">
                        <?php if (!empty($ObjUserFields->arConfFieldNames ['veracidadedados'])) : ?>
                            <div class="form-group">
                                <div class="col-md-2 " style="text-align: right;padding: 0;">
                                    <input type="checkbox" name="veracidadedados" id="veracidadedados" class="make-switch form-control" value="1"
                                        <?php echo $arUserFieldsConfig['UserField_VERACIDADEDADOS_Required']; ?>
                                           data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                                           data-on-color="success" data-off-color="danger"
                                        <?php if((int)$this->data->veracidadedados>0)  echo 'checked="checked"' ?>
                                    />

                                </div>
                                <label class="col-md-10 " ><?php echo JText::_( 'COM_VIRTUALDESK_USER_VERACIDADEDADOS_LABEL' ); ?></label>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($ObjUserFields->arConfFieldNames ['politicaprivacidade'])) : ?>
                            <div class="form-group">
                                <div class="col-md-2 " style="text-align: right;padding: 0;">
                                    <input type="checkbox" name="politicaprivacidade" id="politicaprivacidade" class="make-switch form-control" value="1"
                                        <?php echo $arUserFieldsConfig['UserField_POLITICAPRIVACIDADE_Required']; ?>
                                           data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>"
                                           data-on-color="success" data-off-color="danger"
                                        <?php if((int)$this->data->politicaprivacidade>0)  echo 'checked="checked"' ?>
                                    />
                                </div>
                                <label class="col-md-10 "  ><?php  echo JText::sprintf( 'COM_VIRTUALDESK_USER_POLITICAPRIVACIDADE_LABEL', JUri::root() ); ?></label>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>


                <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SAVECHANGES'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userfullview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->id)); ?>"
                               title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('permadmin_user_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt('permadmin.userfullupdate',$setencrypt_forminputhidden); ?>"/>
                <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>


<?php echo $localScripts; ?>
<script>
    <?php
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/permadmin/tmpl/userfullview.js.php');
    ?>

    jQuery(document).ready(function() {

        jQuery('#vdChangeProfileLogin').click(function(){
            jQuery('input#login').removeAttr('disabled');
        });

        jQuery('#vdChangeProfilePassword').click(function(){
            jQuery('input#password1').removeAttr('disabled');
            jQuery('input#password2').removeAttr('disabled');
        });

        jQuery('#vdChangeProfileEmail').click(function(){
            jQuery('input#email1').removeAttr('disabled');
            jQuery('input#email2').removeAttr('disabled');
        });



        var uploadFile;
        // edit this line to the id of the file element
        uploadFile = 'useravatar';
        uploadFile = jQuery('#'+uploadFile);
        uploadFile.on('change', function() {
            var files, reader;
            files = !!this.files ? this.files : [];
            if ( !files.length || !window.FileReader ) {
                jQuery('img#loadedAvatarImg').show();
                return; // no file selected, or no FileReader support
            }

            var fsize = files[0].size;
            if( fsize><?php echo $useravatar_max_size; ?>) //do something if file size more than 1 mb (1048576)
            {
                swal("<?php echo JText::_('COM_VIRTUALDESK_USERAVATAR_FILE_TOO_LARGE') . $useravatar_max_size_Mb . "Mb"; ?>",'',"warning");
                uploadFile.val('');
                jQuery('img#loadedAvatarImg').show();
                return(false);
            }

            //get the file size and file type from file input field
            var fsize = files[0].size;
            var ftype = files[0].type;
            var fname = files[0].name;

            switch(ftype)
            {
                case 'image/png':
                case 'image/gif':
                case 'image/jpeg':
                    break;
                default:
                    swal("<?php echo JText::_('COM_VIRTUALDESK_USERAVATAR_ERROR_FILE_TYPE'); ?>",'',"warning");
                    uploadFile.val('');
                    jQuery('img#loadedAvatarImg').show();
                    return(false);
            }

            if ( /^image/.test(files[0].type) ) { // only image file
                // retira a imagem anterior
                jQuery('img#loadedAvatarImg').hide();
            }

        });

    });
</script>















