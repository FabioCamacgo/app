<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');


/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'listdefault');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');



//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');
$labelseparator = ' : ';

// Flush dos dados editados temporários na sessão
VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-xs-10 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: 50% !important;  -moz-border-radius: 50% !important;  border-radius: 50% !important;   width: 150px; height: auto;}

    .portfolio-content.portfolio-1 .cbp-caption-activeWrap {background-color: rgba(50, 197, 210, 0.5); }

</style>


    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-briefcase font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            </div>
            <div class="actions">
               <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
            </div>
        </div>

        <div class="portlet-body ">


                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tabModulos" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_MODULOS'); ?></a>
                    </li>
                    <li>
                        <a href="#tabGroups" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_GRUPOS'); ?></a>
                    </li>
                    <li>
                        <a href="#tabUsers" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_USERS'); ?></a>
                    </li>
                    <li>
                        <a href="#tabTipos" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_TIPOS'); ?></a>
                    </li>
                    <li>
                        <a href="#tabActions" data-toggle="tab"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_ACTIONS'); ?></a>
                    </li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tabModulos">

                        <div class="portlet light">

                            <div class="portlet-body ">
                                <div class="actions">
                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=moduloaddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
                                </div>
                                <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_perm_modulo">
                                    <thead>
                                    <tr>
                                        <th style="min-width: 150px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?> </th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG'); ?></th>
                                        <th ><?php echo JText::_('COM_VIRTUALDESK_LOGADMIN_CMP_ENABLED'); ?> </th>
                                        <th> </th>


                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>

                    </div>


                    <div class="tab-pane" id="tabGroups">
                        <div class="portlet light">
                            <div class="portlet-body ">
                                <div class="actions">
                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=grupoaddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
                                </div>
                                <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_perm_groups">
                                    <thead>
                                    <tr>
                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?> </th>
                                        <th> <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_EMAIL'); ?></th>
                                        <th style="width: 100px;"> <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ISMANAGER_SHORT'); ?></th>
                                        <th> </th>
                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>


                    <div class="tab-pane" id="tabUsers">
                        <div class="portlet light">
                            <div class="portlet-body ">

                                <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_perm_users">
                                    <thead>
                                    <tr>
                                        <th style="min-width: 60px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ID'); ?></th>
                                        <th style="min-width: 60px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_IDJOOMLA'); ?></th>
                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?></th>
                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_LOGIN'); ?> </th>
                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_EMAIL'); ?> </th>
                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_BLOCKED'); ?> </th>
                                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ACTIVATED'); ?> </th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_VDADMIN'); ?> </th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_VDMANAGER'); ?> </th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_VDUSER'); ?> </th>
                                        <th> </th>
                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>


                    <div class="tab-pane" id="tabTipos">
                        <div class="portlet light">
                            <div class="portlet-body ">
                                <div class="actions">
                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=tipoaddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
                                </div>
                                <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_perm_tipo">
                                    <thead>
                                    <tr>
                                        <th style="min-width: 150px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?> </th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG'); ?></th>
                                        <th> </th>
                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>


                    <div class="tab-pane" id="tabActions">
                        <div class="portlet light">
                            <div class="portlet-body ">
                                <div class="actions">
                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=actionaddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>

                                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=actionlistbymodule&vdcleanstate=1'); ?>" class="btn btn btn-circle btn-default">
                                        <i class="fa fa-list"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_LISTA_ACTIONBYMODULO' ); ?></a>

                                </div>
                                <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_perm_action">
                                    <thead>
                                    <tr>
                                        <th style="min-width: 150px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?> </th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TAG'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_DESC'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_MODULO'); ?></th>
                                        <th><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_TIPO'); ?></th>
                                        <th> </th>
                                    </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>



        </div>
    </div>



<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/permadmin/tmpl/default.js.php');
echo ('</script>');
?>