<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');



/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'userview'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$app    = JFactory::getApplication();
$jinput = $app->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$labelseparator = ' : ';

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ladda/spin.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/ladda/ladda.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/ladda/ladda-themeless.min.css');



//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');

$getInputPermUser_id = JFactory::getApplication()->input->getInt('permadmin_user_id');

$this->data = array();
$this->data = VirtualDeskSitePermAdminHelper::getPermUserDetail($getInputPermUser_id);
$this->dataUserActions = array();
$this->dataUserActions = VirtualDeskSitePermAdminHelper::getPermUserActionsDetail($getInputPermUser_id);

if(!empty($this->data)) {
    // se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
}

//$itemmenuid_lista = $params->get('permadmin_menuitemid_list');

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-xs-10 { background-color: #fff !important;}
    .static-info { margin-bottom: 15px;}
    .static-info .value { border: 1px solid #ddd; background-color: #f6f6f6;;  font-weight: normal;  padding: 5px 10px;  border-radius: 10px;}
    .static-info .name  { font-weight: 600;  padding: 5px 10px;}
    .portlet { margin-bottom: 0px;}
    .portlet > .portlet-title > .caption {font-size: 14px;}
    .text-grey {color: #b7b7b7}
    a.vd-button-blockuser > i.fa {font-size: 18px;}
    button.vd-button-blockuser2 {margin-left:15px;}
    button.vd-button-sendactivationuser {margin-left:15px;}
    .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled.collapsed { background-position: left 12px; margin-left:15px; padding-left: 25px;}
    .accordion .panel .panel-title .accordion-toggle.accordion-toggle-styled { background-position: left -19px; margin-left:15px; padding-left: 25px;}


</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-lock  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_TAB_USERS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <?php
        // Data not empty ?
        if(!empty($this->data)) :
            ?>
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabUsers'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userfullview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-table"></i>  <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_VER_USERFULL'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userfulledit&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_EDITAR_USERFULL'); ?> </a>
           <!-- <a href="<?php // echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=useraddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
                 -->

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        <?php else :
            ?>
        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabUsers'); ?>" class="btn btn-circle btn-default">
            <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
        <a href="<?php // echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=useraddnew&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
            <i class="fa fa-plus"></i>  <?php  echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
    </div>
        <?php endif;?>
        <!-- END TITLE ACTIONS -->
    </div>



    <div class="portlet-body ">

        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-blue  ">
                    <i class="fa fa-user font-blue"></i>
                    <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_USERINFO'); ?>
                </div>
            </div>

            <div class="portlet-body ">
                        <?php

                        // Data not empty ?
                        $iconCheckOk    = '<i class="fa fa-check font-green-jungle"></i>';
                        $iconCheckNotOk = '<i class="fa fa-close font-red"></i>';
                        $iconBlocked    = '<i class="fa fa-close font-red"></i>';
                        $iconNotBlocked = '<i class="fa fa-minus font-grey"></i>';
                        if(!empty($this->data)) :
                        ?>
                    <div class="well">
                        <div class="row static-info ">
                            <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_ID' ).$labelseparator; ?></div>
                            <div class="col-md-4 value"> <?php echo htmlentities( $this->data->id, ENT_QUOTES, 'UTF-8');?> </div>
                            <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_BLOCKED' ).$labelseparator; ?></div>
                            <div class="col-md-4">
                                <span class="vd_span_button_blockuser">
                                <?php if((string)$this->data->blocked=='1') { echo $iconBlocked; } else {  echo $iconNotBlocked; } ?>
                                </span>

                                <!--
                                <a href="javascript;"
                                   class="btn btn-circle btn-outline vd-button-blockuser"
                                   data-title="<?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CHANGEBLOCKED_CONFIRM');?>"
                                   data-type="warning"
                                   data-allow-outside-click="true"
                                   data-show-confirm-button="true"
                                   data-show-cancel-button="true"
                                   data-cancel-button-class="btn-danger"
                                   data-cancel-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CONFIRM_NO' ); ?> "
                                   data-confirm-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CONFIRM_YES' ); ?> "
                                   data-confirm-button-class="btn-info"
                                   data-placement="top"
                                   data-trigger="hover"
                                   data-vd-url-blockuser="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.setUserBlockByAjax&permadmin_user_id='. $this->data->permadmin_user_id); ?>"
                                   data-vd-user-id2block="<?php if((string)$this->data->blocked=='1') { echo '0'; } else {  echo '1'; } ?>"
                                  >
                                  <i class="fa fa-edit"></i> <?php //echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
-->

                                <button type="button" class="btn blue mt-ladda-btn ladda-button btn-outline vd-button-blockuser2" data-style="zoom-in" data-spinner-color="#333"
                                        data-title="<?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CHANGEBLOCKED_CONFIRM');?>"
                                        data-type="warning"
                                        data-allow-outside-click="true"
                                        data-show-confirm-button="true"
                                        data-show-cancel-button="true"
                                        data-cancel-button-class="btn-danger"
                                        data-cancel-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CONFIRM_NO' ); ?> "
                                        data-confirm-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CONFIRM_YES' ); ?> "
                                        data-confirm-button-class="btn-info"
                                        data-placement="top"
                                        data-trigger="hover"
                                        data-vd-url-blockuser="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.setUserBlockByAjax&permadmin_user_id='. $this->data->permadmin_user_id); ?>"
                                        data-vd-url-checkuserblocked="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.getUserVDBlockedValByAjax&permadmin_user_id='. $this->data->permadmin_user_id); ?>"
                                        >
                                    <span class="ladda-label">
                                      <i class="fa fa-pencil"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CHANGE' ); ?></span>
                                    <span class="ladda-spinner"></span>
                                </button>

                               <!-- <button type="button" class="btn red mt-ladda-btn ladda-button btn-outline vd-button-blockuser3" data-style="zoom-in" data-spinner-color="#333">
                                    <span class="ladda-label">
                                      <i class="fa fa-edit"></i> Slide Right 2</span>
                                    <span class="ladda-spinner"></span>
                                </button>
-->

                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_NOME' ).$labelseparator; ?></div>
                            <div class="col-md-4 value"> <?php echo htmlentities( $this->data->name, ENT_QUOTES, 'UTF-8');?> </div>
                            <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_ACTIVATED' ).$labelseparator; ?></div>
                            <div class="col-md-4 ">
                                <span class="vd_span_button_sendactivationuser">
                                    <?php if((string)$this->data->activated=='1') { echo $iconCheckOk; } else {  echo $iconCheckNotOk; } ?>
                                </span>
                                <button type="button" class="btn blue mt-ladda-btn ladda-button btn-outline vd-button-sendactivationuser" data-style="zoom-in" data-spinner-color="#333"
                                        data-title="<?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_SENDACTIVATION_CONFIRM');?>"
                                        data-type="warning"
                                        data-allow-outside-click="true"
                                        data-show-confirm-button="true"
                                        data-show-cancel-button="true"
                                        data-cancel-button-class="btn-danger"
                                        data-cancel-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CONFIRM_NO' ); ?> "
                                        data-confirm-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CONFIRM_YES' ); ?> "
                                        data-confirm-button-class="btn-info"
                                        data-placement="top"
                                        data-trigger="hover"
                                        data-vd-url-sendactivationuser="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.sendActivationForUserByAjax&permadmin_user_id='. $this->data->permadmin_user_id); ?>"
                                        data-vd-url-checkuseractivated="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.getUserVDActivatedValByAjax&permadmin_user_id='. $this->data->permadmin_user_id); ?>"
                                >
                                    <span class="ladda-label">

                                    <i class="fa fa-envelope"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_SENDACTIVATION' ); ?></span>
                                    <span class="ladda-spinner"></span>
                                </button>

                            </div>
                        </div>

                        <div class="row static-info">
                            <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_LOGIN' ).$labelseparator; ?></div>
                            <div class="col-md-4 value"> <?php echo htmlentities( $this->data->login, ENT_QUOTES, 'UTF-8');?> </div>

                        </div>

                        <div class="row static-info">
                            <div class="col-md-2 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_EMAIL' ).$labelseparator; ?></div>
                            <div class="col-md-4 value"> <?php echo htmlentities( $this->data->email, ENT_QUOTES, 'UTF-8');?> </div>
                        </div>
                    </div>
                <?php // Data empty
                        else :
                ?>
                <div class="form-actions right">
                    <div class="row">

                        <div  class="col-md-12 text-left">
                            <div class="alert alert-info fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                                <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_EMPTYLIST'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif;  ?>

            </div>
        </div>
    </div>




<div class="portlet">
    <div class="portlet-body ">

        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tabPermissions" data-toggle="tab"> <i class="fa fa-lock font-blue"></i> <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_USERASSOCPERMISSIONS'); ?></a>
            </li>
            <li>
                <a href="#tabGrupos" data-toggle="tab"> <i class="fa fa-user font-blue"></i>  <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_GROUPSOFUSER'); ?></a>
            </li>

            <li>
                <a href="#tabLoadedPerm" data-toggle="tab"> <i class="fa fa-user font-blue"></i>  <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_TAB_LOADEDERMISSIONS'); ?></a>
            </li>
        </ul>


        <div class="tab-content">

            <div class="tab-pane active" id="tabPermissions">

                <?php
                require_once (JPATH_SITE . '/components/com_virtualdesk/views/permadmin/tmpl/templates/userview_listpermissions.php');
                ?>

            </div>

            <div class="tab-pane " id="tabGrupos">

                <div class="form-actions center">
                    <div class="row">
                        <div  class="col-md-12 text-center">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=usergruposedit&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id)); ?>" class="btn btn-circle btn-outline green">
                        <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_EDITARUSERGROUPS'); ?> </a>
                        </div>
                    </div>
                </div>

                <table class="table table-striped table-bordered table-hover order-column" style="width:100%" id="tabela_lista_perm_grupos">
                    <thead>
                    <tr>
                        <th style="min-width: 60px;"><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_ID'); ?></th>
                        <th ><?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_NOME'); ?></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>

            </div>

            <div class="tab-pane " id="tabLoadedPerm">

                <?php

                $objCheckPerm = new VirtualDeskSitePermissionsHelper();

                $getInputPermUserJoomla =  VirtualDeskUserHelper::getUserJoomlaByVDUserId($getInputPermUser_id);

                $objCheckPerm->loadPermission(true, $getInputPermUserJoomla->id);
                $userPermArray = $objCheckPerm->getPermArray();

                require_once (JPATH_SITE . '/components/com_virtualdesk/views/permadmin/tmpl/templates/userview_loadedpermissions.php');
                ?>

            </div>

        </div>






    </div>
</div>










<!-- Export Action URls to be encrypted...-->
<div style="display:none;">
    <a id="ExportActions_Print" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint' ); ?>"></a>
    <a id="ExportActions_PrintLandscape" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint&tmplorientation=landscape' ); ?>"></a>
    <a id="ExportActions_PrintPDF" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint&tmplformat=pdf' ); ?>"></a>
    <a id="ExportActions_PrintPDFLandscape" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint&tmplformat=pdf&tmplorientation=landscape' ); ?>"></a>
</div>

<?php echo $localScripts; ?>

<script>
<?php
  require_once (JPATH_SITE . '/components/com_virtualdesk/views/permadmin/tmpl/userview.js.php');
?>
jQuery(document).ready(function() {
    // handle action tools
    jQuery('#default_permadmin_tools li > a.tool-action').on('click', function() {

        var action = jQuery(this).attr('data-action');
        var actionURLPrint = '';

        if(action=='0'){
            actionURLPrint = jQuery('#ExportActions_Print').attr('href');
            if( jQuery("#DocumentsBlockByGrid").length > 0 )
            {
                if(jQuery("#DocumentsBlockByGrid").find(".cbp-item").length > 0) actionURLPrint = jQuery('#ExportActions_PrintLandscape').attr('href');
            }
            window.open(actionURLPrint)
        }

        if(action=='2'){
            //Verifica se tem ficheiros, nesse caso gera em landscape
           actionURLPrint = jQuery('#ExportActions_PrintPDF').attr('href');
           if( jQuery("#DocumentsBlockByGrid").length > 0 )
           {
               if(jQuery("#DocumentsBlockByGrid").find(".cbp-item").length > 0) actionURLPrint = jQuery('#ExportActions_PrintPDFLandscape').attr('href');
           }
           window.open(actionURLPrint)
        }


    });
});


</script>