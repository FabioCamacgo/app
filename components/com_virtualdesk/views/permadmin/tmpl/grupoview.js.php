<?php
defined('_JEXEC') or die;

?>
var TableDatatablesManaged = function () {

    var initTableUsers = function () {

        var table = jQuery('#tabela_lista_perm_users');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"frtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 20,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },

                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[10] + '">' + data + '</a>');
                    }
                },

                // Blocked
                {
                    "targets": 5,
                    "data": 5,
                    "render": function ( data, type, row, meta) {
                        var ret = '<i class="fa fa-minus font-grey"></i>';
                        if(data=='1') {
                            ret = '<i class="fa fa-close font-red"></i>';
                        }
                        return (ret);
                    }
                },

                // Activated
                {
                    "targets": 6,
                    "data": 6,
                    "render": function ( data, type, row, meta) {
                        var ret = '<i class="fa fa-close font-red"></i>';
                        if(data=='1') {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                // VDADMIN
                {
                    "targets": 7,
                    "data": 7,
                    "render": function ( data, type, row, meta) {
                        var ret ='';
                        if(parseInt(data)>0) {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                // VDUSER
                {
                    "targets": 8,
                    "data": 8,
                    "render": function ( data, type, row, meta) {
                        var ret ='';
                        if(parseInt(data)>0) {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                // VDMANAGER
                {
                    "targets": 9,
                    "data": 9,
                    "render": function ( data, type, row, meta) {
                        var ret ='';
                        if(parseInt(data)>0) {
                            ret = '<i class="fa fa-check font-green-jungle"></i>';
                        }
                        return (ret);
                    }
                },

                {
                    "targets": 10,
                    "data": 10,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + data + returnLinkf);
                    }
                }

            ],
            "order": [
                [0, "asc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=permadmin.getDataGroupUsersListByAjax&permadmin_grupo_id=<?php echo($getInputPermGrupo_id); ?>",
            "drawCallback": function( settings ) {
                console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {

            }


        });

    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableUsers();
        }

    };

}();


jQuery(document).ready(function() {
        TableDatatablesManaged.init();
});