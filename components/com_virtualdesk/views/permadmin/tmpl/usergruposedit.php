<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');


/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$vbInAdminGroup = $objCheckPerm->checkSessionUserInVDAdmin();
if($vbInAdminGroup===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWEDVDADMIN'), 'error' );
    return false;
}
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('permissionsadmin');                  // verifica permissão de update
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'usergruposedit'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$app    = JFactory::getApplication();
$jinput = $app->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$labelseparator = ' : ';

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/permadmin/tmpl/usergruposedit.js'  . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/jquery-multi-select/css/multi-select.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');



//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');

$getInputPermUser_id = JFactory::getApplication()->input->getInt('permadmin_user_id');

$this->data = array();
$this->data = VirtualDeskSitePermAdminHelper::getPermUserDetail($getInputPermUser_id);

$dataUserGrupos  = array();
$dataUserGrupos  = VirtualDeskSitePermAdminHelper::getUserGroupsOnlyIDList (-1,$getInputPermUser_id);

$dataAllGroupsList = array();
$dataAllGroupsList = VirtualDeskSitePermAdminHelper::getPermGroupList(false, -1);


// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSitePermAdminHelper::cleanAllTmpUserStare();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.usergruposedit.permadmin.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}


// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
   .form-group { margin-bottom: 0;}
   .mt-checkbox.mt-checkbox-outline.vdcheckbox {margin-bottom: 0;}
   .form-horizontal .form-group .mt-checkbox-inline {padding-top: 0;}
   .mt-checkbox-inline {padding: 0;}
   #portletUser.portlet, #portletListPerm.portlet {padding-bottom: 0px;}
   .mt-checkbox > span::after {border-color: #26C281;}
   .portlet > .portlet-title > .caption {font-size: 14px;}
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-lock font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_TAB_USERS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_VER_DETALHE' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_EDITARUSERGROUPS' ); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id)); ?>#tabGrupos" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>

    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <?php
        // Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
        ?>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>



    <div class="portlet-body ">
        <div class="portlet light " id="portletUser">
            <div class="portlet-title">
                <div class="caption font-blue  ">
                    <i class="fa fa-user font-blue"></i>
                    <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_USERINFO'); ?>
                </div>
            </div>
            <div class="portlet-body ">
                    <div class="row static-info ">
                        <div class="col-md-1 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_ID' ).$labelseparator; ?></div>
                        <div class="col-md-1 value"> <?php echo htmlentities( $this->data->id, ENT_QUOTES, 'UTF-8');?> </div>
                        <div class="col-md-1 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_CMP_NOME' ).$labelseparator; ?></div>
                        <div class="col-md-2 value"> <?php echo htmlentities( $this->data->name, ENT_QUOTES, 'UTF-8');?> </div>
                        
                    </div>
            </div>
        </div>
    </div>



    <div class="portlet-body ">

        <div class="portlet light " id="portletListPerm">
            <div class="portlet-title">
                <div class="caption font-blue ">
                    <i class="fa fa-lock font-blue"></i>
                    <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_TAB_GROUPSOFUSER'); ?>
                </div>
            </div
        </div>
    </div>



    <div class="portlet-body form">

        <form id="form-permadminuser"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=permadmin.usergruposupdate'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="form-body">

                <div class="form-group">
                    <label class="control-label col-md-3">Default</label>
                    <div class="col-md-9">
                        <select multiple="multiple" class="mt-multiselect" id="ugrpsid" name="ugrpsid[]">
                            <?php foreach($dataAllGroupsList as $rowGrupo) :
                               $GrupoId     = $rowGrupo->id;
                               $GrupoNome   = $rowGrupo->nome;
                               $vsSelected = '';
                               if(in_array($GrupoId, $dataUserGrupos) ) $vsSelected = 'selected'
                            ?>
                            <option value="<?php echo $GrupoId; ?>"  <?php echo $vsSelected; ?>><?php echo $GrupoNome; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>


            </div>


            <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=userview&vdcleanstate=1&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id)); ?>#tabGrupos"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
            </div>


            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('permadmin_user_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->permadmin_user_id) ,$setencrypt_forminputhidden); ?>"/>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"            value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"              value="<?php echo $obVDCrypt->formInputValueEncrypt('permadmin.usergruposupdate',$setencrypt_forminputhidden); ?>"/>

            <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>

</div>

<?php echo $localScripts; ?>
