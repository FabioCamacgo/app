/*
* Define estrutura da validação.
* A validação é iniciada depois no final deste ficheiro.
* Foi acrescentado antes da inicialização um método ( .addMethod("vdpasscheck"... ) que permite fazer a validação específica da password.
*/
var PermAdminEdit = function() {

    var handlePermAdminEdit = function() {

        jQuery('#form-permadmingrupo').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   

                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = MessageAlert.getRequiredMissed(errors);
                        var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                        MainMessageBlock.find('span').html(message);
                        MainMessageBlock.show();
                        App.scrollTo(MainMessageBlock, -200);
                    }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

              if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                 form.submit();
            }
        });

        jQuery('#form-permadmingrupo').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#form-permadmingrupo').validate().form()) {
                    jQuery('#form-permadmingrupo').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handlePermAdminEdit();
        }
    };

}();



var ComponentsBootstrapMultiselect = function () {

    return {
        //main function to initiate the module
        init: function () {

            //console.log('multi...2');

            jQuery('.mt-multiselect').each(function(){

                console.log('mt-multiselect > each > ');

                var btn_class = jQuery(this).attr('class');
                var clickable_groups = (jQuery(this).data('clickable-groups')) ? jQuery(this).data('clickable-groups') : false ;
                var collapse_groups = (jQuery(this).data('collapse-groups')) ? jQuery(this).data('collapse-groups') : false ;
                var drop_right = (jQuery(this).data('drop-right')) ? jQuery(this).data('drop-right') : false ;
                var drop_up = (jQuery(this).data('drop-up')) ? jQuery(this).data('drop-up') : false ;
                var select_all = (jQuery(this).data('select-all')) ? jQuery(this).data('select-all') : false ;
                var width = (jQuery(this).data('width')) ? jQuery(this).data('width') : '' ;
                var height = (jQuery(this).data('height')) ? jQuery(this).data('height') : '' ;
                var filter = (jQuery(this).data('filter')) ? jQuery(this).data('filter') : false ;

                // advanced functions
                var onchange_function = function(option, checked, select) {
                    alert('Changed option ' + jQuery(option).val() + '.');
                }
                var dropdownshow_function = function(event) {
                    alert('Dropdown shown.');
                }
                var dropdownhide_function = function(event) {
                    alert('Dropdown Hidden.');
                }

                // init advanced functions
                //var onchange = (jQuery(this).data('action-onchange') == true) ? onchange_function : '';
                var dropdownshow = (jQuery(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
                var dropdownhide = (jQuery(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

                // template functions
                // init variables
                var li_template;
                if (jQuery(this).attr('multiple')){
                    li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
                } else {
                    li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
                }

                // init multiselect
                $(this).multiSelect({
                    enableClickableOptGroups: clickable_groups,
                    enableCollapsibleOptGroups: collapse_groups,
                    disableIfEmpty: true,
                    enableFiltering: filter,
                    includeSelectAllOption: select_all,
                    dropRight: drop_right,
                    buttonWidth: width,
                    maxHeight: height,
                    //onChange: onchange,
                    onDropdownShow: dropdownshow,
                    onDropdownHide: dropdownhide,
                    buttonClass: btn_class,
                    //optionClass: function(element) { return "mt-checkbox"; },
                    //optionLabel: function(element) { console.log(element); return jQuery(element).html() + '<span></span>'; },
                    /*templates: {
                        li: li_template,
                    }*/
                });
            });

        }
    };

}();



/*
* Inicialização da validação
*/

jQuery(document).ready(function() {

   // Validações
    PermAdminEdit.init();

    console.log('multi...');

    ComponentsBootstrapMultiselect.init();

});