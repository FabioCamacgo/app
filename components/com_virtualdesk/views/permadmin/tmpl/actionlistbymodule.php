<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSitePermAdminHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permadmin.php');


/*
* Check Permissões - no caso do PermAdmin,para ecrão e ações de criaçã ou alteração de dados, existe uma verificação extra do VD Admin
*/
$objCheckPerm   = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailReadAccess('permissionsadmin');                  // verifica permissão de read
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('permissionsadmin', 'actionview'); // verifica permissão acesso ao layout para editar
if($vbHasAccess===false || $vbHasAccess2===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$app    = JFactory::getApplication();
$jinput = $app->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');
$labelseparator = '  ';

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');



//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');

$getInputPermUser_id = JFactory::getApplication()->input->getInt('permadmin_user_id');


$this->dataModuleActions = array();
$this->dataModuleActions = VirtualDeskSitePermAdminHelper::getPermModuleActionsFullList();


//$itemmenuid_lista = $params->get('permadmin_menuitemid_list');

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-xs-10 { background-color: #fff !important;}
</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-lock  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_TAB_ACTIONS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PERMADMIN_LISTA_ACTIONBYMODULO' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <?php
        // Data not empty ?
        if(!empty($this->data)) :
            ?>
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabActions'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        <?php else :
            ?>
        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin#tabActions'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
        <?php endif;?>
        <!-- END TITLE ACTIONS -->
    </div>



    <div class="portlet-body form">
<!--
        <div class="row" id="t1">
            <div class="col-xs-12">
                <div class="table-scrollable">

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th> Modulo </th>
                            <th> Tipo </th>
                            <th> Action </th>
                            <th> </th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach($this->dataModuleActions as $rowActions) : ?>

                            <tr>
                                <td> <?php echo htmlentities( $rowActions->modulo_nome, ENT_QUOTES, 'UTF-8');?> </td>
                                <td> <?php echo htmlentities( $rowActions->tipo_nome, ENT_QUOTES, 'UTF-8');?> </td>
                                <td> <?php echo htmlentities( $rowActions->action_nome, ENT_QUOTES, 'UTF-8');?> </td>
                                <td> <?php if( empty($rowActions->action_id)) : ?>
                                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=actionaddnew&vdcleanstate=1'); ?>" class="btn btn-circle btn-outline green">
                                        <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_ADDNEW'); ?> </a>
                                     <?php endif; ?>
                                  </td>
                            </tr>

                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div>
        </div>

        </div>
        -->

        <div class="row" id="t2">
            <div class="col-xs-12">
                <div class="panel-group accordion" id="accordion1">
                    <?php
                    $currModulo = '';
                    $prevModulo = '';
                    foreach($this->dataModuleActions as $rowActions) :
                    $currModulo = $rowActions->modulo_tagchave;
                    //inicia um novo acordion
                    if($prevModulo!=$currModulo) :  ?>
                    <?php if( ($prevModulo!=$currModulo) && ($prevModulo!='')) :  ?>
                    </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
<?php endif;?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#<?php echo htmlentities( $rowActions->modulo_tagchave, ENT_QUOTES, 'UTF-8');?>" >
                <i class="icon-plug font-dark"></i>
                <?php echo JText::_('COM_VIRTUALDESK_PERMADMIN_CMP_MODULO'); ?> : <span class="font-dark sbold uppercase"><?php echo htmlentities( $rowActions->modulo_nome, ENT_QUOTES, 'UTF-8');?></span>
            </a>
        </h4>
    </div>
    <div id="<?php echo htmlentities( $rowActions->modulo_tagchave, ENT_QUOTES, 'UTF-8');?>" class="panel-collapse in"  style="">
        <div class="panel-body">

            <div class="">
                <table class="table table-hover">
                    <tr>
                        <th> Modulo </th>
                        <th> Tipo </th>
                        <th> Action </th>
                        <th> </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php endif;?>

                    <tr>
                        <td> <?php echo htmlentities( $rowActions->modulo_nome, ENT_QUOTES, 'UTF-8');?> </td>
                        <td> <?php echo htmlentities( $rowActions->tipo_nome, ENT_QUOTES, 'UTF-8');?> </td>
                        <td> <?php echo htmlentities( $rowActions->action_nome, ENT_QUOTES, 'UTF-8');?> </td>
                        <td> <?php if( empty($rowActions->action_id)) : ?>
                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&layout=actionaddnew&vdcleanstate=1'); ?>" class="btn btn-circle btn-outline green">
                                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_ADDNEW'); ?> </a>
                            <?php endif; ?>
                        </td>
                    </tr>

                    <?php $prevModulo = $rowActions->modulo_tagchave; ?>
                    <?php endforeach; ?>
                    <?php if ($prevModulo!='') :  ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<?php endif;?>

</div>
</div>
</div>



</div>


<!-- Export Action URls to be encrypted...-->
<div style="display:none;">
    <a id="ExportActions_Print" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint' ); ?>"></a>
    <a id="ExportActions_PrintLandscape" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint&tmplorientation=landscape' ); ?>"></a>
    <a id="ExportActions_PrintPDF" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint&tmplformat=pdf' ); ?>"></a>
    <a id="ExportActions_PrintPDFLandscape" class="btn green" target="_blank" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=permadmin&permadmin_user_id=' . $this->escape($this->data->permadmin_user_id).'&tmpl=tplprint&tmplformat=pdf&tmplorientation=landscape' ); ?>"></a>
</div>

<?php echo $localScripts; ?>


<script>
jQuery(document).ready(function() {
    // handle action tools
    jQuery('#default_permadmin_tools li > a.tool-action').on('click', function() {

        var action = jQuery(this).attr('data-action');
        var actionURLPrint = '';
        //console.log('action= '+action);

        if(action=='0'){
            actionURLPrint = jQuery('#ExportActions_Print').attr('href');
            if( jQuery("#DocumentsBlockByGrid").length > 0 )
            {
                if(jQuery("#DocumentsBlockByGrid").find(".cbp-item").length > 0) actionURLPrint = jQuery('#ExportActions_PrintLandscape').attr('href');
            }
            //console.log('actionURLPrint= '+actionURLPrint);
            window.open(actionURLPrint)
        }

        if(action=='2'){
            //Verifica se tem ficheiros, nesse caso gera em landscape
           actionURLPrint = jQuery('#ExportActions_PrintPDF').attr('href');
           if( jQuery("#DocumentsBlockByGrid").length > 0 )
           {
               if(jQuery("#DocumentsBlockByGrid").find(".cbp-item").length > 0) actionURLPrint = jQuery('#ExportActions_PrintPDFLandscape').attr('href');
           }
           //console.log('actionURLPrint= '+actionURLPrint);
           window.open(actionURLPrint)
        }


    });
});


</script>