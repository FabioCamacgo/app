/*
* Define estrutura da validação.
* A validação é iniciada depois no final deste ficheiro.
* Foi acrescentado antes da inicialização um método ( .addMethod("vdpasscheck"... ) que permite fazer a validação específica da password.
*/
var PermAdminEdit = function() {

    var handlePermAdminEdit = function() {

        jQuery('#form-permadminmodulo').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   

                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = MessageAlert.getRequiredMissed(errors);
                        var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                        MainMessageBlock.find('span').html(message);
                        MainMessageBlock.show();
                        App.scrollTo(MainMessageBlock, -200);
                    }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

              if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                 form.submit();
            }
        });

        jQuery('#form-permadminmodulo').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#form-permadminmodulo').validate().form()) {
                    jQuery('#form-permadminmodulo').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handlePermAdminEdit();
        }
    };

}();




/*
* Inicialização da validação
*/

jQuery(document).ready(function() {


   // Validações
    PermAdminEdit.init();


    jQuery('.vd-button-delete').click(function(evt) {
        evt.preventDefault();
        var sa_title = jQuery(this).data('title');
        var sa_message = jQuery(this).data('message');
        var sa_type = jQuery(this).data('type');
        var sa_allowOutsideClick = jQuery(this).data('allow-outside-click');
        var sa_showConfirmButton = jQuery(this).data('show-confirm-button');
        var sa_showCancelButton = jQuery(this).data('show-cancel-button');
        var sa_closeOnConfirm = jQuery(this).data('close-on-confirm');
        var sa_closeOnCancel = jQuery(this).data('close-on-cancel');
        var sa_confirmButtonText = jQuery(this).data('confirm-button-text');
        var sa_cancelButtonText = jQuery(this).data('cancel-button-text');
        var sa_confirmButtonClass = jQuery(this).data('confirm-button-class');
        var sa_cancelButtonClass = jQuery(this).data('cancel-button-class');

        var vd_url_delete = jQuery(this).data('vd-url-delete');

        swal({  title: sa_title,
                text: sa_message,
                type: sa_type,
                allowOutsideClick: sa_allowOutsideClick,
                showConfirmButton: sa_showConfirmButton,
                showCancelButton: sa_showCancelButton,
                confirmButtonClass: sa_confirmButtonClass,
                cancelButtonClass: sa_cancelButtonClass,
                closeOnConfirm: sa_closeOnConfirm,
                closeOnCancel: sa_closeOnCancel,
                confirmButtonText: sa_confirmButtonText,
                cancelButtonText: sa_cancelButtonText
            },
            function(isConfirm){
                if (isConfirm){
                    window.location.href = vd_url_delete;
                }
            });
    });







});