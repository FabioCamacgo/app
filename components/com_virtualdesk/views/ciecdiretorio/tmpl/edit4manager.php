<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteciecdiretorioHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio.php');
JLoader::register('VirtualDeskSiteciecdiretorioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio_files.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailEditAccess('ciecdiretorio');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'edit4managers'); // verifica permissão acesso ao layout para editar
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-gallery.css');

//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/ciecdiretorio/tmpl/edit4manager.css');

//Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');
$getInputciecdiretorio_Id = JFactory::getApplication()->input->getInt('ciecdiretorio_id');

// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSiteciecdiretorioHelper::getciecdiretorioView4ManagerDetail($getInputciecdiretorio_Id);
if( empty($this->data) ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
    return false;
}

// Carrega de ficheiros associados
if(!empty($this->data->referencia)) {
    $objAlertFiles = new VirtualDeskSiteciecdiretorioFilesHelper();
    $ListFilesAlert = $objAlertFiles->getFileGuestLinkByRefId ($this->data->referencia);
}


// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteciecdiretorioHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit4manager.ciecdiretorio.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


$dataAllUsersList = array();
$dataAllUsersList = VirtualDeskSiteciecdiretorioHelper::getUsersListAtiveNotBlocked();

$dataAllTipologia = VirtualDeskSiteciecdiretorioHelper::getTipologiaListAll();


$obParam      = new VirtualDeskSiteParamsHelper();

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
    <style>
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
        .fileinput-preview img {display:block; float: none; margin: 0 auto;}
        .select2-bootstrap-prepend .input-group-btn {vertical-align: bottom};
    </style>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LISTA' ); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_EDIT' ); ?></span>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            <?php
            // Objecto inicializado com as mensagens de ciecdiretorio já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
            ?>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-ciecdiretorio"
                  action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=ciecdiretorio.update4manager'); ?>" method="post"
                  class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                  role="form">

                <div class="form-body">

                    <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_EDIT'); ?></h3></legend>

                    <!--   Nome do projeto  -->
                    <div class="col-md-12">
                        <div class="form-group static-info" id="projName">
                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_NOMEPROJETO'); ?></div>
                            <div class="value">
                                <input type="text" class="form-control" autocomplete="off" placeholder="" name="nomeProjeto" id="nomeProjeto" maxlength="500" value="<?php echo $this->data->projeto; ?>"/>
                            </div>
                        </div>
                    </div>


                    <!--   DESCRIÇAO   -->
                    <div class="col-md-12">
                        <div class="form-group static-info" id="desc">
                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DESIGNACAO_LABEL'); ?></div>
                            <div class="value">
                                <textarea  class="form-control wysihtml5" rows="12" name="descricao" id="descricao" maxlength="4000">
                                                <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->designacao); ?></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <!--   Programa  -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="prog">
                                <div class="name" for="field_programa"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PROGRAMA') ?></div>
                                <?php $Programa = VirtualDeskSiteFormularioHelper::getPrograma()?>
                                <div class="value">
                                    <select name="programa" value="<?php echo $this->data->idPrograma; ?>" id="programa" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($this->data->idPrograma)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                            <?php foreach($Programa as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['programa']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $this->data->idPrograma; ?>"><?php echo VirtualDeskSiteFormularioHelper::getProgSelect($this->data->idPrograma) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludePrograma = VirtualDeskSiteFormularioHelper::excludePrograma($this->data->idPrograma)?>
                                            <?php foreach($ExcludePrograma as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['programa']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!--   Linhas Financiamento   -->
                        <div class="col-md-6">
                            <?php
                            // Carrega menusec se o id de menumain estiver definido.
                            $ListaFinanciamento = array();
                            if(!empty($this->data->idPrograma)) {
                                if( (int) $this->data->idPrograma > 0) $ListaFinanciamento = VirtualDeskSiteFormularioHelper::getLinhasFinanciamento($this->data->idPrograma);
                            }

                            ?>
                            <div id="blocoLinhasFinanciamento" class="form-group static-info">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO'); ?><span class="required">*</span></div>
                                <div class="value">
                                    <select name="linhaFinanciamento" id="linhaFinanciamento" value="<?php echo $this->data->idLinha;?>"
                                        <?php
                                        if(empty($this->data->idPrograma)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($this->data->idLinha)){
                                            ?>
                                            <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EUROPA_OPCAO' ); ?></option>
                                            <?php foreach($ListaFinanciamento as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                    <?php
                                                    if(!empty($this->data->linhaFinanciamento)) {
                                                        if($this->data->linhaFinanciamento == $rowMM['id']) echo 'selected';
                                                    }
                                                    ?>
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach; ?>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?php echo $this->data->idLinha; ?>"><?php echo VirtualDeskSiteFormularioHelper::getLinhasFinanciamentoName($this->data->idLinha) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeLinhasFinanciamento = VirtualDeskSiteFormularioHelper::excludeLinhasFinanciamento($this->data->idPrograma, $this->data->idLinha)?>
                                            <?php foreach($ExcludeLinhasFinanciamento as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="blocoEscala">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group static-info" id="blocoEscalasCooperacao">
                                    <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ESCALA') ?></div>
                                    <?php $ListaEscala = VirtualDeskSiteFormularioHelper::getListaEscala()?>
                                    <div class="value">
                                        <select name="escala" value="<?php echo $this->data->escala; ?>" id="escala" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                            <?php
                                            if(empty($this->data->id_escala)){
                                                ?>
                                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                <?php foreach($ListaEscala as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id']; ?>"
                                                    ><?php echo $rowWSL['nome']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $this->data->id_escala; ?>"><?php echo VirtualDeskSiteFormularioHelper::getEscalaName($this->data->id_escala) ?></option>
                                                <option value=""><?php echo '-'; ?></option>
                                                <?php $ExcludeEscala = VirtualDeskSiteFormularioHelper::excludeEscala($this->data->id_escala)?>
                                                <?php foreach($ExcludeEscala as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id']; ?>"
                                                    ><?php echo $rowWSL['nome']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">

                        <!--   Nome da Entidade  -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="entName">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ENTIDADE'); ?></div>
                                <div class="value">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="entidade" id="entidade"
                                           maxlength="500" value="<?php echo $this->data->entidade; ?>"/>
                                </div>
                            </div>
                        </div>

                        <!--   Ano atribuição -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="year">
                                <div class="name" for="field_ano"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ANO') ?></div>
                                <?php $Ano = VirtualDeskSiteFormularioHelper::getAno()?>
                                <div class="value">
                                    <select name="ano" value="<?php echo $this->data->idAnoApoio; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($this->data->idAnoApoio)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                            <?php foreach($Ano as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['ano']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $this->data->idAnoApoio; ?>"><?php echo VirtualDeskSiteFormularioHelper::getAnoSelect($this->data->idAnoApoio) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeAno = VirtualDeskSiteFormularioHelper::excludeAno($this->data->idAnoApoio)?>
                                            <?php foreach($ExcludeAno as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['ano']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="col-md-12">

                        <!--   URL DO PROJETO  -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="linkProj">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LINK'); ?></div>
                                <div class="value">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="link" id="link"
                                           maxlength="1000" value="<?php echo $this->data->link; ?>"/>
                                </div>
                            </div>
                        </div>


                        <!--   CALL DO PROJETO  -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="callProj">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_CALL'); ?></div>
                                <div class="value">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="call" id="call"
                                           maxlength="100" value="<?php echo $this->data->call; ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">

                        <?php
                        if($this->data->data_Inicio == 'Em curso' || $this->data->data_Inicio == '--'){
                            $dataInicio = '';
                        } else {
                            $pieces = explode("-", $this->data->data_Inicio);

                            $dataInicio = $pieces[2] . '-' . $pieces[1] . '-' . $pieces[0];
                        }
                        ?>

                        <!--   Data de inicio  -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="projStart">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAINICIO'); ?><span class="required">*</span></div>
                                <div class="value">
                                    <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                        <input type="text" placeholder="dd-mm-aaaa" name="dataInicio" id="dataInicio"
                                               value="<?php echo $dataInicio; ?>">
                                        <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php
                        if($this->data->data_Fim == 'Em curso' || $this->data->data_Fim == '--'){
                            $dataFim = '';
                        } else {
                            $pieces2 = explode("-", $this->data->data_Fim);

                            $dataFim = $pieces2[2] . '-' . $pieces2[1] . '-' . $pieces2[0];
                        }
                        ?>
                        <!--   Data de fim  -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="projEnd">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAFIM'); ?><span class="required">*</span></div>
                                <div class="value">
                                    <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                        <input type="text" placeholder="dd-mm-aaaa" name="dataFim" id="dataFim"
                                               value="<?php echo $dataFim; ?>">
                                        <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">

                        <!--   VALOR FINANCIAMENTO  -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="valorProj">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_VALOR'); ?></div>
                                <div class="value">
                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="valor"
                                           id="valor" maxlength="30" value="<?php echo $this->data->valorFinanciamento; ?>"/>
                                    <span>€</span>
                                </div>
                            </div>
                        </div>


                        <!--   Layout Detalhe -->
                        <div class="col-md-6">
                            <div class="form-group static-info" id="layoutDetalhe">
                                <div class="name" for="field_ano"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LAYOUT') ?><span class="required">*</span></div>
                                <?php $Layout = VirtualDeskSiteFormularioHelper::getLayout()?>
                                <div class="value">
                                    <select name="layoutDetalhe" value="<?php echo $this->data->idLayout; ?>" id="layout" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($this->data->idLayout)){
                                            ?>
                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                            <?php foreach($Layout as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['layout']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $this->data->idLayout; ?>"><?php echo VirtualDeskSiteFormularioHelper::getLayoutSelect($this->data->idLayout) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeLayout = VirtualDeskSiteFormularioHelper::excludeLayout($this->data->idLayout)?>
                                            <?php foreach($ExcludeLayout as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['layout']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="blocoFormCultura">

                        <div class="col-md-12">
                            <!--   Líder  -->
                            <div class="col-md-6">
                                <div class="form-group static-info" id="liderName">
                                    <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER'); ?></div>
                                    <div class="value">
                                        <input type="text" class="form-control" autocomplete="off" placeholder="" name="lider" id="lider"
                                               maxlength="500" value="<?php echo $this->data->lider; ?>"/>
                                    </div>
                                </div>
                            </div>

                            <!--   País Lider  -->
                            <div class="col-md-6">
                                <div class="form-group static-info" id="paisLider">
                                    <div class="name" for="field_pais"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PAIS') ?></div>
                                    <?php $Pais = VirtualDeskSiteFormularioHelper::getPais()?>
                                    <div class="value">
                                        <select name="pais" value="<?php echo $this->data->idPaisLider; ?>" id="pais" class="form-control select2 select2-search" >
                                            <?php
                                            if(empty($this->data->idPaisLider)){
                                                ?>
                                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                <?php foreach($Pais as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id']; ?>"
                                                    ><?php echo $rowWSL['pais']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $this->data->idPaisLider; ?>"><?php echo VirtualDeskSiteFormularioHelper::getPaisSelect($this->data->idPaisLider) ?></option>
                                                <option value=""><?php echo '-'; ?></option>
                                                <?php $ExcludePais = VirtualDeskSiteFormularioHelper::excludePais($this->data->idPaisLider)?>
                                                <?php foreach($ExcludePais as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id']; ?>"
                                                    ><?php echo $rowWSL['pais']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">

                            <div class="form-group static-info">
                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_TIPOLOGIA'); ?></div>
                                <div class="value">
                                    <div class="input-group select2-bootstrap-prepend">
                                                <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-select2-open="tipologia">
                                                            <span class="glyphicon glyphicon-search"></span>
                                                        </button>
                                                </span>
                                        <div>
                                            <select name="tipologia[]"  id="tipologia" class="form-control input-lg select2-multiple select2-hidden-accessible" multiple tabindex="-1" aria-hidden="true">
                                                <option></option>
                                                <?php foreach($dataAllTipologia as $rowTipologia) : ?>

                                                    <option value="<?php echo $rowTipologia['id']; ?>"
                                                        <?php if(is_array($this->data->tipologia)) {
                                                            if( in_array($rowTipologia['id'],$this->data->tipologia) )
                                                            {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?>
                                                    ><?php echo $rowTipologia['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="col-md-12 docs">

                        <!--   Upload Imagens  -->
                        <div class="form-group static-info" id="uploadField">
                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_UPLOAD'); ?></div>
                            <div class="value">

                                <div class="file-loading">
                                    <input type="file" name="fileupload[]" id="fileupload" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>
                            </div>
                            <input type="hidden" name="vdFileUpChanged" value="0">


                        </div>

                    </div>

                </div>

                <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-6">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio'); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('ciecdiretorio_id',$setencrypt_forminputhidden); ?>"        value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->ciecdiretorio_id) ,$setencrypt_forminputhidden); ?>"/>


                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('ciecdiretorio.update4manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                <?php echo JHtml::_('form.token'); ?>

            </form>

        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/ciecdiretorio/tmpl/edit4manager.js.php');
echo ('</script>');
?>