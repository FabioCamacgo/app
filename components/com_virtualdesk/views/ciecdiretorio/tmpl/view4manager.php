<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteciecdiretorioHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio.php');
    //JLoader::register('VirtualDeskSiteciecdiretorioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio_files.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('ciecdiretorio');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;


    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-gallery.css');


    // ciecdiretorio - CSS Comum
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/ciecdiretorio/tmpl/ciecdiretorio-comum.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/ciecdiretorio/tmpl/view4manager.css');

    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputciecdiretorio_Id = JFactory::getApplication()->input->getInt('ciecdiretorio_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteciecdiretorioHelper::getciecdiretorioView4ManagerDetail($getInputciecdiretorio_Id);


    // Carrega de ficheiros associados
    if(!empty($this->data->referencia)) {
        $objAlertFiles = new VirtualDeskSiteciecdiretorioFilesHelper();
        $ListFilesAlert = $objAlertFiles->getFileGuestLinkByRefId ($this->data->referencia);
    }

    $ciecdiretorioEstadoList2Change = VirtualDeskSiteciecdiretorioHelper::getciecdiretorioEstadoAllOptions($language_tag);

    $listaParceiros = VirtualDeskSiteciecdiretorioHelper::getListaParceiros($this->data->referencia);

    $listaTipologias = VirtualDeskSiteciecdiretorioHelper::getListaTipologias($this->data->ciecdiretorio_id);
    $aux = 0;
    foreach($listaTipologias as $rowEstado) :
        if($aux == 0){
            $aux = 1;
            $tipologiasView = VirtualDeskSiteciecdiretorioHelper::getTipologiaName($rowEstado['id_tipologia']);
        } else {
            $tipologiasView = $tipologiasView . ', ' . VirtualDeskSiteciecdiretorioHelper::getTipologiaName($rowEstado['id_tipologia']);
        }
    endforeach;


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
        return false;
    }

?>

<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: 50% !important;  -moz-border-radius: 50% !important;  border-radius: 50% !important;   width: 150px; height: auto;}
    .portfolio-content.portfolio-1 .cbp-caption-activeWrap {background-color: rgba(50, 197, 210, 0.5); }
    .portlet.light.bordered>.portlet-title { border-bottom: 1px solid #dcdcdc; }
    .mt-timeline-2>.mt-container>.mt-item>.mt-timeline-content>.mt-content-container { border: 1px solid #d3d7e9; }
    .text-wrap {white-space:normal;}
    .static-info{margin-bottom: 20px;}
    .static-info .name {line-height: 2;}

    .static-info .value {background-color: #f9f9f9; padding: 5px; margin:5px;font-weight: normal; border: 1px solid #e7ecf1;}

    .fileuploader-items .fileuploader-item .fileuploader-item-image img { left: 0;  top: 0; -webkit-transform: none; transform: none; position: relative;
        max-width: 100%;
        max-height: 100%;
    }


</style>

<div class="portlet light bordered form-fit">
    <div class="portlet-title">

        <div class="caption">
            <i class="icon-calendar  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_PROJETOSAPOIADOS' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio&layout=edit4manager&vdcleanstate=1&ciecdiretorio_id=' . $this->escape($this->data->ciecdiretorio_id)); ?>" class="btn btn-circle btn-outline green">
                <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_ADDNEW' ); ?></a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->

    </div>


    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">

            <div class="form-body">

                <div class="portlet light">

                    <div class="portlet-body">
                        <div class="col-md-8 main">
                            <div class="portlet light bg-inverse bordered" style="min-height: 395px;">
                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_DADOSDETALHE'); ?></div></div>
                                <div class="portlet-body">



                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LISTA_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->referencia, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="row static-info">
                                                <div class=" name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_ESTADO_LABEL' ); ?></div>
                                                <div class=" value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=ciecdiretorio.getNewEstadoName4ManagerByAjax&ciecdiretorio_id='. $this->escape($this->data->ciecdiretorio_id)); ?>" >
                                                     <span class="label <?php echo VirtualDeskSiteciecdiretorioHelper::getciecdiretorioEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estadoProjeto, ENT_QUOTES, 'UTF-8');?> </span>
                                                 </div>

                                            </div>
                                        </div>


                                        <div class="col-md-3" style="margin-top: -5px;">
                                            <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                        </div>

                                        <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                    </div>
                                                    <div class="modal-body blocoAlterar2NewEstado">

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group" style="padding:0">
                                                                        <label> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                        <div style="padding:0;">
                                                                            <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                <?php foreach($ciecdiretorioEstadoList2Change as $rowEstado) : ?>
                                                                                    <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group">
                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                        <textarea rows="5" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" ></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group blocoIconsMsgAviso">
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span> <i class="fa"></i> </span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span class="blocoIconsMsgAviso_Texto"></span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                        <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=ciecdiretorio.sendAlterar2NewEstado4ManagerByAjax&ciecdiretorio_id=' . $this->escape($this->data->ciecdiretorio_id)); ?>" >
                                                        <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                        </button>
                                                        <span> <i class="fa"></i> </span>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>



                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_PROGRAMA_LABEL' ); ?></div>
                                                <?php
                                                if($this->data->idPrograma == '1'){
                                                    ?>
                                                    <div class="value cultura"><?php echo htmlentities( $this->data->programa, ENT_QUOTES, 'UTF-8');?></div>
                                                    <?php
                                                } else if($this->data->idPrograma == '2'){
                                                    ?>
                                                    <div class="value media"><?php echo htmlentities( $this->data->programa, ENT_QUOTES, 'UTF-8');?></div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_PROJETO_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->projeto, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_DESIGNACAO_LABEL' ); ?></div>
                                                <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->designacao);?></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_ENTIDADE_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->entidade, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php
                                            if($this->data->idPrograma == '1'){
                                                ?>
                                                    <div class="col-md-6">
                                                        <div class="row static-info">
                                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LIDER_LABEL' ); ?></div>
                                                            <div class="value">
                                                                <?php
                                                                if(empty($this->data->lider) || $this->data->lider == 'Null' || $this->data->lider == 'NULL'){
                                                                    echo '';
                                                                } else {
                                                                    echo htmlentities( $this->data->lider, ENT_QUOTES, 'UTF-8') . ' - ' . htmlentities( $this->data->pais_lider, ENT_QUOTES, 'UTF-8');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                            }

                                        ?>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LINHAFINANCIAMENTO_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->linha_Financiamento, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php

                                        if($this->data->idLinha == 15){
                                            ?>
                                            <div class="col-md-6">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_ESCALA_LABEL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->escala, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                            <?php
                                        }

                                        if($this->data->idPrograma == 1){
                                            ?>
                                            <div class="col-md-6">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_TIPOLOGIA_LABEL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $tipologiasView, ENT_QUOTES, 'UTF-8');?> </div>

                                                </div>
                                            </div>

                                            <?php
                                        } else if($this->data->idPrograma == 2){
                                            ?>
                                            <div class="col-md-6">
                                                <div class="row static-info">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_CATEGORIA_LABEL' ); ?></div>
                                                    <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_ANOAPOIO_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->ano_Apoio, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_VALORFINANCIAMENTO_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->valorFinanciamento, ENT_QUOTES, 'UTF-8') . '€';?> </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_CALL_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->call, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LINK_LABEL' ); ?></div>
                                                <div class="value">
                                                    <?php
                                                        if(!empty($this->data->link) && $this->data->link != 'NULL' && $this->data->link != 'Null'){
                                                            ?>
                                                                <a href="<?php echo $this->data->link;?>" target="_blank"><?php echo substr($this->data->link, 0, 30) . '...';?></a>
                                                            <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_DATAINICIO_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->data_Inicio, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_DATAFIM_LABEL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->data_Fim, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        if(count($listaParceiros) > 0){
                                            ?>
                                                <div class="row partners">
                                                    <table width="100%">
                                                        <thead>
                                                            <tr>
                                                                <td><?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_PARCEIROS_LABEL' ); ?></td>
                                                                <td><?php echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_PAIS_LABEL' ); ?></td>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <?php
                                                                foreach($listaParceiros as $rowWSL) :
                                                                    ?>
                                                                    <tr>
                                                                        <td><?php echo $rowWSL['parceiro'];?></td>
                                                                        <td><?php echo $rowWSL['pais'];?></td>
                                                                    </tr>
                                                                <?php
                                                                endforeach;
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php
                                        }
                                    ?>


                                </div>


                                <div class="row">
                                    <div class="row static-info">
                                        <div class="col-md-12 ">
                                            <h6>
                                                <?php
                                                echo JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LISTA_DATACRIACAO' ).$labelseparator;
                                                echo htmlentities( $this->data->data_Criacao, ENT_QUOTES, 'UTF-8');
                                                if($this->data->data_Criacao != $this->data->data_Alteracao) echo ' , ' . JText::_( 'COM_VIRTUALDESK_CIECDIRETORIO_LISTA_DATAALTERACAO' ) . ' ' . htmlentities( $this->data->data_Alteracao, ENT_QUOTES, 'UTF-8');
                                                ?>
                                            </h6>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 main">

                            <div class="portlet light bg-inverse bordered attach" style="min-height: 395px;">
                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_ALERTA_TAB_IMGDOCS'); ?></div></div>
                                <div class="portlet-body">

                                    <div class="form-group" id="uploadField">

                                        <div class="col-md-12">

                                            <div class="file-loading">
                                                <input type="file" name="fileupload" id="fileupload" >
                                            </div>
                                            <div id="errorBlock" class="help-block"></div>
                                        </div>
                                        <input type="hidden" name="vdFileUpChanged" value="0">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio&layout=edit4manager&ciecdiretorio_id=' . $this->escape($this->data->ciecdiretorio_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('ciecdiretorio_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->ciecdiretorio_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('ciecdiretorio_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('ciecdiretorio_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

        </form>
    </div>
</div>

<?php
    echo $localScripts;
    echo ('<script>');
        require_once (JPATH_SITE . '/components/com_virtualdesk/views/ciecdiretorio/tmpl/view4manager.js.php');
    echo ('</script>');
?>