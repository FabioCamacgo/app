<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteciecdiretorioHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio.php');
JLoader::register('VirtualDeskSiteciecdiretorioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio_files.php');
JLoader::register('VirtualDeskSiteFormularioHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/EuropaCriativa/Formulario.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('ciecdiretorio');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('ciecdiretorio', 'addnew4managers'); // verifica permissão acesso ao layout para editar
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/ciecdiretorio/tmpl/addnew4manager.css');

//Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');
// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteciecdiretorioHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.ciecdiretorio.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


$dataAllUsersList = array();
$dataAllUsersList = VirtualDeskSiteciecdiretorioHelper::getUsersListAtiveNotBlocked();

$obParam      = new VirtualDeskSiteParamsHelper();

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
    <style>
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
        .fileinput-preview img {display:block; float: none; margin: 0 auto;}
        .select2-bootstrap-prepend .input-group-btn {vertical-align: bottom};
    </style>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            <?php
            // Objecto inicializado com as mensagens de ciecdiretorio já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
            ?>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <ul class="nav nav-tabs  ">

                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio&layout=list4manager'); ?>">
                            <h4>
                                <i class="fa fa-tasks"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_TAB_SUBMETIDOS'); ?>
                            </h4>
                        </a>
                    </li>


                    <li class="active">

                        <a href="#tab_ciecdiretorio_Novo" data-toggle="tab">
                            <h4>
                                <i class="fa fa-plus"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_ADDNEW'); ?>
                            </h4>
                        </a>

                    </li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="tab_ciecdiretorio_Novo">

                        <form id="new-ciecdiretorio"
                              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=ciecdiretorio.create4manager'); ?>" method="post"
                              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                              role="form">

                            <div class="form-body">

                                <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_ADDNEW'); ?></h3></legend>

                                <!--   Nome do projeto  -->
                                <div class="col-md-12">
                                    <div class="form-group static-info" id="projName">
                                        <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_NOMEPROJETO'); ?></div>
                                        <div class="value">
                                            <input type="text" class="form-control" autocomplete="off" placeholder="" name="nomeProjeto" id="nomeProjeto" maxlength="500" value="<?php echo $nomeProjeto; ?>"/>
                                        </div>
                                    </div>
                                </div>


                                <!--   DESCRIÇAO   -->
                                <div class="col-md-12">
                                    <div class="form-group static-info" id="desc">
                                        <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DESIGNACAO_LABEL'); ?></div>
                                        <div class="value">
                                            <textarea  class="form-control wysihtml5" rows="12" name="descricao" id="descricao" maxlength="4000">
                                                <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao); ?></textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <!--   Programa  -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="prog">
                                            <div class="name" for="field_programa"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PROGRAMA') ?></div>
                                            <?php $Programa = VirtualDeskSiteFormularioHelper::getPrograma()?>
                                            <div class="value">
                                                <select name="programa" value="<?php echo $programa; ?>" id="programa" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                                    <?php
                                                    if(empty($programa)){
                                                        ?>
                                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                        <?php foreach($Programa as $rowWSL) : ?>
                                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                            ><?php echo $rowWSL['programa']; ?></option>
                                                        <?php endforeach;
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $programa; ?>"><?php echo VirtualDeskSiteFormularioHelper::getProgSelect($programa) ?></option>
                                                        <option value=""><?php echo '-'; ?></option>
                                                        <?php $ExcludePrograma = VirtualDeskSiteFormularioHelper::excludePrograma($programa)?>
                                                        <?php foreach($ExcludePrograma as $rowWSL) : ?>
                                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                            ><?php echo $rowWSL['programa']; ?></option>
                                                        <?php endforeach;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <!--   Linhas Financiamento   -->
                                    <div class="col-md-6">
                                        <?php
                                        // Carrega menusec se o id de menumain estiver definido.
                                        $ListaFinanciamento = array();
                                        if(!empty($programa)) {
                                            if( (int) $programa > 0) $ListaFinanciamento = VirtualDeskSiteFormularioHelper::getLinhasFinanciamento($programa);
                                        }

                                        ?>
                                        <div id="blocoLinhasFinanciamento" class="form-group static-info">
                                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_LINHASFINANCIAMENTO'); ?><span class="required">*</span></div>
                                            <div class="value">
                                                <select name="linhaFinanciamento" id="linhaFinanciamento" value="<?php echo $linhaFinanciamento;?>"
                                                    <?php
                                                    if(empty($programa)) {
                                                        echo 'disabled';
                                                    }
                                                    ?>
                                                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                                    <?php
                                                    if(empty($linhaFinanciamento)){
                                                        ?>
                                                        <option value=""><?php echo JText::_( 'COM_VIRTUALDESK_EUROPA_OPCAO' ); ?></option>
                                                        <?php foreach($ListaFinanciamento as $rowMM) : ?>
                                                            <option value="<?php echo $rowMM['id']; ?>"
                                                                <?php
                                                                if(!empty($this->data->linhaFinanciamento)) {
                                                                    if($this->data->linhaFinanciamento == $rowMM['id']) echo 'selected';
                                                                }
                                                                ?>
                                                            ><?php echo $rowMM['name']; ?></option>
                                                        <?php endforeach; ?>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $linhaFinanciamento; ?>"><?php echo VirtualDeskSiteFormularioHelper::getLinhasFinanciamentoName($linhaFinanciamento) ?></option>
                                                        <option value=""><?php echo '-'; ?></option>
                                                        <?php $ExcludeLinhasFinanciamento = VirtualDeskSiteFormularioHelper::excludeLinhasFinanciamento($programa, $linhaFinanciamento)?>
                                                        <?php foreach($ExcludeLinhasFinanciamento as $rowWSL) : ?>
                                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                            ><?php echo $rowWSL['name']; ?></option>
                                                        <?php endforeach;
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="blocoEscala">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group static-info" id="blocoEscalasCooperacao">
                                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_ESCALA') ?></div>
                                                <?php $ListaEscala = VirtualDeskSiteFormularioHelper::getListaEscala()?>
                                                <div class="value">
                                                    <select name="escala" value="<?php echo $escala; ?>" id="escala" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                                        <?php
                                                        if(empty($escala)){
                                                            ?>
                                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                            <?php foreach($ListaEscala as $rowWSL) : ?>
                                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                                ><?php echo $rowWSL['nome']; ?></option>
                                                            <?php endforeach;
                                                        } else {
                                                            ?>
                                                            <option value="<?php echo $escala; ?>"><?php echo VirtualDeskSiteFormularioHelper::getEscalaName($escala) ?></option>
                                                            <option value=""><?php echo '-'; ?></option>
                                                            <?php $ExcludeEscala = VirtualDeskSiteFormularioHelper::excludeEscala($escala)?>
                                                            <?php foreach($ExcludeEscala as $rowWSL) : ?>
                                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                                ><?php echo $rowWSL['nome']; ?></option>
                                                            <?php endforeach;
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">

                                    <!--   Nome da Entidade  -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="entName">
                                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ENTIDADE'); ?></div>
                                            <div class="value">
                                                <input type="text" class="form-control" autocomplete="off" placeholder="" name="entidade" id="entidade" maxlength="500" value="<?php echo $entidade; ?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <!--   Ano atribuição -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="year">
                                            <div class="name" for="field_ano"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_ANO') ?></div>
                                            <?php $Ano = VirtualDeskSiteFormularioHelper::getAno()?>
                                            <div class="value">
                                                <select name="ano" value="<?php echo $ano; ?>" id="ano" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                                    <?php
                                                    if(empty($ano)){
                                                        ?>
                                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                        <?php foreach($Ano as $rowWSL) : ?>
                                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                            ><?php echo $rowWSL['ano']; ?></option>
                                                        <?php endforeach;
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $ano; ?>"><?php echo VirtualDeskSiteFormularioHelper::getAnoSelect($ano) ?></option>
                                                        <option value=""><?php echo '-'; ?></option>
                                                        <?php $ExcludeAno = VirtualDeskSiteFormularioHelper::excludeAno($ano)?>
                                                        <?php foreach($ExcludeAno as $rowWSL) : ?>
                                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                            ><?php echo $rowWSL['ano']; ?></option>
                                                        <?php endforeach;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="col-md-12">

                                    <!--   URL DO PROJETO  -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="linkProj">
                                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LINK'); ?></div>
                                            <div class="value">
                                                <input type="text" class="form-control" autocomplete="off" placeholder="" name="link" id="link" maxlength="1000" value="<?php echo $link; ?>"/>
                                            </div>
                                        </div>
                                    </div>


                                    <!--   CALL DO PROJETO  -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="callProj">
                                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_CALL'); ?></div>
                                            <div class="value">
                                                <input type="text" class="form-control" autocomplete="off" placeholder="" name="call" id="call" maxlength="100" value="<?php echo $call; ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">

                                    <!--   Data de inicio  -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="projStart">
                                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAINICIO'); ?><span class="required">*</span></div>
                                            <div class="value">
                                                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                                    <input type="text" placeholder="dd-mm-aaaa" name="dataInicio" id="dataInicio" value="<?php echo $dataInicio; ?>">
                                                    <span class="input-group-btn">
                                                    <button class="btn default" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--   Data de fim  -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="projEnd">
                                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_DATAFIM'); ?><span class="required">*</span></div>
                                            <div class="value">
                                                <div class="input-group date date-picker1" data-date-format="dd-mm-yyyy">
                                                    <input type="text" placeholder="dd-mm-aaaa" name="dataFim" id="dataFim" value="<?php echo $dataFim; ?>">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12">

                                    <!--   VALOR FINANCIAMENTO  -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="valorProj">
                                            <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_VALOR'); ?></div>
                                            <div class="value">
                                                <input type="text" class="form-control" autocomplete="off" placeholder="" name="valor" id="valor" maxlength="30" value="<?php echo $valor; ?>"/>
                                                <span>€</span>
                                            </div>
                                        </div>
                                    </div>

                                    <!--   Layout Detalhe -->
                                    <div class="col-md-6">
                                        <div class="form-group static-info" id="layoutDetalhe">
                                            <div class="name" for="field_ano"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LAYOUT') ?><span class="required">*</span></div>
                                            <?php $Layout = VirtualDeskSiteFormularioHelper::getLayout()?>
                                            <div class="value">
                                                <select name="layout" value="<?php echo $layout; ?>" id="layout" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                                    <?php
                                                    if(empty($layout)){
                                                        ?>
                                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                        <?php foreach($Layout as $rowWSL) : ?>
                                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                            ><?php echo $rowWSL['layout']; ?></option>
                                                        <?php endforeach;
                                                    } else {
                                                        ?>
                                                        <option value="<?php echo $layout; ?>"><?php echo VirtualDeskSiteFormularioHelper::getLayoutSelect($layout) ?></option>
                                                        <option value=""><?php echo '-'; ?></option>
                                                        <?php $ExcludeLayout = VirtualDeskSiteFormularioHelper::excludeLayout($layout)?>
                                                        <?php foreach($ExcludeLayout as $rowWSL) : ?>
                                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                            ><?php echo $rowWSL['layout']; ?></option>
                                                        <?php endforeach;
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="blocoFormCultura">

                                    <div class="col-md-12">
                                        <!--   Líder  -->
                                        <div class="col-md-6">
                                            <div class="form-group static-info" id="liderName">
                                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_LIDER'); ?></div>
                                                <div class="value">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="lider" id="lider" maxlength="500" value="<?php echo $lider; ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                        <!--   País Lider  -->
                                        <div class="col-md-6">
                                            <div class="form-group static-info" id="paisLider">
                                                <div class="name" for="field_pais"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PAIS') ?></div>
                                                <?php $Pais = VirtualDeskSiteFormularioHelper::getPais()?>
                                                <div class="value">
                                                    <select name="pais" value="<?php echo $pais; ?>" id="pais" class="form-control select2 select2-search" >
                                                        <?php
                                                        if(empty($pais)){
                                                            ?>
                                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                            <?php foreach($Pais as $rowWSL) : ?>
                                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                                ><?php echo $rowWSL['pais']; ?></option>
                                                            <?php endforeach;
                                                        } else {
                                                            ?>
                                                            <option value="<?php echo $pais; ?>"><?php echo VirtualDeskSiteFormularioHelper::getPaisSelect($pais) ?></option>
                                                            <option value=""><?php echo '-'; ?></option>
                                                            <?php $ExcludePais = VirtualDeskSiteFormularioHelper::excludePais($pais)?>
                                                            <?php foreach($ExcludePais as $rowWSL) : ?>
                                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                                ><?php echo $rowWSL['pais']; ?></option>
                                                            <?php endforeach;
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">

                                        <!--   Tipologia   -->
                                        <div class="col-md-6">
                                            <div class="form-group static-info" id="tipo">
                                                <?php $TIPOLOGIAS = VirtualDeskSiteFormularioHelper::getTipologia()?>
                                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPA_TIPOLOGIA'); ?><span class="required">*</span></div>
                                                <div class="value">
                                                    <div class="input-group ">
                                                        <select name="tipologia[]" value="<?php echo $tipologia; ?>" id="tipologia" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true" multiple="multiple">
                                                            <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                            <?php foreach ($TIPOLOGIAS as $rowWSL) : ?>
                                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                                ><?php echo $rowWSL['tipologia']; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 partners">

                                        <h4><?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_PARCEIROS'); ?></h4>

                                        <div  class="mt-repeater">
                                            <div data-repeater-list="ParceiroInput">
                                                <div data-repeater-item class="mt-repeater-item ">

                                                    <div class="mt-repeater-input ParceiroInput ParceiroInput-new" data-provides="ParceiroInput">
                                                        <div class="input-group">
                                                            <div class="form-control uneditable-input input-fixed" data-trigger="ParceiroInput">

                                                                <div class="col-md-6">
                                                                    <div class="form-group static-info" id="parceiroName">
                                                                        <div class="name"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PARCEIRO'); ?><span class="required">*</span></div>
                                                                        <div class="value">
                                                                            <input type="text" class="form-control" autocomplete="off" placeholder="" name="parceiro" maxlength="500" value="<?php echo $parceiro1; ?>"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <!--   País Parceiro  -->
                                                                <div class="col-md-6">
                                                                    <div class="form-group static-info" id="paisPartner">
                                                                        <div class="name" for="field_pais"><?php echo JText::_('COM_VIRTUALDESK_EUROPACRIATIVA_PAISPARCEIRO') ?><span class="required">*</span></div>
                                                                        <?php $Pais = VirtualDeskSiteFormularioHelper::getPais()?>
                                                                        <div class="value">
                                                                            <select name="paisParceiro" value="<?php echo $paisParceiro; ?>" class="form-control select2 select2-search" >
                                                                                <?php
                                                                                if(empty($paisParceiro)){
                                                                                    ?>
                                                                                    <option value=""><?php echo JText::_('COM_VIRTUALDESK_EUROPA_OPCAO'); ?></option>
                                                                                    <?php foreach($Pais as $rowWSL) : ?>
                                                                                        <option value="<?php echo $rowWSL['id']; ?>"
                                                                                        ><?php echo $rowWSL['pais']; ?></option>
                                                                                    <?php endforeach;
                                                                                } else {
                                                                                    ?>
                                                                                    <option value="<?php echo $paisParceiro; ?>"><?php echo VirtualDeskSiteFormularioHelper::getPaisSelect($paisParceiro) ?></option>
                                                                                    <option value=""><?php echo '-'; ?></option>
                                                                                    <?php $ExcludePais = VirtualDeskSiteFormularioHelper::excludePais($paisParceiro)?>
                                                                                    <?php foreach($ExcludePais as $rowWSL) : ?>
                                                                                        <option value="<?php echo $rowWSL['id']; ?>"
                                                                                        ><?php echo $rowWSL['pais']; ?></option>
                                                                                    <?php endforeach;
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <a href="javascript:;" class="btn btn-danger mt-repeater-delete ParceiroInput-exists" data-repeater-delete data-dismiss="ParceiroInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>
                                                        </div>
                                                    </div>

                                                    <div class="mt-repeater-input">
                                                    </div>

                                                </div>
                                            </div>

                                            <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                                <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                                        </div>

                                    </div>

                                </div>



                                <div class="col-md-12 docs">
                                    <!--   Upload Docs -->
                                    <h4><?php echo JText::_('COM_VIRTUALDESK_CIECDIRETORIO_DOCS'); ?></h4>
                                    <div class="form-group static-info" id="uploadField">
                                        <div class="value">
                                            <div class="file-loading">
                                                <input type="file" name="fileupload[]" id="fileupload" multiple>
                                            </div>
                                            <div id="errorBlock" class="help-block"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="form-actions right">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                        </button>
                                        <a class="btn default"
                                           href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=ciecdiretorio'); ?>"
                                           title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('ciecdiretorio.create4manager',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                            <?php echo JHtml::_('form.token'); ?>

                        </form>

                    </div>

                </div>
            </div>

        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/ciecdiretorio/tmpl/addnew4manager.js.php');
echo ('</script>');
?>