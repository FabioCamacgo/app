<?php

JLoader::register('VirtualDeskSiteciecdiretorioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_ciecdiretorio_files.php');

$objFiles              = new VirtualDeskSiteciecdiretorioFilesHelper();
$vdPreFileUploaderLista = $objFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->referencia);
?>
var ciecdiretorioEdit = function() {

    var handleciecdiretorioEdit= function() {

        jQuery('#edit-ciecdiretorio').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#edit-ciecdiretorio').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#edit-ciecdiretorio').validate().form()) {
                    jQuery('#edit-ciecdiretorio').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleciecdiretorioEdit();
        }
    };

}();


/*
 * Inicialização do Select 2
 */
var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();


var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 1,

            // if null - has no limits
            // example: 3
            fileMaxSize: 3,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png'],

            enableApi: true,

            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                '<ul class="fileuploader-items-list">' +
                '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                '</ul>' +
                '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploaderLista)) echo(', files:  '.$vdPreFileUploaderLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val('1');
                console.log( jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val());
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val('1');
                console.log( jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val());
                return true;
            }

        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();


var ComponentsEditors = function () {

    var handleWysihtml5 = function () {

        let  ckconfig_removeButtons = 'Subscript,Superscript,Anchor,SpecialChar,Image,About,Scayt';
        let  ckconfig_toolbarGroups =   [
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'others', groups: [ 'others' ] },
            { name: 'colors', groups: [ 'colors' ] }
        ];

        CKEDITOR.replace( 'descricao', {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: ckconfig_toolbarGroups,
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: ckconfig_removeButtons
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();

        }
    };

}();



/*
* Inicialização da Validação
*/
jQuery(document).ready(function() {

    var valorProg = jQuery("#programa").val();
    var valorLinhaFinanciamento = jQuery("#linhaFinanciamento").val();

    if(valorProg == 1){
        jQuery('.blocoFormCultura').css('display', 'block');
    } else {
        jQuery('.blocoFormCultura').css('display', 'none');
    }

    if(valorLinhaFinanciamento == '15'){
        jQuery('.blocoEscala').css('display','block');
    } else {
        jQuery('.blocoEscala').css('display','none');
    }

    // Validações
    ciecdiretorioEdit.init();

    // Select 2
    ComponentsSelect2.init();

    ComponentFileUploader.init();

    ComponentsEditors.init();

    jQuery('.date-picker1').datepicker({autoclose:true});
    jQuery('.date-picker1').on('hide', function (e) { e.preventDefault(); });
    jQuery('#dataFim').attr('readonly',false);
    jQuery('#dataInicio').attr('readonly',false);

    jQuery("#programa").change(function() { /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var programa = jQuery(this).val();
        /* GET THE VALUE OF THE SELECTED DATA */

        if (programa == '1') {
            jQuery('.blocoFormCultura').css('display', 'block');
        } else {
            jQuery('.blocoFormCultura').css('display', 'none');
            jQuery('.blocoEscala').css('display','none');
        }

        if(programa == null || programa == '') {

            jQuery('.blocoEscala').css('display','none');

            var select = jQuery('#linhaFinanciamento');
            select.find('option').remove();
            select.prop('disabled', true);
            select.append(jQuery('<option>').text('Linhas de financiamento'));

        } else {
            var dataString = "programa="+programa; /* STORE THAT TO A DATA STRING */

            // show loader
            App.blockUI({ target:'#blocoLinhasFinanciamento',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=europa_getlinha&programa=' + programa ,

                success: function(output) {

                    var select = jQuery('#linhaFinanciamento');
                    select.find('option').remove();

                    if (output == null || output=='') {
                        select.prop('disabled', true);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoLinhasFinanciamento');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoLinhasFinanciamento');
                }
            });
        }

    });


    jQuery("#linhaFinanciamento").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var linhaFinanciamento = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(linhaFinanciamento == '15'){
            jQuery('.blocoEscala').css('display','block');
        } else {
            jQuery('.blocoEscala').css('display','none');
        }

    });

});