<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteContactUsFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactusfields.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkDetailEditAccess('contactus');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/components-select2.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/contactus/tmpl/edit.js'  . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/contactus/tmpl/maps.js'  . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/contactus/tmpl/edit_cubeportfolio.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');


//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');
$useravatar_extensions  = $params->get('useravatar_extensions');
$useravatar_max_size_Mb = (int)$params->get('useravatar_max_size');
$useravatar_max_size    = (int)$params->get('useravatar_max_size') * 1024 * 1024;
$useravatar_width       = $params->get('useravatar_width');
$useravatar_height      = $params->get('useravatar_height');


//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


// Definição dos campos de utilizador
$ObjContactUsFields = new VirtualDeskSiteContactUsFieldsHelper();
$arContactUsFieldsConfig = $ObjContactUsFields->getFields();
// Dropdowns
// Multichoice / select2 : Áreas de Atuação
$AreaActListAllNyGroup = VirtualDeskSiteContactUsFieldsHelper::getAreaActListAllByGroup($fileLangSufix);

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput{display:block;}
    .fileinput-preview {display:block; max-height: 100%; }
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}
    .select2-bootstrap-prepend .input-group-btn {vertical-align: bottom};
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.cancel'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
            <a href="javascript;"
               class="btn btn-circle btn-outline red vd-button-delete"
               data-title="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DELETE_CONFIRM');?>"
               data-type="warning"
               data-allow-outside-click="true"
               data-show-confirm-button="true"
               data-show-cancel-button="true"
               data-cancel-button-class="btn-danger"
               data-cancel-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_NOCANCEL' ); ?> "
               data-confirm-button-text="<?php echo JText::_( 'COM_VIRTUALDESK_YESDELETE' ); ?> "
               data-confirm-button-class="btn-info"
               data-placement="top"
               data-trigger="hover"
               data-vd-url-delete="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.delete&contactus_id='. $this->data->contactus_id); ?>" >
                <i class="fa fa-trash-o"></i>  <?php echo JText::_('COM_VIRTUALDESK_DELETE'); ?> </a>

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>



    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <?php
        // Objecto inicializado com as mensagens de alerta já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
        ?>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body form">

        <form id="form-contactus"
              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.update'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
              role="form">

            <div class="form-body">

                <div class="row">
                    <div class="col-md-9">

                        <?php if (array_key_exists('status',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_STATUS_LABEL'); ?></label>
                                <div class="col-md-9">
                                    <?php $ContactUsStatus = VirtualDeskSiteContactUsHelper:: getContactUsStatusAll($fileLangSufix); ?>
                                    <div class="input-group ">
                                        <select name="status" required
                                            <?php // echo $arContactUsFieldsConfig['ContactUsField_STATUS_Required']; ?>
                                                id="status" class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                                            <option></option>
                                            <?php foreach($ContactUsStatus as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                    <?php
                                                    if(!empty($this->data->status)) {
                                                        if( (int)($rowStatus['id'] == (int) $this->data->status) ) echo 'selected';
                                                    }
                                                    ?>
                                                ><?php echo $rowStatus['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>



                        <?php if (array_key_exists('ctlanguage',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_CTLANGUAGE_LABEL'); ?></label>
                                <div class="col-md-9">
                                    <?php $ContactUsLanguageAll = VirtualDeskSiteContactUsHelper::getContactUsLanguageAll($fileLangSufix); ?>
                                    <div class="input-group ">
                                        <select name="ctlanguage" required
                                            <?php //echo $arContactUsFieldsConfig['ContactUsField_CTLANGUAGE_Required']; ?>
                                                id="ctlanguage" class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                                            <option></option>
                                            <?php foreach($ContactUsLanguageAll as $rowLang) : ?>
                                                <option value="<?php echo $rowLang['id']; ?>"
                                                    <?php
                                                    if(!empty($this->data->ctlanguage)) {
                                                        if( (int)($rowLang['id'] == (int) $this->data->ctlanguage) ) echo 'selected';
                                                    }
                                                    ?>
                                                ><?php echo $rowLang['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if (array_key_exists('areaact',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                            <div class="form-group">

                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_AREAACT_LABEL'); ?></label>
                                <div class="col-md-9">

                                    <div class="input-group select2-bootstrap-prepend">
                                                <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" data-select2-open="areaact">
                                                            <span class="glyphicon glyphicon-search"></span>
                                                        </button>
                                                </span>
                                        <div>
                                            <select name="areaact[]"  id="areaact" required
                                                <?php // echo $arContactUsFieldsConfig['ContactUsField_AREAACT_Required']; ?>
                                                    class="form-control input-lg select2-multiple select2-hidden-accessible" multiple tabindex="-1" aria-hidden="true">
                                                <option></option>
                                                <?php foreach($AreaActListAllNyGroup as $rowGroupAreaAct) : ?>
                                                    <optgroup label="<?php echo $rowGroupAreaAct['group']['groupname']; ?>">
                                                        <?php foreach($rowGroupAreaAct['rows'] as  $rowAreaAct) : ?>
                                                            <option value="<?php echo $rowAreaAct['id']; ?>"
                                                                <?php if(is_array($this->data->areaact)) {
                                                                    if( in_array($rowAreaAct['id'],$this->data->areaact) )
                                                                    {
                                                                        echo 'selected';
                                                                    }
                                                                }
                                                                ?>
                                                            ><?php echo $rowAreaAct['name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </optgroup>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        <?php endif; ?>



                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_SUBJECT_LABEL'); ?></label>
                            <div class="col-md-9">
                                <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_SUBJECT_PLACEHOLDER'); ?>" name="subject"
                                       id="subject" maxlength="250"
                                       value="<?php  if(!empty($this->data->subject)) echo htmlentities($this->data->subject, ENT_QUOTES, 'UTF-8'); ?>">
                            </div>
                        </div>



                        <?php if (array_key_exists('vdwebsitelist',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_VDWEBSITELIST_LABEL'); ?></label>
                                <div class="col-md-9">
                                    <?php $ContactUsWebSiteListAll = VirtualDeskSiteContactUsHelper::getContactUsWebSiteListAll($fileLangSufix)?>
                                    <div class="input-group ">
                                        <select name="vdwebsitelist" required
                                            <?php //echo $arContactUsFieldsConfig['ContactUsField_VDWEBSITELIST_Required']; ?>
                                                id="vdwebsitelist" class="form-control select2 select2-hidden-accessible select2-nosearch" tabindex="-1" aria-hidden="true">
                                            <option></option>
                                            <?php foreach($ContactUsWebSiteListAll as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                    <?php
                                                    if(!empty($this->data->vdwebsitelist)) {
                                                        if( (int)($rowWSL['id'] == (int) $this->data->vdwebsitelist) ) echo 'selected';
                                                    }
                                                    ?>
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>




                        <?php if (array_key_exists('vdmenumain',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                            <?php
                            // Carrega menusec se o id de menumain estiver definido.
                            $ListaDeMenuMain = array();
                            if( (int) $this->data->vdwebsitelist > 0) $ListaDeMenuMain = VirtualDeskSiteContactUsFieldsHelper::getMenuMainByWebSiteList($this->data->vdwebsitelist);
                            ?>
                            <div id="blocoMenuMain" class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_VDMENUMAIN_LABEL' ); ?></label>
                                <div class="col-md-9">

                                    <select name="vdmenumain"  id="vdmenumain"  required
                                        <?php if( (int)$this->data->vdwebsitelist <= 0 || empty($ListaDeMenuMain) ) echo 'disabled'; ?>
                                            class="form-control " tabindex="-1">
                                        <option> </option>
                                        <?php foreach($ListaDeMenuMain as $rowMM) : ?>
                                            <option value="<?php echo $rowMM['id']; ?>" <?php if($this->data->vdmenumain == $rowMM['id']) echo 'selected'; ?>><?php echo $rowMM['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                            </div>
                        <?php endif; ?>




                        <?php if (array_key_exists('vdmenusec',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                            <?php
                            // Carrega menusec se o id de menumain estiver definido.
                            $ListaDeMenuSec = array();
                            if( (int) $this->data->vdmenumain > 0) $ListaDeMenuSec = VirtualDeskSiteContactUsFieldsHelper::getMenuSecByMenuMainList($this->data->vdmenumain);
                            ?>

                            <div id="blocoMenuSec" class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_VDMENUSEC_LABEL' ); ?></label>
                                <div class="col-md-9">

                                    <select name="vdmenusec"  id="vdmenusec"  required
                                        <?php if( (int)$this->data->vdmenumain <= 0 || empty($ListaDeMenuSec) ) echo 'disabled'; ?>
                                            class="form-control " tabindex="-1">
                                        <option> </option>
                                        <?php foreach($ListaDeMenuSec as $rowMS) : ?>
                                            <option value="<?php echo $rowMS['id']; ?>" <?php if($this->data->vdmenusec == $rowMS['id']) echo 'selected'; ?>><?php echo $rowMS['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                            </div>
                        <?php endif; ?>








                        <?php if ((int)$this->data->category == 1) : ?>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION_LABEL'); ?></label>
                                <div class="col-md-9">
                                <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION_LABEL'); ?>"
                                          name="description"  id="description" maxlength="250"><?php if(!empty($this->data->description)) echo htmlentities($this->data->description, ENT_QUOTES, 'UTF-8'); ?></textarea>
                                </div>
                            </div>
                        <?php else : ?>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION2_LABEL'); ?></label>
                                <div class="col-md-9">
                                <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION2_PLACEHOLDER'); ?>"
                                          name="description2"  id="description2" maxlength="250"><?php if(!empty($this->data->description2)) echo htmlentities($this->data->description2, ENT_QUOTES, 'UTF-8'); ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION3_LABEL'); ?></label>
                                <div class="col-md-9">
                                <textarea required class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION3_PLACEHOLDER'); ?>"
                                          name="description3"  id="description3" maxlength="250"><?php if(!empty($this->data->description3)) echo htmlentities($this->data->description3, ENT_QUOTES, 'UTF-8'); ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_LABEL'); ?></label>
                                <div class="col-md-9">
                                <textarea  class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_PLACEHOLDER'); ?>"
                                           name="description4"  id="description4" maxlength="250"><?php if(!empty($this->data->description4)) echo htmlentities($this->data->description4, ENT_QUOTES, 'UTF-8'); ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_OBS_LABEL'); ?></label>
                                    <div class="col-md-9">
                                    <textarea
                                        <?php //echo $arContactUsFieldsConfig['ContactUsField_' . $valConfFieldNames . '_Required']; ?>
                                            class="form-control" rows="3"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_OBS_PLACEHOLDER'); ?>"
                                            name="obs"  id="obs" ><?php if(!empty($this->data->obs)) echo htmlentities($this->data->obs, ENT_QUOTES, 'UTF-8'); ?></textarea>

                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>




                    </div>


                </div>


                <div class="row">
                    <div class="col-md-9">


                        <?php foreach ($ObjContactUsFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                            <?php if($arContactUsFieldsConfig['ContactUsField_' . $valConfFieldNames ]===true) : ?>
                                <?php if($valConfFieldNames == 'TYPE') : ?>
                                    <?php // retirei o TYPe e colocquei em rodapé por defeito, visto neste caso não estar a servir para nada. ?>

                                <?php elseif ($valConfFieldNames == 'CATEGORY') : ?>

                                <?php elseif ($valConfFieldNames == 'SUBCATEGORY') : ?>

                                <?php elseif ($valConfFieldNames == 'STATUS') : ?>

                                <?php elseif ($valConfFieldNames == 'CTLANGUAGE') : ?>

                                <?php elseif ($valConfFieldNames == 'VDWEBSITELIST') : ?>

                                <?php elseif ($valConfFieldNames == 'VDMENUMAIN') : ?>

                                <?php elseif ($valConfFieldNames == 'VDMENUSEC') : ?>

                                <?php elseif ($valConfFieldNames == 'AREAACT') : ?>

                                <?php elseif ($valConfFieldNames == 'OBS') : ?>

                                <?php elseif ($valConfFieldNames == 'ATTACHMENT') : ?>
                                    <?php $userReqFileList = VirtualDeskSiteContactUsHelper::getContactUsFilesLayout ($this->data); ?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_' .$valConfFieldNames. '_LABEL' ); ?></label>
                                        <div class="col-md-9">
                                            <?php require_once( JPATH_SITE . '/components/com_virtualdesk/helpers/html/tpl_contactus_file_list_edit.php'); ?>
                                        </div>
                                    </div>


                                <?php else : ?>


                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>

                    </div>

                    <div class="col-md-9">
                        <p></p>
                    </div>

                </div>

                <div class="col-md-6"></div>

            </div>


            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                        </button>
                        <a class="btn default"
                           href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.cancel'); ?>"
                           title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                    </div>
                </div>
            </div>


            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('contactus_idtype',$setencrypt_forminputhidden); ?>"      value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('contactus_id',$setencrypt_forminputhidden); ?>"          value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->contactus_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('contactus_idcategory',$setencrypt_forminputhidden); ?>"  value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('contactus.update',$setencrypt_forminputhidden); ?>"/>



            <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>


<?php echo $localScripts; ?>
