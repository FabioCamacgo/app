<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('contactus', 'list');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
//$jinput = JFactory::getApplication()->input;
//$language_tag = $jinput->get('lang', 'pt-PT', 'string');
//
//// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
//switch($language_tag)
//{ case 'pt-PT':
//    $fileLangSufix = 'pt_PT';
//    break;
//    default:
//        $fileLangSufix = substr($language_tag, 0, 2);
//    break;
//}

$fileLangSufix = VirtualDeskHelper::getLanguageTag();

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');

// Definição dos campos de utilizador
$ObjContactUsFields = new VirtualDeskSiteContactUsFieldsHelper();
$arContactUsFieldsConfig = $ObjContactUsFields->getFields();

$IdCategoryContactUsGET = JFactory::getApplication()->input->getInt('contactus_idcategory');
$NameCategoryContactUsGET = VirtualDeskSiteContactUsHelper::getContactUsCategoryNameById ($IdCategoryContactUsGET, $fileLangSufix);
?>
    <style>
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
        .fileinput{display:block;}
        .fileinput-preview {display:block; max-height: 100%; }
        .fileinput-preview img {display:block; float: none; margin: 0 auto;}
        .iconVDModified {padding-left: 15px; }
    </style>

    <div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading'))   ; ?></span>
        </div>

        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.addnewcat&layout=addnewcat&contactus_idcategory=' . $this->escape($IdCategoryContactUsGET));  ?>" class="btn btn-circle blue-steel btn-outline">
                <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?></a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
        </div>

    </div>


    <div class="portlet-body ">

        <?php echo $NameCategoryContactUsGET; ?>


        <div class="table-scrollable">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th> <?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_CREATED_LABEL'); ?> </th>
                    <th> <?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_SUBJECT_LABEL'); ?> </th>

                    <?php if (array_key_exists('subcategory',$ObjContactUsFields->arConfFieldNames)) : ?>
                     <th> <?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_AREA_LABEL'); ?> </th>
                    <?php endif; ?>

                    <th></th>
                </tr>
                </thead>

                <tbody>

                <?php foreach($this->data as $contactus) :    ?>
                    <?php  $datecreated = new DateTime($contactus->created);
                           $datemodified= new DateTime($contactus->modified);
                    ?>
                    <tr>
                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&contactus_id=' . $contactus->contactus_id .'&layout=defaultcat'); ?>" > <?php echo $datecreated->format('Y-m-d H:i');  ?> </a>

                            <?php if($datecreated != $datemodified) :?>
                                <span class="iconVDModified"></span>
                                <a href="javascript:;" class="btn btn-sm grey-salsa btn-outline popovers" data-placement="top" data-trigger="hover"
                                   data-content="<?php
                                   echo JText::_('COM_VIRTUALDESK_CONTACTUS_CREATED_LABEL') .' ' . $datecreated->format('Y-m-d H:i') . "\n";
                                   echo JText::_('COM_VIRTUALDESK_CONTACTUS_MODIFIED_LABEL') .' ' . $datemodified->format('Y-m-d H:i');
                                   ?>"
                                ><i class="fa fa-info"></i></a>
                            <?php endif; ?>

                        </td>
                        <td><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&contactus_id=' . $contactus->contactus_id .'&layout=defaultcat'); ?>" > <?php echo $contactus->subject; ?> </a> </td>

                        <?php if (array_key_exists('subcategory',$ObjContactUsFields->arConfFieldNames)) : ?>
                        <td><a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&contactus_id=' . $contactus->contactus_id .'&layout=defaultcat' ); ?>" > <?php echo $contactus->subcategory_name; ?> </a> </td>
                        <?php endif; ?>

                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&contactus_id=' . $contactus->contactus_id .'&layout=defaultcat'); ?>" class=" btn btn-sm grey-salsa btn-outline sbold uppercase">
                                <i class="fa fa-share"></i> View </a>
                        </td>
                    </tr>

                <?php endforeach;    ?>

                </tbody>
            </table>
        </div>

    </div>

</div>

<?php

echo $localScripts;

?>