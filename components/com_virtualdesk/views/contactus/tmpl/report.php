<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteContactUsReportsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus_reports.php');
JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');

$jinput = JFactory::getApplication()->input;

// Verifica qual o report que deve ser carregado
$reporttag    = $jinput->get('reporttag', '', 'string');
$reportformat = $jinput->get('reportformat', '', 'string');

if(empty($reporttag)) exit();

// check session +  joomla user
//$UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();

// NOTA: Grupos e Permissões : É depois verificado em cada ficheiro do report se o utilizador atual tem acesso ao relatório PEDIDO
switch($reporttag)
    { case 'listResume':
            //os dados vão ser carregados por ajax na task getDataForReportsByAjax
            include_once(JPATH_SITE . '/components/com_virtualdesk/views/contactus/tmpl/reports/report_list_resume.php');
            break;

       case 'dashboard':
            // Todo Colocar php com CHARTS para DASHBOARD, por exemplo pedidos no ano/mês com "am charts"
           include_once(JPATH_SITE . '/components/com_virtualdesk/views/contactus/tmpl/reports/report_list_dashboard.php');

            break;

        case 'listResumePDF':
            // Todo Colocar esta opção na caixa de entrada da lista -> report...
            require_once(JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskpdf.php');
            $objPDF = new VirtualDeskPDFHelper();
            ob_start();
            include_once(JPATH_SITE . '/components/com_virtualdesk/views/contactus/tmpl/reports/report_list_resume_pdf.php');
            $pdfContent = ob_get_contents();
            ob_end_clean ();
            $objPDF->pdfContentToBrowser($pdfContent);
            //echo $pdfContent;  // para testes no ecrã---
            exit();
        break;

        default:
            exit();
            break;
    }
?>