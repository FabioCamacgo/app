<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkReportAccess('contactus','resume');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Apens uma verificação
$reporttag    = $jinput->get('reporttag', '', 'string');
$reportformat = $jinput->get('reportformat', '', 'string');

if(empty($reporttag)) exit();

// check session +  joomla user
$UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();


// ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a este relatório


$data = VirtualDeskSiteContactUsHelper::getContactUsList($UserJoomlaID, false);


// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<link href="';
$addscript_end = '" rel="stylesheet">';
$templateName  = 'virtualdesk';

$localStyle   = $addscript_ini . $baseurl . 'templates/' . $templateName .  '/assets/global/plugins/font-awesome/css/font-awesome.min.css' . $addscript_end;
$localStyle  .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/simple-line-icons/simple-line-icons.min.css' . $addscript_end;
$localStyle  .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap/css/bootstrap.min.css' . $addscript_end;
$localStyle  .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css' . $addscript_end;
$localStyle  .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/css/components-rounded.min.css' . $addscript_end;
$localStyle  .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/layouts/layout/css/layout.min.css' . $addscript_end;
$localStyle  .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/layouts/layout/css/themes/darkblue.min.css' . $addscript_end;
$localStyle  .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/layouts/layout/css/custom.min.css' . $addscript_end;

?>
<html>
<head>
<?php echo $localStyle; ?>
</head>
<body>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-briefcase font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); echo JText::_('COM_VIRTUALDESK_CONTACTUS_REPORT_LISTRESUME');; ?> </span>
            </div>



            <div class="portlet-body ">

                <div class="table-scrollable">
                    <table class="table table-hover" cellspacing="0" cellpadding="1" border="1" style="border-color: #0a6aa1;" >
                        <thead>
                        <tr>
                            <th> <?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_CREATED_LABEL'); ?> </th>
                            <th> <?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_SUBJECT_LABEL'); ?> </th>
                        </tr>
                        </thead>

                        <tbody>

                        <?php foreach($data as $contactus) :    ?>
                            <?php  $datecreated = new DateTime($contactus->created);
                            $datemodified= new DateTime($contactus->modified);
                            ?>
                            <tr>
                                <td>
                                    <?php echo $datecreated->format('Y-m-d H:i');  ?>
                                    <?php if($datecreated != $datemodified) :?>
                                        ( <?php echo $datecreated->format('Y-m-d H:i'); ?> )
                                    <?php endif; ?>
                                </td>
                                <td> <?php echo $contactus->subject; ?>  </td>
                            </tr>

                        <?php endforeach;    ?>

                        </tbody>
                    </table>
                </div>

            </div>


        </div>
    </div>

</body>
</html>

