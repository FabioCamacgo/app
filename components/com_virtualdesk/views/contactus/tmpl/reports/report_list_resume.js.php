<?php
defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkReportAccess('contactus','resume');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}
?>
var TableDatatablesManaged = function () {

    var initTable1 = function () {

        var table = jQuery('#tabela_contactus_estadoconta');
        var tableTools = jQuery('#tabela_contactus_estadoconta_tools');

        // begin first table
        var oTable = table.dataTable({
            "dom": '',
            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_CONTACTUS_ESTADONAOINICIADO"); ?>';
                        switch (row[2])
                        {   case '1':
                                defCss = 'label-info';
                                break;
                            case '2':
                                defCss = 'label-success';
                                break;
                            case '3':
                                defCss = 'label-warning';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc
            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=contactus.getDataForReportsByAjax&datatag=listResumoEstado",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },
            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1] } }
            ]

        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });
    }



    var initTable2 = function () {

        var table = jQuery('#tabela_contactus_categoriaconta');
        //var tableTools = jQuery('#tabela_contactus_estadoconta_tools');

        // begin first table
        var oTable = table.dataTable({
            "dom": '',
            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc
            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=contactus.getDataForReportsByAjax&datatag=listResumoCategoria",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();
            },
            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }
            ]

        });

        // handle datatable custom tools
        /*tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });*/
    }



    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();

            initTable2();
        }

    };

}();


jQuery(document).ready(function() {
        TableDatatablesManaged.init();
});