<?php
defined('_JEXEC') or die;

JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkReportAccess('contactus','dashboard');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}

// ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a este relatório


$objReport           = new VirtualDeskSiteContactUsReportsHelper();
$dataTotAnoMes       = $objReport->getContactUsDashboard ($UserJoomlaID);
$dataTotaisPorCat    = $objReport->getContactUsTotaisPorCategoria($UserJoomlaID);
$dataTotaisPorEstado = $objReport->getContactUsTotaisPorEstado($UserJoomlaID);



?>
var ChartsAmcharts = function() {

    var initChartSample1 = function() {
        var chart = AmCharts.makeChart("chart_1", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',
            "color":    '#888',

            "dataProvider": [
             <?php foreach ($dataTotAnoMes as $row) : ?>
                {
                    "year":  '<?php echo $row->year .' / '. $row->mes ; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
             <?php endforeach; ?>


              ],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] em [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Nº de Pedidos",
                "type": "column",
                "valueField": "npedidos"
            }],
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });

        jQuery('#chart_1').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSample6 = function() {
        var chart = AmCharts.makeChart("chart_6", {
            "type": "pie",
            "theme": "light",

            "fontFamily": 'Open Sans',

            "color":    '#888',

            "dataProvider": [
                <?php foreach ($dataTotaisPorCat as $row) : ?>
                {
                    "categoria":  '<?php echo $row->categoria; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>
                ],
            "valueField": "npedidos",
            "titleField": "categoria",
            "exportConfig": {
                menuItems: [{
                    icon: App.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                    format: 'png'
                }]
            }
        });

        jQuery('#chart_6').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSample6_2 = function() {
        var chart = AmCharts.makeChart("chart_6_2", {
            "type": "pie",
            "theme": "light",

            "fontFamily": 'Open Sans',

            "color":    '#888',

            "dataProvider": [
                <?php foreach ($dataTotaisPorEstado as $row) : ?>
                {
                    "estado":  '<?php echo $row->estado; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>
            ],
            "valueField": "npedidos",
            "titleField": "estado",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });

        jQuery('#chart_6_2').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSample10 = function() {
        var latlong = {};
        latlong["FUNCHAL"] = {
            "latitude": 32.67,
            "longitude": -16.93
        };
        latlong["MACHICO"] = {
            "latitude": 32.72,
            "longitude": -16.76
        };


        var mapData = [{
            "code": "FUNCHAL",
            "name": "Funchal",
            "value": 5,
            "color": "#eea638"
        }, {
            "code": "MACHICO",
            "name": "Machico",
            "value": 10,
            "color": "#d8854f"
        }];


        var map;
        var minBulletSize = 10;
        var maxBulletSize = 50;
        var min = Infinity;
        var max = -Infinity;


        // get min and max values
        for (var i = 0; i < mapData.length; i++) {
            var value = mapData[i].value;
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }

        // build map
        AmCharts.ready(function() {
            AmCharts.theme = AmCharts.themes.dark;
            map = new AmCharts.AmMap();
            map.pathToImages = App.getGlobalPluginsPath() + "amcharts/ammap/images/",

            map.fontFamily = 'Open Sans';
            map.fontSize = '13';
            map.color = '#888';

            map.addTitle("Pedidos por Freguesias/Local", 14);
            map.addTitle("", 11);
            map.areasSettings = {
                unlistedAreasColor: "#000000",
                unlistedAreasAlpha: 0.1
            };
            map.imagesSettings.balloonText = "<span style='font-size:14px;'><b>[[title]]</b>: [[value]]</span>";

           // map.geodataSource.url = '<?php echo $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/ammap/maps/MadeiraFreguesias1.json'; ?>';

            var dataProvider = {
                mapVar: AmCharts.maps.portugalRegionsHigh,
                images: []
            }

            // create circle for each country
            for (var i = 0; i < mapData.length; i++) {
                var dataItem = mapData[i];
                var value = dataItem.value;
                // calculate size of a bubble
                var size = (value - min) / (max - min) * (maxBulletSize - minBulletSize) + minBulletSize;
                if (size < minBulletSize) {
                    size = minBulletSize;
                }
                var id = dataItem.code;

                dataProvider.images.push({
                    type: "circle",
                    width: size,
                    height: size,
                    color: dataItem.color,
                    longitude: latlong[id].longitude,
                    latitude: latlong[id].latitude,
                    title: dataItem.name,
                    value: value
                });
            }

            map.dataProvider = dataProvider;

            map.write("chart_10");

            map.zoomToLongLat(25, -17.00,32.74);

        });

        jQuery('#chart_10').closest('.portlet').find('.fullscreen').click(function() {
            map.invalidateSize();
        });
    }



    var initChartSample11 = function() {
        var latlong = {};
        latlong["MADALENADOMAR"] = {
            "latitude": 32.69,
            "longitude": -17.09
        };
        latlong["CANHAS"] = {
            "latitude": 32.68,
            "longitude": -17.10
        };


        var mapData = [{
            "code": "MADALENADOMAR",
            "name": "Madalena do Mar",
            "value": 5,
            "color": "#eea638"
        }, {
            "code": "CANHAS",
            "name": "Canhas",
            "value": 3,
            "color": "#d8854f"
        }];


        var map;
        var minBulletSize = 5;
        var maxBulletSize = 20;
        var min = Infinity;
        var max = -Infinity;


        // get min and max values
        for (var i = 0; i < mapData.length; i++) {
            var value = mapData[i].value;
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }

        // build map
        AmCharts.ready(function() {
            AmCharts.theme = AmCharts.themes.dark;
            map = new AmCharts.AmMap();
            map.pathToImages = App.getGlobalPluginsPath() + "amcharts/ammap/images/",

                map.fontFamily = 'Open Sans';
            map.fontSize = '13';
            map.color = '#888';

            map.addTitle("Pedidos por Freguesias", 14);
            map.addTitle("", 11);
            map.areasSettings = {
                unlistedAreasColor: "#000000",
                unlistedAreasAlpha: 0.1
            };
            map.imagesSettings.balloonText = "<span style='font-size:14px;'><b>[[title]]</b>: [[value]]</span>";

            var dataProvider = {
                mapVar: AmCharts.maps.portugalRegionsLowv2,
                images: [],
                "areas":[
                    { "id": "MAD-PS-MDM", "color": "#c6feda" },
                    { "id": "MAD-PS-C", "color": "#cedefe" },
                    { "id": "MADPS", "color": "#fcdbf0" }
                ]
            }

            // create circle for each country
            for (var i = 0; i < mapData.length; i++) {
                var dataItem = mapData[i];
                var value = dataItem.value;
                // calculate size of a bubble
                var size = (value - min) / (max - min) * (maxBulletSize - minBulletSize) + minBulletSize;
                if (size < minBulletSize) {
                    size = minBulletSize;
                }
                var id = dataItem.code;

                dataProvider.images.push({
                    type: "circle",
                    width: size,
                    height: size,
                    color: dataItem.color,
                    longitude: latlong[id].longitude,
                    latitude: latlong[id].latitude,
                    title: dataItem.name,
                    value: value
                });
            }

            map.dataProvider = dataProvider;

            map.write("chart_11");
        });

        jQuery('#chart_11').closest('.portlet').find('.fullscreen').click(function() {
            map.invalidateSize();
        });
    }


    return {
        //main function to initiate the module

        init: function() {

            initChartSample1();

            initChartSample6();

            initChartSample6_2();

            initChartSample10();

            initChartSample11();
        }

    };

}();

jQuery(document).ready(function() {
    ChartsAmcharts.init();
});