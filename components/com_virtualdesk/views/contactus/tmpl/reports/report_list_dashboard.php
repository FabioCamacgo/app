<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskUserHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdeskuser.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkReportAccess('contactus','dashboard');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Apens uma verificação
$reporttag    = $jinput->get('reporttag', '', 'string');
$reportformat = $jinput->get('reportformat', '', 'string');

if(empty($reporttag)) exit();

// check session +  joomla user
$UserJoomlaID = VirtualDeskSiteUserHelper::getUserSessionId();


// ToDo : Grupos e Permissões : Deve ser verificar se o utilizador atual tem acesso a este relatório


// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
 default:
    $fileLangSufix = substr($language_tag, 0, 2);
    break;
}





// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/amcharts.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/serial.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/pie.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/light.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/patterns.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/chalk.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/ammap/ammap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/ammap/maps/js/portugalRegionsHigh.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/ammap/maps/portugalRegionsLow.js' . $addscript_end;

//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amstockcharts/amstock.js' . $addscript_end;
//$doc  = JFactory::getDocument();
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
?>
    <style>
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
        .fileinput{display:block;}
        .fileinput-preview {display:block; max-height: 100%; }
        .fileinput-preview img {display:block; float: none; margin: 0 auto;}
        .iconVDModified {padding-left: 15px; }
        .EstadoFilterDropBox {float: right; padding-right: 30px; }
    </style>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-briefcase font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); echo JText::_('COM_VIRTUALDESK_CONTACTUS_REPORT_DASHBOARD');; ?> </span>
            </div>

            <div class="actions">
                <!-- Relatórios -->
                <div class="btn-group">
                    <a class="btn green btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                        <i class="icon-bar-chart"></i>
                        <span class="hidden-xs"><?php echo 'Relatórios' ?></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right" >
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=report&reporttag=listResume'); ?>">
                                <i class="icon-bar-chart"></i> <?php echo 'Relatório 1' ?></a>
                        </li>

                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=report&reporttag=dashboard'); ?>">
                                <i class="icon-bar-chart"></i> <?php echo 'Dashboard 1' ?></a>
                        </li>

                    </ul>
                </div>


                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
            </div>
        </div>


        <div class="portlet-body">
            <div class="row">
                <div id="chart_1" class="chart" style="height: 300px;"> </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="chart_6" class="chart" style="height: 400px;"> </div>
                </div>
                <div class="col-md-6">
                    <div id="chart_6_2" class="chart" style="height: 400px;"> </div>
                </div>
            </div>

            <div class="row">
                <div id="chart_10" class="chart" style="height: 600px;"> </div>
            </div>

            <div class="row">
                <div id="chart_11" class="chart" style="height: 600px;"> </div>
            </div>

        </div>

    </div>







<?php

echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/contactus/tmpl/reports/report_list_dashboard.js.php');
echo ('</script>');
?>