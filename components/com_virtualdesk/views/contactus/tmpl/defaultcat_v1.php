<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteContactUsFieldsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactusfields.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check Permissões
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkDetailReadAccess('contactus');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/contactus/tmpl/maps.js'  . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/views/contactus/tmpl/default_cubeportfolio.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');



//Parâmetros
$params = JComponentHelper::getParams('com_virtualdesk');
$useravatar_width = $params->get('useravatar_width');
$useravatar_height = $params->get('useravatar_height');
$contactusavatar_defaultimg = $params->get('contactusavatar_defaultimg');
$labelseparator = ' : ';

if(!empty($this->data)) {

    // se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
    if(!empty($this->data->contactusavatar)) {
        if(is_array($this->data->contactusavatar)) $this->data->contactusavatar = '';
    }
}

// Definição dos campos de utilizador
$ObjContactUsFields = new VirtualDeskSiteContactUsFieldsHelper();
$arContactUsFieldsConfig = $ObjContactUsFields->getFields();

$itemmenuid_lista= $params->get('contactus_menuitemid_list');

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
    <style>
        .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
        .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #fff !important;}
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}

        .portfolio-content.portfolio-1 .cbp-caption-activeWrap {background-color: rgba(50, 197, 210, 0.5); }

    </style>

    <div class="portlet light bordered form-fit">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-envelope  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <?php
            // Data not empty ?
            if(!empty($this->data)) :
                ?>
                <div class="actions">
                    <a href="<?php echo JRoute::_('index.php?'.$itemmenuid_lista.'&option=com_virtualdesk&task=contactus.cancel&layout=listcat&contactus_idcategory=' . $this->escape($this->data->category)); ?>" class="btn btn-circle btn-default">
                        <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=editcat&contactus_id=' . $this->escape($this->data->contactus_id)); ?>" class="btn btn-circle btn-outline green">
                        <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.addnewcat&layout=addnewcat&contactus_idcategory=' . $this->escape($this->data->category)); ?>" class="btn btn-circle blue-steel btn-outline">
                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?>  </a>
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            <?php else :
                $IdCategoryContactUsGET = JFactory::getApplication()->input->getInt('contactus_idcategory');
                ?>
                <div class="actions">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.cancel&layout=listcat&contactus_idcategory=' . $this->escape($IdCategoryContactUsGET)); ?>" class="btn btn-circle btn-default">
                        <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.addnewcat&layout=addnewcat&contactus_idcategory=' . $this->escape($IdCategoryContactUsGET)); ?>" class="btn btn-circle blue-steel btn-outline">
                        <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ADDNEW' ); ?>  </a>
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            <?php endif;?>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">
                <div class="form-body">



                    <?php
                    // Data not empty ?
                    if(!empty($this->data)) :
                    ?>

                    <div class="row">
                        <div class="col-md-12">

                            <?php $getContactUsCategoryName  = VirtualDeskSiteContactUsHelper::getContactUsCategoryNameById($this->data->category, $fileLangSufix); ?>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_CATEGORY_LABEL' ).$labelseparator; ?></label>
                                <div class="col-md-8">
                                    <p class="form-control-static"><?php echo htmlentities( $getContactUsCategoryName, ENT_QUOTES, 'UTF-8');?></p>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_CREATED_LABEL' ).$labelseparator; ?></label>
                                <div class="col-md-8">
                                    <p class="form-control-static"><?php
                                        echo htmlentities( $this->data->created, ENT_QUOTES, 'UTF-8');
                                        if($this->data->created != $this->data->modified) echo ' , ' . JText::_( 'COM_VIRTUALDESK_CONTACTUS_MODIFIED_LABEL' ) . ' ' . htmlentities( $this->data->modified, ENT_QUOTES, 'UTF-8');
                                        ?></p>
                                </div>
                            </div>

                            <?php if (array_key_exists('status',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                                <?php $getContactUsStatusName  = VirtualDeskSiteContactUsHelper::getContactUsStatusNameById($this->data->status, $fileLangSufix); ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_STATUS_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php echo htmlentities( $getContactUsStatusName, ENT_QUOTES, 'UTF-8'); ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if (array_key_exists('ctlanguage',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                                <?php $getContactUsLanguageName  = VirtualDeskSiteContactUsHelper::getContactUsLanguageNameById($this->data->ctlanguage, $fileLangSufix); ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_CTLANGUAGE_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php echo htmlentities( $getContactUsLanguageName, ENT_QUOTES, 'UTF-8'); ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if (array_key_exists('areaact',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_USER_AREAACT_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php  if(!is_array($AreaActListByName)) $AreaActListByName = array();
                                            echo htmlentities(implode(', ',$AreaActListByName), ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_SUBJECT_LABEL' ).$labelseparator; ?></label>
                                <div class="col-md-8">
                                    <p class="form-control-static"><?php echo htmlentities( $this->data->subject, ENT_QUOTES, 'UTF-8');?></p>
                                </div>
                            </div>



                            <?php if (array_key_exists('vdwebsitelist',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                                <?php $getContactUsWebSiteName  = VirtualDeskSiteContactUsHelper::getContactUsWebSiteListNameById($this->data->vdwebsitelist, $fileLangSufix); ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_VDWEBSITELIST_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php echo htmlentities( $getContactUsWebSiteName, ENT_QUOTES, 'UTF-8'); ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (array_key_exists('vdmenumain',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                                <?php $getContactUsVDMenuMain  = VirtualDeskSiteContactUsFieldsHelper::getMenuMainById($this->data->vdmenumain); ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_VDMENUMAIN_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php echo htmlentities( $getContactUsVDMenuMain, ENT_QUOTES, 'UTF-8'); ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (array_key_exists('vdmenusec',$ObjContactUsFields->arConfFieldNames) && ((int)$this->data->category > 1)) : ?>
                                <?php $getContactUsVDMenuSec  = VirtualDeskSiteContactUsFieldsHelper::getMenuSecById($this->data->vdmenusec); ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_VDMENUSEC_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"> <?php echo htmlentities( $getContactUsVDMenuSec, ENT_QUOTES, 'UTF-8'); ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>



                            <?php if ((int)$this->data->category == 1) : ?>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_DESCRIPTION_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo htmlentities( $this->data->description, ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>
                            <?php else : ?>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_DESCRIPTION2_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo htmlentities( $this->data->description2, ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_DESCRIPTION3_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo htmlentities( $this->data->description3, ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo htmlentities( $this->data->description4, ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_OBS_LABEL' ).$labelseparator; ?></label>
                                    <div class="col-md-8">
                                        <p class="form-control-static"><?php echo htmlentities( $this->data->obs, ENT_QUOTES, 'UTF-8');?></p>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php foreach ($ObjContactUsFields->arConfFieldNames as $keyConfFieldNames => $valConfFieldNames)  : ?>

                                <?php if($arContactUsFieldsConfig['ContactUsField_' . $valConfFieldNames ]===true) : ?>

                                    <?php if ($valConfFieldNames == 'TYPE') : ?>

                                    <?php elseif ($valConfFieldNames == 'CATEGORY') : ?>

                                    <?php elseif ($valConfFieldNames == 'SUBCATEGORY') : ?>

                                    <?php elseif ($valConfFieldNames == 'STATUS') : ?>

                                    <?php elseif ($valConfFieldNames == 'CTLANGUAGE') : ?>

                                    <?php elseif ($valConfFieldNames == 'OBS') : ?>

                                    <?php elseif ( $valConfFieldNames == 'ATTACHMENT' ) : ?>

                                        <?php $userReqFileList = VirtualDeskSiteContactUsHelper::getContactUsFilesLayout ($this->data); ?>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_CONTACTUS_' .$valConfFieldNames. '_LABEL' ).$labelseparator; ?></label>
                                            <div class="col-md-8">
                                                <?php require_once( JPATH_SITE . '/components/com_virtualdesk/helpers/html/tpl_contactus_file_list.php'); ?>
                                            </div>
                                        </div>

                                    <?php else : ?>

                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </div>

                    </div>

                </div>



                <div class="form-actions right">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=editcat&contactus_id=' . $this->escape($this->data->contactus_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=contactus&layout=defaultcat&contactus_idcategory=' . $this->escape($this->data->category)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>


                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('contactus_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->contactus_id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('contactus_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('contactus_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>




                <?php
                // Data empty ?
                else :
                    $IdCategoryContactUsGET = JFactory::getApplication()->input->getInt('contactus_idcategory');
                    ?>
                    <div class="form-actions right">
                        <div class="row">




                            <div  class="col-md-12 text-left">
                                <div class="alert alert-info fade in">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
                                    <?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_EMPTYLIST'); ?>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.addnewcat&layout=addnewcat&contactus_idcategory=' . $this->escape($IdCategoryContactUsGET)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_NEW'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_NEW'); ?></a>
                                <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=contactus.cancel&layout=listcat&contactus_idcategory=' . $this->escape($IdCategoryContactUsGET)); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php
                endif;
                ?>


            </form>
        </div>
    </div>


<?php echo $localScripts; ?>