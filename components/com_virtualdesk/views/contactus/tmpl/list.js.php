<?php
defined('_JEXEC') or die;

$objEstadoOptions  = VirtualDeskSiteContactUsHelper::getContactUsEstadoAllOptions($language_tag);

$EstadoOptionsHTML = '';
foreach($objEstadoOptions as $row) {
    $EstadoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}


?>
var TableDatatablesManaged = function () {

    var initTable2 = function () {

        var table = jQuery('#tabela_lista_contactus');
        var tableTools = jQuery('#tabela_lista_contactus_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            //lfrtip
            "dom": '<"wrapper"lf<"EstadoFilterDropBox">rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                    //"targets": [2]
                },

                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);

                        var retVal = '<a href="' + row[4] + '">' + row[0] + '</a>';
                       if(row[6] != row[7]) {
                           retVal += '<span class="iconVDModified"></span>';
                           retVal += '<a href="javascript:;" class="btn btn-sm grey-salsa btn-outline popovers" data-toggle="popover" data-placement="top" data-trigger="hover" ';
                           retVal += ' data-content="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_CREATED_LABEL');?> ' + row[7] + '\n <?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_MODIFIED_LABEL');?> ' + row[6]+'">';
                           retVal += '<i class="fa fa-info"></i></a>';
                       }

                       return (retVal);
                    }
                },

                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);
                        return ('<a href="' + row[4] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        //console.log('row=' + row);
                        return ('<a href="' + row[4] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        // console.log('row8=' + row[8]);
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_CONTACTUS_ESTADONAOINICIADO"); ?>';
                        switch (row[5])
                        {   case '1':
                                defCss = 'label-info';
                                break;
                            case '2':
                                defCss = 'label-success';
                                break;
                            case '3':
                                defCss = 'label-warning';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 4,
                    "data": 4,
                    "orderable": false,
                    "render": function ( data, type, row, meta) {
                        //console.log('data=' + data);
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';

                        return (returnLinki + data + returnLinkf);
                    }
                },
                {
                    "targets": 5,
                    "visible":false
                }


            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=contactus.getDataMainByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // username column
                this.api().column(5).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_CONTACTUS_FILTERBY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option><option value="0"><?php echo JText::_("COM_VIRTUALDESK_CONTACTUS_ESTADONAOINICIADO"); ?></option>';
                        SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                        SelectText += '</select></label>';


                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBox').empty() )
                    jQuery('.EstadoFilterDropBox select').on( 'change', function () {
                            column.search( jQuery(this).val() ).draw();
                        } );
                });
            }


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_contactus_wrapper');

    }


    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            // initTable1();

            initTable2();
        }

    };

}();


jQuery(document).ready(function() {
        TableDatatablesManaged.init();
});