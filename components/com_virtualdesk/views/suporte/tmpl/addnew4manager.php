<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteSuporteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte.php');
JLoader::register('VirtualDeskSiteSuporteFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte_files.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('suporte');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'addnew4managers'); // verifica permissão acesso ao layout para editar
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js' . $addscript_end;
//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');
//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.min.css');

//Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');
// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteSuporteHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.suporte.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


//$UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
//if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
//$UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
//$fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);

//$dataAllUsersList = array();
//$dataAllUsersList = VirtualDeskSiteSuporteHelper::getUsersListAtiveNotBlocked();

$obParam      = new VirtualDeskSiteParamsHelper();

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
    <style>
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
        .fileinput-preview img {display:block; float: none; margin: 0 auto;}
        .select2-bootstrap-prepend .input-group-btn {vertical-align: bottom};
    </style>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            <?php
            // Objecto inicializado com as mensagens de suporte já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
            ?>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <ul class="nav nav-tabs  ">
                    
                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=list4manager#tab_Suporte_Submetidos'); ?>" >
                            <h4>
                                <i class="fa fa-tasks"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_SUPORTE_TAB_SUBMETIDOS'); ?>
                            </h4>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=list4manager#tab_Suporte_Estatistica'); ?>" >
                            <h4>
                                <i class="fa fa-bar-chart"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                            </h4>
                        </a>
                    </li>
                    <li class="active">

                        <a href="#tab_Suporte_Novo" data-toggle="tab">
                            <h4>
                                <i class="fa fa-plus"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_SUPORTE_ADDNEW'); ?>
                            </h4>
                        </a>

                    </li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="tab_Suporte_Novo">

                        <form id="new-suporte"
                              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=suporte.create4manager'); ?>" method="post"
                              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                              role="form">

                            <div class="form-body">

                                <?php
                                /*
                                <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_INFO_UTILIZADORPEDIDO'); ?></h3></legend>


                                <!--   Utilizador -->
                                <div class="form-group ">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_UTILIZADOR_LABEL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">

                                        <select name="utilizador" required value="" id="utilizador" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                            <?php foreach($dataAllUsersList as $rowUser) : ?>
                                                <option value="<?php echo $rowUser->iduser; ?>" ><?php echo $rowUser->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>

                                    </div>
                                </div>
                                */
                                ?>


                                <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_INFO'); ?></h3></legend>

                                <!--   ASSUNTO   -->
                                <div class="form-group" >
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_ASSUNTO_LABEL'); ?><span class="required">*</span></label>
                                    <?php $Assuntos = VirtualDeskSiteSuporteHelper::getAssunto()?>
                                    <div class="col-md-9">
                                        <select name="assunto" required id="assunto" class="form-control select" tabindex="-1" maxlength="40" >
                                            <option value=""></option>
                                            <?php foreach($Assuntos as $rowAsst) : ?>
                                                <option value="<?php echo $rowAsst['assunto']; ?>"
                                                ><?php echo $rowAsst['assunto']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <!--   DESCRIÇAO   -->
                                <div class="form-group" id="desc">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_DESCRICAO_LABEL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                <textarea  class="form-control wysihtml5" rows="6"  required placeholder="<?php echo JText::_('COM_VIRTUALDESK_SUPORTE_DESCRICAO_LABEL'); ?>"
                                           name="descricao" id="descricao" maxlength="250"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao); ?></textarea>
                                    </div>
                                </div>


                                <?php
                                /*
                                <!--   DEPARTAMENTO   -->
                                <div class="form-group" id="departamento">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_DEPARTAMENTO_LABEL'); ?><span class="required">*</span></label>
                                    <?php $departamentos = VirtualDeskSiteSuporteHelper::getDepartamento()?>
                                    <div class="col-md-9">
                                        <select name="departamento" required id="departamento" class="form-control select" tabindex="-1" >
                                                <option value=""></option>
                                                <?php foreach($departamentos as $rowDep) : ?>
                                                    <option value="<?php echo $rowDep['id']; ?>"
                                                    ><?php echo $rowDep['departamento']; ?></option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_LABEL'); ?></label>
                                    <div class="col-md-9">
                                    <textarea  class="form-control wysihtml5" rows="6"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_PLACEHOLDER'); ?>"
                                           name="observacoes"  id="observacoes" ><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($observacoes); ?></textarea>
                                    </div>
                                </div>
                                   */
                                ?>


                                <!--   Upload Docs -->
                                <div class="form-group" id="uploadField">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_DOCS'); ?></label>
                                    <div class="col-md-9">
                                        <div class="file-loading">
                                            <input type="file" name="fileupload[]" id="fileupload" multiple>
                                        </div>
                                        <div id="errorBlock" class="help-block"></div>
                                    </div>
                                </div>

                            </div>


                            <div class="form-actions right">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                        </button>
                                        <a class="btn default"
                                           href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=suporte'); ?>"
                                           title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('suporte.create4manager',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                            <?php echo JHtml::_('form.token'); ?>

                        </form>

                    </div>

                    <div class="tab-pane " id="tab_Suporte_Estatistica">
                    </div>

                    <div class="tab-pane " id="tab_Suporte_Submetidos">
                    </div>

                </div>
            </div>
















        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/suporte/tmpl/addnew4manager.js.php');
echo ('</script>');
?>