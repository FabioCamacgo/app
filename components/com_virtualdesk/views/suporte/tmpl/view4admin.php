<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteSuporteHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte.php');
JLoader::register('VirtualDeskSiteSuporteFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_suporte_files.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailReadAccess('suporte');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('suporte', 'view4admins'); // verifica permissão acesso ao layout para editar
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdmin(); // Verifica se está no grupo Admin ou Admin da APP
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';


$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;


$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;




$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


// Suporte - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/suporte/tmpl/suporte-comum.css');

// Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');

$getInputSuporte_Id = JFactory::getApplication()->input->getInt('suporte_id');

// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSiteSuporteHelper::getSuporteView4AdminDetail($getInputSuporte_Id);

// Carrega de ficheiros associados
if(!empty($this->data->codigo)) {
    //$objSuporteFiles  = new VirtualDeskSiteSuporteFilesHelper();
    //$ListFilesSuporte = $objSuporteFiles->getFileGuestLinkByRefId ($this->data->codigo);
}

$SuporteEstadoList2Change = VirtualDeskSiteSuporteHelper::getSuporteEstadoAllOptions($language_tag);

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

// Dados vazios...
if(empty($this->data)) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
    return false;
}


?>
<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: 50% !important;  -moz-border-radius: 50% !important;  border-radius: 50% !important;   width: 150px; height: auto;}
    .portfolio-content.portfolio-1 .cbp-caption-activeWrap {background-color: rgba(50, 197, 210, 0.5); }
    .portlet.light.bordered>.portlet-title { border-bottom: 1px solid #dcdcdc; }
    .mt-timeline-2>.mt-container>.mt-item>.mt-timeline-content>.mt-content-container { border: 1px solid #d3d7e9; }
    .text-wrap {white-space:normal;}
    .static-info{margin-bottom: 20px;}
    .static-info .name {line-height: 2;}

    .static-info .value {background-color: #f9f9f9; padding: 5px; margin:5px;font-weight: normal; border: 1px solid #e7ecf1;}

</style>

<div class="portlet light bordered form-fit">
   <div class="portlet-title">

        <div class="caption">
            <i class="icon-calendar  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=suporte'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-ban"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CANCEL' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=edit4admin&vdcleanstate=1&suporte_id=' . $this->escape($this->data->suporte_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=addnew4admin&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_ADDNEW' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->

    </div>


    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">

            <div class="form-body">

            <div class="portlet light">




                        <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                            <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_DADOSDETALHE'); ?></div></div>
                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_LISTA_REFERENCIA' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>

                                    <?php
                                    /*
                                    ?>
                                    <div class="col-md-3">
                                        <div class="row static-info">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_DEPARTAMENTO_LABEL' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->departamento, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>
                                    */
                                    ?>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row static-info ">
                                            <div class="col-md-2 name "> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_LISTA_ESTADO' ); ?></div>
                                            <div class="col-md-3 value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=suporte.getNewEstadoName4AdminByAjax&suporte_id='. $this->escape($this->data->suporte_id)); ?>" >
                                                <span class="label <?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>


                                             <?php
                                            $checkAlterarEstado4Admins = $objCheckPerm->checkFunctionAccess('suporte', 'alterarestado4admins');
                                            if($checkAlterarEstado4Admins===true ) : ?>
                                            <div class="col-md-3" style="margin-top: -5px;">
                                                <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                            </div>

                                            <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                        </div>
                                                        <div class="modal-body blocoAlterar2NewEstado">

                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-12 ">
                                                                        <div class="form-group" style="padding:0">
                                                                            <label> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                            <div style="padding:0;">
                                                                                <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                    <?php foreach($SuporteEstadoList2Change as $rowEstado) : ?>
                                                                                        <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 ">
                                                                        <div class="form-group">
                                                                            <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                            <textarea rows="5" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" ></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="form-group blocoIconsMsgAviso">
                                                                <div class="row">
                                                                    <div class="col-md-12 text-center">
                                                                        <span> <i class="fa"></i> </span>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 text-center">
                                                                        <span class="blocoIconsMsgAviso_Texto"></span>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                            <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=suporte.sendAlterar2NewEstado4AdminByAjax&suporte_id=' . $this->escape($this->data->suporte_id)); ?>" >
                                                            <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                            </button>
                                                            <span> <i class="fa"></i> </span>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                                            <?php endif; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row static-info ">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_ASSUNTO_LABEL' ); ?></div>
                                            <div class="value"> <?php echo htmlentities( $this->data->assunto, ENT_QUOTES, 'UTF-8');?> </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row static-info">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_DESCRICAO_LABEL' ); ?></div>
                                            <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao);?></div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                /*
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row static-info">
                                            <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_SUPORTE_OBSERVACOES' ); ?></div>
                                            <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->observacoes); ?></div>
                                        </div>
                                    </div>
                                </div>
                                */
                                ?>


                                <div class="row static-info">
                                    <div class="col-md-12 ">
                                        <h6>
                                            <?php
                                            echo JText::_( 'COM_VIRTUALDESK_SUPORTE_LISTA_DATACRIACAO' ).$labelseparator;
                                            echo htmlentities( $this->data->data_criacao, ENT_QUOTES, 'UTF-8');
                                            if($this->data->data_criacao != $this->data->data_alteracao) echo ' , ' . JText::_( 'COM_VIRTUALDESK_SUPORTE_LISTA_DATAALTERACAO' ) . ' ' . htmlentities( $this->data->data_alteracao, ENT_QUOTES, 'UTF-8');
                                            ?>
                                        </h6>
                                    </div>
                                </div>



                                <div class="portlet light bg-inverse bordered">
                                    <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_SUPORTE_DOCS'); ?></div></div>
                                    <div class="portlet-body">


                                        <div class="form-group" id="uploadField">

                                            <div class="col-md-9">

                                                <div class="file-loading">
                                                    <input type="file" name="fileupload" id="fileupload" >
                                                </div>
                                                <div id="errorBlock" class="help-block"></div>
                                            </div>
                                            <input type="hidden" name="vdFileUpChanged" value="0">
                                        </div>

                                    </div>
                                </div>







                            </div>
                        </div>




            </div>

            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=suporte&layout=edit4admin&suporte_id=' . $this->escape($this->data->suporte_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=suporte'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('suporte_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->suporte_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('suporte_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('suporte_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

        </form>
    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/suporte/tmpl/view4admin.js.php');
echo ('</script>');
?>