<?php
defined('_JEXEC') or die;

$objEstadoOptions  = VirtualDeskSiteSuporteHelper::getSuporteEstadoAllOptions($language_tag);

$EstadoOptionsHTML = '';
foreach($objEstadoOptions as $row) {
    $EstadoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}

/*
$objDepartOptions  = VirtualDeskSiteSuporteHelper::getDepartamentoAllOptions();
$DepartOptionsHTML = '';
foreach($objDepartOptions as $rowD) {
    $DepartOptionsHTML .= '<option value="' . $rowD['id'] . '">' . $rowD['name'] . '</option>';
}
*/

?>

var ChartsAmcharts = function() {

    var initChartSuporteTotPorAnoMes = function() {
        var chart = AmCharts.makeChart("chartSuporteTotPorAnoMes", {
            "type": "serial",
            "theme": "light",
            "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,

            "fontFamily": 'Open Sans',
            "color":    '#888',

            "dataProvider": [
                <?php foreach ($dataSuporteTotPorAnoMes as $row) : ?>
                {
                    "year":  '<?php echo $row->year .' / '. $row->mes ; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>


            ],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
            "startEffect": "easeOutSine", // easeOutSine, easeInSine, elastic, bounce
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] em [[category]]:<b>[[value]]</b> [[additional]]</span>",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Nº de Pedidos",
                "type": "column",
                "valueField": "npedidos"
            }],
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
        });

        jQuery('#chartSuporteTotPorAnoMes').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSuporteTotaisPorEstado = function() {
        var chart = AmCharts.makeChart("chartSuporteTotaisPorEstado", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [
                <?php foreach ($dataSuporteTotaisPorEstado as $row) : ?>
                {
                    "estado":  '<?php echo $row->estado; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>
            ],
            "startEffect": "easeOutSine", // easeOutSine, easeInSine, elastic, bounce
            "startRadius":"90%",
            "valueField": "npedidos",
            "titleField": "estado",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        jQuery('#chartSuporteTotaisPorEstado').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    var initChartSuporteTotaisPorCategoria = function() {
        var chart = AmCharts.makeChart("chartSuporteTotaisPorCategoria", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [
                <?php foreach ($dataSuporteTotaisPorCategoria as $row) : ?>
                {
                    "categoria":  '<?php echo $row->categoria; ?>',
                    "npedidos": <?php echo $row->npedidos ; ?>
                },
                <?php endforeach; ?>
            ],
            "startAnimation": 1,
            "startEffect": "easeInSine", // easeOutSine, easeInSine, elastic, bounce
            "valueField": "npedidos",
            "titleField": "categoria",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        jQuery('#chartSuporteTotaisPorCategoria').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }


    return {
        //main function to initiate the module

        init: function() {

            initChartSuporteTotaisPorEstado();

            initChartSuporteTotaisPorCategoria();

            initChartSuporteTotPorAnoMes();

        }
    };
}();


var TableDatatablesManaged = function () {

    var initTableSuporte = function () {

        var table = jQuery('#tabela_lista_suporte');
        var tableTools = jQuery('#tabela_lista_suporte_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip <"DepartFilterDropBoxSuporte">
            "dom": '<"wrapper"lf <"EstadoFilterDropBoxSuporte">  rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [10, 20, 30, -1],
                [10, 20, 30, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                        var retVal = '<a href="' + row[5] + '">' + data + '</a>';
                        if(row[8] != row[9]) {
                            retVal += '<span class="iconVDModified"></span>';
                            retVal += '<a href="javascript:;" class="btn btn-sm grey-salsa btn-outline popovers" data-toggle="popover" data-placement="top" data-trigger="hover" ';
                            retVal += ' data-content="<?php echo JText::_('COM_VIRTUALDESK_SUPORTE_LISTA_DATACRIACAO');?> ' + row[9] + '\n <?php echo JText::_('COM_VIRTUALDESK_SUPORTE_LISTA_DATAALTERACAO');?> ' + row[6]+'">';
                            retVal += '<i class="fa fa-info"></i></a>';
                        }
                        return (retVal);
                    }
                },
                {
                    "targets": 1,
                    "data": 1,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[5] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[5] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "visible":false
                },
                {
                    "targets": 4,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_SUPORTE_ESTADONAOINICIADO"); ?>';

                        switch (row[6])
                        {   case '1':
                                defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(1);?>';
                                break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(2);?>';
                                break;
                            case '3':
                                defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(3);?>';
                                break;
                            case '4':
                                defCss = '<?php echo VirtualDeskSiteSuporteHelper::getSuporteEstadoCSS(4);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 5,
                    "data": 5,
                    "orderable": false,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + row[5] + returnLinkf);
                    }
                },
                {
                    "targets": 6,
                    "visible":false
                },
                {
                    "targets": 7,
                    "visible":false
                },
                {
                    "targets": 8,
                    "visible":false
                }
                ,
                {
                    "targets": 9,
                    "visible":false
                }
                ,
                {
                    "targets": 10,
                    "visible":false
                }
                ,
                {
                    "targets": 11,
                    "visible":false
                }
            ],
            "order": [
                [0, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=suporte.getSuporteList4adminByAjax",
            "drawCallback": function( settings ) {
                //console.log( 'DataTables has redrawn the table' );
                table.find('[data-toggle="popover"]').popover();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // Filtrar ESTADO
                this.api().column(6).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_SUPORTE_FILTERBY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                    SelectText += '</select></label>';


                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBoxSuporte').empty() )
                    jQuery('.EstadoFilterDropBoxSuporte select').on( 'change', function () {
                        column.search( jQuery(this).val() ).draw();
                    } );
                });

                <?php
                /*
                // Filtrar DEPARTAMENTO
                this.api().column(11).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_SUPORTE_FILTERBY"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($DepartOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.DepartFilterDropBoxSuporte').empty() )
                    jQuery('.DepartFilterDropBoxSuporte select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });
                */
                ?>
            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_suporte_wrapper');

    }




    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableSuporte();

        }

    };

}();


jQuery(document).ready(function() {

    ChartsAmcharts.init();

    TableDatatablesManaged.init();
});