<?php
defined('_JEXEC') or die;

$objSuporteFiles = new VirtualDeskSiteSuporteFilesHelper();
$vdPreFileUploderLista = $objSuporteFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

// Se o estado não for o inicial disable o botão editar
$jsIdEstadoInicial = (int) VirtualDeskSiteSuporteHelper::getEstadoIdInicio();
$jsIdEstadoAtual   = (int)$this->data->idestado;

$jsDesligarBotaoEditar = true;
if($jsIdEstadoInicial == $jsIdEstadoAtual && $jsIdEstadoAtual>0) $jsDesligarBotaoEditar = false;

?>


var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 6,

            // if null - has no limits
            // example: 3
            fileMaxSize: 6,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png','pdf'],

            enableApi: true,

            addMore: false,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                '<ul class="fileuploader-items-list">' +
                /* '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' + */
                '</ul>' +
                '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                /* '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' + */
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                /* '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' + */
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: null,
                onItemRemove: null
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploderLista)) echo(', files:  '.$vdPreFileUploderLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val('1');
                console.log( jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val());
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val('1');
                console.log( jQuery('#uploadField').find('input[name="vdFileUpChanged"]').val());
                return true;
            }


        });

    }

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();


var ButtonHandle = function () {


    var handleButtonAlterarEstadoModal = function (evt) {

        jQuery(".btAlterarEstadoModalOpen").on('click',function(evt,data){
            let setClassSpinner = 'fa-spin fa-spinner fa-4x';
            let setClassSuccess = 'fa-check fa-4x text-success';
            let setClassError   = 'fa-times-circle fa-2x text-danger';

            let elModal        = jQuery("#AlterarNewEstadoModal");
            let elBlocoOptions = elModal.find('div.blocoAlterar2NewEstado');
            let elModalContent = elBlocoOptions.closest('div.modal-content');

            let elBtAlterar = elModalContent.find(".alterar2newestadoSend");
            elBtAlterar.removeAttr('disabled').removeClass('disabled');

            let elNewProcManagerId   = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let elNewProcManagerDesc = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            elNewProcManagerId.removeAttr('disabled').removeClass('disabled');
            elNewProcManagerDesc.removeAttr('disabled').removeClass('disabled');

            let vdClosestI = elModalContent.find('div.blocoIconsMsgAviso').find('span > i.fa');
            let blocoIconsMsgAviso_Texto = elModalContent.find('span.blocoIconsMsgAviso_Texto');

            vdClosestI.removeClass(setClassError).removeClass(setClassSpinner).removeClass(setClassSuccess);
            blocoIconsMsgAviso_Texto.text('');

            elModal.modal('show');
        });
    };


    var handleButtonAlterarEstado = function (evt) {

        jQuery(".alterar2newestadoSend").on('click',function(evt,data){
            let vd_url_send    = jQuery(this).data('vd-url-send');

            let elBlocoOptions = jQuery(this).closest('div.modal-content').find('div.blocoAlterar2NewEstado');

            let elNewEstadoDesc  = elBlocoOptions.find('textarea.Alterar2NewEstadoDesc');
            let setNewEstadoDesc = elNewEstadoDesc.val();

            let elNewEstadoId  = elBlocoOptions.find('select.Alterar2NewEstadoId');
            let setNewEstadoId = elNewEstadoId.val();

            vdAjaxCall.sendAlterar2NewEstado(jQuery(this), vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId );

        });
    };


    return {
        //main function to initiate the module
        init: function () {

            handleButtonAlterarEstadoModal();
            handleButtonAlterarEstado();
        }

    };

}();


var ModalHandle = function () {

    var handleModalAlterarNewEstado = function (evt) {
        jQuery("#AlterarNewEstadoModal").modal({
            show: false,
            keyboard: true
        })
    };



    return {
        //main function to initiate the module
        init: function () {
            handleModalAlterarNewEstado();

        }
    };
}();


var vdAjaxCall = function () {

// Gravar novo estado
    var sendAlterar2NewEstado = function (el, vd_url_send, elNewEstadoDesc, elNewEstadoId, setNewEstadoDesc, setNewEstadoId) {
        let setClassSpinner = 'fa-spin fa-spinner fa-4x';
        let setClassSuccess = 'fa-check fa-4x text-success';
        let setClassError   = 'fa-times-circle fa-4x text-danger';
        el.attr('disabled','disabled').addClass('disabled');
        elNewEstadoDesc.attr('disabled','disabled').addClass('disabled');
        elNewEstadoId.attr('disabled','disabled').addClass('disabled');
        let vdClosestI = el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span > i.fa');
        vdClosestI.removeClass(setClassError).addClass(setClassSpinner);

        setNewEstadoDesc =  encodeURIComponent(setNewEstadoDesc);

        jQuery.ajax({
            url: vd_url_send,
            type: "POST",
            data:'setNewEstadoDesc=' + setNewEstadoDesc + '&setNewEstadoId=' + setNewEstadoId  ,
            indexValue: {el:el, vd_url_send:vd_url_send, elNewEstadoId:elNewEstadoId, elNewEstadoDesc:elNewEstadoDesc, setClassSpinner:setClassSpinner, setClassSuccess:setClassSuccess, vdClosestI:vdClosestI, setClassError:setClassError },
            success: function(data){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassSuccess);

                setTimeout(
                    function()
                    {
                        jQuery("#AlterarNewEstadoModal").modal('hide');
                        vdClosestI.removeClass(setClassSuccess);

                        vdAjaxCall.setNewEstadoStaticVal();

                        el.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');
                        elNewEstadoDesc.val('')
                    }, 800);

            },
            error: function(error){
                vdClosestI.removeClass(setClassSpinner).addClass(setClassError);

                el.removeAttr('disabled').removeClass('disabled');
                elNewEstadoId.removeAttr('disabled').removeClass('disabled');
                elNewEstadoDesc.removeAttr('disabled').removeClass('disabled');

                var objResponseText = JSON.parse(error.responseText);
                if(objResponseText.message!='') {
                    let blocoIconsMsgAviso_Texto =el.closest('div.modal-content').find('div.blocoIconsMsgAviso').find('span.blocoIconsMsgAviso_Texto');
                    blocoIconsMsgAviso_Texto.text(objResponseText.message);
                }
            }
        });
    };

// obter o valor estático do novo estado
    var setNewEstadoStaticVal = function () {
        let elEstadoStatic = jQuery('.ValorEstadoAtualStatic');
        let vd_url_get = elEstadoStatic .data('vd-url-get');

        jQuery.ajax({
            url: vd_url_get,
            type: "POST",
            indexValue: {elEstadoStatic:elEstadoStatic, vd_url_get:vd_url_get},
            success: function(data){
                var d = JSON.parse(data);
                elEstadoStatic.find('span').fadeOut().html(d.name).removeClass().addClass('label ' + d.cssClass).fadeIn();

            },
            error: function(error){

            }
        });
    };

    return {
        sendAlterar2NewEstado   : sendAlterar2NewEstado,
        setNewEstadoStaticVal   : setNewEstadoStaticVal
    };

}();




jQuery(document).ready(function() {

    ButtonHandle.init();

    ModalHandle.init();

    ComponentFileUploader.init();

    // Prevent default submit by Enter Key
    jQuery("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

    <?php if($jsDesligarBotaoEditar===true) : ?>
    jQuery(".vdBotaoEditar").attr('disabled','disabled');
    jQuery("a.vdBotaoEditar").attr('href','').attr('onclick','return false;');
    <?php endif; ?>


});