<?php
defined('_JEXEC') or die;
?>

var LicenciamentoRecintoImprovisadoNew = function() {

    var handleLicenciamentoRecintoImprovisadoNew = function() {

        jQuery('#new-licenciamentorecintoimprovisado').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#new-licenciamentorecintoimprovisado').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#new-licenciamentorecintoimprovisado').validate().form()) {
                    jQuery('#new-licenciamentorecintoimprovisado').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleLicenciamentoRecintoImprovisadoNew();
        }
    };

}();

var ComponentsSelect2 = function() {

    var handleDemo = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var ComponentsEditors = function () {

    var handleWysihtml5 = function () {
        let  ckconfig_removeButtons = 'Subscript,Superscript,Anchor,SpecialChar,Image,About,Scayt';
        let  ckconfig_toolbarGroups =   [
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'others', groups: [ 'others' ] },
            { name: 'colors', groups: [ 'colors' ] }
        ];

        CKEDITOR.replace( 'fieldPeriodoFuncionamento', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'fieldHorarios', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'fieldObservacoes', {
            toolbarGroups: ckconfig_toolbarGroups,
            removeButtons: ckconfig_removeButtons
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();

        }
    };

}();

jQuery(document).ready(function() {

    LicenciamentoRecintoImprovisadoNew.init();

    ComponentsSelect2.init();

    ComponentsEditors.init();

    document.getElementById('cc').onclick = function() {
        document.getElementById("fieldDocId").value = 1;
    };

    document.getElementById('passaporte').onclick = function() {
        document.getElementById("fieldDocId").value = 2;
    };

    document.getElementById('ccRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 1;
    };

    document.getElementById('passaporteRep').onclick = function() {
        document.getElementById("fieldDocIdRep").value = 2;
    };

    document.getElementById('repLegalRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 1;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('gestNegRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 2;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('mandatRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 3;
        document.getElementById("hideFieldOutraQualRep").style.display = "none";
    };

    document.getElementById('outraRep').onclick = function() {
        document.getElementById("fieldQualidadeRep").value = 4;
        document.getElementById("hideFieldOutraQualRep").style.display = "block";
    };

    document.getElementById('tendas').onclick = function() {
        document.getElementById("fieldtipopedido").value = 1;
        document.getElementById("hideFieldQualRecinto").style.display = "none";
    };

    document.getElementById('barracoes').onclick = function() {
        document.getElementById("fieldtipopedido").value = 2;
        document.getElementById("hideFieldQualRecinto").style.display = "none";
    };

    document.getElementById('palanques').onclick = function() {
        document.getElementById("fieldtipopedido").value = 3;
        document.getElementById("hideFieldQualRecinto").style.display = "none";
    };

    document.getElementById('estrados').onclick = function() {
        document.getElementById("fieldtipopedido").value = 4;
        document.getElementById("hideFieldQualRecinto").style.display = "none";
    };

    document.getElementById('palcos').onclick = function() {
        document.getElementById("fieldtipopedido").value = 5;
        document.getElementById("hideFieldQualRecinto").style.display = "none";
    };

    document.getElementById('bancadas').onclick = function() {
        document.getElementById("fieldtipopedido").value = 6;
        document.getElementById("hideFieldQualRecinto").style.display = "none";
    };

    document.getElementById('outroRecinto').onclick = function() {
        document.getElementById("fieldtipopedido").value = 7;
        document.getElementById("hideFieldQualRecinto").style.display = "block";
    };

    document.getElementById('sim').onclick = function() {
        document.getElementById("fieldInstalacoesSanitarias").value = 1;
    };

    document.getElementById('nao').onclick = function() {
        document.getElementById("fieldInstalacoesSanitarias").value = 2;
    };

});
