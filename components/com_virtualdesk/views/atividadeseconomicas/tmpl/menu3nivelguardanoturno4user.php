<?php
    /**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteAtividadesEconomicasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_atividadeseconomicas.php');
    JLoader::register('VirtualDeskSiteFormmainHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_formmain.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('atividadeseconomicas', 'list4users');
    if($vbHasAccess===false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');

?>

    <div class="portlet light bordered">

        <div class="portlet-title">
            <div class="caption">
                <i class="icon-pointer font-dark"></i>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas'); ?>"><span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span></a>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_TAB_GUARDANOTURNO'); ?></span>
            </div>

            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>">
                    <i class="fa fa-arrow-left"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
            </div>

        </div>

        <div class="portlet-body ">
            <div class="tabbable-line nav-justified ">
                <div class="tab-content">
                    <div class="row">

                        <?php
                            $Enabled_cessacaoAtividade = VirtualDeskSiteFormmainHelper::checkFormEnabled('Odeq7JgR');
                            $Enabled_criacaoServico = VirtualDeskSiteFormmainHelper::checkFormEnabled('TXNysitG');
                            $Enabled_2ViaLicenca = VirtualDeskSiteFormmainHelper::checkFormEnabled('5CA4jJhW');
                            $Enabled_fixacaoAreaAtuacao = VirtualDeskSiteFormmainHelper::checkFormEnabled('Z7ojjGDe');
                            $Enabled_provaAnual = VirtualDeskSiteFormmainHelper::checkFormEnabled('eBPU2tT2');
                            $Enabled_renovacaoLicenca = VirtualDeskSiteFormmainHelper::checkFormEnabled('AsuUxwwW');
                            $Enabled_candidaturaLicenca = VirtualDeskSiteFormmainHelper::checkFormEnabled('MD158iKM');
                        ?>

                        <?php if($Enabled_cessacaoAtividade == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('Odeq7JgR'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('Odeq7JgR'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas&layout=addnewcessacaoatividade4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_criacaoServico == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('TXNysitG'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('TXNysitG'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas&layout=addnewcriacaoextincaoservico4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_2ViaLicenca == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('5CA4jJhW'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('5CA4jJhW'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas&layout=addnew2vialicencaguardanoturno4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_fixacaoAreaAtuacao == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('Z7ojjGDe'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('Z7ojjGDe'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas&layout=addnewfixacaoareasatuacao4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_provaAnual == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('eBPU2tT2'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('eBPU2tT2'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas&layout=addnewprovaanualcumprimento4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_renovacaoLicenca == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('AsuUxwwW'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('AsuUxwwW'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas&layout=addnewrenovacaolicencaatividade4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if($Enabled_candidaturaLicenca == 1) :?>
                            <div class="col-lg-4">
                                <div class="kt-portlet kt-callout  kt-callout--diagonal-bg">
                                    <div class="kt-portlet__body">
                                        <div class="kt-callout__body">
                                            <div class="kt-callout__content">
                                                <h3 class="kt-callout__title"><?php echo VirtualDeskSiteFormmainHelper::getFormNameByTag('MD158iKM'); ?></h3>
                                                <p class="kt-callout__desc">
                                                    <?php echo VirtualDeskSiteFormmainHelper::getRefTag('MD158iKM'); ?>
                                                </p>
                                            </div>
                                            <div class="kt-callout__action">
                                                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=atividadeseconomicas&layout=addnewcandidaturalicenca4user'); ?>" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success"><?php echo JText::_('COM_VIRTUALDESK_ATIVIDADESECONOMICAS_MENU_ACEDER'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>



<?php
echo $localScripts;
?>