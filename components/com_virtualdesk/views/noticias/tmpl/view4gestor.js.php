<?php
defined('_JEXEC') or die;
?>

var PortofolioHandle = function () {

    var initNoticiasFileGridCapa = function (evt) {
        // init cubeportfolio
        jQuery('#vdNoticiasFileGridCapa').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyNoticiasFileGridCapa = function (evt) {
        // init cubeportfolio
        jQuery('#vdNoticiasFileGridCapa').cubeportfolio('destroy');
    };


    var initNoticiasFileGridGaleria = function (evt) {
        // init cubeportfolio
        jQuery('#vdNoticiasFileGridGaleria').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyNoticiasFileGridGaleria = function (evt) {
        // init cubeportfolio
        jQuery('#vdNoticiasFileGridGaleria').cubeportfolio('destroy');
    };

    return {
        //main function to initiate the module
        init: function () {
            initNoticiasFileGridCapa();
            initNoticiasFileGridGaleria();
        },
        initNoticiasFileGridCapa    :  initNoticiasFileGridCapa,
        destroyNoticiasFileGridCapa  : destroyNoticiasFileGridCapa,
        initNoticiasFileGridGaleria  : initNoticiasFileGridGaleria,
        destroyNoticiasFileGridGaleria  : destroyNoticiasFileGridGaleria

    };
}();

jQuery(document).ready(function() {

    PortofolioHandle.init();

});
