<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteNoticiasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias.php');
    JLoader::register('VirtualDeskSiteNoticiasFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias_fotoCapa_files.php');
    JLoader::register('VirtualDeskSiteNoticiasGaleriaHelper', JPATH_SITE . '/virtualdesksite_noticias_galeria_files');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('noticias');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('noticias', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/noticias/tmpl/noticias.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputNoticia_Id = JFactory::getApplication()->input->getInt('noticia_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteNoticiasHelper::getNoticiasDetail4Manager($getInputNoticia_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
        $ListFilesAlert = array();

        $objFotoCapaFiles = new VirtualDeskSiteNoticiasFilesHelper();
        $ListFilesFotoCapa = $objFotoCapaFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objGaleriaFiles = new VirtualDeskSiteNoticiasGaleriaHelper();
        $ListFilesGaleria = $objGaleriaFiles->getFileGuestLinkByRefId ($this->data->codigo);

    }

    $NoticiasEstadoList2Change = VirtualDeskSiteNoticiasHelper::getEstadoAllOptions($language_tag);

    // Todo Ter parametro com o Id de Menu Principal de cada módulo
    //$itemmenuid_lista = $params->get('agenda_menuitemid_list');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
        return false;
    }

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_VIEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=edit4manager&noticia_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <button id="duplicar" class="btn btn-circle btn-outline yellow-gold"> <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_DUPLICAR'); ?> </button>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_NOVO' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">

                <div class="form-body">

                    <div class="portlet light">

                        <div class="col-md-8">

                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend>
                                            <h3><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_DADOSNOTICIA'); ?></h3>
                                        </legend>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half state">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_ESTADO' ); ?></div>
                                                <div class="value">
                                                    <span class="label <?php echo VirtualDeskSiteNoticiasHelper::getEstadoCSS($this->data->id_estado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_TITULO' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->titulo, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all multiline">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_DESCRICAO' ); ?></div>
                                                <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao); ?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_CATEGORIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php
                                            $dataPub = explode('-', $this->data->data_publicacao);
                                            $dataPublicacao = $dataPub[2] . '-' . $dataPub[1] . '-' . $dataPub[0];
                                        ?>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_DATAPUBLICACAO' ); ?></div>
                                                <div class="value"> <?php echo htmlentities($dataPublicacao, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php
                                            $urlHttps = explode("https://", $this->data->facebook);
                                            if(count($urlHttps) == 1){
                                                $hasLink = 0;
                                                $urlHttp = explode("http://", $this->data->facebook);
                                                if(count($urlHttp) == 1){
                                                    $hasLink = 0;
                                                } else {
                                                    $hasLink = 1;
                                                }
                                            } else {
                                                $hasLink = 1;
                                            }

                                            if(!empty($this->data->facebook)){
                                                if($hasLink == 0){
                                                    $url = 'https://' . $this->data->facebook;
                                                } else {
                                                    $url = $this->data->facebook;
                                                }
                                            } else {
                                                $url = '';
                                            }

                                        ?>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_FACEBOOK' ); ?></div>
                                                <div class="value"> <a href="<?php echo $url;?>" target="_blank"><?php echo $url;?></a> </div>
                                            </div>
                                        </div>



                                        <?php
                                            $urlHttps = explode("https://", $this->data->video);
                                            if(count($urlHttps) == 1){
                                                $hasLink = 0;
                                                $urlHttp = explode("http://", $this->data->video);
                                                if(count($urlHttp) == 1){
                                                    $hasLink = 0;
                                                } else {
                                                    $hasLink = 1;
                                                }
                                            } else {
                                                $hasLink = 1;
                                            }

                                            if(!empty($this->data->video)){
                                                if($hasLink == 0){
                                                    $urlVideo = 'https://' . $this->data->video;
                                                } else {
                                                    $urlVideo = $this->data->video;
                                                }
                                            } else {
                                                $urlVideo = '';
                                            }
                                        ?>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_NOTICIAS_VIDEO' ); ?></div>
                                                <div class="value"> <a href="<?php echo $urlVideo;?>" target="_blank"><?php echo $urlVideo;?></a> </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        if((int)$ListFilesGaleria != 0) {
                                            ?>
                                                <div class="bloco">

                                                <legend>
                                                    <h3><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_GALERIA'); ?></h3>
                                                </legend>

                                                <div id="vdNoticiasFileGridGaleria" class="cbp">
                                                    <?php
                                                    if(!is_array($ListFilesGaleria)) $ListFilesGaleria = array();
                                                    foreach ($ListFilesGaleria as $rowFile) : ?>
                                                        <div class="cbp-item identity logos">
                                                            <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                                <div class="cbp-caption-defaultWrap">
                                                                    <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                                <div class="cbp-caption-activeWrap">
                                                                    <div class="cbp-l-caption-alignLeft">
                                                                        <div class="cbp-l-caption-body">
                                                                            <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                            <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    <?php endforeach;?>
                                                </div>


                                            </div>
                                            <?php
                                        }
                                    ?>

                                </div>

                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_PERCURSOSPEDESTRES_FOTOCAPA'); ?></h3></legend>

                                        <div id="vdNoticiasFileGridCapa" class="cbp">
                                            <?php
                                            if(!is_array($ListFilesFotoCapa)) $ListFilesFotoCapa = array();
                                            foreach ($ListFilesFotoCapa as $rowFile) : ?>
                                                <div class="cbp-item identity logos">
                                                    <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                        <div class="cbp-caption-defaultWrap">
                                                            <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                        <div class="cbp-caption-activeWrap">
                                                            <div class="cbp-l-caption-alignLeft">
                                                                <div class="cbp-l-caption-body">
                                                                    <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                    <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=edit4manager&noticia_id=' . $this->escape($this->data->id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=list4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('noticia_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->noticia_id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/noticias/tmpl/view4manager.js.php');
    echo ('</script>');
?>