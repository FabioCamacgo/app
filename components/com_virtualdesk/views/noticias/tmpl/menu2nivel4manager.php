<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);


    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteNoticiasHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/noticias/virtualdesksite_noticias.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess = $objCheckPerm->checkLayoutAccess('noticias', 'list4managers');
    $vbInGroupAM = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/css/kt.style.bundle.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/noticias/tmpl/noticias.css');

?>

<div class="portlet light bordered">


    <div class="portlet-title">
        <div class="caption">
            <i class="icon-briefcase font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>

        <div class="actions">

            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>

        </div>

    </div>

    <div class="portlet-body ">

        <div class="tab-content">

            <div class="tab-pane" style="display:block;">
                <div class="row">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=list4manager&vdcleanstate=1'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_NOTICIAS'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS'); ?></h3>
                            </div>
                        </div>
                    </a>

                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=noticias&layout=listcat4manager&vdcleanstate=1'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_CATEGORIAS'); ?>">
                        <div class="col-md-3">
                            <div class="portlet light bg-inverse bordered">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_NOTICIAS_CATEGORIAS'); ?></h3>
                            </div>
                        </div>
                    </a>
                </div>

            </div>

        </div>

    </div>
</div>



<?php

echo $localScripts;

?>


