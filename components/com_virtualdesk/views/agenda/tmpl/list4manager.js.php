<?php
defined('_JEXEC') or die;

$objEstadoOptions  = VirtualDeskSiteAgendaHelper::getAgendaEstadoAllOptions($language_tag);
$objCatOptions     = VirtualDeskSiteAgendaHelper::getAgendaCategoriasAllOptions();
$objConcOptions     = VirtualDeskSiteAgendaHelper::getAgendaConcelhosAllOptions();

$EstadoOptionsHTML = '';
foreach($objEstadoOptions as $row) {
    $EstadoOptionsHTML .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
}

$CatOptionsHTML = '';
foreach($objCatOptions as $rowC) {
    $CatOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

$ConcOptionsHTML = '';
foreach($objConcOptions as $rowC) {
    $ConcOptionsHTML .= '<option value="' . $rowC['id'] . '">' . $rowC['name'] . '</option>';
}

?>

var TableDatatablesManaged = function () {

    var initTableAgenda = function () {

        var table = jQuery('#tabela_lista_agenda');
        var tableTools = jQuery('#tabela_lista_agenda_tools');

        // begin first table
        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTASC'); ?>",
                    "sortDescending": ": <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SORTDESC'); ?> "
                },
                "processing": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "emptyTable": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "info": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOWING'); ?> _START_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_TO'); ?> _END_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_OF'); ?> _TOTAL_ <?php echo JText::_('COM_VIRTUALDESK_DATATABLES_RECORDS'); ?>",
                "infoEmpty": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SHOW'); ?> _MENU_",
                "search": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_SEARCH'); ?>:",
                "zeroRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NOTFOUND'); ?>",
                "loadingRecords": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LOADING'); ?>",
                "paginate": {
                    "previous":"<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_PREV'); ?>",
                    "next": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_NEXT'); ?>",
                    "last": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_LAST'); ?>",
                    "first": "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_FIRST'); ?>"
                }
            },

            //lfrtip
            "dom": '<"wrapper"lf <"EstadoFilterDropBoxAgenda"> <"CatFilterDropBoxAgenda"> <"ConcFilterDropBoxAgenda"> rtip>',

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
                [50, 100, 150, -1],
                [50, 100, 150, "<?php echo JText::_('COM_VIRTUALDESK_DATATABLES_ALL'); ?>"] // change per page values here
            ],
            // set the initial value
            "pageLength": 50,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-left",
                },
                {
                    "targets": 0,
                    "data": 0,
                    "render": function ( data, type, row, meta) {
                       var retVal = '<a href="' + row[4] + '">' + row[0] + '</a>';
                       return (retVal);
                    }
                },
                {
                    "targets": 1,
                    "data": 9,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[4] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 2,
                    "data": 2,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[4] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 3,
                    "data": 4,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[4] + '">' + row[1] + '</a>');
                    }
                },
                {
                    "targets": 4,
                    "data": 14,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        let currState = '';
                        if(data=='1') {
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledPlugin_' + meta.row + '"' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-pluginenable="' + row[15] + '" ';
                        ret += ' data-vd-url-checkpluginenable="' + row[16] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                    //    ret='';

                        return (ret);
                    }
                },
                {
                    "targets": 5,
                    "data": 17,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        let currState = '';
                        if(data=='1') {
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledPremiumPlugin_' + meta.row + '"' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-pluginenable-premium="' + row[18] + '" ';
                        ret += ' data-vd-url-checkpluginenable-premium="' + row[19] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        return (ret);
                    }
                },
                {
                    "targets": 6,
                    "data": 20,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        let currState = '';
                        if(data=='1') {
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledagendamunicipalPlugin_' + meta.row + '"' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-pluginenable-agendamunicipal="' + row[21] + '" ';
                        ret += ' data-vd-url-checkpluginenable-agendamunicipal="' + row[22] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        return (ret);
                    }
                },
                {
                    "targets": 7,
                    "data": 23,
                    "render": function ( data, type, row, meta) {
                        let ret = '';
                        let currState = '';
                        if(data=='1') {
                            currState = 'checked';
                        }

                        ret +=' <input type="checkbox" name="enabledmesamesPlugin_' + meta.row + '"' + currState +' class="make-switch form-control" value="1" data-on-color="success" data-off-color="danger" data-size="small" ';
                        ret += ' data-vd-url-pluginenable-mesames="' + row[24] + '" ';
                        ret += ' data-vd-url-checkpluginenable-mesames="' + row[25] + '" ';
                        ret += ' /><span><i class="fa"></i></span>';

                        return (ret);
                    }
                },
                {
                    "targets": 8,
                    "data": 26,
                    "render": function ( data, type, row, meta) {
                        return ('<a href="' + row[4] + '">' + data + '</a>');
                    }
                },
                {
                    "targets": 9,
                    "data": 3,
                    "render": function ( data, type, row, meta) {
                        var defCss = 'label-default';
                        if(data==' ') data = '<?php echo JText::_("COM_VIRTUALDESK_AGENDA_ESTADONAOINICIADO"); ?>';

                        switch (row[5])
                        {   case '1':
                                defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(1);?>';
                                break;
                            case '2':
                                defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(2);?>';
                                break;
                            case '3':
                                defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(3);?>';
                                break;
                            case '4':
                                defCss = '<?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS(4);?>';
                                break;
                        }
                        var retVal = '<span class="label '+ defCss +'">'+data+'</span>';
                        return (retVal);
                    }
                },
                {
                    "targets": 10,
                    "data": 5,
                    "orderable": false,
                    "visible":false,
                    "render": function ( data, type, row, meta) {
                        var returnLinki = '<a href="';
                        var returnLinkf = '" class=" btn btn-sm grey-salsa btn-outline sbold uppercase"> <i class="fa fa-share"></i> View </a>';
                        return (returnLinki + row[4] + returnLinkf);
                    }
                },
                {
                    "targets": 11,
                    "visible":false
                },
                {
                    "targets": 12,
                    "visible":false
                },
                {
                    "targets": 13,
                    "visible":false
                }
                ,
                {
                    "targets": 14,
                    "visible":false
                }
                ,
                {
                    "targets": 15,
                    "visible":false
                }
                ,
                {
                    "targets": 16,
                    "visible":false
                }
                ,
                {
                    "targets": 17,
                    "visible":false
                }
                ,
                {
                    "targets": 18,
                    "visible":false
                }
                ,
                {
                    "targets": 19,
                    "visible":false
                }
                ,
                {
                    "targets": 20,
                    "visible":false
                }
                ,
                {
                    "targets": 21,
                    "visible":false
                }
                ,
                {
                    "targets": 22,
                    "visible":false
                }
            ],
            "order": [
                [0, "desc"],
                [10, "desc"]
            ], // set first column as a default sort by asc

            "processing": true,
            "serverSide": true,
            "ajax": "?option=com_virtualdesk&task=agenda.getAgendaList4ManagerByAjax",
            "drawCallback": function( settings ) {
                table.find('[data-toggle="popover"]').popover();

                UISwitch.initSwitchPlugin();
                UISwitch.initSwitchPluginPremium();
                UISwitch.initSwitchPluginagendamunicipal();
                UISwitch.initSwitchPluginmesames();
            },

            "buttons": [
                {
                    extend: 'print',
                    autoPrint: false,
                    exportOptions: { columns:[0,1,2,3] },
                    customize: function ( win ) {
                        jQuery(win.document.body).css( 'background-color', 'transparent' );
                    }
                },
                { extend: 'copy', className: 'btn red btn-outline' , exportOptions: { columns:[0,1,2,3] } },
                { extend: 'pdf', className: 'btn green btn-outline', orientation: 'landscape',  pageSize: 'A4', exportOptions: { columns:[0,1,2,3] } },
                { extend: 'excel', className: 'btn yellow btn-outline ', exportOptions: { columns:[0,1,2,3] } }

            ],

            initComplete: function () {
                // username column
                this.api().column(10).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_AGENDA_FILTERBY_ESTADO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                        SelectText += '<?php echo ($EstadoOptionsHTML); ?>';
                        SelectText += '</select></label>';


                    var select = jQuery(SelectText).appendTo(jQuery('.EstadoFilterDropBoxAgenda').empty() )
                    jQuery('.EstadoFilterDropBoxAgenda select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar CATEGORIA
                this.api().column(11).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_AGENDA_FILTERBY_CATEGORIA"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($CatOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.CatFilterDropBoxAgenda').empty() )
                    jQuery('.CatFilterDropBoxAgenda select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });

                // Filtrar CONCELHO
                this.api().column(13).every(function(){
                    var column = this;

                    var SelectText  = '<label><?php echo JText::_("COM_VIRTUALDESK_AGENDA_FILTERBY_CONCELHO"); ?>  <select  class="form-control input-sm input-inline"><option value=""> </option>';
                    SelectText += '<?php echo ($ConcOptionsHTML); ?>';
                    SelectText += '</select></label>';

                    var select = jQuery(SelectText).appendTo(jQuery('.ConcFilterDropBoxAgenda').empty() )
                    jQuery('.ConcFilterDropBoxAgenda select').on( 'change', function () {
                        var SearchTerm = jQuery(this).val() ;
                        if(SearchTerm!='' && SearchTerm!=undefined) SearchTerm ='^'+SearchTerm+'$';
                        column.search( SearchTerm, true, false ).draw();
                    } );
                });
            }

            // O pârametro na tabela é importante para o resize e tem de estar a FALSE -> "autoWidth": false
            // Caso contrário o mdatatabgle não se coloca no tamanho certo do ecrã
            ,"autoWidth": false


        });

        // handle datatable custom tools
        tableTools.find('li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            oTable.DataTable().button(action).trigger();
        });


        var tableWrapper = jQuery('#tabela_lista_agenda_wrapper');

    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTableAgenda();

        }

    };

}();


var UISwitch = function () {

    var initSwitchPlugin =  function (evt) {
        jQuery("input[name^='enabledPlugin_']").bootstrapSwitch();
    };
    var handleSwitchPlugin = function (evt) {

        jQuery("input[name^='enabledPlugin_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);

            let vd_url_pluginenable = jQuery(this).data('vd-url-pluginenable');
            let vd_url_checkpluginenable = jQuery(this).data('vd-url-checkpluginenable');
            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setPluginEnable(jQuery(this), vd_url_pluginenable, vd_url_checkpluginenable , enableVal);
        });
    };


    var initSwitchPluginPremium =  function (evt) {
        jQuery("input[name^='enabledPremiumPlugin_']").bootstrapSwitch();
    };
    var handleSwitchPluginPremium = function (evt) {

        jQuery("input[name^='enabledPremiumPlugin_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);
            //console.log('clicked ' + jQuery(this).attr('name') );
            let vd_url_pluginenablePremium = jQuery(this).data('vd-url-pluginenable-premium');
            let vd_url_checkpluginenablePremium = jQuery(this).data('vd-url-checkpluginenable-premium');

            console.log('vd_url_pluginenablePremium ' + vd_url_pluginenablePremium );
            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setPluginEnablePremium(jQuery(this), vd_url_pluginenablePremium, vd_url_checkpluginenablePremium , enableVal);
        });
    };


    var initSwitchPluginagendamunicipal =  function (evt) {
        jQuery("input[name^='enabledagendamunicipalPlugin_']").bootstrapSwitch();
    };
    var handleSwitchPluginagendamunicipal = function (evt) {

        jQuery("input[name^='enabledagendamunicipalPlugin_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);
            //console.log('clicked ' + jQuery(this).attr('name') );
            let vd_url_pluginenableagendamunicipal = jQuery(this).data('vd-url-pluginenable-agendamunicipal');
            let vd_url_checkpluginenableagendamunicipal = jQuery(this).data('vd-url-checkpluginenable-agendamunicipal');

            console.log('vd_url_pluginenableagendamunicipal ' + vd_url_pluginenableagendamunicipal );
            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setPluginEnableagendamunicipal(jQuery(this), vd_url_pluginenableagendamunicipal, vd_url_checkpluginenableagendamunicipal , enableVal);
        });
    };


    var initSwitchPluginmesames =  function (evt) {
        jQuery("input[name^='enabledmesamesPlugin_']").bootstrapSwitch();
    };
    var handleSwitchPluginmesames = function (evt) {

        jQuery("input[name^='enabledmesamesPlugin_']").on('switchChange.bootstrapSwitch',function(evt,data){
            jQuery(this).bootstrapSwitch('disabled',true);
            //console.log('clicked ' + jQuery(this).attr('name') );
            let vd_url_pluginenablemesames = jQuery(this).data('vd-url-pluginenable-mesames');
            let vd_url_checkpluginenablemesames = jQuery(this).data('vd-url-checkpluginenable-mesames');

            console.log('vd_url_pluginenablemesames ' + vd_url_pluginenablemesames );
            let enableVal = 0;
            if(data===true) enableVal= 1;

            vdAjaxCall.setPluginEnablemesames(jQuery(this), vd_url_pluginenablemesames, vd_url_checkpluginenablemesames , enableVal);
        });
    };


    return {
        //main function to initiate the module
        init: function () {
        },
        initSwitchPlugin: function () {
            initSwitchPlugin();
            handleSwitchPlugin();
        },
        initSwitchPluginPremium: function () {
            initSwitchPluginPremium();
            handleSwitchPluginPremium();
        },
        initSwitchPluginagendamunicipal: function () {
            initSwitchPluginagendamunicipal();
            handleSwitchPluginagendamunicipal();
        },
        initSwitchPluginmesames: function () {
            initSwitchPluginmesames();
            handleSwitchPluginmesames();
        }
    };

}();


var vdAjaxCall = function () {

    var setPluginEnable = function (el, vd_url_pluginenable, vd_url_checkpluginenable, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_pluginenable = vd_url_pluginenable + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_pluginenable,
            type: "POST",
            indexValue: {el:el, vd_url_checkpluginenable:vd_url_checkpluginenable},
            success: function(data){

                vdAjaxCall.getPluginEnable(this.indexValue.el, this.indexValue.vd_url_checkpluginenable);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var getPluginEnable = function (el, vd_user_checkpluginenable) {

        jQuery.ajax({
            url: vd_user_checkpluginenable,
            type: "POST",
            indexValue: {el:el},
            success: function(data){

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    //vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var setPluginEnablePremium = function (el, vd_url_pluginenablePremium, vd_url_checkpluginenablePremium, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_pluginenablePremium = vd_url_pluginenablePremium + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_pluginenablePremium,
            type: "POST",
            indexValue: {el:el, vd_url_checkpluginenablePremium:vd_url_checkpluginenablePremium},
            success: function(data){

                vdAjaxCall.getPluginEnablePremium(this.indexValue.el, this.indexValue.vd_url_checkpluginenablePremium);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var getPluginEnablePremium = function (el, vd_user_checkpluginenablePremium) {

        jQuery.ajax({
            url: vd_user_checkpluginenablePremium,
            type: "POST",
            indexValue: {el:el},
            success: function(data){

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    //vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var setPluginEnableagendamunicipal = function (el, vd_url_pluginenableagendamunicipal, vd_url_checkpluginenableagendamunicipal, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_pluginenableagendamunicipal = vd_url_pluginenableagendamunicipal + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_pluginenableagendamunicipal,
            type: "POST",
            indexValue: {el:el, vd_url_checkpluginenableagendamunicipal:vd_url_checkpluginenableagendamunicipal},
            success: function(data){

                vdAjaxCall.getPluginEnableagendamunicipal(this.indexValue.el, this.indexValue.vd_url_checkpluginenableagendamunicipal);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var getPluginEnableagendamunicipal = function (el, vd_user_checkpluginenableagendamunicipal) {

        jQuery.ajax({
            url: vd_user_checkpluginenableagendamunicipal,
            type: "POST",
            indexValue: {el:el},
            success: function(data){

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    //vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var setPluginEnablemesames = function (el, vd_url_pluginenablemesames, vd_url_checkpluginenablemesames, enabledVal) {
        let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
        vdClosestI.addClass("fa-spin fa-spinner fa-lg");

        vd_url_pluginenablemesames = vd_url_pluginenablemesames + '&enabled=' + enabledVal;

        jQuery.ajax({
            url: vd_url_pluginenablemesames,
            type: "POST",
            indexValue: {el:el, vd_url_checkpluginenablemesames:vd_url_checkpluginenablemesames},
            success: function(data){

                vdAjaxCall.getPluginEnablemesames(this.indexValue.el, this.indexValue.vd_url_checkpluginenablemesames);

            },
            error: function(error){
                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    var getPluginEnablemesames = function (el, vd_user_checkpluginenablemesames) {

        jQuery.ajax({
            url: vd_user_checkpluginenablemesames,
            type: "POST",
            indexValue: {el:el},
            success: function(data){

                let vdClosestI = el.parents('.bootstrap-switch-wrapper').parent().find('span > i.fa');
                let currState = el.bootstrapSwitch('state');

                if(data==='1' && currState===true) {
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else if(data==='0' && currState===false){
                    vdClosestI.removeClass('fa-spin fa-spinner fa-lg');
                }
                else {
                    //vdClosestI.parent().html(' ERRO! <i class="fa"></i>');
                }
                vdClosestI.parent().html(data + '<i class="fa"></i>');
                el.bootstrapSwitch('disabled',false);
            }
        });
    };

    return {
        setPluginEnable   : setPluginEnable,
        getPluginEnable   : getPluginEnable,
        setPluginEnablePremium   : setPluginEnablePremium,
        getPluginEnablePremium   : getPluginEnablePremium,
        setPluginEnableagendamunicipal   : setPluginEnableagendamunicipal,
        getPluginEnableagendamunicipal   : getPluginEnableagendamunicipal,
        setPluginEnablemesames   : setPluginEnablemesames,
        getPluginEnablemesames   : getPluginEnablemesames
    };

}();


jQuery(document).ready(function() {
    TableDatatablesManaged.init();
});