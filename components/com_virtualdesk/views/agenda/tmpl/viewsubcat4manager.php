<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'viewsubcat4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');


    // Agenda - CSS Comum
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/agenda-comum.css');

    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputAgenda_Id = JFactory::getApplication()->input->getInt('agenda_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAgendaHelper::getAgendaViewSubCat4ManagerDetail($getInputAgenda_Id);

    $AgendaEstadoList2Change = VirtualDeskSiteAgendaHelper::getAgendaEstadoAllOptions($language_tag);


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
        return false;
    }

?>

    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_VER_DETALHE_SUBCATEGORIA' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=subcatlist4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editsubcat4manager&vdcleanstate=1&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=addnewsubcat4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_ADDNEW_SUBCAT' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->
        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">
                <div class="form-body">
                    <div class="portlet light">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                    <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_VER_DETALHE_SUBCATEGORIA'); ?></div></div>
                                    <div class="portlet-body">

                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_CATEGORIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->cat_PT, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_ESTADO' ); ?></div>
                                                <div class="col-md-6 value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.getNewCatEstadoName4ManagerByAjax&agenda_id='. $this->escape($this->data->agenda_id)); ?>" >
                                                    <span class="label <?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>

                                                <?php
                                                $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers');
                                                if($checkAlterarEstado4Managers===true ) : ?>
                                                    <div class="col-md-6" style="margin-top: -5px;">
                                                        <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                                    </div>

                                                    <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                                </div>
                                                                <div class="modal-body blocoAlterar2NewEstado">


                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-12 ">
                                                                                <div class="form-group" style="padding:0">
                                                                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                                    <div style="padding:0;">
                                                                                        <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                            <?php foreach($AgendaEstadoList2Change as $rowEstado) : ?>
                                                                                                <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                            <?php endforeach; ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 ">
                                                                                <div class="form-group">
                                                                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                                    <textarea  rows="5" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" ></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group blocoIconsMsgAviso">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <span> <i class="fa"></i> </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <span class="blocoIconsMsgAviso_Texto"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                                    <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.sendAlterarSubCat2NewEstado4ManagerByAjax&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" >
                                                                    <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                                    </button>
                                                                    <span> <i class="fa"></i> </span>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>

                                                <?php endif; ?>

                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEPT' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->subcat_PT, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEEN' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->subcat_EN, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEFR' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->subcat_FR, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEDE' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->subcat_DE, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editsubcat4manager&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=subcatlist4manager&vdcleanstate=1'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('agenda_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->agenda_id) ,$setencrypt_forminputhidden); ?>"/>
            </form>
        </div>

    </div>


<?php
    echo $localScripts;

    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/viewsubcat4manager.js.php');
    echo ('</script>');
?>