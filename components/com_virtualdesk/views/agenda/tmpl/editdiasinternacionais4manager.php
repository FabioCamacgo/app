<?php
    /**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editdiainternacional4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/diasinternacionais.css');


    // Parâmetros
    $labelseparator    = ' : ';
    $params            = JComponentHelper::getParams('com_virtualdesk');
    $getDiaInternacional_Id = JFactory::getApplication()->input->getInt('diainternacional_id');


    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAgendaHelper::getDiasInternacionaisDetail4Manager($getDiaInternacional_Id);
    if( empty($this->data) ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
        return false;
    }

    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.editdiasinternacionais4manager.agenda.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EDITAR_DIASINTERNACIONAIS' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewdiasinternacionais4manager&diainternacional_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-diasinternacionais" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.updatediasinternacionais4manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                <div class="form-body">

                    <input type="hidden" id="referencia" name="referencia" value="<?php echo $this->data->referencia;?>">

                    <div class="bloco">

                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_NOME'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" required class="form-control" name="nome_evento" id="nome_evento" maxlength="500" value="<?php echo $this->data->dia; ?>"/>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_DESCRICAO'); ?></label>
                            <div class="value col-md-12">
                                <textarea class="form-control wysihtml5" rows="12" name="descricao_evento" id="descricao_evento" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descritivo); ?></textarea>
                            </div>
                        </div>

                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_CATEGORIA'); ?><span class="required">*</span></label>
                            <?php $categorias = VirtualDeskSiteAgendaHelper::getCategoriasDiasInternacionais()?>
                            <div class="value col-md-12">
                                <select name="categoria" required value="<?php echo $this->data->idCategoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idCategoria)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($categorias as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idCategoria; ?>"><?php echo VirtualDeskSiteAgendaHelper::getCategoriasDiasInternacionaisName($this->data->idCategoria) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeCat = VirtualDeskSiteAgendaHelper::excludeCategoriasDiasInternacionais($this->data->idCategoria)?>
                                        <?php foreach($ExcludeCat as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_DATA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="date" required class="form-control" name="data_evento" id="data_evento" value="<?php echo $this->data->data_inicio; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_PATROCINADOR'); ?></label>
                            <?php $patrocinadores = VirtualDeskSiteAgendaHelper::getPatrocinadores()?>
                            <div class="value col-md-12">
                                <select name="patrocinador" value="<?php echo $this->data->idPatrocinador; ?>" id="patrocinador" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->idPatrocinador)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($patrocinadores as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['nome']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->idPatrocinador; ?>"><?php echo VirtualDeskSiteAgendaHelper::getPatrocinadorName($this->data->idPatrocinador) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludePatrocinador = VirtualDeskSiteAgendaHelper::excludePatrocinador($this->data->idPatrocinador)?>
                                        <?php foreach($ExcludePatrocinador as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['nome']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_ESTADO'); ?></label>
                            <?php $estados = VirtualDeskSiteAgendaHelper::getEstadoDiasInternacionais()?>
                            <div class="value col-md-12">
                                <select name="estado" value="<?php echo $this->data->id_estado; ?>" id="estado" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->id_estado)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($estados as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['estado']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->id_estado; ?>"><?php echo VirtualDeskSiteAgendaHelper::getEstadoDiaInternacionalName($this->data->id_estado) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeEstadoDiaInternacional = VirtualDeskSiteAgendaHelper::excludeEstadoDiaInternacional($this->data->id_estado)?>
                                        <?php foreach($ExcludeEstadoDiaInternacional as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['estado']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>


                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewdiasinternacionais4manager&diainternacional_id=' . $this->escape($this->data->id)); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('diainternacional_id',$setencrypt_forminputhidden); ?>"  value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('agenda.updatediasinternacionais4manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('listdiasinternacionais4manager',$setencrypt_forminputhidden); ?>"/>

                <?php echo JHtml::_('form.token'); ?>

            </form>

        </div>


    </div>

<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/editdiasinternacionais4manager.js.php');
    echo ('</script>');
?>