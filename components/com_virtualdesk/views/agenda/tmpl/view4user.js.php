<?php
defined('_JEXEC') or die;

// Se o estado não for o inicial disable o botão editar
$jsIdEstadoInicial = (int) VirtualDeskSiteAgendaHelper::getEstadoIdInicio();
$jsIdEstadoAtual   = (int)$this->data->idestado;

$jsDesligarBotaoEditar = true;
if($jsIdEstadoInicial == $jsIdEstadoAtual && $jsIdEstadoAtual>0) $jsDesligarBotaoEditar = false;
?>


var PortofolioHandle = function () {

    var initAgendaFileGridFotoCapa = function (evt) {

        // init cubeportfolio
        jQuery('#vdAgendaFileGridFotoCapa').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: '',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>'

        });

    };

    var destroyAgendaFileGridFotoCapa = function (evt) {
        // init cubeportfolio
        jQuery('#vdAgendaFileGridFotoCapa').cubeportfolio('destroy');
    };

    var initAgendaFileGridGaleria = function (evt) {

        // init cubeportfolio
        jQuery('#vdAgendaFileGridGaleria').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        });

    };

    var destroyAgendaFileGridGaleria = function (evt) {
        // init cubeportfolio
        jQuery('#vdAgendaFileGridGaleria').cubeportfolio('destroy');
    };

    var initAgendaFileGridCartaz = function (evt) {

        // init cubeportfolio
        jQuery('#vdAgendaFileGridCartaz').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        });

    };

    var destroyAgendaFileGridCartaz = function (evt) {
        // init cubeportfolio
        jQuery('#vdAgendaFileGridCartaz').cubeportfolio('destroy');
    };

    return {
        //main function to initiate the module
        init: function () {
            initAgendaFileGridFotoCapa();
            initAgendaFileGridGaleria();
            initAgendaFileGridCartaz();
        },
        initAgendaFileGridFotoCapa    :  initAgendaFileGridFotoCapa,
        destroyAgendaFileGridFotoCapa  : destroyAgendaFileGridFotoCapa,
        initAgendaFileGridGaleria     :  initAgendaFileGridGaleria,
        destroyAgendaFileGridGaleria  : destroyAgendaFileGridGaleria,
        initAgendaFileGridCartaz     :  initAgendaFileGridCartaz,
        destroyAgendaFileGridCartaz  : destroyAgendaFileGridCartaz

    };
}();


jQuery(document).ready(function() {

    PortofolioHandle.init();

    jQuery('.nav-tabs.vdMainNavTab').find('a.vdtabAgendaDetalhe').on('shown.bs.tab', function () {
        // ao clicar no detalhe inicializa a grid
        PortofolioHandle.destroyAgendaFileGridDetalhe();
        PortofolioHandle.initAgendaFileGridDetalhe();

    });

    // Prevent default submit by Enter Key
    jQuery("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

    <?php if($jsDesligarBotaoEditar===true) : ?>
    jQuery(".vdBotaoEditar").attr('disabled','disabled');
    jQuery("a.vdBotaoEditar").attr('href','').attr('onclick','return false;');
    <?php endif; ?>

});