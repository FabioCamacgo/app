/*
 * Inicialização do Select 2
 */
var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {
    // Select 2
    ComponentsSelect2.init();

    if(document.getElementById("tipoSel").value == 1){
        document.getElementById("catAssoc").style.display = "block";
        document.getElementById("subcatAssoc").style.display = "none";
        document.getElementById("selectCat").value = 1;
    } else if(document.getElementById("tipoSel").value == 2) {
        document.getElementById("catAssoc").style.display = "none";
        document.getElementById("subcatAssoc").style.display = "block";
        document.getElementById("selectCat").value = 2;
    } else {
        document.getElementById("catAssoc").style.display = "none";
        document.getElementById("subcatAssoc").style.display = "none";
        document.getElementById("selectCat").value = 3;
    }

    document.getElementById('selectCat').onclick = function() {
        document.getElementById("tipoSel").value = 1;
        document.getElementById("catAssoc").style.display = "block";
        document.getElementById("subcatAssoc").style.display = "none";
    };

    document.getElementById('selectSubCat').onclick = function() {
        document.getElementById("tipoSel").value = 2;
        document.getElementById("catAssoc").style.display = "none";
        document.getElementById("subcatAssoc").style.display = "block";
    };

    document.getElementById('selectall').onclick = function() {
        document.getElementById("tipoSel").value = 3;
        document.getElementById("catAssoc").style.display = "none";
        document.getElementById("subcatAssoc").style.display = "none";
    };
})