<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
    JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
    JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');
    JLoader::register('VirtualDeskSiteVAgendaPrecarioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_precario_files.php');

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'view4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');

    // Agenda - CSS Comum
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/agenda-comum.css');

    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $obParam      = new VirtualDeskSiteParamsHelper();

    $getInputAgenda_Id = JFactory::getApplication()->input->getInt('agenda_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAgendaHelper::getAgendaView4ManagerDetail($getInputAgenda_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
       $ListFilesAlert = array();

        $objFotoCapaFiles = new VirtualDeskSiteVAgendaCapaFilesHelper();
        $ListFilesFotoCapa = $objFotoCapaFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objGaleriaFiles = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
        $ListFilesGaleria = $objGaleriaFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objCartazFiles = new VirtualDeskSiteVAgendaCartazFilesHelper();
        $ListFilesCartaz = $objCartazFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objPrecarioFiles = new VirtualDeskSiteVAgendaPrecarioFilesHelper();
        $ListFilesPrecario = $objPrecarioFiles->getFileGuestLinkByRefId ($this->data->codigo);
    }


    // Carregamento de Dados Promotor
    $this->dataPromotor = array();
    $this->dataPromotor = VirtualDeskSiteAgendaHelper::getDadosPromotor($this->data->nif);


    $AgendaEstadoList2Change = VirtualDeskSiteAgendaHelper::getAgendaEstadoAllOptions($language_tag);


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
        return false;
    }

?>


<div class="portlet light bordered form-fit">
   <div class="portlet-title">

        <div class="caption">
            <i class="icon-calendar  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=list4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=edit4manager&vdcleanstate=1&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <button id="duplicar" class="btn btn-circle btn-outline yellow-gold"> <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_AGENDA_DUPLICAR'); ?> </button>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_ADDNEW' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->

    </div>

    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">

            <div class="form-body">

                <div class="portlet light">

                    <div class="row">

                        <!-- COL1 -->
                        <div class="col-md-8">

                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_DADOSDETALHE'); ?></div></div>
                                <div class="portlet-body">

                                    <div class="bloco">
                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info ">
                                                <div class="col-md-3 name text-right"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_ESTADO' ); ?></div>
                                                <div class="col-md-6 value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.getNewEstadoName4ManagerByAjax&agenda_id='. $this->escape($this->data->agenda_id)); ?>" >
                                                    <span class="label <?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>

                                                <?php
                                                $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers');
                                                if($checkAlterarEstado4Managers===true ) : ?>
                                                    <div class="col-md-3" style="margin-top: -5px;">
                                                        <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                                    </div>

                                                    <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                    <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                                </div>
                                                                <div class="modal-body blocoAlterar2NewEstado">


                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-12 ">
                                                                                <div class="form-group" style="padding:0">
                                                                                    <label> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                                    <div style="padding:0;">
                                                                                        <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                            <?php foreach($AgendaEstadoList2Change as $rowEstado) : ?>
                                                                                                <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                            <?php endforeach; ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 ">
                                                                                <div class="form-group">
                                                                                    <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                                    <textarea  rows="5" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" ></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group blocoIconsMsgAviso">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <span> <i class="fa"></i> </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-center">
                                                                                <span class="blocoIconsMsgAviso_Texto"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                    <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.sendAlterar2NewEstado4ManagerByAjax&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" >
                                                                    <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                                    </button>
                                                                    <span> <i class="fa"></i> </span>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>

                                                <?php endif; ?>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="bloco">
                                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DADOSPROMOTOR'); ?></h3>

                                        <div class="col-md-12">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_PROMOTOR' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->dataPromotor->nomePromotor, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_NIF' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nif, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EMAIL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->dataPromotor->emailPromotor, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="bloco">

                                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DADOSEVENTO'); ?></h3>

                                        <div class="col-md-12">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_NOMEEVENTO' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nome_evento, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_CATEGORIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_SUBCATEGORIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->subcategoria, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_TAGS_HEADER' ); ?></div>
                                                <?php
                                                    $explodeTags = explode(';;', $this->data->tags);

                                                    for($i=0;$i<count($explodeTags);$i++){
                                                        if($i==0){
                                                            $tags = $explodeTags[$i];
                                                        } else {
                                                            $tags .= '; ' . $explodeTags[$i];
                                                        }
                                                    }
                                                ?>

                                                <div class="value"> <?php echo htmlentities($tags, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DESCRICAO' ); ?></div>
                                                <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao_evento); ?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DATAINICIO' ); ?></div>
                                                <?php
                                                    if($this->data->data_inicio == '0000-00-00'){
                                                        $dataInicio = '';
                                                    } else {
                                                        $invertDataInicio = explode("-", $this->data->data_inicio);
                                                        $dataInicio = $invertDataInicio[2] . '-' . $invertDataInicio[1] . '-' . $invertDataInicio[0];
                                                    }
                                                ?>
                                                <div class="value"> <?php echo htmlentities( $dataInicio, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_HORAINICIO' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->hora_inicio, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DATAFIM' ); ?></div>
                                                <?php
                                                    if($this->data->data_fim == '0000-00-00'){
                                                        $dataFim = '';
                                                    } else {
                                                        $invertDataFim = explode("-", $this->data->data_fim);
                                                        $dataFim = $invertDataFim[2] . '-' . $invertDataFim[1] . '-' . $invertDataFim[0];
                                                    }
                                                ?>
                                                <div class="value"> <?php echo htmlentities( $dataFim, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_HORAFIM' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->hora_fim, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="bloco">
                                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_LOCALIZACAOEVENTO'); ?></h3>


                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_CONCELHO' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->concelho, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_FREGUESIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_LOCAL' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->local_evento, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>


                                        <?php
                                            if(!empty($this->data->latitude) || !empty($this->data->longitude)){
                                                ?>
                                                    <div class="col-md-12">
                                                        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                                        <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                                                               value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                    </div>


                                    <div class="bloco">
                                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PRECOS'); ?></h3>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_PRECOEVENTO' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->tipo_evento, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>


                                        <?php
                                            if($this->data->id_tipoEvento == 1){
                                                ?>
                                                    <div class="blocoPrecario">
                                                        <div class="col-md-12">
                                                            <div class="row static-info">
                                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DESCRICAO_PRECARIO' ); ?></div>
                                                                <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->observacoes_precario); ?></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="row static-info">
                                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_FOTOPRECARIO' ); ?></div>
                                                                <div id="vdAgendaFileGridPrecario" class="cbp">
                                                                    <?php
                                                                    if(!is_array($ListFilesPrecario)) $ListFilesPrecario = array();
                                                                    foreach ($ListFilesPrecario as $rowFile) : ?>
                                                                        <div class="cbp-item identity logos">
                                                                            <a href="<?php echo $rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                                                <div class="cbp-caption-defaultWrap">
                                                                                    <img src="<?php echo $rowFile->guestlink; ?>" alt=""> </div>
                                                                                <div class="cbp-caption-activeWrap">
                                                                                    <div class="cbp-l-caption-alignLeft">
                                                                                        <div class="cbp-l-caption-body">
                                                                                            <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                                            <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    <?php endforeach;?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                    </div>


                                    <div class="bloco">
                                        <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_REDESOCIAIS'); ?></h3>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_FACEBOOK' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->facebook, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_INSTAGRAM' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->instagram, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_YOUTUBE' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->youtube, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row static-info">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_VIMEO' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->vimeo, ENT_QUOTES, 'UTF-8');?></div>
                                            </div>
                                        </div>

                                    </div>


                                    <?php
                                        if(!empty($this->data->videosEvento)){
                                            ?>

                                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MULTIMEDIA'); ?></h3></legend>

                                            <?php

                                            $piecesVideos = explode(";;", $this->data->videosEvento);

                                            for($i=0; $i< count($piecesVideos); $i++){
                                                if (strpos($piecesVideos[$i], 'youtube') !== false) {

                                                    $youtubeCode = $obParam->getParamsByTag('youtubeCode');

                                                    if (strpos($piecesVideos[$i], 'watch?v=') !== false) {
                                                        $explodeLink = explode("watch?v=", $piecesVideos[$i]);
                                                        $idVideo = explode('&', $explodeLink[1]);
                                                        ?>
                                                        <div class="video">
                                                            <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                        <?php
                                                    }

                                                    if (strpos($piecesVideos[$i], 'embed') !== false) {
                                                        $explodeLink = explode("embed/", $piecesVideos[$i]);
                                                        $idVideo = explode('?', $explodeLink[1]);
                                                        ?>
                                                        <div class="video">
                                                            <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                        <?php
                                                    }
                                                }

                                                if (strpos($piecesVideos[$i], 'youtu.be') !== false) {
                                                    $explodeLink = explode("youtu.be/", $piecesVideos[$i]);
                                                    $idVideo = explode('?', $explodeLink[1]);

                                                    $youtubeCode = $obParam->getParamsByTag('youtubeCode');
                                                    ?>
                                                    <div class="video">
                                                        <iframe src="<?php echo $youtubeCode . $idVideo[0];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <?php
                                                }

                                                if (strpos($piecesVideos[$i], 'facebook') !== false){

                                                    $explodeLink = explode("facebook.com/", $piecesVideos[$i]);
                                                    $idVideo = $explodeLink[1];
                                                    $facebookCode = $obParam->getParamsByTag('facebookCode');


                                                    ?>
                                                    <div class="video">
                                                        <iframe src="<?php echo $facebookCode . urlencode($piecesVideos[$i]);?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <?php
                                                }

                                                if (strpos($piecesVideos[$i], 'vimeo') !== false){
                                                    $vimeoCode = $obParam->getParamsByTag('vimeoCode');

                                                    if (strpos($piecesVideos[$i], 'player.vimeo.com/video/') !== false) {
                                                        $explodeLink = explode("player.vimeo.com/video/", $piecesVideos[$i]);

                                                        ?>
                                                        <div class="video">
                                                            <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                        <?php
                                                    }

                                                    if (strpos($piecesVideos[$i], '/vimeo.com/') !== false) {
                                                        $explodeLink = explode("/vimeo.com/", $piecesVideos[$i]);
                                                        ?>
                                                        <div class="video">
                                                            <iframe src="<?php echo $vimeoCode . $explodeLink[1];?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                    ?>

                                    <div class="row static-info">
                                        <div class="col-md-12 ">
                                            <h6>
                                                <?php
                                                echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_DATACRIACAO' ).$labelseparator;
                                                echo htmlentities( $this->data->data_criacao, ENT_QUOTES, 'UTF-8');
                                                if($this->data->data_criacao != $this->data->data_alteracao) echo ' , ' . JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_DATAALTERACAO' ) . ' ' . htmlentities( $this->data->data_alteracao, ENT_QUOTES, 'UTF-8');
                                                ?>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- COL2 -->
                        <div class="col-md-4">

                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_FOTOCAPA'); ?></div></div>
                                <div class="portlet-body">
                                    <div id="vdAgendaFileGridFotoCapa" class="cbp">
                                        <?php
                                        if(!is_array($ListFilesFotoCapa)) $ListFilesFotoCapa = array();
                                        foreach ($ListFilesFotoCapa as $rowFile) : ?>
                                            <div class="cbp-item identity logos">
                                                <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                    <div class="cbp-caption-defaultWrap">
                                                        <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                    <div class="cbp-caption-activeWrap">
                                                        <div class="cbp-l-caption-alignLeft">
                                                            <div class="cbp-l-caption-body">
                                                                <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_CARTAZ'); ?></div></div>
                                <div class="portlet-body">
                                    <div id="vdAgendaFileGridCartaz" class="cbp">
                                        <?php
                                        if(!is_array($ListFilesCartaz)) $ListFilesCartaz = array();
                                        foreach ($ListFilesCartaz as $rowFile) : ?>
                                            <div class="cbp-item identity logos">
                                                <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                    <div class="cbp-caption-defaultWrap">
                                                        <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                    <div class="cbp-caption-activeWrap">
                                                        <div class="cbp-l-caption-alignLeft">
                                                            <div class="cbp-l-caption-body">
                                                                <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet light bg-inverse bordered">
                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_GALERIA'); ?></div></div>
                                <div class="portlet-body">
                                    <div id="vdAgendaFileGridGaleria" class="cbp">
                                        <?php
                                        if(!is_array($ListFilesGaleria)) $ListFilesGaleria = array();
                                        foreach ($ListFilesGaleria as $rowFile) : ?>
                                            <div class="cbp-item identity logos">
                                                <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                    <div class="cbp-caption-defaultWrap">
                                                        <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                    <div class="cbp-caption-activeWrap">
                                                        <div class="cbp-l-caption-alignLeft">
                                                            <div class="cbp-l-caption-body">
                                                                <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                </div>

            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=edit4manager&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agenda'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('agenda_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->agenda_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('agenda_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('agenda_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

        </form>
    </div>

</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/maps.js.php');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/view4manager.js.php');
echo ('</script>');
?>