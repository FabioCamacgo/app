<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskHelper', JPATH_ADMINISTRATOR . '/components/com_virtualdesk/helpers/virtualdesk.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
JLoader::register('VirtualDeskSiteAgendaStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda_stats.php');


JLoader::register('VirtualDeskSiteStatsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_stats.php');
JLoader::register('VirtualDeskSiteContactUsReportsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_contactus_reports.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkLayoutAccess('agenda', 'list4users');
if($vbHasAccess===false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}




// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.waypoints.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . '/templates/' . $templateName . '/assets/global/plugins/counterup/jquery.counterup.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/amcharts.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/serial.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/pie.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/light.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/amcharts/amcharts/themes/dark.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');

// Agenda - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/agenda-comum.css');

$userSessionID      = VirtualDeskSiteUserHelper::getUserSessionId();

/*  ESTATISTICAS */
$objAgendaStats = new VirtualDeskSiteAgendaStatsHelper();
$objAgendaStats->setAllStats4User();

$agendaNumPedidosOnline       = $objAgendaStats->TotalPedidosOnline4User;
$agendaNumPedidosBalcao       = $objAgendaStats->TotalPedidosBalcao4User;
$agendaNumResolvidos          = $objAgendaStats->TotalResolvidos4User;
$agendaNumNaoResolvidos       = $objAgendaStats->TotalNaoResolvidos4User;
$dataAgendaTotaisPorCategoria = $objAgendaStats->TotaisPorCategoria4User;
$dataAgendaTotPorAnoMes       = $objAgendaStats->TotaisPorAnoMes4User;
$dataAgendaTotaisPorEstado    = $objAgendaStats->TotalPedidosPorEstado4User;

?>
<style>
    .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
    .fileinput{display:block;}
    .fileinput-preview {display:block; max-height: 100%; }
    .fileinput-preview img {display:block; float: none; margin: 0 auto;}

    .tabbable-line>.tab-content {  padding: 0;  }
    .iconVDModified {padding-left: 15px; }

    .EstadoFilterDropBoxAgenda , .CatFilterDropBoxAgenda {float: right; margin-left: 5px}
    @media (min-width:1364px) {
        .EstadoFilterDropBoxAgenda{padding-right: 100px;}
    }

    div.vdBlocoContador {float:left; padding-left: 15px; }
    div.vdBlocoContadorTexto {margin-top: -26px; margin-left: 52px;}

    .gmaps { height: 350px !important; }

</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-calendar font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
        </div>
    </div>
    <!--
    <div class="portlet-title">
        <div class="caption">
             <i class="icon-pointer  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA' ); ?></span>
        </div>
    </div>
     -->

    <div class="portlet-body ">


        <div class="tabbable-line nav-justified ">
            <ul class="nav nav-tabs  ">

                <li class="active">
                    <a href="#tab_Agenda_EventosSubmetidos" data-toggle="tab">
                        <h4>
                            <i class="fa fa-tasks"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_AGENDA_TAB_EVENTOSSUBMETIDOS'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="#tab_Agenda_Estatistica" data-toggle="tab">
                        <h4>
                            <i class="fa fa-bar-chart"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                        </h4>
                    </a>
                </li>

                <li class="">
                    <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=addnew4user&vdcleanstate=1'); ?>" >
                        <h4>
                            <i class="fa fa-plus"></i>
                            <?php echo JText::_('COM_VIRTUALDESK_AGENDA_ADDNEW'); ?>
                        </h4>
                    </a>
                </li>

            </ul>


            <div class="tab-content">

                <div class="tab-pane " id="tab_Agenda_Estatistica">

                    <div class="row widget-row">


                        <div class="col-md-4">
                            <!-- BEGIN NUM PEDIDOS EFETUADOS -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMPEDIDOS_EFETUADOS'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-blue icon-users"></i>

                                    <div class="widget-thumb-body vdBlocoContador">
                                        <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_ONLINE'); ?></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $agendaNumPedidosOnline; ?>"><?php echo $agendaNumPedidosOnline; ?></span>
                                    </div>
                                    <div class="widget-thumb-body vdBlocoContador">
                                        <span class="widget-thumb-subtitle"><?php echo JText::_('COM_VIRTUALDESK_STATS_BALCAO'); ?></span>
                                        <span class="widget-thumb-body-stat " data-counter="counterup" data-value="<?php echo $agendaNumPedidosBalcao; ?>"><?php echo $agendaNumPedidosBalcao; ?></span>
                                    </div>

                                </div>
                            </div>
                            <!-- END NUM PEDIDOS EFETUADOS -->
                        </div>

                        <div class="col-md-4">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_RESOLVIDOS'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-purple icon-speedometer"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $agendaNumResolvidos; ?>"><?php echo $agendaNumResolvidos; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>

                        <div class="col-md-4">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 ">
                                <h4 class="widget-thumb-heading"><?php echo JText::_('COM_VIRTUALDESK_STATS_PENDENTES'); ?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-red icon-paper-clip"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?php echo $agendaNumNaoResolvidos; ?>"><?php echo $agendaNumNaoResolvidos; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-blue"></i>
                                        <span class="caption-subject bold uppercase font-blue"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORMES'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartAgendaTotPorAnoMes" class="chart" style="height: 250px;"></div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pie-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase font-green-haze"><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORESTADO'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartAgendaTotaisPorEstado" class="chart" style="height: 250px;"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-pie-chart "></i>
                                        <span class="caption-subject bold uppercase "><?php echo JText::_('COM_VIRTUALDESK_STATS_NUMREQUESTS_PORCATEGORIA'); ?></span>
                                        <span class="caption-helper"></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="chartAgendaTotaisPorCategoria" class="chart" style="height: 500px;"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="tab-pane active" id="tab_Agenda_EventosSubmetidos">

                    <div class="portlet">
                        <div class="portlet-title">


                            <div class="actions">

                                <!-- Botões DataTables -->
                                <div class="btn-group">
                                    <a class="btn green btn-outline btn-circle" href="javascript:;"
                                       data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"><?php echo JText::_('COM_VIRTUALDESK_2PRINTANDEXPORT'); ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="tabela_lista_agenda_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="fa fa-print"></i> <?php echo JText::_('COM_VIRTUALDESK_2PRINT'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="1" class="tool-action">
                                                <i class="icon-paper-clip"></i> <?php echo JText::_('COM_VIRTUALDESK_2COPY'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="fa fa-file-pdf-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2PDF'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="fa fa-file-excel-o"></i> <?php echo JText::_('COM_VIRTUALDESK_2EXCEL'); ?>
                                            </a>
                                        </li>


                                    </ul>
                                </div>


                                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"
                                   data-original-title="" title=""></a>

                            </div>

                        </div>

                        <div class="portlet-body ">

                            <table class="table table-striped table-bordered table-hover order-column"
                                   id="tabela_lista_agenda">
                                <thead>
                                <tr>
                                    <th style="min-width: 100px;"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTA_DATA'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTA_EVENTO'); ?> </th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTA_CATEGORIA'); ?></th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTA_ESTADO'); ?></th>
                                    <th><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTA_REFERENCIA'); ?> </th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>


                        </div>
                    </div>

                </div>

                <div class="tab-pane " id="tab_Agenda_NovoEvento">
                </div>

            </div>

        </div>




    </div>

</div>

<?php

echo $localScripts;


echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/list4user.js.php');
echo ('</script>');

?>