<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess = $objCheckPerm->checkDetailReadAccess('agenda');
if( $vbHasAccess===false ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';


$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');

// Agenda - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/agenda-comum.css');

// Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');

$getInputAgenda_Id = JFactory::getApplication()->input->getInt('agenda_id');

// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSiteAgendaHelper::getAgendaView4UserDetail($getInputAgenda_Id);

// Carrega de ficheiros associados
if(!empty($this->data->codigo)) {
// $objAlertFiles = new VirtualDeskSiteAlertsFilesHelper();
// $ListFilesAlert = $objAlertFiles->getFileGuestLinkByRefId ($this->data->codigo);
   $ListFilesAlert = array();

$objFotoCapaFiles = new VirtualDeskSiteVAgendaCapaFilesHelper();
$ListFilesFotoCapa = $objFotoCapaFiles->getFileGuestLinkByRefId ($this->data->codigo);


$objGaleriaFiles = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
$ListFilesGaleria = $objGaleriaFiles->getFileGuestLinkByRefId ($this->data->codigo);


$objCartazFiles = new VirtualDeskSiteVAgendaCartazFilesHelper();
$ListFilesCartaz = $objCartazFiles->getFileGuestLinkByRefId ($this->data->codigo);



}



// Todo Ter parametro com o Id de Menu Principal de cada módulo
//$itemmenuid_lista = $params->get('agenda_menuitemid_list');

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

// Dados vazios...
if(empty($this->data)) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
    return false;
}


?>
<style>
    .form .form-horizontal.form-bordered .form-group { background-color: #f1f4f7 !important;}
    .form .form-horizontal.form-bordered .form-group div.col-md-8 { background-color: #fff !important;}
    .profile-userpic img {-webkit-border-radius: 50% !important;  -moz-border-radius: 50% !important;  border-radius: 50% !important;   width: 150px; height: auto;}
    .portfolio-content.portfolio-1 .cbp-caption-activeWrap {background-color: rgba(50, 197, 210, 0.5); }
    .portlet.light.bordered>.portlet-title { border-bottom: 1px solid #dcdcdc; }
    .mt-timeline-2>.mt-container>.mt-item>.mt-timeline-content>.mt-content-container { border: 1px solid #d3d7e9; }
    .text-wrap {white-space:normal;}
    .static-info{margin-bottom: 20px;}
    .static-info .name {line-height: 2;}

    .static-info .value {background-color: #f9f9f9; padding: 5px; margin:5px;font-weight: normal; border: 1px solid #e7ecf1;}

</style>

<div class="portlet light bordered form-fit">
   <div class="portlet-title">

        <div class="caption">
            <i class="icon-calendar  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=edit4user&vdcleanstate=1&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" class="btn btn-circle btn-outline green vdBotaoEditar">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=addnew4user&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_ADDNEW' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->

    </div>


    <div class="portlet-body ">
        <form class="form-horizontal form-bordered">
            <div class="portlet light">

                <div class="row">

                    <!-- COL1 -->
                    <div class="col-md-8">

                        <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                            <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_DADOSDETALHE'); ?></div></div>
                            <div class="portlet-body">

                                <div class="col-md-6">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_REFERENCIA' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_ESTADO' ); ?></div>
                                        <div class="value"> <span class="label <?php echo VirtualDeskSiteAgendaHelper::getAgendaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_NOMEEVENTO' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->nome_evento, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DESCRICAO' ); ?></div>
                                        <div class="value"> <?php  echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao_evento); ?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_CATEGORIA' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_SUBCATEGORIA' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->subcategoria, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_TIPOEVENTO' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->tipo_evento, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_LOCAL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->local_evento, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DATAINICIO' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->data_inicio, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_HORAINICIO' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->hora_inicio, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DATAFIM' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->data_fim, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_HORAFIM' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->hora_fim, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_CONCELHO' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->concelho, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_FREGUESIA' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_FACEBOOK' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->facebook, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_INSTAGRAM' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->instagram, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_VIMEO' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->vimeo, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_YOUTUBE' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->youtube, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="row static-info">

                                    </div>
                                </div>


                                <div class="row static-info">
                                    <div class="col-md-12 ">
                                        <h6>
                                            <?php
                                            echo JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_DATACRIACAO' ).$labelseparator;
                                            echo htmlentities( $this->data->data_criacao, ENT_QUOTES, 'UTF-8');
                                            if($this->data->data_criacao != $this->data->data_alteracao) echo ' , ' . JText::_( 'COM_VIRTUALDESK_AGENDA_LISTA_DATAALTERACAO' ) . ' ' . htmlentities( $this->data->data_alteracao, ENT_QUOTES, 'UTF-8');
                                            ?>
                                        </h6>
                                    </div>
                                </div>




                            </div>
                        </div>

                        <div class="portlet light bg-inverse bordered">
                                <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_MAPA'); ?></div></div>
                                <div class="portlet-body">
                                    <div>
                                        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                        <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                                               value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                    </div>
                                </div>
                            </div>

                    </div>

                    <!-- COL2 -->
                    <div class="col-md-4">

                        <div class="portlet light bg-inverse bordered">
                            <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_FOTOCAPA'); ?></div></div>
                            <div class="portlet-body">
                                <div id="vdAgendaFileGridFotoCapa" class="cbp">
                                    <?php
                                    if(!is_array($ListFilesFotoCapa)) $ListFilesFotoCapa = array();
                                    foreach ($ListFilesFotoCapa as $rowFileCapa) : ?>
                                        <div class="cbp-item identity logos">
                                            <a href="<?php echo$rowFileCapa->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFileCapa->filename; ?>">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="<?php echo$rowFileCapa->guestlink; ?>" alt=""> </div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title"><?php echo $rowFileCapa->filename; ?></div>
                                                            <div class="cbp-l-caption-desc"><?php echo $rowFileCapa->desc; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>

                        <div class="portlet light bg-inverse bordered">
                            <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_CARTAZ'); ?></div></div>
                            <div class="portlet-body">
                                <div id="vdAgendaFileGridCartaz" class="cbp">
                                    <?php
                                    if(!is_array($ListFilesCartaz)) $ListFilesCartaz = array();
                                    foreach ($ListFilesCartaz as $rowFileCartaz) : ?>
                                        <div class="cbp-item identity logos">
                                            <a href="<?php echo$rowFileCartaz->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFileCartaz->filename; ?>">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="<?php echo$rowFileCartaz->guestlink; ?>" alt=""> </div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title"><?php echo $rowFileCartaz->filename; ?></div>
                                                            <div class="cbp-l-caption-desc"><?php echo $rowFileCartaz->desc; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">



                </div>


                <div class="row">
                    <div class="portlet light bg-inverse bordered">
                        <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_GALERIA'); ?></div></div>
                        <div class="portlet-body">
                            <div id="vdAgendaFileGridGaleria" class="cbp">
                                <?php
                                if(!is_array($ListFilesGaleria)) $ListFilesGaleria = array();
                                foreach ($ListFilesGaleria as $rowFileGaleria) : ?>
                                    <div class="cbp-item identity logos">
                                        <a href="<?php echo$rowFileGaleria->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFileGaleria->filename; ?>">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="<?php echo$rowFileGaleria->guestlink; ?>" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title"><?php echo $rowFileGaleria->filename; ?></div>
                                                        <div class="cbp-l-caption-desc"><?php echo $rowFileGaleria->desc; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-actions right">
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn green vdBotaoEditar" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=edit4user&agenda_id=' . $this->escape($this->data->agenda_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agenda'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('agenda_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->agenda_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('agenda_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('agenda_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

        </form>
    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/maps.js.php');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/view4user.js.php');
echo ('</script>');
?>