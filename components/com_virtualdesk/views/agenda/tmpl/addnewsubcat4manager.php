<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnewsubcat4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    $localScripts .= $addscript_ini . 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js' . $addscript_end;


    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    // Agenda - CSS Comum
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/agenda-comum.css');

    // Parâmetros
    $labelseparator    = ' : ';
    $params            = JComponentHelper::getParams('com_virtualdesk');

    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnewsubcat4manager.agenda.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;

    $obParam    = new VirtualDeskSiteParamsHelper();

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-calendar font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_ADDNEW_SUBCAT' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
            <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=subcatlist4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>


    <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
    </div>

    <script>
        <?php
        // Objecto inicializado com as mensagens de agenda já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
        ?>
        var MessageAlert = new function() {
            this.getRequiredMissed = function (nErrors) {
                if (nErrors == null)
                { return(''); }
                if(nErrors==1)
                { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                else  if(nErrors>1)
                { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                    return (msg.replace("%s",nErrors) );
                }
            };
        }
    </script>


    <div class="portlet-body form">

        <form id="new-cat" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.createsubcat4Manager'); ?>" method="post"
              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

            <div class="form-body">
                <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DADOSSUBCATEGORIA'); ?></h3>

                <!--   SUBCAT PT   -->
                <div class="form-group" id="subcatPT">
                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEPT'); ?><span class="required">*</span></label>
                    <div class="col-md-12">
                        <input type="text" required class="form-control" name="subcat_PT" id="subcat_PT" maxlength="250" value="<?php echo $subcat_PT; ?>"/>
                    </div>
                </div>

                <!--   SUBCAT EN   -->
                <div class="form-group" id="subcatEN">
                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEEN'); ?><span class="required">*</span></label>
                    <div class="col-md-12">
                        <input type="text" required class="form-control" name="subcat_EN" id="subcat_EN" maxlength="250" value="<?php echo $subcat_EN; ?>"/>
                    </div>
                </div>

                <!--   SUBCAT FR   -->
                <div class="form-group" id="subcatFR">
                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEFR'); ?><span class="required">*</span></label>
                    <div class="col-md-12">
                        <input type="text" required class="form-control" name="subcat_FR" id="subcat_FR" maxlength="250" value="<?php echo $subcat_FR; ?>"/>
                    </div>
                </div>

                <!--   SUBCAT DE   -->
                <div class="form-group" id="subcatDE">
                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_SUBCATNAMEDE'); ?><span class="required">*</span></label>
                    <div class="col-md-12">
                        <input type="text" required class="form-control" name="subcat_DE" id="subcat_DE" maxlength="250" value="<?php echo $subcat_DE; ?>"/>
                    </div>
                </div>


                <!--   Categoria   -->
                <div class="form-group" id="state">
                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_CATEGORIA'); ?><span class="required">*</span></label>
                    <?php $AgendaCategoriaList2Change = VirtualDeskSiteVAgendaHelper::getCategoria();?>
                    <div class="col-md-12">
                        <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                            <?php
                            if(empty($categoria)){
                                ?>
                                <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($AgendaCategoriaList2Change as $rowStatus) : ?>
                                    <option value="<?php echo $rowStatus['id']; ?>"
                                    ><?php echo $rowStatus['categoria']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getCatSelect($categoria) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php $ExcludeCategoria = VirtualDeskSiteVAgendaHelper::excludeCat($categoria)?>
                                <?php foreach($ExcludeCategoria as $rowStatus) : ?>
                                    <option value="<?php echo $rowStatus['id']; ?>"
                                    ><?php echo $rowStatus['categoria']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <!--   Estado   -->
                <div class="form-group" id="state">
                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_LISTACATEGORIAS_CATESTADO'); ?><span class="required">*</span></label>
                    <?php $AgendaEstadoList2Change = VirtualDeskSiteVAgendaHelper::getAgendaEstado();?>
                    <div class="col-md-12">
                        <select name="estado" value="<?php echo $estado; ?>" id="estado" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                            <?php
                            if(empty($estado)){
                                ?>
                                <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                <?php foreach($AgendaEstadoList2Change as $rowStatus) : ?>
                                    <option value="<?php echo $rowStatus['id']; ?>"
                                    ><?php echo $rowStatus['estado']; ?></option>
                                <?php endforeach;
                            } else {
                                ?>
                                <option value="<?php echo $estado; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getestadoSelect($estado) ?></option>
                                <option value=""><?php echo '-'; ?></option>
                                <?php $ExcludeState = VirtualDeskSiteVAgendaHelper::excludeEstado($estado)?>
                                <?php foreach($ExcludeState as $rowStatus) : ?>
                                    <option value="<?php echo $rowStatus['id']; ?>"
                                    ><?php echo $rowStatus['estado']; ?></option>
                                <?php endforeach;
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span></button>
                        <a class="btn default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=subcatlist4manager&vdcleanstate=1'); ?>"
                           title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('agenda.createsubcat4Manager',$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('subcatlist4manager',$setencrypt_forminputhidden); ?>"/>


            <?php echo JHtml::_('form.token'); ?>

        </form>

    </div>

</div>




<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/addnewsubcat4manager.js.php');
    echo ('</script>');
?>
