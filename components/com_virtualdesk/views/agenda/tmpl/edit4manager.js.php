<?php
    $pinMapa = $obParam->getParamsByTag('pinMapa');
    $limitVideoEventoFree = $obParam->getParamsByTag('limitVideoEventoFree');

    JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
    JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
    JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');
    JLoader::register('VirtualDeskSiteVAgendaPrecarioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_precario_files.php');

    $objCapaFiles                = new VirtualDeskSiteVAgendaCapaFilesHelper();
    $vdPreFileUploderCapaLista   = $objCapaFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

    $objCartazFiles              = new VirtualDeskSiteVAgendaCartazFilesHelper();
    $vdPreFileUploderCartazLista = $objCartazFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

    $objGaleriaFiles              = new VirtualDeskSiteVAgendaGaleriaFilesHelper();
    $vdPreFileUploderGaleriaLista = $objGaleriaFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

    $objPrecarioFiles              = new VirtualDeskSiteVAgendaPrecarioFilesHelper();
    $vdPreFileUploderPrecarioLista = $objPrecarioFiles->getFileUploaderJS_ByRefId_4Manager ($this->data->codigo);

    $explodeVideos = explode(';;', $this->data->videosEvento);
?>

var iconPath = '<?php echo JUri::base() . $pinMapa; ?>';
var beginVideo = '<?php echo count($explodeVideos); ?>';
var beginTags = '<?php echo count($explodeTags); ?>';
var maxvideo = '<?php echo $limitVideoEventoFree; ?>';
var maxTags = 10;

var AgendaEdit = function() {

    var handleAgendaEdit = function() {

        jQuery('#edit-agenda').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nipc: {
                    vdnifcheck: ['', 'nipc']
                },
            },

            invalidHandler: function(event, validator) { //display error alert on form submit

                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = MessageAlert.getRequiredMissed(errors);
                    var MainMessageBlock = jQuery("#MainMessageAlertBlock");
                    MainMessageBlock.find('span').html(message);
                    MainMessageBlock.show();
                    App.scrollTo(MainMessageBlock, -200);
                }
            },

            highlight: function(element) { // hightlight error inputs
                jQuery(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {

                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {

                    error.insertAfter(element);
                }
            },

            submitHandler: function(form) {
                form.submit();
            }
        });

        jQuery('#edit-agenda').find('input').keypress(function(e) {
            if (e.which == 13) {
                if (jQuery('#edit-agenda').validate().form()) {
                    jQuery('#edit-agenda').submit();
                }
                return false;
            }
        });

    }
    return {
        //main function to initiate the module
        init: function() {
            handleAgendaEdit();
        }
    };

}();

/*
 * Inicialização do Select 2
 */
var ComponentsSelect2 = function() {

    var handleDemo = function() {

        // Set the "bootstrap" theme as the default theme for all Select2
        // widgets.
        //
        // @see https://github.com/select2/select2/issues/2927
        $.fn.select2.defaults.set("theme", "bootstrap");

        var placeholder = "";

        $(".select2, .select2-multiple").select2({
            // placeholder: placeholder,

            width: null
        });

        $(".select2-nosearch").select2({
            minimumResultsForSearch: -1,
            width: null
        });


        $(".select2-allow-clear").select2({
            allowClear: true,
            //placeholder: placeholder,
            width: null
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(":checkbox").on("click", function() {
            $(this).parent().nextAll("select").prop("disabled", !this.checked);
        });

        // copy Bootstrap validation states to Select2 dropdown
        //
        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
        // body > .select2-container) if _any_ of the opened Select2's parents
        // has one of these forementioned classes (YUCK! ;-))
        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
            if ($(this).parents("[class*='has-']").length) {
                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

                for (var i = 0; i < classNames.length; ++i) {
                    if (classNames[i].match("has-")) {
                        $("body > .select2-container").addClass(classNames[i]);
                    }
                }
            }
        });

        $(".js-btn-set-scaling-classes").on("click", function() {
            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleDemo();
        }
    };

}();

var ComponentFileUploader = function() {

    var handleFileUploader = function() {

        // FileUploader - innostudio
        jQuery("#fileupload_capa").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 1,

            // if null - has no limits
            // example: 3
            fileMaxSize: 3,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png'],

            enableApi: true,

            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                '<ul class="fileuploader-items-list">' +
                '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                '</ul>' +
                '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploderCapaLista)) echo(', files:  '.$vdPreFileUploderCapaLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#uploadFieldCapa').find('input[name="vdFileUpChangedCapa"]').val('1');
                console.log( jQuery('#uploadFieldCapa').find('input[name="vdFileUpChangedCapa"]').val());
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#uploadFieldCapa').find('input[name="vdFileUpChangedCapa"]').val('1');
                console.log( jQuery('#uploadFieldCapa').find('input[name="vdFileUpChangedCapa"]').val());
                return true;
            }

        });

        // FileUploader - innostudio
        jQuery("#fileupload_cartaz").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 2,

            // if null - has no limits
            // example: 3
            fileMaxSize: 3,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png'],

            enableApi: true,

            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                '<ul class="fileuploader-items-list">' +
                '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                '</ul>' +
                '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploderCartazLista)) echo(', files:  '.$vdPreFileUploderCartazLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#uploadFieldCartaz').find('input[name="vdFileUpChangedCartaz"]').val('1');
                console.log( jQuery('#uploadFieldCartaz').find('input[name="vdFileUpChangedCartaz"]').val());
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#uploadFieldCartaz').find('input[name="vdFileUpChangedCartaz"]').val('1');
                console.log( jQuery('#uploadFieldCartaz').find('input[name="vdFileUpChangedCartaz"]').val());
                return true;
            }

        });

        // FileUploader - innostudio
        jQuery("#fileupload_galeria").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 4,

            // if null - has no limits
            // example: 3
            fileMaxSize: 3,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png'],

            enableApi: true,

            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                '<ul class="fileuploader-items-list">' +
                '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                '</ul>' +
                '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="type-holder">${extension}</div>' +
                '<div class="actions-holder">' +
                '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                '</div>' +
                '<div class="thumbnail-holder">' +
                '${image}' +
                '<span class="fileuploader-action-popup"></span>' +
                '</div>' +
                '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                '<div class="progress-holder">${progressBar}</div>' +
                '</div>' +
                '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploderGaleriaLista)) echo(', files:  '.$vdPreFileUploderGaleriaLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#uploadFieldGaleria').find('input[name="vdFileUpChangedGaleria"]').val('1');
                console.log( jQuery('#uploadFieldGaleria').find('input[name="vdFileUpChangedGaleria"]').val());
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#uploadFieldGaleria').find('input[name="vdFileUpChangedGaleria"]').val('1');
                console.log( jQuery('#uploadFieldGaleria').find('input[name="vdFileUpChangedGaleria"]').val());
                return true;
            }

        });

        // FileUploader - innostudio
        jQuery("#fileupload_precario").fileuploader({

            changeInput: ' ',

            // if null - has no limits
            // example: 6
            limit: 2,

            // if null - has no limits
            // example: 3
            fileMaxSize: 3,

            // if null - has no limits
            // example: ['jpg', 'jpeg', 'png', 'text/plain', 'audio/*']
            extensions: ['jpg', 'jpeg', 'png'],

            enableApi: true,

            addMore: true,

            theme: 'thumbnails',

            thumbnails: {
                box: '<div class="fileuploader-items">' +
                    '<ul class="fileuploader-items-list">' +
                    '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner"><i>+</i></div></li>' +
                    '</ul>' +
                    '</div>',
                item: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5>${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                item2: '<li class="fileuploader-item file-has-popup">' +
                    '<div class="fileuploader-item-inner">' +
                    '<div class="type-holder">${extension}</div>' +
                    '<div class="actions-holder">' +
                    '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                    '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i></i></a>' +
                    '</div>' +
                    '<div class="thumbnail-holder">' +
                    '${image}' +
                    '<span class="fileuploader-action-popup"></span>' +
                    '</div>' +
                    '<div class="content-holder"><h5 title="${name}">${name}</h5><span>${size2}</span></div>' +
                    '<div class="progress-holder">${progressBar}</div>' +
                    '</div>' +
                    '</li>',
                startImageRenderer: true,
                canvasImage: false,
                _selectors: {
                    list: '.fileuploader-items-list',
                    item: '.fileuploader-item',
                    start: '.fileuploader-action-start',
                    retry: '.fileuploader-action-retry',
                    remove: '.fileuploader-action-remove'
                },
                onItemShow: function(item, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    plusInput.insertAfter(item.html)[api.getOptions().limit && api.getChoosedFiles().length >= api.getOptions().limit ? 'hide' : 'show']();

                    if(item.format == 'image') {
                        item.html.find('.fileuploader-item-icon').hide();
                    }
                },
                onItemRemove: function(html, listEl, parentEl, newInputEl, inputEl) {
                    var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                        api = $.fileuploader.getInstance(inputEl.get(0));

                    html.children().animate({'opacity': 0}, 200, function() {
                        html.remove();

                        if (api.getOptions().limit && api.getChoosedFiles().length - 1 < api.getOptions().limit)
                            plusInput.show();
                    });
                }
            },
            dragDrop: {
                container: '.fileuploader-thumbnails-input'
            },
            afterRender: function(listEl, parentEl, newInputEl, inputEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                    api = jQuery.fileuploader.getInstance(inputEl.get(0));

                plusInput.on('click', function() {
                    api.open();
                });
            },


            // by default - false
            editor: {
                cropper: {
                    showGrid: true
                },
                maxWidth: 800,
                maxHeight: 600,
                quality: 98
            },
            reader: {
                timeout: 12000,
                maxSize: 20
            }

            ,captions: {
                confirm: '<?php echo JText::_('COM_VIRTUALDESK_CONFIRMAR'); ?>',
                cancel: '<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>',
                name: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_NOME'); ?>',
                type: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_TYPE'); ?>',
                size: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_SIZE'); ?>',
                dimensions: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_DIMENSIONS'); ?>',
                remove: '<?php echo JText::_('COM_VIRTUALDESK_FILEUPLOADER_CMP_REMOVE'); ?>',

                errors: {
                    filesLimit: 'Apenas ${limit} são permitidos.',
                    filesType: 'Apenas ficheiros ${extensions} são permitidos.',
                    fileSize: '${name} é demasiado grande! Escolha um ficheiro com tamanho máximo de ${fileMaxSize}MB.',
                    filesSizeAll: 'Os ficheiros escolhidos são demasiado grandes! Escolha ficheiros com tamanho máximo com ${maxSize} MB.',
                    fileName: 'O ficheiro ${name} já foi escolhido.',
                    folderUpload: 'Não tem permissão para carregar ficheiros.'
                }
            }

            <?php if(!empty($vdPreFileUploderPrecarioLista)) echo(', files:  '.$vdPreFileUploderPrecarioLista); ?>

            // Callback fired on selecting and processing a file
            ,onSelect: function(item, listEl, parentEl, newInputEl, inputEl) {
                jQuery('#uploadFieldPrecario').find('input[name="vdFileUpChangedPrecario"]').val('1');
                console.log( jQuery('#uploadFieldPrecario').find('input[name="vdFileUpChangedPrecario"]').val());
                return true;
            },

            // Callback fired after deleting a file
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                // callback will go here
                jQuery('#uploadFieldPrecario').find('input[name="vdFileUpChangedPrecario"]').val('1');
                console.log( jQuery('#uploadFieldPrecario').find('input[name="vdFileUpChangedPrecario"]').val());
                return true;
            }

        });

}

    return {
        //main function to initiate the module
        init: function() {
            handleFileUploader();
        }
    };
}();

var MapsGoogle = function () {
    var mapMarker = function () {
        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });
        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });
        map.setZoom(12);
        GMaps.on('click', map.map, function(event) {
            map.removeMarkers();
            var index = map.markers.length;
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            jQuery('#coordenadas').val(lat + ',' + lng);
            map.addMarker({
                lat: lat,
                lng: lng,
                title: 'Marker #' + index,
                icon: iconPath,
                infoWindow: {
                    content : ''
                }
            });
        });


        <?php /*require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps.js.php'); */?>

    }
    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
        }
    };
}();

<?php require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps_default.js.php'); ?>

var ComponentsEditors = function () {

    var handleWysihtml5 = function () {
        let  ckconfig_removeButtons = 'Subscript,Superscript,Anchor,SpecialChar,Image,About,Scayt';
        let  ckconfig_toolbarGroups =   [
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'others', groups: [ 'others' ] },
            { name: 'colors', groups: [ 'colors' ] }
        ];

        CKEDITOR.replace( 'descricao_evento', {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: ckconfig_toolbarGroups,
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: ckconfig_removeButtons
        });

        CKEDITOR.replace( 'observacoes_precario' , {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: ckconfig_toolbarGroups,
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: ckconfig_removeButtons
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();

        }
    };

}();

var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
            jQuery('.mt-repeater.video').each(function(){

                jQuery(this).repeater({
                    show: function () {
                        jQuery(this).slideDown();

                        jQuery('.select2-container').remove();
                        jQuery('.select2').select2({
                            placeholder: "<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?>",
                            allowClear: true
                        });
                        jQuery('.select2-container').css('width','100%');

                        beginVideo++;
                        if(beginVideo >= maxvideo){
                            jQuery('.video .btn.btn-success.mt-repeater-add').css('display','none');
                        } else {
                            jQuery('.video .btn.btn-success.mt-repeater-add').css('display','inline-block');
                        }

                        if(beginVideo == 1){
                            jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','none');
                        } else {
                            jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                        }
                    },

                    hide: function (deleteElement) {

                        if(confirm('Deseja apagar este elemento?')) {
                            jQuery(this).slideUp(deleteElement);
                            beginVideo--;
                            if(beginVideo >= maxvideo){
                                jQuery('.video .btn.btn-success.mt-repeater-add').css('display','none');
                            } else {
                                jQuery('.video .btn.btn-success.mt-repeater-add').css('display','inline-block');
                            }

                            if(beginVideo == 1){
                                jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','none');
                            } else {
                                jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                            }
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });

            jQuery('.mt-repeater.tags').each(function(){

                jQuery(this).repeater({
                    show: function () {
                        jQuery(this).slideDown();

                        jQuery('.select2-container').remove();
                        jQuery('.select2').select2({
                            placeholder: "<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?>",
                            allowClear: true
                        });
                        jQuery('.select2-container').css('width','100%');

                        beginTags++;
                        if(beginTags >= maxTags){
                            jQuery('.tags .btn.btn-success.mt-repeater-add').css('display','none');
                        } else {
                            jQuery('.tags .btn.btn-success.mt-repeater-add').css('display','inline-block');
                        }

                        if(beginTags == 1){
                            jQuery('.tags .btn.btn-danger.mt-repeater-delete').css('display','none');
                        } else {
                            jQuery('.tags .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                        }
                    },

                    hide: function (deleteElement) {

                        if(confirm('Deseja apagar este elemento?')) {
                            jQuery(this).slideUp(deleteElement);
                            beginTags--;
                            if(beginTags >= maxTags){
                                jQuery('.tags .btn.btn-success.mt-repeater-add').css('display','none');
                            } else {
                                jQuery('.tags .btn.btn-success.mt-repeater-add').css('display','inline-block');
                            }

                            if(beginTags == 1){
                                jQuery('.tags .btn.btn-danger.mt-repeater-delete').css('display','none');
                            } else {
                                jQuery('.tags .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
                            }
                        }
                    },

                    ready: function (setIndexes) {

                    }
                });
            });

        }

    };

}();


/*
* Inicialização da Validação
*/
jQuery(document).ready(function() {
    var preco1 = jQuery('#preco').val();

    if(preco1 == 1){
        jQuery('.blocoPrecario').css('display','block');
    } else {
        jQuery('.blocoPrecario').css('display','none');
    }

    // Validações
    AgendaEdit.init();

    // Select 2
    ComponentsSelect2.init();

    // Maps
    var InputLatLong = jQuery('#coordenadas').val();
    if( InputLatLong!=undefined && InputLatLong!="") {
        var ArrayLatLong = InputLatLong.split(",");
        if(ArrayLatLong.length == 2) {
            LatToSet  =  ArrayLatLong[0];
            LongToSet =  ArrayLatLong[1];
        }
    }

    MapsGoogle.init();

    ComponentFileUploader.init();

    ComponentsEditors.init();

    FormRepeater.init();

    if(beginVideo == 1){
        jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','none');
    } else {
        jQuery('.video .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
    }

    if(beginVideo == maxvideo){
        jQuery('.video a.btn.btn-success.mt-repeater-add').css('display','none');
    } else {
        jQuery('.video a.btn.btn-success.mt-repeater-add').css('display','inline-block');
    }

    if(beginTags == 1){
        jQuery('.tags .btn.btn-danger.mt-repeater-delete').css('display','none');
    } else {
        jQuery('.tags .btn.btn-danger.mt-repeater-delete').css('display','inline-block');
    }

    if(beginTags == maxTags){
        jQuery('.tags a.btn.btn-success.mt-repeater-add').css('display','none');
    } else {
        jQuery('.tags a.btn.btn-success.mt-repeater-add').css('display','inline-block');
    }

    jQuery("#categoria").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var categoria = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(categoria == null || categoria == '') {

            var select = jQuery('#subcategoria');
            select.find('option').remove();
            select.prop('disabled', true);

        } else {

            var dataString = "categoria="+categoria; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoSubCategoria',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=agenda_getsubcat&categoria=' + categoria ,

                success: function(output) {

                    var select = jQuery('#subcategoria');
                    select.find('option').remove();

                    if (output == null || output == '') {
                        select.prop('disabled', true);
                        console.log(output);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoSubCategoria');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoSubCategoria');
                }
            });
        }


    });

    jQuery("#concelho").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */

        var pathArray = window.location.pathname.split('/');

        var secondLevelLocation = pathArray[1];

        var concelho = jQuery(this).val(); /* GET THE VALUE OF THE SELECTED DATA */

        if(concelho == null || concelho == '') {

            var select = jQuery('#freguesia_evento');
            select.find('option').remove();
            select.prop('disabled', true);

        } else {

            var dataString = "concelho="+concelho; /* STORE THAT TO A DATA STRING */

            App.blockUI({ target:'#blocoFreg',  animate: true});

            jQuery.ajax({
                url: '<?php echo JUri::base() . 'onAjaxVD/'; ?>',
                data:'m=agenda_getfreg&concelho=' + concelho ,

                success: function(output) {

                    var select = jQuery('#freguesia_evento');
                    select.find('option').remove();

                    if (output == null || output == '') {
                        select.prop('disabled', true);
                        console.log(output);
                    }
                    else {
                        select.prop('disabled', false);
                        select.append(jQuery('<option>'));
                        jQuery.each(JSON.parse(output), function (i, obj) {
                            //console.log('i=' + i);
                            select.append(jQuery('<option>').text(obj.name).attr('value', obj.id));
                        });
                    }

                    // hide loader
                    App.unblockUI('#blocoFreg');

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert(xhr.status +  ' ' + thrownError);
                    // hide loader
                    select.find('option').remove();
                    select.prop('disabled', true);

                    App.unblockUI('#blocoFreg');
                }
            });
        }


    });

    jQuery("#preco").change(function(){
        var preco = jQuery(this).val();

        if(preco == 1){
            jQuery('.blocoPrecario').css('display','block');
        } else {
            jQuery('.blocoPrecario').css('display','none');
        }
    });

});