<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaPatrocinadoresLogosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_patrocinadores_logos.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'viewpatrocinador4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/patrocinadores.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputPatrocinador_Id = JFactory::getApplication()->input->getInt('patrocinador_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAgendaHelper::getPatrocinadorDetail4Manager($getInputPatrocinador_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
        //$ListFilesLogo = array();

        $objLogoFiles = new VirtualDeskSiteVAgendaPatrocinadoresLogosHelper();
        $ListFilesLogo = $objLogoFiles->getFileGuestLinkByRefId ($this->data->codigo);
    }

    $PatrocinadorEstadoList2Change = VirtualDeskSiteAgendaHelper::getEstadoPatrocinadorAllOptions($language_tag);

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PATROCINADORES_EMPTYLIST'), 'error' );
        return false;
    }

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_PATROCINADORES_VIEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=listpatrocinadores4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editpatrocinadores4manager&patrocinador_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EDITAR_PATROCINADOR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=addnewpatrocinadores4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_ADDNEW_PATROCINADOR' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">
                <div class="form-body">
                    <div class="portlet light">
                        <div class="col-md-8">
                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">
                                    <div class="bloco">
                                        <legend>
                                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_VIEW'); ?></h3>
                                        </legend>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_PATROCINADORES_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half state">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_PATROCINADORES_ESTADO' ); ?></div>
                                                <div class="value">
                                                    <span class="label <?php echo VirtualDeskSiteAgendaHelper::getEstadoPatrocinadorCSS($this->data->id_estado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_NIF'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nif, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_NOME'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nome, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_WEBSITE'); ?></div>
                                                <div class="value"><a href="<?php echo $this->data->url;?>" target="_blank"><?php echo htmlentities( $this->data->url, ENT_QUOTES, 'UTF-8');?></a></div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PATROCINADORES_LOGO'); ?></h3></legend>

                                        <div id="vdPatrocinadoresFileGridLogo" class="cbp">
                                            <?php
                                            if(!is_array($ListFilesLogo)) $ListFilesLogo = array();
                                            foreach ($ListFilesLogo as $rowFile) : ?>
                                                <div class="cbp-item identity logos">
                                                    <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                        <div class="cbp-caption-defaultWrap">
                                                            <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                        <div class="cbp-caption-activeWrap">
                                                            <div class="cbp-l-caption-alignLeft">
                                                                <div class="cbp-l-caption-body">
                                                                    <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                    <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editpatrocinadores4manager&patrocinador_id=' . $this->escape($this->data->id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=listpatrocinadores4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('patrocinador_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/viewpatrocinadores4manager.js.php');
    echo ('</script>');
?>