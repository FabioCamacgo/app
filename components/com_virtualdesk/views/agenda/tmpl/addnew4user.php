<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');
if($vbHasAccess===false ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}



// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

#$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js' . $addscript_end;
#$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

#$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');

//Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');
// Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4user.agenda.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

//se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


$UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
$UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
$nome =  $UserProfileData->name;
$email    = $UserProfileData->email;
$email2   = $UserProfileData->email;
$fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
$telefone = $UserProfileData->phone1;

$obParam      = new VirtualDeskSiteParamsHelper();


$concelhoAgenda      = $obParam->getParamsByTag('agendaConcelho');
$espera              = $obParam->getParamsByTag('espera');
$indicativoConcelho  = $obParam->getParamsByTag('AgendaIndConcelho');
$linkPolitica        = $obParam->getParamsByTag('linkPoliticaAgenda');


$concelhoAgenda = 4;  // TODO ALTERAR !!!!!!!!!!!!!!!!!!!!


// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>
    <style>
        .profile-userpic img {-webkit-border-radius: inherit !important;  -moz-border-radius: inherit !important;  border-radius: inherit !important;   width: <?php echo $useravatar_width; ?>px; height: auto;}
        .fileinput-preview img {display:block; float: none; margin: 0 auto;}
        .select2-bootstrap-prepend .input-group-btn {vertical-align: bottom};
    </style>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            <?php
            // Objecto inicializado com as mensagens de agenda já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
            ?>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <ul class="nav nav-tabs  ">
                    <li class="active">

                        <a href="#tab_Agenda_NovoEvento" data-toggle="tab">
                            <h4>
                                <i class="fa fa-plus"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_AGENDA_ADDNEW'); ?>
                            </h4>
                        </a>

                    </li>

                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=list4user#tab_Agenda_Estatistica'); ?>" >
                            <h4>
                                <i class="fa fa-bar-chart"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                            </h4>
                        </a>

                    </li>
                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=list4user#tab_Agenda_EventosSubmetidos'); ?>" >
                            <h4>
                                <i class="fa fa-tasks"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_AGENDA_TAB_EVENTOSSUBMETIDOS'); ?>
                            </h4>
                        </a>
                    </li>

                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="tab_Agenda_NovoEvento">

                        <form id="new-agenda"
                              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.create4user'); ?>" method="post"
                              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                              role="form">

                            <div class="form-body">


                                <!--   NIF -> pesquisa mas aceita novos   -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NIF'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="nipc" required readonly class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NIF'); ?>"
                                               name="nif_evento" id="nif_evento" maxlength="9" value="<?php echo $fiscalid; ?>"/>
                                    </div>
                                </div>

                                <!--   EMail  -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMAIL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="email" required readonly class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMAIL'); ?>"
                                               name="email_evento" id="email_evento" maxlength="100" value="<?php echo $email; ?>"/>
                                    </div>
                                </div>


                                <!--   NOME   -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NOMEEVENTO'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NOMEEVENTO'); ?>"
                                               name="nome_evento" id="nome_evento" maxlength="40" value="<?php echo $nome_evento; ?>"/>
                                    </div>
                                </div>

                                <!--   DESCRIÇAO   -->
                                <div class="form-group" id="desc">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DESCRICAO'); ?></label>
                                    <div class="col-md-9">
                                <textarea  class="form-control wysihtml5" rows="12"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DESCRICAO'); ?>"
                               name="descricao_evento" id="descricao_evento" maxlength="250"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao_evento); ?></textarea>
                                    </div>
                                </div>


                                <!--   CATEGORIA   -->
                                <div class="form-group" id="cat">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CATEGORIA'); ?><span class="required">*</span></label>
                                    <?php $categorias = VirtualDeskSiteVAgendaHelper::getCategoria()?>
                                    <div class="col-md-9">
                                        <select name="categoria" required value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                            <?php
                                            if(empty($categoria)){
                                                ?>
                                                <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                                <?php foreach($categorias as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['categoria']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getCatSelect($categoria) ?></option>
                                                <option value=""><?php echo '-'; ?></option>
                                                <?php $ExcludeCat = VirtualDeskSiteVAgendaHelper::excludeCat($categoria)?>
                                                <?php foreach($ExcludeCat as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['categoria']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <!--   SUBCATEGORIA   -->
                                <?php
                                // Carrega menusec se o id de menumain estiver definido.
                                $ListaDeMenuMain = array();
                                if(!empty($categoria)) {
                                    if( (int) $categoria > 0) $ListaDeSubCategoria = VirtualDeskSiteVAgendaHelper::getSubCat($categoria);
                                }
                                ?>
                                <div id="blocoSubCategoria" class="form-group">
                                    <label class="col-md-3 control-label"><?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_SUBCATEGORIA' ); ?><span class=""></span></label>
                                    <div class="col-md-9">
                                        <select name="subcategoria" id="subcategoria" value="<?php echo $subcategoria;?>"
                                            <?php
                                            if(empty($categoria)) {
                                                echo 'disabled';
                                            }
                                            ?>
                                                class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                            <?php
                                            if(empty($subcategoria)){
                                                ?>
                                                <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                                <?php foreach($ListaDeSubCategoria as $rowMM) : ?>
                                                    <option value="<?php echo $rowMM['id']; ?>"
                                                    ><?php echo $rowMM['name']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $subcategoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getSubCatSelect($subcategoria);?></option>
                                                <option value=""><?php echo '-'; ?></option>
                                                <?php $ExcludeSubCat = VirtualDeskSiteVAgendaHelper::getSubCatMissing($categoria, $subcategoria);?>
                                                <?php foreach($ExcludeSubCat as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id']; ?>"
                                                    ><?php echo $rowWSL['name']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>

                                    </div>
                                </div>


                                <!--   CONCELHO   -->
                                <input type="hidden" class="form-control"  name="concelho_evento" id="concelho_evento"   value="<?php echo $concelhoAgenda; ?>"/>

                                <!--   FREGUESIAS   -->
                                <div class="form-group" id="freg">
                                    <label class="col-md-3 control-label" for="field_freguesia"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FREGUESIA') ?><span class="required">*</span></label>

                                    <?php

                                    $AgendaFreguesias = VirtualDeskSiteVAgendaHelper::getFreg($concelhoAgenda);
                                    ?>
                                    <div class="col-md-9">
                                        <select name="freguesia_evento" value="" id="freguesia_evento" required class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                            <?php
                                            if(empty($freguesias)){
                                                ?>
                                                <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                                <?php foreach($AgendaFreguesias as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                                    ><?php echo $rowWSL['name']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $freguesias; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getFreg($freguesias) ?></option>
                                                <option value=""><?php echo '-'; ?></option>
                                                <?php $ExcludeFreg = VirtualDeskSiteVAgendaHelper::getFregMissing($concelhoAgenda, $freguesias)?>
                                                <?php foreach($ExcludeFreg as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id_freguesia']; ?>"
                                                    ><?php echo $rowWSL['name']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <!--   LOCAL   -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_LOCAL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_LOCAL'); ?>"
                                               name="local_evento" id="local_evento" maxlength="50" value="<?php echo $local_evento; ?>"/>
                                    </div>
                                </div>

                                <!--   PREÇO   -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PRECOEVENTO'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PRECOEVENTO'); ?>"
                                               name="preco_evento" id="preco_evento" maxlength="50" value="<?php echo $preco_evento; ?>"/>
                                    </div>
                                </div>

                                <!--  DATA HORA de Inicio   -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAINICIO'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="date" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAINICIO'); ?>"
                                               name="data_inicio" id="data_inicio" value="<?php echo $data_inicio; ?>"/>
                                    </div>
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAINICIO'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="time" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAINICIO'); ?>"
                                               name="hora_inicio" id="hora_inicio"  value="<?php echo $hora_inicio; ?>"/>
                                    </div>
                                </div>


                                <!--  DATA HORA de Fim   -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAFIM'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="date" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAFIM'); ?>"
                                               name="data_fim" id="data_fim" value="<?php echo $data_fim; ?>"/>
                                    </div>
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAFIM'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="time" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAFIM'); ?>"
                                               name="hora_fim" id="hora_fim"  value="<?php echo $hora_fim; ?>"/>
                                    </div>
                                </div>


                                <!--   Redes Sociais   -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FACEBOOK'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FACEBOOK'); ?>"
                                               name="facebook" id="facebook" maxlength="50" value="<?php echo $facebook; ?>"/>
                                    </div>
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_INSTAGRAM'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_INSTAGRAM'); ?>"
                                               name="instagram" id="instagram" maxlength="50" value="<?php echo $instagram; ?>"/>
                                    </div>
                                </div>

                                <!--   Redes Sociais    -->
                                <div class="form-group" id="place">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_YOUTUBE'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_YOUTUBE'); ?>"
                                               name="youtube" id="youtube" maxlength="50" value="<?php echo $youtube; ?>"/>
                                    </div>
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VIMEO'); ?><span class="required">*</span></label>
                                    <div class="col-md-3">
                                        <input type="text" required class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VIMEO'); ?>"
                                               name="vimeo" id="vimeo" maxlength="50" value="<?php echo $vimeo; ?>"/>
                                    </div>
                                </div>

                                <!--   MAPA   -->
                                <div class="form-group" id="mapa">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_MAPA'); ?></label>
                                    <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                    <input type="hidden" required class="form-control"
                                           name="coordenadas" id="coordenadas"
                                           value=""/>
                                </div>


                                <!--   Tipo de LAYOUT   -->
                                <div class="form-group" id="cat">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_TIPOLAYOUT'); ?><span class="required">*</span></label>
                                    <?php $tipolayouts = VirtualDeskSiteVAgendaHelper::getLayout()?>
                                    <div class="col-md-9">
                                        <select name="layout_evento" required value="<?php echo $tipolayout; ?>" id="layout_evento" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                            <?php
                                            if(empty($tipolayout)){
                                                ?>
                                                <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                                <?php foreach($tipolayouts as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['layout']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $tipolayout; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getLayoutSelect($tipolayout) ?></option>
                                                <option value=""><?php echo '-'; ?></option>
                                                <?php $ExcludeCat = VirtualDeskSiteVAgendaHelper::excludelayout($tipolayout)?>
                                                <?php foreach($ExcludeCat as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['layout']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>



                                <!--   Upload Imagens Capa -->
                                <div class="form-group" id="uploadFieldCapa">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_FOTOCAPA'); ?></label>
                                    <div class="col-md-9">
                                        <div class="file-loading">
                                            <input type="file" name="fileupload_capa[]" id="fileupload_capa" multiple>
                                        </div>
                                        <div id="errorBlock" class="help-block"></div>
                                    </div>
                                </div>


                                <!--   Upload Imagens Cartaz  -->
                                <div class="form-group" id="uploadFieldCartaz">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_CARTAZ'); ?></label>
                                    <div class="col-md-9">
                                        <div class="file-loading">
                                            <input type="file" name="fileupload_cartaz[]" id="fileupload_cartaz" multiple>
                                        </div>
                                        <div id="errorBlock" class="help-block"></div>
                                    </div>
                                </div>

                                <!--   Upload Imagens Galeria  -->
                                <div class="form-group" id="uploadFieldGaleria">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_GALERIA'); ?></label>
                                    <div class="col-md-9">
                                        <div class="file-loading">
                                            <input type="file" name="fileupload_galeria[]" id="fileupload_galeria" multiple>
                                        </div>
                                        <div id="errorBlock" class="help-block"></div>
                                    </div>
                                </div>




                            </div>


                            <div class="form-actions right">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                        </button>
                                        <a class="btn default"
                                           href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda'); ?>"
                                           title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('agenda.create4user',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4user',$setencrypt_forminputhidden); ?>"/>


                            <?php echo JHtml::_('form.token'); ?>

                        </form>

                    </div>

                    <div class="tab-pane " id="tab_Agenda_Estatistica">
                    </div>

                    <div class="tab-pane " id="tab_Agenda_EventosSubmetidos">
                    </div>

                </div>
            </div>
















        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/addnew4user.js.php');
echo ('</script>');
?>