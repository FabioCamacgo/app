<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaEmpresasFotoCapaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_capa_files.php');
    JLoader::register('VirtualDeskSiteVAgendaEmpresasLogosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_logos_files.php');
    JLoader::register('VirtualDeskSiteVAgendaEmpresasGaleriaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_galeria_files.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailEditAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'editEmpresas4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/empresas.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator    = ' : ';
    $params            = JComponentHelper::getParams('com_virtualdesk');
    $getEmpresa_Id = JFactory::getApplication()->input->getInt('empresa_id');


    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAgendaHelper::getEmpresaDetail4Manager($getEmpresa_Id);
    if( empty($this->data) ) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_NORESULTS'), 'error' );
        return false;
    }

    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.edit4manager.noticias.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    //se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';

    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;


    $obParam      = new VirtualDeskSiteParamsHelper();
    $IdDistrito = $obParam->getParamsByTag('distritoVagenda');


    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EMPRESAS_EDIT' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewempresas4manager&empresa_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">

            <form id="edit-empresa" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.updateempresas4manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                <div class="form-body">

                    <input type="hidden" id="referencia" name="referencia" value="<?php echo $this->data->codigo;?>">

                    <div class="bloco">
                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_IDENTIFICACAO'); ?></h3></legend>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_NOMEEMPRESA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" required class="form-control" autocomplete="off" name="nomeEmpresa" id="nomeEmpresa" maxlength="250" value="<?php echo $this->data->nome_empresa;?>"/>
                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_DESIGNACAOCOMERCIAL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="designacaoComercial" id="designacaoComercial" maxlength="250" value="<?php echo $this->data->nome_comercial; ?>"/>
                            </div>
                        </div>

                        <?php
                            if(empty($this->data->nipc) || $this->data->nipc == 0){
                                $nipc = '';
                            } else {
                                $nipc = $this->data->nipc;
                            }
                        ?>

                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_NIPC'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="nipcEmpresa" id="nipcEmpresa" maxlength="9" value="<?php echo $nipc; ?>" />
                            </div>
                        </div>


                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EMAILCONTACTO'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="emailContacto" id="emailContacto" maxlength="250" value="<?php echo htmlentities($this->data->email_contacto, ENT_QUOTES, 'UTF-8'); ?>" />
                            </div>
                        </div>

                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_DESCRICAO'); ?></label>
                            <div class="value col-md-12">
                                <textarea  class="form-control" rows="15" name="descricao" id="descricao" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao); ?></textarea>
                            </div>
                        </div>

                    </div>


                    <div class="bloco">
                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_AREAATUACAO'); ?></h3></legend>

                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_CATEGORIA'); ?><span class="required">*</span></label>
                            <?php $Categorias = VirtualDeskSiteAgendaHelper::getCategoriaEmpresa()?>
                            <div class="value col-md-12">
                                <select name="categoria" value="<?php echo $this->data->id_categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->id_categoria)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Categorias as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->id_categoria; ?>"><?php echo VirtualDeskSiteAgendaHelper::getCategoriaEmpresaName($this->data->id_categoria) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeCategoria = VirtualDeskSiteAgendaHelper::excludeCategoriaEmpresa($this->data->id_categoria)?>
                                        <?php foreach($ExcludeCategoria as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['categoria']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <?php
                        $ListaSubcategorias = array();
                        if(!empty($this->data->id_categoria)) {
                            if( (int) $this->data->id_categoria > 0) $ListaSubcategorias = VirtualDeskSiteAgendaHelper::getSubcategoriaEmpresa($this->data->id_categoria);
                        }
                        ?>
                        <div id="blocoSubcategoria" class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_SUBCATEGORIA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <select name="subcategoria[]" id="subcategoria" value="<?php echo $this->data->subcategoria;?>"
                                    <?php
                                    if(empty($this->data->id_categoria)) {
                                        echo 'disabled';
                                    }
                                    ?>
                                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true" multiple="multiple">
                                    <?php
                                    if(empty($this->data->subcategoria)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($ListaSubcategorias as $rowMM) : ?>
                                            <option value="<?php echo $rowMM['id']; ?>"
                                            ><?php echo $rowMM['name']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        $subcats = explode(',', $this->data->subcategoria);
                                        ?>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php foreach($ListaSubcategorias as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                                <?php
                                                for($i=0; $i<count($subcats); $i++){
                                                    if($subcats[$i] == $rowWSL['id']) echo 'selected';
                                                }
                                                ?>
                                            ><?php echo $rowWSL['name']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group all tipoEvento">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TIPOSEVENTOS'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">


                                <?php
                                $tipoEvento = VirtualDeskSiteAgendaHelper::getTipoEventos();

                                $tipoEventoSaved = explode(';', $this->data->tipoEventos);
                                $tipoEvento1 = 0;
                                $tipoEvento2 = 0;
                                $tipoEvento3 = 0;
                                $tipoEvento4 = 0;
                                $tipoEvento5 = 0;
                                $tipoEvento6 = 0;
                                $tipoEvento7 = 0;
                                $tipoEvento8 = 0;
                                $tipoEvento9 = 0;
                                $tipoEvento10 = 0;

                                for($i=0; $i<count($tipoEventoSaved); $i++){
                                    if($tipoEventoSaved[$i] == 1){
                                        $tipoEvento1 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 2){
                                        $tipoEvento2 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 3){
                                        $tipoEvento3 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 4){
                                        $tipoEvento4 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 5){
                                        $tipoEvento5 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 6){
                                        $tipoEvento6 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 7){
                                        $tipoEvento7 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 8){
                                        $tipoEvento8 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 9){
                                        $tipoEvento9 = 1;
                                    }

                                    if($tipoEventoSaved[$i] == 10){
                                        $tipoEvento10 = 1;
                                    }
                                }

                                foreach($tipoEvento as $rowWSL) :
                                    $idTipoEvento = $rowWSL['id'];
                                    $nameTipoEvento = $rowWSL['tipoEvento'];

                                    if($idTipoEvento == 1){
                                        ?>
                                        <span><input type="checkbox" name="checkbox1" id="checkbox1" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento1 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 2){
                                        ?>
                                        <span><input type="checkbox" name="checkbox2" id="checkbox2" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento2 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 3){
                                        ?>
                                        <span><input type="checkbox" name="checkbox3" id="checkbox3" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento3 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 4){
                                        ?>
                                        <span><input type="checkbox" name="checkbox4" id="checkbox4" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento4 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 5){
                                        ?>
                                        <span><input type="checkbox" name="checkbox5" id="checkbox5" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento5 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 6){
                                        ?>
                                        <span><input type="checkbox" name="checkbox6" id="checkbox6" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento6 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 7){
                                        ?>
                                        <span><input type="checkbox" name="checkbox7" id="checkbox7" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento7 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 8){
                                        ?>
                                        <span><input type="checkbox" name="checkbox8" id="checkbox8" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento8 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 9){
                                        ?>
                                        <span><input type="checkbox" name="checkbox9" id="checkbox9" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento9 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    } else if($idTipoEvento == 10){
                                        ?>
                                        <span><input type="checkbox" name="checkbox10" id="checkbox10" value="<?php echo $idTipoEvento ?>"<?php if ($tipoEvento10 == 1) {echo 'checked="checked"'; } ?>/><?php echo $nameTipoEvento;?></span>
                                        <?php
                                    }
                                endforeach;

                                foreach($tipoEvento as $rowWSL) :
                                    $idTipoEventoHide = $rowWSL['id'];

                                    if($idTipoEventoHide == 1){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide1" name="checkboxHide1" value="<?php echo $tipoEvento1;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 2){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide2" name="checkboxHide2" value="<?php echo $tipoEvento2;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 3){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide3" name="checkboxHide3" value="<?php echo $tipoEvento3;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 4){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide4" name="checkboxHide4" value="<?php echo $tipoEvento4;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 5){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide5" name="checkboxHide5" value="<?php echo $tipoEvento5;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 6){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide6" name="checkboxHide6" value="<?php echo $tipoEvento6;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 7){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide7" name="checkboxHide7" value="<?php echo $tipoEvento7;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 8){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide8" name="checkboxHide8" value="<?php echo $tipoEvento8;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 9){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide9" name="checkboxHide9" value="<?php echo $tipoEvento9;?>"></span>
                                        <?php
                                    } else if($idTipoEventoHide == 10){
                                        ?>
                                        <span><input type="hidden" id="checkboxHide10" name="checkboxHide10" value="<?php echo $tipoEvento10;?>"></span>
                                        <?php
                                    }
                                endforeach;

                                ?>

                            </div>
                        </div>

                        <div id="otherEvents" style="display:none;">
                            <div class="form-group all outrosEventos">
                                <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_OUTROSTIPOSEVENTOS'); ?><span class="required">*</span></label>
                                <div class="value col-md-12">
                                    <input type="text" class="form-control" name="outroTipoEvento" id="outroTipoEvento" maxlength="250" value="<?php echo $this->data->outroTipoEventos;?>" />
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="bloco">
                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_LOCALIZACAO'); ?></h3></legend>

                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_CONCELHO'); ?><span class="required">*</span></label>
                            <?php $Concelhos = VirtualDeskSiteAgendaHelper::getConcelho($IdDistrito)?>
                            <div class="value col-md-12">
                                <select name="concelho" value="<?php echo $this->data->id_concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->id_concelho)){
                                        ?>
                                        <option value=""><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($Concelhos as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['concelho']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->id_concelho; ?>"><?php echo VirtualDeskSiteAgendaHelper::getConcelhoName($this->data->id_concelho) ?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeConcelho = VirtualDeskSiteAgendaHelper::excludeConcelho($IdDistrito, $this->data->id_concelho)?>
                                        <?php foreach($ExcludeConcelho as $rowStatus) : ?>
                                            <option value="<?php echo $rowStatus['id']; ?>"
                                            ><?php echo $rowStatus['concelho']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>



                        <?php
                        $ListaFreguesias = array();
                        if(!empty($this->data->id_concelho)) {
                            if( (int) $this->data->id_concelho > 0) $ListaFreguesias = VirtualDeskSiteAgendaHelper::getFreguesia($this->data->id_concelho);
                        }
                        ?>
                        <div id="blocoFreguesia" class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_FREGUESIA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <select name="freguesia" id="freguesia" value="<?php echo $this->data->id_freguesia;?>"
                                    <?php
                                    if(empty($this->data->id_concelho)) {
                                        echo 'disabled';
                                    }
                                    ?>
                                        class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                    <?php
                                    if(empty($this->data->id_freguesia)){
                                        ?>
                                        <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_ESCOLHAOPCAO'); ?></option>
                                        <?php foreach($ListaFreguesias as $rowMM) : ?>
                                            <option value="<?php echo $rowMM['id']; ?>"
                                            ><?php echo $rowMM['name']; ?></option>
                                        <?php endforeach;
                                    } else {
                                        ?>
                                        <option value="<?php echo $this->data->id_freguesia; ?>"><?php echo VirtualDeskSiteAgendaHelper::getFregName($this->data->id_freguesia);?></option>
                                        <option value=""><?php echo '-'; ?></option>
                                        <?php $ExcludeFreguesia = VirtualDeskSiteAgendaHelper::excludeFreguesia($this->data->id_concelho, $this->data->id_freguesia);?>
                                        <?php foreach($ExcludeFreguesia as $rowWSL) : ?>
                                            <option value="<?php echo $rowWSL['id']; ?>"
                                            ><?php echo $rowWSL['name']; ?></option>
                                        <?php endforeach;
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_MORADA'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="morada" id="morada" maxlength="500" value="<?php echo $this->data->morada; ?>"/>
                            </div>
                        </div>

                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_MAPA'); ?></label>
                            <div class="value col-md-12">
                                <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                <input type="hidden" class="form-control" name="coordenadas" id="coordenadas" value="<?php echo htmlentities($this->data->coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
                            </div>
                        </div>

                    </div>


                    <div class="bloco">
                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_CONTACTOS'); ?></h3></legend>

                        <?php
                            if(empty($this->data->telefone) || $this->data->telefone == 0){
                                $telefone = '';
                            } else {
                                $telefone = $this->data->telefone;
                            }
                        ?>

                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_TELEFONE'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="telef" id="telef" maxlength="9" value="<?php echo $telefone; ?>"/>
                            </div>
                        </div>



                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_WEBSITE'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="website" id="website" maxlength="300" value="<?php echo $this->data->website; ?>"/>
                            </div>
                        </div>



                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EMAILCOMERCIAL'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="emailComercial" id="emailComercial" maxlength="250" value="<?php echo htmlentities($this->data->email_comercial, ENT_QUOTES, 'UTF-8'); ?>" />
                            </div>
                        </div>



                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EMAILADMINISTRATIVO'); ?><span class="required">*</span></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" name="emailAdministrativo" id="emailAdministrativo" maxlength="250" value="<?php echo htmlentities($this->data->email_administrativo, ENT_QUOTES, 'UTF-8'); ?>" />
                            </div>
                        </div>

                    </div>


                    <div class="bloco">
                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_REDESSOCIAIS'); ?></h3></legend>

                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_FACEBOOK'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="facebook" id="facebook" maxlength="300" value="<?php echo $this->data->facebook; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_INSTAGRAM'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="instagram" id="instagram" maxlength="300" value="<?php echo $this->data->instagram; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_YOUTUBE'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="youtube" id="youtube" maxlength="300" value="<?php echo $this->data->youtube; ?>"/>
                            </div>
                        </div>


                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_TWITTER'); ?></label>
                            <div class="value col-md-12">
                                <input type="text" class="form-control" autocomplete="off" name="twitter" id="twitter" maxlength="300" value="<?php echo $this->data->twitter; ?>"/>
                            </div>
                        </div>
                    </div>


                    <div class="bloco">

                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_MULTIMEDIA'); ?></h3></legend>


                        <div class="form-group half">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_UPLOAD'); ?></label>
                            <div class="value col-md-12">

                                <div class="file-loading">
                                    <input type="file" name="fileuploadCapa[]" id="fileuploadCapa" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>

                            </div>
                        </div>


                        <div class="form-group half right">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_UPLOAD_LOGO'); ?></label>
                            <div class="value col-md-12">

                                <div class="file-loading">
                                    <input type="file" name="fileuploadLogo[]" id="fileuploadLogo" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>

                            </div>
                        </div>


                        <div class="form-group all">
                            <label class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_UPLOAD_GALERIA'); ?></label>
                            <div class="value col-md-12">

                                <div class="file-loading">
                                    <input type="file" name="fileuploadGaleria[]" id="fileuploadGaleria" multiple>
                                </div>
                                <div id="errorBlock" class="help-block"></div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                            </button>
                            <a class="btn default"
                               href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=viewempresas4manager&empresa_id=' . $this->escape($this->data->id)); ?>"
                               title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('empresa_id',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('agenda.updateempresas4manager',$setencrypt_forminputhidden); ?>"/>
                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('listempresas4manager',$setencrypt_forminputhidden); ?>"/>

                <?php echo JHtml::_('form.token'); ?>

            </form>

        </div>


    </div>

<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/editempresas4manager.js.php');
    echo ('</script>');
?>