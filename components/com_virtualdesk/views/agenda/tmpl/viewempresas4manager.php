<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaEmpresasFotoCapaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_capa_files.php');
    JLoader::register('VirtualDeskSiteVAgendaEmpresasLogosHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_logos_files.php');
    JLoader::register('VirtualDeskSiteVAgendaEmpresasGaleriaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_VAgendaEmpresas_galeria_files.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'viewEmpresas4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/empresas.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputEmpresa_Id = JFactory::getApplication()->input->getInt('empresa_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAgendaHelper::getEmpresaDetail4Manager($getInputEmpresa_Id);

    // Carrega de ficheiros associados
    if(!empty($this->data->codigo)) {
        $ListFilesAlert = array();

        $objFotoCapaFiles = new VirtualDeskSiteVAgendaEmpresasFotoCapaHelper();
        $ListFilesFotoCapa = $objFotoCapaFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objGaleriaFiles = new VirtualDeskSiteVAgendaEmpresasGaleriaHelper();
        $ListFilesGaleria = $objGaleriaFiles->getFileGuestLinkByRefId ($this->data->codigo);

        $objLogoFiles = new VirtualDeskSiteVAgendaEmpresasLogosHelper();
        $ListFilesLogo = $objLogoFiles->getFileGuestLinkByRefId ($this->data->codigo);

    }

    $EmpresasEstadoList2Change = VirtualDeskSiteAgendaHelper::getEstadoEmpresasAllOptions($language_tag);

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_DIRETORIOSERVICOS_EMPTYLIST'), 'error' );
        return false;
    }

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_VIEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=listempresas4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editempresas4manager&empresa_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=addnewempresas4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_ADDNEW_EMPRESA' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">
                <div class="form-body">
                    <div class="portlet light">
                        <div class="col-md-8">
                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">
                                    <div class="bloco">
                                        <legend>
                                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_IDENTIFICACAO'); ?></h3>
                                        </legend>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EMPRESAS_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half state">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EMPRESAS_ESTADO' ); ?></div>
                                                <div class="value">
                                                    <span class="label <?php echo VirtualDeskSiteAgendaHelper::getEstadoEmpresaCSS($this->data->id_estado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>

                                                    <?php
                                                    $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('agenda', 'alterarestado4managers');
                                                    if($checkAlterarEstado4Managers===true ) : ?>
                                                        <div class="col-md-3" style="margin-top: -5px;">
                                                            <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_DIRETORIOSERVICOS_CHANGE_ESTADO' ); ?></button>
                                                        </div>

                                                        <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EMPRESAS_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                                    </div>
                                                                    <div class="modal-body blocoAlterar2NewEstado">

                                                                        <div class="form-group">
                                                                            <div class="row">
                                                                                <div class="col-md-12 ">
                                                                                    <div class="form-group" style="padding:0">
                                                                                        <label> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EMPRESAS_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                                        <div style="padding:0;">
                                                                                            <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                                <?php foreach($EmpresasEstadoList2Change as $rowEstado) : ?>
                                                                                                    <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->id_estado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                                <?php endforeach; ?>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group blocoIconsMsgAviso">
                                                                            <div class="row">
                                                                                <div class="col-md-12 text-center">
                                                                                    <span> <i class="fa"></i> </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12 text-center">
                                                                                    <span class="blocoIconsMsgAviso_Texto"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                        <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.sendAlterar2NewEstadoEmpresas4ManagerByAjax&empresa_id=' . $this->escape($this->data->id)); ?>" >
                                                                        <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_EMPRESAS_CHANGE' ); ?>
                                                                        </button>
                                                                        <span> <i class="fa"></i> </span>
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>

                                                    <?php endif;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_NOMEEMPRESA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nome_empresa, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_DESIGNACAOCOMERCIAL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->nome_comercial, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php
                                            if($this->data->nipc == 0 || empty($this->data->nipc)){
                                                $nipc = '';
                                            } else {
                                                $nipc = $this->data->nipc;
                                            }
                                        ?>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_NIPC'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $nipc, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_EMAILCONTACTO'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->email_contacto, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all textarea">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_DIRETORIOSERVICOS_DESCRICAO'); ?></div>
                                                <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descricao);?> </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="bloco">
                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_AREAATUACAO'); ?></h3></legend>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_CATEGORIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php
                                        $subcats = explode(',', $this->data->subcategoria);
                                        for($i=0; $i < count($subcats); $i++){
                                            if($i == 0){
                                                $subcategorias = VirtualDeskSiteAgendaHelper::getSubcategoriaEmpresaName($subcats[$i]);
                                            } else {
                                                $subcategorias .= '; ' . VirtualDeskSiteAgendaHelper::getSubcategoriaEmpresaName($subcats[$i]);
                                            }
                                        }
                                        ?>

                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_SUBCATEGORIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $subcategorias, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <?php
                                            if(!empty($this->data->tipoEventos)){
                                                $tipoEventos = explode(';', $this->data->tipoEventos);

                                                for($i = 0; $i < count($tipoEventos); $i++){
                                                    if($i == 0){
                                                        $tipoEventoEmpresa = VirtualDeskSiteAgendaHelper::getTipoEventoName($tipoEventos[$i]);
                                                    } else {
                                                        $tipoEventoEmpresa .= ', ' . VirtualDeskSiteAgendaHelper::getTipoEventoName($tipoEventos[$i]);
                                                    }
                                                }

                                                if(!empty($this->data->outroTipoEventos)){
                                                    $tipoEventoEmpresa .= ', ' . $this->data->outroTipoEventos;
                                                }


                                            } else {
                                                $tipoEventoEmpresa = '';
                                            }
                                        ?>

                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TIPOSEVENTOS'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $tipoEventoEmpresa, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="bloco">
                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_LOCALIZACAO'); ?></h3></legend>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_CONCELHO'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->concelho, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_FREGUESIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_MORADA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->morada, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_MAPA_VIEW'); ?></div>
                                                <div class="value">
                                                    <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>
                                                    <input type="hidden" class="form-control" name="coordenadas" id="coordenadas" value="<?php echo htmlentities($this->data->coordenadas, ENT_QUOTES, 'UTF-8'); ?>"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="bloco">
                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_CONTACTOS'); ?></h3></legend>

                                        <?php
                                            if(empty($this->data->telefone) || $this->data->telefone == 0){
                                                $telef = '';
                                            } else {
                                                $telef = $this->data->telefone;
                                            }
                                        ?>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_TELEFONE'); ?></div>
                                                <div class="value"> <?php echo $telef;?> </div>
                                            </div>
                                        </div>

                                        <?php
                                            $urlHttpsWebsite = explode("https://", $this->data->website);
                                            if(count($urlHttpsWebsite) == 1){
                                                $hasLinkWebsite = 0;
                                                $urlHttpWebsite = explode("http://", $this->data->website);
                                                if(count($urlHttpWebsite) == 1){
                                                    $hasLinkWebsite = 0;
                                                } else {
                                                    $hasLinkWebsite = 1;
                                                }
                                            } else {
                                                $hasLinkWebsite = 1;
                                            }

                                            if(!empty($this->data->website)){
                                                if($hasLinkWebsite == 0){
                                                    $urlWebsite = 'https://' . $this->data->website;
                                                } else {
                                                    $urlWebsite = $this->data->website;
                                                }
                                            } else {
                                                $urlWebsite = '';
                                            }
                                        ?>

                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_WEBSITE'); ?></div>
                                                <div class="value"> <?php echo '<a href="' . $urlWebsite . '" target="blank">' . $urlWebsite . '</a>';?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EMAILCOMERCIAL'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->email_comercial, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_EMAILADMINISTRATIVO'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->email_administrativo, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="bloco">
                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_REDESSOCIAIS'); ?></h3></legend>

                                        <?php
                                            $urlHttpsFacebook = explode("https://", $this->data->facebook);
                                            if(count($urlHttpsFacebook) == 1){
                                                $hasLinkFacebook = 0;
                                                $urlHttpFacebook = explode("http://", $this->data->facebook);
                                                if(count($urlHttpFacebook) == 1){
                                                    $hasLinkFacebook = 0;
                                                } else {
                                                    $hasLinkFacebook = 1;
                                                }
                                            } else {
                                                $hasLinkFacebook = 1;
                                            }

                                            if(!empty($this->data->facebook)){
                                                if($hasLinkFacebook == 0){
                                                    $urlFacebook = 'https://' . $this->data->facebook;
                                                } else {
                                                    $urlFacebook = $this->data->facebook;
                                                }
                                            } else {
                                                $urlFacebook = '';
                                            }
                                        ?>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_FACEBOOK'); ?></div>
                                                <div class="value"> <?php echo '<a href="' . $urlFacebook . '" target="blank">' . $urlFacebook . '</a>';?> </div>
                                            </div>
                                        </div>


                                        <?php
                                            $urlHttpsInstagram = explode("https://", $this->data->instagram);
                                            if(count($urlHttpsInstagram) == 1){
                                                $hasLinkInstagram = 0;
                                                $urlHttpInstagram = explode("http://", $this->data->instagram);
                                                if(count($urlHttpInstagram) == 1){
                                                    $hasLinkInstagram = 0;
                                                } else {
                                                    $hasLinkInstagram = 1;
                                                }
                                            } else {
                                                $hasLinkInstagram = 1;
                                            }

                                            if(!empty($this->data->instagram)){
                                                if($hasLinkInstagram == 0){
                                                    $urlInstagram = 'https://' . $this->data->instagram;
                                                } else {
                                                    $urlInstagram = $this->data->instagram;
                                                }
                                            } else {
                                                $urlInstagram = '';
                                            }
                                        ?>

                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_INSTAGRAM'); ?></div>
                                                <div class="value"> <?php echo '<a href="' . $urlInstagram . '" target="blank">' . $urlInstagram . '</a>';?> </div>
                                            </div>
                                        </div>

                                        <?php
                                            $urlHttpsYoutube = explode("https://", $this->data->youtube);
                                            if(count($urlHttpsYoutube) == 1){
                                                $hasLinkYoutube = 0;
                                                $urlHttpYoutube = explode("http://", $this->data->youtube);
                                                if(count($urlHttpYoutube) == 1){
                                                    $hasLinkYoutube = 0;
                                                } else {
                                                    $hasLinkYoutube = 1;
                                                }
                                            } else {
                                                $hasLinkYoutube = 1;
                                            }

                                            if(!empty($this->data->youtube)){
                                                if($hasLinkYoutube == 0){
                                                    $urlYoutube = 'https://' . $this->data->youtube;
                                                } else {
                                                    $urlYoutube = $this->data->youtube;
                                                }
                                            } else {
                                                $urlYoutube = '';
                                            }
                                        ?>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_YOUTUBE'); ?></div>
                                                <div class="value"><?php echo '<a href="' . $urlYoutube . '" target="blank">' . $urlYoutube . '</a>';?></div>
                                            </div>
                                        </div>


                                        <?php
                                            $urlHttpsTwitter = explode("https://", $this->data->twitter);
                                            if(count($urlHttpsTwitter) == 1){
                                                $hasLinkTwitter = 0;
                                                $urlHttpTwitter = explode("http://", $this->data->twitter);
                                                if(count($urlHttpTwitter) == 1){
                                                    $hasLinkTwitter = 0;
                                                } else {
                                                    $hasLinkTwitter = 1;
                                                }
                                            } else {
                                                $hasLinkTwitter = 1;
                                            }

                                            if(!empty($this->data->twitter)){
                                                if($hasLinkTwitter == 0){
                                                    $urlTwitter = 'https://' . $this->data->twitter;
                                                } else {
                                                    $urlTwitter = $this->data->twitter;
                                                }
                                            } else {
                                                $urlTwitter = '';
                                            }
                                        ?>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_TWITTER'); ?></div>
                                                <div class="value"><?php echo '<a href="' . $urlTwitter . '" target="blank">' . $urlTwitter . '</a>';?></div>
                                            </div>
                                        </div>

                                    </div>

                                    <?php
                                        if((int) $ListFilesGaleria > 0){
                                            ?>
                                                <div class="bloco">

                                                    <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_UPLOAD_GALERIA'); ?></h3></legend>

                                                    <div id="vdEmpresasFileGridGaleria" class="cbp">
                                                        <?php
                                                        if(!is_array($ListFilesGaleria)) $ListFilesGaleria = array();
                                                        foreach ($ListFilesGaleria as $rowFile) : ?>
                                                            <div class="cbp-item identity logos">
                                                                <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                                    <div class="cbp-caption-defaultWrap">
                                                                        <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                                    <div class="cbp-caption-activeWrap">
                                                                        <div class="cbp-l-caption-alignLeft">
                                                                            <div class="cbp-l-caption-body">
                                                                                <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                                <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        <?php endforeach;?>
                                                    </div>


                                                </div>
                                            <?php
                                        }
                                    ?>


                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_CAPA'); ?></h3></legend>

                                        <div id="vdEmpresasFileGridCapa" class="cbp">
                                            <?php
                                            if(!is_array($ListFilesFotoCapa)) $ListFilesFotoCapa = array();
                                            foreach ($ListFilesFotoCapa as $rowFile) : ?>
                                                <div class="cbp-item identity logos">
                                                    <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                        <div class="cbp-caption-defaultWrap">
                                                            <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                        <div class="cbp-caption-activeWrap">
                                                            <div class="cbp-l-caption-alignLeft">
                                                                <div class="cbp-l-caption-body">
                                                                    <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                    <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">

                                    <div class="bloco">

                                        <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMPRESAS_UPLOAD_LOGO'); ?></h3></legend>

                                        <div id="vdEmpresasFileGridLogo" class="cbp">
                                            <?php
                                            if(!is_array($ListFilesLogo)) $ListFilesLogo = array();
                                            foreach ($ListFilesLogo as $rowFile) : ?>
                                                <div class="cbp-item identity logos">
                                                    <a href="<?php echo$rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                        <div class="cbp-caption-defaultWrap">
                                                            <img src="<?php echo$rowFile->guestlink; ?>" alt=""> </div>
                                                        <div class="cbp-caption-activeWrap">
                                                            <div class="cbp-l-caption-alignLeft">
                                                                <div class="cbp-l-caption-body">
                                                                    <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                                    <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editempresas4manager&empresa_id=' . $this->escape($this->data->id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=listempresas4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('empresa_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/viewempresas4manager.js.php');
    echo ('</script>');
?>