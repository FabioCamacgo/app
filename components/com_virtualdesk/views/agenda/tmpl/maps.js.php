<?php
defined('_JEXEC') or die;
?>

var iconPath = '<?php echo JUri::base() . 'plugins/system/virtualdesk/layouts/includes/Espera.png'; ?>';

var MapsGoogle = function () {

    var mapMarker = function () {

        var map = new GMaps({
            div: '#gmap_marker',
            lat: LatToSet,
            lng: LongToSet
        });

        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });

        map.setZoom(12);

        <?php require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps.js.php'); ?>

    };

    var mapMarker2 = function () {
        console.log('initMap');
        var map = new GMaps({
            div: '#gmap_marker2',
            lat: LatToSet,
            lng: LongToSet
        });

        map.addMarker({
            lat: LatToSet,
            lng: LongToSet,
            title: '',
            icon: iconPath
        });

        map.setZoom(12);

        <?php require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps.js.php'); ?>

    };

    return {
        //main function to initiate map samples
        init: function () {
            mapMarker();
            mapMarker2();
        }
        , mapMarker: mapMarker
        , mapMarker2: mapMarker2

    };

}();


<?php require_once (JPATH_SITE . '/components/com_virtualdesk/helpers/Mapas/maps_default.js.php'); ?>

/*
* Inicialização
*/

jQuery(document).ready(function() {


    var InputLatLong = jQuery('#coordenadas').val();

    if( InputLatLong!=undefined && InputLatLong!="") {
        var ArrayLatLong = InputLatLong.split(",");
        if(ArrayLatLong.length == 2) {
            LatToSet  =  ArrayLatLong[0];
            LongToSet =  ArrayLatLong[1];

        }
    }

    if( jQuery('#gmap_marker').length ) {
        MapsGoogle.mapMarker()
    }

});