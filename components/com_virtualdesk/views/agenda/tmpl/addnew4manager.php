<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda.php');
    JLoader::register('VirtualDeskSiteVAgendaCapaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_fotoCapa_files.php');
    JLoader::register('VirtualDeskSiteVAgendaGaleriaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_galeria_files.php');
    JLoader::register('VirtualDeskSiteVAgendaCartazFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_cartaz_files.php');
    JLoader::register('VirtualDeskSiteVAgendaPrecarioFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/v_agenda/virtualdesksite_v_agenda_precario_files.php');

    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
    JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
    JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');

    /*
    * Check PERMISSÕES
    */
    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'addnew4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }

    // Idioma
    $app    = JFactory::getApplication();
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
        break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';

    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

    //Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');
    // Verifica se deve limpar os dados do State... ou carregar devido a a ocorrencia de um erro
    $vdcleanstate = $app->input->getInt('vdcleanstate');
    if($vdcleanstate==1) {
        VirtualDeskSiteAgendaHelper::cleanAllTmpUserState();
    }
    else {
        if (!is_array($this->data)) {
            $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4manager.agenda.data', array());
            foreach ($temp as $k => $v) {
                $this->data->{$k} = $v;
            }
        }
    }

    //se ocorrer algum erro na gravação e se foi enviado um ficheiro devemos limpar o array FILE
    if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


    $UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
    if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
    $UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
    $nome =  $UserProfileData->name;
    //$email    = $UserProfileData->email;
    $email2   = $UserProfileData->email;
    //$fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
    $telefone = $UserProfileData->phone1;

    $obParam      = new VirtualDeskSiteParamsHelper();

    $distritoAgenda      = $obParam->getParamsByTag('distritoVagenda');
    $espera              = $obParam->getParamsByTag('espera');
    $linkPolitica        = $obParam->getParamsByTag('linkPoliticaAgenda');

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_AGENDA_ADDNEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            <?php
            // Objecto inicializado com as mensagens de agenda já com o idioma correcto. Depois pode ser invocado pelo jquery validate (newregistration.js)
            ?>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <form id="new-agenda" action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=agenda.create4manager'); ?>" method="post" class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data" role="form">

                    <div class="form-body">

                        <div class="bloco">
                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DADOSPROMOTOR'); ?></h3>

                            <!--   NIF -> pesquisa mas aceita novos   -->
                            <div class="form-group" id="nif">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NIF'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="nipc" class="form-control" name="nif_evento" id="nif_evento" maxlength="9" value="<?php echo $nif_evento; ?>"/>
                                </div>
                            </div>

                            <!--   Promotor  -->
                            <div class="form-group" id="promotor">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PROMOTOR'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="nomepromotor" id="nomepromotor" maxlength="200" value="<?php echo $nomepromotor; ?>"/>
                                </div>
                            </div>


                            <!--   EMail  -->
                            <div class="form-group" id="mail">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EMAIL'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="email" class="form-control" name="email_evento" id="email_evento" maxlength="200" value="<?php echo $email_evento; ?>"/>
                                </div>
                            </div>
                        </div>



                        <div class="bloco">
                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DADOSEVENTO'); ?></h3>

                            <!--   NOME   -->
                            <div class="form-group" id="nomeEvento">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_NOMEEVENTO'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" required class="form-control" name="nome_evento" id="nome_evento" maxlength="500" value="<?php echo $nome_evento; ?>"/>
                                </div>
                            </div>


                            <!--   CATEGORIA   -->
                            <div class="form-group" id="cat">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CATEGORIA'); ?><span class="required">*</span></label>
                                <?php $categorias = VirtualDeskSiteVAgendaHelper::getCategoria()?>
                                <div class="col-md-12">
                                    <select name="categoria" value="<?php echo $categoria; ?>" id="categoria" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($categoria)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($categorias as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['categoria']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getCatSelect($categoria) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeCat = VirtualDeskSiteVAgendaHelper::excludeCat($categoria)?>
                                            <?php foreach($ExcludeCat as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['categoria']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <!--   SUBCATEGORIA   -->
                            <?php
                            // Carrega menusec se o id de menumain estiver definido.
                            $ListaDeMenuMain = array();
                            if(!empty($categoria)) {
                                if( (int) $categoria > 0) $ListaDeSubCategoria = VirtualDeskSiteVAgendaHelper::getSubCat($categoria);
                            }
                            ?>
                            <div id="blocoSubCategoria" class="form-group">
                                <label class="col-md-12"><?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_SUBCATEGORIA' ); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <select name="subcategoria" id="subcategoria" value="<?php echo $subcategoria;?>"
                                        <?php
                                        if(empty($categoria)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($subcategoria)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                            <?php foreach($ListaDeSubCategoria as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $subcategoria; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getSubCatSelect($subcategoria);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeSubCat = VirtualDeskSiteVAgendaHelper::getSubCatMissing($categoria, $subcategoria);?>
                                            <?php foreach($ExcludeSubCat as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>


                            <div class="mt-repeater tags">
                                <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TAGS_HEADER'); ?></h3>
                                <div data-repeater-list="TagsInput">
                                    <div data-repeater-item class="mt-repeater-item ">

                                        <div class="mt-repeater-input TagsInput TagsInput-new" data-provides="TagsInput">

                                            <div class="form-group all" data-trigger="TagsInput">
                                                <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_TAGS'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="tags" maxlength="100" value="<?php echo $tags; ?>"/>
                                                </div>
                                            </div>
                                            <a href="javascript:;" class="btn btn-danger mt-repeater-delete TagsInput-exists" data-repeater-delete data-dismiss="TagsInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>
                                        </div>

                                        <div class="mt-repeater-input">
                                        </div>

                                    </div>

                                </div>

                                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                    <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                            </div>


                            <!--   DESCRIÇAO   -->
                            <div class="form-group" id="desc">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DESCRICAO'); ?></label>
                                <div class="col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="12"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DESCRICAO'); ?>"
                                               name="descricao_evento" id="descricao_evento" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($descricao_evento); ?></textarea>
                                </div>
                            </div>


                            <!--  DATA de Inicio   -->
                            <div class="form-group" id="dataInicio">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAINICIO'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAINICIO'); ?>"
                                           name="data_inicio" id="data_inicio" value="<?php echo $data_inicio; ?>"/>
                                </div>
                            </div>


                            <!--  HORA de Inicio   -->
                            <div class="form-group" id="horaInicio">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAINICIO'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="time" class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAINICIO'); ?>"
                                           name="hora_inicio" id="hora_inicio"  value="<?php echo $hora_inicio; ?>"/>
                                </div>
                            </div>


                            <!--  DATA de Fim   -->
                            <div class="form-group" id="dataFim">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAFIM'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DATAFIM'); ?>"
                                           name="data_fim" id="data_fim" value="<?php echo $data_fim; ?>"/>
                                </div>
                            </div>


                            <!--  HORA de Fim   -->
                            <div class="form-group" id="horaFim">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAFIM'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="time" class="form-control" placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_HORAFIM'); ?>"
                                           name="hora_fim" id="hora_fim"  value="<?php echo $hora_fim; ?>"/>
                                </div>
                            </div>


                            <!--   Upload Imagens Capa -->
                            <div class="form-group" id="uploadFieldCapa">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_FOTOCAPA'); ?></label>
                                <div class="col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_capa[]" id="fileupload_capa" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>


                            <!--   Upload Imagens Cartaz  -->
                            <div class="form-group" id="uploadFieldCartaz">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_CARTAZ'); ?></label>
                                <div class="col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_cartaz[]" id="fileupload_cartaz" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>

                            <!--   Upload Imagens Galeria  -->
                            <div class="form-group" id="uploadFieldGaleria">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_GALERIA'); ?></label>
                                <div class="col-md-12">
                                    <div class="file-loading">
                                        <input type="file" name="fileupload_galeria[]" id="fileupload_galeria" multiple>
                                    </div>
                                    <div id="errorBlock" class="help-block"></div>
                                </div>
                            </div>
                        </div>



                        <div class="bloco">
                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_LOCALIZACAOEVENTO'); ?></h3>


                            <!--   Concelho   -->
                            <div class="form-group" id="conc">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_CONCELHO'); ?><span class="required">*</span></label>
                                <?php $concelhosList = VirtualDeskSiteVAgendaHelper::getConcelho($distritoAgenda)?>
                                <div class="col-md-12">
                                    <select name="concelho" value="<?php echo $concelho; ?>" id="concelho" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($concelho)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($concelhosList as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['concelho']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $concelho; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getConcSelect($distritoAgenda, $concelho) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeConc = VirtualDeskSiteVAgendaHelper::excludeConc($distritoAgenda, $concelho)?>
                                            <?php foreach($ExcludeConc as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['concelho']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <!--   FREGUESIAS   -->
                            <?php
                            // Carrega menusec se o id de menumain estiver definido.
                            $ListaDeMenuMain = array();
                            if(!empty($concelho)) {
                                if( (int) $concelho > 0) $ListaDeFregs = VirtualDeskSiteVAgendaHelper::getFreg($concelho);
                            }
                            ?>
                            <div class="form-group" id="blocoFreg">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FREGUESIA') ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <select name="freguesia_evento" id="freguesia_evento" value="<?php echo $freguesia_evento;?>"
                                        <?php
                                        if(empty($concelho)) {
                                            echo 'disabled';
                                        }
                                        ?>
                                            class="form-control select2 select2-hidden-accessible select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($freguesia_evento)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_ALERTA_OPCAO'); ?></option>
                                            <?php foreach($ListaDeFregs as $rowMM) : ?>
                                                <option value="<?php echo $rowMM['id']; ?>"
                                                ><?php echo $rowMM['name']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $freguesia_evento; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getFregSelect($freguesia_evento);?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludeFreg = VirtualDeskSiteVAgendaHelper::getFregMissing($concelho, $freguesia_evento);?>
                                            <?php foreach($ExcludeFreg as $rowWSL) : ?>
                                                <option value="<?php echo $rowWSL['id']; ?>"
                                                ><?php echo $rowWSL['name']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <!--   LOCAL   -->
                            <div class="form-group" id="place">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_LOCAL'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="local_evento" id="local_evento" maxlength="250" value="<?php echo $local_evento; ?>"/>
                                </div>
                            </div>


                            <!--   MAPA   -->
                            <div class="form-group" id="mapa">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_AGENDA_MAPA'); ?></label>
                                <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                <input type="hidden" class="form-control" name="coordenadas" id="coordenadas" value=""/>
                            </div>
                        </div>


                        <div class="bloco">
                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PRECOS'); ?></h3>

                            <!--   Preco   -->
                            <div class="form-group" id="price">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_PRECOEVENTO'); ?><span class="required">*</span></label>
                                <?php $Precos = VirtualDeskSiteVAgendaHelper::getPreco()?>
                                <div class="col-md-12">
                                    <select name="preco" value="<?php echo $preco; ?>" id="preco" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                        <?php
                                        if(empty($preco)){
                                            ?>
                                            <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                            <?php foreach($Precos as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipo']; ?></option>
                                            <?php endforeach;
                                        } else {
                                            ?>
                                            <option value="<?php echo $preco; ?>"><?php echo VirtualDeskSiteVAgendaHelper::getPrecoSelect($preco) ?></option>
                                            <option value=""><?php echo '-'; ?></option>
                                            <?php $ExcludePreco = VirtualDeskSiteVAgendaHelper::excludePreco($preco)?>
                                            <?php foreach($ExcludePreco as $rowStatus) : ?>
                                                <option value="<?php echo $rowStatus['id']; ?>"
                                                ><?php echo $rowStatus['tipo']; ?></option>
                                            <?php endforeach;
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="blocoPrecario">

                                <!-- Descricao Precario -->
                                <div class="form-group" id="descPrec">
                                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DESCRICAO_PRECARIO'); ?></label>
                                    <div class="col-md-12">
                                    <textarea  class="form-control wysihtml5" rows="12"  placeholder="<?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DESCRICAO_PRECARIO_V2'); ?>"
                                               name="observacoes_precario" id="observacoes_precario" maxlength="5000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($observacoes_precario); ?></textarea>
                                    </div>
                                </div>


                                <!--   Upload Imagens Precario -->
                                <div class="form-group" id="uploadFieldPrecario">
                                    <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FOTOPRECARIO'); ?></label>
                                    <div class="col-md-12">
                                        <div class="file-loading">
                                            <input type="file" name="fileupload_precario[]" id="fileupload_precario" multiple>
                                        </div>
                                        <div id="errorBlock" class="help-block"></div>
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="bloco">
                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_REDESOCIAIS'); ?></h3>

                            <!--   Facebook   -->
                            <div class="form-group" id="face">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_FACEBOOK'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="facebook" id="facebook" maxlength="500" value="<?php echo $facebook; ?>"/>
                                </div>
                            </div>


                            <!--   instagram   -->
                            <div class="form-group" id="insta">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_INSTAGRAM'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="instagram" id="instagram" maxlength="500" value="<?php echo $instagram; ?>"/>
                                </div>
                            </div>


                            <!--   youtube   -->
                            <div class="form-group" id="canalYoutube">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_YOUTUBE'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="youtube" id="youtube" maxlength="500" value="<?php echo $youtube; ?>"/>
                                </div>
                            </div>


                            <!--   vimeo   -->
                            <div class="form-group" id="canalVimeo">
                                <label class="col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VIMEO'); ?><span class="required">*</span></label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="vimeo" id="vimeo" maxlength="500" value="<?php echo $vimeo; ?>"/>
                                </div>
                            </div>
                        </div>


                        <div class="bloco">

                            <legend><h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_MULTIMEDIA'); ?></h3></legend>

                            <div class="mt-repeater video ">
                                <div data-repeater-list="VideoInput">
                                    <div data-repeater-item class="mt-repeater-item ">

                                        <div class="mt-repeater-input VideoInput VideoInput-new" data-provides="VideoInput">

                                            <div class="form-group all" data-trigger="VideoInput">
                                                <div class="name col-md-12"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VIDEOS'); ?></div>
                                                <div class="value col-md-12">
                                                    <input type="text" class="form-control" autocomplete="off" placeholder="" name="video" maxlength="500" value="<?php echo $video; ?>"/>
                                                </div>

                                                <div class="name col-md-12 second"><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_VIDEOS_CATEGORIA'); ?></div>
                                                <div class="value col-md-12">
                                                    <?php $videoCategoria = VirtualDeskSiteVAgendaHelper::getVideoCat()?>
                                                    <select name="catVideo" value="<?php echo $catVideo; ?>" id="catVideo" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                                        <option><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_ESCOLHAOPCAO'); ?></option>
                                                        <?php foreach($videoCategoria as $rowStatus) : ?>
                                                            <option value="<?php echo $rowStatus['id']; ?>"
                                                            ><?php echo $rowStatus['categoria']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>

                                            </div>


                                            <a href="javascript:;" class="btn btn-danger mt-repeater-delete VideoInput-exists" data-repeater-delete data-dismiss="VideoInput"> <?php echo JText::_( 'COM_VIRTUALDESK_REMOVE' ); ?></a>

                                        </div>

                                        <div class="mt-repeater-input">
                                        </div>

                                    </div>

                                </div>

                                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                    <i class="fa fa-plus"></i> <?php echo JText::_( 'COM_VIRTUALDESK_ADD' ); ?></a>

                            </div>

                        </div>
                    </div>


                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                </button>
                                <a class="btn default"
                                   href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda'); ?>"
                                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                            </div>
                        </div>
                    </div>


                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('agenda.create4manager',$setencrypt_forminputhidden); ?>"/>
                    <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4manager',$setencrypt_forminputhidden); ?>"/>


                    <?php echo JHtml::_('form.token'); ?>

                </form>
            </div>

        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/addnew4manager.js.php');
echo ('</script>');
?>