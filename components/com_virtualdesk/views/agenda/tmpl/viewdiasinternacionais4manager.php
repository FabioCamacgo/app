<?php
    /**
     * @package     Joomla.Site
     * @subpackage  com_users
     *
     * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
     * @license     GNU General Public License version 2 or later; see LICENSE.txt
     */

    defined('_JEXEC') or die;
    defined('DS') or define('DS', DIRECTORY_SEPARATOR);

    JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
    JLoader::register('VirtualDeskSiteAgendaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_agenda.php');
    JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
    JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');


    $objCheckPerm = new VirtualDeskSitePermissionsHelper();
    $objCheckPerm->loadPermission();
    $vbHasAccess  = $objCheckPerm->checkDetailReadAccess('agenda');
    $vbHasAccess2 = $objCheckPerm->checkLayoutAccess('agenda', 'viewdiainternacional4managers'); // verifica permissão acesso ao layout para editar
    $vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
    if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
        return false;
    }


    // Idioma
    $jinput = JFactory::getApplication()->input;
    $language_tag = $jinput->get('lang', 'pt-PT', 'string');

    // Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
    switch($language_tag)
    { case 'pt-PT':
        $fileLangSufix = 'pt_PT';
        break;
        default:
            $fileLangSufix = substr($language_tag, 0, 2);
            break;
    }

    // Carregamentos extra de scripts e Styles css
    $baseurl       = JUri::base();
    $addscript_ini = '<script src="';
    $addscript_end = '" type="text/javascript"></script>';
    $templateName  = 'virtualdesk';


    $localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
    $localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

    $doc  = JFactory::getDocument();
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
    $doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
    $doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/agenda/tmpl/diasinternacionais.css');


    // Parâmetros
    $labelseparator = ' : ';
    $params         = JComponentHelper::getParams('com_virtualdesk');

    $getInputDiaInternacional_Id = JFactory::getApplication()->input->getInt('diainternacional_id');

    // Carregamento de Dados
    $this->data = array();
    $this->data = VirtualDeskSiteAgendaHelper::getDiasInternacionaisDetail4Manager($getInputDiaInternacional_Id);

    // Crypt Inpout Hidden
    $setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
    $obVDCrypt = new VirtualDeskSiteCryptHelper();

    // Dados vazios...
    if(empty($this->data)) {
        JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_DIASINTERNACIONAIS_EMPTYLIST'), 'error' );
        return false;
    }

?>


    <div class="portlet light bordered form-fit">
        <div class="portlet-title">

            <div class="caption">
                <i class="icon-calendar  font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_DIASINTERNACIONAIS_VIEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=listdiasinternacionais4manager&vdcleanstate=1'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editdiasinternacionais4manager&diainternacional_id=' . $this->escape($this->data->id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_EDITAR_DIASINTERNACIONAIS'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=addnewdiasinternacionais4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_ADDNEW_DIASINTERNACIONAIS' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
            <!-- END TITLE ACTIONS -->

        </div>

        <div class="portlet-body form">
            <form class="form-horizontal form-bordered">
                <div class="form-body">
                    <div class="portlet light">
                        <div class="col-md-12">
                            <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                                <div class="portlet-body">
                                    <div class="bloco">
                                        <legend>
                                            <h3><?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_VIEW'); ?></h3>
                                        </legend>

                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_REFERENCIA' ); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->referencia, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half state">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_ESTADO' ); ?></div>
                                                <div class="value">
                                                    <span class="label <?php echo VirtualDeskSiteAgendaHelper::getEstadoDiasInternacionaisCSS($this->data->id_estado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group all">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_NOME'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->dia, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group all descritivo">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_DESCRICAO'); ?></div>
                                                <div class="value"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->descritivo); ?></div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_CATEGORIA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                        <?php
                                            $explodeDataInicio = explode('-', $this->data->data_inicio);
                                            $dataInicio = $explodeDataInicio[2] . '-' . $explodeDataInicio[1] . '-' . $explodeDataInicio[0];
                                        ?>


                                        <div class="form-group half right">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_DATA'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $dataInicio, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>


                                        <div class="form-group half">
                                            <div class="row static-info ">
                                                <div class="name"> <?php echo JText::_('COM_VIRTUALDESK_VAGENDA_DIASINTERNACIONAIS_PATROCINADOR'); ?></div>
                                                <div class="value"> <?php echo htmlentities( $this->data->patrocinador, ENT_QUOTES, 'UTF-8');?> </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=editdiasinternacionais4manager&diainternacional_id=' . $this->escape($this->data->id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                            <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=agenda&layout=listdiasinternacionais4manager'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('diainternacional_id',$setencrypt_forminputhidden); ?>" value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->id) ,$setencrypt_forminputhidden); ?>"/>

            </form>
        </div>

    </div>
    </div>


<?php
    echo $localScripts;
    echo ('<script>');
    require_once (JPATH_SITE . '/components/com_virtualdesk/views/agenda/tmpl/viewdiasinternacionais4manager.js.php');
    echo ('</script>');
?>