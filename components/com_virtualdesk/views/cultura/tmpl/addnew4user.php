<?php

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');
JLoader::register('VirtualDeskSiteCulturaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura_files.php');

JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');
JLoader::register('VirtualDeskSiteUserHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_user.php');
JLoader::register('VirtualDeskSiteParamsHelper',JPATH_SITE.'/components/com_virtualdesk/helpers/virtualdesksite_params.php');


/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailAddNewAccess('cultura');
if($vbHasAccess===false ) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$app    = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
    break;
}

$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';

$localScripts  = $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/jquery.validate.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/additional-methods.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'administrator/components/com_virtualdesk/helpers/virtualdesknifcheck.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-validation/js/localization/messages_'.$fileLangSufix.'.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;
$localScripts .= $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-repeater/jquery.repeater.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;

$localScripts .= $addscript_ini . $baseurl . 'components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.js' . $addscript_end;

//$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.all.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/js/ckeditor/ckeditor.js' . $addscript_end;

$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-sweetalert/sweetalert.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');

$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/font/font-fileuploader.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/helpers/assets/fileuploader/jquery.fileuploader-theme-thumbnails.css');

//$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/wysihtml5-bower/bootstrap3-wysihtml5.min.css');

//Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');

$vdcleanstate = $app->input->getInt('vdcleanstate');
if($vdcleanstate==1) {
    VirtualDeskSiteCulturaHelper::cleanAllTmpUserState();
}
else {
    if (!is_array($this->data)) {
        $temp = (array)JFactory::getApplication()->getUserState('com_virtualdesk.addnew4user.cultura.data', array());
        foreach ($temp as $k => $v) {
            $this->data->{$k} = $v;
        }
    }
}

if(!empty($this->data->attachment)) if (is_array($this->data->attachment)) $this->data->attachment = '';


$UserJoomlaID   = VirtualDeskSiteUserHelper::getUserSessionId();
if( empty($UserJoomlaID) || (int)$UserJoomlaID<=0 )  return false;
$UserProfileData = VirtualDeskSiteUserHelper::getUserFromJoomlaID($UserJoomlaID);
$nome =  $UserProfileData->name;
$email    = $UserProfileData->email;
$email2   = $UserProfileData->email;
$fiscalid = VirtualDeskSiteUserHelper::getFiscalNumberFromJoomlaUser($UserJoomlaID);
$telefone = $UserProfileData->phone1;

$obParam      = new VirtualDeskSiteParamsHelper();


$concelhoCultura      = $obParam->getParamsByTag('concelhoCultura');
$espera              = $obParam->getParamsByTag('espera');
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');


// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();
?>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-calendar font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
                <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_ADDNEW' ); ?></span>
            </div>

            <!-- BEGIN TITLE ACTIONS -->
            <div class="actions">
                <a class="btn btn-circle btn-default" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=addnew4user'); ?>"
                   title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>">
                    <i class="fa fa-ban"></i>
                    <?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
            </div>
        </div>


        <div id="MainMessageAlertBlock" class="alert alert-danger fade in" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>&nbsp;<span></span>
        </div>

        <script>
            var MessageAlert = new function() {
                this.getRequiredMissed = function (nErrors) {
                    if (nErrors == null)
                    { return(''); }
                    if(nErrors==1)
                    { return '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_ONEFIELDS'); ?>'; }
                    else  if(nErrors>1)
                    { var msg = '<?php echo JText::_('COM_VIRTUALDESK_FORMVALID_REQUIRED_NFIELDS'); ?>';
                        return (msg.replace("%s",nErrors) );
                    }
                };
            }
        </script>


        <div class="portlet-body form">


            <div class="tabbable-line nav-justified ">
                <ul class="nav nav-tabs">
                    <li class="active">

                        <a href="#tab_Cultura_NovoEvento" data-toggle="tab">
                            <h4>
                                <i class="fa fa-plus"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_CULTURA_ADDNEW'); ?>
                            </h4>
                        </a>

                    </li>

                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=list4user#tab_Cultura_Estatistica'); ?>" >
                            <h4>
                                <i class="fa fa-bar-chart"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_PAGINAHOME_TAB_ESTATISTICA'); ?>
                            </h4>
                        </a>

                    </li>
                    <li class="">
                        <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=list4user#tab_Cultura_EventosSubmetidos'); ?>" >
                            <h4>
                                <i class="fa fa-tasks"></i>
                                <?php echo JText::_('COM_VIRTUALDESK_CULTURA_TAB_EVENTOSSUBMETIDOS'); ?>
                            </h4>
                        </a>
                    </li>

                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="tab_Cultura_NovoEvento">

                        <form id="new-cultura"
                              action="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=cultura.create4user'); ?>" method="post"
                              class="register-form login-form  form-validate form-horizontal well " enctype="multipart/form-data"
                              role="form">



                            <div class="form-body">

                                <legend><h3 class=""><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INFO_EVENTO'); ?></h3></legend>

                                <!-- Coorganizador -->
                                <div class="form-group" id="coOrganizador">
                                    <label class="col-md-3 control-label"><?php echo JText::sprintf('COM_VIRTUALDESK_CULTURA_COORGANIZACAO', $copyrightAPP); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="radio" name="radioval" id="sim" value="sim" <?php if (isset($_POST["submitForm"]) && $_POST['coOrganizacao'] == 1) {echo 'checked="checked"'; } ?>> <?php echo 'Sim'; ?>
                                        <input type="radio" name="radioval" id="nao" value="nao" <?php if (isset($_POST["submitForm"]) && $_POST['coOrganizacao'] == 2) { echo 'checked="checked"'; } ?>> <?php echo 'Não'; ?>
                                    </div>

                                    <input type="hidden" required id="coOrganizacao" name="coOrganizacao" value="<?php echo $coOrganizacao; ?>">
                                </div>


                                <?php
                                if($coOrganizacao == 1){
                                    ?>
                                    <script>
                                        document.getElementById('sim').onclick = function() {
                                            document.getElementById("coOrganizacao").value = 1;
                                        };

                                        document.getElementById('nao').onclick = function() {
                                            document.getElementById("coOrganizacao").value = 2;
                                        };

                                    </script>
                                <?php
                                } else{
                                ?>
                                    <script>
                                        document.getElementById('sim').onclick = function() {
                                            document.getElementById("coOrganizacao").value = 1;
                                        };

                                        document.getElementById('nao').onclick = function() {
                                            document.getElementById("coOrganizacao").value = 2;
                                        };

                                    </script>

                                    <?php
                                }
                                ?>

                                <div class="form-group" id="eventName">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_NOMEEVENTO_LABEL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" required class="form-control" name="nome_evento" id="nome_evento" maxlength="250" value="<?php echo $nome_evento; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="desc">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DESCRICAO_LABEL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <textarea required class="form-control wysihtml5" rows="12" name="desc_evento" id="desc_evento" maxlength="4000"><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($desc_evento); ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group" id="cat">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_CATEGORIA_LABEL'); ?><span class="required">*</span></label>
                                    <?php $categorias = VirtualDeskSiteCulturaHelper::getCategoria()?>
                                    <div class="col-md-9">
                                        <select name="cat_evento" required id="cat_evento" class="form-control select2 select2-search" tabindex="-1" >
                                            <?php
                                            if(empty($categoria)){
                                                ?>
                                                <option value=""></option>
                                                <?php foreach($categorias as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['categoria']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $categoria; ?>"><?php echo VirtualDeskSiteCulturaHelper::getCatSelect($categoria) ?></option>
                                                <option value=""></option>
                                                <?php $ExcludeCat = VirtualDeskSiteCulturaHelper::excludeCat($categoria)?>
                                                <?php foreach($ExcludeCat as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['categoria']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="freg">
                                    <label class="col-md-3 control-label" for="field_freguesia_evento"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FREGUESIAS_LABEL') ?><span class="required">*</span></label>

                                    <?php $CulturaFreguesias = VirtualDeskSiteCulturaHelper::getCulturaFreguesia($concelhoCultura); ?>
                                    <div class="col-md-9">
                                        <select name="freguesia_evento" id="freguesia_evento" required class="form-control select2 select2-search" tabindex="-1" >
                                            <?php
                                            if(empty($freguesia_evento)){
                                                ?>
                                                <option value=""></option>
                                                <?php foreach($CulturaFreguesias as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id']; ?>"
                                                    ><?php echo $rowWSL['name']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $freguesia_evento; ?>"><?php echo VirtualDeskSiteCulturaHelper::getFregSelect($freguesia_evento) ?></option>
                                                <option value=""></option>
                                                <?php $ExcludeFreg = VirtualDeskSiteCulturaHelper::excludeFreguesia($concelhoCultura, $freguesia_evento)?>
                                                <?php foreach($ExcludeFreg as $rowWSL) : ?>
                                                    <option value="<?php echo $rowWSL['id']; ?>"
                                                    ><?php echo $rowWSL['name']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="local">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LOCALEVENTO_LABEL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" required class="form-control" name="local_evento" id="local_evento" maxlength="250" value="<?php echo $local_evento; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="beginDate">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAINICIO_LABEL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="date" required class="form-control" name="data_inicio" id="data_inicio" value="<?php echo $data_inicio; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="beginHour">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAINICIO_LABEL'); ?></label>
                                    <div class="col-md-9">
                                        <input type="time" class="form-control" name="hora_inicio" id="hora_inicio"  value="<?php echo $hora_inicio; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="endDate">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DATAFIM_LABEL'); ?><span class="required">*</span></label>
                                    <div class="col-md-9">
                                        <input type="date" required class="form-control" name="data_fim" id="data_fim" value="<?php echo $data_fim; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="endHour">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_HORAFIM_LABEL'); ?></label>
                                    <div class="col-md-9">
                                        <input type="time" class="form-control" name="hora_fim" id="hora_fim"  value="<?php echo $hora_fim; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="web">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_WEBSITEEVENTO_LABEL'); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="website" id="website" maxlength="250" value="<?php echo $website; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="face">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_FACEBOOKEVENTO_LABEL'); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="facebook" id="facebook" maxlength="250" value="<?php echo $facebook; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="inst">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INSTAGRAMEVENTO_LABEL'); ?></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="instagram" id="instagram" maxlength="250" value="<?php echo $instagram; ?>"/>
                                    </div>
                                </div>

                                <div class="form-group" id="mapa">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_MAPA'); ?></label>
                                    <div class="col-md-9">
                                        <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                        <input type="hidden" class="form-control"
                                               name="coordenadas" id="coordenadas"
                                               value=""/>
                                    </div>
                                </div>

                                <div class="form-group" id="uploadField">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_IMAGENS'); ?></label>
                                    <div class="col-md-9">
                                        <div class="file-loading">
                                            <input type="file" name="fileupload[]" id="fileupload" multiple>
                                        </div>
                                        <div id="errorBlock" class="help-block"></div>
                                    </div>
                                </div>

                                <div class="form-group" id="layout">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_LAYOUT'); ?><span class="required">*</span></label>
                                    <?php $tipolayouts = VirtualDeskSiteCulturaHelper::getLayout()?>
                                    <div class="col-md-9">
                                        <select name="layout_evento" required value="<?php echo $tipolayout; ?>" id="layout_evento" class="form-control select2 select2-search" tabindex="-1" aria-hidden="true">
                                            <?php
                                            if(empty($tipolayout)){
                                                ?>
                                                <option value=""></option>
                                                <?php foreach($tipolayouts as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['layout']; ?></option>
                                                <?php endforeach;
                                            } else {
                                                ?>
                                                <option value="<?php echo $tipolayout; ?>"><?php echo VirtualDeskSiteCulturaHelper::getLayoutSelect($tipolayout) ?></option>
                                                <option value=""><?php echo '-'; ?></option>
                                                <?php $ExcludeCat = VirtualDeskSiteCulturaHelper::excludelayout($tipolayout)?>
                                                <?php foreach($ExcludeCat as $rowStatus) : ?>
                                                    <option value="<?php echo $rowStatus['id']; ?>"
                                                    ><?php echo $rowStatus['layout']; ?></option>
                                                <?php endforeach;
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="obs">
                                    <label class="col-md-3 control-label"><?php echo JText::_('COM_VIRTUALDESK_CONTACTUS_DESCRIPTION4_LABEL'); ?></label>
                                    <div class="col-md-9">
                                        <textarea  class="form-control wysihtml5" rows="6" maxlength="500" name="observacoes"  id="observacoes" ><?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($observacoes); ?></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn green"><span><?php echo JText::_('COM_VIRTUALDESK_SUBMIT'); ?></span>
                                        </button>
                                        <a class="btn default"
                                           href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=addnew4user'); ?>"
                                           title="<?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?>"><?php echo JText::_('COM_VIRTUALDESK_CANCEL'); ?></a>
                                    </div>
                                </div>
                            </div>


                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('option',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('com_virtualdesk',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('task',$setencrypt_forminputhidden); ?>"             value="<?php echo $obVDCrypt->formInputValueEncrypt('cultura.create4user',$setencrypt_forminputhidden); ?>"/>
                            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('layout',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt('list4user',$setencrypt_forminputhidden); ?>"/>


                            <?php echo JHtml::_('form.token'); ?>

                        </form>

                    </div>

                    <div class="tab-pane " id="tab_Cultura_Estatistica">
                    </div>

                    <div class="tab-pane " id="tab_Cultura_EventosSubmetidos">
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/cultura/tmpl/addnew4user.js.php');
echo ('</script>');
?>