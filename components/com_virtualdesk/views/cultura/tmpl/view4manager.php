<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

JLoader::register('VirtualDeskSiteGeneralHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_general.php');
JLoader::register('VirtualDeskSiteCulturaHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura.php');
JLoader::register('VirtualDeskSiteCulturaFilesHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_cultura_files.php');
JLoader::register('VirtualDeskSiteCryptHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_crypt.php');
JLoader::register('VirtualDeskSitePermissionsHelper', JPATH_SITE . '/components/com_virtualdesk/helpers/virtualdesksite_permissions.php');

/*
* Check PERMISSÕES
*/
$objCheckPerm = new VirtualDeskSitePermissionsHelper();
$objCheckPerm->loadPermission();
$vbHasAccess  = $objCheckPerm->checkDetailReadAccess('cultura');
$vbHasAccess2 = $objCheckPerm->checkLayoutAccess('cultura', 'view4managers'); // verifica permissão acesso ao layout para editar
$vbInGroupAM  = $objCheckPerm->checkSessionUserInAppAdminOrManager(); // Verifica se está no grupo Manager ou Admin da APP
if($vbHasAccess===false || $vbHasAccess2===false || $vbInGroupAM ==false) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_PERMISSIONS_NOTALLOWED'), 'error' );
    return false;
}


// Idioma
$jinput = JFactory::getApplication()->input;
$language_tag = $jinput->get('lang', 'pt-PT', 'string');

// Define um sufixo para ser utilizado no carregamento de ficheiros com idiomas/traduções
switch($language_tag)
{ case 'pt-PT':
    $fileLangSufix = 'pt_PT';
    break;
    default:
        $fileLangSufix = substr($language_tag, 0, 2);
        break;
}

// Carregamentos extra de scripts e Styles css
$baseurl       = JUri::base();
$addscript_ini = '<script src="';
$addscript_end = '" type="text/javascript"></script>';
$templateName  = 'virtualdesk';


$localScripts = $addscript_ini . 'https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDwZUUW7PkkV1cuzaSxRYMQ6GSwih12YMU' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/gmaps/gmaps.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery-ui/jquery-ui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/jquery.blockui.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'. $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/scripts/datatable.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/datatables.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/pages/scripts/ui-modals.min.js' . $addscript_end;
$localScripts .= $addscript_ini . $baseurl . 'templates/' . $templateName . '/assets/global/plugins/select2/js/select2.full.min.js' . $addscript_end;


$doc  = JFactory::getDocument();
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/profile.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/pages/css/portfolio.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/datatables.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/font-awesome/css/font-awesome.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2.min.css');
$doc->addStyleSheetVersion($this->baseurl . '/templates/virtualdesk/assets/global/plugins/select2/css/select2-bootstrap.min.css');


// Cultura - CSS Comum
$doc->addStyleSheetVersion($this->baseurl . '/components/com_virtualdesk/views/cultura/tmpl/cultura-comum.css');

// Parâmetros
$labelseparator = ' : ';
$params         = JComponentHelper::getParams('com_virtualdesk');

$getInputCultura_Id = JFactory::getApplication()->input->getInt('cultura_id');

$obParam      = new VirtualDeskSiteParamsHelper();
$copyrightAPP = $obParam->getParamsByTag('copyrightAPP');

// Carregamento de Dados
$this->data = array();
$this->data = VirtualDeskSiteCulturaHelper::getCulturaView4ManagerDetail($getInputCultura_Id);

// Carrega de ficheiros associados
if(!empty($this->data->codigo)) {
    $objCulturaFiles  = new VirtualDeskSiteCulturaFilesHelper();
    $ListFilesCultura = $objCulturaFiles->getFileGuestLinkByRefId ($this->data->codigo);
}

$CulturaEstadoList2Change = VirtualDeskSiteCulturaHelper::getCulturaEstadoAllOptions($language_tag);

// Crypt Inpout Hidden
$setencrypt_forminputhidden = $params->get('setencrypt_forminputhidden');
$obVDCrypt = new VirtualDeskSiteCryptHelper();

// Dados vazios...
if(empty($this->data)) {
    JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_VIRTUALDESK_ALERTA_EMPTYLIST'), 'error' );
    return false;
}


?>

<div class="portlet light bordered form-fit">
   <div class="portlet-title">

        <div class="caption">
            <i class="icon-calendar  font-dark"></i>
            <span class="caption-subject font-dark sbold uppercase"><?php echo $this->escape($this->params->get('page_heading')); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_LISTA' ); ?></span>
            <span class="caption-subject font-dark "><i class="fa fa-chevron-right font-grey"></i> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_VER_DETALHE' ); ?></span>
        </div>

        <!-- BEGIN TITLE ACTIONS -->
        <div class="actions">
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura'); ?>" class="btn btn-circle btn-default">
                    <i class="fa fa-arrow-left"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_GOBACK' ); ?>  </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=edit4manager&vdcleanstate=1&cultura_id=' . $this->escape($this->data->cultura_id)); ?>" class="btn btn-circle btn-outline green">
                    <i class="fa fa-pencil"></i>  <?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?> </a>
                <a href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=addnew4manager&vdcleanstate=1'); ?>" class="btn btn-circle blue-steel btn-outline">
                    <i class="fa fa-plus"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_ADDNEW' ); ?></a>

                <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <!-- END TITLE ACTIONS -->

    </div>


    <div class="portlet-body form">
        <form class="form-horizontal form-bordered">

            <div class="form-body">

            <div class="portlet light">

                <div class="row">

                    <div class="col-md-8">

                        <div class="portlet light bg-inverse bordered " style="min-height: 395px;">
                            <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_DADOSDETALHE'); ?></div></div>
                            <div class="portlet-body">

                                <div class="col-md-6">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_LISTA_REFERENCIA' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->codigo, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info ">
                                        <div class="col-md-12 name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_LISTA_ESTADO' ); ?></div>
                                        <div class="col-md-6 value ValorEstadoAtualStatic" data-vd-url-get="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=cultura.getNewEstadoName4ManagerByAjax&cultura_id='. $this->escape($this->data->cultura_id)); ?>" >
                                            <span class="label <?php echo VirtualDeskSiteCulturaHelper::getCulturaEstadoCSS($this->data->idestado); ?>"><?php echo htmlentities( $this->data->estado, ENT_QUOTES, 'UTF-8');?> </span></div>

                                        <?php
                                        $checkAlterarEstado4Managers = $objCheckPerm->checkFunctionAccess('cultura', 'alterarestado4managers');
                                        if($checkAlterarEstado4Managers===true ) : ?>
                                        <div class="col-md-6 changeState">
                                            <button class="btn btn-circle btn-outline green btn-sm btAbrirModal btAlterarEstadoModalOpen " type="button"><i class="fa fa-pencil"></i>  <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE_ESTADO' ); ?></button>
                                        </div>

                                        <div class="modal fade" id="AlterarNewEstadoModal" tabindex="-1" role="basic" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title"><?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_CHANGE_ESTADO_ATUAL' ); ?></h4>
                                                    </div>
                                                    <div class="modal-body blocoAlterar2NewEstado">


                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group" style="padding:0">
                                                                        <label> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_CHANGE_ESTADO_ATUAL' ).$labelseparator; ?></label>
                                                                        <div style="padding:0;">
                                                                            <select name="Alterar2NewEstadoId" class="bs-select form-control Alterar2NewEstadoId" data-show-subtext="true">
                                                                                <?php foreach($CulturaEstadoList2Change as $rowEstado) : ?>
                                                                                    <option value="<?php echo $rowEstado['id']; ?>" <?php if((int)$rowEstado['id']==$this->data->idestado) echo 'selected';?> ><?php echo $rowEstado['name']; ?></option>
                                                                                <?php endforeach; ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 ">
                                                                    <div class="form-group">
                                                                        <label><?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_DESCRICAO_OPCIONAL' ); ?></label>
                                                                        <textarea rows="5" class="form-control Alterar2NewEstadoDesc" name="Alterar2NewEstadoDesc" ></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group blocoIconsMsgAviso">
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span> <i class="fa"></i> </span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <span class="blocoIconsMsgAviso_Texto"></span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Fechar</button>
                                                        <button class="btn green alterar2newestadoSend" type="button" name"Alterar" data-vd-url-send="<?php echo JRoute::_('index.php?option=com_virtualdesk&task=cultura.sendAlterar2NewEstado4ManagerByAjax&cultura_id=' . $this->escape($this->data->cultura_id)); ?>" >
                                                        <?php echo JText::_( 'COM_VIRTUALDESK_ALERTA_CHANGE' ); ?>
                                                        </button>
                                                        <span> <i class="fa"></i> </span>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                        <?php endif; ?>

                                    </div>
                                </div>

                                <?php
                                    if($this->data->coOrganizacao == 1){
                                        ?>
                                            <div class="col-md-12">
                                                <div class="row static-info ">
                                                    <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_COORGANIZACAO_LABEL' ); ?></div>
                                                    <div class="value"> <?php echo $copyrightAPP;?> </div>
                                                </div>
                                            </div>
                                        <?php
                                    }
                                ?>

                                <div class="col-md-12">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_NOMEEVENTO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->nome_evento, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_DESCRICAO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->desc_evento);?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_CATEGORIA_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->categoria, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_LOCALEVENTO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->local_evento, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_DATAINICIO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->data_inicio, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_HORAINICIO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->hora_inicio, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_DATAFIM_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->data_fim, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_HORAFIM_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->hora_fim, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_FREGUESIAS_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->freguesia, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_LAYOUT' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->layout_evento, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_WEBSITEEVENTO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->website, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_FACEBOOKEVENTO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->facebook, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_INSTAGRAMEVENTO_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->instagram, ENT_QUOTES, 'UTF-8');?></div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="row static-info">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_OBSERVACOES' ); ?></div>
                                        <div class="value"> <?php echo VirtualDeskSiteGeneralHelper::decodeDatabase2HTML($this->data->observacoes); ?></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row static-info">

                                    </div>
                                </div>


                                <div class="row static-info">
                                    <div class="col-md-12 ">
                                        <h6>
                                            <?php
                                            echo JText::_( 'COM_VIRTUALDESK_CULTURA_LISTA_DATACRIACAO' ).$labelseparator;
                                            echo htmlentities( $this->data->data_criacao, ENT_QUOTES, 'UTF-8');
                                            if($this->data->data_criacao != $this->data->data_alteracao) echo ' , ' . JText::_( 'COM_VIRTUALDESK_CULTURA_LISTA_DATAALTERACAO' ) . ' ' . htmlentities( $this->data->data_alteracao, ENT_QUOTES, 'UTF-8');
                                            ?>
                                        </h6>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="portlet light bg-inverse bordered">
                            <div class="portlet-title "><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_INFO_PROMOTOR'); ?></div></div>
                            <div class="portlet-body">

                                <div class="col-md-12">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_FISCALID_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->nif_evento, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_NOME_LABEL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->promotor_nome, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row static-info ">
                                        <div class="name"> <?php echo JText::_( 'COM_VIRTUALDESK_CULTURA_EMAIL' ); ?></div>
                                        <div class="value"> <?php echo htmlentities( $this->data->promotor_email, ENT_QUOTES, 'UTF-8');?> </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="portlet light bg-inverse bordered">
                            <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_MAPA'); ?></div></div>
                            <div class="portlet-body">
                                <div>
                                    <div id="gmap_marker" class="gmaps" style="position: relative; overflow: hidden;"></div>

                                    <input type="hidden" required class="form-control" name="coordenadas" id="coordenadas"
                                           value="<?php echo htmlentities($this->data->latitude . ',' . $this->data->longitude , ENT_QUOTES, 'UTF-8'); ?>"/>
                                </div>
                            </div>
                        </div>


                        <div class="portlet light bg-inverse bordered">
                            <div class="portlet-title"><div class="caption"><?php echo JText::_('COM_VIRTUALDESK_CULTURA_IMAGENS'); ?></div></div>
                            <div class="portlet-body">
                                <div id="vdCulturaFileGrid" class="cbp">
                                    <?php
                                    if(!is_array($ListFilesCultura)) $ListFilesCultura = array();
                                    foreach ($ListFilesCultura as $rowFile) : ?>
                                        <div class="cbp-item identity logos">
                                            <a href="<?php echo $rowFile->guestlink; ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $rowFile->filename; ?>">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="<?php echo $rowFile->guestlink; ?>" alt=""> </div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title"><?php echo $rowFile->filename; ?></div>
                                                            <div class="cbp-l-caption-desc"><?php echo $rowFile->desc; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn green" href="<?php echo JRoute::_('index.php?option=com_virtualdesk&view=cultura&layout=edit4manager&cultura_id=' . $this->escape($this->data->cultura_id)); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?>"><?php echo JText::_('COM_VIRTUALDESK_EDITAR'); ?></a>
                        <a class="btn default" href="<?php  echo JRoute::_('index.php?option=com_virtualdesk&view=cultura'); ?>" title="<?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?>"><?php echo JText::_('COM_VIRTUALDESK_GOBACK'); ?></a>
                    </div>
                </div>
            </div>

            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('cultura_id',$setencrypt_forminputhidden); ?>"           value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->cultura_id) ,$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('cultura_idtype',$setencrypt_forminputhidden); ?>"       value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->type),$setencrypt_forminputhidden); ?>"/>
            <input type="hidden" name="<?php echo $obVDCrypt->formInputNameEncrypt('cultura_idcategory',$setencrypt_forminputhidden); ?>"   value="<?php echo $obVDCrypt->formInputValueEncrypt($this->escape($this->data->category) ,$setencrypt_forminputhidden); ?>"/>

        </form>
    </div>
</div>


<?php echo $localScripts;

echo ('<script>');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/cultura/tmpl/maps.js.php');
require_once (JPATH_SITE . '/components/com_virtualdesk/views/cultura/tmpl/view4manager.js.php');
echo ('</script>');
?>