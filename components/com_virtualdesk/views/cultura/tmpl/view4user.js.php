<?php
defined('_JEXEC') or die;

// Se o estado não for o inicial disable o botão editar
$jsIdEstadoInicial = (int) VirtualDeskSiteCulturaHelper::getEstadoIdInicio();
$jsIdEstadoAtual   = (int)$this->data->idestado;

$jsDesligarBotaoEditar = true;
if($jsIdEstadoInicial == $jsIdEstadoAtual && $jsIdEstadoAtual>0) $jsDesligarBotaoEditar = false;

?>


var PortofolioHandle = function () {

    var initCulturaFileGrid = function (evt) {

        // init cubeportfolio
        jQuery('#vdCulturaFileGrid').cubeportfolio({
            //filters: '#js-filters-juicy-projects',
            //loadMore: '#js-loadMore-juicy-projects',
            //loadMoreAction: 'auto',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: '',
            gapHorizontal: 35,
            gapVertical: 30,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 5
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 80,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        });

    };

    var destroyCulturaFileGrid = function (evt) {
        // init cubeportfolio
        jQuery('#vdCulturaFileGrid').cubeportfolio('destroy');
    };



    return {
        //main function to initiate the module
        init: function () {
            initCulturaFileGrid();
        },
        initCulturaFileGrid     :  initCulturaFileGrid,
        destroyCulturaFileGrid  : destroyCulturaFileGrid
    };
}();


jQuery(document).ready(function() {

    PortofolioHandle.init();

    <?php if($jsDesligarBotaoEditar===true) : ?>
    jQuery(".vdBotaoEditar").attr('disabled','disabled');
    jQuery("a.vdBotaoEditar").attr('href','').attr('onclick','return false;');
    <?php endif; ?>

});